* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslr_bgm                                                        *
*              Genera tabelle mirror                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-04-14                                                      *
* Last revis.: 2009-04-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPar
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgslr_bgm",oParentObject,m.pPar)
return(i_retval)

define class tgslr_bgm as StdBatch
  * --- Local variables
  pPar = space(1)
  w_ARCHIVIO = space(30)
  w_RES = space(254)
  w_PADRE = .NULL.
  w_POS = 0
  w_OBJ = .NULL.
  * --- WorkFile variables
  SUENMAST_idx=0
  TAB_SECO_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Genera le tabelle Mirror per le entit�
    *     Cicla sulle entit� definite e per ognuna genera una tabella sul DB
    *     con:
    *     a) Campi chiave
    *     b) Ancestor (CPCCCHK dell'ultima validazione)
    *     c) Codice sede di pubblicazione
    *     d) ultimo CPCCCHK importato
    this.w_PADRE = this.oParentObject
    NC=This.oParentObject.w_SUPERENT.cCursor 
 SELECT (NC)
    this.w_POS = RECNO()
    this.oParentObject.w_Msg = ""
    do case
      case this.pPar="C"
        * --- Il cursore  SECONDARIE conterra i nomi di tutte le tabelel secondarie di mirror che dovranno essere aggiornate. Il cursore SECONDARIE  conterra tutte le tabelle secondarie che dovranno essere aggiorante 
        *     Le istanze di tabelle secondarie di mirror che compaiono pi� volte all interno del cursore SECONDARIE  verranno aggiorante una sola volta 
        Create cursor SECONDARIE (TSCODTAB char (30))
        WRCURSOR ("SECONDARIE")
        * --- Assegna il focus ad un campo che si trova a pag 1. Si posiziona il focus a pagina 1 per vedere ii messaggi di avanzamento elaborazioni che venogono scritti nel campo memo che si trova a pag1
        this.w_PADRE.opgfrm.activepage = 1
        this.w_OBJ = this.oparentobject.getctrl("w_ELIMINA")
        this.w_OBJ.setfocus()     
        * --- Genera le tabelle di mirror per tutte le super entit� selezionate
        SELECT (NC) 
 Go TOP 
 SCAN FOR XCHK=1
        this.w_ARCHIVIO = SUCODICE
        this.w_RES = CREATBLM( this.w_ARCHIVIO, this.oParentObject.w_ELIMINA="S", this.oParentObject.w_VERBOSE, this.w_PADRE, .F.) 
        if Not Empty ( this.w_RES )
          * --- Gestione log errori generazione tabelle Mirror
          AddMsgNL("%1",this, this.w_RES , , , ,, )
        else
          AddMsgNL("Creato con successo archivio di mirror per super entit� %1",this, this.w_ARCHIVIO , , , ,, )
        endif
        if this.oParentObject.w_VERBOSE
          AddMsgNl("%1",this, Repl("-",50) , , , ,, )
        endif
        * --- Ricerca le tabelle secondarie di mirror da aggiornare. Le tabelle secondarie di mirror vengono memorizzate nel cursore SECONDARIE dopodich�  verranno aggiornate
        *     Se una tabella secondaria di mirror compare piu volte all interno del cursore SECONDARIE allora verr� aggiornata una sola volta
        * --- Select from TAB_SECO
        i_nConn=i_TableProp[this.TAB_SECO_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TAB_SECO_idx,2],.t.,this.TAB_SECO_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select TSCODTAB  from "+i_cTable+" TAB_SECO ";
              +" where TSCODICE="+cp_ToStrODBC(this.w_ARCHIVIO)+"";
               ,"_Curs_TAB_SECO")
        else
          select TSCODTAB from (i_cTable);
           where TSCODICE=this.w_ARCHIVIO;
            into cursor _Curs_TAB_SECO
        endif
        if used('_Curs_TAB_SECO')
          select _Curs_TAB_SECO
          locate for 1=1
          do while not(eof())
          INSERT INTO SECONDARIE values (_Curs_TAB_SECO.TSCODTAB)
            select _Curs_TAB_SECO
            continue
          enddo
          use
        endif
        SELECT (NC) 
 ENDSCAN
        * --- Genera le tabelle secondarie di mirror
        SELECT DISTINCT TSCODTAB FROM SECONDARIE into cursor AGGTAB 
 SELECT AGGTAB 
 Go TOP 
 SCAN
        * --- Crea mirror per le tabelle secondarie gestite con mirror
        this.w_ARCHIVIO = AGGTAB.TSCODTAB
        this.w_RES = CREATBLM( this.w_ARCHIVIO, this.oParentObject.w_ELIMINA="S", this.oParentObject.w_VERBOSE, this.w_PADRE, .T.) 
        if Not Empty ( this.w_RES )
          * --- Gestione log errori generazione tabelle Mirror
          AddMsgNL("%1",this, this.w_RES , , , ,, )
        else
          AddMsgNL("Creato con successo archivio di mirror per TSM %1",this, this.w_ARCHIVIO , , , ,, )
        endif
        if this.oParentObject.w_VERBOSE
          AddMsgNl("%1",this, Repl("-",50) , , , ,, )
        endif
        SELECT AGGTAB 
 ENDSCAN
        this.oParentObject.w_EDIT = .F.
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pPar="S"
        SELECT (NC) 
 Go TOP
        if this.oParentObject.w_SELEZI="S"
          UPdate (NC) set XCHK=1
        else
          UPdate (NC) set XCHK=0
        endif
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    SELECT (NC)
    * --- Riposiziona il cursore dello zoom delle supe entit�
    if this.w_POS<= RECCOUNT() AND this.w_POS>0 
      Go this.w_POS
    endif
    if USED ("SECONDARIE")
      SELECT SECONDARIE 
 USE
    endif
    if USED ("AGGTAB")
      SELECT AGGTAB 
 USE
    endif
  endproc


  proc Init(oParentObject,pPar)
    this.pPar=pPar
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='SUENMAST'
    this.cWorkTables[2]='TAB_SECO'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_TAB_SECO')
      use in _Curs_TAB_SECO
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPar"
endproc
