* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslr_mes                                                        *
*              Tabelle estese                                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-05-13                                                      *
* Last revis.: 2017-07-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgslr_mes"))

* --- Class definition
define class tgslr_mes as StdTrsForm
  Top    = 110
  Left   = 8

  * --- Standard Properties
  Width  = 791
  Height = 310+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2017-07-27"
  HelpContextID=63532137
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=9

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  TAB_ESTE_IDX = 0
  XDC_TABLE_IDX = 0
  cFile = "TAB_ESTE"
  cKeySelect = "ESCODTAB"
  cKeyWhere  = "ESCODTAB=this.w_ESCODTAB"
  cKeyDetail  = "ESCODTAB=this.w_ESCODTAB and ES_TABLE=this.w_ES_TABLE and ESTABALI=this.w_ESTABALI"
  cKeyWhereODBC = '"ESCODTAB="+cp_ToStrODBC(this.w_ESCODTAB)';

  cKeyDetailWhereODBC = '"ESCODTAB="+cp_ToStrODBC(this.w_ESCODTAB)';
      +'+" and ES_TABLE="+cp_ToStrODBC(this.w_ES_TABLE)';
      +'+" and ESTABALI="+cp_ToStrODBC(this.w_ESTABALI)';

  cKeyWhereODBCqualified = '"TAB_ESTE.ESCODTAB="+cp_ToStrODBC(this.w_ESCODTAB)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cPrg = "gslr_mes"
  cComment = "Tabelle estese"
  i_nRowNum = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  cAutoZoom = 'GSLR_MES'
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_ESCODTAB = space(30)
  o_ESCODTAB = space(30)
  w_ES_TABLE = space(30)
  o_ES_TABLE = space(30)
  w_ESTABALI = space(30)
  o_ESTABALI = space(30)
  w_ESRELAZI = space(254)
  o_ESRELAZI = space(254)
  w_DESCRI = space(254)
  w_CONTROLLO = .F.
  w_MESS = space(254)
  w_TABPRINC = space(10)
  w_NRIGA = space(10)

  * --- Children pointers
  GSLR_MCE = .NULL.
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'TAB_ESTE','gslr_mes')
    stdPageFrame::Init()
    *set procedure to GSLR_MCE additive
    with this
      .Pages(1).addobject("oPag","tgslr_mesPag1","gslr_mes",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Tabelle estese")
      .Pages(1).HelpContextID = 67185754
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oESCODTAB_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure GSLR_MCE
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='XDC_TABLE'
    this.cWorkTables[2]='TAB_ESTE'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.TAB_ESTE_IDX,5],7]
    this.nPostItConn=i_TableProp[this.TAB_ESTE_IDX,3]
  return

  function CreateChildren()
    this.GSLR_MCE = CREATEOBJECT('stdDynamicChild',this,'GSLR_MCE',this.oPgFrm.Page1.oPag.oLinkPC_2_3)
    this.GSLR_MCE.createrealchild()
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSLR_MCE)
      this.GSLR_MCE.DestroyChildrenChain()
      this.GSLR_MCE=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_2_3')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSLR_MCE.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSLR_MCE.IsAChildUpdated(.t.)
    endif	
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSLR_MCE.NewDocument()
    return

  procedure ChildrenChangeRow()
    local i_cOldSel,i_cRow
    i_cOldSel=select()
    select (this.cTrsName)
    i_cRow=str(recno(),7,0)
    with this
      .GSLR_MCE.ChangeRow(this.cRowID+i_cRow,1;
             ,.w_ESCODTAB,"CMCODTAB";
             ,.w_ES_TABLE,"CM_TABLE";
             ,.w_ESTABALI,"CMTABALI";
             )
      .WriteTo_GSLR_MCE()
    endwith
    select (i_cOldSel)
    return

procedure WriteTo_GSLR_MCE()
  if at('gslr_mce',lower(this.GSLR_MCE.class))<>0
    if this.GSLR_MCE.w_CM_TABLE<>this.w_ES_TABLE or this.GSLR_MCE.w_CMTABALI<>this.w_ESTABALI or this.GSLR_MCE.w_CMCODTAB<>this.w_ESCODTAB
      this.GSLR_MCE.w_CM_TABLE = this.w_ES_TABLE
      this.GSLR_MCE.w_CMTABALI = this.w_ESTABALI
      this.GSLR_MCE.w_CMCODTAB = this.w_ESCODTAB
      this.GSLR_MCE.mCalc(.t.)
    endif
  endif
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_ESCODTAB = NVL(ESCODTAB,space(30))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_1_joined
    link_1_1_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from TAB_ESTE where ESCODTAB=KeySet.ESCODTAB
    *                            and ES_TABLE=KeySet.ES_TABLE
    *                            and ESTABALI=KeySet.ESTABALI
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.TAB_ESTE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TAB_ESTE_IDX,2],this.bLoadRecFilter,this.TAB_ESTE_IDX,"gslr_mes")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('TAB_ESTE')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "TAB_ESTE.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' TAB_ESTE '
      link_1_1_joined=this.AddJoinedLink_1_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'ESCODTAB',this.w_ESCODTAB  )
      select * from (i_cTable) TAB_ESTE where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DESCRI = space(254)
        .w_CONTROLLO = .f.
        .w_MESS = space(254)
        .w_ESCODTAB = NVL(ESCODTAB,space(30))
          if link_1_1_joined
            this.w_ESCODTAB = NVL(TBNAME101,NVL(this.w_ESCODTAB,space(30)))
            this.w_DESCRI = NVL(TBCOMMENT101,space(254))
          else
          .link_1_1('Load')
          endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'TAB_ESTE')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_ES_TABLE = NVL(ES_TABLE,space(30))
          * evitabile
          *.link_2_1('Load')
          .w_ESTABALI = NVL(ESTABALI,space(30))
          .w_ESRELAZI = NVL(ESRELAZI,space(254))
        .w_TABPRINC = .w_ES_TABLE
        .w_NRIGA = alltrim (STRTRAN (alltrim (.w_ESTABALI),alltrim(.w_ES_TABLE),''))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace ES_TABLE with .w_ES_TABLE
          replace ESTABALI with .w_ESTABALI
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page1.oPag.oBtn_1_4.enabled = .oPgFrm.Page1.oPag.oBtn_1_4.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_ESCODTAB=space(30)
      .w_ES_TABLE=space(30)
      .w_ESTABALI=space(30)
      .w_ESRELAZI=space(254)
      .w_DESCRI=space(254)
      .w_CONTROLLO=.f.
      .w_MESS=space(254)
      .w_TABPRINC=space(10)
      .w_NRIGA=space(10)
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_ESCODTAB))
         .link_1_1('Full')
        endif
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_ES_TABLE))
         .link_2_1('Full')
        endif
        .DoRTCalc(3,7,.f.)
        .w_TABPRINC = .w_ES_TABLE
        .w_NRIGA = alltrim (STRTRAN (alltrim (.w_ESTABALI),alltrim(.w_ES_TABLE),''))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'TAB_ESTE')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page1.oPag.oBtn_1_4.enabled = this.oPgFrm.Page1.oPag.oBtn_1_4.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oESCODTAB_1_1.enabled = i_bVal
      .Page1.oPag.oESRELAZI_2_4.enabled = i_bVal
      .Page1.oPag.oBtn_1_4.enabled = .Page1.oPag.oBtn_1_4.mCond()
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oESCODTAB_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oESCODTAB_1_1.enabled = .t.
      endif
    endwith
    this.GSLR_MCE.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'TAB_ESTE',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSLR_MCE.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.TAB_ESTE_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ESCODTAB,"ESCODTAB",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.TAB_ESTE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TAB_ESTE_IDX,2])
    i_lTable = "TAB_ESTE"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.TAB_ESTE_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select distinct "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select distinct &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSLR_STE with this
    endif
    return

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_ES_TABLE C(30);
      ,t_ESTABALI C(30);
      ,t_ESRELAZI C(254);
      ,ES_TABLE C(30);
      ,ESTABALI C(30);
      ,t_TABPRINC C(10);
      ,t_NRIGA C(10);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgslr_mesbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oES_TABLE_2_1.controlsource=this.cTrsName+'.t_ES_TABLE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oESTABALI_2_2.controlsource=this.cTrsName+'.t_ESTABALI'
    this.oPgFRm.Page1.oPag.oESRELAZI_2_4.controlsource=this.cTrsName+'.t_ESRELAZI'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(202)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oES_TABLE_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.TAB_ESTE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TAB_ESTE_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.TAB_ESTE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TAB_ESTE_IDX,2])
      *
      * insert into TAB_ESTE
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'TAB_ESTE')
        i_extval=cp_InsertValODBCExtFlds(this,'TAB_ESTE')
        i_cFldBody=" "+;
                  "(ESCODTAB,ES_TABLE,ESTABALI,ESRELAZI,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBCNull(this.w_ESCODTAB)+","+cp_ToStrODBCNull(this.w_ES_TABLE)+","+cp_ToStrODBC(this.w_ESTABALI)+","+cp_ToStrODBC(this.w_ESRELAZI)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'TAB_ESTE')
        i_extval=cp_InsertValVFPExtFlds(this,'TAB_ESTE')
        cp_CheckDeletedKey(i_cTable,0,'ESCODTAB',this.w_ESCODTAB,'ES_TABLE',this.w_ES_TABLE,'ESTABALI',this.w_ESTABALI)
        INSERT INTO (i_cTable) (;
                   ESCODTAB;
                  ,ES_TABLE;
                  ,ESTABALI;
                  ,ESRELAZI;
                  ,CPCCCHK &i_extfld.) VALUES (;
                  this.w_ESCODTAB;
                  ,this.w_ES_TABLE;
                  ,this.w_ESTABALI;
                  ,this.w_ESRELAZI;
                  ,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.TAB_ESTE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TAB_ESTE_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (NOT EMPTY (t_ES_TABLE) ) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'TAB_ESTE')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and ES_TABLE="+cp_ToStrODBC(&i_TN.->ES_TABLE)+;
                 " and ESTABALI="+cp_ToStrODBC(&i_TN.->ESTABALI)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'TAB_ESTE')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and ES_TABLE=&i_TN.->ES_TABLE;
                      and ESTABALI=&i_TN.->ESTABALI;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (NOT EMPTY (t_ES_TABLE) ) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete row children
              this.GSLR_MCE.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
                     ,this.w_ESCODTAB,"CMCODTAB";
                     ,this.w_ES_TABLE,"CM_TABLE";
                     ,this.w_ESTABALI,"CMTABALI";
                     )
              this.GSLR_MCE.mDelete()
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and ES_TABLE="+cp_ToStrODBC(&i_TN.->ES_TABLE)+;
                            " and ESTABALI="+cp_ToStrODBC(&i_TN.->ESTABALI)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and ES_TABLE=&i_TN.->ES_TABLE;
                            and ESTABALI=&i_TN.->ESTABALI;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = 0
              replace ES_TABLE with this.w_ES_TABLE
              replace ESTABALI with this.w_ESTABALI
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update TAB_ESTE
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'TAB_ESTE')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " ESRELAZI="+cp_ToStrODBC(this.w_ESRELAZI)+;
                     ",ES_TABLE="+cp_ToStrODBC(this.w_ES_TABLE)+;
                     ",ESTABALI="+cp_ToStrODBC(this.w_ESTABALI)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+;
                             " and ES_TABLE="+cp_ToStrODBC(ES_TABLE)+;
                             " and ESTABALI="+cp_ToStrODBC(ESTABALI)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'TAB_ESTE')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      ESRELAZI=this.w_ESRELAZI;
                     ,ES_TABLE=this.w_ES_TABLE;
                     ,ESTABALI=this.w_ESTABALI;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                                      and ES_TABLE=&i_TN.->ES_TABLE;
                                      and ESTABALI=&i_TN.->ESTABALI;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
      * --- Ask to row children to save themselves
      select (this.cTrsName)
      i_TN = this.cTrsName
      scan for (NOT EMPTY (t_ES_TABLE) )
        * --- > Optimize children saving
        i_nRec = recno()
        this.WorkFromTrs()
        if not(deleted())
          this.GSLR_MCE.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
               ,this.w_ESCODTAB,"CMCODTAB";
               ,this.w_ES_TABLE,"CM_TABLE";
               ,this.w_ESTABALI,"CMTABALI";
               )
          this.GSLR_MCE.mReplace()
          this.GSLR_MCE.bSaveContext=.f.
        endif
      endscan
     this.GSLR_MCE.bSaveContext=.t.
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.TAB_ESTE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TAB_ESTE_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (NOT EMPTY (t_ES_TABLE) ) and I_SRV<>'A'
        this.WorkFromTrs()
        i_nRec = recno()
        * --- GSLR_MCE : Deleting
        this.GSLR_MCE.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
               ,this.w_ESCODTAB,"CMCODTAB";
               ,this.w_ES_TABLE,"CM_TABLE";
               ,this.w_ESTABALI,"CMTABALI";
               )
        this.GSLR_MCE.mDelete()
        if bTrsErr
          i_nModRow = -1
          exit
        endif
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete TAB_ESTE
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and ES_TABLE="+cp_ToStrODBC(&i_TN.->ES_TABLE)+;
                            " and ESTABALI="+cp_ToStrODBC(&i_TN.->ESTABALI)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and ES_TABLE=&i_TN.->ES_TABLE;
                              and ESTABALI=&i_TN.->ESTABALI;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (NOT EMPTY (t_ES_TABLE) ) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.TAB_ESTE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TAB_ESTE_IDX,2])
    if i_bUpd
      with this
        if  .o_ES_TABLE<>.w_ES_TABLE.or. .o_ESTABALI<>.w_ESTABALI.or. .o_ESCODTAB<>.w_ESCODTAB
          .WriteTo_GSLR_MCE()
        endif
        if .o_ES_TABLE<>.w_ES_TABLE
          .Calculate_ULMAIQAMEK()
        endif
        if .o_ESTABALI<>.w_ESTABALI
          .Calculate_QPDCUBBPSR()
        endif
        .DoRTCalc(1,7,.t.)
          .w_TABPRINC = .w_ES_TABLE
        if .o_ES_TABLE<>.w_ES_TABLE.or. .o_ESCODTAB<>.w_ESCODTAB
          .Calculate_UAFMWRGRYO()
        endif
        if .o_ESRELAZI<>.w_ESRELAZI
          .Calculate_JQRLKQBTSE()
        endif
        if .o_ESTABALI<>.w_ESTABALI
          .w_NRIGA = alltrim (STRTRAN (alltrim (.w_ESTABALI),alltrim(.w_ES_TABLE),''))
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_TABPRINC with this.w_TABPRINC
      replace t_NRIGA with this.w_NRIGA
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return
  proc Calculate_ULMAIQAMEK()
    with this
          * --- Inizializza alias tabella
          GSLR_BCC(this;
              ,'D';
             )
    endwith
  endproc
  proc Calculate_QEDLPGUXZP()
    with this
          * --- Verifica che non esistano 2 alias di campo uguli (altrimenti in fase di creazione query si verificherebbe un errore)
          GSLR_BCC(this;
              ,'C';
             )
    endwith
  endproc
  proc Calculate_QPDCUBBPSR()
    with this
          * --- aggiorna  relazione
          .w_ESRELAZI = strtran (' ' + alltrim(.w_ESRELAZI) , ' '+alltrim(.o_ESTABALI)+'.' , ' '+alltrim (.w_ESTABALI)+'.')
    endwith
  endproc
  proc Calculate_UAFMWRGRYO()
    with this
          * --- Cerca relazione
          .w_ESRELAZI = FINDLINK(.w_ES_TABLE, .w_ESCODTAB , 0,  .T.)
    endwith
  endproc
  proc Calculate_JQRLKQBTSE()
    with this
          * --- aggiorna  relazione
          .w_ESRELAZI = IIF ( EMPTY (.w_ESTABALI) ,.w_ESRELAZI, strtran (' ' + alltrim(.w_ESRELAZI) , ' '+alltrim(.w_ES_TABLE)+'.' , ' '+alltrim (.w_ESTABALI)+'.'))
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_4.enabled = this.oPgFrm.Page1.oPag.oBtn_1_4.mCond()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oESRELAZI_2_4.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oESRELAZI_2_4.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Controlli")
          .Calculate_QEDLPGUXZP()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=ESCODTAB
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.XDC_TABLE_IDX,3]
    i_lTable = "XDC_TABLE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.XDC_TABLE_IDX,2], .t., this.XDC_TABLE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.XDC_TABLE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ESCODTAB) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'XDC_TABLE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TBNAME like "+cp_ToStrODBC(trim(this.w_ESCODTAB)+"%");

          i_ret=cp_SQL(i_nConn,"select TBNAME,TBCOMMENT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TBNAME","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TBNAME',trim(this.w_ESCODTAB))
          select TBNAME,TBCOMMENT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TBNAME into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ESCODTAB)==trim(_Link_.TBNAME) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ESCODTAB) and !this.bDontReportError
            deferred_cp_zoom('XDC_TABLE','*','TBNAME',cp_AbsName(oSource.parent,'oESCODTAB_1_1'),i_cWhere,'',"Archivi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,TBCOMMENT";
                     +" from "+i_cTable+" "+i_lTable+" where TBNAME="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TBNAME',oSource.xKey(1))
            select TBNAME,TBCOMMENT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ESCODTAB)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,TBCOMMENT";
                   +" from "+i_cTable+" "+i_lTable+" where TBNAME="+cp_ToStrODBC(this.w_ESCODTAB);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TBNAME',this.w_ESCODTAB)
            select TBNAME,TBCOMMENT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ESCODTAB = NVL(_Link_.TBNAME,space(30))
      this.w_DESCRI = NVL(_Link_.TBCOMMENT,space(254))
    else
      if i_cCtrl<>'Load'
        this.w_ESCODTAB = space(30)
      endif
      this.w_DESCRI = space(254)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.XDC_TABLE_IDX,2])+'\'+cp_ToStr(_Link_.TBNAME,1)
      cp_ShowWarn(i_cKey,this.XDC_TABLE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ESCODTAB Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.XDC_TABLE_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.XDC_TABLE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_1.TBNAME as TBNAME101"+ ",link_1_1.TBCOMMENT as TBCOMMENT101"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_1 on TAB_ESTE.ESCODTAB=link_1_1.TBNAME"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_1"
          i_cKey=i_cKey+'+" and TAB_ESTE.ESCODTAB=link_1_1.TBNAME(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ES_TABLE
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.XDC_TABLE_IDX,3]
    i_lTable = "XDC_TABLE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.XDC_TABLE_IDX,2], .t., this.XDC_TABLE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.XDC_TABLE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ES_TABLE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'XDC_TABLE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TBNAME like "+cp_ToStrODBC(trim(this.w_ES_TABLE)+"%");

          i_ret=cp_SQL(i_nConn,"select TBNAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TBNAME","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TBNAME',trim(this.w_ES_TABLE))
          select TBNAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TBNAME into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ES_TABLE)==trim(_Link_.TBNAME) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ES_TABLE) and !this.bDontReportError
            deferred_cp_zoom('XDC_TABLE','*','TBNAME',cp_AbsName(oSource.parent,'oES_TABLE_2_1'),i_cWhere,'',"Archivi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME";
                     +" from "+i_cTable+" "+i_lTable+" where TBNAME="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TBNAME',oSource.xKey(1))
            select TBNAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ES_TABLE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME";
                   +" from "+i_cTable+" "+i_lTable+" where TBNAME="+cp_ToStrODBC(this.w_ES_TABLE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TBNAME',this.w_ES_TABLE)
            select TBNAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ES_TABLE = NVL(_Link_.TBNAME,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_ES_TABLE = space(30)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.XDC_TABLE_IDX,2])+'\'+cp_ToStr(_Link_.TBNAME,1)
      cp_ShowWarn(i_cKey,this.XDC_TABLE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ES_TABLE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oESCODTAB_1_1.value==this.w_ESCODTAB)
      this.oPgFrm.Page1.oPag.oESCODTAB_1_1.value=this.w_ESCODTAB
    endif
    if not(this.oPgFrm.Page1.oPag.oESRELAZI_2_4.value==this.w_ESRELAZI)
      this.oPgFrm.Page1.oPag.oESRELAZI_2_4.value=this.w_ESRELAZI
      replace t_ESRELAZI with this.oPgFrm.Page1.oPag.oESRELAZI_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_2.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_2.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oES_TABLE_2_1.value==this.w_ES_TABLE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oES_TABLE_2_1.value=this.w_ES_TABLE
      replace t_ES_TABLE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oES_TABLE_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oESTABALI_2_2.value==this.w_ESTABALI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oESTABALI_2_2.value=this.w_ESTABALI
      replace t_ESTABALI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oESTABALI_2_2.value
    endif
    cp_SetControlsValueExtFlds(this,'TAB_ESTE')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gslr_mes
      
      if i_bRes
         this.notifyevent("Controlli")
         IF this.w_CONTROLLO
          i_bnoChk = .f.
          i_bRes=.f.
          i_cErrorMsg = this.w_MESS
         ENDIF
      ENDIF
      
      
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    local i_oldarea
    i_oldarea=select()
    select count(*) as cnt from (this.cTrsName);
       where not(deleted()) and (NOT EMPTY (t_ES_TABLE) );
        into cursor __chk__
    if not(max(0,1)<=cnt)
      do cp_ErrorMsg with MSG_YOU_MUST_INSERT_AT_LEAST_ONE_ROW
      use
      select (i_oldarea)
      return(.f.)
    endif
    use
    select (i_oldarea)
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   empty(.w_ESTABALI) and (NOT EMPTY (.w_ES_TABLE) )
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oESTABALI_2_2
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
        case   empty(.w_ESRELAZI) and (NOT EMPTY (.w_ES_TABLE)) and (NOT EMPTY (.w_ES_TABLE) )
          .oNewFocus=.oPgFrm.Page1.oPag.oESRELAZI_2_4
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
      endcase
      i_bRes = i_bRes .and. .GSLR_MCE.CheckForm()
      if NOT EMPTY (.w_ES_TABLE) 
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_ESCODTAB = this.w_ESCODTAB
    this.o_ES_TABLE = this.w_ES_TABLE
    this.o_ESTABALI = this.w_ESTABALI
    this.o_ESRELAZI = this.w_ESRELAZI
    * --- GSLR_MCE : Depends On
    this.GSLR_MCE.SaveDependsOn()
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(NOT EMPTY (t_ES_TABLE) )
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_ES_TABLE=space(30)
      .w_ESTABALI=space(30)
      .w_ESRELAZI=space(254)
      .w_TABPRINC=space(10)
      .w_NRIGA=space(10)
      .DoRTCalc(1,2,.f.)
      if not(empty(.w_ES_TABLE))
        .link_2_1('Full')
      endif
      .DoRTCalc(3,7,.f.)
        .w_TABPRINC = .w_ES_TABLE
        .w_NRIGA = alltrim (STRTRAN (alltrim (.w_ESTABALI),alltrim(.w_ES_TABLE),''))
    endwith
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_ES_TABLE = t_ES_TABLE
    this.w_ESTABALI = t_ESTABALI
    this.w_ESRELAZI = t_ESRELAZI
    this.w_TABPRINC = t_TABPRINC
    this.w_NRIGA = t_NRIGA
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_ES_TABLE with this.w_ES_TABLE
    replace t_ESTABALI with this.w_ESTABALI
    replace t_ESRELAZI with this.w_ESRELAZI
    replace t_TABPRINC with this.w_TABPRINC
    replace t_NRIGA with this.w_NRIGA
    if i_srv='A'
      replace ES_TABLE with this.w_ES_TABLE
      replace ESTABALI with this.w_ESTABALI
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgslr_mesPag1 as StdContainer
  Width  = 787
  height = 310
  stdWidth  = 787
  stdheight = 310
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oESCODTAB_1_1 as StdField with uid="GOPEXKZKRP",rtseq=1,rtrep=.f.,;
    cFormVar = "w_ESCODTAB", cQueryName = "ESCODTAB",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Nome tabella estesa",;
    HelpContextID = 80354184,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=8, Top=10, InputMask=replicate('X',30), bHasZoom = .t. , cLinkFile="XDC_TABLE", oKey_1_1="TBNAME", oKey_1_2="this.w_ESCODTAB"

  func oESCODTAB_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oESCODTAB_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oESCODTAB_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    if not(this.parent.oESCODTAB_1_1.readonly and this.parent.oESCODTAB_1_1.isprimarykey)
    do cp_zoom with 'XDC_TABLE','*','TBNAME',cp_AbsName(this.parent,'oESCODTAB_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Archivi",'',this.parent.oContained
   endif
  endproc

  add object oDESCRI_1_2 as StdField with uid="WRLMTSEBVH",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione Tabella estesa",;
    HelpContextID = 90239690,;
   bGlobalFont=.t.,;
    Height=22, Width=552, Left=233, Top=10, InputMask=replicate('X',254)


  add object oBtn_1_4 as StdButton with uid="ORSWOUHUED",left=11, top=265, width=48,height=45,;
    CpPicture="BMP\TRACCIABILITA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare la treeview delle tabelle che dipendono dalla tabella estesa";
    , HelpContextID = 46217964;
    , TabStop=.f.,Caption='\<Relazioni';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_4.Click()
      with this.Parent.oContained
        GSUT1BCR(this.Parent.oContained,"Entita")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_4.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_ESCODTAB) And Not Empty(.w_ES_TABLE))
    endwith
  endfunc


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=8, top=45, width=406,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=2,Field1="ES_TABLE",Label1="Archivio di estensione",Field2="ESTABALI",Label2="Alias archivio di estensione",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 252558714
  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gslr_mce",lower(this.oContained.GSLR_MCE.class))=0
        this.oContained.GSLR_MCE.createrealchild()
        this.oContained.WriteTo_GSLR_MCE()
      endif
      this.createchildren=.f.
    endif
  endproc

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-2,top=64,;
    width=402+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-1,top=65,width=401+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='XDC_TABLE|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oESRELAZI_2_4.Refresh()
      this.Parent.oContained.ChildrenChangeRow()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='XDC_TABLE'
        oDropInto=this.oBodyCol.oRow.oES_TABLE_2_1
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oLinkPC_2_3 as stdDynamicChildContainer with uid="OZJZVUSRXD",bOnScreen=.t.,width=369,height=243,;
   left=418, top=36;


  add object oESRELAZI_2_4 as StdTrsField with uid="GJUURWZWKP",rtseq=4,rtrep=.t.,;
    cFormVar="w_ESRELAZI",value=space(254),;
    ToolTipText = "Relazione tra la tabella estesa e la tabella che estende",;
    HelpContextID = 230618225,;
    cTotal="", bFixedPos=.t., cQueryName = "ESRELAZI",;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=630, Left=153, Top=286, InputMask=replicate('X',254), bHasZoom = .t. 

  func oESRELAZI_2_4.mCond()
    with this.Parent.oContained
      return (NOT EMPTY (.w_ES_TABLE))
    endwith
  endfunc

  proc oESRELAZI_2_4.mZoom
      with this.Parent.oContained
        GSUT1BCR(this.Parent.oContained,"Entita")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oStr_2_5 as StdString with uid="HLWFXMQGQB",Visible=.t., Left=64, Top=290,;
    Alignment=1, Width=88, Height=18,;
    Caption="Relazione:"  ;
  , bGlobalFont=.t.

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgslr_mesBodyRow as CPBodyRowCnt
  Width=392
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oES_TABLE_2_1 as StdTrsField with uid="DUOZOCDOGI",rtseq=2,rtrep=.t.,;
    cFormVar="w_ES_TABLE",value=space(30),isprimarykey=.t.,;
    ToolTipText = "Nome archivio di estensione",;
    HelpContextID = 224339061,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=193, Left=-2, Top=0, InputMask=replicate('X',30), bHasZoom = .t. , cLinkFile="XDC_TABLE", oKey_1_1="TBNAME", oKey_1_2="this.w_ES_TABLE"

  func oES_TABLE_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oES_TABLE_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oES_TABLE_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    if not(this.parent.oES_TABLE_2_1.readonly and this.parent.oES_TABLE_2_1.isprimarykey)
    do cp_zoom with 'XDC_TABLE','*','TBNAME',cp_AbsName(this.parent,'oES_TABLE_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Archivi",'',this.parent.oContained
   endif
  endproc

  add object oESTABALI_2_2 as StdTrsField with uid="UWUNDJPOOA",rtseq=3,rtrep=.t.,;
    cFormVar="w_ESTABALI",value=space(30),isprimarykey=.t.,;
    ToolTipText = "Aliasarchivio di estensione",;
    HelpContextID = 241357937,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=193, Left=194, Top=0, InputMask=replicate('X',30)
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oES_TABLE_2_1.When()
    return(.t.)
  proc oES_TABLE_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oES_TABLE_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gslr_mes','TAB_ESTE','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".ESCODTAB=TAB_ESTE.ESCODTAB";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
