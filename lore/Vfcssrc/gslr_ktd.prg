* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslr_ktd                                                        *
*              Treeview pacchetto dati                                         *
*                                                                              *
*      Author: Zucchetti SPA                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161][VRS_67]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-09-12                                                      *
* Last revis.: 2008-09-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgslr_ktd",oParentObject))

* --- Class definition
define class tgslr_ktd as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 563
  Height = 591
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-09-13"
  HelpContextID=82406505
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=7

  * --- Constant Properties
  _IDX = 0
  SUENMAST_IDX = 0
  PARALORE_IDX = 0
  cPrg = "gslr_ktd"
  cComment = "Treeview pacchetto dati"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CURSORNA = space(10)
  w_PATH = space(254)
  w_CODAZI = space(5)
  w_PATERR = space(254)
  w_FLGZIP = .F.
  w_SEPWDZIP = space(254)
  w_TESTPATH = space(254)
  w_TreeView = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgslr_ktdPag1","gslr_ktd",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPATH_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_TreeView = this.oPgFrm.Pages(1).oPag.TreeView
    DoDefault()
    proc Destroy()
      this.w_TreeView = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='SUENMAST'
    this.cWorkTables[2]='PARALORE'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CURSORNA=space(10)
      .w_PATH=space(254)
      .w_CODAZI=space(5)
      .w_PATERR=space(254)
      .w_FLGZIP=.f.
      .w_SEPWDZIP=space(254)
      .w_TESTPATH=space(254)
      .oPgFrm.Page1.oPag.oObj_1_1.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
      .oPgFrm.Page1.oPag.TreeView.Calculate()
          .DoRTCalc(1,2,.f.)
        .w_CODAZI = i_CODAZI
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_CODAZI))
          .link_1_12('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_14.Calculate()
          .DoRTCalc(4,4,.f.)
        .w_FLGZIP = .F.
          .DoRTCalc(6,6,.f.)
        .w_TESTPATH = Alltrim(Nvl( .w_TREEVIEW.GetVar('PATH'),' '))
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_1.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
        .oPgFrm.Page1.oPag.TreeView.Calculate()
        .DoRTCalc(1,2,.t.)
            .w_CODAZI = i_CODAZI
          .link_1_12('Full')
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate()
        .DoRTCalc(4,6,.t.)
            .w_TESTPATH = Alltrim(Nvl( .w_TREEVIEW.GetVar('PATH'),' '))
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_1.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
        .oPgFrm.Page1.oPag.TreeView.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_4.enabled = this.oPgFrm.Page1.oPag.oBtn_1_4.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_1.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_5.Event(cEvent)
      .oPgFrm.Page1.oPag.TreeView.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_14.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PARALORE_IDX,3]
    i_lTable = "PARALORE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PARALORE_IDX,2], .t., this.PARALORE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PARALORE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PLCODAZI,PLPATERR";
                   +" from "+i_cTable+" "+i_lTable+" where PLCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PLCODAZI',this.w_CODAZI)
            select PLCODAZI,PLPATERR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.PLCODAZI,space(5))
      this.w_PATERR = NVL(_Link_.PLPATERR,space(254))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_PATERR = space(254)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PARALORE_IDX,2])+'\'+cp_ToStr(_Link_.PLCODAZI,1)
      cp_ShowWarn(i_cKey,this.PARALORE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oPATH_1_3.value==this.w_PATH)
      this.oPgFrm.Page1.oPag.oPATH_1_3.value=this.w_PATH
    endif
    if not(this.oPgFrm.Page1.oPag.oSEPWDZIP_1_16.value==this.w_SEPWDZIP)
      this.oPgFrm.Page1.oPag.oSEPWDZIP_1_16.value=this.w_SEPWDZIP
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgslr_ktdPag1 as StdContainer
  Width  = 559
  height = 591
  stdWidth  = 559
  stdheight = 591
  resizeXpos=349
  resizeYpos=490
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oObj_1_1 as cp_runprogram with uid="MTIFDHWRUR",left=1, top=594, width=167,height=19,;
    caption='GSLR_BTD(Open)',;
   bGlobalFont=.t.,;
    prg="GSLR_BTD('Open')",;
    cEvent = "Calcola",;
    nPag=1;
    , HelpContextID = 61472042

  add object oPATH_1_3 as StdField with uid="APLVUXDHME",rtseq=2,rtrep=.f.,;
    cFormVar = "w_PATH", cQueryName = "PATH",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Path del file zip contenente il pacchetto dati nella cartella locale",;
    HelpContextID = 77325834,;
   bGlobalFont=.t.,;
    Height=21, Width=426, Left=100, Top=13, InputMask=replicate('X',254), bHasZoom = .t. 

  proc oPATH_1_3.mZoom
    this.parent.oContained.w_PATH=left(GETFILE("Ahz", "AdHoc zip", "Open", 0, PROPER (g_APPLICATION) ),200)
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc


  add object oBtn_1_4 as StdButton with uid="ACUBIZKGYY",left=463, top=86, width=48,height=45,;
    CpPicture="BMP\treeview.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare la treeview";
    , HelpContextID = 198466374;
    , caption='\<Esegui';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_4.Click()
      with this.Parent.oContained
        .NotifyEvent('Calcola')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_4.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty(.w_PATH))
      endwith
    endif
  endfunc


  add object oObj_1_5 as cp_runprogram with uid="ENSKRVEEVZ",left=1, top=615, width=167,height=19,;
    caption='GSLR_BTD(Close)',;
   bGlobalFont=.t.,;
    prg="GSLR_BTD('Close')",;
    cEvent = "Done",;
    nPag=1;
    , HelpContextID = 241962


  add object TreeView as cp_Treeview with uid="GYODZTEHZL",left=13, top=86, width=438,height=496,;
    caption='Object',;
   bGlobalFont=.t.,;
    cNodeBmp="tree1.bmp",cLeafBmp="",nIndent=20,cCursor="Vista",cShowFields="ALLTRIM(ENTITA)+' - '+ALLTRIM(DESCRI)",cNodeShowField="",cLeafShowField="",;
    cEvent = "Esegui",;
    nPag=1;
    , HelpContextID = 95591142


  add object oBtn_1_8 as StdButton with uid="UQKMJNJALB",left=463, top=468, width=48,height=45,;
    CpPicture="BMP\DETTAGLI.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare i dati";
    , HelpContextID = 123585121;
    , Caption='\<Dettagli';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_8.Click()
      with this.Parent.oContained
        GSLR_BTD(this.Parent.oContained,"Dett")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_8.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty(.w_TESTPATH))
      endwith
    endif
  endfunc


  add object oBtn_1_9 as StdButton with uid="AYBNNJWSNT",left=463, top=537, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire dalla visualizzazione";
    , HelpContextID = 75089082;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_10 as StdButton with uid="RQSKFAEHAZ",left=463, top=379, width=48,height=45,;
    CpPicture="BMP\ESPLODI.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per esplodere la treeview";
    , HelpContextID = 159773882;
    , caption='E\<splodi';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_10.Click()
      with this.Parent.oContained
        .w_TREEVIEW.ExpandAll(.T.)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_10.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_PATH))
      endwith
    endif
  endfunc


  add object oBtn_1_11 as StdButton with uid="POVNMSFBDI",left=463, top=424, width=48,height=45,;
    CpPicture="BMP\IMPLODI.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per implodere la treeview";
    , HelpContextID = 159775354;
    , caption='\<Implodi';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_11.Click()
      with this.Parent.oContained
        .w_TREEVIEW.ExpandAll(.F.)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_11.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_PATH))
      endwith
    endif
  endfunc


  add object oObj_1_14 as cp_runprogram with uid="BICCTZKADQ",left=2, top=636, width=167,height=19,;
    caption='GSLR_BTD(DxBtn)',;
   bGlobalFont=.t.,;
    prg="GSLR_BTD('DxBtn')",;
    cEvent = "w_treeview NodeRightClick",;
    nPag=1;
    , HelpContextID = 149385770

  add object oSEPWDZIP_1_16 as StdField with uid="OPUKRBYPVI",rtseq=6,rtrep=.f.,;
    cFormVar = "w_SEPWDZIP", cQueryName = "SEPWDZIP",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Password di apertura file zip",;
    HelpContextID = 105718154,;
   bGlobalFont=.t.,;
    Height=21, Width=426, Left=100, Top=48, InputMask=replicate('X',254), PasswordChar="*"


  add object oBtn_1_19 as StdButton with uid="TPKCIVXWSD",left=463, top=263, width=48,height=45,;
    CpPicture="BMP\ELIMINA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eliminare il DBF selezionato dal pacchetto";
    , HelpContextID = 1741894;
    , Caption='Eli\<mina', tabstop=.f.;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_19.Click()
      with this.Parent.oContained
        GSLR_BTD(this.Parent.oContained,"Canc")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_19.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty(.w_TESTPATH))
      endwith
    endif
  endfunc

  add object oStr_1_7 as StdString with uid="KSDUJTFZWN",Visible=.t., Left=4, Top=14,;
    Alignment=1, Width=93, Height=18,;
    Caption="Pacchetto (zip):"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="PAYBJDRUXL",Visible=.t., Left=27, Top=48,;
    Alignment=1, Width=70, Height=18,;
    Caption="Password:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gslr_ktd','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
