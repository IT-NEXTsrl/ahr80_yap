* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslr_apl                                                        *
*              ( lore ) parametri logistica remota                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-06-20                                                      *
* Last revis.: 2012-09-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgslr_apl")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgslr_apl")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgslr_apl")
  return

* --- Class definition
define class tgslr_apl as StdPCForm
  Width  = 557
  Height = 159
  Top    = 14
  Left   = 20
  cComment = "( lore ) parametri logistica remota"
  cPrg = "gslr_apl"
  HelpContextID=108434327
  add object cnt as tcgslr_apl
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgslr_apl as PCContext
  w_PLCODAZI = space(5)
  w_PL__SEDE = space(2)
  w_PLTIPTRS = space(1)
  w_PLTIPTRS = space(1)
  w_PLDBFERR = space(1)
  w_PLSINCAN = space(1)
  w_PLPATERR = space(254)
  w_PLPATBCK = space(254)
  w_PLPUBBCK = space(1)
  w_PLSINBCK = space(1)
  proc Save(oFrom)
    this.w_PLCODAZI = oFrom.w_PLCODAZI
    this.w_PL__SEDE = oFrom.w_PL__SEDE
    this.w_PLTIPTRS = oFrom.w_PLTIPTRS
    this.w_PLTIPTRS = oFrom.w_PLTIPTRS
    this.w_PLDBFERR = oFrom.w_PLDBFERR
    this.w_PLSINCAN = oFrom.w_PLSINCAN
    this.w_PLPATERR = oFrom.w_PLPATERR
    this.w_PLPATBCK = oFrom.w_PLPATBCK
    this.w_PLPUBBCK = oFrom.w_PLPUBBCK
    this.w_PLSINBCK = oFrom.w_PLSINBCK
    PCContext::Save(oFrom)
  proc Load(oTo)
    oTo.w_PLCODAZI = this.w_PLCODAZI
    oTo.w_PL__SEDE = this.w_PL__SEDE
    oTo.w_PLTIPTRS = this.w_PLTIPTRS
    oTo.w_PLTIPTRS = this.w_PLTIPTRS
    oTo.w_PLDBFERR = this.w_PLDBFERR
    oTo.w_PLSINCAN = this.w_PLSINCAN
    oTo.w_PLPATERR = this.w_PLPATERR
    oTo.w_PLPATBCK = this.w_PLPATBCK
    oTo.w_PLPUBBCK = this.w_PLPUBBCK
    oTo.w_PLSINBCK = this.w_PLSINBCK
    PCContext::Load(oTo)
enddefine

define class tcgslr_apl as StdPCContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 557
  Height = 159
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-09-11"
  HelpContextID=108434327
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=10

  * --- Constant Properties
  PARALORE_IDX = 0
  cFile = "PARALORE"
  cKeySelect = "PLCODAZI"
  cKeyWhere  = "PLCODAZI=this.w_PLCODAZI"
  cKeyWhereODBC = '"PLCODAZI="+cp_ToStrODBC(this.w_PLCODAZI)';

  cKeyWhereODBCqualified = '"PARALORE.PLCODAZI="+cp_ToStrODBC(this.w_PLCODAZI)';

  cPrg = "gslr_apl"
  cComment = "( lore ) parametri logistica remota"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_PLCODAZI = space(5)
  w_PL__SEDE = space(2)
  w_PLTIPTRS = space(1)
  w_PLTIPTRS = space(1)
  w_PLDBFERR = space(1)
  w_PLSINCAN = space(1)
  w_PLPATERR = space(254)
  o_PLPATERR = space(254)
  w_PLPATBCK = space(254)
  o_PLPATBCK = space(254)
  w_PLPUBBCK = space(1)
  w_PLSINBCK = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgslr_aplPag1","gslr_apl",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).HelpContextID = 166881782
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPL__SEDE_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='PARALORE'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.PARALORE_IDX,5],7]
    this.nPostItConn=i_TableProp[this.PARALORE_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgslr_apl'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from PARALORE where PLCODAZI=KeySet.PLCODAZI
    *
    i_nConn = i_TableProp[this.PARALORE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PARALORE_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('PARALORE')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "PARALORE.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' PARALORE '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'PLCODAZI',this.w_PLCODAZI  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_PLCODAZI = NVL(PLCODAZI,space(5))
        .w_PL__SEDE = NVL(PL__SEDE,space(2))
        .w_PLTIPTRS = NVL(PLTIPTRS,space(1))
        .w_PLTIPTRS = NVL(PLTIPTRS,space(1))
        .w_PLDBFERR = NVL(PLDBFERR,space(1))
        .w_PLSINCAN = NVL(PLSINCAN,space(1))
        .w_PLPATERR = NVL(PLPATERR,space(254))
        .w_PLPATBCK = NVL(PLPATBCK,space(254))
        .w_PLPUBBCK = NVL(PLPUBBCK,space(1))
        .w_PLSINBCK = NVL(PLSINBCK,space(1))
        cp_LoadRecExtFlds(this,'PARALORE')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bLoaded=.f.
    with this
      .w_PLCODAZI = space(5)
      .w_PL__SEDE = space(2)
      .w_PLTIPTRS = space(1)
      .w_PLTIPTRS = space(1)
      .w_PLDBFERR = space(1)
      .w_PLSINCAN = space(1)
      .w_PLPATERR = space(254)
      .w_PLPATBCK = space(254)
      .w_PLPUBBCK = space(1)
      .w_PLSINBCK = space(1)
      if .cFunction<>"Filter"
          .DoRTCalc(1,2,.f.)
        .w_PLTIPTRS = 'G'
        .w_PLTIPTRS = 'G'
          .DoRTCalc(5,5,.f.)
        .w_PLSINCAN = 'S'
        .w_PLPATERR = IIF(Right(Alltrim(.w_PLPATERR),1)='\',.w_PLPATERR, Alltrim(.w_PLPATERR)+'\')
        .w_PLPATBCK = IIF(Right(Alltrim(.w_PLPATBCK),1)='\',.w_PLPATBCK, IIF (EMPTY (.w_PLPATBCK), Alltrim(.w_PLPATBCK) ,  Alltrim(.w_PLPATBCK)+'\'))
          .DoRTCalc(9,9,.f.)
        .w_PLSINBCK = 'N'
      endif
    endwith
    cp_BlankRecExtFlds(this,'PARALORE')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disable List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oPL__SEDE_1_2.enabled = i_bVal
      .Page1.oPag.oPLTIPTRS_1_3.enabled = i_bVal
      .Page1.oPag.oPLTIPTRS_1_4.enabled = i_bVal
      .Page1.oPag.oPLDBFERR_1_7.enabled = i_bVal
      .Page1.oPag.oPLSINCAN_1_8.enabled = i_bVal
      .Page1.oPag.oPLPATERR_1_9.enabled = i_bVal
      .Page1.oPag.oPLPATBCK_1_10.enabled = i_bVal
      .Page1.oPag.oPLPUBBCK_1_13.enabled = i_bVal
      .Page1.oPag.oPLSINBCK_1_14.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'PARALORE',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.PARALORE_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PLCODAZI,"PLCODAZI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PL__SEDE,"PL__SEDE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PLTIPTRS,"PLTIPTRS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PLTIPTRS,"PLTIPTRS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PLDBFERR,"PLDBFERR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PLSINCAN,"PLSINCAN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PLPATERR,"PLPATERR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PLPATBCK,"PLPATBCK",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PLPUBBCK,"PLPUBBCK",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PLSINBCK,"PLSINBCK",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.PARALORE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PARALORE_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.PARALORE_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into PARALORE
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'PARALORE')
        i_extval=cp_InsertValODBCExtFlds(this,'PARALORE')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(PLCODAZI,PL__SEDE,PLTIPTRS,PLDBFERR,PLSINCAN"+;
                  ",PLPATERR,PLPATBCK,PLPUBBCK,PLSINBCK "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_PLCODAZI)+;
                  ","+cp_ToStrODBC(this.w_PL__SEDE)+;
                  ","+cp_ToStrODBC(this.w_PLTIPTRS)+;
                  ","+cp_ToStrODBC(this.w_PLDBFERR)+;
                  ","+cp_ToStrODBC(this.w_PLSINCAN)+;
                  ","+cp_ToStrODBC(this.w_PLPATERR)+;
                  ","+cp_ToStrODBC(this.w_PLPATBCK)+;
                  ","+cp_ToStrODBC(this.w_PLPUBBCK)+;
                  ","+cp_ToStrODBC(this.w_PLSINBCK)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'PARALORE')
        i_extval=cp_InsertValVFPExtFlds(this,'PARALORE')
        cp_CheckDeletedKey(i_cTable,0,'PLCODAZI',this.w_PLCODAZI)
        INSERT INTO (i_cTable);
              (PLCODAZI,PL__SEDE,PLTIPTRS,PLDBFERR,PLSINCAN,PLPATERR,PLPATBCK,PLPUBBCK,PLSINBCK  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_PLCODAZI;
                  ,this.w_PL__SEDE;
                  ,this.w_PLTIPTRS;
                  ,this.w_PLDBFERR;
                  ,this.w_PLSINCAN;
                  ,this.w_PLPATERR;
                  ,this.w_PLPATBCK;
                  ,this.w_PLPUBBCK;
                  ,this.w_PLSINBCK;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated and i_bEditing
      this.mRestoreTrs()
      this.mUpdateTrs()
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.PARALORE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PARALORE_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.PARALORE_IDX,i_nConn)
      *
      * update PARALORE
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'PARALORE')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " PL__SEDE="+cp_ToStrODBC(this.w_PL__SEDE)+;
             ",PLTIPTRS="+cp_ToStrODBC(this.w_PLTIPTRS)+;
             ",PLDBFERR="+cp_ToStrODBC(this.w_PLDBFERR)+;
             ",PLSINCAN="+cp_ToStrODBC(this.w_PLSINCAN)+;
             ",PLPATERR="+cp_ToStrODBC(this.w_PLPATERR)+;
             ",PLPATBCK="+cp_ToStrODBC(this.w_PLPATBCK)+;
             ",PLPUBBCK="+cp_ToStrODBC(this.w_PLPUBBCK)+;
             ",PLSINBCK="+cp_ToStrODBC(this.w_PLSINBCK)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'PARALORE')
        i_cWhere = cp_PKFox(i_cTable  ,'PLCODAZI',this.w_PLCODAZI  )
        UPDATE (i_cTable) SET;
              PL__SEDE=this.w_PL__SEDE;
             ,PLTIPTRS=this.w_PLTIPTRS;
             ,PLDBFERR=this.w_PLDBFERR;
             ,PLSINCAN=this.w_PLSINCAN;
             ,PLPATERR=this.w_PLPATERR;
             ,PLPATBCK=this.w_PLPATBCK;
             ,PLPUBBCK=this.w_PLPUBBCK;
             ,PLSINBCK=this.w_PLSINBCK;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && force loadrec in future
      this.LoadRec()             && record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.PARALORE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PARALORE_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.PARALORE_IDX,i_nConn)
      *
      * delete PARALORE
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'PLCODAZI',this.w_PLCODAZI  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.PARALORE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PARALORE_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,6,.t.)
        if .o_PLPATERR<>.w_PLPATERR
            .w_PLPATERR = IIF(Right(Alltrim(.w_PLPATERR),1)='\',.w_PLPATERR, Alltrim(.w_PLPATERR)+'\')
        endif
        if .o_PLPATBCK<>.w_PLPATBCK
            .w_PLPATBCK = IIF(Right(Alltrim(.w_PLPATBCK),1)='\',.w_PLPATBCK, IIF (EMPTY (.w_PLPATBCK), Alltrim(.w_PLPATBCK) ,  Alltrim(.w_PLPATBCK)+'\'))
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(9,10,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oPLTIPTRS_1_3.visible=!this.oPgFrm.Page1.oPag.oPLTIPTRS_1_3.mHide()
    this.oPgFrm.Page1.oPag.oPLTIPTRS_1_4.visible=!this.oPgFrm.Page1.oPag.oPLTIPTRS_1_4.mHide()
    this.oPgFrm.Page1.oPag.oPLPUBBCK_1_13.visible=!this.oPgFrm.Page1.oPag.oPLPUBBCK_1_13.mHide()
    this.oPgFrm.Page1.oPag.oPLSINBCK_1_14.visible=!this.oPgFrm.Page1.oPag.oPLSINBCK_1_14.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oPL__SEDE_1_2.value==this.w_PL__SEDE)
      this.oPgFrm.Page1.oPag.oPL__SEDE_1_2.value=this.w_PL__SEDE
    endif
    if not(this.oPgFrm.Page1.oPag.oPLTIPTRS_1_3.RadioValue()==this.w_PLTIPTRS)
      this.oPgFrm.Page1.oPag.oPLTIPTRS_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPLTIPTRS_1_4.RadioValue()==this.w_PLTIPTRS)
      this.oPgFrm.Page1.oPag.oPLTIPTRS_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPLDBFERR_1_7.RadioValue()==this.w_PLDBFERR)
      this.oPgFrm.Page1.oPag.oPLDBFERR_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPLSINCAN_1_8.RadioValue()==this.w_PLSINCAN)
      this.oPgFrm.Page1.oPag.oPLSINCAN_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPLPATERR_1_9.value==this.w_PLPATERR)
      this.oPgFrm.Page1.oPag.oPLPATERR_1_9.value=this.w_PLPATERR
    endif
    if not(this.oPgFrm.Page1.oPag.oPLPATBCK_1_10.value==this.w_PLPATBCK)
      this.oPgFrm.Page1.oPag.oPLPATBCK_1_10.value=this.w_PLPATBCK
    endif
    if not(this.oPgFrm.Page1.oPag.oPLPUBBCK_1_13.RadioValue()==this.w_PLPUBBCK)
      this.oPgFrm.Page1.oPag.oPLPUBBCK_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPLSINBCK_1_14.RadioValue()==this.w_PLSINBCK)
      this.oPgFrm.Page1.oPag.oPLSINBCK_1_14.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'PARALORE')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PLPATERR = this.w_PLPATERR
    this.o_PLPATBCK = this.w_PLPATBCK
    return

enddefine

* --- Define pages as container
define class tgslr_aplPag1 as StdContainer
  Width  = 553
  height = 159
  stdWidth  = 553
  stdheight = 159
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPL__SEDE_1_2 as StdField with uid="JJRGMCUQJI",rtseq=2,rtrep=.f.,;
    cFormVar = "w_PL__SEDE", cQueryName = "PL__SEDE",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    HelpContextID = 250882757,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=174, Top=10, InputMask=replicate('X',2)


  add object oPLTIPTRS_1_3 as StdCombo with uid="VRXEFHALYC",rtseq=3,rtrep=.f.,left=398,top=10,width=126,height=21;
    , ToolTipText = "Tipo transazione suggerita in fase di sincronizzazione";
    , HelpContextID = 3857079;
    , cFormVar="w_PLTIPTRS",RowSource=""+"Globale,"+"Per sede,"+"Per entit�,"+"Per istanza", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPLTIPTRS_1_3.RadioValue()
    return(iif(this.value =1,'G',;
    iif(this.value =2,'S',;
    iif(this.value =3,'E',;
    iif(this.value =4,'I',;
    space(1))))))
  endfunc
  func oPLTIPTRS_1_3.GetRadio()
    this.Parent.oContained.w_PLTIPTRS = this.RadioValue()
    return .t.
  endfunc

  func oPLTIPTRS_1_3.SetRadio()
    this.Parent.oContained.w_PLTIPTRS=trim(this.Parent.oContained.w_PLTIPTRS)
    this.value = ;
      iif(this.Parent.oContained.w_PLTIPTRS=='G',1,;
      iif(this.Parent.oContained.w_PLTIPTRS=='S',2,;
      iif(this.Parent.oContained.w_PLTIPTRS=='E',3,;
      iif(this.Parent.oContained.w_PLTIPTRS=='I',4,;
      0))))
  endfunc

  func oPLTIPTRS_1_3.mHide()
    with this.Parent.oContained
      return (.w_PLSINCAN='S')
    endwith
  endfunc


  add object oPLTIPTRS_1_4 as StdCombo with uid="IXVSYYYBOX",rtseq=4,rtrep=.f.,left=398,top=10,width=126,height=21;
    , ToolTipText = "Tipo transazione suggerita in fase di sincronizzazione";
    , HelpContextID = 3857079;
    , cFormVar="w_PLTIPTRS",RowSource=""+"Globale,"+"Per sede,"+"Per flussi,"+"Per istanza", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPLTIPTRS_1_4.RadioValue()
    return(iif(this.value =1,'G',;
    iif(this.value =2,'S',;
    iif(this.value =3,'E',;
    iif(this.value =4,'I',;
    space(1))))))
  endfunc
  func oPLTIPTRS_1_4.GetRadio()
    this.Parent.oContained.w_PLTIPTRS = this.RadioValue()
    return .t.
  endfunc

  func oPLTIPTRS_1_4.SetRadio()
    this.Parent.oContained.w_PLTIPTRS=trim(this.Parent.oContained.w_PLTIPTRS)
    this.value = ;
      iif(this.Parent.oContained.w_PLTIPTRS=='G',1,;
      iif(this.Parent.oContained.w_PLTIPTRS=='S',2,;
      iif(this.Parent.oContained.w_PLTIPTRS=='E',3,;
      iif(this.Parent.oContained.w_PLTIPTRS=='I',4,;
      0))))
  endfunc

  func oPLTIPTRS_1_4.mHide()
    with this.Parent.oContained
      return (.w_PLSINCAN<>'S')
    endwith
  endfunc


  add object oPLDBFERR_1_7 as StdCombo with uid="UBTOHITLDA",rtseq=5,rtrep=.f.,left=174,top=42,width=111,height=21;
    , ToolTipText = "Tabella: crea un DBF con tutti i record errati per tabella. Record: crea un DBF per ogni record errato di ogni tabella.";
    , HelpContextID = 266525368;
    , cFormVar="w_PLDBFERR",RowSource=""+"Tabella,"+"Record", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPLDBFERR_1_7.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'R',;
    space(1))))
  endfunc
  func oPLDBFERR_1_7.GetRadio()
    this.Parent.oContained.w_PLDBFERR = this.RadioValue()
    return .t.
  endfunc

  func oPLDBFERR_1_7.SetRadio()
    this.Parent.oContained.w_PLDBFERR=trim(this.Parent.oContained.w_PLDBFERR)
    this.value = ;
      iif(this.Parent.oContained.w_PLDBFERR=='T',1,;
      iif(this.Parent.oContained.w_PLDBFERR=='R',2,;
      0))
  endfunc

  add object oPLSINCAN_1_8 as StdCheck with uid="PZKKEJIAAQ",rtseq=6,rtrep=.f.,left=355, top=42, caption="Sincronizza per flussi",;
    ToolTipText = "In fase di soncronizzazione elabora le entit� delle sedi in ordine (1� flusso) inverso per sincronizzare i record cancellati e nell ordine normale (2� flusso) per i restanti record",;
    HelpContextID = 22735548,;
    cFormVar="w_PLSINCAN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPLSINCAN_1_8.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oPLSINCAN_1_8.GetRadio()
    this.Parent.oContained.w_PLSINCAN = this.RadioValue()
    return .t.
  endfunc

  func oPLSINCAN_1_8.SetRadio()
    this.Parent.oContained.w_PLSINCAN=trim(this.Parent.oContained.w_PLSINCAN)
    this.value = ;
      iif(this.Parent.oContained.w_PLSINCAN=='S',1,;
      0)
  endfunc

  add object oPLPATERR_1_9 as StdField with uid="QQLIQVEHPO",rtseq=7,rtrep=.f.,;
    cFormVar = "w_PLPATERR", cQueryName = "PLPATERR",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Path locale di memorizzazione dati errati o salvataggio dati preesistenti. Se inesistente o non definito non verranno creati i DBF di log",;
    HelpContextID = 251861688,;
   bGlobalFont=.t.,;
    Height=21, Width=370, Left=174, Top=72, InputMask=replicate('X',254), bHasZoom = .t. 

  proc oPLPATERR_1_9.mZoom
    this.parent.oContained.w_PLPATERR=left(cp_getdir(IIF(EMPTY(this.parent.oContained.w_PLPATERR),Sys(5)+Sys(2003),this.parent.oContained.w_PLPATERR),"Salvataggio dati e log")+space(200),200)
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oPLPATBCK_1_10 as StdField with uid="RVSBYSLCTS",rtseq=8,rtrep=.f.,;
    cFormVar = "w_PLPATBCK", cQueryName = "PLPATBCK",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Path locale di memorizzazione copie di backup dei pacchetti. Se inesistente o non definito allora in fase di pubblicazione non verranno create le copie ",;
    HelpContextID = 33757887,;
   bGlobalFont=.t.,;
    Height=21, Width=370, Left=174, Top=102, InputMask=replicate('X',254), bHasZoom = .t. 

  proc oPLPATBCK_1_10.mZoom
    this.parent.oContained.w_PLPATBCK=left(cp_getdir(IIF(EMPTY(this.parent.oContained.w_PLPATBCK),Sys(5)+Sys(2003),this.parent.oContained.w_PLPATBCK),"Salvataggio dati e log")+space(200),200)
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oPLPUBBCK_1_13 as StdCheck with uid="ARJFNAQOQA",rtseq=9,rtrep=.f.,left=173, top=128, caption="Back up pre pubblicazione",;
    HelpContextID = 51321535,;
    cFormVar="w_PLPUBBCK", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPLPUBBCK_1_13.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oPLPUBBCK_1_13.GetRadio()
    this.Parent.oContained.w_PLPUBBCK = this.RadioValue()
    return .t.
  endfunc

  func oPLPUBBCK_1_13.SetRadio()
    this.Parent.oContained.w_PLPUBBCK=trim(this.Parent.oContained.w_PLPUBBCK)
    this.value = ;
      iif(this.Parent.oContained.w_PLPUBBCK=='S',1,;
      0)
  endfunc

  func oPLPUBBCK_1_13.mHide()
    with this.Parent.oContained
      return (cp_dbtype='DB2')
    endwith
  endfunc

  add object oPLSINBCK_1_14 as StdCheck with uid="AXKUCOZOKJ",rtseq=10,rtrep=.f.,left=355, top=128, caption="Back up pre sincronizzizzazione",;
    HelpContextID = 39512767,;
    cFormVar="w_PLSINBCK", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPLSINBCK_1_14.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oPLSINBCK_1_14.GetRadio()
    this.Parent.oContained.w_PLSINBCK = this.RadioValue()
    return .t.
  endfunc

  func oPLSINBCK_1_14.SetRadio()
    this.Parent.oContained.w_PLSINBCK=trim(this.Parent.oContained.w_PLSINBCK)
    this.value = ;
      iif(this.Parent.oContained.w_PLSINBCK=='S',1,;
      0)
  endfunc

  func oPLSINBCK_1_14.mHide()
    with this.Parent.oContained
      return (cp_dbtype='DB2')
    endwith
  endfunc

  add object oStr_1_5 as StdString with uid="GLCNKVCARM",Visible=.t., Left=22, Top=10,;
    Alignment=1, Width=148, Height=18,;
    Caption="Identificativo sede:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="DLNKLLMYGJ",Visible=.t., Left=22, Top=42,;
    Alignment=1, Width=148, Height=18,;
    Caption="Creazione DBF di log per:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="WPEPQOPSFC",Visible=.t., Left=22, Top=73,;
    Alignment=1, Width=148, Height=18,;
    Caption="Log e salvataggio dati:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="CWQJUVOUKK",Visible=.t., Left=232, Top=10,;
    Alignment=1, Width=163, Height=18,;
    Caption="Tipo transazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="LUFIDUJJKC",Visible=.t., Left=3, Top=103,;
    Alignment=1, Width=167, Height=18,;
    Caption="Copia di backup dei pacchetti:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gslr_apl','PARALORE','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".PLCODAZI=PARALORE.PLCODAZI";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
