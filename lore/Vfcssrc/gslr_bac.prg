* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslr_bac                                                        *
*              Aggiornamento commessa                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-09-25                                                      *
* Last revis.: 2006-09-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_SERIAL,w_AGGSAL
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgslr_bac",oParentObject,m.w_SERIAL,m.w_AGGSAL)
return(i_retval)

define class tgslr_bac as StdBatch
  * --- Local variables
  w_SERIAL = space(10)
  w_AGGSAL = 0
  * --- WorkFile variables
  MA_COSTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch di aggiornamento commessa in fase di sincronizzazione.
    *     Lanciato da FLR_DOCPRE e FLR_DOCPOS
    * --- w_AGGSAL valo + o -
    *     - Storno la commessa con i vecchi valori
    *     + Aggiorno la commessa con i nuovi valori
    *     
    *     - Viene lanciato da FLR_DOCPRE
    *     + Viene lanciato da FLR_DOCPOS
    * --- Write into MA_COSTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.MA_COSTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MA_COSTI_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="CSCODCOM,CSTIPSTR,CSCODMAT,CSCODCOS"
      do vq_exec with 'gslr_qac',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.MA_COSTI_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="MA_COSTI.CSCODCOM = _t2.CSCODCOM";
              +" and "+"MA_COSTI.CSTIPSTR = _t2.CSTIPSTR";
              +" and "+"MA_COSTI.CSCODMAT = _t2.CSCODMAT";
              +" and "+"MA_COSTI.CSCODCOS = _t2.CSCODCOS";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CSCONSUN = MA_COSTI.CSCONSUN+_t2.CONSUN";
          +",CSORDIN = MA_COSTI.CSORDIN+_t2.ORDINA";
          +i_ccchkf;
          +" from "+i_cTable+" MA_COSTI, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="MA_COSTI.CSCODCOM = _t2.CSCODCOM";
              +" and "+"MA_COSTI.CSTIPSTR = _t2.CSTIPSTR";
              +" and "+"MA_COSTI.CSCODMAT = _t2.CSCODMAT";
              +" and "+"MA_COSTI.CSCODCOS = _t2.CSCODCOS";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MA_COSTI, "+i_cQueryTable+" _t2 set ";
          +"MA_COSTI.CSCONSUN = MA_COSTI.CSCONSUN+_t2.CONSUN";
          +",MA_COSTI.CSORDIN = MA_COSTI.CSORDIN+_t2.ORDINA";
          +Iif(Empty(i_ccchkf),"",",MA_COSTI.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="MA_COSTI.CSCODCOM = t2.CSCODCOM";
              +" and "+"MA_COSTI.CSTIPSTR = t2.CSTIPSTR";
              +" and "+"MA_COSTI.CSCODMAT = t2.CSCODMAT";
              +" and "+"MA_COSTI.CSCODCOS = t2.CSCODCOS";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MA_COSTI set (";
          +"CSCONSUN,";
          +"CSORDIN";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"CSCONSUN+t2.CONSUN,";
          +"CSORDIN+t2.ORDINA";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="MA_COSTI.CSCODCOM = _t2.CSCODCOM";
              +" and "+"MA_COSTI.CSTIPSTR = _t2.CSTIPSTR";
              +" and "+"MA_COSTI.CSCODMAT = _t2.CSCODMAT";
              +" and "+"MA_COSTI.CSCODCOS = _t2.CSCODCOS";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MA_COSTI set ";
          +"CSCONSUN = MA_COSTI.CSCONSUN+_t2.CONSUN";
          +",CSORDIN = MA_COSTI.CSORDIN+_t2.ORDINA";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".CSCODCOM = "+i_cQueryTable+".CSCODCOM";
              +" and "+i_cTable+".CSTIPSTR = "+i_cQueryTable+".CSTIPSTR";
              +" and "+i_cTable+".CSCODMAT = "+i_cQueryTable+".CSCODMAT";
              +" and "+i_cTable+".CSCODCOS = "+i_cQueryTable+".CSCODCOS";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CSCONSUN = (select "+i_cTable+".CSCONSUN+CONSUN from "+i_cQueryTable+" where "+i_cWhere+")";
          +",CSORDIN = (select "+i_cTable+".CSORDIN+ORDINA from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Nel caso non scrivo niente indico che non ho aggiornato la commessa
    i_retcode = 'stop'
    i_retval = i_Rows<>0
    return
  endproc


  proc Init(oParentObject,w_SERIAL,w_AGGSAL)
    this.w_SERIAL=w_SERIAL
    this.w_AGGSAL=w_AGGSAL
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='MA_COSTI'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_SERIAL,w_AGGSAL"
endproc
