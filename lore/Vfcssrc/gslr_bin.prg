* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslr_bin                                                        *
*              Informazioni mirror                                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-07-04                                                      *
* Last revis.: 2006-07-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgslr_bin",oParentObject)
return(i_retval)

define class tgslr_bin as StdBatch
  * --- Local variables
  w_LOOP = 0
  w_CURDB = space(50)
  w_PADRE = .NULL.
  w_CTRL = .NULL.
  w_OLDPOSTIT = .f.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione Init Maschera informazioni Mirror
    *     Riceve come parametro il nome dell'archivio mirror e l'array delle chiavi per
    *     identificare il record...
    *     (si appoggioa a g_OMENU.Key per avere le informazioni legate)
    * --- Costruisco da g_OMENU.OKEY l'array da passare alla READTABLE
    this.w_PADRE = This.oParentObject
    this.w_LOOP = 1
    do while this.w_LOOP<= g_OMENU.nKeyCount
       
 Dimension ArrRead[ this.w_LOOP, 2] 
 ArrRead[ this.w_LOOP, 1] = g_OMENU.oKEY[this.w_LOOP, 1] 
 ArrRead[ this.w_LOOP, 2] = g_OMENU.oKEY[this.w_LOOP, 3]
      this.w_LOOP = this.w_LOOP + 1
    enddo
    * --- Leggo il dato...
    this.w_CURDB = READTABLE( this.oParentObject.w_ARCHIVIO , "*" , @ArrRead)
    if Not Empty( this.w_CURDB ) And not Eof( this.w_CURDB ) and not Bof( this.w_CURDB )
      * --- Aggiungo informazioni di sistema (CPCCCHK, AVERSION, ANCESTOR, CPCCCHKPU e PUBCOSED)
       
 Select( this.w_CURDB ) 
 Go Top
      this.oParentObject.w_CPCCCHK = CPCCCHK
      this.oParentObject.w_AVERSION = AVERSION
      this.oParentObject.w_CPCCCHK_PU = CPCCCHK_PU
      this.oParentObject.w_PUBCOSED = PUBCOSED
      this.oParentObject.w_ANCESTOR = ANCESTOR
      * --- Ricarico il record...
      this.w_OLDPOSTIT = g_OMENU.oParentObject.bPostIt
      * --- Ricarico il record per avere il CPCCCHK aggiornato
      *     (dopo F10 sulla gestione � presenta ancora quello al momento della modifica)
      *     (disabilitato la maschera di informazioni va sotto)
       
 *g_OMENU.oParentObject.bPostIt=.f. 
 *g_OMENU.oParentObject.LoadRecWarn() 
 *g_OMENU.oParentObject.bPostIt=this.w_OLDPOSTIT
      L_Macro = g_omenu.oParentObject.cCursor+".CPCCCHK"
      this.oParentObject.w_CPCCCHK_NOW = &L_Macro
    else
      this.w_PADRE.EcpQuit()     
      ah_ErrorMsg("Nessuna informazione da visualizzare","!","")
    endif
    if Not Empty( this.w_CURDB ) 
       
 Select( this.w_CURDB ) 
 Use
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
