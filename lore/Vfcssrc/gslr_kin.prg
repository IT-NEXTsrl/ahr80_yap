* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslr_kin                                                        *
*              Informazioni mirror                                             *
*                                                                              *
*      Author: Zucchetti SPA                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161][VRS_10]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-07-04                                                      *
* Last revis.: 2008-09-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgslr_kin",oParentObject))

* --- Class definition
define class tgslr_kin as StdForm
  Top    = 9
  Left   = 9

  * --- Standard Properties
  Width  = 436
  Height = 148
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-09-23"
  HelpContextID=1479575
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=7

  * --- Constant Properties
  _IDX = 0
  cPrg = "gslr_kin"
  cComment = "Informazioni mirror"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_ARCHIVIO = space(30)
  w_ANCESTOR = space(10)
  w_CPCCCHK = space(10)
  w_CPCCCHK_PU = space(10)
  w_PUBCOSED = space(2)
  w_AVERSION = 0
  w_CPCCCHK_NOW = space(10)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgslr_kinPag1","gslr_kin",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_ARCHIVIO=space(30)
      .w_ANCESTOR=space(10)
      .w_CPCCCHK=space(10)
      .w_CPCCCHK_PU=space(10)
      .w_PUBCOSED=space(2)
      .w_AVERSION=0
      .w_CPCCCHK_NOW=space(10)
        .w_ARCHIVIO = GetMirrorName(g_OMENU.cLinkFile)
    endwith
    this.DoRTCalc(2,7,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,7,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_OJYYEHVROH()
    with this
          * --- Carica la maschera
          gslr_bin(this;
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Init")
          .Calculate_OJYYEHVROH()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oANCESTOR_1_6.value==this.w_ANCESTOR)
      this.oPgFrm.Page1.oPag.oANCESTOR_1_6.value=this.w_ANCESTOR
    endif
    if not(this.oPgFrm.Page1.oPag.oCPCCCHK_1_8.value==this.w_CPCCCHK)
      this.oPgFrm.Page1.oPag.oCPCCCHK_1_8.value=this.w_CPCCCHK
    endif
    if not(this.oPgFrm.Page1.oPag.oCPCCCHK_PU_1_10.value==this.w_CPCCCHK_PU)
      this.oPgFrm.Page1.oPag.oCPCCCHK_PU_1_10.value=this.w_CPCCCHK_PU
    endif
    if not(this.oPgFrm.Page1.oPag.oPUBCOSED_1_12.value==this.w_PUBCOSED)
      this.oPgFrm.Page1.oPag.oPUBCOSED_1_12.value=this.w_PUBCOSED
    endif
    if not(this.oPgFrm.Page1.oPag.oAVERSION_1_14.value==this.w_AVERSION)
      this.oPgFrm.Page1.oPag.oAVERSION_1_14.value=this.w_AVERSION
    endif
    if not(this.oPgFrm.Page1.oPag.oCPCCCHK_NOW_1_16.value==this.w_CPCCCHK_NOW)
      this.oPgFrm.Page1.oPag.oCPCCCHK_NOW_1_16.value=this.w_CPCCCHK_NOW
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgslr_kinPag1 as StdContainer
  Width  = 432
  height = 148
  stdWidth  = 432
  stdheight = 148
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oBtn_1_1 as StdButton with uid="SKOOVQACPP",left=327, top=98, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 1508326;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_1.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_2 as StdButton with uid="AUWPCNUDEI",left=378, top=98, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 8796998;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_2.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oANCESTOR_1_6 as StdField with uid="SKXGFGWZZI",rtseq=2,rtrep=.f.,;
    cFormVar = "w_ANCESTOR", cQueryName = "ANCESTOR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Crc autorizzato dal validatore",;
    HelpContextID = 107997608,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=83, Top=57, InputMask=replicate('X',10)

  add object oCPCCCHK_1_8 as StdField with uid="KLNQEMEIXP",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CPCCCHK", cQueryName = "CPCCCHK",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Crc ultima pubblicazione",;
    HelpContextID = 210638886,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=83, Top=86, InputMask=replicate('X',10)

  add object oCPCCCHK_PU_1_10 as StdField with uid="VYJCCUWSOF",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CPCCCHK_PU", cQueryName = "CPCCCHK_PU",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Crc passatomi con l'ultima pubblicazione",;
    HelpContextID = 210662021,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=326, Top=28, InputMask=replicate('X',10)

  add object oPUBCOSED_1_12 as StdField with uid="YGMREEDIIC",rtseq=5,rtrep=.f.,;
    cFormVar = "w_PUBCOSED", cQueryName = "PUBCOSED",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sede di pubblicazione",;
    HelpContextID = 129102278,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=326, Top=57, InputMask=replicate('X',2)

  add object oAVERSION_1_14 as StdField with uid="WQRVGWITPN",rtseq=6,rtrep=.f.,;
    cFormVar = "w_AVERSION", cQueryName = "AVERSION",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Versione imposta dal validatore",;
    HelpContextID = 23249324,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=83, Top=28

  add object oCPCCCHK_NOW_1_16 as StdField with uid="BBGPETTBKT",rtseq=7,rtrep=.f.,;
    cFormVar = "w_CPCCCHK_NOW", cQueryName = "CPCCCHK_NOW",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Crc attuale",;
    HelpContextID = 211016805,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=83, Top=115, InputMask=replicate('X',10)

  add object oStr_1_5 as StdString with uid="AYXPWVZKOF",Visible=.t., Left=5, Top=57,;
    Alignment=1, Width=75, Height=18,;
    Caption="Ancestor:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="IGPALZCKUV",Visible=.t., Left=5, Top=86,;
    Alignment=1, Width=75, Height=18,;
    Caption="Crc:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="HNMXYHCRLH",Visible=.t., Left=186, Top=28,;
    Alignment=1, Width=136, Height=18,;
    Caption="Crc pubblicatore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="ONTBOGSXLQ",Visible=.t., Left=186, Top=57,;
    Alignment=1, Width=136, Height=18,;
    Caption="Sede di pubblicazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="LDEEISVRID",Visible=.t., Left=5, Top=28,;
    Alignment=1, Width=75, Height=18,;
    Caption="Versione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="CDANHFVEKF",Visible=.t., Left=5, Top=115,;
    Alignment=1, Width=75, Height=18,;
    Caption="Crc attuale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="VOPKHXJNFK",Visible=.t., Left=11, Top=5,;
    Alignment=2, Width=177, Height=18,;
    Caption="Fase di pubblicazione"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_18 as StdString with uid="JZUELJDPUI",Visible=.t., Left=197, Top=5,;
    Alignment=2, Width=223, Height=18,;
    Caption="Fase di acquisizione"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_1_19 as StdBox with uid="TQSVPZWPZR",left=7, top=20, width=417,height=4
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gslr_kin','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
