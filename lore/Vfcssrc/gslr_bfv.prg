* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslr_bfv                                                        *
*              Verifica che i filtri verticali dinamici siano impostati solo su sedi verso cui si importa*
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-04-22                                                      *
* Last revis.: 2008-04-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgslr_bfv",oParentObject)
return(i_retval)

define class tgslr_bfv as StdBatch
  * --- Local variables
  w_PADRE = .NULL.
  w_DETARCH = .NULL.
  w_FILTVERT = .NULL.
  w_DINAMICI = .f.
  * --- WorkFile variables
  ENT_DETT_idx=0
  PUBLVERT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controlla che i filtri verticali dinamici siano impostati solo sulle sedi da cui si ricevono dati
    *     Sulle sedi verso cui pubblico non si possono definire filtri verticali dinamici
    this.w_DINAMICI = .F.
    * --- Il controllo � effettuato solo sulle sedi verso cui si pubblica
    if this.oParentObject.w_SE__TIPO="R"
      this.w_PADRE = THIS.oParentObject
      this.w_PADRE.MarkPos()     
      this.w_PADRE.FirstRow()     
      * --- Scandisce le righe di dettaglio delle sedi
      do while Not this.w_PADRE.Eof_Trs()
        this.w_PADRE.SetRow()     
        this.w_PADRE.ChildrenChangeRow()     
        if this.w_PADRE.FullRow() AND this.w_PADRE.RowStatus()<>"D"
          * --- Scandisce gli archivi su cui sono stati impostati i filtri verticali
          this.w_DETARCH = this.w_PADRE.GSLR_MDP
          this.w_DETARCH.MarkPos()     
          this.w_DETARCH.FirstRow()     
          this.w_DETARCH.ChildrenChangeRow()     
          do while Not this.w_DETARCH.Eof_Trs()
            this.w_DETARCH.SetRow()     
            if this.w_DETARCH.FullRow() AND this.w_DETARCH.RowStatus()<>"D"
              * --- Scandisce i filtri verticali 
              this.w_FILTVERT = this.w_DETARCH.GSLR_MFV.CNT
              this.w_FILTVERT.MarkPos()     
              this.w_FILTVERT.FirstRow()     
              do while Not this.w_FILTVERT.Eof_Trs()
                this.w_FILTVERT.SetRow()     
                if this.w_FILTVERT.FullRow() AND this.w_FILTVERT.RowStatus()<>"D"
                  * --- E stato trovato un filtro dinamico
                  if this.w_FILTVERT.w_PVSTADIN="D"
                    this.w_DINAMICI = .T.
                    exit
                  endif
                endif
                this.w_FILTVERT.NextRow()     
              enddo
              this.w_FILTVERT.RePos(.F.)     
            endif
            if this.w_DINAMICI
              exit
            endif
            this.w_DETARCH.NextRow()     
          enddo
          this.w_DETARCH.RePos(.F.)     
          this.w_DETARCH.ChildrenChangeRow()     
        endif
        if this.w_DINAMICI
          exit
        endif
        this.w_PADRE.NextRow()     
      enddo
      this.w_PADRE.RePos(.F.)     
      this.w_PADRE.ChildrenChangeRow()     
      if this.w_DINAMICI
        this.oParentObject.w_SE__TIPO = "P"
        this.w_PADRE.mcalc(.T.)     
        Ah_ErrorMsg ("Sulle sedi verso cui si pubblica non si possono definire filtri verticali dinamici.")
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='ENT_DETT'
    this.cWorkTables[2]='PUBLVERT'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
