* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslr_bcf                                                        *
*              Verifica correttezza filtri verticali dinamici da sedi          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-04-22                                                      *
* Last revis.: 2008-04-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgslr_bcf",oParentObject,m.pOper)
return(i_retval)

define class tgslr_bcf as StdBatch
  * --- Local variables
  pOper = space(1)
  w_MESS = space(100)
  w_PADRE = .NULL.
  w_EN_TABLE = space(1)
  * --- WorkFile variables
  ENT_DETT_idx=0
  PUBLVERT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Se si definisce un filtro verticale su una chiave di una tabella principale allora tutte le relazioni definite sulla tabella principale che utilizzano il campo modificato con filtro verticale dovranno contenere 
    *     espressioni nel formato tab1CAMPO=tab2.CAMPO2 (non sono ammesse espressioni composte  (per ex:   ABS(tab1CAMPO)=tab2.CAMPO2 + 4 
    *     Se lanciato  dall' anagrafica delle entit� GAR_MEN allora verifica se sulla Entit� e sulla tabella principale sono definiti dei viltri verticali che coinvolgono campi chiave. In caso affermativo effettua la verifica della relazione contenuta nell anagrafica sedi
    if not bTrsErr
      * --- L'immancabile chiamata alla CP_READXDC...
      =cp_ReadXdc()
      * --- Il controllo si effettua solo sulle relazioni definite sulle tabelle collegate
      if this.oParentObject.w_ENPRINCI<>"S"
        this.w_PADRE = THIS.oParentObject
        this.w_PADRE.MarkPos()     
        this.w_PADRE.FirstRow()     
        * --- Ricerca la tabella prinmcipale definita sull entita 
        do while Not this.w_PADRE.Eof_Trs()
          this.w_PADRE.SetRow()     
          if this.w_PADRE.FullRow() AND this.w_PADRE.RowStatus()<>"D"
            if  this.oParentObject.w_ENPRINCI="S"
              this.w_EN_TABLE = this.w_PADRE.w_EN_TABLE
              exit
            endif
          endif
          this.w_PADRE.NextRow()     
        enddo
        this.w_PADRE.RePos(.F.)     
        * --- Se nell entit� � definita una tabella principale
        if NOT EMPTY ( this.w_EN_TABLE )
          * --- Controlla tutti i filtri verticali definiti sulla tabella principale 
          * --- Select from PUBLVERT
          i_nConn=i_TableProp[this.PUBLVERT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PUBLVERT_idx,2],.t.,this.PUBLVERT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select PVFIELDS  from "+i_cTable+" PUBLVERT ";
                +" where PVSTADIN='D' AND PVCODARC="+cp_ToStrODBC(this.w_EN_TABLE)+" AND PVCODENT="+cp_ToStrODBC(this.oParentObject.w_ENCODICE)+"";
                 ,"_Curs_PUBLVERT")
          else
            select PVFIELDS from (i_cTable);
             where PVSTADIN="D" AND PVCODARC=this.w_EN_TABLE AND PVCODENT=this.oParentObject.w_ENCODICE;
              into cursor _Curs_PUBLVERT
          endif
          if used('_Curs_PUBLVERT')
            select _Curs_PUBLVERT
            locate for 1=1
            do while not(eof())
            * --- Se il campo su cui � definito il filtro verticale � un campo chiave
            if alltrim(_Curs_PUBLVERT.PVFIELDS) $ cp_KeyToSQL ( I_DCX.GetIdxDef( this.w_EN_TABLE ,1 ) )
              * --- Su uno dei campi chiave della tabella principale � stato impostato un filtro vericale quindi bisogna controllare tutte le relazioni dell entit� per verificare che siano in un formato corretto 
              *     (ossia  che le espresioni che coinvolgono il campo chiave siano in forma semplice : tab1.campo=tab2.campo. Se una delle relazioni non � corretata si interrompe la transazione
              if  NOT checkrel ( alltrim(this.oParentObject.w_EN__LINK), alltrim(this.w_EN_TABLE) , alltrim(_Curs_PUBLVERT.PVFIELDS))
                this.w_MESS = ah_MsgFormat("La relazione tra le tabelle %2 e %3 coinvolge il campo %4 sottoposto a filtro verticale%0L' espressioni che coinvolge il campo %4 deve essere nel formato semplice (tab1.campo1=tab2.campo2) !%0Operazione interrotta.",alltrim(this.oParentObject.w_ENCODICE), alltrim(this.w_EN_TABLE) , alltrim(this.w_PADRE.w_EN_TABLE) ,alltrim(_Curs_PUBLVERT.PVFIELDS))
                exit
              endif
            endif
              select _Curs_PUBLVERT
              continue
            enddo
            use
          endif
        endif
        if NOT EMPTY (this.w_MESS)
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_MESS
        endif
      endif
    endif
  endproc


  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='ENT_DETT'
    this.cWorkTables[2]='PUBLVERT'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_PUBLVERT')
      use in _Curs_PUBLVERT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
