* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslr_ksi                                                        *
*              Sincronizzazione                                                *
*                                                                              *
*      Author: Zucchetti SPA                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161][VRS_17]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-04-14                                                      *
* Last revis.: 2012-10-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgslr_ksi",oParentObject))

* --- Class definition
define class tgslr_ksi as StdForm
  Top    = 39
  Left   = 34

  * --- Standard Properties
  Width  = 593
  Height = 409
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-10-04"
  HelpContextID=169251735
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=11

  * --- Constant Properties
  _IDX = 0
  PARALORE_IDX = 0
  AZIENDA_IDX = 0
  AZBACKUP_IDX = 0
  cPrg = "gslr_ksi"
  cComment = "Sincronizzazione"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODAZI = space(5)
  o_CODAZI = space(5)
  w_CODAZI2 = space(5)
  o_CODAZI2 = space(5)
  w_TIPTRS = space(1)
  w_TIPTRS = space(1)
  w_BACKUP = space(1)
  w_PLSINCAN = space(1)
  w_VERBOSE = .F.
  w_Msg = space(0)
  w_TIPTRS1 = space(1)
  w_DAYBCK = 0
  w_BCKNUM = 0
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgslr_ksiPag1","gslr_ksi",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Generazione")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTIPTRS_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='PARALORE'
    this.cWorkTables[2]='AZIENDA'
    this.cWorkTables[3]='AZBACKUP'
    return(this.OpenAllTables(3))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do gslr_bsi with this
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI=space(5)
      .w_CODAZI2=space(5)
      .w_TIPTRS=space(1)
      .w_TIPTRS=space(1)
      .w_BACKUP=space(1)
      .w_PLSINCAN=space(1)
      .w_VERBOSE=.f.
      .w_Msg=space(0)
      .w_TIPTRS1=space(1)
      .w_DAYBCK=0
      .w_BCKNUM=0
        .w_CODAZI = i_CODAZI
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODAZI))
          .link_1_1('Full')
        endif
        .w_CODAZI2 = i_CODAZI
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODAZI2))
          .link_1_2('Full')
        endif
        .w_TIPTRS = .w_TIPTRS1
        .w_TIPTRS = .w_TIPTRS1
    endwith
    this.DoRTCalc(5,11,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        if .o_CODAZI<>.w_CODAZI
          .link_1_1('Full')
        endif
        if .o_CODAZI2<>.w_CODAZI2
          .link_1_2('Full')
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(3,11,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oTIPTRS_1_3.visible=!this.oPgFrm.Page1.oPag.oTIPTRS_1_3.mHide()
    this.oPgFrm.Page1.oPag.oTIPTRS_1_4.visible=!this.oPgFrm.Page1.oPag.oTIPTRS_1_4.mHide()
    this.oPgFrm.Page1.oPag.oBACKUP_1_5.visible=!this.oPgFrm.Page1.oPag.oBACKUP_1_5.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PARALORE_IDX,3]
    i_lTable = "PARALORE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PARALORE_IDX,2], .t., this.PARALORE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PARALORE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PLCODAZI,PLTIPTRS,PLSINBCK,PLSINCAN";
                   +" from "+i_cTable+" "+i_lTable+" where PLCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PLCODAZI',this.w_CODAZI)
            select PLCODAZI,PLTIPTRS,PLSINBCK,PLSINCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.PLCODAZI,space(5))
      this.w_TIPTRS1 = NVL(_Link_.PLTIPTRS,space(1))
      this.w_BACKUP = NVL(_Link_.PLSINBCK,space(1))
      this.w_PLSINCAN = NVL(_Link_.PLSINCAN,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_TIPTRS1 = space(1)
      this.w_BACKUP = space(1)
      this.w_PLSINCAN = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PARALORE_IDX,2])+'\'+cp_ToStr(_Link_.PLCODAZI,1)
      cp_ShowWarn(i_cKey,this.PARALORE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODAZI2
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZBACKUP_IDX,3]
    i_lTable = "AZBACKUP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZBACKUP_IDX,2], .t., this.AZBACKUP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZBACKUP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZBCKDAY,AZBCKNUM";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_CODAZI2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_CODAZI2)
            select AZCODAZI,AZBCKDAY,AZBCKNUM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI2 = NVL(_Link_.AZCODAZI,space(5))
      this.w_DAYBCK = NVL(_Link_.AZBCKDAY,0)
      this.w_BCKNUM = NVL(_Link_.AZBCKNUM,0)
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI2 = space(5)
      endif
      this.w_DAYBCK = 0
      this.w_BCKNUM = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZBACKUP_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZBACKUP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTIPTRS_1_3.RadioValue()==this.w_TIPTRS)
      this.oPgFrm.Page1.oPag.oTIPTRS_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPTRS_1_4.RadioValue()==this.w_TIPTRS)
      this.oPgFrm.Page1.oPag.oTIPTRS_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oBACKUP_1_5.RadioValue()==this.w_BACKUP)
      this.oPgFrm.Page1.oPag.oBACKUP_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPLSINCAN_1_6.RadioValue()==this.w_PLSINCAN)
      this.oPgFrm.Page1.oPag.oPLSINCAN_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oVERBOSE_1_7.RadioValue()==this.w_VERBOSE)
      this.oPgFrm.Page1.oPag.oVERBOSE_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMsg_1_8.value==this.w_Msg)
      this.oPgFrm.Page1.oPag.oMsg_1_8.value=this.w_Msg
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODAZI = this.w_CODAZI
    this.o_CODAZI2 = this.w_CODAZI2
    return

enddefine

* --- Define pages as container
define class tgslr_ksiPag1 as StdContainer
  Width  = 589
  height = 409
  stdWidth  = 589
  stdheight = 409
  resizeXpos=152
  resizeYpos=120
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oTIPTRS_1_3 as StdCombo with uid="ZJXQQTNWHB",rtseq=3,rtrep=.f.,left=470,top=13,width=107,height=18;
    , ToolTipText = "Indica la granularitÓ della transazione";
    , HelpContextID = 42983990;
    , cFormVar="w_TIPTRS",RowSource=""+"Globale,"+"Per sede,"+"Per flussi,"+"Per istanza", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPTRS_1_3.RadioValue()
    return(iif(this.value =1,'G',;
    iif(this.value =2,'S',;
    iif(this.value =3,'E',;
    iif(this.value =4,'I',;
    space(1))))))
  endfunc
  func oTIPTRS_1_3.GetRadio()
    this.Parent.oContained.w_TIPTRS = this.RadioValue()
    return .t.
  endfunc

  func oTIPTRS_1_3.SetRadio()
    this.Parent.oContained.w_TIPTRS=trim(this.Parent.oContained.w_TIPTRS)
    this.value = ;
      iif(this.Parent.oContained.w_TIPTRS=='G',1,;
      iif(this.Parent.oContained.w_TIPTRS=='S',2,;
      iif(this.Parent.oContained.w_TIPTRS=='E',3,;
      iif(this.Parent.oContained.w_TIPTRS=='I',4,;
      0))))
  endfunc

  func oTIPTRS_1_3.mHide()
    with this.Parent.oContained
      return (.w_PLSINCAN<>'S')
    endwith
  endfunc


  add object oTIPTRS_1_4 as StdCombo with uid="KSJBWKTELU",rtseq=4,rtrep=.f.,left=470,top=13,width=107,height=18;
    , ToolTipText = "Indica la granularitÓ della transazione";
    , HelpContextID = 42983990;
    , cFormVar="w_TIPTRS",RowSource=""+"Globale,"+"Per sede,"+"Per entitÓ,"+"Per istanza", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPTRS_1_4.RadioValue()
    return(iif(this.value =1,'G',;
    iif(this.value =2,'S',;
    iif(this.value =3,'E',;
    iif(this.value =4,'I',;
    space(1))))))
  endfunc
  func oTIPTRS_1_4.GetRadio()
    this.Parent.oContained.w_TIPTRS = this.RadioValue()
    return .t.
  endfunc

  func oTIPTRS_1_4.SetRadio()
    this.Parent.oContained.w_TIPTRS=trim(this.Parent.oContained.w_TIPTRS)
    this.value = ;
      iif(this.Parent.oContained.w_TIPTRS=='G',1,;
      iif(this.Parent.oContained.w_TIPTRS=='S',2,;
      iif(this.Parent.oContained.w_TIPTRS=='E',3,;
      iif(this.Parent.oContained.w_TIPTRS=='I',4,;
      0))))
  endfunc

  func oTIPTRS_1_4.mHide()
    with this.Parent.oContained
      return (.w_PLSINCAN='S')
    endwith
  endfunc

  add object oBACKUP_1_5 as StdCheck with uid="AIAQKTRDST",rtseq=5,rtrep=.f.,left=15, top=8, caption="Backup",;
    ToolTipText = "Se attivo prima della pubblicazione esegue un back up dei dati",;
    HelpContextID = 263588118,;
    cFormVar="w_BACKUP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oBACKUP_1_5.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oBACKUP_1_5.GetRadio()
    this.Parent.oContained.w_BACKUP = this.RadioValue()
    return .t.
  endfunc

  func oBACKUP_1_5.SetRadio()
    this.Parent.oContained.w_BACKUP=trim(this.Parent.oContained.w_BACKUP)
    this.value = ;
      iif(this.Parent.oContained.w_BACKUP=='S',1,;
      0)
  endfunc

  func oBACKUP_1_5.mHide()
    with this.Parent.oContained
      return (cp_dbtype='DB2')
    endwith
  endfunc

  add object oPLSINCAN_1_6 as StdCheck with uid="BJJTBUDFZF",rtseq=6,rtrep=.f.,left=15, top=27, caption="Sincronizza per flussi",;
    ToolTipText = "In fase di soncronizzazione elabora le entitÓ delle sedi in ordine (1░ flusso) inverso per sincronizzare i record cancellati e nell ordine normale (2░ flusso) per i restanti record",;
    HelpContextID = 38081860,;
    cFormVar="w_PLSINCAN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPLSINCAN_1_6.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oPLSINCAN_1_6.GetRadio()
    this.Parent.oContained.w_PLSINCAN = this.RadioValue()
    return .t.
  endfunc

  func oPLSINCAN_1_6.SetRadio()
    this.Parent.oContained.w_PLSINCAN=trim(this.Parent.oContained.w_PLSINCAN)
    this.value = ;
      iif(this.Parent.oContained.w_PLSINCAN=='S',1,;
      0)
  endfunc

  add object oVERBOSE_1_7 as StdCheck with uid="FVKZVFABRD",rtseq=7,rtrep=.f.,left=506, top=50, caption="Verbose",;
    ToolTipText = "Se attivo il log mostra tutte le frasi svolte sul database",;
    HelpContextID = 229769642,;
    cFormVar="w_VERBOSE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oVERBOSE_1_7.RadioValue()
    return(iif(this.value =1,.T.,;
    .f.))
  endfunc
  func oVERBOSE_1_7.GetRadio()
    this.Parent.oContained.w_VERBOSE = this.RadioValue()
    return .t.
  endfunc

  func oVERBOSE_1_7.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_VERBOSE==.T.,1,;
      0)
  endfunc

  add object oMsg_1_8 as StdMemo with uid="QYBPPIIQMH",rtseq=8,rtrep=.f.,;
    cFormVar = "w_Msg", cQueryName = "Msg",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 169704390,;
    FontName = "Courier New", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=281, Width=574, Left=6, Top=71, tabstop = .f., readonly = .t.


  add object oBtn_1_9 as StdButton with uid="QKNERQNDZN",left=8, top=359, width=48,height=45,;
    CpPicture="BMP\SAVE.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per salvare il log di elaborazione";
    , HelpContextID = 10730022;
    , Caption='\<Salva';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      with this.Parent.oContained
        STRTOFILE(.w_MSG, PUTFILE('',cp_MsgFormat("Log_Elaboraz_Sincronizzazione"),"TXT"))
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_9.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty( .w_Msg ))
      endwith
    endif
  endfunc


  add object oBtn_1_11 as StdButton with uid="SKOOVQACPP",left=481, top=359, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 169280486;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_11.Click()
      with this.Parent.oContained
        do gslr_bsi with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_12 as StdButton with uid="AUWPCNUDEI",left=532, top=359, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 176569158;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_12.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_10 as StdString with uid="BKVEGTRFKF",Visible=.t., Left=7, Top=50,;
    Alignment=0, Width=315, Height=18,;
    Caption="Sincronizzazione"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="HHMZYOLITE",Visible=.t., Left=319, Top=13,;
    Alignment=1, Width=147, Height=18,;
    Caption="Transazione:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gslr_ksi','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
