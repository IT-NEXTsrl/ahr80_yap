* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslr_bie                                                        *
*              Exp/imp tabelle import DBF                                      *
*                                                                              *
*      Author: TAM SOFTWARE                                                    *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_379]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-10-12                                                      *
* Last revis.: 2008-07-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgslr_bie",oParentObject)
return(i_retval)

define class tgslr_bie as StdBatch
  * --- Local variables
  Messaggio = space(254)
  response = space(1)
  w_nt = 0
  w_nConn = 0
  w_cTable = space(50)
  w_cTableName = space(50)
  w_FileZip = space(254)
  w_OK = .f.
  w_Cartella = space(254)
  w_cWhere = space(50)
  w_OKTOTAL = .f.
  w_Loop = 0
  w_LoopC = 0
  w_CurName = space(20)
  w_SFILTRO = space(254)
  w_LoopK = 0
  w_CHIAVE = space(40)
  w_IDX = 0
  w_CAMPO = space(30)
  w_Cicla = .f.
  w_Mess = space(254)
  * --- WorkFile variables
  ENT_DETT_idx=0
  ENT_MAST_idx=0
  SUENMAST_idx=0
  SUENDETT_idx=0
  TAB_SECO_idx=0
  CAMPIEST_idx=0
  TAB_ESTE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Impotr Export Entit� e super Entit� Logistica Remota da GSLR_KIE
    * --- Campi di attivazione Import/Export
    * --- Variabili per le connessioni al database
    * --- Numero area di lavoro tabella
    this.w_nt = 0
    * --- Numero connessione
    this.w_nConn = 0
    * --- Nome fisico tabella
    this.w_cTable = ""
    * --- Nome Logico Tabella
    this.w_cTableName = ""
    * --- Filtro nella select in Export
    * --- Svuoto il log
    this.oParentObject.w_Msg = ""
    * --- Gestione errori
    this.w_OK = .T.
    * --- Path assoluta del file Zip
    this.w_FileZip = Alltrim(this.oParentObject.w_PATH)
    * --- Creo una cartella particolare nella temp per poi cancellarla
    this.w_Cartella = Alltrim(Tempadhoc() )+"\"+ Sys(2015)+"\"
    * --- Nel caso in cui fallisse la creazione dello Zip o dei DBF (possibile mancanza di diritti)
     
 l_OldErr = On("Error") 
 On Error this.w_OK=.F.
    if this.oParentObject.w_VERBOSE
      if this.oParentObject.w_IMPEXP="E"
        AddMsgNL("Inizio procedura di esportazione...", this)
      else
        AddMsgNL("Inizio procedura di importazione...", this)
      endif
    endif
    this.w_OKTOTAL = .T.
    if this.oParentObject.w_IMPEXP = "E"
      this.Pag4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if this.Response="S"
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      if this.w_OK
        * --- Creo Zip e cancello cartella nella temp
        this.Pag8()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      else
        if this.oParentObject.w_VERBOSE
          AddMsgNL("Errore nella creazione dei file DBF", this )
        else
          ah_ErrorMsg("Errore nella creazione dei file DBF","Stop","")
        endif
      endif
    else
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    wait clear
    * --- Ripristino l'errore
     
 On Error &l_OldErr
    if this.w_OKTOTAL
      if this.oParentObject.w_IMPEXP="I"
        if this.oParentObject.w_VERBOSE
          AddMsgNL("Procedura di importazione terminata con successo", this )
        else
          ah_ErrorMsg("Procedura di importazione terminata con successo","i","")
        endif
      else
        if this.oParentObject.w_VERBOSE
          AddMsgNL("Procedura di esportazione terminata con successo", this)
        else
          ah_ErrorMsg("Procedura di esportazione terminata con successo","i","")
        endif
      endif
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Export Tabelle
    * --- Databases ENTITA' - Archivi delle entit�
    * --- Nel nome dei DBF metto come prefisso il numero che indica l'ordine di importazione
    *     seguito da una cancelletto (#) per dividere il numero dal nome vero della tabella
    this.w_cWhere = ""
    if ! Directory(this.w_Cartella)
      * --- Se non esiste la cartella la creo
      if ! MakeDir(this.w_Cartella)
        if this.oParentObject.w_VERBOSE
          AddMsgNL("Impossibile creare la cartella %1", this, Alltrim(this.w_Cartella) )
        else
          ah_ErrorMsg("Impossibile creare la cartella %1","Stop","", Alltrim(this.w_Cartella))
        endif
      endif
    endif
    if this.oParentObject.w_VERBOSE
      AddMsgNL("Export super entit�...", this )
    else
      ah_Msg("Export super entit�...",.T.)
    endif
    this.w_cTableName = "SUENMAST"
    Filedbf = this.w_Cartella+"1#"+this.w_cTableName+".DBF"
    this.Pag7()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if used("CursDBF")
      * --- Export Super Entit� (SUENMAST.DBF)
      if this.response="S"
        this.Pag6()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    * --- Fine esportazione di Super Entit� SUENMAST
    *     Inizio Esportazione dellaglio Super Entit� SUENDETT
    if this.oParentObject.w_VERBOSE
      AddMsgNL("Export dettaglio super entit�...", this )
    else
      ah_Msg("Export dettaglio super entit�...",.T.)
    endif
    this.w_cTableName = "SUENDETT"
    Filedbf = this.w_Cartella+"2#"+this.w_cTableName+".DBF"
    this.Pag7()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if used("CursDBF")
      * --- Export super entit� dettaglio (SUENDETT.DBF)
      if this.response="S"
        this.Pag6()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    * --- Fine esportazione di Super Entit� dettaglio SUENDETT
    *     Inizio Esportazione TSM TAB_SECO
    if this.oParentObject.w_VERBOSE
      AddMsgNL("Export TSM...", this )
    else
      ah_Msg("Export TSM...",.T.)
    endif
    this.w_cTableName = "TAB_SECO"
    Filedbf = this.w_Cartella+"3#"+this.w_cTableName+".DBF"
    this.Pag7()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if used("CursDBF")
      * --- Export TSM(TAB_SECO.DBF)
      if this.response="S"
        this.Pag6()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    if this.oParentObject.w_VERBOSE
      AddMsgNL("Export entit�...", this )
    else
      ah_Msg("Export entit�...",.T.)
    endif
    this.w_cTableName = "ENT_MAST"
    this.w_cWhere = " where ENTIPENT = 'L' "
    Filedbf = this.w_Cartella+"4#"+this.w_cTableName+".DBF"
    this.Pag7()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_cWhere = ""
    if used("CursDBF")
      * --- Export Entit� (ENT_MAST.DBF)
      if this.response="S"
        this.Pag6()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    * --- Fine esportazione di ENT_MAST
    if this.oParentObject.w_VERBOSE
      AddMsgNL("Export dettaglio entit�...", this )
    else
      ah_Msg("Export dettaglio entit�...",.T.)
    endif
    this.w_cTableName = "ENT_DETT"
    Filedbf = this.w_Cartella+"5#"+this.w_cTableName+".DBF"
    this.Pag7()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if used("CursDBF")
      * --- Export dettagilo entit�(ENT_DETT.DBF)
      if this.response="S"
        this.Pag6()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    if this.oParentObject.w_VERBOSE
      AddMsgNL("Tabelle estese...", this )
    else
      ah_Msg("Tabelle estese...",.T.)
    endif
    this.w_cTableName = "TAB_ESTE"
    Filedbf = this.w_Cartella+"4#"+this.w_cTableName+".DBF"
    this.Pag7()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if used("CursDBF")
      * --- Export Entit� (TAB_ESTE)
      if this.response="S"
        this.Pag6()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    * --- Fine esportazione di CAMPIEST
    if this.oParentObject.w_VERBOSE
      AddMsgNL("Export campi estensione..", this )
    else
      ah_Msg("Export campi estensione...",.T.)
    endif
    this.w_cTableName = "CAMPIEST"
    Filedbf = this.w_Cartella+"5#"+this.w_cTableName+".DBF"
    this.Pag7()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if used("CursDBF")
      * --- Export dettagilo entit�(CAMPIEST.DBF)
      if this.response="S"
        this.Pag6()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    this.Pag5()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Import Tabelle
    * --- L'immancabile chiamata alla CP_READXDC...
    =cp_ReadXdc()
    * --- Scompattazione zip
    Local L_RetMsg
    L_RetMsg = ""
    if ! ZIPUNZIP("U",this.w_FileZip, this.w_Cartella, this.oParentObject.w_PWD, @L_RetMsg)
      if this.oParentObject.w_VERBOSE
        AddMsgNL("Impossibile scompattare il pacchetto.%0%1", this, L_RetMsg)
      else
        ah_ErrorMsg("Impossibile scompattare il pacchetto.%0%1","i","", L_RetMsg)
      endif
      this.w_OKTOTAL = .F.
    endif
    * --- Elimino i DBF e la cartella
    * --- Lettura files DBF
    * --- Super Entit�
    *     Se non esiste il DBF mi fermo perch� non � possibile generare le Entit� senza super entit�
    if File( this.w_Cartella+"1#SUENMAST.DBF")
      * --- Try
      local bErr_037C66B0
      bErr_037C66B0=bTrsErr
      this.Try_037C66B0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        this.w_OKTOTAL = .F.
        * --- Raise
        i_Error=""
        return
      endif
      bTrsErr=bTrsErr or bErr_037C66B0
      * --- End
    else
      if this.oParentObject.w_VERBOSE
        AddMsgNL("Non � presente il DBF relativo alle super entit�. Impossibile continuare", this)
      else
        ah_ErrorMsg("Non � presente il DBF relativo alle super entit�. Impossibile continuare","i","")
      endif
      this.w_OKTOTAL = .F.
    endif
    if ! DeleteFolder(this.w_Cartella)
      if this.oParentObject.w_VERBOSE
        AddMsgNL("Problemi nella cancellazione della cartella temporanea %1", this, Alltrim(this.w_Cartella))
      else
        ah_ErrorMsg("Problemi nella cancellazione della cartella temporanea %1","i","", Alltrim(this.w_Cartella))
      endif
      this.w_OKTOTAL = .F.
    endif
    this.Pag5()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc
  proc Try_037C66B0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Creo array con elenco DBF presenti nello zip
    Adir(ArrDbf, this.w_Cartella+"*.DBF")
    if this.oParentObject.w_DELTAB="S"
      * --- Se devo cancellare gli archivi esamino l'array partendo dal fondo...
      this.w_Loop = Alen(ArrDbf,1)
      do while this.w_LOOP>0
        this.w_cTableName = Alltrim( ArrDbf( this.w_Loop, 1) )
        * --- Elimino dal nome tabella il numero usato per l'ordine di importazione
        this.w_cTableName = SubStr( this.w_cTableName, At("#", this.w_cTableName) +1 )
        * --- Elimino l'estensione per avere il solo nome tabella
        this.w_cTableName = StrTran( this.w_cTableName, ".DBF", "")
        if i_Dcx.GetFieldsCount( this.w_cTableName )>0
          * --- Se Entit� allora devo eliminare solo quelle della logistica remota (ENTIPENT='L'
          if this.w_cTableName="ENT_DETT"
            AddMsgNL("Eliminazione archivio ENT_DETT", this , , )
            * --- Delete from ENT_DETT
            i_nConn=i_TableProp[this.ENT_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ENT_DETT_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              local i_cQueryTable,i_cWhere,i_aIndex
              declare i_aIndex[1]
              i_cQueryTable=cp_SetAzi(i_TableProp[this.ENT_MAST_idx,2])
              i_cTempTable=cp_GetTempTableName(i_nConn)
              i_aIndex[1]='ENCODICE'
              cp_CreateTempTable(i_nConn,i_cTempTable,"ENCODICE "," from "+i_cQueryTable+" where ENTIPENT='L'",.f.,@i_aIndex)
              i_cQueryTable=i_cTempTable
              i_cWhere=i_cTable+".ENCODICE = "+i_cQueryTable+".ENCODICE";
            
              i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
                    +" where exists( select 1 from "+i_cQueryTable+" where ";
                    +""+i_cTable+".ENCODICE = "+i_cQueryTable+".ENCODICE";
                    +")")
              cp_DropTempTable(i_nConn,i_cQueryTable)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              * --- Error: delete not accepted
              i_Error=MSG_DELETE_ERROR
              return
            endif
          else
            if this.w_cTableName="ENT_MAST"
              this.w_SFILTRO = "ENTIPENT="+Cp_tostrodbc("L")
            else
              this.w_SFILTRO = "1=1"
            endif
            if Not Empty( DELETETABLE( this.w_cTableName , , this , this.oParentObject.w_VERBOSE , this.w_SFILTRO ) )
              * --- Raise
              i_Error="Errore cancellazione"
              return
            endif
          endif
        endif
        this.w_Loop = this.w_Loop -1
      enddo
    endif
    this.w_Loop = 1
    * --- Ciclo sui Dbf presenti nello Zip
    do while this.w_Loop <= Alen(ArrDbf,1)
      * --- Path del DBF
      Filedbf = this.w_Cartella + Alltrim( ArrDbf( this.w_Loop, 1) )
      this.w_cTableName = Alltrim( ArrDbf( this.w_Loop, 1) )
      * --- Elimino dal nome tabella il numero usato per l'ordine di importazione
      this.w_cTableName = SubStr( this.w_cTableName, At("#", this.w_cTableName) +1 )
      * --- Elimino l'estensione per avere il solo nome tabella
      this.w_cTableName = StrTran( this.w_cTableName, ".DBF", "")
      this.w_LoopC = 1
      * --- Ciclo sui campi della tabella per creare l'array dei campi con i valori
      if i_Dcx.GetFieldsCount( this.w_cTableName )>0
        do while this.w_LoopC<= i_Dcx.GetFieldsCount( this.w_cTableName )
           
 Dimension ArrCampi[ this.w_LoopC , 2 ] 
 ArrCampi[ this.w_LoopC ,1 ] = Alltrim( i_Dcx.GetFieldName( this.w_cTableName , this.w_LoopC ) ) 
          this.w_LoopC = this.w_LoopC + 1
        enddo
         
 Dimension ArrCampi[ this.w_LoopC ,2 ] 
 ArrCampi[ this.w_LoopC ,1 ] = "CPCCCHK"
        Dimension pArrDb[1,2]
        this.w_CHIAVE = cp_KeyToSQL ( I_DCX.GetIdxDef( this.w_cTableName ,1 ) )
        this.w_IDX = 1
        this.w_LoopK = 1
        do while this.w_LoopK<>0
          this.w_LoopK = At( "," , this.w_CHIAVE )
          if this.w_LoopK>0
            this.w_CAMPO = Left( this.w_CHIAVE , this.w_LoopK -1 )
          else
            this.w_CAMPO = Alltrim( this.w_CHIAVE )
          endif
           
 Dimension pArrDb[ this.w_IDX ,2 ] 
 pArrDb[ this.w_IDX ,1 ] = this.w_CAMPO
          this.w_CHIAVE = Alltrim( Substr( this.w_CHIAVE , this.w_LoopK+1 , Len ( this.w_CHIAVE ) ) )
          this.w_IDX = this.w_IDX + 1
        enddo
        * --- Ora ciclo sul Dbf.
        *     Riga per riga valorizzo l'array e inserisco nella tabella
        * --- Apro il Dbf e gli do il nome della tabella poich� il # nel nome del Dbf fa si
        *     che lo apra con un nome errato
         
 Use( Filedbf ) in 0 alias (this.w_cTableName)
        * --- Ciclo sul Dbf
        do while Not Eof()
          this.w_LoopC = 1
          this.w_CHIAVE = ""
          * --- Ciclo sull'array dei campi e inserisco il valore di ciascuno
          do while this.w_LoopC <= Alen(ArrCampi,1)
            * --- Se sto inserendo il valore della validazione e si tratta della tabella principale
            *     e non ho attivo il check 'mantieni validazione', metto sempre validazione 1=0
            if Upper(Alltrim(this.w_cTableName))="ENT_DETT" And ArrCampi[ this.w_LoopC ,1 ]="ENVALEXP" And Eval(this.w_cTableName+".ENPRINCI")="S" And this.oParentObject.w_MANVAL="N"
              ArrCampi[ this.w_LoopC ,2 ] = "1=0"
            else
              ArrCampi[ this.w_LoopC ,2 ] = Eval(this.w_cTableName+"."+ArrCampi[ this.w_LoopC ,1 ])
            endif
            * --- Controllo se il campo fa parte della chiave
            if Ascan(pArrDb, Alltrim(ArrCampi[ this.w_LoopC ,1 ]) ) >0
              this.w_IDX = 1
              this.w_Cicla = .T.
              do while this.w_Cicla
                if Upper(Alltrim(pArrDb(this.w_IDX, 1))) = Upper(Alltrim(ArrCampi[ this.w_LoopC ,1 ]))
                  * --- Valorizzo l'array delle chiavi
                   
 pArrDb[ this.w_IDX ,2 ] = ArrCampi[ this.w_LoopC , 2 ]
                  * --- Elenco le chiavi per messaggio
                  this.w_CHIAVE = this.w_CHIAVE + IIF(Empty(this.w_CHIAVE),"", " - " ) + Alltrim(ArrCampi[ this.w_LoopC , 2 ])
                  * --- Esco dal ciclo
                  this.w_Cicla = .F.
                endif
                this.w_IDX = this.w_IDX + 1
              enddo
            endif
            this.w_LoopC = this.w_LoopC + 1
          enddo
          if Not Empty( INSERTTABLE( this.w_cTableName , @ArrCampi , this, this.oParentObject.w_Verbose ) )
            * --- Se la insert da errore accetto e provo a fare la Update
            * --- accept error
            bTrsErr=.f.
            this.w_Mess = WRITETABLE( this.w_cTableName , @ArrCampi ,@pArrDb ,this,this.oParentObject.w_Verbose )
            if Empty( this.w_Mess )
              if this.oParentObject.w_Verbose
                AddMsgNL("Modificato record con chiave %1 nella tabella %2", this , this.w_Chiave , this.w_cTableName)
              endif
            else
              * --- Raise
              i_Error=this.w_Mess
              return
            endif
          else
            * --- Altrimenti record inserito
            if this.oParentObject.w_Verbose
              AddMsgNL("Inserito record con chiave %1 nella tabella %2", this , this.w_Chiave , this.w_cTableName)
            endif
          endif
           
 Select( this.w_cTableName ) 
 Skip
        enddo
        * --- Rilascio il DBF aperto con il nome della tabella come alias
         
 Select( this.w_cTableName ) 
 Use 
      else
        if this.oParentObject.w_VERBOSE
          AddMsgNL("Errore tabella %1 non in analisi...", this , this.w_CTABLENAME )
        endif
        * --- Raise
        i_Error="Tabella non in analisi"
        return
      endif
      this.w_Loop = this.w_Loop + 1
    enddo
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Verifica esistenza file da esportare
    this.response = "S"
    if file( this.w_FileZip)
      if not ah_YesNo("E' gi� presente il file:%1. Si desidera sovrascrivere?","",this.w_FileZip)
        this.response = "N"
      else
        * --- Visto che vado sempre in append dello Zip, se voglio sovrascrivere prima lo elimino
        Erase(this.w_FileZip)
      endif
    endif
  endproc


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Chiusura cursore generico
    if used("Cursdbf")
      select Cursdbf
      use
    endif
    if Used( this.w_cTableName )
      select( this.w_cTableName )
      use
    endif
  endproc


  procedure Pag6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Scrittura su file DBF
    if used("CursDBF")
      if this.oParentObject.w_VERBOSE
        AddMsgNL("Aggiornamento zip per tabella %1", this, Alltrim(this.w_cTableName) )
      else
        ah_Msg("Aggiornamento zip per tabella %1",.T., , Alltrim(this.w_cTableName))
      endif
      select CursDBF
      go top
      copy to (Filedbf) TYPE FOX2X
    endif
  endproc


  procedure Pag7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Crea il cursore "CursDBF" dalla tabella (w_cTableName)
    *     
    *     Ritorno:
    *     - CursDBF
    *     - i_Rows con numero record tabella
    * --- --
    if used("CursDBF")
      select CursDBF
      use
    endif
    * --- Cerca area di lavoro tabella aperta in Work Table
    private cIDX
    cIDX = alltrim(this.w_cTableName)+"_idx"
    this.w_nT = this.&cIDX
    this.w_nConn = i_TableProp[ this.w_nT ,3]
    this.w_cTable = cp_SetAzi(i_TableProp[ this.w_nT ,2])
    if this.w_nConn<>0
      * --- SQL Server
      i_Rows = SqlExec( this.w_nConn,"select * from " + this.w_cTable + this.w_cWhere,"CursDBF")
      i_Rows = iif(used("CursDBF"),reccount(),0)
    else
      * --- Fox Pro
       
 Local cWhere 
 cWhere = this.w_cWhere
      cTable = this.w_cTable
      select * from (cTable) &cWhere into cursor CursDBF
      i_Rows = _TALLY
    endif
    if not used("CursDBF")
      if this.oParentObject.w_VERBOSE
        AddMsgNL("Impossibile creare il cursore di esportazione per la tabella %1", this, Alltrim(this.w_cTableName) )
      else
        AH_ERRORMSG("Impossibile creare il cursore di esportazione per la tabella %1" ,"!","",Alltrim(this.w_cTableName))
      endif
    endif
    if i_Rows=0
      if this.oParentObject.w_VERBOSE
        AddMsgNL("La tabella %1 non contiene dati", this, Alltrim(this.w_cTableName) )
      endif
    endif
  endproc


  procedure Pag8
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Genera Zip e cancella cartella DBF
    * --- Aggiungo il DBF allo Zip
    Local L_RetMsg
    L_RetMsg = ""
    if ! ZIPUNZIP("Z",this.w_FileZip, this.w_Cartella, this.oParentObject.w_PWD, @L_RetMsg)
      if this.oParentObject.w_VERBOSE
        AddMsgNL("Problemi nella creazione dello zip %1%0%2",this, Alltrim(this.w_FileZip), L_RetMsg)
      else
        ah_ErrorMsg("Problemi nella creazione dello zip %1%0%2","i","",Alltrim(this.w_FileZip), L_RetMsg)
      endif
      this.w_OK = .F.
    endif
    if ! DeleteFolder(this.w_Cartella)
      if this.oParentObject.w_VERBOSE
        AddMsgNL("Problemi nella cancellazione della cartella temporanea %1",this, Alltrim(this.w_Cartella))
      else
        ah_ErrorMsg("Problemi nella cancellazione della cartella temporanea %1","i","",Alltrim(this.w_FileZip), L_RetMsg)
      endif
    endif
    if Not this.w_OK
      if this.oParentObject.w_VERBOSE
        AddMsgNL("Errore nella creazione del file Ahz", this )
      else
        ah_ErrorMsg("Errore nella creazione del file Ahz","Stop","")
      endif
      i_retcode = 'stop'
      return
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,7)]
    this.cWorkTables[1]='ENT_DETT'
    this.cWorkTables[2]='ENT_MAST'
    this.cWorkTables[3]='SUENMAST'
    this.cWorkTables[4]='SUENDETT'
    this.cWorkTables[5]='TAB_SECO'
    this.cWorkTables[6]='CAMPIEST'
    this.cWorkTables[7]='TAB_ESTE'
    return(this.OpenAllTables(7))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
