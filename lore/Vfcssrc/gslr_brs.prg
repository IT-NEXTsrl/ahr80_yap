* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslr_brs                                                        *
*              Ras phonebook                                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-10-03                                                      *
* Last revis.: 2006-10-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgslr_brs",oParentObject)
return(i_retval)

define class tgslr_brs as StdBatch
  * --- Local variables
  w_ConnSstatus = space(254)
  w_StruSize = 0
  w_EntCnt = 0
  w_Ret = 0
  w_Loop = 0
  w_Offset = 0
  w_SERASCON = space(254)
  * --- WorkFile variables
  PUBLMAST_idx=0
  TMP_PHONEBOOK_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- RAS PHONEBOOK
     
 DECLARE INTEGER RasEnumEntries IN rasapi32 ; 
 STRING reserved,; 
 STRING phonebook,; 
 STRING @rasentry,; 
 INTEGER @lnSize,; 
 INTEGER @lnEntries
    this.w_ConnSstatus = this.long2str(264) + replicate(chr(0),257)
    this.w_StruSize = 0
    this.w_EntCnt = 0
    this.w_Loop = 1
    this.w_Offset = 0
    * --- Utilizzo variabili locali
     
 LOCAL l_ConnSstatus, l_StruSize, l_EntCnt 
 l_ConnSstatus = this.w_ConnSstatus 
 l_StruSize = this.w_StruSize 
 l_EntCnt = this.w_EntCnt
    * --- La prima chiamta ritorna 603 che indica che la struttura � troppo piccola
    *     La lancio per sapere il numero di entry
    this.w_Ret = RasEnumEntries(null, null, @l_ConnSstatus, @l_StruSize, @l_EntCnt)
    this.w_StruSize = l_StruSize
    this.w_EntCnt = l_EntCnt
    this.w_ConnSstatus = ""
    do while this.w_Loop <= this.w_EntCnt
      this.w_ConnSstatus = this.w_ConnSstatus + this.long2str(264) + replicate(chr(0),257)
      this.w_Loop = this.w_Loop + 1
    enddo
     
 l_ConnSstatus = this.w_ConnSstatus 
 l_StruSize = this.w_StruSize 
 l_EntCnt = this.w_EntCnt
    this.w_Ret = RasEnumEntries(null, null, @l_ConnSstatus, @l_StruSize, @l_EntCnt)
    this.w_StruSize = l_StruSize
    this.w_EntCnt = l_EntCnt
    this.w_ConnSstatus = l_ConnSstatus
    * --- Create temporary table TMP_PHONEBOOK
    i_nIdx=cp_AddTableDef('TMP_PHONEBOOK') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.PUBLMAST_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.PUBLMAST_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"SERASCON "," from "+i_cTable;
          +" where 1=0";
          )
    this.TMP_PHONEBOOK_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    this.w_Loop = 1
    this.w_Offset = 5
    do while this.w_Loop <= this.w_EntCnt
      * --- Insert into TMP_PHONEBOOK
      i_nConn=i_TableProp[this.TMP_PHONEBOOK_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMP_PHONEBOOK_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMP_PHONEBOOK_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"SERASCON"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.TrimRAS(substr(this.w_ConnSstatus, this.w_Offset, 256))),'TMP_PHONEBOOK','SERASCON');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'SERASCON',this.TrimRAS(substr(this.w_ConnSstatus, this.w_Offset, 256)))
        insert into (i_cTable) (SERASCON &i_ccchkf. );
           values (;
             this.TrimRAS(substr(this.w_ConnSstatus, this.w_Offset, 256));
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      this.w_Offset = this.w_Offset + 264
      this.w_Loop = this.w_Loop + 1
    enddo
    vx_exec("..\LORE\EXE\QUERY\GSLR_BRS.VZM",this)
    * --- Drop temporary table TMP_PHONEBOOK
    i_nIdx=cp_GetTableDefIdx('TMP_PHONEBOOK')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMP_PHONEBOOK')
    endif
    * --- Valorizzo caller
    This.oParentObject.w_SERASCON = this.w_SERASCON
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='PUBLMAST'
    this.cWorkTables[2]='*TMP_PHONEBOOK'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- gslr_brs
   *--- convert integer to high low string to pass in structure. DWORD size is 4 bytes, 8 bits to a byte
   *--- Function long2str - convert 4-byte integer into low-high format Character string.   
  PROCEDURE long2str
    PARAMETERS longval
    PRIVATE intloop, chrret
          chrret = ""   
          FOR i = 24 TO 0 STEP -8
              chrret = CHR(INT(longval/(2^i))) + chrret
              longval = MOD(longval, (2^i))   
          NEXT   
          RETURN chrret
  ENDPROC
  
  *-- to remove all chr(0) from return string in structures. foxpro only works with chr(32) spaces.
  PROCEDURE TrimRAS
      parameter chrstr
      *. when passing structure members all of the space is not used leaving the remainder with the
      *. initialized chr(0).  FoxPros trim function only works with spaces chr(32).
      private intlen, intloop, chrret, chrone
      intlen = 0
      intloop = 0
      chrret = ''
      chrone = ''
      if parameters() < 1
          return .f.
      endif
  
      intlen = len(chrstr)
      for intloop = 1 to intlen
          chrone = substr(chrstr,intloop,1)
          chrret = chrret + iif(between(asc(chrone),32,126),chrone,'')
      next
      return chrret
  ENDPROC
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
