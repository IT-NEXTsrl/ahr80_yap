* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslr_mds                                                        *
*              Dettaglio pacchetti sede (log)                                  *
*                                                                              *
*      Author: Zucchetti SPA                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161][VRS_9]                                                    *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-10-13                                                      *
* Last revis.: 2011-02-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgslr_mds")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgslr_mds")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgslr_mds")
  return

* --- Class definition
define class tgslr_mds as StdPCForm
  Width  = 586
  Height = 541
  Top    = 24
  Left   = 13
  cComment = "Dettaglio pacchetti sede (log)"
  cPrg = "gslr_mds"
  HelpContextID=80309353
  add object cnt as tcgslr_mds
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgslr_mds as PCContext
  w_DSSERIAL = space(10)
  w_DS__SEDE = space(2)
  w_DSNUMPAC = 0
  w_DESSED = space(50)
  w_TIPSED = space(1)
  proc Save(i_oFrom)
    this.w_DSSERIAL = i_oFrom.w_DSSERIAL
    this.w_DS__SEDE = i_oFrom.w_DS__SEDE
    this.w_DSNUMPAC = i_oFrom.w_DSNUMPAC
    this.w_DESSED = i_oFrom.w_DESSED
    this.w_TIPSED = i_oFrom.w_TIPSED
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_DSSERIAL = this.w_DSSERIAL
    i_oTo.w_DS__SEDE = this.w_DS__SEDE
    i_oTo.w_DSNUMPAC = this.w_DSNUMPAC
    i_oTo.w_DESSED = this.w_DESSED
    i_oTo.w_TIPSED = this.w_TIPSED
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgslr_mds as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 586
  Height = 541
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-02-25"
  HelpContextID=80309353
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=5

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  DETT_LOG_IDX = 0
  PUBLMAST_IDX = 0
  cFile = "DETT_LOG"
  cKeySelect = "DSSERIAL"
  cKeyWhere  = "DSSERIAL=this.w_DSSERIAL"
  cKeyDetail  = "DSSERIAL=this.w_DSSERIAL and DS__SEDE=this.w_DS__SEDE"
  cKeyWhereODBC = '"DSSERIAL="+cp_ToStrODBC(this.w_DSSERIAL)';

  cKeyDetailWhereODBC = '"DSSERIAL="+cp_ToStrODBC(this.w_DSSERIAL)';
      +'+" and DS__SEDE="+cp_ToStrODBC(this.w_DS__SEDE)';

  cKeyWhereODBCqualified = '"DETT_LOG.DSSERIAL="+cp_ToStrODBC(this.w_DSSERIAL)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cPrg = "gslr_mds"
  cComment = "Dettaglio pacchetti sede (log)"
  i_nRowNum = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_DSSERIAL = space(10)
  w_DS__SEDE = space(2)
  w_DSNUMPAC = 0
  w_DESSED = space(50)
  w_TIPSED = space(1)

  * --- Children pointers
  GSLR_MDE = .NULL.
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    *set procedure to GSLR_MDE additive
    with this
      .Pages(1).addobject("oPag","tgslr_mdsPag1","gslr_mds",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Dettaglio")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    *release procedure GSLR_MDE
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='PUBLMAST'
    this.cWorkTables[2]='DETT_LOG'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.DETT_LOG_IDX,5],7]
    this.nPostItConn=i_TableProp[this.DETT_LOG_IDX,3]
  return

  function CreateChildren()
    this.GSLR_MDE = CREATEOBJECT('stdDynamicChild',this,'GSLR_MDE',this.oPgFrm.Page1.oPag.oLinkPC_2_3)
    this.GSLR_MDE.createrealchild()
    return

  procedure NewContext()
    return(createobject('tsgslr_mds'))

  function DestroyChildrenChain()
    this.oParentObject=.NULL.
    if !ISNULL(this.GSLR_MDE)
      this.GSLR_MDE.DestroyChildrenChain()
    endif
    return

  function HideChildrenChain()
    *this.Hide()
    this.bOnScreen = .f.
    this.GSLR_MDE.HideChildrenChain()
    return

  function ShowChildrenChain()
    this.bOnScreen=.t.
    this.GSLR_MDE.ShowChildrenChain()
    DoDefault()
    return
  procedure DestroyChildren()
    if !ISNULL(this.GSLR_MDE)
      this.GSLR_MDE.DestroyChildrenChain()
      this.GSLR_MDE=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_2_3')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSLR_MDE.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSLR_MDE.IsAChildUpdated(.t.)
    endif	
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSLR_MDE.NewDocument()
    return

  procedure ChildrenChangeRow()
    local i_cOldSel,i_cRow
    i_cOldSel=select()
    select (this.cTrsName)
    i_cRow=str(recno(),7,0)
    with this
      .GSLR_MDE.ChangeRow(this.cRowID+i_cRow,1;
             ,.w_DSSERIAL,"DESERIAL";
             ,.w_DS__SEDE,"DE__SEDE";
             )
    endwith
    select (i_cOldSel)
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from DETT_LOG where DSSERIAL=KeySet.DSSERIAL
    *                            and DS__SEDE=KeySet.DS__SEDE
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.DETT_LOG_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DETT_LOG_IDX,2],this.bLoadRecFilter,this.DETT_LOG_IDX,"gslr_mds")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('DETT_LOG')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "DETT_LOG.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' DETT_LOG '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'DSSERIAL',this.w_DSSERIAL  )
      select * from (i_cTable) DETT_LOG where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_TIPSED = "P"
        .w_DSSERIAL = NVL(DSSERIAL,space(10))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'DETT_LOG')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_DESSED = space(50)
          .w_DS__SEDE = NVL(DS__SEDE,space(2))
          .link_2_1('Load')
          .w_DSNUMPAC = NVL(DSNUMPAC,0)
          select (this.cTrsName)
          append blank
          replace DSSERIAL with .w_DSSERIAL
          replace DS__SEDE with .w_DS__SEDE
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_DSSERIAL=space(10)
      .w_DS__SEDE=space(2)
      .w_DSNUMPAC=0
      .w_DESSED=space(50)
      .w_TIPSED=space(1)
      if .cFunction<>"Filter"
        .DoRTCalc(1,2,.f.)
        if not(empty(.w_DS__SEDE))
         .link_2_1('Full')
        endif
        .DoRTCalc(3,4,.f.)
        .w_TIPSED = "P"
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'DETT_LOG')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    this.GSLR_MDE.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'DETT_LOG',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSLR_MDE.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.DETT_LOG_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DSSERIAL,"DSSERIAL",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_DS__SEDE C(2);
      ,t_DSNUMPAC N(10);
      ,t_DESSED C(50);
      ,DSSERIAL C(10);
      ,DS__SEDE C(2);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgslr_mdsbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDS__SEDE_2_1.controlsource=this.cTrsName+'.t_DS__SEDE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDSNUMPAC_2_2.controlsource=this.cTrsName+'.t_DSNUMPAC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDESSED_2_4.controlsource=this.cTrsName+'.t_DESSED'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(73)
    this.AddVLine(382)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDS__SEDE_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.DETT_LOG_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DETT_LOG_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DETT_LOG_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DETT_LOG_IDX,2])
      *
      * insert into DETT_LOG
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'DETT_LOG')
        i_extval=cp_InsertValODBCExtFlds(this,'DETT_LOG')
        i_cFldBody=" "+;
                  "(DSSERIAL,DS__SEDE,DSNUMPAC,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_DSSERIAL)+","+cp_ToStrODBCNull(this.w_DS__SEDE)+","+cp_ToStrODBC(this.w_DSNUMPAC)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'DETT_LOG')
        i_extval=cp_InsertValVFPExtFlds(this,'DETT_LOG')
        cp_CheckDeletedKey(i_cTable,0,'DSSERIAL',this.w_DSSERIAL,'DS__SEDE',this.w_DS__SEDE)
        INSERT INTO (i_cTable) (;
                   DSSERIAL;
                  ,DS__SEDE;
                  ,DSNUMPAC;
                  ,CPCCCHK &i_extfld.) VALUES (;
                  this.w_DSSERIAL;
                  ,this.w_DS__SEDE;
                  ,this.w_DSNUMPAC;
                  ,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.DETT_LOG_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DETT_LOG_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
          if this.w_DSSERIAL<>DSSERIAL
            i_bUpdAll = .t.
          endif
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_DS__SEDE))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'DETT_LOG')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and DS__SEDE="+cp_ToStrODBC(&i_TN.->DS__SEDE)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'DETT_LOG')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and DS__SEDE=&i_TN.->DS__SEDE;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
          if this.w_DSSERIAL<>DSSERIAL
            i_bUpdAll = .t.
          endif
        endif
        scan for (not(Empty(t_DS__SEDE))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete row children
              this.GSLR_MDE.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
                     ,this.w_DSSERIAL,"DESERIAL";
                     ,this.w_DS__SEDE,"DE__SEDE";
                     )
              this.GSLR_MDE.mDelete()
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and DS__SEDE="+cp_ToStrODBC(&i_TN.->DS__SEDE)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and DS__SEDE=&i_TN.->DS__SEDE;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = 0
              replace DS__SEDE with this.w_DS__SEDE
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update DETT_LOG
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'DETT_LOG')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " DSNUMPAC="+cp_ToStrODBC(this.w_DSNUMPAC)+;
                     ",DS__SEDE="+cp_ToStrODBC(this.w_DS__SEDE)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+;
                             " and DS__SEDE="+cp_ToStrODBC(DS__SEDE)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'DETT_LOG')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      DSNUMPAC=this.w_DSNUMPAC;
                     ,DS__SEDE=this.w_DS__SEDE;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                                      and DS__SEDE=&i_TN.->DS__SEDE;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
      * --- Ask to row children to save themselves
      select (this.cTrsName)
      i_TN = this.cTrsName
      scan for (not(Empty(t_DS__SEDE)))
        * --- > Optimize children saving
        i_nRec = recno()
        this.WorkFromTrs()
        if not(deleted())
          this.GSLR_MDE.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
               ,this.w_DSSERIAL,"DESERIAL";
               ,this.w_DS__SEDE,"DE__SEDE";
               )
          this.GSLR_MDE.mReplace()
          this.GSLR_MDE.bSaveContext=.f.
        endif
      endscan
     this.GSLR_MDE.bSaveContext=.t.
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.DETT_LOG_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DETT_LOG_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_DS__SEDE))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_nRec = recno()
        * --- GSLR_MDE : Deleting
        this.GSLR_MDE.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
               ,this.w_DSSERIAL,"DESERIAL";
               ,this.w_DS__SEDE,"DE__SEDE";
               )
        this.GSLR_MDE.mDelete()
        if bTrsErr
          i_nModRow = -1
          exit
        endif
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete DETT_LOG
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and DS__SEDE="+cp_ToStrODBC(&i_TN.->DS__SEDE)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and DS__SEDE=&i_TN.->DS__SEDE;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_DS__SEDE))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DETT_LOG_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DETT_LOG_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,5,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=DS__SEDE
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PUBLMAST_IDX,3]
    i_lTable = "PUBLMAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PUBLMAST_IDX,2], .t., this.PUBLMAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PUBLMAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DS__SEDE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'PUBLMAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SECODICE like "+cp_ToStrODBC(trim(this.w_DS__SEDE)+"%");
                   +" and SE__TIPO="+cp_ToStrODBC(this.w_TIPSED);

          i_ret=cp_SQL(i_nConn,"select SE__TIPO,SECODICE,SEDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SE__TIPO,SECODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SE__TIPO',this.w_TIPSED;
                     ,'SECODICE',trim(this.w_DS__SEDE))
          select SE__TIPO,SECODICE,SEDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SE__TIPO,SECODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DS__SEDE)==trim(_Link_.SECODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DS__SEDE) and !this.bDontReportError
            deferred_cp_zoom('PUBLMAST','*','SE__TIPO,SECODICE',cp_AbsName(oSource.parent,'oDS__SEDE_2_1'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPSED<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SE__TIPO,SECODICE,SEDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select SE__TIPO,SECODICE,SEDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SE__TIPO,SECODICE,SEDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where SECODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and SE__TIPO="+cp_ToStrODBC(this.w_TIPSED);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SE__TIPO',oSource.xKey(1);
                       ,'SECODICE',oSource.xKey(2))
            select SE__TIPO,SECODICE,SEDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DS__SEDE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SE__TIPO,SECODICE,SEDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where SECODICE="+cp_ToStrODBC(this.w_DS__SEDE);
                   +" and SE__TIPO="+cp_ToStrODBC(this.w_TIPSED);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SE__TIPO',this.w_TIPSED;
                       ,'SECODICE',this.w_DS__SEDE)
            select SE__TIPO,SECODICE,SEDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DS__SEDE = NVL(_Link_.SECODICE,space(2))
      this.w_DESSED = NVL(_Link_.SEDESCRI,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_DS__SEDE = space(2)
      endif
      this.w_DESSED = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PUBLMAST_IDX,2])+'\'+cp_ToStr(_Link_.SE__TIPO,1)+'\'+cp_ToStr(_Link_.SECODICE,1)
      cp_ShowWarn(i_cKey,this.PUBLMAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DS__SEDE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDS__SEDE_2_1.value==this.w_DS__SEDE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDS__SEDE_2_1.value=this.w_DS__SEDE
      replace t_DS__SEDE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDS__SEDE_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDSNUMPAC_2_2.value==this.w_DSNUMPAC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDSNUMPAC_2_2.value=this.w_DSNUMPAC
      replace t_DSNUMPAC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDSNUMPAC_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESSED_2_4.value==this.w_DESSED)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESSED_2_4.value=this.w_DESSED
      replace t_DESSED with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESSED_2_4.value
    endif
    cp_SetControlsValueExtFlds(this,'DETT_LOG')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
      endcase
      i_bRes = i_bRes .and. .GSLR_MDE.CheckForm()
      if not(Empty(.w_DS__SEDE))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    * --- GSLR_MDE : Depends On
    this.GSLR_MDE.SaveDependsOn()
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_DS__SEDE)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_DS__SEDE=space(2)
      .w_DSNUMPAC=0
      .w_DESSED=space(50)
      .DoRTCalc(1,2,.f.)
      if not(empty(.w_DS__SEDE))
        .link_2_1('Full')
      endif
    endwith
    this.DoRTCalc(3,5,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_DS__SEDE = t_DS__SEDE
    this.w_DSNUMPAC = t_DSNUMPAC
    this.w_DESSED = t_DESSED
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_DS__SEDE with this.w_DS__SEDE
    replace t_DSNUMPAC with this.w_DSNUMPAC
    replace t_DESSED with this.w_DESSED
    if i_srv='A'
      replace DS__SEDE with this.w_DS__SEDE
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgslr_mdsPag1 as StdContainer
  Width  = 582
  height = 541
  stdWidth  = 582
  stdheight = 541
  resizeXpos=309
  resizeYpos=504
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=30, top=11, width=455,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=3,Field1="DS__SEDE",Label1="Sede",Field2="DESSED",Label2="Descrizione",Field3="DSNUMPAC",Label3="Pacchetto",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 235781498
  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gslr_mde",lower(this.oContained.GSLR_MDE.class))=0
        this.oContained.GSLR_MDE.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=20,top=30,;
    width=451+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=21,top=31,width=450+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='PUBLMAST|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oContained.ChildrenChangeRow()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='PUBLMAST'
        oDropInto=this.oBodyCol.oRow.oDS__SEDE_2_1
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oLinkPC_2_3 as stdDynamicChildContainer with uid="XUBVUWKJHF",bOnScreen=.t.,width=572,height=310,;
   left=5, top=226;
    , caption='\<Dettaglio';


  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgslr_mdsBodyRow as CPBodyRowCnt
  Width=441
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oDS__SEDE_2_1 as StdTrsField with uid="QSJEWBKGPN",rtseq=2,rtrep=.t.,;
    cFormVar="w_DS__SEDE",value=space(2),isprimarykey=.t.,;
    ToolTipText = "Sede dalla quale ho ricevuto i dati",;
    HelpContextID = 97246075,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=41, Left=-2, Top=0, InputMask=replicate('X',2), bHasZoom = .t. , cLinkFile="PUBLMAST", oKey_1_1="SE__TIPO", oKey_1_2="this.w_TIPSED", oKey_2_1="SECODICE", oKey_2_2="this.w_DS__SEDE"

  func oDS__SEDE_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oDS__SEDE_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oDS__SEDE_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    if not(this.parent.oDS__SEDE_2_1.readonly and this.parent.oDS__SEDE_2_1.isprimarykey)
    if i_TableProp[this.parent.oContained.PUBLMAST_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"SE__TIPO="+cp_ToStrODBC(this.Parent.oContained.w_TIPSED)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"SE__TIPO="+cp_ToStr(this.Parent.oContained.w_TIPSED)
    endif
    do cp_zoom with 'PUBLMAST','*','SE__TIPO,SECODICE',cp_AbsName(this.parent,'oDS__SEDE_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
   endif
  endproc

  add object oDSNUMPAC_2_2 as StdTrsField with uid="GTNKLMNDYJ",rtseq=3,rtrep=.t.,;
    cFormVar="w_DSNUMPAC",value=0,;
    ToolTipText = "Numero pacchetto sincronizzato",;
    HelpContextID = 6343545,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=83, Left=353, Top=0, cSayPict=["9999999999"], cGetPict=["9999999999"]

  add object oDESSED_2_4 as StdTrsField with uid="QELURJRVPJ",rtseq=4,rtrep=.t.,;
    cFormVar="w_DESSED",value=space(50),enabled=.f.,;
    ToolTipText = "Descrizione sede",;
    HelpContextID = 203485898,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=303, Left=44, Top=0, InputMask=replicate('X',50)
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oDS__SEDE_2_1.When()
    return(.t.)
  proc oDS__SEDE_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oDS__SEDE_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gslr_mds','DETT_LOG','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".DSSERIAL=DETT_LOG.DSSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
