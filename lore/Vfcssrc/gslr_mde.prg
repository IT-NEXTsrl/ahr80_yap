* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslr_mde                                                        *
*              Dettaglio entit� (log)                                          *
*                                                                              *
*      Author: Zucchetti SPA                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161][VRS_23]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-10-13                                                      *
* Last revis.: 2011-02-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgslr_mde")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgslr_mde")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgslr_mde")
  return

* --- Class definition
define class tgslr_mde as StdPCForm
  Width  = 580
  Height = 312
  Top    = 10
  Left   = 10
  cComment = "Dettaglio entit� (log)"
  cPrg = "gslr_mde"
  HelpContextID=80309353
  add object cnt as tcgslr_mde
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgslr_mde as PCContext
  w_DESERIAL = space(10)
  w_DE__SEDE = space(2)
  w_DEENTITA = space(15)
  w_DENUMINS = 0
  w_DENUMUPD = 0
  w_DENUMDEL = 0
  w_DENUMRIF = 0
  w_DENUMOVW = 0
  w_DEZIPERR = space(254)
  w_DESENT = space(50)
  w_DEZIPOLD = space(254)
  w_TIPSED = space(10)
  w_SEDE = space(2)
  w_PWD = space(254)
  proc Save(i_oFrom)
    this.w_DESERIAL = i_oFrom.w_DESERIAL
    this.w_DE__SEDE = i_oFrom.w_DE__SEDE
    this.w_DEENTITA = i_oFrom.w_DEENTITA
    this.w_DENUMINS = i_oFrom.w_DENUMINS
    this.w_DENUMUPD = i_oFrom.w_DENUMUPD
    this.w_DENUMDEL = i_oFrom.w_DENUMDEL
    this.w_DENUMRIF = i_oFrom.w_DENUMRIF
    this.w_DENUMOVW = i_oFrom.w_DENUMOVW
    this.w_DEZIPERR = i_oFrom.w_DEZIPERR
    this.w_DESENT = i_oFrom.w_DESENT
    this.w_DEZIPOLD = i_oFrom.w_DEZIPOLD
    this.w_TIPSED = i_oFrom.w_TIPSED
    this.w_SEDE = i_oFrom.w_SEDE
    this.w_PWD = i_oFrom.w_PWD
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_DESERIAL = this.w_DESERIAL
    i_oTo.w_DE__SEDE = this.w_DE__SEDE
    i_oTo.w_DEENTITA = this.w_DEENTITA
    i_oTo.w_DENUMINS = this.w_DENUMINS
    i_oTo.w_DENUMUPD = this.w_DENUMUPD
    i_oTo.w_DENUMDEL = this.w_DENUMDEL
    i_oTo.w_DENUMRIF = this.w_DENUMRIF
    i_oTo.w_DENUMOVW = this.w_DENUMOVW
    i_oTo.w_DEZIPERR = this.w_DEZIPERR
    i_oTo.w_DESENT = this.w_DESENT
    i_oTo.w_DEZIPOLD = this.w_DEZIPOLD
    i_oTo.w_TIPSED = this.w_TIPSED
    i_oTo.w_SEDE = this.w_SEDE
    i_oTo.w_PWD = this.w_PWD
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgslr_mde as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 580
  Height = 312
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-02-25"
  HelpContextID=80309353
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=14

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  ENT_LOG_IDX = 0
  ENT_MAST_IDX = 0
  PUBLMAST_IDX = 0
  cFile = "ENT_LOG"
  cKeySelect = "DESERIAL,DE__SEDE"
  cKeyWhere  = "DESERIAL=this.w_DESERIAL and DE__SEDE=this.w_DE__SEDE"
  cKeyDetail  = "DESERIAL=this.w_DESERIAL and DE__SEDE=this.w_DE__SEDE and DEENTITA=this.w_DEENTITA"
  cKeyWhereODBC = '"DESERIAL="+cp_ToStrODBC(this.w_DESERIAL)';
      +'+" and DE__SEDE="+cp_ToStrODBC(this.w_DE__SEDE)';

  cKeyDetailWhereODBC = '"DESERIAL="+cp_ToStrODBC(this.w_DESERIAL)';
      +'+" and DE__SEDE="+cp_ToStrODBC(this.w_DE__SEDE)';
      +'+" and DEENTITA="+cp_ToStrODBC(this.w_DEENTITA)';

  cKeyWhereODBCqualified = '"ENT_LOG.DESERIAL="+cp_ToStrODBC(this.w_DESERIAL)';
      +'+" and ENT_LOG.DE__SEDE="+cp_ToStrODBC(this.w_DE__SEDE)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cPrg = "gslr_mde"
  cComment = "Dettaglio entit� (log)"
  i_nRowNum = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_DESERIAL = space(10)
  w_DE__SEDE = space(2)
  w_DEENTITA = space(15)
  w_DENUMINS = 0
  w_DENUMUPD = 0
  w_DENUMDEL = 0
  w_DENUMRIF = 0
  w_DENUMOVW = 0
  w_DEZIPERR = space(254)
  w_DESENT = space(50)
  w_DEZIPOLD = space(254)
  w_TIPSED = space(10)
  w_SEDE = space(2)
  w_PWD = space(254)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgslr_mdePag1","gslr_mde",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Dettaglio entit�")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='ENT_MAST'
    this.cWorkTables[2]='PUBLMAST'
    this.cWorkTables[3]='ENT_LOG'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.ENT_LOG_IDX,5],7]
    this.nPostItConn=i_TableProp[this.ENT_LOG_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgslr_mde'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_1_joined
    link_2_1_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from ENT_LOG where DESERIAL=KeySet.DESERIAL
    *                            and DE__SEDE=KeySet.DE__SEDE
    *                            and DEENTITA=KeySet.DEENTITA
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.ENT_LOG_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ENT_LOG_IDX,2],this.bLoadRecFilter,this.ENT_LOG_IDX,"gslr_mde")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('ENT_LOG')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "ENT_LOG.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' ENT_LOG '
      link_2_1_joined=this.AddJoinedLink_2_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'DESERIAL',this.w_DESERIAL  ,'DE__SEDE',this.w_DE__SEDE  )
      select * from (i_cTable) ENT_LOG where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DESENT = space(50)
        .w_PWD = space(254)
        .w_DESERIAL = NVL(DESERIAL,space(10))
        .w_DE__SEDE = NVL(DE__SEDE,space(2))
        .w_TIPSED = "P"
        .w_SEDE = .w_DE__SEDE
          .link_1_7('Load')
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'ENT_LOG')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_DEENTITA = NVL(DEENTITA,space(15))
          if link_2_1_joined
            this.w_DEENTITA = NVL(ENCODICE201,NVL(this.w_DEENTITA,space(15)))
            this.w_DESENT = NVL(ENDESCRI201,space(50))
          else
          .link_2_1('Load')
          endif
          .w_DENUMINS = NVL(DENUMINS,0)
          .w_DENUMUPD = NVL(DENUMUPD,0)
          .w_DENUMDEL = NVL(DENUMDEL,0)
          .w_DENUMRIF = NVL(DENUMRIF,0)
          .w_DENUMOVW = NVL(DENUMOVW,0)
          .w_DEZIPERR = NVL(DEZIPERR,space(254))
          .w_DEZIPOLD = NVL(DEZIPOLD,space(254))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace DEENTITA with .w_DEENTITA
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .w_TIPSED = "P"
        .w_SEDE = .w_DE__SEDE
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page1.oPag.oBtn_2_10.enabled = .oPgFrm.Page1.oPag.oBtn_2_10.mCond()
        .oPgFrm.Page1.oPag.oBtn_2_11.enabled = .oPgFrm.Page1.oPag.oBtn_2_11.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_DESERIAL=space(10)
      .w_DE__SEDE=space(2)
      .w_DEENTITA=space(15)
      .w_DENUMINS=0
      .w_DENUMUPD=0
      .w_DENUMDEL=0
      .w_DENUMRIF=0
      .w_DENUMOVW=0
      .w_DEZIPERR=space(254)
      .w_DESENT=space(50)
      .w_DEZIPOLD=space(254)
      .w_TIPSED=space(10)
      .w_SEDE=space(2)
      .w_PWD=space(254)
      if .cFunction<>"Filter"
        .DoRTCalc(1,3,.f.)
        if not(empty(.w_DEENTITA))
         .link_2_1('Full')
        endif
        .DoRTCalc(4,11,.f.)
        .w_TIPSED = "P"
        .w_SEDE = .w_DE__SEDE
        .DoRTCalc(13,13,.f.)
        if not(empty(.w_SEDE))
         .link_1_7('Full')
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'ENT_LOG')
    this.DoRTCalc(14,14,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page1.oPag.oBtn_2_10.enabled = this.oPgFrm.Page1.oPag.oBtn_2_10.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_11.enabled = this.oPgFrm.Page1.oPag.oBtn_2_11.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oBtn_2_10.enabled = .Page1.oPag.oBtn_2_10.mCond()
      .Page1.oPag.oBtn_2_11.enabled = .Page1.oPag.oBtn_2_11.mCond()
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'ENT_LOG',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.ENT_LOG_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DESERIAL,"DESERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DE__SEDE,"DE__SEDE",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_DEENTITA C(15);
      ,t_DENUMINS N(6);
      ,t_DENUMUPD N(6);
      ,t_DENUMDEL N(6);
      ,t_DENUMRIF N(6);
      ,t_DENUMOVW N(6);
      ,t_DEZIPERR C(254);
      ,t_DEZIPOLD C(254);
      ,DEENTITA C(15);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgslr_mdebodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDEENTITA_2_1.controlsource=this.cTrsName+'.t_DEENTITA'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDENUMINS_2_2.controlsource=this.cTrsName+'.t_DENUMINS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDENUMUPD_2_3.controlsource=this.cTrsName+'.t_DENUMUPD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDENUMDEL_2_4.controlsource=this.cTrsName+'.t_DENUMDEL'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDENUMRIF_2_5.controlsource=this.cTrsName+'.t_DENUMRIF'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDENUMOVW_2_6.controlsource=this.cTrsName+'.t_DENUMOVW'
    this.oPgFRm.Page1.oPag.oDEZIPERR_2_7.controlsource=this.cTrsName+'.t_DEZIPERR'
    this.oPgFRm.Page1.oPag.oDEZIPOLD_2_9.controlsource=this.cTrsName+'.t_DEZIPOLD'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(170)
    this.AddVLine(242)
    this.AddVLine(316)
    this.AddVLine(390)
    this.AddVLine(464)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEENTITA_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.ENT_LOG_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ENT_LOG_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.ENT_LOG_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ENT_LOG_IDX,2])
      *
      * insert into ENT_LOG
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'ENT_LOG')
        i_extval=cp_InsertValODBCExtFlds(this,'ENT_LOG')
        i_cFldBody=" "+;
                  "(DESERIAL,DE__SEDE,DEENTITA,DENUMINS,DENUMUPD"+;
                  ",DENUMDEL,DENUMRIF,DENUMOVW,DEZIPERR,DEZIPOLD,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_DESERIAL)+","+cp_ToStrODBC(this.w_DE__SEDE)+","+cp_ToStrODBCNull(this.w_DEENTITA)+","+cp_ToStrODBC(this.w_DENUMINS)+","+cp_ToStrODBC(this.w_DENUMUPD)+;
             ","+cp_ToStrODBC(this.w_DENUMDEL)+","+cp_ToStrODBC(this.w_DENUMRIF)+","+cp_ToStrODBC(this.w_DENUMOVW)+","+cp_ToStrODBC(this.w_DEZIPERR)+","+cp_ToStrODBC(this.w_DEZIPOLD)+;
             ","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'ENT_LOG')
        i_extval=cp_InsertValVFPExtFlds(this,'ENT_LOG')
        cp_CheckDeletedKey(i_cTable,0,'DESERIAL',this.w_DESERIAL,'DE__SEDE',this.w_DE__SEDE,'DEENTITA',this.w_DEENTITA)
        INSERT INTO (i_cTable) (;
                   DESERIAL;
                  ,DE__SEDE;
                  ,DEENTITA;
                  ,DENUMINS;
                  ,DENUMUPD;
                  ,DENUMDEL;
                  ,DENUMRIF;
                  ,DENUMOVW;
                  ,DEZIPERR;
                  ,DEZIPOLD;
                  ,CPCCCHK &i_extfld.) VALUES (;
                  this.w_DESERIAL;
                  ,this.w_DE__SEDE;
                  ,this.w_DEENTITA;
                  ,this.w_DENUMINS;
                  ,this.w_DENUMUPD;
                  ,this.w_DENUMDEL;
                  ,this.w_DENUMRIF;
                  ,this.w_DENUMOVW;
                  ,this.w_DEZIPERR;
                  ,this.w_DEZIPOLD;
                  ,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.ENT_LOG_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ENT_LOG_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_DEENTITA))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'ENT_LOG')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and DEENTITA="+cp_ToStrODBC(&i_TN.->DEENTITA)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'ENT_LOG')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and DEENTITA=&i_TN.->DEENTITA;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_DEENTITA))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and DEENTITA="+cp_ToStrODBC(&i_TN.->DEENTITA)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and DEENTITA=&i_TN.->DEENTITA;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = 0
              replace DEENTITA with this.w_DEENTITA
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update ENT_LOG
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'ENT_LOG')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " DENUMINS="+cp_ToStrODBC(this.w_DENUMINS)+;
                     ",DENUMUPD="+cp_ToStrODBC(this.w_DENUMUPD)+;
                     ",DENUMDEL="+cp_ToStrODBC(this.w_DENUMDEL)+;
                     ",DENUMRIF="+cp_ToStrODBC(this.w_DENUMRIF)+;
                     ",DENUMOVW="+cp_ToStrODBC(this.w_DENUMOVW)+;
                     ",DEZIPERR="+cp_ToStrODBC(this.w_DEZIPERR)+;
                     ",DEZIPOLD="+cp_ToStrODBC(this.w_DEZIPOLD)+;
                     ",DEENTITA="+cp_ToStrODBC(this.w_DEENTITA)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+;
                             " and DEENTITA="+cp_ToStrODBC(DEENTITA)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'ENT_LOG')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      DENUMINS=this.w_DENUMINS;
                     ,DENUMUPD=this.w_DENUMUPD;
                     ,DENUMDEL=this.w_DENUMDEL;
                     ,DENUMRIF=this.w_DENUMRIF;
                     ,DENUMOVW=this.w_DENUMOVW;
                     ,DEZIPERR=this.w_DEZIPERR;
                     ,DEZIPOLD=this.w_DEZIPOLD;
                     ,DEENTITA=this.w_DEENTITA;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                                      and DEENTITA=&i_TN.->DEENTITA;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.ENT_LOG_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ENT_LOG_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_DEENTITA))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete ENT_LOG
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and DEENTITA="+cp_ToStrODBC(&i_TN.->DEENTITA)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and DEENTITA=&i_TN.->DEENTITA;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_DEENTITA))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.ENT_LOG_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ENT_LOG_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,11,.t.)
          .w_TIPSED = "P"
          .w_SEDE = .w_DE__SEDE
          .link_1_7('Full')
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(14,14,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBtn_2_10.enabled =this.oPgFrm.Page1.oPag.oBtn_2_10.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_11.enabled =this.oPgFrm.Page1.oPag.oBtn_2_11.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=DEENTITA
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ENT_MAST_IDX,3]
    i_lTable = "ENT_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ENT_MAST_IDX,2], .t., this.ENT_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ENT_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DEENTITA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ENT_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ENCODICE like "+cp_ToStrODBC(trim(this.w_DEENTITA)+"%");

          i_ret=cp_SQL(i_nConn,"select ENCODICE,ENDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ENCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ENCODICE',trim(this.w_DEENTITA))
          select ENCODICE,ENDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ENCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DEENTITA)==trim(_Link_.ENCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DEENTITA) and !this.bDontReportError
            deferred_cp_zoom('ENT_MAST','*','ENCODICE',cp_AbsName(oSource.parent,'oDEENTITA_2_1'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ENCODICE,ENDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ENCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ENCODICE',oSource.xKey(1))
            select ENCODICE,ENDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DEENTITA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ENCODICE,ENDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ENCODICE="+cp_ToStrODBC(this.w_DEENTITA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ENCODICE',this.w_DEENTITA)
            select ENCODICE,ENDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DEENTITA = NVL(_Link_.ENCODICE,space(15))
      this.w_DESENT = NVL(_Link_.ENDESCRI,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_DEENTITA = space(15)
      endif
      this.w_DESENT = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ENT_MAST_IDX,2])+'\'+cp_ToStr(_Link_.ENCODICE,1)
      cp_ShowWarn(i_cKey,this.ENT_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DEENTITA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ENT_MAST_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ENT_MAST_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_1.ENCODICE as ENCODICE201"+ ",link_2_1.ENDESCRI as ENDESCRI201"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_1 on ENT_LOG.DEENTITA=link_2_1.ENCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_1"
          i_cKey=i_cKey+'+" and ENT_LOG.DEENTITA=link_2_1.ENCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=SEDE
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PUBLMAST_IDX,3]
    i_lTable = "PUBLMAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PUBLMAST_IDX,2], .t., this.PUBLMAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PUBLMAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SEDE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SEDE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SE__TIPO,SECODICE,SEPWDZIP";
                   +" from "+i_cTable+" "+i_lTable+" where SECODICE="+cp_ToStrODBC(this.w_SEDE);
                   +" and SE__TIPO="+cp_ToStrODBC(this.w_TIPSED);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SE__TIPO',this.w_TIPSED;
                       ,'SECODICE',this.w_SEDE)
            select SE__TIPO,SECODICE,SEPWDZIP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SEDE = NVL(_Link_.SECODICE,space(2))
      this.w_PWD = NVL(_Link_.SEPWDZIP,space(254))
    else
      if i_cCtrl<>'Load'
        this.w_SEDE = space(2)
      endif
      this.w_PWD = space(254)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PUBLMAST_IDX,2])+'\'+cp_ToStr(_Link_.SE__TIPO,1)+'\'+cp_ToStr(_Link_.SECODICE,1)
      cp_ShowWarn(i_cKey,this.PUBLMAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SEDE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oDEZIPERR_2_7.value==this.w_DEZIPERR)
      this.oPgFrm.Page1.oPag.oDEZIPERR_2_7.value=this.w_DEZIPERR
      replace t_DEZIPERR with this.oPgFrm.Page1.oPag.oDEZIPERR_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDESENT_2_8.value==this.w_DESENT)
      this.oPgFrm.Page1.oPag.oDESENT_2_8.value=this.w_DESENT
    endif
    if not(this.oPgFrm.Page1.oPag.oDEZIPOLD_2_9.value==this.w_DEZIPOLD)
      this.oPgFrm.Page1.oPag.oDEZIPOLD_2_9.value=this.w_DEZIPOLD
      replace t_DEZIPOLD with this.oPgFrm.Page1.oPag.oDEZIPOLD_2_9.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEENTITA_2_1.value==this.w_DEENTITA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEENTITA_2_1.value=this.w_DEENTITA
      replace t_DEENTITA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEENTITA_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDENUMINS_2_2.value==this.w_DENUMINS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDENUMINS_2_2.value=this.w_DENUMINS
      replace t_DENUMINS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDENUMINS_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDENUMUPD_2_3.value==this.w_DENUMUPD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDENUMUPD_2_3.value=this.w_DENUMUPD
      replace t_DENUMUPD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDENUMUPD_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDENUMDEL_2_4.value==this.w_DENUMDEL)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDENUMDEL_2_4.value=this.w_DENUMDEL
      replace t_DENUMDEL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDENUMDEL_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDENUMRIF_2_5.value==this.w_DENUMRIF)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDENUMRIF_2_5.value=this.w_DENUMRIF
      replace t_DENUMRIF with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDENUMRIF_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDENUMOVW_2_6.value==this.w_DENUMOVW)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDENUMOVW_2_6.value=this.w_DENUMOVW
      replace t_DENUMOVW with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDENUMOVW_2_6.value
    endif
    cp_SetControlsValueExtFlds(this,'ENT_LOG')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if not(Empty(.w_DEENTITA))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_DEENTITA)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_DEENTITA=space(15)
      .w_DENUMINS=0
      .w_DENUMUPD=0
      .w_DENUMDEL=0
      .w_DENUMRIF=0
      .w_DENUMOVW=0
      .w_DEZIPERR=space(254)
      .w_DEZIPOLD=space(254)
      .DoRTCalc(1,3,.f.)
      if not(empty(.w_DEENTITA))
        .link_2_1('Full')
      endif
    endwith
    this.DoRTCalc(4,14,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_DEENTITA = t_DEENTITA
    this.w_DENUMINS = t_DENUMINS
    this.w_DENUMUPD = t_DENUMUPD
    this.w_DENUMDEL = t_DENUMDEL
    this.w_DENUMRIF = t_DENUMRIF
    this.w_DENUMOVW = t_DENUMOVW
    this.w_DEZIPERR = t_DEZIPERR
    this.w_DEZIPOLD = t_DEZIPOLD
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_DEENTITA with this.w_DEENTITA
    replace t_DENUMINS with this.w_DENUMINS
    replace t_DENUMUPD with this.w_DENUMUPD
    replace t_DENUMDEL with this.w_DENUMDEL
    replace t_DENUMRIF with this.w_DENUMRIF
    replace t_DENUMOVW with this.w_DENUMOVW
    replace t_DEZIPERR with this.w_DEZIPERR
    replace t_DEZIPOLD with this.w_DEZIPOLD
    if i_srv='A'
      replace DEENTITA with this.w_DEENTITA
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgslr_mdePag1 as StdContainer
  Width  = 576
  height = 312
  stdWidth  = 576
  stdheight = 312
  resizeXpos=166
  resizeYpos=188
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=25, top=5, width=521,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=6,Field1="DEENTITA",Label1="Entit�",Field2="DENUMINS",Label2="Inseriti",Field3="DENUMUPD",Label3="Modificati",Field4="DENUMDEL",Label4="Cancellati",Field5="DENUMRIF",Label5="Rifiutati",Field6="DENUMOVW",Label6="Sovrascritti",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 235781498

  add object oStr_1_3 as StdString with uid="UFRNRBNDKJ",Visible=.t., Left=7, Top=254,;
    Alignment=1, Width=128, Height=18,;
    Caption="Zip dati errati:"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="JOPHHZHSCF",Visible=.t., Left=7, Top=221,;
    Alignment=1, Width=128, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="HODZFHDZQH",Visible=.t., Left=7, Top=285,;
    Alignment=1, Width=128, Height=18,;
    Caption="Zip dati sovrascritti:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=15,top=24,;
    width=517+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=16,top=25,width=516+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='ENT_MAST|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oDEZIPERR_2_7.Refresh()
      this.Parent.oDEZIPOLD_2_9.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='ENT_MAST'
        oDropInto=this.oBodyCol.oRow.oDEENTITA_2_1
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oDEZIPERR_2_7 as StdTrsField with uid="DWDZBYCFAN",rtseq=9,rtrep=.t.,;
    cFormVar="w_DEZIPERR",value=space(254),enabled=.f.,;
    ToolTipText = "Path dello zip generato dal log contenente i DBF di salvataggio dati errati",;
    HelpContextID = 92634504,;
    cTotal="", bFixedPos=.t., cQueryName = "DEZIPERR",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=410, Left=138, Top=253, InputMask=replicate('X',254)

  add object oDESENT_2_8 as StdField with uid="OCMXWZMNDW",rtseq=10,rtrep=.f.,;
    cFormVar="w_DESENT",value=space(50),enabled=.f.,;
    ToolTipText = "Descrizione entit�",;
    HelpContextID = 73469238,;
    cTotal="", bFixedPos=.t., cQueryName = "DESENT",;
    bObbl = .f. , nPag = 2, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=410, Left=138, Top=221, InputMask=replicate('X',50)

  add object oDEZIPOLD_2_9 as StdTrsField with uid="TKYWFAGANF",rtseq=11,rtrep=.t.,;
    cFormVar="w_DEZIPOLD",value=space(254),enabled=.f.,;
    ToolTipText = "Path dello zip generato dal log contenente i DBF di salvataggio dati sovrascritti",;
    HelpContextID = 8028806,;
    cTotal="", bFixedPos=.t., cQueryName = "DEZIPOLD",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=410, Left=138, Top=285, InputMask=replicate('X',254)

  add object oBtn_2_10 as StdButton with uid="WZGRGMINYR",width=19,height=18,;
   left=550, top=255,;
    caption="...", nPag=2;
    , ToolTipText = "Premere per aprire il pacchetto memorizzato nello zip";
    , HelpContextID = 80108330;
  , bGlobalFont=.t.

    proc oBtn_2_10.Click()
      with this.Parent.oContained
        GSLR_BAP(this.Parent.oContained,.w_DEZIPERR, Alltrim(.w_PWD))
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_10.mCond()
    with this.Parent.oContained
      return (Not Empty(.w_DEZIPERR))
    endwith
  endfunc

  add object oBtn_2_11 as StdButton with uid="BADGAVOWMV",width=19,height=18,;
   left=550, top=287,;
    caption="...", nPag=2;
    , ToolTipText = "Premere per aprire il pacchetto memorizzato nello zip";
    , HelpContextID = 80108330;
  , bGlobalFont=.t.

    proc oBtn_2_11.Click()
      with this.Parent.oContained
        GSLR_BAP(this.Parent.oContained,.w_DEZIPOLD, Alltrim(.w_PWD))
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_11.mCond()
    with this.Parent.oContained
      return (Not Empty(.w_DEZIPOLD))
    endwith
  endfunc

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgslr_mdeBodyRow as CPBodyRowCnt
  Width=507
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oDEENTITA_2_1 as StdTrsField with uid="VRYCMJRNAX",rtseq=3,rtrep=.t.,;
    cFormVar="w_DEENTITA",value=space(15),isprimarykey=.t.,;
    HelpContextID = 164179319,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=137, Left=-2, Top=0, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ENT_MAST", oKey_1_1="ENCODICE", oKey_1_2="this.w_DEENTITA"

  func oDEENTITA_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oDEENTITA_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oDEENTITA_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    if not(this.parent.oDEENTITA_2_1.readonly and this.parent.oDEENTITA_2_1.isprimarykey)
    do cp_zoom with 'ENT_MAST','*','ENCODICE',cp_AbsName(this.parent,'oDEENTITA_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
   endif
  endproc

  add object oDENUMINS_2_2 as StdTrsField with uid="BHOUIILLPU",rtseq=4,rtrep=.t.,;
    cFormVar="w_DENUMINS",value=0,;
    HelpContextID = 111100535,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=55, Left=151, Top=0, cSayPict=["999999"], cGetPict=["999999"]

  add object oDENUMUPD_2_3 as StdTrsField with uid="YQIAASRBJK",rtseq=5,rtrep=.t.,;
    cFormVar="w_DENUMUPD",value=0,;
    HelpContextID = 90226042,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=55, Left=225, Top=0, cSayPict=["999999"], cGetPict=["999999"]

  add object oDENUMDEL_2_4 as StdTrsField with uid="EMCOKKFGRK",rtseq=6,rtrep=.t.,;
    cFormVar="w_DENUMDEL",value=0,;
    HelpContextID = 73448834,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=55, Left=299, Top=0, cSayPict=["999999"], cGetPict=["999999"]

  add object oDENUMRIF_2_5 as StdTrsField with uid="XBUDWPQPAS",rtseq=7,rtrep=.t.,;
    cFormVar="w_DENUMRIF",value=0,;
    HelpContextID = 228541060,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=55, Left=373, Top=0, cSayPict=["999999"], cGetPict=["999999"]

  add object oDENUMOVW_2_6 as StdTrsField with uid="UJNQYETIMX",rtseq=8,rtrep=.t.,;
    cFormVar="w_DENUMOVW",value=0,;
    HelpContextID = 257998221,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=55, Left=447, Top=0, cSayPict=["999999"], cGetPict=["999999"]
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oDEENTITA_2_1.When()
    return(.t.)
  proc oDEENTITA_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oDEENTITA_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gslr_mde','ENT_LOG','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".DESERIAL=ENT_LOG.DESERIAL";
  +" and "+i_cAliasName2+".DE__SEDE=ENT_LOG.DE__SEDE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
