* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslr_sse                                                        *
*              Stampa sedi                                                     *
*                                                                              *
*      Author: Zucchetti SPA                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161][VRS_12]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-11-21                                                      *
* Last revis.: 2007-07-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgslr_sse",oParentObject))

* --- Class definition
define class tgslr_sse as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 529
  Height = 153
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-07-18"
  HelpContextID=90795113
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=3

  * --- Constant Properties
  _IDX = 0
  PUBLMAST_IDX = 0
  cPrg = "gslr_sse"
  cComment = "Stampa sedi"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_SECODICE = space(2)
  w_SE__TIPO = space(1)
  w_SEDESCRI = space(50)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgslr_ssePag1","gslr_sse",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSECODICE_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='PUBLMAST'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_SECODICE=space(2)
      .w_SE__TIPO=space(1)
      .w_SEDESCRI=space(50)
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_SECODICE))
          .link_1_2('Full')
        endif
        .w_SE__TIPO = 'P'
      .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
    endwith
    this.DoRTCalc(3,3,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,3,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_6.enabled = this.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_5.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=SECODICE
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PUBLMAST_IDX,3]
    i_lTable = "PUBLMAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PUBLMAST_IDX,2], .t., this.PUBLMAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PUBLMAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SECODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'PUBLMAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SECODICE like "+cp_ToStrODBC(trim(this.w_SECODICE)+"%");
                   +" and SE__TIPO="+cp_ToStrODBC(this.w_SE__TIPO);

          i_ret=cp_SQL(i_nConn,"select SE__TIPO,SECODICE,SEDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SE__TIPO,SECODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SE__TIPO',this.w_SE__TIPO;
                     ,'SECODICE',trim(this.w_SECODICE))
          select SE__TIPO,SECODICE,SEDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SE__TIPO,SECODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SECODICE)==trim(_Link_.SECODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SECODICE) and !this.bDontReportError
            deferred_cp_zoom('PUBLMAST','*','SE__TIPO,SECODICE',cp_AbsName(oSource.parent,'oSECODICE_1_2'),i_cWhere,'',"Sedi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_SE__TIPO<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SE__TIPO,SECODICE,SEDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select SE__TIPO,SECODICE,SEDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SE__TIPO,SECODICE,SEDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where SECODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and SE__TIPO="+cp_ToStrODBC(this.w_SE__TIPO);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SE__TIPO',oSource.xKey(1);
                       ,'SECODICE',oSource.xKey(2))
            select SE__TIPO,SECODICE,SEDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SECODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SE__TIPO,SECODICE,SEDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where SECODICE="+cp_ToStrODBC(this.w_SECODICE);
                   +" and SE__TIPO="+cp_ToStrODBC(this.w_SE__TIPO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SE__TIPO',this.w_SE__TIPO;
                       ,'SECODICE',this.w_SECODICE)
            select SE__TIPO,SECODICE,SEDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SECODICE = NVL(_Link_.SECODICE,space(2))
      this.w_SEDESCRI = NVL(_Link_.SEDESCRI,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_SECODICE = space(2)
      endif
      this.w_SEDESCRI = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PUBLMAST_IDX,2])+'\'+cp_ToStr(_Link_.SE__TIPO,1)+'\'+cp_ToStr(_Link_.SECODICE,1)
      cp_ShowWarn(i_cKey,this.PUBLMAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SECODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSECODICE_1_2.value==this.w_SECODICE)
      this.oPgFrm.Page1.oPag.oSECODICE_1_2.value=this.w_SECODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oSE__TIPO_1_4.RadioValue()==this.w_SE__TIPO)
      this.oPgFrm.Page1.oPag.oSE__TIPO_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSEDESCRI_1_9.value==this.w_SEDESCRI)
      this.oPgFrm.Page1.oPag.oSEDESCRI_1_9.value=this.w_SEDESCRI
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgslr_ssePag1 as StdContainer
  Width  = 525
  height = 153
  stdWidth  = 525
  stdheight = 153
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSECODICE_1_2 as StdField with uid="MQPFKCNFRO",rtseq=1,rtrep=.f.,;
    cFormVar = "w_SECODICE", cQueryName = "SECODICE",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    HelpContextID = 136973931,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=117, Top=6, InputMask=replicate('X',2), bHasZoom = .t. , cLinkFile="PUBLMAST", oKey_1_1="SE__TIPO", oKey_1_2="this.w_SE__TIPO", oKey_2_1="SECODICE", oKey_2_2="this.w_SECODICE"

  func oSECODICE_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oSECODICE_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSECODICE_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.PUBLMAST_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"SE__TIPO="+cp_ToStrODBC(this.Parent.oContained.w_SE__TIPO)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"SE__TIPO="+cp_ToStr(this.Parent.oContained.w_SE__TIPO)
    endif
    do cp_zoom with 'PUBLMAST','*','SE__TIPO,SECODICE',cp_AbsName(this.parent,'oSECODICE_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Sedi",'',this.parent.oContained
  endproc


  add object oSE__TIPO_1_4 as StdCombo with uid="YHGWGOZTZV",rtseq=2,rtrep=.f.,left=117,top=37,width=188,height=21;
    , HelpContextID = 154914421;
    , cFormVar="w_SE__TIPO",RowSource=""+"Sede da cui ricevo,"+"Sede verso cui pubblico", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSE__TIPO_1_4.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'R',;
    space(1))))
  endfunc
  func oSE__TIPO_1_4.GetRadio()
    this.Parent.oContained.w_SE__TIPO = this.RadioValue()
    return .t.
  endfunc

  func oSE__TIPO_1_4.SetRadio()
    this.Parent.oContained.w_SE__TIPO=trim(this.Parent.oContained.w_SE__TIPO)
    this.value = ;
      iif(this.Parent.oContained.w_SE__TIPO=='P',1,;
      iif(this.Parent.oContained.w_SE__TIPO=='R',2,;
      0))
  endfunc

  func oSE__TIPO_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_SECODICE)
        bRes2=.link_1_2('Full')
      endif
    endwith
    return bRes
  endfunc


  add object oObj_1_5 as cp_outputCombo with uid="ZLERXIVPWT",left=116, top=75, width=382,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 87202534


  add object oBtn_1_6 as StdButton with uid="CCGSUHUDGX",left=419, top=103, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , HelpContextID = 50994470;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_6.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_6.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oBtn_1_7 as StdButton with uid="YXYHLPQOEL",left=469, top=103, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 83477690;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_7.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oSEDESCRI_1_9 as StdField with uid="SHOKQHXTJZ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_SEDESCRI", cQueryName = "SEDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 51388015,;
   bGlobalFont=.t.,;
    Height=21, Width=349, Left=169, Top=6, InputMask=replicate('X',50)

  add object oStr_1_1 as StdString with uid="DHELKCNPMQ",Visible=.t., Left=69, Top=6,;
    Alignment=1, Width=44, Height=18,;
    Caption="Sede:"  ;
  , bGlobalFont=.t.

  add object oStr_1_3 as StdString with uid="HEJILYPIBN",Visible=.t., Left=82, Top=37,;
    Alignment=1, Width=31, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="KURZTBJWHQ",Visible=.t., Left=6, Top=75,;
    Alignment=1, Width=105, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gslr_sse','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
