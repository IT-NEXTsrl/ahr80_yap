* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_bp1                                                        *
*              Calcolo percentuali ammortamen                                  *
*                                                                              *
*      Author: Zucchetti spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][207]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-11-20                                                      *
* Last revis.: 2008-06-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsce_bp1",oParentObject)
return(i_retval)

define class tgsce_bp1 as StdBatch
  * --- Local variables
  w_FINESE = ctod("  /  /  ")
  w_MAXANT = 0
  w_PROESE = 0
  w_Ammabu = 0
  w_CODAZI = space(5)
  w_PACODCAT = space(15)
  w_COEFANT = 0
  w_PERRID = 0
  w_GIORNI = 0
  * --- WorkFile variables
  PAR_CESP_idx=0
  ESERCIZI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch utilizzato per verificare se il cespite in questione ha superato il numero massimo di ammortamenti anticipati.
    * --- Lanciato da Gsce_Bpa
    this.w_COEFANT = this.oparentobject.Oparentobject.w_Pacoefi2
    this.w_CODAZI = I_Codazi
    this.w_FINESE = this.Oparentobject.Oparentobject.w_ESEFIN
    this.w_PACODCAT = this.Oparentobject.Oparentobject.w_Pacodcat
    * --- Leggo  il campo contenente il Numero Ammortamenti Anticipati per Beni Usati ed il numero massimo di ammortamenti anticipati.
    * --- Read from PAR_CESP
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_CESP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_CESP_idx,2],.t.,this.PAR_CESP_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PCAMMABU,PCMAXANT,PCPERESE"+;
        " from "+i_cTable+" PAR_CESP where ";
            +"PCCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PCAMMABU,PCMAXANT,PCPERESE;
        from (i_cTable) where;
            PCCODAZI = this.w_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_AMMABU = NVL(cp_ToDate(_read_.PCAMMABU),cp_NullValue(_read_.PCAMMABU))
      this.w_MAXANT = NVL(cp_ToDate(_read_.PCMAXANT),cp_NullValue(_read_.PCMAXANT))
      this.w_PERRID = NVL(cp_ToDate(_read_.PCPERESE),cp_NullValue(_read_.PCPERESE))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Effettuo una lettura sulla tabella ESERCIZI  per contare il numero 
    *     di esercizio che intercorrono tra la data di entrata in funzione del bene 
    *     e la data nella quale si effettua l'ammortanto.
    this.w_PROESE = 0
    this.w_GIORNI = 0
    if g_APPLICATION="ADHOC REVOLUTION"
      * --- Select from ESERCIZI
      i_nConn=i_TableProp[this.ESERCIZI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2],.t.,this.ESERCIZI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select ESINIESE,ESFINESE, COUNT(*) AS CONTA  from "+i_cTable+" ESERCIZI ";
            +" where ESCODAZI="+cp_ToStrODBC(this.w_CODAZI)+" AND ESFINESE>="+cp_ToStrODBC(this.oParentObject.w_DTPRIU)+" AND ESFINESE<="+cp_ToStrODBC(this.w_FINESE)+"";
            +" group by ESINIESE, ESFINESE";
            +" order by ESINIESE";
             ,"_Curs_ESERCIZI")
      else
        select ESINIESE,ESFINESE, COUNT(*) AS CONTA from (i_cTable);
         where ESCODAZI=this.w_CODAZI AND ESFINESE>=this.oParentObject.w_DTPRIU AND ESFINESE<=this.w_FINESE;
         group by ESINIESE, ESFINESE;
         order by ESINIESE;
          into cursor _Curs_ESERCIZI
      endif
      if used('_Curs_ESERCIZI')
        select _Curs_ESERCIZI
        locate for 1=1
        do while not(eof())
        this.w_GIORNI = this.w_GIORNI+(CP_TODATE(_Curs_ESERCIZI.ESFINESE)-CP_TODATE(_Curs_ESERCIZI.ESINIESE)+1)
        this.w_PROESE = this.w_PROESE + CONTA
        * --- Calcolo il numero di giorni dalla data di inizio esercizio alla data di fine
        *     esercizio successivo.
          select _Curs_ESERCIZI
          continue
        enddo
        use
      endif
      * --- Controllo finale
      if this.w_PROESE=0 and this.w_GIORNI<>0
        this.w_PROESE = this.w_PROESE+1
      endif
    else
      * --- Select from ESERCIZI
      i_nConn=i_TableProp[this.ESERCIZI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2],.t.,this.ESERCIZI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select ESINIESE,ESFINESE  from "+i_cTable+" ESERCIZI ";
            +" where ESCODAZI="+cp_ToStrODBC(this.w_CODAZI)+" AND ESFINESE>="+cp_ToStrODBC(this.oParentObject.w_DTPRIU)+" AND ESFINESE<="+cp_ToStrODBC(this.w_FINESE)+"";
            +" order by ESINIESE";
             ,"_Curs_ESERCIZI")
      else
        select ESINIESE,ESFINESE from (i_cTable);
         where ESCODAZI=this.w_CODAZI AND ESFINESE>=this.oParentObject.w_DTPRIU AND ESFINESE<=this.w_FINESE;
         order by ESINIESE;
          into cursor _Curs_ESERCIZI
      endif
      if used('_Curs_ESERCIZI')
        select _Curs_ESERCIZI
        locate for 1=1
        do while not(eof())
        this.w_GIORNI = this.w_GIORNI+(CP_TODATE(_Curs_ESERCIZI.ESFINESE)-CP_TODATE(_Curs_ESERCIZI.ESINIESE)+1)
        * --- Calcolo il numero di giorni dalla data di inizio esercizio alla data di fine esercizio.
        if this.w_GIORNI=365 or this.w_GIORNI=366
          this.w_PROESE = this.w_PROESE+1
          this.w_GIORNI = 0
        else
          if this.w_GIORNI>366
            this.w_PROESE = this.w_PROESE+2
            this.w_GIORNI = 0
          endif
        endif
          select _Curs_ESERCIZI
          continue
        enddo
        use
      endif
      * --- Controllo finale
      if this.w_GIORNI<365 and this.w_GIORNI<>0
        this.w_PROESE = this.w_PROESE+1
      endif
    endif
    * --- Controllo se ho superato il massimo di ammortamenti anticipati.
    if this.w_PROESE > this.w_MAXANT
      this.oParentObject.w_COEFI2 = 0
    endif
    * --- Verifico se ho settato nel cespite il flag cespite usato e se ho superato il
    * --- il numero di ammortamenti anticipati per beni usati.
    if this.oParentObject.w_Flceus="S" and this.w_Proese>this.w_Ammabu
      this.oParentObject.w_COEFI2 = 0
    endif
    if this.w_Proese=1 and this.oParentObject.w_FLRIDU="S" AND this.w_PERRID<>0
      this.oParentObject.w_COEFI2 = cp_Round(Nvl(this.w_Coefant,0)*(this.w_PERRID/100),2)
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='PAR_CESP'
    this.cWorkTables[2]='ESERCIZI'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_ESERCIZI')
      use in _Curs_ESERCIZI
    endif
    if used('_Curs_ESERCIZI')
      use in _Curs_ESERCIZI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
