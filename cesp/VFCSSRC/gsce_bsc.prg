* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_bsc                                                        *
*              Saldi cespiti                                                   *
*                                                                              *
*      Author: TAM Software srl                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_14]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-02-26                                                      *
* Last revis.: 2008-02-11                                                      *
*                                                                              *
* Calcola i Movimenti Progressivi dei Saldi Cespiti                            *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsce_bsc",oParentObject)
return(i_retval)

define class tgsce_bsc as StdBatch
  * --- Local variables
  * --- WorkFile variables
  SAL_CESE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcola i movimenti progressivi degli esercizi precedenti (da GSCE_ASC)
    * --- Variabili lette
    * --- Variabili scritte
    * --- Se valuta saldo Euro leggo anche il riepilogo dei saldi in lire trasformati in euro..
    if this.oParentObject.w_CODVAL=g_PERVAL AND (i_DATSYS >(GETVALUT(g_CODLIR, "VADATEUR") ) AND NOT EMPTY(GETVALUT(g_CODLIR, "VADATEUR") ))
      * --- Read from SAL_CESE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.SAL_CESE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SAL_CESE_idx,2],.t.,this.SAL_CESE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "SCINCVAL,SCDECVAL,SCIMPRIV,SCIMPNOA,SCIMPONS,SCIMPSVA,SCQUOPER,SCACCCIV,SCUTICIV,SCACCFIS,SCUTIFIS,SCUTIANT,SCACCANT,SCARRAMM,SCARRFON,SCARRFOC,SCARRAMC,SCIMPRIC,SCIMPSVC,SCDECVAC,SCIMPONC,SCINCVAC"+;
          " from "+i_cTable+" SAL_CESE where ";
              +"SCCODCES = "+cp_ToStrODBC(this.oParentObject.w_SCCODCES );
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          SCINCVAL,SCDECVAL,SCIMPRIV,SCIMPNOA,SCIMPONS,SCIMPSVA,SCQUOPER,SCACCCIV,SCUTICIV,SCACCFIS,SCUTIFIS,SCUTIANT,SCACCANT,SCARRAMM,SCARRFON,SCARRFOC,SCARRAMC,SCIMPRIC,SCIMPSVC,SCDECVAC,SCIMPONC,SCINCVAC;
          from (i_cTable) where;
              SCCODCES = this.oParentObject.w_SCCODCES ;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.oParentObject.w_TINCVAL = NVL(cp_ToDate(_read_.SCINCVAL),cp_NullValue(_read_.SCINCVAL))
        this.oParentObject.w_TDECVAL = NVL(cp_ToDate(_read_.SCDECVAL),cp_NullValue(_read_.SCDECVAL))
        this.oParentObject.w_TIMPRIV = NVL(cp_ToDate(_read_.SCIMPRIV),cp_NullValue(_read_.SCIMPRIV))
        this.oParentObject.w_TIMPNOA = NVL(cp_ToDate(_read_.SCIMPNOA),cp_NullValue(_read_.SCIMPNOA))
        this.oParentObject.w_TIMPONS = NVL(cp_ToDate(_read_.SCIMPONS),cp_NullValue(_read_.SCIMPONS))
        this.oParentObject.w_TIMPSVA = NVL(cp_ToDate(_read_.SCIMPSVA),cp_NullValue(_read_.SCIMPSVA))
        this.oParentObject.w_TQUOPER = NVL(cp_ToDate(_read_.SCQUOPER),cp_NullValue(_read_.SCQUOPER))
        this.oParentObject.w_TACCCIV = NVL(cp_ToDate(_read_.SCACCCIV),cp_NullValue(_read_.SCACCCIV))
        this.oParentObject.w_TUTICIV = NVL(cp_ToDate(_read_.SCUTICIV),cp_NullValue(_read_.SCUTICIV))
        this.oParentObject.w_TACCFIS = NVL(cp_ToDate(_read_.SCACCFIS),cp_NullValue(_read_.SCACCFIS))
        this.oParentObject.w_TUTIFIS = NVL(cp_ToDate(_read_.SCUTIFIS),cp_NullValue(_read_.SCUTIFIS))
        this.oParentObject.w_TUTIANT = NVL(cp_ToDate(_read_.SCUTIANT),cp_NullValue(_read_.SCUTIANT))
        this.oParentObject.w_TACCANT = NVL(cp_ToDate(_read_.SCACCANT),cp_NullValue(_read_.SCACCANT))
        this.oParentObject.w_SCARRAMM = NVL(cp_ToDate(_read_.SCARRAMM),cp_NullValue(_read_.SCARRAMM))
        this.oParentObject.w_SCARRFON = NVL(cp_ToDate(_read_.SCARRFON),cp_NullValue(_read_.SCARRFON))
        this.oParentObject.w_SCARRFOC = NVL(cp_ToDate(_read_.SCARRFOC),cp_NullValue(_read_.SCARRFOC))
        this.oParentObject.w_SCARRAMC = NVL(cp_ToDate(_read_.SCARRAMC),cp_NullValue(_read_.SCARRAMC))
        this.oParentObject.w_TIMPRIC = NVL(cp_ToDate(_read_.SCIMPRIC),cp_NullValue(_read_.SCIMPRIC))
        this.oParentObject.w_TIMPSVC = NVL(cp_ToDate(_read_.SCIMPSVC),cp_NullValue(_read_.SCIMPSVC))
        this.oParentObject.w_TDECVAC = NVL(cp_ToDate(_read_.SCDECVAC),cp_NullValue(_read_.SCDECVAC))
        this.oParentObject.w_TIMPONC = NVL(cp_ToDate(_read_.SCIMPONC),cp_NullValue(_read_.SCIMPONC))
        this.oParentObject.w_TINCVAC = NVL(cp_ToDate(_read_.SCINCVAC),cp_NullValue(_read_.SCINCVAC))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    this.oParentObject.w_TQTARESI = 0
    * --- Seleziona i record in base al Cespite (=), Esercizio (<) e Azienda (=).
    *     Filtro anche sulla valuta...
    * --- Select from GSCE_TSC
    do vq_exec with 'GSCE_TSC',this,'_Curs_GSCE_TSC','',.f.,.t.
    if used('_Curs_GSCE_TSC')
      select _Curs_GSCE_TSC
      locate for 1=1
      do while not(eof())
      this.oParentObject.w_TINCVAL = this.oParentObject.w_TINCVAL + NVL(_Curs_GSCE_TSC.SCINCVAL,0)
      this.oParentObject.w_TDECVAL = this.oParentObject.w_TDECVAL + NVL(_Curs_GSCE_TSC.SCDECVAL,0)
      this.oParentObject.w_TIMPRIV = this.oParentObject.w_TIMPRIV + NVL(_Curs_GSCE_TSC.SCIMPRIV,0)
      this.oParentObject.w_TIMPNOA = this.oParentObject.w_TIMPNOA + NVL(_Curs_GSCE_TSC.SCIMPNOA,0)
      this.oParentObject.w_TIMPONS = this.oParentObject.w_TIMPONS + NVL(_Curs_GSCE_TSC.SCIMPONS,0)
      this.oParentObject.w_TIMPSVA = this.oParentObject.w_TIMPSVA + NVL(_Curs_GSCE_TSC.SCIMPSVA,0)
      this.oParentObject.w_TQUOPER = this.oParentObject.w_TQUOPER + NVL(_Curs_GSCE_TSC.SCQUOPER,0)
      this.oParentObject.w_TACCCIV = this.oParentObject.w_TACCCIV + NVL(_Curs_GSCE_TSC.SCACCCIV,0)
      this.oParentObject.w_TUTICIV = this.oParentObject.w_TUTICIV + NVL(_Curs_GSCE_TSC.SCUTICIV,0)
      this.oParentObject.w_TACCFIS = this.oParentObject.w_TACCFIS + NVL(_Curs_GSCE_TSC.SCACCFIS,0)
      this.oParentObject.w_TUTIFIS = this.oParentObject.w_TUTIFIS + NVL(_Curs_GSCE_TSC.SCUTIFIS,0)
      this.oParentObject.w_TUTIANT = this.oParentObject.w_TUTIANT + NVL(_Curs_GSCE_TSC.SCUTIANT,0)
      this.oParentObject.w_TACCANT = this.oParentObject.w_TACCANT + NVL(_Curs_GSCE_TSC.SCACCANT,0)
      this.oParentObject.w_TQTARESI = this.oParentObject.w_TQTARESI + NVL(_Curs_GSCE_TSC.SCQTACES,0)
      this.oParentObject.w_TINCVAC = this.oParentObject.w_TINCVAC + NVL(_Curs_GSCE_TSC.SCINCVAC,0)
      this.oParentObject.w_TIMPONC = this.oParentObject.w_TIMPONC + NVL(_Curs_GSCE_TSC.SCIMPONC,0)
      this.oParentObject.w_TDECVAC = this.oParentObject.w_TDECVAC + NVL(_Curs_GSCE_TSC.SCDECVAC,0)
      this.oParentObject.w_TIMPRIC = this.oParentObject.w_TIMPRIC + NVL(_Curs_GSCE_TSC.SCIMPRIC,0)
      this.oParentObject.w_TIMPSVC = this.oParentObject.w_TIMPSVC + NVL(_Curs_GSCE_TSC.SCIMPSVC,0)
        select _Curs_GSCE_TSC
        continue
      enddo
      use
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='SAL_CESE'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_GSCE_TSC')
      use in _Curs_GSCE_TSC
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
