* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_mea                                                        *
*              Categoria/ammortamenti                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30] [VRS_40]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-02-26                                                      *
* Last revis.: 2012-11-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsce_mea")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsce_mea")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsce_mea")
  return

* --- Class definition
define class tgsce_mea as StdPCForm
  Width  = 364
  Height = 163
  Top    = 78
  Left   = 158
  cComment = "Categoria/ammortamenti"
  cPrg = "gsce_mea"
  HelpContextID=204847767
  add object cnt as tcgsce_mea
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsce_mea as PCContext
  w_EACODCAT = space(15)
  w_CODAZI = space(10)
  w_AMMANT = 0
  w_MAXANT = 0
  w_PERRID1 = 0
  w_TIPBEN = space(1)
  w_EAPROESE = 0
  w_EATIPAMM = space(1)
  w_MAXPEORD = 0
  w_COEFIS = 0
  w_EAPERORD = 0
  w_MAXPEANT = 0
  w_EAPERANT = 0
  w_TOTPEORD = 0
  w_TOTPEANT = 0
  w_TOTALE = 0
  w_TIPAMM = space(1)
  w_IASADO = space(1)
  w_EAPERORD_T = 0
  w_TOTRIGA = 0
  w_FLRIDU = space(1)
  w_PMAXPEORD = 0
  proc Save(i_oFrom)
    this.w_EACODCAT = i_oFrom.w_EACODCAT
    this.w_CODAZI = i_oFrom.w_CODAZI
    this.w_AMMANT = i_oFrom.w_AMMANT
    this.w_MAXANT = i_oFrom.w_MAXANT
    this.w_PERRID1 = i_oFrom.w_PERRID1
    this.w_TIPBEN = i_oFrom.w_TIPBEN
    this.w_EAPROESE = i_oFrom.w_EAPROESE
    this.w_EATIPAMM = i_oFrom.w_EATIPAMM
    this.w_MAXPEORD = i_oFrom.w_MAXPEORD
    this.w_COEFIS = i_oFrom.w_COEFIS
    this.w_EAPERORD = i_oFrom.w_EAPERORD
    this.w_MAXPEANT = i_oFrom.w_MAXPEANT
    this.w_EAPERANT = i_oFrom.w_EAPERANT
    this.w_TOTPEORD = i_oFrom.w_TOTPEORD
    this.w_TOTPEANT = i_oFrom.w_TOTPEANT
    this.w_TOTALE = i_oFrom.w_TOTALE
    this.w_TIPAMM = i_oFrom.w_TIPAMM
    this.w_IASADO = i_oFrom.w_IASADO
    this.w_EAPERORD_T = i_oFrom.w_EAPERORD_T
    this.w_TOTRIGA = i_oFrom.w_TOTRIGA
    this.w_FLRIDU = i_oFrom.w_FLRIDU
    this.w_PMAXPEORD = i_oFrom.w_PMAXPEORD
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_EACODCAT = this.w_EACODCAT
    i_oTo.w_CODAZI = this.w_CODAZI
    i_oTo.w_AMMANT = this.w_AMMANT
    i_oTo.w_MAXANT = this.w_MAXANT
    i_oTo.w_PERRID1 = this.w_PERRID1
    i_oTo.w_TIPBEN = this.w_TIPBEN
    i_oTo.w_EAPROESE = this.w_EAPROESE
    i_oTo.w_EATIPAMM = this.w_EATIPAMM
    i_oTo.w_MAXPEORD = this.w_MAXPEORD
    i_oTo.w_COEFIS = this.w_COEFIS
    i_oTo.w_EAPERORD = this.w_EAPERORD
    i_oTo.w_MAXPEANT = this.w_MAXPEANT
    i_oTo.w_EAPERANT = this.w_EAPERANT
    i_oTo.w_TOTPEORD = this.w_TOTPEORD
    i_oTo.w_TOTPEANT = this.w_TOTPEANT
    i_oTo.w_TOTALE = this.w_TOTALE
    i_oTo.w_TIPAMM = this.w_TIPAMM
    i_oTo.w_IASADO = this.w_IASADO
    i_oTo.w_EAPERORD_T = this.w_EAPERORD_T
    i_oTo.w_TOTRIGA = this.w_TOTRIGA
    i_oTo.w_FLRIDU = this.w_FLRIDU
    i_oTo.w_PMAXPEORD = this.w_PMAXPEORD
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsce_mea as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 364
  Height = 163
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-11-05"
  HelpContextID=204847767
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=22

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  CAT_AMMO_IDX = 0
  PAR_CESP_IDX = 0
  cFile = "CAT_AMMO"
  cKeySelect = "EACODCAT"
  cKeyWhere  = "EACODCAT=this.w_EACODCAT"
  cKeyDetail  = "EACODCAT=this.w_EACODCAT and EAPROESE=this.w_EAPROESE"
  cKeyWhereODBC = '"EACODCAT="+cp_ToStrODBC(this.w_EACODCAT)';

  cKeyDetailWhereODBC = '"EACODCAT="+cp_ToStrODBC(this.w_EACODCAT)';
      +'+" and EAPROESE="+cp_ToStrODBC(this.w_EAPROESE)';

  cKeyWhereODBCqualified = '"CAT_AMMO.EACODCAT="+cp_ToStrODBC(this.w_EACODCAT)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cPrg = "gsce_mea"
  cComment = "Categoria/ammortamenti"
  i_nRowNum = 0
  i_nRowPerPage = 5
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_EACODCAT = space(15)
  w_CODAZI = space(10)
  w_AMMANT = 0
  w_MAXANT = 0
  w_PERRID1 = 0
  w_TIPBEN = space(1)
  w_EAPROESE = 0
  o_EAPROESE = 0
  w_EATIPAMM = space(1)
  o_EATIPAMM = space(1)
  w_MAXPEORD = 0
  o_MAXPEORD = 0
  w_COEFIS = 0
  o_COEFIS = 0
  w_EAPERORD = 0
  o_EAPERORD = 0
  w_MAXPEANT = 0
  o_MAXPEANT = 0
  w_EAPERANT = 0
  w_TOTPEORD = 0
  w_TOTPEANT = 0
  w_TOTALE = 0
  w_TIPAMM = space(1)
  w_IASADO = space(1)
  w_EAPERORD_T = 0
  w_TOTRIGA = 0
  w_FLRIDU = space(1)
  o_FLRIDU = space(1)
  w_PMAXPEORD = 0
  o_PMAXPEORD = 0
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsce_meaPag1","gsce_mea",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='PAR_CESP'
    this.cWorkTables[2]='CAT_AMMO'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CAT_AMMO_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CAT_AMMO_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsce_mea'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from CAT_AMMO where EACODCAT=KeySet.EACODCAT
    *                            and EAPROESE=KeySet.EAPROESE
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- gsce_mea
      * --- Setta Ordine per Progressivo Periodo
      i_cOrder = 'order by EAPROESE '
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.CAT_AMMO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_AMMO_IDX,2],this.bLoadRecFilter,this.CAT_AMMO_IDX,"gsce_mea")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CAT_AMMO')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CAT_AMMO.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' CAT_AMMO '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'EACODCAT',this.w_EACODCAT  )
      select * from (i_cTable) CAT_AMMO where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_CODAZI = i_CODAZI
        .w_AMMANT = 0
        .w_MAXANT = 0
        .w_PERRID1 = 0
        .w_TIPBEN = space(1)
        .w_COEFIS = 0
        .w_TOTPEORD = 0
        .w_TOTPEANT = 0
        .w_TOTALE = 0
        .w_TIPAMM = space(1)
        .w_IASADO = space(1)
        .w_FLRIDU = space(1)
        .w_EACODCAT = NVL(EACODCAT,space(15))
          .link_1_2('Load')
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate()
        .w_PMAXPEORD = cp_ROUND(.w_COEFIS*iif( .w_TIPBEN='M' AND .w_FLRIDU='S' AND .w_PERRID1<>0 , .w_PERRID1/100, 1),2)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'CAT_AMMO')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      this.w_TOTPEORD = 0
      this.w_TOTPEANT = 0
      this.w_TOTALE = 0
      scan
        with this
          .w_EAPROESE = NVL(EAPROESE,0)
          .w_EATIPAMM = NVL(EATIPAMM,space(1))
        .w_MAXPEORD = iif(.w_EAPROESE=1, .w_PMAXPEORD, .w_COEFIS)
          .w_EAPERORD = NVL(EAPERORD,0)
        .w_MAXPEANT = cp_ROUND(IIF(.w_TIPBEN='I',0,(.w_EAPERORD*(.w_AMMANT/100))),2)
          .w_EAPERANT = NVL(EAPERANT,0)
        .w_EAPERORD_T = IIF(.w_EATIPAMM='R',0,.w_EAPERORD)
        .w_TOTRIGA = .w_EAPERORD_T+.w_EAPERANT
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          .w_TOTPEORD = .w_TOTPEORD+.w_EAPERORD_T
          .w_TOTPEANT = .w_TOTPEANT+.w_EAPERANT
          .w_TOTALE = .w_TOTALE+.w_TOTRIGA
          replace EAPROESE with .w_EAPROESE
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate()
        .w_PMAXPEORD = cp_ROUND(.w_COEFIS*iif( .w_TIPBEN='M' AND .w_FLRIDU='S' AND .w_PERRID1<>0 , .w_PERRID1/100, 1),2)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_EACODCAT=space(15)
      .w_CODAZI=space(10)
      .w_AMMANT=0
      .w_MAXANT=0
      .w_PERRID1=0
      .w_TIPBEN=space(1)
      .w_EAPROESE=0
      .w_EATIPAMM=space(1)
      .w_MAXPEORD=0
      .w_COEFIS=0
      .w_EAPERORD=0
      .w_MAXPEANT=0
      .w_EAPERANT=0
      .w_TOTPEORD=0
      .w_TOTPEANT=0
      .w_TOTALE=0
      .w_TIPAMM=space(1)
      .w_IASADO=space(1)
      .w_EAPERORD_T=0
      .w_TOTRIGA=0
      .w_FLRIDU=space(1)
      .w_PMAXPEORD=0
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
        .w_CODAZI = i_CODAZI
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODAZI))
         .link_1_2('Full')
        endif
        .DoRTCalc(3,7,.f.)
        .w_EATIPAMM = 'O'
        .w_MAXPEORD = iif(.w_EAPROESE=1, .w_PMAXPEORD, .w_COEFIS)
        .DoRTCalc(10,10,.f.)
        .w_EAPERORD = iif(.w_EAPERORD=0, INT(iif(.w_EAPROESE=1, .w_PMAXPEORD, .w_COEFIS) * iif(.w_EAPROESE>0,1,0)*100)/100, .w_EAPERORD)
        .w_MAXPEANT = cp_ROUND(IIF(.w_TIPBEN='I',0,(.w_EAPERORD*(.w_AMMANT/100))),2)
        .w_EAPERANT = INT(iif(.w_EATIPAMM='A', .w_MAXPEANT, 0) * iif(.w_EAPROESE>0,1,0)*100)/100
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate()
        .DoRTCalc(14,18,.f.)
        .w_EAPERORD_T = IIF(.w_EATIPAMM='R',0,.w_EAPERORD)
        .w_TOTRIGA = .w_EAPERORD_T+.w_EAPERANT
        .DoRTCalc(21,21,.f.)
        .w_PMAXPEORD = cp_ROUND(.w_COEFIS*iif( .w_TIPBEN='M' AND .w_FLRIDU='S' AND .w_PERRID1<>0 , .w_PERRID1/100, 1),2)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'CAT_AMMO')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oObj_1_10.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'CAT_AMMO',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CAT_AMMO_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EACODCAT,"EACODCAT",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_EAPROESE N(3);
      ,t_EATIPAMM N(3);
      ,t_EAPERORD N(6,2);
      ,t_EAPERANT N(6,2);
      ,EAPROESE N(3);
      ,t_MAXPEORD N(6,2);
      ,t_MAXPEANT N(6,2);
      ,t_EAPERORD_T N(6,2);
      ,t_TOTRIGA N(6,2);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsce_meabodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oEAPROESE_2_1.controlsource=this.cTrsName+'.t_EAPROESE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oEATIPAMM_2_2.controlsource=this.cTrsName+'.t_EATIPAMM'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oEAPERORD_2_4.controlsource=this.cTrsName+'.t_EAPERORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oEAPERANT_2_6.controlsource=this.cTrsName+'.t_EAPERANT'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(58)
    this.AddVLine(183)
    this.AddVLine(263)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oEAPROESE_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.CAT_AMMO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_AMMO_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CAT_AMMO_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_AMMO_IDX,2])
      *
      * insert into CAT_AMMO
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CAT_AMMO')
        i_extval=cp_InsertValODBCExtFlds(this,'CAT_AMMO')
        i_cFldBody=" "+;
                  "(EACODCAT,EAPROESE,EATIPAMM,EAPERORD,EAPERANT,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_EACODCAT)+","+cp_ToStrODBC(this.w_EAPROESE)+","+cp_ToStrODBC(this.w_EATIPAMM)+","+cp_ToStrODBC(this.w_EAPERORD)+","+cp_ToStrODBC(this.w_EAPERANT)+;
             ","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CAT_AMMO')
        i_extval=cp_InsertValVFPExtFlds(this,'CAT_AMMO')
        cp_CheckDeletedKey(i_cTable,0,'EACODCAT',this.w_EACODCAT,'EAPROESE',this.w_EAPROESE)
        INSERT INTO (i_cTable) (;
                   EACODCAT;
                  ,EAPROESE;
                  ,EATIPAMM;
                  ,EAPERORD;
                  ,EAPERANT;
                  ,CPCCCHK &i_extfld.) VALUES (;
                  this.w_EACODCAT;
                  ,this.w_EAPROESE;
                  ,this.w_EATIPAMM;
                  ,this.w_EAPERORD;
                  ,this.w_EAPERANT;
                  ,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.CAT_AMMO_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_AMMO_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (t_EAPROESE<>0) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'CAT_AMMO')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and EAPROESE="+cp_ToStrODBC(&i_TN.->EAPROESE)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'CAT_AMMO')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and EAPROESE=&i_TN.->EAPROESE;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (t_EAPROESE<>0) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and EAPROESE="+cp_ToStrODBC(&i_TN.->EAPROESE)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and EAPROESE=&i_TN.->EAPROESE;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = 0
              replace EAPROESE with this.w_EAPROESE
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update CAT_AMMO
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'CAT_AMMO')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " EATIPAMM="+cp_ToStrODBC(this.w_EATIPAMM)+;
                     ",EAPERORD="+cp_ToStrODBC(this.w_EAPERORD)+;
                     ",EAPERANT="+cp_ToStrODBC(this.w_EAPERANT)+;
                     ",EAPROESE="+cp_ToStrODBC(this.w_EAPROESE)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+;
                             " and EAPROESE="+cp_ToStrODBC(EAPROESE)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'CAT_AMMO')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      EATIPAMM=this.w_EATIPAMM;
                     ,EAPERORD=this.w_EAPERORD;
                     ,EAPERANT=this.w_EAPERANT;
                     ,EAPROESE=this.w_EAPROESE;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                                      and EAPROESE=&i_TN.->EAPROESE;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- gsce_mea
    *Controlla la sequenza dei progressivi
    this.NotifyEvent('Controlli')
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.CAT_AMMO_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_AMMO_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (t_EAPROESE<>0) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete CAT_AMMO
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and EAPROESE="+cp_ToStrODBC(&i_TN.->EAPROESE)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and EAPROESE=&i_TN.->EAPROESE;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (t_EAPROESE<>0) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CAT_AMMO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_AMMO_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
          .link_1_2('Full')
        .DoRTCalc(3,8,.t.)
        if .o_EAPROESE<>.w_EAPROESE.or. .o_EATIPAMM<>.w_EATIPAMM.or. .o_FLRIDU<>.w_FLRIDU.or. .o_COEFIS<>.w_COEFIS.or. .o_PMAXPEORD<>.w_PMAXPEORD.or. .o_EAPERORD<>.w_EAPERORD
          .w_MAXPEORD = iif(.w_EAPROESE=1, .w_PMAXPEORD, .w_COEFIS)
        endif
        .DoRTCalc(10,10,.t.)
        if .o_MAXPEORD<>.w_MAXPEORD.or. .o_EAPROESE<>.w_EAPROESE.or. .o_EATIPAMM<>.w_EATIPAMM.or. .o_FLRIDU<>.w_FLRIDU.or. .o_COEFIS<>.w_COEFIS
          .w_EAPERORD = iif(.w_EAPERORD=0, INT(iif(.w_EAPROESE=1, .w_PMAXPEORD, .w_COEFIS) * iif(.w_EAPROESE>0,1,0)*100)/100, .w_EAPERORD)
        endif
        if .o_EAPROESE<>.w_EAPROESE.or. .o_EATIPAMM<>.w_EATIPAMM
          .w_MAXPEANT = cp_ROUND(IIF(.w_TIPBEN='I',0,(.w_EAPERORD*(.w_AMMANT/100))),2)
        endif
        if .o_MAXPEANT<>.w_MAXPEANT.or. .o_EAPROESE<>.w_EAPROESE.or. .o_EATIPAMM<>.w_EATIPAMM
          .w_TOTPEANT = .w_TOTPEANT-.w_eaperant
          .w_EAPERANT = INT(iif(.w_EATIPAMM='A', .w_MAXPEANT, 0) * iif(.w_EAPROESE>0,1,0)*100)/100
          .w_TOTPEANT = .w_TOTPEANT+.w_eaperant
        endif
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate()
        .DoRTCalc(14,18,.t.)
        if .o_EAPERORD<>.w_EAPERORD.or. .o_EATIPAMM<>.w_EATIPAMM
          .w_TOTPEORD = .w_TOTPEORD-.w_eaperord_t
          .w_EAPERORD_T = IIF(.w_EATIPAMM='R',0,.w_EAPERORD)
          .w_TOTPEORD = .w_TOTPEORD+.w_eaperord_t
        endif
          .w_TOTALE = .w_TOTALE-.w_totriga
          .w_TOTRIGA = .w_EAPERORD_T+.w_EAPERANT
          .w_TOTALE = .w_TOTALE+.w_totriga
        .DoRTCalc(21,21,.t.)
        if .o_EATIPAMM<>.w_EATIPAMM.or. .o_FLRIDU<>.w_FLRIDU.or. .o_COEFIS<>.w_COEFIS.or. .o_EAPROESE<>.w_EAPROESE
          .w_PMAXPEORD = cp_ROUND(.w_COEFIS*iif( .w_TIPBEN='M' AND .w_FLRIDU='S' AND .w_PERRID1<>0 , .w_PERRID1/100, 1),2)
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        this.Calculate_OGAKVPPLSY()
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_MAXPEORD with this.w_MAXPEORD
      replace t_MAXPEANT with this.w_MAXPEANT
      replace t_EAPERORD_T with this.w_EAPERORD_T
      replace t_TOTRIGA with this.w_TOTRIGA
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return
  proc Calculate_OGAKVPPLSY()
    with this
          * --- w_CCFLRIDU Cambia
          .w_FLRIDU = IIF(.w_EAPROESE<>0, .w_FLRIDU, THIS.OPARENTOBJECT.w_CCFLRIDU)
          .w_PMAXPEORD = cp_ROUND(.w_COEFIS*iif( .w_TIPBEN='M' AND THIS.OPARENTOBJECT.w_CCFLRIDU='S' AND .w_PERRID1<>0 , .w_PERRID1/100, 1),2)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oEATIPAMM_2_2.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oEATIPAMM_2_2.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oEAPERORD_2_4.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oEAPERORD_2_4.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oEAPERANT_2_6.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oEAPERANT_2_6.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsce_mea
    * --- Messaggio di avviso per Rateo utilizzo
    
    If cEvent = "w_EATIPAMM Changed" And this.w_EATIPAMM='R'
       Ah_ErrorMsg("Attenzione! La percentuale impostata su riga con tipo ammortamento Rateo Utilizzo non verr� considerata nel totale")
    Endif
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_10.Event(cEvent)
        if lower(cEvent)==lower("Ricalcola")
          .Calculate_OGAKVPPLSY()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_CESP_IDX,3]
    i_lTable = "PAR_CESP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_CESP_IDX,2], .t., this.PAR_CESP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_CESP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PCCODAZI,PCPERANT,PCMAXANT,PCPERESE,PCTIPAMM,PCIASADO";
                   +" from "+i_cTable+" "+i_lTable+" where PCCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PCCODAZI',this.w_CODAZI)
            select PCCODAZI,PCPERANT,PCMAXANT,PCPERESE,PCTIPAMM,PCIASADO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.PCCODAZI,space(10))
      this.w_AMMANT = NVL(_Link_.PCPERANT,0)
      this.w_MAXANT = NVL(_Link_.PCMAXANT,0)
      this.w_PERRID1 = NVL(_Link_.PCPERESE,0)
      this.w_TIPAMM = NVL(_Link_.PCTIPAMM,space(1))
      this.w_IASADO = NVL(_Link_.PCIASADO,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(10)
      endif
      this.w_AMMANT = 0
      this.w_MAXANT = 0
      this.w_PERRID1 = 0
      this.w_TIPAMM = space(1)
      this.w_IASADO = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_CESP_IDX,2])+'\'+cp_ToStr(_Link_.PCCODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_CESP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oTOTPEORD_3_1.value==this.w_TOTPEORD)
      this.oPgFrm.Page1.oPag.oTOTPEORD_3_1.value=this.w_TOTPEORD
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTPEANT_3_2.value==this.w_TOTPEANT)
      this.oPgFrm.Page1.oPag.oTOTPEANT_3_2.value=this.w_TOTPEANT
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTALE_3_3.value==this.w_TOTALE)
      this.oPgFrm.Page1.oPag.oTOTALE_3_3.value=this.w_TOTALE
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oEAPROESE_2_1.value==this.w_EAPROESE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oEAPROESE_2_1.value=this.w_EAPROESE
      replace t_EAPROESE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oEAPROESE_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oEATIPAMM_2_2.RadioValue()==this.w_EATIPAMM)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oEATIPAMM_2_2.SetRadio()
      replace t_EATIPAMM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oEATIPAMM_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oEAPERORD_2_4.value==this.w_EAPERORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oEAPERORD_2_4.value=this.w_EAPERORD
      replace t_EAPERORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oEAPERORD_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oEAPERANT_2_6.value==this.w_EAPERANT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oEAPERANT_2_6.value=this.w_EAPERANT
      replace t_EAPERANT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oEAPERANT_2_6.value
    endif
    cp_SetControlsValueExtFlds(this,'CAT_AMMO')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsce_mea
      
            if this.w_TOTALE<>100 AND this.w_TOTALE<>0
                  i_bnoChk=.f.
                  i_bRes=.f.
                  i_cErrorMsg=ah_MsgFormat("Il totale delle percentuali deve essere uguale a 100")
            endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(.w_EATIPAMM='O' OR (.w_TIPBEN='M' AND .w_EATIPAMM='A' AND .w_EAPROESE<=.w_MAXANT) OR ( (.w_TIPBEN='I' OR (.w_IASADO='S' and .w_TIPBEN='M') ) AND .w_EATIPAMM='R' AND .w_EAPROESE=1)) and ( .w_TIPAMM<>'C') and (.w_EAPROESE<>0)
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oEATIPAMM_2_2
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Tipo ammortamento non corretto")
        case   not((.w_EAPERORD>0 AND .w_EAPERORD<=iif(.w_EAPROESE=1, .w_PMAXPEORD, .w_COEFIS)) OR .w_EAPROESE=0 OR (.w_EAPERORD=100 AND .w_COEFIS=100)) and (.w_TIPAMM<>'C') and (.w_EAPROESE<>0)
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oEAPERORD_2_4
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Valore errato")
        case   not((.w_EAPERANT>=0 AND .w_EAPERANT<=.w_MAXPEANT) OR .w_EAPROESE=0) and (.w_EATIPAMM='A' AND .w_TIPAMM<>'C') and (.w_EAPROESE<>0)
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oEAPERANT_2_6
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Valore errato")
      endcase
      if .w_EAPROESE<>0
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_EAPROESE = this.w_EAPROESE
    this.o_EATIPAMM = this.w_EATIPAMM
    this.o_MAXPEORD = this.w_MAXPEORD
    this.o_COEFIS = this.w_COEFIS
    this.o_EAPERORD = this.w_EAPERORD
    this.o_MAXPEANT = this.w_MAXPEANT
    this.o_FLRIDU = this.w_FLRIDU
    this.o_PMAXPEORD = this.w_PMAXPEORD
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(t_EAPROESE<>0)
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_EAPROESE=0
      .w_EATIPAMM=space(1)
      .w_MAXPEORD=0
      .w_EAPERORD=0
      .w_MAXPEANT=0
      .w_EAPERANT=0
      .w_EAPERORD_T=0
      .w_TOTRIGA=0
      .DoRTCalc(1,7,.f.)
        .w_EATIPAMM = 'O'
        .w_MAXPEORD = iif(.w_EAPROESE=1, .w_PMAXPEORD, .w_COEFIS)
      .DoRTCalc(10,10,.f.)
        .w_EAPERORD = iif(.w_EAPERORD=0, INT(iif(.w_EAPROESE=1, .w_PMAXPEORD, .w_COEFIS) * iif(.w_EAPROESE>0,1,0)*100)/100, .w_EAPERORD)
        .w_MAXPEANT = cp_ROUND(IIF(.w_TIPBEN='I',0,(.w_EAPERORD*(.w_AMMANT/100))),2)
        .w_EAPERANT = INT(iif(.w_EATIPAMM='A', .w_MAXPEANT, 0) * iif(.w_EAPROESE>0,1,0)*100)/100
      .DoRTCalc(14,18,.f.)
        .w_EAPERORD_T = IIF(.w_EATIPAMM='R',0,.w_EAPERORD)
        .w_TOTRIGA = .w_EAPERORD_T+.w_EAPERANT
    endwith
    this.DoRTCalc(21,22,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_EAPROESE = t_EAPROESE
    this.w_EATIPAMM = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oEATIPAMM_2_2.RadioValue(.t.)
    this.w_MAXPEORD = t_MAXPEORD
    this.w_EAPERORD = t_EAPERORD
    this.w_MAXPEANT = t_MAXPEANT
    this.w_EAPERANT = t_EAPERANT
    this.w_EAPERORD_T = t_EAPERORD_T
    this.w_TOTRIGA = t_TOTRIGA
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_EAPROESE with this.w_EAPROESE
    replace t_EATIPAMM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oEATIPAMM_2_2.ToRadio()
    replace t_MAXPEORD with this.w_MAXPEORD
    replace t_EAPERORD with this.w_EAPERORD
    replace t_MAXPEANT with this.w_MAXPEANT
    replace t_EAPERANT with this.w_EAPERANT
    replace t_EAPERORD_T with this.w_EAPERORD_T
    replace t_TOTRIGA with this.w_TOTRIGA
    if i_srv='A'
      replace EAPROESE with this.w_EAPROESE
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
     with this
        .w_TOTPEANT = .w_TOTPEANT-.w_eaperant
        .w_TOTPEORD = .w_TOTPEORD-.w_eaperord_t
        .w_TOTALE = .w_TOTALE-.w_totriga
        .SetControlsValue()
      endwith
  EndProc
enddefine

* --- Define pages as container
define class tgsce_meaPag1 as StdContainer
  Width  = 360
  height = 163
  stdWidth  = 360
  stdheight = 163
  resizeYpos=100
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oObj_1_10 as cp_runprogram with uid="ZNESPFWTXH",left=87, top=178, width=143,height=24,;
    caption='GSCE_BMA(CT)',;
   bGlobalFont=.t.,;
    prg="GSCE_BMA('CT')",;
    cEvent = "Controlli",;
    nPag=1;
    , HelpContextID = 190986201


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=6, top=5, width=342,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=4,Field1="EAPROESE",Label1="Prog.es.",Field2="EATIPAMM",Label2="Tipo ammortamento",Field3="EAPERORD",Label3="% Ordinario",Field4="EAPERANT",Label4="% Anticipato",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 252503162

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-4,top=24,;
    width=344+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*5*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-3,top=25,width=343+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*5*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1

  add object oTOTPEORD_3_1 as StdField with uid="ELBYGNGHIN",rtseq=14,rtrep=.f.,;
    cFormVar="w_TOTPEORD",value=0,enabled=.f.,;
    HelpContextID = 2404486,;
    cQueryName = "TOTPEORD",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=181, Top=129, cSayPict=["999.99"], cGetPict=["999.99"]

  add object oTOTPEANT_3_2 as StdField with uid="TIWSAYCQQO",rtseq=15,rtrep=.f.,;
    cFormVar="w_TOTPEANT",value=0,enabled=.f.,;
    HelpContextID = 237285494,;
    cQueryName = "TOTPEANT",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=267, Top=129, cSayPict=["999.99"], cGetPict=["999.99"]

  add object oTOTALE_3_3 as StdField with uid="YWFOHMELCC",rtseq=16,rtrep=.f.,;
    cFormVar="w_TOTALE",value=0,enabled=.f.,;
    HelpContextID = 163819722,;
    cQueryName = "TOTALE",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=101, Top=129, cSayPict=["999.99"], cGetPict=["999.99"]

  add object oStr_3_4 as StdString with uid="PKNJBSEHRU",Visible=.t., Left=5, Top=129,;
    Alignment=1, Width=92, Height=15,;
    Caption="Totale:"  ;
  , bGlobalFont=.t.
enddefine

* --- Defining Body row
define class tgsce_meaBodyRow as CPBodyRowCnt
  Width=334
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oEAPROESE_2_1 as StdTrsField with uid="YDKCJNNRVJ",rtseq=7,rtrep=.t.,;
    cFormVar="w_EAPROESE",value=0,isprimarykey=.t.,;
    ToolTipText = "Progressivo esercizio",;
    HelpContextID = 159580021,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=44, Left=-2, Top=0, cSayPict=["999"], cGetPict=["999"]

  add object oEATIPAMM_2_2 as StdTrsCombo with uid="AMUJLXLDUR",rtrep=.t.,;
    cFormVar="w_EATIPAMM", RowSource=""+"Anticipato,"+"Ordinario,"+"Rateo utilizzo" , ;
    ToolTipText = "Tipo ammortamento",;
    HelpContextID = 226213741,;
    Height=21, Width=110, Left=59, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2  , sErrorMsg = "Tipo ammortamento non corretto";
, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oEATIPAMM_2_2.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..EATIPAMM,&i_cF..t_EATIPAMM),this.value)
    return(iif(xVal =1,'A',;
    iif(xVal =2,'O',;
    iif(xVal =3,'R',;
    space(1)))))
  endfunc
  func oEATIPAMM_2_2.GetRadio()
    this.Parent.oContained.w_EATIPAMM = this.RadioValue()
    return .t.
  endfunc

  func oEATIPAMM_2_2.ToRadio()
    this.Parent.oContained.w_EATIPAMM=trim(this.Parent.oContained.w_EATIPAMM)
    return(;
      iif(this.Parent.oContained.w_EATIPAMM=='A',1,;
      iif(this.Parent.oContained.w_EATIPAMM=='O',2,;
      iif(this.Parent.oContained.w_EATIPAMM=='R',3,;
      0))))
  endfunc

  func oEATIPAMM_2_2.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oEATIPAMM_2_2.mCond()
    with this.Parent.oContained
      return ( .w_TIPAMM<>'C')
    endwith
  endfunc

  func oEATIPAMM_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_EATIPAMM='O' OR (.w_TIPBEN='M' AND .w_EATIPAMM='A' AND .w_EAPROESE<=.w_MAXANT) OR ( (.w_TIPBEN='I' OR (.w_IASADO='S' and .w_TIPBEN='M') ) AND .w_EATIPAMM='R' AND .w_EAPROESE=1))
    endwith
    return bRes
  endfunc

  add object oEAPERORD_2_4 as StdTrsField with uid="DAZWDRADGM",rtseq=11,rtrep=.t.,;
    cFormVar="w_EAPERORD",value=0,;
    ToolTipText = "Percentuale ammortamento ordinario",;
    HelpContextID = 257949558,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Valore errato",;
   bGlobalFont=.t.,;
    Height=17, Width=69, Left=181, Top=0, cSayPict=["999.99"], cGetPict=["999.99"]

  func oEAPERORD_2_4.mCond()
    with this.Parent.oContained
      return (.w_TIPAMM<>'C')
    endwith
  endfunc

  func oEAPERORD_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_EAPERORD>0 AND .w_EAPERORD<=iif(.w_EAPROESE=1, .w_PMAXPEORD, .w_COEFIS)) OR .w_EAPROESE=0 OR (.w_EAPERORD=100 AND .w_COEFIS=100))
    endwith
    return bRes
  endfunc

  add object oEAPERANT_2_6 as StdTrsField with uid="YYLVJSAKLV",rtseq=13,rtrep=.t.,;
    cFormVar="w_EAPERANT",value=0,;
    ToolTipText = "Percentuale ammortamento anticipato",;
    HelpContextID = 224395110,;
    cTotal = "this.Parent.oContained.w_totpeant", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Valore errato",;
   bGlobalFont=.t.,;
    Height=17, Width=69, Left=260, Top=0, cSayPict=["999.99"], cGetPict=["999.99"]

  func oEAPERANT_2_6.mCond()
    with this.Parent.oContained
      return (.w_EATIPAMM='A' AND .w_TIPAMM<>'C')
    endwith
  endfunc

  func oEAPERANT_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_EAPERANT>=0 AND .w_EAPERANT<=.w_MAXPEANT) OR .w_EAPROESE=0)
    endwith
    return bRes
  endfunc
  add object oLast as LastKeyMover
  * ---
  func oEAPROESE_2_1.When()
    return(.t.)
  proc oEAPROESE_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oEAPROESE_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=4
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsce_mea','CAT_AMMO','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".EACODCAT=CAT_AMMO.EACODCAT";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
