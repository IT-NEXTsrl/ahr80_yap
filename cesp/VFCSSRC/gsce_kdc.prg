* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_kdc                                                        *
*              Dati cespite                                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-03-01                                                      *
* Last revis.: 2008-09-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsce_kdc",oParentObject))

* --- Class definition
define class tgsce_kdc as StdForm
  Top    = 65
  Left   = 10

  * --- Standard Properties
  Width  = 652
  Height = 439
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-09-15"
  HelpContextID=82462057
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=21

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsce_kdc"
  cComment = "Dati cespite"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_IMPV01 = 0
  w_IMPV02 = 0
  w_IMPV03 = 0
  w_IMPV04 = 0
  w_IMPV05 = 0
  w_IMPV06 = 0
  w_IMPV14 = 0
  w_IMPV07 = 0
  w_IMPV08 = 0
  w_IMPV09 = 0
  w_IMPV13 = 0
  w_IMPV10 = 0
  w_MCCODCES = space(20)
  w_DESCES = space(40)
  w_TCOECIV = 0
  w_TCOEFI1 = 0
  w_TCOEFI2 = 0
  w_IMPV11 = 0
  w_IMPV12 = 0
  w_IMPV16 = 0
  w_IMPV17 = 0
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsce_kdcPag1","gsce_kdc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_IMPV01=0
      .w_IMPV02=0
      .w_IMPV03=0
      .w_IMPV04=0
      .w_IMPV05=0
      .w_IMPV06=0
      .w_IMPV14=0
      .w_IMPV07=0
      .w_IMPV08=0
      .w_IMPV09=0
      .w_IMPV13=0
      .w_IMPV10=0
      .w_MCCODCES=space(20)
      .w_DESCES=space(40)
      .w_TCOECIV=0
      .w_TCOEFI1=0
      .w_TCOEFI2=0
      .w_IMPV11=0
      .w_IMPV12=0
      .w_IMPV16=0
      .w_IMPV17=0
      .w_IMPV01=oParentObject.w_IMPV01
      .w_IMPV02=oParentObject.w_IMPV02
      .w_IMPV03=oParentObject.w_IMPV03
      .w_IMPV04=oParentObject.w_IMPV04
      .w_IMPV05=oParentObject.w_IMPV05
      .w_IMPV06=oParentObject.w_IMPV06
      .w_IMPV14=oParentObject.w_IMPV14
      .w_IMPV07=oParentObject.w_IMPV07
      .w_IMPV08=oParentObject.w_IMPV08
      .w_IMPV09=oParentObject.w_IMPV09
      .w_IMPV13=oParentObject.w_IMPV13
      .w_IMPV10=oParentObject.w_IMPV10
      .w_MCCODCES=oParentObject.w_MCCODCES
      .w_DESCES=oParentObject.w_DESCES
      .w_TCOECIV=oParentObject.w_TCOECIV
      .w_TCOEFI1=oParentObject.w_TCOEFI1
      .w_TCOEFI2=oParentObject.w_TCOEFI2
      .w_IMPV11=oParentObject.w_IMPV11
      .w_IMPV12=oParentObject.w_IMPV12
      .w_IMPV16=oParentObject.w_IMPV16
      .w_IMPV17=oParentObject.w_IMPV17
    endwith
    this.DoRTCalc(1,21,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_42.enabled = this.oPgFrm.Page1.oPag.oBtn_1_42.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_IMPV01=.w_IMPV01
      .oParentObject.w_IMPV02=.w_IMPV02
      .oParentObject.w_IMPV03=.w_IMPV03
      .oParentObject.w_IMPV04=.w_IMPV04
      .oParentObject.w_IMPV05=.w_IMPV05
      .oParentObject.w_IMPV06=.w_IMPV06
      .oParentObject.w_IMPV14=.w_IMPV14
      .oParentObject.w_IMPV07=.w_IMPV07
      .oParentObject.w_IMPV08=.w_IMPV08
      .oParentObject.w_IMPV09=.w_IMPV09
      .oParentObject.w_IMPV13=.w_IMPV13
      .oParentObject.w_IMPV10=.w_IMPV10
      .oParentObject.w_MCCODCES=.w_MCCODCES
      .oParentObject.w_DESCES=.w_DESCES
      .oParentObject.w_TCOECIV=.w_TCOECIV
      .oParentObject.w_TCOEFI1=.w_TCOEFI1
      .oParentObject.w_TCOEFI2=.w_TCOEFI2
      .oParentObject.w_IMPV11=.w_IMPV11
      .oParentObject.w_IMPV12=.w_IMPV12
      .oParentObject.w_IMPV16=.w_IMPV16
      .oParentObject.w_IMPV17=.w_IMPV17
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,21,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oIMPV01_1_1.value==this.w_IMPV01)
      this.oPgFrm.Page1.oPag.oIMPV01_1_1.value=this.w_IMPV01
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPV02_1_2.value==this.w_IMPV02)
      this.oPgFrm.Page1.oPag.oIMPV02_1_2.value=this.w_IMPV02
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPV03_1_3.value==this.w_IMPV03)
      this.oPgFrm.Page1.oPag.oIMPV03_1_3.value=this.w_IMPV03
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPV04_1_4.value==this.w_IMPV04)
      this.oPgFrm.Page1.oPag.oIMPV04_1_4.value=this.w_IMPV04
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPV05_1_5.value==this.w_IMPV05)
      this.oPgFrm.Page1.oPag.oIMPV05_1_5.value=this.w_IMPV05
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPV06_1_6.value==this.w_IMPV06)
      this.oPgFrm.Page1.oPag.oIMPV06_1_6.value=this.w_IMPV06
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPV14_1_7.value==this.w_IMPV14)
      this.oPgFrm.Page1.oPag.oIMPV14_1_7.value=this.w_IMPV14
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPV07_1_8.value==this.w_IMPV07)
      this.oPgFrm.Page1.oPag.oIMPV07_1_8.value=this.w_IMPV07
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPV08_1_9.value==this.w_IMPV08)
      this.oPgFrm.Page1.oPag.oIMPV08_1_9.value=this.w_IMPV08
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPV09_1_10.value==this.w_IMPV09)
      this.oPgFrm.Page1.oPag.oIMPV09_1_10.value=this.w_IMPV09
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPV13_1_11.value==this.w_IMPV13)
      this.oPgFrm.Page1.oPag.oIMPV13_1_11.value=this.w_IMPV13
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPV10_1_12.value==this.w_IMPV10)
      this.oPgFrm.Page1.oPag.oIMPV10_1_12.value=this.w_IMPV10
    endif
    if not(this.oPgFrm.Page1.oPag.oMCCODCES_1_24.value==this.w_MCCODCES)
      this.oPgFrm.Page1.oPag.oMCCODCES_1_24.value=this.w_MCCODCES
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCES_1_25.value==this.w_DESCES)
      this.oPgFrm.Page1.oPag.oDESCES_1_25.value=this.w_DESCES
    endif
    if not(this.oPgFrm.Page1.oPag.oTCOECIV_1_27.value==this.w_TCOECIV)
      this.oPgFrm.Page1.oPag.oTCOECIV_1_27.value=this.w_TCOECIV
    endif
    if not(this.oPgFrm.Page1.oPag.oTCOEFI1_1_28.value==this.w_TCOEFI1)
      this.oPgFrm.Page1.oPag.oTCOEFI1_1_28.value=this.w_TCOEFI1
    endif
    if not(this.oPgFrm.Page1.oPag.oTCOEFI2_1_29.value==this.w_TCOEFI2)
      this.oPgFrm.Page1.oPag.oTCOEFI2_1_29.value=this.w_TCOEFI2
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPV11_1_30.value==this.w_IMPV11)
      this.oPgFrm.Page1.oPag.oIMPV11_1_30.value=this.w_IMPV11
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPV12_1_31.value==this.w_IMPV12)
      this.oPgFrm.Page1.oPag.oIMPV12_1_31.value=this.w_IMPV12
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPV16_1_43.value==this.w_IMPV16)
      this.oPgFrm.Page1.oPag.oIMPV16_1_43.value=this.w_IMPV16
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPV17_1_46.value==this.w_IMPV17)
      this.oPgFrm.Page1.oPag.oIMPV17_1_46.value=this.w_IMPV17
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsce_kdcPag1 as StdContainer
  Width  = 648
  height = 439
  stdWidth  = 648
  stdheight = 439
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oIMPV01_1_1 as StdField with uid="VWJCAYQUNV",rtseq=1,rtrep=.f.,;
    cFormVar = "w_IMPV01", cQueryName = "IMPV01",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 9368442,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=177, Top=207, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  add object oIMPV02_1_2 as StdField with uid="IXFRRCPPMV",rtseq=2,rtrep=.f.,;
    cFormVar = "w_IMPV02", cQueryName = "IMPV02",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 261026682,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=517, Top=207, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  add object oIMPV03_1_3 as StdField with uid="AQSMXYKXZQ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_IMPV03", cQueryName = "IMPV03",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 244249466,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=177, Top=130, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  add object oIMPV04_1_4 as StdField with uid="LNPGLJBZGR",rtseq=4,rtrep=.f.,;
    cFormVar = "w_IMPV04", cQueryName = "IMPV04",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 227472250,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=517, Top=130, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  add object oIMPV05_1_5 as StdField with uid="ZCXQXAEDZA",rtseq=5,rtrep=.f.,;
    cFormVar = "w_IMPV05", cQueryName = "IMPV05",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 210695034,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=177, Top=235, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  add object oIMPV06_1_6 as StdField with uid="DSYXRXDDNX",rtseq=6,rtrep=.f.,;
    cFormVar = "w_IMPV06", cQueryName = "IMPV06",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 193917818,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=177, Top=262, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  add object oIMPV14_1_7 as StdField with uid="NKJNPOXBXO",rtseq=7,rtrep=.f.,;
    cFormVar = "w_IMPV14", cQueryName = "IMPV14",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 226423674,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=177, Top=289, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  add object oIMPV07_1_8 as StdField with uid="DQQBBSWNAQ",rtseq=8,rtrep=.f.,;
    cFormVar = "w_IMPV07", cQueryName = "IMPV07",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 177140602,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=517, Top=289, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  add object oIMPV08_1_9 as StdField with uid="PAEJGVFBXM",rtseq=9,rtrep=.f.,;
    cFormVar = "w_IMPV08", cQueryName = "IMPV08",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 160363386,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=517, Top=157, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  add object oIMPV09_1_10 as StdField with uid="XGEKJYZQWC",rtseq=10,rtrep=.f.,;
    cFormVar = "w_IMPV09", cQueryName = "IMPV09",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 143586170,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=517, Top=313, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  add object oIMPV13_1_11 as StdField with uid="XKLXUBVMLY",rtseq=11,rtrep=.f.,;
    cFormVar = "w_IMPV13", cQueryName = "IMPV13",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 243200890,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=517, Top=337, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  add object oIMPV10_1_12 as StdField with uid="UOCLYIGRGI",rtseq=12,rtrep=.f.,;
    cFormVar = "w_IMPV10", cQueryName = "IMPV10",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 25097082,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=517, Top=361, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  add object oMCCODCES_1_24 as StdField with uid="SVCHBHDYLW",rtseq=13,rtrep=.f.,;
    cFormVar = "w_MCCODCES", cQueryName = "MCCODCES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 44643097,;
   bGlobalFont=.t.,;
    Height=21, Width=156, Left=174, Top=14, InputMask=replicate('X',20)

  add object oDESCES_1_25 as StdField with uid="SIOXGPDUBN",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DESCES", cQueryName = "DESCES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 223464394,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=336, Top=14, InputMask=replicate('X',40)

  add object oTCOECIV_1_27 as StdField with uid="VWLZAWPNRO",rtseq=15,rtrep=.f.,;
    cFormVar = "w_TCOECIV", cQueryName = "TCOECIV",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 143651638,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=177, Top=157, cSayPict='"999.99"', cGetPict='"999.99"'

  add object oTCOEFI1_1_28 as StdField with uid="QLUAHPGNHV",rtseq=16,rtrep=.f.,;
    cFormVar = "w_TCOEFI1", cQueryName = "TCOEFI1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 121638090,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=177, Top=313, cSayPict='"999.99"', cGetPict='"999.99"'

  add object oTCOEFI2_1_29 as StdField with uid="RESIUEUNED",rtseq=17,rtrep=.f.,;
    cFormVar = "w_TCOEFI2", cQueryName = "TCOEFI2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 146797366,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=250, Top=313, cSayPict='"999.99"', cGetPict='"999.99"'

  add object oIMPV11_1_30 as StdField with uid="KMRCVJVEFB",rtseq=18,rtrep=.f.,;
    cFormVar = "w_IMPV11", cQueryName = "IMPV11",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 8319866,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=177, Top=340, cSayPict='"999.99"', cGetPict='"999.99"'

  add object oIMPV12_1_31 as StdField with uid="ULMBQLBURW",rtseq=19,rtrep=.f.,;
    cFormVar = "w_IMPV12", cQueryName = "IMPV12",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 259978106,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=177, Top=367, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"


  add object oBtn_1_42 as StdButton with uid="JWIBHYOYEX",left=590, top=389, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 75144634;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_42.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oIMPV16_1_43 as StdField with uid="ESMTMPSBHI",rtseq=20,rtrep=.f.,;
    cFormVar = "w_IMPV16", cQueryName = "IMPV16",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 192869242,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=177, Top=101, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  add object oIMPV17_1_46 as StdField with uid="YPGVFVOSLQ",rtseq=21,rtrep=.f.,;
    cFormVar = "w_IMPV17", cQueryName = "IMPV17",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 176092026,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=517, Top=101, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  add object oStr_1_13 as StdString with uid="OYZGHNGBFV",Visible=.t., Left=3, Top=103,;
    Alignment=1, Width=171, Height=15,;
    Caption="Valore del bene:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="TBYXSQIDVM",Visible=.t., Left=315, Top=209,;
    Alignment=1, Width=199, Height=15,;
    Caption="Valore componenti selezionati:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="YBWRIHDHXW",Visible=.t., Left=3, Top=130,;
    Alignment=1, Width=171, Height=15,;
    Caption="Totale accantonato civile:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="SKNRRNPOCA",Visible=.t., Left=300, Top=130,;
    Alignment=1, Width=214, Height=15,;
    Caption="Residuo da ammortamento civile:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="DYUTMOSOTZ",Visible=.t., Left=318, Top=157,;
    Alignment=1, Width=196, Height=15,;
    Caption="Quota accantonamento civile:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="BOUZLLGTBM",Visible=.t., Left=3, Top=235,;
    Alignment=1, Width=171, Height=15,;
    Caption="Valore non ammortizzabile:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="YYEMPVSEVJ",Visible=.t., Left=3, Top=262,;
    Alignment=1, Width=171, Height=15,;
    Caption="Totale accantonato ordinario:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="QWHHBIBSXL",Visible=.t., Left=305, Top=289,;
    Alignment=1, Width=209, Height=15,;
    Caption="Residuo da ammortamento fiscale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="ASJRLLPIXU",Visible=.t., Left=409, Top=361,;
    Alignment=1, Width=105, Height=15,;
    Caption="Quota persa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="IRCUZJSSMD",Visible=.t., Left=6, Top=76,;
    Alignment=0, Width=129, Height=15,;
    Caption="Aspetto civilistico"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_23 as StdString with uid="EVKJCVKKKX",Visible=.t., Left=6, Top=186,;
    Alignment=0, Width=128, Height=15,;
    Caption="Aspetto fiscale"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_26 as StdString with uid="RGNLWBTLDZ",Visible=.t., Left=99, Top=14,;
    Alignment=1, Width=71, Height=15,;
    Caption="Cespite:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="PSDYVVYUPV",Visible=.t., Left=79, Top=340,;
    Alignment=1, Width=95, Height=15,;
    Caption="% DeducibilitÓ:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="YODMDJMJLY",Visible=.t., Left=119, Top=367,;
    Alignment=1, Width=55, Height=15,;
    Caption="Limite:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="VVRYAKZQAH",Visible=.t., Left=3, Top=157,;
    Alignment=1, Width=171, Height=15,;
    Caption="Coefficiente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="PTBZTZIFBY",Visible=.t., Left=90, Top=313,;
    Alignment=1, Width=84, Height=15,;
    Caption="Coefficienti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="XIAQXACGQN",Visible=.t., Left=239, Top=313,;
    Alignment=0, Width=8, Height=15,;
    Caption="+"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="CBNTBCMZJM",Visible=.t., Left=316, Top=313,;
    Alignment=1, Width=198, Height=15,;
    Caption="Quota accantonam.ordinario:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="KTMDPRJZPP",Visible=.t., Left=316, Top=337,;
    Alignment=1, Width=198, Height=15,;
    Caption="Quota accantonam.anticipato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="DIIOHELLUS",Visible=.t., Left=3, Top=289,;
    Alignment=1, Width=171, Height=15,;
    Caption="Totale accantonato anticipato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="AZXVROJAFL",Visible=.t., Left=3, Top=209,;
    Alignment=1, Width=171, Height=15,;
    Caption="Valore del bene:"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="QZCEZKJOSG",Visible=.t., Left=304, Top=103,;
    Alignment=1, Width=210, Height=15,;
    Caption="Valore componenti selezionati:"  ;
  , bGlobalFont=.t.

  add object oBox_1_37 as StdBox with uid="TELJHJHFVL",left=6, top=93, width=636,height=1

  add object oBox_1_38 as StdBox with uid="LOVXRSSZNL",left=6, top=203, width=636,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsce_kdc','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
