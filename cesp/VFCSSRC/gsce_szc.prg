* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_szc                                                        *
*              Visualizza schede cespiti                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30] [VRS_47]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-03-23                                                      *
* Last revis.: 2013-01-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsce_szc",oParentObject))

* --- Class definition
define class tgsce_szc as StdForm
  Top    = 3
  Left   = 1

  * --- Standard Properties
  Width  = 785
  Height = 571
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-01-08"
  HelpContextID=241845609
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=36

  * --- Constant Properties
  _IDX = 0
  AZIENDA_IDX = 0
  CES_PITI_IDX = 0
  ESERCIZI_IDX = 0
  VALUTE_IDX = 0
  UBI_CESP_IDX = 0
  CAN_TIER_IDX = 0
  PAR_CESP_IDX = 0
  cPrg = "gsce_szc"
  cComment = "Visualizza schede cespiti"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODAZI = space(5)
  w_PCTIPAMM = space(1)
  w_CODCES = space(20)
  w_DESCES = space(40)
  w_COMPET = space(4)
  w_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  w_ANNESE = space(4)
  w_VALNAZ = space(3)
  w_VALNAZ = space(3)
  w_DECTOT = 0
  w_CALCPICT = 0
  w_FINESE = ctod('  /  /  ')
  w_STATUS = space(1)
  w_COMMESSA = space(15)
  w_SERIALE = space(10)
  w_NUMRIF = 0
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_DESCRI = space(40)
  w_DESCAU = space(35)
  w_VBESAL = 0
  w_ACUSAL = 0
  w_PMISAL = 0
  w_VBESAL1 = 0
  w_ACUSAL1 = 0
  w_PMISAL1 = 0
  w_DESCAUC = space(35)
  w_VBESAL1C = 0
  w_ACUSAL1C = 0
  w_PMISAL1C = 0
  w_VBESALC = 0
  w_ACUSALC = 0
  w_PMISALC = 0
  w_SERIALEC = space(10)
  w_NUMRIFC = 0
  w_ZoomCesF = .NULL.
  w_ZoomCesC = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsce_szcPag1","gsce_szc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Selezioni")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODCES_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomCesF = this.oPgFrm.Pages(1).oPag.ZoomCesF
    this.w_ZoomCesC = this.oPgFrm.Pages(1).oPag.ZoomCesC
    DoDefault()
    proc Destroy()
      this.w_ZoomCesF = .NULL.
      this.w_ZoomCesC = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[7]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='CES_PITI'
    this.cWorkTables[3]='ESERCIZI'
    this.cWorkTables[4]='VALUTE'
    this.cWorkTables[5]='UBI_CESP'
    this.cWorkTables[6]='CAN_TIER'
    this.cWorkTables[7]='PAR_CESP'
    return(this.OpenAllTables(7))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI=space(5)
      .w_PCTIPAMM=space(1)
      .w_CODCES=space(20)
      .w_DESCES=space(40)
      .w_COMPET=space(4)
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_ANNESE=space(4)
      .w_VALNAZ=space(3)
      .w_VALNAZ=space(3)
      .w_DECTOT=0
      .w_CALCPICT=0
      .w_FINESE=ctod("  /  /  ")
      .w_STATUS=space(1)
      .w_COMMESSA=space(15)
      .w_SERIALE=space(10)
      .w_NUMRIF=0
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_DESCRI=space(40)
      .w_DESCAU=space(35)
      .w_VBESAL=0
      .w_ACUSAL=0
      .w_PMISAL=0
      .w_VBESAL1=0
      .w_ACUSAL1=0
      .w_PMISAL1=0
      .w_DESCAUC=space(35)
      .w_VBESAL1C=0
      .w_ACUSAL1C=0
      .w_PMISAL1C=0
      .w_VBESALC=0
      .w_ACUSALC=0
      .w_PMISALC=0
      .w_SERIALEC=space(10)
      .w_NUMRIFC=0
        .w_CODAZI = i_CODAZI
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODAZI))
          .link_1_1('Full')
        endif
        .DoRTCalc(2,3,.f.)
        if not(empty(.w_CODCES))
          .link_1_3('Full')
        endif
          .DoRTCalc(4,4,.f.)
        .w_COMPET = g_CODESE
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_COMPET))
          .link_1_5('Full')
        endif
          .DoRTCalc(6,7,.f.)
        .w_ANNESE = g_CODESE
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_ANNESE))
          .link_1_8('Full')
        endif
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_VALNAZ))
          .link_1_9('Full')
        endif
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_VALNAZ))
          .link_1_10('Full')
        endif
          .DoRTCalc(11,11,.f.)
        .w_CALCPICT = DEFPIC(.w_DECTOT)
          .DoRTCalc(13,13,.f.)
        .w_STATUS = ''
        .DoRTCalc(15,15,.f.)
        if not(empty(.w_COMMESSA))
          .link_1_15('Full')
        endif
        .w_SERIALE = .w_ZoomCesF.getVar('MCSERIAL')
        .w_NUMRIF = .w_ZoomCesF.getVar('MCNUMREG')
        .w_OBTEST = i_datsys
      .oPgFrm.Page1.oPag.ZoomCesF.Calculate()
      .oPgFrm.Page1.oPag.ZoomCesC.Calculate()
          .DoRTCalc(19,20,.f.)
        .w_DESCAU = .w_ZoomCesF.getVar('CCDESCRI')
      .oPgFrm.Page1.oPag.oObj_1_37.Calculate('   Saldo al '+dtoc( .w_DATINI-1 )+':')
      .oPgFrm.Page1.oPag.oObj_1_41.Calculate('   Saldo al '+dtoc( .w_DATFIN )+':')
          .DoRTCalc(22,27,.f.)
        .w_DESCAUC = .w_ZoomCesC.getVar('CCDESCRI')
      .oPgFrm.Page1.oPag.oObj_1_46.Calculate('   Saldo al '+dtoc( .w_DATINI-1 )+':')
      .oPgFrm.Page1.oPag.oObj_1_53.Calculate('   Saldo al '+dtoc( .w_DATFIN )+':')
          .DoRTCalc(29,34,.f.)
        .w_SERIALEC = .w_ZoomCesC.getVar('MCSERIAL')
        .w_NUMRIFC = .w_ZoomCesC.getVar('MCNUMREG')
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_25.enabled = this.oPgFrm.Page1.oPag.oBtn_1_25.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_26.enabled = this.oPgFrm.Page1.oPag.oBtn_1_26.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_27.enabled = this.oPgFrm.Page1.oPag.oBtn_1_27.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_54.enabled = this.oPgFrm.Page1.oPag.oBtn_1_54.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,7,.t.)
            .w_ANNESE = g_CODESE
          .link_1_8('Full')
          .link_1_9('Full')
          .link_1_10('Full')
        .DoRTCalc(11,15,.t.)
            .w_SERIALE = .w_ZoomCesF.getVar('MCSERIAL')
            .w_NUMRIF = .w_ZoomCesF.getVar('MCNUMREG')
        .oPgFrm.Page1.oPag.ZoomCesF.Calculate()
        .oPgFrm.Page1.oPag.ZoomCesC.Calculate()
        .DoRTCalc(18,20,.t.)
            .w_DESCAU = .w_ZoomCesF.getVar('CCDESCRI')
        .oPgFrm.Page1.oPag.oObj_1_37.Calculate('   Saldo al '+dtoc( .w_DATINI-1 )+':')
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate('   Saldo al '+dtoc( .w_DATFIN )+':')
        .DoRTCalc(22,27,.t.)
            .w_DESCAUC = .w_ZoomCesC.getVar('CCDESCRI')
        .oPgFrm.Page1.oPag.oObj_1_46.Calculate('   Saldo al '+dtoc( .w_DATINI-1 )+':')
        .oPgFrm.Page1.oPag.oObj_1_53.Calculate('   Saldo al '+dtoc( .w_DATFIN )+':')
        .DoRTCalc(29,34,.t.)
            .w_SERIALEC = .w_ZoomCesC.getVar('MCSERIAL')
            .w_NUMRIFC = .w_ZoomCesC.getVar('MCNUMREG')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZoomCesF.Calculate()
        .oPgFrm.Page1.oPag.ZoomCesC.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_37.Calculate('   Saldo al '+dtoc( .w_DATINI-1 )+':')
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate('   Saldo al '+dtoc( .w_DATFIN )+':')
        .oPgFrm.Page1.oPag.oObj_1_46.Calculate('   Saldo al '+dtoc( .w_DATINI-1 )+':')
        .oPgFrm.Page1.oPag.oObj_1_53.Calculate('   Saldo al '+dtoc( .w_DATFIN )+':')
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_25.enabled = this.oPgFrm.Page1.oPag.oBtn_1_25.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_26.enabled = this.oPgFrm.Page1.oPag.oBtn_1_26.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_54.enabled = this.oPgFrm.Page1.oPag.oBtn_1_54.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZoomCesF.Event(cEvent)
      .oPgFrm.Page1.oPag.ZoomCesC.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_37.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_41.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_46.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_53.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_CESP_IDX,3]
    i_lTable = "PAR_CESP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_CESP_IDX,2], .t., this.PAR_CESP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_CESP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PCCODAZI,PCTIPAMM";
                   +" from "+i_cTable+" "+i_lTable+" where PCCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PCCODAZI',this.w_CODAZI)
            select PCCODAZI,PCTIPAMM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.PCCODAZI,space(5))
      this.w_PCTIPAMM = NVL(_Link_.PCTIPAMM,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_PCTIPAMM = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_CESP_IDX,2])+'\'+cp_ToStr(_Link_.PCCODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_CESP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCES
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CES_PITI_IDX,3]
    i_lTable = "CES_PITI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CES_PITI_IDX,2], .t., this.CES_PITI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CES_PITI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCES) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCE_ACE',True,'CES_PITI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CECODICE like "+cp_ToStrODBC(trim(this.w_CODCES)+"%");

          i_ret=cp_SQL(i_nConn,"select CECODICE,CEDESCRI,CEDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CECODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CECODICE',trim(this.w_CODCES))
          select CECODICE,CEDESCRI,CEDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CECODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCES)==trim(_Link_.CECODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CEDESCRI like "+cp_ToStrODBC(trim(this.w_CODCES)+"%");

            i_ret=cp_SQL(i_nConn,"select CECODICE,CEDESCRI,CEDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CEDESCRI like "+cp_ToStr(trim(this.w_CODCES)+"%");

            select CECODICE,CEDESCRI,CEDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODCES) and !this.bDontReportError
            deferred_cp_zoom('CES_PITI','*','CECODICE',cp_AbsName(oSource.parent,'oCODCES_1_3'),i_cWhere,'GSCE_ACE',"Cespiti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CECODICE,CEDESCRI,CEDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CECODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CECODICE',oSource.xKey(1))
            select CECODICE,CEDESCRI,CEDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCES)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CECODICE,CEDESCRI,CEDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CECODICE="+cp_ToStrODBC(this.w_CODCES);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CECODICE',this.w_CODCES)
            select CECODICE,CEDESCRI,CEDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCES = NVL(_Link_.CECODICE,space(20))
      this.w_DESCES = NVL(_Link_.CEDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CEDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODCES = space(20)
      endif
      this.w_DESCES = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice cespite inesistente oppure obsoleto")
        endif
        this.w_CODCES = space(20)
        this.w_DESCES = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CES_PITI_IDX,2])+'\'+cp_ToStr(_Link_.CECODICE,1)
      cp_ShowWarn(i_cKey,this.CES_PITI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCES Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COMPET
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COMPET) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_COMPET)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESFINESE,ESINIESE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_CODAZI;
                     ,'ESCODESE',trim(this.w_COMPET))
          select ESCODAZI,ESCODESE,ESFINESE,ESINIESE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COMPET)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COMPET) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oCOMPET_1_5'),i_cWhere,'',"Esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESFINESE,ESINIESE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESFINESE,ESINIESE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESFINESE,ESINIESE";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESFINESE,ESINIESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COMPET)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESFINESE,ESINIESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_COMPET);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_COMPET)
            select ESCODAZI,ESCODESE,ESFINESE,ESINIESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COMPET = NVL(_Link_.ESCODESE,space(4))
      this.w_DATFIN = NVL(cp_ToDate(_Link_.ESFINESE),ctod("  /  /  "))
      this.w_DATINI = NVL(cp_ToDate(_Link_.ESINIESE),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_COMPET = space(4)
      endif
      this.w_DATFIN = ctod("  /  /  ")
      this.w_DATINI = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COMPET Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ANNESE
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANNESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANNESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ,ESFINESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_ANNESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_ANNESE)
            select ESCODAZI,ESCODESE,ESVALNAZ,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANNESE = NVL(_Link_.ESCODESE,space(4))
      this.w_VALNAZ = NVL(_Link_.ESVALNAZ,space(3))
      this.w_FINESE = NVL(cp_ToDate(_Link_.ESFINESE),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ANNESE = space(4)
      endif
      this.w_VALNAZ = space(3)
      this.w_FINESE = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANNESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VALNAZ
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALNAZ) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALNAZ)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALNAZ);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALNAZ)
            select VACODVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALNAZ = NVL(_Link_.VACODVAL,space(3))
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_VALNAZ = space(3)
      endif
      this.w_DECTOT = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALNAZ Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VALNAZ
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALNAZ) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALNAZ)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALNAZ);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALNAZ)
            select VACODVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALNAZ = NVL(_Link_.VACODVAL,space(3))
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_VALNAZ = space(3)
      endif
      this.w_DECTOT = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALNAZ Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COMMESSA
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COMMESSA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACN',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_COMMESSA)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_COMMESSA))
          select CNCODCAN,CNDESCAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COMMESSA)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStrODBC(trim(this.w_COMMESSA)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStr(trim(this.w_COMMESSA)+"%");

            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_COMMESSA) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCOMMESSA_1_15'),i_cWhere,'GSAR_ACN',"Commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COMMESSA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_COMMESSA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_COMMESSA)
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COMMESSA = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCRI = NVL(_Link_.CNDESCAN,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_COMMESSA = space(15)
      endif
      this.w_DESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COMMESSA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODCES_1_3.value==this.w_CODCES)
      this.oPgFrm.Page1.oPag.oCODCES_1_3.value=this.w_CODCES
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCES_1_4.value==this.w_DESCES)
      this.oPgFrm.Page1.oPag.oDESCES_1_4.value=this.w_DESCES
    endif
    if not(this.oPgFrm.Page1.oPag.oCOMPET_1_5.value==this.w_COMPET)
      this.oPgFrm.Page1.oPag.oCOMPET_1_5.value=this.w_COMPET
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_6.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_6.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_7.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_7.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oVALNAZ_1_9.value==this.w_VALNAZ)
      this.oPgFrm.Page1.oPag.oVALNAZ_1_9.value=this.w_VALNAZ
    endif
    if not(this.oPgFrm.Page1.oPag.oVALNAZ_1_10.value==this.w_VALNAZ)
      this.oPgFrm.Page1.oPag.oVALNAZ_1_10.value=this.w_VALNAZ
    endif
    if not(this.oPgFrm.Page1.oPag.oSTATUS_1_14.RadioValue()==this.w_STATUS)
      this.oPgFrm.Page1.oPag.oSTATUS_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCOMMESSA_1_15.value==this.w_COMMESSA)
      this.oPgFrm.Page1.oPag.oCOMMESSA_1_15.value=this.w_COMMESSA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_29.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_29.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAU_1_32.value==this.w_DESCAU)
      this.oPgFrm.Page1.oPag.oDESCAU_1_32.value=this.w_DESCAU
    endif
    if not(this.oPgFrm.Page1.oPag.oVBESAL_1_34.value==this.w_VBESAL)
      this.oPgFrm.Page1.oPag.oVBESAL_1_34.value=this.w_VBESAL
    endif
    if not(this.oPgFrm.Page1.oPag.oACUSAL_1_35.value==this.w_ACUSAL)
      this.oPgFrm.Page1.oPag.oACUSAL_1_35.value=this.w_ACUSAL
    endif
    if not(this.oPgFrm.Page1.oPag.oPMISAL_1_36.value==this.w_PMISAL)
      this.oPgFrm.Page1.oPag.oPMISAL_1_36.value=this.w_PMISAL
    endif
    if not(this.oPgFrm.Page1.oPag.oVBESAL1_1_38.value==this.w_VBESAL1)
      this.oPgFrm.Page1.oPag.oVBESAL1_1_38.value=this.w_VBESAL1
    endif
    if not(this.oPgFrm.Page1.oPag.oACUSAL1_1_39.value==this.w_ACUSAL1)
      this.oPgFrm.Page1.oPag.oACUSAL1_1_39.value=this.w_ACUSAL1
    endif
    if not(this.oPgFrm.Page1.oPag.oPMISAL1_1_40.value==this.w_PMISAL1)
      this.oPgFrm.Page1.oPag.oPMISAL1_1_40.value=this.w_PMISAL1
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAUC_1_44.value==this.w_DESCAUC)
      this.oPgFrm.Page1.oPag.oDESCAUC_1_44.value=this.w_DESCAUC
    endif
    if not(this.oPgFrm.Page1.oPag.oVBESAL1C_1_47.value==this.w_VBESAL1C)
      this.oPgFrm.Page1.oPag.oVBESAL1C_1_47.value=this.w_VBESAL1C
    endif
    if not(this.oPgFrm.Page1.oPag.oACUSAL1C_1_48.value==this.w_ACUSAL1C)
      this.oPgFrm.Page1.oPag.oACUSAL1C_1_48.value=this.w_ACUSAL1C
    endif
    if not(this.oPgFrm.Page1.oPag.oPMISAL1C_1_49.value==this.w_PMISAL1C)
      this.oPgFrm.Page1.oPag.oPMISAL1C_1_49.value=this.w_PMISAL1C
    endif
    if not(this.oPgFrm.Page1.oPag.oVBESALC_1_50.value==this.w_VBESALC)
      this.oPgFrm.Page1.oPag.oVBESALC_1_50.value=this.w_VBESALC
    endif
    if not(this.oPgFrm.Page1.oPag.oACUSALC_1_51.value==this.w_ACUSALC)
      this.oPgFrm.Page1.oPag.oACUSALC_1_51.value=this.w_ACUSALC
    endif
    if not(this.oPgFrm.Page1.oPag.oPMISALC_1_52.value==this.w_PMISALC)
      this.oPgFrm.Page1.oPag.oPMISALC_1_52.value=this.w_PMISALC
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_CODCES)) or not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODCES_1_3.SetFocus()
            i_bnoObbl = !empty(.w_CODCES)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice cespite inesistente oppure obsoleto")
          case   ((empty(.w_DATINI)) or not(.w_DATINI<=.w_DATFIN OR EMPTY(.w_DATFIN)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATINI_1_6.SetFocus()
            i_bnoObbl = !empty(.w_DATINI)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � pi� grande di quella finale")
          case   ((empty(.w_DATFIN)) or not(.w_DATINI<=.w_DATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFIN_1_7.SetFocus()
            i_bnoObbl = !empty(.w_DATFIN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data finale � anteriore a quella iniziale")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsce_szcPag1 as StdContainer
  Width  = 781
  height = 571
  stdWidth  = 781
  stdheight = 571
  resizeXpos=453
  resizeYpos=216
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODCES_1_3 as StdField with uid="YQGMQJKUIA",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CODCES", cQueryName = "CODCES",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice cespite inesistente oppure obsoleto",;
    ToolTipText = "Cespite selezionato",;
    HelpContextID = 153964070,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=78, Top=7, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="CES_PITI", cZoomOnZoom="GSCE_ACE", oKey_1_1="CECODICE", oKey_1_2="this.w_CODCES"

  func oCODCES_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCES_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCES_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CES_PITI','*','CECODICE',cp_AbsName(this.parent,'oCODCES_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCE_ACE',"Cespiti",'',this.parent.oContained
  endproc
  proc oCODCES_1_3.mZoomOnZoom
    local i_obj
    i_obj=GSCE_ACE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CECODICE=this.parent.oContained.w_CODCES
     i_obj.ecpSave()
  endproc

  add object oDESCES_1_4 as StdField with uid="NTFWKBHZGE",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DESCES", cQueryName = "DESCES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione cespite",;
    HelpContextID = 154022966,;
   bGlobalFont=.t.,;
    Height=21, Width=268, Left=229, Top=7, InputMask=replicate('X',40)

  add object oCOMPET_1_5 as StdField with uid="REZYRBBTNW",rtseq=5,rtrep=.f.,;
    cFormVar = "w_COMPET", cQueryName = "COMPET",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Competenza selezionata",;
    HelpContextID = 171630118,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=588, Top=6, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_COMPET"

  func oCOMPET_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOMPET_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOMPET_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CODAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_CODAZI)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oCOMPET_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Esercizi",'',this.parent.oContained
  endproc

  add object oDATINI_1_6 as StdField with uid="STDEZMCELC",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � pi� grande di quella finale",;
    ToolTipText = "Data registrazione di inizio interrogazione",;
    HelpContextID = 3915722,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=696, Top=6

  func oDATINI_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATINI<=.w_DATFIN OR EMPTY(.w_DATFIN))
    endwith
    return bRes
  endfunc

  add object oDATFIN_1_7 as StdField with uid="GILLVSXLSD",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data finale � anteriore a quella iniziale",;
    ToolTipText = "Data registrazione di fine interrogazione",;
    HelpContextID = 74530870,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=696, Top=29

  func oDATFIN_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATINI<=.w_DATFIN)
    endwith
    return bRes
  endfunc

  add object oVALNAZ_1_9 as StdField with uid="BQGTWZUJOV",rtseq=9,rtrep=.f.,;
    cFormVar = "w_VALNAZ", cQueryName = "VALNAZ",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice valuta dei valori",;
    HelpContextID = 267960662,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=691, Top=253, InputMask=replicate('X',3), cLinkFile="VALUTE", oKey_1_1="VACODVAL", oKey_1_2="this.w_VALNAZ"

  func oVALNAZ_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oVALNAZ_1_10 as StdField with uid="NLSJKTZXBI",rtseq=10,rtrep=.f.,;
    cFormVar = "w_VALNAZ", cQueryName = "VALNAZ",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice valuta dei valori",;
    HelpContextID = 267960662,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=691, Top=492, InputMask=replicate('X',3), cLinkFile="VALUTE", oKey_1_1="VACODVAL", oKey_1_2="this.w_VALNAZ"

  func oVALNAZ_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc


  add object oSTATUS_1_14 as StdCombo with uid="EZCWWKAAJK",value=3,rtseq=14,rtrep=.f.,left=78,top=33,width=86,height=21;
    , ToolTipText = "Status dei movimenti selezionati";
    , HelpContextID = 171844646;
    , cFormVar="w_STATUS",RowSource=""+"Confermato,"+"Provvisorio,"+"Tutto", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSTATUS_1_14.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'P',;
    iif(this.value =3,'',;
    space(1)))))
  endfunc
  func oSTATUS_1_14.GetRadio()
    this.Parent.oContained.w_STATUS = this.RadioValue()
    return .t.
  endfunc

  func oSTATUS_1_14.SetRadio()
    this.Parent.oContained.w_STATUS=trim(this.Parent.oContained.w_STATUS)
    this.value = ;
      iif(this.Parent.oContained.w_STATUS=='C',1,;
      iif(this.Parent.oContained.w_STATUS=='P',2,;
      iif(this.Parent.oContained.w_STATUS=='',3,;
      0)))
  endfunc

  add object oCOMMESSA_1_15 as StdField with uid="FLZYAVYDTD",rtseq=15,rtrep=.f.,;
    cFormVar = "w_COMMESSA", cQueryName = "COMMESSA",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice commessa",;
    HelpContextID = 154656359,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=78, Top=59, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="GSAR_ACN", oKey_1_1="CNCODCAN", oKey_1_2="this.w_COMMESSA"

  func oCOMMESSA_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOMMESSA_1_15.ecpDrop(oSource)
    this.Parent.oContained.link_1_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOMMESSA_1_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCOMMESSA_1_15'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACN',"Commesse",'',this.parent.oContained
  endproc
  proc oCOMMESSA_1_15.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_COMMESSA
     i_obj.ecpSave()
  endproc


  add object oBtn_1_25 as StdButton with uid="LCZIDVPVYS",left=720, top=59, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Esegue la ricerca con le selezioni impostate";
    , HelpContextID = 203512086;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_25.Click()
      with this.Parent.oContained
        do GSCE_BVC with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_25.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_DATINI) AND NOT EMPTY(.w_DATFIN))
      endwith
    endif
  endfunc


  add object oBtn_1_26 as StdButton with uid="FPXATHRNJC",left=10, top=521, width=48,height=45,;
    CpPicture="bmp\mcesp.bmp", caption="", nPag=1;
    , ToolTipText = "Visualizza il movimento cespite associato";
    , HelpContextID = 36268502;
    , caption='\<Mov.cesp.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_26.Click()
      with this.Parent.oContained
        GSCE_BZC(this.Parent.oContained,.w_SERIALE)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_26.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(NVL(.w_SERIALE,'')))
      endwith
    endif
  endfunc


  add object oBtn_1_27 as StdButton with uid="ZQCYXHBPGO",left=720, top=521, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 241816858;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_27.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESCRI_1_29 as StdField with uid="QNIGJCLPTF",rtseq=20,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 117706,;
   bGlobalFont=.t.,;
    Height=21, Width=270, Left=229, Top=59, InputMask=replicate('X',40)


  add object ZoomCesF as cp_zoombox with uid="VFBNMRXEUQ",left=2, top=350, width=770,height=140,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="MOV_CESP",cZoomFile="GSCE_SZF",bOptions=.f.,bQueryOnLoad=.f.,bReadOnly=.f.,;
    cEvent = "EseguiF",;
    nPag=1;
    , HelpContextID = 204587494


  add object ZoomCesC as cp_zoombox with uid="RNBAOIZQMK",left=2, top=112, width=770,height=140,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="MOV_CESP",cZoomFile="GSCE_SZC",bOptions=.f.,bQueryOnLoad=.f.,bReadOnly=.f.,;
    cEvent = "EseguiC",;
    nPag=1;
    , HelpContextID = 204587494

  add object oDESCAU_1_32 as StdField with uid="QKSUOSNRWR",rtseq=21,rtrep=.f.,;
    cFormVar = "w_DESCAU", cQueryName = "DESCAU",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione causale",;
    HelpContextID = 183383094,;
   bGlobalFont=.t.,;
    Height=21, Width=268, Left=138, Top=521, InputMask=replicate('X',35)

  add object oVBESAL_1_34 as StdField with uid="RASJITZRQX",rtseq=22,rtrep=.f.,;
    cFormVar = "w_VBESAL", cQueryName = "VBESAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Saldo del valore del bene fiscale",;
    HelpContextID = 33378902,;
   bGlobalFont=.t.,;
    Height=21, Width=109, Left=347, Top=492, cSayPict="v_PV(38+VVP)", cGetPict="v_GV(38+VVP)"

  add object oACUSAL_1_35 as StdField with uid="OYCDWSBFRU",rtseq=23,rtrep=.f.,;
    cFormVar = "w_ACUSAL", cQueryName = "ACUSAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Saldo di accantonamento-utilizzazione fiscale",;
    HelpContextID = 33444358,;
   bGlobalFont=.t.,;
    Height=21, Width=109, Left=458, Top=492, cSayPict="v_PV(38+VVP)", cGetPict="v_GV(38+VVP)"

  add object oPMISAL_1_36 as StdField with uid="ISFJSZTNHQ",rtseq=24,rtrep=.f.,;
    cFormVar = "w_PMISAL", cQueryName = "PMISAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Saldo dell'importo non ammortiz.",;
    HelpContextID = 33398006,;
   bGlobalFont=.t.,;
    Height=21, Width=109, Left=570, Top=492, cSayPict="v_PV(38+VVP)", cGetPict="v_GV(38+VVP)"


  add object oObj_1_37 as cp_calclbl with uid="NLMVYYZOMO",left=223, top=330, width=121,height=16,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="      Saldo Iniziale:",;
    cEvent = "w_DATINI Changed",;
    nPag=1;
    , HelpContextID = 204587494

  add object oVBESAL1_1_38 as StdField with uid="FOVSNAMVNI",rtseq=25,rtrep=.f.,;
    cFormVar = "w_VBESAL1", cQueryName = "VBESAL1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Saldo iniziale del valore del bene fiscale",;
    HelpContextID = 33378902,;
   bGlobalFont=.t.,;
    Height=21, Width=109, Left=347, Top=328, cSayPict="v_PV(38+VVP)", cGetPict="v_GV(38+VVP)"

  add object oACUSAL1_1_39 as StdField with uid="PZMQDEDSJZ",rtseq=26,rtrep=.f.,;
    cFormVar = "w_ACUSAL1", cQueryName = "ACUSAL1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Saldo iniziale di accantonamento-utilizzazione fiscale",;
    HelpContextID = 33444358,;
   bGlobalFont=.t.,;
    Height=21, Width=109, Left=458, Top=328, cSayPict="v_PV(38+VVP)", cGetPict="v_GV(38+VVP)"

  add object oPMISAL1_1_40 as StdField with uid="CDZEKFYFDX",rtseq=27,rtrep=.f.,;
    cFormVar = "w_PMISAL1", cQueryName = "PMISAL1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Saldo iniziale dell'importo non ammortiz.",;
    HelpContextID = 33398006,;
   bGlobalFont=.t.,;
    Height=21, Width=109, Left=570, Top=328, cSayPict="v_PV(38+VVP)", cGetPict="v_GV(38+VVP)"


  add object oObj_1_41 as cp_calclbl with uid="XVWWVQEBUA",left=223, top=492, width=121,height=16,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption='      Saldo Finale:',;
    cEvent = "w_DATFIN Changed",;
    nPag=1;
    , HelpContextID = 204587494

  add object oDESCAUC_1_44 as StdField with uid="SCGEMIBSAG",rtseq=28,rtrep=.f.,;
    cFormVar = "w_DESCAUC", cQueryName = "DESCAUC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione causale",;
    HelpContextID = 183383094,;
   bGlobalFont=.t.,;
    Height=21, Width=268, Left=138, Top=282, InputMask=replicate('X',35)


  add object oObj_1_46 as cp_calclbl with uid="RLZCNELFIX",left=223, top=92, width=121,height=16,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="      Saldo Iniziale:",;
    cEvent = "w_DATINI Changed",;
    nPag=1;
    , HelpContextID = 204587494

  add object oVBESAL1C_1_47 as StdField with uid="EQAYNMWMYG",rtseq=29,rtrep=.f.,;
    cFormVar = "w_VBESAL1C", cQueryName = "VBESAL1C",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Saldo iniziale valore del bene civile",;
    HelpContextID = 33378969,;
   bGlobalFont=.t.,;
    Height=21, Width=109, Left=347, Top=90, cSayPict="v_PV(38+VVP)", cGetPict="v_GV(38+VVP)"

  add object oACUSAL1C_1_48 as StdField with uid="XGILUXGGTG",rtseq=30,rtrep=.f.,;
    cFormVar = "w_ACUSAL1C", cQueryName = "ACUSAL1C",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Saldo iniziale di accantonamento-utilizzazione civile",;
    HelpContextID = 33444425,;
   bGlobalFont=.t.,;
    Height=21, Width=109, Left=458, Top=90, cSayPict="v_PV(38+VVP)", cGetPict="v_GV(38+VVP)"

  add object oPMISAL1C_1_49 as StdField with uid="FKIFHPVEUY",rtseq=31,rtrep=.f.,;
    cFormVar = "w_PMISAL1C", cQueryName = "PMISAL1C",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Saldo iniziale di plusvalenza-minusvalenza civili",;
    HelpContextID = 33398073,;
   bGlobalFont=.t.,;
    Height=21, Width=109, Left=570, Top=90, cSayPict="v_PV(38+VVP)", cGetPict="v_GV(38+VVP)"

  add object oVBESALC_1_50 as StdField with uid="VNVFQCHVAQ",rtseq=32,rtrep=.f.,;
    cFormVar = "w_VBESALC", cQueryName = "VBESALC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Saldo valore del bene civile",;
    HelpContextID = 33378902,;
   bGlobalFont=.t.,;
    Height=21, Width=109, Left=347, Top=253, cSayPict="v_PV(38+VVP)", cGetPict="v_GV(38+VVP)"

  add object oACUSALC_1_51 as StdField with uid="JLSOZBQSWN",rtseq=33,rtrep=.f.,;
    cFormVar = "w_ACUSALC", cQueryName = "ACUSALC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Saldo di accantonamento-utilizzazione civile",;
    HelpContextID = 33444358,;
   bGlobalFont=.t.,;
    Height=21, Width=109, Left=458, Top=253, cSayPict="v_PV(38+VVP)", cGetPict="v_GV(38+VVP)"

  add object oPMISALC_1_52 as StdField with uid="WJMSBJUOMP",rtseq=34,rtrep=.f.,;
    cFormVar = "w_PMISALC", cQueryName = "PMISALC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Saldo di plusvalenza-minusvalenza civile",;
    HelpContextID = 33398006,;
   bGlobalFont=.t.,;
    Height=21, Width=109, Left=570, Top=253, cSayPict="v_PV(38+VVP)", cGetPict="v_GV(38+VVP)"


  add object oObj_1_53 as cp_calclbl with uid="RCCYAVWLMY",left=223, top=256, width=121,height=16,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption='      Saldo Finale:',;
    cEvent = "w_DATFIN Changed",;
    nPag=1;
    , HelpContextID = 204587494


  add object oBtn_1_54 as StdButton with uid="VXTTYWVMPK",left=10, top=255, width=48,height=45,;
    CpPicture="bmp\mcesp.bmp", caption="", nPag=1;
    , ToolTipText = "Visualizza il movimento cespite associato";
    , HelpContextID = 36268502;
    , caption='\<Mov.cesp.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_54.Click()
      with this.Parent.oContained
        GSCE_BZC(this.Parent.oContained,.w_SERIALEC)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_54.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(NVL(.w_SERIALEC,'')))
      endwith
    endif
  endfunc

  add object oStr_1_16 as StdString with uid="DCIREAPNQV",Visible=.t., Left=643, Top=9,;
    Alignment=1, Width=50, Height=15,;
    Caption="Da:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="CHKICECMSA",Visible=.t., Left=647, Top=31,;
    Alignment=1, Width=46, Height=15,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="FLDCGDBCJO",Visible=.t., Left=5, Top=9,;
    Alignment=1, Width=71, Height=15,;
    Caption="Cespite:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="QDGYMJNQPH",Visible=.t., Left=481, Top=9,;
    Alignment=1, Width=105, Height=15,;
    Caption="Competenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="ZUORRUEPSC",Visible=.t., Left=32, Top=34,;
    Alignment=1, Width=44, Height=15,;
    Caption="Status:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="DLCGVWUAQP",Visible=.t., Left=2, Top=59,;
    Alignment=1, Width=74, Height=18,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="TXTXCFDHPS",Visible=.t., Left=60, Top=521,;
    Alignment=1, Width=76, Height=15,;
    Caption="Causale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="CCHTIRZPMH",Visible=.t., Left=8, Top=97,;
    Alignment=0, Width=87, Height=17,;
    Caption="Aspetto civile"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_43 as StdString with uid="UTQUMTVSLZ",Visible=.t., Left=4, Top=334,;
    Alignment=0, Width=96, Height=19,;
    Caption="Aspetto fiscale"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_45 as StdString with uid="XVGLNHJNVD",Visible=.t., Left=60, Top=283,;
    Alignment=1, Width=76, Height=15,;
    Caption="Causale:"  ;
  , bGlobalFont=.t.

  add object oBox_1_57 as StdBox with uid="IUVZBRXIPT",left=2, top=313, width=770,height=3
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsce_szc','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
