* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_bvm                                                        *
*              Controlli variaz. movim.                                        *
*                                                                              *
*      Author: TAM Software Srl                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_42]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-05-12                                                      *
* Last revis.: 2014-11-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsce_bvm",oParentObject)
return(i_retval)

define class tgsce_bvm as StdBatch
  * --- Local variables
  w_CFUNC = space(20)
  w_FINCOM = ctod("  /  /  ")
  w_MESS = space(10)
  w_COMPRC = space(4)
  w_OK = .f.
  w_CODAZI = space(5)
  w_APPO = space(20)
  w_ESPRIU = space(4)
  w_ESPRIC = space(4)
  w_MSG = space(0)
  w_OKLOG = .f.
  * --- WorkFile variables
  PAR_CESP_idx=0
  CES_PITI_idx=0
  PNT_MAST_idx=0
  CES_AMMO_idx=0
  ESERCIZI_idx=0
  DOC_DETT_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controllo Variazione dei Movimenti Cespiti (da GSCE_AMC)
    this.w_CFUNC = this.oParentObject.cFunction
    this.w_MESS = "Transazione abbandonata"
    this.w_OK = .T.
    this.w_CODAZI = i_CODAZI
    if NOT EMPTY(this.oParentObject.w_MCDTPRIU) AND NOT EMPTY(this.oParentObject.w_MCDTPRIC)
      * --- Devo verificare che entrambe le date appartengano allo stesso esercizio.
      this.w_ESPRIU = CALCESER(this.oParentObject.w_MCDTPRIU,space(4))
      this.w_ESPRIC = CALCESER(this.oParentObject.w_MCDTPRIC,space(4))
      if this.w_ESPRIU<>this.w_ESPRIC
        this.w_MESS = "Le date di primo utilizzo, fiscale e civile, devono appartenere allo stesso esercizio"
        this.w_OK = .F.
      endif
    endif
    if NOT EMPTY(this.oParentObject.w_PNSERIALC)
      * --- Riferimento Cespite
      if this.w_CFUNC="Load" 
        * --- Inserisce Rif.Movimento Cespite
        * --- Write into PNT_MAST
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.PNT_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.PNT_MAST_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PNRIFCES ="+cp_NullLink(cp_ToStrODBC("S"),'PNT_MAST','PNRIFCES');
              +i_ccchkf ;
          +" where ";
              +"PNSERIAL = "+cp_ToStrODBC(this.oParentObject.w_PNSERIALC);
                 )
        else
          update (i_cTable) set;
              PNRIFCES = "S";
              &i_ccchkf. ;
           where;
              PNSERIAL = this.oParentObject.w_PNSERIALC;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      else
        this.w_MESS = "Movimento contabilizzato; impossibile variare/eliminare"
        this.w_OK = .F.
      endif
    endif
    if this.w_OK=.T. AND (this.oParentObject.w_ABILITA OR NOT EMPTY(this.oParentObject.w_DOCSERIAL)) AND this.w_CFUNC="Query"
      * --- Riferimento Cespite var. w_ABILITA (controlla che ci siano reg. di Pnota abbinate manualmente) o DOCSERIAL (Accesso in cancellazione)
      this.w_MESS = "Movimento abbinato a movimento contabile o a documento; impossibile eliminare"
      this.w_OK = .F.
    endif
    if this.w_OK=.T. AND (this.oParentObject.w_ABILITA OR NOT EMPTY(this.oParentObject.w_DOCSERIAL)) AND this.w_CFUNC="Edit"
      * --- Riferimento Cespite var. w_ABILITA o DOCSERIAL (Accesso in variazione)
      this.w_OK = ah_YesNo("Attenzione: movimento abbinato a movimento contabile o a documento%0Confermi le modifiche?")
    endif
    if this.w_OK=.T.
      * --- Read from PAR_CESP
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PAR_CESP_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_CESP_idx,2],.t.,this.PAR_CESP_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "PCDATCON"+;
          " from "+i_cTable+" PAR_CESP where ";
              +"PCCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          PCDATCON;
          from (i_cTable) where;
              PCCODAZI = this.w_CODAZI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_COMPRC = NVL(cp_ToDate(_read_.PCDATCON),cp_NullValue(_read_.PCDATCON))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if NOT EMPTY(this.w_COMPRC)
        * --- Read from ESERCIZI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ESERCIZI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2],.t.,this.ESERCIZI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ESFINESE"+;
            " from "+i_cTable+" ESERCIZI where ";
                +"ESCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
                +" and ESCODESE = "+cp_ToStrODBC(this.w_COMPRC);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ESFINESE;
            from (i_cTable) where;
                ESCODAZI = this.w_CODAZI;
                and ESCODESE = this.w_COMPRC;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_FINCOM = NVL(cp_ToDate(_read_.ESFINESE),cp_NullValue(_read_.ESFINESE))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_FINCOM>=this.oParentObject.w_INIESE
          this.w_MESS = "Esercizio di competenza compreso entro la data di consolidamento"
          this.w_OK = .F.
        endif
      endif
      * --- Nel caso in cui sia stato modificato il cespite nel movimento cespiti Aggiorna il cespite nei documenti associati
      if  this.w_CFUNC="Edit" 
        Select ( this.oparentobject.cCursor )
        if this.oParentObject.w_MCCODCES<>MCCODCES
          * --- Write into DOC_DETT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.DOC_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MVCODCES ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MCCODCES),'DOC_DETT','MVCODCES');
                +i_ccchkf ;
            +" where ";
                +"MVCESSER = "+cp_ToStrODBC(this.oParentObject.w_MCSERIAL);
                   )
          else
            update (i_cTable) set;
                MVCODCES = this.oParentObject.w_MCCODCES;
                &i_ccchkf. ;
             where;
                MVCESSER = this.oParentObject.w_MCSERIAL;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      endif
    endif
    if this.w_OK=.T. AND this.w_CFUNC="Query"
      this.w_APPO = "xxxxxxxxxxxxxxxxxxxx"
      * --- Read from CES_AMMO
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CES_AMMO_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CES_AMMO_idx,2],.t.,this.CES_AMMO_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ACCODCES"+;
          " from "+i_cTable+" CES_AMMO where ";
              +"ACRIFMOV = "+cp_ToStrODBC(this.oParentObject.w_MCSERIAL);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ACCODCES;
          from (i_cTable) where;
              ACRIFMOV = this.oParentObject.w_MCSERIAL;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_APPO = NVL(cp_ToDate(_read_.ACCODCES),cp_NullValue(_read_.ACCODCES))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if i_Rows<>0 AND this.w_APPO=this.oParentObject.w_MCCODCES
        this.w_MESS = "Movimento associato ad un piano di ammortamento%0Proseguo ugualmente?"
        this.w_OK = ah_YesNo(this.w_MESS)
      endif
      if this.w_OK=.T. AND this.oParentObject.w_MCFLPRIU = "="
        * --- Write into CES_PITI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.CES_PITI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CES_PITI_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CES_PITI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CEDTPRIU ="+cp_NullLink(cp_ToStrODBC( cp_CharToDate("  -  -  ")),'CES_PITI','CEDTPRIU');
          +",CEESPRIU ="+cp_NullLink(cp_ToStrODBC( "    "),'CES_PITI','CEESPRIU');
          +",CESTABEN ="+cp_NullLink(cp_ToStrODBC( "N"),'CES_PITI','CESTABEN');
              +i_ccchkf ;
          +" where ";
              +"CECODICE = "+cp_ToStrODBC(this.oParentObject.w_MCCODCES);
                 )
        else
          update (i_cTable) set;
              CEDTPRIU =  cp_CharToDate("  -  -  ");
              ,CEESPRIU =  "    ";
              ,CESTABEN =  "N";
              &i_ccchkf. ;
           where;
              CECODICE = this.oParentObject.w_MCCODCES;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
      if this.w_OK=.T. AND this.oParentObject.w_MCFLPRIC = "="
        * --- Write into CES_PITI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.CES_PITI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CES_PITI_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CES_PITI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CESTABEN ="+cp_NullLink(cp_ToStrODBC( "N"),'CES_PITI','CESTABEN');
          +",CEDTPRIC ="+cp_NullLink(cp_ToStrODBC( cp_CharToDate("  -  -  ")),'CES_PITI','CEDTPRIC');
          +",CEESPRIC ="+cp_NullLink(cp_ToStrODBC( "    "),'CES_PITI','CEESPRIC');
              +i_ccchkf ;
          +" where ";
              +"CECODICE = "+cp_ToStrODBC(this.oParentObject.w_MCCODCES);
                 )
        else
          update (i_cTable) set;
              CESTABEN =  "N";
              ,CEDTPRIC =  cp_CharToDate("  -  -  ");
              ,CEESPRIC =  "    ";
              &i_ccchkf. ;
           where;
              CECODICE = this.oParentObject.w_MCCODCES;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
    endif
    if this.w_OK AND this.oParentObject.w_FLGA01="S" AND this.w_CFUNC="Load"
      * --- Controlli Vendita Cespite
      if this.oParentObject.w_TIPCES $ "PC-PS"
        this.w_MESS = "Vendita cespite pertinenza; ricordarsi di aggiornare anche le pertinenze"
        ah_ErrorMsg(this.w_MESS,,"")
      else
        this.w_OKLOG = .T.
        * --- Select from CES_PITI
        i_nConn=i_TableProp[this.CES_PITI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CES_PITI_idx,2],.t.,this.CES_PITI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" CES_PITI ";
              +" where CECODPER="+cp_ToStrODBC(this.oParentObject.w_MCCODCES)+"";
               ,"_Curs_CES_PITI")
        else
          select * from (i_cTable);
           where CECODPER=this.oParentObject.w_MCCODCES;
            into cursor _Curs_CES_PITI
        endif
        if used('_Curs_CES_PITI')
          select _Curs_CES_PITI
          locate for 1=1
          do while not(eof())
          if this.w_oklog
            this.w_MSG = "Vendita cespite pertinenza; ricordarsi di aggiornare anche le pertinenze"
            this.w_OKLOG = .F.
          endif
          this.w_MSG = this.w_MSG + CHR(13) + _Curs_CES_PITI.CECODICE
            select _Curs_CES_PITI
            continue
          enddo
          use
        endif
        if not empty(this.w_msg)
          do GSCE_KLG with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      endif
    endif
    if this.w_OK=.F.
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=ah_Msgformat(this.w_MESS)
      this.oParentObject.w_BLOCCA = .T.
      i_retcode = 'stop'
      return
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,6)]
    this.cWorkTables[1]='PAR_CESP'
    this.cWorkTables[2]='CES_PITI'
    this.cWorkTables[3]='PNT_MAST'
    this.cWorkTables[4]='CES_AMMO'
    this.cWorkTables[5]='ESERCIZI'
    this.cWorkTables[6]='DOC_DETT'
    return(this.OpenAllTables(6))

  proc CloseCursors()
    if used('_Curs_CES_PITI')
      use in _Curs_CES_PITI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
