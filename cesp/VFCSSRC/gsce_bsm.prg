* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_bsm                                                        *
*              Stampa schede cespiti                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_42]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-04-01                                                      *
* Last revis.: 2014-11-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsce_bsm",oParentObject)
return(i_retval)

define class tgsce_bsm as StdBatch
  * --- Local variables
  * --- WorkFile variables
  AZIENDA_idx=0
  SALDIART_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- (da GSCE_KSM)
    * --- Parametri delle query
    * --- Movimenti Cespiti selezionati
    vq_exec("..\CESP\EXE\QUERY\GSCE_KSM.VQR",this,"mycrs_movces")
    * --- Aggiorna i valori in funzione della valuta di visualizzazione
    WRCURSOR("mycrs_movces")
    * --- E' stato usato SCAN perch� UPDATE ha generato degli errori
    Select Mycrs_movces 
 Go Top
    do case
      case this.oParentObject.w_STAMPA="F"
        Delete from mycrs_movces where mcimpa01=0 and mcimpa02=0 and mcimpa03=0 and mcimpa04=0 and mcimpa05=0; 
 and mcimpa06=0 and mcfiscal=0 and mcimpa10=0 and mcimpa11=0 and mcimpa12=0 and mcimpa13=0; 
 and mcimpa14=0 and mcimpa15=0 and mcimpa16=0 and empty(cp_todate(mcdtpriu))
      case this.oParentObject.w_STAMPA="C"
        Delete from mycrs_movces where mcimpa01=0 and mcimpa23=0 and mcimpa24=0 and mcimpa18=0 and mcimpa22=0; 
 and mcimpa19=0 and mcimpa07=0 and mcimpa08=0 and mcimpa20=0 and mcimpa21=0 and empty(cp_todate(mcdtpric))
    endcase
    SELECT("mycrs_movces")
    SCAN FOR MCCODVAL<>this.oParentObject.W_VALNAZ
    REPLACE ;
    MCIMPA01 WITH cp_ROUND(VALCAM(NVL(MCIMPA01,0), MCCODVAL, this.oParentObject.W_VALNAZ, this.oParentObject.w_DATFIN, 0), this.oParentObject.W_DECTOT), ; 
 MCIMPA02 WITH cp_ROUND(VALCAM(NVL(MCIMPA02,0), MCCODVAL, this.oParentObject.W_VALNAZ, this.oParentObject.w_DATFIN, 0), this.oParentObject.W_DECTOT), ; 
 MCIMPA03 WITH cp_ROUND(VALCAM(NVL(MCIMPA03,0), MCCODVAL, this.oParentObject.W_VALNAZ, this.oParentObject.w_DATFIN, 0), this.oParentObject.W_DECTOT), ; 
 MCIMPA04 WITH cp_ROUND(VALCAM(NVL(MCIMPA04,0), MCCODVAL, this.oParentObject.W_VALNAZ, this.oParentObject.w_DATFIN, 0), this.oParentObject.W_DECTOT), ; 
 MCIMPA05 WITH cp_ROUND(VALCAM(NVL(MCIMPA05,0), MCCODVAL, this.oParentObject.W_VALNAZ, this.oParentObject.w_DATFIN, 0), this.oParentObject.W_DECTOT), ; 
 MCIMPA06 WITH cp_ROUND(VALCAM(NVL(MCIMPA06,0), MCCODVAL, this.oParentObject.W_VALNAZ, this.oParentObject.w_DATFIN, 0), this.oParentObject.W_DECTOT), ; 
 MCIMPA07 WITH cp_ROUND(VALCAM(NVL(MCIMPA07,0), MCCODVAL, this.oParentObject.W_VALNAZ, this.oParentObject.w_DATFIN, 0), this.oParentObject.W_DECTOT), ; 
 MCIMPA08 WITH cp_ROUND(VALCAM(NVL(MCIMPA08,0), MCCODVAL, this.oParentObject.W_VALNAZ, this.oParentObject.w_DATFIN, 0), this.oParentObject.W_DECTOT), ; 
 MCFISCAL WITH cp_ROUND(VALCAM(NVL(MCFISCAL,0), MCCODVAL, this.oParentObject.W_VALNAZ, this.oParentObject.w_DATFIN, 0), this.oParentObject.W_DECTOT), ; 
 MCIMPA11 WITH cp_ROUND(VALCAM(NVL(MCIMPA11,0), MCCODVAL, this.oParentObject.W_VALNAZ, this.oParentObject.w_DATFIN, 0), this.oParentObject.W_DECTOT), ; 
 MCIMPA12 WITH cp_ROUND(VALCAM(NVL(MCIMPA12,0), MCCODVAL, this.oParentObject.W_VALNAZ, this.oParentObject.w_DATFIN, 0), this.oParentObject.W_DECTOT), ; 
 MCIMPA13 WITH cp_ROUND(VALCAM(NVL(MCIMPA13,0), MCCODVAL, this.oParentObject.W_VALNAZ, this.oParentObject.w_DATFIN, 0), this.oParentObject.W_DECTOT), ; 
 MCIMPA14 WITH cp_ROUND(VALCAM(NVL(MCIMPA14,0), MCCODVAL, this.oParentObject.W_VALNAZ, this.oParentObject.w_DATFIN, 0), this.oParentObject.W_DECTOT), ; 
 MCIMPA10 WITH cp_ROUND(VALCAM(NVL(MCIMPA10,0), MCCODVAL, this.oParentObject.W_VALNAZ, this.oParentObject.w_DATFIN, 0), this.oParentObject.W_DECTOT), ; 
 MCIMPA15 WITH cp_ROUND(VALCAM(NVL(MCIMPA15,0), MCCODVAL, this.oParentObject.W_VALNAZ, this.oParentObject.w_DATFIN, 0), this.oParentObject.W_DECTOT), ; 
 MCIMPA16 WITH cp_ROUND(VALCAM(NVL(MCIMPA16,0), MCCODVAL, this.oParentObject.W_VALNAZ, this.oParentObject.w_DATFIN, 0), this.oParentObject.W_DECTOT), ; 
 MCIMPA18 WITH cp_ROUND(VALCAM(NVL(MCIMPA18,0), MCCODVAL, this.oParentObject.W_VALNAZ, this.oParentObject.w_DATFIN, 0), this.oParentObject.W_DECTOT), ; 
 MCIMPA19 WITH cp_ROUND(VALCAM(NVL(MCIMPA19,0), MCCODVAL, this.oParentObject.W_VALNAZ, this.oParentObject.w_DATFIN, 0), this.oParentObject.W_DECTOT), ; 
 MCIMPA20 WITH cp_ROUND(VALCAM(NVL(MCIMPA20,0), MCCODVAL, this.oParentObject.W_VALNAZ, this.oParentObject.w_DATFIN, 0), this.oParentObject.W_DECTOT), ; 
 MCIMPA21 WITH cp_ROUND(VALCAM(NVL(MCIMPA21,0), MCCODVAL, this.oParentObject.W_VALNAZ, this.oParentObject.w_DATFIN, 0), this.oParentObject.W_DECTOT), ; 
 MCIMPA22 WITH cp_ROUND(VALCAM(NVL(MCIMPA22,0), MCCODVAL, this.oParentObject.W_VALNAZ, this.oParentObject.w_DATFIN, 0), this.oParentObject.W_DECTOT), ; 
 MCIMPA23 WITH cp_ROUND(VALCAM(NVL(MCIMPA23,0), MCCODVAL, this.oParentObject.W_VALNAZ, this.oParentObject.w_DATFIN, 0), this.oParentObject.W_DECTOT), ; 
 MCIMPA24 WITH cp_ROUND(VALCAM(NVL(MCIMPA24,0), MCCODVAL, this.oParentObject.W_VALNAZ, this.oParentObject.w_DATFIN, 0), this.oParentObject.W_DECTOT), ; 
 MCCODVAL WITH this.oParentObject.W_VALNAZ
    ENDSCAN
    * --- Saldi Cespiti alla data finale di selezione w_DATFIN
    * --- NON filtra sulla competenza e NON usa i Saldi Cespiti; ricalcola i saldi dai movimenti
    select * from mycrs_movces into cursor mycrs_salces
    * --- Aggiorna i valori in funzione della valuta di visualizzazione
    WRCURSOR("mycrs_salces")
    Select Mycrs_salces 
 Go Top
    do case
      case this.oParentObject.w_STAMPA="F"
        Delete from mycrs_salces where mcimpa01=0 and mcimpa02=0 and mcimpa03=0 and mcimpa04=0 and mcimpa05=0; 
 and mcimpa06=0 and mcfiscal=0 and mcimpa10=0 and mcimpa11=0 and mcimpa12=0 and mcimpa13=0; 
 and mcimpa14=0 and mcimpa15=0 and mcimpa16=0
      case this.oParentObject.w_STAMPA="C"
        Delete from mycrs_salces where mcimpa01=0 and mcimpa23=0 and mcimpa24=0 and mcimpa18=0 and mcimpa22=0; 
 and mcimpa19=0 and mcimpa07=0 and mcimpa08=0 and mcimpa20=0 and mcimpa21=0
    endcase
    * --- E' stato usato SCAN perch� UPDATE ha generato degli errori
    * --- Aggiorna i valori in funzione della valuta di visualizzazione
    SELECT("mycrs_salces")
    SCAN FOR MCCODVAL<>this.oParentObject.W_VALNAZ
    REPLACE ;
    MCIMPA01 WITH cp_ROUND(VALCAM(NVL(MCIMPA01,0), MCCODVAL, this.oParentObject.W_VALNAZ, this.oParentObject.w_DATFIN, 0), this.oParentObject.W_DECTOT), ; 
 MCIMPA02 WITH cp_ROUND(VALCAM(NVL(MCIMPA02,0), MCCODVAL, this.oParentObject.W_VALNAZ, this.oParentObject.w_DATFIN, 0), this.oParentObject.W_DECTOT), ; 
 MCIMPA03 WITH cp_ROUND(VALCAM(NVL(MCIMPA03,0), MCCODVAL, this.oParentObject.W_VALNAZ, this.oParentObject.w_DATFIN, 0), this.oParentObject.W_DECTOT), ; 
 MCIMPA04 WITH cp_ROUND(VALCAM(NVL(MCIMPA04,0), MCCODVAL, this.oParentObject.W_VALNAZ, this.oParentObject.w_DATFIN, 0), this.oParentObject.W_DECTOT), ; 
 MCIMPA05 WITH cp_ROUND(VALCAM(NVL(MCIMPA05,0), MCCODVAL, this.oParentObject.W_VALNAZ, this.oParentObject.w_DATFIN, 0), this.oParentObject.W_DECTOT), ; 
 MCIMPA06 WITH cp_ROUND(VALCAM(NVL(MCIMPA06,0), MCCODVAL, this.oParentObject.W_VALNAZ, this.oParentObject.w_DATFIN, 0), this.oParentObject.W_DECTOT), ; 
 MCIMPA07 WITH cp_ROUND(VALCAM(NVL(MCIMPA07,0), MCCODVAL, this.oParentObject.W_VALNAZ, this.oParentObject.w_DATFIN, 0), this.oParentObject.W_DECTOT), ; 
 MCIMPA08 WITH cp_ROUND(VALCAM(NVL(MCIMPA08,0), MCCODVAL, this.oParentObject.W_VALNAZ, this.oParentObject.w_DATFIN, 0), this.oParentObject.W_DECTOT), ; 
 MCFISCAL WITH cp_ROUND(VALCAM(NVL(MCFISCAL,0), MCCODVAL, this.oParentObject.W_VALNAZ, this.oParentObject.w_DATFIN, 0), this.oParentObject.W_DECTOT), ; 
 MCIMPA11 WITH cp_ROUND(VALCAM(NVL(MCIMPA11,0), MCCODVAL, this.oParentObject.W_VALNAZ, this.oParentObject.w_DATFIN, 0), this.oParentObject.W_DECTOT), ; 
 MCIMPA12 WITH cp_ROUND(VALCAM(NVL(MCIMPA12,0), MCCODVAL, this.oParentObject.W_VALNAZ, this.oParentObject.w_DATFIN, 0), this.oParentObject.W_DECTOT), ; 
 MCIMPA13 WITH cp_ROUND(VALCAM(NVL(MCIMPA13,0), MCCODVAL, this.oParentObject.W_VALNAZ, this.oParentObject.w_DATFIN, 0), this.oParentObject.W_DECTOT), ; 
 MCIMPA14 WITH cp_ROUND(VALCAM(NVL(MCIMPA14,0), MCCODVAL, this.oParentObject.W_VALNAZ, this.oParentObject.w_DATFIN, 0), this.oParentObject.W_DECTOT), ; 
 MCIMPA10 WITH cp_ROUND(VALCAM(NVL(MCIMPA10,0), MCCODVAL, this.oParentObject.W_VALNAZ, this.oParentObject.w_DATFIN, 0), this.oParentObject.W_DECTOT), ; 
 MCIMPA15 WITH cp_ROUND(VALCAM(NVL(MCIMPA15,0), MCCODVAL, this.oParentObject.W_VALNAZ, this.oParentObject.w_DATFIN, 0), this.oParentObject.W_DECTOT), ; 
 MCIMPA16 WITH cp_ROUND(VALCAM(NVL(MCIMPA16,0), MCCODVAL, this.oParentObject.W_VALNAZ, this.oParentObject.w_DATFIN, 0), this.oParentObject.W_DECTOT), ; 
 MCIMPA18 WITH cp_ROUND(VALCAM(NVL(MCIMPA18,0), MCCODVAL, this.oParentObject.W_VALNAZ, this.oParentObject.w_DATFIN, 0), this.oParentObject.W_DECTOT), ; 
 MCIMPA19 WITH cp_ROUND(VALCAM(NVL(MCIMPA19,0), MCCODVAL, this.oParentObject.W_VALNAZ, this.oParentObject.w_DATFIN, 0), this.oParentObject.W_DECTOT), ; 
 MCIMPA20 WITH cp_ROUND(VALCAM(NVL(MCIMPA20,0), MCCODVAL, this.oParentObject.W_VALNAZ, this.oParentObject.w_DATFIN, 0), this.oParentObject.W_DECTOT), ; 
 MCIMPA21 WITH cp_ROUND(VALCAM(NVL(MCIMPA21,0), MCCODVAL, this.oParentObject.W_VALNAZ, this.oParentObject.w_DATFIN, 0), this.oParentObject.W_DECTOT), ; 
 MCIMPA22 WITH cp_ROUND(VALCAM(NVL(MCIMPA22,0), MCCODVAL, this.oParentObject.W_VALNAZ, this.oParentObject.w_DATFIN, 0), this.oParentObject.W_DECTOT), ; 
 MCIMPA23 WITH cp_ROUND(VALCAM(NVL(MCIMPA23,0), MCCODVAL, this.oParentObject.W_VALNAZ, this.oParentObject.w_DATFIN, 0), this.oParentObject.W_DECTOT), ; 
 MCIMPA24 WITH cp_ROUND(VALCAM(NVL(MCIMPA24,0), MCCODVAL, this.oParentObject.W_VALNAZ, this.oParentObject.w_DATFIN, 0), this.oParentObject.W_DECTOT), ; 
 MCCODVAL WITH this.oParentObject.W_VALNAZ
    ENDSCAN
    * --- Raggruppo il cursore Mycrs_Salces per codice Cespite
    Select Max(Cecodcat) as Cecodcat,Max(CCdescri) as CCDescri,Cecodice,Max(Cedescri) as Cedescri,; 
 Max(Mcnumreg) as Mcnumreg,Max(Mcdatreg) as Mcdatreg,Max(Mccompet) as Mccompet,; 
 Max(Mccodese) as Mccodese,Max(Mccodcau) as Mccodcau,Sum(Mcimpa01) as Mcimpa01,; 
 Sum(Mcimpa02) as Mcimpa02,Sum(Mcimpa07) as Mcimpa07,; 
 Sum(Mcfiscal) as Mcfiscal,Sum(Mcimpa11) as Mcimpa11,Sum(Mcimpa13) as Mcimpa13,; 
 Max(Mccodval) as Mccodval,Max(Mcnumdoc) as Mcnumdoc,; 
 Max(Mcalfdoc) as Mcalfdoc,Max(Mcdatdoc) as Mcdatdoc,Max(Mcstatus) as Mcstatus,; 
 Max(Cecodubi) as Cecodubi,Max(Cecodmat) as Cecodmat,Sum(Mcimpa10) as Mcimpa10,; 
 Sum(Mcimpa15) as Mcimpa15,Sum(Mcimpa03) as Mcimpa03,Sum(Mcimpa04) as Mcimpa04,; 
 Sum(Mcimpa05) as Mcimpa05,Sum(Mcimpa06) as Mcimpa06,; 
 Sum(Mcimpa08) as Mcimpa08,Sum(Mcimpa12) as Mcimpa12,Sum(Mcimpa14) as Mcimpa14,Sum(Mcimpa16) as Mcimpa16,; 
 Sum(Mcqtaces) as Mcqtaces,Max(Cetipces) as Cetipces,Max(Ceunimis) as Ceunimis,Sum(Mcimpa18) as Mcimpa18,; 
 Sum(Mcimpa19) as Mcimpa19,Sum(Mcimpa20) as Mcimpa20,Sum(Mcimpa21) as Mcimpa21,Sum(Mcimpa22) as Mcimpa22,; 
 Sum(Mcimpa23) as Mcimpa23,Sum(Mcimpa24) as Mcimpa24,Max(Mcdtpriu) as Mcdtpriu,Max(Mcdtpric) as Mcdtpric, min(Mcserial) as Mcserial, Max(Vasimval) as Vasimval, Max(Mctipcon) as Mctipcon, Max(Mccodcon) as Mccodcon,Max(Andescri) as Andescri ; 
 from mycrs_salces into cursor Salces group by Cecodice order by cecodice 
    * --- Unisce i movimenti ed i saldi per il report
    select *,"M" as MOVSAL FROM mycrs_movces ;
    UNION select Max(CECODCAT) as Cecodcat, Max(CCDESCRI) as Ccdescri,CECODICE, Max(CEDESCRI) as Cedescri,Max(MCNUMREG) as Mcnumreg, ;
     Max(MCDATREG) as Mcdatreg, Max(MCCOMPET) as Mccompet, Max(MCCODESE) as Mccodese,;
    Max(MCCODCAU) as Mccodcau, SUM(MCIMPA01) as Mcimpa01, SUM(MCIMPA02) as Mcimpa02, ;
    Sum(Mcimpa07) as Mcimpa07,Sum(Mcfiscal) as Mcfiscal,Sum(Mcimpa11) as Mcimpa11,;
    Sum(Mcimpa13) as Mcimpa13,MCCODVAL, MCNUMDOC, MCALFDOC, MCDATDOC, ;
    MCSTATUS, CECODUBI, CECODMAT,Sum(MCIMPA10) as Mcimpa10, Sum(Mcimpa15) as Mcimpa15,;
    Sum(Mcimpa03) as Mcimpa03,Sum(Mcimpa04) as Mcimpa04,Sum(Mcimpa05) as Mcimpa05, Sum(Mcimpa06) as Mcimpa06,;
    Sum(Mcimpa08) as Mcimpa08,Sum(Mcimpa12) as Mcimpa12,Sum(Mcimpa14) as Mcimpa14,Sum(Mcimpa16) as Mcimpa16,;
    Sum(Mcqtaces) as Mcqtaces,Max(Cetipces) as Cetipces,Max(Ceunimis) as Ceunimis,Sum(Mcimpa18) as Mcimpa18,;
    Sum(Mcimpa19) as Mcimpa19,Sum(Mcimpa20) as Mcimpa20,Sum(Mcimpa21) as Mcimpa21,Sum(Mcimpa22) as Mcimpa22,;
    Sum(Mcimpa23) as Mcimpa23,Sum(Mcimpa24) as Mcimpa24,Max(Mcdtpriu) as Mcdtpriu,Max(Mcdtpric) as Mcdtpric, min(Mcserial) as Mcserial ,Max(Vasimval) as Vasimval, Max(Mctipcon) as Mctipcon, Max(Mccodcon) as Mccodcon,Max(Andescri) as Andescri,"S" as MOVSAL ;
    FROM Salces group by CECODICE;
    ORDER BY 1,3,MOVSAL,6,5 INTO CURSOR __TMP__
    if used("mycrs_movces")
      select ("mycrs_movces")
      use
    endif
    if used("mycrs_salces")
      select ("mycrs_salces")
      use
    endif
    if used("Salces")
      select ("Salces")
      use
    endif
    * --- Parametri della query passati al report
    l_codce1 = this.oParentObject.w_CODCE1
    l_codce2 = this.oParentObject.w_CODCE2
    l_codcat = this.oParentObject.w_CODCAT
    l_codfam = this.oParentObject.w_CODFAM
    l_compet = this.oParentObject.w_COMPET
    l_datini = this.oParentObject.w_DATINI
    l_datfin = this.oParentObject.w_DATFIN
    l_status = this.oParentObject.w_STATUS
    l_dectot = this.oParentObject.w_DECTOT
    l_commessa = this.oParentObject.w_commessa
    l_codubi = this.oParentObject.w_codubi
    select ("__TMP__")
    do case
      case this.oParentObject.w_STAMPA="S"
        CP_CHPRN("..\CESP\EXE\QUERY\GSCE_KSM.FRX","",this.oParentObject)
      case this.oParentObject.w_STAMPA="C"
        CP_CHPRN("..\CESP\EXE\QUERY\GSCE1KSM.FRX","",this.oParentObject)
      case this.oParentObject.w_STAMPA="F"
        CP_CHPRN("..\CESP\EXE\QUERY\GSCE2KSM.FRX","",this.oParentObject)
    endcase
    if used("__TMP__")
      select ("__TMP__")
      use
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='SALDIART'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
