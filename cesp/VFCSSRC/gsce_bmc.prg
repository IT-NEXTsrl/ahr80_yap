* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_bmc                                                        *
*              Aggiornam.vendita.cespite                                       *
*                                                                              *
*      Author: Zucchetti TAM Srl                                               *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_101]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-05-12                                                      *
* Last revis.: 2000-07-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_param
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsce_bmc",oParentObject,m.w_param)
return(i_retval)

define class tgsce_bmc as StdBatch
  * --- Local variables
  w_param = space(3)
  w_CFUNC = space(20)
  w_SCCOMPON = space(40)
  w_SCROWNUM = 0
  w_DATADIS = ctod("  /  /  ")
  w_CODICE = space(20)
  w_COMPON = space(40)
  w_SERIAL = space(10)
  w_DATDISMI = ctod("  /  /  ")
  w_DISMISSI = ctod("  /  /  ")
  w_DATACONT = ctod("  /  /  ")
  w_MESS = space(50)
  w_QTASAL = 0
  * --- WorkFile variables
  CES_PITI_idx=0
  COM_PUBI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controllo Variazione dei Movimenti Cespiti (da GSCE_AMC)
    * --- Controllo che il batch GSCE_BVM non restituisca un'errore.
    if bTrsErr
      i_retcode = 'stop'
      return
    endif
    do case
      case this.w_param="AMC" OR this.w_param="BRS"
        * --- Chiamato da GSCE_AMC
        this.w_CFUNC = this.oParentObject.cFunction
        this.w_DATACONT = Cp_Todate(this.oParentObject.w_Mcdatdis)
        if this.w_CFUNC="Query"
          * --- Sono in cancellazione
          if (this.oParentObject.w_FLGA01="S" OR NOT EMPTY(this.w_DATACONT)) AND (this.oParentObject.w_TIPCES="CS" OR this.oParentObject.w_TIPCES="PS" OR this.oParentObject.w_TIPCES="CQ")
            * --- Se causale di vendita e cespite singolo
            * --- Aggiorno la data di dismissione nell'anagrafica cespiti
            this.w_DATDISMI = cp_CharToDate("  -  -  ")
            * --- Write into CES_PITI
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.CES_PITI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CES_PITI_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.CES_PITI_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"CESTABEN ="+cp_NullLink(cp_ToStrODBC("U"),'CES_PITI','CESTABEN');
              +",CEDATDIS ="+cp_NullLink(cp_ToStrODBC(this.w_DATDISMI),'CES_PITI','CEDATDIS');
                  +i_ccchkf ;
              +" where ";
                  +"CECODICE = "+cp_ToStrODBC(this.oParentObject.w_MCCODCES);
                     )
            else
              update (i_cTable) set;
                  CESTABEN = "U";
                  ,CEDATDIS = this.w_DATDISMI;
                  &i_ccchkf. ;
               where;
                  CECODICE = this.oParentObject.w_MCCODCES;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            * --- Aggiorno Cespite in Uso
          endif
          if (this.oParentObject.w_FLGA01="S" OR NOT EMPTY(this.w_DATACONT)) AND (this.oParentObject.w_TIPCES="CC" Or this.oParentObject.w_Tipces="PC")
            * --- La scrittura sull'anagrafica dei cespite viene effettuata quando tutti i componenti sono dismessi
            * --- Se causale di vendita e cespite composto
            this.w_SERIAL = this.oParentObject.w_MCSERIAL
            * --- Aggiorno la data di dismissione nell'anagrafica cespiti
            this.w_DATDISMI = cp_CharToDate("  -  -  ")
            * --- Write into CES_PITI
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.CES_PITI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CES_PITI_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.CES_PITI_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"CESTABEN ="+cp_NullLink(cp_ToStrODBC("U"),'CES_PITI','CESTABEN');
              +",CEDATDIS ="+cp_NullLink(cp_ToStrODBC(this.w_DATDISMI),'CES_PITI','CEDATDIS');
                  +i_ccchkf ;
              +" where ";
                  +"CECODICE = "+cp_ToStrODBC(this.oParentObject.w_MCCODCES);
                     )
            else
              update (i_cTable) set;
                  CESTABEN = "U";
                  ,CEDATDIS = this.w_DATDISMI;
                  &i_ccchkf. ;
               where;
                  CECODICE = this.oParentObject.w_MCCODCES;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            * --- Aggiorno Cespite in Uso
            * --- Devo annullare anche le date di dismissione dei componenti
            VQ_EXEC("..\CESP\EXE\QUERY\GSCE2BMC.VQR",this,"CompDis")
            SELECT CompDis
            GO TOP
            SCAN
            this.w_DATADIS = cp_CharToDate("  -  -  ")
            this.w_COMPON = NVL(SCCOMPON,"")
            * --- Write into COM_PUBI
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.COM_PUBI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.COM_PUBI_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.COM_PUBI_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"CUDATVEN ="+cp_NullLink(cp_ToStrODBC(this.w_DATADIS),'COM_PUBI','CUDATVEN');
                  +i_ccchkf ;
              +" where ";
                  +"CUCODICE = "+cp_ToStrODBC(this.oParentObject.w_MCCODCES);
                  +" and CUCOMPON = "+cp_ToStrODBC(this.w_COMPON);
                     )
            else
              update (i_cTable) set;
                  CUDATVEN = this.w_DATADIS;
                  &i_ccchkf. ;
               where;
                  CUCODICE = this.oParentObject.w_MCCODCES;
                  and CUCOMPON = this.w_COMPON;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            ENDSCAN
            if USED("CompDis")
              SELECT CompDis
              USE
            endif
          endif
        else
          if  (this.oParentObject.w_TIPCES="CS" OR this.oParentObject.w_TIPCES="PS" OR this.oParentObject.w_TIPCES="CQ")
            * --- Controllo se nel movimento � valorizzata la data di dismissione 
            if this.oParentObject.w_FLDADI="S" 
              this.w_DISMISSI = iif(Not Empty(this.w_Datacont),this.w_Datacont,this.oParentObject.w_Mcdatreg)
              * --- Write into CES_PITI
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.CES_PITI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.CES_PITI_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.CES_PITI_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"CESTABEN ="+cp_NullLink(cp_ToStrODBC("C"),'CES_PITI','CESTABEN');
                +",CEDATDIS ="+cp_NullLink(cp_ToStrODBC(this.w_DISMISSI),'CES_PITI','CEDATDIS');
                    +i_ccchkf ;
                +" where ";
                    +"CECODICE = "+cp_ToStrODBC(this.oParentObject.w_MCCODCES);
                       )
              else
                update (i_cTable) set;
                    CESTABEN = "C";
                    ,CEDATDIS = this.w_DISMISSI;
                    &i_ccchkf. ;
                 where;
                    CECODICE = this.oParentObject.w_MCCODCES;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
              * --- Aggiorno Cespite Ceduto
            endif
          endif
          if  (this.oParentObject.w_TIPCES="CC" or this.oParentObject.w_Tipces="PC")
            * --- Controllo se nel movimento � valorizzata la data di dismissione 
            if this.oParentObject.w_FLDADI="S" 
              this.w_DISMISSI = iif(Not Empty(this.w_Datacont),this.w_Datacont,this.oParentObject.w_Mcdatreg)
              * --- Se vengo da saldi cespiti controllo il cursore altrimenti la variabile nella maschera
              VQ_EXEC("..\CESP\EXE\QUERY\GSCE_BMC.VQR",this,"CursCompVen")
              if this.w_param="AMC"
                if this.oParentObject.w_Allrow Or Not Empty(this.w_DATACONT)
                  * --- Write into CES_PITI
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.CES_PITI_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.CES_PITI_idx,2])
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.CES_PITI_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"CESTABEN ="+cp_NullLink(cp_ToStrODBC("C"),'CES_PITI','CESTABEN');
                    +",CEDATDIS ="+cp_NullLink(cp_ToStrODBC(this.w_DISMISSI),'CES_PITI','CEDATDIS');
                        +i_ccchkf ;
                    +" where ";
                        +"CECODICE = "+cp_ToStrODBC(this.oParentObject.w_MCCODCES);
                           )
                  else
                    update (i_cTable) set;
                        CESTABEN = "C";
                        ,CEDATDIS = this.w_DISMISSI;
                        &i_ccchkf. ;
                     where;
                        CECODICE = this.oParentObject.w_MCCODCES;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error=MSG_WRITE_ERROR
                    return
                  endif
                  * --- Aggiorno Cespite Ceduto
                endif
                if this.oParentObject.w_FLGA01<>"S" AND RECCOUNT("CursCompVen")>0
                  * --- Se il movimento non � di vendita non posso accedere alla maschera scarico componenti 
                  *     e aggiornare la data di dismissione dei componenti.
                  *     Pertanto devono essere aggiornate adesso.
                  SELECT CursCompVen
                  GO TOP
                  SCAN
                  this.w_COMPON = NVL(CUCOMPON,"")
                  * --- Write into COM_PUBI
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.COM_PUBI_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.COM_PUBI_idx,2])
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.COM_PUBI_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"CUDATVEN ="+cp_NullLink(cp_ToStrODBC(this.w_DISMISSI),'COM_PUBI','CUDATVEN');
                        +i_ccchkf ;
                    +" where ";
                        +"CUCODICE = "+cp_ToStrODBC(this.oParentObject.w_MCCODCES);
                        +" and CUCOMPON = "+cp_ToStrODBC(this.w_COMPON);
                           )
                  else
                    update (i_cTable) set;
                        CUDATVEN = this.w_DISMISSI;
                        &i_ccchkf. ;
                     where;
                        CUCODICE = this.oParentObject.w_MCCODCES;
                        and CUCOMPON = this.w_COMPON;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error=MSG_WRITE_ERROR
                    return
                  endif
                  ENDSCAN
                endif
              else
                if RECCOUNT("CursCompVen")=0
                  * --- Write into CES_PITI
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.CES_PITI_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.CES_PITI_idx,2])
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.CES_PITI_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"CESTABEN ="+cp_NullLink(cp_ToStrODBC("C"),'CES_PITI','CESTABEN');
                    +",CEDATDIS ="+cp_NullLink(cp_ToStrODBC(this.w_DISMISSI),'CES_PITI','CEDATDIS');
                        +i_ccchkf ;
                    +" where ";
                        +"CECODICE = "+cp_ToStrODBC(this.oParentObject.w_MCCODCES);
                           )
                  else
                    update (i_cTable) set;
                        CESTABEN = "C";
                        ,CEDATDIS = this.w_DISMISSI;
                        &i_ccchkf. ;
                     where;
                        CECODICE = this.oParentObject.w_MCCODCES;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error=MSG_WRITE_ERROR
                    return
                  endif
                  * --- Aggiorno Cespite Ceduto
                endif
              endif
              if USED("CursCompVen")
                SELECT CursCompVen
                USE
              endif
            endif
          endif
        endif
      case this.w_param="MMS"
        * --- Chiamato da GSCE_MMS
        SELECT t_SCCOMPON,t_SCROWNUM FROM (this.oParentObject.cTrsName) ;
        INTO CURSOR CursSelez WHERE t_SCFLSELE=0 AND EMPTY(t_DTVEND)
        if RECCOUNT("CursSelez")=0
          this.oParentObject.oParentObject.w_ALLROW=.T.
        else
          this.oParentObject.oParentObject.w_ALLROW=.F.
        endif
        if USED("CursSelez")
          SELECT CursSelez
          USE
        endif
      case this.w_param="QTA"
        if this.oParentObject.w_TIPCES ="CQ"
          VQ_EXEC("..\CESP\EXE\QUERY\GSCE_VQS.VQR",this,"VerificaQTA")
          this.w_QTASAL = 0
          if RECCOUNT("VerificaQTA")>0
            SELECT VerificaQTA
            this.w_QTASAL = NVL(SAQTACES,0)
          endif
          if USED("VerificaQTA")
            SELECT VerificaQTA
            USE
          endif
          if this.w_QTASAL < 0
            this.w_MESS = ah_Msgformat("Quantit� cespite negativa")
            * --- transaction error
            bTrsErr=.t.
            i_TrsMsg=this.w_MESS
            i_retcode = 'stop'
            return
          endif
        endif
    endcase
  endproc


  proc Init(oParentObject,w_param)
    this.w_param=w_param
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='CES_PITI'
    this.cWorkTables[2]='COM_PUBI'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_param"
endproc
