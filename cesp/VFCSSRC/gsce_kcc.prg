* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_kcc                                                        *
*              Stampa causali cespiti                                          *
*                                                                              *
*      Author: TAM Software srl                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-03-18                                                      *
* Last revis.: 2007-07-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsce_kcc",oParentObject))

* --- Class definition
define class tgsce_kcc as StdForm
  Top    = 50
  Left   = 100

  * --- Standard Properties
  Width  = 504
  Height = 204
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-07-18"
  HelpContextID=99239273
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=27

  * --- Constant Properties
  _IDX = 0
  CAT_CESP_IDX = 0
  GRU_COCE_IDX = 0
  CES_PITI_IDX = 0
  CAU_CESP_IDX = 0
  PAR_CESP_IDX = 0
  cPrg = "gsce_kcc"
  cComment = "Stampa causali cespiti"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CODAZI = space(10)
  w_CO1 = space(30)
  w_CO2 = space(30)
  w_CO3 = space(30)
  w_CO4 = space(30)
  w_CO5 = space(30)
  w_CO6 = space(30)
  w_CO7 = space(30)
  w_CO8 = space(30)
  w_CO9 = space(30)
  w_CO0 = space(30)
  w_CO11 = space(30)
  w_CO12 = space(30)
  w_CO13A = space(30)
  w_CO14 = space(30)
  w_CO15 = space(30)
  w_CO16 = space(30)
  w_CO13 = space(25)
  w_CO46 = space(25)
  w_CO79 = space(25)
  w_ALLCOX = space(25)
  w_SELINI = space(5)
  w_DESINI = space(40)
  w_SELFIN = space(5)
  w_DESFIN = space(40)
  w_DATSTAM = ctod('  /  /  ')
  w_OBSODAT1 = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsce_kccPag1","gsce_kcc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSELINI_1_26
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='CAT_CESP'
    this.cWorkTables[2]='GRU_COCE'
    this.cWorkTables[3]='CES_PITI'
    this.cWorkTables[4]='CAU_CESP'
    this.cWorkTables[5]='PAR_CESP'
    return(this.OpenAllTables(5))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      vx_exec(""+alltrim(THIS.w_OQRY)+", "+alltrim(THIS.w_orep)+"",this)
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI=space(10)
      .w_CO1=space(30)
      .w_CO2=space(30)
      .w_CO3=space(30)
      .w_CO4=space(30)
      .w_CO5=space(30)
      .w_CO6=space(30)
      .w_CO7=space(30)
      .w_CO8=space(30)
      .w_CO9=space(30)
      .w_CO0=space(30)
      .w_CO11=space(30)
      .w_CO12=space(30)
      .w_CO13A=space(30)
      .w_CO14=space(30)
      .w_CO15=space(30)
      .w_CO16=space(30)
      .w_CO13=space(25)
      .w_CO46=space(25)
      .w_CO79=space(25)
      .w_ALLCOX=space(25)
      .w_SELINI=space(5)
      .w_DESINI=space(40)
      .w_SELFIN=space(5)
      .w_DESFIN=space(40)
      .w_DATSTAM=ctod("  /  /  ")
      .w_OBSODAT1=space(1)
        .w_CODAZI = i_CODAZI
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODAZI))
          .link_1_1('Full')
        endif
          .DoRTCalc(2,17,.f.)
        .w_CO13 = left(alltrim(.w_CO1)+space(30),30)+ left(alltrim(.w_CO2)+space(30),30)+ left(alltrim(.w_CO3)+space(30),30)+left(alltrim(.w_CO4)+space(30),30)+ left(alltrim(.w_CO5)+space(30),30)+ left(alltrim(.w_CO6)+space(30),30)
        .w_CO46 = left(alltrim(.w_CO7)+space(30),30)+ left(alltrim(.w_CO8)+space(30),30)+ left(alltrim(.w_CO9)+space(30),30)+left(alltrim(.w_CO0)+space(30),30)+ left(alltrim(.w_CO11)+space(30),30)+ left(alltrim(.w_CO12)+space(30),30)
        .w_CO79 = left(alltrim(.w_CO13A)+space(30),30)+ left(alltrim(.w_CO14)+space(30),30)+ left(alltrim(.w_CO15)+space(30),30)+left(alltrim(.w_CO16)+space(30),30)
        .w_ALLCOX = .w_CO13+ .w_CO46+ .w_CO79
        .DoRTCalc(22,22,.f.)
        if not(empty(.w_SELINI))
          .link_1_26('Full')
        endif
        .DoRTCalc(23,24,.f.)
        if not(empty(.w_SELFIN))
          .link_1_28('Full')
        endif
          .DoRTCalc(25,25,.f.)
        .w_DATSTAM = i_datsys
        .w_OBSODAT1 = 'N'
      .oPgFrm.Page1.oPag.oObj_1_32.Calculate()
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_33.enabled = this.oPgFrm.Page1.oPag.oBtn_1_33.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_34.enabled = this.oPgFrm.Page1.oPag.oBtn_1_34.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsce_kcc
        PUBLIC TRADCOX
        TRADCOX=THIS.W_ALLCOX
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_1('Full')
        .oPgFrm.Page1.oPag.oObj_1_32.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(2,27,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_32.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_33.enabled = this.oPgFrm.Page1.oPag.oBtn_1_33.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_32.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_CESP_IDX,3]
    i_lTable = "PAR_CESP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_CESP_IDX,2], .t., this.PAR_CESP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_CESP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PCCODAZI,PCCONC01,PCCONC02,PCCONC03,PCCONC04,PCCONC05,PCCONC06,PCCONC07,PCCONC08,PCCONC09,PCCONC10,PCCONC11,PCCONC12,PCCONC13,PCCONC14,PCCONC15,PCCONC16";
                   +" from "+i_cTable+" "+i_lTable+" where PCCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PCCODAZI',this.w_CODAZI)
            select PCCODAZI,PCCONC01,PCCONC02,PCCONC03,PCCONC04,PCCONC05,PCCONC06,PCCONC07,PCCONC08,PCCONC09,PCCONC10,PCCONC11,PCCONC12,PCCONC13,PCCONC14,PCCONC15,PCCONC16;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.PCCODAZI,space(10))
      this.w_CO1 = NVL(_Link_.PCCONC01,space(30))
      this.w_CO2 = NVL(_Link_.PCCONC02,space(30))
      this.w_CO3 = NVL(_Link_.PCCONC03,space(30))
      this.w_CO4 = NVL(_Link_.PCCONC04,space(30))
      this.w_CO5 = NVL(_Link_.PCCONC05,space(30))
      this.w_CO6 = NVL(_Link_.PCCONC06,space(30))
      this.w_CO7 = NVL(_Link_.PCCONC07,space(30))
      this.w_CO8 = NVL(_Link_.PCCONC08,space(30))
      this.w_CO9 = NVL(_Link_.PCCONC09,space(30))
      this.w_CO0 = NVL(_Link_.PCCONC10,space(30))
      this.w_CO11 = NVL(_Link_.PCCONC11,space(30))
      this.w_CO12 = NVL(_Link_.PCCONC12,space(30))
      this.w_CO13A = NVL(_Link_.PCCONC13,space(30))
      this.w_CO14 = NVL(_Link_.PCCONC14,space(30))
      this.w_CO15 = NVL(_Link_.PCCONC15,space(30))
      this.w_CO16 = NVL(_Link_.PCCONC16,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(10)
      endif
      this.w_CO1 = space(30)
      this.w_CO2 = space(30)
      this.w_CO3 = space(30)
      this.w_CO4 = space(30)
      this.w_CO5 = space(30)
      this.w_CO6 = space(30)
      this.w_CO7 = space(30)
      this.w_CO8 = space(30)
      this.w_CO9 = space(30)
      this.w_CO0 = space(30)
      this.w_CO11 = space(30)
      this.w_CO12 = space(30)
      this.w_CO13A = space(30)
      this.w_CO14 = space(30)
      this.w_CO15 = space(30)
      this.w_CO16 = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_CESP_IDX,2])+'\'+cp_ToStr(_Link_.PCCODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_CESP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SELINI
  func Link_1_26(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CESP_IDX,3]
    i_lTable = "CAU_CESP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CESP_IDX,2], .t., this.CAU_CESP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CESP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SELINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCE_ACC',True,'CAU_CESP')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_SELINI)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_SELINI))
          select CCCODICE,CCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SELINI)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStrODBC(trim(this.w_SELINI)+"%");

            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStr(trim(this.w_SELINI)+"%");

            select CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_SELINI) and !this.bDontReportError
            deferred_cp_zoom('CAU_CESP','*','CCCODICE',cp_AbsName(oSource.parent,'oSELINI_1_26'),i_cWhere,'GSCE_ACC',"Causali cespiti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SELINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_SELINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_SELINI)
            select CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SELINI = NVL(_Link_.CCCODICE,space(5))
      this.w_DESINI = NVL(_Link_.CCDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_SELINI = space(5)
      endif
      this.w_DESINI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_SELINI<=.w_SELFIN or empty(.w_SELFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale iniziale maggiore di quella finale")
        endif
        this.w_SELINI = space(5)
        this.w_DESINI = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CESP_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CESP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SELINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SELFIN
  func Link_1_28(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CESP_IDX,3]
    i_lTable = "CAU_CESP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CESP_IDX,2], .t., this.CAU_CESP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CESP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SELFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCE_ACC',True,'CAU_CESP')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_SELFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_SELFIN))
          select CCCODICE,CCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SELFIN)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStrODBC(trim(this.w_SELFIN)+"%");

            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStr(trim(this.w_SELFIN)+"%");

            select CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_SELFIN) and !this.bDontReportError
            deferred_cp_zoom('CAU_CESP','*','CCCODICE',cp_AbsName(oSource.parent,'oSELFIN_1_28'),i_cWhere,'GSCE_ACC',"Causali cespiti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SELFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_SELFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_SELFIN)
            select CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SELFIN = NVL(_Link_.CCCODICE,space(5))
      this.w_DESFIN = NVL(_Link_.CCDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_SELFIN = space(5)
      endif
      this.w_DESFIN = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_SELINI<=.w_SELFIN or empty(.w_SELINI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale iniziale maggiore di quella finale")
        endif
        this.w_SELFIN = space(5)
        this.w_DESFIN = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CESP_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CESP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SELFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSELINI_1_26.value==this.w_SELINI)
      this.oPgFrm.Page1.oPag.oSELINI_1_26.value=this.w_SELINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESINI_1_27.value==this.w_DESINI)
      this.oPgFrm.Page1.oPag.oDESINI_1_27.value=this.w_DESINI
    endif
    if not(this.oPgFrm.Page1.oPag.oSELFIN_1_28.value==this.w_SELFIN)
      this.oPgFrm.Page1.oPag.oSELFIN_1_28.value=this.w_SELFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFIN_1_29.value==this.w_DESFIN)
      this.oPgFrm.Page1.oPag.oDESFIN_1_29.value=this.w_DESFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDATSTAM_1_30.value==this.w_DATSTAM)
      this.oPgFrm.Page1.oPag.oDATSTAM_1_30.value=this.w_DATSTAM
    endif
    if not(this.oPgFrm.Page1.oPag.oOBSODAT1_1_31.RadioValue()==this.w_OBSODAT1)
      this.oPgFrm.Page1.oPag.oOBSODAT1_1_31.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_SELINI<=.w_SELFIN or empty(.w_SELFIN))  and not(empty(.w_SELINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSELINI_1_26.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale iniziale maggiore di quella finale")
          case   not(.w_SELINI<=.w_SELFIN or empty(.w_SELINI))  and not(empty(.w_SELFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSELFIN_1_28.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale iniziale maggiore di quella finale")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsce_kccPag1 as StdContainer
  Width  = 500
  height = 204
  stdWidth  = 500
  stdheight = 204
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSELINI_1_26 as StdField with uid="DKSOHNOPIK",rtseq=22,rtrep=.f.,;
    cFormVar = "w_SELINI", cQueryName = "SELINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale iniziale maggiore di quella finale",;
    HelpContextID = 129776346,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=110, Top=15, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CESP", cZoomOnZoom="GSCE_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_SELINI"

  func oSELINI_1_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_26('Part',this)
    endwith
    return bRes
  endfunc

  proc oSELINI_1_26.ecpDrop(oSource)
    this.Parent.oContained.link_1_26('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSELINI_1_26.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CESP','*','CCCODICE',cp_AbsName(this.parent,'oSELINI_1_26'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCE_ACC',"Causali cespiti",'',this.parent.oContained
  endproc
  proc oSELINI_1_26.mZoomOnZoom
    local i_obj
    i_obj=GSCE_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_SELINI
     i_obj.ecpSave()
  endproc

  add object oDESINI_1_27 as StdField with uid="LUTMJUKGZP",rtseq=23,rtrep=.f.,;
    cFormVar = "w_DESINI", cQueryName = "DESINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 129747914,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=175, Top=15, InputMask=replicate('X',40)

  add object oSELFIN_1_28 as StdField with uid="KUOZWQWSRW",rtseq=24,rtrep=.f.,;
    cFormVar = "w_SELFIN", cQueryName = "SELFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale iniziale maggiore di quella finale",;
    HelpContextID = 51329754,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=110, Top=40, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CESP", cZoomOnZoom="GSCE_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_SELFIN"

  func oSELFIN_1_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_28('Part',this)
    endwith
    return bRes
  endfunc

  proc oSELFIN_1_28.ecpDrop(oSource)
    this.Parent.oContained.link_1_28('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSELFIN_1_28.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CESP','*','CCCODICE',cp_AbsName(this.parent,'oSELFIN_1_28'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCE_ACC',"Causali cespiti",'',this.parent.oContained
  endproc
  proc oSELFIN_1_28.mZoomOnZoom
    local i_obj
    i_obj=GSCE_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_SELFIN
     i_obj.ecpSave()
  endproc

  add object oDESFIN_1_29 as StdField with uid="RVUBAJNZZQ",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DESFIN", cQueryName = "DESFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 51301322,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=175, Top=40, InputMask=replicate('X',40)

  add object oDATSTAM_1_30 as StdField with uid="BMUSQOPOAP",rtseq=26,rtrep=.f.,;
    cFormVar = "w_DATSTAM", cQueryName = "DATSTAM",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di riferimento sul controllo dell'obsolescenza",;
    HelpContextID = 257015754,;
   bGlobalFont=.t.,;
    Height=21, Width=75, Left=110, Top=81

  add object oOBSODAT1_1_31 as StdCheck with uid="HHVZKYDKCE",rtseq=27,rtrep=.f.,left=204, top=81, caption="Stampa obsoleti",;
    ToolTipText = "Stampa solo obsoleti alla data di stampa",;
    HelpContextID = 262812183,;
    cFormVar="w_OBSODAT1", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oOBSODAT1_1_31.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oOBSODAT1_1_31.GetRadio()
    this.Parent.oContained.w_OBSODAT1 = this.RadioValue()
    return .t.
  endfunc

  func oOBSODAT1_1_31.SetRadio()
    this.Parent.oContained.w_OBSODAT1=trim(this.Parent.oContained.w_OBSODAT1)
    this.value = ;
      iif(this.Parent.oContained.w_OBSODAT1=='S',1,;
      0)
  endfunc


  add object oObj_1_32 as cp_outputCombo with uid="ZLERXIVPWT",left=110, top=124, width=378,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 78758374


  add object oBtn_1_33 as StdButton with uid="ZMIZZNTUDJ",left=394, top=154, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per stampare";
    , HelpContextID = 44084202;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_33.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_33.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY) and not empty(.w_datstam))
      endwith
    endif
  endfunc


  add object oBtn_1_34 as StdButton with uid="JWIBHYOYEX",left=446, top=154, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 91921850;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_34.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_22 as StdString with uid="BMWZAUQDQW",Visible=.t., Left=16, Top=81,;
    Alignment=1, Width=87, Height=15,;
    Caption="Data di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="EAAWDYYSRX",Visible=.t., Left=6, Top=124,;
    Alignment=1, Width=97, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="ZYPZJLVODJ",Visible=.t., Left=27, Top=15,;
    Alignment=1, Width=76, Height=15,;
    Caption="Da causale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="IIQLCOBLAD",Visible=.t., Left=36, Top=40,;
    Alignment=1, Width=67, Height=15,;
    Caption="A causale:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsce_kcc','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
