* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_boo                                                        *
*              Ricalcolo zoom                                                  *
*                                                                              *
*      Author: Zucchetti TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_9]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-12-22                                                      *
* Last revis.: 2012-08-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_SEGNO
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsce_boo",oParentObject,m.w_SEGNO)
return(i_retval)

define class tgsce_boo as StdBatch
  * --- Local variables
  w_SEGNO = space(1)
  w_PANUMREG = space(6)
  w_PACODESE = space(4)
  w_PASTATO = space(1)
  w_PADATREG = ctod("  /  /  ")
  w_PADESCRI = space(40)
  w_PANUMFRA = 0
  w_PACODCAT = space(15)
  w_PAFLDEFI = space(1)
  w_PATIPGEN = space(1)
  w_PADATINI = ctod("  /  /  ")
  w_PADATFIN = ctod("  /  /  ")
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Chiudo la Maschera - utilizzato in GSCE_KGA,GSCE_SPA
    if this.w_SEGNO="M"
      this.w_PACODCAT = this.oParentObject.w_CODCAT
      this.w_PACODESE = this.oParentObject.w_CODESE
      this.w_PADATREG = this.oParentObject.w_DATREG
      this.w_PADESCRI = this.oParentObject.w_DESCRI
      this.w_PAFLDEFI = this.oParentObject.w_FLDEFI
      this.w_PANUMFRA = this.oParentObject.w_NUMFRA
      this.w_PANUMREG = this.oParentObject.w_NUMREG
      this.w_PASTATO = this.oParentObject.w_STATO
      this.w_PATIPGEN = this.oParentObject.w_TIPGEN
      this.w_PADATINI = this.oParentObject.w_DATINI
      this.w_PADATFIN = this.oParentObject.w_DATFIN
      do GSCE_KG1 with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.oParentObject.w_CODCAT = this.w_PACODCAT
      this.oParentObject.w_CODESE = this.w_PACODESE
      this.oParentObject.w_DATREG = Cp_Todate(this.w_PADATREG)
      this.oParentObject.w_DESCRI = this.w_PADESCRI
      this.oParentObject.w_FLDEFI = this.w_PAFLDEFI
      this.oParentObject.w_NUMFRA = this.w_PANUMFRA
      this.oParentObject.w_NUMREG = this.w_PANUMREG
      this.oParentObject.w_STATO = this.w_PASTATO
      this.oParentObject.w_TIPGEN = this.w_PATIPGEN
      this.oParentObject.w_DATINI = Cp_Todate(this.w_PADATINI)
      this.oParentObject.w_DATFIN = Cp_Todate(this.w_PADATFIN)
    else
      this.oparentObject.ecpsave()
      i_retcode = 'stop'
      return
    endif
  endproc


  proc Init(oParentObject,w_SEGNO)
    this.w_SEGNO=w_SEGNO
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_SEGNO"
endproc
