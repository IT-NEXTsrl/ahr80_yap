* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_kdm                                                        *
*              Disabbina cespiti                                               *
*                                                                              *
*      Author: Zucchetti TAM S.r.l.                                            *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30] [VRS_99]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-04-13                                                      *
* Last revis.: 2009-12-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsce_kdm",oParentObject))

* --- Class definition
define class tgsce_kdm as StdForm
  Top    = 13
  Left   = 16

  * --- Standard Properties
  Width  = 751
  Height = 432
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-12-11"
  HelpContextID=185973399
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=19

  * --- Constant Properties
  _IDX = 0
  CAU_CESP_IDX = 0
  CES_PITI_IDX = 0
  AZIENDA_IDX = 0
  ESERCIZI_IDX = 0
  cPrg = "gsce_kdm"
  cComment = "Disabbina cespiti"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODAZI = space(5)
  w_OBTEST = ctod('  /  /  ')
  w_DANUMREG = 0
  o_DANUMREG = 0
  w_AANUMREG = 0
  w_DACODCES = space(20)
  o_DACODCES = space(20)
  w_AACODCES = space(20)
  w_ESER = space(4)
  w_CODCAU = space(5)
  w_DADATA = ctod('  /  /  ')
  o_DADATA = ctod('  /  /  ')
  w_AADATA = ctod('  /  /  ')
  w_DTOBSO = ctod('  /  /  ')
  w_OK = .F.
  w_SELEZI = space(1)
  w_FLSELE = 0
  w_MCSERIAL = space(10)
  w_DESCAU = space(40)
  w_DTBSOCA = ctod('  /  /  ')
  w_DACESDES = space(40)
  w_AADESCES = space(40)
  w_ZoomGepr = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsce_kdmPag1","gsce_kdm",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDANUMREG_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomGepr = this.oPgFrm.Pages(1).oPag.ZoomGepr
    DoDefault()
    proc Destroy()
      this.w_ZoomGepr = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='CAU_CESP'
    this.cWorkTables[2]='CES_PITI'
    this.cWorkTables[3]='AZIENDA'
    this.cWorkTables[4]='ESERCIZI'
    return(this.OpenAllTables(4))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI=space(5)
      .w_OBTEST=ctod("  /  /  ")
      .w_DANUMREG=0
      .w_AANUMREG=0
      .w_DACODCES=space(20)
      .w_AACODCES=space(20)
      .w_ESER=space(4)
      .w_CODCAU=space(5)
      .w_DADATA=ctod("  /  /  ")
      .w_AADATA=ctod("  /  /  ")
      .w_DTOBSO=ctod("  /  /  ")
      .w_OK=.f.
      .w_SELEZI=space(1)
      .w_FLSELE=0
      .w_MCSERIAL=space(10)
      .w_DESCAU=space(40)
      .w_DTBSOCA=ctod("  /  /  ")
      .w_DACESDES=space(40)
      .w_AADESCES=space(40)
        .w_CODAZI = I_CODAZI
        .w_OBTEST = i_datsys
          .DoRTCalc(3,3,.f.)
        .w_AANUMREG = iif(EMPTY(.w_AANUMREG),.w_DANUMREG,.w_AANUMREG)
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_DACODCES))
          .link_1_5('Full')
        endif
        .w_AACODCES = iif(EMPTY(.w_AACODCES),.w_DACODCES,.w_AACODCES)
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_AACODCES))
          .link_1_6('Full')
        endif
        .w_ESER = g_codese
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_ESER))
          .link_1_7('Full')
        endif
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_CODCAU))
          .link_1_8('Full')
        endif
          .DoRTCalc(9,9,.f.)
        .w_AADATA = iif(EMPTY(.w_AADATA),.w_DADATA,.w_AADATA)
          .DoRTCalc(11,11,.f.)
        .w_OK = .F.
        .w_SELEZI = 'D'
      .oPgFrm.Page1.oPag.ZoomGepr.Calculate(.F.)
      .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
        .w_FLSELE = 0
        .w_MCSERIAL = Nvl( .w_ZoomGepr.getVar('MCSERIAL') , Space(10))
    endwith
    this.DoRTCalc(16,19,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_27.enabled = this.oPgFrm.Page1.oPag.oBtn_1_27.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_29.enabled = this.oPgFrm.Page1.oPag.oBtn_1_29.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_30.enabled = this.oPgFrm.Page1.oPag.oBtn_1_30.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,3,.t.)
        if .o_DANUMREG<>.w_DANUMREG
            .w_AANUMREG = iif(EMPTY(.w_AANUMREG),.w_DANUMREG,.w_AANUMREG)
        endif
        .DoRTCalc(5,5,.t.)
        if .o_DACODCES<>.w_DACODCES
            .w_AACODCES = iif(EMPTY(.w_AACODCES),.w_DACODCES,.w_AACODCES)
          .link_1_6('Full')
        endif
        .DoRTCalc(7,9,.t.)
        if .o_DADATA<>.w_DADATA
            .w_AADATA = iif(EMPTY(.w_AADATA),.w_DADATA,.w_AADATA)
        endif
        .oPgFrm.Page1.oPag.ZoomGepr.Calculate(.F.)
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
        .DoRTCalc(11,14,.t.)
            .w_MCSERIAL = Nvl( .w_ZoomGepr.getVar('MCSERIAL') , Space(10))
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(16,19,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZoomGepr.Calculate(.F.)
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZoomGepr.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_20.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=DACODCES
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CES_PITI_IDX,3]
    i_lTable = "CES_PITI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CES_PITI_IDX,2], .t., this.CES_PITI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CES_PITI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DACODCES) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCE_ACE',True,'CES_PITI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CECODICE like "+cp_ToStrODBC(trim(this.w_DACODCES)+"%");

          i_ret=cp_SQL(i_nConn,"select CECODICE,CEDESCRI,CEDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CECODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CECODICE',trim(this.w_DACODCES))
          select CECODICE,CEDESCRI,CEDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CECODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DACODCES)==trim(_Link_.CECODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CEDESCRI like "+cp_ToStrODBC(trim(this.w_DACODCES)+"%");

            i_ret=cp_SQL(i_nConn,"select CECODICE,CEDESCRI,CEDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CEDESCRI like "+cp_ToStr(trim(this.w_DACODCES)+"%");

            select CECODICE,CEDESCRI,CEDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_DACODCES) and !this.bDontReportError
            deferred_cp_zoom('CES_PITI','*','CECODICE',cp_AbsName(oSource.parent,'oDACODCES_1_5'),i_cWhere,'GSCE_ACE',"Elenco cespiti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CECODICE,CEDESCRI,CEDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CECODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CECODICE',oSource.xKey(1))
            select CECODICE,CEDESCRI,CEDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DACODCES)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CECODICE,CEDESCRI,CEDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CECODICE="+cp_ToStrODBC(this.w_DACODCES);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CECODICE',this.w_DACODCES)
            select CECODICE,CEDESCRI,CEDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DACODCES = NVL(_Link_.CECODICE,space(20))
      this.w_DACESDES = NVL(_Link_.CEDESCRI,space(40))
      this.w_DTOBSO = NVL(cp_ToDate(_Link_.CEDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_DACODCES = space(20)
      endif
      this.w_DACESDES = space(40)
      this.w_DTOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_DTOBSO>.w_OBTEST OR EMPTY(.w_DTOBSO)) AND (EMPTY(.w_AACODCES) OR .w_AACODCES>=.w_DACODCES)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Cespite inesistente o obsoleto o cespite arrivo < cespite partenza")
        endif
        this.w_DACODCES = space(20)
        this.w_DACESDES = space(40)
        this.w_DTOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CES_PITI_IDX,2])+'\'+cp_ToStr(_Link_.CECODICE,1)
      cp_ShowWarn(i_cKey,this.CES_PITI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DACODCES Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=AACODCES
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CES_PITI_IDX,3]
    i_lTable = "CES_PITI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CES_PITI_IDX,2], .t., this.CES_PITI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CES_PITI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AACODCES) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCE_ACE',True,'CES_PITI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CECODICE like "+cp_ToStrODBC(trim(this.w_AACODCES)+"%");

          i_ret=cp_SQL(i_nConn,"select CECODICE,CEDESCRI,CEDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CECODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CECODICE',trim(this.w_AACODCES))
          select CECODICE,CEDESCRI,CEDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CECODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_AACODCES)==trim(_Link_.CECODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CEDESCRI like "+cp_ToStrODBC(trim(this.w_AACODCES)+"%");

            i_ret=cp_SQL(i_nConn,"select CECODICE,CEDESCRI,CEDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CEDESCRI like "+cp_ToStr(trim(this.w_AACODCES)+"%");

            select CECODICE,CEDESCRI,CEDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_AACODCES) and !this.bDontReportError
            deferred_cp_zoom('CES_PITI','*','CECODICE',cp_AbsName(oSource.parent,'oAACODCES_1_6'),i_cWhere,'GSCE_ACE',"Elenco cespiti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CECODICE,CEDESCRI,CEDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CECODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CECODICE',oSource.xKey(1))
            select CECODICE,CEDESCRI,CEDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AACODCES)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CECODICE,CEDESCRI,CEDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CECODICE="+cp_ToStrODBC(this.w_AACODCES);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CECODICE',this.w_AACODCES)
            select CECODICE,CEDESCRI,CEDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AACODCES = NVL(_Link_.CECODICE,space(20))
      this.w_AADESCES = NVL(_Link_.CEDESCRI,space(40))
      this.w_DTOBSO = NVL(cp_ToDate(_Link_.CEDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_AACODCES = space(20)
      endif
      this.w_AADESCES = space(40)
      this.w_DTOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_DTOBSO>.w_OBTEST OR EMPTY(.w_DTOBSO)) AND (EMPTY(.w_AACODCES) OR .w_AACODCES>=.w_DACODCES)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Cespite inesistente o obsoleto o cespite arrivo < cespite partenza")
        endif
        this.w_AACODCES = space(20)
        this.w_AADESCES = space(40)
        this.w_DTOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CES_PITI_IDX,2])+'\'+cp_ToStr(_Link_.CECODICE,1)
      cp_ShowWarn(i_cKey,this.CES_PITI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AACODCES Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ESER
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ESER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_ESER)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_CODAZI;
                     ,'ESCODESE',trim(this.w_ESER))
          select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ESER)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ESER) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oESER_1_7'),i_cWhere,'',"Esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ESER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_ESER);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_ESER)
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ESER = NVL(_Link_.ESCODESE,space(4))
      this.w_DADATA = NVL(cp_ToDate(_Link_.ESINIESE),ctod("  /  /  "))
      this.w_AADATA = NVL(cp_ToDate(_Link_.ESFINESE),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ESER = space(4)
      endif
      this.w_DADATA = ctod("  /  /  ")
      this.w_AADATA = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ESER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCAU
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CESP_IDX,3]
    i_lTable = "CAU_CESP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CESP_IDX,2], .t., this.CAU_CESP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CESP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCAU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCE_ACC',True,'CAU_CESP')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_CODCAU)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_CODCAU))
          select CCCODICE,CCDESCRI,CCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCAU)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStrODBC(trim(this.w_CODCAU)+"%");

            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStr(trim(this.w_CODCAU)+"%");

            select CCCODICE,CCDESCRI,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODCAU) and !this.bDontReportError
            deferred_cp_zoom('CAU_CESP','*','CCCODICE',cp_AbsName(oSource.parent,'oCODCAU_1_8'),i_cWhere,'GSCE_ACC',"Causali cespiti",'GSCE_KAM.CAU_CESP_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCAU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_CODCAU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_CODCAU)
            select CCCODICE,CCDESCRI,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCAU = NVL(_Link_.CCCODICE,space(5))
      this.w_DESCAU = NVL(_Link_.CCDESCRI,space(40))
      this.w_DTBSOCA = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODCAU = space(5)
      endif
      this.w_DESCAU = space(40)
      this.w_DTBSOCA = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DTBSOCA>.w_OBTEST OR EMPTY(.w_DTBSOCA)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale cespite inesistente o obsoleta")
        endif
        this.w_CODCAU = space(5)
        this.w_DESCAU = space(40)
        this.w_DTBSOCA = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CESP_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CESP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCAU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDANUMREG_1_3.value==this.w_DANUMREG)
      this.oPgFrm.Page1.oPag.oDANUMREG_1_3.value=this.w_DANUMREG
    endif
    if not(this.oPgFrm.Page1.oPag.oAANUMREG_1_4.value==this.w_AANUMREG)
      this.oPgFrm.Page1.oPag.oAANUMREG_1_4.value=this.w_AANUMREG
    endif
    if not(this.oPgFrm.Page1.oPag.oDACODCES_1_5.value==this.w_DACODCES)
      this.oPgFrm.Page1.oPag.oDACODCES_1_5.value=this.w_DACODCES
    endif
    if not(this.oPgFrm.Page1.oPag.oAACODCES_1_6.value==this.w_AACODCES)
      this.oPgFrm.Page1.oPag.oAACODCES_1_6.value=this.w_AACODCES
    endif
    if not(this.oPgFrm.Page1.oPag.oESER_1_7.value==this.w_ESER)
      this.oPgFrm.Page1.oPag.oESER_1_7.value=this.w_ESER
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCAU_1_8.value==this.w_CODCAU)
      this.oPgFrm.Page1.oPag.oCODCAU_1_8.value=this.w_CODCAU
    endif
    if not(this.oPgFrm.Page1.oPag.oDADATA_1_9.value==this.w_DADATA)
      this.oPgFrm.Page1.oPag.oDADATA_1_9.value=this.w_DADATA
    endif
    if not(this.oPgFrm.Page1.oPag.oAADATA_1_10.value==this.w_AADATA)
      this.oPgFrm.Page1.oPag.oAADATA_1_10.value=this.w_AADATA
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEZI_1_14.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page1.oPag.oSELEZI_1_14.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(EMPTY(.w_AANUMREG) OR .w_DANUMREG<=.w_AANUMREG)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDANUMREG_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Numero registrazione di partenza < di quello di arrivo")
          case   not(EMPTY(.w_DANUMREG) OR .w_DANUMREG<=.w_AANUMREG)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAANUMREG_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Numero registrazione di arrivo < di quello di partenza")
          case   not((.w_DTOBSO>.w_OBTEST OR EMPTY(.w_DTOBSO)) AND (EMPTY(.w_AACODCES) OR .w_AACODCES>=.w_DACODCES))  and not(empty(.w_DACODCES))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDACODCES_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Cespite inesistente o obsoleto o cespite arrivo < cespite partenza")
          case   not((.w_DTOBSO>.w_OBTEST OR EMPTY(.w_DTOBSO)) AND (EMPTY(.w_AACODCES) OR .w_AACODCES>=.w_DACODCES))  and not(empty(.w_AACODCES))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAACODCES_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Cespite inesistente o obsoleto o cespite arrivo < cespite partenza")
          case   not(.w_DTBSOCA>.w_OBTEST OR EMPTY(.w_DTBSOCA))  and not(empty(.w_CODCAU))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODCAU_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale cespite inesistente o obsoleta")
          case   not(EMPTY(.w_AADATA) OR .w_DADATA<=.w_AADATA)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDADATA_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data di inizio selezione > di data fine")
          case   not(EMPTY(.w_DADATA) OR .w_DADATA<=.w_AADATA)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAADATA_1_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data di fine selezione < di data inizio")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DANUMREG = this.w_DANUMREG
    this.o_DACODCES = this.w_DACODCES
    this.o_DADATA = this.w_DADATA
    return

enddefine

* --- Define pages as container
define class tgsce_kdmPag1 as StdContainer
  Width  = 747
  height = 432
  stdWidth  = 747
  stdheight = 432
  resizeXpos=406
  resizeYpos=232
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDANUMREG_1_3 as StdField with uid="NJMTGCSVBO",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DANUMREG", cQueryName = "DANUMREG",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Numero registrazione di partenza < di quello di arrivo",;
    ToolTipText = "Da numero registrazione",;
    HelpContextID = 230694787,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=94, Top=28, cSayPict='"999999"', cGetPict='"999999"'

  func oDANUMREG_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (EMPTY(.w_AANUMREG) OR .w_DANUMREG<=.w_AANUMREG)
    endwith
    return bRes
  endfunc

  add object oAANUMREG_1_4 as StdField with uid="JGOMXLUDOO",rtseq=4,rtrep=.f.,;
    cFormVar = "w_AANUMREG", cQueryName = "AANUMREG",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Numero registrazione di arrivo < di quello di partenza",;
    ToolTipText = "A numero registrazione",;
    HelpContextID = 230694835,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=94, Top=55, cSayPict='"999999"', cGetPict='"999999"'

  func oAANUMREG_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (EMPTY(.w_DANUMREG) OR .w_DANUMREG<=.w_AANUMREG)
    endwith
    return bRes
  endfunc

  add object oDACODCES_1_5 as StdField with uid="KBUBCEAJXW",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DACODCES", cQueryName = "DACODCES",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Cespite inesistente o obsoleto o cespite arrivo < cespite partenza",;
    ToolTipText = "Da codice del cespite",;
    HelpContextID = 223793015,;
   bGlobalFont=.t.,;
    Height=21, Width=156, Left=237, Top=28, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="CES_PITI", cZoomOnZoom="GSCE_ACE", oKey_1_1="CECODICE", oKey_1_2="this.w_DACODCES"

  func oDACODCES_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oDACODCES_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDACODCES_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CES_PITI','*','CECODICE',cp_AbsName(this.parent,'oDACODCES_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCE_ACE',"Elenco cespiti",'',this.parent.oContained
  endproc
  proc oDACODCES_1_5.mZoomOnZoom
    local i_obj
    i_obj=GSCE_ACE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CECODICE=this.parent.oContained.w_DACODCES
     i_obj.ecpSave()
  endproc

  add object oAACODCES_1_6 as StdField with uid="IEAOKHJUDU",rtseq=6,rtrep=.f.,;
    cFormVar = "w_AACODCES", cQueryName = "AACODCES",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Cespite inesistente o obsoleto o cespite arrivo < cespite partenza",;
    ToolTipText = "A codice del cespite",;
    HelpContextID = 223793063,;
   bGlobalFont=.t.,;
    Height=21, Width=156, Left=237, Top=55, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="CES_PITI", cZoomOnZoom="GSCE_ACE", oKey_1_1="CECODICE", oKey_1_2="this.w_AACODCES"

  func oAACODCES_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oAACODCES_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oAACODCES_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CES_PITI','*','CECODICE',cp_AbsName(this.parent,'oAACODCES_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCE_ACE',"Elenco cespiti",'',this.parent.oContained
  endproc
  proc oAACODCES_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSCE_ACE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CECODICE=this.parent.oContained.w_AACODCES
     i_obj.ecpSave()
  endproc

  add object oESER_1_7 as StdField with uid="REZLLKPSEK",rtseq=7,rtrep=.f.,;
    cFormVar = "w_ESER", cQueryName = "ESER",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Esercizio di selezione",;
    HelpContextID = 191652422,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=476, Top=28, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_ESER"

  func oESER_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oESER_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oESER_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CODAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_CODAZI)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oESER_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Esercizi",'',this.parent.oContained
  endproc

  add object oCODCAU_1_8 as StdField with uid="SLRLFSEWTW",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CODCAU", cQueryName = "CODCAU",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale cespite inesistente o obsoleta",;
    ToolTipText = "Codice causale di movimentazione cespite",;
    HelpContextID = 194163162,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=476, Top=55, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CESP", cZoomOnZoom="GSCE_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_CODCAU"

  func oCODCAU_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCAU_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCAU_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CESP','*','CCCODICE',cp_AbsName(this.parent,'oCODCAU_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCE_ACC',"Causali cespiti",'GSCE_KAM.CAU_CESP_VZM',this.parent.oContained
  endproc
  proc oCODCAU_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSCE_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_CODCAU
     i_obj.ecpSave()
  endproc

  add object oDADATA_1_9 as StdField with uid="PEDEBYHXMV",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DADATA", cQueryName = "DADATA",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data di inizio selezione > di data fine",;
    ToolTipText = "Da data registrazione",;
    HelpContextID = 241483722,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=615, Top=28

  func oDADATA_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (EMPTY(.w_AADATA) OR .w_DADATA<=.w_AADATA)
    endwith
    return bRes
  endfunc

  add object oAADATA_1_10 as StdField with uid="LDMUMXXKOY",rtseq=10,rtrep=.f.,;
    cFormVar = "w_AADATA", cQueryName = "AADATA",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data di fine selezione < di data inizio",;
    ToolTipText = "A data registrazione",;
    HelpContextID = 241483770,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=615, Top=55

  func oAADATA_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (EMPTY(.w_DADATA) OR .w_DADATA<=.w_AADATA)
    endwith
    return bRes
  endfunc


  add object oBtn_1_11 as StdButton with uid="MKOSEFFHHY",left=692, top=32, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per ricercare movimenti da elaborare";
    , HelpContextID = 173975274;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_11.Click()
      with this.Parent.oContained
        GSCE_BEZ(this.Parent.oContained,"CAR")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oSELEZI_1_14 as StdRadio with uid="ZXLDALFOBQ",rtseq=13,rtrep=.f.,left=9, top=384, width=127,height=32;
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZI_1_14.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutto"
      this.Buttons(1).HelpContextID = 100678362
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutto"
      this.Buttons(2).HelpContextID = 100678362
      this.Buttons(2).Top=15
      this.SetAll("Width",125)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELEZI_1_14.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oSELEZI_1_14.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_1_14.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=='S',1,;
      iif(this.Parent.oContained.w_SELEZI=='D',2,;
      0))
  endfunc


  add object ZoomGepr as cp_szoombox with uid="VTGJRMYHQJ",left=3, top=80, width=738,height=298,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable='MOV_CESP',cZoomFile='GSCE_KDM',bOptions=.F.,bAdvOptions=.F.,bQueryOnLoad=.F.,;
    cEvent = "Esegui",;
    nPag=1;
    , HelpContextID = 172899866


  add object oObj_1_20 as cp_runprogram with uid="DNCXRYGZBW",left=2, top=448, width=239,height=25,;
    caption='GSCG_BEZ(SEL)',;
   bGlobalFont=.t.,;
    prg='GSCE_BEZ("SEL")',;
    cEvent = "w_SELEZI Changed",;
    nPag=1;
    , HelpContextID = 164501440


  add object oBtn_1_27 as StdButton with uid="RJMQCVMRGY",left=148, top=383, width=48,height=45,;
    CpPicture="bmp\cesp.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per accedere al movimento cespite";
    , HelpContextID = 102585382;
    , caption='\<Cespite';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_27.Click()
      with this.Parent.oContained
        GSCE_BEZ(this.Parent.oContained,"RMC")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_29 as StdButton with uid="IYJFLJYWKA",left=640, top=383, width=48,height=45,;
    CpPicture="bmp\separa.bmp", caption="", nPag=1;
    , ToolTipText = "Avvia disabbinamento";
    , HelpContextID = 60710580;
    , caption='\<Disabbina';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_29.Click()
      with this.Parent.oContained
        GSCE_BEZ(this.Parent.oContained,"DIS")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_30 as StdButton with uid="HMXTPXNMQD",left=692, top=383, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Esci dalla visualizzazione";
    , HelpContextID = 193290822;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_30.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_16 as StdString with uid="QEOOXZFLEB",Visible=.t., Left=10, Top=28,;
    Alignment=1, Width=81, Height=15,;
    Caption="Da reg. n.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="MYJIDOZOXL",Visible=.t., Left=398, Top=55,;
    Alignment=1, Width=76, Height=15,;
    Caption="Causale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="YKETQMQCBJ",Visible=.t., Left=6, Top=10,;
    Alignment=0, Width=77, Height=15,;
    Caption="Filtri"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_19 as StdString with uid="LJATINOUJM",Visible=.t., Left=398, Top=28,;
    Alignment=1, Width=76, Height=15,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="GHBVXFXPDO",Visible=.t., Left=25, Top=55,;
    Alignment=1, Width=66, Height=15,;
    Caption="A reg. n.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="RFWEYQEDCO",Visible=.t., Left=546, Top=28,;
    Alignment=1, Width=67, Height=15,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="NQFTQRCZTK",Visible=.t., Left=546, Top=55,;
    Alignment=1, Width=67, Height=15,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="WHXUOSCZTM",Visible=.t., Left=158, Top=28,;
    Alignment=1, Width=77, Height=15,;
    Caption="Da cespite:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="JOLMIFKZQC",Visible=.t., Left=158, Top=55,;
    Alignment=1, Width=77, Height=15,;
    Caption="A cespite:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsce_kdm','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
