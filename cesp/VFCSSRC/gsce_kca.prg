* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_kca                                                        *
*              Stampa categorie cespiti                                        *
*                                                                              *
*      Author: TAM Software srl                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-02-24                                                      *
* Last revis.: 2008-09-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsce_kca",oParentObject))

* --- Class definition
define class tgsce_kca as StdForm
  Top    = 50
  Left   = 100

  * --- Standard Properties
  Width  = 543
  Height = 260
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-09-15"
  HelpContextID=169196183
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=11

  * --- Constant Properties
  _IDX = 0
  CAT_CESP_IDX = 0
  GRU_COCE_IDX = 0
  cPrg = "gsce_kca"
  cComment = "Stampa categorie cespiti"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_GRUPPO = space(15)
  o_GRUPPO = space(15)
  w_DESGRU = space(40)
  w_TIPBEN = space(1)
  w_CATINI = space(15)
  w_DESINI = space(40)
  w_GRUINI = space(15)
  w_CATFIN = space(15)
  w_DESFIN = space(40)
  w_GRUFIN = space(15)
  w_DATSTAM = ctod('  /  /  ')
  w_OBSODAT1 = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsce_kcaPag1","gsce_kca",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oGRUPPO_1_7
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='CAT_CESP'
    this.cWorkTables[2]='GRU_COCE'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      vx_exec(""+alltrim(THIS.w_OQRY)+", "+alltrim(THIS.w_orep)+"",this)
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_GRUPPO=space(15)
      .w_DESGRU=space(40)
      .w_TIPBEN=space(1)
      .w_CATINI=space(15)
      .w_DESINI=space(40)
      .w_GRUINI=space(15)
      .w_CATFIN=space(15)
      .w_DESFIN=space(40)
      .w_GRUFIN=space(15)
      .w_DATSTAM=ctod("  /  /  ")
      .w_OBSODAT1=space(1)
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_GRUPPO))
          .link_1_7('Full')
        endif
          .DoRTCalc(2,2,.f.)
        .w_TIPBEN = ''
        .w_CATINI = space(15)
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_CATINI))
          .link_1_10('Full')
        endif
          .DoRTCalc(5,6,.f.)
        .w_CATFIN = space(15)
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_CATFIN))
          .link_1_13('Full')
        endif
          .DoRTCalc(8,9,.f.)
        .w_DATSTAM = i_datsys
        .w_OBSODAT1 = 'N'
      .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_20.enabled = this.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,3,.t.)
        if .o_GRUPPO<>.w_GRUPPO
            .w_CATINI = space(15)
          .link_1_10('Full')
        endif
        .DoRTCalc(5,6,.t.)
        if .o_GRUPPO<>.w_GRUPPO
            .w_CATFIN = space(15)
          .link_1_13('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(8,11,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_18.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=GRUPPO
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRU_COCE_IDX,3]
    i_lTable = "GRU_COCE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRU_COCE_IDX,2], .t., this.GRU_COCE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRU_COCE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRUPPO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCE_AGP',True,'GRU_COCE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GPCODICE like "+cp_ToStrODBC(trim(this.w_GRUPPO)+"%");

          i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GPCODICE',trim(this.w_GRUPPO))
          select GPCODICE,GPDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GRUPPO)==trim(_Link_.GPCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" GPDESCRI like "+cp_ToStrODBC(trim(this.w_GRUPPO)+"%");

            i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" GPDESCRI like "+cp_ToStr(trim(this.w_GRUPPO)+"%");

            select GPCODICE,GPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_GRUPPO) and !this.bDontReportError
            deferred_cp_zoom('GRU_COCE','*','GPCODICE',cp_AbsName(oSource.parent,'oGRUPPO_1_7'),i_cWhere,'GSCE_AGP',"Gruppi contabili",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where GPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GPCODICE',oSource.xKey(1))
            select GPCODICE,GPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRUPPO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where GPCODICE="+cp_ToStrODBC(this.w_GRUPPO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GPCODICE',this.w_GRUPPO)
            select GPCODICE,GPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRUPPO = NVL(_Link_.GPCODICE,space(15))
      this.w_DESGRU = NVL(_Link_.GPDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_GRUPPO = space(15)
      endif
      this.w_DESGRU = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRU_COCE_IDX,2])+'\'+cp_ToStr(_Link_.GPCODICE,1)
      cp_ShowWarn(i_cKey,this.GRU_COCE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRUPPO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATINI
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_CESP_IDX,3]
    i_lTable = "CAT_CESP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_CESP_IDX,2], .t., this.CAT_CESP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_CESP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCE_ACT',True,'CAT_CESP')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_CATINI)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCGRUCON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_CATINI))
          select CCCODICE,CCDESCRI,CCGRUCON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATINI)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStrODBC(trim(this.w_CATINI)+"%");

            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCGRUCON";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStr(trim(this.w_CATINI)+"%");

            select CCCODICE,CCDESCRI,CCGRUCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CATINI) and !this.bDontReportError
            deferred_cp_zoom('CAT_CESP','*','CCCODICE',cp_AbsName(oSource.parent,'oCATINI_1_10'),i_cWhere,'GSCE_ACT',"Categorie",'GSCE_KCA.CAT_CESP_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCGRUCON";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCGRUCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCGRUCON";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_CATINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_CATINI)
            select CCCODICE,CCDESCRI,CCGRUCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATINI = NVL(_Link_.CCCODICE,space(15))
      this.w_DESINI = NVL(_Link_.CCDESCRI,space(40))
      this.w_GRUINI = NVL(_Link_.CCGRUCON,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_CATINI = space(15)
      endif
      this.w_DESINI = space(40)
      this.w_GRUINI = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_GRUPPO = .w_GRUINI OR empty(.w_GRUPPO)) AND (.w_CATINI <= .w_CATFIN OR empty(.w_CATFIN))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Categoria non congruente alle selezioni impostate")
        endif
        this.w_CATINI = space(15)
        this.w_DESINI = space(40)
        this.w_GRUINI = space(15)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_CESP_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_CESP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATFIN
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_CESP_IDX,3]
    i_lTable = "CAT_CESP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_CESP_IDX,2], .t., this.CAT_CESP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_CESP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCE_ACT',True,'CAT_CESP')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_CATFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCGRUCON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_CATFIN))
          select CCCODICE,CCDESCRI,CCGRUCON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATFIN)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStrODBC(trim(this.w_CATFIN)+"%");

            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCGRUCON";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStr(trim(this.w_CATFIN)+"%");

            select CCCODICE,CCDESCRI,CCGRUCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CATFIN) and !this.bDontReportError
            deferred_cp_zoom('CAT_CESP','*','CCCODICE',cp_AbsName(oSource.parent,'oCATFIN_1_13'),i_cWhere,'GSCE_ACT',"",'GSCE_KCA.CAT_CESP_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCGRUCON";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCGRUCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCGRUCON";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_CATFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_CATFIN)
            select CCCODICE,CCDESCRI,CCGRUCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATFIN = NVL(_Link_.CCCODICE,space(15))
      this.w_DESFIN = NVL(_Link_.CCDESCRI,space(40))
      this.w_GRUFIN = NVL(_Link_.CCGRUCON,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_CATFIN = space(15)
      endif
      this.w_DESFIN = space(40)
      this.w_GRUFIN = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_GRUPPO = .w_GRUFIN OR empty(.w_GRUPPO)) AND (.w_CATINI <= .w_CATFIN OR empty(.w_CATINI))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Categoria non congruente alle selezioni impostate")
        endif
        this.w_CATFIN = space(15)
        this.w_DESFIN = space(40)
        this.w_GRUFIN = space(15)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_CESP_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_CESP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oGRUPPO_1_7.value==this.w_GRUPPO)
      this.oPgFrm.Page1.oPag.oGRUPPO_1_7.value=this.w_GRUPPO
    endif
    if not(this.oPgFrm.Page1.oPag.oDESGRU_1_8.value==this.w_DESGRU)
      this.oPgFrm.Page1.oPag.oDESGRU_1_8.value=this.w_DESGRU
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPBEN_1_9.RadioValue()==this.w_TIPBEN)
      this.oPgFrm.Page1.oPag.oTIPBEN_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCATINI_1_10.value==this.w_CATINI)
      this.oPgFrm.Page1.oPag.oCATINI_1_10.value=this.w_CATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESINI_1_11.value==this.w_DESINI)
      this.oPgFrm.Page1.oPag.oDESINI_1_11.value=this.w_DESINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCATFIN_1_13.value==this.w_CATFIN)
      this.oPgFrm.Page1.oPag.oCATFIN_1_13.value=this.w_CATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFIN_1_14.value==this.w_DESFIN)
      this.oPgFrm.Page1.oPag.oDESFIN_1_14.value=this.w_DESFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDATSTAM_1_16.value==this.w_DATSTAM)
      this.oPgFrm.Page1.oPag.oDATSTAM_1_16.value=this.w_DATSTAM
    endif
    if not(this.oPgFrm.Page1.oPag.oOBSODAT1_1_17.RadioValue()==this.w_OBSODAT1)
      this.oPgFrm.Page1.oPag.oOBSODAT1_1_17.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((.w_GRUPPO = .w_GRUINI OR empty(.w_GRUPPO)) AND (.w_CATINI <= .w_CATFIN OR empty(.w_CATFIN)))  and not(empty(.w_CATINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCATINI_1_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Categoria non congruente alle selezioni impostate")
          case   not((.w_GRUPPO = .w_GRUFIN OR empty(.w_GRUPPO)) AND (.w_CATINI <= .w_CATFIN OR empty(.w_CATINI)))  and not(empty(.w_CATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCATFIN_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Categoria non congruente alle selezioni impostate")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_GRUPPO = this.w_GRUPPO
    return

enddefine

* --- Define pages as container
define class tgsce_kcaPag1 as StdContainer
  Width  = 539
  height = 260
  stdWidth  = 539
  stdheight = 260
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oGRUPPO_1_7 as StdField with uid="CJPBZEGFGG",rtseq=1,rtrep=.f.,;
    cFormVar = "w_GRUPPO", cQueryName = "GRUPPO",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Gruppo categoria selezionato",;
    HelpContextID = 26517146,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=107, Top=16, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="GRU_COCE", cZoomOnZoom="GSCE_AGP", oKey_1_1="GPCODICE", oKey_1_2="this.w_GRUPPO"

  func oGRUPPO_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oGRUPPO_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGRUPPO_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRU_COCE','*','GPCODICE',cp_AbsName(this.parent,'oGRUPPO_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCE_AGP',"Gruppi contabili",'',this.parent.oContained
  endproc
  proc oGRUPPO_1_7.mZoomOnZoom
    local i_obj
    i_obj=GSCE_AGP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GPCODICE=this.parent.oContained.w_GRUPPO
     i_obj.ecpSave()
  endproc

  add object oDESGRU_1_8 as StdField with uid="AHXSZMJFYQ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DESGRU", cQueryName = "DESGRU",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione gruppo",;
    HelpContextID = 192793546,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=243, Top=16, InputMask=replicate('X',40)


  add object oTIPBEN_1_9 as StdCombo with uid="XALXKTAZJE",value=1,rtseq=3,rtrep=.f.,left=107,top=41,width=80,height=21;
    , ToolTipText = "Tipo del bene categoria selezionato";
    , HelpContextID = 55768778;
    , cFormVar="w_TIPBEN",RowSource=""+"Tutti,"+"Immateriali,"+"Materiali", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPBEN_1_9.RadioValue()
    return(iif(this.value =1,'',;
    iif(this.value =2,'I',;
    iif(this.value =3,'M',;
    space(1)))))
  endfunc
  func oTIPBEN_1_9.GetRadio()
    this.Parent.oContained.w_TIPBEN = this.RadioValue()
    return .t.
  endfunc

  func oTIPBEN_1_9.SetRadio()
    this.Parent.oContained.w_TIPBEN=trim(this.Parent.oContained.w_TIPBEN)
    this.value = ;
      iif(this.Parent.oContained.w_TIPBEN=='',1,;
      iif(this.Parent.oContained.w_TIPBEN=='I',2,;
      iif(this.Parent.oContained.w_TIPBEN=='M',3,;
      0)))
  endfunc

  add object oCATINI_1_10 as StdField with uid="YKECSNJHJJ",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CATINI", cQueryName = "CATINI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Categoria non congruente alle selezioni impostate",;
    ToolTipText = "Codice categoria di inizio selezione",;
    HelpContextID = 129744858,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=107, Top=69, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAT_CESP", cZoomOnZoom="GSCE_ACT", oKey_1_1="CCCODICE", oKey_1_2="this.w_CATINI"

  func oCATINI_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATINI_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATINI_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_CESP','*','CCCODICE',cp_AbsName(this.parent,'oCATINI_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCE_ACT',"Categorie",'GSCE_KCA.CAT_CESP_VZM',this.parent.oContained
  endproc
  proc oCATINI_1_10.mZoomOnZoom
    local i_obj
    i_obj=GSCE_ACT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_CATINI
     i_obj.ecpSave()
  endproc

  add object oDESINI_1_11 as StdField with uid="GHPHHISZMW",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DESINI", cQueryName = "DESINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione categoria",;
    HelpContextID = 129747914,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=243, Top=69, InputMask=replicate('X',40)

  add object oCATFIN_1_13 as StdField with uid="IKBMOUVYHI",rtseq=7,rtrep=.f.,;
    cFormVar = "w_CATFIN", cQueryName = "CATFIN",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Categoria non congruente alle selezioni impostate",;
    ToolTipText = "Codice categoria di fine selezione",;
    HelpContextID = 51298266,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=107, Top=94, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAT_CESP", cZoomOnZoom="GSCE_ACT", oKey_1_1="CCCODICE", oKey_1_2="this.w_CATFIN"

  func oCATFIN_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATFIN_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATFIN_1_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_CESP','*','CCCODICE',cp_AbsName(this.parent,'oCATFIN_1_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCE_ACT',"",'GSCE_KCA.CAT_CESP_VZM',this.parent.oContained
  endproc
  proc oCATFIN_1_13.mZoomOnZoom
    local i_obj
    i_obj=GSCE_ACT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_CATFIN
     i_obj.ecpSave()
  endproc

  add object oDESFIN_1_14 as StdField with uid="NBRNJKHHPZ",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DESFIN", cQueryName = "DESFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione categoria",;
    HelpContextID = 51301322,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=243, Top=94, InputMask=replicate('X',40)

  add object oDATSTAM_1_16 as StdField with uid="BMWZAUQDQW",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DATSTAM", cQueryName = "DATSTAM",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di riferimento sul controllo dell'obsolescenza",;
    HelpContextID = 257015754,;
   bGlobalFont=.t.,;
    Height=21, Width=75, Left=107, Top=131

  add object oOBSODAT1_1_17 as StdCheck with uid="EAAWDYYSRX",rtseq=11,rtrep=.f.,left=206, top=131, caption="Stampa obsoleti",;
    ToolTipText = "Stampa solo obsoleti alla data di stampa",;
    HelpContextID = 5623273,;
    cFormVar="w_OBSODAT1", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oOBSODAT1_1_17.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oOBSODAT1_1_17.GetRadio()
    this.Parent.oContained.w_OBSODAT1 = this.RadioValue()
    return .t.
  endfunc

  func oOBSODAT1_1_17.SetRadio()
    this.Parent.oContained.w_OBSODAT1=trim(this.Parent.oContained.w_OBSODAT1)
    this.value = ;
      iif(this.Parent.oContained.w_OBSODAT1=='S',1,;
      0)
  endfunc


  add object oObj_1_18 as cp_outputCombo with uid="ZLERXIVPWT",left=107, top=171, width=382,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 189677082


  add object oBtn_1_19 as StdButton with uid="ZMIZZNTUDJ",left=431, top=208, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per stampare";
    , HelpContextID = 44084202;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_19.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_19.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY) and not empty(.w_datstam))
      endwith
    endif
  endfunc


  add object oBtn_1_20 as StdButton with uid="JWIBHYOYEX",left=483, top=208, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 176513606;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_20.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_1 as StdString with uid="SMPXPXXJTH",Visible=.t., Left=16, Top=131,;
    Alignment=1, Width=87, Height=15,;
    Caption="Data di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_2 as StdString with uid="AFAHCNLYBA",Visible=.t., Left=6, Top=171,;
    Alignment=1, Width=97, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_3 as StdString with uid="GNNSOXVFQX",Visible=.t., Left=15, Top=69,;
    Alignment=1, Width=88, Height=15,;
    Caption="Da categoria:"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="AOUJVIZUDM",Visible=.t., Left=24, Top=94,;
    Alignment=1, Width=79, Height=15,;
    Caption="A categoria:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="WVRFCINVVG",Visible=.t., Left=44, Top=41,;
    Alignment=1, Width=59, Height=15,;
    Caption="Tipo bene:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="ZKHTAJUTRX",Visible=.t., Left=59, Top=16,;
    Alignment=1, Width=44, Height=15,;
    Caption="Gruppo:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsce_kca','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
