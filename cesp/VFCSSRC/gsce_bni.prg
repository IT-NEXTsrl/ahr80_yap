* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_bni                                                        *
*              Stampa note integrative                                         *
*                                                                              *
*      Author: Zucchetti spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][207]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-04-13                                                      *
* Last revis.: 2010-01-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsce_bni",oParentObject)
return(i_retval)

define class tgsce_bni as StdBatch
  * --- Local variables
  w_COMPET = space(4)
  w_DECTOT = 0
  w_DATFIN = ctod("  /  /  ")
  w_MESS = space(255)
  * --- WorkFile variables
  VALUTE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampa Nota Integrativa (da GSCE_KNI)
    this.w_DATFIN = this.oParentObject.w_FINESE
    this.w_COMPET = this.oParentObject.w_CODESE
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VADECTOT"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(this.oParentObject.w_VALNAZ);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VADECTOT;
        from (i_cTable) where;
            VACODVAL = this.oParentObject.w_VALNAZ;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DECTOT = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Parametri della query passati al report
    l_compet = this.w_COMPET
    l_dectot = this.w_DECTOT
    l_conto=this.oParentObject.w_conto
    do case
      case this.oParentObject.w_ODES=1
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_ODES=2
        vx_exec("..\CESP\EXE\QUERY\GSCE_NI8.VQR",this)
      case this.oParentObject.W_ODES=3
        vx_exec("..\CESP\EXE\QUERY\GSCE_N13.VQR",this)
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Saldi Gruppo Contabile - Group by: gruppo contabile,valuta; Filter: competenza (<), azienda(=)
    if g_APPLICATION="ADHOC REVOLUTION"
      * --- Saldi Gruppo Contabile - Group by: gruppo contabile,valuta; Filter: competenza (<), azienda(=)
      vq_exec("..\CESP\EXE\QUERY\GSCE_NI2.VQR", this, "mycrs_salgru")
      WRCURSOR("mycrs_salgru")
    else
      vq_exec("..\CESP\EXE\QUERY\GSCE_NI2.VQR", this, "mycrs1salgru")
      WRCURSOR("mycrs1salgru")
      * --- E' stato usato SCAN+REPLACE perch� UPDATE ha generato degli errori
      SCAN FOR ESVALNAZ<>this.oParentObject.W_VALNAZ
      REPLACE ;
      SCINCVAL WITH cp_ROUND(VALCAM(NVL(SCINCVAL,0), ESVALNAZ, this.oParentObject.W_VALNAZ, this.w_DATFIN, 0), this.W_DECTOT), ;
      SCIMPONS WITH cp_ROUND(VALCAM(NVL(SCIMPONS,0), ESVALNAZ, this.oParentObject.W_VALNAZ, this.w_DATFIN, 0), this.W_DECTOT), ;
      SCDECVAL WITH cp_ROUND(VALCAM(NVL(SCDECVAL,0), ESVALNAZ, this.oParentObject.W_VALNAZ, this.w_DATFIN, 0), this.W_DECTOT), ;
      SCIMPRIV WITH cp_ROUND(VALCAM(NVL(SCIMPRIV,0), ESVALNAZ, this.oParentObject.W_VALNAZ, this.w_DATFIN, 0), this.W_DECTOT), ;
      SCACCCIV WITH cp_ROUND(VALCAM(NVL(SCACCCIV,0), ESVALNAZ, this.oParentObject.W_VALNAZ, this.w_DATFIN, 0), this.W_DECTOT), ;
      SCUTICIV WITH cp_ROUND(VALCAM(NVL(SCUTICIV,0), ESVALNAZ, this.oParentObject.W_VALNAZ, this.w_DATFIN, 0), this.W_DECTOT), ;
      SCIMPSVA WITH cp_ROUND(VALCAM(NVL(SCIMPSVA,0), ESVALNAZ, this.oParentObject.W_VALNAZ, this.w_DATFIN, 0), this.W_DECTOT)
      ENDSCAN
      * --- Devo raggruppare per Gruppo Contabile e Valuta
      Select Esvalnaz as Esvalnaz,Sum(Scincval) as Scincval,Sum(Scimpriv) as Scimpriv,Sum(Scimpons) as Scimpons,; 
 Sum(Scdecval) as Scdecval,Sum(Scaccciv) as Scaccciv,Sum(Scimpsva) as Scimpsva,Sum(Scuticiv) as Scuticiv,; 
 Ccgrucon as Ccgrucon,Max(Gpdescri) as Gpdescri,Max(Gpconc01) as Gpconc01,Max(Gpconc02) as Gpconc02,; 
 Max(Gpconc03) as Gpconc03,Max(Gpconc04) as Gpconc04,Max(Gpconc05) as Gpconc05,Max(Gpconc06) as Gpconc06,; 
 Max(Gpconc07) as Gpconc07,Max(Gpconc08) as Gpconc08,Max(Gpconc09) as Gpconc09,Max(Gpconc10) as Gpconc10,; 
 Max(Gpconc11) as Gpconc11,Max(Gpconc12) as Gpconc12,Max(Gpconc13) as Gpconc13,Max(Gpconc14) as Gpconc14,; 
 Max(Gpconc15) as Gpconc15,Max(Gpconc16) as Gpconc16; 
 from Mycrs1salgru into cursor Mycrs_salgru order by Ccgrucon group by Ccgrucon
    endif
    * --- Movimenti Gruppo Contabile - Filter:competenza (=)
    vq_exec("..\CESP\EXE\QUERY\GSCE1NI2.VQR", this, "mycrs_movgru")
    * --- Unisce i dati (saldi iniziali + movimenti)
    if g_APPLICATION="ADHOC REVOLUTION"
      Select mycrs_movgru.*, "M" as movsal ; 
 FROM mycrs_movgru ; 
 UNION SELECT esvalnaz AS mccodval, SUM(scincval) AS mcimpa02, SUM(scimpriv) AS mcimpa04, ; 
 SUM(scaccciv) AS mcimpa07, SUM(scimpsva) AS mcimpa06, SUM(scimpriv) AS mcimpa05, ; 
 ccgrucon, gpdescri,max(gpconc01),max(gpconc02), max(gpconc03), max(gpconc04), ; 
 max(gpconc05), max(gpconc06), max(gpconc07), max(gpconc08), max(gpconc09), max(gpconc10), ; 
 MAX(GPCONC11), MAX(GPCONC12), ; 
 MAX(GPCONC13), MAX(GPCONC14), MAX(GPCONC15), MAX(GPCONC16), "S" AS movsal ; 
 FROM mycrs_salgru ; 
 GROUP BY ccgrucon ; 
 ORDER BY 7, movsal DESC INTO CURSOR __tmp__
    else
      SELECT mycrs_movgru.*, "M" as movsal ; 
 FROM mycrs_movgru ; 
 UNION SELECT esvalnaz AS mccodval, SUM(scincval+scimpons-scdecval) AS mcimpa02, SUM(scimpriv) AS mcimpa04,; 
 SUM(scaccciv-scuticiv) AS mcimpa07, SUM(scimpsva) AS mcimpa06, SUM(scimpriv-scimpriv) AS mcimpa05,; 
 ccgrucon, gpdescri, gpconc01,gpconc02,gpconc03,gpconc04,gpconc05,gpconc06,gpconc07,gpconc08,; 
 gpconc09,gpconc10,gpconc11,gpconc12,gpconc13,gpconc14,gpconc15,gpconc16, "S" AS movsal ; 
 FROM mycrs_salgru ; 
 GROUP BY ccgrucon ; 
 ORDER BY 7, movsal DESC INTO CURSOR __tmp__
    endif
    * --- Chiude i cursori
    if used("mycrs_movgru")
      select ("mycrs_movgru")
      use
    endif
    if used("mycrs_salgru")
      select ("mycrs_salgru")
      use
    endif
    if used("mycrs1salgru")
      select ("mycrs1salgru")
      use
    endif
    * --- Parametri della query passati al report
    l_compet = this.w_COMPET
    l_valnaz = this.oParentObject.w_VALNAZ
    l_dectot = this.w_DECTOT
    l_CONTO= this.oParentObject.w_CONTO
    select ("__TMP__")
    CP_CHPRN("..\CESP\EXE\QUERY\GSCE_NI2.FRX","",this.oParentObject)
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='VALUTE'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
