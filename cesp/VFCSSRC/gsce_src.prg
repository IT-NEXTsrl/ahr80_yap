* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_src                                                        *
*              Stampa registro cespiti                                         *
*                                                                              *
*      Author: TAM Software Srl                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30] [VRS_75]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-04-02                                                      *
* Last revis.: 2009-06-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsce_src",oParentObject))

* --- Class definition
define class tgsce_src as StdForm
  Top    = 53
  Left   = 88

  * --- Standard Properties
  Width  = 531
  Height = 378
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-06-12"
  HelpContextID=107627881
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=27

  * --- Constant Properties
  _IDX = 0
  ESERCIZI_IDX = 0
  AZIENDA_IDX = 0
  VALUTE_IDX = 0
  PAR_CESP_IDX = 0
  ATTIMAST_IDX = 0
  CAT_CESP_IDX = 0
  GRU_COCE_IDX = 0
  cPrg = "gsce_src"
  cComment = "Stampa registro cespiti"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODAZI = space(5)
  w_PCFLSTIN = space(1)
  w_STAREG = space(1)
  w_PCTIPAMM = space(1)
  w_FLSALP = space(1)
  w_CODESE = space(4)
  w_CODATT = space(5)
  w_CATINI = space(15)
  o_CATINI = space(15)
  w_CATFIN = space(15)
  o_CATFIN = space(15)
  w_TIPSTAM = space(1)
  w_INIESE = ctod('  /  /  ')
  w_TIPAMM = space(1)
  o_TIPAMM = space(1)
  w_FINESE = ctod('  /  /  ')
  w_VALNAZ = space(3)
  w_TESTO = space(1)
  w_STAREC = space(4)
  w_ULTDAT = ctod('  /  /  ')
  w_PREFIS = space(20)
  w_DESATT = space(35)
  w_DESINI = space(40)
  w_DESFIN = space(40)
  w_INFAGG = space(1)
  w_GRUCINI = space(15)
  w_GRUCFIN = space(15)
  w_GCDESINI = space(40)
  w_GCDESFIN = space(40)
  w_TOTCAT = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsce_srcPag1","gsce_src",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODESE_1_6
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[7]
    this.cWorkTables[1]='ESERCIZI'
    this.cWorkTables[2]='AZIENDA'
    this.cWorkTables[3]='VALUTE'
    this.cWorkTables[4]='PAR_CESP'
    this.cWorkTables[5]='ATTIMAST'
    this.cWorkTables[6]='CAT_CESP'
    this.cWorkTables[7]='GRU_COCE'
    return(this.OpenAllTables(7))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI=space(5)
      .w_PCFLSTIN=space(1)
      .w_STAREG=space(1)
      .w_PCTIPAMM=space(1)
      .w_FLSALP=space(1)
      .w_CODESE=space(4)
      .w_CODATT=space(5)
      .w_CATINI=space(15)
      .w_CATFIN=space(15)
      .w_TIPSTAM=space(1)
      .w_INIESE=ctod("  /  /  ")
      .w_TIPAMM=space(1)
      .w_FINESE=ctod("  /  /  ")
      .w_VALNAZ=space(3)
      .w_TESTO=space(1)
      .w_STAREC=space(4)
      .w_ULTDAT=ctod("  /  /  ")
      .w_PREFIS=space(20)
      .w_DESATT=space(35)
      .w_DESINI=space(40)
      .w_DESFIN=space(40)
      .w_INFAGG=space(1)
      .w_GRUCINI=space(15)
      .w_GRUCFIN=space(15)
      .w_GCDESINI=space(40)
      .w_GCDESFIN=space(40)
      .w_TOTCAT=space(1)
        .w_CODAZI = i_CODAZI
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODAZI))
          .link_1_1('Full')
        endif
          .DoRTCalc(2,5,.f.)
        .w_CODESE = g_CODESE
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_CODESE))
          .link_1_6('Full')
        endif
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_CODATT))
          .link_1_7('Full')
        endif
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_CATINI))
          .link_1_8('Full')
        endif
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_CATFIN))
          .link_1_9('Full')
        endif
        .w_TIPSTAM = 'S'
          .DoRTCalc(11,11,.f.)
        .w_TIPAMM = IIF(.w_PCTIPAMM<>'C','F','C')
        .DoRTCalc(13,16,.f.)
        if not(empty(.w_STAREC))
          .link_1_16('Full')
        endif
          .DoRTCalc(17,21,.f.)
        .w_INFAGG = IIF(.w_TIPAMM='C','S',.w_INFAGG)
        .DoRTCalc(23,23,.f.)
        if not(empty(.w_GRUCINI))
          .link_1_38('Full')
        endif
        .DoRTCalc(24,24,.f.)
        if not(empty(.w_GRUCFIN))
          .link_1_39('Full')
        endif
    endwith
    this.DoRTCalc(25,27,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_24.enabled = this.oPgFrm.Page1.oPag.oBtn_1_24.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_25.enabled = this.oPgFrm.Page1.oPag.oBtn_1_25.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_26.enabled = this.oPgFrm.Page1.oPag.oBtn_1_26.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,7,.t.)
        if .o_CATFIN<>.w_CATFIN
          .link_1_8('Full')
        endif
        if .o_CATINI<>.w_CATINI
          .link_1_9('Full')
        endif
        .DoRTCalc(10,15,.t.)
          .link_1_16('Full')
        .DoRTCalc(17,21,.t.)
        if .o_TIPAMM<>.w_TIPAMM
            .w_INFAGG = IIF(.w_TIPAMM='C','S',.w_INFAGG)
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(23,27,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oTESTO_1_15.visible=!this.oPgFrm.Page1.oPag.oTESTO_1_15.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_22.visible=!this.oPgFrm.Page1.oPag.oStr_1_22.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_23.visible=!this.oPgFrm.Page1.oPag.oStr_1_23.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_24.visible=!this.oPgFrm.Page1.oPag.oBtn_1_24.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_26.visible=!this.oPgFrm.Page1.oPag.oBtn_1_26.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_36.visible=!this.oPgFrm.Page1.oPag.oStr_1_36.mHide()
    this.oPgFrm.Page1.oPag.oINFAGG_1_37.visible=!this.oPgFrm.Page1.oPag.oINFAGG_1_37.mHide()
    this.oPgFrm.Page1.oPag.oGRUCINI_1_38.visible=!this.oPgFrm.Page1.oPag.oGRUCINI_1_38.mHide()
    this.oPgFrm.Page1.oPag.oGRUCFIN_1_39.visible=!this.oPgFrm.Page1.oPag.oGRUCFIN_1_39.mHide()
    this.oPgFrm.Page1.oPag.oGCDESINI_1_40.visible=!this.oPgFrm.Page1.oPag.oGCDESINI_1_40.mHide()
    this.oPgFrm.Page1.oPag.oGCDESFIN_1_41.visible=!this.oPgFrm.Page1.oPag.oGCDESFIN_1_41.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_42.visible=!this.oPgFrm.Page1.oPag.oStr_1_42.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_43.visible=!this.oPgFrm.Page1.oPag.oStr_1_43.mHide()
    this.oPgFrm.Page1.oPag.oTOTCAT_1_44.visible=!this.oPgFrm.Page1.oPag.oTOTCAT_1_44.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_45.visible=!this.oPgFrm.Page1.oPag.oStr_1_45.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_CESP_IDX,3]
    i_lTable = "PAR_CESP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_CESP_IDX,2], .t., this.PAR_CESP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_CESP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PCCODAZI,PCDATCON,PCFLSTIN,PCSTAREG,PCPREFIS,PCFLSALP,PCTIPAMM";
                   +" from "+i_cTable+" "+i_lTable+" where PCCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   +" and PCCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PCCODAZI',this.w_CODAZI;
                       ,'PCCODAZI',this.w_CODAZI)
            select PCCODAZI,PCDATCON,PCFLSTIN,PCSTAREG,PCPREFIS,PCFLSALP,PCTIPAMM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.PCCODAZI,space(5))
      this.w_STAREC = NVL(_Link_.PCDATCON,space(4))
      this.w_PCFLSTIN = NVL(_Link_.PCFLSTIN,space(1))
      this.w_STAREG = NVL(_Link_.PCSTAREG,space(1))
      this.w_PREFIS = NVL(_Link_.PCPREFIS,space(20))
      this.w_FLSALP = NVL(_Link_.PCFLSALP,space(1))
      this.w_PCTIPAMM = NVL(_Link_.PCTIPAMM,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_STAREC = space(4)
      this.w_PCFLSTIN = space(1)
      this.w_STAREG = space(1)
      this.w_PREFIS = space(20)
      this.w_FLSALP = space(1)
      this.w_PCTIPAMM = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_CESP_IDX,2])+'\'+cp_ToStr(_Link_.PCCODAZI,1)+'\'+cp_ToStr(_Link_.PCCODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_CESP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODESE
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_CODESE)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESFINESE,ESINIESE,ESVALNAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_CODAZI;
                     ,'ESCODESE',trim(this.w_CODESE))
          select ESCODAZI,ESCODESE,ESFINESE,ESINIESE,ESVALNAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODESE)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODESE) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oCODESE_1_6'),i_cWhere,'',"Esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESFINESE,ESINIESE,ESVALNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESFINESE,ESINIESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESFINESE,ESINIESE,ESVALNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESFINESE,ESINIESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESFINESE,ESINIESE,ESVALNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_CODESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_CODESE)
            select ESCODAZI,ESCODESE,ESFINESE,ESINIESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODESE = NVL(_Link_.ESCODESE,space(4))
      this.w_FINESE = NVL(cp_ToDate(_Link_.ESFINESE),ctod("  /  /  "))
      this.w_INIESE = NVL(cp_ToDate(_Link_.ESINIESE),ctod("  /  /  "))
      this.w_VALNAZ = NVL(_Link_.ESVALNAZ,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_CODESE = space(4)
      endif
      this.w_FINESE = ctod("  /  /  ")
      this.w_INIESE = ctod("  /  /  ")
      this.w_VALNAZ = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODATT
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIMAST_IDX,3]
    i_lTable = "ATTIMAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIMAST_IDX,2], .t., this.ATTIMAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIMAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ATTIMAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_CODATT)+"%");

          i_ret=cp_SQL(i_nConn,"select ATCODATT,ATDESATT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODATT',trim(this.w_CODATT))
          select ATCODATT,ATDESATT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODATT)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ATDESATT like "+cp_ToStrODBC(trim(this.w_CODATT)+"%");

            i_ret=cp_SQL(i_nConn,"select ATCODATT,ATDESATT";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ATDESATT like "+cp_ToStr(trim(this.w_CODATT)+"%");

            select ATCODATT,ATDESATT;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODATT) and !this.bDontReportError
            deferred_cp_zoom('ATTIMAST','*','ATCODATT',cp_AbsName(oSource.parent,'oCODATT_1_7'),i_cWhere,'',"Attivit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODATT,ATDESATT";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODATT',oSource.xKey(1))
            select ATCODATT,ATDESATT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODATT,ATDESATT";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_CODATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODATT',this.w_CODATT)
            select ATCODATT,ATDESATT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODATT = NVL(_Link_.ATCODATT,space(5))
      this.w_DESATT = NVL(_Link_.ATDESATT,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODATT = space(5)
      endif
      this.w_DESATT = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIMAST_IDX,2])+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIMAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATINI
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_CESP_IDX,3]
    i_lTable = "CAT_CESP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_CESP_IDX,2], .t., this.CAT_CESP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_CESP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCE_ACT',True,'CAT_CESP')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_CATINI)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_CATINI))
          select CCCODICE,CCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATINI)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStrODBC(trim(this.w_CATINI)+"%");

            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStr(trim(this.w_CATINI)+"%");

            select CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CATINI) and !this.bDontReportError
            deferred_cp_zoom('CAT_CESP','*','CCCODICE',cp_AbsName(oSource.parent,'oCATINI_1_8'),i_cWhere,'GSCE_ACT',"Categorie cespiti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_CATINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_CATINI)
            select CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATINI = NVL(_Link_.CCCODICE,space(15))
      this.w_DESINI = NVL(_Link_.CCDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CATINI = space(15)
      endif
      this.w_DESINI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CATINI <= .w_CATFIN OR empty(.w_CATFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Categoria inesistente o obsoleta o intervallo scorretto")
        endif
        this.w_CATINI = space(15)
        this.w_DESINI = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_CESP_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_CESP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATFIN
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_CESP_IDX,3]
    i_lTable = "CAT_CESP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_CESP_IDX,2], .t., this.CAT_CESP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_CESP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCE_ACT',True,'CAT_CESP')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_CATFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_CATFIN))
          select CCCODICE,CCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATFIN)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStrODBC(trim(this.w_CATFIN)+"%");

            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStr(trim(this.w_CATFIN)+"%");

            select CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CATFIN) and !this.bDontReportError
            deferred_cp_zoom('CAT_CESP','*','CCCODICE',cp_AbsName(oSource.parent,'oCATFIN_1_9'),i_cWhere,'GSCE_ACT',"Categorie cespiti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_CATFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_CATFIN)
            select CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATFIN = NVL(_Link_.CCCODICE,space(15))
      this.w_DESFIN = NVL(_Link_.CCDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CATFIN = space(15)
      endif
      this.w_DESFIN = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CATINI <= .w_CATFIN OR empty(.w_CATINI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Categoria inesistente o obsoleta o intervallo scorretto")
        endif
        this.w_CATFIN = space(15)
        this.w_DESFIN = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_CESP_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_CESP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=STAREC
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_STAREC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_STAREC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESFINESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_STAREC);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_STAREC)
            select ESCODAZI,ESCODESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_STAREC = NVL(_Link_.ESCODESE,space(4))
      this.w_ULTDAT = NVL(cp_ToDate(_Link_.ESFINESE),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_STAREC = space(4)
      endif
      this.w_ULTDAT = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_STAREC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GRUCINI
  func Link_1_38(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRU_COCE_IDX,3]
    i_lTable = "GRU_COCE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRU_COCE_IDX,2], .t., this.GRU_COCE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRU_COCE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRUCINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCE_AGP',True,'GRU_COCE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GPCODICE like "+cp_ToStrODBC(trim(this.w_GRUCINI)+"%");

          i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GPCODICE',trim(this.w_GRUCINI))
          select GPCODICE,GPDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GRUCINI)==trim(_Link_.GPCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" GPDESCRI like "+cp_ToStrODBC(trim(this.w_GRUCINI)+"%");

            i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" GPDESCRI like "+cp_ToStr(trim(this.w_GRUCINI)+"%");

            select GPCODICE,GPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_GRUCINI) and !this.bDontReportError
            deferred_cp_zoom('GRU_COCE','*','GPCODICE',cp_AbsName(oSource.parent,'oGRUCINI_1_38'),i_cWhere,'GSCE_AGP',"Gruppi contabili",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where GPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GPCODICE',oSource.xKey(1))
            select GPCODICE,GPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRUCINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where GPCODICE="+cp_ToStrODBC(this.w_GRUCINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GPCODICE',this.w_GRUCINI)
            select GPCODICE,GPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRUCINI = NVL(_Link_.GPCODICE,space(15))
      this.w_GCDESINI = NVL(_Link_.GPDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_GRUCINI = space(15)
      endif
      this.w_GCDESINI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_GRUCINI <= .w_GRUCFIN OR empty(.w_GRUCFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Gruppo contabile inesistente o obsoleto o intervallo scorretto")
        endif
        this.w_GRUCINI = space(15)
        this.w_GCDESINI = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRU_COCE_IDX,2])+'\'+cp_ToStr(_Link_.GPCODICE,1)
      cp_ShowWarn(i_cKey,this.GRU_COCE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRUCINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GRUCFIN
  func Link_1_39(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRU_COCE_IDX,3]
    i_lTable = "GRU_COCE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRU_COCE_IDX,2], .t., this.GRU_COCE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRU_COCE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRUCFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCE_AGP',True,'GRU_COCE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GPCODICE like "+cp_ToStrODBC(trim(this.w_GRUCFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GPCODICE',trim(this.w_GRUCFIN))
          select GPCODICE,GPDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GRUCFIN)==trim(_Link_.GPCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" GPDESCRI like "+cp_ToStrODBC(trim(this.w_GRUCFIN)+"%");

            i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" GPDESCRI like "+cp_ToStr(trim(this.w_GRUCFIN)+"%");

            select GPCODICE,GPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_GRUCFIN) and !this.bDontReportError
            deferred_cp_zoom('GRU_COCE','*','GPCODICE',cp_AbsName(oSource.parent,'oGRUCFIN_1_39'),i_cWhere,'GSCE_AGP',"Gruppi contabili",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where GPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GPCODICE',oSource.xKey(1))
            select GPCODICE,GPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRUCFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where GPCODICE="+cp_ToStrODBC(this.w_GRUCFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GPCODICE',this.w_GRUCFIN)
            select GPCODICE,GPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRUCFIN = NVL(_Link_.GPCODICE,space(15))
      this.w_GCDESFIN = NVL(_Link_.GPDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_GRUCFIN = space(15)
      endif
      this.w_GCDESFIN = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_GRUCINI <= .w_GRUCFIN OR empty(.w_GRUCINI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Gruppo contabile inesistente o obsoleto o intervallo scorretto")
        endif
        this.w_GRUCFIN = space(15)
        this.w_GCDESFIN = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRU_COCE_IDX,2])+'\'+cp_ToStr(_Link_.GPCODICE,1)
      cp_ShowWarn(i_cKey,this.GRU_COCE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRUCFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODESE_1_6.value==this.w_CODESE)
      this.oPgFrm.Page1.oPag.oCODESE_1_6.value=this.w_CODESE
    endif
    if not(this.oPgFrm.Page1.oPag.oCODATT_1_7.value==this.w_CODATT)
      this.oPgFrm.Page1.oPag.oCODATT_1_7.value=this.w_CODATT
    endif
    if not(this.oPgFrm.Page1.oPag.oCATINI_1_8.value==this.w_CATINI)
      this.oPgFrm.Page1.oPag.oCATINI_1_8.value=this.w_CATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCATFIN_1_9.value==this.w_CATFIN)
      this.oPgFrm.Page1.oPag.oCATFIN_1_9.value=this.w_CATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPSTAM_1_10.RadioValue()==this.w_TIPSTAM)
      this.oPgFrm.Page1.oPag.oTIPSTAM_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPAMM_1_12.RadioValue()==this.w_TIPAMM)
      this.oPgFrm.Page1.oPag.oTIPAMM_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTESTO_1_15.RadioValue()==this.w_TESTO)
      this.oPgFrm.Page1.oPag.oTESTO_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSTAREC_1_16.value==this.w_STAREC)
      this.oPgFrm.Page1.oPag.oSTAREC_1_16.value=this.w_STAREC
    endif
    if not(this.oPgFrm.Page1.oPag.oPREFIS_1_28.value==this.w_PREFIS)
      this.oPgFrm.Page1.oPag.oPREFIS_1_28.value=this.w_PREFIS
    endif
    if not(this.oPgFrm.Page1.oPag.oDESATT_1_31.value==this.w_DESATT)
      this.oPgFrm.Page1.oPag.oDESATT_1_31.value=this.w_DESATT
    endif
    if not(this.oPgFrm.Page1.oPag.oDESINI_1_34.value==this.w_DESINI)
      this.oPgFrm.Page1.oPag.oDESINI_1_34.value=this.w_DESINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFIN_1_35.value==this.w_DESFIN)
      this.oPgFrm.Page1.oPag.oDESFIN_1_35.value=this.w_DESFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oINFAGG_1_37.RadioValue()==this.w_INFAGG)
      this.oPgFrm.Page1.oPag.oINFAGG_1_37.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGRUCINI_1_38.value==this.w_GRUCINI)
      this.oPgFrm.Page1.oPag.oGRUCINI_1_38.value=this.w_GRUCINI
    endif
    if not(this.oPgFrm.Page1.oPag.oGRUCFIN_1_39.value==this.w_GRUCFIN)
      this.oPgFrm.Page1.oPag.oGRUCFIN_1_39.value=this.w_GRUCFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oGCDESINI_1_40.value==this.w_GCDESINI)
      this.oPgFrm.Page1.oPag.oGCDESINI_1_40.value=this.w_GCDESINI
    endif
    if not(this.oPgFrm.Page1.oPag.oGCDESFIN_1_41.value==this.w_GCDESFIN)
      this.oPgFrm.Page1.oPag.oGCDESFIN_1_41.value=this.w_GCDESFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTCAT_1_44.RadioValue()==this.w_TOTCAT)
      this.oPgFrm.Page1.oPag.oTOTCAT_1_44.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CODESE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODESE_1_6.SetFocus()
            i_bnoObbl = !empty(.w_CODESE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CATINI <= .w_CATFIN OR empty(.w_CATFIN))  and not(empty(.w_CATINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCATINI_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Categoria inesistente o obsoleta o intervallo scorretto")
          case   not(.w_CATINI <= .w_CATFIN OR empty(.w_CATINI))  and not(empty(.w_CATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCATFIN_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Categoria inesistente o obsoleta o intervallo scorretto")
          case   not(.w_GRUCINI <= .w_GRUCFIN OR empty(.w_GRUCFIN))  and not(.w_STAREG<>'G')  and not(empty(.w_GRUCINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGRUCINI_1_38.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Gruppo contabile inesistente o obsoleto o intervallo scorretto")
          case   not(.w_GRUCINI <= .w_GRUCFIN OR empty(.w_GRUCINI))  and not(.w_STAREG<>'G')  and not(empty(.w_GRUCFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGRUCFIN_1_39.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Gruppo contabile inesistente o obsoleto o intervallo scorretto")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsce_src
      * Simulo il bottone F10, effettuo l'operazione
      *all'interno dell'area manuale Check form perch� sono presenti
      *pi� bottoni sovrapposti
      if this.w_stareg='A'
          * Stampa registro cespiti per categoria
          do gsce_brc with this
      else
          * Stampa registro cespiti per cespite o gruppo contabile
          do Gsce_br1 with this
          
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CATINI = this.w_CATINI
    this.o_CATFIN = this.w_CATFIN
    this.o_TIPAMM = this.w_TIPAMM
    return

enddefine

* --- Define pages as container
define class tgsce_srcPag1 as StdContainer
  Width  = 527
  height = 378
  stdWidth  = 527
  stdheight = 378
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODESE_1_6 as StdField with uid="KGFPEERGAS",rtseq=6,rtrep=.f.,;
    cFormVar = "w_CODESE", cQueryName = "CODESE",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Esercizio da cui si ricavano i progressivi - non fa filtro sulla stampa",;
    HelpContextID = 200323546,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=135, Top=36, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_CODESE"

  func oCODESE_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODESE_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODESE_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CODAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_CODAZI)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oCODESE_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Esercizi",'',this.parent.oContained
  endproc

  add object oCODATT_1_7 as StdField with uid="NVHYJKEQGE",rtseq=7,rtrep=.f.,;
    cFormVar = "w_CODATT", cQueryName = "CODATT",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice attivit� IVA: se specificata verranno considerati solo i cespiti che hanno associata questa attivit� in anagrafica",;
    HelpContextID = 52121126,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=135, Top=64, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="ATTIMAST", oKey_1_1="ATCODATT", oKey_1_2="this.w_CODATT"

  func oCODATT_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODATT_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODATT_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ATTIMAST','*','ATCODATT',cp_AbsName(this.parent,'oCODATT_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Attivit�",'',this.parent.oContained
  endproc

  add object oCATINI_1_8 as StdField with uid="TUBUGXLANS",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CATINI", cQueryName = "CATINI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Categoria inesistente o obsoleta o intervallo scorretto",;
    ToolTipText = "Codice categoria di inizio selezione",;
    HelpContextID = 138133466,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=135, Top=92, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAT_CESP", cZoomOnZoom="GSCE_ACT", oKey_1_1="CCCODICE", oKey_1_2="this.w_CATINI"

  func oCATINI_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATINI_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATINI_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_CESP','*','CCCODICE',cp_AbsName(this.parent,'oCATINI_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCE_ACT',"Categorie cespiti",'',this.parent.oContained
  endproc
  proc oCATINI_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSCE_ACT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_CATINI
     i_obj.ecpSave()
  endproc

  add object oCATFIN_1_9 as StdField with uid="BPFRBNJLFR",rtseq=9,rtrep=.f.,;
    cFormVar = "w_CATFIN", cQueryName = "CATFIN",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Categoria inesistente o obsoleta o intervallo scorretto",;
    ToolTipText = "Codice categoria di fine selezione",;
    HelpContextID = 59686874,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=135, Top=120, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAT_CESP", cZoomOnZoom="GSCE_ACT", oKey_1_1="CCCODICE", oKey_1_2="this.w_CATFIN"

  func oCATFIN_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATFIN_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATFIN_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_CESP','*','CCCODICE',cp_AbsName(this.parent,'oCATFIN_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCE_ACT',"Categorie cespiti",'',this.parent.oContained
  endproc
  proc oCATFIN_1_9.mZoomOnZoom
    local i_obj
    i_obj=GSCE_ACT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_CATFIN
     i_obj.ecpSave()
  endproc


  add object oTIPSTAM_1_10 as StdCombo with uid="CJPFFWAUKD",rtseq=10,rtrep=.f.,left=28,top=234,width=107,height=21;
    , ToolTipText = "Tipo di stampa selezionata";
    , HelpContextID = 265418442;
    , cFormVar="w_TIPSTAM",RowSource=""+"Simulata,"+"Definitiva,"+"Ristampa", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPSTAM_1_10.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    iif(this.value =3,'R',;
    space(1)))))
  endfunc
  func oTIPSTAM_1_10.GetRadio()
    this.Parent.oContained.w_TIPSTAM = this.RadioValue()
    return .t.
  endfunc

  func oTIPSTAM_1_10.SetRadio()
    this.Parent.oContained.w_TIPSTAM=trim(this.Parent.oContained.w_TIPSTAM)
    this.value = ;
      iif(this.Parent.oContained.w_TIPSTAM=='S',1,;
      iif(this.Parent.oContained.w_TIPSTAM=='D',2,;
      iif(this.Parent.oContained.w_TIPSTAM=='R',3,;
      0)))
  endfunc


  add object oTIPAMM_1_12 as StdCombo with uid="HFTRZXDHUJ",rtseq=12,rtrep=.f.,left=28,top=261,width=107,height=21;
    , ToolTipText = "Tipo di ammortamento: civile o fiscale";
    , HelpContextID = 72611530;
    , cFormVar="w_TIPAMM",RowSource=""+"Civile,"+"Fiscale", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPAMM_1_12.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oTIPAMM_1_12.GetRadio()
    this.Parent.oContained.w_TIPAMM = this.RadioValue()
    return .t.
  endfunc

  func oTIPAMM_1_12.SetRadio()
    this.Parent.oContained.w_TIPAMM=trim(this.Parent.oContained.w_TIPAMM)
    this.value = ;
      iif(this.Parent.oContained.w_TIPAMM=='C',1,;
      iif(this.Parent.oContained.w_TIPAMM=='F',2,;
      0))
  endfunc

  add object oTESTO_1_15 as StdCheck with uid="YSVYTKMYZA",rtseq=15,rtrep=.f.,left=28, top=295, caption="Stampa solo testo",;
    ToolTipText = "Stampa solo testo",;
    HelpContextID = 18926282,;
    cFormVar="w_TESTO", bObbl = .f. , nPag = 1;
    , TabStop=.F.;
   , bGlobalFont=.t.


  func oTESTO_1_15.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTESTO_1_15.GetRadio()
    this.Parent.oContained.w_TESTO = this.RadioValue()
    return .t.
  endfunc

  func oTESTO_1_15.SetRadio()
    this.Parent.oContained.w_TESTO=trim(this.Parent.oContained.w_TESTO)
    this.value = ;
      iif(this.Parent.oContained.w_TESTO=='S',1,;
      0)
  endfunc

  func oTESTO_1_15.mHide()
    with this.Parent.oContained
      return (.w_STAREG<>'A')
    endwith
  endfunc

  add object oSTAREC_1_16 as StdField with uid="MAKFAOMKNC",rtseq=16,rtrep=.f.,;
    cFormVar = "w_STAREC", cQueryName = "STAREC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Esercizio dell'ultima stampa",;
    HelpContextID = 247716826,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=351, Top=234, InputMask=replicate('X',4), cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_STAREC"

  func oSTAREC_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc


  add object oBtn_1_24 as StdButton with uid="KFNKLGXOTF",left=404, top=323, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 107599130;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_24.Click()
      with this.Parent.oContained
        do GSCE_BR1 with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_24.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_STAREG='A')
     endwith
    endif
  endfunc


  add object oBtn_1_25 as StdButton with uid="VPCHBYKXOV",left=456, top=323, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 100310458;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_25.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_26 as StdButton with uid="GPDMGWTIEJ",left=404, top=323, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 107599130;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_26.Click()
      with this.Parent.oContained
        do GSCE_BRC with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_26.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_STAREG<>'A')
     endwith
    endif
  endfunc

  add object oPREFIS_1_28 as StdField with uid="KRYKQGYOGP",rtseq=18,rtrep=.f.,;
    cFormVar = "w_PREFIS", cQueryName = "PREFIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 24142326,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=351, Top=261, InputMask=replicate('X',20)

  add object oDESATT_1_31 as StdField with uid="OMZKQBSWNW",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DESATT", cQueryName = "DESATT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 52180022,;
   bGlobalFont=.t.,;
    Height=21, Width=267, Left=210, Top=64, InputMask=replicate('X',35)

  add object oDESINI_1_34 as StdField with uid="PIILYQLAMX",rtseq=20,rtrep=.f.,;
    cFormVar = "w_DESINI", cQueryName = "DESINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 138136522,;
   bGlobalFont=.t.,;
    Height=21, Width=267, Left=256, Top=92, InputMask=replicate('X',40)

  add object oDESFIN_1_35 as StdField with uid="IKCFYVLYAL",rtseq=21,rtrep=.f.,;
    cFormVar = "w_DESFIN", cQueryName = "DESFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 59689930,;
   bGlobalFont=.t.,;
    Height=21, Width=267, Left=256, Top=120, InputMask=replicate('X',40)

  add object oINFAGG_1_37 as StdCheck with uid="PFKRGKRCKV",rtseq=22,rtrep=.f.,left=28, top=319, caption="Informazioni aggiuntive",;
    ToolTipText = "Se attivo vengono stampati i valori civili",;
    HelpContextID = 179606138,;
    cFormVar="w_INFAGG", bObbl = .f. , nPag = 1;
    , TabStop=.F.;
   , bGlobalFont=.t.


  func oINFAGG_1_37.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oINFAGG_1_37.GetRadio()
    this.Parent.oContained.w_INFAGG = this.RadioValue()
    return .t.
  endfunc

  func oINFAGG_1_37.SetRadio()
    this.Parent.oContained.w_INFAGG=trim(this.Parent.oContained.w_INFAGG)
    this.value = ;
      iif(this.Parent.oContained.w_INFAGG=='S',1,;
      0)
  endfunc

  func oINFAGG_1_37.mHide()
    with this.Parent.oContained
      return (.w_TIPAMM='C')
    endwith
  endfunc

  add object oGRUCINI_1_38 as StdField with uid="SIYBQTZTDF",rtseq=23,rtrep=.f.,;
    cFormVar = "w_GRUCINI", cQueryName = "GRUCINI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Gruppo contabile inesistente o obsoleto o intervallo scorretto",;
    ToolTipText = "Gruppo contabile di inizio selezione",;
    HelpContextID = 59874970,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=135, Top=148, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="GRU_COCE", cZoomOnZoom="GSCE_AGP", oKey_1_1="GPCODICE", oKey_1_2="this.w_GRUCINI"

  func oGRUCINI_1_38.mHide()
    with this.Parent.oContained
      return (.w_STAREG<>'G')
    endwith
  endfunc

  func oGRUCINI_1_38.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_38('Part',this)
    endwith
    return bRes
  endfunc

  proc oGRUCINI_1_38.ecpDrop(oSource)
    this.Parent.oContained.link_1_38('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGRUCINI_1_38.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRU_COCE','*','GPCODICE',cp_AbsName(this.parent,'oGRUCINI_1_38'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCE_AGP',"Gruppi contabili",'',this.parent.oContained
  endproc
  proc oGRUCINI_1_38.mZoomOnZoom
    local i_obj
    i_obj=GSCE_AGP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GPCODICE=this.parent.oContained.w_GRUCINI
     i_obj.ecpSave()
  endproc

  add object oGRUCFIN_1_39 as StdField with uid="SMFBWLQCTQ",rtseq=24,rtrep=.f.,;
    cFormVar = "w_GRUCFIN", cQueryName = "GRUCFIN",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Gruppo contabile inesistente o obsoleto o intervallo scorretto",;
    ToolTipText = "Gruppo contabile di fine selezione",;
    HelpContextID = 146906778,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=135, Top=176, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="GRU_COCE", cZoomOnZoom="GSCE_AGP", oKey_1_1="GPCODICE", oKey_1_2="this.w_GRUCFIN"

  func oGRUCFIN_1_39.mHide()
    with this.Parent.oContained
      return (.w_STAREG<>'G')
    endwith
  endfunc

  func oGRUCFIN_1_39.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_39('Part',this)
    endwith
    return bRes
  endfunc

  proc oGRUCFIN_1_39.ecpDrop(oSource)
    this.Parent.oContained.link_1_39('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGRUCFIN_1_39.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRU_COCE','*','GPCODICE',cp_AbsName(this.parent,'oGRUCFIN_1_39'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCE_AGP',"Gruppi contabili",'',this.parent.oContained
  endproc
  proc oGRUCFIN_1_39.mZoomOnZoom
    local i_obj
    i_obj=GSCE_AGP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GPCODICE=this.parent.oContained.w_GRUCFIN
     i_obj.ecpSave()
  endproc

  add object oGCDESINI_1_40 as StdField with uid="TJXSWXNNUV",rtseq=25,rtrep=.f.,;
    cFormVar = "w_GCDESINI", cQueryName = "GCDESINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 133217617,;
   bGlobalFont=.t.,;
    Height=21, Width=267, Left=256, Top=148, InputMask=replicate('X',40)

  func oGCDESINI_1_40.mHide()
    with this.Parent.oContained
      return (.w_STAREG<>'G')
    endwith
  endfunc

  add object oGCDESFIN_1_41 as StdField with uid="IPJBXHDMIF",rtseq=26,rtrep=.f.,;
    cFormVar = "w_GCDESFIN", cQueryName = "GCDESFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 183549260,;
   bGlobalFont=.t.,;
    Height=21, Width=267, Left=256, Top=176, InputMask=replicate('X',40)

  func oGCDESFIN_1_41.mHide()
    with this.Parent.oContained
      return (.w_STAREG<>'G')
    endwith
  endfunc

  add object oTOTCAT_1_44 as StdCheck with uid="VXCSXSGEPI",rtseq=27,rtrep=.f.,left=28, top=348, caption="Stampa totali per categoria",;
    ToolTipText = "Se attivo viene stampato un riepilogo dei totali per categoria",;
    HelpContextID = 32395062,;
    cFormVar="w_TOTCAT", bObbl = .f. , nPag = 1;
    , TabStop=.F.;
   , bGlobalFont=.t.


  func oTOTCAT_1_44.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oTOTCAT_1_44.GetRadio()
    this.Parent.oContained.w_TOTCAT = this.RadioValue()
    return .t.
  endfunc

  func oTOTCAT_1_44.SetRadio()
    this.Parent.oContained.w_TOTCAT=trim(this.Parent.oContained.w_TOTCAT)
    this.value = ;
      iif(this.Parent.oContained.w_TOTCAT=='S',1,;
      0)
  endfunc

  func oTOTCAT_1_44.mHide()
    with this.Parent.oContained
      return (.w_STAREG<>'C')
    endwith
  endfunc

  add object oStr_1_17 as StdString with uid="FSVIDNFOFE",Visible=.t., Left=6, Top=36,;
    Alignment=1, Width=124, Height=15,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="DZVNRBLSBH",Visible=.t., Left=28, Top=205,;
    Alignment=0, Width=132, Height=15,;
    Caption="Opzioni di stampa"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="EKMJSDROSS",Visible=.t., Left=351, Top=205,;
    Alignment=0, Width=100, Height=15,;
    Caption="Ultima stampa"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="FTIRZMKBYG",Visible=.t., Left=12, Top=13,;
    Alignment=0, Width=217, Height=15,;
    Caption="Raggruppata per cespite"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_22.mHide()
    with this.Parent.oContained
      return (.w_STAREG<>'C')
    endwith
  endfunc

  add object oStr_1_23 as StdString with uid="CJGQCTZQDJ",Visible=.t., Left=12, Top=13,;
    Alignment=0, Width=217, Height=15,;
    Caption="Raggruppata per categoria"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_23.mHide()
    with this.Parent.oContained
      return (.w_STAREG<>'A')
    endwith
  endfunc

  add object oStr_1_29 as StdString with uid="OOMLVNCMLT",Visible=.t., Left=205, Top=261,;
    Alignment=1, Width=145, Height=18,;
    Caption="Prefisso num. pag.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="USPYXCWWSJ",Visible=.t., Left=6, Top=65,;
    Alignment=1, Width=124, Height=18,;
    Caption="Attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="TKZZPFHVTZ",Visible=.t., Left=6, Top=94,;
    Alignment=1, Width=124, Height=18,;
    Caption="Da categoria:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="NUSUOAUOOL",Visible=.t., Left=6, Top=122,;
    Alignment=1, Width=124, Height=18,;
    Caption="A categoria:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="FSTTNXIVDM",Visible=.t., Left=12, Top=13,;
    Alignment=0, Width=217, Height=15,;
    Caption="Raggruppata per gruppo contabile"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_36.mHide()
    with this.Parent.oContained
      return (.w_STAREG<>'G')
    endwith
  endfunc

  add object oStr_1_42 as StdString with uid="OYETWUMIOH",Visible=.t., Left=6, Top=148,;
    Alignment=1, Width=124, Height=18,;
    Caption="Da gr.contabile:"  ;
  , bGlobalFont=.t.

  func oStr_1_42.mHide()
    with this.Parent.oContained
      return (.w_STAREG<>'G')
    endwith
  endfunc

  add object oStr_1_43 as StdString with uid="WCCVRBMEFA",Visible=.t., Left=6, Top=176,;
    Alignment=1, Width=124, Height=18,;
    Caption="A gr.contabile:"  ;
  , bGlobalFont=.t.

  func oStr_1_43.mHide()
    with this.Parent.oContained
      return (.w_STAREG<>'G')
    endwith
  endfunc

  add object oStr_1_45 as StdString with uid="XCCHILCCZM",Visible=.t., Left=1, Top=391,;
    Alignment=0, Width=723, Height=18,;
    Caption="Se vengono modifcate le condizioni dei bottoni oppure aggiunto un nuovo bottone ricordarsi di modificare l'area manuale Check form"  ;
  , bGlobalFont=.t.

  func oStr_1_45.mHide()
    with this.Parent.oContained
      return (1=1)
    endwith
  endfunc

  add object oBox_1_18 as StdBox with uid="PLJQCEMLDO",left=18, top=221, width=153,height=1

  add object oBox_1_20 as StdBox with uid="PZDTTZKRNN",left=341, top=221, width=162,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsce_src','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
