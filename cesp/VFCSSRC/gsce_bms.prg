* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_bms                                                        *
*              Controllo manutenzione                                          *
*                                                                              *
*      Author: TAM Software & CODELAB                                          *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_140]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-05-11                                                      *
* Last revis.: 2012-10-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsce_bms",oParentObject)
return(i_retval)

define class tgsce_bms as StdBatch
  * --- Local variables
  w_TINCVAL = 0
  w_TIMPRIV = 0
  w_TIMPSVA = 0
  w_VALBEN = 0
  w_GIORNI = 0
  w_ESEPREC = space(4)
  w_TIMPONS = 0
  w_TDECVAL = 0
  w_GIOSER = 0
  * --- WorkFile variables
  TMPVEND1_idx=0
  TMPVEND2_idx=0
  TMPVEND3_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampa Controllo Manutenzione (da GSCE_SCM)
    * --- Variabili che mi servono per calcolare il valore del bene
    * --- Passo queste variabili al report
    L_catego=this.oParentObject.w_catego
    L_iniese=this.oParentObject.w_iniese
    L_codese=this.oParentObject.w_codese
    L_dectot=this.oParentObject.w_dectot
    L_finese=this.oParentObject.w_finese
    L_conto=this.oParentObject.w_conto
    L_stampa=this.oParentObject.w_stampa
    L_DETMOV=this.oParentObject.w_DETMOV
    L_calc_sempl=this.oParentObject.w_calc_sempl
    * --- Calcolo i saldi precedenti all'esercizio impostato
    * --- Create temporary table TMPVEND1
    i_nIdx=cp_AddTableDef('TMPVEND1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('gsce10scm',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPVEND1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Controllo il tipo di stampa selezionato se stampa � uguale a C
    *      lancio la stampa per Categoria mentre se � uguale a D quella per Conto Manutenzione
    if this.oParentObject.w_STAMPA="C"
      * --- Questa query legge i dati delle spese di manutenzione riferite alla categoria cespiti
      *     e li mette in union con TMPVEND1
      * --- Create temporary table TMPVEND2
      i_nIdx=cp_AddTableDef('TMPVEND2') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('gsce16scm',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.TMPVEND2_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      if this.oParentObject.w_DETMOV = "S"
        * --- in questo caso CECODICE contiene il codice della matricola del componente
        * --- Create temporary table TMPVEND3
        i_nIdx=cp_AddTableDef('TMPVEND3') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('..\CESP\EXE\QUERY\GSCE15SCM.VQR',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.TMPVEND3_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        * --- Create temporary table TMPVEND1
        i_nIdx=cp_AddTableDef('TMPVEND1') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('..\CESP\EXE\QUERY\GSCE19SCM_1.VQR',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.TMPVEND1_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        * --- Segno i cespiti che hanno componenti dismessi nell'esercizio
        * --- Write into TMPVEND1
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.TMPVEND1_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
        if i_nConn<>0
          local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
          declare i_aIndex[1]
          i_cQueryTable=cp_getTempTableName(i_nConn)
          i_aIndex(1)="CESPITE,CECODICE"
          do vq_exec with '..\CESP\EXE\QUERY\GSCE21SCM',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPVEND1_idx,i_nConn)	
          i_cDB=cp_GetDatabaseType(i_nConn)
          do case
          case i_cDB="SQLServer"
            i_cWhere="TMPVEND1.CESPITE = _t2.CESPITE";
                  +" and "+"TMPVEND1.CECODICE = _t2.CECODICE";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"TESTCOMP ="+cp_NullLink(cp_ToStrODBC("S"),'TMPVEND1','TESTCOMP');
              +i_ccchkf;
              +" from "+i_cTable+" TMPVEND1, "+i_cQueryTable+" _t2 where "+i_cWhere)
          case i_cDB="MySQL"
            i_cWhere="TMPVEND1.CESPITE = _t2.CESPITE";
                  +" and "+"TMPVEND1.CECODICE = _t2.CECODICE";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND1, "+i_cQueryTable+" _t2 set ";
          +"TMPVEND1.TESTCOMP ="+cp_NullLink(cp_ToStrODBC("S"),'TMPVEND1','TESTCOMP');
              +Iif(Empty(i_ccchkf),"",",TMPVEND1.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
              +" where "+i_cWhere)
          case i_cDB="Oracle"
            i_cWhere="TMPVEND1.CESPITE = t2.CESPITE";
                  +" and "+"TMPVEND1.CECODICE = t2.CECODICE";
            
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND1 set (";
              +"TESTCOMP";
              +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
              +cp_NullLink(cp_ToStrODBC("S"),'TMPVEND1','TESTCOMP')+"";
              +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
              +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
          case i_cDB="PostgreSQL"
            i_cWhere="TMPVEND1.CESPITE = _t2.CESPITE";
                  +" and "+"TMPVEND1.CECODICE = _t2.CECODICE";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND1 set ";
          +"TESTCOMP ="+cp_NullLink(cp_ToStrODBC("S"),'TMPVEND1','TESTCOMP');
              +i_ccchkf;
              +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
          otherwise
            i_cWhere=i_cTable+".CESPITE = "+i_cQueryTable+".CESPITE";
                  +" and "+i_cTable+".CECODICE = "+i_cQueryTable+".CECODICE";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"TESTCOMP ="+cp_NullLink(cp_ToStrODBC("S"),'TMPVEND1','TESTCOMP');
              +i_ccchkf;
              +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          endcase
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        vq_exec("..\CESP\EXE\QUERY\GSCE22SCM.VQR",this,"__tmp__")
        CP_CHPRN("..\CESP\EXE\QUERY\GSCEDSCM.FRX"," ",this)
      else
        vq_exec("..\CESP\EXE\QUERY\GSCE23SCM.VQR",this,"__tmp__")
        CP_CHPRN("..\CESP\EXE\QUERY\GSCE_SCM.FRX"," ",this)
      endif
    else
      * --- Questa query legge i dati delle spese di manutenzione riferite alla categoria cespiti
      * --- Create temporary table TMPVEND2
      i_nIdx=cp_AddTableDef('TMPVEND2') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('gsce25scm',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.TMPVEND2_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      vq_exec("..\CESP\EXE\QUERY\GSCE26SCM.VQR",this,"__tmp__")
      CP_CHPRN("..\CESP\EXE\QUERY\GSCE1SCM.FRX"," ",this)
    endif
    * --- Drop temporary table TMPVEND1
    i_nIdx=cp_GetTableDefIdx('TMPVEND1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPVEND1')
    endif
    * --- Drop temporary table TMPVEND2
    i_nIdx=cp_GetTableDefIdx('TMPVEND2')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPVEND2')
    endif
    * --- Drop temporary table TMPVEND3
    i_nIdx=cp_GetTableDefIdx('TMPVEND3')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPVEND3')
    endif
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Pagina dedicata alla chiusura dei cursori
    if used("__TMP__")
      select ("__TMP__")
      use
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='*TMPVEND1'
    this.cWorkTables[2]='*TMPVEND2'
    this.cWorkTables[3]='*TMPVEND3'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
