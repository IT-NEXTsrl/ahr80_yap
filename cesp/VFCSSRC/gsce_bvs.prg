* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_bvs                                                        *
*              Verifica contabile cespiti                                      *
*                                                                              *
*      Author: TAM Software srl                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_209]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-02-26                                                      *
* Last revis.: 2012-08-31                                                      *
*                                                                              *
* Calcola i Movimenti Progressivi dei Saldi Cespiti                            *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsce_bvs",oParentObject)
return(i_retval)

define class tgsce_bvs as StdBatch
  * --- Local variables
  w_TINCVAL = 0
  w_TIMPRIV = 0
  w_TIMPSVA = 0
  w_TQUOPER = 0
  w_TACCCIV = 0
  w_TACCFIS = 0
  w_TIMPONS = 0
  w_TDECVAL = 0
  w_TIMPNOA = 0
  w_TUTIFIS = 0
  w_TUTICIV = 0
  w_TACCANT = 0
  w_TUTIANT = 0
  w_CONT = 0
  w_CODGRU = space(15)
  w_ANNO = space(4)
  w_VALINI = 0
  w_VALFIN = 0
  w_MOVESE = 0
  w_TINCVAC = 0
  w_TIMPONC = 0
  w_TIMPRIC = 0
  w_TDECVAC = 0
  w_TIMPSVC = 0
  w_VALINC = 0
  w_MOVESC = 0
  w_VALFIC = 0
  w_DESCRI = space(40)
  w_VAUNIVAL = space(3)
  w_ESEPREC = space(4)
  w_VALESPR = space(3)
  w_CAMBIO = 0
  w_SIMBOLO = space(5)
  w_PROVA = space(15)
  w_GPCODICE = space(15)
  w_TIPSEZ = space(1)
  w_CODCON = space(15)
  * --- WorkFile variables
  PAR_CESP_idx=0
  GRU_COCE_idx=0
  ESERCIZI_idx=0
  VALUTE_idx=0
  TMPCESP_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcola i movimenti progressivi degli esercizi precedenti (da GSCE_ASC)
    * --- Variabili lette
    * --- Variabili scritte
    this.w_VAUNIVAL = GETVALUT(this.oParentObject.w_CODVAL, "VAUNIVAL")
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Prepara Tabella Collegamento Contabilita/Cespiti
    vq_exec("..\CESP\EXE\QUERY\GSCE_QVC.VQR",this,"SALDI")
    if USED("SALDI") 
      * --- Creo un cursore raggruppato per Gruppo contabile se i saldi provengono 
      *     dagli esercizi precedenti, in union con lo stesso derivante dai saldi dell'esercizio selezionato
       
 Select Saldi 
 Go Top
      * --- Campo servizio per distinguere i saldi pre-euro (SERVIZIO=0) dai saldi normali. Nei primi infatti ESFINESE � valorizzato con i_datsys
       
 SELECT GPCODICE, MIN(CCCODICE) AS CCCODICE , MIN(SCCODCES) AS SCCODCES, ; 
 SCCODESE, MIN(ESVALNAZ) AS ESVALNAZ, MIN(ESFINESE) AS ESFINESE, ; 
 SUM(SCINCVAL) AS SCINCVAL, SUM(SCDECVAL) AS SCDECVAL, SUM(SCIMPRIV) AS SCIMPRIV, ; 
 SUM(SCIMPNOA) AS SCIMPNOA, SUM(SCIMPONS) AS SCIMPONS, SUM(SCIMPSVA) AS SCIMPSVA, ; 
 SUM(SCQUOPER) AS SCQUOPER, SUM(SCACCCIV) AS SCACCCIV, SUM(SCUTICIV) AS SCUTICIV, ; 
 SUM(SCACCFIS) AS SCACCFIS, SUM(SCUTIFIS) AS SCUTIFIS, SUM(SCACCANT) AS SCACCANT, ; 
 SUM(SCUTIANT) AS SCUTIANT, SUM(SCINCVAC) AS SCINCVAC, ; 
 SUM(SCIMPONC) AS SCIMPONC,SUM(SCDECVAC) AS SCDECVAC,SUM(SCIMPSVC) AS SCIMPSVC,; 
 SUM(SCPLUSVC) AS SCPLUSVC,SUM(SCMINSVC) AS SCMINSVC,SUM(SCIMPRIC) AS SCIMPRIC, MIN(GPDESCRI) AS GPDESCRI ; 
 FROM SALDI WHERE ESFINESE< this.oParentObject.w_INIESE OR SERVIZIO=0 GROUP BY GPCODICE ; 
 UNION ; 
 SELECT GPCODICE, CCCODICE , SCCODCES, ; 
 SCCODESE, ESVALNAZ, ESFINESE, ; 
 SCINCVAL, SCDECVAL, SCIMPRIV, ; 
 SCIMPNOA, SCIMPONS, SCIMPSVA, ; 
 SCQUOPER, SCACCCIV, SCUTICIV, ; 
 SCACCFIS, SCUTIFIS, SCACCANT, SCUTIANT,SCINCVAC, SCIMPONC, SCDECVAC, ; 
 SCIMPSVC, SCPLUSVC,SCMINSVC,SCIMPRIC,GPDESCRI ; 
 FROM SALDI WHERE ESFINESE= this.oParentObject.w_FINESE AND SERVIZIO=1 ; 
 ORDER BY 1,4 INTO CURSOR TOTSALDI 
    endif
    * --- Creazione cursore contenente dati per stampa:
    *     SEZIONE: 'A' Immobilizzazioni, 'B' Fondo ammortamento Civile, 'C' Fondo ammortamento Fiscale  
    *     ORIGINE: 'A' Valore da saldi cespiti, 'B' Valore da saldi contabili
    *     Vengono utilizzati anche per l'ordinamento
    *     Il campo CODCON viene valorizzato con il codice conto contabile nel caso di stampa dettagliata
    CREATE CURSOR DATI(SEZIONE C(1), ORIGINE C(1), GRUPPO C(15), VALINI N(18,5), MOVESE N(18,5), VALFIN N(18,5), ; 
 CODCON C(15), VALINC N(18,5), MOVESC N(18,5), VALFIC N(18,5), GPDESCRI C(40),ANDESCRI C(40) )
    if USED("SALDI")
       
 Select SALDI 
 Use
    endif
    this.w_CONT = 1
    if USED("TOTSALDI")
      ah_Msg("Lettura saldi cespiti...")
      do while this.w_CONT<=3
        this.w_TINCVAL = 0
        this.w_TIMPONS = 0
        this.w_TIMPRIV = 0
        this.w_TDECVAL = 0
        this.w_TIMPSVA = 0
        this.w_TINCVAC = 0
        this.w_TIMPONC = 0
        this.w_TIMPRIC = 0
        this.w_TDECVAC = 0
        this.w_TIMPSVC = 0
        this.w_VALINI = 0
        this.w_VALFIN = 0
        this.w_MOVESE = 0
        this.w_VALINC = 0
        this.w_VALFIC = 0
        this.w_MOVESC = 0
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      enddo
      SELECT DATI 
      GO TOP
       
 SELECT SEZIONE, ORIGINE, GRUPPO , SUM(VALINI) AS VALINI, SUM(MOVESE) AS MOVESE, ; 
 SUM(VALINI)+SUM(MOVESE) AS VALFIN, MIN(CODCON) AS CODCON, SUM(VALINC) AS VALINC, SUM(MOVESC) AS MOVESC, SUM(VALINC)+SUM(MOVESC) AS VALFIC,; 
 MIN(GPDESCRI) AS GPDESCRI,MIN(ANDESCRI) AS ANDESCRI; 
 FROM DATI GROUP BY SEZIONE, ORIGINE, GRUPPO ; 
 ORDER BY 1,2,3 INTO CURSOR DATI 
      A = WRCURSOR("DATI")
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if this.oParentObject.w_DETGRU="S"
        * --- Nel caso di stampa dettagliata Ordino per Sezione per codice Gruppo e poi per Origine
        SELECT * FROM DATI ORDER BY 1,3,2 INTO CURSOR __TMP__
      else
        * --- Nel caso di stampa normale ordino per Sezione per Origine e poi per codice Gruppo
        SELECT * FROM DATI ORDER BY 1,2,3 INTO CURSOR __TMP__
      endif
      SELECT __TMP__
      GO TOP
    endif
    L_CODESE=this.oParentObject.w_CODESE
    if this.oParentObject.w_DETGRU="S"
      CP_CHPRN("..\CESP\EXE\QUERY\GSCEDSVC.FRX", " ", this)
    else
      CP_CHPRN("..\CESP\EXE\QUERY\GSCE_SVC.FRX", " ", this)
    endif
    if USED("TOTSALDI")
      SELECT TOTSALDI 
      USE
    endif
    if USED("DATI")
      SELECT DATI
      USE
    endif
    if USED("SALDICON")
      SELECT SALDICON
      USE
    endif
    if USED("__TMP__")
      SELECT __TMP__
      USE
    endif
    * --- Drop temporary table TMPCESP
    i_nIdx=cp_GetTableDefIdx('TMPCESP')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPCESP')
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Costruzione del cursore per i valori finali
    SELECT TOTSALDI
    GO TOP
    this.w_CODGRU = "NCHFURHDYETEKXC"
    SCAN
    this.w_ANNO = SCCODESE
    * --- Il cursore DATI contiene i valori che mi serviranno per la stampa
    *     In questa pagina inserisco i valori derivanti dai Saldi Cespiti
    *     ORIGINE = 'A'
    do case
      case this.w_CONT=1
        * --- IMMOBILIZZAZIONI: Movimenti nei saldi cespiti
        if this.w_CODGRU <> ALLTRIM(GPCODICE) AND this.w_ANNO <> this.oParentObject.w_CODESE
          this.w_CODGRU = GPCODICE
          this.w_DESCRI = NVL(GPDESCRI,"")
          this.w_ANNO = SCCODESE
          this.w_TINCVAL = NVL(SCINCVAL,0)
          this.w_TIMPONS = NVL(SCIMPONS,0)
          this.w_TIMPRIV = NVL(SCIMPRIV,0)
          this.w_TDECVAL = NVL(SCDECVAL,0)
          this.w_TIMPSVA = NVL(SCIMPSVA,0)
          this.w_TINCVAC = NVL(SCINCVAC,0)
          this.w_TIMPONC = NVL(SCIMPONC,0)
          this.w_TIMPRIC = NVL(SCIMPRIC,0)
          this.w_TDECVAC = NVL(SCDECVAC,0)
          this.w_TIMPSVC = NVL(SCIMPSVC,0)
          this.w_VALINI = this.w_TINCVAL + this.w_TIMPONS + this.w_TIMPRIV - this.w_TDECVAL - this.w_TIMPSVA
          this.w_VALINC = this.w_TINCVAC + this.w_TIMPONC + this.w_TIMPRIC - this.w_TDECVAC - this.w_TIMPSVC
          INSERT INTO DATI VALUES ;
          ("A","A",this.w_CODGRU, this.w_VALINI, 0, 0, SPACE(15), this.w_VALINC,0,0,this.w_DESCRI,SPACE(40)) 
          SELECT TOTSALDI
        else
          this.w_CODGRU = GPCODICE
          this.w_DESCRI = NVL(GPDESCRI,"")
          this.w_ANNO = SCCODESE
          this.w_TINCVAL = NVL(SCINCVAL,0)
          this.w_TIMPONS = NVL(SCIMPONS,0)
          this.w_TIMPRIV = NVL(SCIMPRIV,0)
          this.w_TDECVAL = NVL(SCDECVAL,0)
          this.w_TIMPSVA = NVL(SCIMPSVA,0)
          this.w_TINCVAC = NVL(SCINCVAC,0)
          this.w_TIMPONC = NVL(SCIMPONC,0)
          this.w_TIMPRIC = NVL(SCIMPRIC,0)
          this.w_TDECVAC = NVL(SCDECVAC,0)
          this.w_TIMPSVC = NVL(SCIMPSVC,0)
          this.w_MOVESE = this.w_TINCVAL + this.w_TIMPONS + this.w_TIMPRIV - this.w_TDECVAL - this.w_TIMPSVA
          this.w_MOVESC = this.w_TINCVAC + this.w_TIMPONC + this.w_TIMPRIC - this.w_TDECVAC - this.w_TIMPSVC
          SELECT DATI
          INSERT INTO DATI VALUES ;
          ("A","A",this.w_CODGRU, 0, this.w_MOVESE, 0, SPACE(15),0,this.w_MOVESC,0,this.w_DESCRI,SPACE(40)) 
          SELECT TOTSALDI
        endif
      case this.w_CONT=2
        * --- Accantonamento Civile
        if this.w_CODGRU <> ALLTRIM(GPCODICE) AND this.w_ANNO <> this.oParentObject.w_CODESE
          this.w_CODGRU = GPCODICE
          this.w_DESCRI = NVL(GPDESCRI,"")
          this.w_ANNO = SCCODESE
          this.w_TACCCIV = NVL(SCACCCIV,0)
          this.w_TUTICIV = NVL(SCUTICIV,0)
          this.w_VALINC = this.w_TACCCIV - this.w_TUTICIV
          INSERT INTO DATI VALUES ;
          ("B","A",this.w_CODGRU, 0, 0, 0, SPACE(15),this.w_VALINC,0,0,this.w_DESCRI,SPACE(40)) 
          SELECT TOTSALDI
        else
          this.w_CODGRU = GPCODICE
          this.w_DESCRI = NVL(GPDESCRI,"")
          this.w_ANNO = SCCODESE
          this.w_TACCCIV = NVL(SCACCCIV,0)
          this.w_TUTICIV = NVL(SCUTICIV,0)
          this.w_MOVESC = this.w_TACCCIV - this.w_TUTICIV
          SELECT DATI
          INSERT INTO DATI VALUES ;
          ("B","A",this.w_CODGRU, 0, 0, 0, SPACE(15),0,this.w_MOVESC,0,this.w_DESCRI,SPACE(40)) 
          SELECT TOTSALDI
        endif
      case this.w_CONT=3
        * --- Accantonamento Fiscale
        if this.w_CODGRU <> ALLTRIM(GPCODICE) AND this.w_ANNO <> this.oParentObject.w_CODESE
          this.w_CODGRU = GPCODICE
          this.w_DESCRI = NVL(GPDESCRI,"")
          this.w_ANNO = SCCODESE
          this.w_TACCFIS = NVL(SCACCFIS,0)
          this.w_TUTIFIS = NVL(SCUTIFIS,0)
          this.w_TACCANT = NVL(SCACCANT,0)
          this.w_TUTIANT = NVL(SCUTIANT,0)
          this.w_VALINI = (this.w_TACCFIS + this.w_TACCANT) - (this.w_TUTIFIS + this.w_TUTIANT)
          INSERT INTO DATI VALUES ;
          ("C","A",this.w_CODGRU, this.w_VALINI, 0, 0, SPACE(15),0,0,0,this.w_DESCRI,SPACE(40)) 
          SELECT TOTSALDI
        else
          this.w_CODGRU = GPCODICE
          this.w_DESCRI = NVL(GPDESCRI,"")
          this.w_ANNO = SCCODESE
          this.w_TACCFIS = NVL(SCACCFIS,0)
          this.w_TUTIFIS = NVL(SCUTIFIS,0)
          this.w_TACCANT = NVL(SCACCANT,0)
          this.w_TUTIANT = NVL(SCUTIANT,0)
          this.w_MOVESE = (this.w_TACCFIS + this.w_TACCANT) - (this.w_TUTIFIS + this.w_TUTIANT)
          SELECT DATI
          INSERT INTO DATI VALUES ;
          ("C","A",this.w_CODGRU, 0, this.w_MOVESE, 0, SPACE(15),0,0,0,this.w_DESCRI,SPACE(40)) 
          SELECT TOTSALDI
        endif
    endcase
    SELECT TOTSALDI
    ENDSCAN
    this.w_CONT = this.w_CONT+1
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    ah_Msg("Lettura saldi contabili......")
    this.w_ESEPREC = CALCESPR(i_codazi,this.oParentObject.w_INIESE,.t.)
    if empty(this.w_ESEPREC)
      ah_ErrorMsg("Non esiste un esercizio precedente a %1",,"", this.oParentObject.w_CODESE)
      i_retcode = 'stop'
      return
    endif
    * --- Leggo la valuta dell'esercizio precedente
    * --- Read from ESERCIZI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ESERCIZI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2],.t.,this.ESERCIZI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ESVALNAZ"+;
        " from "+i_cTable+" ESERCIZI where ";
            +"ESCODAZI = "+cp_ToStrODBC(i_CODAZI);
            +" and ESCODESE = "+cp_ToStrODBC(this.w_ESEPREC);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ESVALNAZ;
        from (i_cTable) where;
            ESCODAZI = i_CODAZI;
            and ESCODESE = this.w_ESEPREC;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_VALESPR = NVL(cp_ToDate(_read_.ESVALNAZ),cp_NullValue(_read_.ESVALNAZ))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Se i due Esercizi  hanno valute diverse vado a leggere il Tasso fisso della Valuta dell'esercizio prec.
    if this.oParentObject.w_CODVAL<>this.w_VALESPR
      this.w_CAMBIO = GETCAM(this.w_VALESPR, this.oParentObject.w_INIESE)
      this.w_SIMBOLO = GETVALUT(this.w_VALESPR, "VASIMVAL")
    endif
    vq_exec("..\cesp\exe\query\gsce_bso.vqr",this,"SALDICON")
    * --- La Query mi recupera anche i saldi dell'esercizio precedente
    if this.oParentObject.w_DETGRU="S"
      * --- Nel caso di dettaglio devo mantenere sia il codice Gruppo sia il codice Conto
      *     E' verosimile solo se per ogni gruppo esiste un conto specifico
      SELECT SEZIONE, SLCODICE, CODGRU,MIN(ANDESCRI) AS ANDESCRI,;
      SUM(SLDARINI-SLAVEINI) as VALINI,;
      SUM(((SLDARPRO1+SLDARPER1)-(SLAVEPRO1+SLAVEPER1))-((SLDARINI1-SLAVEINI1)-(SLDARFIN1-SLAVEFIN1))) as VALPREC, ;
      SUM(((SLDARPRO+SLDARPER)-(SLAVEPRO+SLAVEPER))-((SLDARINI-SLAVEINI)-(SLDARFIN-SLAVEFIN))) as MOVESE,;
      SUM(SLDARFIN-SLAVEFIN) as VALFIN;
      from SALDICON GROUP BY SEZIONE,SLCODICE, CODGRU INTO CURSOR SALDICON ORDER BY SEZIONE, SLCODICE, CODGRU
    else
      * --- Nella stampa normale mi serve solo il codice conto
      SELECT SEZIONE, SLCODICE,MIN(ANDESCRI) AS ANDESCRI,;
      SUM(SLDARINI-SLAVEINI) as VALINI,;
      SUM(((SLDARPRO1+SLDARPER1)-(SLAVEPRO1+ SLAVEPER1))-((SLDARINI1-SLAVEINI1)-(SLDARFIN1-SLAVEFIN1))) as VALPREC, ;
      SUM(((SLDARPRO+SLDARPER)-(SLAVEPRO+SLAVEPER))-((SLDARINI-SLAVEINI)-(SLDARFIN-SLAVEFIN))) as MOVESE,;
      SUM(SLDARFIN-SLAVEFIN) as VALFIN;
      from SALDICON GROUP BY SEZIONE,SLCODICE INTO CURSOR SALDICON ORDER BY SEZIONE, SLCODICE
    endif
    SELECT SALDICON
    GO TOP
    SCAN FOR NOT EMPTY(NVL(SLCODICE,""))
    this.w_TIPSEZ = SEZIONE
    if this.oParentObject.w_DETGRU="S"
      * --- Nella stampa dettagliata conservo sia il codice gruppo che il codice conto
      this.w_CODGRU = CODGRU
      this.w_CODCON = SLCODICE
    else
      * --- Per i saldi contabili nel cursore DATI nel campo CODGRU vado a scrivere il
      *     Conto Contabile invece del Gruppo Contabile cespite poich� pi� gruppi contabili
      *     potrebbero usare lo stesso conto per una delle sezioni; in tal caso la stampa risulterebbe fuorviante
      this.w_CODGRU = SLCODICE
      this.w_CODCON = SLCODICE
    endif
    this.w_DESCRI = NVL(ANDESCRI,"")
    this.w_MOVESE = NVL(MOVESE,0)
    this.w_VALINI = IIF(EMPTY(NVL(VALINI,0)), NVL(VALPREC,0), NVL(VALINI,0))
    this.w_VALFIN = IIF(NOT EMPTY(NVL(VALFIN,0)),VALFIN,this.w_VALINI + this.w_MOVESE)
    * --- Inserisco nel cursore DATI i valori derivanti dai Saldi Contabili 
    *     ORIGINE = 'B'
    INSERT INTO DATI VALUES ;
    (this.w_TIPSEZ,"B",this.w_CODGRU, 0, 0, 0, this.w_CODCON,this.w_VALINI,this.w_MOVESE,this.w_VALFIN,SPACE(40),this.w_DESCRI) 
    SELECT SALDICON
    ENDSCAN
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Crea Tabella Temporanea
    ah_Msg("Verifica conti contabili......")
    * --- Create temporary table TMPCESP
    i_nIdx=cp_AddTableDef('TMPCESP') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('..\CESP\EXE\QUERY\GSCE_TMP',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPCESP_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Costruisco gli array PARC per contenere il tipo sezione dai Parametri Cespiti
    *     e GRUC per contenere il gruppo contabile cespite
    DIMENSION PARC[16], GRUC[16]
    PARC[1] = SPACE(1)
    PARC[2] = SPACE(1)
    PARC[3] = SPACE(1)
    PARC[4] = SPACE(1)
    PARC[5] = SPACE(1)
    PARC[6] = SPACE(1)
    PARC[7] = SPACE(1)
    PARC[8] = SPACE(1)
    PARC[9] = SPACE(1)
    PARC[10] = SPACE(1)
    PARC[11] = SPACE(1)
    PARC[12] = SPACE(1)
    PARC[13] = SPACE(1)
    PARC[14] = SPACE(1)
    PARC[15] = SPACE(1)
    PARC[16] = SPACE(1)
    * --- Read from PAR_CESP
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_CESP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_CESP_idx,2],.t.,this.PAR_CESP_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PCTIPC01,PCTIPC02,PCTIPC03,PCTIPC04,PCTIPC05,PCTIPC06,PCTIPC07,PCTIPC08,PCTIPC09,PCTIPC10,PCTIPC11,PCTIPC12,PCTIPC13,PCTIPC14,PCTIPC15,PCTIPC16"+;
        " from "+i_cTable+" PAR_CESP where ";
            +"PCCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PCTIPC01,PCTIPC02,PCTIPC03,PCTIPC04,PCTIPC05,PCTIPC06,PCTIPC07,PCTIPC08,PCTIPC09,PCTIPC10,PCTIPC11,PCTIPC12,PCTIPC13,PCTIPC14,PCTIPC15,PCTIPC16;
        from (i_cTable) where;
            PCCODAZI = this.oParentObject.w_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      PARC[1] = NVL(cp_ToDate(_read_.PCTIPC01),cp_NullValue(_read_.PCTIPC01))
      PARC[2] = NVL(cp_ToDate(_read_.PCTIPC02),cp_NullValue(_read_.PCTIPC02))
      PARC[3] = NVL(cp_ToDate(_read_.PCTIPC03),cp_NullValue(_read_.PCTIPC03))
      PARC[4] = NVL(cp_ToDate(_read_.PCTIPC04),cp_NullValue(_read_.PCTIPC04))
      PARC[5] = NVL(cp_ToDate(_read_.PCTIPC05),cp_NullValue(_read_.PCTIPC05))
      PARC[6] = NVL(cp_ToDate(_read_.PCTIPC06),cp_NullValue(_read_.PCTIPC06))
      PARC[7] = NVL(cp_ToDate(_read_.PCTIPC07),cp_NullValue(_read_.PCTIPC07))
      PARC[8] = NVL(cp_ToDate(_read_.PCTIPC08),cp_NullValue(_read_.PCTIPC08))
      PARC[9] = NVL(cp_ToDate(_read_.PCTIPC09),cp_NullValue(_read_.PCTIPC09))
      PARC[10] = NVL(cp_ToDate(_read_.PCTIPC10),cp_NullValue(_read_.PCTIPC10))
      PARC[11] = NVL(cp_ToDate(_read_.PCTIPC11),cp_NullValue(_read_.PCTIPC11))
      PARC[12] = NVL(cp_ToDate(_read_.PCTIPC12),cp_NullValue(_read_.PCTIPC12))
      PARC[13] = NVL(cp_ToDate(_read_.PCTIPC13),cp_NullValue(_read_.PCTIPC13))
      PARC[14] = NVL(cp_ToDate(_read_.PCTIPC14),cp_NullValue(_read_.PCTIPC14))
      PARC[15] = NVL(cp_ToDate(_read_.PCTIPC15),cp_NullValue(_read_.PCTIPC15))
      PARC[16] = NVL(cp_ToDate(_read_.PCTIPC16),cp_NullValue(_read_.PCTIPC16))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Select from GRU_COCE
    i_nConn=i_TableProp[this.GRU_COCE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.GRU_COCE_idx,2],.t.,this.GRU_COCE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" GRU_COCE ";
          +" order by GPCODICE";
           ,"_Curs_GRU_COCE")
    else
      select * from (i_cTable);
       order by GPCODICE;
        into cursor _Curs_GRU_COCE
    endif
    if used('_Curs_GRU_COCE')
      select _Curs_GRU_COCE
      locate for 1=1
      do while not(eof())
      GRUC[1] = SPACE(15)
      GRUC[2] = SPACE(15)
      GRUC[3] = SPACE(15)
      GRUC[4] = SPACE(15)
      GRUC[5] = SPACE(15)
      GRUC[6] = SPACE(15)
      GRUC[7] = SPACE(15)
      GRUC[8] = SPACE(15)
      GRUC[9] = SPACE(15)
      GRUC[10] = SPACE(15)
      GRUC[11] = SPACE(15)
      GRUC[12] = SPACE(15)
      GRUC[13] = SPACE(15)
      GRUC[14] = SPACE(15)
      GRUC[15] = SPACE(15)
      GRUC[16] = SPACE(15)
      this.w_GPCODICE = NVL(_Curs_GRU_COCE.GPCODICE, "")
      if NOT EMPTY(this.w_GPCODICE)
        GRUC[1] = NVL(_Curs_GRU_COCE.GPCONC01, SPACE(15))
        GRUC[2] = NVL(_Curs_GRU_COCE.GPCONC02, SPACE(15))
        GRUC[3] = NVL(_Curs_GRU_COCE.GPCONC03, SPACE(15))
        GRUC[4] = NVL(_Curs_GRU_COCE.GPCONC04, SPACE(15))
        GRUC[5] = NVL(_Curs_GRU_COCE.GPCONC05, SPACE(15))
        GRUC[6] = NVL(_Curs_GRU_COCE.GPCONC06, SPACE(15))
        GRUC[7] = NVL(_Curs_GRU_COCE.GPCONC07, SPACE(15))
        GRUC[8] = NVL(_Curs_GRU_COCE.GPCONC08, SPACE(15))
        GRUC[9] = NVL(_Curs_GRU_COCE.GPCONC09, SPACE(15))
        GRUC[10] = NVL(_Curs_GRU_COCE.GPCONC10, SPACE(15))
        GRUC[11] = NVL(_Curs_GRU_COCE.GPCONC11, SPACE(15))
        GRUC[12] = NVL(_Curs_GRU_COCE.GPCONC12, SPACE(15))
        GRUC[13] = NVL(_Curs_GRU_COCE.GPCONC13, SPACE(15))
        GRUC[14] = NVL(_Curs_GRU_COCE.GPCONC14, SPACE(15))
        GRUC[15] = NVL(_Curs_GRU_COCE.GPCONC15, SPACE(15))
        GRUC[16] = NVL(_Curs_GRU_COCE.GPCONC16, SPACE(15))
        FOR L_i = 1 TO 16
        this.w_TIPSEZ = PARC[L_i]
        this.w_CODCON = GRUC[L_i]
        if this.w_TIPSEZ $ "ICF" AND NOT EMPTY(this.w_CODCON)
          this.w_TIPSEZ = IIF(this.w_TIPSEZ="I", "A", IIF(this.w_TIPSEZ="C", "B", "C"))
          * --- Read from TMPCESP
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.TMPCESP_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPCESP_idx,2],.t.,this.TMPCESP_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CODGRU"+;
              " from "+i_cTable+" TMPCESP where ";
                  +"SEZIONE = "+cp_ToStrODBC(this.w_TIPSEZ);
                  +" and CODCON = "+cp_ToStrODBC(this.w_CODCON);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CODGRU;
              from (i_cTable) where;
                  SEZIONE = this.w_TIPSEZ;
                  and CODCON = this.w_CODCON;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_PROVA = NVL(cp_ToDate(_read_.CODGRU),cp_NullValue(_read_.CODGRU))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if i_Rows=0
            * --- Inserisco nella tabella temporanea i collegamenti fra sezione, conto contabile e gruppo contabile cespite
            * --- Insert into TMPCESP
            i_nConn=i_TableProp[this.TMPCESP_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPCESP_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMPCESP_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"SEZIONE"+",CODCON"+",CODGRU"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.w_TIPSEZ),'TMPCESP','SEZIONE');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CODCON),'TMPCESP','CODCON');
              +","+cp_NullLink(cp_ToStrODBC(this.w_GPCODICE),'TMPCESP','CODGRU');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'SEZIONE',this.w_TIPSEZ,'CODCON',this.w_CODCON,'CODGRU',this.w_GPCODICE)
              insert into (i_cTable) (SEZIONE,CODCON,CODGRU &i_ccchkf. );
                 values (;
                   this.w_TIPSEZ;
                   ,this.w_CODCON;
                   ,this.w_GPCODICE;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          endif
        endif
        ENDFOR
      endif
        select _Curs_GRU_COCE
        continue
      enddo
      use
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='PAR_CESP'
    this.cWorkTables[2]='GRU_COCE'
    this.cWorkTables[3]='ESERCIZI'
    this.cWorkTables[4]='VALUTE'
    this.cWorkTables[5]='*TMPCESP'
    return(this.OpenAllTables(5))

  proc CloseCursors()
    if used('_Curs_GRU_COCE')
      use in _Curs_GRU_COCE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
