* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_bam                                                        *
*              Elab.zoom abbina.mov.cespiti                                    *
*                                                                              *
*      Author: Zucchetti TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_128]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-04-13                                                      *
* Last revis.: 2006-12-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsce_bam",oParentObject,m.pOper)
return(i_retval)

define class tgsce_bam as StdBatch
  * --- Local variables
  pOper = space(4)
  w_CPROWNUM = 0
  w_TDCAUCES = space(5)
  w_ZOOM = space(10)
  w_ZOOM2 = space(10)
  w_DATCON = ctod("  /  /  ")
  w_RIFCON = space(10)
  w_MESS = space(100)
  w_CANCEL = .f.
  w_SERIAL = space(10)
  w_PROG = .NULL.
  w_MVFLVEAC = space(1)
  w_MVCLADOC = space(2)
  w_oERRORLOG = .NULL.
  w_CSERIAL = space(10)
  w_PSERIAL = space(10)
  w_DATREG = ctod("  /  /  ")
  w_CDATREG = ctod("  /  /  ")
  w_NUMRER = 0
  w_CNUMREG = 0
  w_CODESE = space(4)
  w_CCODESE = space(4)
  w_PNCLFO = space(15)
  w_MCDATDOC = ctod("  /  /  ")
  w_PNDATA = ctod("  /  /  ")
  w_MCALFDOC = space(10)
  w_PNALFD = space(10)
  w_MCNUMDOC = 0
  w_PNNUMD = 0
  w_CPROW = 0
  w_ERRORE = .f.
  w_MESS1 = space(10)
  w_TEST = space(10)
  w_DSERIAL = space(10)
  w_MVDATREG = ctod("  /  /  ")
  w_CDATREG = ctod("  /  /  ")
  w_MVNUMREG = 0
  w_CNUMREG = 0
  w_MVCODESE = space(4)
  w_CCODESE = space(4)
  w_MCCODCON = space(15)
  w_MVCODCON = space(15)
  w_MCDATDOC = ctod("  /  /  ")
  w_MVDATDOC = ctod("  /  /  ")
  w_MVALFDOC = space(10)
  w_MVNUMDOC = 0
  w_MCCAUCES = space(5)
  w_MCCODCES = space(20)
  w_MVCODART = space(20)
  w_TIPART = space(2)
  w_TIPCES = space(2)
  w_STOP = .f.
  w_CPROWORD = 0
  w_MESS1 = space(10)
  * --- WorkFile variables
  MOV_CESP_idx=0
  PNT_MAST_idx=0
  PNT_DETT_idx=0
  SALDICON_idx=0
  MOVICOST_idx=0
  PNT_CESP_idx=0
  DOC_MAST_idx=0
  DOC_DETT_idx=0
  ART_ICOL_idx=0
  CES_PITI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gsetione maschera abbinamento movimenti cespiti
    * --- Parametro operazione
    * --- Variabili maschera
    * --- Variabili Locali
    this.w_ZOOM = this.oParentObject.w_ZoomGepr
    this.w_ZOOM2 = this.oParentObject.w_Zoom
    this.w_DATCON = cp_CharToDate("  -  -    ")
    this.w_RIFCON = space(10)
    this.w_MESS = space(100)
    this.w_CANCEL = .F.
    this.w_SERIAL = space(10)
    * --- Stanzia l'oggetto per la gestione delle messaggistiche di errore
    this.w_oERRORLOG=createobject("AH_ErrorLog")
    do case
      case this.pOper="ABB"
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pOper="CAR"
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pOper="RMC"
        * --- Primanota
        this.w_PROG = GSCE_AMC()
        * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
        if !(this.w_PROG.bSec1)
          i_retcode = 'stop'
          return
        endif
        this.w_PROG.w_MCSERIAL = this.oParentObject.w_MCSERIAL
        this.w_PROG.QueryKeySet("MCSERIAL='"+this.oParentObject.w_MCSERIAL+"'" ,"")     
        * --- carico il record
        this.w_PROG.LoadRec()     
      case this.pOper="RPN"
        * --- Primanota
        gsar_bzp(this,this.oParentObject.w_PNSERIAL)
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pOper="ABD"
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pOper="RDO"
        * --- Gestione Documenti
        gsar_bzm(this,this.oParentObject.w_MVSERIAL,-20)
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pOper="DES"
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Abbina Movimenti Cespiti
    NC = This.OparentObject.w_ZoomGepr.cCursor
    NP = This.OparentObject.w_Zoom.cCursor
    SELECT (NC)
    GO TOP
    SCAN FOR XCHK=1
    this.w_CSERIAL = NVL(MCSERIAL,SPACE(10))
    * --- Read from MOV_CESP
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.MOV_CESP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOV_CESP_idx,2],.t.,this.MOV_CESP_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "MCCODESE,MCNUMREG,MCDATREG,MCNUMDOC,MCALFDOC,MCDATDOC,MCCODCON"+;
        " from "+i_cTable+" MOV_CESP where ";
            +"MCSERIAL = "+cp_ToStrODBC(this.w_CSERIAL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        MCCODESE,MCNUMREG,MCDATREG,MCNUMDOC,MCALFDOC,MCDATDOC,MCCODCON;
        from (i_cTable) where;
            MCSERIAL = this.w_CSERIAL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CCODESE = NVL(cp_ToDate(_read_.MCCODESE),cp_NullValue(_read_.MCCODESE))
      this.w_CNUMREG = NVL(cp_ToDate(_read_.MCNUMREG),cp_NullValue(_read_.MCNUMREG))
      this.w_CDATREG = NVL(cp_ToDate(_read_.MCDATREG),cp_NullValue(_read_.MCDATREG))
      this.w_MCNUMDOC = NVL(cp_ToDate(_read_.MCNUMDOC),cp_NullValue(_read_.MCNUMDOC))
      this.w_MCALFDOC = NVL(cp_ToDate(_read_.MCALFDOC),cp_NullValue(_read_.MCALFDOC))
      this.w_MCDATDOC = NVL(cp_ToDate(_read_.MCDATDOC),cp_NullValue(_read_.MCDATDOC))
      this.w_MCCODCON = NVL(cp_ToDate(_read_.MCCODCON),cp_NullValue(_read_.MCCODCON))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    SELECT * FROM (NP) INTO CURSOR TEMP WHERE XCHK=1 AND (this.w_MCNUMDOC<>NVL(PNNUMDOC,0) OR this.w_MCALFDOC<>NVL(PNALFDOC,Space(10)) OR this.w_MCDATDOC<>NVL(PNDATDOC,cp_CharToDate("  -  -  ")) OR this.w_MCCODCON<>NVL(PNCODCLF,SPACE(15)))
    SELECT TEMP 
    GO TOP 
    SCAN
    this.w_PSERIAL = NVL(PNSERIAL,SPACE(10))
    * --- Read from PNT_MAST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PNT_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2],.t.,this.PNT_MAST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PNCOMPET,PNNUMRER,PNDATREG,PNNUMDOC,PNDATDOC,PNALFDOC,PNCODCLF"+;
        " from "+i_cTable+" PNT_MAST where ";
            +"PNSERIAL = "+cp_ToStrODBC(this.w_PSERIAL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PNCOMPET,PNNUMRER,PNDATREG,PNNUMDOC,PNDATDOC,PNALFDOC,PNCODCLF;
        from (i_cTable) where;
            PNSERIAL = this.w_PSERIAL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CODESE = NVL(cp_ToDate(_read_.PNCOMPET),cp_NullValue(_read_.PNCOMPET))
      this.w_NUMRER = NVL(cp_ToDate(_read_.PNNUMRER),cp_NullValue(_read_.PNNUMRER))
      this.w_DATREG = NVL(cp_ToDate(_read_.PNDATREG),cp_NullValue(_read_.PNDATREG))
      this.w_PNNUMD = NVL(cp_ToDate(_read_.PNNUMDOC),cp_NullValue(_read_.PNNUMDOC))
      this.w_PNDATA = NVL(cp_ToDate(_read_.PNDATDOC),cp_NullValue(_read_.PNDATDOC))
      this.w_PNALFD = NVL(cp_ToDate(_read_.PNALFDOC),cp_NullValue(_read_.PNALFDOC))
      this.w_PNCLFO = NVL(cp_ToDate(_read_.PNCODCLF),cp_NullValue(_read_.PNCODCLF))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Controlla la congruenza dell'abbinamento: N: Doc, Alfa Doc, Data Doc e Cli/For
    if this.w_MCNUMDOC<>this.w_PNNUMD OR this.w_MCALFDOC<>this.w_PNALFD OR this.w_MCDATDOC<>this.w_PNDATA OR this.w_MCCODCON<>this.w_PNCLFO
      this.w_oERRORLOG.AddMsgLog("Numero, data, serie, o cliente/fornitore del doc. n.%1 del %2 non congruente%0con numero, data, serie, o cliente/fornitore della registrazione contabile con doc. n.%3 del %4", ALLTRIM(STR(this.w_MCNUMDOC,15,0))+ IIF(NOT EMPTY(this.w_MCALFDOC),"/"+Alltrim(this.w_MCALFDOC), ""), DTOC(this.w_MCDATDOC), ALLTRIM(STR(this.w_PNNUMD,15,0))+ IIF(NOT EMPTY(this.w_PNALFD),"/"+Alltrim(this.w_PNALFD), ""), DTOC(this.w_PNDATA) )     
    endif
    ENDSCAN
    SELECT (NC)
    ENDSCAN
    * --- LOG Errori
    if this.w_oErrorLog.IsFullLog()
      ah_ErrorMsg("Sono state riscontrate delle incongruenze nei movimenti da abbinare")
      this.w_oERRORLOG.PrintLog(this,"Errori riscontrati")     
    endif
    SELECT * FROM (NC) INTO CURSOR CESP WHERE XCHK=1 
    SELECT * FROM (NP) INTO CURSOR PNOT WHERE XCHK=1 
    if RECCOUNT("CESP")>0 AND RECCOUNT("PNOT")>0
      if ah_YesNo("Effettuo gli abbinamenti selezionati?")
        * --- Try
        local bErr_03A4E9F0
        bErr_03A4E9F0=bTrsErr
        this.Try_03A4E9F0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          ah_ErrorMsg("Errore nell'aggiornamento della tabella PTN_CESP")
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
        endif
        bTrsErr=bTrsErr or bErr_03A4E9F0
        * --- End
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    if USED("TEMP")
      SELECT TEMP
      USE
    endif
    if USED("CESP")
      SELECT CESP
      USE
    endif
    if USED("PNOT")
      SELECT PNOT
      USE
    endif
  endproc
  proc Try_03A4E9F0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    SELECT CESP
    GO TOP
    SCAN
    this.w_CSERIAL = NVL(MCSERIAL,SPACE(10))
    SELECT PNOT
    GO TOP 
    SCAN
    this.w_PSERIAL = NVL(PNSERIAL,SPACE(10))
    this.w_CPROW = 0
    * --- Calcolo il CPROWORD pi� alto
    * --- Select from PNT_CESP
    i_nConn=i_TableProp[this.PNT_CESP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PNT_CESP_idx,2],.t.,this.PNT_CESP_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select CPROWORD  from "+i_cTable+" PNT_CESP ";
          +" where ACSERIAL="+cp_ToStrODBC(this.w_PSERIAL)+"";
          +" order by CPROWORD DESC";
           ,"_Curs_PNT_CESP")
    else
      select CPROWORD from (i_cTable);
       where ACSERIAL=this.w_PSERIAL;
       order by CPROWORD DESC;
        into cursor _Curs_PNT_CESP
    endif
    if used('_Curs_PNT_CESP')
      select _Curs_PNT_CESP
      locate for 1=1
      do while not(eof())
      this.w_CPROW = _Curs_PNT_CESP.CPROWORD
      EXIT
        select _Curs_PNT_CESP
        continue
      enddo
      use
    endif
    this.w_CPROW = this.w_CPROW + 10
    * --- Controllo che lo stesso movimento non sia gi� stato abbinato manualmente
    * --- Read from PNT_CESP
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PNT_CESP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PNT_CESP_idx,2],.t.,this.PNT_CESP_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ACMOVCES"+;
        " from "+i_cTable+" PNT_CESP where ";
            +"ACSERIAL = "+cp_ToStrODBC(this.w_PSERIAL);
            +" and ACMOVCES = "+cp_ToStrODBC(this.w_CSERIAL);
            +" and ACTIPASS = "+cp_ToStrODBC("M");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ACMOVCES;
        from (i_cTable) where;
            ACSERIAL = this.w_PSERIAL;
            and ACMOVCES = this.w_CSERIAL;
            and ACTIPASS = "M";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_TEST = NVL(cp_ToDate(_read_.ACMOVCES),cp_NullValue(_read_.ACMOVCES))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if i_ROWS=0
      * --- Insert into PNT_CESP
      i_nConn=i_TableProp[this.PNT_CESP_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PNT_CESP_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PNT_CESP_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"ACSERIAL"+",ACMOVCES"+",ACTIPASS"+",CPROWORD"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_PSERIAL),'PNT_CESP','ACSERIAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CSERIAL),'PNT_CESP','ACMOVCES');
        +","+cp_NullLink(cp_ToStrODBC("M"),'PNT_CESP','ACTIPASS');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CPROW),'PNT_CESP','CPROWORD');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'ACSERIAL',this.w_PSERIAL,'ACMOVCES',this.w_CSERIAL,'ACTIPASS',"M",'CPROWORD',this.w_CPROW)
        insert into (i_cTable) (ACSERIAL,ACMOVCES,ACTIPASS,CPROWORD &i_ccchkf. );
           values (;
             this.w_PSERIAL;
             ,this.w_CSERIAL;
             ,"M";
             ,this.w_CPROW;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    else
      this.w_MESS1 = "Movimento cespite gi� associato alla registrazione contabile selezionata%0Impossibile rieseguire l'associazione"
      ah_ErrorMsg(this.w_MESS1)
    endif
    ENDSCAN
    SELECT CESP
    ENDSCAN
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    CREATE CURSOR MessErr (MSG M(10))
    NC = This.OparentObject.w_ZoomGepr.cCursor
    ND = This.OparentObject.w_Zoom.cCursor
    SELECT (NC)
    GO TOP
    SCAN FOR XCHK=1
    this.w_CSERIAL = NVL(MCSERIAL,SPACE(10))
    * --- Read from MOV_CESP
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.MOV_CESP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOV_CESP_idx,2],.t.,this.MOV_CESP_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "MCCODESE,MCNUMREG,MCDATREG,MCNUMDOC,MCALFDOC,MCDATDOC,MCCODCON,MCCODCAU,MCCODCES"+;
        " from "+i_cTable+" MOV_CESP where ";
            +"MCSERIAL = "+cp_ToStrODBC(this.w_CSERIAL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        MCCODESE,MCNUMREG,MCDATREG,MCNUMDOC,MCALFDOC,MCDATDOC,MCCODCON,MCCODCAU,MCCODCES;
        from (i_cTable) where;
            MCSERIAL = this.w_CSERIAL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CCODESE = NVL(cp_ToDate(_read_.MCCODESE),cp_NullValue(_read_.MCCODESE))
      this.w_CNUMREG = NVL(cp_ToDate(_read_.MCNUMREG),cp_NullValue(_read_.MCNUMREG))
      this.w_CDATREG = NVL(cp_ToDate(_read_.MCDATREG),cp_NullValue(_read_.MCDATREG))
      this.w_MCNUMDOC = NVL(cp_ToDate(_read_.MCNUMDOC),cp_NullValue(_read_.MCNUMDOC))
      this.w_MCALFDOC = NVL(cp_ToDate(_read_.MCALFDOC),cp_NullValue(_read_.MCALFDOC))
      this.w_MCDATDOC = NVL(cp_ToDate(_read_.MCDATDOC),cp_NullValue(_read_.MCDATDOC))
      this.w_MCCODCON = NVL(cp_ToDate(_read_.MCCODCON),cp_NullValue(_read_.MCCODCON))
      this.w_MCCAUCES = NVL(cp_ToDate(_read_.MCCODCAU),cp_NullValue(_read_.MCCODCAU))
      this.w_MCCODCES = NVL(cp_ToDate(_read_.MCCODCES),cp_NullValue(_read_.MCCODCES))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from CES_PITI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CES_PITI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CES_PITI_idx,2],.t.,this.CES_PITI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CETIPCES"+;
        " from "+i_cTable+" CES_PITI where ";
            +"CECODICE = "+cp_ToStrODBC(this.w_MCCODCES);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CETIPCES;
        from (i_cTable) where;
            CECODICE = this.w_MCCODCES;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_TIPCES = NVL(cp_ToDate(_read_.CETIPCES),cp_NullValue(_read_.CETIPCES))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    SELECT * FROM (ND) INTO CURSOR TEMP WHERE XCHK=1 AND (this.w_MCNUMDOC<>NVL(MVNUMDOC,0) OR this.w_MCALFDOC<>NVL(MVALFDOC,"  ") OR this.w_MCDATDOC<>NVL(MVDATDOC,cp_CharToDate("  -  -  ")) OR this.w_MCCODCON<>NVL(MVCODCON,SPACE(15)) OR (NVL(TDCAUCES,SPACE(5))<>this.w_MCCAUCES AND NOT EMPTY(NVL(TDCAUCES,SPACE(5)))) OR ((this.w_TIPCES="CQ" AND "FM"<>NVL(TIPART,"  ")) OR (this.w_TIPCES<>"CQ" AND "FO"<>NVL(TIPART,"  "))))
    SELECT TEMP 
    GO TOP 
    SCAN
    this.w_DSERIAL = NVL(MVSERIAL,SPACE(10))
    this.w_TDCAUCES = NVL(TDCAUCES, SPACE(5))
    this.w_TIPART = NVL(TIPART,"  ")
    this.w_CPROWNUM = NVL(CPROWNUM,0)
    this.w_CPROWORD = NVL(CPROWORD,0)
    * --- Read from DOC_MAST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "MVCODESE,MVNUMREG,MVDATREG,MVNUMDOC,MVDATDOC,MVALFDOC,MVCODCON"+;
        " from "+i_cTable+" DOC_MAST where ";
            +"MVSERIAL = "+cp_ToStrODBC(this.w_DSERIAL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        MVCODESE,MVNUMREG,MVDATREG,MVNUMDOC,MVDATDOC,MVALFDOC,MVCODCON;
        from (i_cTable) where;
            MVSERIAL = this.w_DSERIAL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_MVCODESE = NVL(cp_ToDate(_read_.MVCODESE),cp_NullValue(_read_.MVCODESE))
      this.w_MVNUMREG = NVL(cp_ToDate(_read_.MVNUMREG),cp_NullValue(_read_.MVNUMREG))
      this.w_MVDATREG = NVL(cp_ToDate(_read_.MVDATREG),cp_NullValue(_read_.MVDATREG))
      this.w_MVNUMDOC = NVL(cp_ToDate(_read_.MVNUMDOC),cp_NullValue(_read_.MVNUMDOC))
      this.w_MVDATDOC = NVL(cp_ToDate(_read_.MVDATDOC),cp_NullValue(_read_.MVDATDOC))
      this.w_MVALFDOC = NVL(cp_ToDate(_read_.MVALFDOC),cp_NullValue(_read_.MVALFDOC))
      this.w_MVCODCON = NVL(cp_ToDate(_read_.MVCODCON),cp_NullValue(_read_.MVCODCON))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Controlla la congruenza dell'abbinamento: N: Doc, Alfa Doc, Data Doc e Cli/For
    if this.w_MCNUMDOC<>this.w_MVNUMDOC OR this.w_MCALFDOC<>this.w_MVALFDOC OR this.w_MCDATDOC<>this.w_MVDATDOC OR this.w_MCCODCON<>this.w_MVCODCON OR (this.w_TDCAUCES<>this.w_MCCAUCES AND NOT EMPTY(this.w_TDCAUCES))
      this.w_oERRORLOG.AddMsgLog("Numero, data, serie, cliente/fornitore o causale cespite del mov.cesp con doc. n.%1 del %2 non congruente%0con numero, data, serie, cliente/fornitore o causale cespite del doc. n.%3 riga %4 del %5", ALLTRIM(STR(this.w_MCNUMDOC,15,0))+ IIF(NOT EMPTY(this.w_MCALFDOC),"/"+Alltrim(this.w_MCALFDOC), ""), DTOC(this.w_MCDATDOC), ALLTRIM(STR(this.w_MVNUMDOC,15,0))+ IIF(NOT EMPTY(this.w_MVALFDOC),"/"+Alltrim(this.w_MVALFDOC), ""), ALLTRIM(STR(this.w_CPROWORD)) , DTOC(this.w_MVDATDOC))     
    endif
    * --- controllo tipo articolo (quantit� e valore o valore) con tipo cespite
    if (this.w_TIPCES="CQ" AND "FM"<>this.w_TIPART) OR (this.w_TIPCES<>"CQ" AND "FO"<>this.w_TIPART)
      this.w_oERRORLOG.AddMsgLog("Tipo cespite del mov. cespite con doc. n.%1 del %2 non congruente%0con il tipo articolo del doc. n. %3 riga %4 del %5%0L'abbinamento non sar� consentito", ALLTRIM(STR(this.w_MCNUMDOC,15,0))+ IIF(NOT EMPTY(this.w_MCALFDOC),"/"+Alltrim(this.w_MCALFDOC), ""), DTOC(this.w_MCDATDOC), ALLTRIM(STR(this.w_MVNUMDOC,15,0))+ IIF(NOT EMPTY(this.w_MVALFDOC),"/"+Alltrim(this.w_MVALFDOC), ""), ALLTRIM(STR(this.w_CPROWORD)) , DTOC(this.w_MVDATDOC))     
      this.w_STOP = .T.
    endif
    ENDSCAN
    SELECT (NC)
    ENDSCAN
    * --- LOG Errori
    if this.w_oErrorLog.IsFullLog()
      ah_ErrorMsg("Sono state riscontrate delle incongruenze nei movimenti da abbinare")
      this.w_oERRORLOG.PrintLog(this,"Errori riscontrati")     
    endif
    if this.w_STOP
      ah_ErrorMsg("Sono state riscontrate delle incongruenze bloccanti nei movimenti da abbinare. Impossibile effettuare gli abbinamenti",,"")
      if USED("TEMP")
        SELECT TEMP
        USE
      endif
      if USED("CESP")
        SELECT CESP
        USE
      endif
      if USED("DOCU")
        SELECT DOCU
        USE
      endif
      i_retcode = 'stop'
      return
    endif
    SELECT * FROM (NC) INTO CURSOR CESP WHERE XCHK=1 
    SELECT * FROM (ND) INTO CURSOR DOCU WHERE XCHK=1 
    if RECCOUNT("CESP")>0 AND RECCOUNT("DOCU")>0
      if ah_YesNo("Effettuo gli abbinamenti selezionati?")
        * --- Try
        local bErr_03A94998
        bErr_03A94998=bTrsErr
        this.Try_03A94998()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          ah_ErrorMsg("Errore nell'aggiornamento della tabella DOC_DETT")
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
        endif
        bTrsErr=bTrsErr or bErr_03A94998
        * --- End
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    if USED("TEMP")
      SELECT TEMP
      USE
    endif
    if USED("CESP")
      SELECT CESP
      USE
    endif
    if USED("DOCU")
      SELECT DOCU
      USE
    endif
  endproc
  proc Try_03A94998()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    SELECT CESP
    GO TOP
    SCAN
    this.w_CSERIAL = NVL(MCSERIAL,SPACE(10))
    SELECT DOCU
    GO TOP 
    SCAN
    this.w_DSERIAL = NVL(MVSERIAL,SPACE(10))
    this.w_CPROWNUM = NVL(CPROWNUM,0)
    * --- Write into DOC_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVCESSER ="+cp_NullLink(cp_ToStrODBC(this.w_CSERIAL),'DOC_DETT','MVCESSER');
      +",MVCODCES ="+cp_NullLink(cp_ToStrODBC(this.w_MCCODCES),'DOC_DETT','MVCODCES');
          +i_ccchkf ;
      +" where ";
          +"MVSERIAL = "+cp_ToStrODBC(this.w_DSERIAL);
          +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
             )
    else
      update (i_cTable) set;
          MVCESSER = this.w_CSERIAL;
          ,MVCODCES = this.w_MCCODCES;
          &i_ccchkf. ;
       where;
          MVSERIAL = this.w_DSERIAL;
          and CPROWNUM = this.w_CPROWNUM;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    ENDSCAN
    SELECT CESP
    ENDSCAN
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Carica Zoom di Selezione
    this.oParentObject.NotifyEvent("Esegui")
  endproc


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    NC = This.OparentObject.w_ZoomGepr.cCursor
    PUNT=RECNO()
    SELECT (NC)
    GO TOP
    UPDATE (NC) SET XCHK=0 WHERE NVL(MCSERIAL,SPACE(10)) <>this.oParentObject.w_MCSERIAL AND XCHK=1
    GOTO PUNT
  endproc


  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,10)]
    this.cWorkTables[1]='MOV_CESP'
    this.cWorkTables[2]='PNT_MAST'
    this.cWorkTables[3]='PNT_DETT'
    this.cWorkTables[4]='SALDICON'
    this.cWorkTables[5]='MOVICOST'
    this.cWorkTables[6]='PNT_CESP'
    this.cWorkTables[7]='DOC_MAST'
    this.cWorkTables[8]='DOC_DETT'
    this.cWorkTables[9]='ART_ICOL'
    this.cWorkTables[10]='CES_PITI'
    return(this.OpenAllTables(10))

  proc CloseCursors()
    if used('_Curs_PNT_CESP')
      use in _Curs_PNT_CESP
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
