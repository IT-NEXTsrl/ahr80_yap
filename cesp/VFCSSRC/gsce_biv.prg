* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_biv                                                        *
*              Import cespiti iniz variabili                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_6]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-09-27                                                      *
* Last revis.: 2001-09-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pExec
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsce_biv",oParentObject,m.pExec)
return(i_retval)

define class tgsce_biv as StdBatch
  * --- Local variables
  pExec = space(1)
  w_CODAZI = space(5)
  w_OLDDTC = space(4)
  * --- WorkFile variables
  PAR_CESP_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiornamento Esercizio di Consolidamento nei Parametri Cespiti (da GSIM_BAC)
    if this.pExec="R"
      * --- Codice Azienda
      this.w_CODAZI = i_CODAZI
      * --- Esercizio di Consolidamento Saldi Cespiti
      * --- Read from PAR_CESP
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PAR_CESP_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_CESP_idx,2],.t.,this.PAR_CESP_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "PCDATCON"+;
          " from "+i_cTable+" PAR_CESP where ";
              +"PCCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          PCDATCON;
          from (i_cTable) where;
              PCCODAZI = this.w_CODAZI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.oParentObject.w_DATCON = NVL(cp_ToDate(_read_.PCDATCON),cp_NullValue(_read_.PCDATCON))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if EMPTY(this.oParentObject.w_DATCON)
        this.oParentObject.w_DATCON = "1900"
      endif
      * --- Variabile per controllare se l'Esercizio di Consolidamento deve essere aggiornato
      this.w_OLDDTC = this.oParentObject.w_DATCON
    else
      * --- Aggiornamento dell'Esercizio di Consolidamento nei Parametri Cespiti
      if this.w_OLDDTC<>this.oParentObject.w_DATCON
        * --- Write into PAR_CESP
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.PAR_CESP_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_CESP_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_CESP_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PCDATCON ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DATCON),'PAR_CESP','PCDATCON');
              +i_ccchkf ;
          +" where ";
              +"PCCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
                 )
        else
          update (i_cTable) set;
              PCDATCON = this.oParentObject.w_DATCON;
              &i_ccchkf. ;
           where;
              PCCODAZI = this.w_CODAZI;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
    endif
  endproc


  proc Init(oParentObject,pExec)
    this.pExec=pExec
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='PAR_CESP'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pExec"
endproc
