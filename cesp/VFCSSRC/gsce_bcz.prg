* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_bcz                                                        *
*              Associa mov.cespiti                                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_196]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-10-01                                                      *
* Last revis.: 2003-10-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsce_bcz",oParentObject,m.pOPER)
return(i_retval)

define class tgsce_bcz as StdBatch
  * --- Local variables
  pOPER = space(4)
  w_PROG = .NULL.
  w_CODCES = space(20)
  w_DESCRI = space(40)
  w_OBJ = .NULL.
  w_FLPRIU = space(1)
  w_FLDADI = space(1)
  w_FLGA01 = space(1)
  w_FLGA02 = space(1)
  w_FLGA04 = space(1)
  w_FLGA05 = space(1)
  w_FLGA06 = space(1)
  w_FLGA18 = space(1)
  w_FLGA19 = space(1)
  w_FLGA22 = space(1)
  w_FLGA23 = space(1)
  w_NUMREG = 0
  w_CODESE = space(4)
  w_DATREG = ctod("  /  /  ")
  w_CESSER = space(10)
  w_CODCON = space(15)
  w_TIPCON = space(1)
  w_MDATREG = ctod("  /  /  ")
  w_CAUCES1 = space(5)
  w_ORIGINE = space(1)
  w_OREC = 0
  w_nAbsRow = 0
  w_nRelRow = 0
  w_TIPO = space(2)
  w_FLRIFE = space(1)
  w_FLVEAC = space(1)
  w_FLPRIC = space(1)
  w_OBJ1 = .NULL.
  w_OBJ2 = .NULL.
  w_OBJ3 = .NULL.
  w_OBJ4 = .NULL.
  w_OBJ5 = .NULL.
  w_PADRE = .NULL.
  w_OBJ1 = .NULL.
  w_OSOURCE = .NULL.
  w_OBJ2 = .NULL.
  * --- WorkFile variables
  MOV_CESP_idx=0
  CAU_CESP_idx=0
  CES_PITI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Da GSVE_KDA , GSAC_KDA e GSCG_MAC gestisce le varie parti  dell'associazione cespiti a documenti e primanota
    do case
      case this.pOper="Load"
        * --- Apre masch. Cespiti
        this.w_PROG = GSCE_ACE()
        * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
        if !(this.w_PROG.bSec1)
          Ah_ErrorMsg("Impossibile aprire la gestione collegata!",48,"")
          i_retcode = 'stop'
          return
        endif
        this.w_PROG.w_OBJMASK = This.oParentObject
        this.w_PROG.EcpLoad()     
        this.w_PROG.w_CEDESCRI = this.oParentObject.w_MVDESART
        this.w_PROG.w_CETIPCES = IIF(this.oParentObject.w_TIPART="FM","CQ","CS")
        if NOT EMPTY(this.oParentObject.w_CATCES)
          this.w_OBJ = this.w_PROG.GetcTRL("w_CECODCAT")
          * --- legge il valore
          this.w_OBJ.Value = this.oParentObject.w_CATCES
          * --- esegue il link, il changed e l'mcalc
          this.w_OBJ.valid()     
        else
          * --- Se la categoria � vuota forzo il refresh
          this.w_PROG.mCalc(.T.)     
        endif
      case this.pOper="Valo"
        if NOT EMPTY(this.oParentObject.w_MVCESSER)
          * --- Read from MOV_CESP
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.MOV_CESP_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MOV_CESP_idx,2],.t.,this.MOV_CESP_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MCNUMREG,MCDATREG,MCCODESE,MCCODCES,MCCODCAU"+;
              " from "+i_cTable+" MOV_CESP where ";
                  +"MCSERIAL = "+cp_ToStrODBC(this.oParentObject.w_MVCESSER);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MCNUMREG,MCDATREG,MCCODESE,MCCODCES,MCCODCAU;
              from (i_cTable) where;
                  MCSERIAL = this.oParentObject.w_MVCESSER;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_MCNUMREG = NVL(cp_ToDate(_read_.MCNUMREG),cp_NullValue(_read_.MCNUMREG))
            this.oParentObject.w_MCDATREG = NVL(cp_ToDate(_read_.MCDATREG),cp_NullValue(_read_.MCDATREG))
            this.oParentObject.w_MCCODESE = NVL(cp_ToDate(_read_.MCCODESE),cp_NullValue(_read_.MCCODESE))
            this.oParentObject.w_MVCODCES = NVL(cp_ToDate(_read_.MCCODCES),cp_NullValue(_read_.MCCODCES))
            this.oParentObject.w_CAUCES = NVL(cp_ToDate(_read_.MCCODCAU),cp_NullValue(_read_.MCCODCAU))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        if NOT EMPTY(this.oParentObject.w_MVCODCES)
          * --- Read from CES_PITI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CES_PITI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CES_PITI_idx,2],.t.,this.CES_PITI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CEDESCRI,CETIPCES,CESTABEN"+;
              " from "+i_cTable+" CES_PITI where ";
                  +"CECODICE = "+cp_ToStrODBC(this.oParentObject.w_MVCODCES);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CEDESCRI,CETIPCES,CESTABEN;
              from (i_cTable) where;
                  CECODICE = this.oParentObject.w_MVCODCES;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_DESCCESP = NVL(cp_ToDate(_read_.CEDESCRI),cp_NullValue(_read_.CEDESCRI))
            this.oParentObject.w_CETIPO = NVL(cp_ToDate(_read_.CETIPCES),cp_NullValue(_read_.CETIPCES))
            this.oParentObject.w_STABEN = NVL(cp_ToDate(_read_.CESTABEN),cp_NullValue(_read_.CESTABEN))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        if NOT EMPTY(this.oParentObject.w_CAUCES)
          * --- Read from CAU_CESP
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CAU_CESP_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAU_CESP_idx,2],.t.,this.CAU_CESP_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CCDESCRI"+;
              " from "+i_cTable+" CAU_CESP where ";
                  +"CCCODICE = "+cp_ToStrODBC(this.oParentObject.w_CAUCES);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CCDESCRI;
              from (i_cTable) where;
                  CCCODICE = this.oParentObject.w_CAUCES;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_DESCAUCES = NVL(cp_ToDate(_read_.CCDESCRI),cp_NullValue(_read_.CCDESCRI))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        * --- Se la riga � in append w_CESPRES diventa .t. e fa editare i campi
        SELECT (THIS.OPARENTOBJECT.OPARENTOBJECT.CTRSNAME)
        if this.oParentObject.w_MVCLADOC<>"OR" AND (I_srv="A" OR EMPTY(this.oParentObject.w_MVCESSER))
          this.oParentObject.w_CESPRES = .T.
        endif
        * --- Per gli ordini l'editabilit� riguarda il campo w_mvcodces
        if this.oParentObject.w_MVCLADOC="OR" AND (I_srv="A" OR EMPTY(this.oParentObject.w_MVCODCES))
          this.oParentObject.w_CESPRES = .T.
        endif
      case this.pOper="Newm"
        * --- Apre masch. movimenti Cespiti
        this.w_PROG = GSCE_AMC()
        * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
        if !(this.w_PROG.bSec1)
          Ah_ErrorMsg("Impossibile aprire la gestione collegata!",48,"")
          i_retcode = 'stop'
          return
        endif
        this.w_PROG.w_OBJMASK = This.oParentObject
        this.w_PROG.EcpLoad()     
        * --- Read from CAU_CESP
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CAU_CESP_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAU_CESP_idx,2],.t.,this.CAU_CESP_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CCFLPRIU,CCFLDADI,CCFLGA01,CCFLGA02,CCFLGA04,CCFLGA05,CCFLGA06,CCFLRIFE,CCFLGA18,CCFLGA19,CCFLGA22,CCFLPRIC,CCFLGA23"+;
            " from "+i_cTable+" CAU_CESP where ";
                +"CCCODICE = "+cp_ToStrODBC(this.oParentObject.w_CAUCES);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CCFLPRIU,CCFLDADI,CCFLGA01,CCFLGA02,CCFLGA04,CCFLGA05,CCFLGA06,CCFLRIFE,CCFLGA18,CCFLGA19,CCFLGA22,CCFLPRIC,CCFLGA23;
            from (i_cTable) where;
                CCCODICE = this.oParentObject.w_CAUCES;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_FLPRIU = NVL(cp_ToDate(_read_.CCFLPRIU),cp_NullValue(_read_.CCFLPRIU))
          this.w_FLDADI = NVL(cp_ToDate(_read_.CCFLDADI),cp_NullValue(_read_.CCFLDADI))
          this.w_FLGA01 = NVL(cp_ToDate(_read_.CCFLGA01),cp_NullValue(_read_.CCFLGA01))
          this.w_FLGA02 = NVL(cp_ToDate(_read_.CCFLGA02),cp_NullValue(_read_.CCFLGA02))
          this.w_FLGA04 = NVL(cp_ToDate(_read_.CCFLGA04),cp_NullValue(_read_.CCFLGA04))
          this.w_FLGA05 = NVL(cp_ToDate(_read_.CCFLGA05),cp_NullValue(_read_.CCFLGA05))
          this.w_FLGA06 = NVL(cp_ToDate(_read_.CCFLGA06),cp_NullValue(_read_.CCFLGA06))
          this.w_FLRIFE = NVL(cp_ToDate(_read_.CCFLRIFE),cp_NullValue(_read_.CCFLRIFE))
          this.w_FLGA18 = NVL(cp_ToDate(_read_.CCFLGA18),cp_NullValue(_read_.CCFLGA18))
          this.w_FLGA19 = NVL(cp_ToDate(_read_.CCFLGA19),cp_NullValue(_read_.CCFLGA19))
          this.w_FLGA22 = NVL(cp_ToDate(_read_.CCFLGA22),cp_NullValue(_read_.CCFLGA22))
          this.w_FLPRIC = NVL(cp_ToDate(_read_.CCFLPRIC),cp_NullValue(_read_.CCFLPRIC))
          this.w_FLGA23 = NVL(cp_ToDate(_read_.CCFLGA23),cp_NullValue(_read_.CCFLGA23))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_FLPRIU="S"
          this.w_PROG.w_MCDTPRIU = this.oParentObject.w_MVDATREG
        endif
        if this.w_FLPRIC="S"
          this.w_PROG.w_MCDTPRIC = this.oParentObject.w_MVDATREG
        endif
        if this.w_FLDADI="S"
          this.w_PROG.w_MCDATDIS = this.oParentObject.w_MVDATREG
        endif
        do case
          case this.w_FLGA01="S"
            this.w_PROG.w_MCIMPA01 = this.oParentObject.w_MVIMPNAZ
          case this.w_FLGA02="S" OR this.w_FLGA23="S"
            if this.w_FLGA02="S"
              this.w_PROG.w_MCIMPA02 = this.oParentObject.w_MVIMPNAZ
            endif
            if this.w_FLGA23="S"
              this.w_PROG.w_MCIMPA23 = this.oParentObject.w_MVIMPNAZ
            endif
          case this.w_FLGA04="S" OR this.w_FLGA18="S"
            if this.w_FLGA04="S"
              this.w_PROG.w_MCIMPA04 = this.oParentObject.w_MVIMPNAZ
            endif
            if this.w_FLGA18="S"
              this.w_PROG.w_MCIMPA18 = this.oParentObject.w_MVIMPNAZ
            endif
          case this.w_FLGA05="S" OR this.w_FLGA22="S"
            if this.w_FLGA05="S"
              this.w_PROG.w_MCIMPA05 = this.oParentObject.w_MVIMPNAZ
            endif
            if this.w_FLGA22="S"
              this.w_PROG.w_MCIMPA22 = this.oParentObject.w_MVIMPNAZ
            endif
          case this.w_FLGA06="S" OR this.w_FLGA19="S"
            if this.w_FLGA06="S"
              this.w_PROG.w_MCIMPA06 = this.oParentObject.w_MVIMPNAZ
            endif
            if this.w_FLGA19="S"
              this.w_PROG.w_MCIMPA19 = this.oParentObject.w_MVIMPNAZ
            endif
        endcase
        this.w_PROG.w_MCDATREG = this.oParentObject.w_MVDATREG
        this.w_PROG.w_MCNUMDOC = this.oParentObject.w_MVNUMDOC
        this.w_PROG.w_MCALFDOC = this.oParentObject.w_MVALFDOC
        this.w_PROG.w_MCDATDOC = this.oParentObject.w_MVDATDOC
        if this.oParentObject.w_MVFLVEAC="V"
          this.w_PROG.w_MCQTACES = -this.oParentObject.w_MVQTAUM1
        else
          this.w_PROG.w_MCQTACES = this.oParentObject.w_MVQTAUM1
        endif
        if this.w_FLRIFE<>"N"
          * --- Se la causale prevede l'intestatario forzo l'inserimento del tipo conto
          this.w_PROG.w_MCTIPCON = this.oParentObject.w_MVTIPCON
        endif
        this.w_PROG.w_ORIGINE = "D"
        this.w_OBJ = this.w_PROG.GetcTRL("w_MCCODCAU")
        * --- legge il valore
        this.w_OBJ.Value = this.oParentObject.w_CAUCES
        * --- esegue il link, il changed e l'mcalc
        this.w_OBJ.valid()     
        if this.w_FLRIFE<>"N"
          * --- Se la causale prevede l'intestatario forzo l'inserimento del codice conto
          this.w_OBJ1 = this.w_PROG.GetcTRL("w_MCCODCON")
          this.w_OBJ1.Value = this.oParentObject.w_MVCODCON
          this.w_OBJ1.valid()     
        endif
        this.w_OBJ2 = this.w_PROG.GetcTRL("w_MCCODESE")
        this.w_OBJ2.Value = this.oParentObject.w_MVCODESE
        this.w_OBJ2.valid()     
        if NOT EMPTY(this.oParentObject.w_MVCODCES)
          this.w_OBJ3 = this.w_PROG.GetcTRL("w_MCCODCES")
          this.w_OBJ3.Value = this.oParentObject.w_MVCODCES
          this.w_OBJ3.valid()     
        endif
        this.w_OBJ4 = this.w_PROG.GetcTRL("w_MCVOCCEN")
        this.w_OBJ4.Value = this.oParentObject.w_MVVOCCEN
        this.w_OBJ4.valid()     
        this.w_OBJ5 = this.w_PROG.GetcTRL("w_MCCODCEN")
        this.w_OBJ5.Value = this.oParentObject.w_MVCODCEN
        this.w_OBJ5.valid()     
      case this.pOper="Movi"
        this.w_CESSER = SPACE(10)
        this.w_CODCES = this.oParentObject.w_MVCODCES
        this.w_CODCON = this.oParentObject.w_MVCODCON
        * --- filtro per corrispettivi
        this.w_TIPCON = IIF(EMPTY(this.oParentObject.w_MVCODCON),"N",this.oParentObject.w_MVTIPCON)
        this.w_MDATREG = this.oParentObject.w_MVDATREG
        this.w_CAUCES1 = this.oParentObject.w_CAUCES
        this.w_TIPO = this.oParentObject.w_TIPART
        this.w_FLVEAC = this.oParentObject.w_MVFLVEAC
        vq_exec("QUERY\GSVE_KCM",this,"Elab")
        if Reccount("Elab") >0 
          do GSAR_KCM with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if NOT EMPTY(this.w_CESSER)
            this.oParentObject.w_MVCESSER = this.w_CESSER
            This.OparentObject.NotifyEvent("Valorizzacespiti")
          endif
        else
          ah_ErrorMsg("Non esistono movimenti cespiti per le selezioni effettuate","OK","")
        endif
        if Used("Elab")
           
 SELECT Elab 
 Use
        endif
      case this.pOper="Clos"
        This.OparentObject.EcpSave()
      case this.pOper="Pnot"
        this.w_CESSER = SPACE(10)
        this.w_CODCON = this.oParentObject.w_PNCODCON
        this.w_TIPCON = this.oParentObject.w_PNTIPCON
        this.w_MDATREG = this.oParentObject.w_PNDATREG
        this.w_CAUCES1 = this.oParentObject.w_CAUCES
        this.w_FLVEAC = this.oParentObject.w_PNTIPREG
        do GSAR_KCM with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if NOT EMPTY(this.w_CESSER)
          this.oParentObject.w_ACMOVCES = this.w_CESSER
          this.oParentObject.w_ACTIPASS = "M"
          This.OparentObject.NotifyEvent("Valorizzacespiti")
        endif
      case this.pOper="Valp"
        this.w_PADRE = THIS.OPARENTOBJECT
        SELECT (this.w_PADRE.cTrsName)
        this.w_PADRE.MarkPos()     
        GO TOP
        if not(Empty(t_ACMOVCES))
          SCAN FOR NOT EMPTY(t_ACMOVCES)
          * --- Legge i Dati del Temporaneo
          this.oParentObject.WorkFromTrs()
          * --- Aggiorna variabili
          * --- Read from MOV_CESP
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.MOV_CESP_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MOV_CESP_idx,2],.t.,this.MOV_CESP_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MCNUMREG,MCDATREG,MCCODESE"+;
              " from "+i_cTable+" MOV_CESP where ";
                  +"MCSERIAL = "+cp_ToStrODBC(this.oParentObject.w_ACMOVCES);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MCNUMREG,MCDATREG,MCCODESE;
              from (i_cTable) where;
                  MCSERIAL = this.oParentObject.w_ACMOVCES;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_PNUMREG = NVL(cp_ToDate(_read_.MCNUMREG),cp_NullValue(_read_.MCNUMREG))
            this.oParentObject.w_PDATREG = NVL(cp_ToDate(_read_.MCDATREG),cp_NullValue(_read_.MCDATREG))
            this.oParentObject.w_PCODESE = NVL(cp_ToDate(_read_.MCCODESE),cp_NullValue(_read_.MCCODESE))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Se la riga non � in append w_CESPRES diventa .t. e  non fa editare i campi
          if !(I_srv="A" OR EMPTY (this.oParentObject.w_ACMOVCES))
            this.oParentObject.w_CESPRES = .T.
          endif
          * --- Carica il Temporaneo dei Dati
          this.oParentObject.TrsFromWork()
          * --- Flag Notifica Riga Variata
          if i_SRV<>"A"
            replace i_SRV with "U"
          endif
          ENDSCAN
          this.w_PADRE.RePos()     
        endif
      case this.pOper="Val1"
        * --- Read from MOV_CESP
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.MOV_CESP_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MOV_CESP_idx,2],.t.,this.MOV_CESP_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MCNUMREG,MCDATREG,MCCODESE"+;
            " from "+i_cTable+" MOV_CESP where ";
                +"MCSERIAL = "+cp_ToStrODBC(this.oParentObject.w_ACMOVCES);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MCNUMREG,MCDATREG,MCCODESE;
            from (i_cTable) where;
                MCSERIAL = this.oParentObject.w_ACMOVCES;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_PNUMREG = NVL(cp_ToDate(_read_.MCNUMREG),cp_NullValue(_read_.MCNUMREG))
          this.oParentObject.w_PDATREG = NVL(cp_ToDate(_read_.MCDATREG),cp_NullValue(_read_.MCDATREG))
          this.oParentObject.w_PCODESE = NVL(cp_ToDate(_read_.MCCODESE),cp_NullValue(_read_.MCCODESE))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      case this.pOper="Newp"
        * --- Apre masch. Cespiti
        this.w_PROG = GSCE_AMC()
        * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
        if !(this.w_PROG.bSec1)
          Ah_ErrorMsg("Impossibile aprire la gestione collegata!",48,"")
          i_retcode = 'stop'
          return
        endif
        this.w_PROG.w_OBJMASK = This.oParentObject
        this.w_PROG.EcpLoad()     
        * --- Read from CAU_CESP
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CAU_CESP_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAU_CESP_idx,2],.t.,this.CAU_CESP_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CCFLPRIU,CCFLDADI,CCFLPRIC,CCFLRIFE"+;
            " from "+i_cTable+" CAU_CESP where ";
                +"CCCODICE = "+cp_ToStrODBC(this.oParentObject.w_CAUCES);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CCFLPRIU,CCFLDADI,CCFLPRIC,CCFLRIFE;
            from (i_cTable) where;
                CCCODICE = this.oParentObject.w_CAUCES;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_FLPRIU = NVL(cp_ToDate(_read_.CCFLPRIU),cp_NullValue(_read_.CCFLPRIU))
          this.w_FLDADI = NVL(cp_ToDate(_read_.CCFLDADI),cp_NullValue(_read_.CCFLDADI))
          this.w_FLPRIC = NVL(cp_ToDate(_read_.CCFLPRIC),cp_NullValue(_read_.CCFLPRIC))
          this.w_FLRIFE = NVL(cp_ToDate(_read_.CCFLRIFE),cp_NullValue(_read_.CCFLRIFE))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_FLPRIU="S"
          this.w_PROG.w_MCDTPRIU = this.oParentObject.w_PNDATREG
        endif
        if this.w_FLPRIC="S"
          this.w_PROG.w_MCDTPRIC = this.oParentObject.w_PNDATREG
        endif
        if this.w_FLDADI="S"
          this.w_PROG.w_MCDATDIS = this.oParentObject.w_PNDATREG
        endif
        this.w_PROG.w_MCDATREG = this.oParentObject.w_PNDATREG
        this.w_PROG.w_MCNUMDOC = this.oParentObject.w_PNNUMDOC
        this.w_PROG.w_MCALFDOC = this.oParentObject.w_PNALFDOC
        this.w_PROG.w_MCDATDOC = this.oParentObject.w_PNDATDOC
        * --- Verifico se il tipo conto (Cliente-Fornitore) specificato nel documento � uguale 
        *     al campo contenente il riferimento ( Cliente - Fornitore - Nessuno) nella causale
        *     cespite associata.
        *     Non effettuo l'assegnamento se:
        *     Riferimento valorizzato a Nessuno
        *     Tipo documento Nessuno
        *     Riferimento diverso da Tipo documento
        if this.w_FLRIFE $ "CF" AND this.oParentObject.w_PNTIPCON $ "CF" AND this.w_FLRIFE=this.oParentObject.w_PNTIPCON
          this.w_PROG.w_MCTIPCON = this.oParentObject.w_PNTIPCON
        endif
        this.w_PROG.w_ORIGINE = "P"
        this.w_OBJ = this.w_PROG.GetcTRL("w_MCCODCAU")
        * --- legge il valore
        this.w_OBJ.Value = this.oParentObject.w_CAUCES
        * --- esegue il link, il changed e l'mcalc
        this.w_OBJ.valid()     
        * --- Non eseguo la Valid per il problema della funzione CalcZer in After Input
        if this.w_FLRIFE $ "CF" AND this.oParentObject.w_PNTIPCON $ "CF" AND this.w_FLRIFE=this.oParentObject.w_PNTIPCON
          this.w_OBJ1 = this.w_PROG.GetcTRL("w_MCCODCON")
          this.w_OSOURCE.xKey( 1 ) = this.oParentObject.w_PNTIPCON
          this.w_OSOURCE.xKey( 2 ) = this.oParentObject.w_PNCODCON
          this.w_OBJ1.ecpDrop(this.w_OSOURCE)     
        endif
        this.w_OBJ2 = this.w_PROG.GetcTRL("w_MCCODESE")
        this.w_OBJ2.Value = this.oParentObject.w_PNCODESE
        this.w_OBJ2.valid()     
      case this.pOper="Apri"
        * --- Movimento Cespite
        this.w_PROG = GSCE_AMC()
        * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
        if !(this.w_PROG.bSec1)
          Ah_ErrorMsg("Impossibile aprire la gestione collegata!",48,"")
          i_retcode = 'stop'
          return
        endif
        this.w_PROG.w_MCSERIAL = this.oParentObject.w_MCSERIAL
        this.w_PROG.QueryKeySet("MCSERIAL='"+this.oParentObject.w_MCSERIAL+"'" ,"")     
        * --- carico il record
        this.w_PROG.LoadRec()     
      case this.pOper="Aprc"
        * --- Cespite
        this.w_PROG = GSCE_ACE()
        * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
        if !(this.w_PROG.bSec1)
          Ah_ErrorMsg("Impossibile aprire la gestione collegata!",48,"")
          i_retcode = 'stop'
          return
        endif
        this.w_PROG.Ecpfilter()     
        this.w_PROG.w_CECODICE = this.oParentObject.w_MVCODCES
        this.w_PROG.Ecpsave()     
        * --- carico il record
        this.w_PROG.LoadRec()     
      case this.pOper="Canc"
        if this.oParentObject.w_CESPRES=.T.
          ah_ErrorMsg("Impossibile cancellare")
          this.oParentObject.w_HASEVENT = .F.
        else
          this.oParentObject.w_HASEVENT = .T.
        endif
      case this.pOPER="Camb"
        if (this.oParentObject.o_MVCODCES<>this.oParentObject.w_MVCODCES AND NOT EMPTY(this.oParentObject.o_MVCODCES)) OR (this.oParentObject.o_CAUCES<>this.oParentObject.w_CAUCES AND NOT EMPTY(this.oParentObject.o_CAUCES))
          if (this.oParentObject.o_CAUCES<>this.oParentObject.w_CAUCES AND NOT EMPTY(this.oParentObject.o_CAUCES))
            this.oParentObject.w_MVCODCES = SPACE(20)
            this.oParentObject.w_DESCCESP = SPACE(40)
            this.oParentObject.w_CETIPO = SPACE(2)
            this.oParentObject.w_STABEN = " "
          endif
          this.oParentObject.w_MVCESSER = SPACE(10)
          this.oParentObject.w_MCNUMREG = 0
          this.oParentObject.w_MCCODESE = SPACE(4)
          this.oParentObject.w_MCDATREG = cp_CharToDate("  -  -  ")
        endif
    endcase
  endproc


  proc Init(oParentObject,pOPER)
    this.pOPER=pOPER
    this.w_OSOURCE=createobject("_Osource")
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='MOV_CESP'
    this.cWorkTables[2]='CAU_CESP'
    this.cWorkTables[3]='CES_PITI'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER"
endproc
