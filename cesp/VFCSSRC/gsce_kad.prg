* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_kad                                                        *
*              Abbina cespiti a documenti                                      *
*                                                                              *
*      Author: Zucchetti TAM S.r.l.                                            *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30] [VRS_133]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-04-13                                                      *
* Last revis.: 2009-12-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsce_kad",oParentObject))

* --- Class definition
define class tgsce_kad as StdForm
  Top    = 1
  Left   = 6

  * --- Standard Properties
  Width  = 784
  Height = 488
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-12-11"
  HelpContextID=132793705
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=39

  * --- Constant Properties
  _IDX = 0
  CAU_CESP_IDX = 0
  CES_PITI_IDX = 0
  AZIENDA_IDX = 0
  ESERCIZI_IDX = 0
  cPrg = "gsce_kad"
  cComment = "Abbina cespiti a documenti"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODAZI = space(5)
  w_OBTEST = ctod('  /  /  ')
  w_ESER = space(4)
  o_ESER = space(4)
  w_ABBINA = space(1)
  w_SENZRIFE = space(1)
  w_DANUMREG = 0
  o_DANUMREG = 0
  w_DADATA = ctod('  /  /  ')
  o_DADATA = ctod('  /  /  ')
  w_AANUMREG = 0
  w_ESE = space(4)
  w_DATA1 = ctod('  /  /  ')
  w_AADATA = ctod('  /  /  ')
  w_DATA2 = ctod('  /  /  ')
  w_CODCAU = space(5)
  w_DACODCES = space(20)
  o_DACODCES = space(20)
  w_AACODCES = space(20)
  w_ESER1 = space(4)
  w_DANUMREG1 = 0
  w_DADATA1 = ctod('  /  /  ')
  o_DADATA1 = ctod('  /  /  ')
  w_AANUMREG1 = 0
  w_AADATA1 = ctod('  /  /  ')
  w_DANUMDOC = 0
  w_SERIE1 = space(10)
  w_AANUMDOC = 0
  w_SERIE2 = space(10)
  w_DADATAD = ctod('  /  /  ')
  o_DADATAD = ctod('  /  /  ')
  w_AADATAD = ctod('  /  /  ')
  w_DTOBSO = ctod('  /  /  ')
  w_MCSERIAL = space(10)
  w_MVSERIAL = space(10)
  w_MVDATREG = space(10)
  w_TIPDOC = space(1)
  o_TIPDOC = space(1)
  w_FLVEAC = space(1)
  w_CLADOC = space(2)
  w_CPROWNUM = 0
  w_TDCAUCES = space(5)
  w_DESCESDA = space(40)
  w_DESCESAA = space(40)
  w_DESCAU = space(40)
  w_DTOBSOCA = ctod('  /  /  ')
  w_ZoomGepr = .NULL.
  w_ZOOM = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsce_kadPag1","gsce_kad",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oESER_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomGepr = this.oPgFrm.Pages(1).oPag.ZoomGepr
    this.w_ZOOM = this.oPgFrm.Pages(1).oPag.ZOOM
    DoDefault()
    proc Destroy()
      this.w_ZoomGepr = .NULL.
      this.w_ZOOM = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='CAU_CESP'
    this.cWorkTables[2]='CES_PITI'
    this.cWorkTables[3]='AZIENDA'
    this.cWorkTables[4]='ESERCIZI'
    return(this.OpenAllTables(4))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI=space(5)
      .w_OBTEST=ctod("  /  /  ")
      .w_ESER=space(4)
      .w_ABBINA=space(1)
      .w_SENZRIFE=space(1)
      .w_DANUMREG=0
      .w_DADATA=ctod("  /  /  ")
      .w_AANUMREG=0
      .w_ESE=space(4)
      .w_DATA1=ctod("  /  /  ")
      .w_AADATA=ctod("  /  /  ")
      .w_DATA2=ctod("  /  /  ")
      .w_CODCAU=space(5)
      .w_DACODCES=space(20)
      .w_AACODCES=space(20)
      .w_ESER1=space(4)
      .w_DANUMREG1=0
      .w_DADATA1=ctod("  /  /  ")
      .w_AANUMREG1=0
      .w_AADATA1=ctod("  /  /  ")
      .w_DANUMDOC=0
      .w_SERIE1=space(10)
      .w_AANUMDOC=0
      .w_SERIE2=space(10)
      .w_DADATAD=ctod("  /  /  ")
      .w_AADATAD=ctod("  /  /  ")
      .w_DTOBSO=ctod("  /  /  ")
      .w_MCSERIAL=space(10)
      .w_MVSERIAL=space(10)
      .w_MVDATREG=space(10)
      .w_TIPDOC=space(1)
      .w_FLVEAC=space(1)
      .w_CLADOC=space(2)
      .w_CPROWNUM=0
      .w_TDCAUCES=space(5)
      .w_DESCESDA=space(40)
      .w_DESCESAA=space(40)
      .w_DESCAU=space(40)
      .w_DTOBSOCA=ctod("  /  /  ")
        .w_CODAZI = I_CODAZI
        .w_OBTEST = i_datsys
        .w_ESER = g_codese
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_ESER))
          .link_1_3('Full')
        endif
        .w_ABBINA = 'D'
        .w_SENZRIFE = 'N'
          .DoRTCalc(6,7,.f.)
        .w_AANUMREG = iif(EMPTY(.w_AANUMREG),.w_DANUMREG,.w_AANUMREG)
        .w_ESE = .w_ESER
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_ESE))
          .link_1_9('Full')
        endif
        .w_DATA1 = .w_DADATA
        .w_AADATA = iif(EMPTY(.w_AADATA),.w_DADATA,.w_AADATA)
        .w_DATA2 = .w_AADATA
        .DoRTCalc(13,13,.f.)
        if not(empty(.w_CODCAU))
          .link_1_13('Full')
        endif
        .DoRTCalc(14,14,.f.)
        if not(empty(.w_DACODCES))
          .link_1_14('Full')
        endif
        .w_AACODCES = iif(EMPTY(.w_AACODCES),.w_DACODCES,.w_AACODCES)
        .DoRTCalc(15,15,.f.)
        if not(empty(.w_AACODCES))
          .link_1_15('Full')
        endif
        .w_ESER1 = g_codese
        .DoRTCalc(16,16,.f.)
        if not(empty(.w_ESER1))
          .link_1_16('Full')
        endif
          .DoRTCalc(17,18,.f.)
        .w_AANUMREG1 = iif(EMPTY(.w_AANUMREG1),.w_DANUMREG1,.w_AANUMREG1)
        .w_AADATA1 = iif(EMPTY(.w_AADATA1),.w_DADATA1,.w_AADATA1)
          .DoRTCalc(21,21,.f.)
        .w_SERIE1 = ''
        .w_AANUMDOC = iif(EMPTY(.w_AANUMDOC),.w_DANUMDOC,.w_AANUMRDOC)
        .w_SERIE2 = ''
          .DoRTCalc(25,25,.f.)
        .w_AADATAD = iif(EMPTY(.w_AADATAD),.w_DADATAD,.w_AADATAD)
      .oPgFrm.Page1.oPag.ZoomGepr.Calculate()
          .DoRTCalc(27,27,.f.)
        .w_MCSERIAL = .w_ZoomGepr.getVar('MCSERIAL')
        .w_MVSERIAL = .w_Zoom.getVar('MVSERIAL')
        .w_MVDATREG = .w_Zoom.getVar('MVDATREG')
        .w_TIPDOC = 'T'
        .w_FLVEAC = IIF(.w_TIPDOC$'A-F','A',IIF(.w_TIPDOC$'V-N','V',' '))
        .w_CLADOC = IIF(.w_TIPDOC$'A-V','FA', IIF(.w_TIPDOC$'F-N','NC',IIF(.w_TIPDOC='C','RF','  ')))
        .w_CPROWNUM = .w_Zoom.getVar('CPROWNUM')
        .w_TDCAUCES = .w_Zoom.getVar('TDCAUCES')
      .oPgFrm.Page1.oPag.ZOOM.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_74.Calculate()
    endwith
    this.DoRTCalc(36,39,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_59.enabled = this.oPgFrm.Page1.oPag.oBtn_1_59.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_60.enabled = this.oPgFrm.Page1.oPag.oBtn_1_60.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_61.enabled = this.oPgFrm.Page1.oPag.oBtn_1_61.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_62.enabled = this.oPgFrm.Page1.oPag.oBtn_1_62.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_63.enabled = this.oPgFrm.Page1.oPag.oBtn_1_63.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,7,.t.)
        if .o_DANUMREG<>.w_DANUMREG
            .w_AANUMREG = iif(EMPTY(.w_AANUMREG),.w_DANUMREG,.w_AANUMREG)
        endif
        if .o_ESER<>.w_ESER
            .w_ESE = .w_ESER
          .link_1_9('Full')
        endif
        if .o_DADATA<>.w_DADATA
            .w_DATA1 = .w_DADATA
        endif
        if .o_DADATA<>.w_DADATA
            .w_AADATA = iif(EMPTY(.w_AADATA),.w_DADATA,.w_AADATA)
        endif
            .w_DATA2 = .w_AADATA
        .DoRTCalc(13,14,.t.)
        if .o_DACODCES<>.w_DACODCES
            .w_AACODCES = iif(EMPTY(.w_AACODCES),.w_DACODCES,.w_AACODCES)
          .link_1_15('Full')
        endif
        .DoRTCalc(16,18,.t.)
        if .o_DANUMREG<>.w_DANUMREG
            .w_AANUMREG1 = iif(EMPTY(.w_AANUMREG1),.w_DANUMREG1,.w_AANUMREG1)
        endif
        if .o_DADATA1<>.w_DADATA1
            .w_AADATA1 = iif(EMPTY(.w_AADATA1),.w_DADATA1,.w_AADATA1)
        endif
        .DoRTCalc(21,22,.t.)
        if .o_DANUMREG<>.w_DANUMREG
            .w_AANUMDOC = iif(EMPTY(.w_AANUMDOC),.w_DANUMDOC,.w_AANUMRDOC)
        endif
        .DoRTCalc(24,25,.t.)
        if .o_DADATAD<>.w_DADATAD
            .w_AADATAD = iif(EMPTY(.w_AADATAD),.w_DADATAD,.w_AADATAD)
        endif
        .oPgFrm.Page1.oPag.ZoomGepr.Calculate()
        .DoRTCalc(27,27,.t.)
            .w_MCSERIAL = .w_ZoomGepr.getVar('MCSERIAL')
            .w_MVSERIAL = .w_Zoom.getVar('MVSERIAL')
            .w_MVDATREG = .w_Zoom.getVar('MVDATREG')
        .DoRTCalc(31,31,.t.)
        if .o_TIPDOC<>.w_TIPDOC
            .w_FLVEAC = IIF(.w_TIPDOC$'A-F','A',IIF(.w_TIPDOC$'V-N','V',' '))
        endif
        if .o_TIPDOC<>.w_TIPDOC
            .w_CLADOC = IIF(.w_TIPDOC$'A-V','FA', IIF(.w_TIPDOC$'F-N','NC',IIF(.w_TIPDOC='C','RF','  ')))
        endif
            .w_CPROWNUM = .w_Zoom.getVar('CPROWNUM')
            .w_TDCAUCES = .w_Zoom.getVar('TDCAUCES')
        .oPgFrm.Page1.oPag.ZOOM.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_74.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(36,39,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZoomGepr.Calculate()
        .oPgFrm.Page1.oPag.ZOOM.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_74.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZoomGepr.Event(cEvent)
      .oPgFrm.Page1.oPag.ZOOM.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_74.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=ESER
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ESER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_ESER)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_CODAZI;
                     ,'ESCODESE',trim(this.w_ESER))
          select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ESER)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ESER) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oESER_1_3'),i_cWhere,'',"Esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ESER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_ESER);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_ESER)
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ESER = NVL(_Link_.ESCODESE,space(4))
      this.w_DADATA = NVL(cp_ToDate(_Link_.ESINIESE),ctod("  /  /  "))
      this.w_AADATA = NVL(cp_ToDate(_Link_.ESFINESE),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ESER = space(4)
      endif
      this.w_DADATA = ctod("  /  /  ")
      this.w_AADATA = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ESER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ESE
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_ESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_ESE)
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ESE = NVL(_Link_.ESCODESE,space(4))
    else
      if i_cCtrl<>'Load'
        this.w_ESE = space(4)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCAU
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CESP_IDX,3]
    i_lTable = "CAU_CESP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CESP_IDX,2], .t., this.CAU_CESP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CESP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCAU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCE_ACC',True,'CAU_CESP')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_CODCAU)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_CODCAU))
          select CCCODICE,CCDESCRI,CCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCAU)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStrODBC(trim(this.w_CODCAU)+"%");

            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStr(trim(this.w_CODCAU)+"%");

            select CCCODICE,CCDESCRI,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODCAU) and !this.bDontReportError
            deferred_cp_zoom('CAU_CESP','*','CCCODICE',cp_AbsName(oSource.parent,'oCODCAU_1_13'),i_cWhere,'GSCE_ACC',"Causali cespiti",'GSCE_KAM.CAU_CESP_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCAU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_CODCAU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_CODCAU)
            select CCCODICE,CCDESCRI,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCAU = NVL(_Link_.CCCODICE,space(5))
      this.w_DESCAU = NVL(_Link_.CCDESCRI,space(40))
      this.w_DTOBSOCA = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODCAU = space(5)
      endif
      this.w_DESCAU = space(40)
      this.w_DTOBSOCA = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DTOBSOCA>.w_OBTEST OR EMPTY(.w_DTOBSOCA)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale cespite inesistente o obsoleta")
        endif
        this.w_CODCAU = space(5)
        this.w_DESCAU = space(40)
        this.w_DTOBSOCA = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CESP_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CESP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCAU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DACODCES
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CES_PITI_IDX,3]
    i_lTable = "CES_PITI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CES_PITI_IDX,2], .t., this.CES_PITI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CES_PITI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DACODCES) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCE_ACE',True,'CES_PITI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CECODICE like "+cp_ToStrODBC(trim(this.w_DACODCES)+"%");

          i_ret=cp_SQL(i_nConn,"select CECODICE,CEDESCRI,CEDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CECODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CECODICE',trim(this.w_DACODCES))
          select CECODICE,CEDESCRI,CEDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CECODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DACODCES)==trim(_Link_.CECODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CEDESCRI like "+cp_ToStrODBC(trim(this.w_DACODCES)+"%");

            i_ret=cp_SQL(i_nConn,"select CECODICE,CEDESCRI,CEDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CEDESCRI like "+cp_ToStr(trim(this.w_DACODCES)+"%");

            select CECODICE,CEDESCRI,CEDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_DACODCES) and !this.bDontReportError
            deferred_cp_zoom('CES_PITI','*','CECODICE',cp_AbsName(oSource.parent,'oDACODCES_1_14'),i_cWhere,'GSCE_ACE',"Elenco cespiti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CECODICE,CEDESCRI,CEDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CECODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CECODICE',oSource.xKey(1))
            select CECODICE,CEDESCRI,CEDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DACODCES)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CECODICE,CEDESCRI,CEDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CECODICE="+cp_ToStrODBC(this.w_DACODCES);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CECODICE',this.w_DACODCES)
            select CECODICE,CEDESCRI,CEDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DACODCES = NVL(_Link_.CECODICE,space(20))
      this.w_DESCESDA = NVL(_Link_.CEDESCRI,space(40))
      this.w_DTOBSO = NVL(cp_ToDate(_Link_.CEDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_DACODCES = space(20)
      endif
      this.w_DESCESDA = space(40)
      this.w_DTOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_DTOBSO>.w_OBTEST OR EMPTY(.w_DTOBSO)) AND (EMPTY(.w_AACODCES) OR .w_AACODCES>=.w_DACODCES)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Cespite inesistente o obsoleto o intervallo scorretto")
        endif
        this.w_DACODCES = space(20)
        this.w_DESCESDA = space(40)
        this.w_DTOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CES_PITI_IDX,2])+'\'+cp_ToStr(_Link_.CECODICE,1)
      cp_ShowWarn(i_cKey,this.CES_PITI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DACODCES Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=AACODCES
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CES_PITI_IDX,3]
    i_lTable = "CES_PITI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CES_PITI_IDX,2], .t., this.CES_PITI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CES_PITI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AACODCES) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCE_ACE',True,'CES_PITI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CECODICE like "+cp_ToStrODBC(trim(this.w_AACODCES)+"%");

          i_ret=cp_SQL(i_nConn,"select CECODICE,CEDESCRI,CEDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CECODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CECODICE',trim(this.w_AACODCES))
          select CECODICE,CEDESCRI,CEDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CECODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_AACODCES)==trim(_Link_.CECODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CEDESCRI like "+cp_ToStrODBC(trim(this.w_AACODCES)+"%");

            i_ret=cp_SQL(i_nConn,"select CECODICE,CEDESCRI,CEDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CEDESCRI like "+cp_ToStr(trim(this.w_AACODCES)+"%");

            select CECODICE,CEDESCRI,CEDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_AACODCES) and !this.bDontReportError
            deferred_cp_zoom('CES_PITI','*','CECODICE',cp_AbsName(oSource.parent,'oAACODCES_1_15'),i_cWhere,'GSCE_ACE',"Elenco cespiti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CECODICE,CEDESCRI,CEDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CECODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CECODICE',oSource.xKey(1))
            select CECODICE,CEDESCRI,CEDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AACODCES)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CECODICE,CEDESCRI,CEDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CECODICE="+cp_ToStrODBC(this.w_AACODCES);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CECODICE',this.w_AACODCES)
            select CECODICE,CEDESCRI,CEDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AACODCES = NVL(_Link_.CECODICE,space(20))
      this.w_DESCESAA = NVL(_Link_.CEDESCRI,space(40))
      this.w_DTOBSO = NVL(cp_ToDate(_Link_.CEDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_AACODCES = space(20)
      endif
      this.w_DESCESAA = space(40)
      this.w_DTOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_DTOBSO>.w_OBTEST OR EMPTY(.w_DTOBSO)) AND (EMPTY(.w_AACODCES) OR .w_AACODCES>=.w_DACODCES)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Cespite inesistente o obsoleto o intervallo scorretto")
        endif
        this.w_AACODCES = space(20)
        this.w_DESCESAA = space(40)
        this.w_DTOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CES_PITI_IDX,2])+'\'+cp_ToStr(_Link_.CECODICE,1)
      cp_ShowWarn(i_cKey,this.CES_PITI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AACODCES Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ESER1
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ESER1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_ESER1)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_CODAZI;
                     ,'ESCODESE',trim(this.w_ESER1))
          select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ESER1)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ESER1) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oESER1_1_16'),i_cWhere,'',"Esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ESER1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_ESER1);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_ESER1)
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ESER1 = NVL(_Link_.ESCODESE,space(4))
      this.w_DADATA1 = NVL(cp_ToDate(_Link_.ESINIESE),ctod("  /  /  "))
      this.w_AADATA1 = NVL(cp_ToDate(_Link_.ESFINESE),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ESER1 = space(4)
      endif
      this.w_DADATA1 = ctod("  /  /  ")
      this.w_AADATA1 = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ESER1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oESER_1_3.value==this.w_ESER)
      this.oPgFrm.Page1.oPag.oESER_1_3.value=this.w_ESER
    endif
    if not(this.oPgFrm.Page1.oPag.oABBINA_1_4.RadioValue()==this.w_ABBINA)
      this.oPgFrm.Page1.oPag.oABBINA_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDANUMREG_1_6.value==this.w_DANUMREG)
      this.oPgFrm.Page1.oPag.oDANUMREG_1_6.value=this.w_DANUMREG
    endif
    if not(this.oPgFrm.Page1.oPag.oDADATA_1_7.value==this.w_DADATA)
      this.oPgFrm.Page1.oPag.oDADATA_1_7.value=this.w_DADATA
    endif
    if not(this.oPgFrm.Page1.oPag.oAANUMREG_1_8.value==this.w_AANUMREG)
      this.oPgFrm.Page1.oPag.oAANUMREG_1_8.value=this.w_AANUMREG
    endif
    if not(this.oPgFrm.Page1.oPag.oAADATA_1_11.value==this.w_AADATA)
      this.oPgFrm.Page1.oPag.oAADATA_1_11.value=this.w_AADATA
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCAU_1_13.value==this.w_CODCAU)
      this.oPgFrm.Page1.oPag.oCODCAU_1_13.value=this.w_CODCAU
    endif
    if not(this.oPgFrm.Page1.oPag.oDACODCES_1_14.value==this.w_DACODCES)
      this.oPgFrm.Page1.oPag.oDACODCES_1_14.value=this.w_DACODCES
    endif
    if not(this.oPgFrm.Page1.oPag.oAACODCES_1_15.value==this.w_AACODCES)
      this.oPgFrm.Page1.oPag.oAACODCES_1_15.value=this.w_AACODCES
    endif
    if not(this.oPgFrm.Page1.oPag.oESER1_1_16.value==this.w_ESER1)
      this.oPgFrm.Page1.oPag.oESER1_1_16.value=this.w_ESER1
    endif
    if not(this.oPgFrm.Page1.oPag.oDANUMREG1_1_17.value==this.w_DANUMREG1)
      this.oPgFrm.Page1.oPag.oDANUMREG1_1_17.value=this.w_DANUMREG1
    endif
    if not(this.oPgFrm.Page1.oPag.oDADATA1_1_18.value==this.w_DADATA1)
      this.oPgFrm.Page1.oPag.oDADATA1_1_18.value=this.w_DADATA1
    endif
    if not(this.oPgFrm.Page1.oPag.oAANUMREG1_1_19.value==this.w_AANUMREG1)
      this.oPgFrm.Page1.oPag.oAANUMREG1_1_19.value=this.w_AANUMREG1
    endif
    if not(this.oPgFrm.Page1.oPag.oAADATA1_1_20.value==this.w_AADATA1)
      this.oPgFrm.Page1.oPag.oAADATA1_1_20.value=this.w_AADATA1
    endif
    if not(this.oPgFrm.Page1.oPag.oDANUMDOC_1_21.value==this.w_DANUMDOC)
      this.oPgFrm.Page1.oPag.oDANUMDOC_1_21.value=this.w_DANUMDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oSERIE1_1_22.value==this.w_SERIE1)
      this.oPgFrm.Page1.oPag.oSERIE1_1_22.value=this.w_SERIE1
    endif
    if not(this.oPgFrm.Page1.oPag.oAANUMDOC_1_23.value==this.w_AANUMDOC)
      this.oPgFrm.Page1.oPag.oAANUMDOC_1_23.value=this.w_AANUMDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oSERIE2_1_24.value==this.w_SERIE2)
      this.oPgFrm.Page1.oPag.oSERIE2_1_24.value=this.w_SERIE2
    endif
    if not(this.oPgFrm.Page1.oPag.oDADATAD_1_25.value==this.w_DADATAD)
      this.oPgFrm.Page1.oPag.oDADATAD_1_25.value=this.w_DADATAD
    endif
    if not(this.oPgFrm.Page1.oPag.oAADATAD_1_26.value==this.w_AADATAD)
      this.oPgFrm.Page1.oPag.oAADATAD_1_26.value=this.w_AADATAD
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPDOC_1_57.RadioValue()==this.w_TIPDOC)
      this.oPgFrm.Page1.oPag.oTIPDOC_1_57.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCESDA_1_68.value==this.w_DESCESDA)
      this.oPgFrm.Page1.oPag.oDESCESDA_1_68.value=this.w_DESCESDA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCESAA_1_69.value==this.w_DESCESAA)
      this.oPgFrm.Page1.oPag.oDESCESAA_1_69.value=this.w_DESCESAA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAU_1_70.value==this.w_DESCAU)
      this.oPgFrm.Page1.oPag.oDESCAU_1_70.value=this.w_DESCAU
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(EMPTY(.w_AANUMREG) OR .w_DANUMREG<=.w_AANUMREG)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDANUMREG_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Numero registrazione di partenza < di quello di arrivo")
          case   not(EMPTY(.w_AADATA) OR .w_DADATA<=.w_AADATA)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDADATA_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data di inizio selezione > di data fine")
          case   not(EMPTY(.w_DANUMREG) OR .w_DANUMREG<=.w_AANUMREG)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAANUMREG_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Numero registrazione di arrivo < di quello di partenza")
          case   not(EMPTY(.w_DADATA) OR .w_DADATA<=.w_AADATA)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAADATA_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data di fine selezione < di data inizio")
          case   not(.w_DTOBSOCA>.w_OBTEST OR EMPTY(.w_DTOBSOCA))  and not(empty(.w_CODCAU))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODCAU_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale cespite inesistente o obsoleta")
          case   not((.w_DTOBSO>.w_OBTEST OR EMPTY(.w_DTOBSO)) AND (EMPTY(.w_AACODCES) OR .w_AACODCES>=.w_DACODCES))  and not(empty(.w_DACODCES))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDACODCES_1_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Cespite inesistente o obsoleto o intervallo scorretto")
          case   not((.w_DTOBSO>.w_OBTEST OR EMPTY(.w_DTOBSO)) AND (EMPTY(.w_AACODCES) OR .w_AACODCES>=.w_DACODCES))  and not(empty(.w_AACODCES))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAACODCES_1_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Cespite inesistente o obsoleto o intervallo scorretto")
          case   not(EMPTY(.w_AANUMREG1) OR .w_DANUMREG1<=.w_AANUMREG1)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDANUMREG1_1_17.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Numero registrazione di partenza < di quello di arrivo")
          case   not(EMPTY(.w_AADATA1) OR .w_DADATA1<=.w_AADATA1)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDADATA1_1_18.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data di inizio selezione > di data fine")
          case   not(EMPTY(.w_DANUMREG1) OR .w_DANUMREG1<=.w_AANUMREG1)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAANUMREG1_1_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Numero registrazione di arrivo < di quello di partenza")
          case   not(EMPTY(.w_DADATA1) OR .w_DADATA1<=.w_AADATA1)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAADATA1_1_20.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data di fine selezione < di data inizio")
          case   not(EMPTY(.w_AANUMDOC) OR .w_DANUMDOC<=.w_AANUMDOC)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDANUMDOC_1_21.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Numero documento di partenza < di quello di arrivo")
          case   not((empty(.w_SERIE2)) OR  (.w_SERIE1<=.w_SERIE2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSERIE1_1_22.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La serie iniziale � pi� grande della serie finale")
          case   not(EMPTY(.w_DANUMDOC) OR .w_DANUMDOC<=.w_AANUMDOC)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAANUMDOC_1_23.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Numero documento di arrivo < di quello di partenza")
          case   not((.w_serie2>=.w_serie1) or (empty(.w_serie1)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSERIE2_1_24.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La serie iniziale � maggire della serie finale")
          case   not(EMPTY(.w_AADATAD) OR .w_DADATAD<=.w_AADATAD)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDADATAD_1_25.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data di inizio selezione > di data fine")
          case   not(EMPTY(.w_DADATAD) OR .w_DADATAD<=.w_AADATAD)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAADATAD_1_26.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data di fine selezione < di data inizio")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_ESER = this.w_ESER
    this.o_DANUMREG = this.w_DANUMREG
    this.o_DADATA = this.w_DADATA
    this.o_DACODCES = this.w_DACODCES
    this.o_DADATA1 = this.w_DADATA1
    this.o_DADATAD = this.w_DADATAD
    this.o_TIPDOC = this.w_TIPDOC
    return

enddefine

* --- Define pages as container
define class tgsce_kadPag1 as StdContainer
  Width  = 780
  height = 488
  stdWidth  = 780
  stdheight = 488
  resizeYpos=315
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oESER_1_3 as StdField with uid="DEJFMWIGRW",rtseq=3,rtrep=.f.,;
    cFormVar = "w_ESER", cQueryName = "ESER",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Esercizio di selezione movimenti cespiti",;
    HelpContextID = 127114682,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=89, Top=31, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_ESER"

  func oESER_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oESER_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oESER_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CODAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_CODAZI)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oESER_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Esercizi",'',this.parent.oContained
  endproc


  add object oABBINA_1_4 as StdCombo with uid="CDAJXHAPDL",rtseq=4,rtrep=.f.,left=276,top=31,width=122,height=21;
    , ToolTipText = "Da abbinare, gi� abbinati alla primanota o tutti";
    , HelpContextID = 29155066;
    , cFormVar="w_ABBINA",RowSource=""+"Da abbinare,"+"Gi� abbinati,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oABBINA_1_4.RadioValue()
    return(iif(this.value =1,'D',;
    iif(this.value =2,'A',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oABBINA_1_4.GetRadio()
    this.Parent.oContained.w_ABBINA = this.RadioValue()
    return .t.
  endfunc

  func oABBINA_1_4.SetRadio()
    this.Parent.oContained.w_ABBINA=trim(this.Parent.oContained.w_ABBINA)
    this.value = ;
      iif(this.Parent.oContained.w_ABBINA=='D',1,;
      iif(this.Parent.oContained.w_ABBINA=='A',2,;
      iif(this.Parent.oContained.w_ABBINA=='T',3,;
      0)))
  endfunc

  add object oDANUMREG_1_6 as StdField with uid="VQGGMZIIKU",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DANUMREG", cQueryName = "DANUMREG",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Numero registrazione di partenza < di quello di arrivo",;
    ToolTipText = "Da numero registrazione movimenti cespiti",;
    HelpContextID = 255844477,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=89, Top=56, cSayPict='"999999"', cGetPict='"999999"'

  func oDANUMREG_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (EMPTY(.w_AANUMREG) OR .w_DANUMREG<=.w_AANUMREG)
    endwith
    return bRes
  endfunc

  add object oDADATA_1_7 as StdField with uid="JHENWKOELX",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DADATA", cQueryName = "DADATA",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data di inizio selezione > di data fine",;
    ToolTipText = "Da data registrazione movimenti cespiti",;
    HelpContextID = 23379914,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=326, Top=56

  func oDADATA_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (EMPTY(.w_AADATA) OR .w_DADATA<=.w_AADATA)
    endwith
    return bRes
  endfunc

  add object oAANUMREG_1_8 as StdField with uid="BTHDGLGOQQ",rtseq=8,rtrep=.f.,;
    cFormVar = "w_AANUMREG", cQueryName = "AANUMREG",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Numero registrazione di arrivo < di quello di partenza",;
    ToolTipText = "A numero registrazione movimenti cespiti",;
    HelpContextID = 255844429,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=89, Top=81, cSayPict='"999999"', cGetPict='"999999"'

  func oAANUMREG_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (EMPTY(.w_DANUMREG) OR .w_DANUMREG<=.w_AANUMREG)
    endwith
    return bRes
  endfunc

  add object oAADATA_1_11 as StdField with uid="SPAMFTUYCI",rtseq=11,rtrep=.f.,;
    cFormVar = "w_AADATA", cQueryName = "AADATA",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data di fine selezione < di data inizio",;
    ToolTipText = "A data registrazione movimenti cespiti",;
    HelpContextID = 23379962,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=326, Top=81

  func oAADATA_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (EMPTY(.w_DADATA) OR .w_DADATA<=.w_AADATA)
    endwith
    return bRes
  endfunc

  add object oCODCAU_1_13 as StdField with uid="EVOEAWHUTF",rtseq=13,rtrep=.f.,;
    cFormVar = "w_CODCAU", cQueryName = "CODCAU",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale cespite inesistente o obsoleta",;
    ToolTipText = "Codice causale di movimentazione cespite",;
    HelpContextID = 23940646,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=89, Top=106, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CESP", cZoomOnZoom="GSCE_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_CODCAU"

  func oCODCAU_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCAU_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCAU_1_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CESP','*','CCCODICE',cp_AbsName(this.parent,'oCODCAU_1_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCE_ACC',"Causali cespiti",'GSCE_KAM.CAU_CESP_VZM',this.parent.oContained
  endproc
  proc oCODCAU_1_13.mZoomOnZoom
    local i_obj
    i_obj=GSCE_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_CODCAU
     i_obj.ecpSave()
  endproc

  add object oDACODCES_1_14 as StdField with uid="GYOVDDLWYT",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DACODCES", cQueryName = "DACODCES",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Cespite inesistente o obsoleto o intervallo scorretto",;
    ToolTipText = "Da codice del cespite",;
    HelpContextID = 262746249,;
   bGlobalFont=.t.,;
    Height=21, Width=144, Left=89, Top=131, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="CES_PITI", cZoomOnZoom="GSCE_ACE", oKey_1_1="CECODICE", oKey_1_2="this.w_DACODCES"

  func oDACODCES_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oDACODCES_1_14.ecpDrop(oSource)
    this.Parent.oContained.link_1_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDACODCES_1_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CES_PITI','*','CECODICE',cp_AbsName(this.parent,'oDACODCES_1_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCE_ACE',"Elenco cespiti",'',this.parent.oContained
  endproc
  proc oDACODCES_1_14.mZoomOnZoom
    local i_obj
    i_obj=GSCE_ACE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CECODICE=this.parent.oContained.w_DACODCES
     i_obj.ecpSave()
  endproc

  add object oAACODCES_1_15 as StdField with uid="YMQGKDVEZI",rtseq=15,rtrep=.f.,;
    cFormVar = "w_AACODCES", cQueryName = "AACODCES",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Cespite inesistente o obsoleto o intervallo scorretto",;
    ToolTipText = "A codice del cespite",;
    HelpContextID = 262746201,;
   bGlobalFont=.t.,;
    Height=21, Width=144, Left=89, Top=156, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="CES_PITI", cZoomOnZoom="GSCE_ACE", oKey_1_1="CECODICE", oKey_1_2="this.w_AACODCES"

  func oAACODCES_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oAACODCES_1_15.ecpDrop(oSource)
    this.Parent.oContained.link_1_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oAACODCES_1_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CES_PITI','*','CECODICE',cp_AbsName(this.parent,'oAACODCES_1_15'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCE_ACE',"Elenco cespiti",'',this.parent.oContained
  endproc
  proc oAACODCES_1_15.mZoomOnZoom
    local i_obj
    i_obj=GSCE_ACE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CECODICE=this.parent.oContained.w_AACODCES
     i_obj.ecpSave()
  endproc

  add object oESER1_1_16 as StdField with uid="VYETCQCPWH",rtseq=16,rtrep=.f.,;
    cFormVar = "w_ESER1", cQueryName = "ESER1",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Esercizio di selezione documenti",;
    HelpContextID = 75734458,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=487, Top=31, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_ESER1"

  func oESER1_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oESER1_1_16.ecpDrop(oSource)
    this.Parent.oContained.link_1_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oESER1_1_16.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CODAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_CODAZI)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oESER1_1_16'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Esercizi",'',this.parent.oContained
  endproc

  add object oDANUMREG1_1_17 as StdField with uid="IXTBPVSKKR",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DANUMREG1", cQueryName = "DANUMREG1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Numero registrazione di partenza < di quello di arrivo",;
    ToolTipText = "Da numero registrazione documenti",;
    HelpContextID = 255845261,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=487, Top=56, cSayPict='"999999"', cGetPict='"999999"'

  func oDANUMREG1_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (EMPTY(.w_AANUMREG1) OR .w_DANUMREG1<=.w_AANUMREG1)
    endwith
    return bRes
  endfunc

  add object oDADATA1_1_18 as StdField with uid="JVBHRQBPRZ",rtseq=18,rtrep=.f.,;
    cFormVar = "w_DADATA1", cQueryName = "DADATA1",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data di inizio selezione > di data fine",;
    ToolTipText = "Da data registrazione movimenti primanota",;
    HelpContextID = 23379914,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=697, Top=56

  func oDADATA1_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (EMPTY(.w_AADATA1) OR .w_DADATA1<=.w_AADATA1)
    endwith
    return bRes
  endfunc

  add object oAANUMREG1_1_19 as StdField with uid="VVSBEURTEI",rtseq=19,rtrep=.f.,;
    cFormVar = "w_AANUMREG1", cQueryName = "AANUMREG1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Numero registrazione di arrivo < di quello di partenza",;
    ToolTipText = "A numero registrazione documenti",;
    HelpContextID = 255845213,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=487, Top=81, cSayPict='"999999"', cGetPict='"999999"'

  func oAANUMREG1_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (EMPTY(.w_DANUMREG1) OR .w_DANUMREG1<=.w_AANUMREG1)
    endwith
    return bRes
  endfunc

  add object oAADATA1_1_20 as StdField with uid="UKVULLZJYP",rtseq=20,rtrep=.f.,;
    cFormVar = "w_AADATA1", cQueryName = "AADATA1",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data di fine selezione < di data inizio",;
    ToolTipText = "A data registrazione movimenti primanota",;
    HelpContextID = 23379962,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=697, Top=81

  func oAADATA1_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (EMPTY(.w_DADATA1) OR .w_DADATA1<=.w_AADATA1)
    endwith
    return bRes
  endfunc

  add object oDANUMDOC_1_21 as StdField with uid="HGARDKDIXS",rtseq=21,rtrep=.f.,;
    cFormVar = "w_DANUMDOC", cQueryName = "DANUMDOC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Numero documento di partenza < di quello di arrivo",;
    ToolTipText = "Da numero documento",;
    HelpContextID = 247472007,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=487, Top=106, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  func oDANUMDOC_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (EMPTY(.w_AANUMDOC) OR .w_DANUMDOC<=.w_AANUMDOC)
    endwith
    return bRes
  endfunc

  add object oSERIE1_1_22 as StdField with uid="HVPVSEHHGU",rtseq=22,rtrep=.f.,;
    cFormVar = "w_SERIE1", cQueryName = "SERIE1",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "La serie iniziale � pi� grande della serie finale",;
    ToolTipText = "Numero del documento iniziale selezionato",;
    HelpContextID = 38525658,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=613, Top=106, cSayPict='"!!!!!!!!!!"', cGetPict='"!!!!!!!!!!"', InputMask=replicate('X',10)

  func oSERIE1_1_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((empty(.w_SERIE2)) OR  (.w_SERIE1<=.w_SERIE2))
    endwith
    return bRes
  endfunc

  add object oAANUMDOC_1_23 as StdField with uid="GTVLADSZRI",rtseq=23,rtrep=.f.,;
    cFormVar = "w_AANUMDOC", cQueryName = "AANUMDOC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Numero documento di arrivo < di quello di partenza",;
    ToolTipText = "A numero documento",;
    HelpContextID = 247472055,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=487, Top=131, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  func oAANUMDOC_1_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (EMPTY(.w_DANUMDOC) OR .w_DANUMDOC<=.w_AANUMDOC)
    endwith
    return bRes
  endfunc

  add object oSERIE2_1_24 as StdField with uid="LHPNDMBVHJ",rtseq=24,rtrep=.f.,;
    cFormVar = "w_SERIE2", cQueryName = "SERIE2",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "La serie iniziale � maggire della serie finale",;
    ToolTipText = "Numero del documento finale selezionato",;
    HelpContextID = 21748442,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=613, Top=131, cSayPict='"!!!!!!!!!!"', cGetPict='"!!!!!!!!!!"', InputMask=replicate('X',10)

  func oSERIE2_1_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_serie2>=.w_serie1) or (empty(.w_serie1)))
    endwith
    return bRes
  endfunc

  add object oDADATAD_1_25 as StdField with uid="LFKEBECYCD",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DADATAD", cQueryName = "DADATAD",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data di inizio selezione > di data fine",;
    ToolTipText = "Da data documento",;
    HelpContextID = 245055542,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=487, Top=156

  func oDADATAD_1_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (EMPTY(.w_AADATAD) OR .w_DADATAD<=.w_AADATAD)
    endwith
    return bRes
  endfunc

  add object oAADATAD_1_26 as StdField with uid="AKAPKLRFYP",rtseq=26,rtrep=.f.,;
    cFormVar = "w_AADATAD", cQueryName = "AADATAD",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data di fine selezione < di data inizio",;
    ToolTipText = "A data documento",;
    HelpContextID = 245055494,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=697, Top=156

  func oAADATAD_1_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (EMPTY(.w_DADATAD) OR .w_DADATAD<=.w_AADATAD)
    endwith
    return bRes
  endfunc


  add object ZoomGepr as cp_szoombox with uid="JRRNXIAJMX",left=1, top=231, width=398,height=207,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="MOV_CESP",bReadOnly=.f.,bQueryOnLoad=.f.,bAdvOptions=.f.,cZoomFile="GSCE_KAM",bOptions=.f.,bQueryOnDblClick=.t.,cMenuFile="",cZoomOnZoom="",;
    cEvent = "Esegui",;
    nPag=1;
    , HelpContextID = 45203942


  add object oTIPDOC_1_57 as StdCombo with uid="XWDNZCRRFI",rtseq=31,rtrep=.f.,left=627,top=31,width=142,height=21;
    , ToolTipText = "Causale documento di selezione";
    , HelpContextID = 263255754;
    , cFormVar="w_TIPDOC",RowSource=""+"Tutti,"+"Fatture acquisto,"+"Fatture vendita,"+"Nota credito fornitore,"+"Nota credito cliente,"+"Corrispettivi", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPDOC_1_57.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'A',;
    iif(this.value =3,'V',;
    iif(this.value =4,'F',;
    iif(this.value =5,'N',;
    iif(this.value =6,'C',;
    space(1))))))))
  endfunc
  func oTIPDOC_1_57.GetRadio()
    this.Parent.oContained.w_TIPDOC = this.RadioValue()
    return .t.
  endfunc

  func oTIPDOC_1_57.SetRadio()
    this.Parent.oContained.w_TIPDOC=trim(this.Parent.oContained.w_TIPDOC)
    this.value = ;
      iif(this.Parent.oContained.w_TIPDOC=='T',1,;
      iif(this.Parent.oContained.w_TIPDOC=='A',2,;
      iif(this.Parent.oContained.w_TIPDOC=='V',3,;
      iif(this.Parent.oContained.w_TIPDOC=='F',4,;
      iif(this.Parent.oContained.w_TIPDOC=='N',5,;
      iif(this.Parent.oContained.w_TIPDOC=='C',6,;
      0))))))
  endfunc


  add object oBtn_1_59 as StdButton with uid="WTNXQBKWPQ",left=721, top=187, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per ricercare i movimenti da elaborare";
    , HelpContextID = 44128534;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_59.Click()
      with this.Parent.oContained
        GSCE_BAM(this.Parent.oContained,"CAR")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_60 as StdButton with uid="RTCJASDYLY",left=12, top=440, width=48,height=45,;
    CpPicture="bmp\mcesp.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per accedere al movimento cespite";
    , HelpContextID = 52253734;
    , caption='\<Cespite';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_60.Click()
      with this.Parent.oContained
        GSCE_BAM(this.Parent.oContained,"RMC")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_61 as StdButton with uid="VQNQDPBZVM",left=413, top=440, width=48,height=45,;
    CpPicture="bmp\documenti.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per accedere al documento associato";
    , HelpContextID = 194943590;
    , Caption='\<Docum.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_61.Click()
      with this.Parent.oContained
        GSCE_BAM(this.Parent.oContained,"RDO")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_62 as StdButton with uid="FZIYGHIBOV",left=670, top=440, width=48,height=45,;
    CpPicture="bmp\abbina.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per abbinare";
    , HelpContextID = 6635782;
    , caption='\<Abbina';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_62.Click()
      with this.Parent.oContained
        GSCE_BAM(this.Parent.oContained,"ABD")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_63 as StdButton with uid="EJOQUFKXVH",left=721, top=440, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 125476282;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_63.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESCESDA_1_68 as StdField with uid="SDUCHNGYOD",rtseq=36,rtrep=.f.,;
    cFormVar = "w_DESCESDA", cQueryName = "DESCESDA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 263074935,;
   bGlobalFont=.t.,;
    Height=21, Width=160, Left=238, Top=131, InputMask=replicate('X',40)

  add object oDESCESAA_1_69 as StdField with uid="EUYJZRCNHZ",rtseq=37,rtrep=.f.,;
    cFormVar = "w_DESCESAA", cQueryName = "DESCESAA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 263074935,;
   bGlobalFont=.t.,;
    Height=21, Width=160, Left=238, Top=156, InputMask=replicate('X',40)

  add object oDESCAU_1_70 as StdField with uid="QLYGBNPLHH",rtseq=38,rtrep=.f.,;
    cFormVar = "w_DESCAU", cQueryName = "DESCAU",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 23999542,;
   bGlobalFont=.t.,;
    Height=21, Width=244, Left=154, Top=106, InputMask=replicate('X',40)


  add object ZOOM as cp_szoombox with uid="NYGNGLKXNV",left=399, top=231, width=380,height=207,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="DOC_MAST",bReadOnly=.t.,bQueryOnLoad=.f.,bAdvOptions=.f.,cZoomFile="GSCE1ZDO",bOptions=.f.,;
    cEvent = "Esegui",;
    nPag=1;
    , HelpContextID = 45203942


  add object oObj_1_74 as cp_runprogram with uid="ABCYZAXKDJ",left=2, top=492, width=271,height=24,;
    caption='GSCE_BAM(DES)',;
   bGlobalFont=.t.,;
    prg="GSCE_BAM('DES')",;
    cEvent = "w_zoomgepr row checked,",;
    nPag=1;
    , HelpContextID = 53926195

  add object oStr_1_28 as StdString with uid="UQJQQFNRGA",Visible=.t., Left=3, Top=56,;
    Alignment=1, Width=83, Height=15,;
    Caption="Da reg. n.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="GDJCKFGEHP",Visible=.t., Left=3, Top=106,;
    Alignment=1, Width=83, Height=15,;
    Caption="Caus.cespite:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="QEUWZIFIIZ",Visible=.t., Left=8, Top=6,;
    Alignment=0, Width=110, Height=15,;
    Caption="Filtri mov. cespiti"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_31 as StdString with uid="WWVVZYVJRP",Visible=.t., Left=3, Top=31,;
    Alignment=1, Width=83, Height=15,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="IFJGTTKAQP",Visible=.t., Left=3, Top=81,;
    Alignment=1, Width=83, Height=15,;
    Caption="A reg. n.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="PYIMHEHIIS",Visible=.t., Left=255, Top=56,;
    Alignment=1, Width=70, Height=15,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="YVMWWYFGVD",Visible=.t., Left=262, Top=81,;
    Alignment=1, Width=63, Height=15,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="DSJMUQUPDP",Visible=.t., Left=3, Top=131,;
    Alignment=1, Width=83, Height=15,;
    Caption="Da cespite:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="BDPZFQMHTO",Visible=.t., Left=3, Top=156,;
    Alignment=1, Width=83, Height=15,;
    Caption="A cespite:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="OELPWMTJJO",Visible=.t., Left=11, Top=195,;
    Alignment=0, Width=132, Height=15,;
    Caption="Movimenti cespiti"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="QPLJVXYJPA",Visible=.t., Left=415, Top=195,;
    Alignment=0, Width=112, Height=15,;
    Caption="Documenti"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="LBFAABVARB",Visible=.t., Left=404, Top=56,;
    Alignment=1, Width=81, Height=15,;
    Caption="Da reg. n.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="BXPTWGZPDL",Visible=.t., Left=404, Top=31,;
    Alignment=1, Width=81, Height=15,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="FKKUEDOHQD",Visible=.t., Left=404, Top=81,;
    Alignment=1, Width=81, Height=15,;
    Caption="A reg. n.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_47 as StdString with uid="EVQWYLEMKN",Visible=.t., Left=595, Top=56,;
    Alignment=1, Width=99, Height=15,;
    Caption="Da data reg.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_48 as StdString with uid="ZJINGWXBNW",Visible=.t., Left=619, Top=81,;
    Alignment=1, Width=75, Height=18,;
    Caption="A data reg.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_50 as StdString with uid="NIHJGTFCUV",Visible=.t., Left=424, Top=6,;
    Alignment=0, Width=112, Height=18,;
    Caption="Filtri documenti"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_51 as StdString with uid="HHCRXJBUZS",Visible=.t., Left=404, Top=106,;
    Alignment=1, Width=81, Height=18,;
    Caption="Da num.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_52 as StdString with uid="VXFANLVWUM",Visible=.t., Left=404, Top=131,;
    Alignment=1, Width=81, Height=18,;
    Caption="A num.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_53 as StdString with uid="LVVBINNLET",Visible=.t., Left=607, Top=106,;
    Alignment=0, Width=8, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_54 as StdString with uid="PKVCFMTCZS",Visible=.t., Left=607, Top=131,;
    Alignment=0, Width=8, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_55 as StdString with uid="EKLQWFAAMN",Visible=.t., Left=404, Top=156,;
    Alignment=1, Width=81, Height=18,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_56 as StdString with uid="IOBJQMHNYI",Visible=.t., Left=631, Top=156,;
    Alignment=1, Width=63, Height=18,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_58 as StdString with uid="KTGGEKFVHY",Visible=.t., Left=538, Top=31,;
    Alignment=1, Width=84, Height=18,;
    Caption="Tipo doc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_72 as StdString with uid="KQTIQMCDOD",Visible=.t., Left=180, Top=30,;
    Alignment=1, Width=94, Height=18,;
    Caption="Mov.cespiti:"  ;
  , bGlobalFont=.t.

  add object oBox_1_43 as StdBox with uid="JSFVPYAUVA",left=-2, top=182, width=775,height=1

  add object oBox_1_49 as StdBox with uid="GRFDMUGYOD",left=402, top=1, width=1,height=181
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsce_kad','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
