* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_bag                                                        *
*              Inter. 1913                                                     *
*                                                                              *
*      Author: Zucchetti spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][207]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-04-19                                                      *
* Last revis.: 2000-04-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsce_bag",oParentObject)
return(i_retval)

define class tgsce_bag as StdBatch
  * --- Local variables
  w_MCSERIAL = space(10)
  w_MCRIFCON = space(10)
  * --- WorkFile variables
  MOV_CESP_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiornamento campi DB per intervento 1913
    * --- Select from MOV_CESP
    i_nConn=i_TableProp[this.MOV_CESP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOV_CESP_idx,2],.t.,this.MOV_CESP_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select MCSERIAL  from "+i_cTable+" MOV_CESP ";
          +" where MCRIFFAT is NULL";
           ,"_Curs_MOV_CESP")
    else
      select MCSERIAL from (i_cTable);
       where MCRIFFAT is NULL;
        into cursor _Curs_MOV_CESP
    endif
    if used('_Curs_MOV_CESP')
      select _Curs_MOV_CESP
      locate for 1=1
      do while not(eof())
      this.w_MCSERIAL = _Curs_MOV_CESP.MCSERIAL
      * --- Write into MOV_CESP
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.MOV_CESP_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MOV_CESP_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.MOV_CESP_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MCRIFFAT ="+cp_NullLink(cp_ToStrODBC(space(10)),'MOV_CESP','MCRIFFAT');
            +i_ccchkf ;
        +" where ";
            +"MCSERIAL = "+cp_ToStrODBC(this.w_MCSERIAL);
               )
      else
        update (i_cTable) set;
            MCRIFFAT = space(10);
            &i_ccchkf. ;
         where;
            MCSERIAL = this.w_MCSERIAL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
        select _Curs_MOV_CESP
        continue
      enddo
      use
    endif
    * --- Select from MOV_CESP
    i_nConn=i_TableProp[this.MOV_CESP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOV_CESP_idx,2],.t.,this.MOV_CESP_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select MCSERIAL, MCRIFCON  from "+i_cTable+" MOV_CESP ";
          +" where not MCRIFCON is null and not MCRIFCON like ' %' and MCDATCON is NULL";
           ,"_Curs_MOV_CESP")
    else
      select MCSERIAL, MCRIFCON from (i_cTable);
       where not MCRIFCON is null and not MCRIFCON like " %" and MCDATCON is NULL;
        into cursor _Curs_MOV_CESP
    endif
    if used('_Curs_MOV_CESP')
      select _Curs_MOV_CESP
      locate for 1=1
      do while not(eof())
      this.w_MCSERIAL = _Curs_MOV_CESP.MCSERIAL
      this.w_MCRIFCON = _Curs_MOV_CESP.MCRIFCON
      * --- Write into MOV_CESP
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.MOV_CESP_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MOV_CESP_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.MOV_CESP_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MCRIFFAT ="+cp_NullLink(cp_ToStrODBC(this.w_MCRIFCON),'MOV_CESP','MCRIFFAT');
        +",MCRIFCON ="+cp_NullLink(cp_ToStrODBC(space(10)),'MOV_CESP','MCRIFCON');
            +i_ccchkf ;
        +" where ";
            +"MCSERIAL = "+cp_ToStrODBC(this.w_MCSERIAL);
               )
      else
        update (i_cTable) set;
            MCRIFFAT = this.w_MCRIFCON;
            ,MCRIFCON = space(10);
            &i_ccchkf. ;
         where;
            MCSERIAL = this.w_MCSERIAL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
        select _Curs_MOV_CESP
        continue
      enddo
      use
    endif
    ah_ErrorMsg("Elaborazione terminata",64)
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='MOV_CESP'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_MOV_CESP')
      use in _Curs_MOV_CESP
    endif
    if used('_Curs_MOV_CESP')
      use in _Curs_MOV_CESP
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
