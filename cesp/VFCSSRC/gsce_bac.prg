* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_bac                                                        *
*              Lancia anagrafica clienti                                       *
*                                                                              *
*      Author: Mimmo Fasano                                                    *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_39]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-10-04                                                      *
* Last revis.: 2002-10-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTIPO
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsce_bac",oParentObject,m.pTIPO)
return(i_retval)

define class tgsce_bac as StdBatch
  * --- Local variables
  pTIPO = space(1)
  w_PROG = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lancia  maschera anagrafica clienti  GSAR_ACL
    *     pTIPO :  N= Chiamato dai Nominativi     
    do case
      case this.pTIPO="N" 
 
        this.w_PROG = GSAR_ACL()
        * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
        if !(this.w_PROG.bSec1)
          i_retcode = 'stop'
          return
        endif
        this.w_PROG.ecpFilter()     
        this.w_PROG.w_ANTIPCON = "C"
        this.w_PROG.w_ANCODICE = this.oParentObject.w_NOCODCLI
        this.w_PROG.ecpSave()     
        this.w_PROG.ecpQuery()     
    endcase
  endproc


  proc Init(oParentObject,pTIPO)
    this.pTIPO=pTIPO
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTIPO"
endproc
