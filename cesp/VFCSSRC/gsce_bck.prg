* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_bck                                                        *
*              Controlli finali cespite                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_41]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-03-09                                                      *
* Last revis.: 2008-09-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipo
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsce_bck",oParentObject,m.pTipo)
return(i_retval)

define class tgsce_bck as StdBatch
  * --- Local variables
  pTipo = space(1)
  w_APPO = space(10)
  w_MESS = space(10)
  w_ESERF = space(4)
  w_ESERC = space(4)
  w_PADRE = .NULL.
  w_oMESS = .NULL.
  w_oPART = .NULL.
  w_SERIALE = space(10)
  w_NUMDOC = 0
  w_ALFDOC = space(10)
  w_DATDOC = ctod("  /  /  ")
  w_RIGA = 0
  w_CODICE = space(20)
  w_OLDTIPCES = space(2)
  w_OK = .f.
  w_CESMOV = space(20)
  * --- WorkFile variables
  MOV_CESP_idx=0
  DOC_DETT_idx=0
  DOC_MAST_idx=0
  SAL_CESE_idx=0
  CES_PITI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controlli Finali sul Cespite, in Variazione (da GSCE_ACE)
    this.w_PADRE = this.oParentObject
    * --- Istanzio Oggetto Messaggio Incrementale
    this.w_oMESS=createobject("ah_message")
    do case
      case this.pTipo="D"
        * --- Controlli in cancellazione/modifica
        * --- Read from DOC_DETT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DOC_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MVSERIAL,CPROWORD"+;
            " from "+i_cTable+" DOC_DETT where ";
                +"MVCODCES = "+cp_ToStrODBC(this.oParentObject.w_CECODICE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MVSERIAL,CPROWORD;
            from (i_cTable) where;
                MVCODCES = this.oParentObject.w_CECODICE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_SERIALE = NVL(cp_ToDate(_read_.MVSERIAL),cp_NullValue(_read_.MVSERIAL))
          this.w_RIGA = NVL(cp_ToDate(_read_.CPROWORD),cp_NullValue(_read_.CPROWORD))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if i_ROWS>0
          * --- Read from DOC_MAST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DOC_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MVNUMDOC,MVALFDOC,MVDATDOC"+;
              " from "+i_cTable+" DOC_MAST where ";
                  +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIALE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MVNUMDOC,MVALFDOC,MVDATDOC;
              from (i_cTable) where;
                  MVSERIAL = this.w_SERIALE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_NUMDOC = NVL(cp_ToDate(_read_.MVNUMDOC),cp_NullValue(_read_.MVNUMDOC))
            this.w_ALFDOC = NVL(cp_ToDate(_read_.MVALFDOC),cp_NullValue(_read_.MVALFDOC))
            this.w_DATDOC = NVL(cp_ToDate(_read_.MVDATDOC),cp_NullValue(_read_.MVDATDOC))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_oPART = this.w_oMESS.addmsgpartNL("Codice cespite presente nei documenti%0(Doc.n.%1 del %2 riga n. %3)")
          this.w_oPART.addParam(ALLTRIM(STR(this.w_NUMDOC,15,0))+IIF(NOT EMPTY(this.w_ALFDOC),"/"+Alltrim(this.w_ALFDOC),""))     
          this.w_oPART.addParam(DTOC(this.w_DATDOC))     
          this.w_oPART.addParam(ALLTRIM(STR(this.w_RIGA,5,0)))     
          if this.w_PADRE.cfunction="Edit"
            * --- Read from CES_PITI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.CES_PITI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CES_PITI_idx,2],.t.,this.CES_PITI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "CETIPCES"+;
                " from "+i_cTable+" CES_PITI where ";
                    +"CECODICE = "+cp_ToStrODBC(this.oParentObject.w_CECODICE);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                CETIPCES;
                from (i_cTable) where;
                    CECODICE = this.oParentObject.w_CECODICE;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_OLDTIPCES = NVL(cp_ToDate(_read_.CETIPCES),cp_NullValue(_read_.CETIPCES))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if this.oParentObject.w_CETIPCES<>this.w_OLDTIPCES
              this.w_oMESS.addmsgpartNL("Impossibile modificare la tipologia del cespite")     
              this.w_MESS = this.w_oMESS.ComposeMessage()
              * --- transaction error
              bTrsErr=.t.
              i_TrsMsg=this.w_MESS
            else
              this.w_oMESS.addmsgpartNL("Le modifiche apportate potrebbero essere incongruenti con gli abbinamenti effettuati (solo warning)")     
              this.w_oMESS.ah_ErrorMsg()     
            endif
          else
            this.w_oMESS.addmsgpartNL("Impossibile cancellare")     
            this.w_MESS = this.w_oMESS.ComposeMessage()
            * --- transaction error
            bTrsErr=.t.
            i_TrsMsg=this.w_MESS
          endif
        endif
        * --- Verifico se il Cespite � presente nei Saldi Cespiti Pre-Euro
        *     e se il cespite � presente nei movimenti cespiti
        if this.w_PADRE.cfunction="Query"
          this.w_OK = .t.
          * --- Read from SAL_CESE
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.SAL_CESE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SAL_CESE_idx,2],.t.,this.SAL_CESE_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "SCCODCES"+;
              " from "+i_cTable+" SAL_CESE where ";
                  +"SCCODCES = "+cp_ToStrODBC(this.oParentObject.w_CECODICE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              SCCODCES;
              from (i_cTable) where;
                  SCCODCES = this.oParentObject.w_CECODICE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CODICE = NVL(cp_ToDate(_read_.SCCODCES),cp_NullValue(_read_.SCCODCES))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if i_ROWS>0
            this.w_oMESS.addmsgpartNL("Codice cespite presente nei saldi cespiti pre-Euro")     
            this.w_OK = .f.
          endif
          * --- Read from MOV_CESP
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.MOV_CESP_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MOV_CESP_idx,2],.t.,this.MOV_CESP_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MCCODCES"+;
              " from "+i_cTable+" MOV_CESP where ";
                  +"MCCODCES = "+cp_ToStrODBC(this.oParentObject.w_CECODICE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MCCODCES;
              from (i_cTable) where;
                  MCCODCES = this.oParentObject.w_CECODICE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CESMOV = NVL(cp_ToDate(_read_.MCCODCES),cp_NullValue(_read_.MCCODCES))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if i_ROWS>0 and this.w_OK
            this.w_oMESS.addmsgpartNL("Codice cespite presente nei movimenti cespiti")     
            this.w_OK = .f.
          endif
          if Not this.w_OK
            this.w_oMESS.addmsgpartNL("Impossibile cancellare")     
            this.w_MESS = this.w_oMESS.ComposeMessage()
            * --- transaction error
            bTrsErr=.t.
            i_TrsMsg=this.w_MESS
          endif
        endif
        if this.w_PADRE.cfunction="Edit"
          * --- Se Variata Categoria Cespite, Controlla che non ci siano Movimenti riferiti al Cespite
          this.w_APPO = SPACE(10)
          if this.oParentObject.w_CECODCAT<>this.oParentObject.w_OLDCAT AND NOT EMPTY(this.oParentObject.w_OLDCAT)
            * --- Read from MOV_CESP
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.MOV_CESP_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.MOV_CESP_idx,2],.t.,this.MOV_CESP_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "MCSERIAL"+;
                " from "+i_cTable+" MOV_CESP where ";
                    +"MCCODCES = "+cp_ToStrODBC(this.oParentObject.w_CECODICE);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                MCSERIAL;
                from (i_cTable) where;
                    MCCODCES = this.oParentObject.w_CECODICE;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_APPO = NVL(cp_ToDate(_read_.MCSERIAL),cp_NullValue(_read_.MCSERIAL))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if i_Rows<>0
              this.w_MESS = ah_Msgformat("Impossibile variare la categoria cespite%0Gi� inseriti movimenti riferiti al cespite")
              * --- transaction error
              bTrsErr=.t.
              i_TrsMsg=this.w_MESS
            endif
          endif
        endif
      case this.pTipo="C"
        * --- Controllo che la data di primo utilizzo sia inserita almeno in un esercizio.
        this.w_ESERF = CALCESER(this.oParentObject.w_CEDTPRIU,"   ")
        if EMPTY(this.w_ESERF)
          ah_ErrorMsg("Attenzione, data primo utilizzo fiscale non presente in alcun esercizio","!","")
          * --- Data non corretta (Sbianco la data inserita)
          this.oParentObject.w_CEDTPRIU = cp_CharToDate("  -  -  ")
        endif
      case this.pTipo="E"
        * --- Controllo che la data di primo utilizzo sia inserita almeno in un esercizio.
        this.w_ESERC = CALCESER(this.oParentObject.w_CEDTPRIC,"   ")
        if EMPTY(this.w_ESERC)
          ah_ErrorMsg("Attenzione, data primo utilizzo civile non presente in alcun esercizio","!","")
          * --- Data non corretta (Sbianco la data inserita)
          this.oParentObject.w_CEDTPRIC = cp_CharToDate("  -  -  ")
        endif
      case this.pTipo="I"
        if EMPTY(this.oParentObject.w_CECODICE)
          this.w_MESS = ah_Msgformat("Impossibile confermare%0Codice cespite obbligatorio")
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_MESS
        endif
      case this.pTipo="F"
        * --- Controllo che la data di dismissione sia maggiore della data di primo utilizzo
        if (this.oParentObject.w_CEDTPRIU>=this.oParentObject.w_CEDATDIS) OR (this.oParentObject.w_CEDTPRIC>=this.oParentObject.w_CEDATDIS)
          ah_ErrorMsg("Data di dismissione inferiore alla data di primo utilizzo","","")
          this.oParentObject.w_CEDATDIS = cp_CharToDate("  -  -  ")
          i_retcode = 'stop'
          return
        endif
        * --- Controllo che la data di dismissione sia inserita almeno in un esercizio.
        vq_exec("..\CESP\EXE\QUERY\FINESERCI.VQR",this,"ESERCI1")
        if RECCOUNT("ESERCI1")>0
          * --- Data compresa in almeno un'esercizio
        else
          ah_ErrorMsg("Data non presente in alcun esercizio")
          * --- Data non corretta (Sbianco la data inserita)
          this.oParentObject.w_CEDATDIS = cp_CharToDate("  -  -  ")
          i_retcode = 'stop'
          return
        endif
    endcase
    * --- Elimina Cursori di Appoggio
    if USED("ESERCI1")
      SELECT ESERCI1
      USE
    endif
    * --- Elimina Cursori di Appoggio
    if USED("ESERCI")
      SELECT ESERCI
      USE
    endif
  endproc


  proc Init(oParentObject,pTipo)
    this.pTipo=pTipo
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='MOV_CESP'
    this.cWorkTables[2]='DOC_DETT'
    this.cWorkTables[3]='DOC_MAST'
    this.cWorkTables[4]='SAL_CESE'
    this.cWorkTables[5]='CES_PITI'
    return(this.OpenAllTables(5))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipo"
endproc
