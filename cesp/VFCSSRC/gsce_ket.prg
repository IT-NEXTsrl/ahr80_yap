* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_ket                                                        *
*              Stampa etichette                                                *
*                                                                              *
*      Author: TAM Software srl                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-04-15                                                      *
* Last revis.: 2014-09-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsce_ket",oParentObject))

* --- Class definition
define class tgsce_ket as StdForm
  Top    = 50
  Left   = 127

  * --- Standard Properties
  Width  = 513
  Height = 200
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-09-16"
  HelpContextID=65684841
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=6

  * --- Constant Properties
  _IDX = 0
  CES_PITI_IDX = 0
  cPrg = "gsce_ket"
  cComment = "Stampa etichette"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CESPINI = space(20)
  w_CESPFIN = space(20)
  w_NOCESP = space(1)
  o_NOCESP = space(1)
  w_DATDISM = ctod('  /  /  ')
  w_DESCINI = space(40)
  w_DESCFIN = space(40)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsce_ketPag1","gsce_ket",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCESPINI_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='CES_PITI'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      vx_exec(""+alltrim(THIS.w_OQRY)+", "+alltrim(THIS.w_orep)+"",this)
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CESPINI=space(20)
      .w_CESPFIN=space(20)
      .w_NOCESP=space(1)
      .w_DATDISM=ctod("  /  /  ")
      .w_DESCINI=space(40)
      .w_DESCFIN=space(40)
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CESPINI))
          .link_1_1('Full')
        endif
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CESPFIN))
          .link_1_2('Full')
        endif
        .w_NOCESP = 'N'
        .w_DATDISM = IIF(.w_NOCESP='S',i_DATSYS,CP_CHARTODATE('  -  -  '))
      .oPgFrm.Page1.oPag.oObj_1_10.Calculate()
    endwith
    this.DoRTCalc(5,6,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,3,.t.)
        if .o_NOCESP<>.w_NOCESP
            .w_DATDISM = IIF(.w_NOCESP='S',i_DATSYS,CP_CHARTODATE('  -  -  '))
        endif
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(5,6,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oDATDISM_1_4.visible=!this.oPgFrm.Page1.oPag.oDATDISM_1_4.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_13.visible=!this.oPgFrm.Page1.oPag.oStr_1_13.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_10.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CESPINI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CES_PITI_IDX,3]
    i_lTable = "CES_PITI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CES_PITI_IDX,2], .t., this.CES_PITI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CES_PITI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CESPINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCE_ACE',True,'CES_PITI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CECODICE like "+cp_ToStrODBC(trim(this.w_CESPINI)+"%");

          i_ret=cp_SQL(i_nConn,"select CECODICE,CEDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CECODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CECODICE',trim(this.w_CESPINI))
          select CECODICE,CEDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CECODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CESPINI)==trim(_Link_.CECODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CESPINI) and !this.bDontReportError
            deferred_cp_zoom('CES_PITI','*','CECODICE',cp_AbsName(oSource.parent,'oCESPINI_1_1'),i_cWhere,'GSCE_ACE',"Anagrafica cespiti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CECODICE,CEDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CECODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CECODICE',oSource.xKey(1))
            select CECODICE,CEDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CESPINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CECODICE,CEDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CECODICE="+cp_ToStrODBC(this.w_CESPINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CECODICE',this.w_CESPINI)
            select CECODICE,CEDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CESPINI = NVL(_Link_.CECODICE,space(20))
      this.w_DESCINI = NVL(_Link_.CEDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CESPINI = space(20)
      endif
      this.w_DESCINI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CESPINI<=.w_CESPFIN OR EMPTY(.w_CESPFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CESPINI = space(20)
        this.w_DESCINI = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CES_PITI_IDX,2])+'\'+cp_ToStr(_Link_.CECODICE,1)
      cp_ShowWarn(i_cKey,this.CES_PITI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CESPINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CESPFIN
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CES_PITI_IDX,3]
    i_lTable = "CES_PITI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CES_PITI_IDX,2], .t., this.CES_PITI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CES_PITI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CESPFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCE_ACE',True,'CES_PITI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CECODICE like "+cp_ToStrODBC(trim(this.w_CESPFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select CECODICE,CEDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CECODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CECODICE',trim(this.w_CESPFIN))
          select CECODICE,CEDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CECODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CESPFIN)==trim(_Link_.CECODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CESPFIN) and !this.bDontReportError
            deferred_cp_zoom('CES_PITI','*','CECODICE',cp_AbsName(oSource.parent,'oCESPFIN_1_2'),i_cWhere,'GSCE_ACE',"Anagrafica cespiti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CECODICE,CEDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CECODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CECODICE',oSource.xKey(1))
            select CECODICE,CEDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CESPFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CECODICE,CEDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CECODICE="+cp_ToStrODBC(this.w_CESPFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CECODICE',this.w_CESPFIN)
            select CECODICE,CEDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CESPFIN = NVL(_Link_.CECODICE,space(20))
      this.w_DESCFIN = NVL(_Link_.CEDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CESPFIN = space(20)
      endif
      this.w_DESCFIN = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CESPFIN>=.w_CESPINI OR EMPTY(.w_CESPINI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CESPFIN = space(20)
        this.w_DESCFIN = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CES_PITI_IDX,2])+'\'+cp_ToStr(_Link_.CECODICE,1)
      cp_ShowWarn(i_cKey,this.CES_PITI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CESPFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCESPINI_1_1.value==this.w_CESPINI)
      this.oPgFrm.Page1.oPag.oCESPINI_1_1.value=this.w_CESPINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCESPFIN_1_2.value==this.w_CESPFIN)
      this.oPgFrm.Page1.oPag.oCESPFIN_1_2.value=this.w_CESPFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oNOCESP_1_3.RadioValue()==this.w_NOCESP)
      this.oPgFrm.Page1.oPag.oNOCESP_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDATDISM_1_4.value==this.w_DATDISM)
      this.oPgFrm.Page1.oPag.oDATDISM_1_4.value=this.w_DATDISM
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCINI_1_7.value==this.w_DESCINI)
      this.oPgFrm.Page1.oPag.oDESCINI_1_7.value=this.w_DESCINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCFIN_1_8.value==this.w_DESCFIN)
      this.oPgFrm.Page1.oPag.oDESCFIN_1_8.value=this.w_DESCFIN
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_CESPINI<=.w_CESPFIN OR EMPTY(.w_CESPFIN))  and not(empty(.w_CESPINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCESPINI_1_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CESPFIN>=.w_CESPINI OR EMPTY(.w_CESPINI))  and not(empty(.w_CESPFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCESPFIN_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_NOCESP = this.w_NOCESP
    return

enddefine

* --- Define pages as container
define class tgsce_ketPag1 as StdContainer
  Width  = 509
  height = 200
  stdWidth  = 509
  stdheight = 200
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCESPINI_1_1 as StdField with uid="VWJCAYQUNV",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CESPINI", cQueryName = "CESPINI",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Cespite di inizio selezione",;
    HelpContextID = 17091546,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=106, Top=21, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="CES_PITI", cZoomOnZoom="GSCE_ACE", oKey_1_1="CECODICE", oKey_1_2="this.w_CESPINI"

  func oCESPINI_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oCESPINI_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCESPINI_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CES_PITI','*','CECODICE',cp_AbsName(this.parent,'oCESPINI_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCE_ACE',"Anagrafica cespiti",'',this.parent.oContained
  endproc
  proc oCESPINI_1_1.mZoomOnZoom
    local i_obj
    i_obj=GSCE_ACE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CECODICE=this.parent.oContained.w_CESPINI
     i_obj.ecpSave()
  endproc

  add object oCESPFIN_1_2 as StdField with uid="IXFRRCPPMV",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CESPFIN", cQueryName = "CESPFIN",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Cespite di fine selezione",;
    HelpContextID = 104123354,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=106, Top=50, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="CES_PITI", cZoomOnZoom="GSCE_ACE", oKey_1_1="CECODICE", oKey_1_2="this.w_CESPFIN"

  func oCESPFIN_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCESPFIN_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCESPFIN_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CES_PITI','*','CECODICE',cp_AbsName(this.parent,'oCESPFIN_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCE_ACE',"Anagrafica cespiti",'',this.parent.oContained
  endproc
  proc oCESPFIN_1_2.mZoomOnZoom
    local i_obj
    i_obj=GSCE_ACE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CECODICE=this.parent.oContained.w_CESPFIN
     i_obj.ecpSave()
  endproc

  add object oNOCESP_1_3 as StdCheck with uid="MXGTBKTWTF",rtseq=3,rtrep=.f.,left=106, top=79, caption="Escludi cespiti dismessi",;
    ToolTipText = "Se attivo esclude cespiti dismessi",;
    HelpContextID = 26164950,;
    cFormVar="w_NOCESP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oNOCESP_1_3.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oNOCESP_1_3.GetRadio()
    this.Parent.oContained.w_NOCESP = this.RadioValue()
    return .t.
  endfunc

  func oNOCESP_1_3.SetRadio()
    this.Parent.oContained.w_NOCESP=trim(this.Parent.oContained.w_NOCESP)
    this.value = ;
      iif(this.Parent.oContained.w_NOCESP=='S',1,;
      0)
  endfunc

  add object oDATDISM_1_4 as StdField with uid="WKLHJYORBI",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DATDISM", cQueryName = "DATDISM",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 202424266,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=427, Top=79

  func oDATDISM_1_4.mHide()
    with this.Parent.oContained
      return (.w_NOCESP<>'S')
    endwith
  endfunc

  add object oDESCINI_1_7 as StdField with uid="NKJNPOXBXO",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DESCINI", cQueryName = "DESCINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 17943498,;
   bGlobalFont=.t.,;
    Height=21, Width=229, Left=274, Top=21, InputMask=replicate('X',40)

  add object oDESCFIN_1_8 as StdField with uid="DQQBBSWNAQ",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DESCFIN", cQueryName = "DESCFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 104975306,;
   bGlobalFont=.t.,;
    Height=21, Width=229, Left=274, Top=50, InputMask=replicate('X',40)


  add object oObj_1_10 as cp_outputCombo with uid="ZLERXIVPWT",left=106, top=120, width=385,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 112312806


  add object oBtn_1_11 as StdButton with uid="ZMIZZNTUDJ",left=402, top=148, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per stampare";
    , HelpContextID = 10529770;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_11.Click()
      with this.Parent.oContained
        do GSCE_BET with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_12 as StdButton with uid="JWIBHYOYEX",left=454, top=148, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 58367418;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_12.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_5 as StdString with uid="AQSMXYKXZQ",Visible=.t., Left=20, Top=22,;
    Alignment=1, Width=84, Height=15,;
    Caption="Da cespite:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="DSYXRXDDNX",Visible=.t., Left=23, Top=51,;
    Alignment=1, Width=81, Height=15,;
    Caption="A cespite:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="PAEJGVFBXM",Visible=.t., Left=7, Top=120,;
    Alignment=1, Width=97, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="GCQIKMAFZI",Visible=.t., Left=299, Top=79,;
    Alignment=1, Width=125, Height=18,;
    Caption="Data di dismissione:"  ;
  , bGlobalFont=.t.

  func oStr_1_13.mHide()
    with this.Parent.oContained
      return (.w_NOCESP<>'S')
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsce_ket','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
