* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_apa                                                        *
*              Piano di ammortamento                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30] [VRS_74]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-03-19                                                      *
* Last revis.: 2012-08-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsce_apa"))

* --- Class definition
define class tgsce_apa as StdForm
  Top    = 2
  Left   = 9

  * --- Standard Properties
  Width  = 560
  Height = 320+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-08-31"
  HelpContextID=160056681
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=32

  * --- Constant Properties
  CES_PIAN_IDX = 0
  ESERCIZI_IDX = 0
  CAT_CESP_IDX = 0
  VALUTE_IDX = 0
  PAR_CESP_IDX = 0
  cFile = "CES_PIAN"
  cKeySelect = "PANUMREG,PACODESE"
  cKeyWhere  = "PANUMREG=this.w_PANUMREG and PACODESE=this.w_PACODESE"
  cKeyWhereODBC = '"PANUMREG="+cp_ToStrODBC(this.w_PANUMREG)';
      +'+" and PACODESE="+cp_ToStrODBC(this.w_PACODESE)';

  cKeyWhereODBCqualified = '"CES_PIAN.PANUMREG="+cp_ToStrODBC(this.w_PANUMREG)';
      +'+" and CES_PIAN.PACODESE="+cp_ToStrODBC(this.w_PACODESE)';

  cPrg = "gsce_apa"
  cComment = "Piano di ammortamento"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CODAZI = space(5)
  w_PCTIPAMM = space(1)
  w_PEARANT = 0
  o_PEARANT = 0
  w_PERRID = 0
  w_ELABORA = .F.
  w_PANUMREG = space(6)
  o_PANUMREG = space(6)
  w_PADATREG = ctod('  /  /  ')
  w_PACODESE = space(4)
  o_PACODESE = space(4)
  w_VALNAZ = space(3)
  w_ESEFIN = ctod('  /  /  ')
  o_ESEFIN = ctod('  /  /  ')
  w_ESEINI = ctod('  /  /  ')
  o_ESEINI = ctod('  /  /  ')
  w_TOTDEC = 0
  o_TOTDEC = 0
  w_TIPOPE = space(10)
  w_PADESCRI = space(40)
  w_PAFLDEFI = space(1)
  w_NUMMESI = 0
  w_PATIPBEN = space(1)
  w_PA_STATO = space(1)
  w_PATIPGEN = space(1)
  w_PANUMFRA = 0
  w_PANUMMES = 0
  w_PADATINI = ctod('  /  /  ')
  w_PADATFIN = ctod('  /  /  ')
  w_PACODCAT = space(15)
  o_PACODCAT = space(15)
  w_DESCAT = space(40)
  w_TIPBEN = space(1)
  w_PACOECIV = 0
  w_PACOEFI1 = 0
  o_PACOEFI1 = 0
  w_PERCANT = 0
  o_PERCANT = 0
  w_PACOEFI2 = 0
  w_OFLDEFI = space(10)
  w_CALCPIC = 0

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_PACODESE = this.W_PACODESE
  op_PANUMREG = this.W_PANUMREG
  * --- Area Manuale = Declare Variables
  * --- gsce_apa
  *Ridefinita la ecpsave per far s� che al salvataggio la maschera
  *nn si riposizioni in caricamento bens� in interroga
  
  proc ecpsave()
      DoDefault()
      if this.cFunction='Load'
  		  if Not this.bUpdated
  			 this.ecpquit()
  		  endif
      endif
    endproc
  
    proc ecpload()
     this.bUpdated =.T.
     DoDefault()
    endproc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'CES_PIAN','gsce_apa')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsce_apaPag1","gsce_apa",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Piano di ammortamento")
      .Pages(1).HelpContextID = 22431600
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPANUMREG_1_6
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='ESERCIZI'
    this.cWorkTables[2]='CAT_CESP'
    this.cWorkTables[3]='VALUTE'
    this.cWorkTables[4]='PAR_CESP'
    this.cWorkTables[5]='CES_PIAN'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(5))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CES_PIAN_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CES_PIAN_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_PANUMREG = NVL(PANUMREG,space(6))
      .w_PACODESE = NVL(PACODESE,space(4))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_24_joined
    link_1_24_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from CES_PIAN where PANUMREG=KeySet.PANUMREG
    *                            and PACODESE=KeySet.PACODESE
    *
    i_nConn = i_TableProp[this.CES_PIAN_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CES_PIAN_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CES_PIAN')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CES_PIAN.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' CES_PIAN '
      link_1_24_joined=this.AddJoinedLink_1_24(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'PANUMREG',this.w_PANUMREG  ,'PACODESE',this.w_PACODESE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_CODAZI = i_CODAZI
        .w_PCTIPAMM = space(1)
        .w_PEARANT = 0
        .w_PERRID = 0
        .w_ELABORA = .T.
        .w_VALNAZ = space(3)
        .w_ESEFIN = ctod("  /  /  ")
        .w_ESEINI = ctod("  /  /  ")
        .w_TOTDEC = 0
        .w_DESCAT = space(40)
        .w_TIPBEN = space(1)
          .link_1_1('Load')
        .w_PANUMREG = NVL(PANUMREG,space(6))
        .op_PANUMREG = .w_PANUMREG
        .w_PADATREG = NVL(cp_ToDate(PADATREG),ctod("  /  /  "))
        .w_PACODESE = NVL(PACODESE,space(4))
        .op_PACODESE = .w_PACODESE
          .link_1_8('Load')
          .link_1_9('Load')
        .w_TIPOPE = this.cFunction
        .w_PADESCRI = NVL(PADESCRI,space(40))
        .w_PAFLDEFI = NVL(PAFLDEFI,space(1))
        .w_NUMMESI = IIF(NOT EMPTY(.w_PACODESE),DIFFMESE(.w_ESEINI,.w_ESEFIN),0)
        .w_PATIPBEN = NVL(PATIPBEN,space(1))
        .w_PA_STATO = NVL(PA_STATO,space(1))
        .w_PATIPGEN = NVL(PATIPGEN,space(1))
        .w_PANUMFRA = NVL(PANUMFRA,0)
        .w_PANUMMES = NVL(PANUMMES,0)
        .w_PADATINI = NVL(cp_ToDate(PADATINI),ctod("  /  /  "))
        .w_PADATFIN = NVL(cp_ToDate(PADATFIN),ctod("  /  /  "))
        .w_PACODCAT = NVL(PACODCAT,space(15))
          if link_1_24_joined
            this.w_PACODCAT = NVL(CCCODICE124,NVL(this.w_PACODCAT,space(15)))
            this.w_DESCAT = NVL(CCDESCRI124,space(40))
            this.w_TIPBEN = NVL(CCTIPBEN124,space(1))
          else
          .link_1_24('Load')
          endif
        .w_PACOECIV = NVL(PACOECIV,0)
        .w_PACOEFI1 = NVL(PACOEFI1,0)
        .w_PERCANT = cp_ROUND(.w_PACOEFI1*(.w_PEARANT/100),2)
        .w_PACOEFI2 = NVL(PACOEFI2,0)
        .oPgFrm.Page1.oPag.oObj_1_37.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_40.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
        .w_OFLDEFI = .w_PAFLDEFI
        .w_CALCPIC = DEFPIC(.w_TOTDEC)
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'CES_PIAN')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_45.enabled = this.oPgFrm.Page1.oPag.oBtn_1_45.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_46.enabled = this.oPgFrm.Page1.oPag.oBtn_1_46.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_59.enabled = this.oPgFrm.Page1.oPag.oBtn_1_59.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI = space(5)
      .w_PCTIPAMM = space(1)
      .w_PEARANT = 0
      .w_PERRID = 0
      .w_ELABORA = .f.
      .w_PANUMREG = space(6)
      .w_PADATREG = ctod("  /  /  ")
      .w_PACODESE = space(4)
      .w_VALNAZ = space(3)
      .w_ESEFIN = ctod("  /  /  ")
      .w_ESEINI = ctod("  /  /  ")
      .w_TOTDEC = 0
      .w_TIPOPE = space(10)
      .w_PADESCRI = space(40)
      .w_PAFLDEFI = space(1)
      .w_NUMMESI = 0
      .w_PATIPBEN = space(1)
      .w_PA_STATO = space(1)
      .w_PATIPGEN = space(1)
      .w_PANUMFRA = 0
      .w_PANUMMES = 0
      .w_PADATINI = ctod("  /  /  ")
      .w_PADATFIN = ctod("  /  /  ")
      .w_PACODCAT = space(15)
      .w_DESCAT = space(40)
      .w_TIPBEN = space(1)
      .w_PACOECIV = 0
      .w_PACOEFI1 = 0
      .w_PERCANT = 0
      .w_PACOEFI2 = 0
      .w_OFLDEFI = space(10)
      .w_CALCPIC = 0
      if .cFunction<>"Filter"
        .w_CODAZI = i_CODAZI
        .DoRTCalc(1,1,.f.)
          if not(empty(.w_CODAZI))
          .link_1_1('Full')
          endif
          .DoRTCalc(2,4,.f.)
        .w_ELABORA = .T.
          .DoRTCalc(6,6,.f.)
        .w_PADATREG = i_datsys
        .w_PACODESE = g_CODESE
        .DoRTCalc(8,8,.f.)
          if not(empty(.w_PACODESE))
          .link_1_8('Full')
          endif
        .DoRTCalc(9,9,.f.)
          if not(empty(.w_VALNAZ))
          .link_1_9('Full')
          endif
          .DoRTCalc(10,12,.f.)
        .w_TIPOPE = this.cFunction
          .DoRTCalc(14,14,.f.)
        .w_PAFLDEFI = 'P'
        .w_NUMMESI = IIF(NOT EMPTY(.w_PACODESE),DIFFMESE(.w_ESEINI,.w_ESEFIN),0)
        .w_PATIPBEN = 'T'
        .w_PA_STATO = 'E'
        .w_PATIPGEN = 'D'
        .w_PANUMFRA = .w_NUMMESI
        .w_PANUMMES = 12
        .w_PADATINI = .w_Eseini
        .w_PADATFIN = .w_ESEFIN
        .DoRTCalc(24,24,.f.)
          if not(empty(.w_PACODCAT))
          .link_1_24('Full')
          endif
          .DoRTCalc(25,26,.f.)
        .w_PACOECIV = IIF(NOT EMPTY(.w_PACODCAT),.w_PACOECIV,0)
        .w_PACOEFI1 = IIF(NOT EMPTY(.w_PACODCAT),.w_PACOEFI1,0)
        .w_PERCANT = cp_ROUND(.w_PACOEFI1*(.w_PEARANT/100),2)
        .w_PACOEFI2 = IIF(.w_TIPBEN='M' AND NOT EMPTY(.w_PACODCAT),.w_PERCANT,0)
        .oPgFrm.Page1.oPag.oObj_1_37.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_40.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
        .w_OFLDEFI = .w_PAFLDEFI
        .w_CALCPIC = DEFPIC(.w_TOTDEC)
      endif
    endwith
    cp_BlankRecExtFlds(this,'CES_PIAN')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_45.enabled = this.oPgFrm.Page1.oPag.oBtn_1_45.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_46.enabled = this.oPgFrm.Page1.oPag.oBtn_1_46.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_59.enabled = this.oPgFrm.Page1.oPag.oBtn_1_59.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CES_PIAN_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CES_PIAN_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"PREPA","i_codazi,w_PACODESE,w_PANUMREG")
      .op_codazi = .w_codazi
      .op_PACODESE = .w_PACODESE
      .op_PANUMREG = .w_PANUMREG
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oPANUMREG_1_6.enabled = i_bVal
      .Page1.oPag.oPADATREG_1_7.enabled = i_bVal
      .Page1.oPag.oPACODESE_1_8.enabled = i_bVal
      .Page1.oPag.oPADESCRI_1_14.enabled = i_bVal
      .Page1.oPag.oPAFLDEFI_1_15.enabled = i_bVal
      .Page1.oPag.oPATIPBEN_1_17.enabled = i_bVal
      .Page1.oPag.oPA_STATO_1_18.enabled = i_bVal
      .Page1.oPag.oPATIPGEN_1_19.enabled = i_bVal
      .Page1.oPag.oPANUMFRA_1_20.enabled = i_bVal
      .Page1.oPag.oPANUMMES_1_21.enabled = i_bVal
      .Page1.oPag.oPADATINI_1_22.enabled = i_bVal
      .Page1.oPag.oPADATFIN_1_23.enabled = i_bVal
      .Page1.oPag.oPACODCAT_1_24.enabled = i_bVal
      .Page1.oPag.oPACOECIV_1_27.enabled = i_bVal
      .Page1.oPag.oPACOEFI1_1_28.enabled = i_bVal
      .Page1.oPag.oPACOEFI2_1_30.enabled = i_bVal
      .Page1.oPag.oBtn_1_44.enabled = i_bVal
      .Page1.oPag.oBtn_1_45.enabled = .Page1.oPag.oBtn_1_45.mCond()
      .Page1.oPag.oBtn_1_46.enabled = .Page1.oPag.oBtn_1_46.mCond()
      .Page1.oPag.oBtn_1_59.enabled = .Page1.oPag.oBtn_1_59.mCond()
      .Page1.oPag.oObj_1_37.enabled = i_bVal
      .Page1.oPag.oObj_1_39.enabled = i_bVal
      .Page1.oPag.oObj_1_40.enabled = i_bVal
      .Page1.oPag.oObj_1_41.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oPANUMREG_1_6.enabled = .f.
        .Page1.oPag.oPACODESE_1_8.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oPANUMREG_1_6.enabled = .t.
        .Page1.oPag.oPACODESE_1_8.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'CES_PIAN',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CES_PIAN_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PANUMREG,"PANUMREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PADATREG,"PADATREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PACODESE,"PACODESE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PADESCRI,"PADESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAFLDEFI,"PAFLDEFI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PATIPBEN,"PATIPBEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PA_STATO,"PA_STATO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PATIPGEN,"PATIPGEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PANUMFRA,"PANUMFRA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PANUMMES,"PANUMMES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PADATINI,"PADATINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PADATFIN,"PADATFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PACODCAT,"PACODCAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PACOECIV,"PACOECIV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PACOEFI1,"PACOEFI1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PACOEFI2,"PACOEFI2",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.CES_PIAN_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CES_PIAN_IDX,2])
    i_lTable = "CES_PIAN"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.CES_PIAN_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CES_PIAN_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CES_PIAN_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.CES_PIAN_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"PREPA","i_codazi,w_PACODESE,w_PANUMREG")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into CES_PIAN
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CES_PIAN')
        i_extval=cp_InsertValODBCExtFlds(this,'CES_PIAN')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(PANUMREG,PADATREG,PACODESE,PADESCRI,PAFLDEFI"+;
                  ",PATIPBEN,PA_STATO,PATIPGEN,PANUMFRA,PANUMMES"+;
                  ",PADATINI,PADATFIN,PACODCAT,PACOECIV,PACOEFI1"+;
                  ",PACOEFI2 "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_PANUMREG)+;
                  ","+cp_ToStrODBC(this.w_PADATREG)+;
                  ","+cp_ToStrODBCNull(this.w_PACODESE)+;
                  ","+cp_ToStrODBC(this.w_PADESCRI)+;
                  ","+cp_ToStrODBC(this.w_PAFLDEFI)+;
                  ","+cp_ToStrODBC(this.w_PATIPBEN)+;
                  ","+cp_ToStrODBC(this.w_PA_STATO)+;
                  ","+cp_ToStrODBC(this.w_PATIPGEN)+;
                  ","+cp_ToStrODBC(this.w_PANUMFRA)+;
                  ","+cp_ToStrODBC(this.w_PANUMMES)+;
                  ","+cp_ToStrODBC(this.w_PADATINI)+;
                  ","+cp_ToStrODBC(this.w_PADATFIN)+;
                  ","+cp_ToStrODBCNull(this.w_PACODCAT)+;
                  ","+cp_ToStrODBC(this.w_PACOECIV)+;
                  ","+cp_ToStrODBC(this.w_PACOEFI1)+;
                  ","+cp_ToStrODBC(this.w_PACOEFI2)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CES_PIAN')
        i_extval=cp_InsertValVFPExtFlds(this,'CES_PIAN')
        cp_CheckDeletedKey(i_cTable,0,'PANUMREG',this.w_PANUMREG,'PACODESE',this.w_PACODESE)
        INSERT INTO (i_cTable);
              (PANUMREG,PADATREG,PACODESE,PADESCRI,PAFLDEFI,PATIPBEN,PA_STATO,PATIPGEN,PANUMFRA,PANUMMES,PADATINI,PADATFIN,PACODCAT,PACOECIV,PACOEFI1,PACOEFI2  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_PANUMREG;
                  ,this.w_PADATREG;
                  ,this.w_PACODESE;
                  ,this.w_PADESCRI;
                  ,this.w_PAFLDEFI;
                  ,this.w_PATIPBEN;
                  ,this.w_PA_STATO;
                  ,this.w_PATIPGEN;
                  ,this.w_PANUMFRA;
                  ,this.w_PANUMMES;
                  ,this.w_PADATINI;
                  ,this.w_PADATFIN;
                  ,this.w_PACODCAT;
                  ,this.w_PACOECIV;
                  ,this.w_PACOEFI1;
                  ,this.w_PACOEFI2;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.CES_PIAN_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CES_PIAN_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.CES_PIAN_IDX,i_nConn)
      *
      * update CES_PIAN
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'CES_PIAN')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " PADATREG="+cp_ToStrODBC(this.w_PADATREG)+;
             ",PADESCRI="+cp_ToStrODBC(this.w_PADESCRI)+;
             ",PAFLDEFI="+cp_ToStrODBC(this.w_PAFLDEFI)+;
             ",PATIPBEN="+cp_ToStrODBC(this.w_PATIPBEN)+;
             ",PA_STATO="+cp_ToStrODBC(this.w_PA_STATO)+;
             ",PATIPGEN="+cp_ToStrODBC(this.w_PATIPGEN)+;
             ",PANUMFRA="+cp_ToStrODBC(this.w_PANUMFRA)+;
             ",PANUMMES="+cp_ToStrODBC(this.w_PANUMMES)+;
             ",PADATINI="+cp_ToStrODBC(this.w_PADATINI)+;
             ",PADATFIN="+cp_ToStrODBC(this.w_PADATFIN)+;
             ",PACODCAT="+cp_ToStrODBCNull(this.w_PACODCAT)+;
             ",PACOECIV="+cp_ToStrODBC(this.w_PACOECIV)+;
             ",PACOEFI1="+cp_ToStrODBC(this.w_PACOEFI1)+;
             ",PACOEFI2="+cp_ToStrODBC(this.w_PACOEFI2)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'CES_PIAN')
        i_cWhere = cp_PKFox(i_cTable  ,'PANUMREG',this.w_PANUMREG  ,'PACODESE',this.w_PACODESE  )
        UPDATE (i_cTable) SET;
              PADATREG=this.w_PADATREG;
             ,PADESCRI=this.w_PADESCRI;
             ,PAFLDEFI=this.w_PAFLDEFI;
             ,PATIPBEN=this.w_PATIPBEN;
             ,PA_STATO=this.w_PA_STATO;
             ,PATIPGEN=this.w_PATIPGEN;
             ,PANUMFRA=this.w_PANUMFRA;
             ,PANUMMES=this.w_PANUMMES;
             ,PADATINI=this.w_PADATINI;
             ,PADATFIN=this.w_PADATFIN;
             ,PACODCAT=this.w_PACODCAT;
             ,PACOECIV=this.w_PACOECIV;
             ,PACOEFI1=this.w_PACOEFI1;
             ,PACOEFI2=this.w_PACOEFI2;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.CES_PIAN_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CES_PIAN_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.CES_PIAN_IDX,i_nConn)
      *
      * delete CES_PIAN
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'PANUMREG',this.w_PANUMREG  ,'PACODESE',this.w_PACODESE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CES_PIAN_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CES_PIAN_IDX,2])
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,8,.t.)
        if .o_PACODESE<>.w_PACODESE
          .link_1_9('Full')
        endif
        .DoRTCalc(10,12,.t.)
            .w_TIPOPE = this.cFunction
        .DoRTCalc(14,15,.t.)
        if .o_PACODESE<>.w_PACODESE
            .w_NUMMESI = IIF(NOT EMPTY(.w_PACODESE),DIFFMESE(.w_ESEINI,.w_ESEFIN),0)
        endif
        .DoRTCalc(17,19,.t.)
        if .o_PACODESE<>.w_PACODESE
            .w_PANUMFRA = .w_NUMMESI
        endif
        if .o_PACODESE<>.w_PACODESE
            .w_PANUMMES = 12
        endif
        if .o_ESEINI<>.w_ESEINI
            .w_PADATINI = .w_Eseini
        endif
        if .o_ESEFIN<>.w_ESEFIN
            .w_PADATFIN = .w_ESEFIN
        endif
        .DoRTCalc(24,26,.t.)
        if .o_PACODCAT<>.w_PACODCAT
            .w_PACOECIV = IIF(NOT EMPTY(.w_PACODCAT),.w_PACOECIV,0)
        endif
        if .o_PACODCAT<>.w_PACODCAT
            .w_PACOEFI1 = IIF(NOT EMPTY(.w_PACODCAT),.w_PACOEFI1,0)
        endif
        if .o_PEARANT<>.w_PEARANT.or. .o_PACOEFI1<>.w_PACOEFI1
            .w_PERCANT = cp_ROUND(.w_PACOEFI1*(.w_PEARANT/100),2)
        endif
        if .o_PERCANT<>.w_PERCANT.or. .o_PACODCAT<>.w_PACODCAT
            .w_PACOEFI2 = IIF(.w_TIPBEN='M' AND NOT EMPTY(.w_PACODCAT),.w_PERCANT,0)
        endif
        .oPgFrm.Page1.oPag.oObj_1_37.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_40.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
        if .o_PANUMREG<>.w_PANUMREG
            .w_OFLDEFI = .w_PAFLDEFI
        endif
        if .o_TOTDEC<>.w_TOTDEC
            .w_CALCPIC = DEFPIC(.w_TOTDEC)
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_codazi<>.w_codazi .or. .op_PACODESE<>.w_PACODESE
           cp_AskTableProg(this,i_nConn,"PREPA","i_codazi,w_PACODESE,w_PANUMREG")
          .op_PANUMREG = .w_PANUMREG
        endif
        .op_codazi = .w_codazi
        .op_PACODESE = .w_PACODESE
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_37.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_40.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oPAFLDEFI_1_15.enabled = this.oPgFrm.Page1.oPag.oPAFLDEFI_1_15.mCond()
    this.oPgFrm.Page1.oPag.oPATIPBEN_1_17.enabled = this.oPgFrm.Page1.oPag.oPATIPBEN_1_17.mCond()
    this.oPgFrm.Page1.oPag.oPATIPGEN_1_19.enabled = this.oPgFrm.Page1.oPag.oPATIPGEN_1_19.mCond()
    this.oPgFrm.Page1.oPag.oPACOECIV_1_27.enabled = this.oPgFrm.Page1.oPag.oPACOECIV_1_27.mCond()
    this.oPgFrm.Page1.oPag.oPACOEFI1_1_28.enabled = this.oPgFrm.Page1.oPag.oPACOEFI1_1_28.mCond()
    this.oPgFrm.Page1.oPag.oPACOEFI2_1_30.enabled = this.oPgFrm.Page1.oPag.oPACOEFI2_1_30.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_46.enabled = this.oPgFrm.Page1.oPag.oBtn_1_46.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_59.enabled = this.oPgFrm.Page1.oPag.oBtn_1_59.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oPANUMFRA_1_20.visible=!this.oPgFrm.Page1.oPag.oPANUMFRA_1_20.mHide()
    this.oPgFrm.Page1.oPag.oPANUMMES_1_21.visible=!this.oPgFrm.Page1.oPag.oPANUMMES_1_21.mHide()
    this.oPgFrm.Page1.oPag.oPADATINI_1_22.visible=!this.oPgFrm.Page1.oPag.oPADATINI_1_22.mHide()
    this.oPgFrm.Page1.oPag.oPADATFIN_1_23.visible=!this.oPgFrm.Page1.oPag.oPADATFIN_1_23.mHide()
    this.oPgFrm.Page1.oPag.oPACOECIV_1_27.visible=!this.oPgFrm.Page1.oPag.oPACOECIV_1_27.mHide()
    this.oPgFrm.Page1.oPag.oPACOEFI1_1_28.visible=!this.oPgFrm.Page1.oPag.oPACOEFI1_1_28.mHide()
    this.oPgFrm.Page1.oPag.oPACOEFI2_1_30.visible=!this.oPgFrm.Page1.oPag.oPACOEFI2_1_30.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_34.visible=!this.oPgFrm.Page1.oPag.oStr_1_34.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_48.visible=!this.oPgFrm.Page1.oPag.oStr_1_48.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_49.visible=!this.oPgFrm.Page1.oPag.oStr_1_49.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_51.visible=!this.oPgFrm.Page1.oPag.oStr_1_51.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_52.visible=!this.oPgFrm.Page1.oPag.oStr_1_52.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_53.visible=!this.oPgFrm.Page1.oPag.oStr_1_53.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_55.visible=!this.oPgFrm.Page1.oPag.oStr_1_55.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_57.visible=!this.oPgFrm.Page1.oPag.oStr_1_57.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_58.visible=!this.oPgFrm.Page1.oPag.oStr_1_58.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_37.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_39.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_40.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_41.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_CESP_IDX,3]
    i_lTable = "PAR_CESP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_CESP_IDX,2], .t., this.PAR_CESP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_CESP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PCCODAZI,PCPERANT,PCPERESE,PCTIPAMM";
                   +" from "+i_cTable+" "+i_lTable+" where PCCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PCCODAZI',this.w_CODAZI)
            select PCCODAZI,PCPERANT,PCPERESE,PCTIPAMM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.PCCODAZI,space(5))
      this.w_PEARANT = NVL(_Link_.PCPERANT,0)
      this.w_PERRID = NVL(_Link_.PCPERESE,0)
      this.w_PCTIPAMM = NVL(_Link_.PCTIPAMM,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_PEARANT = 0
      this.w_PERRID = 0
      this.w_PCTIPAMM = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_CESP_IDX,2])+'\'+cp_ToStr(_Link_.PCCODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_CESP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PACODESE
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PACODESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_PACODESE)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ,ESFINESE,ESINIESE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_CODAZI;
                     ,'ESCODESE',trim(this.w_PACODESE))
          select ESCODAZI,ESCODESE,ESVALNAZ,ESFINESE,ESINIESE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PACODESE)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PACODESE) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oPACODESE_1_8'),i_cWhere,'',"Esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ,ESFINESE,ESINIESE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESVALNAZ,ESFINESE,ESINIESE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ,ESFINESE,ESINIESE";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESVALNAZ,ESFINESE,ESINIESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PACODESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ,ESFINESE,ESINIESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_PACODESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_PACODESE)
            select ESCODAZI,ESCODESE,ESVALNAZ,ESFINESE,ESINIESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PACODESE = NVL(_Link_.ESCODESE,space(4))
      this.w_VALNAZ = NVL(_Link_.ESVALNAZ,space(3))
      this.w_ESEFIN = NVL(cp_ToDate(_Link_.ESFINESE),ctod("  /  /  "))
      this.w_ESEINI = NVL(cp_ToDate(_Link_.ESINIESE),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_PACODESE = space(4)
      endif
      this.w_VALNAZ = space(3)
      this.w_ESEFIN = ctod("  /  /  ")
      this.w_ESEINI = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PACODESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VALNAZ
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALNAZ) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALNAZ)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALNAZ);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALNAZ)
            select VACODVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALNAZ = NVL(_Link_.VACODVAL,space(3))
      this.w_TOTDEC = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_VALNAZ = space(3)
      endif
      this.w_TOTDEC = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALNAZ Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PACODCAT
  func Link_1_24(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_CESP_IDX,3]
    i_lTable = "CAT_CESP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_CESP_IDX,2], .t., this.CAT_CESP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_CESP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PACODCAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCE_ACT',True,'CAT_CESP')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_PACODCAT)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPBEN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_PACODCAT))
          select CCCODICE,CCDESCRI,CCTIPBEN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PACODCAT)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PACODCAT) and !this.bDontReportError
            deferred_cp_zoom('CAT_CESP','*','CCCODICE',cp_AbsName(oSource.parent,'oPACODCAT_1_24'),i_cWhere,'GSCE_ACT',"Categorie cespiti",'GSCE_APA.CAT_CESP_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPBEN";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCTIPBEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PACODCAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPBEN";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_PACODCAT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_PACODCAT)
            select CCCODICE,CCDESCRI,CCTIPBEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PACODCAT = NVL(_Link_.CCCODICE,space(15))
      this.w_DESCAT = NVL(_Link_.CCDESCRI,space(40))
      this.w_TIPBEN = NVL(_Link_.CCTIPBEN,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PACODCAT = space(15)
      endif
      this.w_DESCAT = space(40)
      this.w_TIPBEN = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPBEN=.w_PATIPBEN OR .w_PATIPBEN='T'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PACODCAT = space(15)
        this.w_DESCAT = space(40)
        this.w_TIPBEN = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_CESP_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_CESP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PACODCAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_24(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAT_CESP_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAT_CESP_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_24.CCCODICE as CCCODICE124"+ ",link_1_24.CCDESCRI as CCDESCRI124"+ ",link_1_24.CCTIPBEN as CCTIPBEN124"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_24 on CES_PIAN.PACODCAT=link_1_24.CCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_24"
          i_cKey=i_cKey+'+" and CES_PIAN.PACODCAT=link_1_24.CCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oPANUMREG_1_6.value==this.w_PANUMREG)
      this.oPgFrm.Page1.oPag.oPANUMREG_1_6.value=this.w_PANUMREG
    endif
    if not(this.oPgFrm.Page1.oPag.oPADATREG_1_7.value==this.w_PADATREG)
      this.oPgFrm.Page1.oPag.oPADATREG_1_7.value=this.w_PADATREG
    endif
    if not(this.oPgFrm.Page1.oPag.oPACODESE_1_8.value==this.w_PACODESE)
      this.oPgFrm.Page1.oPag.oPACODESE_1_8.value=this.w_PACODESE
    endif
    if not(this.oPgFrm.Page1.oPag.oPADESCRI_1_14.value==this.w_PADESCRI)
      this.oPgFrm.Page1.oPag.oPADESCRI_1_14.value=this.w_PADESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oPAFLDEFI_1_15.RadioValue()==this.w_PAFLDEFI)
      this.oPgFrm.Page1.oPag.oPAFLDEFI_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPATIPBEN_1_17.RadioValue()==this.w_PATIPBEN)
      this.oPgFrm.Page1.oPag.oPATIPBEN_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPA_STATO_1_18.RadioValue()==this.w_PA_STATO)
      this.oPgFrm.Page1.oPag.oPA_STATO_1_18.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPATIPGEN_1_19.RadioValue()==this.w_PATIPGEN)
      this.oPgFrm.Page1.oPag.oPATIPGEN_1_19.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPANUMFRA_1_20.value==this.w_PANUMFRA)
      this.oPgFrm.Page1.oPag.oPANUMFRA_1_20.value=this.w_PANUMFRA
    endif
    if not(this.oPgFrm.Page1.oPag.oPANUMMES_1_21.value==this.w_PANUMMES)
      this.oPgFrm.Page1.oPag.oPANUMMES_1_21.value=this.w_PANUMMES
    endif
    if not(this.oPgFrm.Page1.oPag.oPADATINI_1_22.value==this.w_PADATINI)
      this.oPgFrm.Page1.oPag.oPADATINI_1_22.value=this.w_PADATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oPADATFIN_1_23.value==this.w_PADATFIN)
      this.oPgFrm.Page1.oPag.oPADATFIN_1_23.value=this.w_PADATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oPACODCAT_1_24.value==this.w_PACODCAT)
      this.oPgFrm.Page1.oPag.oPACODCAT_1_24.value=this.w_PACODCAT
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAT_1_25.value==this.w_DESCAT)
      this.oPgFrm.Page1.oPag.oDESCAT_1_25.value=this.w_DESCAT
    endif
    if not(this.oPgFrm.Page1.oPag.oPACOECIV_1_27.value==this.w_PACOECIV)
      this.oPgFrm.Page1.oPag.oPACOECIV_1_27.value=this.w_PACOECIV
    endif
    if not(this.oPgFrm.Page1.oPag.oPACOEFI1_1_28.value==this.w_PACOEFI1)
      this.oPgFrm.Page1.oPag.oPACOEFI1_1_28.value=this.w_PACOEFI1
    endif
    if not(this.oPgFrm.Page1.oPag.oPACOEFI2_1_30.value==this.w_PACOEFI2)
      this.oPgFrm.Page1.oPag.oPACOEFI2_1_30.value=this.w_PACOEFI2
    endif
    cp_SetControlsValueExtFlds(this,'CES_PIAN')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_PADATREG))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPADATREG_1_7.SetFocus()
            i_bnoObbl = !empty(.w_PADATREG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_PACODESE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPACODESE_1_8.SetFocus()
            i_bnoObbl = !empty(.w_PACODESE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_PA_STATO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPA_STATO_1_18.SetFocus()
            i_bnoObbl = !empty(.w_PA_STATO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_PANUMFRA)) or not(.w_PANUMFRA<=.w_NUMMESI))  and not(.w_PATIPGEN='X')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPANUMFRA_1_20.SetFocus()
            i_bnoObbl = !empty(.w_PANUMFRA)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((.w_PANUMMES<=.w_NUMMESI OR .w_PANUMMES<=12) AND .w_PANUMMES<>0)  and not(.w_PATIPGEN='X')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPANUMMES_1_21.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_Padatini<=.w_Padatfin)  and not(.w_PATIPGEN='D')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPADATINI_1_22.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data di inizio competenza maggiore della data di fine")
          case   not(.w_Padatini<=.w_Padatfin)  and not(.w_PATIPGEN='D')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPADATFIN_1_23.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data di inizio competenza maggiore della data di fine")
          case   not(.w_TIPBEN=.w_PATIPBEN OR .w_PATIPBEN='T')  and not(empty(.w_PACODCAT))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPACODCAT_1_24.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PEARANT = this.w_PEARANT
    this.o_PANUMREG = this.w_PANUMREG
    this.o_PACODESE = this.w_PACODESE
    this.o_ESEFIN = this.w_ESEFIN
    this.o_ESEINI = this.w_ESEINI
    this.o_TOTDEC = this.w_TOTDEC
    this.o_PACODCAT = this.w_PACODCAT
    this.o_PACOEFI1 = this.w_PACOEFI1
    this.o_PERCANT = this.w_PERCANT
    return

enddefine

* --- Define pages as container
define class tgsce_apaPag1 as StdContainer
  Width  = 556
  height = 321
  stdWidth  = 556
  stdheight = 321
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPANUMREG_1_6 as StdField with uid="TYLSUSBMNT",rtseq=6,rtrep=.f.,;
    cFormVar = "w_PANUMREG", cQueryName = "PANUMREG",;
    bObbl = .f. , nPag = 1, value=space(6), bMultilanguage =  .f.,;
    ToolTipText = "Numero progressivo del piano di ammortamento",;
    HelpContextID = 228581693,;
   bGlobalFont=.t.,;
    Height=21, Width=77, Left=127, Top=13, cSayPict="'999999'", cGetPict="'999999'", InputMask=replicate('X',6)

  add object oPADATREG_1_7 as StdField with uid="ABUUXQBTLG",rtseq=7,rtrep=.f.,;
    cFormVar = "w_PADATREG", cQueryName = "PADATREG",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di elaborazione del piano di ammortamento",;
    HelpContextID = 234570045,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=285, Top=13

  add object oPACODESE_1_8 as StdField with uid="UAVPERMVRN",rtseq=8,rtrep=.f.,;
    cFormVar = "w_PACODESE", cQueryName = "PANUMREG,PACODESE",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Codice esercizio di riferimento",;
    HelpContextID = 602427,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=503, Top=13, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_PACODESE"

  func oPACODESE_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oPACODESE_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPACODESE_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CODAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_CODAZI)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oPACODESE_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Esercizi",'',this.parent.oContained
  endproc

  add object oPADESCRI_1_14 as StdField with uid="ZCNCBSXBZZ",rtseq=14,rtrep=.f.,;
    cFormVar = "w_PADESCRI", cQueryName = "PADESCRI",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Note descrittive del piano di ammortamento",;
    HelpContextID = 17874625,;
   bGlobalFont=.t.,;
    Height=21, Width=331, Left=127, Top=44, InputMask=replicate('X',40)


  add object oPAFLDEFI_1_15 as StdCombo with uid="MPRGWMEWLC",rtseq=15,rtrep=.f.,left=127,top=75,width=106,height=21;
    , ToolTipText = "Se definitivo: il piano di ammortamento ha generato i movimenti di accantonament";
    , HelpContextID = 418111;
    , cFormVar="w_PAFLDEFI",RowSource=""+"Provvisorio,"+"Definitivo", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPAFLDEFI_1_15.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oPAFLDEFI_1_15.GetRadio()
    this.Parent.oContained.w_PAFLDEFI = this.RadioValue()
    return .t.
  endfunc

  func oPAFLDEFI_1_15.SetRadio()
    this.Parent.oContained.w_PAFLDEFI=trim(this.Parent.oContained.w_PAFLDEFI)
    this.value = ;
      iif(this.Parent.oContained.w_PAFLDEFI=='P',1,;
      iif(this.Parent.oContained.w_PAFLDEFI=='D',2,;
      0))
  endfunc

  func oPAFLDEFI_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PAFLDEFI='D')
    endwith
   endif
  endfunc


  add object oPATIPBEN_1_17 as StdCombo with uid="YEYIKYOLVM",rtseq=17,rtrep=.f.,left=330,top=75,width=106,height=21;
    , ToolTipText = "Tipo bene di selezione (materiale, immateriale o tutti)";
    , HelpContextID = 230965572;
    , cFormVar="w_PATIPBEN",RowSource=""+"Tutti,"+"Materiali,"+"Immateriali", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPATIPBEN_1_17.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'M',;
    iif(this.value =3,'I',;
    space(1)))))
  endfunc
  func oPATIPBEN_1_17.GetRadio()
    this.Parent.oContained.w_PATIPBEN = this.RadioValue()
    return .t.
  endfunc

  func oPATIPBEN_1_17.SetRadio()
    this.Parent.oContained.w_PATIPBEN=trim(this.Parent.oContained.w_PATIPBEN)
    this.value = ;
      iif(this.Parent.oContained.w_PATIPBEN=='T',1,;
      iif(this.Parent.oContained.w_PATIPBEN=='M',2,;
      iif(this.Parent.oContained.w_PATIPBEN=='I',3,;
      0)))
  endfunc

  func oPATIPBEN_1_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_PACODCAT))
    endwith
   endif
  endfunc


  add object oPA_STATO_1_18 as StdCombo with uid="BIRXBBZZVM",rtseq=18,rtrep=.f.,left=127,top=109,width=129,height=21;
    , ToolTipText = "Status dei beni di selezione";
    , HelpContextID = 219083077;
    , cFormVar="w_PA_STATO",RowSource=""+"Effettivi,"+"Previsionali", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oPA_STATO_1_18.RadioValue()
    return(iif(this.value =1,'E',;
    iif(this.value =2,'P',;
    space(1))))
  endfunc
  func oPA_STATO_1_18.GetRadio()
    this.Parent.oContained.w_PA_STATO = this.RadioValue()
    return .t.
  endfunc

  func oPA_STATO_1_18.SetRadio()
    this.Parent.oContained.w_PA_STATO=trim(this.Parent.oContained.w_PA_STATO)
    this.value = ;
      iif(this.Parent.oContained.w_PA_STATO=='E',1,;
      iif(this.Parent.oContained.w_PA_STATO=='P',2,;
      0))
  endfunc


  add object oPATIPGEN_1_19 as StdCombo with uid="TYFSTDMIGU",rtseq=19,rtrep=.f.,left=127,top=143,width=83,height=21;
    , ToolTipText = "Ammortamento in dodicesimi oppure dietimi";
    , HelpContextID = 46416196;
    , cFormVar="w_PATIPGEN",RowSource=""+"Dodicesimi,"+"Dietimi", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPATIPGEN_1_19.RadioValue()
    return(iif(this.value =1,'D',;
    iif(this.value =2,'X',;
    space(1))))
  endfunc
  func oPATIPGEN_1_19.GetRadio()
    this.Parent.oContained.w_PATIPGEN = this.RadioValue()
    return .t.
  endfunc

  func oPATIPGEN_1_19.SetRadio()
    this.Parent.oContained.w_PATIPGEN=trim(this.Parent.oContained.w_PATIPGEN)
    this.value = ;
      iif(this.Parent.oContained.w_PATIPGEN=='D',1,;
      iif(this.Parent.oContained.w_PATIPGEN=='X',2,;
      0))
  endfunc

  func oPATIPGEN_1_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  add object oPANUMFRA_1_20 as StdField with uid="BFDKLJQQRC",rtseq=20,rtrep=.f.,;
    cFormVar = "w_PANUMFRA", cQueryName = "PANUMFRA",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Frazione su cui ripartire i calcoli in base al numero di mesi dell'esercizio",;
    HelpContextID = 241180361,;
   bGlobalFont=.t.,;
    Height=21, Width=26, Left=330, Top=142, cSayPict='"99"', cGetPict='"99"'

  func oPANUMFRA_1_20.mHide()
    with this.Parent.oContained
      return (.w_PATIPGEN='X')
    endwith
  endfunc

  func oPANUMFRA_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PANUMFRA<=.w_NUMMESI)
    endwith
    return bRes
  endfunc

  add object oPANUMMES_1_21 as StdField with uid="BKLSAABPIL",rtseq=21,rtrep=.f.,;
    cFormVar = "w_PANUMMES", cQueryName = "PANUMMES",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero di mesi dell'esercizio",;
    HelpContextID = 144695625,;
   bGlobalFont=.t.,;
    Height=21, Width=26, Left=372, Top=142

  func oPANUMMES_1_21.mHide()
    with this.Parent.oContained
      return (.w_PATIPGEN='X')
    endwith
  endfunc

  func oPANUMMES_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_PANUMMES<=.w_NUMMESI OR .w_PANUMMES<=12) AND .w_PANUMMES<>0)
    endwith
    return bRes
  endfunc

  add object oPADATINI_1_22 as StdField with uid="LPMWDFILLV",rtseq=22,rtrep=.f.,;
    cFormVar = "w_PADATINI", cQueryName = "PADATINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data di inizio competenza maggiore della data di fine",;
    ToolTipText = "Data di inizio competenza del piano di ammortamento da elaborare",;
    HelpContextID = 184860353,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=330, Top=142

  func oPADATINI_1_22.mHide()
    with this.Parent.oContained
      return (.w_PATIPGEN='D')
    endwith
  endfunc

  func oPADATINI_1_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_Padatini<=.w_Padatfin)
    endwith
    return bRes
  endfunc

  add object oPADATFIN_1_23 as StdField with uid="YYPCQYRVXX",rtseq=23,rtrep=.f.,;
    cFormVar = "w_PADATFIN", cQueryName = "PADATFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data di inizio competenza maggiore della data di fine",;
    ToolTipText = "Data di fine competenza del piano di ammortamento da elaborare",;
    HelpContextID = 33243460,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=469, Top=142

  func oPADATFIN_1_23.mHide()
    with this.Parent.oContained
      return (.w_PATIPGEN='D')
    endwith
  endfunc

  func oPADATFIN_1_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_Padatini<=.w_Padatfin)
    endwith
    return bRes
  endfunc

  add object oPACODCAT_1_24 as StdField with uid="JHZGMYTSTF",rtseq=24,rtrep=.f.,;
    cFormVar = "w_PACODCAT", cQueryName = "PACODCAT",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice categoria cespiti di selezione (se vuoto= no selezione)",;
    HelpContextID = 32951990,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=127, Top=180, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAT_CESP", cZoomOnZoom="GSCE_ACT", oKey_1_1="CCCODICE", oKey_1_2="this.w_PACODCAT"

  func oPACODCAT_1_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_24('Part',this)
    endwith
    return bRes
  endfunc

  proc oPACODCAT_1_24.ecpDrop(oSource)
    this.Parent.oContained.link_1_24('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPACODCAT_1_24.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_CESP','*','CCCODICE',cp_AbsName(this.parent,'oPACODCAT_1_24'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCE_ACT',"Categorie cespiti",'GSCE_APA.CAT_CESP_VZM',this.parent.oContained
  endproc
  proc oPACODCAT_1_24.mZoomOnZoom
    local i_obj
    i_obj=GSCE_ACT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_PACODCAT
     i_obj.ecpSave()
  endproc

  add object oDESCAT_1_25 as StdField with uid="SMZKUTHMVH",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DESCAT", cQueryName = "DESCAT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 20040650,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=263, Top=180, InputMask=replicate('X',40)

  add object oPACOECIV_1_27 as StdField with uid="DWXVJDGORB",rtseq=27,rtrep=.f.,;
    cFormVar = "w_PACOECIV", cQueryName = "PACOECIV",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Percentuale accantonamento civile",;
    HelpContextID = 236532044,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=80, Top=244, cSayPict='"999.99"', cGetPict='"999.99"'

  func oPACOECIV_1_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PCTIPAMM<>'F')
    endwith
   endif
  endfunc

  func oPACOECIV_1_27.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_PACODCAT))
    endwith
  endfunc

  add object oPACOEFI1_1_28 as StdField with uid="OLFCALDRKN",rtseq=28,rtrep=.f.,;
    cFormVar = "w_PACOEFI1", cQueryName = "PACOEFI1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Percentuale accantonamento ordinario",;
    HelpContextID = 18428199,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=309, Top=244, cSayPict='"999.99"', cGetPict='"999.99"'

  func oPACOEFI1_1_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PCTIPAMM<>'C')
    endwith
   endif
  endfunc

  func oPACOEFI1_1_28.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_PACODCAT))
    endwith
  endfunc

  add object oPACOEFI2_1_30 as StdField with uid="ORDNPQFZDY",rtseq=30,rtrep=.f.,;
    cFormVar = "w_PACOEFI2", cQueryName = "PACOEFI2",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Percentuale accantonamento anticipato",;
    HelpContextID = 18428200,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=383, Top=244, cSayPict='"999.99"', cGetPict='"999.99"'

  func oPACOEFI2_1_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PACOEFI1<>0 AND .w_PCTIPAMM<>'C' AND .w_TIPBEN='M')
    endwith
   endif
  endfunc

  func oPACOEFI2_1_30.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_PACODCAT))
    endwith
  endfunc


  add object oObj_1_37 as cp_runprogram with uid="OWTPGQCYHG",left=1, top=404, width=238,height=21,;
    caption='GSCE_BA1',;
   bGlobalFont=.t.,;
    prg="GSCE_BA1",;
    cEvent = "Insert start,Update start",;
    nPag=1;
    , HelpContextID = 22068585


  add object oObj_1_39 as cp_runprogram with uid="EBZMASPSJT",left=1, top=335, width=238,height=21,;
    caption='GSCE_BPA(I)',;
   bGlobalFont=.t.,;
    prg='GSCE_BPA("I")',;
    cEvent = "Record Inserted",;
    nPag=1;
    , HelpContextID = 21881305


  add object oObj_1_40 as cp_runprogram with uid="LIQHBDJUNV",left=1, top=380, width=238,height=21,;
    caption='GSCE_BPA(U)',;
   bGlobalFont=.t.,;
    prg='GSCE_BPA("U")',;
    cEvent = "Update start",;
    nPag=1;
    , HelpContextID = 21878233


  add object oObj_1_41 as cp_runprogram with uid="ADLTPAJMZJ",left=1, top=357, width=238,height=21,;
    caption='GSCE_BPA(D)',;
   bGlobalFont=.t.,;
    prg='GSCE_BPA("D")',;
    cEvent = "Delete start",;
    nPag=1;
    , HelpContextID = 21882585


  add object oBtn_1_44 as StdButton with uid="IRZVMAPDQB",left=456, top=276, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per elaborare il piano di ammortamento impostato";
    , HelpContextID = 160027930;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_44.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_45 as StdButton with uid="XQEEHFSMTT",left=506, top=276, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per abbandonare le modifiche";
    , HelpContextID = 152739258;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_45.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_46 as StdButton with uid="LAKPLEFRQV",left=7, top=276, width=48,height=45,;
    CpPicture="BMP\DETTAGLI.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare i dettagli del piano di ammortamento";
    , HelpContextID = 201235297;
    , caption='\<Dettagli';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_46.Click()
      do GSCE_KPA with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_46.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_PANUMREG) AND NOT EMPTY(.w_PACODESE) AND .w_TIPOPE<>'Load')
      endwith
    endif
  endfunc


  add object oBtn_1_59 as StdButton with uid="HNAAOYVXDA",left=59, top=276, width=48,height=45,;
    CpPicture="BMP\ELIMINA2.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eliminare i movimenti cespiti associati";
    , HelpContextID = 257290046;
    , tabstop=.f., Caption='\<Elimina';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_59.Click()
      with this.Parent.oContained
        GSCE_BPA(this.Parent.oContained,"E")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_59.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction='Query' and .w_PAFLDEFI='D')
      endwith
    endif
  endfunc

  add object oStr_1_31 as StdString with uid="NCDEAGORYY",Visible=.t., Left=24, Top=13,;
    Alignment=1, Width=97, Height=15,;
    Caption="Numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="AJEXATPKJK",Visible=.t., Left=220, Top=13,;
    Alignment=1, Width=61, Height=15,;
    Caption="Data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="UMVPARZHFV",Visible=.t., Left=376, Top=13,;
    Alignment=1, Width=122, Height=15,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="YFRDKHVPEF",Visible=.t., Left=249, Top=146,;
    Alignment=1, Width=76, Height=15,;
    Caption="Mesi:"  ;
  , bGlobalFont=.t.

  func oStr_1_34.mHide()
    with this.Parent.oContained
      return (.w_PATIPGEN='X')
    endwith
  endfunc

  add object oStr_1_35 as StdString with uid="XBXGKPVZCC",Visible=.t., Left=24, Top=109,;
    Alignment=1, Width=97, Height=15,;
    Caption="Beni:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="WVCTMOTNSF",Visible=.t., Left=24, Top=44,;
    Alignment=1, Width=97, Height=15,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="VKBTWLVEAO",Visible=.t., Left=24, Top=183,;
    Alignment=1, Width=97, Height=15,;
    Caption="Categoria:"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="VLXWATDUTX",Visible=.t., Left=24, Top=75,;
    Alignment=1, Width=97, Height=15,;
    Caption="Status:"  ;
  , bGlobalFont=.t.

  add object oStr_1_48 as StdString with uid="WJNWMIRDDM",Visible=.t., Left=7, Top=224,;
    Alignment=0, Width=87, Height=18,;
    Caption="Aspetto civile"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_48.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_PACODCAT))
    endwith
  endfunc

  add object oStr_1_49 as StdString with uid="IDWQKCVHKT",Visible=.t., Left=242, Top=224,;
    Alignment=0, Width=87, Height=18,;
    Caption="Aspetto fiscale"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_49.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_PACODCAT))
    endwith
  endfunc

  add object oStr_1_51 as StdString with uid="ZFSHCEUZAP",Visible=.t., Left=7, Top=246,;
    Alignment=1, Width=68, Height=18,;
    Caption="Coefficiente:"  ;
  , bGlobalFont=.t.

  func oStr_1_51.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_PACODCAT))
    endwith
  endfunc

  add object oStr_1_52 as StdString with uid="TSRDGWYGMR",Visible=.t., Left=233, Top=246,;
    Alignment=1, Width=74, Height=18,;
    Caption="Coefficienti:"  ;
  , bGlobalFont=.t.

  func oStr_1_52.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_PACODCAT))
    endwith
  endfunc

  add object oStr_1_53 as StdString with uid="WHFNGMLAAG",Visible=.t., Left=370, Top=248,;
    Alignment=0, Width=18, Height=18,;
    Caption="+"  ;
  , bGlobalFont=.t.

  func oStr_1_53.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_PACODCAT))
    endwith
  endfunc

  add object oStr_1_54 as StdString with uid="IWGREWELOZ",Visible=.t., Left=255, Top=75,;
    Alignment=1, Width=70, Height=18,;
    Caption="Tipo beni:"  ;
  , bGlobalFont=.t.

  add object oStr_1_55 as StdString with uid="WEPQMDDZQJ",Visible=.t., Left=360, Top=145,;
    Alignment=0, Width=9, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  func oStr_1_55.mHide()
    with this.Parent.oContained
      return (.w_PATIPGEN='X')
    endwith
  endfunc

  add object oStr_1_56 as StdString with uid="WCMJIHLWHP",Visible=.t., Left=2, Top=143,;
    Alignment=1, Width=119, Height=18,;
    Caption="Ammortamento in:"  ;
  , bGlobalFont=.t.

  add object oStr_1_57 as StdString with uid="ZNRVWQYVLA",Visible=.t., Left=268, Top=146,;
    Alignment=1, Width=57, Height=15,;
    Caption="da data:"  ;
  , bGlobalFont=.t.

  func oStr_1_57.mHide()
    with this.Parent.oContained
      return (.w_PATIPGEN='D')
    endwith
  endfunc

  add object oStr_1_58 as StdString with uid="TGCUJPJKSG",Visible=.t., Left=419, Top=146,;
    Alignment=1, Width=45, Height=15,;
    Caption="a data:"  ;
  , bGlobalFont=.t.

  func oStr_1_58.mHide()
    with this.Parent.oContained
      return (.w_PATIPGEN='D')
    endwith
  endfunc

  add object oBox_1_50 as StdBox with uid="GAKCIORZWX",left=-1, top=238, width=556,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsce_apa','CES_PIAN','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".PANUMREG=CES_PIAN.PANUMREG";
  +" and "+i_cAliasName2+".PACODESE=CES_PIAN.PACODESE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
