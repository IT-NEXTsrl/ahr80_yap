* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_bc3                                                        *
*              Conferma mov.cespite contabilizzato                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_7]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-12-12                                                      *
* Last revis.: 2000-12-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsce_bc3",oParentObject)
return(i_retval)

define class tgsce_bc3 as StdBatch
  * --- Local variables
  * --- WorkFile variables
  MOV_CESP_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questa Funzione Consente la Conferma di Un movimento Cespite Contabilizzato (da GSCE_AMC)
    if this.oParentObject.w_TIPOPE="Query" AND NOT EMPTY(this.oParentObject.w_MCSERIAL) AND this.oParentObject.w_MCSTATUS="P" AND NOT EMPTY(this.oParentObject.w_PNSERIALC)
      this.oParentObject.w_MCSTATUS = "C"
      * --- Write into MOV_CESP
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.MOV_CESP_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MOV_CESP_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.MOV_CESP_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MCSTATUS ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MCSTATUS),'MOV_CESP','MCSTATUS');
            +i_ccchkf ;
        +" where ";
            +"MCSERIAL = "+cp_ToStrODBC(this.oParentObject.w_MCSERIAL);
               )
      else
        update (i_cTable) set;
            MCSTATUS = this.oParentObject.w_MCSTATUS;
            &i_ccchkf. ;
         where;
            MCSERIAL = this.oParentObject.w_MCSERIAL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='MOV_CESP'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
