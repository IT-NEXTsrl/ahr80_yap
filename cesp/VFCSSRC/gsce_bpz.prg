* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_bpz                                                        *
*              Esegue zoom piano ammortamento                                  *
*                                                                              *
*      Author: Zucchetti spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][207]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-03-19                                                      *
* Last revis.: 2000-04-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParam
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsce_bpz",oParentObject,m.pParam)
return(i_retval)

define class tgsce_bpz as StdBatch
  * --- Local variables
  pParam = space(1)
  w_PROG = .NULL.
  w_PROG = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Esegue lo Zoom in Visualizzazione Piano di Ammortamento (da GSCE_KPA)
    * --- Z =Esegue lo Zoom; D=Dettaglio Piano di Ammortamento
    do case
      case this.pParam="Z"
        * --- Lancia lo Zoom
        this.oParentObject.NotifyEvent("CalcZoom")
      case this.pParam="D"
        * --- Lancia dettaglio Piano Ammortamento
        this.w_PROG = GSCE_AAC()
        * --- Controllo se passa il Test di Accesso
        if NOT (this.w_PROG.bSec1)
          i_retcode = 'stop'
          return
        endif
        * --- Inizializza la Chiave
        this.w_PROG.w_ACNUMREG = this.oParentObject.w_NUMREG
        this.w_PROG.w_ACCODESE = this.oParentObject.w_CODESE
        this.w_PROG.w_ACCODCES = this.oParentObject.w_CODCES
        * --- Creazione Cursore delle Chiavi
        this.w_PROG.QueryKeySet("ACNUMREG='"+this.oParentObject.w_NUMREG+"' AND ACCODESE='"+this.oParentObject.w_CODESE +"' AND ACCODCES='"+this.oParentObject.w_CODCES +"'","")     
        * --- Carica il Record
        this.w_PROG.LoadRecWarn()     
      case this.pParam="G"
        * --- Lancia dettaglio Piano Ammortamento Gestionale
        this.w_PROG = GSCE_AAG()
        * --- Controllo se passa il Test di Accesso
        if NOT (this.w_PROG.bSec1)
          i_retcode = 'stop'
          return
        endif
        * --- Inizializza la Chiave
        this.w_PROG.w_AGNUMREG = this.oParentObject.w_NUMREG
        this.w_PROG.w_AGCODESE = this.oParentObject.w_CODESE
        this.w_PROG.w_AGCODCES = this.oParentObject.w_CODCES
        * --- Creazione Cursore delle Chiavi
        this.w_PROG.QueryKeySet("AGNUMREG='"+this.oParentObject.w_NUMREG+"' AND AGCODESE='"+this.oParentObject.w_CODESE +"' AND AGCODCES='"+this.oParentObject.w_CODCES +"'","")     
        * --- Carica il Record
        this.w_PROG.LoadRecWarn()     
    endcase
  endproc


  proc Init(oParentObject,pParam)
    this.pParam=pParam
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParam"
endproc
