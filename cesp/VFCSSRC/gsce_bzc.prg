* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_bzc                                                        *
*              Moviment. schede cespiti                                        *
*                                                                              *
*      Author: Zucchetti spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][207]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-03-30                                                      *
* Last revis.: 2011-12-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pSERIAL
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsce_bzc",oParentObject,m.pSERIAL)
return(i_retval)

define class tgsce_bzc as StdBatch
  * --- Local variables
  pSERIAL = space(10)
  w_CARICA = space(15)
  w_PADRE = .NULL.
  w_CODCAT = space(15)
  w_FLPRIC = space(1)
  w_FLPRIU = space(1)
  w_PROG = .NULL.
  * --- WorkFile variables
  DOC_MAST_idx=0
  CAU_CESP_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- lancia Movimenti Cespiti da Visualizzazione Schede Cespiti (GSCE_SZC)
    * --- Oggetto definito come Movimenti Cespiti
    this.w_PADRE = THIS.OPARENTOBJECT
    if UPPER(THIS.OPARENTOBJECT.CLASS)="TGSCE_ACE"
      if .cFunction = "Edit" AND (this.oParentObject.w_FLPRIU="S" OR this.oParentObject.w_FLPRIC="S" OR this.oParentObject.w_FLDADI="S")
        i_retcode = 'stop'
        return
      endif
    endif
    this.w_PROG = GSCE_AMC()
    * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
    if !(this.w_PROG.bSec1)
      i_retcode = 'stop'
      return
    endif
    this.w_PROG.w_MCSERIAL = this.pSERIAL
    this.w_PROG.QueryKeySet("MCSERIAL='"+this.pSERIAL+ "'","")     
    * --- carico il record
    this.w_PROG.LoadRecWarn()     
  endproc


  proc Init(oParentObject,pSERIAL)
    this.pSERIAL=pSERIAL
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='DOC_MAST'
    this.cWorkTables[2]='CAU_CESP'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pSERIAL"
endproc
