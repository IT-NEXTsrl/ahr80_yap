* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_mca                                                        *
*              Categoria/ammortamenti civili                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-02-26                                                      *
* Last revis.: 2012-09-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsce_mca")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsce_mca")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsce_mca")
  return

* --- Class definition
define class tgsce_mca as StdPCForm
  Width  = 298
  Height = 133
  Top    = 78
  Left   = 158
  cComment = "Categoria/ammortamenti civili"
  cPrg = "gsce_mca"
  HelpContextID=171293335
  add object cnt as tcgsce_mca
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsce_mca as PCContext
  w_ECCODCAT = space(15)
  w_ECPROESE = 0
  w_ECTIPAMM = space(1)
  w_MAXPEORD = 0
  w_COECIV = 0
  w_ECPERORD = 0
  w_TOTPEORD = 0
  w_ECPERORD_T = 0
  proc Save(i_oFrom)
    this.w_ECCODCAT = i_oFrom.w_ECCODCAT
    this.w_ECPROESE = i_oFrom.w_ECPROESE
    this.w_ECTIPAMM = i_oFrom.w_ECTIPAMM
    this.w_MAXPEORD = i_oFrom.w_MAXPEORD
    this.w_COECIV = i_oFrom.w_COECIV
    this.w_ECPERORD = i_oFrom.w_ECPERORD
    this.w_TOTPEORD = i_oFrom.w_TOTPEORD
    this.w_ECPERORD_T = i_oFrom.w_ECPERORD_T
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_ECCODCAT = this.w_ECCODCAT
    i_oTo.w_ECPROESE = this.w_ECPROESE
    i_oTo.w_ECTIPAMM = this.w_ECTIPAMM
    i_oTo.w_MAXPEORD = this.w_MAXPEORD
    i_oTo.w_COECIV = this.w_COECIV
    i_oTo.w_ECPERORD = this.w_ECPERORD
    i_oTo.w_TOTPEORD = this.w_TOTPEORD
    i_oTo.w_ECPERORD_T = this.w_ECPERORD_T
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsce_mca as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 298
  Height = 133
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-09-13"
  HelpContextID=171293335
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=8

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  CAT_AMMC_IDX = 0
  cFile = "CAT_AMMC"
  cKeySelect = "ECCODCAT"
  cKeyWhere  = "ECCODCAT=this.w_ECCODCAT"
  cKeyDetail  = "ECCODCAT=this.w_ECCODCAT and ECPROESE=this.w_ECPROESE"
  cKeyWhereODBC = '"ECCODCAT="+cp_ToStrODBC(this.w_ECCODCAT)';

  cKeyDetailWhereODBC = '"ECCODCAT="+cp_ToStrODBC(this.w_ECCODCAT)';
      +'+" and ECPROESE="+cp_ToStrODBC(this.w_ECPROESE)';

  cKeyWhereODBCqualified = '"CAT_AMMC.ECCODCAT="+cp_ToStrODBC(this.w_ECCODCAT)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cPrg = "gsce_mca"
  cComment = "Categoria/ammortamenti civili"
  i_nRowNum = 0
  i_nRowPerPage = 4
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_ECCODCAT = space(15)
  w_ECPROESE = 0
  o_ECPROESE = 0
  w_ECTIPAMM = space(1)
  o_ECTIPAMM = space(1)
  w_MAXPEORD = 0
  o_MAXPEORD = 0
  w_COECIV = 0
  w_ECPERORD = 0
  o_ECPERORD = 0
  w_TOTPEORD = 0
  w_ECPERORD_T = 0
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsce_mcaPag1","gsce_mca",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='CAT_AMMC'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CAT_AMMC_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CAT_AMMC_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsce_mca'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from CAT_AMMC where ECCODCAT=KeySet.ECCODCAT
    *                            and ECPROESE=KeySet.ECPROESE
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- gsce_mca
      * --- Setta Ordine per Progressivo Periodo
      i_cOrder = 'order by ECPROESE '
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.CAT_AMMC_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_AMMC_IDX,2],this.bLoadRecFilter,this.CAT_AMMC_IDX,"gsce_mca")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CAT_AMMC')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CAT_AMMC.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' CAT_AMMC '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'ECCODCAT',this.w_ECCODCAT  )
      select * from (i_cTable) CAT_AMMC where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_TOTPEORD = 0
        .w_ECCODCAT = NVL(ECCODCAT,space(15))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'CAT_AMMC')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      this.w_TOTPEORD = 0
      scan
        with this
          .w_ECPROESE = NVL(ECPROESE,0)
          .w_ECTIPAMM = NVL(ECTIPAMM,space(1))
        .w_MAXPEORD = cp_ROUND(this.oParentObject.w_CCCOECIV*iif(.w_ECPROESE>0 AND .w_ECTIPAMM='5', 50/100, 1),2)
        .w_COECIV = this.oParentObject.w_CCCOECIV
          .w_ECPERORD = NVL(ECPERORD,0)
        .w_ECPERORD_T = .w_ECPERORD
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          .w_TOTPEORD = .w_TOTPEORD+.w_ECPERORD_T
          replace ECPROESE with .w_ECPROESE
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_ECCODCAT=space(15)
      .w_ECPROESE=0
      .w_ECTIPAMM=space(1)
      .w_MAXPEORD=0
      .w_COECIV=0
      .w_ECPERORD=0
      .w_TOTPEORD=0
      .w_ECPERORD_T=0
      if .cFunction<>"Filter"
        .DoRTCalc(1,2,.f.)
        .w_ECTIPAMM = 'I'
        .w_MAXPEORD = cp_ROUND(this.oParentObject.w_CCCOECIV*iif(.w_ECPROESE>0 AND .w_ECTIPAMM='5', 50/100, 1),2)
        .w_COECIV = this.oParentObject.w_CCCOECIV
        .w_ECPERORD = INT(.w_MAXPEORD * iif(.w_ECPROESE>0,1,0)*100)/100
        .DoRTCalc(7,7,.f.)
        .w_ECPERORD_T = .w_ECPERORD
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'CAT_AMMC')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'CAT_AMMC',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CAT_AMMC_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ECCODCAT,"ECCODCAT",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_ECPROESE N(3);
      ,t_ECTIPAMM N(3);
      ,t_ECPERORD N(6,2);
      ,ECPROESE N(3);
      ,t_MAXPEORD N(6,2);
      ,t_COECIV N(6,2);
      ,t_ECPERORD_T N(6,2);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsce_mcabodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oECPROESE_2_1.controlsource=this.cTrsName+'.t_ECPROESE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oECTIPAMM_2_2.controlsource=this.cTrsName+'.t_ECTIPAMM'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oECPERORD_2_5.controlsource=this.cTrsName+'.t_ECPERORD'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(57)
    this.AddVLine(188)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oECPROESE_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.CAT_AMMC_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_AMMC_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CAT_AMMC_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_AMMC_IDX,2])
      *
      * insert into CAT_AMMC
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CAT_AMMC')
        i_extval=cp_InsertValODBCExtFlds(this,'CAT_AMMC')
        i_cFldBody=" "+;
                  "(ECCODCAT,ECPROESE,ECTIPAMM,ECPERORD,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_ECCODCAT)+","+cp_ToStrODBC(this.w_ECPROESE)+","+cp_ToStrODBC(this.w_ECTIPAMM)+","+cp_ToStrODBC(this.w_ECPERORD)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CAT_AMMC')
        i_extval=cp_InsertValVFPExtFlds(this,'CAT_AMMC')
        cp_CheckDeletedKey(i_cTable,0,'ECCODCAT',this.w_ECCODCAT,'ECPROESE',this.w_ECPROESE)
        INSERT INTO (i_cTable) (;
                   ECCODCAT;
                  ,ECPROESE;
                  ,ECTIPAMM;
                  ,ECPERORD;
                  ,CPCCCHK &i_extfld.) VALUES (;
                  this.w_ECCODCAT;
                  ,this.w_ECPROESE;
                  ,this.w_ECTIPAMM;
                  ,this.w_ECPERORD;
                  ,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.CAT_AMMC_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_AMMC_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (t_ECPROESE<>0  AND  t_ECPERORD<>0) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'CAT_AMMC')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and ECPROESE="+cp_ToStrODBC(&i_TN.->ECPROESE)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'CAT_AMMC')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and ECPROESE=&i_TN.->ECPROESE;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (t_ECPROESE<>0  AND  t_ECPERORD<>0) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and ECPROESE="+cp_ToStrODBC(&i_TN.->ECPROESE)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and ECPROESE=&i_TN.->ECPROESE;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = 0
              replace ECPROESE with this.w_ECPROESE
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update CAT_AMMC
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'CAT_AMMC')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " ECTIPAMM="+cp_ToStrODBC(this.w_ECTIPAMM)+;
                     ",ECPERORD="+cp_ToStrODBC(this.w_ECPERORD)+;
                     ",ECPROESE="+cp_ToStrODBC(this.w_ECPROESE)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+;
                             " and ECPROESE="+cp_ToStrODBC(ECPROESE)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'CAT_AMMC')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      ECTIPAMM=this.w_ECTIPAMM;
                     ,ECPERORD=this.w_ECPERORD;
                     ,ECPROESE=this.w_ECPROESE;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                                      and ECPROESE=&i_TN.->ECPROESE;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.CAT_AMMC_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_AMMC_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (t_ECPROESE<>0  AND  t_ECPERORD<>0) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete CAT_AMMC
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and ECPROESE="+cp_ToStrODBC(&i_TN.->ECPROESE)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and ECPROESE=&i_TN.->ECPROESE;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (t_ECPROESE<>0  AND  t_ECPERORD<>0) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CAT_AMMC_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_AMMC_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,3,.t.)
        if .o_ECPROESE<>.w_ECPROESE.or. .o_ECTIPAMM<>.w_ECTIPAMM.or. .o_MAXPEORD<>.w_MAXPEORD
          .w_MAXPEORD = cp_ROUND(this.oParentObject.w_CCCOECIV*iif(.w_ECPROESE>0 AND .w_ECTIPAMM='5', 50/100, 1),2)
        endif
        if .o_ECPROESE<>.w_ECPROESE.or. .o_ECTIPAMM<>.w_ECTIPAMM.or. .o_ECPERORD<>.w_ECPERORD
          .w_COECIV = this.oParentObject.w_CCCOECIV
        endif
        if .o_MAXPEORD<>.w_MAXPEORD.or. .o_ECPROESE<>.w_ECPROESE.or. .o_ECTIPAMM<>.w_ECTIPAMM
          .w_ECPERORD = INT(.w_MAXPEORD * iif(.w_ECPROESE>0,1,0)*100)/100
        endif
        .DoRTCalc(7,7,.t.)
          .w_TOTPEORD = .w_TOTPEORD-.w_ecperord_t
          .w_ECPERORD_T = .w_ECPERORD
          .w_TOTPEORD = .w_TOTPEORD+.w_ecperord_t
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_MAXPEORD with this.w_MAXPEORD
      replace t_COECIV with this.w_COECIV
      replace t_ECPERORD_T with this.w_ECPERORD_T
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oTOTPEORD_3_1.value==this.w_TOTPEORD)
      this.oPgFrm.Page1.oPag.oTOTPEORD_3_1.value=this.w_TOTPEORD
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oECPROESE_2_1.value==this.w_ECPROESE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oECPROESE_2_1.value=this.w_ECPROESE
      replace t_ECPROESE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oECPROESE_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oECTIPAMM_2_2.RadioValue()==this.w_ECTIPAMM)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oECTIPAMM_2_2.SetRadio()
      replace t_ECTIPAMM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oECTIPAMM_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oECPERORD_2_5.value==this.w_ECPERORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oECPERORD_2_5.value=this.w_ECPERORD
      replace t_ECPERORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oECPERORD_2_5.value
    endif
    cp_SetControlsValueExtFlds(this,'CAT_AMMC')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsce_mca
      if this.w_TOTPEORD<>100 AND this.w_TOTPEORD<>0
         i_bnoChk=.f.
         i_bRes=.f.
         i_cErrorMsg=ah_MsgFormat("Il totale delle percentuali deve essere uguale a 100")
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(.w_ECTIPAMM='I' OR (.w_ECTIPAMM $ 'R5D' AND .w_ECPROESE=1)) and (.w_ECPROESE<>0  AND  .w_ECPERORD<>0)
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oECTIPAMM_2_2
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Tipo ammortamento non corretto")
        case   not(.w_ECPERORD>0 OR .w_ECPROESE=0 OR (.w_ECPERORD=100 AND .w_COECIV=100)) and (.w_ECPROESE<>0  AND  .w_ECPERORD<>0)
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oECPERORD_2_5
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Valore errato")
      endcase
      if .w_ECPROESE<>0  AND  .w_ECPERORD<>0
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_ECPROESE = this.w_ECPROESE
    this.o_ECTIPAMM = this.w_ECTIPAMM
    this.o_MAXPEORD = this.w_MAXPEORD
    this.o_ECPERORD = this.w_ECPERORD
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(t_ECPROESE<>0  AND  t_ECPERORD<>0)
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_ECPROESE=0
      .w_ECTIPAMM=space(1)
      .w_MAXPEORD=0
      .w_COECIV=0
      .w_ECPERORD=0
      .w_ECPERORD_T=0
      .DoRTCalc(1,2,.f.)
        .w_ECTIPAMM = 'I'
        .w_MAXPEORD = cp_ROUND(this.oParentObject.w_CCCOECIV*iif(.w_ECPROESE>0 AND .w_ECTIPAMM='5', 50/100, 1),2)
        .w_COECIV = this.oParentObject.w_CCCOECIV
        .w_ECPERORD = INT(.w_MAXPEORD * iif(.w_ECPROESE>0,1,0)*100)/100
      .DoRTCalc(7,7,.f.)
        .w_ECPERORD_T = .w_ECPERORD
    endwith
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_ECPROESE = t_ECPROESE
    this.w_ECTIPAMM = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oECTIPAMM_2_2.RadioValue(.t.)
    this.w_MAXPEORD = t_MAXPEORD
    this.w_COECIV = t_COECIV
    this.w_ECPERORD = t_ECPERORD
    this.w_ECPERORD_T = t_ECPERORD_T
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_ECPROESE with this.w_ECPROESE
    replace t_ECTIPAMM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oECTIPAMM_2_2.ToRadio()
    replace t_MAXPEORD with this.w_MAXPEORD
    replace t_COECIV with this.w_COECIV
    replace t_ECPERORD with this.w_ECPERORD
    replace t_ECPERORD_T with this.w_ECPERORD_T
    if i_srv='A'
      replace ECPROESE with this.w_ECPROESE
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
     with this
        .w_TOTPEORD = .w_TOTPEORD-.w_ecperord_t
        .SetControlsValue()
      endwith
  EndProc
enddefine

* --- Define pages as container
define class tgsce_mcaPag1 as StdContainer
  Width  = 294
  height = 133
  stdWidth  = 294
  stdheight = 133
  resizeXpos=133
  resizeYpos=70
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=6, top=4, width=278,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=3,Field1="ECPROESE",Label1="Prog.es.",Field2="ECTIPAMM",Label2="Tipo ammortamento",Field3="ECPERORD",Label3="% Ordinario",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 218948730

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-4,top=24,;
    width=274+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*4*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-3,top=25,width=273+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*4*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1

  add object oTOTPEORD_3_1 as StdField with uid="YCNKJKGRSN",rtseq=7,rtrep=.f.,;
    cFormVar="w_TOTPEORD",value=0,enabled=.f.,;
    HelpContextID = 35958918,;
    cQueryName = "TOTPEORD",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=191, Top=104, cSayPict=["999.99"], cGetPict=["999.99"]

  add object oStr_3_2 as StdString with uid="RMAUMWQCRS",Visible=.t., Left=143, Top=108,;
    Alignment=1, Width=44, Height=15,;
    Caption="Totale:"  ;
  , bGlobalFont=.t.
enddefine

* --- Defining Body row
define class tgsce_mcaBodyRow as CPBodyRowCnt
  Width=264
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oECPROESE_2_1 as StdTrsField with uid="CVMYHRXMWM",rtseq=2,rtrep=.t.,;
    cFormVar="w_ECPROESE",value=0,isprimarykey=.t.,;
    ToolTipText = "Progressivo esercizio",;
    HelpContextID = 193133941,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=48, Left=-2, Top=0, cSayPict=["999"], cGetPict=["999"]

  add object oECTIPAMM_2_2 as StdTrsCombo with uid="QOKPVLLJNZ",rtrep=.t.,;
    cFormVar="w_ECTIPAMM", RowSource=""+"Rateo utilizzo,"+"Ridotto 50%,"+"Intero,"+"Diverso" , ;
    ToolTipText = "Tipo ammortamento",;
    HelpContextID = 259767661,;
    Height=22, Width=125, Left=53, Top=-2,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2  , sErrorMsg = "Tipo ammortamento non corretto";
, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oECTIPAMM_2_2.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..ECTIPAMM,&i_cF..t_ECTIPAMM),this.value)
    return(iif(xVal =1,'R',;
    iif(xVal =2,'5',;
    iif(xVal =3,'I',;
    iif(xVal =4,'D',;
    space(1))))))
  endfunc
  func oECTIPAMM_2_2.GetRadio()
    this.Parent.oContained.w_ECTIPAMM = this.RadioValue()
    return .t.
  endfunc

  func oECTIPAMM_2_2.ToRadio()
    this.Parent.oContained.w_ECTIPAMM=trim(this.Parent.oContained.w_ECTIPAMM)
    return(;
      iif(this.Parent.oContained.w_ECTIPAMM=='R',1,;
      iif(this.Parent.oContained.w_ECTIPAMM=='5',2,;
      iif(this.Parent.oContained.w_ECTIPAMM=='I',3,;
      iif(this.Parent.oContained.w_ECTIPAMM=='D',4,;
      0)))))
  endfunc

  func oECTIPAMM_2_2.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oECTIPAMM_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_ECTIPAMM='I' OR (.w_ECTIPAMM $ 'R5D' AND .w_ECPROESE=1))
    endwith
    return bRes
  endfunc

  add object oECPERORD_2_5 as StdTrsField with uid="ZBXCFJLKWC",rtseq=6,rtrep=.t.,;
    cFormVar="w_ECPERORD",value=0,;
    ToolTipText = "Percentuale ammortamento ordinario",;
    HelpContextID = 23068022,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Valore errato",;
   bGlobalFont=.t.,;
    Height=17, Width=76, Left=183, Top=0, cSayPict=["999.99"], cGetPict=["999.99"]

  func oECPERORD_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_ECPERORD>0 OR .w_ECPROESE=0 OR (.w_ECPERORD=100 AND .w_COECIV=100))
    endwith
    return bRes
  endfunc
  add object oLast as LastKeyMover
  * ---
  func oECPROESE_2_1.When()
    return(.t.)
  proc oECPROESE_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oECPROESE_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=3
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsce_mca','CAT_AMMC','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".ECCODCAT=CAT_AMMC.ECCODCAT";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
