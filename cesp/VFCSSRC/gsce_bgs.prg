* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_bgs                                                        *
*              Lancia programma collegato                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][22]                                                      *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-02-26                                                      *
* Last revis.: 2014-01-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTIPO,pCODI
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsce_bgs",oParentObject,m.pTIPO,m.pCODI)
return(i_retval)

define class tgsce_bgs as StdBatch
  * --- Local variables
  pTIPO = space(1)
  pCODI = space(20)
  w_PROG = .NULL.
  w_APPO = space(4)
  w_CESPITE = space(20)
  w_ESERCI = space(4)
  w_FINESE = ctod("  /  /  ")
  w_CODAZI = space(5)
  w_DATA = ctod("  /  /  ")
  w_SELESER = space(4)
  w_CODI = space(20)
  w_MVCLADOC = space(2)
  w_MVFLVEAC = space(1)
  w_PRES = .f.
  w_ERR = space(10)
  * --- WorkFile variables
  MOV_CESP_idx=0
  ESERCIZI_idx=0
  DOC_MAST_idx=0
  CONTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Se il parametro vale 'C' batch lanciato dall'anagrafica cespiti (Gsce_Ace) per poter risalire ai saldi
    *     Se il parametro vale 'M' lanciato dall'anagrafica Dettaglio piano di ammortamento (Gsce_Aac) per vedere il movimento cespite associato
    *     Se il parametro vale 'D' lanciato dal bottone righe nei movimenti cespiti (Gsce_Km2) per vedere il documento associato. 
    *     Se il parametro vale 'A' o 'R' lanciato dalla maschera Abbinamento cespiti (Gsce_Kab)
    *     Se il parametro vale 'V' lanciato dalla anagrafica clienti (GSAR_ACL)
    do case
      case this.pTipo="C"
        * --- Operazioni svolte per poter vedere il saldo del cespite indipendentemente
        * --- se � presente una movimentazione nell'anno.
        this.w_CESPITE = this.pCodi
        this.w_ESERCI = g_codese
        this.w_CODAZI = i_Codazi
        this.w_CODI = this.pCODI
        * --- Effettuo una Read sulla tabella esercizi per avere la data di fine esercizio
        * --- Read from ESERCIZI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ESERCIZI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2],.t.,this.ESERCIZI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ESFINESE"+;
            " from "+i_cTable+" ESERCIZI where ";
                +"ESCODAZI = "+cp_ToStrODBC(this.w_Codazi);
                +" and ESCODESE = "+cp_ToStrODBC(this.w_Eserci);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ESFINESE;
            from (i_cTable) where;
                ESCODAZI = this.w_Codazi;
                and ESCODESE = this.w_Eserci;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_Finese = NVL(cp_ToDate(_read_.ESFINESE),cp_NullValue(_read_.ESFINESE))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Questa query mi permette di avere per il cespite specificato la data di fine esercizio pi� elevata.
        vq_exec("..\CESP\EXE\QUERY\Sald_pre.vqr",this,"Data")
        Select Data
        Go Top
        this.w_DATA = Cp_todate(Data.Esfinese)
        * --- Calcolo l'esercizio in cui si trova la data
        this.w_SELESER = calceser(this.w_Data, space(4))
        * --- Chiudo il cursore Data
        if used("Data")
          select ("Data")
          use
        endif
        * --- Saldi Cespiti
        this.w_PROG = GSCE_ASC()
        * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
        if !(this.w_PROG.bSec1)
          i_retcode = 'stop'
          return
        endif
        this.w_PROG.w_SCCODCES = this.pCODI
        this.w_PROG.w_SCCODESE = this.w_Seleser
        this.w_PROG.QueryKeySet("SCCODCES="+ CP_TOSTRODBC(this.pCODI) + " AND SCCODESE="+CP_TOSTRODBC(this.w_Seleser),"")     
        * --- carico il record
        this.w_PROG.LoadRecWarn()     
      case this.pTipo="M"
        if Not Empty( this.pCODI )
          * --- Movimento Cespite
          this.w_PROG = GSCE_AMC()
          * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
          if !(this.w_PROG.bSec1)
            i_retcode = 'stop'
            return
          endif
          this.w_PROG.w_MCSERIAL = this.pCODI
          this.w_PROG.QueryKeySet("MCSERIAL='"+this.pCODI+"'" ,"")     
          * --- carico il record
          this.w_PROG.LoadRecWarn()     
        endif
      case this.pTipo="D"
        if Not Empty( this.pCODI )
          if g_APPLICATION="ADHOC REVOLUTION"
            gsar_bzm(this,this.pCODI,-20)
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          else
            * --- testa il tipo  e la classe documento
            * --- Read from DOC_MAST
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.DOC_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "MVCLADOC,MVFLVEAC"+;
                " from "+i_cTable+" DOC_MAST where ";
                    +"MVSERIAL = "+cp_ToStrODBC(this.pCODI);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                MVCLADOC,MVFLVEAC;
                from (i_cTable) where;
                    MVSERIAL = this.pCODI;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_MVCLADOC = NVL(cp_ToDate(_read_.MVCLADOC),cp_NullValue(_read_.MVCLADOC))
              this.w_MVFLVEAC = NVL(cp_ToDate(_read_.MVFLVEAC),cp_NullValue(_read_.MVFLVEAC))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.w_PROG = GSVE_MDV(this.w_MVFLVEAC+this.w_MVCLADOC)
            * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
            if !(this.w_PROG.bSec1)
              i_retcode = 'stop'
              return
            endif
            this.w_PROG.w_MVSERIAL = this.pCODI
            this.w_PROG.QueryKeySet("MVSERIAL='"+this.pCODI+"'" ,"")     
            * --- carico il record
            this.w_PROG.LoadRecWarn()     
          endif
        endif
      case this.pTipo="A"
        * --- Abbinamento tra Movimenti Cespiti e Primanota
        * --- Controlla la congruenza dell'abbinamento: N: Doc, Alfa Doc, Data Doc e Cli/For
        if this.oParentObject.w_MCNUMDOC<>this.oParentObject.w_PNNUMD OR this.oParentObject.w_MCALFDOC<>this.oParentObject.w_PNALFD OR this.oParentObject.w_MCDATDOC<>this.oParentObject.w_PNDATA OR this.oParentObject.w_MCCODCON<>this.oParentObject.w_PNCLFO
          ah_ErrorMsg("Numero o data o serie del documento non congruente")
          i_retcode = 'stop'
          return
        endif
        this.oParentObject.w_MCRIFCON = this.pCODI
        THIS.oParentObject.ecpSave
      case this.pTIPO="R"
        * --- Ricalcola zoom
        if EMPTY(this.oParentObject.w_DATA1) OR (NOT EMPTY(this.oParentObject.w_DATA1) AND NOT EMPTY(this.oParentObject.w_DATA2))
          THIS.oParentObject.NotifyEvent("Init")
        endif
      case this.pTIPO="V"
        * --- Select from MOV_CESP
        i_nConn=i_TableProp[this.MOV_CESP_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MOV_CESP_idx,2],.t.,this.MOV_CESP_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" MOV_CESP ";
              +" where MCTIPCON="+cp_ToStrODBC(this.oParentObject.w_ANTIPCON)+" AND MCCODCON="+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+"";
               ,"_Curs_MOV_CESP")
        else
          select * from (i_cTable);
           where MCTIPCON=this.oParentObject.w_ANTIPCON AND MCCODCON=this.oParentObject.w_ANCODICE;
            into cursor _Curs_MOV_CESP
        endif
        if used('_Curs_MOV_CESP')
          select _Curs_MOV_CESP
          locate for 1=1
          do while not(eof())
          this.w_PRES = .T.
            select _Curs_MOV_CESP
            continue
          enddo
          use
        endif
        if this.w_PRES
          if this.oParentObject.w_ANTIPCON="C"
            this.w_ERR = AH_MSGFORMAT("Codice cliente presente nei movimenti cespiti, impossibile cancellare")
          else
            this.w_ERR = AH_MSGFORMAT("Codice fornitore presente nei movimenti cespiti, impossibile cancellare")
          endif
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_ERR
        endif
        if ISAHR()
          * --- Select from CONTI
          i_nConn=i_TableProp[this.CONTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select * from "+i_cTable+" CONTI ";
                +" where ANCONRIF="+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+" ";
                 ,"_Curs_CONTI")
          else
            select * from (i_cTable);
             where ANCONRIF=this.oParentObject.w_ANCODICE ;
              into cursor _Curs_CONTI
          endif
          if used('_Curs_CONTI')
            select _Curs_CONTI
            locate for 1=1
            do while not(eof())
            this.w_PRES = .T.
              select _Curs_CONTI
              continue
            enddo
            use
          endif
          if this.w_PRES
            if this.oParentObject.w_ANTIPCON="C"
              this.w_ERR = AH_MSGFORMAT("Codice cliente presente nel campo collegamento, impossibile cancellare")
            else
              this.w_ERR = AH_MSGFORMAT("Codice fornitore presente nel campo collegamento, impossibile cancellare")
            endif
            * --- transaction error
            bTrsErr=.t.
            i_TrsMsg=this.w_ERR
          endif
        endif
    endcase
  endproc


  proc Init(oParentObject,pTIPO,pCODI)
    this.pTIPO=pTIPO
    this.pCODI=pCODI
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='MOV_CESP'
    this.cWorkTables[2]='ESERCIZI'
    this.cWorkTables[3]='DOC_MAST'
    this.cWorkTables[4]='CONTI'
    return(this.OpenAllTables(4))

  proc CloseCursors()
    if used('_Curs_MOV_CESP')
      use in _Curs_MOV_CESP
    endif
    if used('_Curs_CONTI')
      use in _Curs_CONTI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTIPO,pCODI"
endproc
