* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_bcr                                                        *
*              Caricamento rapido cespiti                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_48]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-10-10                                                      *
* Last revis.: 2012-05-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipo
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsce_bcr",oParentObject,m.pTipo)
return(i_retval)

define class tgsce_bcr as StdBatch
  * --- Local variables
  pTipo = space(1)
  w_CATEGORIA = space(10)
  w_SER = space(10)
  w_CODCESP = space(20)
  * --- WorkFile variables
  CAT_CESP_idx=0
  FAM_CESP_idx=0
  UBI_CESP_idx=0
  CES_PITI_idx=0
  CAN_TIER_idx=0
  VOC_COST_idx=0
  CENCOST_idx=0
  UNIMIS_idx=0
  BUSIUNIT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Caricamento Rapido Cespiti da GSCE_ACE Evento: Record Inserted
    * --- Se parametro R = caricamento rapido
    *     Se parametro C, calcola il codice del cespite
    if This.oparentobject.cfunction<>"Edit"
      * --- Inibito in modifica per evitare che sia cambiata la chiave primaria di un record gi� inserito
      if this.pTipo = "C" 
        if !EMPTY(this.oParentObject.w_PREFISSO)
          this.w_SER = this.oParentObject.w_CE_SERIA
          this.w_CATEGORIA = this.oParentObject.w_CECODCAT
          i_nConn = i_TableProp[this.OPARENTOBJECT.CES_PITI_IDX,3]
          i_cTable = cp_SetAzi(i_TableProp[this.OPARENTOBJECT.CES_PITI_IDX,2])
          cp_AskTableProg(this,i_nConn,"CESSER","i_CODAZI,w_CATEGORIA,w_SER")
          this.oParentObject.w_CE_SERIA = this.w_SER
          this.oParentObject.w_CECODICE = IIF(EMPTY(this.oParentObject.w_PREFISSO),this.oParentObject.w_CECODICE,ALLTRIM(this.oParentObject.w_PREFISSO) + this.oParentObject.w_CE_SERIA)
        else
          this.oParentObject.w_CECODICE = space(20)
        endif
        i_retcode = 'stop'
        return
      endif
      if g_F4RAPIDO="S" AND NOT EMPTY(g_CODCESP)
        * --- Rilegge Ultimo Record Inserito
        this.w_CODCESP = g_CODCESP
        vq_exec("..\CESP\EXE\query\GSCE_BCR.VQR",this,"CARCESP")
        if USED("CARCESP")
          SELECT CARCESP
          if RECCOUNT() > 0
            this.oParentObject.w_CECODICE = NVL(CECODICE, SPACE(40))
            this.oParentObject.w_CECODCAT = SPACE(15)
            this.oParentObject.w_CEDESSUP = CEDESSUP
            this.oParentObject.w_CEDESCRI = NVL(CEDESCRI, SPACE(40))
            this.oParentObject.w_CETIPCES = NVL(CETIPCES, SPACE(2))
            this.oParentObject.w_CE_STATO = NVL(CE_STATO, SPACE(2))
            this.oParentObject.w_CESTABEN = NVL(CESTABEN, SPACE(1))
            setvaluelinked("M", this.oParentObject, "w_CEUNIMIS", NVL(CEUNIMIS, SPACE(3)))
            setvaluelinked("M", this.oParentObject, "w_CECODUBI", NVL(CECODUBI, SPACE(20)))
            this.oParentObject.w_CECODMAT = NVL(CECODMAT, SPACE(20))
            setvaluelinked("M", this.oParentObject, "w_CECODFAM", NVL(CECODFAM, SPACE(5)))
            setvaluelinked("M", this.oParentObject, "w_CECODPER", NVL(CECODPER, SPACE(20)))
            setvaluelinked("M", this.oParentObject, "w_CECODCOM", NVL(CECODCOM, SPACE(15)))
            this.oParentObject.w_CEDTPRIU = NVL(CEDTPRIU, cp_CharToDate("  -  -  "))
            this.oParentObject.w_CEESPRIU = NVL(CEESPRIU, SPACE(4))
            this.oParentObject.w_CEDTPRIC = NVL(CEDTPRIC, cp_CharToDate("  -  -  "))
            this.oParentObject.w_CEESPRIC = NVL(CEESPRIC, SPACE(4))
            this.oParentObject.w_CEDATDIS = NVL(CEDATDIS, cp_CharToDate("  -  -  "))
            this.oParentObject.w_CEFLSPEM = NVL(CEFLSPEM, SPACE(1))
            this.oParentObject.w_CEFLCEUS = NVL(CEFLCEUS, SPACE(1))
            setvaluelinked("M", this.oParentObject, "w_CEVOCCEN", NVL(CEVOCCEN, SPACE(15)))
            setvaluelinked("M", this.oParentObject, "w_CECODCEN", NVL(CECODCEN, SPACE(15)))
          endif
          USE
        endif
      endif
      g_CODCESP = SPACE(20)
      * --- Il codice del cespite deve sempre essere reimpostato
      this.oParentObject.w_CECODICE = space(20)
    endif
  endproc


  proc Init(oParentObject,pTipo)
    this.pTipo=pTipo
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,9)]
    this.cWorkTables[1]='CAT_CESP'
    this.cWorkTables[2]='FAM_CESP'
    this.cWorkTables[3]='UBI_CESP'
    this.cWorkTables[4]='CES_PITI'
    this.cWorkTables[5]='CAN_TIER'
    this.cWorkTables[6]='VOC_COST'
    this.cWorkTables[7]='CENCOST'
    this.cWorkTables[8]='UNIMIS'
    this.cWorkTables[9]='BUSIUNIT'
    return(this.OpenAllTables(9))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipo"
endproc
