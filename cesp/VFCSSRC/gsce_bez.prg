* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_bez                                                        *
*              Elab.zoom disab.movim.cespiti                                   *
*                                                                              *
*      Author: Zucchetti TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_60]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-04-13                                                      *
* Last revis.: 2000-05-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsce_bez",oParentObject,m.pOper)
return(i_retval)

define class tgsce_bez as StdBatch
  * --- Local variables
  pOper = space(4)
  w_ZOOM = space(10)
  w_DATCON = ctod("  /  /  ")
  w_RIFCON = space(10)
  w_MESS = space(100)
  w_CANCEL = .f.
  w_SERIAL = space(10)
  w_PROG = .NULL.
  w_CONT = 0
  w_TIPO = space(1)
  w_ACSERIAL = space(10)
  w_MOVSER = space(10)
  w_MVSERIAL = space(10)
  w_CPROWNUM = 0
  * --- WorkFile variables
  MOV_CESP_idx=0
  PNT_MAST_idx=0
  PNT_DETT_idx=0
  SALDICON_idx=0
  MOVICOST_idx=0
  PNT_CESP_idx=0
  DOC_DETT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gsetione maschera disabbinamento movimenti cespiti
    * --- Parametro operazione
    * --- Variabili maschera
    * --- Variabili Locali
    this.w_ZOOM = this.oParentObject.w_ZoomGepr
    this.w_DATCON = cp_CharToDate("  -  -    ")
    this.w_RIFCON = space(10)
    this.w_MESS = space(100)
    this.w_CANCEL = .F.
    this.w_SERIAL = space(10)
    do case
      case this.pOper="SEL"
        * --- Seleziona/Deseleziona Tutto
        NC = this.w_ZOOM.cCursor
        if this.oParentObject.w_SELEZI="S"
          UPDATE &NC SET XCHK = 1
          this.oParentObject.w_FLSELE = 1
        else
          UPDATE &NC SET XCHK = 0
          this.oParentObject.w_FLSELE = 0
        endif
      case this.pOper="DIS"
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pOper="CAR"
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pOper="RMC"
        * --- Cespite
        this.w_PROG = GSCE_AMC()
        * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
        if !(this.w_PROG.bSec1)
          i_retcode = 'stop'
          return
        endif
        this.w_PROG.w_MCSERIAL = this.oParentObject.w_MCSERIAL
        this.w_PROG.QueryKeySet("MCSERIAL='"+this.oParentObject.w_MCSERIAL+"'" ,"")     
        * --- carico il record
        this.w_PROG.LoadRec()     
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Disabbina Movimenti Cespiti
    NC = this.w_ZOOM.cCursor
    SELECT &NC
    GO TOP
    this.w_CONT = 0
    * --- Cicla sui record Selezionati
    SCAN FOR XCHK<>0
    this.w_CONT = this.w_CONT+1
    this.w_TIPO = NVL(TIPO," ")
    if this.w_TIPO="P"
      this.w_ACSERIAL = NVL(ACSERIAL,SPACE(10))
      this.w_MOVSER = NVL(MCSERIAL,SPACE(10))
      * --- Delete from PNT_CESP
      i_nConn=i_TableProp[this.PNT_CESP_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PNT_CESP_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"ACSERIAL = "+cp_ToStrODBC(this.w_ACSERIAL);
              +" and ACMOVCES = "+cp_ToStrODBC(this.w_MOVSER);
              +" and ACTIPASS = "+cp_ToStrODBC("M");
               )
      else
        delete from (i_cTable) where;
              ACSERIAL = this.w_ACSERIAL;
              and ACMOVCES = this.w_MOVSER;
              and ACTIPASS = "M";

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
    else
      this.w_MVSERIAL = NVL(MVSERIAL,SPACE(10))
      this.w_CPROWNUM = NVL(CPROWNUM,0)
      * --- Write into DOC_DETT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DOC_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MVCESSER ="+cp_NullLink(cp_ToStrODBC(SPACE(10)),'DOC_DETT','MVCESSER');
            +i_ccchkf ;
        +" where ";
            +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
            +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
               )
      else
        update (i_cTable) set;
            MVCESSER = SPACE(10);
            &i_ccchkf. ;
         where;
            MVSERIAL = this.w_MVSERIAL;
            and CPROWNUM = this.w_CPROWNUM;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    ENDSCAN
    this.w_MESS = "Movimenti disabbinati"
    if this.w_CONT>0
      ah_ErrorMsg(this.w_Mess,"!","")
    else
      i_retcode = 'stop'
      return
    endif
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Carica Zoom di Selezione
    this.oParentObject.NotifyEvent("Esegui")
  endproc


  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,7)]
    this.cWorkTables[1]='MOV_CESP'
    this.cWorkTables[2]='PNT_MAST'
    this.cWorkTables[3]='PNT_DETT'
    this.cWorkTables[4]='SALDICON'
    this.cWorkTables[5]='MOVICOST'
    this.cWorkTables[6]='PNT_CESP'
    this.cWorkTables[7]='DOC_DETT'
    return(this.OpenAllTables(7))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
