* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_bcc                                                        *
*              Controlli causali cespiti                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_22]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-03-25                                                      *
* Last revis.: 2010-05-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipo
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsce_bcc",oParentObject,m.pTipo)
return(i_retval)

define class tgsce_bcc as StdBatch
  * --- Local variables
  pTipo = space(1)
  w_TROV = .f.
  w_MESS = space(10)
  w_OK = .f.
  w_MESS1 = space(50)
  w_CODCAU = space(5)
  w_TIPDOC = space(5)
  * --- WorkFile variables
  TIP_DOCU_idx=0
  CAU_CONT_idx=0
  CAU_CESP_idx=0
  MOV_CESP_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controlli Finali Causali Cespiti (da GSCE_ACC)
    do case
      case this.pTipo="C"
        this.w_OK = .T.
        this.w_MESS = ah_Msgformat("Transazione abbandonata")
        * --- Controllo che non siano settati sia la data di primo utilizzo che la data di dismissione
        if this.oParentObject.w_CCFLDADI="S" AND (this.oParentObject.w_CCFLPRIU="S" OR this.oParentObject.w_CCFLPRIC="S")
          this.w_MESS = ah_Msgformat("Un cespite non pu� essere attivato e dismesso con un solo movimento")
          this.w_OK = .F.
        endif
        if this.oParentObject.w_CCFLCONT="S" And this.w_OK
          FOR L_i = 1 TO 24
          AA = RIGHT("00"+ALLTRIM(STR(L_i)), 2)
          if this.oParentObject.w_CCFLGA&AA = "S" AND AA<>"17"
            this.w_TROV = .F.
            SELECT (this.oParentObject.GSCE_MOC.cTrsName)
            GO TOP
            SCAN FOR t_CPROWORD<>0 AND NOT EMPTY(NVL(t_MCCODICE," "))
            if t_MCCODICE="A"+AA
              this.w_TROV = .T.
            endif
            ENDSCAN
            if this.w_TROV=.F.
              this.w_MESS = ah_Msgformat("Campo: A %1 non presente nei modelli contabili", AA)
              this.w_OK = .F.
              EXIT
            endif
          endif
          ENDFOR
        endif
        if Not this.w_OK
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_MESS
        endif
      case this.pTipo="O" And Not Empty( this.oParentObject.w_CCDTOBSO )
        * --- Al cambio data obsolescenza verifico se le casuali documenti / contabili
        *     che utilizzano la casuale cespite sono obsolete alla data, se no impedisco
        *     la valorizzazione
        this.w_MESS = ""
        * --- Verifico se una casuale documento utilizza la casuale, se si impedisco la modifica..
        * --- Read from TIP_DOCU
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "TDTIPDOC"+;
            " from "+i_cTable+" TIP_DOCU where ";
                +"TDCAUCES = "+cp_ToStrODBC(this.oParentObject.w_CCCODICE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            TDTIPDOC;
            from (i_cTable) where;
                TDCAUCES = this.oParentObject.w_CCCODICE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_TIPDOC = NVL(cp_ToDate(_read_.TDTIPDOC),cp_NullValue(_read_.TDTIPDOC))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if not Empty( this.w_TIPDOC )
          this.w_MESS = ah_Msgformat("Impossibile impostare la data obsolescenza. Causale utilizzata nella causale documento %1",this.w_TIPDOC)
        else
          * --- Verifico la minima data obsolescenza legata alle causali contabili
          *     che utilizzano la casuale cespiti...
          * --- Select from CAU_CONT
          i_nConn=i_TableProp[this.CAU_CONT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAU_CONT_idx,2],.t.,this.CAU_CONT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select CCCODICE  from "+i_cTable+" CAU_CONT ";
                +" where CCCAUCES = "+cp_ToStrODBC(this.oParentObject.w_CCCODICE)+" And ( CCDTOBSO Is Null Or CCDTOBSO< "+cp_ToStrODBC(this.oParentObject.w_CCDTOBSO)+" )";
                +" order by CCDTOBSO";
                 ,"_Curs_CAU_CONT")
          else
            select CCCODICE from (i_cTable);
             where CCCAUCES = this.oParentObject.w_CCCODICE And ( CCDTOBSO Is Null Or CCDTOBSO< this.oParentObject.w_CCDTOBSO );
             order by CCDTOBSO;
              into cursor _Curs_CAU_CONT
          endif
          if used('_Curs_CAU_CONT')
            select _Curs_CAU_CONT
            locate for 1=1
            do while not(eof())
            this.w_MESS = ah_Msgformat("Impossibile impostare la data obsolescenza%0Causale utilizzata in causale contabile %1%0con data obsolescenza non impostata o minore alla data impostata", _Curs_CAU_CONT.CCCODICE)
              select _Curs_CAU_CONT
              continue
            enddo
            use
          endif
        endif
        if Not Empty( this.w_MESS )
          * --- Rileggo dal database il valore originario...
          * --- Read from CAU_CESP
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CAU_CESP_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAU_CESP_idx,2],.t.,this.CAU_CESP_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CCDTOBSO"+;
              " from "+i_cTable+" CAU_CESP where ";
                  +"CCCODICE = "+cp_ToStrODBC(this.oParentObject.w_CCCODICE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CCDTOBSO;
              from (i_cTable) where;
                  CCCODICE = this.oParentObject.w_CCCODICE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_CCDTOBSO = NVL(cp_ToDate(_read_.CCDTOBSO),cp_NullValue(_read_.CCDTOBSO))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          ah_ErrorMsg("%1",,"", this.w_MESS)
        endif
      case this.pTipo="D"
        * --- Read from MOV_CESP
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.MOV_CESP_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MOV_CESP_idx,2],.t.,this.MOV_CESP_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MCCODCAU"+;
            " from "+i_cTable+" MOV_CESP where ";
                +"MCCODCAU = "+cp_ToStrODBC(this.oParentObject.w_CCCODICE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MCCODCAU;
            from (i_cTable) where;
                MCCODCAU = this.oParentObject.w_CCCODICE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CODCAU = NVL(cp_ToDate(_read_.MCCODCAU),cp_NullValue(_read_.MCCODCAU))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if i_ROWS>0
          this.w_MESS = "Causale presente nei movimenti cespiti, impossibile cancellare"
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_MESS
        endif
    endcase
  endproc


  proc Init(oParentObject,pTipo)
    this.pTipo=pTipo
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='TIP_DOCU'
    this.cWorkTables[2]='CAU_CONT'
    this.cWorkTables[3]='CAU_CESP'
    this.cWorkTables[4]='MOV_CESP'
    return(this.OpenAllTables(4))

  proc CloseCursors()
    if used('_Curs_CAU_CONT')
      use in _Curs_CAU_CONT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipo"
endproc
