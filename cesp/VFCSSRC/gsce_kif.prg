* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_kif                                                        *
*              Stampa inventario fisico cespiti                                *
*                                                                              *
*      Author: TAM Software srl                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_21]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-04-13                                                      *
* Last revis.: 2008-09-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsce_kif",oParentObject))

* --- Class definition
define class tgsce_kif as StdForm
  Top    = 50
  Left   = 100

  * --- Standard Properties
  Width  = 573
  Height = 237
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-09-15"
  HelpContextID=267011433
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=11

  * --- Constant Properties
  _IDX = 0
  CAT_CESP_IDX = 0
  GRU_COCE_IDX = 0
  CES_PITI_IDX = 0
  FAM_CESP_IDX = 0
  UBI_CESP_IDX = 0
  cPrg = "gsce_kif"
  cComment = "Stampa inventario fisico cespiti"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CATINI = space(15)
  w_DESCAT = space(40)
  w_CATFIN = space(15)
  w_DESCAT1 = space(40)
  w_CODFAM = space(15)
  w_CODUBI = space(20)
  w_DATSTA = ctod('  /  /  ')
  o_DATSTA = ctod('  /  /  ')
  w_DESFAM = space(35)
  w_STAMPADES = space(1)
  w_DESUBI = space(40)
  w_DATSTA1 = ctod('  /  /  ')
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsce_kifPag1","gsce_kif",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCATINI_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='CAT_CESP'
    this.cWorkTables[2]='GRU_COCE'
    this.cWorkTables[3]='CES_PITI'
    this.cWorkTables[4]='FAM_CESP'
    this.cWorkTables[5]='UBI_CESP'
    return(this.OpenAllTables(5))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      vx_exec(""+alltrim(THIS.w_OQRY)+", "+alltrim(THIS.w_orep)+"",this)
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CATINI=space(15)
      .w_DESCAT=space(40)
      .w_CATFIN=space(15)
      .w_DESCAT1=space(40)
      .w_CODFAM=space(15)
      .w_CODUBI=space(20)
      .w_DATSTA=ctod("  /  /  ")
      .w_DESFAM=space(35)
      .w_STAMPADES=space(1)
      .w_DESUBI=space(40)
      .w_DATSTA1=ctod("  /  /  ")
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CATINI))
          .link_1_3('Full')
        endif
        .DoRTCalc(2,3,.f.)
        if not(empty(.w_CATFIN))
          .link_1_5('Full')
        endif
        .DoRTCalc(4,5,.f.)
        if not(empty(.w_CODFAM))
          .link_1_7('Full')
        endif
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_CODUBI))
          .link_1_8('Full')
        endif
        .w_DATSTA = i_datsys
          .DoRTCalc(8,8,.f.)
        .w_STAMPADES = ' '
      .oPgFrm.Page1.oPag.oObj_1_14.Calculate()
          .DoRTCalc(10,10,.f.)
        .w_DATSTA1 = IIF(NOT EMPTY(.w_DATSTA),.w_DATSTA, i_INIDAT)
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate()
        .DoRTCalc(1,10,.t.)
        if .o_DATSTA<>.w_DATSTA
            .w_DATSTA1 = IIF(NOT EMPTY(.w_DATSTA),.w_DATSTA, i_INIDAT)
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_14.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CATINI
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_CESP_IDX,3]
    i_lTable = "CAT_CESP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_CESP_IDX,2], .t., this.CAT_CESP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_CESP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCE_ACT',True,'CAT_CESP')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_CATINI)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_CATINI))
          select CCCODICE,CCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATINI)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStrODBC(trim(this.w_CATINI)+"%");

            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStr(trim(this.w_CATINI)+"%");

            select CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CATINI) and !this.bDontReportError
            deferred_cp_zoom('CAT_CESP','*','CCCODICE',cp_AbsName(oSource.parent,'oCATINI_1_3'),i_cWhere,'GSCE_ACT',"Categorie",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_CATINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_CATINI)
            select CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATINI = NVL(_Link_.CCCODICE,space(15))
      this.w_DESCAT = NVL(_Link_.CCDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CATINI = space(15)
      endif
      this.w_DESCAT = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_CESP_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_CESP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATFIN
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_CESP_IDX,3]
    i_lTable = "CAT_CESP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_CESP_IDX,2], .t., this.CAT_CESP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_CESP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCE_ACT',True,'CAT_CESP')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_CATFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_CATFIN))
          select CCCODICE,CCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATFIN)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStrODBC(trim(this.w_CATFIN)+"%");

            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStr(trim(this.w_CATFIN)+"%");

            select CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CATFIN) and !this.bDontReportError
            deferred_cp_zoom('CAT_CESP','*','CCCODICE',cp_AbsName(oSource.parent,'oCATFIN_1_5'),i_cWhere,'GSCE_ACT',"Categorie",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_CATFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_CATFIN)
            select CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATFIN = NVL(_Link_.CCCODICE,space(15))
      this.w_DESCAT1 = NVL(_Link_.CCDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CATFIN = space(15)
      endif
      this.w_DESCAT1 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_CESP_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_CESP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFAM
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_CESP_IDX,3]
    i_lTable = "FAM_CESP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_CESP_IDX,2], .t., this.FAM_CESP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_CESP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFAM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCE_AFC',True,'FAM_CESP')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_CODFAM)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_CODFAM))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODFAM)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" FADESCRI like "+cp_ToStrODBC(trim(this.w_CODFAM)+"%");

            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" FADESCRI like "+cp_ToStr(trim(this.w_CODFAM)+"%");

            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODFAM) and !this.bDontReportError
            deferred_cp_zoom('FAM_CESP','*','FACODICE',cp_AbsName(oSource.parent,'oCODFAM_1_7'),i_cWhere,'GSCE_AFC',"Categorie",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFAM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_CODFAM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_CODFAM)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFAM = NVL(_Link_.FACODICE,space(15))
      this.w_DESFAM = NVL(_Link_.FADESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODFAM = space(15)
      endif
      this.w_DESFAM = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_CESP_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_CESP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFAM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODUBI
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UBI_CESP_IDX,3]
    i_lTable = "UBI_CESP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UBI_CESP_IDX,2], .t., this.UBI_CESP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UBI_CESP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODUBI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCE_AUB',True,'UBI_CESP')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UBCODICE like "+cp_ToStrODBC(trim(this.w_CODUBI)+"%");

          i_ret=cp_SQL(i_nConn,"select UBCODICE,UBDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UBCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UBCODICE',trim(this.w_CODUBI))
          select UBCODICE,UBDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UBCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODUBI)==trim(_Link_.UBCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" UBDESCRI like "+cp_ToStrODBC(trim(this.w_CODUBI)+"%");

            i_ret=cp_SQL(i_nConn,"select UBCODICE,UBDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" UBDESCRI like "+cp_ToStr(trim(this.w_CODUBI)+"%");

            select UBCODICE,UBDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODUBI) and !this.bDontReportError
            deferred_cp_zoom('UBI_CESP','*','UBCODICE',cp_AbsName(oSource.parent,'oCODUBI_1_8'),i_cWhere,'GSCE_AUB',"Elenco ubicazioni cespiti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODICE,UBDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where UBCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UBCODICE',oSource.xKey(1))
            select UBCODICE,UBDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODUBI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODICE,UBDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where UBCODICE="+cp_ToStrODBC(this.w_CODUBI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UBCODICE',this.w_CODUBI)
            select UBCODICE,UBDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODUBI = NVL(_Link_.UBCODICE,space(20))
      this.w_DESUBI = NVL(_Link_.UBDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODUBI = space(20)
      endif
      this.w_DESUBI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UBI_CESP_IDX,2])+'\'+cp_ToStr(_Link_.UBCODICE,1)
      cp_ShowWarn(i_cKey,this.UBI_CESP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODUBI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCATINI_1_3.value==this.w_CATINI)
      this.oPgFrm.Page1.oPag.oCATINI_1_3.value=this.w_CATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAT_1_4.value==this.w_DESCAT)
      this.oPgFrm.Page1.oPag.oDESCAT_1_4.value=this.w_DESCAT
    endif
    if not(this.oPgFrm.Page1.oPag.oCATFIN_1_5.value==this.w_CATFIN)
      this.oPgFrm.Page1.oPag.oCATFIN_1_5.value=this.w_CATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAT1_1_6.value==this.w_DESCAT1)
      this.oPgFrm.Page1.oPag.oDESCAT1_1_6.value=this.w_DESCAT1
    endif
    if not(this.oPgFrm.Page1.oPag.oCODFAM_1_7.value==this.w_CODFAM)
      this.oPgFrm.Page1.oPag.oCODFAM_1_7.value=this.w_CODFAM
    endif
    if not(this.oPgFrm.Page1.oPag.oCODUBI_1_8.value==this.w_CODUBI)
      this.oPgFrm.Page1.oPag.oCODUBI_1_8.value=this.w_CODUBI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATSTA_1_9.value==this.w_DATSTA)
      this.oPgFrm.Page1.oPag.oDATSTA_1_9.value=this.w_DATSTA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFAM_1_10.value==this.w_DESFAM)
      this.oPgFrm.Page1.oPag.oDESFAM_1_10.value=this.w_DESFAM
    endif
    if not(this.oPgFrm.Page1.oPag.oSTAMPADES_1_11.RadioValue()==this.w_STAMPADES)
      this.oPgFrm.Page1.oPag.oSTAMPADES_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESUBI_1_17.value==this.w_DESUBI)
      this.oPgFrm.Page1.oPag.oDESUBI_1_17.value=this.w_DESUBI
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DATSTA = this.w_DATSTA
    return

enddefine

* --- Define pages as container
define class tgsce_kifPag1 as StdContainer
  Width  = 569
  height = 237
  stdWidth  = 569
  stdheight = 237
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCATINI_1_3 as StdField with uid="FRMWIRUYQJ",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CATINI", cQueryName = "CATINI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Categoria dei cespiti",;
    HelpContextID = 239353894,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=135, Top=15, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAT_CESP", cZoomOnZoom="GSCE_ACT", oKey_1_1="CCCODICE", oKey_1_2="this.w_CATINI"

  func oCATINI_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATINI_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATINI_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_CESP','*','CCCODICE',cp_AbsName(this.parent,'oCATINI_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCE_ACT',"Categorie",'',this.parent.oContained
  endproc
  proc oCATINI_1_3.mZoomOnZoom
    local i_obj
    i_obj=GSCE_ACT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_CATINI
     i_obj.ecpSave()
  endproc

  add object oDESCAT_1_4 as StdField with uid="XKTNCEADTU",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DESCAT", cQueryName = "DESCAT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 141440054,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=264, Top=15, InputMask=replicate('X',40)

  add object oCATFIN_1_5 as StdField with uid="HDXYEMZFTM",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CATFIN", cQueryName = "CATFIN",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Categoria dei cespiti",;
    HelpContextID = 49365030,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=135, Top=43, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAT_CESP", cZoomOnZoom="GSCE_ACT", oKey_1_1="CCCODICE", oKey_1_2="this.w_CATFIN"

  func oCATFIN_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATFIN_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATFIN_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_CESP','*','CCCODICE',cp_AbsName(this.parent,'oCATFIN_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCE_ACT',"Categorie",'',this.parent.oContained
  endproc
  proc oCATFIN_1_5.mZoomOnZoom
    local i_obj
    i_obj=GSCE_ACT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_CATFIN
     i_obj.ecpSave()
  endproc

  add object oDESCAT1_1_6 as StdField with uid="OBQATKSNWW",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DESCAT1", cQueryName = "DESCAT1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 141440054,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=264, Top=43, InputMask=replicate('X',40)

  add object oCODFAM_1_7 as StdField with uid="VZNJMNFQUV",rtseq=5,rtrep=.f.,;
    cFormVar = "w_CODFAM", cQueryName = "CODFAM",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Famiglia di appartenenza dei cespiti",;
    HelpContextID = 24137254,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=135, Top=71, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="FAM_CESP", cZoomOnZoom="GSCE_AFC", oKey_1_1="FACODICE", oKey_1_2="this.w_CODFAM"

  func oCODFAM_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODFAM_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODFAM_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_CESP','*','FACODICE',cp_AbsName(this.parent,'oCODFAM_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCE_AFC',"Categorie",'',this.parent.oContained
  endproc
  proc oCODFAM_1_7.mZoomOnZoom
    local i_obj
    i_obj=GSCE_AFC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_FACODICE=this.parent.oContained.w_CODFAM
     i_obj.ecpSave()
  endproc

  add object oCODUBI_1_8 as StdField with uid="UMETFNDEQO",rtseq=6,rtrep=.f.,;
    cFormVar = "w_CODUBI", cQueryName = "CODUBI",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice ubicazione cespite selezionato",;
    HelpContextID = 227495462,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=135, Top=99, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="UBI_CESP", cZoomOnZoom="GSCE_AUB", oKey_1_1="UBCODICE", oKey_1_2="this.w_CODUBI"

  func oCODUBI_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODUBI_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODUBI_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'UBI_CESP','*','UBCODICE',cp_AbsName(this.parent,'oCODUBI_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCE_AUB',"Elenco ubicazioni cespiti",'',this.parent.oContained
  endproc
  proc oCODUBI_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSCE_AUB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_UBCODICE=this.parent.oContained.w_CODUBI
     i_obj.ecpSave()
  endproc

  add object oDATSTA_1_9 as StdField with uid="NTPHNKOGLQ",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DATSTA", cQueryName = "DATSTA",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Stampa solo i cespiti ancora attivi alla data di elaborazione",;
    HelpContextID = 112082998,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=135, Top=127

  add object oDESFAM_1_10 as StdField with uid="JOWCBXEOCB",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DESFAM", cQueryName = "DESFAM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 24196150,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=264, Top=71, InputMask=replicate('X',35)

  add object oSTAMPADES_1_11 as StdCheck with uid="KYSZZXGUNH",rtseq=9,rtrep=.f.,left=285, top=127, caption="Descr. aggiuntiva",;
    ToolTipText = "Stampa descrizione aggiuntiva presente sui cespiti",;
    HelpContextID = 107424155,;
    cFormVar="w_STAMPADES", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSTAMPADES_1_11.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oSTAMPADES_1_11.GetRadio()
    this.Parent.oContained.w_STAMPADES = this.RadioValue()
    return .t.
  endfunc

  func oSTAMPADES_1_11.SetRadio()
    this.Parent.oContained.w_STAMPADES=trim(this.Parent.oContained.w_STAMPADES)
    this.value = ;
      iif(this.Parent.oContained.w_STAMPADES=='S',1,;
      0)
  endfunc


  add object oObj_1_14 as cp_outputCombo with uid="ZLERXIVPWT",left=134, top=157, width=399,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 179421670


  add object oBtn_1_15 as StdButton with uid="ULOUTAIBXP",left=462, top=187, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 143213606;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_15.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_15.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oBtn_1_16 as StdButton with uid="UIWYYWPXJO",left=513, top=187, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 259694010;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_16.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESUBI_1_17 as StdField with uid="IXHINNSOVP",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DESUBI", cQueryName = "DESUBI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione ubicazione",;
    HelpContextID = 227554358,;
   bGlobalFont=.t.,;
    Height=21, Width=278, Left=285, Top=99, InputMask=replicate('X',40)

  add object oStr_1_1 as StdString with uid="VXOCINIWTI",Visible=.t., Left=6, Top=157,;
    Alignment=1, Width=128, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_2 as StdString with uid="NTMRMXCNGY",Visible=.t., Left=6, Top=16,;
    Alignment=1, Width=128, Height=15,;
    Caption="Da categoria:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="IYLFEGVLTN",Visible=.t., Left=6, Top=72,;
    Alignment=1, Width=128, Height=15,;
    Caption="Famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="VPAFDGFCRP",Visible=.t., Left=6, Top=44,;
    Alignment=1, Width=128, Height=15,;
    Caption="A categoria:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="KAJNUODAOO",Visible=.t., Left=6, Top=100,;
    Alignment=1, Width=128, Height=15,;
    Caption="Ubicazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="MPRKOYOFIG",Visible=.t., Left=6, Top=130,;
    Alignment=1, Width=128, Height=18,;
    Caption="Data di elaborazione:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsce_kif','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
