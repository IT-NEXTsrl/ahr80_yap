* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_kce                                                        *
*              Stampa cespiti                                                  *
*                                                                              *
*      Author: TAM Software srl                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_15]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-02-24                                                      *
* Last revis.: 2008-09-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsce_kce",oParentObject))

* --- Class definition
define class tgsce_kce as StdForm
  Top    = 50
  Left   = 100

  * --- Standard Properties
  Width  = 563
  Height = 264
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-09-15"
  HelpContextID=99239273
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=14

  * --- Constant Properties
  _IDX = 0
  CAT_CESP_IDX = 0
  GRU_COCE_IDX = 0
  CES_PITI_IDX = 0
  FAM_CESP_IDX = 0
  AZIENDA_IDX = 0
  UBI_CESP_IDX = 0
  cPrg = "gsce_kce"
  cComment = "Stampa cespiti"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CATEG = space(15)
  o_CATEG = space(15)
  w_DESCAT = space(40)
  w_CODFAM = space(15)
  w_DESFAM = space(35)
  w_SELINI = space(20)
  w_DESINI = space(40)
  w_CATINI = space(15)
  w_SELFIN = space(20)
  w_DESFIN = space(40)
  w_CODUBI = space(20)
  w_CATFIN = space(15)
  w_DATSTAM = ctod('  /  /  ')
  w_OBSODAT1 = space(1)
  w_DESUBI = space(40)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsce_kcePag1","gsce_kce",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCATEG_1_6
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='CAT_CESP'
    this.cWorkTables[2]='GRU_COCE'
    this.cWorkTables[3]='CES_PITI'
    this.cWorkTables[4]='FAM_CESP'
    this.cWorkTables[5]='AZIENDA'
    this.cWorkTables[6]='UBI_CESP'
    return(this.OpenAllTables(6))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      vx_exec(""+alltrim(THIS.w_OQRY)+", "+alltrim(THIS.w_orep)+"",this)
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CATEG=space(15)
      .w_DESCAT=space(40)
      .w_CODFAM=space(15)
      .w_DESFAM=space(35)
      .w_SELINI=space(20)
      .w_DESINI=space(40)
      .w_CATINI=space(15)
      .w_SELFIN=space(20)
      .w_DESFIN=space(40)
      .w_CODUBI=space(20)
      .w_CATFIN=space(15)
      .w_DATSTAM=ctod("  /  /  ")
      .w_OBSODAT1=space(1)
      .w_DESUBI=space(40)
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CATEG))
          .link_1_6('Full')
        endif
        .DoRTCalc(2,3,.f.)
        if not(empty(.w_CODFAM))
          .link_1_8('Full')
        endif
          .DoRTCalc(4,4,.f.)
        .w_SELINI = space(20)
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_SELINI))
          .link_1_10('Full')
        endif
          .DoRTCalc(6,7,.f.)
        .w_SELFIN = space(20)
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_SELFIN))
          .link_1_13('Full')
        endif
        .DoRTCalc(9,10,.f.)
        if not(empty(.w_CODUBI))
          .link_1_15('Full')
        endif
          .DoRTCalc(11,11,.f.)
        .w_DATSTAM = i_datsys
        .w_OBSODAT1 = 'N'
      .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
    endwith
    this.DoRTCalc(14,14,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_23.enabled = this.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_24.enabled = this.oPgFrm.Page1.oPag.oBtn_1_24.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,4,.t.)
        if .o_CATEG<>.w_CATEG
            .w_SELINI = space(20)
          .link_1_10('Full')
        endif
        .DoRTCalc(6,7,.t.)
        if .o_CATEG<>.w_CATEG
            .w_SELFIN = space(20)
          .link_1_13('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(9,14,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_23.enabled = this.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_22.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CATEG
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_CESP_IDX,3]
    i_lTable = "CAT_CESP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_CESP_IDX,2], .t., this.CAT_CESP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_CESP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATEG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCE_ACT',True,'CAT_CESP')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_CATEG)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_CATEG))
          select CCCODICE,CCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATEG)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStrODBC(trim(this.w_CATEG)+"%");

            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStr(trim(this.w_CATEG)+"%");

            select CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CATEG) and !this.bDontReportError
            deferred_cp_zoom('CAT_CESP','*','CCCODICE',cp_AbsName(oSource.parent,'oCATEG_1_6'),i_cWhere,'GSCE_ACT',"Categorie",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATEG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_CATEG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_CATEG)
            select CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATEG = NVL(_Link_.CCCODICE,space(15))
      this.w_DESCAT = NVL(_Link_.CCDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CATEG = space(15)
      endif
      this.w_DESCAT = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_CESP_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_CESP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATEG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFAM
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_CESP_IDX,3]
    i_lTable = "FAM_CESP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_CESP_IDX,2], .t., this.FAM_CESP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_CESP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFAM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCE_AFC',True,'FAM_CESP')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_CODFAM)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_CODFAM))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODFAM)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" FADESCRI like "+cp_ToStrODBC(trim(this.w_CODFAM)+"%");

            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" FADESCRI like "+cp_ToStr(trim(this.w_CODFAM)+"%");

            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODFAM) and !this.bDontReportError
            deferred_cp_zoom('FAM_CESP','*','FACODICE',cp_AbsName(oSource.parent,'oCODFAM_1_8'),i_cWhere,'GSCE_AFC',"Famiglie cespiti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFAM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_CODFAM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_CODFAM)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFAM = NVL(_Link_.FACODICE,space(15))
      this.w_DESFAM = NVL(_Link_.FADESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODFAM = space(15)
      endif
      this.w_DESFAM = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_CESP_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_CESP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFAM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SELINI
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CES_PITI_IDX,3]
    i_lTable = "CES_PITI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CES_PITI_IDX,2], .t., this.CES_PITI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CES_PITI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SELINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCE_ACE',True,'CES_PITI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CECODICE like "+cp_ToStrODBC(trim(this.w_SELINI)+"%");

          i_ret=cp_SQL(i_nConn,"select CECODICE,CEDESCRI,CECODCAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CECODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CECODICE',trim(this.w_SELINI))
          select CECODICE,CEDESCRI,CECODCAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CECODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SELINI)==trim(_Link_.CECODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CEDESCRI like "+cp_ToStrODBC(trim(this.w_SELINI)+"%");

            i_ret=cp_SQL(i_nConn,"select CECODICE,CEDESCRI,CECODCAT";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CEDESCRI like "+cp_ToStr(trim(this.w_SELINI)+"%");

            select CECODICE,CEDESCRI,CECODCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_SELINI) and !this.bDontReportError
            deferred_cp_zoom('CES_PITI','*','CECODICE',cp_AbsName(oSource.parent,'oSELINI_1_10'),i_cWhere,'GSCE_ACE',"Cespiti",'GSCE_KCE.CES_PITI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CECODICE,CEDESCRI,CECODCAT";
                     +" from "+i_cTable+" "+i_lTable+" where CECODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CECODICE',oSource.xKey(1))
            select CECODICE,CEDESCRI,CECODCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SELINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CECODICE,CEDESCRI,CECODCAT";
                   +" from "+i_cTable+" "+i_lTable+" where CECODICE="+cp_ToStrODBC(this.w_SELINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CECODICE',this.w_SELINI)
            select CECODICE,CEDESCRI,CECODCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SELINI = NVL(_Link_.CECODICE,space(20))
      this.w_DESINI = NVL(_Link_.CEDESCRI,space(40))
      this.w_CATINI = NVL(_Link_.CECODCAT,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_SELINI = space(20)
      endif
      this.w_DESINI = space(40)
      this.w_CATINI = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_CATEG = .w_CATINI OR empty(.w_CATEG)) AND (.w_SELINI <= .w_SELFIN OR empty(.w_SELFIN))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Cespite non congruente alle selezioni impostate")
        endif
        this.w_SELINI = space(20)
        this.w_DESINI = space(40)
        this.w_CATINI = space(15)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CES_PITI_IDX,2])+'\'+cp_ToStr(_Link_.CECODICE,1)
      cp_ShowWarn(i_cKey,this.CES_PITI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SELINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SELFIN
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CES_PITI_IDX,3]
    i_lTable = "CES_PITI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CES_PITI_IDX,2], .t., this.CES_PITI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CES_PITI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SELFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCE_ACE',True,'CES_PITI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CECODICE like "+cp_ToStrODBC(trim(this.w_SELFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select CECODICE,CEDESCRI,CECODCAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CECODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CECODICE',trim(this.w_SELFIN))
          select CECODICE,CEDESCRI,CECODCAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CECODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SELFIN)==trim(_Link_.CECODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CEDESCRI like "+cp_ToStrODBC(trim(this.w_SELFIN)+"%");

            i_ret=cp_SQL(i_nConn,"select CECODICE,CEDESCRI,CECODCAT";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CEDESCRI like "+cp_ToStr(trim(this.w_SELFIN)+"%");

            select CECODICE,CEDESCRI,CECODCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_SELFIN) and !this.bDontReportError
            deferred_cp_zoom('CES_PITI','*','CECODICE',cp_AbsName(oSource.parent,'oSELFIN_1_13'),i_cWhere,'GSCE_ACE',"",'GSCE_KCE.CES_PITI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CECODICE,CEDESCRI,CECODCAT";
                     +" from "+i_cTable+" "+i_lTable+" where CECODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CECODICE',oSource.xKey(1))
            select CECODICE,CEDESCRI,CECODCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SELFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CECODICE,CEDESCRI,CECODCAT";
                   +" from "+i_cTable+" "+i_lTable+" where CECODICE="+cp_ToStrODBC(this.w_SELFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CECODICE',this.w_SELFIN)
            select CECODICE,CEDESCRI,CECODCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SELFIN = NVL(_Link_.CECODICE,space(20))
      this.w_DESFIN = NVL(_Link_.CEDESCRI,space(40))
      this.w_CATFIN = NVL(_Link_.CECODCAT,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_SELFIN = space(20)
      endif
      this.w_DESFIN = space(40)
      this.w_CATFIN = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_CATEG = .w_CATFIN OR empty(.w_CATEG)) AND (.w_SELINI <= .w_SELFIN OR empty(.w_SELINI))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Cespite non congruente alle selezioni impostate")
        endif
        this.w_SELFIN = space(20)
        this.w_DESFIN = space(40)
        this.w_CATFIN = space(15)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CES_PITI_IDX,2])+'\'+cp_ToStr(_Link_.CECODICE,1)
      cp_ShowWarn(i_cKey,this.CES_PITI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SELFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODUBI
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UBI_CESP_IDX,3]
    i_lTable = "UBI_CESP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UBI_CESP_IDX,2], .t., this.UBI_CESP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UBI_CESP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODUBI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCE_AUB',True,'UBI_CESP')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UBCODICE like "+cp_ToStrODBC(trim(this.w_CODUBI)+"%");

          i_ret=cp_SQL(i_nConn,"select UBCODICE,UBDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UBCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UBCODICE',trim(this.w_CODUBI))
          select UBCODICE,UBDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UBCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODUBI)==trim(_Link_.UBCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" UBDESCRI like "+cp_ToStrODBC(trim(this.w_CODUBI)+"%");

            i_ret=cp_SQL(i_nConn,"select UBCODICE,UBDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" UBDESCRI like "+cp_ToStr(trim(this.w_CODUBI)+"%");

            select UBCODICE,UBDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODUBI) and !this.bDontReportError
            deferred_cp_zoom('UBI_CESP','*','UBCODICE',cp_AbsName(oSource.parent,'oCODUBI_1_15'),i_cWhere,'GSCE_AUB',"Elenco ubicazioni cespiti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODICE,UBDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where UBCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UBCODICE',oSource.xKey(1))
            select UBCODICE,UBDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODUBI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODICE,UBDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where UBCODICE="+cp_ToStrODBC(this.w_CODUBI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UBCODICE',this.w_CODUBI)
            select UBCODICE,UBDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODUBI = NVL(_Link_.UBCODICE,space(20))
      this.w_DESUBI = NVL(_Link_.UBDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODUBI = space(20)
      endif
      this.w_DESUBI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UBI_CESP_IDX,2])+'\'+cp_ToStr(_Link_.UBCODICE,1)
      cp_ShowWarn(i_cKey,this.UBI_CESP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODUBI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCATEG_1_6.value==this.w_CATEG)
      this.oPgFrm.Page1.oPag.oCATEG_1_6.value=this.w_CATEG
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAT_1_7.value==this.w_DESCAT)
      this.oPgFrm.Page1.oPag.oDESCAT_1_7.value=this.w_DESCAT
    endif
    if not(this.oPgFrm.Page1.oPag.oCODFAM_1_8.value==this.w_CODFAM)
      this.oPgFrm.Page1.oPag.oCODFAM_1_8.value=this.w_CODFAM
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFAM_1_9.value==this.w_DESFAM)
      this.oPgFrm.Page1.oPag.oDESFAM_1_9.value=this.w_DESFAM
    endif
    if not(this.oPgFrm.Page1.oPag.oSELINI_1_10.value==this.w_SELINI)
      this.oPgFrm.Page1.oPag.oSELINI_1_10.value=this.w_SELINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESINI_1_11.value==this.w_DESINI)
      this.oPgFrm.Page1.oPag.oDESINI_1_11.value=this.w_DESINI
    endif
    if not(this.oPgFrm.Page1.oPag.oSELFIN_1_13.value==this.w_SELFIN)
      this.oPgFrm.Page1.oPag.oSELFIN_1_13.value=this.w_SELFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFIN_1_14.value==this.w_DESFIN)
      this.oPgFrm.Page1.oPag.oDESFIN_1_14.value=this.w_DESFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCODUBI_1_15.value==this.w_CODUBI)
      this.oPgFrm.Page1.oPag.oCODUBI_1_15.value=this.w_CODUBI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATSTAM_1_17.value==this.w_DATSTAM)
      this.oPgFrm.Page1.oPag.oDATSTAM_1_17.value=this.w_DATSTAM
    endif
    if not(this.oPgFrm.Page1.oPag.oOBSODAT1_1_18.RadioValue()==this.w_OBSODAT1)
      this.oPgFrm.Page1.oPag.oOBSODAT1_1_18.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESUBI_1_20.value==this.w_DESUBI)
      this.oPgFrm.Page1.oPag.oDESUBI_1_20.value=this.w_DESUBI
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((.w_CATEG = .w_CATINI OR empty(.w_CATEG)) AND (.w_SELINI <= .w_SELFIN OR empty(.w_SELFIN)))  and not(empty(.w_SELINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSELINI_1_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Cespite non congruente alle selezioni impostate")
          case   not((.w_CATEG = .w_CATFIN OR empty(.w_CATEG)) AND (.w_SELINI <= .w_SELFIN OR empty(.w_SELINI)))  and not(empty(.w_SELFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSELFIN_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Cespite non congruente alle selezioni impostate")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CATEG = this.w_CATEG
    return

enddefine

* --- Define pages as container
define class tgsce_kcePag1 as StdContainer
  Width  = 559
  height = 264
  stdWidth  = 559
  stdheight = 264
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCATEG_1_6 as StdField with uid="FQNYPFCKVP",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CATEG", cQueryName = "CATEG",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Categoria cespiti selezionata",;
    HelpContextID = 19906522,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=103, Top=16, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAT_CESP", cZoomOnZoom="GSCE_ACT", oKey_1_1="CCCODICE", oKey_1_2="this.w_CATEG"

  func oCATEG_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATEG_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATEG_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_CESP','*','CCCODICE',cp_AbsName(this.parent,'oCATEG_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCE_ACT',"Categorie",'',this.parent.oContained
  endproc
  proc oCATEG_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSCE_ACT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_CATEG
     i_obj.ecpSave()
  endproc

  add object oDESCAT_1_7 as StdField with uid="UKFKPLRHFB",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DESCAT", cQueryName = "DESCAT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 40776758,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=261, Top=16, InputMask=replicate('X',40)

  add object oCODFAM_1_8 as StdField with uid="TIKCKEUOZC",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CODFAM", cQueryName = "CODFAM",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Famiglia di appartenenza dei cespiti selezionata",;
    HelpContextID = 191909414,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=103, Top=43, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="FAM_CESP", cZoomOnZoom="GSCE_AFC", oKey_1_1="FACODICE", oKey_1_2="this.w_CODFAM"

  func oCODFAM_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODFAM_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODFAM_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_CESP','*','FACODICE',cp_AbsName(this.parent,'oCODFAM_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCE_AFC',"Famiglie cespiti",'',this.parent.oContained
  endproc
  proc oCODFAM_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSCE_AFC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_FACODICE=this.parent.oContained.w_CODFAM
     i_obj.ecpSave()
  endproc

  add object oDESFAM_1_9 as StdField with uid="VGPVUIFSEP",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DESFAM", cQueryName = "DESFAM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 191968310,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=261, Top=43, InputMask=replicate('X',35)

  add object oSELINI_1_10 as StdField with uid="DKVXYHLIEH",rtseq=5,rtrep=.f.,;
    cFormVar = "w_SELINI", cQueryName = "SELINI",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Cespite non congruente alle selezioni impostate",;
    ToolTipText = "Codice cespite di inizio selezione",;
    HelpContextID = 138659110,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=103, Top=70, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="CES_PITI", cZoomOnZoom="GSCE_ACE", oKey_1_1="CECODICE", oKey_1_2="this.w_SELINI"

  func oSELINI_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oSELINI_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSELINI_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CES_PITI','*','CECODICE',cp_AbsName(this.parent,'oSELINI_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCE_ACE',"Cespiti",'GSCE_KCE.CES_PITI_VZM',this.parent.oContained
  endproc
  proc oSELINI_1_10.mZoomOnZoom
    local i_obj
    i_obj=GSCE_ACE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CECODICE=this.parent.oContained.w_SELINI
     i_obj.ecpSave()
  endproc

  add object oDESINI_1_11 as StdField with uid="KRPJDBOKOQ",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DESINI", cQueryName = "DESINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 138687542,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=261, Top=70, InputMask=replicate('X',40)

  add object oSELFIN_1_13 as StdField with uid="LDIMVCDXMP",rtseq=8,rtrep=.f.,;
    cFormVar = "w_SELFIN", cQueryName = "SELFIN",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Cespite non congruente alle selezioni impostate",;
    ToolTipText = "Codice cespite di fine selezione",;
    HelpContextID = 217105702,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=103, Top=97, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="CES_PITI", cZoomOnZoom="GSCE_ACE", oKey_1_1="CECODICE", oKey_1_2="this.w_SELFIN"

  func oSELFIN_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oSELFIN_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSELFIN_1_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CES_PITI','*','CECODICE',cp_AbsName(this.parent,'oSELFIN_1_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCE_ACE',"",'GSCE_KCE.CES_PITI_VZM',this.parent.oContained
  endproc
  proc oSELFIN_1_13.mZoomOnZoom
    local i_obj
    i_obj=GSCE_ACE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CECODICE=this.parent.oContained.w_SELFIN
     i_obj.ecpSave()
  endproc

  add object oDESFIN_1_14 as StdField with uid="JPQTOSABPC",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DESFIN", cQueryName = "DESFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 217134134,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=261, Top=97, InputMask=replicate('X',40)

  add object oCODUBI_1_15 as StdField with uid="UMETFNDEQO",rtseq=10,rtrep=.f.,;
    cFormVar = "w_CODUBI", cQueryName = "CODUBI",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice ubicazione cespite selezionato",;
    HelpContextID = 126832166,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=103, Top=124, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="UBI_CESP", cZoomOnZoom="GSCE_AUB", oKey_1_1="UBCODICE", oKey_1_2="this.w_CODUBI"

  func oCODUBI_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODUBI_1_15.ecpDrop(oSource)
    this.Parent.oContained.link_1_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODUBI_1_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'UBI_CESP','*','UBCODICE',cp_AbsName(this.parent,'oCODUBI_1_15'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCE_AUB',"Elenco ubicazioni cespiti",'',this.parent.oContained
  endproc
  proc oCODUBI_1_15.mZoomOnZoom
    local i_obj
    i_obj=GSCE_AUB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_UBCODICE=this.parent.oContained.w_CODUBI
     i_obj.ecpSave()
  endproc

  add object oDATSTAM_1_17 as StdField with uid="RKKNOARBRU",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DATSTAM", cQueryName = "DATSTAM",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di riferimento sul controllo dell'obsolescenza",;
    HelpContextID = 257015754,;
   bGlobalFont=.t.,;
    Height=21, Width=75, Left=103, Top=156

  add object oOBSODAT1_1_18 as StdCheck with uid="QNMPBLICOI",rtseq=13,rtrep=.f.,left=202, top=156, caption="Stampa obsoleti",;
    ToolTipText = "Stampa solo obsoleti alla data di stampa",;
    HelpContextID = 262812183,;
    cFormVar="w_OBSODAT1", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oOBSODAT1_1_18.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oOBSODAT1_1_18.GetRadio()
    this.Parent.oContained.w_OBSODAT1 = this.RadioValue()
    return .t.
  endfunc

  func oOBSODAT1_1_18.SetRadio()
    this.Parent.oContained.w_OBSODAT1=trim(this.Parent.oContained.w_OBSODAT1)
    this.value = ;
      iif(this.Parent.oContained.w_OBSODAT1=='S',1,;
      0)
  endfunc

  add object oDESUBI_1_20 as StdField with uid="IXHINNSOVP",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DESUBI", cQueryName = "DESUBI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione ubicazione",;
    HelpContextID = 126891062,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=261, Top=124, InputMask=replicate('X',40)


  add object oObj_1_22 as cp_outputCombo with uid="ZLERXIVPWT",left=103, top=187, width=382,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 78758374


  add object oBtn_1_23 as StdButton with uid="KFNKLGXOTF",left=452, top=214, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 42550310;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_23.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_23.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY) and not empty(.w_datstam))
      endwith
    endif
  endfunc


  add object oBtn_1_24 as StdButton with uid="VPCHBYKXOV",left=502, top=214, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 91921850;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_24.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_1 as StdString with uid="WIJLKFTBDZ",Visible=.t., Left=6, Top=156,;
    Alignment=1, Width=96, Height=15,;
    Caption="Data di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_2 as StdString with uid="QAMEXYTCED",Visible=.t., Left=6, Top=187,;
    Alignment=1, Width=96, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_3 as StdString with uid="PZHJBYNAFU",Visible=.t., Left=6, Top=71,;
    Alignment=1, Width=96, Height=15,;
    Caption="Da cespite:"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="XOYEOYDLID",Visible=.t., Left=6, Top=98,;
    Alignment=1, Width=96, Height=15,;
    Caption="A cespite:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="JFMSIYYQZN",Visible=.t., Left=6, Top=17,;
    Alignment=1, Width=96, Height=15,;
    Caption="Categoria:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="EHTNTNATJI",Visible=.t., Left=6, Top=44,;
    Alignment=1, Width=96, Height=15,;
    Caption="Famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="KAJNUODAOO",Visible=.t., Left=6, Top=125,;
    Alignment=1, Width=96, Height=15,;
    Caption="Ubicazione:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsce_kce','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
