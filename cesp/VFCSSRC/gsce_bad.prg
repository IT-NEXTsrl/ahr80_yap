* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_bad                                                        *
*              Elimina piano di ammortamento                                   *
*                                                                              *
*      Author: Zucchetti spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][207]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-03-19                                                      *
* Last revis.: 1999-08-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsce_bad",oParentObject)
return(i_retval)

define class tgsce_bad as StdBatch
  * --- Local variables
  w_MESS = space(10)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elimina il Dettaglio Piano di Ammortamento (da GSCE_AAC)
    if NOT EMPTY(this.oParentObject.w_ACRIFMOV)
      * --- Se generato Movimento da Accantonamento lo Elimina
      this.w_MESS = AH_MSGFORMAT ("La registrazione ha generato un movimento cespite%0Impossibile variare o eliminare %0Operare direttamente sul piano di ammortamento")
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=this.w_MESS
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
