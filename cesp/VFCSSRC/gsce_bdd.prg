* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_bdd                                                        *
*              Aggiorna data dismissione                                       *
*                                                                              *
*      Author: Zucchetti spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][207]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-07-20                                                      *
* Last revis.: 2000-07-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsce_bdd",oParentObject)
return(i_retval)

define class tgsce_bdd as StdBatch
  * --- Local variables
  w_OK = .f.
  w_CODICE = space(20)
  w_DATAREG = ctod("  /  /  ")
  w_oMESS = .NULL.
  * --- WorkFile variables
  CES_PITI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questa procedura permette di aggiornare le date di dismissione
    * --- nei cespiti che risultano 'CEDUTI'
    this.w_oMESS=createobject("AH_Message")
    this.w_oMESS.AddMsgPartNL("Questa funzione permette l'aggiornamento del campo data dismissione aggiunto nell'anagrafica cespiti")     
    this.w_oMESS.AddMsgPartNL("Il valore assegnato al campo dipende dalla data di registrazione del movimento di vendita")     
    this.w_oMESS.AddMsgPartNL("Confermi l'aggiornamento?")     
    this.w_OK = this.w_oMESS.ah_YESNO()
    if this.w_OK
      vq_exec("..\CESP\EXE\QUERY\GSCE_BDD",this,"DATADIS")
      if used("datadis")
        =WRCURSOR("DATADIS")
        SELECT DATADIS
        GO TOP
        SCAN FOR NOT EMPTY(CP_TODATE(MCDATREG))
        this.w_CODICE = NVL(CECODICE,"")
        this.w_DATAREG = CP_TODATE(MCDATREG)
        * --- Scrivo la data di dismissione nell'anagrafica dei cespiti.
        * --- Write into CES_PITI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.CES_PITI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CES_PITI_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CES_PITI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CEDATDIS ="+cp_NullLink(cp_ToStrODBC(this.w_DATAREG),'CES_PITI','CEDATDIS');
              +i_ccchkf ;
          +" where ";
              +"CECODICE = "+cp_ToStrODBC(this.w_CODICE);
                 )
        else
          update (i_cTable) set;
              CEDATDIS = this.w_DATAREG;
              &i_ccchkf. ;
           where;
              CECODICE = this.w_CODICE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        ENDSCAN
        ah_ERRORMSG("Aggiornamento completato",64)
      else
        ah_ERRORMSG("Non esistono cespiti ceduti",48)
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='CES_PITI'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
