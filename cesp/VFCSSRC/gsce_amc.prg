* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_amc                                                        *
*              Movimenti cespiti                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30] [VRS_329]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-02-23                                                      *
* Last revis.: 2015-01-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsce_amc"))

* --- Class definition
define class tgsce_amc as StdForm
  Top    = 0
  Left   = 6

  * --- Standard Properties
  Width  = 806
  Height = 404+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-01-13"
  HelpContextID=210388329
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=202

  * --- Constant Properties
  MOV_CESP_IDX = 0
  CES_PITI_IDX = 0
  VALUTE_IDX = 0
  CAU_CESP_IDX = 0
  ESERCIZI_IDX = 0
  CONTI_IDX = 0
  SAL_CESP_IDX = 0
  MOV_PCES_IDX = 0
  MOV_SCES_IDX = 0
  CENCOST_IDX = 0
  VOC_COST_IDX = 0
  CAN_TIER_IDX = 0
  CAU_CONT_IDX = 0
  PNT_MAST_IDX = 0
  UNIMIS_IDX = 0
  PAR_CESP_IDX = 0
  AMM_CESP_IDX = 0
  cFile = "MOV_CESP"
  cKeySelect = "MCSERIAL"
  cKeyWhere  = "MCSERIAL=this.w_MCSERIAL"
  cKeyWhereODBC = '"MCSERIAL="+cp_ToStrODBC(this.w_MCSERIAL)';

  cKeyWhereODBCqualified = '"MOV_CESP.MCSERIAL="+cp_ToStrODBC(this.w_MCSERIAL)';

  cPrg = "gsce_amc"
  cComment = "Movimenti cespiti"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_MCSERIAL = space(10)
  o_MCSERIAL = space(10)
  w_DOCSERIAL = space(10)
  w_PNSERIALM = space(10)
  w_PNSERIALC = space(10)
  w_CODAZI = space(5)
  w_PCTIPAMM = space(1)
  w_MCVALNAZ = space(3)
  w_TIPOPE = space(10)
  w_MCNUMREG = 0
  w_MCCODESE = space(4)
  w_PRIDAT = ctod('  /  /  ')
  w_MCDATREG = ctod('  /  /  ')
  o_MCDATREG = ctod('  /  /  ')
  w_MCNUMDOC = 0
  w_MCALFDOC = space(10)
  w_MCDATDOC = ctod('  /  /  ')
  o_MCDATDOC = ctod('  /  /  ')
  w_FLPRIC = space(1)
  o_FLPRIC = space(1)
  w_MCCODCAU = space(5)
  o_MCCODCAU = space(5)
  w_MCDTPRIC = ctod('  /  /  ')
  o_MCDTPRIC = ctod('  /  /  ')
  w_DESCAU = space(40)
  w_MCDATDIS = ctod('  /  /  ')
  o_MCDATDIS = ctod('  /  /  ')
  w_FLPRIU = space(1)
  o_FLPRIU = space(1)
  w_MCCOMPET = space(4)
  o_MCCOMPET = space(4)
  w_FLDADI = space(1)
  o_FLDADI = space(1)
  w_MCFLPRIU = space(1)
  w_CAUCON = space(5)
  w_FLANAL = space(1)
  w_NFOR = 0
  w_MCDTPRIU = ctod('  /  /  ')
  o_MCDTPRIU = ctod('  /  /  ')
  w_MCCOMPET = space(4)
  w_MCTIPCON = space(1)
  w_FLGA01 = space(1)
  w_FLGA02 = space(1)
  w_FLGA03 = space(1)
  w_FLGA04 = space(1)
  w_FLGA05 = space(1)
  w_FLGA06 = space(1)
  w_FLGA07 = space(1)
  w_FLGA08 = space(1)
  w_FLGA09 = space(1)
  w_FLGA10 = space(1)
  w_FLGA11 = space(1)
  w_FLGA12 = space(1)
  w_FLGA13 = space(1)
  w_FLGA14 = space(1)
  w_FLGA15 = space(1)
  w_FLGA16 = space(1)
  w_MCDESMOV = space(40)
  w_MCDESMO2 = space(40)
  w_MCCODCON = space(15)
  w_DESCON = space(40)
  w_MCCODCES = space(20)
  o_MCCODCES = space(20)
  w_CEESPRIU = space(4)
  w_DESCES = space(40)
  w_STACES = space(1)
  w_TIPCES = space(2)
  w_PERDEF = 0
  w_IMPMAX = 0
  w_MCSTATUS = space(1)
  w_MCVOCCEN = space(15)
  o_MCVOCCEN = space(15)
  w_DESVOC = space(40)
  w_MCCODCEN = space(15)
  o_MCCODCEN = space(15)
  w_DESCEN = space(40)
  w_MCCODCOM = space(15)
  o_MCCODCOM = space(15)
  w_DESCAN = space(30)
  w_TCAR = 0
  w_FLCAR = .F.
  w_FLSCA = .F.
  w_CON1CAU = .F.
  w_CON2CAU = .F.
  w_OBTEST = ctod('  /  /  ')
  w_IMPV01 = 0
  w_IMPV02 = 0
  w_IMPV03 = 0
  w_IMPV04 = 0
  w_IMPV05 = 0
  w_IMPV06 = 0
  w_IMPV07 = 0
  w_IMPV08 = 0
  w_IMPV09 = 0
  w_IMPV10 = 0
  w_IMPV11 = 0
  w_IMPV12 = 0
  w_IMPV13 = 0
  w_IMPV14 = 0
  w_DTOBSO = ctod('  /  /  ')
  w_DTPRIU = ctod('  /  /  ')
  w_COECIV = 0
  w_QUOCIV = 0
  w_COEFI1 = 0
  w_COEFI2 = 0
  w_ABILITA = .F.
  w_QUOFIS = 0
  w_QUOFI1 = 0
  w_FLAUMU = space(1)
  w_STABEN = space(1)
  w_DATAGG = space(1)
  w_INIESE = ctod('  /  /  ')
  w_FINESE = ctod('  /  /  ')
  w_TIPAMM = space(1)
  w_CODESE = space(4)
  w_CODCAT = space(15)
  w_TCOECIV = 0
  w_TCOEFI1 = 0
  w_TCOEFI2 = 0
  w_MCCOMPET = space(4)
  w_FLCONT = space(1)
  w_MCRIFCON = space(10)
  w_MCRIFFAT = space(10)
  w_FLPROV = space(1)
  w_MCDATCON = ctod('  /  /  ')
  w_ORIFCON = space(10)
  w_MCSTABEN = space(1)
  w_DaScartare = .F.
  w_DaGenera = .F.
  w_ALLROW = .F.
  w_FLCEUS = space(1)
  w_OKFISORD = .F.
  w_OKFISANT = .F.
  w_OKCIVILE = .F.
  w_IMPRIV = 0
  w_UNIMIS = space(3)
  w_DESUNI = space(35)
  w_MCQTACES = 0
  w_AMMIMM = space(1)
  w_FLGFRAZ = space(1)
  w_FLGA17 = space(1)
  w_TIPOAGG = space(1)
  w_ORIGINE = space(1)
  w_BLOCCA = .F.
  w_IMQTACES = 0
  w_MCCODVAL = space(3)
  o_MCCODVAL = space(3)
  w_MCCAOVAL = 0
  w_DECTOT = 0
  w_CAOVAL = 0
  w_CALCPIC = 0
  w_MCIMPA01 = 0
  o_MCIMPA01 = 0
  w_MCIMPA23 = 0
  w_MCIMPA02 = 0
  o_MCIMPA02 = 0
  w_MCIMPA24 = 0
  w_MCIMPA03 = 0
  o_MCIMPA03 = 0
  w_MCIMPA18 = 0
  w_MCIMPA04 = 0
  o_MCIMPA04 = 0
  w_MCIMPA22 = 0
  w_MCIMPA05 = 0
  o_MCIMPA05 = 0
  w_MCIMPA19 = 0
  w_MCIMPA06 = 0
  o_MCIMPA06 = 0
  w_MCIMPA07 = 0
  o_MCIMPA07 = 0
  w_CODCAU = space(5)
  w_MCCOECIV = 0
  w_MCIMPA08 = 0
  o_MCIMPA08 = 0
  w_MCIMPA09 = 0
  o_MCIMPA09 = 0
  w_DESCAU = space(40)
  w_MCCOEFIS = 0
  w_MCIMPA10 = 0
  o_MCIMPA10 = 0
  w_MCIMPA15 = 0
  o_MCIMPA15 = 0
  w_CODCES = space(20)
  w_MCCOEFI1 = 0
  w_MCIMPA16 = 0
  o_MCIMPA16 = 0
  w_DESCES = space(40)
  w_MCIMPA11 = 0
  o_MCIMPA11 = 0
  w_MCIMPA12 = 0
  o_MCIMPA12 = 0
  w_MCIMPA20 = 0
  w_MCIMPA13 = 0
  o_MCIMPA13 = 0
  w_MCIMPA21 = 0
  w_MCIMPA14 = 0
  o_MCIMPA14 = 0
  w_SIMVAL = space(5)
  w_RIVALU = space(10)
  w_IMPRIV2 = 0
  w_MCIMPA17 = 0
  w_DTOBSOCA = ctod('  /  /  ')
  w_DTOBSOCL = ctod('  /  /  ')
  w_CEFLAMCI = space(1)
  w_CEPERCIV = 0
  w_CEVALLIM = space(3)
  w_CEDURCES = 0
  w_CODESF = space(4)
  w_MCFLPRIC = space(1)
  w_AMMIMC = space(1)
  w_CCNSOAMM = space(1)
  w_CCNSOAMF = space(1)
  w_CENSOAMM = space(1)
  w_CENSOAMF = space(1)
  w_DTPRIC = ctod('  /  /  ')
  w_FLGA18 = space(1)
  w_FLGA19 = space(1)
  w_FLGA20 = space(1)
  w_FLGA21 = space(1)
  w_FLGA21 = space(1)
  w_FLGA22 = space(1)
  w_FLGA23 = space(1)
  w_FLGA24 = space(1)
  w_IMPV16 = 0
  w_IMPV17 = 0
  w_TCARC = 0
  w_CEESPRIC = space(4)
  w_MCAGGSTA = space(1)
  w_AMTIPAMM = space(1)
  w_CODESC = space(4)
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_MCSERIAL = this.W_MCSERIAL
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_MCCODESE = this.W_MCCODESE
  op_MCNUMREG = this.W_MCNUMREG

  * --- Children pointers
  GSCE_MMC = .NULL.
  GSCE_MMS = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsce_amc
  * Evenutale rifermento maschera documenti( se lanciato da gsve_kda all'f10 chiude la maschera)
  w_OBJMASK=.f.
  
  proc ecpSave()
    DoDefault()
    if type('This.w_OBJMASK.cPrg')='C' And this.cFunction='Load'
     this.ecpquit()
     this.ecpquit()
    Endif
  
  EndProc
  
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'MOV_CESP','gsce_amc')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsce_amcPag1","gsce_amc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Dati generali")
      .Pages(1).HelpContextID = 65595636
      .Pages(2).addobject("oPag","tgsce_amcPag2","gsce_amc",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Importi")
      .Pages(2).HelpContextID = 15979386
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[17]
    this.cWorkTables[1]='CES_PITI'
    this.cWorkTables[2]='VALUTE'
    this.cWorkTables[3]='CAU_CESP'
    this.cWorkTables[4]='ESERCIZI'
    this.cWorkTables[5]='CONTI'
    this.cWorkTables[6]='SAL_CESP'
    this.cWorkTables[7]='MOV_PCES'
    this.cWorkTables[8]='MOV_SCES'
    this.cWorkTables[9]='CENCOST'
    this.cWorkTables[10]='VOC_COST'
    this.cWorkTables[11]='CAN_TIER'
    this.cWorkTables[12]='CAU_CONT'
    this.cWorkTables[13]='PNT_MAST'
    this.cWorkTables[14]='UNIMIS'
    this.cWorkTables[15]='PAR_CESP'
    this.cWorkTables[16]='AMM_CESP'
    this.cWorkTables[17]='MOV_CESP'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(17))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.MOV_CESP_IDX,5],7]
    this.nPostItConn=i_TableProp[this.MOV_CESP_IDX,3]
  return

  function CreateChildren()
    this.GSCE_MMC = CREATEOBJECT('stdLazyChild',this,'GSCE_MMC')
    this.GSCE_MMS = CREATEOBJECT('stdLazyChild',this,'GSCE_MMS')
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSCE_MMC)
      this.GSCE_MMC.DestroyChildrenChain()
      this.GSCE_MMC=.NULL.
    endif
    if !ISNULL(this.GSCE_MMS)
      this.GSCE_MMS.DestroyChildrenChain()
      this.GSCE_MMS=.NULL.
    endif
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSCE_MMC.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSCE_MMS.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSCE_MMC.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSCE_MMS.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSCE_MMC.NewDocument()
    this.GSCE_MMS.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSCE_MMC.SetKey(;
            .w_MCSERIAL,"MCSERIAL";
            )
      this.GSCE_MMS.SetKey(;
            .w_MCSERIAL,"SCSERIAL";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSCE_MMC.ChangeRow(this.cRowID+'      1',1;
             ,.w_MCSERIAL,"MCSERIAL";
             )
      .GSCE_MMS.ChangeRow(this.cRowID+'      1',1;
             ,.w_MCSERIAL,"SCSERIAL";
             )
    endwith
    return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_MCSERIAL = NVL(MCSERIAL,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_18_joined
    link_1_18_joined=.f.
    local link_1_52_joined
    link_1_52_joined=.f.
    local link_1_60_joined
    link_1_60_joined=.f.
    local link_1_62_joined
    link_1_62_joined=.f.
    local link_1_64_joined
    link_1_64_joined=.f.
    local link_2_20_joined
    link_2_20_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from MOV_CESP where MCSERIAL=KeySet.MCSERIAL
    *
    i_nConn = i_TableProp[this.MOV_CESP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOV_CESP_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('MOV_CESP')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "MOV_CESP.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' MOV_CESP '
      link_1_18_joined=this.AddJoinedLink_1_18(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_52_joined=this.AddJoinedLink_1_52(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_60_joined=this.AddJoinedLink_1_60(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_62_joined=this.AddJoinedLink_1_62(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_64_joined=this.AddJoinedLink_1_64(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_20_joined=this.AddJoinedLink_2_20(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'MCSERIAL',this.w_MCSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DOCSERIAL = space(10)
        .w_PNSERIALM = space(10)
        .w_PNSERIALC = space(10)
        .w_CODAZI = i_CODAZI
        .w_PCTIPAMM = space(1)
        .w_PRIDAT = ctod("  /  /  ")
        .w_FLPRIC = space(1)
        .w_DESCAU = space(40)
        .w_FLPRIU = space(1)
        .w_FLDADI = space(1)
        .w_CAUCON = space(5)
        .w_FLANAL = space(1)
        .w_NFOR = 0
        .w_FLGA01 = space(1)
        .w_FLGA02 = space(1)
        .w_FLGA03 = space(1)
        .w_FLGA04 = space(1)
        .w_FLGA05 = space(1)
        .w_FLGA06 = space(1)
        .w_FLGA07 = space(1)
        .w_FLGA08 = space(1)
        .w_FLGA09 = space(1)
        .w_FLGA10 = space(1)
        .w_FLGA11 = space(1)
        .w_FLGA12 = space(1)
        .w_FLGA13 = space(1)
        .w_FLGA14 = space(1)
        .w_FLGA15 = space(1)
        .w_FLGA16 = space(1)
        .w_DESCON = space(40)
        .w_CEESPRIU = space(4)
        .w_DESCES = space(40)
        .w_STACES = space(1)
        .w_TIPCES = space(2)
        .w_PERDEF = 0
        .w_IMPMAX = 0
        .w_DESVOC = space(40)
        .w_DESCEN = space(40)
        .w_DESCAN = space(30)
        .w_OBTEST = i_datsys
        .w_IMPV01 = 0
        .w_IMPV02 = 0
        .w_IMPV03 = 0
        .w_IMPV04 = 0
        .w_IMPV05 = 0
        .w_IMPV06 = 0
        .w_IMPV07 = 0
        .w_IMPV08 = 0
        .w_IMPV09 = 0
        .w_IMPV10 = 0
        .w_IMPV11 = 0
        .w_IMPV12 = 0
        .w_IMPV13 = 0
        .w_IMPV14 = 0
        .w_DTOBSO = ctod("  /  /  ")
        .w_DTPRIU = ctod("  /  /  ")
        .w_COECIV = 0
        .w_QUOCIV = 0
        .w_COEFI1 = 0
        .w_COEFI2 = 0
        .w_ABILITA = .f.
        .w_QUOFIS = 0
        .w_QUOFI1 = 0
        .w_FLAUMU = space(1)
        .w_STABEN = space(1)
        .w_DATAGG = space(1)
        .w_INIESE = ctod("  /  /  ")
        .w_FINESE = ctod("  /  /  ")
        .w_TIPAMM = space(1)
        .w_CODESE = space(4)
        .w_CODCAT = space(15)
        .w_TCOECIV = 0
        .w_TCOEFI1 = 0
        .w_TCOEFI2 = 0
        .w_FLCONT = space(1)
        .w_FLPROV = space(1)
        .w_DaScartare = .f.
        .w_DaGenera = .f.
        .w_ALLROW = .F.
        .w_FLCEUS = space(1)
        .w_OKFISORD = .F.
        .w_OKFISANT = .F.
        .w_OKCIVILE = .F.
        .w_IMPRIV = 0
        .w_UNIMIS = space(3)
        .w_DESUNI = space(35)
        .w_AMMIMM = space(1)
        .w_FLGFRAZ = space(1)
        .w_FLGA17 = space(1)
        .w_TIPOAGG = space(1)
        .w_ORIGINE = space(1)
        .w_BLOCCA = .f.
        .w_IMQTACES = 0
        .w_DECTOT = 0
        .w_DESCAU = space(40)
        .w_DESCES = space(40)
        .w_SIMVAL = space(5)
        .w_RIVALU = space(10)
        .w_IMPRIV2 = 0
        .w_DTOBSOCA = ctod("  /  /  ")
        .w_DTOBSOCL = ctod("  /  /  ")
        .w_CEFLAMCI = space(1)
        .w_CEPERCIV = 0
        .w_CEVALLIM = space(3)
        .w_CEDURCES = 0
        .w_CODESF = space(4)
        .w_AMMIMC = space(1)
        .w_CCNSOAMM = 'N'
        .w_CCNSOAMF = 'N'
        .w_CENSOAMM = space(1)
        .w_CENSOAMF = space(1)
        .w_DTPRIC = ctod("  /  /  ")
        .w_FLGA18 = space(1)
        .w_FLGA19 = space(1)
        .w_FLGA20 = space(1)
        .w_FLGA21 = space(1)
        .w_FLGA21 = space(1)
        .w_FLGA22 = space(1)
        .w_FLGA23 = space(1)
        .w_FLGA24 = space(1)
        .w_IMPV16 = 0
        .w_IMPV17 = 0
        .w_CEESPRIC = space(4)
        .w_AMTIPAMM = space(1)
        .w_CODESC = space(4)
        .w_MCSERIAL = NVL(MCSERIAL,space(10))
        .op_MCSERIAL = .w_MCSERIAL
        .oPgFrm.Page1.oPag.oObj_1_2.Calculate()
          .link_1_6('Load')
        .w_MCVALNAZ = NVL(MCVALNAZ,space(3))
        .w_TIPOPE = this.cFunction
        .w_MCNUMREG = NVL(MCNUMREG,0)
        .op_MCNUMREG = .w_MCNUMREG
        .w_MCCODESE = NVL(MCCODESE,space(4))
        .op_MCCODESE = .w_MCCODESE
          .link_1_11('Load')
        .w_MCDATREG = NVL(cp_ToDate(MCDATREG),ctod("  /  /  "))
        .w_MCNUMDOC = NVL(MCNUMDOC,0)
        .w_MCALFDOC = NVL(MCALFDOC,space(10))
        .w_MCDATDOC = NVL(cp_ToDate(MCDATDOC),ctod("  /  /  "))
        .w_MCCODCAU = NVL(MCCODCAU,space(5))
          if link_1_18_joined
            this.w_MCCODCAU = NVL(CCCODICE118,NVL(this.w_MCCODCAU,space(5)))
            this.w_DESCAU = NVL(CCDESCRI118,space(40))
            this.w_MCTIPCON = NVL(CCFLRIFE118,space(1))
            this.w_FLGA01 = NVL(CCFLGA01118,space(1))
            this.w_FLGA02 = NVL(CCFLGA02118,space(1))
            this.w_FLGA03 = NVL(CCFLGA03118,space(1))
            this.w_FLGA04 = NVL(CCFLGA04118,space(1))
            this.w_FLGA05 = NVL(CCFLGA05118,space(1))
            this.w_FLGA06 = NVL(CCFLGA06118,space(1))
            this.w_FLGA07 = NVL(CCFLGA07118,space(1))
            this.w_FLGA08 = NVL(CCFLGA08118,space(1))
            this.w_FLGA09 = NVL(CCFLGA09118,space(1))
            this.w_FLGA10 = NVL(CCFLGA10118,space(1))
            this.w_FLGA11 = NVL(CCFLGA11118,space(1))
            this.w_FLGA12 = NVL(CCFLGA12118,space(1))
            this.w_FLGA13 = NVL(CCFLGA13118,space(1))
            this.w_FLGA14 = NVL(CCFLGA14118,space(1))
            this.w_DATAGG = NVL(CCDATAGG118,space(1))
            this.w_CAUCON = NVL(CCCAUCON118,space(5))
            this.w_FLPRIU = NVL(CCFLPRIU118,space(1))
            this.w_FLCONT = NVL(CCFLCONT118,space(1))
            this.w_FLGA15 = NVL(CCFLGA15118,space(1))
            this.w_FLGA16 = NVL(CCFLGA16118,space(1))
            this.w_FLDADI = NVL(CCFLDADI118,space(1))
            this.w_FLGA17 = NVL(CCFLGA17118,space(1))
            this.w_TIPOAGG = NVL(CCFLGOPE118,space(1))
            this.w_RIVALU = NVL(CCRIVALU118,space(10))
            this.w_DTOBSOCA = NVL(cp_ToDate(CCDTOBSO118),ctod("  /  /  "))
            this.w_FLGA18 = NVL(CCFLGA18118,space(1))
            this.w_FLGA19 = NVL(CCFLGA19118,space(1))
            this.w_FLGA20 = NVL(CCFLGA20118,space(1))
            this.w_FLGA21 = NVL(CCFLGA21118,space(1))
            this.w_FLGA22 = NVL(CCFLGA22118,space(1))
            this.w_FLPRIC = NVL(CCFLPRIC118,space(1))
            this.w_FLGA23 = NVL(CCFLGA23118,space(1))
            this.w_FLGA24 = NVL(CCFLGA24118,space(1))
          else
          .link_1_18('Load')
          endif
        .w_MCDTPRIC = NVL(cp_ToDate(MCDTPRIC),ctod("  /  /  "))
        .w_MCDATDIS = NVL(cp_ToDate(MCDATDIS),ctod("  /  /  "))
        .w_MCCOMPET = NVL(MCCOMPET,space(4))
          .link_1_23('Load')
        .w_MCFLPRIU = NVL(MCFLPRIU,space(1))
          .link_1_26('Load')
        .w_MCDTPRIU = NVL(cp_ToDate(MCDTPRIU),ctod("  /  /  "))
        .w_MCCOMPET = NVL(MCCOMPET,space(4))
          .link_1_30('Load')
        .w_MCTIPCON = NVL(MCTIPCON,space(1))
        .w_MCDESMOV = NVL(MCDESMOV,space(40))
        .w_MCDESMO2 = NVL(MCDESMO2,space(40))
        .w_MCCODCON = NVL(MCCODCON,space(15))
          .link_1_50('Load')
        .w_MCCODCES = NVL(MCCODCES,space(20))
          if link_1_52_joined
            this.w_MCCODCES = NVL(CECODICE152,NVL(this.w_MCCODCES,space(20)))
            this.w_DESCES = NVL(CEDESCRI152,space(40))
            this.w_DTPRIU = NVL(cp_ToDate(CEDTPRIU152),ctod("  /  /  "))
            this.w_PERDEF = NVL(CEPERDEF152,0)
            this.w_IMPMAX = NVL(CEIMPMAX152,0)
            this.w_FLAUMU = NVL(CEFLAUMU152,space(1))
            this.w_DTOBSO = NVL(cp_ToDate(CEDTOBSO152),ctod("  /  /  "))
            this.w_STABEN = NVL(CESTABEN152,space(1))
            this.w_TIPAMM = NVL(CETIPAMM152,space(1))
            this.w_CODESE = NVL(CECODESE152,space(4))
            this.w_CODCAT = NVL(CECODCAT152,space(15))
            this.w_TIPCES = NVL(CETIPCES152,space(2))
            this.w_MCCODCEN = NVL(CECODCEN152,space(15))
            this.w_MCVOCCEN = NVL(CEVOCCEN152,space(15))
            this.w_MCCODCOM = NVL(CECODCOM152,space(15))
            this.w_STACES = NVL(CE_STATO152,space(1))
            this.w_FLCEUS = NVL(CEFLCEUS152,space(1))
            this.w_UNIMIS = NVL(CEUNIMIS152,space(3))
            this.w_AMMIMM = NVL(CEAMMIMM152,space(1))
            this.w_CEESPRIU = NVL(CEESPRIU152,space(4))
            this.w_CEVALLIM = NVL(CEVALLIM152,space(3))
            this.w_CEDURCES = NVL(CEDURCES152,0)
            this.w_AMMIMC = NVL(CEAMMIMC152,space(1))
            this.w_DTPRIC = NVL(cp_ToDate(CEDTPRIC152),ctod("  /  /  "))
            this.w_CENSOAMM = NVL(CENSOAMM152,space(1))
            this.w_CENSOAMF = NVL(CENSOAMF152,space(1))
            this.w_CEESPRIC = NVL(CEESPRIC152,space(4))
          else
          .link_1_52('Load')
          endif
        .w_MCSTATUS = NVL(MCSTATUS,space(1))
        .w_MCVOCCEN = NVL(MCVOCCEN,space(15))
          if link_1_60_joined
            this.w_MCVOCCEN = NVL(VCCODICE160,NVL(this.w_MCVOCCEN,space(15)))
            this.w_DESVOC = NVL(VCDESCRI160,space(40))
          else
          .link_1_60('Load')
          endif
        .w_MCCODCEN = NVL(MCCODCEN,space(15))
          if link_1_62_joined
            this.w_MCCODCEN = NVL(CC_CONTO162,NVL(this.w_MCCODCEN,space(15)))
            this.w_DESCEN = NVL(CCDESPIA162,space(40))
          else
          .link_1_62('Load')
          endif
        .w_MCCODCOM = NVL(MCCODCOM,space(15))
          if link_1_64_joined
            this.w_MCCODCOM = NVL(CNCODCAN164,NVL(this.w_MCCODCOM,space(15)))
            this.w_DESCAN = NVL(CNDESCAN164,space(30))
          else
          .link_1_64('Load')
          endif
        .w_TCAR = (.w_MCIMPA02+.w_MCIMPA03+.w_MCIMPA04)-.w_MCIMPA06
        .w_FLCAR = IIF(.w_TIPCES $ 'CC-PC' AND (.w_FLGA02='S' OR .w_FLGA23='S' OR .w_FLGA03='S' OR .w_FLGA24='S' OR .w_FLGA04='S' OR .w_FLGA18='S' OR .w_FLGA06='S' OR .w_FLGA19='S') AND .w_FLGA05<>'S' AND .w_FLGA22<>'S',.T.,.F.)
        .w_FLSCA = IIF(.w_TIPCES $ 'CC-PC' AND .w_FLGA02<>'S' AND .w_FLGA23<>'S' AND .w_FLGA03<>'S' AND .w_FLGA24<>'S' AND .w_FLGA04<>'S' AND .w_FLGA18<>'S' AND .w_FLGA06<>'S' AND .w_FLGA19<>'S' AND (.w_FLGA05='S' OR .w_FLGA22='S'),.T.,.F.)
        .w_CON1CAU = IIF(.w_Mcimpa01<>0 Or .w_Mcimpa02<>0 Or .w_Mcimpa03<>0 Or .w_Mcimpa04<>0 Or .w_Mcimpa05<>0 Or .w_Mcimpa06<>0 Or .w_Mcimpa07<>0 Or .w_Mcimpa08<>0,.F.,.T.)
        .w_CON2CAU = IIF(.w_Mcimpa09<>0 Or .w_Mcimpa10<>0 Or .w_Mcimpa11<>0 Or .w_Mcimpa12<>0 Or .w_Mcimpa13<>0 Or .w_Mcimpa14<>0 Or .w_Mcimpa15<>0 Or .w_Mcimpa16<>0,.F.,.T.)
        .w_MCCOMPET = NVL(MCCOMPET,space(4))
          * evitabile
          *.link_1_117('Load')
        .w_MCRIFCON = NVL(MCRIFCON,space(10))
        .w_MCRIFFAT = NVL(MCRIFFAT,space(10))
        .w_MCDATCON = NVL(cp_ToDate(MCDATCON),ctod("  /  /  "))
        .w_ORIFCON = .w_MCRIFCON
        .w_MCSTABEN = NVL(MCSTABEN,space(1))
        .oPgFrm.Page1.oPag.oObj_1_139.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_140.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_141.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_142.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_143.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_144.Calculate()
          .link_1_148('Load')
        .w_MCQTACES = NVL(MCQTACES,0)
        .oPgFrm.Page1.oPag.oObj_1_156.Calculate()
        .w_MCCODVAL = NVL(MCCODVAL,space(3))
          if link_2_20_joined
            this.w_MCCODVAL = NVL(VACODVAL220,NVL(this.w_MCCODVAL,space(3)))
            this.w_DECTOT = NVL(VADECTOT220,0)
            this.w_CAOVAL = NVL(VACAOVAL220,0)
            this.w_SIMVAL = NVL(VASIMVAL220,space(5))
          else
          .link_2_20('Load')
          endif
        .w_MCCAOVAL = NVL(MCCAOVAL,0)
        .w_CAOVAL = GETCAM(.w_MCCODVAL, IIF(EMPTY(.w_MCDATDOC),.w_MCDATREG,.w_MCDATDOC), 7)
        .w_CALCPIC = DEFPIC(.w_DECTOT)
        .w_MCIMPA01 = NVL(MCIMPA01,0)
        .w_MCIMPA23 = NVL(MCIMPA23,0)
        .w_MCIMPA02 = NVL(MCIMPA02,0)
        .w_MCIMPA24 = NVL(MCIMPA24,0)
        .w_MCIMPA03 = NVL(MCIMPA03,0)
        .w_MCIMPA18 = NVL(MCIMPA18,0)
        .w_MCIMPA04 = NVL(MCIMPA04,0)
        .w_MCIMPA22 = NVL(MCIMPA22,0)
        .w_MCIMPA05 = NVL(MCIMPA05,0)
        .w_MCIMPA19 = NVL(MCIMPA19,0)
        .w_MCIMPA06 = NVL(MCIMPA06,0)
        .w_MCIMPA07 = NVL(MCIMPA07,0)
        .w_CODCAU = .w_MCCODCAU
        .w_MCCOECIV = NVL(MCCOECIV,0)
        .w_MCIMPA08 = NVL(MCIMPA08,0)
        .w_MCIMPA09 = NVL(MCIMPA09,0)
        .w_MCCOEFIS = NVL(MCCOEFIS,0)
        .w_MCIMPA10 = NVL(MCIMPA10,0)
        .w_MCIMPA15 = NVL(MCIMPA15,0)
        .w_CODCES = .w_MCCODCES
        .w_MCCOEFI1 = NVL(MCCOEFI1,0)
        .w_MCIMPA16 = NVL(MCIMPA16,0)
        .w_MCIMPA11 = NVL(MCIMPA11,0)
        .w_MCIMPA12 = NVL(MCIMPA12,0)
        .w_MCIMPA20 = NVL(MCIMPA20,0)
        .w_MCIMPA13 = NVL(MCIMPA13,0)
        .w_MCIMPA21 = NVL(MCIMPA21,0)
        .w_MCIMPA14 = NVL(MCIMPA14,0)
        .oPgFrm.Page2.oPag.oObj_2_64.Calculate()
        .w_MCIMPA17 = NVL(MCIMPA17,0)
        .w_MCFLPRIC = NVL(MCFLPRIC,space(1))
        .w_TCARC = (.w_MCIMPA23+.w_MCIMPA24+.w_MCIMPA04)-.w_MCIMPA19
        .w_MCAGGSTA = NVL(MCAGGSTA,space(1))
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'MOV_CESP')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_135.enabled = this.oPgFrm.Page1.oPag.oBtn_1_135.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_169.enabled = this.oPgFrm.Page1.oPag.oBtn_1_169.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gsce_amc
    this.w_IMPV02=this.w_MCIMPA05
    this.w_CON2CAU=.T.
    this.w_CON1CAU=.T.
    this.w_IMPV17=this.w_MCIMPA22
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_MCSERIAL = space(10)
      .w_DOCSERIAL = space(10)
      .w_PNSERIALM = space(10)
      .w_PNSERIALC = space(10)
      .w_CODAZI = space(5)
      .w_PCTIPAMM = space(1)
      .w_MCVALNAZ = space(3)
      .w_TIPOPE = space(10)
      .w_MCNUMREG = 0
      .w_MCCODESE = space(4)
      .w_PRIDAT = ctod("  /  /  ")
      .w_MCDATREG = ctod("  /  /  ")
      .w_MCNUMDOC = 0
      .w_MCALFDOC = space(10)
      .w_MCDATDOC = ctod("  /  /  ")
      .w_FLPRIC = space(1)
      .w_MCCODCAU = space(5)
      .w_MCDTPRIC = ctod("  /  /  ")
      .w_DESCAU = space(40)
      .w_MCDATDIS = ctod("  /  /  ")
      .w_FLPRIU = space(1)
      .w_MCCOMPET = space(4)
      .w_FLDADI = space(1)
      .w_MCFLPRIU = space(1)
      .w_CAUCON = space(5)
      .w_FLANAL = space(1)
      .w_NFOR = 0
      .w_MCDTPRIU = ctod("  /  /  ")
      .w_MCCOMPET = space(4)
      .w_MCTIPCON = space(1)
      .w_FLGA01 = space(1)
      .w_FLGA02 = space(1)
      .w_FLGA03 = space(1)
      .w_FLGA04 = space(1)
      .w_FLGA05 = space(1)
      .w_FLGA06 = space(1)
      .w_FLGA07 = space(1)
      .w_FLGA08 = space(1)
      .w_FLGA09 = space(1)
      .w_FLGA10 = space(1)
      .w_FLGA11 = space(1)
      .w_FLGA12 = space(1)
      .w_FLGA13 = space(1)
      .w_FLGA14 = space(1)
      .w_FLGA15 = space(1)
      .w_FLGA16 = space(1)
      .w_MCDESMOV = space(40)
      .w_MCDESMO2 = space(40)
      .w_MCCODCON = space(15)
      .w_DESCON = space(40)
      .w_MCCODCES = space(20)
      .w_CEESPRIU = space(4)
      .w_DESCES = space(40)
      .w_STACES = space(1)
      .w_TIPCES = space(2)
      .w_PERDEF = 0
      .w_IMPMAX = 0
      .w_MCSTATUS = space(1)
      .w_MCVOCCEN = space(15)
      .w_DESVOC = space(40)
      .w_MCCODCEN = space(15)
      .w_DESCEN = space(40)
      .w_MCCODCOM = space(15)
      .w_DESCAN = space(30)
      .w_TCAR = 0
      .w_FLCAR = .f.
      .w_FLSCA = .f.
      .w_CON1CAU = .f.
      .w_CON2CAU = .f.
      .w_OBTEST = ctod("  /  /  ")
      .w_IMPV01 = 0
      .w_IMPV02 = 0
      .w_IMPV03 = 0
      .w_IMPV04 = 0
      .w_IMPV05 = 0
      .w_IMPV06 = 0
      .w_IMPV07 = 0
      .w_IMPV08 = 0
      .w_IMPV09 = 0
      .w_IMPV10 = 0
      .w_IMPV11 = 0
      .w_IMPV12 = 0
      .w_IMPV13 = 0
      .w_IMPV14 = 0
      .w_DTOBSO = ctod("  /  /  ")
      .w_DTPRIU = ctod("  /  /  ")
      .w_COECIV = 0
      .w_QUOCIV = 0
      .w_COEFI1 = 0
      .w_COEFI2 = 0
      .w_ABILITA = .f.
      .w_QUOFIS = 0
      .w_QUOFI1 = 0
      .w_FLAUMU = space(1)
      .w_STABEN = space(1)
      .w_DATAGG = space(1)
      .w_INIESE = ctod("  /  /  ")
      .w_FINESE = ctod("  /  /  ")
      .w_TIPAMM = space(1)
      .w_CODESE = space(4)
      .w_CODCAT = space(15)
      .w_TCOECIV = 0
      .w_TCOEFI1 = 0
      .w_TCOEFI2 = 0
      .w_MCCOMPET = space(4)
      .w_FLCONT = space(1)
      .w_MCRIFCON = space(10)
      .w_MCRIFFAT = space(10)
      .w_FLPROV = space(1)
      .w_MCDATCON = ctod("  /  /  ")
      .w_ORIFCON = space(10)
      .w_MCSTABEN = space(1)
      .w_DaScartare = .f.
      .w_DaGenera = .f.
      .w_ALLROW = .f.
      .w_FLCEUS = space(1)
      .w_OKFISORD = .f.
      .w_OKFISANT = .f.
      .w_OKCIVILE = .f.
      .w_IMPRIV = 0
      .w_UNIMIS = space(3)
      .w_DESUNI = space(35)
      .w_MCQTACES = 0
      .w_AMMIMM = space(1)
      .w_FLGFRAZ = space(1)
      .w_FLGA17 = space(1)
      .w_TIPOAGG = space(1)
      .w_ORIGINE = space(1)
      .w_BLOCCA = .f.
      .w_IMQTACES = 0
      .w_MCCODVAL = space(3)
      .w_MCCAOVAL = 0
      .w_DECTOT = 0
      .w_CAOVAL = 0
      .w_CALCPIC = 0
      .w_MCIMPA01 = 0
      .w_MCIMPA23 = 0
      .w_MCIMPA02 = 0
      .w_MCIMPA24 = 0
      .w_MCIMPA03 = 0
      .w_MCIMPA18 = 0
      .w_MCIMPA04 = 0
      .w_MCIMPA22 = 0
      .w_MCIMPA05 = 0
      .w_MCIMPA19 = 0
      .w_MCIMPA06 = 0
      .w_MCIMPA07 = 0
      .w_CODCAU = space(5)
      .w_MCCOECIV = 0
      .w_MCIMPA08 = 0
      .w_MCIMPA09 = 0
      .w_DESCAU = space(40)
      .w_MCCOEFIS = 0
      .w_MCIMPA10 = 0
      .w_MCIMPA15 = 0
      .w_CODCES = space(20)
      .w_MCCOEFI1 = 0
      .w_MCIMPA16 = 0
      .w_DESCES = space(40)
      .w_MCIMPA11 = 0
      .w_MCIMPA12 = 0
      .w_MCIMPA20 = 0
      .w_MCIMPA13 = 0
      .w_MCIMPA21 = 0
      .w_MCIMPA14 = 0
      .w_SIMVAL = space(5)
      .w_RIVALU = space(10)
      .w_IMPRIV2 = 0
      .w_MCIMPA17 = 0
      .w_DTOBSOCA = ctod("  /  /  ")
      .w_DTOBSOCL = ctod("  /  /  ")
      .w_CEFLAMCI = space(1)
      .w_CEPERCIV = 0
      .w_CEVALLIM = space(3)
      .w_CEDURCES = 0
      .w_CODESF = space(4)
      .w_MCFLPRIC = space(1)
      .w_AMMIMC = space(1)
      .w_CCNSOAMM = space(1)
      .w_CCNSOAMF = space(1)
      .w_CENSOAMM = space(1)
      .w_CENSOAMF = space(1)
      .w_DTPRIC = ctod("  /  /  ")
      .w_FLGA18 = space(1)
      .w_FLGA19 = space(1)
      .w_FLGA20 = space(1)
      .w_FLGA21 = space(1)
      .w_FLGA21 = space(1)
      .w_FLGA22 = space(1)
      .w_FLGA23 = space(1)
      .w_FLGA24 = space(1)
      .w_IMPV16 = 0
      .w_IMPV17 = 0
      .w_TCARC = 0
      .w_CEESPRIC = space(4)
      .w_MCAGGSTA = space(1)
      .w_AMTIPAMM = space(1)
      .w_CODESC = space(4)
      .w_UTCC = 0
      .w_UTCV = 0
      .w_UTDC = ctot("")
      .w_UTDV = ctot("")
      if .cFunction<>"Filter"
        .oPgFrm.Page1.oPag.oObj_1_2.Calculate()
          .DoRTCalc(1,4,.f.)
        .w_CODAZI = i_CODAZI
        .DoRTCalc(5,5,.f.)
          if not(empty(.w_CODAZI))
          .link_1_6('Full')
          endif
          .DoRTCalc(6,7,.f.)
        .w_TIPOPE = this.cFunction
          .DoRTCalc(9,9,.f.)
        .w_MCCODESE = g_CODESE
        .DoRTCalc(10,10,.f.)
          if not(empty(.w_MCCODESE))
          .link_1_11('Full')
          endif
          .DoRTCalc(11,11,.f.)
        .w_MCDATREG = i_datsys
        .DoRTCalc(13,17,.f.)
          if not(empty(.w_MCCODCAU))
          .link_1_18('Full')
          endif
        .w_MCDTPRIC = IIF(.w_FLPRIC='S' AND .w_PCTIPAMM<>'F',.w_MCDATREG,cp_CharToDate('  -  -  ') )
          .DoRTCalc(19,21,.f.)
        .w_MCCOMPET = iif(.w_FLPRIU="S",CALCESER(.w_MCDTPRIU,g_CODESE),iif(.w_FLPRIC="S",CALCESER(.w_MCDTPRIC,g_CODESE),iif(.w_FLDADI="S",CALCESER(.w_MCDATDIS,g_CODESE),g_CODESE)))
        .DoRTCalc(22,22,.f.)
          if not(empty(.w_MCCOMPET))
          .link_1_23('Full')
          endif
          .DoRTCalc(23,23,.f.)
        .w_MCFLPRIU = IIF(.w_FLPRIU='S' AND .w_PCTIPAMM<>'C', '=', ' ')
        .DoRTCalc(25,25,.f.)
          if not(empty(.w_CAUCON))
          .link_1_26('Full')
          endif
          .DoRTCalc(26,26,.f.)
        .w_NFOR = 0
        .w_MCDTPRIU = IIF(.w_FLPRIU='S' AND .w_PCTIPAMM<>'C',.w_MCDATREG,cp_CharToDate('  -  -  ') )
        .w_MCCOMPET = iif(.w_FLPRIU<>"S",g_CODESE,CALCESER(.w_MCDTPRIU, g_CODESE))
        .DoRTCalc(29,29,.f.)
          if not(empty(.w_MCCOMPET))
          .link_1_30('Full')
          endif
          .DoRTCalc(30,48,.f.)
        .w_MCCODCON = SPACE(15)
        .DoRTCalc(49,49,.f.)
          if not(empty(.w_MCCODCON))
          .link_1_50('Full')
          endif
        .DoRTCalc(50,51,.f.)
          if not(empty(.w_MCCODCES))
          .link_1_52('Full')
          endif
          .DoRTCalc(52,57,.f.)
        .w_MCSTATUS = 'C'
        .w_MCVOCCEN = .w_MCVOCCEN
        .DoRTCalc(59,59,.f.)
          if not(empty(.w_MCVOCCEN))
          .link_1_60('Full')
          endif
          .DoRTCalc(60,60,.f.)
        .w_MCCODCEN = .w_MCCODCEN
        .DoRTCalc(61,61,.f.)
          if not(empty(.w_MCCODCEN))
          .link_1_62('Full')
          endif
          .DoRTCalc(62,62,.f.)
        .w_MCCODCOM = .w_MCCODCOM
        .DoRTCalc(63,63,.f.)
          if not(empty(.w_MCCODCOM))
          .link_1_64('Full')
          endif
          .DoRTCalc(64,64,.f.)
        .w_TCAR = (.w_MCIMPA02+.w_MCIMPA03+.w_MCIMPA04)-.w_MCIMPA06
        .w_FLCAR = IIF(.w_TIPCES $ 'CC-PC' AND (.w_FLGA02='S' OR .w_FLGA23='S' OR .w_FLGA03='S' OR .w_FLGA24='S' OR .w_FLGA04='S' OR .w_FLGA18='S' OR .w_FLGA06='S' OR .w_FLGA19='S') AND .w_FLGA05<>'S' AND .w_FLGA22<>'S',.T.,.F.)
        .w_FLSCA = IIF(.w_TIPCES $ 'CC-PC' AND .w_FLGA02<>'S' AND .w_FLGA23<>'S' AND .w_FLGA03<>'S' AND .w_FLGA24<>'S' AND .w_FLGA04<>'S' AND .w_FLGA18<>'S' AND .w_FLGA06<>'S' AND .w_FLGA19<>'S' AND (.w_FLGA05='S' OR .w_FLGA22='S'),.T.,.F.)
        .w_CON1CAU = IIF(.w_Mcimpa01<>0 Or .w_Mcimpa02<>0 Or .w_Mcimpa03<>0 Or .w_Mcimpa04<>0 Or .w_Mcimpa05<>0 Or .w_Mcimpa06<>0 Or .w_Mcimpa07<>0 Or .w_Mcimpa08<>0,.F.,.T.)
        .w_CON2CAU = IIF(.w_Mcimpa09<>0 Or .w_Mcimpa10<>0 Or .w_Mcimpa11<>0 Or .w_Mcimpa12<>0 Or .w_Mcimpa13<>0 Or .w_Mcimpa14<>0 Or .w_Mcimpa15<>0 Or .w_Mcimpa16<>0,.F.,.T.)
        .w_OBTEST = i_datsys
          .DoRTCalc(71,86,.f.)
        .w_COECIV = 0
        .w_QUOCIV = 0
        .DoRTCalc(89,105,.f.)
          if not(empty(.w_MCCOMPET))
          .link_1_117('Full')
          endif
          .DoRTCalc(106,110,.f.)
        .w_ORIFCON = .w_MCRIFCON
        .w_MCSTABEN = 'U'
          .DoRTCalc(113,114,.f.)
        .w_ALLROW = .F.
          .DoRTCalc(116,116,.f.)
        .w_OKFISORD = .F.
        .w_OKFISANT = .F.
        .w_OKCIVILE = .F.
        .oPgFrm.Page1.oPag.oObj_1_139.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_140.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_141.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_142.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_143.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_144.Calculate()
        .DoRTCalc(120,121,.f.)
          if not(empty(.w_UNIMIS))
          .link_1_148('Full')
          endif
        .oPgFrm.Page1.oPag.oObj_1_156.Calculate()
        .DoRTCalc(122,131,.f.)
          if not(empty(.w_MCCODVAL))
          .link_2_20('Full')
          endif
        .w_MCCAOVAL = GETCAM(.w_MCCODVAL, IIF(EMPTY(.w_MCDATDOC),.w_MCDATREG,.w_MCDATDOC), 7)
          .DoRTCalc(133,133,.f.)
        .w_CAOVAL = GETCAM(.w_MCCODVAL, IIF(EMPTY(.w_MCDATDOC),.w_MCDATREG,.w_MCDATDOC), 7)
        .w_CALCPIC = DEFPIC(.w_DECTOT)
          .DoRTCalc(136,147,.f.)
        .w_CODCAU = .w_MCCODCAU
        .w_MCCOECIV = .w_TCOECIV
          .DoRTCalc(150,152,.f.)
        .w_MCCOEFIS = .w_TCOEFI1
          .DoRTCalc(154,155,.f.)
        .w_CODCES = .w_MCCODCES
        .w_MCCOEFI1 = .w_TCOEFI2
        .oPgFrm.Page2.oPag.oObj_2_64.Calculate()
          .DoRTCalc(158,172,.f.)
        .w_CEPERCIV = 0
          .DoRTCalc(174,176,.f.)
        .w_MCFLPRIC = IIF(.w_FLPRIC='S' AND .w_PCTIPAMM<>'F', '=', ' ')
          .DoRTCalc(178,178,.f.)
        .w_CCNSOAMM = 'N'
        .w_CCNSOAMF = 'N'
          .DoRTCalc(181,193,.f.)
        .w_TCARC = (.w_MCIMPA23+.w_MCIMPA24+.w_MCIMPA04)-.w_MCIMPA19
          .DoRTCalc(195,195,.f.)
        .w_MCAGGSTA = IIF(.w_FLPRIC='S', '=', IIF(.w_FLPRIU='S','=',' ') )
      endif
    endwith
    cp_BlankRecExtFlds(this,'MOV_CESP')
    this.DoRTCalc(197,202,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_135.enabled = this.oPgFrm.Page1.oPag.oBtn_1_135.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_169.enabled = this.oPgFrm.Page1.oPag.oBtn_1_169.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.MOV_CESP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOV_CESP_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"SEMCE","i_codazi,w_MCSERIAL")
      cp_AskTableProg(this,i_nConn,"PRMCE","i_codazi,w_MCCODESE,w_MCNUMREG")
      .op_codazi = .w_codazi
      .op_MCSERIAL = .w_MCSERIAL
      .op_codazi = .w_codazi
      .op_MCCODESE = .w_MCCODESE
      .op_MCNUMREG = .w_MCNUMREG
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oMCNUMREG_1_10.enabled = i_bVal
      .Page1.oPag.oMCCODESE_1_11.enabled = i_bVal
      .Page1.oPag.oMCDATREG_1_13.enabled = i_bVal
      .Page1.oPag.oMCNUMDOC_1_14.enabled = i_bVal
      .Page1.oPag.oMCALFDOC_1_15.enabled = i_bVal
      .Page1.oPag.oMCDATDOC_1_16.enabled = i_bVal
      .Page1.oPag.oMCCODCAU_1_18.enabled = i_bVal
      .Page1.oPag.oMCDTPRIC_1_19.enabled = i_bVal
      .Page1.oPag.oMCDATDIS_1_21.enabled = i_bVal
      .Page1.oPag.oMCCOMPET_1_23.enabled = i_bVal
      .Page1.oPag.oMCDTPRIU_1_29.enabled = i_bVal
      .Page1.oPag.oMCCOMPET_1_30.enabled = i_bVal
      .Page1.oPag.oMCDESMOV_1_48.enabled = i_bVal
      .Page1.oPag.oMCDESMO2_1_49.enabled = i_bVal
      .Page1.oPag.oMCCODCON_1_50.enabled = i_bVal
      .Page1.oPag.oMCCODCES_1_52.enabled = i_bVal
      .Page1.oPag.oMCSTATUS_1_59.enabled = i_bVal
      .Page1.oPag.oMCVOCCEN_1_60.enabled = i_bVal
      .Page1.oPag.oMCCODCEN_1_62.enabled = i_bVal
      .Page1.oPag.oMCCODCOM_1_64.enabled = i_bVal
      .Page1.oPag.oMCQTACES_1_151.enabled = i_bVal
      .Page2.oPag.oMCCAOVAL_2_21.enabled = i_bVal
      .Page2.oPag.oMCIMPA01_2_28.enabled = i_bVal
      .Page2.oPag.oMCIMPA23_2_29.enabled = i_bVal
      .Page2.oPag.oMCIMPA02_2_30.enabled = i_bVal
      .Page2.oPag.oMCIMPA24_2_31.enabled = i_bVal
      .Page2.oPag.oMCIMPA03_2_32.enabled = i_bVal
      .Page2.oPag.oMCIMPA18_2_33.enabled = i_bVal
      .Page2.oPag.oMCIMPA04_2_35.enabled = i_bVal
      .Page2.oPag.oMCIMPA22_2_36.enabled = i_bVal
      .Page2.oPag.oMCIMPA05_2_37.enabled = i_bVal
      .Page2.oPag.oMCIMPA19_2_38.enabled = i_bVal
      .Page2.oPag.oMCIMPA06_2_39.enabled = i_bVal
      .Page2.oPag.oMCIMPA07_2_40.enabled = i_bVal
      .Page2.oPag.oMCCOECIV_2_42.enabled = i_bVal
      .Page2.oPag.oMCIMPA08_2_43.enabled = i_bVal
      .Page2.oPag.oMCIMPA09_2_44.enabled = i_bVal
      .Page2.oPag.oMCCOEFIS_2_46.enabled = i_bVal
      .Page2.oPag.oMCIMPA10_2_48.enabled = i_bVal
      .Page2.oPag.oMCIMPA15_2_50.enabled = i_bVal
      .Page2.oPag.oMCCOEFI1_2_52.enabled = i_bVal
      .Page2.oPag.oMCIMPA16_2_53.enabled = i_bVal
      .Page2.oPag.oMCIMPA11_2_55.enabled = i_bVal
      .Page2.oPag.oMCIMPA12_2_56.enabled = i_bVal
      .Page2.oPag.oMCIMPA20_2_57.enabled = i_bVal
      .Page2.oPag.oMCIMPA13_2_58.enabled = i_bVal
      .Page2.oPag.oMCIMPA21_2_59.enabled = i_bVal
      .Page2.oPag.oMCIMPA14_2_60.enabled = i_bVal
      .Page1.oPag.oBtn_1_135.enabled = .Page1.oPag.oBtn_1_135.mCond()
      .Page2.oPag.oBtn_2_62.enabled = i_bVal
      .Page2.oPag.oBtn_2_63.enabled = i_bVal
      .Page1.oPag.oBtn_1_169.enabled = .Page1.oPag.oBtn_1_169.mCond()
      .Page1.oPag.oObj_1_2.enabled = i_bVal
      .Page1.oPag.oObj_1_139.enabled = i_bVal
      .Page1.oPag.oObj_1_140.enabled = i_bVal
      .Page1.oPag.oObj_1_141.enabled = i_bVal
      .Page1.oPag.oObj_1_142.enabled = i_bVal
      .Page1.oPag.oObj_1_143.enabled = i_bVal
      .Page1.oPag.oObj_1_144.enabled = i_bVal
      .Page1.oPag.oObj_1_156.enabled = i_bVal
      .Page2.oPag.oObj_2_64.enabled = i_bVal
      if i_cOp = "Query"
        .Page1.oPag.oMCNUMREG_1_10.enabled = .t.
        .Page1.oPag.oMCDATREG_1_13.enabled = .t.
        .Page1.oPag.oMCCODCES_1_52.enabled = .t.
      endif
    endwith
    this.GSCE_MMC.SetStatus(i_cOp)
    this.GSCE_MMS.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'MOV_CESP',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSCE_MMC.SetChildrenStatus(i_cOp)
  *  this.GSCE_MMS.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.MOV_CESP_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCSERIAL,"MCSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCVALNAZ,"MCVALNAZ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCNUMREG,"MCNUMREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCCODESE,"MCCODESE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCDATREG,"MCDATREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCNUMDOC,"MCNUMDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCALFDOC,"MCALFDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCDATDOC,"MCDATDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCCODCAU,"MCCODCAU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCDTPRIC,"MCDTPRIC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCDATDIS,"MCDATDIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCCOMPET,"MCCOMPET",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCFLPRIU,"MCFLPRIU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCDTPRIU,"MCDTPRIU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCCOMPET,"MCCOMPET",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCTIPCON,"MCTIPCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCDESMOV,"MCDESMOV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCDESMO2,"MCDESMO2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCCODCON,"MCCODCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCCODCES,"MCCODCES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCSTATUS,"MCSTATUS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCVOCCEN,"MCVOCCEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCCODCEN,"MCCODCEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCCODCOM,"MCCODCOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCCOMPET,"MCCOMPET",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCRIFCON,"MCRIFCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCRIFFAT,"MCRIFFAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCDATCON,"MCDATCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCSTABEN,"MCSTABEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCQTACES,"MCQTACES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCCODVAL,"MCCODVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCCAOVAL,"MCCAOVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCIMPA01,"MCIMPA01",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCIMPA23,"MCIMPA23",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCIMPA02,"MCIMPA02",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCIMPA24,"MCIMPA24",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCIMPA03,"MCIMPA03",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCIMPA18,"MCIMPA18",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCIMPA04,"MCIMPA04",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCIMPA22,"MCIMPA22",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCIMPA05,"MCIMPA05",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCIMPA19,"MCIMPA19",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCIMPA06,"MCIMPA06",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCIMPA07,"MCIMPA07",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCCOECIV,"MCCOECIV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCIMPA08,"MCIMPA08",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCIMPA09,"MCIMPA09",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCCOEFIS,"MCCOEFIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCIMPA10,"MCIMPA10",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCIMPA15,"MCIMPA15",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCCOEFI1,"MCCOEFI1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCIMPA16,"MCIMPA16",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCIMPA11,"MCIMPA11",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCIMPA12,"MCIMPA12",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCIMPA20,"MCIMPA20",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCIMPA13,"MCIMPA13",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCIMPA21,"MCIMPA21",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCIMPA14,"MCIMPA14",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCIMPA17,"MCIMPA17",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCFLPRIC,"MCFLPRIC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCAGGSTA,"MCAGGSTA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.MOV_CESP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOV_CESP_IDX,2])
    i_lTable = "MOV_CESP"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.MOV_CESP_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSCE_KMC with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.MOV_CESP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MOV_CESP_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.MOV_CESP_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"SEMCE","i_codazi,w_MCSERIAL")
          cp_NextTableProg(this,i_nConn,"PRMCE","i_codazi,w_MCCODESE,w_MCNUMREG")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into MOV_CESP
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'MOV_CESP')
        i_extval=cp_InsertValODBCExtFlds(this,'MOV_CESP')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(MCSERIAL,MCVALNAZ,MCNUMREG,MCCODESE,MCDATREG"+;
                  ",MCNUMDOC,MCALFDOC,MCDATDOC,MCCODCAU,MCDTPRIC"+;
                  ",MCDATDIS,MCCOMPET,MCFLPRIU,MCDTPRIU,MCTIPCON"+;
                  ",MCDESMOV,MCDESMO2,MCCODCON,MCCODCES,MCSTATUS"+;
                  ",MCVOCCEN,MCCODCEN,MCCODCOM,MCRIFCON,MCRIFFAT"+;
                  ",MCDATCON,MCSTABEN,MCQTACES,MCCODVAL,MCCAOVAL"+;
                  ",MCIMPA01,MCIMPA23,MCIMPA02,MCIMPA24,MCIMPA03"+;
                  ",MCIMPA18,MCIMPA04,MCIMPA22,MCIMPA05,MCIMPA19"+;
                  ",MCIMPA06,MCIMPA07,MCCOECIV,MCIMPA08,MCIMPA09"+;
                  ",MCCOEFIS,MCIMPA10,MCIMPA15,MCCOEFI1,MCIMPA16"+;
                  ",MCIMPA11,MCIMPA12,MCIMPA20,MCIMPA13,MCIMPA21"+;
                  ",MCIMPA14,MCIMPA17,MCFLPRIC,MCAGGSTA,UTCC"+;
                  ",UTCV,UTDC,UTDV "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_MCSERIAL)+;
                  ","+cp_ToStrODBC(this.w_MCVALNAZ)+;
                  ","+cp_ToStrODBC(this.w_MCNUMREG)+;
                  ","+cp_ToStrODBCNull(this.w_MCCODESE)+;
                  ","+cp_ToStrODBC(this.w_MCDATREG)+;
                  ","+cp_ToStrODBC(this.w_MCNUMDOC)+;
                  ","+cp_ToStrODBC(this.w_MCALFDOC)+;
                  ","+cp_ToStrODBC(this.w_MCDATDOC)+;
                  ","+cp_ToStrODBCNull(this.w_MCCODCAU)+;
                  ","+cp_ToStrODBC(this.w_MCDTPRIC)+;
                  ","+cp_ToStrODBC(this.w_MCDATDIS)+;
                  ","+cp_ToStrODBCNull(this.w_MCCOMPET)+;
                  ","+cp_ToStrODBC(this.w_MCFLPRIU)+;
                  ","+cp_ToStrODBC(this.w_MCDTPRIU)+;
                  ","+cp_ToStrODBC(this.w_MCTIPCON)+;
                  ","+cp_ToStrODBC(this.w_MCDESMOV)+;
                  ","+cp_ToStrODBC(this.w_MCDESMO2)+;
                  ","+cp_ToStrODBCNull(this.w_MCCODCON)+;
                  ","+cp_ToStrODBCNull(this.w_MCCODCES)+;
                  ","+cp_ToStrODBC(this.w_MCSTATUS)+;
                  ","+cp_ToStrODBCNull(this.w_MCVOCCEN)+;
                  ","+cp_ToStrODBCNull(this.w_MCCODCEN)+;
                  ","+cp_ToStrODBCNull(this.w_MCCODCOM)+;
                  ","+cp_ToStrODBC(this.w_MCRIFCON)+;
                  ","+cp_ToStrODBC(this.w_MCRIFFAT)+;
                  ","+cp_ToStrODBC(this.w_MCDATCON)+;
                  ","+cp_ToStrODBC(this.w_MCSTABEN)+;
                  ","+cp_ToStrODBC(this.w_MCQTACES)+;
                  ","+cp_ToStrODBCNull(this.w_MCCODVAL)+;
                  ","+cp_ToStrODBC(this.w_MCCAOVAL)+;
                  ","+cp_ToStrODBC(this.w_MCIMPA01)+;
                  ","+cp_ToStrODBC(this.w_MCIMPA23)+;
                  ","+cp_ToStrODBC(this.w_MCIMPA02)+;
                  ","+cp_ToStrODBC(this.w_MCIMPA24)+;
                  ","+cp_ToStrODBC(this.w_MCIMPA03)+;
                  ","+cp_ToStrODBC(this.w_MCIMPA18)+;
                  ","+cp_ToStrODBC(this.w_MCIMPA04)+;
                  ","+cp_ToStrODBC(this.w_MCIMPA22)+;
                  ","+cp_ToStrODBC(this.w_MCIMPA05)+;
                  ","+cp_ToStrODBC(this.w_MCIMPA19)+;
                  ","+cp_ToStrODBC(this.w_MCIMPA06)+;
                  ","+cp_ToStrODBC(this.w_MCIMPA07)+;
                  ","+cp_ToStrODBC(this.w_MCCOECIV)+;
                  ","+cp_ToStrODBC(this.w_MCIMPA08)+;
                  ","+cp_ToStrODBC(this.w_MCIMPA09)+;
                  ","+cp_ToStrODBC(this.w_MCCOEFIS)+;
                  ","+cp_ToStrODBC(this.w_MCIMPA10)+;
                  ","+cp_ToStrODBC(this.w_MCIMPA15)+;
                  ","+cp_ToStrODBC(this.w_MCCOEFI1)+;
                  ","+cp_ToStrODBC(this.w_MCIMPA16)+;
                  ","+cp_ToStrODBC(this.w_MCIMPA11)+;
                  ","+cp_ToStrODBC(this.w_MCIMPA12)+;
                  ","+cp_ToStrODBC(this.w_MCIMPA20)+;
                  ","+cp_ToStrODBC(this.w_MCIMPA13)+;
                  ","+cp_ToStrODBC(this.w_MCIMPA21)+;
                  ","+cp_ToStrODBC(this.w_MCIMPA14)+;
                  ","+cp_ToStrODBC(this.w_MCIMPA17)+;
                  ","+cp_ToStrODBC(this.w_MCFLPRIC)+;
                  ","+cp_ToStrODBC(this.w_MCAGGSTA)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'MOV_CESP')
        i_extval=cp_InsertValVFPExtFlds(this,'MOV_CESP')
        cp_CheckDeletedKey(i_cTable,0,'MCSERIAL',this.w_MCSERIAL)
        INSERT INTO (i_cTable);
              (MCSERIAL,MCVALNAZ,MCNUMREG,MCCODESE,MCDATREG,MCNUMDOC,MCALFDOC,MCDATDOC,MCCODCAU,MCDTPRIC,MCDATDIS,MCCOMPET,MCFLPRIU,MCDTPRIU,MCTIPCON,MCDESMOV,MCDESMO2,MCCODCON,MCCODCES,MCSTATUS,MCVOCCEN,MCCODCEN,MCCODCOM,MCRIFCON,MCRIFFAT,MCDATCON,MCSTABEN,MCQTACES,MCCODVAL,MCCAOVAL,MCIMPA01,MCIMPA23,MCIMPA02,MCIMPA24,MCIMPA03,MCIMPA18,MCIMPA04,MCIMPA22,MCIMPA05,MCIMPA19,MCIMPA06,MCIMPA07,MCCOECIV,MCIMPA08,MCIMPA09,MCCOEFIS,MCIMPA10,MCIMPA15,MCCOEFI1,MCIMPA16,MCIMPA11,MCIMPA12,MCIMPA20,MCIMPA13,MCIMPA21,MCIMPA14,MCIMPA17,MCFLPRIC,MCAGGSTA,UTCC,UTCV,UTDC,UTDV  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_MCSERIAL;
                  ,this.w_MCVALNAZ;
                  ,this.w_MCNUMREG;
                  ,this.w_MCCODESE;
                  ,this.w_MCDATREG;
                  ,this.w_MCNUMDOC;
                  ,this.w_MCALFDOC;
                  ,this.w_MCDATDOC;
                  ,this.w_MCCODCAU;
                  ,this.w_MCDTPRIC;
                  ,this.w_MCDATDIS;
                  ,this.w_MCCOMPET;
                  ,this.w_MCFLPRIU;
                  ,this.w_MCDTPRIU;
                  ,this.w_MCTIPCON;
                  ,this.w_MCDESMOV;
                  ,this.w_MCDESMO2;
                  ,this.w_MCCODCON;
                  ,this.w_MCCODCES;
                  ,this.w_MCSTATUS;
                  ,this.w_MCVOCCEN;
                  ,this.w_MCCODCEN;
                  ,this.w_MCCODCOM;
                  ,this.w_MCRIFCON;
                  ,this.w_MCRIFFAT;
                  ,this.w_MCDATCON;
                  ,this.w_MCSTABEN;
                  ,this.w_MCQTACES;
                  ,this.w_MCCODVAL;
                  ,this.w_MCCAOVAL;
                  ,this.w_MCIMPA01;
                  ,this.w_MCIMPA23;
                  ,this.w_MCIMPA02;
                  ,this.w_MCIMPA24;
                  ,this.w_MCIMPA03;
                  ,this.w_MCIMPA18;
                  ,this.w_MCIMPA04;
                  ,this.w_MCIMPA22;
                  ,this.w_MCIMPA05;
                  ,this.w_MCIMPA19;
                  ,this.w_MCIMPA06;
                  ,this.w_MCIMPA07;
                  ,this.w_MCCOECIV;
                  ,this.w_MCIMPA08;
                  ,this.w_MCIMPA09;
                  ,this.w_MCCOEFIS;
                  ,this.w_MCIMPA10;
                  ,this.w_MCIMPA15;
                  ,this.w_MCCOEFI1;
                  ,this.w_MCIMPA16;
                  ,this.w_MCIMPA11;
                  ,this.w_MCIMPA12;
                  ,this.w_MCIMPA20;
                  ,this.w_MCIMPA13;
                  ,this.w_MCIMPA21;
                  ,this.w_MCIMPA14;
                  ,this.w_MCIMPA17;
                  ,this.w_MCFLPRIC;
                  ,this.w_MCAGGSTA;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- gsce_amc
    *Al termine della cancellazione interrogo i saldi per vedere
    *se la quantit� � in negativo.
    this.NotifyEvent('CheckQTA')
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.MOV_CESP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MOV_CESP_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.MOV_CESP_IDX,i_nConn)
      *
      * update MOV_CESP
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'MOV_CESP')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " MCVALNAZ="+cp_ToStrODBC(this.w_MCVALNAZ)+;
             ",MCNUMREG="+cp_ToStrODBC(this.w_MCNUMREG)+;
             ",MCCODESE="+cp_ToStrODBCNull(this.w_MCCODESE)+;
             ",MCDATREG="+cp_ToStrODBC(this.w_MCDATREG)+;
             ",MCNUMDOC="+cp_ToStrODBC(this.w_MCNUMDOC)+;
             ",MCALFDOC="+cp_ToStrODBC(this.w_MCALFDOC)+;
             ",MCDATDOC="+cp_ToStrODBC(this.w_MCDATDOC)+;
             ",MCCODCAU="+cp_ToStrODBCNull(this.w_MCCODCAU)+;
             ",MCDTPRIC="+cp_ToStrODBC(this.w_MCDTPRIC)+;
             ",MCDATDIS="+cp_ToStrODBC(this.w_MCDATDIS)+;
             ",MCCOMPET="+cp_ToStrODBCNull(this.w_MCCOMPET)+;
             ",MCFLPRIU="+cp_ToStrODBC(this.w_MCFLPRIU)+;
             ",MCDTPRIU="+cp_ToStrODBC(this.w_MCDTPRIU)+;
             ",MCTIPCON="+cp_ToStrODBC(this.w_MCTIPCON)+;
             ",MCDESMOV="+cp_ToStrODBC(this.w_MCDESMOV)+;
             ",MCDESMO2="+cp_ToStrODBC(this.w_MCDESMO2)+;
             ",MCCODCON="+cp_ToStrODBCNull(this.w_MCCODCON)+;
             ",MCCODCES="+cp_ToStrODBCNull(this.w_MCCODCES)+;
             ",MCSTATUS="+cp_ToStrODBC(this.w_MCSTATUS)+;
             ",MCVOCCEN="+cp_ToStrODBCNull(this.w_MCVOCCEN)+;
             ",MCCODCEN="+cp_ToStrODBCNull(this.w_MCCODCEN)+;
             ",MCCODCOM="+cp_ToStrODBCNull(this.w_MCCODCOM)+;
             ",MCRIFCON="+cp_ToStrODBC(this.w_MCRIFCON)+;
             ",MCRIFFAT="+cp_ToStrODBC(this.w_MCRIFFAT)+;
             ",MCDATCON="+cp_ToStrODBC(this.w_MCDATCON)+;
             ",MCSTABEN="+cp_ToStrODBC(this.w_MCSTABEN)+;
             ",MCQTACES="+cp_ToStrODBC(this.w_MCQTACES)+;
             ",MCCODVAL="+cp_ToStrODBCNull(this.w_MCCODVAL)+;
             ",MCCAOVAL="+cp_ToStrODBC(this.w_MCCAOVAL)+;
             ",MCIMPA01="+cp_ToStrODBC(this.w_MCIMPA01)+;
             ",MCIMPA23="+cp_ToStrODBC(this.w_MCIMPA23)+;
             ",MCIMPA02="+cp_ToStrODBC(this.w_MCIMPA02)+;
             ",MCIMPA24="+cp_ToStrODBC(this.w_MCIMPA24)+;
             ",MCIMPA03="+cp_ToStrODBC(this.w_MCIMPA03)+;
             ",MCIMPA18="+cp_ToStrODBC(this.w_MCIMPA18)+;
             ",MCIMPA04="+cp_ToStrODBC(this.w_MCIMPA04)+;
             ",MCIMPA22="+cp_ToStrODBC(this.w_MCIMPA22)+;
             ",MCIMPA05="+cp_ToStrODBC(this.w_MCIMPA05)+;
             ",MCIMPA19="+cp_ToStrODBC(this.w_MCIMPA19)+;
             ",MCIMPA06="+cp_ToStrODBC(this.w_MCIMPA06)+;
             ",MCIMPA07="+cp_ToStrODBC(this.w_MCIMPA07)+;
             ",MCCOECIV="+cp_ToStrODBC(this.w_MCCOECIV)+;
             ",MCIMPA08="+cp_ToStrODBC(this.w_MCIMPA08)+;
             ",MCIMPA09="+cp_ToStrODBC(this.w_MCIMPA09)+;
             ",MCCOEFIS="+cp_ToStrODBC(this.w_MCCOEFIS)+;
             ",MCIMPA10="+cp_ToStrODBC(this.w_MCIMPA10)+;
             ",MCIMPA15="+cp_ToStrODBC(this.w_MCIMPA15)+;
             ",MCCOEFI1="+cp_ToStrODBC(this.w_MCCOEFI1)+;
             ",MCIMPA16="+cp_ToStrODBC(this.w_MCIMPA16)+;
             ",MCIMPA11="+cp_ToStrODBC(this.w_MCIMPA11)+;
             ",MCIMPA12="+cp_ToStrODBC(this.w_MCIMPA12)+;
             ",MCIMPA20="+cp_ToStrODBC(this.w_MCIMPA20)+;
             ",MCIMPA13="+cp_ToStrODBC(this.w_MCIMPA13)+;
             ",MCIMPA21="+cp_ToStrODBC(this.w_MCIMPA21)+;
             ",MCIMPA14="+cp_ToStrODBC(this.w_MCIMPA14)+;
             ",MCIMPA17="+cp_ToStrODBC(this.w_MCIMPA17)+;
             ",MCFLPRIC="+cp_ToStrODBC(this.w_MCFLPRIC)+;
             ",MCAGGSTA="+cp_ToStrODBC(this.w_MCAGGSTA)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'MOV_CESP')
        i_cWhere = cp_PKFox(i_cTable  ,'MCSERIAL',this.w_MCSERIAL  )
        UPDATE (i_cTable) SET;
              MCVALNAZ=this.w_MCVALNAZ;
             ,MCNUMREG=this.w_MCNUMREG;
             ,MCCODESE=this.w_MCCODESE;
             ,MCDATREG=this.w_MCDATREG;
             ,MCNUMDOC=this.w_MCNUMDOC;
             ,MCALFDOC=this.w_MCALFDOC;
             ,MCDATDOC=this.w_MCDATDOC;
             ,MCCODCAU=this.w_MCCODCAU;
             ,MCDTPRIC=this.w_MCDTPRIC;
             ,MCDATDIS=this.w_MCDATDIS;
             ,MCCOMPET=this.w_MCCOMPET;
             ,MCFLPRIU=this.w_MCFLPRIU;
             ,MCDTPRIU=this.w_MCDTPRIU;
             ,MCTIPCON=this.w_MCTIPCON;
             ,MCDESMOV=this.w_MCDESMOV;
             ,MCDESMO2=this.w_MCDESMO2;
             ,MCCODCON=this.w_MCCODCON;
             ,MCCODCES=this.w_MCCODCES;
             ,MCSTATUS=this.w_MCSTATUS;
             ,MCVOCCEN=this.w_MCVOCCEN;
             ,MCCODCEN=this.w_MCCODCEN;
             ,MCCODCOM=this.w_MCCODCOM;
             ,MCRIFCON=this.w_MCRIFCON;
             ,MCRIFFAT=this.w_MCRIFFAT;
             ,MCDATCON=this.w_MCDATCON;
             ,MCSTABEN=this.w_MCSTABEN;
             ,MCQTACES=this.w_MCQTACES;
             ,MCCODVAL=this.w_MCCODVAL;
             ,MCCAOVAL=this.w_MCCAOVAL;
             ,MCIMPA01=this.w_MCIMPA01;
             ,MCIMPA23=this.w_MCIMPA23;
             ,MCIMPA02=this.w_MCIMPA02;
             ,MCIMPA24=this.w_MCIMPA24;
             ,MCIMPA03=this.w_MCIMPA03;
             ,MCIMPA18=this.w_MCIMPA18;
             ,MCIMPA04=this.w_MCIMPA04;
             ,MCIMPA22=this.w_MCIMPA22;
             ,MCIMPA05=this.w_MCIMPA05;
             ,MCIMPA19=this.w_MCIMPA19;
             ,MCIMPA06=this.w_MCIMPA06;
             ,MCIMPA07=this.w_MCIMPA07;
             ,MCCOECIV=this.w_MCCOECIV;
             ,MCIMPA08=this.w_MCIMPA08;
             ,MCIMPA09=this.w_MCIMPA09;
             ,MCCOEFIS=this.w_MCCOEFIS;
             ,MCIMPA10=this.w_MCIMPA10;
             ,MCIMPA15=this.w_MCIMPA15;
             ,MCCOEFI1=this.w_MCCOEFI1;
             ,MCIMPA16=this.w_MCIMPA16;
             ,MCIMPA11=this.w_MCIMPA11;
             ,MCIMPA12=this.w_MCIMPA12;
             ,MCIMPA20=this.w_MCIMPA20;
             ,MCIMPA13=this.w_MCIMPA13;
             ,MCIMPA21=this.w_MCIMPA21;
             ,MCIMPA14=this.w_MCIMPA14;
             ,MCIMPA17=this.w_MCIMPA17;
             ,MCFLPRIC=this.w_MCFLPRIC;
             ,MCAGGSTA=this.w_MCAGGSTA;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSCE_MMC : Saving
      this.GSCE_MMC.ChangeRow(this.cRowID+'      1',0;
             ,this.w_MCSERIAL,"MCSERIAL";
             )
      this.GSCE_MMC.mReplace()
      * --- GSCE_MMS : Saving
      this.GSCE_MMS.ChangeRow(this.cRowID+'      1',0;
             ,this.w_MCSERIAL,"SCSERIAL";
             )
      this.GSCE_MMS.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- gsce_amc
    if  NOT bTrsErr
        * --- controlla in caso di Componenti
        this.NotifyEvent('ControlliFinali')
        *Al termine della cancellazione interrogo i saldi per vedere
        *se la quantit� � in negativo.
        this.NotifyEvent('CheckQTA')
        if this.w_BLOCCA
           obj = this.getctrl('w_MCDATDIS')
           obj.SetFocus()
           release obj
        endif
    endif
    * valorizzo maschera documenti o Primanota
    If this.cfunction='Load'
     if type('This.w_OBJMASK.cPrg')='C' and This.w_BLOCCA=.F.
       If not empty(This.w_ORIGINE)
         If This.w_ORIGINE='D'
           This.w_OBJMASK.w_MVCESSER=this.w_MCSERIAL
           This.w_OBJMASK.w_MVCODCES=this.w_MCCODCES
           This.w_OBJMASK.w_CETIPO= this.w_TIPCES
           This.w_OBJMASK.w_STABEN= this.w_STABEN
           l_CTRL=This.w_OBJMASK.GETCTRL('w_MVCODCES')
           l_CTRL.CHECK()
           IF EMPTY(This.w_OBJMASK.w_MVCODCES)
             ah_ErrorMsg("Cespite inesistente o di tipo non valido")
             This.w_OBJMASK.w_MVCESSER=SPACE(10)
           ENDIF
         else
           This.w_OBJMASK.AddRow()
           This.w_OBJMASK.w_ACMOVCES=this.w_MCSERIAL
           This.w_OBJMASK.w_ACTIPASS='M'
         Endif
       Endif
       This.w_OBJMASK.mcalc(.T.)
       This.w_OBJMASK.NotifyEvent('Valorizzacespiti')
     Endif
    Endif
    
    
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Restore Transaction
  proc mRestoreTrs(i_bCanSkip)
    local i_cWhere,i_cF,i_nConn,i_cTable
    local i_cOp1,i_cOp2,i_cOp3,i_cOp4,i_cOp5,i_cOp6,i_cOp7,i_cOp8,i_cOp9,i_cOp10,i_cOp11,i_cOp12,i_cOp13,i_cOp14,i_cOp15,i_cOp16,i_cOp17,i_cOp18,i_cOp19,i_cOp20,i_cOp21,i_cOp22,i_cOp23

    i_cF=this.cCursor
    i_nConn = i_TableProp[this.CES_PITI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CES_PITI_IDX,2])
    if i_nConn<>0
      local i_bSkip
      i_bSkip=i_bCanSkip ;
              and NVL(&i_cF..MCFLPRIU,space(1))==this.w_MCFLPRIU;
              and NVL(&i_cF..MCDTPRIU,ctod("  /  /  "))==this.w_MCDTPRIU;
              and NVL(&i_cF..MCFLPRIU,space(1))==this.w_MCFLPRIU;
              and NVL(&i_cF..MCCOMPET,space(4))==this.w_MCCOMPET;
              and NVL(&i_cF..MCAGGSTA,space(1))==this.w_MCAGGSTA;
              and NVL(&i_cF..MCSTABEN,space(1))==this.w_MCSTABEN;
              and NVL(&i_cF..MCFLPRIC,space(1))==this.w_MCFLPRIC;
              and NVL(&i_cF..MCDTPRIC,ctod("  /  /  "))==this.w_MCDTPRIC;
              and NVL(&i_cF..MCFLPRIC,space(1))==this.w_MCFLPRIC;
              and NVL(&i_cF..MCCOMPET,space(4))==this.w_MCCOMPET;
              and NVL(&i_cF..MCCODCES,space(20))==this.w_MCCODCES;

      i_cOp1=cp_SetTrsOp(NVL(&i_cF..MCFLPRIU,space(1)),'CEDTPRIU','',NVL(&i_cF..MCDTPRIU,ctod("  /  /  ")),'restore',i_nConn)
      i_cOp2=cp_SetTrsOp(NVL(&i_cF..MCFLPRIU,space(1)),'CEESPRIU','',NVL(&i_cF..MCCOMPET,space(4)),'restore',i_nConn)
      i_cOp3=cp_SetTrsOp(NVL(&i_cF..MCAGGSTA,space(1)),'CESTABEN','',NVL(&i_cF..MCSTABEN,space(1)),'restore',i_nConn)
      i_cOp4=cp_SetTrsOp(NVL(&i_cF..MCFLPRIC,space(1)),'CEDTPRIC','',NVL(&i_cF..MCDTPRIC,ctod("  /  /  ")),'restore',i_nConn)
      i_cOp5=cp_SetTrsOp(NVL(&i_cF..MCFLPRIC,space(1)),'CEESPRIC','',NVL(&i_cF..MCCOMPET,space(4)),'restore',i_nConn)
      Local i_cValueForTrsact
      i_cValueForTrsact=NVL(&i_cF..MCCODCES,space(20))
      if !i_bSkip and !Empty(i_cValueForTrsact)
        =cp_TrsSQL(i_nConn,"UPDATE "+i_cTable+" SET ";
           +" CEDTPRIU="+i_cOp1+","           +" CEESPRIU="+i_cOp2+","           +" CESTABEN="+i_cOp3+","           +" CEDTPRIC="+i_cOp4+","           +" CEESPRIC="+i_cOp5+","  +"CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+" WHERE CECODICE="+cp_ToStrODBC(NVL(&i_cF..MCCODCES,space(20)));
             )
      endif
    else
      i_cOp1=cp_SetTrsOp(&i_cF..MCFLPRIU,'CEDTPRIU',i_cF+'.MCDTPRIU',&i_cF..MCDTPRIU,'restore',0)
      i_cOp2=cp_SetTrsOp(&i_cF..MCFLPRIU,'CEESPRIU',i_cF+'.MCCOMPET',&i_cF..MCCOMPET,'restore',0)
      i_cOp3=cp_SetTrsOp(&i_cF..MCAGGSTA,'CESTABEN',i_cF+'.MCSTABEN',&i_cF..MCSTABEN,'restore',0)
      i_cOp4=cp_SetTrsOp(&i_cF..MCFLPRIC,'CEDTPRIC',i_cF+'.MCDTPRIC',&i_cF..MCDTPRIC,'restore',0)
      i_cOp5=cp_SetTrsOp(&i_cF..MCFLPRIC,'CEESPRIC',i_cF+'.MCCOMPET',&i_cF..MCCOMPET,'restore',0)
      i_cWhere = cp_PKFox(i_cTable;
                 ,'CECODICE',&i_cF..MCCODCES)
      UPDATE (i_cTable) SET ;
           CEDTPRIU=&i_cOp1.  ,;
           CEESPRIU=&i_cOp2.  ,;
           CESTABEN=&i_cOp3.  ,;
           CEDTPRIC=&i_cOp4.  ,;
           CEESPRIC=&i_cOp5.  ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere
    endif
    i_nConn = i_TableProp[this.SAL_CESP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SAL_CESP_IDX,2])
    if i_nConn<>0
      local i_bSkip
      i_bSkip=i_bCanSkip ;
              and NVL(&i_cF..MCIMPA02,0)==this.w_MCIMPA02;
              and NVL(&i_cF..MCIMPA03,0)==this.w_MCIMPA03;
              and NVL(&i_cF..MCIMPA04,0)==this.w_MCIMPA04;
              and NVL(&i_cF..MCIMPA05,0)==this.w_MCIMPA05;
              and NVL(&i_cF..MCIMPA06,0)==this.w_MCIMPA06;
              and NVL(&i_cF..MCIMPA07,0)==this.w_MCIMPA07;
              and NVL(&i_cF..MCIMPA08,0)==this.w_MCIMPA08;
              and NVL(&i_cF..MCIMPA09,0)==this.w_MCIMPA09;
              and NVL(&i_cF..MCIMPA10,0)==this.w_MCIMPA10;
              and NVL(&i_cF..MCIMPA11,0)==this.w_MCIMPA11;
              and NVL(&i_cF..MCIMPA12,0)==this.w_MCIMPA12;
              and NVL(&i_cF..MCIMPA13,0)==this.w_MCIMPA13;
              and NVL(&i_cF..MCIMPA14,0)==this.w_MCIMPA14;
              and NVL(&i_cF..MCIMPA15,0)==this.w_MCIMPA15;
              and NVL(&i_cF..MCIMPA16,0)==this.w_MCIMPA16;
              and NVL(&i_cF..MCQTACES,0)==this.w_MCQTACES;
              and NVL(&i_cF..MCIMPA19,0)==this.w_MCIMPA19;
              and NVL(&i_cF..MCIMPA20,0)==this.w_MCIMPA20;
              and NVL(&i_cF..MCIMPA21,0)==this.w_MCIMPA21;
              and NVL(&i_cF..MCIMPA22,0)==this.w_MCIMPA22;
              and NVL(&i_cF..MCIMPA23,0)==this.w_MCIMPA23;
              and NVL(&i_cF..MCIMPA24,0)==this.w_MCIMPA24;
              and NVL(&i_cF..MCIMPA18,0)==this.w_MCIMPA18;
              and NVL(&i_cF..MCCOMPET,space(4))==this.w_MCCOMPET;
              and NVL(&i_cF..MCCODCES,space(20))==this.w_MCCODCES;

      Local i_cValueForTrsact
      i_cValueForTrsact=NVL(&i_cF..MCCOMPET,space(4))
      if !i_bSkip and !Empty(i_cValueForTrsact)
        =cp_TrsSQL(i_nConn,"UPDATE "+i_cTable+" SET ";
           +" SCINCVAL=SCINCVAL - "+cp_ToStrODBC(NVL(&i_cF..MCIMPA02,0))+","           +" SCIMPONS=SCIMPONS - "+cp_ToStrODBC(NVL(&i_cF..MCIMPA03,0))+","           +" SCIMPRIV=SCIMPRIV - "+cp_ToStrODBC(NVL(&i_cF..MCIMPA04,0))+","           +" SCDECVAL=SCDECVAL - "+cp_ToStrODBC(NVL(&i_cF..MCIMPA05,0))+","           +" SCIMPSVA=SCIMPSVA - "+cp_ToStrODBC(NVL(&i_cF..MCIMPA06,0))+","           +" SCACCCIV=SCACCCIV - "+cp_ToStrODBC(NVL(&i_cF..MCIMPA07,0))+","           +" SCUTICIV=SCUTICIV - "+cp_ToStrODBC(NVL(&i_cF..MCIMPA08,0))+","           +" SCACCFIS=SCACCFIS - "+cp_ToStrODBC(NVL(&i_cF..MCIMPA09,0))+","           +" SCUTIFIS=SCUTIFIS - "+cp_ToStrODBC(NVL(&i_cF..MCIMPA10,0))+","           +" SCIMPNOA=SCIMPNOA - "+cp_ToStrODBC(NVL(&i_cF..MCIMPA11,0))+","           +" SCQUOPER=SCQUOPER - "+cp_ToStrODBC(NVL(&i_cF..MCIMPA12,0))+","           +" SCPLUSVA=SCPLUSVA - "+cp_ToStrODBC(NVL(&i_cF..MCIMPA13,0))+","           +" SCMINSVA=SCMINSVA - "+cp_ToStrODBC(NVL(&i_cF..MCIMPA14,0))+","           +" SCACCANT=SCACCANT - "+cp_ToStrODBC(NVL(&i_cF..MCIMPA15,0))+","           +" SCUTIANT=SCUTIANT - "+cp_ToStrODBC(NVL(&i_cF..MCIMPA16,0))+","           +" SCQTACES=SCQTACES - "+cp_ToStrODBC(NVL(&i_cF..MCQTACES,0))+","           +" SCIMPSVC=SCIMPSVC - "+cp_ToStrODBC(NVL(&i_cF..MCIMPA19,0))+","           +" SCPLUSVC=SCPLUSVC - "+cp_ToStrODBC(NVL(&i_cF..MCIMPA20,0))+","           +" SCMINSVC=SCMINSVC - "+cp_ToStrODBC(NVL(&i_cF..MCIMPA21,0))+","           +" SCDECVAC=SCDECVAC - "+cp_ToStrODBC(NVL(&i_cF..MCIMPA22,0))+","           +" SCINCVAC=SCINCVAC - "+cp_ToStrODBC(NVL(&i_cF..MCIMPA23,0))+","           +" SCIMPONC=SCIMPONC - "+cp_ToStrODBC(NVL(&i_cF..MCIMPA24,0))+","           +" SCIMPRIC=SCIMPRIC - "+cp_ToStrODBC(NVL(&i_cF..MCIMPA18,0))+","  +"CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+" WHERE SCCODESE="+cp_ToStrODBC(NVL(&i_cF..MCCOMPET,space(4)));
             +" AND SCCODCES="+cp_ToStrODBC(NVL(&i_cF..MCCODCES,space(20)));
             )
      endif
    else
      i_cWhere = cp_PKFox(i_cTable;
                 ,'SCCODCES',&i_cF..MCCODCES;
                 ,'SCCODESE',&i_cF..MCCOMPET)
      UPDATE (i_cTable) SET ;
           SCINCVAL=SCINCVAL-&i_cF..MCIMPA02  ,;
           SCIMPONS=SCIMPONS-&i_cF..MCIMPA03  ,;
           SCIMPRIV=SCIMPRIV-&i_cF..MCIMPA04  ,;
           SCDECVAL=SCDECVAL-&i_cF..MCIMPA05  ,;
           SCIMPSVA=SCIMPSVA-&i_cF..MCIMPA06  ,;
           SCACCCIV=SCACCCIV-&i_cF..MCIMPA07  ,;
           SCUTICIV=SCUTICIV-&i_cF..MCIMPA08  ,;
           SCACCFIS=SCACCFIS-&i_cF..MCIMPA09  ,;
           SCUTIFIS=SCUTIFIS-&i_cF..MCIMPA10  ,;
           SCIMPNOA=SCIMPNOA-&i_cF..MCIMPA11  ,;
           SCQUOPER=SCQUOPER-&i_cF..MCIMPA12  ,;
           SCPLUSVA=SCPLUSVA-&i_cF..MCIMPA13  ,;
           SCMINSVA=SCMINSVA-&i_cF..MCIMPA14  ,;
           SCACCANT=SCACCANT-&i_cF..MCIMPA15  ,;
           SCUTIANT=SCUTIANT-&i_cF..MCIMPA16  ,;
           SCQTACES=SCQTACES-&i_cF..MCQTACES  ,;
           SCIMPSVC=SCIMPSVC-&i_cF..MCIMPA19  ,;
           SCPLUSVC=SCPLUSVC-&i_cF..MCIMPA20  ,;
           SCMINSVC=SCMINSVC-&i_cF..MCIMPA21  ,;
           SCDECVAC=SCDECVAC-&i_cF..MCIMPA22  ,;
           SCINCVAC=SCINCVAC-&i_cF..MCIMPA23  ,;
           SCIMPONC=SCIMPONC-&i_cF..MCIMPA24  ,;
           SCIMPRIC=SCIMPRIC-&i_cF..MCIMPA18  ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere
    endif
  endproc

  * --- Update Transaction
  proc mUpdateTrs(i_bCanSkip)
    local i_cWhere,i_cF,i_nModRow,i_nConn,i_cTable
    local i_cOp1,i_cOp2,i_cOp3,i_cOp4,i_cOp5,i_cOp6,i_cOp7,i_cOp8,i_cOp9,i_cOp10,i_cOp11,i_cOp12,i_cOp13,i_cOp14,i_cOp15,i_cOp16,i_cOp17,i_cOp18,i_cOp19,i_cOp20,i_cOp21,i_cOp22,i_cOp23

    i_cF=this.cCursor
    i_nConn = i_TableProp[this.CES_PITI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CES_PITI_IDX,2])
    if i_nConn<>0
      local i_bSkip
      i_bSkip=i_bCanSkip ;
              and NVL(&i_cF..MCFLPRIU,space(1))==this.w_MCFLPRIU;
              and NVL(&i_cF..MCDTPRIU,ctod("  /  /  "))==this.w_MCDTPRIU;
              and NVL(&i_cF..MCFLPRIU,space(1))==this.w_MCFLPRIU;
              and NVL(&i_cF..MCCOMPET,space(4))==this.w_MCCOMPET;
              and NVL(&i_cF..MCAGGSTA,space(1))==this.w_MCAGGSTA;
              and NVL(&i_cF..MCSTABEN,space(1))==this.w_MCSTABEN;
              and NVL(&i_cF..MCFLPRIC,space(1))==this.w_MCFLPRIC;
              and NVL(&i_cF..MCDTPRIC,ctod("  /  /  "))==this.w_MCDTPRIC;
              and NVL(&i_cF..MCFLPRIC,space(1))==this.w_MCFLPRIC;
              and NVL(&i_cF..MCCOMPET,space(4))==this.w_MCCOMPET;
              and NVL(&i_cF..MCCODCES,space(20))==this.w_MCCODCES;

      i_cOp1=cp_SetTrsOp(this.w_MCFLPRIU,'CEDTPRIU','this.w_MCDTPRIU',this.w_MCDTPRIU,'update',i_nConn)
      i_cOp2=cp_SetTrsOp(this.w_MCFLPRIU,'CEESPRIU','this.w_MCCOMPET',this.w_MCCOMPET,'update',i_nConn)
      i_cOp3=cp_SetTrsOp(this.w_MCAGGSTA,'CESTABEN','this.w_MCSTABEN',this.w_MCSTABEN,'update',i_nConn)
      i_cOp4=cp_SetTrsOp(this.w_MCFLPRIC,'CEDTPRIC','this.w_MCDTPRIC',this.w_MCDTPRIC,'update',i_nConn)
      i_cOp5=cp_SetTrsOp(this.w_MCFLPRIC,'CEESPRIC','this.w_MCCOMPET',this.w_MCCOMPET,'update',i_nConn)
      if !i_bSkip and !Empty(this.w_MCCODCES)
        i_nModRow=cp_TrsSQL(i_nConn,"UPDATE "+i_cTable+" SET"+;
         +" CEDTPRIU="+i_cOp1  +",";
         +" CEESPRIU="+i_cOp2  +",";
         +" CESTABEN="+i_cOp3  +",";
         +" CEDTPRIC="+i_cOp4  +",";
         +" CEESPRIC="+i_cOp5  +",";
         +" UTCV="+cp_ToStrODBC(i_codute)  +",";
         +" UTDV="+cp_ToStrODBC(SetInfoDate(This.cCalUtd))  +",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+" WHERE CECODICE="+cp_ToStrODBC(this.w_MCCODCES);
           )
      endif
    else
      i_cOp1=cp_SetTrsOp(this.w_MCFLPRIU,'CEDTPRIU','this.w_MCDTPRIU',this.w_MCDTPRIU,'update',0)
      i_cOp2=cp_SetTrsOp(this.w_MCFLPRIU,'CEESPRIU','this.w_MCCOMPET',this.w_MCCOMPET,'update',0)
      i_cOp3=cp_SetTrsOp(this.w_MCAGGSTA,'CESTABEN','this.w_MCSTABEN',this.w_MCSTABEN,'update',0)
      i_cOp4=cp_SetTrsOp(this.w_MCFLPRIC,'CEDTPRIC','this.w_MCDTPRIC',this.w_MCDTPRIC,'update',0)
      i_cOp5=cp_SetTrsOp(this.w_MCFLPRIC,'CEESPRIC','this.w_MCCOMPET',this.w_MCCOMPET,'update',0)
      i_cWhere = cp_PKFox(i_cTable;
                 ,'CECODICE',this.w_MCCODCES)
      UPDATE (i_cTable) SET;
           CEDTPRIU=&i_cOp1.  ,;
           CEESPRIU=&i_cOp2.  ,;
           CESTABEN=&i_cOp3.  ,;
           CEDTPRIC=&i_cOp4.  ,;
           CEESPRIC=&i_cOp5.  ,;
           UTCV=i_codute  ,;
           UTDV=SetInfoDate(This.cCalUtd)  ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere
    endif
    i_nConn = i_TableProp[this.SAL_CESP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SAL_CESP_IDX,2])
    if i_nConn<>0
      local i_bSkip
      i_bSkip=i_bCanSkip ;
              and NVL(&i_cF..MCIMPA02,0)==this.w_MCIMPA02;
              and NVL(&i_cF..MCIMPA03,0)==this.w_MCIMPA03;
              and NVL(&i_cF..MCIMPA04,0)==this.w_MCIMPA04;
              and NVL(&i_cF..MCIMPA05,0)==this.w_MCIMPA05;
              and NVL(&i_cF..MCIMPA06,0)==this.w_MCIMPA06;
              and NVL(&i_cF..MCIMPA07,0)==this.w_MCIMPA07;
              and NVL(&i_cF..MCIMPA08,0)==this.w_MCIMPA08;
              and NVL(&i_cF..MCIMPA09,0)==this.w_MCIMPA09;
              and NVL(&i_cF..MCIMPA10,0)==this.w_MCIMPA10;
              and NVL(&i_cF..MCIMPA11,0)==this.w_MCIMPA11;
              and NVL(&i_cF..MCIMPA12,0)==this.w_MCIMPA12;
              and NVL(&i_cF..MCIMPA13,0)==this.w_MCIMPA13;
              and NVL(&i_cF..MCIMPA14,0)==this.w_MCIMPA14;
              and NVL(&i_cF..MCIMPA15,0)==this.w_MCIMPA15;
              and NVL(&i_cF..MCIMPA16,0)==this.w_MCIMPA16;
              and NVL(&i_cF..MCQTACES,0)==this.w_MCQTACES;
              and NVL(&i_cF..MCIMPA19,0)==this.w_MCIMPA19;
              and NVL(&i_cF..MCIMPA20,0)==this.w_MCIMPA20;
              and NVL(&i_cF..MCIMPA21,0)==this.w_MCIMPA21;
              and NVL(&i_cF..MCIMPA22,0)==this.w_MCIMPA22;
              and NVL(&i_cF..MCIMPA23,0)==this.w_MCIMPA23;
              and NVL(&i_cF..MCIMPA24,0)==this.w_MCIMPA24;
              and NVL(&i_cF..MCIMPA18,0)==this.w_MCIMPA18;
              and NVL(&i_cF..MCCOMPET,space(4))==this.w_MCCOMPET;
              and NVL(&i_cF..MCCODCES,space(20))==this.w_MCCODCES;

      if !i_bSkip and !Empty(this.w_MCCOMPET)
        i_nModRow=cp_TrsSQL(i_nConn,"UPDATE "+i_cTable+" SET"+;
         +" SCINCVAL=SCINCVAL + "+cp_ToStrODBC(this.w_MCIMPA02)  +",";
         +" SCIMPONS=SCIMPONS + "+cp_ToStrODBC(this.w_MCIMPA03)  +",";
         +" SCIMPRIV=SCIMPRIV + "+cp_ToStrODBC(this.w_MCIMPA04)  +",";
         +" SCDECVAL=SCDECVAL + "+cp_ToStrODBC(this.w_MCIMPA05)  +",";
         +" SCIMPSVA=SCIMPSVA + "+cp_ToStrODBC(this.w_MCIMPA06)  +",";
         +" SCACCCIV=SCACCCIV + "+cp_ToStrODBC(this.w_MCIMPA07)  +",";
         +" SCUTICIV=SCUTICIV + "+cp_ToStrODBC(this.w_MCIMPA08)  +",";
         +" SCACCFIS=SCACCFIS + "+cp_ToStrODBC(this.w_MCIMPA09)  +",";
         +" SCUTIFIS=SCUTIFIS + "+cp_ToStrODBC(this.w_MCIMPA10)  +",";
         +" SCIMPNOA=SCIMPNOA + "+cp_ToStrODBC(this.w_MCIMPA11)  +",";
         +" SCQUOPER=SCQUOPER + "+cp_ToStrODBC(this.w_MCIMPA12)  +",";
         +" SCPLUSVA=SCPLUSVA + "+cp_ToStrODBC(this.w_MCIMPA13)  +",";
         +" SCMINSVA=SCMINSVA + "+cp_ToStrODBC(this.w_MCIMPA14)  +",";
         +" SCACCANT=SCACCANT + "+cp_ToStrODBC(this.w_MCIMPA15)  +",";
         +" SCUTIANT=SCUTIANT + "+cp_ToStrODBC(this.w_MCIMPA16)  +",";
         +" SCQTACES=SCQTACES + "+cp_ToStrODBC(this.w_MCQTACES)  +",";
         +" SCIMPSVC=SCIMPSVC + "+cp_ToStrODBC(this.w_MCIMPA19)  +",";
         +" SCPLUSVC=SCPLUSVC + "+cp_ToStrODBC(this.w_MCIMPA20)  +",";
         +" SCMINSVC=SCMINSVC + "+cp_ToStrODBC(this.w_MCIMPA21)  +",";
         +" SCDECVAC=SCDECVAC + "+cp_ToStrODBC(this.w_MCIMPA22)  +",";
         +" SCINCVAC=SCINCVAC + "+cp_ToStrODBC(this.w_MCIMPA23)  +",";
         +" SCIMPONC=SCIMPONC + "+cp_ToStrODBC(this.w_MCIMPA24)  +",";
         +" SCIMPRIC=SCIMPRIC + "+cp_ToStrODBC(this.w_MCIMPA18)  +",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+" WHERE SCCODESE="+cp_ToStrODBC(this.w_MCCOMPET);
           +" AND SCCODCES="+cp_ToStrODBC(this.w_MCCODCES);
           )
        if i_nModRow<1 .and. .not. empty(this.w_MCCOMPET)
          =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" (SCCODESE,SCCODCES  ;
             ,SCINCVAL"+",SCIMPONS"+",SCIMPRIV"+",SCDECVAL"+",SCIMPSVA"+",SCACCCIV"+",SCUTICIV"+",SCACCFIS"+",SCUTIFIS"+",SCIMPNOA"+",SCQUOPER"+",SCPLUSVA"+",SCMINSVA"+",SCACCANT"+",SCUTIANT"+",SCQTACES"+",SCIMPSVC"+",SCPLUSVC"+",SCMINSVC"+",SCDECVAC"+",SCINCVAC"+",SCIMPONC"+",SCIMPRIC,CPCCCHK) VALUES ("+cp_ToStrODBC(this.w_MCCOMPET)+","+cp_ToStrODBC(this.w_MCCODCES)  ;
             +","+cp_ToStrODBC(this.w_MCIMPA02);
             +","+cp_ToStrODBC(this.w_MCIMPA03);
             +","+cp_ToStrODBC(this.w_MCIMPA04);
             +","+cp_ToStrODBC(this.w_MCIMPA05);
             +","+cp_ToStrODBC(this.w_MCIMPA06);
             +","+cp_ToStrODBC(this.w_MCIMPA07);
             +","+cp_ToStrODBC(this.w_MCIMPA08);
             +","+cp_ToStrODBC(this.w_MCIMPA09);
             +","+cp_ToStrODBC(this.w_MCIMPA10);
             +","+cp_ToStrODBC(this.w_MCIMPA11);
             +","+cp_ToStrODBC(this.w_MCIMPA12);
             +","+cp_ToStrODBC(this.w_MCIMPA13);
             +","+cp_ToStrODBC(this.w_MCIMPA14);
             +","+cp_ToStrODBC(this.w_MCIMPA15);
             +","+cp_ToStrODBC(this.w_MCIMPA16);
             +","+cp_ToStrODBC(this.w_MCQTACES);
             +","+cp_ToStrODBC(this.w_MCIMPA19);
             +","+cp_ToStrODBC(this.w_MCIMPA20);
             +","+cp_ToStrODBC(this.w_MCIMPA21);
             +","+cp_ToStrODBC(this.w_MCIMPA22);
             +","+cp_ToStrODBC(this.w_MCIMPA23);
             +","+cp_ToStrODBC(this.w_MCIMPA24);
             +","+cp_ToStrODBC(this.w_MCIMPA18);
             +","+cp_ToStrODBC(cp_NewCCChk())+")")
        endif
      endif
    else
      i_cWhere = cp_PKFox(i_cTable;
                 ,'SCCODCES',this.w_MCCODCES;
                 ,'SCCODESE',this.w_MCCOMPET)
      UPDATE (i_cTable) SET;
           SCINCVAL=SCINCVAL+this.w_MCIMPA02  ,;
           SCIMPONS=SCIMPONS+this.w_MCIMPA03  ,;
           SCIMPRIV=SCIMPRIV+this.w_MCIMPA04  ,;
           SCDECVAL=SCDECVAL+this.w_MCIMPA05  ,;
           SCIMPSVA=SCIMPSVA+this.w_MCIMPA06  ,;
           SCACCCIV=SCACCCIV+this.w_MCIMPA07  ,;
           SCUTICIV=SCUTICIV+this.w_MCIMPA08  ,;
           SCACCFIS=SCACCFIS+this.w_MCIMPA09  ,;
           SCUTIFIS=SCUTIFIS+this.w_MCIMPA10  ,;
           SCIMPNOA=SCIMPNOA+this.w_MCIMPA11  ,;
           SCQUOPER=SCQUOPER+this.w_MCIMPA12  ,;
           SCPLUSVA=SCPLUSVA+this.w_MCIMPA13  ,;
           SCMINSVA=SCMINSVA+this.w_MCIMPA14  ,;
           SCACCANT=SCACCANT+this.w_MCIMPA15  ,;
           SCUTIANT=SCUTIANT+this.w_MCIMPA16  ,;
           SCQTACES=SCQTACES+this.w_MCQTACES  ,;
           SCIMPSVC=SCIMPSVC+this.w_MCIMPA19  ,;
           SCPLUSVC=SCPLUSVC+this.w_MCIMPA20  ,;
           SCMINSVC=SCMINSVC+this.w_MCIMPA21  ,;
           SCDECVAC=SCDECVAC+this.w_MCIMPA22  ,;
           SCINCVAC=SCINCVAC+this.w_MCIMPA23  ,;
           SCIMPONC=SCIMPONC+this.w_MCIMPA24  ,;
           SCIMPRIC=SCIMPRIC+this.w_MCIMPA18  ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere
      i_nModRow = _tally
      if i_nModRow<1 .and. .not. empty(this.w_MCCOMPET)
      cp_CheckDeletedKey(i_cTable,0,'SCCODCES',this.w_MCCODCES,'SCCODESE',this.w_MCCOMPET)
        INSERT INTO (i_cTable) (SCCODESE,SCCODCES  ;
         ,SCINCVAL,SCIMPONS,SCIMPRIV,SCDECVAL,SCIMPSVA,SCACCCIV,SCUTICIV,SCACCFIS,SCUTIFIS,SCIMPNOA,SCQUOPER,SCPLUSVA,SCMINSVA,SCACCANT,SCUTIANT,SCQTACES,SCIMPSVC,SCPLUSVC,SCMINSVC,SCDECVAC,SCINCVAC,SCIMPONC,SCIMPRIC,CPCCCHK) VALUES (this.w_MCCOMPET,this.w_MCCODCES  ;
           ,this.w_MCIMPA02  ;
           ,this.w_MCIMPA03  ;
           ,this.w_MCIMPA04  ;
           ,this.w_MCIMPA05  ;
           ,this.w_MCIMPA06  ;
           ,this.w_MCIMPA07  ;
           ,this.w_MCIMPA08  ;
           ,this.w_MCIMPA09  ;
           ,this.w_MCIMPA10  ;
           ,this.w_MCIMPA11  ;
           ,this.w_MCIMPA12  ;
           ,this.w_MCIMPA13  ;
           ,this.w_MCIMPA14  ;
           ,this.w_MCIMPA15  ;
           ,this.w_MCIMPA16  ;
           ,this.w_MCQTACES  ;
           ,this.w_MCIMPA19  ;
           ,this.w_MCIMPA20  ;
           ,this.w_MCIMPA21  ;
           ,this.w_MCIMPA22  ;
           ,this.w_MCIMPA23  ;
           ,this.w_MCIMPA24  ;
           ,this.w_MCIMPA18  ,cp_NewCCChk())
      endif
    endif
  endproc

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- gsce_amc
    * --- Cancellazione Componenti
    * --- Non ho potuto utilizzare la delete start perch� effettuo dei calcoli
    * --- sul figlio
     this.NotifyEvent('Cancellazione')
    * --- Fine Area Manuale
    * --- GSCE_MMC : Deleting
    this.GSCE_MMC.ChangeRow(this.cRowID+'      1',0;
           ,this.w_MCSERIAL,"MCSERIAL";
           )
    this.GSCE_MMC.mDelete()
    * --- GSCE_MMS : Deleting
    this.GSCE_MMS.ChangeRow(this.cRowID+'      1',0;
           ,this.w_MCSERIAL,"SCSERIAL";
           )
    this.GSCE_MMS.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.MOV_CESP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MOV_CESP_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.MOV_CESP_IDX,i_nConn)
      *
      * delete MOV_CESP
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'MCSERIAL',this.w_MCSERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- gsce_amc
    *Al termine della cancellazione interrogo i saldi per vedere
    *se la quantit� � in negativo.
    this.NotifyEvent('CheckQTA')
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.MOV_CESP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOV_CESP_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_2.Calculate()
        .DoRTCalc(1,4,.t.)
          .link_1_6('Full')
        .DoRTCalc(6,7,.t.)
            .w_TIPOPE = this.cFunction
        .DoRTCalc(9,17,.t.)
        if .o_MCDATDOC<>.w_MCDATDOC.or. .o_MCDATREG<>.w_MCDATREG.or. .o_FLPRIC<>.w_FLPRIC
            .w_MCDTPRIC = IIF(.w_FLPRIC='S' AND .w_PCTIPAMM<>'F',.w_MCDATREG,cp_CharToDate('  -  -  ') )
        endif
        .DoRTCalc(19,21,.t.)
        if .o_MCDTPRIU<>.w_MCDTPRIU.or. .o_FLPRIU<>.w_FLPRIU.or. .o_MCDATDIS<>.w_MCDATDIS.or. .o_FLDADI<>.w_FLDADI.or. .o_FLPRIC<>.w_FLPRIC.or. .o_MCDTPRIC<>.w_MCDTPRIC
            .w_MCCOMPET = iif(.w_FLPRIU="S",CALCESER(.w_MCDTPRIU,g_CODESE),iif(.w_FLPRIC="S",CALCESER(.w_MCDTPRIC,g_CODESE),iif(.w_FLDADI="S",CALCESER(.w_MCDATDIS,g_CODESE),g_CODESE)))
          .link_1_23('Full')
        endif
        .DoRTCalc(23,23,.t.)
            .w_MCFLPRIU = IIF(.w_FLPRIU='S' AND .w_PCTIPAMM<>'C', '=', ' ')
        if .o_MCCODCAU<>.w_MCCODCAU
          .link_1_26('Full')
        endif
        .DoRTCalc(26,27,.t.)
        if .o_MCCODCAU<>.w_MCCODCAU.or. .o_MCDATREG<>.w_MCDATREG
            .w_MCDTPRIU = IIF(.w_FLPRIU='S' AND .w_PCTIPAMM<>'C',.w_MCDATREG,cp_CharToDate('  -  -  ') )
        endif
        if .o_MCDTPRIU<>.w_MCDTPRIU.or. .o_MCCODCAU<>.w_MCCODCAU.or. .o_MCDATREG<>.w_MCDATREG
            .w_MCCOMPET = iif(.w_FLPRIU<>"S",g_CODESE,CALCESER(.w_MCDTPRIU, g_CODESE))
          .link_1_30('Full')
        endif
        .DoRTCalc(30,48,.t.)
        if .o_MCCODCAU<>.w_MCCODCAU
            .w_MCCODCON = SPACE(15)
          .link_1_50('Full')
        endif
        .DoRTCalc(50,58,.t.)
        if .o_MCVOCCEN<>.w_MCVOCCEN
            .w_MCVOCCEN = .w_MCVOCCEN
          .link_1_60('Full')
        endif
        .DoRTCalc(60,60,.t.)
        if .o_MCCODCEN<>.w_MCCODCEN
            .w_MCCODCEN = .w_MCCODCEN
          .link_1_62('Full')
        endif
        .DoRTCalc(62,62,.t.)
        if .o_MCCODCOM<>.w_MCCODCOM
            .w_MCCODCOM = .w_MCCODCOM
          .link_1_64('Full')
        endif
        .DoRTCalc(64,64,.t.)
            .w_TCAR = (.w_MCIMPA02+.w_MCIMPA03+.w_MCIMPA04)-.w_MCIMPA06
            .w_FLCAR = IIF(.w_TIPCES $ 'CC-PC' AND (.w_FLGA02='S' OR .w_FLGA23='S' OR .w_FLGA03='S' OR .w_FLGA24='S' OR .w_FLGA04='S' OR .w_FLGA18='S' OR .w_FLGA06='S' OR .w_FLGA19='S') AND .w_FLGA05<>'S' AND .w_FLGA22<>'S',.T.,.F.)
            .w_FLSCA = IIF(.w_TIPCES $ 'CC-PC' AND .w_FLGA02<>'S' AND .w_FLGA23<>'S' AND .w_FLGA03<>'S' AND .w_FLGA24<>'S' AND .w_FLGA04<>'S' AND .w_FLGA18<>'S' AND .w_FLGA06<>'S' AND .w_FLGA19<>'S' AND (.w_FLGA05='S' OR .w_FLGA22='S'),.T.,.F.)
        Local l_Dep1,l_Dep2
        l_Dep1= .o_MCIMPA01<>.w_MCIMPA01 .or. .o_MCIMPA02<>.w_MCIMPA02 .or. .o_MCIMPA03<>.w_MCIMPA03 .or. .o_MCIMPA04<>.w_MCIMPA04 .or. .o_MCIMPA05<>.w_MCIMPA05
        l_Dep2= .o_MCIMPA06<>.w_MCIMPA06 .or. .o_MCIMPA07<>.w_MCIMPA07 .or. .o_MCIMPA08<>.w_MCIMPA08
        if m.l_Dep1 .or. m.l_Dep2
            .w_CON1CAU = IIF(.w_Mcimpa01<>0 Or .w_Mcimpa02<>0 Or .w_Mcimpa03<>0 Or .w_Mcimpa04<>0 Or .w_Mcimpa05<>0 Or .w_Mcimpa06<>0 Or .w_Mcimpa07<>0 Or .w_Mcimpa08<>0,.F.,.T.)
        endif
        Local l_Dep1,l_Dep2
        l_Dep1= .o_MCIMPA09<>.w_MCIMPA09 .or. .o_MCIMPA10<>.w_MCIMPA10 .or. .o_MCIMPA11<>.w_MCIMPA11 .or. .o_MCIMPA12<>.w_MCIMPA12 .or. .o_MCIMPA13<>.w_MCIMPA13
        l_Dep2= .o_MCIMPA14<>.w_MCIMPA14 .or. .o_MCIMPA15<>.w_MCIMPA15 .or. .o_MCIMPA16<>.w_MCIMPA16
        if m.l_Dep1 .or. m.l_Dep2
            .w_CON2CAU = IIF(.w_Mcimpa09<>0 Or .w_Mcimpa10<>0 Or .w_Mcimpa11<>0 Or .w_Mcimpa12<>0 Or .w_Mcimpa13<>0 Or .w_Mcimpa14<>0 Or .w_Mcimpa15<>0 Or .w_Mcimpa16<>0,.F.,.T.)
        endif
        .DoRTCalc(70,104,.t.)
        if .o_MCSERIAL<>.w_MCSERIAL
          .link_1_117('Full')
        endif
        .DoRTCalc(106,110,.t.)
        if .o_MCSERIAL<>.w_MCSERIAL
            .w_ORIFCON = .w_MCRIFCON
        endif
            .w_MCSTABEN = 'U'
        .oPgFrm.Page1.oPag.oObj_1_139.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_140.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_141.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_142.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_143.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_144.Calculate()
        .DoRTCalc(113,120,.t.)
          .link_1_148('Full')
        .oPgFrm.Page1.oPag.oObj_1_156.Calculate()
        .DoRTCalc(122,130,.t.)
        if .o_MCCOMPET<>.w_MCCOMPET
          .link_2_20('Full')
        endif
        if .o_MCDATREG<>.w_MCDATREG.or. .o_MCDATDOC<>.w_MCDATDOC.or. .o_MCCODVAL<>.w_MCCODVAL
            .w_MCCAOVAL = GETCAM(.w_MCCODVAL, IIF(EMPTY(.w_MCDATDOC),.w_MCDATREG,.w_MCDATDOC), 7)
        endif
        .DoRTCalc(133,133,.t.)
        if .o_MCDATREG<>.w_MCDATREG.or. .o_MCDATDOC<>.w_MCDATDOC.or. .o_MCCODVAL<>.w_MCCODVAL
            .w_CAOVAL = GETCAM(.w_MCCODVAL, IIF(EMPTY(.w_MCDATDOC),.w_MCDATREG,.w_MCDATDOC), 7)
        endif
            .w_CALCPIC = DEFPIC(.w_DECTOT)
        .DoRTCalc(136,147,.t.)
            .w_CODCAU = .w_MCCODCAU
        if .o_MCCOMPET<>.w_MCCOMPET.or. .o_MCCODCES<>.w_MCCODCES
            .w_MCCOECIV = .w_TCOECIV
        endif
        .DoRTCalc(150,152,.t.)
        if .o_MCCOMPET<>.w_MCCOMPET.or. .o_MCCODCES<>.w_MCCODCES
            .w_MCCOEFIS = .w_TCOEFI1
        endif
        .DoRTCalc(154,155,.t.)
            .w_CODCES = .w_MCCODCES
        if .o_MCCOMPET<>.w_MCCOMPET.or. .o_MCCODCES<>.w_MCCODCES
            .w_MCCOEFI1 = .w_TCOEFI2
        endif
        .oPgFrm.Page2.oPag.oObj_2_64.Calculate()
        .DoRTCalc(158,176,.t.)
            .w_MCFLPRIC = IIF(.w_FLPRIC='S' AND .w_PCTIPAMM<>'F', '=', ' ')
        .DoRTCalc(178,193,.t.)
            .w_TCARC = (.w_MCIMPA23+.w_MCIMPA24+.w_MCIMPA04)-.w_MCIMPA19
        .DoRTCalc(195,195,.t.)
        if .o_FLPRIC<>.w_FLPRIC.or. .o_FLPRIU<>.w_FLPRIU
            .w_MCAGGSTA = IIF(.w_FLPRIC='S', '=', IIF(.w_FLPRIU='S','=',' ') )
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_codazi<>.w_codazi
           cp_AskTableProg(this,i_nConn,"SEMCE","i_codazi,w_MCSERIAL")
          .op_MCSERIAL = .w_MCSERIAL
        endif
        if .op_codazi<>.w_codazi .or. .op_MCCODESE<>.w_MCCODESE
           cp_AskTableProg(this,i_nConn,"PRMCE","i_codazi,w_MCCODESE,w_MCNUMREG")
          .op_MCNUMREG = .w_MCNUMREG
        endif
        .op_codazi = .w_codazi
        .op_codazi = .w_codazi
        .op_MCCODESE = .w_MCCODESE
      endwith
      this.DoRTCalc(197,202,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_2.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_139.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_140.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_141.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_142.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_143.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_144.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_156.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_64.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oMCCODESE_1_11.enabled = this.oPgFrm.Page1.oPag.oMCCODESE_1_11.mCond()
    this.oPgFrm.Page1.oPag.oMCCODCAU_1_18.enabled = this.oPgFrm.Page1.oPag.oMCCODCAU_1_18.mCond()
    this.oPgFrm.Page1.oPag.oMCDTPRIC_1_19.enabled = this.oPgFrm.Page1.oPag.oMCDTPRIC_1_19.mCond()
    this.oPgFrm.Page1.oPag.oMCDATDIS_1_21.enabled = this.oPgFrm.Page1.oPag.oMCDATDIS_1_21.mCond()
    this.oPgFrm.Page1.oPag.oMCDTPRIU_1_29.enabled = this.oPgFrm.Page1.oPag.oMCDTPRIU_1_29.mCond()
    this.oPgFrm.Page1.oPag.oMCCOMPET_1_30.enabled = this.oPgFrm.Page1.oPag.oMCCOMPET_1_30.mCond()
    this.oPgFrm.Page1.oPag.oMCCODCON_1_50.enabled = this.oPgFrm.Page1.oPag.oMCCODCON_1_50.mCond()
    this.oPgFrm.Page1.oPag.oMCVOCCEN_1_60.enabled = this.oPgFrm.Page1.oPag.oMCVOCCEN_1_60.mCond()
    this.oPgFrm.Page1.oPag.oMCCODCEN_1_62.enabled = this.oPgFrm.Page1.oPag.oMCCODCEN_1_62.mCond()
    this.oPgFrm.Page1.oPag.oMCQTACES_1_151.enabled = this.oPgFrm.Page1.oPag.oMCQTACES_1_151.mCond()
    this.oPgFrm.Page2.oPag.oMCCAOVAL_2_21.enabled = this.oPgFrm.Page2.oPag.oMCCAOVAL_2_21.mCond()
    this.oPgFrm.Page2.oPag.oMCIMPA01_2_28.enabled = this.oPgFrm.Page2.oPag.oMCIMPA01_2_28.mCond()
    this.oPgFrm.Page2.oPag.oMCIMPA23_2_29.enabled = this.oPgFrm.Page2.oPag.oMCIMPA23_2_29.mCond()
    this.oPgFrm.Page2.oPag.oMCIMPA02_2_30.enabled = this.oPgFrm.Page2.oPag.oMCIMPA02_2_30.mCond()
    this.oPgFrm.Page2.oPag.oMCIMPA24_2_31.enabled = this.oPgFrm.Page2.oPag.oMCIMPA24_2_31.mCond()
    this.oPgFrm.Page2.oPag.oMCIMPA03_2_32.enabled = this.oPgFrm.Page2.oPag.oMCIMPA03_2_32.mCond()
    this.oPgFrm.Page2.oPag.oMCIMPA18_2_33.enabled = this.oPgFrm.Page2.oPag.oMCIMPA18_2_33.mCond()
    this.oPgFrm.Page2.oPag.oMCIMPA04_2_35.enabled = this.oPgFrm.Page2.oPag.oMCIMPA04_2_35.mCond()
    this.oPgFrm.Page2.oPag.oMCIMPA22_2_36.enabled = this.oPgFrm.Page2.oPag.oMCIMPA22_2_36.mCond()
    this.oPgFrm.Page2.oPag.oMCIMPA05_2_37.enabled = this.oPgFrm.Page2.oPag.oMCIMPA05_2_37.mCond()
    this.oPgFrm.Page2.oPag.oMCIMPA19_2_38.enabled = this.oPgFrm.Page2.oPag.oMCIMPA19_2_38.mCond()
    this.oPgFrm.Page2.oPag.oMCIMPA06_2_39.enabled = this.oPgFrm.Page2.oPag.oMCIMPA06_2_39.mCond()
    this.oPgFrm.Page2.oPag.oMCIMPA07_2_40.enabled = this.oPgFrm.Page2.oPag.oMCIMPA07_2_40.mCond()
    this.oPgFrm.Page2.oPag.oMCCOECIV_2_42.enabled = this.oPgFrm.Page2.oPag.oMCCOECIV_2_42.mCond()
    this.oPgFrm.Page2.oPag.oMCIMPA08_2_43.enabled = this.oPgFrm.Page2.oPag.oMCIMPA08_2_43.mCond()
    this.oPgFrm.Page2.oPag.oMCIMPA09_2_44.enabled = this.oPgFrm.Page2.oPag.oMCIMPA09_2_44.mCond()
    this.oPgFrm.Page2.oPag.oMCCOEFIS_2_46.enabled = this.oPgFrm.Page2.oPag.oMCCOEFIS_2_46.mCond()
    this.oPgFrm.Page2.oPag.oMCIMPA10_2_48.enabled = this.oPgFrm.Page2.oPag.oMCIMPA10_2_48.mCond()
    this.oPgFrm.Page2.oPag.oMCIMPA15_2_50.enabled = this.oPgFrm.Page2.oPag.oMCIMPA15_2_50.mCond()
    this.oPgFrm.Page2.oPag.oMCCOEFI1_2_52.enabled = this.oPgFrm.Page2.oPag.oMCCOEFI1_2_52.mCond()
    this.oPgFrm.Page2.oPag.oMCIMPA16_2_53.enabled = this.oPgFrm.Page2.oPag.oMCIMPA16_2_53.mCond()
    this.oPgFrm.Page2.oPag.oMCIMPA11_2_55.enabled = this.oPgFrm.Page2.oPag.oMCIMPA11_2_55.mCond()
    this.oPgFrm.Page2.oPag.oMCIMPA12_2_56.enabled = this.oPgFrm.Page2.oPag.oMCIMPA12_2_56.mCond()
    this.oPgFrm.Page2.oPag.oMCIMPA20_2_57.enabled = this.oPgFrm.Page2.oPag.oMCIMPA20_2_57.mCond()
    this.oPgFrm.Page2.oPag.oMCIMPA13_2_58.enabled = this.oPgFrm.Page2.oPag.oMCIMPA13_2_58.mCond()
    this.oPgFrm.Page2.oPag.oMCIMPA21_2_59.enabled = this.oPgFrm.Page2.oPag.oMCIMPA21_2_59.mCond()
    this.oPgFrm.Page2.oPag.oMCIMPA14_2_60.enabled = this.oPgFrm.Page2.oPag.oMCIMPA14_2_60.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_135.enabled = this.oPgFrm.Page1.oPag.oBtn_1_135.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_62.enabled = this.oPgFrm.Page2.oPag.oBtn_2_62.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_63.enabled = this.oPgFrm.Page2.oPag.oBtn_2_63.mCond()
    this.oPgFrm.Page2.oPag.oLinkPC_2_34.enabled = this.oPgFrm.Page2.oPag.oLinkPC_2_34.mCond()
    this.oPgFrm.Page2.oPag.oLinkPC_2_61.enabled = this.oPgFrm.Page2.oPag.oLinkPC_2_61.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oMCDTPRIC_1_19.visible=!this.oPgFrm.Page1.oPag.oMCDTPRIC_1_19.mHide()
    this.oPgFrm.Page1.oPag.oMCDATDIS_1_21.visible=!this.oPgFrm.Page1.oPag.oMCDATDIS_1_21.mHide()
    this.oPgFrm.Page1.oPag.oMCCOMPET_1_23.visible=!this.oPgFrm.Page1.oPag.oMCCOMPET_1_23.mHide()
    this.oPgFrm.Page1.oPag.oMCDTPRIU_1_29.visible=!this.oPgFrm.Page1.oPag.oMCDTPRIU_1_29.mHide()
    this.oPgFrm.Page1.oPag.oMCCOMPET_1_30.visible=!this.oPgFrm.Page1.oPag.oMCCOMPET_1_30.mHide()
    this.oPgFrm.Page1.oPag.oMCCODCON_1_50.visible=!this.oPgFrm.Page1.oPag.oMCCODCON_1_50.mHide()
    this.oPgFrm.Page1.oPag.oDESCON_1_51.visible=!this.oPgFrm.Page1.oPag.oDESCON_1_51.mHide()
    this.oPgFrm.Page1.oPag.oMCVOCCEN_1_60.visible=!this.oPgFrm.Page1.oPag.oMCVOCCEN_1_60.mHide()
    this.oPgFrm.Page1.oPag.oDESVOC_1_61.visible=!this.oPgFrm.Page1.oPag.oDESVOC_1_61.mHide()
    this.oPgFrm.Page1.oPag.oMCCODCEN_1_62.visible=!this.oPgFrm.Page1.oPag.oMCCODCEN_1_62.mHide()
    this.oPgFrm.Page1.oPag.oDESCEN_1_63.visible=!this.oPgFrm.Page1.oPag.oDESCEN_1_63.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_79.visible=!this.oPgFrm.Page1.oPag.oStr_1_79.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_80.visible=!this.oPgFrm.Page1.oPag.oStr_1_80.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_124.visible=!this.oPgFrm.Page1.oPag.oStr_1_124.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_125.visible=!this.oPgFrm.Page1.oPag.oStr_1_125.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_127.visible=!this.oPgFrm.Page1.oPag.oStr_1_127.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_128.visible=!this.oPgFrm.Page1.oPag.oStr_1_128.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_135.visible=!this.oPgFrm.Page1.oPag.oBtn_1_135.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_145.visible=!this.oPgFrm.Page1.oPag.oStr_1_145.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_147.visible=!this.oPgFrm.Page1.oPag.oStr_1_147.mHide()
    this.oPgFrm.Page1.oPag.oUNIMIS_1_148.visible=!this.oPgFrm.Page1.oPag.oUNIMIS_1_148.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_149.visible=!this.oPgFrm.Page1.oPag.oStr_1_149.mHide()
    this.oPgFrm.Page1.oPag.oDESUNI_1_150.visible=!this.oPgFrm.Page1.oPag.oDESUNI_1_150.mHide()
    this.oPgFrm.Page1.oPag.oMCQTACES_1_151.visible=!this.oPgFrm.Page1.oPag.oMCQTACES_1_151.mHide()
    this.oPgFrm.Page2.oPag.oLinkPC_2_34.visible=!this.oPgFrm.Page2.oPag.oLinkPC_2_34.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_160.visible=!this.oPgFrm.Page1.oPag.oStr_1_160.mHide()
    this.oPgFrm.Page2.oPag.oLinkPC_2_61.visible=!this.oPgFrm.Page2.oPag.oLinkPC_2_61.mHide()
    this.oPgFrm.Page2.oPag.oBtn_2_63.visible=!this.oPgFrm.Page2.oPag.oBtn_2_63.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_169.visible=!this.oPgFrm.Page1.oPag.oBtn_1_169.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_171.visible=!this.oPgFrm.Page1.oPag.oStr_1_171.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_172.visible=!this.oPgFrm.Page1.oPag.oStr_1_172.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_2.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_139.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_140.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_141.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_142.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_143.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_144.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_156.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_64.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_CESP_IDX,3]
    i_lTable = "PAR_CESP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_CESP_IDX,2], .t., this.PAR_CESP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_CESP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PCCODAZI,PCTIPAMM";
                   +" from "+i_cTable+" "+i_lTable+" where PCCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PCCODAZI',this.w_CODAZI)
            select PCCODAZI,PCTIPAMM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.PCCODAZI,space(5))
      this.w_PCTIPAMM = NVL(_Link_.PCTIPAMM,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_PCTIPAMM = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_CESP_IDX,2])+'\'+cp_ToStr(_Link_.PCCODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_CESP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MCCODESE
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MCCODESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_MCCODESE)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_CODAZI;
                     ,'ESCODESE',trim(this.w_MCCODESE))
          select ESCODAZI,ESCODESE,ESINIESE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MCCODESE)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MCCODESE) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oMCCODESE_1_11'),i_cWhere,'',"Esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESINIESE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESINIESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MCCODESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_MCCODESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_MCCODESE)
            select ESCODAZI,ESCODESE,ESINIESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MCCODESE = NVL(_Link_.ESCODESE,space(4))
      this.w_PRIDAT = NVL(cp_ToDate(_Link_.ESINIESE),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_MCCODESE = space(4)
      endif
      this.w_PRIDAT = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MCCODESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MCCODCAU
  func Link_1_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CESP_IDX,3]
    i_lTable = "CAU_CESP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CESP_IDX,2], .t., this.CAU_CESP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CESP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MCCODCAU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCE_ACC',True,'CAU_CESP')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_MCCODCAU)+"%");

          i_ret=cp_SQL(i_nConn,"select *";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_MCCODCAU))
          select *;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MCCODCAU)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MCCODCAU) and !this.bDontReportError
            deferred_cp_zoom('CAU_CESP','*','CCCODICE',cp_AbsName(oSource.parent,'oMCCODCAU_1_18'),i_cWhere,'GSCE_ACC',"Causali cespiti",'GSCE_ZCA.CAU_CESP_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select *";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select *;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MCCODCAU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select *";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_MCCODCAU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_MCCODCAU)
            select *;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MCCODCAU = NVL(_Link_.CCCODICE,space(5))
      this.w_DESCAU = NVL(_Link_.CCDESCRI,space(40))
      this.w_MCTIPCON = NVL(_Link_.CCFLRIFE,space(1))
      this.w_FLGA01 = NVL(_Link_.CCFLGA01,space(1))
      this.w_FLGA02 = NVL(_Link_.CCFLGA02,space(1))
      this.w_FLGA03 = NVL(_Link_.CCFLGA03,space(1))
      this.w_FLGA04 = NVL(_Link_.CCFLGA04,space(1))
      this.w_FLGA05 = NVL(_Link_.CCFLGA05,space(1))
      this.w_FLGA06 = NVL(_Link_.CCFLGA06,space(1))
      this.w_FLGA07 = NVL(_Link_.CCFLGA07,space(1))
      this.w_FLGA08 = NVL(_Link_.CCFLGA08,space(1))
      this.w_FLGA09 = NVL(_Link_.CCFLGA09,space(1))
      this.w_FLGA10 = NVL(_Link_.CCFLGA10,space(1))
      this.w_FLGA11 = NVL(_Link_.CCFLGA11,space(1))
      this.w_FLGA12 = NVL(_Link_.CCFLGA12,space(1))
      this.w_FLGA13 = NVL(_Link_.CCFLGA13,space(1))
      this.w_FLGA14 = NVL(_Link_.CCFLGA14,space(1))
      this.w_DATAGG = NVL(_Link_.CCDATAGG,space(1))
      this.w_CAUCON = NVL(_Link_.CCCAUCON,space(5))
      this.w_FLPRIU = NVL(_Link_.CCFLPRIU,space(1))
      this.w_FLCONT = NVL(_Link_.CCFLCONT,space(1))
      this.w_FLGA15 = NVL(_Link_.CCFLGA15,space(1))
      this.w_FLGA16 = NVL(_Link_.CCFLGA16,space(1))
      this.w_FLDADI = NVL(_Link_.CCFLDADI,space(1))
      this.w_FLGA17 = NVL(_Link_.CCFLGA17,space(1))
      this.w_TIPOAGG = NVL(_Link_.CCFLGOPE,space(1))
      this.w_RIVALU = NVL(_Link_.CCRIVALU,space(10))
      this.w_DTOBSOCA = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
      this.w_FLGA18 = NVL(_Link_.CCFLGA18,space(1))
      this.w_FLGA19 = NVL(_Link_.CCFLGA19,space(1))
      this.w_FLGA20 = NVL(_Link_.CCFLGA20,space(1))
      this.w_FLGA21 = NVL(_Link_.CCFLGA21,space(1))
      this.w_FLGA22 = NVL(_Link_.CCFLGA22,space(1))
      this.w_FLPRIC = NVL(_Link_.CCFLPRIC,space(1))
      this.w_FLGA23 = NVL(_Link_.CCFLGA23,space(1))
      this.w_FLGA24 = NVL(_Link_.CCFLGA24,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MCCODCAU = space(5)
      endif
      this.w_DESCAU = space(40)
      this.w_MCTIPCON = space(1)
      this.w_FLGA01 = space(1)
      this.w_FLGA02 = space(1)
      this.w_FLGA03 = space(1)
      this.w_FLGA04 = space(1)
      this.w_FLGA05 = space(1)
      this.w_FLGA06 = space(1)
      this.w_FLGA07 = space(1)
      this.w_FLGA08 = space(1)
      this.w_FLGA09 = space(1)
      this.w_FLGA10 = space(1)
      this.w_FLGA11 = space(1)
      this.w_FLGA12 = space(1)
      this.w_FLGA13 = space(1)
      this.w_FLGA14 = space(1)
      this.w_DATAGG = space(1)
      this.w_CAUCON = space(5)
      this.w_FLPRIU = space(1)
      this.w_FLCONT = space(1)
      this.w_FLGA15 = space(1)
      this.w_FLGA16 = space(1)
      this.w_FLDADI = space(1)
      this.w_FLGA17 = space(1)
      this.w_TIPOAGG = space(1)
      this.w_RIVALU = space(10)
      this.w_DTOBSOCA = ctod("  /  /  ")
      this.w_FLGA18 = space(1)
      this.w_FLGA19 = space(1)
      this.w_FLGA20 = space(1)
      this.w_FLGA21 = space(1)
      this.w_FLGA22 = space(1)
      this.w_FLPRIC = space(1)
      this.w_FLGA23 = space(1)
      this.w_FLGA24 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DTOBSOCA>.w_MCDATREG OR EMPTY(.w_DTOBSOCA)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale cespite inesistente o obsoleta")
        endif
        this.w_MCCODCAU = space(5)
        this.w_DESCAU = space(40)
        this.w_MCTIPCON = space(1)
        this.w_FLGA01 = space(1)
        this.w_FLGA02 = space(1)
        this.w_FLGA03 = space(1)
        this.w_FLGA04 = space(1)
        this.w_FLGA05 = space(1)
        this.w_FLGA06 = space(1)
        this.w_FLGA07 = space(1)
        this.w_FLGA08 = space(1)
        this.w_FLGA09 = space(1)
        this.w_FLGA10 = space(1)
        this.w_FLGA11 = space(1)
        this.w_FLGA12 = space(1)
        this.w_FLGA13 = space(1)
        this.w_FLGA14 = space(1)
        this.w_DATAGG = space(1)
        this.w_CAUCON = space(5)
        this.w_FLPRIU = space(1)
        this.w_FLCONT = space(1)
        this.w_FLGA15 = space(1)
        this.w_FLGA16 = space(1)
        this.w_FLDADI = space(1)
        this.w_FLGA17 = space(1)
        this.w_TIPOAGG = space(1)
        this.w_RIVALU = space(10)
        this.w_DTOBSOCA = ctod("  /  /  ")
        this.w_FLGA18 = space(1)
        this.w_FLGA19 = space(1)
        this.w_FLGA20 = space(1)
        this.w_FLGA21 = space(1)
        this.w_FLGA22 = space(1)
        this.w_FLPRIC = space(1)
        this.w_FLGA23 = space(1)
        this.w_FLGA24 = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CESP_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CESP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MCCODCAU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_18(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 36 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAU_CESP_IDX,3] and i_nFlds+36<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAU_CESP_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_18.CCCODICE as CCCODICE118"+ ",link_1_18.CCDESCRI as CCDESCRI118"+ ",link_1_18.CCFLRIFE as CCFLRIFE118"+ ",link_1_18.CCFLGA01 as CCFLGA01118"+ ",link_1_18.CCFLGA02 as CCFLGA02118"+ ",link_1_18.CCFLGA03 as CCFLGA03118"+ ",link_1_18.CCFLGA04 as CCFLGA04118"+ ",link_1_18.CCFLGA05 as CCFLGA05118"+ ",link_1_18.CCFLGA06 as CCFLGA06118"+ ",link_1_18.CCFLGA07 as CCFLGA07118"+ ",link_1_18.CCFLGA08 as CCFLGA08118"+ ",link_1_18.CCFLGA09 as CCFLGA09118"+ ",link_1_18.CCFLGA10 as CCFLGA10118"+ ",link_1_18.CCFLGA11 as CCFLGA11118"+ ",link_1_18.CCFLGA12 as CCFLGA12118"+ ",link_1_18.CCFLGA13 as CCFLGA13118"+ ",link_1_18.CCFLGA14 as CCFLGA14118"+ ",link_1_18.CCDATAGG as CCDATAGG118"+ ",link_1_18.CCCAUCON as CCCAUCON118"+ ",link_1_18.CCFLPRIU as CCFLPRIU118"+ ",link_1_18.CCFLCONT as CCFLCONT118"+ ",link_1_18.CCFLGA15 as CCFLGA15118"+ ",link_1_18.CCFLGA16 as CCFLGA16118"+ ",link_1_18.CCFLDADI as CCFLDADI118"+ ",link_1_18.CCFLGA17 as CCFLGA17118"+ ",link_1_18.CCFLGOPE as CCFLGOPE118"+ ",link_1_18.CCRIVALU as CCRIVALU118"+ ",link_1_18.CCDTOBSO as CCDTOBSO118"+ ",link_1_18.CCFLGA18 as CCFLGA18118"+ ",link_1_18.CCFLGA19 as CCFLGA19118"+ ",link_1_18.CCFLGA20 as CCFLGA20118"+ ",link_1_18.CCFLGA21 as CCFLGA21118"+ ",link_1_18.CCFLGA22 as CCFLGA22118"+ ",link_1_18.CCFLPRIC as CCFLPRIC118"+ ",link_1_18.CCFLGA23 as CCFLGA23118"+ ",link_1_18.CCFLGA24 as CCFLGA24118"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_18 on MOV_CESP.MCCODCAU=link_1_18.CCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+36
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_18"
          i_cKey=i_cKey+'+" and MOV_CESP.MCCODCAU=link_1_18.CCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+36
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MCCOMPET
  func Link_1_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MCCOMPET) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_MCCOMPET)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ,ESINIESE,ESFINESE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_CODAZI;
                     ,'ESCODESE',trim(this.w_MCCOMPET))
          select ESCODAZI,ESCODESE,ESVALNAZ,ESINIESE,ESFINESE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MCCOMPET)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MCCOMPET) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oMCCOMPET_1_23'),i_cWhere,'',"Esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ,ESINIESE,ESFINESE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESVALNAZ,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Esercizio di competenza non congruente")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ,ESINIESE,ESFINESE";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESVALNAZ,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MCCOMPET)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ,ESINIESE,ESFINESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_MCCOMPET);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_MCCOMPET)
            select ESCODAZI,ESCODESE,ESVALNAZ,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MCCOMPET = NVL(_Link_.ESCODESE,space(4))
      this.w_MCVALNAZ = NVL(_Link_.ESVALNAZ,space(3))
      this.w_MCCODVAL = NVL(_Link_.ESVALNAZ,space(3))
      this.w_INIESE = NVL(cp_ToDate(_Link_.ESINIESE),ctod("  /  /  "))
      this.w_FINESE = NVL(cp_ToDate(_Link_.ESFINESE),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_MCCOMPET = space(4)
      endif
      this.w_MCVALNAZ = space(3)
      this.w_MCCODVAL = space(3)
      this.w_INIESE = ctod("  /  /  ")
      this.w_FINESE = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_FLPRIU<>'S' And .w_FLDADI<>'S' And .w_FLPRIC<>'S') or (CALCESER(.w_MCDTPRIU,g_CODESE)=.w_MCCOMPET or CALCESER(.w_MCDATDIS,g_CODESE)=.w_MCCOMPET or CALCESER(.w_MCDTPRIC,g_CODESE)=.w_MCCOMPET)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Esercizio di competenza non congruente")
        endif
        this.w_MCCOMPET = space(4)
        this.w_MCVALNAZ = space(3)
        this.w_MCCODVAL = space(3)
        this.w_INIESE = ctod("  /  /  ")
        this.w_FINESE = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MCCOMPET Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CAUCON
  func Link_1_26(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAUCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAUCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCFLANAL";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_CAUCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_CAUCON)
            select CCCODICE,CCFLANAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAUCON = NVL(_Link_.CCCODICE,space(5))
      this.w_FLANAL = NVL(_Link_.CCFLANAL,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CAUCON = space(5)
      endif
      this.w_FLANAL = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAUCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MCCOMPET
  func Link_1_30(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MCCOMPET) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_MCCOMPET)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ,ESINIESE,ESFINESE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_CODAZI;
                     ,'ESCODESE',trim(this.w_MCCOMPET))
          select ESCODAZI,ESCODESE,ESVALNAZ,ESINIESE,ESFINESE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MCCOMPET)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MCCOMPET) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oMCCOMPET_1_30'),i_cWhere,'',"Esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ,ESINIESE,ESFINESE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESVALNAZ,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ,ESINIESE,ESFINESE";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESVALNAZ,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MCCOMPET)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ,ESINIESE,ESFINESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_MCCOMPET);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_MCCOMPET)
            select ESCODAZI,ESCODESE,ESVALNAZ,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MCCOMPET = NVL(_Link_.ESCODESE,space(4))
      this.w_MCVALNAZ = NVL(_Link_.ESVALNAZ,space(3))
      this.w_MCCODVAL = NVL(_Link_.ESVALNAZ,space(3))
      this.w_INIESE = NVL(cp_ToDate(_Link_.ESINIESE),ctod("  /  /  "))
      this.w_FINESE = NVL(cp_ToDate(_Link_.ESFINESE),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_MCCOMPET = space(4)
      endif
      this.w_MCVALNAZ = space(3)
      this.w_MCCODVAL = space(3)
      this.w_INIESE = ctod("  /  /  ")
      this.w_FINESE = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FLPRIU<>'S' or CALCESER(.w_MCDTPRIU,g_CODESE)=.w_MCCOMPET
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_MCCOMPET = space(4)
        this.w_MCVALNAZ = space(3)
        this.w_MCCODVAL = space(3)
        this.w_INIESE = ctod("  /  /  ")
        this.w_FINESE = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MCCOMPET Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MCCODCON
  func Link_1_50(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MCCODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_MCCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_MCTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_MCTIPCON;
                     ,'ANCODICE',trim(this.w_MCCODCON))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MCCODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_MCCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_MCTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_MCCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_MCTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MCCODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oMCCODCON_1_50'),i_cWhere,'GSAR_BZC',"Clienti/fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_MCTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice intestatario inesistente o obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_MCTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MCCODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_MCCODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_MCTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_MCTIPCON;
                       ,'ANCODICE',this.w_MCCODCON)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MCCODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCON = NVL(_Link_.ANDESCRI,space(40))
      this.w_DTOBSOCL = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_MCCODCON = space(15)
      endif
      this.w_DESCON = space(40)
      this.w_DTOBSOCL = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_DTOBSOCL>.w_MCDATREG OR EMPTY(.w_DTOBSOCL))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice intestatario inesistente o obsoleto")
        endif
        this.w_MCCODCON = space(15)
        this.w_DESCON = space(40)
        this.w_DTOBSOCL = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MCCODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MCCODCES
  func Link_1_52(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CES_PITI_IDX,3]
    i_lTable = "CES_PITI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CES_PITI_IDX,2], .t., this.CES_PITI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CES_PITI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MCCODCES) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCE_ACE',True,'CES_PITI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CECODICE like "+cp_ToStrODBC(trim(this.w_MCCODCES)+"%");

          i_ret=cp_SQL(i_nConn,"select *";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CECODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CECODICE',trim(this.w_MCCODCES))
          select *;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CECODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MCCODCES)==trim(_Link_.CECODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CEDESCRI like "+cp_ToStrODBC(trim(this.w_MCCODCES)+"%");

            i_ret=cp_SQL(i_nConn,"select *";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CEDESCRI like "+cp_ToStr(trim(this.w_MCCODCES)+"%");

            select *;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MCCODCES) and !this.bDontReportError
            deferred_cp_zoom('CES_PITI','*','CECODICE',cp_AbsName(oSource.parent,'oMCCODCES_1_52'),i_cWhere,'GSCE_ACE',"Elenco cespiti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select *";
                     +" from "+i_cTable+" "+i_lTable+" where CECODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CECODICE',oSource.xKey(1))
            select *;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MCCODCES)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select *";
                   +" from "+i_cTable+" "+i_lTable+" where CECODICE="+cp_ToStrODBC(this.w_MCCODCES);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CECODICE',this.w_MCCODCES)
            select *;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MCCODCES = NVL(_Link_.CECODICE,space(20))
      this.w_DESCES = NVL(_Link_.CEDESCRI,space(40))
      this.w_DTPRIU = NVL(cp_ToDate(_Link_.CEDTPRIU),ctod("  /  /  "))
      this.w_PERDEF = NVL(_Link_.CEPERDEF,0)
      this.w_IMPMAX = NVL(_Link_.CEIMPMAX,0)
      this.w_FLAUMU = NVL(_Link_.CEFLAUMU,space(1))
      this.w_DTOBSO = NVL(cp_ToDate(_Link_.CEDTOBSO),ctod("  /  /  "))
      this.w_STABEN = NVL(_Link_.CESTABEN,space(1))
      this.w_TIPAMM = NVL(_Link_.CETIPAMM,space(1))
      this.w_CODESE = NVL(_Link_.CECODESE,space(4))
      this.w_CODCAT = NVL(_Link_.CECODCAT,space(15))
      this.w_TIPCES = NVL(_Link_.CETIPCES,space(2))
      this.w_MCCODCEN = NVL(_Link_.CECODCEN,space(15))
      this.w_MCVOCCEN = NVL(_Link_.CEVOCCEN,space(15))
      this.w_MCCODCOM = NVL(_Link_.CECODCOM,space(15))
      this.w_STACES = NVL(_Link_.CE_STATO,space(1))
      this.w_FLCEUS = NVL(_Link_.CEFLCEUS,space(1))
      this.w_UNIMIS = NVL(_Link_.CEUNIMIS,space(3))
      this.w_AMMIMM = NVL(_Link_.CEAMMIMM,space(1))
      this.w_CEESPRIU = NVL(_Link_.CEESPRIU,space(4))
      this.w_CEVALLIM = NVL(_Link_.CEVALLIM,space(3))
      this.w_CEDURCES = NVL(_Link_.CEDURCES,0)
      this.w_AMMIMC = NVL(_Link_.CEAMMIMC,space(1))
      this.w_DTPRIC = NVL(cp_ToDate(_Link_.CEDTPRIC),ctod("  /  /  "))
      this.w_CENSOAMM = NVL(_Link_.CENSOAMM,space(1))
      this.w_CENSOAMF = NVL(_Link_.CENSOAMF,space(1))
      this.w_CEESPRIC = NVL(_Link_.CEESPRIC,space(4))
    else
      if i_cCtrl<>'Load'
        this.w_MCCODCES = space(20)
      endif
      this.w_DESCES = space(40)
      this.w_DTPRIU = ctod("  /  /  ")
      this.w_PERDEF = 0
      this.w_IMPMAX = 0
      this.w_FLAUMU = space(1)
      this.w_DTOBSO = ctod("  /  /  ")
      this.w_STABEN = space(1)
      this.w_TIPAMM = space(1)
      this.w_CODESE = space(4)
      this.w_CODCAT = space(15)
      this.w_TIPCES = space(2)
      this.w_MCCODCEN = space(15)
      this.w_MCVOCCEN = space(15)
      this.w_MCCODCOM = space(15)
      this.w_STACES = space(1)
      this.w_FLCEUS = space(1)
      this.w_UNIMIS = space(3)
      this.w_AMMIMM = space(1)
      this.w_CEESPRIU = space(4)
      this.w_CEVALLIM = space(3)
      this.w_CEDURCES = 0
      this.w_AMMIMC = space(1)
      this.w_DTPRIC = ctod("  /  /  ")
      this.w_CENSOAMM = space(1)
      this.w_CENSOAMF = space(1)
      this.w_CEESPRIC = space(4)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DTOBSO>.w_MCDATREG OR EMPTY(.w_DTOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Cespite inesistente o obsoleto")
        endif
        this.w_MCCODCES = space(20)
        this.w_DESCES = space(40)
        this.w_DTPRIU = ctod("  /  /  ")
        this.w_PERDEF = 0
        this.w_IMPMAX = 0
        this.w_FLAUMU = space(1)
        this.w_DTOBSO = ctod("  /  /  ")
        this.w_STABEN = space(1)
        this.w_TIPAMM = space(1)
        this.w_CODESE = space(4)
        this.w_CODCAT = space(15)
        this.w_TIPCES = space(2)
        this.w_MCCODCEN = space(15)
        this.w_MCVOCCEN = space(15)
        this.w_MCCODCOM = space(15)
        this.w_STACES = space(1)
        this.w_FLCEUS = space(1)
        this.w_UNIMIS = space(3)
        this.w_AMMIMM = space(1)
        this.w_CEESPRIU = space(4)
        this.w_CEVALLIM = space(3)
        this.w_CEDURCES = 0
        this.w_AMMIMC = space(1)
        this.w_DTPRIC = ctod("  /  /  ")
        this.w_CENSOAMM = space(1)
        this.w_CENSOAMF = space(1)
        this.w_CEESPRIC = space(4)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CES_PITI_IDX,2])+'\'+cp_ToStr(_Link_.CECODICE,1)
      cp_ShowWarn(i_cKey,this.CES_PITI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MCCODCES Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_52(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 27 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CES_PITI_IDX,3] and i_nFlds+27<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CES_PITI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_52.CECODICE as CECODICE152"+ ",link_1_52.CEDESCRI as CEDESCRI152"+ ",link_1_52.CEDTPRIU as CEDTPRIU152"+ ",link_1_52.CEPERDEF as CEPERDEF152"+ ",link_1_52.CEIMPMAX as CEIMPMAX152"+ ",link_1_52.CEFLAUMU as CEFLAUMU152"+ ",link_1_52.CEDTOBSO as CEDTOBSO152"+ ",link_1_52.CESTABEN as CESTABEN152"+ ",link_1_52.CETIPAMM as CETIPAMM152"+ ",link_1_52.CECODESE as CECODESE152"+ ",link_1_52.CECODCAT as CECODCAT152"+ ",link_1_52.CETIPCES as CETIPCES152"+ ",link_1_52.CECODCEN as CECODCEN152"+ ",link_1_52.CEVOCCEN as CEVOCCEN152"+ ",link_1_52.CECODCOM as CECODCOM152"+ ",link_1_52.CE_STATO as CE_STATO152"+ ",link_1_52.CEFLCEUS as CEFLCEUS152"+ ",link_1_52.CEUNIMIS as CEUNIMIS152"+ ",link_1_52.CEAMMIMM as CEAMMIMM152"+ ",link_1_52.CEESPRIU as CEESPRIU152"+ ",link_1_52.CEVALLIM as CEVALLIM152"+ ",link_1_52.CEDURCES as CEDURCES152"+ ",link_1_52.CEAMMIMC as CEAMMIMC152"+ ",link_1_52.CEDTPRIC as CEDTPRIC152"+ ",link_1_52.CENSOAMM as CENSOAMM152"+ ",link_1_52.CENSOAMF as CENSOAMF152"+ ",link_1_52.CEESPRIC as CEESPRIC152"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_52 on MOV_CESP.MCCODCES=link_1_52.CECODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+27
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_52"
          i_cKey=i_cKey+'+" and MOV_CESP.MCCODCES=link_1_52.CECODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+27
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MCVOCCEN
  func Link_1_60(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOC_COST_IDX,3]
    i_lTable = "VOC_COST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2], .t., this.VOC_COST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MCVOCCEN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_AVC',True,'VOC_COST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VCCODICE like "+cp_ToStrODBC(trim(this.w_MCVOCCEN)+"%");

          i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VCCODICE',trim(this.w_MCVOCCEN))
          select VCCODICE,VCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MCVOCCEN)==trim(_Link_.VCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" VCDESCRI like "+cp_ToStrODBC(trim(this.w_MCVOCCEN)+"%");

            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" VCDESCRI like "+cp_ToStr(trim(this.w_MCVOCCEN)+"%");

            select VCCODICE,VCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MCVOCCEN) and !this.bDontReportError
            deferred_cp_zoom('VOC_COST','*','VCCODICE',cp_AbsName(oSource.parent,'oMCVOCCEN_1_60'),i_cWhere,'GSCA_AVC',"Voci di costo/ricavo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCCODICE',oSource.xKey(1))
            select VCCODICE,VCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MCVOCCEN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(this.w_MCVOCCEN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCCODICE',this.w_MCVOCCEN)
            select VCCODICE,VCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MCVOCCEN = NVL(_Link_.VCCODICE,space(15))
      this.w_DESVOC = NVL(_Link_.VCDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_MCVOCCEN = space(15)
      endif
      this.w_DESVOC = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])+'\'+cp_ToStr(_Link_.VCCODICE,1)
      cp_ShowWarn(i_cKey,this.VOC_COST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MCVOCCEN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_60(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VOC_COST_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_60.VCCODICE as VCCODICE160"+ ",link_1_60.VCDESCRI as VCDESCRI160"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_60 on MOV_CESP.MCVOCCEN=link_1_60.VCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_60"
          i_cKey=i_cKey+'+" and MOV_CESP.MCVOCCEN=link_1_60.VCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MCCODCEN
  func Link_1_62(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CENCOST_IDX,3]
    i_lTable = "CENCOST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2], .t., this.CENCOST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MCCODCEN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_ACC',True,'CENCOST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CC_CONTO like "+cp_ToStrODBC(trim(this.w_MCCODCEN)+"%");

          i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CC_CONTO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CC_CONTO',trim(this.w_MCCODCEN))
          select CC_CONTO,CCDESPIA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CC_CONTO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MCCODCEN)==trim(_Link_.CC_CONTO) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESPIA like "+cp_ToStrODBC(trim(this.w_MCCODCEN)+"%");

            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESPIA like "+cp_ToStr(trim(this.w_MCCODCEN)+"%");

            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MCCODCEN) and !this.bDontReportError
            deferred_cp_zoom('CENCOST','*','CC_CONTO',cp_AbsName(oSource.parent,'oMCCODCEN_1_62'),i_cWhere,'GSCA_ACC',"Centri di costo/ricavo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                     +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',oSource.xKey(1))
            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MCCODCEN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                   +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(this.w_MCCODCEN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',this.w_MCCODCEN)
            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MCCODCEN = NVL(_Link_.CC_CONTO,space(15))
      this.w_DESCEN = NVL(_Link_.CCDESPIA,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_MCCODCEN = space(15)
      endif
      this.w_DESCEN = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])+'\'+cp_ToStr(_Link_.CC_CONTO,1)
      cp_ShowWarn(i_cKey,this.CENCOST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MCCODCEN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_62(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CENCOST_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_62.CC_CONTO as CC_CONTO162"+ ",link_1_62.CCDESPIA as CCDESPIA162"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_62 on MOV_CESP.MCCODCEN=link_1_62.CC_CONTO"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_62"
          i_cKey=i_cKey+'+" and MOV_CESP.MCCODCEN=link_1_62.CC_CONTO(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MCCODCOM
  func Link_1_64(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MCCODCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACN',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_MCCODCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_MCCODCOM))
          select CNCODCAN,CNDESCAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MCCODCOM)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStrODBC(trim(this.w_MCCODCOM)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStr(trim(this.w_MCCODCOM)+"%");

            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MCCODCOM) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oMCCODCOM_1_64'),i_cWhere,'GSAR_ACN',"Commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MCCODCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_MCCODCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_MCCODCOM)
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MCCODCOM = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCAN = NVL(_Link_.CNDESCAN,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_MCCODCOM = space(15)
      endif
      this.w_DESCAN = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MCCODCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_64(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAN_TIER_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_64.CNCODCAN as CNCODCAN164"+ ",link_1_64.CNDESCAN as CNDESCAN164"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_64 on MOV_CESP.MCCODCOM=link_1_64.CNCODCAN"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_64"
          i_cKey=i_cKey+'+" and MOV_CESP.MCCODCOM=link_1_64.CNCODCAN(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MCCOMPET
  func Link_1_117(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SAL_CESP_IDX,3]
    i_lTable = "SAL_CESP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SAL_CESP_IDX,2], .t., this.SAL_CESP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SAL_CESP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MCCOMPET) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MCCOMPET)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SCCODCES,SCCODESE";
                   +" from "+i_cTable+" "+i_lTable+" where SCCODESE="+cp_ToStrODBC(this.w_MCCOMPET);
                   +" and SCCODCES="+cp_ToStrODBC(this.w_MCCODCES);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SCCODCES',this.w_MCCODCES;
                       ,'SCCODESE',this.w_MCCOMPET)
            select SCCODCES,SCCODESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
    else
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SAL_CESP_IDX,2])+'\'+cp_ToStr(_Link_.SCCODCES,1)+'\'+cp_ToStr(_Link_.SCCODESE,1)
      cp_ShowWarn(i_cKey,this.SAL_CESP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MCCOMPET Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=UNIMIS
  func Link_1_148(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_UNIMIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_UNIMIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMDESCRI,UMFLFRAZ";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_UNIMIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_UNIMIS)
            select UMCODICE,UMDESCRI,UMFLFRAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_UNIMIS = NVL(_Link_.UMCODICE,space(3))
      this.w_DESUNI = NVL(_Link_.UMDESCRI,space(35))
      this.w_FLGFRAZ = NVL(_Link_.UMFLFRAZ,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_UNIMIS = space(3)
      endif
      this.w_DESUNI = space(35)
      this.w_FLGFRAZ = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_UNIMIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MCCODVAL
  func Link_2_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MCCODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MCCODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT,VACAOVAL,VASIMVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_MCCODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_MCCODVAL)
            select VACODVAL,VADECTOT,VACAOVAL,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MCCODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
      this.w_CAOVAL = NVL(_Link_.VACAOVAL,0)
      this.w_SIMVAL = NVL(_Link_.VASIMVAL,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MCCODVAL = space(3)
      endif
      this.w_DECTOT = 0
      this.w_CAOVAL = 0
      this.w_SIMVAL = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MCCODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_20(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VALUTE_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_20.VACODVAL as VACODVAL220"+ ",link_2_20.VADECTOT as VADECTOT220"+ ",link_2_20.VACAOVAL as VACAOVAL220"+ ",link_2_20.VASIMVAL as VASIMVAL220"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_20 on MOV_CESP.MCCODVAL=link_2_20.VACODVAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_20"
          i_cKey=i_cKey+'+" and MOV_CESP.MCCODVAL=link_2_20.VACODVAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oMCNUMREG_1_10.value==this.w_MCNUMREG)
      this.oPgFrm.Page1.oPag.oMCNUMREG_1_10.value=this.w_MCNUMREG
    endif
    if not(this.oPgFrm.Page1.oPag.oMCCODESE_1_11.value==this.w_MCCODESE)
      this.oPgFrm.Page1.oPag.oMCCODESE_1_11.value=this.w_MCCODESE
    endif
    if not(this.oPgFrm.Page1.oPag.oMCDATREG_1_13.value==this.w_MCDATREG)
      this.oPgFrm.Page1.oPag.oMCDATREG_1_13.value=this.w_MCDATREG
    endif
    if not(this.oPgFrm.Page1.oPag.oMCNUMDOC_1_14.value==this.w_MCNUMDOC)
      this.oPgFrm.Page1.oPag.oMCNUMDOC_1_14.value=this.w_MCNUMDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oMCALFDOC_1_15.value==this.w_MCALFDOC)
      this.oPgFrm.Page1.oPag.oMCALFDOC_1_15.value=this.w_MCALFDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oMCDATDOC_1_16.value==this.w_MCDATDOC)
      this.oPgFrm.Page1.oPag.oMCDATDOC_1_16.value=this.w_MCDATDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oMCCODCAU_1_18.value==this.w_MCCODCAU)
      this.oPgFrm.Page1.oPag.oMCCODCAU_1_18.value=this.w_MCCODCAU
    endif
    if not(this.oPgFrm.Page1.oPag.oMCDTPRIC_1_19.value==this.w_MCDTPRIC)
      this.oPgFrm.Page1.oPag.oMCDTPRIC_1_19.value=this.w_MCDTPRIC
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAU_1_20.value==this.w_DESCAU)
      this.oPgFrm.Page1.oPag.oDESCAU_1_20.value=this.w_DESCAU
    endif
    if not(this.oPgFrm.Page1.oPag.oMCDATDIS_1_21.value==this.w_MCDATDIS)
      this.oPgFrm.Page1.oPag.oMCDATDIS_1_21.value=this.w_MCDATDIS
    endif
    if not(this.oPgFrm.Page1.oPag.oMCCOMPET_1_23.value==this.w_MCCOMPET)
      this.oPgFrm.Page1.oPag.oMCCOMPET_1_23.value=this.w_MCCOMPET
    endif
    if not(this.oPgFrm.Page1.oPag.oMCDTPRIU_1_29.value==this.w_MCDTPRIU)
      this.oPgFrm.Page1.oPag.oMCDTPRIU_1_29.value=this.w_MCDTPRIU
    endif
    if not(this.oPgFrm.Page1.oPag.oMCCOMPET_1_30.value==this.w_MCCOMPET)
      this.oPgFrm.Page1.oPag.oMCCOMPET_1_30.value=this.w_MCCOMPET
    endif
    if not(this.oPgFrm.Page1.oPag.oMCDESMOV_1_48.value==this.w_MCDESMOV)
      this.oPgFrm.Page1.oPag.oMCDESMOV_1_48.value=this.w_MCDESMOV
    endif
    if not(this.oPgFrm.Page1.oPag.oMCDESMO2_1_49.value==this.w_MCDESMO2)
      this.oPgFrm.Page1.oPag.oMCDESMO2_1_49.value=this.w_MCDESMO2
    endif
    if not(this.oPgFrm.Page1.oPag.oMCCODCON_1_50.value==this.w_MCCODCON)
      this.oPgFrm.Page1.oPag.oMCCODCON_1_50.value=this.w_MCCODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCON_1_51.value==this.w_DESCON)
      this.oPgFrm.Page1.oPag.oDESCON_1_51.value=this.w_DESCON
    endif
    if not(this.oPgFrm.Page1.oPag.oMCCODCES_1_52.value==this.w_MCCODCES)
      this.oPgFrm.Page1.oPag.oMCCODCES_1_52.value=this.w_MCCODCES
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCES_1_54.value==this.w_DESCES)
      this.oPgFrm.Page1.oPag.oDESCES_1_54.value=this.w_DESCES
    endif
    if not(this.oPgFrm.Page1.oPag.oMCSTATUS_1_59.RadioValue()==this.w_MCSTATUS)
      this.oPgFrm.Page1.oPag.oMCSTATUS_1_59.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMCVOCCEN_1_60.value==this.w_MCVOCCEN)
      this.oPgFrm.Page1.oPag.oMCVOCCEN_1_60.value=this.w_MCVOCCEN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESVOC_1_61.value==this.w_DESVOC)
      this.oPgFrm.Page1.oPag.oDESVOC_1_61.value=this.w_DESVOC
    endif
    if not(this.oPgFrm.Page1.oPag.oMCCODCEN_1_62.value==this.w_MCCODCEN)
      this.oPgFrm.Page1.oPag.oMCCODCEN_1_62.value=this.w_MCCODCEN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCEN_1_63.value==this.w_DESCEN)
      this.oPgFrm.Page1.oPag.oDESCEN_1_63.value=this.w_DESCEN
    endif
    if not(this.oPgFrm.Page1.oPag.oMCCODCOM_1_64.value==this.w_MCCODCOM)
      this.oPgFrm.Page1.oPag.oMCCODCOM_1_64.value=this.w_MCCODCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAN_1_65.value==this.w_DESCAN)
      this.oPgFrm.Page1.oPag.oDESCAN_1_65.value=this.w_DESCAN
    endif
    if not(this.oPgFrm.Page1.oPag.oUNIMIS_1_148.value==this.w_UNIMIS)
      this.oPgFrm.Page1.oPag.oUNIMIS_1_148.value=this.w_UNIMIS
    endif
    if not(this.oPgFrm.Page1.oPag.oDESUNI_1_150.value==this.w_DESUNI)
      this.oPgFrm.Page1.oPag.oDESUNI_1_150.value=this.w_DESUNI
    endif
    if not(this.oPgFrm.Page1.oPag.oMCQTACES_1_151.value==this.w_MCQTACES)
      this.oPgFrm.Page1.oPag.oMCQTACES_1_151.value=this.w_MCQTACES
    endif
    if not(this.oPgFrm.Page2.oPag.oMCCODVAL_2_20.value==this.w_MCCODVAL)
      this.oPgFrm.Page2.oPag.oMCCODVAL_2_20.value=this.w_MCCODVAL
    endif
    if not(this.oPgFrm.Page2.oPag.oMCCAOVAL_2_21.value==this.w_MCCAOVAL)
      this.oPgFrm.Page2.oPag.oMCCAOVAL_2_21.value=this.w_MCCAOVAL
    endif
    if not(this.oPgFrm.Page2.oPag.oMCIMPA01_2_28.value==this.w_MCIMPA01)
      this.oPgFrm.Page2.oPag.oMCIMPA01_2_28.value=this.w_MCIMPA01
    endif
    if not(this.oPgFrm.Page2.oPag.oMCIMPA23_2_29.value==this.w_MCIMPA23)
      this.oPgFrm.Page2.oPag.oMCIMPA23_2_29.value=this.w_MCIMPA23
    endif
    if not(this.oPgFrm.Page2.oPag.oMCIMPA02_2_30.value==this.w_MCIMPA02)
      this.oPgFrm.Page2.oPag.oMCIMPA02_2_30.value=this.w_MCIMPA02
    endif
    if not(this.oPgFrm.Page2.oPag.oMCIMPA24_2_31.value==this.w_MCIMPA24)
      this.oPgFrm.Page2.oPag.oMCIMPA24_2_31.value=this.w_MCIMPA24
    endif
    if not(this.oPgFrm.Page2.oPag.oMCIMPA03_2_32.value==this.w_MCIMPA03)
      this.oPgFrm.Page2.oPag.oMCIMPA03_2_32.value=this.w_MCIMPA03
    endif
    if not(this.oPgFrm.Page2.oPag.oMCIMPA18_2_33.value==this.w_MCIMPA18)
      this.oPgFrm.Page2.oPag.oMCIMPA18_2_33.value=this.w_MCIMPA18
    endif
    if not(this.oPgFrm.Page2.oPag.oMCIMPA04_2_35.value==this.w_MCIMPA04)
      this.oPgFrm.Page2.oPag.oMCIMPA04_2_35.value=this.w_MCIMPA04
    endif
    if not(this.oPgFrm.Page2.oPag.oMCIMPA22_2_36.value==this.w_MCIMPA22)
      this.oPgFrm.Page2.oPag.oMCIMPA22_2_36.value=this.w_MCIMPA22
    endif
    if not(this.oPgFrm.Page2.oPag.oMCIMPA05_2_37.value==this.w_MCIMPA05)
      this.oPgFrm.Page2.oPag.oMCIMPA05_2_37.value=this.w_MCIMPA05
    endif
    if not(this.oPgFrm.Page2.oPag.oMCIMPA19_2_38.value==this.w_MCIMPA19)
      this.oPgFrm.Page2.oPag.oMCIMPA19_2_38.value=this.w_MCIMPA19
    endif
    if not(this.oPgFrm.Page2.oPag.oMCIMPA06_2_39.value==this.w_MCIMPA06)
      this.oPgFrm.Page2.oPag.oMCIMPA06_2_39.value=this.w_MCIMPA06
    endif
    if not(this.oPgFrm.Page2.oPag.oMCIMPA07_2_40.value==this.w_MCIMPA07)
      this.oPgFrm.Page2.oPag.oMCIMPA07_2_40.value=this.w_MCIMPA07
    endif
    if not(this.oPgFrm.Page2.oPag.oCODCAU_2_41.value==this.w_CODCAU)
      this.oPgFrm.Page2.oPag.oCODCAU_2_41.value=this.w_CODCAU
    endif
    if not(this.oPgFrm.Page2.oPag.oMCCOECIV_2_42.value==this.w_MCCOECIV)
      this.oPgFrm.Page2.oPag.oMCCOECIV_2_42.value=this.w_MCCOECIV
    endif
    if not(this.oPgFrm.Page2.oPag.oMCIMPA08_2_43.value==this.w_MCIMPA08)
      this.oPgFrm.Page2.oPag.oMCIMPA08_2_43.value=this.w_MCIMPA08
    endif
    if not(this.oPgFrm.Page2.oPag.oMCIMPA09_2_44.value==this.w_MCIMPA09)
      this.oPgFrm.Page2.oPag.oMCIMPA09_2_44.value=this.w_MCIMPA09
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCAU_2_45.value==this.w_DESCAU)
      this.oPgFrm.Page2.oPag.oDESCAU_2_45.value=this.w_DESCAU
    endif
    if not(this.oPgFrm.Page2.oPag.oMCCOEFIS_2_46.value==this.w_MCCOEFIS)
      this.oPgFrm.Page2.oPag.oMCCOEFIS_2_46.value=this.w_MCCOEFIS
    endif
    if not(this.oPgFrm.Page2.oPag.oMCIMPA10_2_48.value==this.w_MCIMPA10)
      this.oPgFrm.Page2.oPag.oMCIMPA10_2_48.value=this.w_MCIMPA10
    endif
    if not(this.oPgFrm.Page2.oPag.oMCIMPA15_2_50.value==this.w_MCIMPA15)
      this.oPgFrm.Page2.oPag.oMCIMPA15_2_50.value=this.w_MCIMPA15
    endif
    if not(this.oPgFrm.Page2.oPag.oCODCES_2_51.value==this.w_CODCES)
      this.oPgFrm.Page2.oPag.oCODCES_2_51.value=this.w_CODCES
    endif
    if not(this.oPgFrm.Page2.oPag.oMCCOEFI1_2_52.value==this.w_MCCOEFI1)
      this.oPgFrm.Page2.oPag.oMCCOEFI1_2_52.value=this.w_MCCOEFI1
    endif
    if not(this.oPgFrm.Page2.oPag.oMCIMPA16_2_53.value==this.w_MCIMPA16)
      this.oPgFrm.Page2.oPag.oMCIMPA16_2_53.value=this.w_MCIMPA16
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCES_2_54.value==this.w_DESCES)
      this.oPgFrm.Page2.oPag.oDESCES_2_54.value=this.w_DESCES
    endif
    if not(this.oPgFrm.Page2.oPag.oMCIMPA11_2_55.value==this.w_MCIMPA11)
      this.oPgFrm.Page2.oPag.oMCIMPA11_2_55.value=this.w_MCIMPA11
    endif
    if not(this.oPgFrm.Page2.oPag.oMCIMPA12_2_56.value==this.w_MCIMPA12)
      this.oPgFrm.Page2.oPag.oMCIMPA12_2_56.value=this.w_MCIMPA12
    endif
    if not(this.oPgFrm.Page2.oPag.oMCIMPA20_2_57.value==this.w_MCIMPA20)
      this.oPgFrm.Page2.oPag.oMCIMPA20_2_57.value=this.w_MCIMPA20
    endif
    if not(this.oPgFrm.Page2.oPag.oMCIMPA13_2_58.value==this.w_MCIMPA13)
      this.oPgFrm.Page2.oPag.oMCIMPA13_2_58.value=this.w_MCIMPA13
    endif
    if not(this.oPgFrm.Page2.oPag.oMCIMPA21_2_59.value==this.w_MCIMPA21)
      this.oPgFrm.Page2.oPag.oMCIMPA21_2_59.value=this.w_MCIMPA21
    endif
    if not(this.oPgFrm.Page2.oPag.oMCIMPA14_2_60.value==this.w_MCIMPA14)
      this.oPgFrm.Page2.oPag.oMCIMPA14_2_60.value=this.w_MCIMPA14
    endif
    if not(this.oPgFrm.Page2.oPag.oSIMVAL_2_67.value==this.w_SIMVAL)
      this.oPgFrm.Page2.oPag.oSIMVAL_2_67.value=this.w_SIMVAL
    endif
    cp_SetControlsValueExtFlds(this,'MOV_CESP')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_MCNUMREG))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMCNUMREG_1_10.SetFocus()
            i_bnoObbl = !empty(.w_MCNUMREG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_MCCODESE))  and (.cFunction='Load')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMCCODESE_1_11.SetFocus()
            i_bnoObbl = !empty(.w_MCCODESE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_MCDATREG))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMCDATREG_1_13.SetFocus()
            i_bnoObbl = !empty(.w_MCDATREG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_MCCODCAU)) or not(.w_DTOBSOCA>.w_MCDATREG OR EMPTY(.w_DTOBSOCA)))  and (.w_Con1cau And .w_Con2cau)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMCCODCAU_1_18.SetFocus()
            i_bnoObbl = !empty(.w_MCCODCAU)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale cespite inesistente o obsoleta")
          case   (empty(.w_MCDTPRIC))  and not(.w_FLPRIC<>'S' OR .w_PCTIPAMM='F')  and (.w_FLPRIC='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMCDTPRIC_1_19.SetFocus()
            i_bnoObbl = !empty(.w_MCDTPRIC)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_MCCOMPET)) or not((.w_FLPRIU<>'S' And .w_FLDADI<>'S' And .w_FLPRIC<>'S') or (CALCESER(.w_MCDTPRIU,g_CODESE)=.w_MCCOMPET or CALCESER(.w_MCDATDIS,g_CODESE)=.w_MCCOMPET or CALCESER(.w_MCDTPRIC,g_CODESE)=.w_MCCOMPET)))  and not((.w_FLPRIU='S' AND .w_FLPRIC<>'S') OR (.w_FLPRIU='S' And .w_PCTIPAMM='F'))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMCCOMPET_1_23.SetFocus()
            i_bnoObbl = !empty(.w_MCCOMPET)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Esercizio di competenza non congruente")
          case   (empty(.w_MCDTPRIU))  and not(.w_FLPRIU<>'S' OR .w_PCTIPAMM='C')  and (.w_FLPRIU='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMCDTPRIU_1_29.SetFocus()
            i_bnoObbl = !empty(.w_MCDTPRIU)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_MCCOMPET)) or not(.w_FLPRIU<>'S' or CALCESER(.w_MCDTPRIU,g_CODESE)=.w_MCCOMPET))  and not(.w_FLPRIU<>'S'  OR .w_PCTIPAMM='C')  and (.w_PCTIPAMM<>'C')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMCCOMPET_1_30.SetFocus()
            i_bnoObbl = !empty(.w_MCCOMPET)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_MCCODCON)) or not((.w_DTOBSOCL>.w_MCDATREG OR EMPTY(.w_DTOBSOCL))))  and not(NOT .w_MCTIPCON $ 'CF')  and (.w_MCTIPCON $ 'CF')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMCCODCON_1_50.SetFocus()
            i_bnoObbl = !empty(.w_MCCODCON)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice intestatario inesistente o obsoleto")
          case   ((empty(.w_MCCODCES)) or not(.w_DTOBSO>.w_MCDATREG OR EMPTY(.w_DTOBSO)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMCCODCES_1_52.SetFocus()
            i_bnoObbl = !empty(.w_MCCODCES)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Cespite inesistente o obsoleto")
          case   (empty(.w_MCSTATUS))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMCSTATUS_1_59.SetFocus()
            i_bnoObbl = !empty(.w_MCSTATUS)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_MCQTACES)) or not(IIF(.w_FLGFRAZ = 'S', INT(.w_MCQTACES) = .w_MCQTACES, .T.) and IIF(NVL(.w_TIPOAGG,'+') ='+',.w_MCQTACES >0,.w_MCQTACES <0)))  and not(!.w_TIPCES = 'CQ')  and (.w_TIPCES = 'CQ' AND .w_FLGA17= 'S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMCQTACES_1_151.SetFocus()
            i_bnoObbl = !empty(.w_MCQTACES)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Quantit� errata poich� non intera oppure non congruente al segno impostato sulla causale")
          case   (empty(.w_MCCAOVAL))  and (GETVALUT(.w_MCCODVAL, "VADATEUR")>=IIF(EMPTY(.w_MCDATDOC),.w_MCDATREG,.w_MCDATDOC) OR .w_CAOVAL=0)
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oMCCAOVAL_2_21.SetFocus()
            i_bnoObbl = !empty(.w_MCCAOVAL)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_MCIMPA01)) or not(.w_MCIMPA01>0))  and (.w_FLGA01='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oMCIMPA01_2_28.SetFocus()
            i_bnoObbl = !empty(.w_MCIMPA01)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("L'importo di ventita deve essere maggiore di zero")
        endcase
      endif
      *i_bRes = i_bRes .and. .GSCE_MMC.CheckForm()
      if i_bres
        i_bres=  .GSCE_MMC.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=2
        endif
      endif
      *i_bRes = i_bRes .and. .GSCE_MMS.CheckForm()
      if i_bres
        i_bres=  .GSCE_MMS.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=2
        endif
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_MCSERIAL = this.w_MCSERIAL
    this.o_MCDATREG = this.w_MCDATREG
    this.o_MCDATDOC = this.w_MCDATDOC
    this.o_FLPRIC = this.w_FLPRIC
    this.o_MCCODCAU = this.w_MCCODCAU
    this.o_MCDTPRIC = this.w_MCDTPRIC
    this.o_MCDATDIS = this.w_MCDATDIS
    this.o_FLPRIU = this.w_FLPRIU
    this.o_MCCOMPET = this.w_MCCOMPET
    this.o_FLDADI = this.w_FLDADI
    this.o_MCDTPRIU = this.w_MCDTPRIU
    this.o_MCCODCES = this.w_MCCODCES
    this.o_MCVOCCEN = this.w_MCVOCCEN
    this.o_MCCODCEN = this.w_MCCODCEN
    this.o_MCCODCOM = this.w_MCCODCOM
    this.o_MCCODVAL = this.w_MCCODVAL
    this.o_MCIMPA01 = this.w_MCIMPA01
    this.o_MCIMPA02 = this.w_MCIMPA02
    this.o_MCIMPA03 = this.w_MCIMPA03
    this.o_MCIMPA04 = this.w_MCIMPA04
    this.o_MCIMPA05 = this.w_MCIMPA05
    this.o_MCIMPA06 = this.w_MCIMPA06
    this.o_MCIMPA07 = this.w_MCIMPA07
    this.o_MCIMPA08 = this.w_MCIMPA08
    this.o_MCIMPA09 = this.w_MCIMPA09
    this.o_MCIMPA10 = this.w_MCIMPA10
    this.o_MCIMPA15 = this.w_MCIMPA15
    this.o_MCIMPA16 = this.w_MCIMPA16
    this.o_MCIMPA11 = this.w_MCIMPA11
    this.o_MCIMPA12 = this.w_MCIMPA12
    this.o_MCIMPA13 = this.w_MCIMPA13
    this.o_MCIMPA14 = this.w_MCIMPA14
    * --- GSCE_MMC : Depends On
    this.GSCE_MMC.SaveDependsOn()
    * --- GSCE_MMS : Depends On
    this.GSCE_MMS.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgsce_amcPag1 as StdContainer
  Width  = 802
  height = 404
  stdWidth  = 802
  stdheight = 404
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oObj_1_2 as cp_runprogram with uid="YEBQDDQDSO",left=14, top=562, width=280,height=20,;
    caption='GSCE_BVA',;
   bGlobalFont=.t.,;
    prg="GSCE_BVA",;
    cEvent = "Load",;
    nPag=1;
    , HelpContextID = 196035239

  add object oMCNUMREG_1_10 as StdField with uid="ZSGBSBNGGR",rtseq=9,rtrep=.f.,;
    cFormVar = "w_MCNUMREG", cQueryName = "MCNUMREG",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero registrazione",;
    HelpContextID = 178250509,;
   bGlobalFont=.t.,;
    Height=21, Width=60, Left=85, Top=16, cSayPict='"999999"', cGetPict='"999999"'

  add object oMCCODESE_1_11 as StdField with uid="PCFLBRTESV",rtseq=10,rtrep=.f.,;
    cFormVar = "w_MCCODESE", cQueryName = "MCCODESE",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Esercizio di riferimento",;
    HelpContextID = 218706699,;
   bGlobalFont=.t.,;
    Height=21, Width=47, Left=156, Top=16, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_MCCODESE"

  func oMCCODESE_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  func oMCCODESE_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oMCCODESE_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMCCODESE_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CODAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_CODAZI)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oMCCODESE_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Esercizi",'',this.parent.oContained
  endproc

  add object oMCDATREG_1_13 as StdField with uid="IOMINFRKLD",rtseq=12,rtrep=.f.,;
    cFormVar = "w_MCDATREG", cQueryName = "MCDATREG",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data registrazione",;
    HelpContextID = 184238861,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=356, Top=16

  add object oMCNUMDOC_1_14 as StdField with uid="HTCJHENVQP",rtseq=13,rtrep=.f.,;
    cFormVar = "w_MCNUMDOC", cQueryName = "MCNUMDOC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero del documento associato al movimento",;
    HelpContextID = 56630519,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=85, Top=46, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  add object oMCALFDOC_1_15 as StdField with uid="QJATLCYJJL",rtseq=14,rtrep=.f.,;
    cFormVar = "w_MCALFDOC", cQueryName = "MCALFDOC",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Eventuale serie del documento",;
    HelpContextID = 64613623,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=213, Top=46, cSayPict='"!!!!!!!!!!"', cGetPict='"!!!!!!!!!!"', InputMask=replicate('X',10)

  add object oMCDATDOC_1_16 as StdField with uid="ICAYFAEDWF",rtseq=15,rtrep=.f.,;
    cFormVar = "w_MCDATDOC", cQueryName = "MCDATDOC",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data del documento",;
    HelpContextID = 50642167,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=356, Top=46

  add object oMCCODCAU_1_18 as StdField with uid="XWRNWXUHZJ",rtseq=17,rtrep=.f.,;
    cFormVar = "w_MCCODCAU", cQueryName = "MCCODCAU",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale cespite inesistente o obsoleta",;
    ToolTipText = "Codice causale di movimentazione cespite",;
    HelpContextID = 83283173,;
   bGlobalFont=.t.,;
    Height=21, Width=66, Left=85, Top=76, cSayPict="'!!!!!'", cGetPict="'!!!!!'", InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CESP", cZoomOnZoom="GSCE_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_MCCODCAU"

  func oMCCODCAU_1_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Con1cau And .w_Con2cau)
    endwith
   endif
  endfunc

  func oMCCODCAU_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oMCCODCAU_1_18.ecpDrop(oSource)
    this.Parent.oContained.link_1_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMCCODCAU_1_18.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CESP','*','CCCODICE',cp_AbsName(this.parent,'oMCCODCAU_1_18'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCE_ACC',"Causali cespiti",'GSCE_ZCA.CAU_CESP_VZM',this.parent.oContained
  endproc
  proc oMCCODCAU_1_18.mZoomOnZoom
    local i_obj
    i_obj=GSCE_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_MCCODCAU
     i_obj.ecpSave()
  endproc

  add object oMCDTPRIC_1_19 as StdField with uid="EIUUIMWEZP",rtseq=18,rtrep=.f.,;
    cFormVar = "w_MCDTPRIC", cQueryName = "MCDTPRIC",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di primo utilizzo civile del cespite",;
    HelpContextID = 87145719,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=586, Top=74

  func oMCDTPRIC_1_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLPRIC='S')
    endwith
   endif
  endfunc

  func oMCDTPRIC_1_19.mHide()
    with this.Parent.oContained
      return (.w_FLPRIC<>'S' OR .w_PCTIPAMM='F')
    endwith
  endfunc

  add object oDESCAU_1_20 as StdField with uid="VSKAZFCSYB",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DESCAU", cQueryName = "DESCAU",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 53595082,;
   bGlobalFont=.t.,;
    Height=21, Width=269, Left=153, Top=76, InputMask=replicate('X',40)

  add object oMCDATDIS_1_21 as StdField with uid="YLXTIKWVJC",rtseq=20,rtrep=.f.,;
    cFormVar = "w_MCDATDIS", cQueryName = "MCDATDIS",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di dismissione del cespite",;
    HelpContextID = 217793305,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=586, Top=74

  proc oMCDATDIS_1_21.mDefault
    with this.Parent.oContained
      if empty(.w_MCDATDIS)
        .w_MCDATDIS = IIF(.w_FLDADI='S',.w_MCDATREG,cp_CharToDate('  -  -  ') )
      endif
    endwith
  endproc

  func oMCDATDIS_1_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLDADI='S')
    endwith
   endif
  endfunc

  func oMCDATDIS_1_21.mHide()
    with this.Parent.oContained
      return (.w_FLDADI<>'S')
    endwith
  endfunc

  add object oMCCOMPET_1_23 as StdField with uid="WECTMBUSYD",rtseq=22,rtrep=.f.,;
    cFormVar = "w_MCCOMPET", cQueryName = "MCCOMPET",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Esercizio di competenza non congruente",;
    ToolTipText = "Esercizio di competenza",;
    HelpContextID = 144257818,;
   bGlobalFont=.t.,;
    Height=21, Width=49, Left=735, Top=74, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_MCCOMPET"

  func oMCCOMPET_1_23.mHide()
    with this.Parent.oContained
      return ((.w_FLPRIU='S' AND .w_FLPRIC<>'S') OR (.w_FLPRIU='S' And .w_PCTIPAMM='F'))
    endwith
  endfunc

  func oMCCOMPET_1_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_23('Part',this)
    endwith
    return bRes
  endfunc

  proc oMCCOMPET_1_23.ecpDrop(oSource)
    this.Parent.oContained.link_1_23('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMCCOMPET_1_23.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CODAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_CODAZI)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oMCCOMPET_1_23'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Esercizi",'',this.parent.oContained
  endproc

  add object oMCDTPRIU_1_29 as StdField with uid="DAUSZALWGC",rtseq=28,rtrep=.f.,;
    cFormVar = "w_MCDTPRIU", cQueryName = "MCDTPRIU",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di primo utilizzo del cespite",;
    HelpContextID = 87145701,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=586, Top=99

  func oMCDTPRIU_1_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLPRIU='S')
    endwith
   endif
  endfunc

  func oMCDTPRIU_1_29.mHide()
    with this.Parent.oContained
      return (.w_FLPRIU<>'S' OR .w_PCTIPAMM='C')
    endwith
  endfunc

  add object oMCCOMPET_1_30 as StdField with uid="GKIEYVKMNC",rtseq=29,rtrep=.f.,;
    cFormVar = "w_MCCOMPET", cQueryName = "MCCOMPET",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Esercizio di competenza",;
    HelpContextID = 144257818,;
   bGlobalFont=.t.,;
    Height=21, Width=49, Left=735, Top=99, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_MCCOMPET"

  func oMCCOMPET_1_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PCTIPAMM<>'C')
    endwith
   endif
  endfunc

  func oMCCOMPET_1_30.mHide()
    with this.Parent.oContained
      return (.w_FLPRIU<>'S'  OR .w_PCTIPAMM='C')
    endwith
  endfunc

  func oMCCOMPET_1_30.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_30('Part',this)
    endwith
    return bRes
  endfunc

  proc oMCCOMPET_1_30.ecpDrop(oSource)
    this.Parent.oContained.link_1_30('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMCCOMPET_1_30.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CODAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_CODAZI)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oMCCOMPET_1_30'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Esercizi",'',this.parent.oContained
  endproc

  add object oMCDESMOV_1_48 as StdField with uid="SFPHLWIVOQ",rtseq=47,rtrep=.f.,;
    cFormVar = "w_MCDESMOV", cQueryName = "MCDESMOV",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione del movimento",;
    HelpContextID = 168869092,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=85, Top=124, InputMask=replicate('X',40)

  add object oMCDESMO2_1_49 as StdField with uid="SXICRTDIUP",rtseq=48,rtrep=.f.,;
    cFormVar = "w_MCDESMO2", cQueryName = "MCDESMO2",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Ulteriore descrizione aggiuntiva",;
    HelpContextID = 168869128,;
   bGlobalFont=.t.,;
    Height=21, Width=297, Left=375, Top=124, InputMask=replicate('X',40)

  add object oMCCODCON_1_50 as StdField with uid="XLMUBOVURY",rtseq=49,rtrep=.f.,;
    cFormVar = "w_MCCODCON", cQueryName = "MCCODCON",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice intestatario inesistente o obsoleto",;
    ToolTipText = "Codice cliente/fornitore di riferimento",;
    HelpContextID = 83283180,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=85, Top=149, cSayPict="p_CLF", cGetPict="p_CLF", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_MCTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_MCCODCON"

  func oMCCODCON_1_50.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MCTIPCON $ 'CF')
    endwith
   endif
  endfunc

  func oMCCODCON_1_50.mHide()
    with this.Parent.oContained
      return (NOT .w_MCTIPCON $ 'CF')
    endwith
  endfunc

  func oMCCODCON_1_50.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_50('Part',this)
    endwith
    return bRes
  endfunc

  proc oMCCODCON_1_50.ecpDrop(oSource)
    this.Parent.oContained.link_1_50('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMCCODCON_1_50.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_MCTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_MCTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oMCCODCON_1_50'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Clienti/fornitori",'',this.parent.oContained
  endproc
  proc oMCCODCON_1_50.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_MCTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_MCCODCON
     i_obj.ecpSave()
  endproc

  add object oDESCON_1_51 as StdField with uid="UCEZTKYEMO",rtseq=50,rtrep=.f.,;
    cFormVar = "w_DESCON", cQueryName = "DESCON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 156355530,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=245, Top=149, InputMask=replicate('X',40)

  func oDESCON_1_51.mHide()
    with this.Parent.oContained
      return (NOT .w_MCTIPCON $ 'CF')
    endwith
  endfunc

  add object oMCCODCES_1_52 as StdField with uid="ASJHKUNYIY",rtseq=51,rtrep=.f.,;
    cFormVar = "w_MCCODCES", cQueryName = "MCCODCES",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Cespite inesistente o obsoleto",;
    ToolTipText = "Codice del cespite",;
    HelpContextID = 185152281,;
   bGlobalFont=.t.,;
    Height=21, Width=156, Left=85, Top=174, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="CES_PITI", cZoomOnZoom="GSCE_ACE", oKey_1_1="CECODICE", oKey_1_2="this.w_MCCODCES"

  func oMCCODCES_1_52.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_52('Part',this)
      if .not. empty(.w_MCCOMPET)
        bRes2=.link_1_117('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oMCCODCES_1_52.ecpDrop(oSource)
    this.Parent.oContained.link_1_52('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMCCODCES_1_52.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CES_PITI','*','CECODICE',cp_AbsName(this.parent,'oMCCODCES_1_52'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCE_ACE',"Elenco cespiti",'',this.parent.oContained
  endproc
  proc oMCCODCES_1_52.mZoomOnZoom
    local i_obj
    i_obj=GSCE_ACE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CECODICE=this.parent.oContained.w_MCCODCES
     i_obj.ecpSave()
  endproc

  add object oDESCES_1_54 as StdField with uid="IJNPJUUJAS",rtseq=53,rtrep=.f.,;
    cFormVar = "w_DESCES", cQueryName = "DESCES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 82955210,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=245, Top=174, InputMask=replicate('X',40)


  add object oMCSTATUS_1_59 as StdCombo with uid="RYFBTZVGHE",rtseq=58,rtrep=.f.,left=563,top=16,width=102,height=21;
    , tabstop=.f.;
    , ToolTipText = "Movimento confermato o provvisorio";
    , HelpContextID = 199176985;
    , cFormVar="w_MCSTATUS",RowSource=""+"Provvisorio,"+"Confermato", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oMCSTATUS_1_59.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'C',;
    space(1))))
  endfunc
  func oMCSTATUS_1_59.GetRadio()
    this.Parent.oContained.w_MCSTATUS = this.RadioValue()
    return .t.
  endfunc

  func oMCSTATUS_1_59.SetRadio()
    this.Parent.oContained.w_MCSTATUS=trim(this.Parent.oContained.w_MCSTATUS)
    this.value = ;
      iif(this.Parent.oContained.w_MCSTATUS=='P',1,;
      iif(this.Parent.oContained.w_MCSTATUS=='C',2,;
      0))
  endfunc

  add object oMCVOCCEN_1_60 as StdField with uid="NFNUUKNFJL",rtseq=59,rtrep=.f.,;
    cFormVar = "w_MCVOCCEN", cQueryName = "MCVOCCEN",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Voce di costo/ricavo associato al movimento",;
    HelpContextID = 184181524,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=85, Top=199, cSayPict="p_MCE", cGetPict="p_MCE", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="VOC_COST", cZoomOnZoom="GSCA_AVC", oKey_1_1="VCCODICE", oKey_1_2="this.w_MCVOCCEN"

  func oMCVOCCEN_1_60.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERCCR='S' AND .w_FLANAL='S')
    endwith
   endif
  endfunc

  func oMCVOCCEN_1_60.mHide()
    with this.Parent.oContained
      return (g_PERCCR<>'S')
    endwith
  endfunc

  func oMCVOCCEN_1_60.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_60('Part',this)
    endwith
    return bRes
  endfunc

  proc oMCVOCCEN_1_60.ecpDrop(oSource)
    this.Parent.oContained.link_1_60('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMCVOCCEN_1_60.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOC_COST','*','VCCODICE',cp_AbsName(this.parent,'oMCVOCCEN_1_60'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_AVC',"Voci di costo/ricavo",'',this.parent.oContained
  endproc
  proc oMCVOCCEN_1_60.mZoomOnZoom
    local i_obj
    i_obj=GSCA_AVC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VCCODICE=this.parent.oContained.w_MCVOCCEN
     i_obj.ecpSave()
  endproc

  add object oDESVOC_1_61 as StdField with uid="CVHCFUGYZE",rtseq=60,rtrep=.f.,;
    cFormVar = "w_DESVOC", cQueryName = "DESVOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 71224266,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=245, Top=199, InputMask=replicate('X',40)

  func oDESVOC_1_61.mHide()
    with this.Parent.oContained
      return (g_PERCCR<>'S')
    endwith
  endfunc

  add object oMCCODCEN_1_62 as StdField with uid="RRNDMSUUAZ",rtseq=61,rtrep=.f.,;
    cFormVar = "w_MCCODCEN", cQueryName = "MCCODCEN",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Centro di costo/ricavo associato al movimento",;
    HelpContextID = 185152276,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=85, Top=224, cSayPict="p_CEN", cGetPict="p_CEN", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CENCOST", cZoomOnZoom="GSCA_ACC", oKey_1_1="CC_CONTO", oKey_1_2="this.w_MCCODCEN"

  func oMCCODCEN_1_62.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERCCR='S' AND .w_FLANAL='S')
    endwith
   endif
  endfunc

  func oMCCODCEN_1_62.mHide()
    with this.Parent.oContained
      return (g_PERCCR<>'S')
    endwith
  endfunc

  func oMCCODCEN_1_62.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_62('Part',this)
    endwith
    return bRes
  endfunc

  proc oMCCODCEN_1_62.ecpDrop(oSource)
    this.Parent.oContained.link_1_62('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMCCODCEN_1_62.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CENCOST','*','CC_CONTO',cp_AbsName(this.parent,'oMCCODCEN_1_62'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_ACC',"Centri di costo/ricavo",'',this.parent.oContained
  endproc
  proc oMCCODCEN_1_62.mZoomOnZoom
    local i_obj
    i_obj=GSCA_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CC_CONTO=this.parent.oContained.w_MCCODCEN
     i_obj.ecpSave()
  endproc

  add object oDESCEN_1_63 as StdField with uid="PDIMRIBRJS",rtseq=62,rtrep=.f.,;
    cFormVar = "w_DESCEN", cQueryName = "DESCEN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 166841290,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=245, Top=224, InputMask=replicate('X',40)

  func oDESCEN_1_63.mHide()
    with this.Parent.oContained
      return (g_PERCCR<>'S')
    endwith
  endfunc

  add object oMCCODCOM_1_64 as StdField with uid="QCRFPAMPFY",rtseq=63,rtrep=.f.,;
    cFormVar = "w_MCCODCOM", cQueryName = "MCCODCOM",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Commessa associata al movimento",;
    HelpContextID = 83283181,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=85, Top=249, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="GSAR_ACN", oKey_1_1="CNCODCAN", oKey_1_2="this.w_MCCODCOM"

  func oMCCODCOM_1_64.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_64('Part',this)
    endwith
    return bRes
  endfunc

  proc oMCCODCOM_1_64.ecpDrop(oSource)
    this.Parent.oContained.link_1_64('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMCCODCOM_1_64.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oMCCODCOM_1_64'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACN',"Commesse",'',this.parent.oContained
  endproc
  proc oMCCODCOM_1_64.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_MCCODCOM
     i_obj.ecpSave()
  endproc

  add object oDESCAN_1_65 as StdField with uid="OFYGIVROBT",rtseq=64,rtrep=.f.,;
    cFormVar = "w_DESCAN", cQueryName = "DESCAN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 171035594,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=245, Top=249, InputMask=replicate('X',30)


  add object oBtn_1_135 as StdButton with uid="HXIRSDEPRW",left=736, top=16, width=48,height=45,;
    CpPicture="bmp\Cerifica.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per confermare la registrazione";
    , HelpContextID = 63762809;
    , tabstop=.f., caption='Con\<ferma';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_135.Click()
      with this.Parent.oContained
        do GSCE_BC3 with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_135.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_MCSTATUS='P' AND NOT EMPTY(.w_PNSERIALC) AND .w_FLPROV='N' AND NOT EMPTY(.w_MCSERIAL) AND .w_TIPOPE='Query')
      endwith
    endif
  endfunc

  func oBtn_1_135.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (NOT (.w_MCSTATUS='P' AND NOT EMPTY(.w_PNSERIALC) AND .w_FLPROV='N'  AND NOT EMPTY(.w_MCSERIAL)  AND .w_TIPOPE='Query'))
     endwith
    endif
  endfunc


  add object oObj_1_139 as cp_runprogram with uid="STYZVIXAYQ",left=14, top=414, width=280,height=20,;
    caption='GSCE_BC1',;
   bGlobalFont=.t.,;
    prg="GSCE_BC1",;
    cEvent = "w_MCCODCAU Changed",;
    nPag=1;
    , HelpContextID = 196035223


  add object oObj_1_140 as cp_runprogram with uid="NFNQLENXWZ",left=14, top=498, width=280,height=20,;
    caption='GSCE_BMK',;
   bGlobalFont=.t.,;
    prg="GSCE_BMK",;
    cEvent = "ControlliFinali",;
    nPag=1;
    , HelpContextID = 72400207


  add object oObj_1_141 as cp_runprogram with uid="VIKOXEDTDP",left=14, top=436, width=280,height=20,;
    caption='GSCE_BVM',;
   bGlobalFont=.t.,;
    prg="GSCE_BVM",;
    cEvent = "Insert start,Update start,Delete start",;
    nPag=1;
    , HelpContextID = 196035251


  add object oObj_1_142 as cp_runprogram with uid="UTTJGARNHQ",left=14, top=457, width=280,height=20,;
    caption='GSCE_BMC(AMC)',;
   bGlobalFont=.t.,;
    prg='GSCE_BMC("AMC")',;
    cEvent = "Insert end,Update end",;
    nPag=1;
    , HelpContextID = 24685015


  add object oObj_1_143 as cp_runprogram with uid="YIEKRPDJCQ",left=14, top=478, width=280,height=20,;
    caption='GSCE_BMC(AMC)',;
   bGlobalFont=.t.,;
    prg='GSCE_BMC("AMC")',;
    cEvent = "Cancellazione",;
    nPag=1;
    , HelpContextID = 24685015


  add object oObj_1_144 as cp_runprogram with uid="XCFYPHZUOE",left=13, top=520, width=341,height=20,;
    caption='GSCE_BCV(M)',;
   bGlobalFont=.t.,;
    prg='GSCE_BCV("M")',;
    cEvent = "w_MCCODCES Changed,w_MCCOMPET Changed",;
    nPag=1;
    , HelpContextID = 196223548

  add object oUNIMIS_1_148 as StdField with uid="RKBGZEHWED",rtseq=121,rtrep=.f.,;
    cFormVar = "w_UNIMIS", cQueryName = "UNIMIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 78143930,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=85, Top=274, InputMask=replicate('X',3), cLinkFile="UNIMIS", oKey_1_1="UMCODICE", oKey_1_2="this.w_UNIMIS"

  func oUNIMIS_1_148.mHide()
    with this.Parent.oContained
      return (!.w_TIPCES = 'CQ')
    endwith
  endfunc

  func oUNIMIS_1_148.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDESUNI_1_150 as StdField with uid="JLFZYALBNM",rtseq=122,rtrep=.f.,;
    cFormVar = "w_DESUNI", cQueryName = "DESUNI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 240110538,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=149, Top=274, InputMask=replicate('X',35)

  func oDESUNI_1_150.mHide()
    with this.Parent.oContained
      return (!.w_TIPCES = 'CQ')
    endwith
  endfunc

  add object oMCQTACES_1_151 as StdField with uid="XMRZJTUEDR",rtseq=123,rtrep=.f.,;
    cFormVar = "w_MCQTACES", cQueryName = "MCQTACES",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Quantit� errata poich� non intera oppure non congruente al segno impostato sulla causale",;
    ToolTipText = "Quantit� movimentata cespite: in caso di cessione la quantit� deve essere espressa in negativo",;
    HelpContextID = 182391577,;
   bGlobalFont=.t.,;
    Height=21, Width=91, Left=581, Top=275, cSayPict="v_PQ(12)", cGetPict="v_GQ(12)"

  func oMCQTACES_1_151.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPCES = 'CQ' AND .w_FLGA17= 'S')
    endwith
   endif
  endfunc

  func oMCQTACES_1_151.mHide()
    with this.Parent.oContained
      return (!.w_TIPCES = 'CQ')
    endwith
  endfunc

  func oMCQTACES_1_151.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (IIF(.w_FLGFRAZ = 'S', INT(.w_MCQTACES) = .w_MCQTACES, .T.) and IIF(NVL(.w_TIPOAGG,'+') ='+',.w_MCQTACES >0,.w_MCQTACES <0))
    endwith
    return bRes
  endfunc


  add object oObj_1_156 as cp_runprogram with uid="INTGKWOTOG",left=14, top=542, width=280,height=20,;
    caption='GSCE_BMC(QTA)',;
   bGlobalFont=.t.,;
    prg='GSCE_BMC("QTA")',;
    cEvent = "CheckQTA",;
    nPag=1;
    , HelpContextID = 24783319


  add object oBtn_1_169 as StdButton with uid="QOOKDVEIMB",left=736, top=357, width=48,height=45,;
    CpPicture="BMP\CONCLUSI.BMP", caption="", nPag=1;
    , ToolTipText = "Riferimenti registrazioni contabili e documenti";
    , HelpContextID = 200764325;
    , TabStop=.f.,Caption='\<Dati agg.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_169.Click()
      do GSCE_KZP with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_169.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (!.w_ABILITA)
     endwith
    endif
  endfunc

  add object oStr_1_71 as StdString with uid="DJIEWXJKYN",Visible=.t., Left=2, Top=16,;
    Alignment=1, Width=81, Height=15,;
    Caption="Reg.N.:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_72 as StdString with uid="VVWNFKUMEX",Visible=.t., Left=146, Top=17,;
    Alignment=2, Width=10, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_73 as StdString with uid="SMVMOZMIJS",Visible=.t., Left=316, Top=16,;
    Alignment=1, Width=39, Height=15,;
    Caption="Del:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_74 as StdString with uid="HNTGQTTSAB",Visible=.t., Left=2, Top=46,;
    Alignment=1, Width=81, Height=15,;
    Caption="Doc.N.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_75 as StdString with uid="VOCYEOVDWC",Visible=.t., Left=204, Top=47,;
    Alignment=2, Width=9, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_76 as StdString with uid="AOXPUMGKUZ",Visible=.t., Left=321, Top=46,;
    Alignment=1, Width=34, Height=15,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_77 as StdString with uid="XAHDOVENBF",Visible=.t., Left=2, Top=76,;
    Alignment=1, Width=81, Height=15,;
    Caption="Causale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_78 as StdString with uid="QUVUCHYAXY",Visible=.t., Left=1, Top=124,;
    Alignment=1, Width=82, Height=15,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_79 as StdString with uid="DNAASEMYZC",Visible=.t., Left=1, Top=149,;
    Alignment=1, Width=82, Height=15,;
    Caption="Cliente:"  ;
  , bGlobalFont=.t.

  func oStr_1_79.mHide()
    with this.Parent.oContained
      return (.w_MCTIPCON<>'C')
    endwith
  endfunc

  add object oStr_1_80 as StdString with uid="CMDJHDIPXZ",Visible=.t., Left=1, Top=149,;
    Alignment=1, Width=82, Height=15,;
    Caption="Fornitore:"  ;
  , bGlobalFont=.t.

  func oStr_1_80.mHide()
    with this.Parent.oContained
      return (.w_MCTIPCON<>'F')
    endwith
  endfunc

  add object oStr_1_81 as StdString with uid="GRQZQXILFV",Visible=.t., Left=1, Top=174,;
    Alignment=1, Width=82, Height=15,;
    Caption="Cespite:"  ;
  , bGlobalFont=.t.

  add object oStr_1_118 as StdString with uid="YCUYOVHHFA",Visible=.t., Left=487, Top=16,;
    Alignment=1, Width=72, Height=15,;
    Caption="Status:"  ;
  , bGlobalFont=.t.

  add object oStr_1_124 as StdString with uid="OKKPVKHCMT",Visible=.t., Left=1, Top=224,;
    Alignment=1, Width=82, Height=15,;
    Caption="C/C.ricavo:"  ;
  , bGlobalFont=.t.

  func oStr_1_124.mHide()
    with this.Parent.oContained
      return (g_PERCCR<>'S')
    endwith
  endfunc

  add object oStr_1_125 as StdString with uid="MUQCCBRFQE",Visible=.t., Left=1, Top=199,;
    Alignment=1, Width=82, Height=15,;
    Caption="Voce C/R.:"  ;
  , bGlobalFont=.t.

  func oStr_1_125.mHide()
    with this.Parent.oContained
      return (g_PERCCR<>'S')
    endwith
  endfunc

  add object oStr_1_126 as StdString with uid="WWXGPFJQXW",Visible=.t., Left=1, Top=249,;
    Alignment=1, Width=82, Height=15,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_127 as StdString with uid="XIEWASTKKR",Visible=.t., Left=426, Top=102,;
    Alignment=1, Width=157, Height=18,;
    Caption="Data primo utilizzo fiscale:"  ;
  , bGlobalFont=.t.

  func oStr_1_127.mHide()
    with this.Parent.oContained
      return (.w_FLPRIU<>'S' OR .w_PCTIPAMM='C')
    endwith
  endfunc

  add object oStr_1_128 as StdString with uid="IVJPYROVDI",Visible=.t., Left=671, Top=104,;
    Alignment=1, Width=64, Height=15,;
    Caption="Compet.:"  ;
  , bGlobalFont=.t.

  func oStr_1_128.mHide()
    with this.Parent.oContained
      return (.w_FLPRIU<>'S'  OR .w_PCTIPAMM='C')
    endwith
  endfunc

  add object oStr_1_145 as StdString with uid="JTUIBHJAQE",Visible=.t., Left=426, Top=77,;
    Alignment=1, Width=157, Height=16,;
    Caption="Data dismissione:"  ;
  , bGlobalFont=.t.

  func oStr_1_145.mHide()
    with this.Parent.oContained
      return (.w_FLDADI<>'S')
    endwith
  endfunc

  add object oStr_1_147 as StdString with uid="GVHLIYICJH",Visible=.t., Left=440, Top=276,;
    Alignment=1, Width=140, Height=18,;
    Caption="Q.t� movimentata:"  ;
  , bGlobalFont=.t.

  func oStr_1_147.mHide()
    with this.Parent.oContained
      return (!.w_TIPCES = 'CQ')
    endwith
  endfunc

  add object oStr_1_149 as StdString with uid="CJPDPLSFCN",Visible=.t., Left=54, Top=274,;
    Alignment=1, Width=27, Height=18,;
    Caption="U.M.:"  ;
  , bGlobalFont=.t.

  func oStr_1_149.mHide()
    with this.Parent.oContained
      return (!.w_TIPCES = 'CQ')
    endwith
  endfunc

  add object oStr_1_160 as StdString with uid="HASDLPKSCF",Visible=.t., Left=588, Top=337,;
    Alignment=0, Width=196, Height=18,;
    Caption="Riferimenti primanota/documento"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_160.mHide()
    with this.Parent.oContained
      return ( !.w_ABILITA)
    endwith
  endfunc

  add object oStr_1_171 as StdString with uid="JMDYUOVCIX",Visible=.t., Left=671, Top=78,;
    Alignment=1, Width=64, Height=15,;
    Caption="Compet.:"  ;
  , bGlobalFont=.t.

  func oStr_1_171.mHide()
    with this.Parent.oContained
      return ((.w_FLPRIU='S' AND .w_FLPRIC<>'S') OR (.w_FLPRIU='S' And .w_PCTIPAMM='F'))
    endwith
  endfunc

  add object oStr_1_172 as StdString with uid="GPFTMVJPNE",Visible=.t., Left=426, Top=78,;
    Alignment=1, Width=157, Height=15,;
    Caption="Data primo utilizzo civile:"  ;
  , bGlobalFont=.t.

  func oStr_1_172.mHide()
    with this.Parent.oContained
      return (.w_FLPRIC<>'S' OR .w_PCTIPAMM='F')
    endwith
  endfunc
enddefine
define class tgsce_amcPag2 as StdContainer
  Width  = 802
  height = 404
  stdWidth  = 802
  stdheight = 404
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMCCODVAL_2_20 as StdField with uid="JMWPPGFIQO",rtseq=131,rtrep=.f.,;
    cFormVar = "w_MCCODVAL", cQueryName = "MCCODVAL",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice valuta dell'esercizio di competenza",;
    HelpContextID = 235483922,;
   bGlobalFont=.t.,;
    Height=21, Width=42, Left=139, Top=67, InputMask=replicate('X',3), cLinkFile="VALUTE", oKey_1_1="VACODVAL", oKey_1_2="this.w_MCCODVAL"

  func oMCCODVAL_2_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oMCCAOVAL_2_21 as StdField with uid="IPUPEPZDVO",rtseq=132,rtrep=.f.,;
    cFormVar = "w_MCCAOVAL", cQueryName = "MCCAOVAL",;
    bObbl = .t. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Cambio della registrazione",;
    HelpContextID = 246100754,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=296, Top=67, cSayPict='"99999.999999"', cGetPict='"99999.999999"', tabstop=.f.

  func oMCCAOVAL_2_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (GETVALUT(.w_MCCODVAL, "VADATEUR")>=IIF(EMPTY(.w_MCDATDOC),.w_MCDATREG,.w_MCDATDOC) OR .w_CAOVAL=0)
    endwith
   endif
  endfunc

  add object oMCIMPA01_2_28 as StdField with uid="NYIEMSTTIW",rtseq=136,rtrep=.f.,;
    cFormVar = "w_MCIMPA01", cQueryName = "MCIMPA01",;
    bObbl = .t. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "L'importo di ventita deve essere maggiore di zero",;
    HelpContextID = 104361225,;
   bGlobalFont=.t.,;
    Height=21, Width=109, Left=138, Top=108, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oMCIMPA01_2_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLGA01='S')
    endwith
   endif
  endfunc

  func oMCIMPA01_2_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_MCIMPA01>0)
    endwith
    return bRes
  endfunc

  add object oMCIMPA23_2_29 as StdField with uid="PRMQOQRBZV",rtseq=137,rtrep=.f.,;
    cFormVar = "w_MCIMPA23", cQueryName = "MCIMPA23",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    HelpContextID = 164074233,;
   bGlobalFont=.t.,;
    Height=21, Width=116, Left=138, Top=151, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oMCIMPA23_2_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLGA23='S' And not empty(.w_Mccompet)  AND .w_PCTIPAMM<>'F')
    endwith
   endif
  endfunc

  add object oMCIMPA02_2_30 as StdField with uid="PBLRQNLNOB",rtseq=138,rtrep=.f.,;
    cFormVar = "w_MCIMPA02", cQueryName = "MCIMPA02",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    HelpContextID = 104361224,;
   bGlobalFont=.t.,;
    Height=21, Width=116, Left=138, Top=176, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oMCIMPA02_2_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLGA02='S' And not empty(.w_Mccompet)  AND .w_PCTIPAMM<>'C')
    endwith
   endif
  endfunc

  add object oMCIMPA24_2_31 as StdField with uid="POEALAGNRE",rtseq=139,rtrep=.f.,;
    cFormVar = "w_MCIMPA24", cQueryName = "MCIMPA24",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    HelpContextID = 164074234,;
   bGlobalFont=.t.,;
    Height=21, Width=116, Left=138, Top=201, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oMCIMPA24_2_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLGA24='S' And not empty(.w_Mccompet) AND .w_PCTIPAMM<>'F')
    endwith
   endif
  endfunc

  add object oMCIMPA03_2_32 as StdField with uid="KIDGQYZIYL",rtseq=140,rtrep=.f.,;
    cFormVar = "w_MCIMPA03", cQueryName = "MCIMPA03",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    HelpContextID = 104361223,;
   bGlobalFont=.t.,;
    Height=21, Width=116, Left=138, Top=226, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oMCIMPA03_2_32.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLGA03='S' And not empty(.w_Mccompet)  AND .w_PCTIPAMM<>'C')
    endwith
   endif
  endfunc

  add object oMCIMPA18_2_33 as StdField with uid="NMYKIIHOMG",rtseq=141,rtrep=.f.,;
    cFormVar = "w_MCIMPA18", cQueryName = "MCIMPA18",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    HelpContextID = 104361218,;
   bGlobalFont=.t.,;
    Height=21, Width=116, Left=138, Top=251, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oMCIMPA18_2_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLGA18='S' And not empty(.w_Mccompet) AND .w_PCTIPAMM<>'F')
    endwith
   endif
  endfunc


  add object oLinkPC_2_34 as StdButton with uid="YGALOYQKLW",left=643, top=353, width=48,height=45,;
    CpPicture="BMP\COMPONE.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per selezionare i componenti del cespite da movimentare";
    , HelpContextID = 148729556;
    , tabstop=.f., caption='\<Compon.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oLinkPC_2_34.Click()
      this.Parent.oContained.GSCE_MMC.LinkPCClick()
    endproc

  func oLinkPC_2_34.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_FLCAR=.T.)
      endwith
    endif
  endfunc

  func oLinkPC_2_34.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_FLCAR=.F.)
     endwith
    endif
  endfunc

  add object oMCIMPA04_2_35 as StdField with uid="TJHTKWANDQ",rtseq=142,rtrep=.f.,;
    cFormVar = "w_MCIMPA04", cQueryName = "MCIMPA04",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    HelpContextID = 104361222,;
   bGlobalFont=.t.,;
    Height=21, Width=116, Left=138, Top=276, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oMCIMPA04_2_35.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLGA04='S' And not empty(.w_Mccompet)  AND .w_PCTIPAMM<>'C')
    endwith
   endif
  endfunc

  add object oMCIMPA22_2_36 as StdField with uid="MXVLTPDKOK",rtseq=143,rtrep=.f.,;
    cFormVar = "w_MCIMPA22", cQueryName = "MCIMPA22",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    HelpContextID = 164074232,;
   bGlobalFont=.t.,;
    Height=21, Width=116, Left=138, Top=301, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oMCIMPA22_2_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLGA22='S' And not empty(.w_Mccompet) AND .w_PCTIPAMM<>'F')
    endwith
   endif
  endfunc

  add object oMCIMPA05_2_37 as StdField with uid="EBQLOKAYJI",rtseq=144,rtrep=.f.,;
    cFormVar = "w_MCIMPA05", cQueryName = "MCIMPA05",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    HelpContextID = 104361221,;
   bGlobalFont=.t.,;
    Height=21, Width=116, Left=138, Top=326, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oMCIMPA05_2_37.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLGA05='S' And not empty(.w_Mccompet)  AND .w_PCTIPAMM<>'C')
    endwith
   endif
  endfunc

  add object oMCIMPA19_2_38 as StdField with uid="LKSUARJDKO",rtseq=145,rtrep=.f.,;
    cFormVar = "w_MCIMPA19", cQueryName = "MCIMPA19",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    HelpContextID = 104361217,;
   bGlobalFont=.t.,;
    Height=21, Width=116, Left=138, Top=351, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oMCIMPA19_2_38.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLGA19='S' And not empty(.w_Mccompet) AND .w_PCTIPAMM<>'F')
    endwith
   endif
  endfunc

  add object oMCIMPA06_2_39 as StdField with uid="XVXBTJGVIU",rtseq=146,rtrep=.f.,;
    cFormVar = "w_MCIMPA06", cQueryName = "MCIMPA06",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    HelpContextID = 104361220,;
   bGlobalFont=.t.,;
    Height=21, Width=116, Left=138, Top=376, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oMCIMPA06_2_39.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLGA06='S' And not empty(.w_Mccompet)  AND .w_PCTIPAMM<>'C')
    endwith
   endif
  endfunc

  add object oMCIMPA07_2_40 as StdField with uid="OYBYITGKVA",rtseq=147,rtrep=.f.,;
    cFormVar = "w_MCIMPA07", cQueryName = "MCIMPA07",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    HelpContextID = 104361219,;
   bGlobalFont=.t.,;
    Height=21, Width=109, Left=399, Top=151, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oMCIMPA07_2_40.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLGA07='S' AND .w_PCTIPAMM<>'F' And not empty(.w_Mccompet) And .w_Censoamm<>'S' And .w_Ccnsoamm<>'S')
    endwith
   endif
  endfunc

  add object oCODCAU_2_41 as StdField with uid="DGQTISPCCX",rtseq=148,rtrep=.f.,;
    cFormVar = "w_CODCAU", cQueryName = "CODCAU",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 53653978,;
   bGlobalFont=.t.,;
    Height=21, Width=85, Left=139, Top=14, InputMask=replicate('X',5)

  add object oMCCOECIV_2_42 as StdField with uid="MIYFPOYUXJ",rtseq=149,rtrep=.f.,;
    cFormVar = "w_MCCOECIV", cQueryName = "MCCOECIV",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Coefficiente civile utilizzato per la stampa libro cespiti",;
    HelpContextID = 186200860,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=509, Top=151, cSayPict='"999.99"', cGetPict='"999.99"'

  func oMCCOECIV_2_42.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLGA07='S' AND .w_PCTIPAMM<>'F')
    endwith
   endif
  endfunc

  add object oMCIMPA08_2_43 as StdField with uid="JGPERJDMCI",rtseq=150,rtrep=.f.,;
    cFormVar = "w_MCIMPA08", cQueryName = "MCIMPA08",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    HelpContextID = 104361218,;
   bGlobalFont=.t.,;
    Height=21, Width=109, Left=399, Top=176, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oMCIMPA08_2_43.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLGA08='S' AND .w_PCTIPAMM<>'F' And not empty(.w_Mccompet))
    endwith
   endif
  endfunc

  add object oMCIMPA09_2_44 as StdField with uid="RWSFEKBVZM",rtseq=151,rtrep=.f.,;
    cFormVar = "w_MCIMPA09", cQueryName = "MCIMPA09",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    HelpContextID = 104361217,;
   bGlobalFont=.t.,;
    Height=21, Width=109, Left=399, Top=226, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oMCIMPA09_2_44.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLGA09='S' AND .w_PCTIPAMM<>'C' And not empty(.w_Mccompet) And .w_Censoamf<>'S' And .w_Ccnsoamf<>'S')
    endwith
   endif
  endfunc

  add object oDESCAU_2_45 as StdField with uid="BROXJADBKK",rtseq=152,rtrep=.f.,;
    cFormVar = "w_DESCAU", cQueryName = "DESCAU",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 53595082,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=296, Top=14, InputMask=replicate('X',40)

  add object oMCCOEFIS_2_46 as StdField with uid="YNOCMVYZMM",rtseq=153,rtrep=.f.,;
    cFormVar = "w_MCCOEFIS", cQueryName = "MCCOEFIS",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Coefficiente fiscale utilizzato per la stampa libro cespiti",;
    HelpContextID = 236532505,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=509, Top=226, cSayPict='"999.99"', cGetPict='"999.99"'

  func oMCCOEFIS_2_46.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLGA09='S'  AND .w_PCTIPAMM<>'C')
    endwith
   endif
  endfunc

  add object oMCIMPA10_2_48 as StdField with uid="OKTWIMHJCV",rtseq=154,rtrep=.f.,;
    cFormVar = "w_MCIMPA10", cQueryName = "MCIMPA10",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    HelpContextID = 104361226,;
   bGlobalFont=.t.,;
    Height=21, Width=109, Left=399, Top=251, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oMCIMPA10_2_48.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLGA10='S'  AND .w_PCTIPAMM<>'C' And not empty(.w_Mccompet))
    endwith
   endif
  endfunc

  add object oMCIMPA15_2_50 as StdField with uid="QKZOBUWTLQ",rtseq=155,rtrep=.f.,;
    cFormVar = "w_MCIMPA15", cQueryName = "MCIMPA15",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    HelpContextID = 104361221,;
   bGlobalFont=.t.,;
    Height=21, Width=109, Left=399, Top=276, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oMCIMPA15_2_50.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLGA15='S'  AND .w_PCTIPAMM<>'C' And not empty(.w_Mccompet) And .w_Censoamf<>'S' And .w_Ccnsoamf<>'S')
    endwith
   endif
  endfunc

  add object oCODCES_2_51 as StdField with uid="ENWWHARMDJ",rtseq=156,rtrep=.f.,;
    cFormVar = "w_CODCES", cQueryName = "CODCES",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 83014106,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=139, Top=39, InputMask=replicate('X',20)

  add object oMCCOEFI1_2_52 as StdField with uid="ZFSIAWGHGC",rtseq=157,rtrep=.f.,;
    cFormVar = "w_MCCOEFI1", cQueryName = "MCCOEFI1",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Coefficiente anticipato utilizzato per la stampa libro cespiti",;
    HelpContextID = 236532471,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=509, Top=276, cSayPict='"999.99"', cGetPict='"999.99"'

  func oMCCOEFI1_2_52.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLGA15='S'  AND .w_PCTIPAMM<>'C')
    endwith
   endif
  endfunc

  add object oMCIMPA16_2_53 as StdField with uid="UQPELEYIKD",rtseq=158,rtrep=.f.,;
    cFormVar = "w_MCIMPA16", cQueryName = "MCIMPA16",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    HelpContextID = 104361220,;
   bGlobalFont=.t.,;
    Height=21, Width=109, Left=399, Top=301, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oMCIMPA16_2_53.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLGA16='S'  AND .w_PCTIPAMM<>'C' And not empty(.w_Mccompet))
    endwith
   endif
  endfunc

  add object oDESCES_2_54 as StdField with uid="OPLDCKXRUL",rtseq=159,rtrep=.f.,;
    cFormVar = "w_DESCES", cQueryName = "DESCES",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 82955210,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=296, Top=39, InputMask=replicate('X',40)

  add object oMCIMPA11_2_55 as StdField with uid="YUYZMZZDXW",rtseq=160,rtrep=.f.,;
    cFormVar = "w_MCIMPA11", cQueryName = "MCIMPA11",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo del bene non ammortizzabile",;
    HelpContextID = 104361225,;
   bGlobalFont=.t.,;
    Height=21, Width=109, Left=686, Top=151, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oMCIMPA11_2_55.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLGA11='S' AND .w_PCTIPAMM<>'C' And not empty(.w_Mccompet))
    endwith
   endif
  endfunc

  add object oMCIMPA12_2_56 as StdField with uid="BWLQHYLJCO",rtseq=161,rtrep=.f.,;
    cFormVar = "w_MCIMPA12", cQueryName = "MCIMPA12",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quote perse di accantonamento",;
    HelpContextID = 104361224,;
   bGlobalFont=.t.,;
    Height=21, Width=109, Left=686, Top=176, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oMCIMPA12_2_56.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLGA12='S' AND .w_PCTIPAMM<>'C' And not empty(.w_Mccompet))
    endwith
   endif
  endfunc

  add object oMCIMPA20_2_57 as StdField with uid="UFTORHHYLU",rtseq=162,rtrep=.f.,;
    cFormVar = "w_MCIMPA20", cQueryName = "MCIMPA20",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    HelpContextID = 164074230,;
   bGlobalFont=.t.,;
    Height=21, Width=109, Left=686, Top=226, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oMCIMPA20_2_57.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLGA20='S' And not empty(.w_Mccompet) AND .w_PCTIPAMM<>'F')
    endwith
   endif
  endfunc

  add object oMCIMPA13_2_58 as StdField with uid="TXTOHOHTHI",rtseq=163,rtrep=.f.,;
    cFormVar = "w_MCIMPA13", cQueryName = "MCIMPA13",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    HelpContextID = 104361223,;
   bGlobalFont=.t.,;
    Height=21, Width=109, Left=686, Top=251, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oMCIMPA13_2_58.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLGA13='S' And not empty(.w_Mccompet)  AND .w_PCTIPAMM<>'C')
    endwith
   endif
  endfunc

  add object oMCIMPA21_2_59 as StdField with uid="JLOBLRUYHG",rtseq=164,rtrep=.f.,;
    cFormVar = "w_MCIMPA21", cQueryName = "MCIMPA21",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    HelpContextID = 164074231,;
   bGlobalFont=.t.,;
    Height=21, Width=109, Left=686, Top=276, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oMCIMPA21_2_59.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLGA21='S' And not empty(.w_Mccompet) AND .w_PCTIPAMM<>'F')
    endwith
   endif
  endfunc

  add object oMCIMPA14_2_60 as StdField with uid="QYYEFNMVBH",rtseq=165,rtrep=.f.,;
    cFormVar = "w_MCIMPA14", cQueryName = "MCIMPA14",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    HelpContextID = 104361222,;
   bGlobalFont=.t.,;
    Height=21, Width=109, Left=686, Top=301, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oMCIMPA14_2_60.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLGA14='S' And not empty(.w_Mccompet)  AND .w_PCTIPAMM<>'C')
    endwith
   endif
  endfunc


  add object oLinkPC_2_61 as StdButton with uid="SXSPPGRLZW",left=643, top=353, width=48,height=45,;
    CpPicture="BMP\COMPONE.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per selezionare i componenti del cespite da scaricare";
    , HelpContextID = 148729556;
    , tabstop=.f., caption='\<Compon.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oLinkPC_2_61.Click()
      this.Parent.oContained.GSCE_MMS.LinkPCClick()
    endproc

  func oLinkPC_2_61.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_FLSCA=.T.)
      endwith
    endif
  endfunc

  func oLinkPC_2_61.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_FLSCA=.F.)
     endwith
    endif
  endfunc


  add object oBtn_2_62 as StdButton with uid="LVSUMTDDCW",left=694, top=353, width=48,height=45,;
    CpPicture="BMP\DETTAGLI.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per visualizzare i valori di calcolo legati al cespite";
    , HelpContextID = 251566945;
    , tabstop=.f., caption='\<Dettagli';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_62.Click()
      do GSCE_KDC with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_62.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_MCCODCAU) AND NOT EMPTY(.w_MCCODCES) AND UPPER(.w_TIPOPE)='LOAD')
      endwith
    endif
  endfunc


  add object oBtn_2_63 as StdButton with uid="CVAQLNPOZI",left=747, top=353, width=48,height=45,;
    CpPicture="BMP\CALCOLA.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per elaborare le formule collegate alla causale cespite";
    , HelpContextID = 214842182;
    , tabstop=.f., caption='\<Elabora';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_63.Click()
      with this.Parent.oContained
        GSCE_BC2(this.Parent.oContained,"C")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_63.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_MCCODCAU) AND NOT EMPTY(.w_MCCODCES) AND .w_NFOR<>0 AND UPPER(.w_TIPOPE)='LOAD')
      endwith
    endif
  endfunc

  func oBtn_2_63.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (NOT (.w_NFOR<>0 AND UPPER(.w_TIPOPE)='LOAD'))
     endwith
    endif
  endfunc


  add object oObj_2_64 as cp_runprogram with uid="RPHZBJDIIH",left=15, top=425, width=209,height=20,;
    caption='GSCE_BC2(P)',;
   bGlobalFont=.t.,;
    prg='GSCE_BC2("P")',;
    cEvent = "w_MCIMPA01 Changed",;
    nPag=2;
    , HelpContextID = 196224280

  add object oSIMVAL_2_67 as StdField with uid="TSKBNJLDZQ",rtseq=166,rtrep=.f.,;
    cFormVar = "w_SIMVAL", cQueryName = "SIMVAL",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 203368154,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=189, Top=67, InputMask=replicate('X',5)

  add object oStr_2_1 as StdString with uid="FUGMEJRFZN",Visible=.t., Left=254, Top=302,;
    Alignment=1, Width=145, Height=18,;
    Caption="Utilizzazioni anticipate:"  ;
  , bGlobalFont=.t.

  add object oStr_2_2 as StdString with uid="ISFNDTHHMF",Visible=.t., Left=283, Top=277,;
    Alignment=1, Width=116, Height=18,;
    Caption="Accanton. anticipato:"  ;
  , bGlobalFont=.t.

  add object oStr_2_3 as StdString with uid="EVWEAJDJRT",Visible=.t., Left=271, Top=204,;
    Alignment=0, Width=238, Height=18,;
    Caption="Accantonamenti e utilizzazioni fiscali"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_4 as StdString with uid="RYAZWVPVJT",Visible=.t., Left=-4, Top=377,;
    Alignment=1, Width=142, Height=18,;
    Caption="Svalutazioni fiscali:"  ;
  , bGlobalFont=.t.

  add object oStr_2_5 as StdString with uid="IXOVMZVNOW",Visible=.t., Left=560, Top=153,;
    Alignment=1, Width=126, Height=15,;
    Caption="Importo non amm.le:"  ;
  , bGlobalFont=.t.

  add object oStr_2_6 as StdString with uid="VLTWLPDTHE",Visible=.t., Left=499, Top=132,;
    Alignment=2, Width=45, Height=15,;
    Caption="%"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_7 as StdString with uid="KYKNDMDCRJ",Visible=.t., Left=20, Top=132,;
    Alignment=0, Width=231, Height=15,;
    Caption="Valore del bene e variazioni"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_8 as StdString with uid="NWBPIXUBSV",Visible=.t., Left=554, Top=204,;
    Alignment=0, Width=223, Height=15,;
    Caption="Plusvalenze e minusvalenze"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_9 as StdString with uid="WNJQXJRURM",Visible=.t., Left=554, Top=132,;
    Alignment=0, Width=249, Height=15,;
    Caption="Importo non ammortizzabile e q.perse"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_10 as StdString with uid="RYYYVIETYB",Visible=.t., Left=573, Top=301,;
    Alignment=1, Width=113, Height=15,;
    Caption="Minus. fiscale:"  ;
  , bGlobalFont=.t.

  add object oStr_2_11 as StdString with uid="ZENXDAEWCN",Visible=.t., Left=573, Top=251,;
    Alignment=1, Width=113, Height=15,;
    Caption="Plus. fiscale:"  ;
  , bGlobalFont=.t.

  add object oStr_2_12 as StdString with uid="MDAOTENAOZ",Visible=.t., Left=560, Top=176,;
    Alignment=1, Width=126, Height=15,;
    Caption="Quote perse:"  ;
  , bGlobalFont=.t.

  add object oStr_2_13 as StdString with uid="TZZFYSSLGG",Visible=.t., Left=254, Top=251,;
    Alignment=1, Width=145, Height=18,;
    Caption="Utilizzazioni ordinarie:"  ;
  , bGlobalFont=.t.

  add object oStr_2_14 as StdString with uid="WIGFSFKEZQ",Visible=.t., Left=286, Top=226,;
    Alignment=1, Width=113, Height=18,;
    Caption="Accanton. ordinario:"  ;
  , bGlobalFont=.t.

  add object oStr_2_15 as StdString with uid="VDPJIMDDKX",Visible=.t., Left=286, Top=176,;
    Alignment=1, Width=113, Height=15,;
    Caption="Utilizzazioni civili:"  ;
  , bGlobalFont=.t.

  add object oStr_2_16 as StdString with uid="KOXEUAOHUZ",Visible=.t., Left=286, Top=151,;
    Alignment=1, Width=113, Height=15,;
    Caption="Accanton. civili:"  ;
  , bGlobalFont=.t.

  add object oStr_2_17 as StdString with uid="WXNGHRXVYP",Visible=.t., Left=271, Top=131,;
    Alignment=0, Width=223, Height=18,;
    Caption="Accantonamenti e utilizzazioni civili"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_18 as StdString with uid="KHDUAHSTWX",Visible=.t., Left=-4, Top=326,;
    Alignment=1, Width=142, Height=18,;
    Caption="Decr.di valore fiscali:"  ;
  , bGlobalFont=.t.

  add object oStr_2_19 as StdString with uid="YMDWISXLWY",Visible=.t., Left=-4, Top=275,;
    Alignment=1, Width=142, Height=18,;
    Caption="Rivalutazioni fiscali:"  ;
  , bGlobalFont=.t.

  add object oStr_2_25 as StdString with uid="HDWODFXJFO",Visible=.t., Left=-4, Top=224,;
    Alignment=1, Width=142, Height=18,;
    Caption="Oneri e spese fiscali:"  ;
  , bGlobalFont=.t.

  add object oStr_2_26 as StdString with uid="MFGWUFJERV",Visible=.t., Left=-4, Top=176,;
    Alignment=1, Width=142, Height=15,;
    Caption="Incr.di valore fiscali:"  ;
  , bGlobalFont=.t.

  add object oStr_2_27 as StdString with uid="IRSHTVVKSS",Visible=.t., Left=18, Top=108,;
    Alignment=1, Width=120, Height=15,;
    Caption="Importo vendita:"  ;
  , bGlobalFont=.t.

  add object oStr_2_47 as StdString with uid="SDATCVIZKN",Visible=.t., Left=63, Top=15,;
    Alignment=1, Width=74, Height=15,;
    Caption="Causale:"  ;
  , bGlobalFont=.t.

  add object oStr_2_49 as StdString with uid="HMPHMPYYNI",Visible=.t., Left=63, Top=40,;
    Alignment=1, Width=74, Height=15,;
    Caption="Cespite:"  ;
  , bGlobalFont=.t.

  add object oStr_2_65 as StdString with uid="HSQXIOUXJX",Visible=.t., Left=63, Top=67,;
    Alignment=1, Width=74, Height=15,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_2_66 as StdString with uid="IGXZYZDJDX",Visible=.t., Left=229, Top=68,;
    Alignment=1, Width=65, Height=17,;
    Caption="Cambio:"  ;
  , bGlobalFont=.t.

  add object oStr_2_69 as StdString with uid="TANHGQXCNK",Visible=.t., Left=-4, Top=152,;
    Alignment=1, Width=142, Height=15,;
    Caption="Incr. di valore civili:"  ;
  , bGlobalFont=.t.

  add object oStr_2_70 as StdString with uid="HROFLYXBXE",Visible=.t., Left=-4, Top=200,;
    Alignment=1, Width=142, Height=15,;
    Caption="Oneri e spese civili:"  ;
  , bGlobalFont=.t.

  add object oStr_2_71 as StdString with uid="SXCDBEGCFO",Visible=.t., Left=-4, Top=251,;
    Alignment=1, Width=142, Height=15,;
    Caption="Rivalutazioni civili:"  ;
  , bGlobalFont=.t.

  add object oStr_2_72 as StdString with uid="AAOZAMPZTS",Visible=.t., Left=-4, Top=302,;
    Alignment=1, Width=142, Height=15,;
    Caption="Decr. di valore civili:"  ;
  , bGlobalFont=.t.

  add object oStr_2_73 as StdString with uid="HGICFSPWSY",Visible=.t., Left=-4, Top=353,;
    Alignment=1, Width=142, Height=15,;
    Caption="Svalutazioni civili:"  ;
  , bGlobalFont=.t.

  add object oStr_2_74 as StdString with uid="RNFOXEQYLP",Visible=.t., Left=573, Top=226,;
    Alignment=1, Width=113, Height=15,;
    Caption="Plus. civile:"  ;
  , bGlobalFont=.t.

  add object oStr_2_75 as StdString with uid="VIGGZBTMOH",Visible=.t., Left=573, Top=276,;
    Alignment=1, Width=113, Height=15,;
    Caption="Minus. civile:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsce_amc','MOV_CESP','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".MCSERIAL=MOV_CESP.MCSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
