* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_sco                                                        *
*              Stampa controllo saldi cespiti                                  *
*                                                                              *
*      Author: Zucchetti spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30] [VRS_40]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-03-23                                                      *
* Last revis.: 2009-10-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsce_sco",oParentObject))

* --- Class definition
define class tgsce_sco as StdForm
  Top    = 50
  Left   = 104

  * --- Standard Properties
  Width  = 569
  Height = 384
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-10-21"
  HelpContextID=177584791
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=31

  * --- Constant Properties
  _IDX = 0
  CAU_CESP_IDX = 0
  CAT_CESP_IDX = 0
  CES_PITI_IDX = 0
  AZIENDA_IDX = 0
  BUSIUNIT_IDX = 0
  ESERCIZI_IDX = 0
  GRU_COCE_IDX = 0
  VALUTE_IDX = 0
  CENCOST_IDX = 0
  cPrg = "gsce_sco"
  cComment = "Stampa controllo saldi cespiti"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_AZIENDA = space(10)
  w_gestbu = space(1)
  w_COMPET = space(4)
  w_TipoBeni = space(1)
  w_DAGRUPPO = space(15)
  o_DAGRUPPO = space(15)
  w_AGRUPPO = space(15)
  w_DACATEGO = space(15)
  o_DACATEGO = space(15)
  w_ACATEGO = space(15)
  w_DACESPIT = space(20)
  o_DACESPIT = space(20)
  w_ACESPIT = space(20)
  w_DACENTRO = space(20)
  o_DACENTRO = space(20)
  w_ACENTRO = space(20)
  w_DATAPRN = ctod('  /  /  ')
  o_DATAPRN = ctod('  /  /  ')
  w_DESCAT = space(40)
  w_DESCES = space(40)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_MONABB = space(1)
  w_DESGRU1 = space(40)
  w_DESGRU2 = space(40)
  w_DESCAT1 = space(40)
  w_DESCES1 = space(40)
  w_FINEES = ctod('  /  /  ')
  w_VALUTA = space(3)
  w_DECIMALI = 0
  w_DESPIA = space(40)
  w_DESPIA1 = space(40)
  w_BUN = space(3)
  w_BDSI = space(40)
  w_VALNAZ = space(3)
  w_INIZES = ctod('  /  /  ')
  * --- Area Manuale = Declare Variables
  * --- gsce_sco
  * --- Eseuo le query in modo sincrono
  Sync = .t.
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsce_scoPag1","gsce_sco",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCOMPET_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[9]
    this.cWorkTables[1]='CAU_CESP'
    this.cWorkTables[2]='CAT_CESP'
    this.cWorkTables[3]='CES_PITI'
    this.cWorkTables[4]='AZIENDA'
    this.cWorkTables[5]='BUSIUNIT'
    this.cWorkTables[6]='ESERCIZI'
    this.cWorkTables[7]='GRU_COCE'
    this.cWorkTables[8]='VALUTE'
    this.cWorkTables[9]='CENCOST'
    return(this.OpenAllTables(9))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      vx_exec(""+alltrim(THIS.w_OQRY)+", "+alltrim(THIS.w_orep)+"",this)
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_AZIENDA=space(10)
      .w_gestbu=space(1)
      .w_COMPET=space(4)
      .w_TipoBeni=space(1)
      .w_DAGRUPPO=space(15)
      .w_AGRUPPO=space(15)
      .w_DACATEGO=space(15)
      .w_ACATEGO=space(15)
      .w_DACESPIT=space(20)
      .w_ACESPIT=space(20)
      .w_DACENTRO=space(20)
      .w_ACENTRO=space(20)
      .w_DATAPRN=ctod("  /  /  ")
      .w_DESCAT=space(40)
      .w_DESCES=space(40)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_MONABB=space(1)
      .w_DESGRU1=space(40)
      .w_DESGRU2=space(40)
      .w_DESCAT1=space(40)
      .w_DESCES1=space(40)
      .w_FINEES=ctod("  /  /  ")
      .w_VALUTA=space(3)
      .w_DECIMALI=0
      .w_DESPIA=space(40)
      .w_DESPIA1=space(40)
      .w_BUN=space(3)
      .w_BDSI=space(40)
      .w_VALNAZ=space(3)
      .w_INIZES=ctod("  /  /  ")
        .w_AZIENDA = i_CODAZI
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_AZIENDA))
          .link_1_1('Full')
        endif
          .DoRTCalc(2,2,.f.)
        .w_COMPET = g_CODESE
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_COMPET))
          .link_1_3('Full')
        endif
        .w_TipoBeni = 'T'
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_DAGRUPPO))
          .link_1_5('Full')
        endif
        .w_AGRUPPO = IIF(EMPTY(.w_AGRUPPO), .w_DAGRUPPO, .w_AGRUPPO)
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_AGRUPPO))
          .link_1_6('Full')
        endif
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_DACATEGO))
          .link_1_7('Full')
        endif
        .w_ACATEGO = IIF(EMPTY(.w_ACATEGO), .w_DACATEGO, .w_ACATEGO)
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_ACATEGO))
          .link_1_8('Full')
        endif
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_DACESPIT))
          .link_1_9('Full')
        endif
        .w_ACESPIT = IIF(EMPTY(.w_ACESPIT), .w_DACESPIT, .w_ACESPIT)
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_ACESPIT))
          .link_1_10('Full')
        endif
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_DACENTRO))
          .link_1_11('Full')
        endif
        .w_ACENTRO = IIF(EMPTY(.w_ACENTRO), .w_DACENTRO, .w_ACENTRO)
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_ACENTRO))
          .link_1_12('Full')
        endif
        .w_DATAPRN = i_DATSYS
          .DoRTCalc(14,15,.f.)
        .w_OBTEST = .w_DATAPRN
          .DoRTCalc(17,17,.f.)
        .w_MONABB = 'N'
          .DoRTCalc(19,23,.f.)
        .w_VALUTA = g_PERVAL
        .DoRTCalc(24,24,.f.)
        if not(empty(.w_VALUTA))
          .link_1_34('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_42.Calculate()
          .DoRTCalc(25,27,.f.)
        .w_BUN = iif(g_APPLICATION='ADHOC Enterprise',IIF(g_PERBUN<>'N', g_CODBUN, SPACE(3)),space(3))
        .DoRTCalc(28,28,.f.)
        if not(empty(.w_BUN))
          .link_1_44('Full')
        endif
    endwith
    this.DoRTCalc(29,31,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_41.enabled = this.oPgFrm.Page1.oPag.oBtn_1_41.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_43.enabled = this.oPgFrm.Page1.oPag.oBtn_1_43.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,5,.t.)
        if .o_DAGRUPPO<>.w_DAGRUPPO
            .w_AGRUPPO = IIF(EMPTY(.w_AGRUPPO), .w_DAGRUPPO, .w_AGRUPPO)
          .link_1_6('Full')
        endif
        .DoRTCalc(7,7,.t.)
        if .o_DACATEGO<>.w_DACATEGO
            .w_ACATEGO = IIF(EMPTY(.w_ACATEGO), .w_DACATEGO, .w_ACATEGO)
          .link_1_8('Full')
        endif
        .DoRTCalc(9,9,.t.)
        if .o_DACESPIT<>.w_DACESPIT
            .w_ACESPIT = IIF(EMPTY(.w_ACESPIT), .w_DACESPIT, .w_ACESPIT)
          .link_1_10('Full')
        endif
        .DoRTCalc(11,11,.t.)
        if .o_DACENTRO<>.w_DACENTRO
            .w_ACENTRO = IIF(EMPTY(.w_ACENTRO), .w_DACENTRO, .w_ACENTRO)
          .link_1_12('Full')
        endif
        .DoRTCalc(13,15,.t.)
        if .o_DATAPRN<>.w_DATAPRN
            .w_OBTEST = .w_DATAPRN
        endif
        .DoRTCalc(17,23,.t.)
          .link_1_34('Full')
        .oPgFrm.Page1.oPag.oObj_1_42.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(25,31,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_42.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBUN_1_44.enabled = this.oPgFrm.Page1.oPag.oBUN_1_44.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_43.enabled = this.oPgFrm.Page1.oPag.oBtn_1_43.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oBUN_1_44.visible=!this.oPgFrm.Page1.oPag.oBUN_1_44.mHide()
    this.oPgFrm.Page1.oPag.oBDSI_1_45.visible=!this.oPgFrm.Page1.oPag.oBDSI_1_45.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_46.visible=!this.oPgFrm.Page1.oPag.oStr_1_46.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_42.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=AZIENDA
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AZIENDA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AZIENDA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZFLBUNI";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_AZIENDA)
            select AZCODAZI,AZFLBUNI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AZIENDA = NVL(_Link_.AZCODAZI,space(10))
      this.w_gestbu = NVL(_Link_.AZFLBUNI,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_AZIENDA = space(10)
      endif
      this.w_gestbu = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AZIENDA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COMPET
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COMPET) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_COMPET)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESFINESE,ESVALNAZ,ESINIESE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_AZIENDA;
                     ,'ESCODESE',trim(this.w_COMPET))
          select ESCODAZI,ESCODESE,ESFINESE,ESVALNAZ,ESINIESE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COMPET)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COMPET) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oCOMPET_1_3'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_AZIENDA<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESFINESE,ESVALNAZ,ESINIESE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESFINESE,ESVALNAZ,ESINIESE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESFINESE,ESVALNAZ,ESINIESE";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESFINESE,ESVALNAZ,ESINIESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COMPET)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESFINESE,ESVALNAZ,ESINIESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_COMPET);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_AZIENDA;
                       ,'ESCODESE',this.w_COMPET)
            select ESCODAZI,ESCODESE,ESFINESE,ESVALNAZ,ESINIESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COMPET = NVL(_Link_.ESCODESE,space(4))
      this.w_FINEES = NVL(cp_ToDate(_Link_.ESFINESE),ctod("  /  /  "))
      this.w_VALNAZ = NVL(_Link_.ESVALNAZ,space(3))
      this.w_INIZES = NVL(cp_ToDate(_Link_.ESINIESE),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_COMPET = space(4)
      endif
      this.w_FINEES = ctod("  /  /  ")
      this.w_VALNAZ = space(3)
      this.w_INIZES = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COMPET Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DAGRUPPO
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRU_COCE_IDX,3]
    i_lTable = "GRU_COCE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRU_COCE_IDX,2], .t., this.GRU_COCE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRU_COCE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DAGRUPPO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCE_AGP',True,'GRU_COCE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GPCODICE like "+cp_ToStrODBC(trim(this.w_DAGRUPPO)+"%");

          i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI,GPDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GPCODICE',trim(this.w_DAGRUPPO))
          select GPCODICE,GPDESCRI,GPDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DAGRUPPO)==trim(_Link_.GPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DAGRUPPO) and !this.bDontReportError
            deferred_cp_zoom('GRU_COCE','*','GPCODICE',cp_AbsName(oSource.parent,'oDAGRUPPO_1_5'),i_cWhere,'GSCE_AGP',"Gruppi contabili",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI,GPDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where GPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GPCODICE',oSource.xKey(1))
            select GPCODICE,GPDESCRI,GPDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DAGRUPPO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI,GPDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where GPCODICE="+cp_ToStrODBC(this.w_DAGRUPPO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GPCODICE',this.w_DAGRUPPO)
            select GPCODICE,GPDESCRI,GPDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DAGRUPPO = NVL(_Link_.GPCODICE,space(15))
      this.w_DESGRU1 = NVL(_Link_.GPDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.GPDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_DAGRUPPO = space(15)
      endif
      this.w_DESGRU1 = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) and (.w_DAGRUPPO <= .w_AGRUPPO or EMPTY(.w_AGRUPPO))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Gruppo contabile inesistente, non corretto, oppure obsoleto")
        endif
        this.w_DAGRUPPO = space(15)
        this.w_DESGRU1 = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRU_COCE_IDX,2])+'\'+cp_ToStr(_Link_.GPCODICE,1)
      cp_ShowWarn(i_cKey,this.GRU_COCE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DAGRUPPO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=AGRUPPO
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRU_COCE_IDX,3]
    i_lTable = "GRU_COCE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRU_COCE_IDX,2], .t., this.GRU_COCE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRU_COCE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AGRUPPO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCE_AGP',True,'GRU_COCE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GPCODICE like "+cp_ToStrODBC(trim(this.w_AGRUPPO)+"%");

          i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI,GPDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GPCODICE',trim(this.w_AGRUPPO))
          select GPCODICE,GPDESCRI,GPDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_AGRUPPO)==trim(_Link_.GPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_AGRUPPO) and !this.bDontReportError
            deferred_cp_zoom('GRU_COCE','*','GPCODICE',cp_AbsName(oSource.parent,'oAGRUPPO_1_6'),i_cWhere,'GSCE_AGP',"Gruppi contabili",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI,GPDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where GPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GPCODICE',oSource.xKey(1))
            select GPCODICE,GPDESCRI,GPDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AGRUPPO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI,GPDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where GPCODICE="+cp_ToStrODBC(this.w_AGRUPPO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GPCODICE',this.w_AGRUPPO)
            select GPCODICE,GPDESCRI,GPDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AGRUPPO = NVL(_Link_.GPCODICE,space(15))
      this.w_DESGRU2 = NVL(_Link_.GPDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.GPDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_AGRUPPO = space(15)
      endif
      this.w_DESGRU2 = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) and .w_DAGRUPPO <= .w_AGRUPPO
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Gruppo contabile inesistente, non corretto, oppure obsoleto")
        endif
        this.w_AGRUPPO = space(15)
        this.w_DESGRU2 = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRU_COCE_IDX,2])+'\'+cp_ToStr(_Link_.GPCODICE,1)
      cp_ShowWarn(i_cKey,this.GRU_COCE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AGRUPPO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DACATEGO
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_CESP_IDX,3]
    i_lTable = "CAT_CESP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_CESP_IDX,2], .t., this.CAT_CESP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_CESP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DACATEGO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCE_ACT',True,'CAT_CESP')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_DACATEGO)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_DACATEGO))
          select CCCODICE,CCDESCRI,CCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DACATEGO)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStrODBC(trim(this.w_DACATEGO)+"%");

            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStr(trim(this.w_DACATEGO)+"%");

            select CCCODICE,CCDESCRI,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_DACATEGO) and !this.bDontReportError
            deferred_cp_zoom('CAT_CESP','*','CCCODICE',cp_AbsName(oSource.parent,'oDACATEGO_1_7'),i_cWhere,'GSCE_ACT',"Categorie cespiti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DACATEGO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_DACATEGO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_DACATEGO)
            select CCCODICE,CCDESCRI,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DACATEGO = NVL(_Link_.CCCODICE,space(15))
      this.w_DESCAT = NVL(_Link_.CCDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_DACATEGO = space(15)
      endif
      this.w_DESCAT = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND (.w_DACATEGO <= .w_ACATEGO or EMPTY(.w_ACATEGO))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Categoria inesistente, non corretta, oppure obsoleta")
        endif
        this.w_DACATEGO = space(15)
        this.w_DESCAT = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_CESP_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_CESP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DACATEGO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ACATEGO
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_CESP_IDX,3]
    i_lTable = "CAT_CESP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_CESP_IDX,2], .t., this.CAT_CESP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_CESP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ACATEGO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCE_ACT',True,'CAT_CESP')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_ACATEGO)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_ACATEGO))
          select CCCODICE,CCDESCRI,CCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ACATEGO)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStrODBC(trim(this.w_ACATEGO)+"%");

            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStr(trim(this.w_ACATEGO)+"%");

            select CCCODICE,CCDESCRI,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ACATEGO) and !this.bDontReportError
            deferred_cp_zoom('CAT_CESP','*','CCCODICE',cp_AbsName(oSource.parent,'oACATEGO_1_8'),i_cWhere,'GSCE_ACT',"Categorie cespiti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ACATEGO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_ACATEGO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_ACATEGO)
            select CCCODICE,CCDESCRI,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ACATEGO = NVL(_Link_.CCCODICE,space(15))
      this.w_DESCAT1 = NVL(_Link_.CCDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ACATEGO = space(15)
      endif
      this.w_DESCAT1 = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND (.w_DACATEGO <= .w_ACATEGO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Categoria inesistente, non corretta, oppure obsoleta")
        endif
        this.w_ACATEGO = space(15)
        this.w_DESCAT1 = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_CESP_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_CESP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ACATEGO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DACESPIT
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CES_PITI_IDX,3]
    i_lTable = "CES_PITI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CES_PITI_IDX,2], .t., this.CES_PITI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CES_PITI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DACESPIT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCE_ACE',True,'CES_PITI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CECODICE like "+cp_ToStrODBC(trim(this.w_DACESPIT)+"%");

          i_ret=cp_SQL(i_nConn,"select CECODICE,CEDESCRI,CEDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CECODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CECODICE',trim(this.w_DACESPIT))
          select CECODICE,CEDESCRI,CEDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CECODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DACESPIT)==trim(_Link_.CECODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CEDESCRI like "+cp_ToStrODBC(trim(this.w_DACESPIT)+"%");

            i_ret=cp_SQL(i_nConn,"select CECODICE,CEDESCRI,CEDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CEDESCRI like "+cp_ToStr(trim(this.w_DACESPIT)+"%");

            select CECODICE,CEDESCRI,CEDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_DACESPIT) and !this.bDontReportError
            deferred_cp_zoom('CES_PITI','*','CECODICE',cp_AbsName(oSource.parent,'oDACESPIT_1_9'),i_cWhere,'GSCE_ACE',"Cespiti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CECODICE,CEDESCRI,CEDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CECODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CECODICE',oSource.xKey(1))
            select CECODICE,CEDESCRI,CEDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DACESPIT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CECODICE,CEDESCRI,CEDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CECODICE="+cp_ToStrODBC(this.w_DACESPIT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CECODICE',this.w_DACESPIT)
            select CECODICE,CEDESCRI,CEDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DACESPIT = NVL(_Link_.CECODICE,space(20))
      this.w_DESCES = NVL(_Link_.CEDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CEDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_DACESPIT = space(20)
      endif
      this.w_DESCES = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND (.w_DACESPIT <= .w_ACESPIT or EMPTY(.w_ACESPIT))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Cespite inesistente, non corretto, oppure obsoleto")
        endif
        this.w_DACESPIT = space(20)
        this.w_DESCES = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CES_PITI_IDX,2])+'\'+cp_ToStr(_Link_.CECODICE,1)
      cp_ShowWarn(i_cKey,this.CES_PITI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DACESPIT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ACESPIT
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CES_PITI_IDX,3]
    i_lTable = "CES_PITI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CES_PITI_IDX,2], .t., this.CES_PITI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CES_PITI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ACESPIT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCE_ACE',True,'CES_PITI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CECODICE like "+cp_ToStrODBC(trim(this.w_ACESPIT)+"%");

          i_ret=cp_SQL(i_nConn,"select CECODICE,CEDESCRI,CEDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CECODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CECODICE',trim(this.w_ACESPIT))
          select CECODICE,CEDESCRI,CEDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CECODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ACESPIT)==trim(_Link_.CECODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CEDESCRI like "+cp_ToStrODBC(trim(this.w_ACESPIT)+"%");

            i_ret=cp_SQL(i_nConn,"select CECODICE,CEDESCRI,CEDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CEDESCRI like "+cp_ToStr(trim(this.w_ACESPIT)+"%");

            select CECODICE,CEDESCRI,CEDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ACESPIT) and !this.bDontReportError
            deferred_cp_zoom('CES_PITI','*','CECODICE',cp_AbsName(oSource.parent,'oACESPIT_1_10'),i_cWhere,'GSCE_ACE',"Cespiti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CECODICE,CEDESCRI,CEDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CECODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CECODICE',oSource.xKey(1))
            select CECODICE,CEDESCRI,CEDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ACESPIT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CECODICE,CEDESCRI,CEDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CECODICE="+cp_ToStrODBC(this.w_ACESPIT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CECODICE',this.w_ACESPIT)
            select CECODICE,CEDESCRI,CEDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ACESPIT = NVL(_Link_.CECODICE,space(20))
      this.w_DESCES1 = NVL(_Link_.CEDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CEDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ACESPIT = space(20)
      endif
      this.w_DESCES1 = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND (.w_DACESPIT <= .w_ACESPIT)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Cespite inesistente, non corretto, oppure obsoleto")
        endif
        this.w_ACESPIT = space(20)
        this.w_DESCES1 = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CES_PITI_IDX,2])+'\'+cp_ToStr(_Link_.CECODICE,1)
      cp_ShowWarn(i_cKey,this.CES_PITI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ACESPIT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DACENTRO
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CENCOST_IDX,3]
    i_lTable = "CENCOST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2], .t., this.CENCOST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DACENTRO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_ACC',True,'CENCOST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CC_CONTO like "+cp_ToStrODBC(trim(this.w_DACENTRO)+"%");

          i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CC_CONTO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CC_CONTO',trim(this.w_DACENTRO))
          select CC_CONTO,CCDESPIA,CCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CC_CONTO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DACENTRO)==trim(_Link_.CC_CONTO) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESPIA like "+cp_ToStrODBC(trim(this.w_DACENTRO)+"%");

            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESPIA like "+cp_ToStr(trim(this.w_DACENTRO)+"%");

            select CC_CONTO,CCDESPIA,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_DACENTRO) and !this.bDontReportError
            deferred_cp_zoom('CENCOST','*','CC_CONTO',cp_AbsName(oSource.parent,'oDACENTRO_1_11'),i_cWhere,'GSCA_ACC',"Centri di costo/ricavo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',oSource.xKey(1))
            select CC_CONTO,CCDESPIA,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DACENTRO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(this.w_DACENTRO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',this.w_DACENTRO)
            select CC_CONTO,CCDESPIA,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DACENTRO = NVL(_Link_.CC_CONTO,space(20))
      this.w_DESPIA = NVL(_Link_.CCDESPIA,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_DACENTRO = space(20)
      endif
      this.w_DESPIA = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND (.w_DACENTRO <= .w_ACENTRO or EMPTY(.w_ACENTRO))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Centro di costo inesistente, obsoleto, oppure codice iniziale maggiore del codice finale")
        endif
        this.w_DACENTRO = space(20)
        this.w_DESPIA = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])+'\'+cp_ToStr(_Link_.CC_CONTO,1)
      cp_ShowWarn(i_cKey,this.CENCOST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DACENTRO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ACENTRO
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CENCOST_IDX,3]
    i_lTable = "CENCOST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2], .t., this.CENCOST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ACENTRO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_ACC',True,'CENCOST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CC_CONTO like "+cp_ToStrODBC(trim(this.w_ACENTRO)+"%");

          i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CC_CONTO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CC_CONTO',trim(this.w_ACENTRO))
          select CC_CONTO,CCDESPIA,CCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CC_CONTO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ACENTRO)==trim(_Link_.CC_CONTO) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESPIA like "+cp_ToStrODBC(trim(this.w_ACENTRO)+"%");

            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESPIA like "+cp_ToStr(trim(this.w_ACENTRO)+"%");

            select CC_CONTO,CCDESPIA,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ACENTRO) and !this.bDontReportError
            deferred_cp_zoom('CENCOST','*','CC_CONTO',cp_AbsName(oSource.parent,'oACENTRO_1_12'),i_cWhere,'GSCA_ACC',"Centri di costo/ricavo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',oSource.xKey(1))
            select CC_CONTO,CCDESPIA,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ACENTRO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(this.w_ACENTRO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',this.w_ACENTRO)
            select CC_CONTO,CCDESPIA,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ACENTRO = NVL(_Link_.CC_CONTO,space(20))
      this.w_DESPIA1 = NVL(_Link_.CCDESPIA,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ACENTRO = space(20)
      endif
      this.w_DESPIA1 = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND (.w_DACENTRO <= .w_ACENTRO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Centro di costo inesistente, obsoleto, oppure codice iniziale maggiore del codice finale")
        endif
        this.w_ACENTRO = space(20)
        this.w_DESPIA1 = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])+'\'+cp_ToStr(_Link_.CC_CONTO,1)
      cp_ShowWarn(i_cKey,this.CENCOST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ACENTRO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VALUTA
  func Link_1_34(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALUTA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALUTA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALUTA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALUTA)
            select VACODVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALUTA = NVL(_Link_.VACODVAL,space(3))
      this.w_DECIMALI = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_VALUTA = space(3)
      endif
      this.w_DECIMALI = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALUTA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=BUN
  func Link_1_44(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.BUSIUNIT_IDX,3]
    i_lTable = "BUSIUNIT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2], .t., this.BUSIUNIT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_BUN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'BUSIUNIT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BUCODICE like "+cp_ToStrODBC(trim(this.w_BUN)+"%");
                   +" and BUCODAZI="+cp_ToStrODBC(this.w_AZIENDA);

          i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BUCODAZI,BUCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BUCODAZI',this.w_AZIENDA;
                     ,'BUCODICE',trim(this.w_BUN))
          select BUCODAZI,BUCODICE,BUDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BUCODAZI,BUCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_BUN)==trim(_Link_.BUCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_BUN) and !this.bDontReportError
            deferred_cp_zoom('BUSIUNIT','*','BUCODAZI,BUCODICE',cp_AbsName(oSource.parent,'oBUN_1_44'),i_cWhere,'',"Business Unit",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_AZIENDA<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select BUCODAZI,BUCODICE,BUDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where BUCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and BUCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BUCODAZI',oSource.xKey(1);
                       ,'BUCODICE',oSource.xKey(2))
            select BUCODAZI,BUCODICE,BUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_BUN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where BUCODICE="+cp_ToStrODBC(this.w_BUN);
                   +" and BUCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BUCODAZI',this.w_AZIENDA;
                       ,'BUCODICE',this.w_BUN)
            select BUCODAZI,BUCODICE,BUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_BUN = NVL(_Link_.BUCODICE,space(3))
      this.w_BDSI = NVL(_Link_.BUDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_BUN = space(3)
      endif
      this.w_BDSI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2])+'\'+cp_ToStr(_Link_.BUCODAZI,1)+'\'+cp_ToStr(_Link_.BUCODICE,1)
      cp_ShowWarn(i_cKey,this.BUSIUNIT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_BUN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCOMPET_1_3.value==this.w_COMPET)
      this.oPgFrm.Page1.oPag.oCOMPET_1_3.value=this.w_COMPET
    endif
    if not(this.oPgFrm.Page1.oPag.oTipoBeni_1_4.RadioValue()==this.w_TipoBeni)
      this.oPgFrm.Page1.oPag.oTipoBeni_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDAGRUPPO_1_5.value==this.w_DAGRUPPO)
      this.oPgFrm.Page1.oPag.oDAGRUPPO_1_5.value=this.w_DAGRUPPO
    endif
    if not(this.oPgFrm.Page1.oPag.oAGRUPPO_1_6.value==this.w_AGRUPPO)
      this.oPgFrm.Page1.oPag.oAGRUPPO_1_6.value=this.w_AGRUPPO
    endif
    if not(this.oPgFrm.Page1.oPag.oDACATEGO_1_7.value==this.w_DACATEGO)
      this.oPgFrm.Page1.oPag.oDACATEGO_1_7.value=this.w_DACATEGO
    endif
    if not(this.oPgFrm.Page1.oPag.oACATEGO_1_8.value==this.w_ACATEGO)
      this.oPgFrm.Page1.oPag.oACATEGO_1_8.value=this.w_ACATEGO
    endif
    if not(this.oPgFrm.Page1.oPag.oDACESPIT_1_9.value==this.w_DACESPIT)
      this.oPgFrm.Page1.oPag.oDACESPIT_1_9.value=this.w_DACESPIT
    endif
    if not(this.oPgFrm.Page1.oPag.oACESPIT_1_10.value==this.w_ACESPIT)
      this.oPgFrm.Page1.oPag.oACESPIT_1_10.value=this.w_ACESPIT
    endif
    if not(this.oPgFrm.Page1.oPag.oDACENTRO_1_11.value==this.w_DACENTRO)
      this.oPgFrm.Page1.oPag.oDACENTRO_1_11.value=this.w_DACENTRO
    endif
    if not(this.oPgFrm.Page1.oPag.oACENTRO_1_12.value==this.w_ACENTRO)
      this.oPgFrm.Page1.oPag.oACENTRO_1_12.value=this.w_ACENTRO
    endif
    if not(this.oPgFrm.Page1.oPag.oDATAPRN_1_13.value==this.w_DATAPRN)
      this.oPgFrm.Page1.oPag.oDATAPRN_1_13.value=this.w_DATAPRN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAT_1_14.value==this.w_DESCAT)
      this.oPgFrm.Page1.oPag.oDESCAT_1_14.value=this.w_DESCAT
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCES_1_15.value==this.w_DESCES)
      this.oPgFrm.Page1.oPag.oDESCES_1_15.value=this.w_DESCES
    endif
    if not(this.oPgFrm.Page1.oPag.oDESGRU1_1_27.value==this.w_DESGRU1)
      this.oPgFrm.Page1.oPag.oDESGRU1_1_27.value=this.w_DESGRU1
    endif
    if not(this.oPgFrm.Page1.oPag.oDESGRU2_1_28.value==this.w_DESGRU2)
      this.oPgFrm.Page1.oPag.oDESGRU2_1_28.value=this.w_DESGRU2
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAT1_1_29.value==this.w_DESCAT1)
      this.oPgFrm.Page1.oPag.oDESCAT1_1_29.value=this.w_DESCAT1
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCES1_1_31.value==this.w_DESCES1)
      this.oPgFrm.Page1.oPag.oDESCES1_1_31.value=this.w_DESCES1
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPIA_1_37.value==this.w_DESPIA)
      this.oPgFrm.Page1.oPag.oDESPIA_1_37.value=this.w_DESPIA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPIA1_1_39.value==this.w_DESPIA1)
      this.oPgFrm.Page1.oPag.oDESPIA1_1_39.value=this.w_DESPIA1
    endif
    if not(this.oPgFrm.Page1.oPag.oBUN_1_44.value==this.w_BUN)
      this.oPgFrm.Page1.oPag.oBUN_1_44.value=this.w_BUN
    endif
    if not(this.oPgFrm.Page1.oPag.oBDSI_1_45.value==this.w_BDSI)
      this.oPgFrm.Page1.oPag.oBDSI_1_45.value=this.w_BDSI
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_COMPET))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOMPET_1_3.SetFocus()
            i_bnoObbl = !empty(.w_COMPET)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) and (.w_DAGRUPPO <= .w_AGRUPPO or EMPTY(.w_AGRUPPO)))  and not(empty(.w_DAGRUPPO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDAGRUPPO_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Gruppo contabile inesistente, non corretto, oppure obsoleto")
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) and .w_DAGRUPPO <= .w_AGRUPPO)  and not(empty(.w_AGRUPPO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAGRUPPO_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Gruppo contabile inesistente, non corretto, oppure obsoleto")
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND (.w_DACATEGO <= .w_ACATEGO or EMPTY(.w_ACATEGO)))  and not(empty(.w_DACATEGO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDACATEGO_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Categoria inesistente, non corretta, oppure obsoleta")
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND (.w_DACATEGO <= .w_ACATEGO))  and not(empty(.w_ACATEGO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oACATEGO_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Categoria inesistente, non corretta, oppure obsoleta")
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND (.w_DACESPIT <= .w_ACESPIT or EMPTY(.w_ACESPIT)))  and not(empty(.w_DACESPIT))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDACESPIT_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Cespite inesistente, non corretto, oppure obsoleto")
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND (.w_DACESPIT <= .w_ACESPIT))  and not(empty(.w_ACESPIT))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oACESPIT_1_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Cespite inesistente, non corretto, oppure obsoleto")
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND (.w_DACENTRO <= .w_ACENTRO or EMPTY(.w_ACENTRO)))  and not(empty(.w_DACENTRO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDACENTRO_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Centro di costo inesistente, obsoleto, oppure codice iniziale maggiore del codice finale")
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND (.w_DACENTRO <= .w_ACENTRO))  and not(empty(.w_ACENTRO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oACENTRO_1_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Centro di costo inesistente, obsoleto, oppure codice iniziale maggiore del codice finale")
          case   (empty(.w_DATAPRN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATAPRN_1_13.SetFocus()
            i_bnoObbl = !empty(.w_DATAPRN)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DAGRUPPO = this.w_DAGRUPPO
    this.o_DACATEGO = this.w_DACATEGO
    this.o_DACESPIT = this.w_DACESPIT
    this.o_DACENTRO = this.w_DACENTRO
    this.o_DATAPRN = this.w_DATAPRN
    return

enddefine

* --- Define pages as container
define class tgsce_scoPag1 as StdContainer
  Width  = 565
  height = 384
  stdWidth  = 565
  stdheight = 384
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCOMPET_1_3 as StdField with uid="CBDAZNBWIC",rtseq=3,rtrep=.f.,;
    cFormVar = "w_COMPET", cQueryName = "COMPET",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    HelpContextID = 214245850,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=115, Top=9, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_AZIENDA", oKey_2_1="ESCODESE", oKey_2_2="this.w_COMPET"

  func oCOMPET_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOMPET_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOMPET_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_AZIENDA)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_AZIENDA)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oCOMPET_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc


  add object oTipoBeni_1_4 as StdCombo with uid="NKHPDAPENI",rtseq=4,rtrep=.f.,left=407,top=9,width=132,height=21;
    , HelpContextID = 198432353;
    , cFormVar="w_TipoBeni",RowSource=""+"Tutti,"+"Materiali,"+"Immateriali", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTipoBeni_1_4.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'M',;
    iif(this.value =3,'I',;
    space(1)))))
  endfunc
  func oTipoBeni_1_4.GetRadio()
    this.Parent.oContained.w_TipoBeni = this.RadioValue()
    return .t.
  endfunc

  func oTipoBeni_1_4.SetRadio()
    this.Parent.oContained.w_TipoBeni=trim(this.Parent.oContained.w_TipoBeni)
    this.value = ;
      iif(this.Parent.oContained.w_TipoBeni=='T',1,;
      iif(this.Parent.oContained.w_TipoBeni=='M',2,;
      iif(this.Parent.oContained.w_TipoBeni=='I',3,;
      0)))
  endfunc

  add object oDAGRUPPO_1_5 as StdField with uid="KCCOIFCMRV",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DAGRUPPO", cQueryName = "DAGRUPPO",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Gruppo contabile inesistente, non corretto, oppure obsoleto",;
    ToolTipText = "Gruppo contabile selezionato",;
    HelpContextID = 264474491,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=115, Top=38, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="GRU_COCE", cZoomOnZoom="GSCE_AGP", oKey_1_1="GPCODICE", oKey_1_2="this.w_DAGRUPPO"

  func oDAGRUPPO_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oDAGRUPPO_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDAGRUPPO_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRU_COCE','*','GPCODICE',cp_AbsName(this.parent,'oDAGRUPPO_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCE_AGP',"Gruppi contabili",'',this.parent.oContained
  endproc
  proc oDAGRUPPO_1_5.mZoomOnZoom
    local i_obj
    i_obj=GSCE_AGP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GPCODICE=this.parent.oContained.w_DAGRUPPO
     i_obj.ecpSave()
  endproc

  add object oAGRUPPO_1_6 as StdField with uid="UKZPMPQPEB",rtseq=6,rtrep=.f.,;
    cFormVar = "w_AGRUPPO", cQueryName = "AGRUPPO",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Gruppo contabile inesistente, non corretto, oppure obsoleto",;
    ToolTipText = "Gruppo contabile selezionato",;
    HelpContextID = 1038842,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=115, Top=61, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="GRU_COCE", cZoomOnZoom="GSCE_AGP", oKey_1_1="GPCODICE", oKey_1_2="this.w_AGRUPPO"

  func oAGRUPPO_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oAGRUPPO_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oAGRUPPO_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRU_COCE','*','GPCODICE',cp_AbsName(this.parent,'oAGRUPPO_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCE_AGP',"Gruppi contabili",'',this.parent.oContained
  endproc
  proc oAGRUPPO_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSCE_AGP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GPCODICE=this.parent.oContained.w_AGRUPPO
     i_obj.ecpSave()
  endproc

  add object oDACATEGO_1_7 as StdField with uid="ZNGTPYUNPH",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DACATEGO", cQueryName = "DACATEGO",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Categoria inesistente, non corretta, oppure obsoleta",;
    ToolTipText = "Categoria selezionata",;
    HelpContextID = 85667973,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=115, Top=93, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAT_CESP", cZoomOnZoom="GSCE_ACT", oKey_1_1="CCCODICE", oKey_1_2="this.w_DACATEGO"

  func oDACATEGO_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oDACATEGO_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDACATEGO_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_CESP','*','CCCODICE',cp_AbsName(this.parent,'oDACATEGO_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCE_ACT',"Categorie cespiti",'',this.parent.oContained
  endproc
  proc oDACATEGO_1_7.mZoomOnZoom
    local i_obj
    i_obj=GSCE_ACT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_DACATEGO
     i_obj.ecpSave()
  endproc

  add object oACATEGO_1_8 as StdField with uid="ZLJZSPGWVS",rtseq=8,rtrep=.f.,;
    cFormVar = "w_ACATEGO", cQueryName = "ACATEGO",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Categoria inesistente, non corretta, oppure obsoleta",;
    ToolTipText = "Categoria selezionata",;
    HelpContextID = 163704314,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=115, Top=115, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAT_CESP", cZoomOnZoom="GSCE_ACT", oKey_1_1="CCCODICE", oKey_1_2="this.w_ACATEGO"

  func oACATEGO_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oACATEGO_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oACATEGO_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_CESP','*','CCCODICE',cp_AbsName(this.parent,'oACATEGO_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCE_ACT',"Categorie cespiti",'',this.parent.oContained
  endproc
  proc oACATEGO_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSCE_ACT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_ACATEGO
     i_obj.ecpSave()
  endproc

  add object oDACESPIT_1_9 as StdField with uid="NUYJRLKQZS",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DACESPIT", cQueryName = "DACESPIT",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Cespite inesistente, non corretto, oppure obsoleto",;
    ToolTipText = "Cespite selezionato",;
    HelpContextID = 995466,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=115, Top=146, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="CES_PITI", cZoomOnZoom="GSCE_ACE", oKey_1_1="CECODICE", oKey_1_2="this.w_DACESPIT"

  func oDACESPIT_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oDACESPIT_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDACESPIT_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CES_PITI','*','CECODICE',cp_AbsName(this.parent,'oDACESPIT_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCE_ACE',"Cespiti",'',this.parent.oContained
  endproc
  proc oDACESPIT_1_9.mZoomOnZoom
    local i_obj
    i_obj=GSCE_ACE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CECODICE=this.parent.oContained.w_DACESPIT
     i_obj.ecpSave()
  endproc

  add object oACESPIT_1_10 as StdField with uid="FUVWBKVUNC",rtseq=10,rtrep=.f.,;
    cFormVar = "w_ACESPIT", cQueryName = "ACESPIT",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Cespite inesistente, non corretto, oppure obsoleto",;
    ToolTipText = "Cespite selezionato",;
    HelpContextID = 118664698,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=115, Top=168, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="CES_PITI", cZoomOnZoom="GSCE_ACE", oKey_1_1="CECODICE", oKey_1_2="this.w_ACESPIT"

  func oACESPIT_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oACESPIT_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oACESPIT_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CES_PITI','*','CECODICE',cp_AbsName(this.parent,'oACESPIT_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCE_ACE',"Cespiti",'',this.parent.oContained
  endproc
  proc oACESPIT_1_10.mZoomOnZoom
    local i_obj
    i_obj=GSCE_ACE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CECODICE=this.parent.oContained.w_ACESPIT
     i_obj.ecpSave()
  endproc

  add object oDACENTRO_1_11 as StdField with uid="CMAVTBBMMD",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DACENTRO", cQueryName = "DACENTRO",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Centro di costo inesistente, obsoleto, oppure codice iniziale maggiore del codice finale",;
    ToolTipText = "Centro di costo selezionato",;
    HelpContextID = 205574011,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=115, Top=197, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="CENCOST", cZoomOnZoom="GSCA_ACC", oKey_1_1="CC_CONTO", oKey_1_2="this.w_DACENTRO"

  func oDACENTRO_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oDACENTRO_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDACENTRO_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CENCOST','*','CC_CONTO',cp_AbsName(this.parent,'oDACENTRO_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_ACC',"Centri di costo/ricavo",'',this.parent.oContained
  endproc
  proc oDACENTRO_1_11.mZoomOnZoom
    local i_obj
    i_obj=GSCA_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CC_CONTO=this.parent.oContained.w_DACENTRO
     i_obj.ecpSave()
  endproc

  add object oACENTRO_1_12 as StdField with uid="ISVLMGSLOQ",rtseq=12,rtrep=.f.,;
    cFormVar = "w_ACENTRO", cQueryName = "ACENTRO",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Centro di costo inesistente, obsoleto, oppure codice iniziale maggiore del codice finale",;
    ToolTipText = "Centro di costo selezionato",;
    HelpContextID = 232238586,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=115, Top=219, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="CENCOST", cZoomOnZoom="GSCA_ACC", oKey_1_1="CC_CONTO", oKey_1_2="this.w_ACENTRO"

  func oACENTRO_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oACENTRO_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oACENTRO_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CENCOST','*','CC_CONTO',cp_AbsName(this.parent,'oACENTRO_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_ACC',"Centri di costo/ricavo",'',this.parent.oContained
  endproc
  proc oACENTRO_1_12.mZoomOnZoom
    local i_obj
    i_obj=GSCA_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CC_CONTO=this.parent.oContained.w_ACENTRO
     i_obj.ecpSave()
  endproc

  add object oDATAPRN_1_13 as StdField with uid="RQROOTXNYF",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DATAPRN", cQueryName = "DATAPRN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di stampa (condiziona le date di obsolescenza e dismissione)",;
    HelpContextID = 237223882,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=115, Top=276

  add object oDESCAT_1_14 as StdField with uid="GRCJBLFKFZ",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DESCAT", cQueryName = "DESCAT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 219270090,;
   bGlobalFont=.t.,;
    Height=21, Width=247, Left=268, Top=93, InputMask=replicate('X',40)

  add object oDESCES_1_15 as StdField with uid="YEASMULTKV",rtseq=15,rtrep=.f.,;
    cFormVar = "w_DESCES", cQueryName = "DESCES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 231853002,;
   bGlobalFont=.t.,;
    Height=21, Width=247, Left=268, Top=146, InputMask=replicate('X',40)

  add object oDESGRU1_1_27 as StdField with uid="QHPPUCORJJ",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DESGRU1", cQueryName = "DESGRU1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 184404938,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=268, Top=38, InputMask=replicate('X',40)

  add object oDESGRU2_1_28 as StdField with uid="AMZMGZTMKN",rtseq=20,rtrep=.f.,;
    cFormVar = "w_DESGRU2", cQueryName = "DESGRU2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 184404938,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=268, Top=61, InputMask=replicate('X',40)

  add object oDESCAT1_1_29 as StdField with uid="SKLKEDMRIW",rtseq=21,rtrep=.f.,;
    cFormVar = "w_DESCAT1", cQueryName = "DESCAT1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 219270090,;
   bGlobalFont=.t.,;
    Height=21, Width=247, Left=268, Top=115, InputMask=replicate('X',40)

  add object oDESCES1_1_31 as StdField with uid="NTLRYSKWTL",rtseq=22,rtrep=.f.,;
    cFormVar = "w_DESCES1", cQueryName = "DESCES1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 231853002,;
   bGlobalFont=.t.,;
    Height=21, Width=247, Left=268, Top=168, InputMask=replicate('X',40)

  add object oDESPIA_1_37 as StdField with uid="XDVPRPTVGB",rtseq=26,rtrep=.f.,;
    cFormVar = "w_DESPIA", cQueryName = "DESPIA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 260361162,;
   bGlobalFont=.t.,;
    Height=21, Width=247, Left=268, Top=197, InputMask=replicate('X',40)

  add object oDESPIA1_1_39 as StdField with uid="MQEPARQZEF",rtseq=27,rtrep=.f.,;
    cFormVar = "w_DESPIA1", cQueryName = "DESPIA1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 260361162,;
   bGlobalFont=.t.,;
    Height=21, Width=247, Left=268, Top=219, InputMask=replicate('X',40)


  add object oBtn_1_41 as StdButton with uid="QQNTRPGEJB",left=491, top=335, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 184902214;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_41.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_42 as cp_outputCombo with uid="ZLERXIVPWT",left=115, top=305, width=439,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 181288474


  add object oBtn_1_43 as StdButton with uid="AIFCCVYXPF",left=439, top=335, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , HelpContextID = 217496538;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_43.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_43.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc

  add object oBUN_1_44 as StdField with uid="NRYYXKTJDV",rtseq=28,rtrep=.f.,;
    cFormVar = "w_BUN", cQueryName = "BUN",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Business Unit selezionata",;
    HelpContextID = 177927190,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=115, Top=246, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="BUSIUNIT", oKey_1_1="BUCODAZI", oKey_1_2="this.w_AZIENDA", oKey_2_1="BUCODICE", oKey_2_2="this.w_BUN"

  func oBUN_1_44.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERBUN<>'N')
    endwith
   endif
  endfunc

  func oBUN_1_44.mHide()
    with this.Parent.oContained
      return (g_APPLICATION<>'ADHOC Enterprise')
    endwith
  endfunc

  func oBUN_1_44.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_44('Part',this)
    endwith
    return bRes
  endfunc

  proc oBUN_1_44.ecpDrop(oSource)
    this.Parent.oContained.link_1_44('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oBUN_1_44.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.BUSIUNIT_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"BUCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_AZIENDA)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"BUCODAZI="+cp_ToStr(this.Parent.oContained.w_AZIENDA)
    endif
    do cp_zoom with 'BUSIUNIT','*','BUCODAZI,BUCODICE',cp_AbsName(this.parent,'oBUN_1_44'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Business Unit",'',this.parent.oContained
  endproc

  add object oBDSI_1_45 as StdField with uid="ZJVMBPGJWC",rtseq=29,rtrep=.f.,;
    cFormVar = "w_BDSI", cQueryName = "BDSI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 182727446,;
   bGlobalFont=.t.,;
    Height=21, Width=350, Left=165, Top=246, InputMask=replicate('X',40)

  func oBDSI_1_45.mHide()
    with this.Parent.oContained
      return (g_APPLICATION<>'ADHOC Enterprise')
    endwith
  endfunc

  add object oStr_1_16 as StdString with uid="XRORKLZMEB",Visible=.t., Left=5, Top=306,;
    Alignment=1, Width=108, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="TODSHELWPM",Visible=.t., Left=5, Top=93,;
    Alignment=1, Width=108, Height=18,;
    Caption="Da categoria:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="MAODZIERMV",Visible=.t., Left=5, Top=146,;
    Alignment=1, Width=108, Height=18,;
    Caption="Da cespite:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="KQUOUKERDD",Visible=.t., Left=5, Top=9,;
    Alignment=1, Width=108, Height=15,;
    Caption="Competenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="ILNQLFUFBH",Visible=.t., Left=327, Top=9,;
    Alignment=1, Width=73, Height=18,;
    Caption="Tipo beni:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="DZAHGGLLJG",Visible=.t., Left=5, Top=38,;
    Alignment=1, Width=108, Height=18,;
    Caption="Da gruppo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="VLOXAPITKY",Visible=.t., Left=5, Top=61,;
    Alignment=1, Width=108, Height=18,;
    Caption="A gruppo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="PIHBKRKBEF",Visible=.t., Left=5, Top=276,;
    Alignment=1, Width=108, Height=18,;
    Caption="Data di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="FGEXNTLIAD",Visible=.t., Left=5, Top=115,;
    Alignment=1, Width=108, Height=18,;
    Caption="A categoria:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="JINDHGGTSD",Visible=.t., Left=5, Top=168,;
    Alignment=1, Width=108, Height=18,;
    Caption="Da cespite:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="QHJLMLABTS",Visible=.t., Left=7, Top=343,;
    Alignment=0, Width=202, Height=17,;
    Caption="Importi espressi in valuta di conto"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_38 as StdString with uid="RFRGCZJJFD",Visible=.t., Left=5, Top=197,;
    Alignment=1, Width=108, Height=18,;
    Caption="Da centro di costo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="MTQTLIWUYN",Visible=.t., Left=5, Top=219,;
    Alignment=1, Width=108, Height=18,;
    Caption="A centro di costo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="XPPJVXJYSN",Visible=.t., Left=5, Top=246,;
    Alignment=1, Width=108, Height=15,;
    Caption="Business Unit:"  ;
  , bGlobalFont=.t.

  func oStr_1_46.mHide()
    with this.Parent.oContained
      return (g_APPLICATION<>'ADHOC Enterprise')
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsce_sco','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
