* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_mmc                                                        *
*              Dettaglio componenti                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_19]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-03-17                                                      *
* Last revis.: 2011-02-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsce_mmc")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsce_mmc")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsce_mmc")
  return

* --- Class definition
define class tgsce_mmc as StdPCForm
  Width  = 577
  Height = 303
  Top    = 140
  Left   = 228
  cComment = "Dettaglio componenti"
  cPrg = "gsce_mmc"
  HelpContextID=197805417
  add object cnt as tcgsce_mmc
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsce_mmc as PCContext
  w_MCSERIAL = space(10)
  w_CODCES = space(20)
  w_MCCOMPON = space(40)
  w_VALUTA = space(3)
  w_DECTOT1 = 0
  w_VALMOV = space(3)
  w_DECTOT = 0
  w_MCIMPTOT = 0
  w_MCIMPTOC = 0
  w_DATREG = space(8)
  w_TOTIMP = 0
  w_TOTRIP = 0
  w_TOTRES = 0
  w_MCIMPVAL = 0
  w_TOTIMPC = 0
  w_TOTRIPC = 0
  w_TOTRESC = 0
  w_MCIMPVAC = 0
  proc Save(i_oFrom)
    this.w_MCSERIAL = i_oFrom.w_MCSERIAL
    this.w_CODCES = i_oFrom.w_CODCES
    this.w_MCCOMPON = i_oFrom.w_MCCOMPON
    this.w_VALUTA = i_oFrom.w_VALUTA
    this.w_DECTOT1 = i_oFrom.w_DECTOT1
    this.w_VALMOV = i_oFrom.w_VALMOV
    this.w_DECTOT = i_oFrom.w_DECTOT
    this.w_MCIMPTOT = i_oFrom.w_MCIMPTOT
    this.w_MCIMPTOC = i_oFrom.w_MCIMPTOC
    this.w_DATREG = i_oFrom.w_DATREG
    this.w_TOTIMP = i_oFrom.w_TOTIMP
    this.w_TOTRIP = i_oFrom.w_TOTRIP
    this.w_TOTRES = i_oFrom.w_TOTRES
    this.w_MCIMPVAL = i_oFrom.w_MCIMPVAL
    this.w_TOTIMPC = i_oFrom.w_TOTIMPC
    this.w_TOTRIPC = i_oFrom.w_TOTRIPC
    this.w_TOTRESC = i_oFrom.w_TOTRESC
    this.w_MCIMPVAC = i_oFrom.w_MCIMPVAC
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_MCSERIAL = this.w_MCSERIAL
    i_oTo.w_CODCES = this.w_CODCES
    i_oTo.w_MCCOMPON = this.w_MCCOMPON
    i_oTo.w_VALUTA = this.w_VALUTA
    i_oTo.w_DECTOT1 = this.w_DECTOT1
    i_oTo.w_VALMOV = this.w_VALMOV
    i_oTo.w_DECTOT = this.w_DECTOT
    i_oTo.w_MCIMPTOT = this.w_MCIMPTOT
    i_oTo.w_MCIMPTOC = this.w_MCIMPTOC
    i_oTo.w_DATREG = this.w_DATREG
    i_oTo.w_TOTIMP = this.w_TOTIMP
    i_oTo.w_TOTRIP = this.w_TOTRIP
    i_oTo.w_TOTRES = this.w_TOTRES
    i_oTo.w_MCIMPVAL = this.w_MCIMPVAL
    i_oTo.w_TOTIMPC = this.w_TOTIMPC
    i_oTo.w_TOTRIPC = this.w_TOTRIPC
    i_oTo.w_TOTRESC = this.w_TOTRESC
    i_oTo.w_MCIMPVAC = this.w_MCIMPVAC
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsce_mmc as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 577
  Height = 303
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-02-25"
  HelpContextID=197805417
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=18

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  MOV_PCES_IDX = 0
  COM_PCES_IDX = 0
  VALUTE_IDX = 0
  cFile = "MOV_PCES"
  cKeySelect = "MCSERIAL"
  cKeyWhere  = "MCSERIAL=this.w_MCSERIAL"
  cKeyDetail  = "MCSERIAL=this.w_MCSERIAL and MCCOMPON=this.w_MCCOMPON"
  cKeyWhereODBC = '"MCSERIAL="+cp_ToStrODBC(this.w_MCSERIAL)';

  cKeyDetailWhereODBC = '"MCSERIAL="+cp_ToStrODBC(this.w_MCSERIAL)';
      +'+" and MCCOMPON="+cp_ToStrODBC(this.w_MCCOMPON)';

  cKeyWhereODBCqualified = '"MOV_PCES.MCSERIAL="+cp_ToStrODBC(this.w_MCSERIAL)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cPrg = "gsce_mmc"
  cComment = "Dettaglio componenti"
  i_nRowNum = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_MCSERIAL = space(10)
  w_CODCES = space(20)
  w_MCCOMPON = space(40)
  w_VALUTA = space(3)
  w_DECTOT1 = 0
  w_VALMOV = space(3)
  w_DECTOT = 0
  w_MCIMPTOT = 0
  o_MCIMPTOT = 0
  w_MCIMPTOC = 0
  o_MCIMPTOC = 0
  w_DATREG = ctod('  /  /  ')
  w_TOTIMP = 0
  w_TOTRIP = 0
  w_TOTRES = 0
  w_MCIMPVAL = 0
  w_TOTIMPC = 0
  w_TOTRIPC = 0
  w_TOTRESC = 0
  w_MCIMPVAC = 0
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsce_mmcPag1","gsce_mmc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='COM_PCES'
    this.cWorkTables[2]='VALUTE'
    this.cWorkTables[3]='MOV_PCES'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.MOV_PCES_IDX,5],7]
    this.nPostItConn=i_TableProp[this.MOV_PCES_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsce_mmc'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from MOV_PCES where MCSERIAL=KeySet.MCSERIAL
    *                            and MCCOMPON=KeySet.MCCOMPON
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.MOV_PCES_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOV_PCES_IDX,2],this.bLoadRecFilter,this.MOV_PCES_IDX,"gsce_mmc")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('MOV_PCES')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "MOV_PCES.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' MOV_PCES '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'MCSERIAL',this.w_MCSERIAL  )
      select * from (i_cTable) MOV_PCES where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_CODCES = this.oParentObject .w_MCCODCES
        .w_VALUTA = space(3)
        .w_DECTOT = 0
        .w_TOTIMP = 0
        .w_TOTIMPC = 0
        .w_MCSERIAL = NVL(MCSERIAL,space(10))
          .link_2_2('Load')
        .w_TOTRIP = this.oParentObject .w_TCAR
        .w_TOTRES = .w_TOTRIP-.w_TOTIMP
        .w_TOTRIPC = this.oParentObject .w_TCARC
        .w_TOTRESC = .w_TOTRIPC-.w_TOTIMPC
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'MOV_PCES')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      this.w_TOTIMP = 0
      this.w_TOTIMPC = 0
      scan
        with this
          .w_DECTOT1 = 0
          .w_MCCOMPON = NVL(MCCOMPON,space(40))
          .link_2_1('Load')
        .w_VALMOV = THIS.oParentObject .w_MCCODVAL
          .w_MCIMPTOT = NVL(MCIMPTOT,0)
          .w_MCIMPTOC = NVL(MCIMPTOC,0)
        .w_DATREG = THIS.oParentObject .w_MCDATREG
          .w_MCIMPVAL = NVL(MCIMPVAL,0)
          .w_MCIMPVAC = NVL(MCIMPVAC,0)
          select (this.cTrsName)
          append blank
          replace CODCES with .w_CODCES
          replace MCCOMPON with .w_MCCOMPON
          replace MCIMPVAL with .w_MCIMPVAL
          replace MCIMPVAC with .w_MCIMPVAC
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          .w_TOTIMP = .w_TOTIMP+.w_MCIMPTOT
          .w_TOTIMPC = .w_TOTIMPC+.w_MCIMPTOC
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .w_TOTRIP = this.oParentObject .w_TCAR
        .w_TOTRES = .w_TOTRIP-.w_TOTIMP
        .w_TOTRIPC = this.oParentObject .w_TCARC
        .w_TOTRESC = .w_TOTRIPC-.w_TOTIMPC
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_MCSERIAL=space(10)
      .w_CODCES=space(20)
      .w_MCCOMPON=space(40)
      .w_VALUTA=space(3)
      .w_DECTOT1=0
      .w_VALMOV=space(3)
      .w_DECTOT=0
      .w_MCIMPTOT=0
      .w_MCIMPTOC=0
      .w_DATREG=ctod("  /  /  ")
      .w_TOTIMP=0
      .w_TOTRIP=0
      .w_TOTRES=0
      .w_MCIMPVAL=0
      .w_TOTIMPC=0
      .w_TOTRIPC=0
      .w_TOTRESC=0
      .w_MCIMPVAC=0
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
        .w_CODCES = this.oParentObject .w_MCCODCES
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_MCCOMPON))
         .link_2_1('Full')
        endif
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_VALUTA))
         .link_2_2('Full')
        endif
        .DoRTCalc(5,5,.f.)
        .w_VALMOV = THIS.oParentObject .w_MCCODVAL
        .DoRTCalc(7,9,.f.)
        .w_DATREG = THIS.oParentObject .w_MCDATREG
        .DoRTCalc(11,11,.f.)
        .w_TOTRIP = this.oParentObject .w_TCAR
        .w_TOTRES = .w_TOTRIP-.w_TOTIMP
        .w_MCIMPVAL = IIF(.w_Valuta<>.w_Valmov,cp_Round(Valcam(Nvl(.w_Mcimptot,0), .w_Valmov, .w_Valuta, .w_Datreg, 0), .w_Dectot1),.w_Mcimptot)
        .DoRTCalc(15,15,.f.)
        .w_TOTRIPC = this.oParentObject .w_TCARC
        .w_TOTRESC = .w_TOTRIPC-.w_TOTIMPC
        .w_MCIMPVAC = IIF(.w_Valuta<>.w_Valmov,cp_Round(Valcam(Nvl(.w_Mcimptoc,0), .w_Valmov, .w_Valuta, .w_Datreg, 0), .w_Dectot1),.w_Mcimptoc)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'MOV_PCES')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oBtn_1_6.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'MOV_PCES',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.MOV_PCES_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCSERIAL,"MCSERIAL",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_MCCOMPON C(40);
      ,t_MCIMPTOT N(18,4);
      ,t_MCIMPTOC N(18,4);
      ,CODCES C(20);
      ,MCCOMPON C(40);
      ,MCIMPVAL N(18,4);
      ,MCIMPVAC N(18,4);
      ,t_DECTOT1 N(1);
      ,t_VALMOV C(3);
      ,t_DATREG D(8);
      ,t_MCIMPVAL N(18,4);
      ,t_MCIMPVAC N(18,4);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsce_mmcbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMCCOMPON_2_1.controlsource=this.cTrsName+'.t_MCCOMPON'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMCIMPTOT_2_6.controlsource=this.cTrsName+'.t_MCIMPTOT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMCIMPTOC_2_7.controlsource=this.cTrsName+'.t_MCIMPTOC'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(284)
    this.AddVLine(419)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMCCOMPON_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.MOV_PCES_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOV_PCES_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.MOV_PCES_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MOV_PCES_IDX,2])
      *
      * insert into MOV_PCES
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'MOV_PCES')
        i_extval=cp_InsertValODBCExtFlds(this,'MOV_PCES')
        i_cFldBody=" "+;
                  "(MCSERIAL,MCCOMPON,MCIMPTOT,MCIMPTOC,MCIMPVAL"+;
                  ",MCIMPVAC,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_MCSERIAL)+","+cp_ToStrODBCNull(this.w_MCCOMPON)+","+cp_ToStrODBC(this.w_MCIMPTOT)+","+cp_ToStrODBC(this.w_MCIMPTOC)+","+cp_ToStrODBC(this.w_MCIMPVAL)+;
             ","+cp_ToStrODBC(this.w_MCIMPVAC)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'MOV_PCES')
        i_extval=cp_InsertValVFPExtFlds(this,'MOV_PCES')
        cp_CheckDeletedKey(i_cTable,0,'MCSERIAL',this.w_MCSERIAL,'MCCOMPON',this.w_MCCOMPON)
        INSERT INTO (i_cTable) (;
                   MCSERIAL;
                  ,MCCOMPON;
                  ,MCIMPTOT;
                  ,MCIMPTOC;
                  ,MCIMPVAL;
                  ,MCIMPVAC;
                  ,CPCCCHK &i_extfld.) VALUES (;
                  this.w_MCSERIAL;
                  ,this.w_MCCOMPON;
                  ,this.w_MCIMPTOT;
                  ,this.w_MCIMPTOC;
                  ,this.w_MCIMPVAL;
                  ,this.w_MCIMPVAC;
                  ,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.MOV_PCES_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MOV_PCES_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (t_MCCOMPON<>space(40)) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'MOV_PCES')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and MCCOMPON="+cp_ToStrODBC(&i_TN.->MCCOMPON)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'MOV_PCES')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and MCCOMPON=&i_TN.->MCCOMPON;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (t_MCCOMPON<>space(40)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and MCCOMPON="+cp_ToStrODBC(&i_TN.->MCCOMPON)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and MCCOMPON=&i_TN.->MCCOMPON;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = 0
              replace MCCOMPON with this.w_MCCOMPON
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update MOV_PCES
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'MOV_PCES')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " MCIMPTOT="+cp_ToStrODBC(this.w_MCIMPTOT)+;
                     ",MCIMPTOC="+cp_ToStrODBC(this.w_MCIMPTOC)+;
                     ",MCIMPVAL="+cp_ToStrODBC(this.w_MCIMPVAL)+;
                     ",MCIMPVAC="+cp_ToStrODBC(this.w_MCIMPVAC)+;
                     ",MCCOMPON="+cp_ToStrODBC(this.w_MCCOMPON)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+;
                             " and MCCOMPON="+cp_ToStrODBC(MCCOMPON)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'MOV_PCES')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      MCIMPTOT=this.w_MCIMPTOT;
                     ,MCIMPTOC=this.w_MCIMPTOC;
                     ,MCIMPVAL=this.w_MCIMPVAL;
                     ,MCIMPVAC=this.w_MCIMPVAC;
                     ,MCCOMPON=this.w_MCCOMPON;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                                      and MCCOMPON=&i_TN.->MCCOMPON;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Restore Detail Transaction
  proc mRestoreTrsDetail(i_bCanSkip)
    local i_cWherel,i_cF,i_nConn,i_cTable,i_oRow
    local i_cOp1,i_cOp2

    i_cF=this.cTrsName
    i_nConn = i_TableProp[this.COM_PCES_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.COM_PCES_IDX,2])
    i_oRow=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow
    if i_nConn<>0
      local i_bSkip
      i_bSkip=i_bCanSkip ;
              and NVL(&i_cF..MCIMPVAL,0)==this.w_MCIMPVAL;
              and NVL(&i_cF..MCIMPVAC,0)==this.w_MCIMPVAC;
              and NVL(&i_cF..MCCOMPON,space(40))==this.w_MCCOMPON;
              and NVL(&i_cF..CODCES,space(20))==this.w_CODCES;

      Local i_cValueForTrsact
      i_cValueForTrsact=NVL(&i_cF..MCCOMPON,space(40))
      if !i_bSkip and !Empty(i_cValueForTrsact)
        =cp_TrsSQL(i_nConn,"UPDATE "+i_cTable+" SET ";
           +" COIMPTOT=COIMPTOT - "+cp_ToStrODBC(NVL(&i_cF..MCIMPVAL,0))+","           +" COIMPTOC=COIMPTOC - "+cp_ToStrODBC(NVL(&i_cF..MCIMPVAC,0))+","  +"CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+" WHERE COCOMPON="+cp_ToStrODBC(NVL(&i_cF..MCCOMPON,space(40)));
             +" AND COCODICE="+cp_ToStrODBC(NVL(&i_cF..CODCES,space(20)));
             )
      endif
    else
      i_cWhere = cp_PKFox(i_cTable;
                 ,'COCODICE',&i_cF..CODCES;
                 ,'COCOMPON',&i_cF..MCCOMPON)
      UPDATE (i_cTable) SET ;
           COIMPTOT=COIMPTOT-&i_cF..MCIMPVAL  ,;
           COIMPTOC=COIMPTOC-&i_cF..MCIMPVAC  ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere
    endif
  endproc

  * --- Update Detail Transaction
  proc mUpdateTrsDetail(i_bCanSkip)
    local i_cWherel,i_cF,i_nModRow,i_nConn,i_cTable,i_oRow
    local i_cOp1,i_cOp2

    i_cF=this.cTrsName
    i_nConn = i_TableProp[this.COM_PCES_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.COM_PCES_IDX,2])
    i_oRow=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow
    if i_nConn<>0
      local i_bSkip
      i_bSkip=i_bCanSkip ;
              and NVL(&i_cF..MCIMPVAL,0)==this.w_MCIMPVAL;
              and NVL(&i_cF..MCIMPVAC,0)==this.w_MCIMPVAC;
              and NVL(&i_cF..MCCOMPON,space(40))==this.w_MCCOMPON;
              and NVL(&i_cF..CODCES,space(20))==this.w_CODCES;

      if !i_bSkip and !Empty(this.w_MCCOMPON)
        i_nModRow=cp_TrsSQL(i_nConn,"UPDATE "+i_cTable+" SET"+;
         +" COIMPTOT=COIMPTOT + "+cp_ToStrODBC(this.w_MCIMPVAL)  +",";
         +" COIMPTOC=COIMPTOC + "+cp_ToStrODBC(this.w_MCIMPVAC)  +",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+" WHERE COCOMPON="+cp_ToStrODBC(this.w_MCCOMPON);
           +" AND COCODICE="+cp_ToStrODBC(this.w_CODCES);
           )
      endif
    else
      i_cWhere = cp_PKFox(i_cTable;
                 ,'COCODICE',this.w_CODCES;
                 ,'COCOMPON',this.w_MCCOMPON)
      UPDATE (i_cTable) SET;
           COIMPTOT=COIMPTOT+this.w_MCIMPVAL  ,;
           COIMPTOC=COIMPTOC+this.w_MCIMPVAC  ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere
    endif
  endproc

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.MOV_PCES_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MOV_PCES_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (t_MCCOMPON<>space(40)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete MOV_PCES
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and MCCOMPON="+cp_ToStrODBC(&i_TN.->MCCOMPON)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and MCCOMPON=&i_TN.->MCCOMPON;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (t_MCCOMPON<>space(40)) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.MOV_PCES_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOV_PCES_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,3,.t.)
          .link_2_2('Full')
        .DoRTCalc(5,5,.t.)
          .w_VALMOV = THIS.oParentObject .w_MCCODVAL
        .DoRTCalc(7,9,.t.)
          .w_DATREG = THIS.oParentObject .w_MCDATREG
        .DoRTCalc(11,11,.t.)
          .w_TOTRIP = this.oParentObject .w_TCAR
          .w_TOTRES = .w_TOTRIP-.w_TOTIMP
        if .o_MCIMPTOT<>.w_MCIMPTOT
          .w_MCIMPVAL = IIF(.w_Valuta<>.w_Valmov,cp_Round(Valcam(Nvl(.w_Mcimptot,0), .w_Valmov, .w_Valuta, .w_Datreg, 0), .w_Dectot1),.w_Mcimptot)
        endif
        .DoRTCalc(15,15,.t.)
          .w_TOTRIPC = this.oParentObject .w_TCARC
          .w_TOTRESC = .w_TOTRIPC-.w_TOTIMPC
        if .o_MCIMPTOC<>.w_MCIMPTOC
          .w_MCIMPVAC = IIF(.w_Valuta<>.w_Valmov,cp_Round(Valcam(Nvl(.w_Mcimptoc,0), .w_Valmov, .w_Valuta, .w_Datreg, 0), .w_Dectot1),.w_Mcimptoc)
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_DECTOT1 with this.w_DECTOT1
      replace t_VALMOV with this.w_VALMOV
      replace t_DATREG with this.w_DATREG
      replace t_MCIMPVAL with this.w_MCIMPVAL
      replace t_MCIMPVAC with this.w_MCIMPVAC
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=MCCOMPON
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COM_PCES_IDX,3]
    i_lTable = "COM_PCES"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COM_PCES_IDX,2], .t., this.COM_PCES_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COM_PCES_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MCCOMPON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'COM_PCES')
        if i_nConn<>0
          i_cWhere = i_cFlt+" COCOMPON like "+cp_ToStrODBC(trim(this.w_MCCOMPON)+"%");
                   +" and COCODICE="+cp_ToStrODBC(this.w_CODCES);

          i_ret=cp_SQL(i_nConn,"select COCODICE,COCOMPON,COVALUTA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by COCODICE,COCOMPON","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'COCODICE',this.w_CODCES;
                     ,'COCOMPON',trim(this.w_MCCOMPON))
          select COCODICE,COCOMPON,COVALUTA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by COCODICE,COCOMPON into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MCCOMPON)==trim(_Link_.COCOMPON) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MCCOMPON) and !this.bDontReportError
            deferred_cp_zoom('COM_PCES','*','COCODICE,COCOMPON',cp_AbsName(oSource.parent,'oMCCOMPON_2_1'),i_cWhere,'',"Componenti cespite",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODCES<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COCODICE,COCOMPON,COVALUTA";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select COCODICE,COCOMPON,COVALUTA;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COCODICE,COCOMPON,COVALUTA";
                     +" from "+i_cTable+" "+i_lTable+" where COCOMPON="+cp_ToStrODBC(oSource.xKey(2));
                     +" and COCODICE="+cp_ToStrODBC(this.w_CODCES);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COCODICE',oSource.xKey(1);
                       ,'COCOMPON',oSource.xKey(2))
            select COCODICE,COCOMPON,COVALUTA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MCCOMPON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COCODICE,COCOMPON,COVALUTA";
                   +" from "+i_cTable+" "+i_lTable+" where COCOMPON="+cp_ToStrODBC(this.w_MCCOMPON);
                   +" and COCODICE="+cp_ToStrODBC(this.w_CODCES);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COCODICE',this.w_CODCES;
                       ,'COCOMPON',this.w_MCCOMPON)
            select COCODICE,COCOMPON,COVALUTA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MCCOMPON = NVL(_Link_.COCOMPON,space(40))
      this.w_VALUTA = NVL(_Link_.COVALUTA,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_MCCOMPON = space(40)
      endif
      this.w_VALUTA = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COM_PCES_IDX,2])+'\'+cp_ToStr(_Link_.COCODICE,1)+'\'+cp_ToStr(_Link_.COCOMPON,1)
      cp_ShowWarn(i_cKey,this.COM_PCES_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MCCOMPON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VALUTA
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALUTA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALUTA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALUTA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALUTA)
            select VACODVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALUTA = NVL(_Link_.VACODVAL,space(3))
      this.w_DECTOT1 = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_VALUTA = space(3)
      endif
      this.w_DECTOT1 = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALUTA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oTOTIMP_3_1.value==this.w_TOTIMP)
      this.oPgFrm.Page1.oPag.oTOTIMP_3_1.value=this.w_TOTIMP
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTRIP_3_2.value==this.w_TOTRIP)
      this.oPgFrm.Page1.oPag.oTOTRIP_3_2.value=this.w_TOTRIP
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTRES_3_3.value==this.w_TOTRES)
      this.oPgFrm.Page1.oPag.oTOTRES_3_3.value=this.w_TOTRES
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTIMPC_1_7.value==this.w_TOTIMPC)
      this.oPgFrm.Page1.oPag.oTOTIMPC_1_7.value=this.w_TOTIMPC
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTRIPC_1_8.value==this.w_TOTRIPC)
      this.oPgFrm.Page1.oPag.oTOTRIPC_1_8.value=this.w_TOTRIPC
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTRESC_1_9.value==this.w_TOTRESC)
      this.oPgFrm.Page1.oPag.oTOTRESC_1_9.value=this.w_TOTRESC
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMCCOMPON_2_1.value==this.w_MCCOMPON)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMCCOMPON_2_1.value=this.w_MCCOMPON
      replace t_MCCOMPON with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMCCOMPON_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMCIMPTOT_2_6.value==this.w_MCIMPTOT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMCIMPTOT_2_6.value=this.w_MCIMPTOT
      replace t_MCIMPTOT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMCIMPTOT_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMCIMPTOC_2_7.value==this.w_MCIMPTOC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMCIMPTOC_2_7.value=this.w_MCIMPTOC
      replace t_MCIMPTOC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMCIMPTOC_2_7.value
    endif
    cp_SetControlsValueExtFlds(this,'MOV_PCES')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .w_MCCOMPON<>space(40)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_MCIMPTOT = this.w_MCIMPTOT
    this.o_MCIMPTOC = this.w_MCIMPTOC
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(t_MCCOMPON<>space(40))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_MCCOMPON=space(40)
      .w_DECTOT1=0
      .w_VALMOV=space(3)
      .w_MCIMPTOT=0
      .w_MCIMPTOC=0
      .w_DATREG=ctod("  /  /  ")
      .w_MCIMPVAL=0
      .w_MCIMPVAC=0
      .DoRTCalc(1,3,.f.)
      if not(empty(.w_MCCOMPON))
        .link_2_1('Full')
      endif
      .DoRTCalc(4,5,.f.)
        .w_VALMOV = THIS.oParentObject .w_MCCODVAL
      .DoRTCalc(7,9,.f.)
        .w_DATREG = THIS.oParentObject .w_MCDATREG
      .DoRTCalc(11,13,.f.)
        .w_MCIMPVAL = IIF(.w_Valuta<>.w_Valmov,cp_Round(Valcam(Nvl(.w_Mcimptot,0), .w_Valmov, .w_Valuta, .w_Datreg, 0), .w_Dectot1),.w_Mcimptot)
      .DoRTCalc(15,17,.f.)
        .w_MCIMPVAC = IIF(.w_Valuta<>.w_Valmov,cp_Round(Valcam(Nvl(.w_Mcimptoc,0), .w_Valmov, .w_Valuta, .w_Datreg, 0), .w_Dectot1),.w_Mcimptoc)
    endwith
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_MCCOMPON = t_MCCOMPON
    this.w_DECTOT1 = t_DECTOT1
    this.w_VALMOV = t_VALMOV
    this.w_MCIMPTOT = t_MCIMPTOT
    this.w_MCIMPTOC = t_MCIMPTOC
    this.w_DATREG = t_DATREG
    this.w_MCIMPVAL = t_MCIMPVAL
    this.w_MCIMPVAC = t_MCIMPVAC
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_MCCOMPON with this.w_MCCOMPON
    replace t_DECTOT1 with this.w_DECTOT1
    replace t_VALMOV with this.w_VALMOV
    replace t_MCIMPTOT with this.w_MCIMPTOT
    replace t_MCIMPTOC with this.w_MCIMPTOC
    replace t_DATREG with this.w_DATREG
    replace t_MCIMPVAL with this.w_MCIMPVAL
    replace t_MCIMPVAC with this.w_MCIMPVAC
    if i_srv='A'
      replace MCCOMPON with this.w_MCCOMPON
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
     with this
        .w_TOTIMP = .w_TOTIMP-.w_mcimptot
        .w_TOTIMPC = .w_TOTIMPC-.w_mcimptoc
        .SetControlsValue()
      endwith
  EndProc
enddefine

* --- Define pages as container
define class tgsce_mmcPag1 as StdContainer
  Width  = 573
  height = 303
  stdWidth  = 573
  stdheight = 303
  resizeXpos=152
  resizeYpos=129
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oBtn_1_6 as StdButton with uid="YTIBCUKOCB",left=8, top=253, width=48,height=45,;
    CpPicture="BMP\CARICA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per caricare il dettaglio dei componenti";
    , HelpContextID = 198590502;
    , caption='\<Carica';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_6.Click()
      with this.Parent.oContained
        do GSCE_BD1 with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oTOTIMPC_1_7 as StdField with uid="DLEBUHVQZY",rtseq=15,rtrep=.f.,;
    cFormVar = "w_TOTIMPC", cQueryName = "TOTIMPC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 156520246,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=433, Top=225, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  add object oTOTRIPC_1_8 as StdField with uid="UUPWKISMJS",rtseq=16,rtrep=.f.,;
    cFormVar = "w_TOTRIPC", cQueryName = "TOTRIPC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 152915766,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=433, Top=251, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  add object oTOTRESC_1_9 as StdField with uid="WDLKVWSBVJ",rtseq=17,rtrep=.f.,;
    cFormVar = "w_TOTRESC", cQueryName = "TOTRESC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 199053110,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=433, Top=277, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=2, top=4, width=564,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=3,Field1="MCCOMPON",Label1="Componente",Field2="MCIMPTOT",Label2="Valore movim. fiscale",Field3="MCIMPTOC",Label3="Valore movim. civile",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 118285434

  add object oStr_1_2 as StdString with uid="PREDOJRWIX",Visible=.t., Left=130, Top=226,;
    Alignment=1, Width=159, Height=15,;
    Caption="Importo totale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_3 as StdString with uid="ARGNCMUKJA",Visible=.t., Left=130, Top=252,;
    Alignment=1, Width=159, Height=15,;
    Caption="Importo da ripartire:"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="DUYXDNSTIP",Visible=.t., Left=130, Top=278,;
    Alignment=1, Width=159, Height=15,;
    Caption="Residuo:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-8,top=24,;
    width=560+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-7,top=25,width=559+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='COM_PCES|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='COM_PCES'
        oDropInto=this.oBodyCol.oRow.oMCCOMPON_2_1
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1

  add object oTOTIMP_3_1 as StdField with uid="RGHTCQEAPK",rtseq=11,rtrep=.f.,;
    cFormVar="w_TOTIMP",value=0,enabled=.f.,;
    HelpContextID = 111915210,;
    cQueryName = "TOTIMP",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=294, Top=225, cSayPict=[v_PV(38+VVL)], cGetPict=[v_GV(38+VVL)]

  add object oTOTRIP_3_2 as StdField with uid="LDWUYSFPEX",rtseq=12,rtrep=.f.,;
    cFormVar="w_TOTRIP",value=0,enabled=.f.,;
    HelpContextID = 115519690,;
    cQueryName = "TOTRIP",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=294, Top=251, cSayPict=[v_PV(38+VVL)], cGetPict=[v_GV(38+VVL)]

  add object oTOTRES_3_3 as StdField with uid="EOHRXWXLUE",rtseq=13,rtrep=.f.,;
    cFormVar="w_TOTRES",value=0,enabled=.f.,;
    HelpContextID = 69382346,;
    cQueryName = "TOTRES",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=294, Top=277, cSayPict=[v_PV(38+VVL)], cGetPict=[v_GV(38+VVL)]
enddefine

* --- Defining Body row
define class tgsce_mmcBodyRow as CPBodyRowCnt
  Width=550
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oMCCOMPON_2_1 as StdTrsField with uid="IMWLNSIPRT",rtseq=3,rtrep=.t.,;
    cFormVar="w_MCCOMPON",value=space(40),isprimarykey=.t.,;
    HelpContextID = 111594732,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=275, Left=-2, Top=0, InputMask=replicate('X',40), bHasZoom = .t. , cLinkFile="COM_PCES", oKey_1_1="COCODICE", oKey_1_2="this.w_CODCES", oKey_2_1="COCOMPON", oKey_2_2="this.w_MCCOMPON"

  func oMCCOMPON_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oMCCOMPON_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oMCCOMPON_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    if not(this.parent.oMCCOMPON_2_1.readonly and this.parent.oMCCOMPON_2_1.isprimarykey)
    if i_TableProp[this.parent.oContained.COM_PCES_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"COCODICE="+cp_ToStrODBC(this.Parent.oContained.w_CODCES)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"COCODICE="+cp_ToStr(this.Parent.oContained.w_CODCES)
    endif
    do cp_zoom with 'COM_PCES','*','COCODICE,COCOMPON',cp_AbsName(this.parent,'oMCCOMPON_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Componenti cespite",'',this.parent.oContained
   endif
  endproc

  add object oMCIMPTOT_2_6 as StdTrsField with uid="FMZUZGKJJE",rtseq=8,rtrep=.t.,;
    cFormVar="w_MCIMPTOT",value=0,;
    HelpContextID = 41446630,;
    cTotal = "this.Parent.oContained.w_totimp", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=121, Left=288, Top=0, cSayPict=[v_PV(38+VVL)], cGetPict=[v_GV(38+VVL)]

  add object oMCIMPTOC_2_7 as StdTrsField with uid="BWUTOUHNCC",rtseq=9,rtrep=.t.,;
    cFormVar="w_MCIMPTOC",value=0,;
    HelpContextID = 41446647,;
    cTotal = "this.Parent.oContained.w_totimpc", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=124, Left=421, Top=-1, cSayPict=[v_PV(38+VVL)], cGetPict=[v_GV(38+VVL)]
  add object oLast as LastKeyMover
  * ---
  func oMCCOMPON_2_1.When()
    return(.t.)
  proc oMCCOMPON_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oMCCOMPON_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsce_mmc','MOV_PCES','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".MCSERIAL=MOV_PCES.MCSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
