* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_sgc                                                        *
*              Stampa gruppi contabili                                         *
*                                                                              *
*      Author: TAM Software srl                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-02-24                                                      *
* Last revis.: 2007-07-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsce_sgc",oParentObject))

* --- Class definition
define class tgsce_sgc as StdForm
  Top    = 50
  Left   = 100

  * --- Standard Properties
  Width  = 498
  Height = 139
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-07-18"
  HelpContextID=23741801
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=18

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsce_sgc"
  cComment = "Stampa gruppi contabili"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_DATSTAM = ctod('  /  /  ')
  w_OBSODAT1 = space(1)
  w_CONC01 = space(30)
  w_CONC02 = space(30)
  w_CONC03 = space(30)
  w_CONC04 = space(30)
  w_CONC05 = space(30)
  w_CONC06 = space(30)
  w_CONC07 = space(30)
  w_CONC08 = space(30)
  w_CONC09 = space(30)
  w_CONC10 = space(30)
  w_CONC11 = space(30)
  w_CONC12 = space(30)
  w_CONC13 = space(30)
  w_CONC14 = space(30)
  w_CONC15 = space(30)
  w_CONC16 = space(30)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsce_sgcPag1","gsce_sgc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDATSTAM_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      vx_exec(""+alltrim(THIS.w_OQRY)+", "+alltrim(THIS.w_orep)+"",this)
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_DATSTAM=ctod("  /  /  ")
      .w_OBSODAT1=space(1)
      .w_CONC01=space(30)
      .w_CONC02=space(30)
      .w_CONC03=space(30)
      .w_CONC04=space(30)
      .w_CONC05=space(30)
      .w_CONC06=space(30)
      .w_CONC07=space(30)
      .w_CONC08=space(30)
      .w_CONC09=space(30)
      .w_CONC10=space(30)
      .w_CONC11=space(30)
      .w_CONC12=space(30)
      .w_CONC13=space(30)
      .w_CONC14=space(30)
      .w_CONC15=space(30)
      .w_CONC16=space(30)
      .w_CONC01=oParentObject.w_CONC01
      .w_CONC02=oParentObject.w_CONC02
      .w_CONC03=oParentObject.w_CONC03
      .w_CONC04=oParentObject.w_CONC04
      .w_CONC05=oParentObject.w_CONC05
      .w_CONC06=oParentObject.w_CONC06
      .w_CONC07=oParentObject.w_CONC07
      .w_CONC08=oParentObject.w_CONC08
      .w_CONC09=oParentObject.w_CONC09
      .w_CONC10=oParentObject.w_CONC10
      .w_CONC11=oParentObject.w_CONC11
      .w_CONC12=oParentObject.w_CONC12
      .w_CONC13=oParentObject.w_CONC13
      .w_CONC14=oParentObject.w_CONC14
      .w_CONC15=oParentObject.w_CONC15
      .w_CONC16=oParentObject.w_CONC16
        .w_DATSTAM = i_datsys
        .w_OBSODAT1 = 'N'
      .oPgFrm.Page1.oPag.oObj_1_4.Calculate()
    endwith
    this.DoRTCalc(3,18,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_5.enabled = this.oPgFrm.Page1.oPag.oBtn_1_5.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_6.enabled = this.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_CONC01=.w_CONC01
      .oParentObject.w_CONC02=.w_CONC02
      .oParentObject.w_CONC03=.w_CONC03
      .oParentObject.w_CONC04=.w_CONC04
      .oParentObject.w_CONC05=.w_CONC05
      .oParentObject.w_CONC06=.w_CONC06
      .oParentObject.w_CONC07=.w_CONC07
      .oParentObject.w_CONC08=.w_CONC08
      .oParentObject.w_CONC09=.w_CONC09
      .oParentObject.w_CONC10=.w_CONC10
      .oParentObject.w_CONC11=.w_CONC11
      .oParentObject.w_CONC12=.w_CONC12
      .oParentObject.w_CONC13=.w_CONC13
      .oParentObject.w_CONC14=.w_CONC14
      .oParentObject.w_CONC15=.w_CONC15
      .oParentObject.w_CONC16=.w_CONC16
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_4.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,18,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_4.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_5.enabled = this.oPgFrm.Page1.oPag.oBtn_1_5.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_4.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDATSTAM_1_2.value==this.w_DATSTAM)
      this.oPgFrm.Page1.oPag.oDATSTAM_1_2.value=this.w_DATSTAM
    endif
    if not(this.oPgFrm.Page1.oPag.oOBSODAT1_1_3.RadioValue()==this.w_OBSODAT1)
      this.oPgFrm.Page1.oPag.oOBSODAT1_1_3.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsce_sgcPag1 as StdContainer
  Width  = 494
  height = 139
  stdWidth  = 494
  stdheight = 139
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDATSTAM_1_2 as StdField with uid="QPSUXYTZWI",rtseq=1,rtrep=.f.,;
    cFormVar = "w_DATSTAM", cQueryName = "DATSTAM",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di riferimento sul controllo dell'obsolescenza",;
    HelpContextID = 181518282,;
   bGlobalFont=.t.,;
    Height=21, Width=75, Left=107, Top=21

  add object oOBSODAT1_1_3 as StdCheck with uid="BBFHOGRGAR",rtseq=2,rtrep=.f.,left=226, top=21, caption="Stampa obsoleti",;
    ToolTipText = "Stampa solo obsoleti alla data di stampa",;
    HelpContextID = 69874199,;
    cFormVar="w_OBSODAT1", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oOBSODAT1_1_3.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oOBSODAT1_1_3.GetRadio()
    this.Parent.oContained.w_OBSODAT1 = this.RadioValue()
    return .t.
  endfunc

  func oOBSODAT1_1_3.SetRadio()
    this.Parent.oContained.w_OBSODAT1=trim(this.Parent.oContained.w_OBSODAT1)
    this.value = ;
      iif(this.Parent.oContained.w_OBSODAT1=='S',1,;
      0)
  endfunc


  add object oObj_1_4 as cp_outputCombo with uid="ZLERXIVPWT",left=107, top=57, width=381,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 154255846


  add object oBtn_1_5 as StdButton with uid="CBDJJAOLVC",left=388, top=89, width=48,height=45,;
    CpPicture="BMP\stampa.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per effettuare la stampa";
    , HelpContextID = 67204118;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_5.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_5.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY) and not empty(.w_datstam))
      endwith
    endif
  endfunc


  add object oBtn_1_6 as StdButton with uid="AAYJILLVJF",left=439, top=89, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 67204118;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_6.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_1 as StdString with uid="YYMISZIEOR",Visible=.t., Left=17, Top=21,;
    Alignment=1, Width=87, Height=15,;
    Caption="Data di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="QSDYXGWXFW",Visible=.t., Left=7, Top=57,;
    Alignment=1, Width=97, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsce_sgc','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
