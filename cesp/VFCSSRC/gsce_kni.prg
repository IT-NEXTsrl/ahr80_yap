* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_kni                                                        *
*              Stampa nota integrativa                                         *
*                                                                              *
*      Author: TAM Software srl                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-04-12                                                      *
* Last revis.: 2007-11-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsce_kni",oParentObject))

* --- Class definition
define class tgsce_kni as StdForm
  Top    = 50
  Left   = 100

  * --- Standard Properties
  Width  = 496
  Height = 174
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-11-15"
  HelpContextID=85310103
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=24

  * --- Constant Properties
  _IDX = 0
  ESERCIZI_IDX = 0
  VALUTE_IDX = 0
  PAR_CESP_IDX = 0
  cPrg = "gsce_kni"
  cComment = "Stampa nota integrativa"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CODAZI = space(5)
  w_CONC01 = space(30)
  w_CODESE = space(4)
  w_VALNAZ = space(3)
  w_FINESE = ctod('  /  /  ')
  w_DATSTAM = ctod('  /  /  ')
  w_CONTO = space(2)
  w_ODES = 0
  w_INIESE = ctod('  /  /  ')
  w_CONC02 = space(30)
  w_CONC03 = space(30)
  w_CONC04 = space(30)
  w_CONC05 = space(30)
  w_CONC06 = space(30)
  w_CONC07 = space(30)
  w_CONC08 = space(30)
  w_CONC09 = space(30)
  w_CONC10 = space(30)
  w_CONC11 = space(30)
  w_CONC12 = space(30)
  w_CONC13 = space(30)
  w_CONC14 = space(30)
  w_CONC15 = space(30)
  w_CONC16 = space(30)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsce_kniPag1","gsce_kni",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODESE_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='ESERCIZI'
    this.cWorkTables[2]='VALUTE'
    this.cWorkTables[3]='PAR_CESP'
    return(this.OpenAllTables(3))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do GSCE_BNI with this
      endwith
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI=space(5)
      .w_CONC01=space(30)
      .w_CODESE=space(4)
      .w_VALNAZ=space(3)
      .w_FINESE=ctod("  /  /  ")
      .w_DATSTAM=ctod("  /  /  ")
      .w_CONTO=space(2)
      .w_ODES=0
      .w_INIESE=ctod("  /  /  ")
      .w_CONC02=space(30)
      .w_CONC03=space(30)
      .w_CONC04=space(30)
      .w_CONC05=space(30)
      .w_CONC06=space(30)
      .w_CONC07=space(30)
      .w_CONC08=space(30)
      .w_CONC09=space(30)
      .w_CONC10=space(30)
      .w_CONC11=space(30)
      .w_CONC12=space(30)
      .w_CONC13=space(30)
      .w_CONC14=space(30)
      .w_CONC15=space(30)
      .w_CONC16=space(30)
        .w_CODAZI = i_CODAZI
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODAZI))
          .link_1_1('Full')
        endif
          .DoRTCalc(2,2,.f.)
        .w_CODESE = g_CODESE
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_CODESE))
          .link_1_3('Full')
        endif
          .DoRTCalc(4,5,.f.)
        .w_DATSTAM = i_datsys
        .w_CONTO = "1"
        .w_ODES = 1
    endwith
    this.DoRTCalc(9,24,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_1('Full')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(2,24,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCONC01_1_2.visible=!this.oPgFrm.Page1.oPag.oCONC01_1_2.mHide()
    this.oPgFrm.Page1.oPag.oCONTO_1_10.visible=!this.oPgFrm.Page1.oPag.oCONTO_1_10.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_15.visible=!this.oPgFrm.Page1.oPag.oStr_1_15.mHide()
    this.oPgFrm.Page1.oPag.oCONC02_1_16.visible=!this.oPgFrm.Page1.oPag.oCONC02_1_16.mHide()
    this.oPgFrm.Page1.oPag.oCONC03_1_17.visible=!this.oPgFrm.Page1.oPag.oCONC03_1_17.mHide()
    this.oPgFrm.Page1.oPag.oCONC04_1_18.visible=!this.oPgFrm.Page1.oPag.oCONC04_1_18.mHide()
    this.oPgFrm.Page1.oPag.oCONC05_1_19.visible=!this.oPgFrm.Page1.oPag.oCONC05_1_19.mHide()
    this.oPgFrm.Page1.oPag.oCONC06_1_20.visible=!this.oPgFrm.Page1.oPag.oCONC06_1_20.mHide()
    this.oPgFrm.Page1.oPag.oCONC07_1_21.visible=!this.oPgFrm.Page1.oPag.oCONC07_1_21.mHide()
    this.oPgFrm.Page1.oPag.oCONC08_1_22.visible=!this.oPgFrm.Page1.oPag.oCONC08_1_22.mHide()
    this.oPgFrm.Page1.oPag.oCONC09_1_23.visible=!this.oPgFrm.Page1.oPag.oCONC09_1_23.mHide()
    this.oPgFrm.Page1.oPag.oCONC10_1_24.visible=!this.oPgFrm.Page1.oPag.oCONC10_1_24.mHide()
    this.oPgFrm.Page1.oPag.oCONC11_1_25.visible=!this.oPgFrm.Page1.oPag.oCONC11_1_25.mHide()
    this.oPgFrm.Page1.oPag.oCONC12_1_26.visible=!this.oPgFrm.Page1.oPag.oCONC12_1_26.mHide()
    this.oPgFrm.Page1.oPag.oCONC13_1_27.visible=!this.oPgFrm.Page1.oPag.oCONC13_1_27.mHide()
    this.oPgFrm.Page1.oPag.oCONC14_1_28.visible=!this.oPgFrm.Page1.oPag.oCONC14_1_28.mHide()
    this.oPgFrm.Page1.oPag.oCONC15_1_29.visible=!this.oPgFrm.Page1.oPag.oCONC15_1_29.mHide()
    this.oPgFrm.Page1.oPag.oCONC16_1_30.visible=!this.oPgFrm.Page1.oPag.oCONC16_1_30.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_CESP_IDX,3]
    i_lTable = "PAR_CESP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_CESP_IDX,2], .t., this.PAR_CESP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_CESP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PCCODAZI,PCCONC01,PCCONC02,PCCONC03,PCCONC04,PCCONC05,PCCONC06,PCCONC07,PCCONC08,PCCONC09,PCCONC10,PCCONC11,PCCONC12,PCCONC13,PCCONC14,PCCONC15,PCCONC16";
                   +" from "+i_cTable+" "+i_lTable+" where PCCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PCCODAZI',this.w_CODAZI)
            select PCCODAZI,PCCONC01,PCCONC02,PCCONC03,PCCONC04,PCCONC05,PCCONC06,PCCONC07,PCCONC08,PCCONC09,PCCONC10,PCCONC11,PCCONC12,PCCONC13,PCCONC14,PCCONC15,PCCONC16;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.PCCODAZI,space(5))
      this.w_CONC01 = NVL(_Link_.PCCONC01,space(30))
      this.w_CONC02 = NVL(_Link_.PCCONC02,space(30))
      this.w_CONC03 = NVL(_Link_.PCCONC03,space(30))
      this.w_CONC04 = NVL(_Link_.PCCONC04,space(30))
      this.w_CONC05 = NVL(_Link_.PCCONC05,space(30))
      this.w_CONC06 = NVL(_Link_.PCCONC06,space(30))
      this.w_CONC07 = NVL(_Link_.PCCONC07,space(30))
      this.w_CONC08 = NVL(_Link_.PCCONC08,space(30))
      this.w_CONC09 = NVL(_Link_.PCCONC09,space(30))
      this.w_CONC10 = NVL(_Link_.PCCONC10,space(30))
      this.w_CONC11 = NVL(_Link_.PCCONC11,space(30))
      this.w_CONC12 = NVL(_Link_.PCCONC12,space(30))
      this.w_CONC13 = NVL(_Link_.PCCONC13,space(30))
      this.w_CONC14 = NVL(_Link_.PCCONC14,space(30))
      this.w_CONC15 = NVL(_Link_.PCCONC15,space(30))
      this.w_CONC16 = NVL(_Link_.PCCONC16,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_CONC01 = space(30)
      this.w_CONC02 = space(30)
      this.w_CONC03 = space(30)
      this.w_CONC04 = space(30)
      this.w_CONC05 = space(30)
      this.w_CONC06 = space(30)
      this.w_CONC07 = space(30)
      this.w_CONC08 = space(30)
      this.w_CONC09 = space(30)
      this.w_CONC10 = space(30)
      this.w_CONC11 = space(30)
      this.w_CONC12 = space(30)
      this.w_CONC13 = space(30)
      this.w_CONC14 = space(30)
      this.w_CONC15 = space(30)
      this.w_CONC16 = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_CESP_IDX,2])+'\'+cp_ToStr(_Link_.PCCODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_CESP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODESE
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_CODESE)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ,ESFINESE,ESINIESE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_CODAZI;
                     ,'ESCODESE',trim(this.w_CODESE))
          select ESCODAZI,ESCODESE,ESVALNAZ,ESFINESE,ESINIESE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODESE)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODESE) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oCODESE_1_3'),i_cWhere,'',"Esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ,ESFINESE,ESINIESE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESVALNAZ,ESFINESE,ESINIESE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ,ESFINESE,ESINIESE";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESVALNAZ,ESFINESE,ESINIESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ,ESFINESE,ESINIESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_CODESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_CODESE)
            select ESCODAZI,ESCODESE,ESVALNAZ,ESFINESE,ESINIESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODESE = NVL(_Link_.ESCODESE,space(4))
      this.w_VALNAZ = NVL(_Link_.ESVALNAZ,space(3))
      this.w_FINESE = NVL(cp_ToDate(_Link_.ESFINESE),ctod("  /  /  "))
      this.w_INIESE = NVL(cp_ToDate(_Link_.ESINIESE),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODESE = space(4)
      endif
      this.w_VALNAZ = space(3)
      this.w_FINESE = ctod("  /  /  ")
      this.w_INIESE = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCONC01_1_2.value==this.w_CONC01)
      this.oPgFrm.Page1.oPag.oCONC01_1_2.value=this.w_CONC01
    endif
    if not(this.oPgFrm.Page1.oPag.oCODESE_1_3.value==this.w_CODESE)
      this.oPgFrm.Page1.oPag.oCODESE_1_3.value=this.w_CODESE
    endif
    if not(this.oPgFrm.Page1.oPag.oDATSTAM_1_9.value==this.w_DATSTAM)
      this.oPgFrm.Page1.oPag.oDATSTAM_1_9.value=this.w_DATSTAM
    endif
    if not(this.oPgFrm.Page1.oPag.oCONTO_1_10.RadioValue()==this.w_CONTO)
      this.oPgFrm.Page1.oPag.oCONTO_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oODES_1_11.RadioValue()==this.w_ODES)
      this.oPgFrm.Page1.oPag.oODES_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCONC02_1_16.value==this.w_CONC02)
      this.oPgFrm.Page1.oPag.oCONC02_1_16.value=this.w_CONC02
    endif
    if not(this.oPgFrm.Page1.oPag.oCONC03_1_17.value==this.w_CONC03)
      this.oPgFrm.Page1.oPag.oCONC03_1_17.value=this.w_CONC03
    endif
    if not(this.oPgFrm.Page1.oPag.oCONC04_1_18.value==this.w_CONC04)
      this.oPgFrm.Page1.oPag.oCONC04_1_18.value=this.w_CONC04
    endif
    if not(this.oPgFrm.Page1.oPag.oCONC05_1_19.value==this.w_CONC05)
      this.oPgFrm.Page1.oPag.oCONC05_1_19.value=this.w_CONC05
    endif
    if not(this.oPgFrm.Page1.oPag.oCONC06_1_20.value==this.w_CONC06)
      this.oPgFrm.Page1.oPag.oCONC06_1_20.value=this.w_CONC06
    endif
    if not(this.oPgFrm.Page1.oPag.oCONC07_1_21.value==this.w_CONC07)
      this.oPgFrm.Page1.oPag.oCONC07_1_21.value=this.w_CONC07
    endif
    if not(this.oPgFrm.Page1.oPag.oCONC08_1_22.value==this.w_CONC08)
      this.oPgFrm.Page1.oPag.oCONC08_1_22.value=this.w_CONC08
    endif
    if not(this.oPgFrm.Page1.oPag.oCONC09_1_23.value==this.w_CONC09)
      this.oPgFrm.Page1.oPag.oCONC09_1_23.value=this.w_CONC09
    endif
    if not(this.oPgFrm.Page1.oPag.oCONC10_1_24.value==this.w_CONC10)
      this.oPgFrm.Page1.oPag.oCONC10_1_24.value=this.w_CONC10
    endif
    if not(this.oPgFrm.Page1.oPag.oCONC11_1_25.value==this.w_CONC11)
      this.oPgFrm.Page1.oPag.oCONC11_1_25.value=this.w_CONC11
    endif
    if not(this.oPgFrm.Page1.oPag.oCONC12_1_26.value==this.w_CONC12)
      this.oPgFrm.Page1.oPag.oCONC12_1_26.value=this.w_CONC12
    endif
    if not(this.oPgFrm.Page1.oPag.oCONC13_1_27.value==this.w_CONC13)
      this.oPgFrm.Page1.oPag.oCONC13_1_27.value=this.w_CONC13
    endif
    if not(this.oPgFrm.Page1.oPag.oCONC14_1_28.value==this.w_CONC14)
      this.oPgFrm.Page1.oPag.oCONC14_1_28.value=this.w_CONC14
    endif
    if not(this.oPgFrm.Page1.oPag.oCONC15_1_29.value==this.w_CONC15)
      this.oPgFrm.Page1.oPag.oCONC15_1_29.value=this.w_CONC15
    endif
    if not(this.oPgFrm.Page1.oPag.oCONC16_1_30.value==this.w_CONC16)
      this.oPgFrm.Page1.oPag.oCONC16_1_30.value=this.w_CONC16
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsce_kniPag1 as StdContainer
  Width  = 492
  height = 174
  stdWidth  = 492
  stdheight = 174
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCONC01_1_2 as StdField with uid="RCLWMCPMPL",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CONC01", cQueryName = "CONC01",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 157150758,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=263, Top=55, InputMask=replicate('X',30)

  func oCONC01_1_2.mHide()
    with this.Parent.oContained
      return (.w_CONTO<>"1" or .w_ODES=3)
    endwith
  endfunc

  add object oCODESE_1_3 as StdField with uid="BPLTSGVYFI",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CODESE", cQueryName = "CODESE",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Esercizio di competenza",;
    HelpContextID = 261049894,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=135, Top=16, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_CODESE"

  func oCODESE_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODESE_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODESE_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CODAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_CODAZI)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oCODESE_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Esercizi",'',this.parent.oContained
  endproc

  add object oDATSTAM_1_9 as StdField with uid="ZUIPUWXQNK",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DATSTAM", cQueryName = "DATSTAM",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 195969078,;
   bGlobalFont=.t.,;
    Height=21, Width=75, Left=411, Top=16


  add object oCONTO_1_10 as StdCombo with uid="HUEVRGXTYV",rtseq=7,rtrep=.f.,left=198,top=56,width=58,height=21;
    , ToolTipText = "Conto patrimoniale su cui imputare l'importo";
    , HelpContextID = 173993510;
    , cFormVar="w_CONTO",RowSource=""+"C01,"+"C02,"+"C03,"+"C04,"+"C05,"+"C06,"+"C07,"+"C08,"+"C09,"+"C10,"+"C11,"+"C12,"+"C13,"+"C14,"+"C15,"+"C16", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCONTO_1_10.RadioValue()
    return(iif(this.value =1,"1",;
    iif(this.value =2,"2",;
    iif(this.value =3,"3",;
    iif(this.value =4,"4",;
    iif(this.value =5,"5",;
    iif(this.value =6,"6",;
    iif(this.value =7,"7",;
    iif(this.value =8,"8",;
    iif(this.value =9,"9",;
    iif(this.value =10,"10",;
    iif(this.value =11,"11",;
    iif(this.value =12,"12",;
    iif(this.value =13,"13",;
    iif(this.value =14,"14",;
    iif(this.value =15,"15",;
    iif(this.value =16,"16",;
    space(2))))))))))))))))))
  endfunc
  func oCONTO_1_10.GetRadio()
    this.Parent.oContained.w_CONTO = this.RadioValue()
    return .t.
  endfunc

  func oCONTO_1_10.SetRadio()
    this.Parent.oContained.w_CONTO=trim(this.Parent.oContained.w_CONTO)
    this.value = ;
      iif(this.Parent.oContained.w_CONTO=="1",1,;
      iif(this.Parent.oContained.w_CONTO=="2",2,;
      iif(this.Parent.oContained.w_CONTO=="3",3,;
      iif(this.Parent.oContained.w_CONTO=="4",4,;
      iif(this.Parent.oContained.w_CONTO=="5",5,;
      iif(this.Parent.oContained.w_CONTO=="6",6,;
      iif(this.Parent.oContained.w_CONTO=="7",7,;
      iif(this.Parent.oContained.w_CONTO=="8",8,;
      iif(this.Parent.oContained.w_CONTO=="9",9,;
      iif(this.Parent.oContained.w_CONTO=="10",10,;
      iif(this.Parent.oContained.w_CONTO=="11",11,;
      iif(this.Parent.oContained.w_CONTO=="12",12,;
      iif(this.Parent.oContained.w_CONTO=="13",13,;
      iif(this.Parent.oContained.w_CONTO=="14",14,;
      iif(this.Parent.oContained.w_CONTO=="15",15,;
      iif(this.Parent.oContained.w_CONTO=="16",16,;
      0))))))))))))))))
  endfunc

  func oCONTO_1_10.mHide()
    with this.Parent.oContained
      return (.w_ODES=3)
    endwith
  endfunc


  add object oODES_1_11 as StdCombo with uid="FPWXHIKAFP",rtseq=8,rtrep=.f.,left=135,top=96,width=351,height=21;
    , ToolTipText = "Report utilizzato per la stampa";
    , HelpContextID = 91050982;
    , cFormVar="w_ODES",RowSource=""+"(2) Movimenti delle immobilizzazioni,"+"(8) Oneri finanziari imputati nell'esercizio,"+"(13) Composizione delle voci 'Proventi Straordinari' e ...", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oODES_1_11.RadioValue()
    return(iif(this.value =1,1,;
    iif(this.value =2,2,;
    iif(this.value =3,3,;
    0))))
  endfunc
  func oODES_1_11.GetRadio()
    this.Parent.oContained.w_ODES = this.RadioValue()
    return .t.
  endfunc

  func oODES_1_11.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_ODES==1,1,;
      iif(this.Parent.oContained.w_ODES==2,2,;
      iif(this.Parent.oContained.w_ODES==3,3,;
      0)))
  endfunc


  add object oBtn_1_12 as StdButton with uid="CJIPHLZLZP",left=384, top=123, width=48,height=45,;
    CpPicture="BMP\stampa.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per stampare";
    , HelpContextID = 176256022;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_12.Click()
      with this.Parent.oContained
        do GSCE_BNI with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_12.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_datstam) AND not empty(.w_codese))
      endwith
    endif
  endfunc


  add object oBtn_1_13 as StdButton with uid="LKUTNWOIVH",left=435, top=123, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 176256022;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_13.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oCONC02_1_16 as StdField with uid="WUMYZTOEEW",rtseq=10,rtrep=.f.,;
    cFormVar = "w_CONC02", cQueryName = "CONC02",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 173927974,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=263, Top=55, InputMask=replicate('X',30)

  func oCONC02_1_16.mHide()
    with this.Parent.oContained
      return (.w_CONTO<>"2" or .w_ODES=3)
    endwith
  endfunc

  add object oCONC03_1_17 as StdField with uid="RCQTZJZDAI",rtseq=11,rtrep=.f.,;
    cFormVar = "w_CONC03", cQueryName = "CONC03",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 190705190,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=263, Top=55, InputMask=replicate('X',30)

  func oCONC03_1_17.mHide()
    with this.Parent.oContained
      return (.w_CONTO<>"3" or .w_ODES=3)
    endwith
  endfunc

  add object oCONC04_1_18 as StdField with uid="DSKJNGBAFW",rtseq=12,rtrep=.f.,;
    cFormVar = "w_CONC04", cQueryName = "CONC04",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 207482406,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=263, Top=55, InputMask=replicate('X',30)

  func oCONC04_1_18.mHide()
    with this.Parent.oContained
      return (.w_CONTO<>"4" or .w_ODES=3)
    endwith
  endfunc

  add object oCONC05_1_19 as StdField with uid="EFLJQILZMK",rtseq=13,rtrep=.f.,;
    cFormVar = "w_CONC05", cQueryName = "CONC05",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 224259622,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=263, Top=55, InputMask=replicate('X',30)

  func oCONC05_1_19.mHide()
    with this.Parent.oContained
      return (.w_CONTO<>"5" or .w_ODES=3)
    endwith
  endfunc

  add object oCONC06_1_20 as StdField with uid="FBYBHQRXAS",rtseq=14,rtrep=.f.,;
    cFormVar = "w_CONC06", cQueryName = "CONC06",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 241036838,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=263, Top=55, InputMask=replicate('X',30)

  func oCONC06_1_20.mHide()
    with this.Parent.oContained
      return (.w_CONTO<>"6" or .w_ODES=3)
    endwith
  endfunc

  add object oCONC07_1_21 as StdField with uid="YZRNWZUAID",rtseq=15,rtrep=.f.,;
    cFormVar = "w_CONC07", cQueryName = "CONC07",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 257814054,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=263, Top=55, InputMask=replicate('X',30)

  func oCONC07_1_21.mHide()
    with this.Parent.oContained
      return (.w_CONTO<>"7" or .w_ODES=3)
    endwith
  endfunc

  add object oCONC08_1_22 as StdField with uid="IFOPDIVXTP",rtseq=16,rtrep=.f.,;
    cFormVar = "w_CONC08", cQueryName = "CONC08",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 6155814,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=263, Top=55, InputMask=replicate('X',30)

  func oCONC08_1_22.mHide()
    with this.Parent.oContained
      return (.w_CONTO<>"8" or .w_ODES=3)
    endwith
  endfunc

  add object oCONC09_1_23 as StdField with uid="PTSDYEJLML",rtseq=17,rtrep=.f.,;
    cFormVar = "w_CONC09", cQueryName = "CONC09",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 22933030,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=263, Top=55, InputMask=replicate('X',30)

  func oCONC09_1_23.mHide()
    with this.Parent.oContained
      return (.w_CONTO<>"9" or .w_ODES=3)
    endwith
  endfunc

  add object oCONC10_1_24 as StdField with uid="HYWFDCKNGH",rtseq=18,rtrep=.f.,;
    cFormVar = "w_CONC10", cQueryName = "CONC10",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 141422118,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=263, Top=55, InputMask=replicate('X',30)

  func oCONC10_1_24.mHide()
    with this.Parent.oContained
      return (.w_CONTO<>"10" or .w_ODES=3)
    endwith
  endfunc

  add object oCONC11_1_25 as StdField with uid="MULKMWHKPE",rtseq=19,rtrep=.f.,;
    cFormVar = "w_CONC11", cQueryName = "CONC11",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 158199334,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=263, Top=55, InputMask=replicate('X',30)

  func oCONC11_1_25.mHide()
    with this.Parent.oContained
      return (.w_CONTO<>"11" or .w_ODES=3)
    endwith
  endfunc

  add object oCONC12_1_26 as StdField with uid="RFWMFWEJBY",rtseq=20,rtrep=.f.,;
    cFormVar = "w_CONC12", cQueryName = "CONC12",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 174976550,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=263, Top=55, InputMask=replicate('X',30)

  func oCONC12_1_26.mHide()
    with this.Parent.oContained
      return (.w_CONTO<>"12" or .w_ODES=3)
    endwith
  endfunc

  add object oCONC13_1_27 as StdField with uid="AGUZHTLKAM",rtseq=21,rtrep=.f.,;
    cFormVar = "w_CONC13", cQueryName = "CONC13",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 191753766,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=263, Top=55, InputMask=replicate('X',30)

  func oCONC13_1_27.mHide()
    with this.Parent.oContained
      return (.w_CONTO<>"13" or .w_ODES=3)
    endwith
  endfunc

  add object oCONC14_1_28 as StdField with uid="AIWAPZCBGC",rtseq=22,rtrep=.f.,;
    cFormVar = "w_CONC14", cQueryName = "CONC14",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 208530982,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=263, Top=55, InputMask=replicate('X',30)

  func oCONC14_1_28.mHide()
    with this.Parent.oContained
      return (.w_CONTO<>"14" or .w_ODES=3)
    endwith
  endfunc

  add object oCONC15_1_29 as StdField with uid="THLMCDGVNH",rtseq=23,rtrep=.f.,;
    cFormVar = "w_CONC15", cQueryName = "CONC15",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 225308198,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=263, Top=55, InputMask=replicate('X',30)

  func oCONC15_1_29.mHide()
    with this.Parent.oContained
      return (.w_CONTO<>"15" or .w_ODES=3)
    endwith
  endfunc

  add object oCONC16_1_30 as StdField with uid="BHUJQVACCY",rtseq=24,rtrep=.f.,;
    cFormVar = "w_CONC16", cQueryName = "CONC16",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 242085414,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=263, Top=55, InputMask=replicate('X',30)

  func oCONC16_1_30.mHide()
    with this.Parent.oContained
      return (.w_CONTO<>"16" or .w_ODES=3)
    endwith
  endfunc

  add object oStr_1_6 as StdString with uid="BGZIEHMXIR",Visible=.t., Left=322, Top=17,;
    Alignment=1, Width=87, Height=15,;
    Caption="Data di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="NZWIEVFJCH",Visible=.t., Left=35, Top=98,;
    Alignment=1, Width=97, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="SNPKSYPYGH",Visible=.t., Left=75, Top=17,;
    Alignment=1, Width=57, Height=15,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="WVKWITRTZU",Visible=.t., Left=7, Top=58,;
    Alignment=1, Width=191, Height=18,;
    Caption="Conto su cui imputare l'importo:"  ;
  , bGlobalFont=.t.

  func oStr_1_15.mHide()
    with this.Parent.oContained
      return (.w_ODES=3)
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsce_kni','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
