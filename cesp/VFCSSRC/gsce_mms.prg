* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_mms                                                        *
*              Dettaglio componenti                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30] [VRS_31]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-03-18                                                      *
* Last revis.: 2011-03-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsce_mms")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsce_mms")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsce_mms")
  return

* --- Class definition
define class tgsce_mms as StdPCForm
  Width  = 701
  Height = 353
  Top    = 137
  Left   = 59
  cComment = "Dettaglio componenti"
  cPrg = "gsce_mms"
  HelpContextID=197805417
  add object cnt as tcgsce_mms
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsce_mms as PCContext
  w_SCSERIAL = space(10)
  w_CODCES = space(20)
  w_DATVEN = space(10)
  w_VALMOV = space(3)
  w_DECTOT1 = 0
  w_SCCOMPON = space(40)
  w_VALUTA = space(3)
  w_SCROWNUM = 0
  w_DTVEND = space(8)
  w_SCFLSELE = space(1)
  w_SCROWSAL = 0
  w_SCDATVEN = space(8)
  w_CODMAT = space(20)
  w_CODUBI = space(20)
  w_QTACOM = 0
  w_IMPTOT = 0
  w_DECTOT = 0
  w_CALCPIC = 0
  w_IMPSEL = 0
  w_SELEZ1 = space(1)
  w_IMPTOC = 0
  w_IMPSEC = 0
  w_SCAGGDAT = space(1)
  proc Save(i_oFrom)
    this.w_SCSERIAL = i_oFrom.w_SCSERIAL
    this.w_CODCES = i_oFrom.w_CODCES
    this.w_DATVEN = i_oFrom.w_DATVEN
    this.w_VALMOV = i_oFrom.w_VALMOV
    this.w_DECTOT1 = i_oFrom.w_DECTOT1
    this.w_SCCOMPON = i_oFrom.w_SCCOMPON
    this.w_VALUTA = i_oFrom.w_VALUTA
    this.w_SCROWNUM = i_oFrom.w_SCROWNUM
    this.w_DTVEND = i_oFrom.w_DTVEND
    this.w_SCFLSELE = i_oFrom.w_SCFLSELE
    this.w_SCROWSAL = i_oFrom.w_SCROWSAL
    this.w_SCDATVEN = i_oFrom.w_SCDATVEN
    this.w_CODMAT = i_oFrom.w_CODMAT
    this.w_CODUBI = i_oFrom.w_CODUBI
    this.w_QTACOM = i_oFrom.w_QTACOM
    this.w_IMPTOT = i_oFrom.w_IMPTOT
    this.w_DECTOT = i_oFrom.w_DECTOT
    this.w_CALCPIC = i_oFrom.w_CALCPIC
    this.w_IMPSEL = i_oFrom.w_IMPSEL
    this.w_SELEZ1 = i_oFrom.w_SELEZ1
    this.w_IMPTOC = i_oFrom.w_IMPTOC
    this.w_IMPSEC = i_oFrom.w_IMPSEC
    this.w_SCAGGDAT = i_oFrom.w_SCAGGDAT
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_SCSERIAL = this.w_SCSERIAL
    i_oTo.w_CODCES = this.w_CODCES
    i_oTo.w_DATVEN = this.w_DATVEN
    i_oTo.w_VALMOV = this.w_VALMOV
    i_oTo.w_DECTOT1 = this.w_DECTOT1
    i_oTo.w_SCCOMPON = this.w_SCCOMPON
    i_oTo.w_VALUTA = this.w_VALUTA
    i_oTo.w_SCROWNUM = this.w_SCROWNUM
    i_oTo.w_DTVEND = this.w_DTVEND
    i_oTo.w_SCFLSELE = this.w_SCFLSELE
    i_oTo.w_SCROWSAL = this.w_SCROWSAL
    i_oTo.w_SCDATVEN = this.w_SCDATVEN
    i_oTo.w_CODMAT = this.w_CODMAT
    i_oTo.w_CODUBI = this.w_CODUBI
    i_oTo.w_QTACOM = this.w_QTACOM
    i_oTo.w_IMPTOT = this.w_IMPTOT
    i_oTo.w_DECTOT = this.w_DECTOT
    i_oTo.w_CALCPIC = this.w_CALCPIC
    i_oTo.w_IMPSEL = this.w_IMPSEL
    i_oTo.w_SELEZ1 = this.w_SELEZ1
    i_oTo.w_IMPTOC = this.w_IMPTOC
    i_oTo.w_IMPSEC = this.w_IMPSEC
    i_oTo.w_SCAGGDAT = this.w_SCAGGDAT
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsce_mms as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 701
  Height = 353
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-03-02"
  HelpContextID=197805417
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=23

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  MOV_SCES_IDX = 0
  COM_PUBI_IDX = 0
  COM_PCES_IDX = 0
  VALUTE_IDX = 0
  cFile = "MOV_SCES"
  cKeySelect = "SCSERIAL"
  cKeyWhere  = "SCSERIAL=this.w_SCSERIAL"
  cKeyDetail  = "SCSERIAL=this.w_SCSERIAL and SCCOMPON=this.w_SCCOMPON and SCROWNUM=this.w_SCROWNUM"
  cKeyWhereODBC = '"SCSERIAL="+cp_ToStrODBC(this.w_SCSERIAL)';

  cKeyDetailWhereODBC = '"SCSERIAL="+cp_ToStrODBC(this.w_SCSERIAL)';
      +'+" and SCCOMPON="+cp_ToStrODBC(this.w_SCCOMPON)';
      +'+" and SCROWNUM="+cp_ToStrODBC(this.w_SCROWNUM)';

  cKeyWhereODBCqualified = '"MOV_SCES.SCSERIAL="+cp_ToStrODBC(this.w_SCSERIAL)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cPrg = "gsce_mms"
  cComment = "Dettaglio componenti"
  i_nRowNum = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_SCSERIAL = space(10)
  w_CODCES = space(20)
  w_DATVEN = space(10)
  w_VALMOV = space(3)
  w_DECTOT1 = 0
  w_SCCOMPON = space(40)
  o_SCCOMPON = space(40)
  w_VALUTA = space(3)
  w_SCROWNUM = 0
  w_DTVEND = ctod('  /  /  ')
  w_SCFLSELE = space(1)
  o_SCFLSELE = space(1)
  w_SCROWSAL = 0
  w_SCDATVEN = ctod('  /  /  ')
  w_CODMAT = space(20)
  w_CODUBI = space(20)
  w_QTACOM = 0
  w_IMPTOT = 0
  w_DECTOT = 0
  w_CALCPIC = 0
  w_IMPSEL = 0
  w_SELEZ1 = space(1)
  w_IMPTOC = 0
  w_IMPSEC = 0
  w_SCAGGDAT = space(1)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsce_mmsPag1","gsce_mms",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSELEZ1_1_10
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='COM_PUBI'
    this.cWorkTables[2]='COM_PCES'
    this.cWorkTables[3]='VALUTE'
    this.cWorkTables[4]='MOV_SCES'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(4))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.MOV_SCES_IDX,5],7]
    this.nPostItConn=i_TableProp[this.MOV_SCES_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsce_mms'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from MOV_SCES where SCSERIAL=KeySet.SCSERIAL
    *                            and SCCOMPON=KeySet.SCCOMPON
    *                            and SCROWNUM=KeySet.SCROWNUM
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.MOV_SCES_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOV_SCES_IDX,2],this.bLoadRecFilter,this.MOV_SCES_IDX,"gsce_mms")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('MOV_SCES')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "MOV_SCES.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' MOV_SCES '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'SCSERIAL',this.w_SCSERIAL  )
      select * from (i_cTable) MOV_SCES where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_CODCES = this.oParentObject .w_MCCODCES
        .w_DECTOT1 = 0
        .w_IMPSEL = IIF(.cfunction='Edit', .oParentObject .w_MCIMPA05 , 0 )
        .w_SELEZ1 = 'D'
        .w_IMPSEC = IIF(.cfunction='Edit', .oParentObject .w_MCIMPA22 , 0 )
        .w_SCSERIAL = NVL(SCSERIAL,space(10))
        .w_DATVEN = this.oParentObject .w_MCDATREG
        .w_VALMOV = THIS.oParentObject .w_MCCODVAL
          .link_1_7('Load')
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
        .w_SCAGGDAT = NVL(SCAGGDAT,space(1))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'MOV_SCES')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_VALUTA = space(3)
          .w_DTVEND = ctod("  /  /  ")
          .w_CODMAT = space(20)
          .w_CODUBI = space(20)
          .w_QTACOM = 0
          .w_IMPTOT = 0
          .w_DECTOT = 0
          .w_SCCOMPON = NVL(SCCOMPON,space(40))
          .link_2_1('Load')
          .link_2_2('Load')
          .w_SCROWNUM = NVL(SCROWNUM,0)
          .link_2_3('Load')
          .w_SCFLSELE = NVL(SCFLSELE,space(1))
          .w_SCROWSAL = NVL(SCROWSAL,0)
          * evitabile
          *.link_2_6('Load')
          .w_SCDATVEN = NVL(cp_ToDate(SCDATVEN),ctod("  /  /  "))
        .oPgFrm.Page1.oPag.oObj_2_12.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_13.Calculate()
        .w_CALCPIC = DEFPIC(.w_DECTOT1)
        .w_IMPTOC = cp_ROUND(VALCAM(NVL(.w_IMPTOC,0), .w_VALUTA, .w_VALMOV, .w_DATVEN, 0), .w_DECTOT1)
          select (this.cTrsName)
          append blank
          replace CODCES with .w_CODCES
          replace SCCOMPON with .w_SCCOMPON
          replace SCROWSAL with .w_SCROWSAL
          replace SCDATVEN with .w_SCDATVEN
          replace SCAGGDAT with .w_SCAGGDAT
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace SCROWNUM with .w_SCROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .w_DATVEN = this.oParentObject .w_MCDATREG
        .w_VALMOV = THIS.oParentObject .w_MCCODVAL
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_SCSERIAL=space(10)
      .w_CODCES=space(20)
      .w_DATVEN=space(10)
      .w_VALMOV=space(3)
      .w_DECTOT1=0
      .w_SCCOMPON=space(40)
      .w_VALUTA=space(3)
      .w_SCROWNUM=0
      .w_DTVEND=ctod("  /  /  ")
      .w_SCFLSELE=space(1)
      .w_SCROWSAL=0
      .w_SCDATVEN=ctod("  /  /  ")
      .w_CODMAT=space(20)
      .w_CODUBI=space(20)
      .w_QTACOM=0
      .w_IMPTOT=0
      .w_DECTOT=0
      .w_CALCPIC=0
      .w_IMPSEL=0
      .w_SELEZ1=space(1)
      .w_IMPTOC=0
      .w_IMPSEC=0
      .w_SCAGGDAT=space(1)
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
        .w_CODCES = this.oParentObject .w_MCCODCES
        .w_DATVEN = this.oParentObject .w_MCDATREG
        .w_VALMOV = THIS.oParentObject .w_MCCODVAL
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_VALMOV))
         .link_1_7('Full')
        endif
        .DoRTCalc(5,6,.f.)
        if not(empty(.w_SCCOMPON))
         .link_2_1('Full')
        endif
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_VALUTA))
         .link_2_2('Full')
        endif
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_SCROWNUM))
         .link_2_3('Full')
        endif
        .DoRTCalc(9,10,.f.)
        .w_SCROWSAL = IIF(.w_SCFLSELE='S', .w_SCROWNUM, 0)
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_SCROWSAL))
         .link_2_6('Full')
        endif
        .w_SCDATVEN = .w_DATVEN
        .oPgFrm.Page1.oPag.oObj_2_12.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_13.Calculate()
        .DoRTCalc(13,17,.f.)
        .w_CALCPIC = DEFPIC(.w_DECTOT1)
        .w_IMPSEL = IIF(.cfunction='Edit', .oParentObject .w_MCIMPA05 , 0 )
        .w_SELEZ1 = 'D'
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
        .w_IMPTOC = cp_ROUND(VALCAM(NVL(.w_IMPTOC,0), .w_VALUTA, .w_VALMOV, .w_DATVEN, 0), .w_DECTOT1)
        .w_IMPSEC = IIF(.cfunction='Edit', .oParentObject .w_MCIMPA22 , 0 )
        .w_SCAGGDAT = '='
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'MOV_SCES')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oSELEZ1_1_10.enabled_(i_bVal)
      .Page1.oPag.oBtn_1_9.enabled = i_bVal
      .Page1.oPag.oObj_2_12.enabled = i_bVal
      .Page1.oPag.oObj_2_13.enabled = i_bVal
      .Page1.oPag.oObj_1_11.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'MOV_SCES',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.MOV_SCES_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCSERIAL,"SCSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCAGGDAT,"SCAGGDAT",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_SCCOMPON C(40);
      ,t_SCROWNUM N(3);
      ,t_DTVEND D(8);
      ,t_SCFLSELE N(3);
      ,t_CODMAT C(20);
      ,t_CODUBI C(20);
      ,t_QTACOM N(12,3);
      ,t_IMPTOT N(18,4);
      ,t_IMPTOC N(18,4);
      ,CODCES C(20);
      ,SCCOMPON C(40);
      ,SCROWSAL N(3);
      ,SCDATVEN D(8);
      ,SCAGGDAT C(1);
      ,SCROWNUM N(3);
      ,t_VALUTA C(3);
      ,t_SCROWSAL N(3);
      ,t_SCDATVEN D(8);
      ,t_DECTOT N(1);
      ,t_CALCPIC N(1);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsce_mmsbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSCCOMPON_2_1.controlsource=this.cTrsName+'.t_SCCOMPON'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSCROWNUM_2_3.controlsource=this.cTrsName+'.t_SCROWNUM'
    this.oPgFRm.Page1.oPag.oDTVEND_2_4.controlsource=this.cTrsName+'.t_DTVEND'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSCFLSELE_2_5.controlsource=this.cTrsName+'.t_SCFLSELE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCODMAT_2_8.controlsource=this.cTrsName+'.t_CODMAT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCODUBI_2_9.controlsource=this.cTrsName+'.t_CODUBI'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oQTACOM_2_10.controlsource=this.cTrsName+'.t_QTACOM'
    this.oPgFRm.Page1.oPag.oIMPTOT_2_11.controlsource=this.cTrsName+'.t_IMPTOT'
    this.oPgFRm.Page1.oPag.oIMPTOC_2_16.controlsource=this.cTrsName+'.t_IMPTOC'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(252)
    this.AddVLine(297)
    this.AddVLine(356)
    this.AddVLine(495)
    this.AddVLine(635)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCCOMPON_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.MOV_SCES_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOV_SCES_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.MOV_SCES_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MOV_SCES_IDX,2])
      *
      * insert into MOV_SCES
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'MOV_SCES')
        i_extval=cp_InsertValODBCExtFlds(this,'MOV_SCES')
        i_cFldBody=" "+;
                  "(SCSERIAL,SCCOMPON,SCROWNUM,SCFLSELE,SCROWSAL"+;
                  ",SCDATVEN,SCAGGDAT,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_SCSERIAL)+","+cp_ToStrODBCNull(this.w_SCCOMPON)+","+cp_ToStrODBCNull(this.w_SCROWNUM)+","+cp_ToStrODBC(this.w_SCFLSELE)+","+cp_ToStrODBCNull(this.w_SCROWSAL)+;
             ","+cp_ToStrODBC(this.w_SCDATVEN)+","+cp_ToStrODBC(this.w_SCAGGDAT)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'MOV_SCES')
        i_extval=cp_InsertValVFPExtFlds(this,'MOV_SCES')
        cp_CheckDeletedKey(i_cTable,0,'SCSERIAL',this.w_SCSERIAL,'SCCOMPON',this.w_SCCOMPON,'SCROWNUM',this.w_SCROWNUM)
        INSERT INTO (i_cTable) (;
                   SCSERIAL;
                  ,SCCOMPON;
                  ,SCROWNUM;
                  ,SCFLSELE;
                  ,SCROWSAL;
                  ,SCDATVEN;
                  ,SCAGGDAT;
                  ,CPCCCHK &i_extfld.) VALUES (;
                  this.w_SCSERIAL;
                  ,this.w_SCCOMPON;
                  ,this.w_SCROWNUM;
                  ,this.w_SCFLSELE;
                  ,this.w_SCROWSAL;
                  ,this.w_SCDATVEN;
                  ,this.w_SCAGGDAT;
                  ,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.MOV_SCES_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MOV_SCES_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
          if this.w_SCAGGDAT<>SCAGGDAT
            i_bUpdAll = .t.
          endif
        endif
        if not(i_bUpdAll)
          scan for (t_SCCOMPON<>space(40) AND t_SCROWNUM<>0) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'MOV_SCES')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 " SCAGGDAT="+cp_ToStrODBC(this.w_SCAGGDAT)+;
                 ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and SCCOMPON="+cp_ToStrODBC(&i_TN.->SCCOMPON)+;
                 " and SCROWNUM="+cp_ToStrODBC(&i_TN.->SCROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'MOV_SCES')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                  SCAGGDAT=this.w_SCAGGDAT;
                 ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and SCCOMPON=&i_TN.->SCCOMPON;
                      and SCROWNUM=&i_TN.->SCROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
          if this.w_SCAGGDAT<>SCAGGDAT
            i_bUpdAll = .t.
          endif
        endif
        scan for (t_SCCOMPON<>space(40) AND t_SCROWNUM<>0) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and SCCOMPON="+cp_ToStrODBC(&i_TN.->SCCOMPON)+;
                            " and SCROWNUM="+cp_ToStrODBC(&i_TN.->SCROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and SCCOMPON=&i_TN.->SCCOMPON;
                            and SCROWNUM=&i_TN.->SCROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = 0
              replace SCCOMPON with this.w_SCCOMPON
              replace SCROWNUM with this.w_SCROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update MOV_SCES
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'MOV_SCES')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " SCFLSELE="+cp_ToStrODBC(this.w_SCFLSELE)+;
                     ",SCROWSAL="+cp_ToStrODBCNull(this.w_SCROWSAL)+;
                     ",SCDATVEN="+cp_ToStrODBC(this.w_SCDATVEN)+;
                     ",SCAGGDAT="+cp_ToStrODBC(this.w_SCAGGDAT)+;
                     ",SCCOMPON="+cp_ToStrODBC(this.w_SCCOMPON)+;
                     ",SCROWNUM="+cp_ToStrODBC(this.w_SCROWNUM)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+;
                             " and SCCOMPON="+cp_ToStrODBC(SCCOMPON)+;
                             " and SCROWNUM="+cp_ToStrODBC(SCROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'MOV_SCES')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      SCFLSELE=this.w_SCFLSELE;
                     ,SCROWSAL=this.w_SCROWSAL;
                     ,SCDATVEN=this.w_SCDATVEN;
                     ,SCAGGDAT=this.w_SCAGGDAT;
                     ,SCCOMPON=this.w_SCCOMPON;
                     ,SCROWNUM=this.w_SCROWNUM;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                                      and SCCOMPON=&i_TN.->SCCOMPON;
                                      and SCROWNUM=&i_TN.->SCROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Restore Detail Transaction
  proc mRestoreTrsDetail(i_bCanSkip)
    local i_cWherel,i_cF,i_nConn,i_cTable,i_oRow
    local i_cOp1

    i_cF=this.cTrsName
    i_nConn = i_TableProp[this.COM_PUBI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.COM_PUBI_IDX,2])
    i_oRow=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow
    if i_nConn<>0
      local i_bSkip
      i_bSkip=i_bCanSkip ;
              and NVL(&i_cF..SCAGGDAT,space(1))==this.w_SCAGGDAT;
              and NVL(&i_cF..SCDATVEN,ctod("  /  /  "))==this.w_SCDATVEN;
              and NVL(&i_cF..SCROWSAL,0)==this.w_SCROWSAL;
              and NVL(&i_cF..CODCES,space(20))==this.w_CODCES;
              and NVL(&i_cF..SCCOMPON,space(40))==this.w_SCCOMPON;

      i_cOp1=cp_SetTrsOp(NVL(&i_cF..SCAGGDAT,space(1)),'CUDATVEN','',NVL(&i_cF..SCDATVEN,ctod("  /  /  ")),'restore',i_nConn)
      Local i_cValueForTrsact
      i_cValueForTrsact=NVL(&i_cF..SCROWSAL,0)
      if !i_bSkip and !Empty(i_cValueForTrsact)
        =cp_TrsSQL(i_nConn,"UPDATE "+i_cTable+" SET ";
           +" CUDATVEN="+i_cOp1+","  +"CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+" WHERE CPROWNUM="+cp_ToStrODBC(NVL(&i_cF..SCROWSAL,0));
             +" AND CUCODICE="+cp_ToStrODBC(NVL(&i_cF..CODCES,space(20)));
             +" AND CUCOMPON="+cp_ToStrODBC(NVL(&i_cF..SCCOMPON,space(40)));
             )
      endif
    else
      i_cOp1=cp_SetTrsOp(&i_cF..SCAGGDAT,'CUDATVEN',i_cF+'.SCDATVEN',&i_cF..SCDATVEN,'restore',0)
      i_cWhere = cp_PKFox(i_cTable;
                 ,'CUCODICE',&i_cF..CODCES;
                 ,'CUCOMPON',&i_cF..SCCOMPON;
                 ,'CPROWNUM',&i_cF..SCROWSAL)
      UPDATE (i_cTable) SET ;
           CUDATVEN=&i_cOp1.  ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere
    endif
  endproc

  * --- Update Detail Transaction
  proc mUpdateTrsDetail(i_bCanSkip)
    local i_cWherel,i_cF,i_nModRow,i_nConn,i_cTable,i_oRow
    local i_cOp1

    i_cF=this.cTrsName
    i_nConn = i_TableProp[this.COM_PUBI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.COM_PUBI_IDX,2])
    i_oRow=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow
    if i_nConn<>0
      local i_bSkip
      i_bSkip=i_bCanSkip ;
              and NVL(&i_cF..SCAGGDAT,space(1))==this.w_SCAGGDAT;
              and NVL(&i_cF..SCDATVEN,ctod("  /  /  "))==this.w_SCDATVEN;
              and NVL(&i_cF..SCROWSAL,0)==this.w_SCROWSAL;
              and NVL(&i_cF..CODCES,space(20))==this.w_CODCES;
              and NVL(&i_cF..SCCOMPON,space(40))==this.w_SCCOMPON;

      i_cOp1=cp_SetTrsOp(this.w_SCAGGDAT,'CUDATVEN','this.w_SCDATVEN',this.w_SCDATVEN,'update',i_nConn)
      if !i_bSkip and !Empty(this.w_SCROWSAL)
        i_nModRow=cp_TrsSQL(i_nConn,"UPDATE "+i_cTable+" SET"+;
         +" CUDATVEN="+i_cOp1  +",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+" WHERE CPROWNUM="+cp_ToStrODBC(this.w_SCROWSAL);
           +" AND CUCODICE="+cp_ToStrODBC(this.w_CODCES);
           +" AND CUCOMPON="+cp_ToStrODBC(this.w_SCCOMPON);
           )
      endif
    else
      i_cOp1=cp_SetTrsOp(this.w_SCAGGDAT,'CUDATVEN','this.w_SCDATVEN',this.w_SCDATVEN,'update',0)
      i_cWhere = cp_PKFox(i_cTable;
                 ,'CUCODICE',this.w_CODCES;
                 ,'CUCOMPON',this.w_SCCOMPON;
                 ,'CPROWNUM',this.w_SCROWSAL)
      UPDATE (i_cTable) SET;
           CUDATVEN=&i_cOp1.  ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere
    endif
  endproc

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.MOV_SCES_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MOV_SCES_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (t_SCCOMPON<>space(40) AND t_SCROWNUM<>0) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete MOV_SCES
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and SCCOMPON="+cp_ToStrODBC(&i_TN.->SCCOMPON)+;
                            " and SCROWNUM="+cp_ToStrODBC(&i_TN.->SCROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and SCCOMPON=&i_TN.->SCCOMPON;
                              and SCROWNUM=&i_TN.->SCROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (t_SCCOMPON<>space(40) AND t_SCROWNUM<>0) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.MOV_SCES_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOV_SCES_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
          .w_DATVEN = this.oParentObject .w_MCDATREG
          .w_VALMOV = THIS.oParentObject .w_MCCODVAL
          .link_1_7('Full')
        .DoRTCalc(5,6,.t.)
          .link_2_2('Full')
        .DoRTCalc(8,10,.t.)
        if .o_SCFLSELE<>.w_SCFLSELE
          .w_SCROWSAL = IIF(.w_SCFLSELE='S', .w_SCROWNUM, 0)
          .link_2_6('Full')
        endif
          .w_SCDATVEN = .w_DATVEN
        .oPgFrm.Page1.oPag.oObj_2_12.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_13.Calculate()
        .DoRTCalc(13,17,.t.)
          .w_CALCPIC = DEFPIC(.w_DECTOT1)
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
        .DoRTCalc(19,20,.t.)
        if .o_SCCOMPON<>.w_SCCOMPON
          .w_IMPTOC = cp_ROUND(VALCAM(NVL(.w_IMPTOC,0), .w_VALUTA, .w_VALMOV, .w_DATVEN, 0), .w_DECTOT1)
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(22,23,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_VALUTA with this.w_VALUTA
      replace t_SCROWSAL with this.w_SCROWSAL
      replace t_SCDATVEN with this.w_SCDATVEN
      replace t_DECTOT with this.w_DECTOT
      replace t_CALCPIC with this.w_CALCPIC
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_2_12.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_13.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_2_12.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_13.Calculate()
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oSCROWNUM_2_3.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oSCROWNUM_2_3.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oSCFLSELE_2_5.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oSCFLSELE_2_5.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_2_12.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_2_13.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_11.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=VALMOV
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALMOV) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALMOV)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALMOV);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALMOV)
            select VACODVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALMOV = NVL(_Link_.VACODVAL,space(3))
      this.w_DECTOT1 = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_VALMOV = space(3)
      endif
      this.w_DECTOT1 = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALMOV Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SCCOMPON
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COM_PCES_IDX,3]
    i_lTable = "COM_PCES"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COM_PCES_IDX,2], .t., this.COM_PCES_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COM_PCES_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SCCOMPON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'COM_PCES')
        if i_nConn<>0
          i_cWhere = i_cFlt+" COCOMPON like "+cp_ToStrODBC(trim(this.w_SCCOMPON)+"%");
                   +" and COCODICE="+cp_ToStrODBC(this.w_CODCES);

          i_ret=cp_SQL(i_nConn,"select COCODICE,COCOMPON,COIMPTOT,COVALUTA,COIMPTOC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by COCODICE,COCOMPON","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'COCODICE',this.w_CODCES;
                     ,'COCOMPON',trim(this.w_SCCOMPON))
          select COCODICE,COCOMPON,COIMPTOT,COVALUTA,COIMPTOC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by COCODICE,COCOMPON into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SCCOMPON)==trim(_Link_.COCOMPON) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SCCOMPON) and !this.bDontReportError
            deferred_cp_zoom('COM_PCES','*','COCODICE,COCOMPON',cp_AbsName(oSource.parent,'oSCCOMPON_2_1'),i_cWhere,'',"Componenti cespite",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODCES<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COCODICE,COCOMPON,COIMPTOT,COVALUTA,COIMPTOC";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select COCODICE,COCOMPON,COIMPTOT,COVALUTA,COIMPTOC;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COCODICE,COCOMPON,COIMPTOT,COVALUTA,COIMPTOC";
                     +" from "+i_cTable+" "+i_lTable+" where COCOMPON="+cp_ToStrODBC(oSource.xKey(2));
                     +" and COCODICE="+cp_ToStrODBC(this.w_CODCES);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COCODICE',oSource.xKey(1);
                       ,'COCOMPON',oSource.xKey(2))
            select COCODICE,COCOMPON,COIMPTOT,COVALUTA,COIMPTOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SCCOMPON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COCODICE,COCOMPON,COIMPTOT,COVALUTA,COIMPTOC";
                   +" from "+i_cTable+" "+i_lTable+" where COCOMPON="+cp_ToStrODBC(this.w_SCCOMPON);
                   +" and COCODICE="+cp_ToStrODBC(this.w_CODCES);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COCODICE',this.w_CODCES;
                       ,'COCOMPON',this.w_SCCOMPON)
            select COCODICE,COCOMPON,COIMPTOT,COVALUTA,COIMPTOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SCCOMPON = NVL(_Link_.COCOMPON,space(40))
      this.w_IMPTOT = NVL(_Link_.COIMPTOT,0)
      this.w_VALUTA = NVL(_Link_.COVALUTA,space(3))
      this.w_IMPTOC = NVL(_Link_.COIMPTOC,0)
    else
      if i_cCtrl<>'Load'
        this.w_SCCOMPON = space(40)
      endif
      this.w_IMPTOT = 0
      this.w_VALUTA = space(3)
      this.w_IMPTOC = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COM_PCES_IDX,2])+'\'+cp_ToStr(_Link_.COCODICE,1)+'\'+cp_ToStr(_Link_.COCOMPON,1)
      cp_ShowWarn(i_cKey,this.COM_PCES_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SCCOMPON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VALUTA
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALUTA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALUTA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALUTA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALUTA)
            select VACODVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALUTA = NVL(_Link_.VACODVAL,space(3))
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_VALUTA = space(3)
      endif
      this.w_DECTOT = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALUTA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SCROWNUM
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COM_PUBI_IDX,3]
    i_lTable = "COM_PUBI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COM_PUBI_IDX,2], .t., this.COM_PUBI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COM_PUBI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SCROWNUM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCE_MCU',True,'COM_PUBI')
        if i_nConn<>0
          i_cWhere = " CPROWNUM="+cp_ToStrODBC(this.w_SCROWNUM);
                   +" and CUCODICE="+cp_ToStrODBC(this.w_CODCES);
                   +" and CUCOMPON="+cp_ToStrODBC(this.w_SCCOMPON);

          i_ret=cp_SQL(i_nConn,"select CUCODICE,CUCOMPON,CPROWNUM,CUCODMAT,CUCODUBI,CUQTACOM,CUDATVEN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CUCODICE',this.w_CODCES;
                     ,'CUCOMPON',this.w_SCCOMPON;
                     ,'CPROWNUM',this.w_SCROWNUM)
          select CUCODICE,CUCOMPON,CPROWNUM,CUCODMAT,CUCODUBI,CUQTACOM,CUDATVEN;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_SCROWNUM) and !this.bDontReportError
            deferred_cp_zoom('COM_PUBI','*','CUCODICE,CUCOMPON,CPROWNUM',cp_AbsName(oSource.parent,'oSCROWNUM_2_3'),i_cWhere,'GSCE_MCU',"Componenti cespite",'GSCE2MMS.COM_PUBI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODCES<>oSource.xKey(1);
           .or. this.w_SCCOMPON<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CUCODICE,CUCOMPON,CPROWNUM,CUCODMAT,CUCODUBI,CUQTACOM,CUDATVEN";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select CUCODICE,CUCOMPON,CPROWNUM,CUCODMAT,CUCODUBI,CUQTACOM,CUDATVEN;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CUCODICE,CUCOMPON,CPROWNUM,CUCODMAT,CUCODUBI,CUQTACOM,CUDATVEN";
                     +" from "+i_cTable+" "+i_lTable+" where CPROWNUM="+cp_ToStrODBC(oSource.xKey(3));
                     +" and CUCODICE="+cp_ToStrODBC(this.w_CODCES);
                     +" and CUCOMPON="+cp_ToStrODBC(this.w_SCCOMPON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CUCODICE',oSource.xKey(1);
                       ,'CUCOMPON',oSource.xKey(2);
                       ,'CPROWNUM',oSource.xKey(3))
            select CUCODICE,CUCOMPON,CPROWNUM,CUCODMAT,CUCODUBI,CUQTACOM,CUDATVEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SCROWNUM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CUCODICE,CUCOMPON,CPROWNUM,CUCODMAT,CUCODUBI,CUQTACOM,CUDATVEN";
                   +" from "+i_cTable+" "+i_lTable+" where CPROWNUM="+cp_ToStrODBC(this.w_SCROWNUM);
                   +" and CUCODICE="+cp_ToStrODBC(this.w_CODCES);
                   +" and CUCOMPON="+cp_ToStrODBC(this.w_SCCOMPON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CUCODICE',this.w_CODCES;
                       ,'CUCOMPON',this.w_SCCOMPON;
                       ,'CPROWNUM',this.w_SCROWNUM)
            select CUCODICE,CUCOMPON,CPROWNUM,CUCODMAT,CUCODUBI,CUQTACOM,CUDATVEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SCROWNUM = NVL(_Link_.CPROWNUM,0)
      this.w_CODMAT = NVL(_Link_.CUCODMAT,space(20))
      this.w_CODUBI = NVL(_Link_.CUCODUBI,space(20))
      this.w_QTACOM = NVL(_Link_.CUQTACOM,0)
      this.w_DTVEND = NVL(cp_ToDate(_Link_.CUDATVEN),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_SCROWNUM = 0
      endif
      this.w_CODMAT = space(20)
      this.w_CODUBI = space(20)
      this.w_QTACOM = 0
      this.w_DTVEND = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COM_PUBI_IDX,2])+'\'+cp_ToStr(_Link_.CUCODICE,1)+'\'+cp_ToStr(_Link_.CUCOMPON,1)+'\'+cp_ToStr(_Link_.CPROWNUM,1)
      cp_ShowWarn(i_cKey,this.COM_PUBI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SCROWNUM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SCROWSAL
  func Link_2_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COM_PUBI_IDX,3]
    i_lTable = "COM_PUBI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COM_PUBI_IDX,2], .t., this.COM_PUBI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COM_PUBI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SCROWSAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SCROWSAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CUCODICE,CUCOMPON,CPROWNUM";
                   +" from "+i_cTable+" "+i_lTable+" where CPROWNUM="+cp_ToStrODBC(this.w_SCROWSAL);
                   +" and CUCODICE="+cp_ToStrODBC(this.w_CODCES);
                   +" and CUCOMPON="+cp_ToStrODBC(this.w_SCCOMPON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CUCODICE',this.w_CODCES;
                       ,'CUCOMPON',this.w_SCCOMPON;
                       ,'CPROWNUM',this.w_SCROWSAL)
            select CUCODICE,CUCOMPON,CPROWNUM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SCROWSAL = NVL(_Link_.CPROWNUM,0)
    else
      if i_cCtrl<>'Load'
        this.w_SCROWSAL = 0
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COM_PUBI_IDX,2])+'\'+cp_ToStr(_Link_.CUCODICE,1)+'\'+cp_ToStr(_Link_.CUCOMPON,1)+'\'+cp_ToStr(_Link_.CPROWNUM,1)
      cp_ShowWarn(i_cKey,this.COM_PUBI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SCROWSAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oDTVEND_2_4.value==this.w_DTVEND)
      this.oPgFrm.Page1.oPag.oDTVEND_2_4.value=this.w_DTVEND
      replace t_DTVEND with this.oPgFrm.Page1.oPag.oDTVEND_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPTOT_2_11.value==this.w_IMPTOT)
      this.oPgFrm.Page1.oPag.oIMPTOT_2_11.value=this.w_IMPTOT
      replace t_IMPTOT with this.oPgFrm.Page1.oPag.oIMPTOT_2_11.value
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPSEL_3_1.value==this.w_IMPSEL)
      this.oPgFrm.Page1.oPag.oIMPSEL_3_1.value=this.w_IMPSEL
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEZ1_1_10.RadioValue()==this.w_SELEZ1)
      this.oPgFrm.Page1.oPag.oSELEZ1_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPTOC_2_16.value==this.w_IMPTOC)
      this.oPgFrm.Page1.oPag.oIMPTOC_2_16.value=this.w_IMPTOC
      replace t_IMPTOC with this.oPgFrm.Page1.oPag.oIMPTOC_2_16.value
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPSEC_3_2.value==this.w_IMPSEC)
      this.oPgFrm.Page1.oPag.oIMPSEC_3_2.value=this.w_IMPSEC
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCCOMPON_2_1.value==this.w_SCCOMPON)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCCOMPON_2_1.value=this.w_SCCOMPON
      replace t_SCCOMPON with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCCOMPON_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCROWNUM_2_3.value==this.w_SCROWNUM)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCROWNUM_2_3.value=this.w_SCROWNUM
      replace t_SCROWNUM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCROWNUM_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCFLSELE_2_5.RadioValue()==this.w_SCFLSELE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCFLSELE_2_5.SetRadio()
      replace t_SCFLSELE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCFLSELE_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCODMAT_2_8.value==this.w_CODMAT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCODMAT_2_8.value=this.w_CODMAT
      replace t_CODMAT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCODMAT_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCODUBI_2_9.value==this.w_CODUBI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCODUBI_2_9.value=this.w_CODUBI
      replace t_CODUBI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCODUBI_2_9.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oQTACOM_2_10.value==this.w_QTACOM)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oQTACOM_2_10.value=this.w_QTACOM
      replace t_QTACOM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oQTACOM_2_10.value
    endif
    cp_SetControlsValueExtFlds(this,'MOV_SCES')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .w_SCCOMPON<>space(40) AND .w_SCROWNUM<>0
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_SCCOMPON = this.w_SCCOMPON
    this.o_SCFLSELE = this.w_SCFLSELE
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(t_SCCOMPON<>space(40) AND t_SCROWNUM<>0)
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_SCCOMPON=space(40)
      .w_VALUTA=space(3)
      .w_SCROWNUM=0
      .w_DTVEND=ctod("  /  /  ")
      .w_SCFLSELE=space(1)
      .w_SCROWSAL=0
      .w_SCDATVEN=ctod("  /  /  ")
      .w_CODMAT=space(20)
      .w_CODUBI=space(20)
      .w_QTACOM=0
      .w_IMPTOT=0
      .w_DECTOT=0
      .w_CALCPIC=0
      .w_IMPTOC=0
      .DoRTCalc(1,6,.f.)
      if not(empty(.w_SCCOMPON))
        .link_2_1('Full')
      endif
      .DoRTCalc(7,7,.f.)
      if not(empty(.w_VALUTA))
        .link_2_2('Full')
      endif
      .DoRTCalc(8,8,.f.)
      if not(empty(.w_SCROWNUM))
        .link_2_3('Full')
      endif
      .DoRTCalc(9,10,.f.)
        .w_SCROWSAL = IIF(.w_SCFLSELE='S', .w_SCROWNUM, 0)
      .DoRTCalc(11,11,.f.)
      if not(empty(.w_SCROWSAL))
        .link_2_6('Full')
      endif
        .w_SCDATVEN = .w_DATVEN
        .oPgFrm.Page1.oPag.oObj_2_12.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_13.Calculate()
      .DoRTCalc(13,17,.f.)
        .w_CALCPIC = DEFPIC(.w_DECTOT1)
      .DoRTCalc(19,20,.f.)
        .w_IMPTOC = cp_ROUND(VALCAM(NVL(.w_IMPTOC,0), .w_VALUTA, .w_VALMOV, .w_DATVEN, 0), .w_DECTOT1)
    endwith
    this.DoRTCalc(22,23,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_SCCOMPON = t_SCCOMPON
    this.w_VALUTA = t_VALUTA
    this.w_SCROWNUM = t_SCROWNUM
    this.w_DTVEND = t_DTVEND
    this.w_SCFLSELE = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCFLSELE_2_5.RadioValue(.t.)
    this.w_SCROWSAL = t_SCROWSAL
    this.w_SCDATVEN = t_SCDATVEN
    this.w_CODMAT = t_CODMAT
    this.w_CODUBI = t_CODUBI
    this.w_QTACOM = t_QTACOM
    this.w_IMPTOT = t_IMPTOT
    this.w_DECTOT = t_DECTOT
    this.w_CALCPIC = t_CALCPIC
    this.w_IMPTOC = t_IMPTOC
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_SCCOMPON with this.w_SCCOMPON
    replace t_VALUTA with this.w_VALUTA
    replace t_SCROWNUM with this.w_SCROWNUM
    replace t_DTVEND with this.w_DTVEND
    replace t_SCFLSELE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCFLSELE_2_5.ToRadio()
    replace t_SCROWSAL with this.w_SCROWSAL
    replace t_SCDATVEN with this.w_SCDATVEN
    replace t_CODMAT with this.w_CODMAT
    replace t_CODUBI with this.w_CODUBI
    replace t_QTACOM with this.w_QTACOM
    replace t_IMPTOT with this.w_IMPTOT
    replace t_DECTOT with this.w_DECTOT
    replace t_CALCPIC with this.w_CALCPIC
    replace t_IMPTOC with this.w_IMPTOC
    if i_srv='A'
      replace SCCOMPON with this.w_SCCOMPON
      replace SCROWNUM with this.w_SCROWNUM
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsce_mmsPag1 as StdContainer
  Width  = 697
  height = 353
  stdWidth  = 697
  stdheight = 353
  resizeXpos=221
  resizeYpos=158
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oBtn_1_9 as StdButton with uid="VZCKGQXTUS",left=156, top=224, width=48,height=45,;
    CpPicture="BMP\CARICA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per caricare il dettaglio dei componenti";
    , HelpContextID = 198590502;
    , caption='\<Carica';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      with this.Parent.oContained
        do GSCE_BD2 with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oSELEZ1_1_10 as StdRadio with uid="RMUJOFEZEL",rtseq=20,rtrep=.f.,left=9, top=224, width=140,height=32;
    , tabstop=.f.;
    , ToolTipText = "Seleziona/deseleziona i componenti";
    , cFormVar="w_SELEZ1", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZ1_1_10.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutti"
      this.Buttons(1).HelpContextID = 81803994
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutti"
      this.Buttons(2).HelpContextID = 81803994
      this.Buttons(2).Top=15
      this.SetAll("Width",138)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Seleziona/deseleziona i componenti")
      StdRadio::init()
    endproc

  func oSELEZ1_1_10.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..SELEZ1,&i_cF..t_SELEZ1),this.value)
    return(iif(xVal =1,'S',;
    iif(xVal =2,'D',;
    space(1))))
  endfunc
  func oSELEZ1_1_10.GetRadio()
    this.Parent.oContained.w_SELEZ1 = this.RadioValue()
    return .t.
  endfunc

  func oSELEZ1_1_10.ToRadio()
    this.Parent.oContained.w_SELEZ1=trim(this.Parent.oContained.w_SELEZ1)
    return(;
      iif(this.Parent.oContained.w_SELEZ1=='S',1,;
      iif(this.Parent.oContained.w_SELEZ1=='D',2,;
      0)))
  endfunc

  func oSELEZ1_1_10.SetRadio()
    this.value=this.ToRadio()
  endfunc


  add object oObj_1_11 as cp_runprogram with uid="TCAEVXSURF",left=823, top=199, width=183,height=23,;
    caption='GSCE_BD3(SEL)',;
   bGlobalFont=.t.,;
    prg="GSCE_BD3('SEL')",;
    cEvent = "w_SELEZ1 Changed",;
    nPag=1;
    , HelpContextID = 256895001


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=2, top=3, width=675,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=6,Field1="SCCOMPON",Label1="Componente",Field2="SCROWNUM",Label2="N.rif.",Field3="QTACOM",Label3="Qta",Field4="CODMAT",Label4="Matricola",Field5="CODUBI",Label5="Ubicazione",Field6="SCFLSELE",Label6="Sel.",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 118285434

  add object oStr_1_4 as StdString with uid="GZPQALDRUT",Visible=.t., Left=372, Top=286,;
    Alignment=1, Width=184, Height=15,;
    Caption="Valore componenti selezionati:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="EJIXUCNMIH",Visible=.t., Left=26, Top=284,;
    Alignment=1, Width=201, Height=15,;
    Caption="Valore totale del componente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="PJDSVDXURZ",Visible=.t., Left=387, Top=226,;
    Alignment=1, Width=156, Height=15,;
    Caption="Venduto/dismesso il:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="JGYBTRNFID",Visible=.t., Left=6, Top=263,;
    Alignment=0, Width=132, Height=15,;
    Caption="Aspetto fiscale"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="OQQXAEUYTG",Visible=.t., Left=372, Top=321,;
    Alignment=1, Width=184, Height=15,;
    Caption="Valore componenti selezionati:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="YUAOFMNCHZ",Visible=.t., Left=26, Top=321,;
    Alignment=1, Width=201, Height=15,;
    Caption="Valore totale del componente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="AQGWMQHDKL",Visible=.t., Left=6, Top=303,;
    Alignment=0, Width=141, Height=15,;
    Caption="Aspetto civile"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-8,top=23,;
    width=671+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-7,top=24,width=670+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='COM_PCES|COM_PUBI|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oDTVEND_2_4.Refresh()
      this.Parent.oIMPTOT_2_11.Refresh()
      this.Parent.oIMPTOC_2_16.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='COM_PCES'
        oDropInto=this.oBodyCol.oRow.oSCCOMPON_2_1
      case cFile='COM_PUBI'
        oDropInto=this.oBodyCol.oRow.oSCROWNUM_2_3
    endcase
    return(oDropInto)
  EndFunc


  add object oDTVEND_2_4 as StdTrsField with uid="FSRWPQVSKD",rtseq=9,rtrep=.t.,;
    cFormVar="w_DTVEND",value=ctod("  /  /  "),enabled=.f.,;
    ToolTipText = "Data di vendita/dismissione",;
    HelpContextID = 44010698,;
    cTotal="", bFixedPos=.t., cQueryName = "DTVEND",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=548, Top=224

  add object oIMPTOT_2_11 as StdTrsField with uid="HATOARLGNV",rtseq=16,rtrep=.t.,;
    cFormVar="w_IMPTOT",value=0,enabled=.f.,;
    ToolTipText = "Valore fiscale totale del bene",;
    HelpContextID = 42005370,;
    cTotal="", bFixedPos=.t., cQueryName = "IMPTOT",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=233, Top=282, cSayPict=[v_PV(38+VVL)], cGetPict=[v_PV(38+VVL)]

  add object oObj_2_12 as cp_runprogram with uid="ROIIBGEIMZ",width=183,height=23,;
   left=823, top=147,;
    caption='GSCE_BD3(CAL)',;
   bGlobalFont=.t.,;
    prg="GSCE_BD3('CAL')",;
    cEvent = "w_SCFLSELE Changed",;
    nPag=2;
    , HelpContextID = 256874521

  add object oObj_2_13 as cp_runprogram with uid="WUNUAVXOJQ",width=183,height=23,;
   left=823, top=173,;
    caption='GSCE_BMC(MMS)',;
   bGlobalFont=.t.,;
    prg='GSCE_BMC("MMS")',;
    cEvent = "w_SCFLSELE Changed",;
    nPag=2;
    , HelpContextID = 11050455

  add object oIMPTOC_2_16 as StdTrsField with uid="IFMLMBBOXN",rtseq=21,rtrep=.t.,;
    cFormVar="w_IMPTOC",value=0,enabled=.f.,;
    ToolTipText = "Valore civile totale del bene",;
    HelpContextID = 58782586,;
    cTotal="", bFixedPos=.t., cQueryName = "IMPTOC",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=233, Top=315, cSayPict=[v_PV(38+VVL)], cGetPict=[v_PV(38+VVL)]

  add object oBeginFooter as BodyKeyMover with nDirection=-1

  add object oIMPSEL_3_1 as StdField with uid="CHBPOMPARR",rtseq=19,rtrep=.f.,;
    cFormVar="w_IMPSEL",value=0,enabled=.f.,;
    ToolTipText = "Valore fiscale componenti selezionati",;
    HelpContextID = 186774394,;
    cQueryName = "IMPSEL",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=563, Top=282, cSayPict=[v_PV(38+VVL)], cGetPict=[v_PV(38+VVL)]

  add object oIMPSEC_3_2 as StdField with uid="VXKWIYMHGR",rtseq=22,rtrep=.f.,;
    cFormVar="w_IMPSEC",value=0,enabled=.f.,;
    ToolTipText = "Valore civile componenti selezionati",;
    HelpContextID = 69333882,;
    cQueryName = "IMPSEC",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=563, Top=315, cSayPict=[v_PV(38+VVL)], cGetPict=[v_PV(38+VVL)]
enddefine

* --- Defining Body row
define class tgsce_mmsBodyRow as CPBodyRowCnt
  Width=661
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oSCCOMPON_2_1 as StdTrsField with uid="STDYUSPFTY",rtseq=6,rtrep=.t.,;
    cFormVar="w_SCCOMPON",value=space(40),isprimarykey=.t.,;
    HelpContextID = 111594636,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=244, Left=-2, Top=0, InputMask=replicate('X',40), bHasZoom = .t. , cLinkFile="COM_PCES", oKey_1_1="COCODICE", oKey_1_2="this.w_CODCES", oKey_2_1="COCOMPON", oKey_2_2="this.w_SCCOMPON"

  func oSCCOMPON_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
      if .not. empty(.w_SCROWNUM)
        bRes2=.link_2_3('Full')
      endif
      if .not. empty(.w_SCROWSAL)
        bRes2=.link_2_6('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oSCCOMPON_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oSCCOMPON_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    if not(this.parent.oSCCOMPON_2_1.readonly and this.parent.oSCCOMPON_2_1.isprimarykey)
    if i_TableProp[this.parent.oContained.COM_PCES_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"COCODICE="+cp_ToStrODBC(this.Parent.oContained.w_CODCES)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"COCODICE="+cp_ToStr(this.Parent.oContained.w_CODCES)
    endif
    do cp_zoom with 'COM_PCES','*','COCODICE,COCOMPON',cp_AbsName(this.parent,'oSCCOMPON_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Componenti cespite",'',this.parent.oContained
   endif
  endproc

  add object oSCROWNUM_2_3 as StdTrsField with uid="ZROKQSUWZE",rtseq=8,rtrep=.t.,;
    cFormVar="w_SCROWNUM",value=0,isprimarykey=.t.,;
    HelpContextID = 133833587,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=32, Left=255, Top=0, cSayPict=["999"], cGetPict=["999"], bHasZoom = .t. , cLinkFile="COM_PUBI", cZoomOnZoom="GSCE_MCU", oKey_1_1="CUCODICE", oKey_1_2="this.w_CODCES", oKey_2_1="CUCOMPON", oKey_2_2="this.w_SCCOMPON", oKey_3_1="CPROWNUM", oKey_3_2="this.w_SCROWNUM"

  func oSCROWNUM_2_3.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_SCCOMPON))
    endwith
  endfunc

  func oSCROWNUM_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oSCROWNUM_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oSCROWNUM_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    if not(this.parent.oSCROWNUM_2_3.readonly and this.parent.oSCROWNUM_2_3.isprimarykey)
    if i_TableProp[this.parent.oContained.COM_PUBI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CUCODICE="+cp_ToStrODBC(this.Parent.oContained.w_CODCES)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CUCOMPON="+cp_ToStrODBC(this.Parent.oContained.w_SCCOMPON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CUCODICE="+cp_ToStr(this.Parent.oContained.w_CODCES)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CUCOMPON="+cp_ToStr(this.Parent.oContained.w_SCCOMPON)
    endif
    do cp_zoom with 'COM_PUBI','*','CUCODICE,CUCOMPON,CPROWNUM',cp_AbsName(this.parent,'oSCROWNUM_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCE_MCU',"Componenti cespite",'GSCE2MMS.COM_PUBI_VZM',this.parent.oContained
   endif
  endproc
  proc oSCROWNUM_2_3.mZoomOnZoom
    local i_obj
    i_obj=GSCE_MCU()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.CUCODICE=w_CODCES
    i_obj.CUCOMPON=w_SCCOMPON
     i_obj.w_CPROWNUM=this.parent.oContained.w_SCROWNUM
    i_obj.ecpSave()
  endproc

  add object oSCFLSELE_2_5 as StdTrsCheck with uid="PHNPEUMOWU",rtrep=.t.,;
    cFormVar="w_SCFLSELE",  caption="",;
    HelpContextID = 21601429,;
    Left=634, Top=0, Width=22,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , Autosize=.F.;
   , bGlobalFont=.t.


  func oSCFLSELE_2_5.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..SCFLSELE,&i_cF..t_SCFLSELE),this.value)
    return(iif(xVal =1,'S',;
    ' '))
  endfunc
  func oSCFLSELE_2_5.GetRadio()
    this.Parent.oContained.w_SCFLSELE = this.RadioValue()
    return .t.
  endfunc

  func oSCFLSELE_2_5.ToRadio()
    this.Parent.oContained.w_SCFLSELE=trim(this.Parent.oContained.w_SCFLSELE)
    return(;
      iif(this.Parent.oContained.w_SCFLSELE=='S',1,;
      0))
  endfunc

  func oSCFLSELE_2_5.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oSCFLSELE_2_5.mCond()
    with this.Parent.oContained
      return (EMPTY(.w_DTVEND))
    endwith
  endfunc

  add object oCODMAT_2_8 as StdTrsField with uid="DMBZCWMJGD",rtseq=13,rtrep=.t.,;
    cFormVar="w_CODMAT",value=space(20),enabled=.f.,;
    HelpContextID = 57192922,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=135, Left=354, Top=0, InputMask=replicate('X',20)

  add object oCODUBI_2_9 as StdTrsField with uid="PRYPEIIWBC",rtseq=14,rtrep=.t.,;
    cFormVar="w_CODUBI",value=space(20),enabled=.f.,;
    HelpContextID = 240169434,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=135, Left=493, Top=0, InputMask=replicate('X',20)

  add object oQTACOM_2_10 as StdTrsField with uid="EEAQJKLTQU",rtseq=15,rtrep=.t.,;
    cFormVar="w_QTACOM",value=0,enabled=.f.,;
    HelpContextID = 160619514,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=51, Left=300, Top=0, cSayPict=[v_PQ(7)], cGetPict=[v_PQ(7)]
  add object oLast as LastKeyMover
  * ---
  func oSCCOMPON_2_1.When()
    return(.t.)
  proc oSCCOMPON_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oSCCOMPON_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsce_mms','MOV_SCES','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".SCSERIAL=MOV_SCES.SCSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
