* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_bvc                                                        *
*              Visualizzazione schede cespiti                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_24]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-03-24                                                      *
* Last revis.: 2000-03-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsce_bvc",oParentObject)
return(i_retval)

define class tgsce_bvc as StdBatch
  * --- Local variables
  w_PADRE = .NULL.
  w_TMPD = ctod("  /  /  ")
  W_TVALBEN = 0
  W_TVALBEN_C = 0
  w_ZOOM = space(10)
  w_ZOOMC = space(10)
  W_DATSALIN = ctod("  /  /  ")
  W_TFISCAL1 = 0
  W_TFISCAL = 0
  W_TIMPNAM = 0
  W_TPLUMIN = 0
  W_TPLUMIN_C = 0
  W_TPLUMIN1_C = 0
  W_TPLUMIN1 = 0
  W_TCIVILE = 0
  W_TIMPNAM1 = 0
  W_TVALBEN1 = 0
  W_TVALBEN1_C = 0
  W_TCIVILE1 = 0
  * --- WorkFile variables
  AZIENDA_idx=0
  SALDIART_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- LANCIA AL NOTIFYEVENT (da GSCE_SZC)
    * --- Variabili locali
    this.w_PADRE = This.oParentObject
    this.w_ZOOM = this.w_PADRE.w_ZoomCesF
    this.w_ZOOMC = this.w_PADRE.w_ZoomCesC
    if EMPTY(this.oParentObject.w_CODCES)
      ah_ErrorMsg("Inserire il codice cespite")
      i_retcode = 'stop'
      return
    endif
    this.w_PADRE.NotifyEvent("EseguiF")     
    * --- Aggiorna i valori in funzione della valuta di visualizzazione
    UPDATE ( this.w_ZOOM.cCursor ) SET ;
    MCIMPA01 = cp_ROUND(VALCAM(NVL(MCIMPA01,0), MCCODVAL, this.oParentObject.W_VALNAZ, this.oParentObject.w_DATFIN, 0), this.oParentObject.W_DECTOT), ;
    MCVALBEN = cp_ROUND(VALCAM(NVL(MCVALBEN,0), MCCODVAL, this.oParentObject.W_VALNAZ, this.oParentObject.w_DATFIN, 0), this.oParentObject.W_DECTOT), ;
    MCFISCAL = cp_ROUND(VALCAM(NVL(MCFISCAL,0), MCCODVAL, this.oParentObject.W_VALNAZ, this.oParentObject.w_DATFIN, 0), this.oParentObject.W_DECTOT), ;
    MCIMPNAM = cp_ROUND(VALCAM(NVL(MCIMPNAM,0), MCCODVAL, this.oParentObject.W_VALNAZ, this.oParentObject.w_DATFIN, 0), this.oParentObject.W_DECTOT), ;
    MCPLUMIN = cp_ROUND(VALCAM(NVL(MCPLUMIN,0), MCCODVAL, this.oParentObject.W_VALNAZ, this.oParentObject.w_DATFIN, 0), this.oParentObject.W_DECTOT);
    WHERE MCCODVAL<>this.oParentObject.W_VALNAZ
    this.w_PADRE.NotifyEvent("EseguiC")     
    * --- Aggiorna i valori in funzione della valuta di visualizzazione
    UPDATE ( this.w_ZOOMC.cCursor ) SET ;
    MCIMPA01 = cp_ROUND(VALCAM(NVL(MCIMPA01,0), MCCODVAL, this.oParentObject.W_VALNAZ, this.oParentObject.w_DATFIN, 0), this.oParentObject.W_DECTOT), ;
    MCVALBENC = cp_ROUND(VALCAM(NVL(MCVALBENC,0), MCCODVAL, this.oParentObject.W_VALNAZ, this.oParentObject.w_DATFIN, 0), this.oParentObject.W_DECTOT), ;
    MCCIVILE = cp_ROUND(VALCAM(NVL(MCCIVILE,0), MCCODVAL, this.oParentObject.W_VALNAZ, this.oParentObject.w_DATFIN, 0), this.oParentObject.W_DECTOT), ;
    MCPLUMINC = cp_ROUND(VALCAM(NVL(MCPLUMINC,0), MCCODVAL, this.oParentObject.W_VALNAZ, this.oParentObject.w_DATFIN, 0), this.oParentObject.W_DECTOT);
    WHERE MCCODVAL<>this.oParentObject.W_VALNAZ
    * --- Refresh Zoom
    SELECT ( this.w_ZOOM.cCursor )
    GO TOP
    this.w_ZOOM.grd.readonly = .T.
    SELECT ( this.w_ZOOMC.cCursor )
    GO TOP
    this.w_ZOOMC.grd.readonly = .T.
    this.W_DATSALIN = (this.oParentObject.W_DATINI-1)
    * --- Inizializzo le variabili che mi servono per il calcolo dei saldi
    this.W_TVALBEN = 0
    this.W_TVALBEN_C = 0
    this.W_TFISCAL = 0
    this.W_DATSALIN = (this.oParentObject.W_DATINI-1)
    this.W_TVALBEN1 = 0
    this.W_TVALBEN1_C = 0
    this.W_TIMPNAM = 0
    this.W_TCIVILE = 0
    this.W_TIMPNAM1 = 0
    this.W_TPLUMIN = 0
    this.W_TPLUMIN_C = 0
    this.W_TCIVILE1 = 0
    this.W_TPLUMIN1 = 0
    this.W_TPLUMIN1_C = 0
    this.W_TFISCAL1 = 0
    * --- Calcolo i saldi iniziali
    vq_exec("..\CESP\EXE\QUERY\GSCE1SZS.VQR",this,"CESPITI")
    if MCCODVAL<>this.oParentObject.w_VALNAZ
      this.w_TMPD = this.w_DATSALIN
      this.W_TVALBEN1 = this.W_TVALBEN1 + cp_ROUND(VALCAM(NVL(CESPITI.MCVALBEN,0), CESPITI.MCCODVAL, this.oParentObject.w_VALNAZ, this.w_TMPD, 0), this.oParentObject.w_DECTOT)
      this.W_TCIVILE1 = this.W_TCIVILE1+ cp_ROUND(VALCAM(NVL(CESPITI.MCCIVILE,0), CESPITI.MCCODVAL, this.oParentObject.w_VALNAZ, this.w_TMPD, 0), this.oParentObject.w_DECTOT)
      this.W_TFISCAL1 = this.W_TFISCAL1 + cp_ROUND(VALCAM(NVL(CESPITI.MCFISCAL,0), CESPITI.MCCODVAL, this.oParentObject.w_VALNAZ, this.w_TMPD, 0), this.oParentObject.w_DECTOT)
      this.W_TIMPNAM1 = this.W_TIMPNAM1 + cp_ROUND(VALCAM(NVL(CESPITI.MCIMPNAM,0), CESPITI.MCCODVAL, this.oParentObject.w_VALNAZ, this.w_TMPD, 0), this.oParentObject.w_DECTOT)
      this.W_TPLUMIN1 = this.W_TPLUMIN1 + cp_ROUND(VALCAM(NVL(CESPITI.MCPLUMIN,0), CESPITI.MCCODVAL, this.oParentObject.w_VALNAZ, this.w_TMPD, 0), this.oParentObject.w_DECTOT)
      this.W_TVALBEN1_C = this.W_TVALBEN1_C + cp_ROUND(VALCAM(NVL(CESPITI.VALBENC,0), CESPITI.MCCODVAL, this.oParentObject.w_VALNAZ, this.w_TMPD, 0), this.oParentObject.w_DECTOT)
      this.W_TPLUMIN1_C = this.W_TPLUMIN1_C + cp_ROUND(VALCAM(NVL(CESPITI.MCPLUMINC,0), CESPITI.MCCODVAL, this.oParentObject.w_VALNAZ, this.w_TMPD, 0), this.oParentObject.w_DECTOT)
    else
      * --- Valuta uguale a quella di visualizzazione
      this.W_TVALBEN1 = this.W_TVALBEN1 + NVL(CESPITI.MCVALBEN,0)
      this.W_TVALBEN1_C = this.W_TVALBEN1_C + NVL(CESPITI.VALBENC,0)
      this.W_TCIVILE1 = this.W_TCIVILE1+ NVL(CESPITI.MCCIVILE,0)
      this.W_TFISCAL1 = this.W_TFISCAL1 + NVL(CESPITI.MCFISCAL,0)
      this.W_TIMPNAM1 = this.W_TIMPNAM1 + NVL(CESPITI.MCIMPNAM,0)
      this.W_TPLUMIN1 = this.W_TPLUMIN1 + NVL(CESPITI.MCPLUMIN,0)
      this.W_TPLUMIN1_C = this.W_TPLUMIN1_C + NVL(CESPITI.MCPLUMINC,0)
    endif
    * --- Riporto i saldi iniziali nella maschera di visualizzazione
    *     Controllo se l'aspetto � di tipo civile o fiscale
    * --- Saldi iniziali dell'aspetto Civile
    this.oParentObject.w_VBESAL1C = this.W_TVALBEN1_C
    this.oParentObject.w_ACUSAL1C = this.W_TCIVILE1
    this.oParentObject.w_PMISAL1C = this.W_TPLUMIN1_C
    * --- Saldi iniziali dell'aspetto Fiscale
    this.oParentObject.w_VBESAL1 = this.W_TVALBEN1
    this.oParentObject.w_ACUSAL1 = this.W_TFISCAL1
    this.oParentObject.w_PMISAL1 = this.W_TIMPNAM1
    * --- Calcola i saldi alla data di fine selezione
    * --- Select from GSCE_SZS
    do vq_exec with 'GSCE_SZS',this,'_Curs_GSCE_SZS','',.f.,.t.
    if used('_Curs_GSCE_SZS')
      select _Curs_GSCE_SZS
      locate for 1=1
      do while not(eof())
      if _Curs_GSCE_SZS.MCCODVAL <> this.oParentObject.w_VALNAZ
        * --- Valuta diversa da quella di visualizzazione
        this.w_TMPD = this.oParentObject.w_DATFIN
        this.W_TVALBEN = this.W_TVALBEN + cp_ROUND(VALCAM(NVL(_Curs_GSCE_SZS.MCVALBEN,0), _Curs_GSCE_SZS.MCCODVAL, this.oParentObject.w_VALNAZ, this.w_TMPD, 0), this.oParentObject.w_DECTOT)
        this.W_TVALBEN_C = this.W_TVALBEN_C + cp_ROUND(VALCAM(NVL(_Curs_GSCE_SZS.MCVALBENC,0), _Curs_GSCE_SZS.MCCODVAL, this.oParentObject.w_VALNAZ, this.w_TMPD, 0), this.oParentObject.w_DECTOT)
        this.W_TCIVILE = this.W_TCIVILE+ cp_ROUND(VALCAM(NVL(_Curs_GSCE_SZS.MCCIVILE,0), _Curs_GSCE_SZS.MCCODVAL, this.oParentObject.w_VALNAZ, this.w_TMPD, 0), this.oParentObject.w_DECTOT)
        this.W_TFISCAL = this.W_TFISCAL + cp_ROUND(VALCAM(NVL(_Curs_GSCE_SZS.MCFISCAL,0), _Curs_GSCE_SZS.MCCODVAL, this.oParentObject.w_VALNAZ, this.w_TMPD, 0), this.oParentObject.w_DECTOT)
        this.W_TIMPNAM = this.W_TIMPNAM + cp_ROUND(VALCAM(NVL(_Curs_GSCE_SZS.MCIMPNAM,0), _Curs_GSCE_SZS.MCCODVAL, this.oParentObject.w_VALNAZ, this.w_TMPD, 0), this.oParentObject.w_DECTOT)
        this.W_TPLUMIN = this.W_TPLUMIN + cp_ROUND(VALCAM(NVL(_Curs_GSCE_SZS.MCPLUMIN,0), _Curs_GSCE_SZS.MCCODVAL, this.oParentObject.w_VALNAZ, this.w_TMPD, 0), this.oParentObject.w_DECTOT)
        this.W_TPLUMIN_C = this.W_TPLUMIN_C + cp_ROUND(VALCAM(NVL(_Curs_GSCE_SZS.MCPLUMINC,0), _Curs_GSCE_SZS.MCCODVAL, this.oParentObject.w_VALNAZ, this.w_TMPD, 0), this.oParentObject.w_DECTOT)
      else
        * --- Valuta uguale a quella di visualizzazione
        this.W_TVALBEN = this.W_TVALBEN + NVL(_Curs_GSCE_SZS.MCVALBEN,0)
        this.W_TVALBEN_C = this.W_TVALBEN_C + NVL(_Curs_GSCE_SZS.MCVALBENC,0)
        this.W_TCIVILE = this.W_TCIVILE+ NVL(_Curs_GSCE_SZS.MCCIVILE,0)
        this.W_TFISCAL = this.W_TFISCAL + NVL(_Curs_GSCE_SZS.MCFISCAL,0)
        this.W_TIMPNAM = this.W_TIMPNAM + NVL(_Curs_GSCE_SZS.MCIMPNAM,0)
        this.W_TPLUMIN = this.W_TPLUMIN + NVL(_Curs_GSCE_SZS.MCPLUMIN,0)
        this.W_TPLUMIN_C = this.W_TPLUMIN_C + NVL(_Curs_GSCE_SZS.MCPLUMINC,0)
      endif
        select _Curs_GSCE_SZS
        continue
      enddo
      use
    endif
    * --- Saldi finali dell'aspetto Civile
    this.oParentObject.w_VBESALC = this.W_TVALBEN_C
    this.oParentObject.w_ACUSALC = this.W_TCIVILE
    this.oParentObject.w_PMISALC = this.W_TPLUMIN_C
    * --- Saldi finali dell'aspetto Fiscale
    this.oParentObject.w_VBESAL = this.W_TVALBEN
    this.oParentObject.w_ACUSAL = this.W_TFISCAL
    this.oParentObject.w_PMISAL = this.W_TIMPNAM
    * --- Riarrotondo i valori 
    this.W_TVALBEN1 = cp_ROUND(this.W_TVALBEN1,this.oParentObject.w_DECTOT)
    this.W_TCIVILE1 = cp_ROUND(this.W_TCIVILE1,this.oParentObject.w_DECTOT)
    this.W_TFISCAL1 = cp_ROUND(this.W_TFISCAL1,this.oParentObject.w_DECTOT) 
    this.W_TIMPNAM1 = cp_ROUND(this.W_TIMPNAM1,this.oParentObject.w_DECTOT)
    this.W_TPLUMIN1 = cp_ROUND(this.W_TPLUMIN1,this.oParentObject.w_DECTOT)
    this.W_TVALBEN = cp_ROUND(this.W_TVALBEN,this.oParentObject.w_DECTOT)
    this.W_TCIVILE = cp_ROUND(this.W_TCIVILE,this.oParentObject.w_DECTOT)
    this.W_TFISCAL = cp_ROUND(this.W_TFISCAL,this.oParentObject.w_DECTOT)
    this.W_TIMPNAM = cp_ROUND(this.W_TIMPNAM,this.oParentObject.w_DECTOT)
    this.W_TPLUMIN = cp_ROUND(this.W_TPLUMIN ,this.oParentObject.w_DECTOT)
    this.oParentObject.w_VBESAL = cp_ROUND(this.oParentObject.W_VBESAL,this.oParentObject.w_DECTOT)
    this.oParentObject.w_ACUSAL = cp_ROUND(this.oParentObject.w_ACUSAL,this.oParentObject.w_DECTOT)
    this.oParentObject.w_PMISAL = cp_ROUND(this.oParentObject.w_PMISAL,this.oParentObject.w_DECTOT)
    this.oParentObject.w_VBESAL1 = cp_ROUND(this.oParentObject.W_VBESAL1,this.oParentObject.w_DECTOT)
    this.oParentObject.w_ACUSAL1 = cp_ROUND(this.oParentObject.w_ACUSAL1,this.oParentObject.w_DECTOT)
    this.oParentObject.w_PMISAL1 = cp_ROUND(this.oParentObject.w_PMISAL1,this.oParentObject.w_DECTOT)
    this.W_TVALBEN_C = cp_ROUND(this.W_TVALBEN_C,this.oParentObject.w_DECTOT)
    this.W_TPLUMIN_C = cp_ROUND(this.W_TPLUMIN_C,this.oParentObject.w_DECTOT)
    * --- Chiudo il cursore utilizzato
    if used("cespiti")
      select ("cespiti")
      use
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='SALDIART'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_GSCE_SZS')
      use in _Curs_GSCE_SZS
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
