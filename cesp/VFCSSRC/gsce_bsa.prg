* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_bsa                                                        *
*              Controllo flag non soggetto ad ammortamento                     *
*                                                                              *
*      Author: Zucchetti spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][207]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-05-11                                                      *
* Last revis.: 2008-02-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsce_bsa",oParentObject,m.pOper)
return(i_retval)

define class tgsce_bsa as StdBatch
  * --- Local variables
  pOper = space(1)
  w_MESS = space(10)
  w_NUMERO = 0
  w_CESPITE = space(20)
  w_CFUNC = space(10)
  w_OK = .f.
  * --- WorkFile variables
  CES_PITI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questo batch serve per poter controllare se l'utente ha inserito il flag
    *     "Non soggetto ad Ammortamento"
    this.w_CFUNC = this.oParentObject.cFunction
    this.w_OK = .F.
    do case
      case this.pOper="A"
        if this.w_CFUNC="Edit"
          * --- Verifico se la categoria che si intende modificare non si� gi� stata movimentata.
          vq_exec("..\CESP\EXE\QUERY\GSCE_BSA.VQR",this,"CONTROLLO")
          Select Controllo
          Go Top
          this.w_NUMERO = Controllo.Numero
          if this.w_NUMERO>0
            this.w_MESS = "Impossibile variare la categoria cespite%0Sono gi� stati inseriti movimenti riferiti alla categoria"
            ah_ERRORMSG(this.w_MESS,48)
            this.oParentObject.w_CCNSOAMM = this.oParentObject.w_OLD_SOAMM
            this.oParentObject.w_CCNSOAMF = this.oParentObject.w_OLD_SOAMF
          else
            * --- Se la causale non � stata ancora movimentata e sono associati cespiti,
            *     avverto l'utente che i cespiti non verranno considerati nell'accantonamento.
            * --- Select from CES_PITI
            i_nConn=i_TableProp[this.CES_PITI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CES_PITI_idx,2],.t.,this.CES_PITI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select CECODICE  from "+i_cTable+" CES_PITI ";
                  +" where CECODCAT="+cp_ToStrODBC(this.oParentObject.w_CCCODICE)+"";
                   ,"_Curs_CES_PITI")
            else
              select CECODICE from (i_cTable);
               where CECODCAT=this.oParentObject.w_CCCODICE;
                into cursor _Curs_CES_PITI
            endif
            if used('_Curs_CES_PITI')
              select _Curs_CES_PITI
              locate for 1=1
              do while not(eof())
              this.w_OK = .T.
                select _Curs_CES_PITI
                continue
              enddo
              use
            endif
            if this.w_OK
              this.w_MESS = "Attenzione: i cespiti associati alla categoria non verranno aggiornati"
              ah_ERRORMSG(this.w_MESS,48)
            endif
          endif
          if USED("CONTROLLO")
            SELECT CONTROLLO
            USE
          endif
        endif
      case this.pOper="R"
        SELECT * FROM (THIS.OPARENTOBJECT.GSCE_MEA.CTRSNAME) INTO CURSOR CATE WHERE NOT EMPTY(NVL(t_EAPERORD,0))
        * --- Non posso modificare il flag se ho hi� caricato delle righe
        if RECCOUNT("CATE")>0
          if this.oParentObject.w_CCFLRIDU="S"
            ah_ErrorMsg("Attenzione il piano d'ammortamento � gi� caricato. E' obbligatorio cancellarlo e caricare il nuovo piano solo dopo l'attivazione del flag" ,"!","")
            this.oParentObject.w_CCFLRIDU = "N"
          else
            ah_ErrorMsg("Attenzione il piano d'ammortamento � gi� caricato. E' obbligatorio cancellarlo e caricare il nuovo piano solo dopo la disattivazione del flag" ,"!","")
            this.oParentObject.w_CCFLRIDU = "S"
          endif
        else
          THIS.OPARENTOBJECT.GSCE_MEA.NotifyEvent("Ricalcola")
        endif
      case this.pOper="C"
        THIS.OPARENTOBJECT.GSCE_MEA.NotifyEvent("Ricalcola")
    endcase
  endproc


  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='CES_PITI'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_CES_PITI')
      use in _Curs_CES_PITI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
