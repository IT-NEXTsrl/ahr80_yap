* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_kga                                                        *
*              Generazione accantonamenti                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_40]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-03-25                                                      *
* Last revis.: 2010-05-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsce_kga",oParentObject))

* --- Class definition
define class tgsce_kga as StdForm
  Top    = 17
  Left   = 38

  * --- Standard Properties
  Width  = 594
  Height = 246
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2010-05-17"
  HelpContextID=236305047
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=23

  * --- Constant Properties
  _IDX = 0
  CES_PIAN_IDX = 0
  CAT_CESP_IDX = 0
  CAU_CESP_IDX = 0
  PAR_CESP_IDX = 0
  cPrg = "gsce_kga"
  cComment = "Generazione accantonamenti"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CODAZI = space(5)
  w_TIPAMM = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_DATMOV = ctod('  /  /  ')
  w_NUMREG = space(6)
  w_CODESE = space(4)
  w_DATREG = ctod('  /  /  ')
  w_FLDEFI = space(1)
  w_DESCRI = space(40)
  w_NUMFRA = 0
  w_CODCAT = space(15)
  w_DESCAT = space(40)
  w_FLGA07 = space(1)
  w_FLGA09 = space(1)
  w_FLGA12 = space(1)
  w_CODCAU = space(5)
  w_DESCAU = space(40)
  w_STATUS = space(1)
  w_DTOBSO = ctod('  /  /  ')
  w_STATO = space(1)
  w_TIPGEN = space(1)
  w_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsce_kgaPag1","gsce_kga",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDATMOV_1_4
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='CES_PIAN'
    this.cWorkTables[2]='CAT_CESP'
    this.cWorkTables[3]='CAU_CESP'
    this.cWorkTables[4]='PAR_CESP'
    return(this.OpenAllTables(4))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI=space(5)
      .w_TIPAMM=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATMOV=ctod("  /  /  ")
      .w_NUMREG=space(6)
      .w_CODESE=space(4)
      .w_DATREG=ctod("  /  /  ")
      .w_FLDEFI=space(1)
      .w_DESCRI=space(40)
      .w_NUMFRA=0
      .w_CODCAT=space(15)
      .w_DESCAT=space(40)
      .w_FLGA07=space(1)
      .w_FLGA09=space(1)
      .w_FLGA12=space(1)
      .w_CODCAU=space(5)
      .w_DESCAU=space(40)
      .w_STATUS=space(1)
      .w_DTOBSO=ctod("  /  /  ")
      .w_STATO=space(1)
      .w_TIPGEN=space(1)
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
        .w_CODAZI = i_CODAZI
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODAZI))
          .link_1_1('Full')
        endif
          .DoRTCalc(2,2,.f.)
        .w_OBTEST = i_datsys
        .w_DATMOV = i_datsys
        .DoRTCalc(5,11,.f.)
        if not(empty(.w_CODCAT))
          .link_1_12('Full')
        endif
        .DoRTCalc(12,16,.f.)
        if not(empty(.w_CODCAU))
          .link_1_17('Full')
        endif
          .DoRTCalc(17,17,.f.)
        .w_STATUS = 'C'
          .DoRTCalc(19,20,.f.)
        .w_TIPGEN = 'D'
    endwith
    this.DoRTCalc(22,23,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_32.enabled = this.oPgFrm.Page1.oPag.oBtn_1_32.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_33.enabled = this.oPgFrm.Page1.oPag.oBtn_1_33.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
            .w_CODAZI = i_CODAZI
          .link_1_1('Full')
        .DoRTCalc(2,10,.t.)
          .link_1_12('Full')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(12,23,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oNUMFRA_1_11.visible=!this.oPgFrm.Page1.oPag.oNUMFRA_1_11.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_26.visible=!this.oPgFrm.Page1.oPag.oStr_1_26.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_27.visible=!this.oPgFrm.Page1.oPag.oStr_1_27.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_36.visible=!this.oPgFrm.Page1.oPag.oStr_1_36.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_37.visible=!this.oPgFrm.Page1.oPag.oStr_1_37.mHide()
    this.oPgFrm.Page1.oPag.oDATINI_1_38.visible=!this.oPgFrm.Page1.oPag.oDATINI_1_38.mHide()
    this.oPgFrm.Page1.oPag.oDATFIN_1_39.visible=!this.oPgFrm.Page1.oPag.oDATFIN_1_39.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_CESP_IDX,3]
    i_lTable = "PAR_CESP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_CESP_IDX,2], .t., this.PAR_CESP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_CESP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PCCODAZI,PCTIPAMM";
                   +" from "+i_cTable+" "+i_lTable+" where PCCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PCCODAZI',this.w_CODAZI)
            select PCCODAZI,PCTIPAMM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.PCCODAZI,space(5))
      this.w_TIPAMM = NVL(_Link_.PCTIPAMM,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_TIPAMM = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_CESP_IDX,2])+'\'+cp_ToStr(_Link_.PCCODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_CESP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCAT
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_CESP_IDX,3]
    i_lTable = "CAT_CESP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_CESP_IDX,2], .t., this.CAT_CESP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_CESP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_CODCAT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_CODCAT)
            select CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCAT = NVL(_Link_.CCCODICE,space(15))
      this.w_DESCAT = NVL(_Link_.CCDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODCAT = space(15)
      endif
      this.w_DESCAT = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_CESP_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_CESP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCAU
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CESP_IDX,3]
    i_lTable = "CAU_CESP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CESP_IDX,2], .t., this.CAU_CESP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CESP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCAU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCE_ACC',True,'CAU_CESP')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_CODCAU)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLGA07,CCFLGA09,CCFLGA12,CCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_CODCAU))
          select CCCODICE,CCDESCRI,CCFLGA07,CCFLGA09,CCFLGA12,CCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCAU)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStrODBC(trim(this.w_CODCAU)+"%");

            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLGA07,CCFLGA09,CCFLGA12,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStr(trim(this.w_CODCAU)+"%");

            select CCCODICE,CCDESCRI,CCFLGA07,CCFLGA09,CCFLGA12,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODCAU) and !this.bDontReportError
            deferred_cp_zoom('CAU_CESP','*','CCCODICE',cp_AbsName(oSource.parent,'oCODCAU_1_17'),i_cWhere,'GSCE_ACC',"Causali cespiti",'GSCE_KGA.CAU_CESP_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLGA07,CCFLGA09,CCFLGA12,CCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCFLGA07,CCFLGA09,CCFLGA12,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCAU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLGA07,CCFLGA09,CCFLGA12,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_CODCAU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_CODCAU)
            select CCCODICE,CCDESCRI,CCFLGA07,CCFLGA09,CCFLGA12,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCAU = NVL(_Link_.CCCODICE,space(5))
      this.w_DESCAU = NVL(_Link_.CCDESCRI,space(40))
      this.w_FLGA07 = NVL(_Link_.CCFLGA07,space(1))
      this.w_FLGA09 = NVL(_Link_.CCFLGA09,space(1))
      this.w_FLGA12 = NVL(_Link_.CCFLGA12,space(1))
      this.w_DTOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODCAU = space(5)
      endif
      this.w_DESCAU = space(40)
      this.w_FLGA07 = space(1)
      this.w_FLGA09 = space(1)
      this.w_FLGA12 = space(1)
      this.w_DTOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_FLGA07='S' OR .w_TIPAMM='F') AND (.w_FLGA09='S' OR .w_TIPAMM='C') AND (EMPTY(.w_DTOBSO) OR .w_DTOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale cespite inesistente o obsoleta o non abilitata alla gen. accantonamenti")
        endif
        this.w_CODCAU = space(5)
        this.w_DESCAU = space(40)
        this.w_FLGA07 = space(1)
        this.w_FLGA09 = space(1)
        this.w_FLGA12 = space(1)
        this.w_DTOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CESP_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CESP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCAU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDATMOV_1_4.value==this.w_DATMOV)
      this.oPgFrm.Page1.oPag.oDATMOV_1_4.value=this.w_DATMOV
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMREG_1_5.value==this.w_NUMREG)
      this.oPgFrm.Page1.oPag.oNUMREG_1_5.value=this.w_NUMREG
    endif
    if not(this.oPgFrm.Page1.oPag.oCODESE_1_6.value==this.w_CODESE)
      this.oPgFrm.Page1.oPag.oCODESE_1_6.value=this.w_CODESE
    endif
    if not(this.oPgFrm.Page1.oPag.oDATREG_1_8.value==this.w_DATREG)
      this.oPgFrm.Page1.oPag.oDATREG_1_8.value=this.w_DATREG
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_10.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_10.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMFRA_1_11.value==this.w_NUMFRA)
      this.oPgFrm.Page1.oPag.oNUMFRA_1_11.value=this.w_NUMFRA
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCAT_1_12.value==this.w_CODCAT)
      this.oPgFrm.Page1.oPag.oCODCAT_1_12.value=this.w_CODCAT
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAT_1_13.value==this.w_DESCAT)
      this.oPgFrm.Page1.oPag.oDESCAT_1_13.value=this.w_DESCAT
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCAU_1_17.value==this.w_CODCAU)
      this.oPgFrm.Page1.oPag.oCODCAU_1_17.value=this.w_CODCAU
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAU_1_18.value==this.w_DESCAU)
      this.oPgFrm.Page1.oPag.oDESCAU_1_18.value=this.w_DESCAU
    endif
    if not(this.oPgFrm.Page1.oPag.oSTATUS_1_19.RadioValue()==this.w_STATUS)
      this.oPgFrm.Page1.oPag.oSTATUS_1_19.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSTATO_1_31.RadioValue()==this.w_STATO)
      this.oPgFrm.Page1.oPag.oSTATO_1_31.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPGEN_1_35.RadioValue()==this.w_TIPGEN)
      this.oPgFrm.Page1.oPag.oTIPGEN_1_35.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_38.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_38.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_39.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_39.value=this.w_DATFIN
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_DATMOV))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATMOV_1_4.SetFocus()
            i_bnoObbl = !empty(.w_DATMOV)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_CODCAU)) or not((.w_FLGA07='S' OR .w_TIPAMM='F') AND (.w_FLGA09='S' OR .w_TIPAMM='C') AND (EMPTY(.w_DTOBSO) OR .w_DTOBSO>.w_OBTEST)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODCAU_1_17.SetFocus()
            i_bnoObbl = !empty(.w_CODCAU)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale cespite inesistente o obsoleta o non abilitata alla gen. accantonamenti")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsce_kgaPag1 as StdContainer
  Width  = 590
  height = 246
  stdWidth  = 590
  stdheight = 246
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDATMOV_1_4 as StdField with uid="JGDJJTLKTH",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DATMOV", cQueryName = "DATMOV",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di registrazione del movimento cespite",;
    HelpContextID = 111656906,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=142, Top=11

  add object oNUMREG_1_5 as StdField with uid="VPBAWTHQHF",rtseq=5,rtrep=.f.,;
    cFormVar = "w_NUMREG", cQueryName = "NUMREG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(6), bMultilanguage =  .f.,;
    ToolTipText = "Numero del piano di ammortamento da elaborare",;
    HelpContextID = 105061162,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=142, Top=42, cSayPict="'999999'", cGetPict="'999999'", InputMask=replicate('X',6)

  add object oCODESE_1_6 as StdField with uid="IIWKMXCZCB",rtseq=6,rtrep=.f.,;
    cFormVar = "w_CODESE", cQueryName = "CODESE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Esercizio del piano di ammortamento",;
    HelpContextID = 124826074,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=264, Top=42, InputMask=replicate('X',4)


  add object oBtn_1_7 as StdButton with uid="WXIAKFHXCL",left=308, top=44, width=19,height=19,;
    caption="...", nPag=1;
    , ToolTipText = "Zoom di selezione";
    , HelpContextID = 236506070;
  , bGlobalFont=.t.

    proc oBtn_1_7.Click()
      with this.Parent.oContained
        GSCE_BOO(this.Parent.oContained,"M")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDATREG_1_8 as StdField with uid="NZNORDFLSS",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DATREG", cQueryName = "DATREG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 105037770,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=508, Top=42

  add object oDESCRI_1_10 as StdField with uid="WDERWHWKHL",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 58837962,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=142, Top=73, InputMask=replicate('X',40)

  add object oNUMFRA_1_11 as StdField with uid="ZWBGROSRKJ",rtseq=10,rtrep=.f.,;
    cFormVar = "w_NUMFRA", cQueryName = "NUMFRA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 192879402,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=371, Top=104

  func oNUMFRA_1_11.mHide()
    with this.Parent.oContained
      return (.w_TIPGEN='X')
    endwith
  endfunc

  add object oCODCAT_1_12 as StdField with uid="SIHHCGGMWX",rtseq=11,rtrep=.f.,;
    cFormVar = "w_CODCAT", cQueryName = "CODCAT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Categoria cespite associata al piano di ammortamento",;
    HelpContextID = 160608730,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=142, Top=135, InputMask=replicate('X',15), cLinkFile="CAT_CESP", cZoomOnZoom="GSCE_ACT", oKey_1_1="CCCODICE", oKey_1_2="this.w_CODCAT"

  func oCODCAT_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDESCAT_1_13 as StdField with uid="GGEPTJVJRT",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DESCAT", cQueryName = "DESCAT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 160549834,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=268, Top=135, InputMask=replicate('X',40)

  add object oCODCAU_1_17 as StdField with uid="JKDMTHPZJF",rtseq=16,rtrep=.f.,;
    cFormVar = "w_CODCAU", cQueryName = "CODCAU",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale cespite inesistente o obsoleta o non abilitata alla gen. accantonamenti",;
    ToolTipText = "Causale cespite di generazione",;
    HelpContextID = 143831514,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=142, Top=166, cSayPict="'!!!!!'", cGetPict="'!!!!!'", InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CESP", cZoomOnZoom="GSCE_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_CODCAU"

  func oCODCAU_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCAU_1_17.ecpDrop(oSource)
    this.Parent.oContained.link_1_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCAU_1_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CESP','*','CCCODICE',cp_AbsName(this.parent,'oCODCAU_1_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCE_ACC',"Causali cespiti",'GSCE_KGA.CAU_CESP_VZM',this.parent.oContained
  endproc
  proc oCODCAU_1_17.mZoomOnZoom
    local i_obj
    i_obj=GSCE_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_CODCAU
     i_obj.ecpSave()
  endproc

  add object oDESCAU_1_18 as StdField with uid="NJLHLACGWJ",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DESCAU", cQueryName = "DESCAU",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 143772618,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=268, Top=166, InputMask=replicate('X',40)


  add object oSTATUS_1_19 as StdCombo with uid="ZKHNMRSLLI",rtseq=18,rtrep=.f.,left=142,top=197,width=110,height=21;
    , ToolTipText = "Status dei movimenti da generare; confermati o provvisori";
    , HelpContextID = 155311066;
    , cFormVar="w_STATUS",RowSource=""+"Provvisori,"+"Confermati", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSTATUS_1_19.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'C',;
    space(1))))
  endfunc
  func oSTATUS_1_19.GetRadio()
    this.Parent.oContained.w_STATUS = this.RadioValue()
    return .t.
  endfunc

  func oSTATUS_1_19.SetRadio()
    this.Parent.oContained.w_STATUS=trim(this.Parent.oContained.w_STATUS)
    this.value = ;
      iif(this.Parent.oContained.w_STATUS=='P',1,;
      iif(this.Parent.oContained.w_STATUS=='C',2,;
      0))
  endfunc


  add object oSTATO_1_31 as StdCombo with uid="IDNRLHVHVA",rtseq=20,rtrep=.f.,left=358,top=42,width=104,height=21, enabled=.f.;
    , ToolTipText = "Status dei beni del piano di ammortamento";
    , HelpContextID = 211934170;
    , cFormVar="w_STATO",RowSource=""+"Effettivo,"+"Previsionale", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSTATO_1_31.RadioValue()
    return(iif(this.value =1,'E',;
    iif(this.value =2,'P',;
    space(1))))
  endfunc
  func oSTATO_1_31.GetRadio()
    this.Parent.oContained.w_STATO = this.RadioValue()
    return .t.
  endfunc

  func oSTATO_1_31.SetRadio()
    this.Parent.oContained.w_STATO=trim(this.Parent.oContained.w_STATO)
    this.value = ;
      iif(this.Parent.oContained.w_STATO=='E',1,;
      iif(this.Parent.oContained.w_STATO=='P',2,;
      0))
  endfunc


  add object oBtn_1_32 as StdButton with uid="LHXSXCMLHQ",left=480, top=197, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per iniziare la generazione";
    , HelpContextID = 236333798;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_32.Click()
      with this.Parent.oContained
        do GSCE_BGA with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_33 as StdButton with uid="LRRYFZICGX",left=533, top=197, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 243622470;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_33.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oTIPGEN_1_35 as StdCombo with uid="ISULTJYIUF",rtseq=21,rtrep=.f.,left=142,top=103,width=119,height=21, enabled=.f.;
    , ToolTipText = "Ammortamento in dodicesimi oppure dietimi";
    , HelpContextID = 256767690;
    , cFormVar="w_TIPGEN",RowSource=""+"Dodicesimi,"+"Dietimi", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPGEN_1_35.RadioValue()
    return(iif(this.value =1,'D',;
    iif(this.value =2,'X',;
    space(1))))
  endfunc
  func oTIPGEN_1_35.GetRadio()
    this.Parent.oContained.w_TIPGEN = this.RadioValue()
    return .t.
  endfunc

  func oTIPGEN_1_35.SetRadio()
    this.Parent.oContained.w_TIPGEN=trim(this.Parent.oContained.w_TIPGEN)
    this.value = ;
      iif(this.Parent.oContained.w_TIPGEN=='D',1,;
      iif(this.Parent.oContained.w_TIPGEN=='X',2,;
      0))
  endfunc

  add object oDATINI_1_38 as StdField with uid="BHWWNTDWTT",rtseq=22,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di inizio competenza del piano di ammortamento ",;
    HelpContextID = 62635978,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=346, Top=104

  func oDATINI_1_38.mHide()
    with this.Parent.oContained
      return (.w_TIPGEN='D')
    endwith
  endfunc

  add object oDATFIN_1_39 as StdField with uid="XCNZOPCIQG",rtseq=23,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di fine competenza del piano di ammortamento",;
    HelpContextID = 252624842,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=500, Top=104

  func oDATFIN_1_39.mHide()
    with this.Parent.oContained
      return (.w_TIPGEN='D')
    endwith
  endfunc

  add object oStr_1_20 as StdString with uid="GDPXGLONDP",Visible=.t., Left=33, Top=11,;
    Alignment=1, Width=104, Height=15,;
    Caption="Data movimento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="TWKSTJWEZY",Visible=.t., Left=33, Top=42,;
    Alignment=1, Width=104, Height=15,;
    Caption="Piano n.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="FZONHLSGTZ",Visible=.t., Left=465, Top=42,;
    Alignment=1, Width=42, Height=15,;
    Caption="Data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="FHGYUBDOWK",Visible=.t., Left=200, Top=42,;
    Alignment=1, Width=62, Height=15,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="KOVUWVULCE",Visible=.t., Left=33, Top=73,;
    Alignment=1, Width=104, Height=15,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="UQBVSGVCYA",Visible=.t., Left=33, Top=166,;
    Alignment=1, Width=104, Height=15,;
    Caption="Causale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="RKGZSPHGRF",Visible=.t., Left=265, Top=105,;
    Alignment=1, Width=104, Height=15,;
    Caption="Frazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_26.mHide()
    with this.Parent.oContained
      return (.w_TIPGEN='X')
    endwith
  endfunc

  add object oStr_1_27 as StdString with uid="WNKXZPVGQJ",Visible=.t., Left=410, Top=105,;
    Alignment=0, Width=157, Height=15,;
    Caption="(Dodicesimi)"  ;
  , bGlobalFont=.t.

  func oStr_1_27.mHide()
    with this.Parent.oContained
      return (.w_TIPGEN='X')
    endwith
  endfunc

  add object oStr_1_28 as StdString with uid="YHWZSQVIUX",Visible=.t., Left=33, Top=135,;
    Alignment=1, Width=104, Height=15,;
    Caption="Categoria:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="RTBIPKBETP",Visible=.t., Left=33, Top=197,;
    Alignment=1, Width=104, Height=15,;
    Caption="Status:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="CGZGSPSMUY",Visible=.t., Left=4, Top=105,;
    Alignment=1, Width=133, Height=15,;
    Caption="Ammortamento in:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="DXLQMCAVCN",Visible=.t., Left=272, Top=105,;
    Alignment=1, Width=70, Height=15,;
    Caption="da data:"  ;
  , bGlobalFont=.t.

  func oStr_1_36.mHide()
    with this.Parent.oContained
      return (.w_TIPGEN='D')
    endwith
  endfunc

  add object oStr_1_37 as StdString with uid="KDPGTUQPWS",Visible=.t., Left=425, Top=105,;
    Alignment=1, Width=70, Height=15,;
    Caption="a data:"  ;
  , bGlobalFont=.t.

  func oStr_1_37.mHide()
    with this.Parent.oContained
      return (.w_TIPGEN='D')
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsce_kga','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
