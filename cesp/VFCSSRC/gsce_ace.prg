* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_ace                                                        *
*              Cespiti                                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30] [VRS_290]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-07-24                                                      *
* Last revis.: 2014-02-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsce_ace"))

* --- Class definition
define class tgsce_ace as StdForm
  Top    = 0
  Left   = 14

  * --- Standard Properties
  Width  = 787
  Height = 541+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-02-11"
  HelpContextID=109725033
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=93

  * --- Constant Properties
  CES_PITI_IDX = 0
  CENCOST_IDX = 0
  VOC_COST_IDX = 0
  CAT_CESP_IDX = 0
  UBI_CESP_IDX = 0
  CAN_TIER_IDX = 0
  SAL_CESP_IDX = 0
  COM_PCES_IDX = 0
  ESERCIZI_IDX = 0
  VALUTE_IDX = 0
  FAM_CESP_IDX = 0
  ATTIMAST_IDX = 0
  UNIMIS_IDX = 0
  PAR_CESP_IDX = 0
  SAL_CESE_IDX = 0
  cFile = "CES_PITI"
  cKeySelect = "CECODICE"
  cKeyWhere  = "CECODICE=this.w_CECODICE"
  cKeyWhereODBC = '"CECODICE="+cp_ToStrODBC(this.w_CECODICE)';

  cKeyWhereODBCqualified = '"CES_PITI.CECODICE="+cp_ToStrODBC(this.w_CECODICE)';

  cPrg = "gsce_ace"
  cComment = "Cespiti"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CECODCAT = space(15)
  o_CECODCAT = space(15)
  w_CE_SERIA = space(5)
  w_PREFISSO = space(15)
  w_NSOAMM = space(1)
  o_NSOAMM = space(1)
  w_CODAZI = space(5)
  w_OBTEST = ctod('  /  /  ')
  w_CECODICE = space(20)
  o_CECODICE = space(20)
  w_CODESE = space(5)
  w_CEDESCRI = space(40)
  w_CEDESSUP = space(0)
  w_CETIPCES = space(2)
  o_CETIPCES = space(2)
  w_CE_STATO = space(1)
  w_CESTABEN = space(1)
  o_CESTABEN = space(1)
  w_CEUNIMIS = space(3)
  o_CEUNIMIS = space(3)
  w_CECODUBI = space(20)
  w_DESUBI = space(40)
  w_CECODMAT = space(20)
  w_CECODFAM = space(5)
  w_DESFAM = space(35)
  w_CECODPER = space(20)
  w_DESCES = space(40)
  w_CECODCOM = space(15)
  w_CECODATT = space(5)
  w_CEDTPRIC = ctod('  /  /  ')
  o_CEDTPRIC = ctod('  /  /  ')
  w_DESCOM = space(30)
  w_CEDTPRIU = ctod('  /  /  ')
  o_CEDTPRIU = ctod('  /  /  ')
  w_CEDATDIS = ctod('  /  /  ')
  w_CEESPRIC = space(4)
  w_CEFLSPEM = space(1)
  w_CEESPRIU = space(4)
  w_CEFLCEUS = space(1)
  w_CEDTINVA = ctod('  /  /  ')
  w_CEDTOBSO = ctod('  /  /  ')
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_OLDCAT = space(15)
  w_CODAZI = space(1)
  w_PCTIPAMM = space(1)
  w_CODI = space(20)
  w_DESC = space(40)
  w_CETIPAMM = space(1)
  w_CECODESE = space(4)
  o_CECODESE = space(4)
  w_VALECC = space(3)
  w_DECTOP = 0
  w_CEDURCES = 0
  w_CALCPIP = 0
  w_CECOEFIS = 0
  o_CECOEFIS = 0
  w_CEPERDEF = 0
  w_CEIMPMAX = 0
  w_CEVALLIM = space(3)
  o_CEVALLIM = space(3)
  w_DECTOT = 0
  w_CEFLAUMU = space(1)
  w_CALCPIC = 0
  w_FINESE = ctod('  /  /  ')
  w_OK = .F.
  w_DESATT = space(35)
  w_DESUNI = space(35)
  w_F4RAPIDO = space(1)
  w_DESCAT = space(40)
  w_CEVOCCEN = space(15)
  w_CECODCEN = space(15)
  w_DESVOC = space(40)
  w_DESCEN = space(40)
  w_TIPBEN = space(1)
  o_TIPBEN = space(1)
  w_CEAMMIMM = space(1)
  w_CENSOAMM = space(1)
  w_CENSOAMF = space(1)
  w_CEAMMIMC = space(1)
  w_NSOAMF = space(1)
  o_NSOAMF = space(1)
  w_FLRIDU = space(10)
  o_FLRIDU = space(10)
  w_CCOBTEST = ctod('  /  /  ')
  w_COD = space(20)
  w_DES = space(40)
  w_SERIALE = space(10)
  w_ESPRI = space(5)
  w_CAUSALE = space(10)
  w_FLDADI = space(10)
  w_FLPRIC = space(10)
  w_FLPRIU = space(10)
  w_COSBEN = 0
  w_SPEPRE = 0
  w_QUOESE = 0
  w_SPECOR = 0
  w_COSUNI = 0
  w_UNIMIS = space(3)
  w_DURBEN = 0
  w_PERVAR = 0
  w_QTAPRE = 0
  w_CODI = space(20)
  w_DESC = space(40)
  w_ESER = space(4)

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_CODAZI = this.W_CODAZI
  op_CECODCAT = this.W_CECODCAT
  op_CE_SERIA = this.W_CE_SERIA

  * --- Children pointers
  GSCE_MCO = .NULL.
  GSCE_MAM = .NULL.
  GSCE_MCC = .NULL.
  w_MOVIMENTI = .NULL.
  w_SALDI = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsce_ace
  * Eventuale rifermento maschera documenti( se lanciato da gsve_kda all'f10 chiude la maschera)
  w_OBJMASK=.f.
  
  proc ecpSave()
    DoDefault()
    if type('This.w_OBJMASK.Caption')='C'And this.cFunction='Load'
     if not(this.w_cetipces='CQ'and empty(nvl(this.w_ceunimis,space(3))))
      this.ecpquit()
      this.ecpquit()
     endif
    Endif
  
  EndProc
  
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=5, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'CES_PITI','gsce_ace')
    stdPageFrame::Init()
    *set procedure to GSCE_MCO additive
    with this
      .Pages(1).addobject("oPag","tgsce_acePag1","gsce_ace",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Cespite")
      .Pages(1).HelpContextID = 193113050
      .Pages(2).addobject("oPag","tgsce_acePag2","gsce_ace",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Ammortamento")
      .Pages(2).HelpContextID = 92451011
      .Pages(3).addobject("oPag","tgsce_acePag3","gsce_ace",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Ammortamento gestionale")
      .Pages(3).HelpContextID = 251129482
      .Pages(4).addobject("oPag","tgsce_acePag4","gsce_ace",4)
      .Pages(4).oPag.Visible=.t.
      .Pages(4).Caption=cp_Translate("Movimenti")
      .Pages(4).HelpContextID = 95851978
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCECODICE_1_7
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure GSCE_MCO
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
    proc Init()
      this.w_MOVIMENTI = this.oPgFrm.Pages(4).oPag.MOVIMENTI
      this.w_SALDI = this.oPgFrm.Pages(4).oPag.SALDI
      DoDefault()
    proc Destroy()
      this.w_MOVIMENTI = .NULL.
      this.w_SALDI = .NULL.
      DoDefault()

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[15]
    this.cWorkTables[1]='CENCOST'
    this.cWorkTables[2]='VOC_COST'
    this.cWorkTables[3]='CAT_CESP'
    this.cWorkTables[4]='UBI_CESP'
    this.cWorkTables[5]='CAN_TIER'
    this.cWorkTables[6]='SAL_CESP'
    this.cWorkTables[7]='COM_PCES'
    this.cWorkTables[8]='ESERCIZI'
    this.cWorkTables[9]='VALUTE'
    this.cWorkTables[10]='FAM_CESP'
    this.cWorkTables[11]='ATTIMAST'
    this.cWorkTables[12]='UNIMIS'
    this.cWorkTables[13]='PAR_CESP'
    this.cWorkTables[14]='SAL_CESE'
    this.cWorkTables[15]='CES_PITI'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(15))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CES_PITI_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CES_PITI_IDX,3]
  return

  function CreateChildren()
    this.GSCE_MCO = CREATEOBJECT('stdLazyChild',this,'GSCE_MCO')
    this.GSCE_MAM = CREATEOBJECT('stdDynamicChild',this,'GSCE_MAM',this.oPgFrm.Page2.oPag.oLinkPC_2_27)
    this.GSCE_MAM.createrealchild()
    this.GSCE_MCC = CREATEOBJECT('stdDynamicChild',this,'GSCE_MCC',this.oPgFrm.Page2.oPag.oLinkPC_2_49)
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSCE_MCO)
      this.GSCE_MCO.DestroyChildrenChain()
      this.GSCE_MCO=.NULL.
    endif
    if !ISNULL(this.GSCE_MAM)
      this.GSCE_MAM.DestroyChildrenChain()
      this.GSCE_MAM=.NULL.
    endif
    this.oPgFrm.Page2.oPag.RemoveObject('oLinkPC_2_27')
    if !ISNULL(this.GSCE_MCC)
      this.GSCE_MCC.DestroyChildrenChain()
      this.GSCE_MCC=.NULL.
    endif
    this.oPgFrm.Page2.oPag.RemoveObject('oLinkPC_2_49')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSCE_MCO.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSCE_MAM.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSCE_MCC.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSCE_MCO.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSCE_MAM.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSCE_MCC.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSCE_MCO.NewDocument()
    this.GSCE_MAM.NewDocument()
    this.GSCE_MCC.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSCE_MCO.SetKey(;
            .w_CECODICE,"COCODICE";
            )
      this.GSCE_MAM.SetKey(;
            .w_CECODICE,"AMCODCES";
            )
      this.GSCE_MCC.SetKey(;
            .w_CECODICE,"ACCODCES";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSCE_MCO.ChangeRow(this.cRowID+'      1',1;
             ,.w_CECODICE,"COCODICE";
             )
      .GSCE_MAM.ChangeRow(this.cRowID+'      1',1;
             ,.w_CECODICE,"AMCODCES";
             )
      .WriteTo_GSCE_MAM()
      .GSCE_MCC.ChangeRow(this.cRowID+'      1',1;
             ,.w_CECODICE,"ACCODCES";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSCE_MAM)
        i_f=.GSCE_MAM.BuildFilter()
        if !(i_f==.GSCE_MAM.cQueryFilter)
          i_fnidx=.GSCE_MAM.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSCE_MAM.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSCE_MAM.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSCE_MAM.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSCE_MAM.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSCE_MCC)
        i_f=.GSCE_MCC.BuildFilter()
        if !(i_f==.GSCE_MCC.cQueryFilter)
          i_fnidx=.GSCE_MCC.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSCE_MCC.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSCE_MCC.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSCE_MCC.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSCE_MCC.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

procedure WriteTo_GSCE_MAM()
  if at('gsce_mam',lower(this.GSCE_MAM.class))<>0
    if this.GSCE_MAM.w_FLRIDU<>this.w_FLRIDU or this.GSCE_MAM.w_COEFIS<>this.w_CECOEFIS or this.GSCE_MAM.w_TIPBEN<>this.w_TIPBEN
      this.GSCE_MAM.w_FLRIDU = this.w_FLRIDU
      this.GSCE_MAM.w_COEFIS = this.w_CECOEFIS
      this.GSCE_MAM.w_TIPBEN = this.w_TIPBEN
      this.GSCE_MAM.mCalc(.t.)
    endif
  endif
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_CECODICE = NVL(CECODICE,space(20))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_1_joined
    link_1_1_joined=.f.
    local link_1_14_joined
    link_1_14_joined=.f.
    local link_1_15_joined
    link_1_15_joined=.f.
    local link_1_18_joined
    link_1_18_joined=.f.
    local link_1_20_joined
    link_1_20_joined=.f.
    local link_1_22_joined
    link_1_22_joined=.f.
    local link_1_23_joined
    link_1_23_joined=.f.
    local link_2_14_joined
    link_2_14_joined=.f.
    local link_2_30_joined
    link_2_30_joined=.f.
    local link_2_31_joined
    link_2_31_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from CES_PITI where CECODICE=KeySet.CECODICE
    *
    i_nConn = i_TableProp[this.CES_PITI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CES_PITI_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CES_PITI')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CES_PITI.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' CES_PITI '
      link_1_1_joined=this.AddJoinedLink_1_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_14_joined=this.AddJoinedLink_1_14(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_15_joined=this.AddJoinedLink_1_15(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_18_joined=this.AddJoinedLink_1_18(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_20_joined=this.AddJoinedLink_1_20(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_22_joined=this.AddJoinedLink_1_22(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_23_joined=this.AddJoinedLink_1_23(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_14_joined=this.AddJoinedLink_2_14(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_30_joined=this.AddJoinedLink_2_30(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_31_joined=this.AddJoinedLink_2_31(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'CECODICE',this.w_CECODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_PREFISSO = space(15)
        .w_NSOAMM = space(1)
        .w_CODAZI = i_CODAZI
        .w_OBTEST = i_datsys
        .w_CODESE = g_CODESE
        .w_DESUBI = space(40)
        .w_DESFAM = space(35)
        .w_DESCES = space(40)
        .w_DESCOM = space(30)
        .w_CODAZI = i_CODAZI
        .w_PCTIPAMM = space(1)
        .w_VALECC = space(3)
        .w_DECTOP = 0
        .w_DECTOT = 0
        .w_FINESE = ctod("  /  /  ")
        .w_OK = .F.
        .w_DESATT = space(35)
        .w_DESUNI = space(35)
        .w_F4RAPIDO = space(1)
        .w_DESCAT = space(40)
        .w_DESVOC = space(40)
        .w_DESCEN = space(40)
        .w_TIPBEN = space(1)
        .w_NSOAMF = space(1)
        .w_FLRIDU = space(10)
        .w_CCOBTEST = ctod("  /  /  ")
        .w_COSBEN = 0
        .w_SPEPRE = 0
        .w_QUOESE = 0
        .w_SPECOR = 0
        .w_COSUNI = 0
        .w_UNIMIS = space(3)
        .w_DURBEN = 0
        .w_PERVAR = 0
        .w_QTAPRE = 0
        .w_CECODCAT = NVL(CECODCAT,space(15))
        .op_CECODCAT = .w_CECODCAT
          if link_1_1_joined
            this.w_CECODCAT = NVL(CCCODICE101,NVL(this.w_CECODCAT,space(15)))
            this.w_DESCAT = NVL(CCDESCRI101,space(40))
            this.w_PREFISSO = NVL(CCPREFIS101,space(15))
            this.w_TIPBEN = NVL(CCTIPBEN101,space(1))
            this.w_NSOAMM = NVL(CCNSOAMM101,space(1))
            this.w_NSOAMF = NVL(CCNSOAMF101,space(1))
            this.w_FLRIDU = NVL(CCFLRIDU101,space(10))
            this.w_CCOBTEST = NVL(cp_ToDate(CCDTOBSO101),ctod("  /  /  "))
          else
          .link_1_1('Load')
          endif
        .w_CE_SERIA = NVL(CE_SERIA,space(5))
        .op_CE_SERIA = .w_CE_SERIA
          .link_1_5('Load')
        .w_CECODICE = NVL(CECODICE,space(20))
          .link_1_8('Load')
        .w_CEDESCRI = NVL(CEDESCRI,space(40))
        .w_CEDESSUP = NVL(CEDESSUP,space(0))
        .w_CETIPCES = NVL(CETIPCES,space(2))
        .w_CE_STATO = NVL(CE_STATO,space(1))
        .w_CESTABEN = NVL(CESTABEN,space(1))
        .w_CEUNIMIS = NVL(CEUNIMIS,space(3))
          if link_1_14_joined
            this.w_CEUNIMIS = NVL(UMCODICE114,NVL(this.w_CEUNIMIS,space(3)))
            this.w_DESUNI = NVL(UMDESCRI114,space(35))
          else
          .link_1_14('Load')
          endif
        .w_CECODUBI = NVL(CECODUBI,space(20))
          if link_1_15_joined
            this.w_CECODUBI = NVL(UBCODICE115,NVL(this.w_CECODUBI,space(20)))
            this.w_DESUBI = NVL(UBDESCRI115,space(40))
          else
          .link_1_15('Load')
          endif
        .w_CECODMAT = NVL(CECODMAT,space(20))
        .w_CECODFAM = NVL(CECODFAM,space(5))
          if link_1_18_joined
            this.w_CECODFAM = NVL(FACODICE118,NVL(this.w_CECODFAM,space(5)))
            this.w_DESFAM = NVL(FADESCRI118,space(35))
          else
          .link_1_18('Load')
          endif
        .w_CECODPER = NVL(CECODPER,space(20))
          if link_1_20_joined
            this.w_CECODPER = NVL(CECODICE120,NVL(this.w_CECODPER,space(20)))
            this.w_DESCES = NVL(CEDESCRI120,space(40))
          else
          .link_1_20('Load')
          endif
        .w_CECODCOM = NVL(CECODCOM,space(15))
          if link_1_22_joined
            this.w_CECODCOM = NVL(CNCODCAN122,NVL(this.w_CECODCOM,space(15)))
            this.w_DESCOM = NVL(CNDESCAN122,space(30))
          else
          .link_1_22('Load')
          endif
        .w_CECODATT = NVL(CECODATT,space(5))
          if link_1_23_joined
            this.w_CECODATT = NVL(ATCODATT123,NVL(this.w_CECODATT,space(5)))
            this.w_DESATT = NVL(ATDESATT123,space(35))
          else
          .link_1_23('Load')
          endif
        .w_CEDTPRIC = NVL(cp_ToDate(CEDTPRIC),ctod("  /  /  "))
        .w_CEDTPRIU = NVL(cp_ToDate(CEDTPRIU),ctod("  /  /  "))
        .w_CEDATDIS = NVL(cp_ToDate(CEDATDIS),ctod("  /  /  "))
        .w_CEESPRIC = NVL(CEESPRIC,space(4))
          * evitabile
          *.link_1_28('Load')
        .w_CEFLSPEM = NVL(CEFLSPEM,space(1))
        .w_CEESPRIU = NVL(CEESPRIU,space(4))
          * evitabile
          *.link_1_30('Load')
        .w_CEFLCEUS = NVL(CEFLCEUS,space(1))
        .w_CEDTINVA = NVL(cp_ToDate(CEDTINVA),ctod("  /  /  "))
        .w_CEDTOBSO = NVL(cp_ToDate(CEDTOBSO),ctod("  /  /  "))
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .oPgFrm.Page1.oPag.oObj_1_48.Calculate()
        .w_OLDCAT = .w_CECODCAT
        .oPgFrm.Page1.oPag.oObj_1_52.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_54.Calculate()
          .link_2_1('Load')
        .w_CODI = .w_CECODICE
        .w_DESC = .w_CEDESCRI
        .w_CETIPAMM = NVL(CETIPAMM,space(1))
        .w_CECODESE = NVL(CECODESE,space(4))
          .link_2_6('Load')
          .link_2_7('Load')
        .w_CEDURCES = NVL(CEDURCES,0)
        .w_CALCPIP = DEFPIP(.w_DECTOP)
        .w_CECOEFIS = NVL(CECOEFIS,0)
        .w_CEPERDEF = NVL(CEPERDEF,0)
        .w_CEIMPMAX = NVL(CEIMPMAX,0)
        .w_CEVALLIM = NVL(CEVALLIM,space(3))
          if link_2_14_joined
            this.w_CEVALLIM = NVL(VACODVAL214,NVL(this.w_CEVALLIM,space(3)))
            this.w_DECTOT = NVL(VADECTOT214,0)
          else
          .link_2_14('Load')
          endif
        .w_CEFLAUMU = NVL(CEFLAUMU,space(1))
        .w_CALCPIC = DEFPIC(.w_DECTOT)
        .oPgFrm.Page1.oPag.oObj_1_64.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_65.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_66.Calculate()
        .w_CEVOCCEN = NVL(CEVOCCEN,space(15))
          if link_2_30_joined
            this.w_CEVOCCEN = NVL(VCCODICE230,NVL(this.w_CEVOCCEN,space(15)))
            this.w_DESVOC = NVL(VCDESCRI230,space(40))
          else
          .link_2_30('Load')
          endif
        .w_CECODCEN = NVL(CECODCEN,space(15))
          if link_2_31_joined
            this.w_CECODCEN = NVL(CC_CONTO231,NVL(this.w_CECODCEN,space(15)))
            this.w_DESCEN = NVL(CCDESPIA231,space(40))
          else
          .link_2_31('Load')
          endif
        .oPgFrm.Page2.oPag.oObj_2_41.Calculate()
        .w_CEAMMIMM = NVL(CEAMMIMM,space(1))
        .w_CENSOAMM = NVL(CENSOAMM,space(1))
        .w_CENSOAMF = NVL(CENSOAMF,space(1))
        .w_CEAMMIMC = NVL(CEAMMIMC,space(1))
        .oPgFrm.Page1.oPag.oObj_1_74.Calculate()
        .oPgFrm.Page4.oPag.MOVIMENTI.Calculate()
        .oPgFrm.Page4.oPag.SALDI.Calculate()
        .w_COD = .w_CECODICE
        .w_DES = .w_CEDESCRI
        .w_SERIALE = .w_MOVIMENTI.getVar('SERIALE')
        .w_ESPRI = .w_CEESPRIU
        .w_CAUSALE = .w_MOVIMENTI.getVar('MCCODCAU')
        .oPgFrm.Page4.oPag.oObj_4_14.Calculate(NOT(.w_FLPRIU='=' OR .w_FLPRIC='=') )
        .w_FLDADI = .w_MOVIMENTI.getVar('CCFLDADI')
        .w_FLPRIC = .w_MOVIMENTI.getVar('CCFLPRIC')
        .w_FLPRIU = .w_MOVIMENTI.getVar('CCFLPRIU')
        .w_CODI = .w_CECODICE
        .w_DESC = .w_CEDESCRI
        .w_ESER = .w_CODESE
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'CES_PITI')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page4.oPag.oBtn_4_10.enabled = this.oPgFrm.Page4.oPag.oBtn_4_10.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gsce_ace
    * --- Memorizza il Codice del Cespite Caricato in precedenza
    g_CODCESP=this.w_CECODICE
    g_F4RAPIDO=this.w_F4RAPIDO
    * In caso di numerazione automatica non faccio niente
    g_PREFISSO = this.w_PREFISSO
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CECODCAT = space(15)
      .w_CE_SERIA = space(5)
      .w_PREFISSO = space(15)
      .w_NSOAMM = space(1)
      .w_CODAZI = space(5)
      .w_OBTEST = ctod("  /  /  ")
      .w_CECODICE = space(20)
      .w_CODESE = space(5)
      .w_CEDESCRI = space(40)
      .w_CEDESSUP = space(0)
      .w_CETIPCES = space(2)
      .w_CE_STATO = space(1)
      .w_CESTABEN = space(1)
      .w_CEUNIMIS = space(3)
      .w_CECODUBI = space(20)
      .w_DESUBI = space(40)
      .w_CECODMAT = space(20)
      .w_CECODFAM = space(5)
      .w_DESFAM = space(35)
      .w_CECODPER = space(20)
      .w_DESCES = space(40)
      .w_CECODCOM = space(15)
      .w_CECODATT = space(5)
      .w_CEDTPRIC = ctod("  /  /  ")
      .w_DESCOM = space(30)
      .w_CEDTPRIU = ctod("  /  /  ")
      .w_CEDATDIS = ctod("  /  /  ")
      .w_CEESPRIC = space(4)
      .w_CEFLSPEM = space(1)
      .w_CEESPRIU = space(4)
      .w_CEFLCEUS = space(1)
      .w_CEDTINVA = ctod("  /  /  ")
      .w_CEDTOBSO = ctod("  /  /  ")
      .w_UTCC = 0
      .w_UTCV = 0
      .w_UTDC = ctot("")
      .w_UTDV = ctot("")
      .w_OLDCAT = space(15)
      .w_CODAZI = space(1)
      .w_PCTIPAMM = space(1)
      .w_CODI = space(20)
      .w_DESC = space(40)
      .w_CETIPAMM = space(1)
      .w_CECODESE = space(4)
      .w_VALECC = space(3)
      .w_DECTOP = 0
      .w_CEDURCES = 0
      .w_CALCPIP = 0
      .w_CECOEFIS = 0
      .w_CEPERDEF = 0
      .w_CEIMPMAX = 0
      .w_CEVALLIM = space(3)
      .w_DECTOT = 0
      .w_CEFLAUMU = space(1)
      .w_CALCPIC = 0
      .w_FINESE = ctod("  /  /  ")
      .w_OK = .f.
      .w_DESATT = space(35)
      .w_DESUNI = space(35)
      .w_F4RAPIDO = space(1)
      .w_DESCAT = space(40)
      .w_CEVOCCEN = space(15)
      .w_CECODCEN = space(15)
      .w_DESVOC = space(40)
      .w_DESCEN = space(40)
      .w_TIPBEN = space(1)
      .w_CEAMMIMM = space(1)
      .w_CENSOAMM = space(1)
      .w_CENSOAMF = space(1)
      .w_CEAMMIMC = space(1)
      .w_NSOAMF = space(1)
      .w_FLRIDU = space(10)
      .w_CCOBTEST = ctod("  /  /  ")
      .w_COD = space(20)
      .w_DES = space(40)
      .w_SERIALE = space(10)
      .w_ESPRI = space(5)
      .w_CAUSALE = space(10)
      .w_FLDADI = space(10)
      .w_FLPRIC = space(10)
      .w_FLPRIU = space(10)
      .w_COSBEN = 0
      .w_SPEPRE = 0
      .w_QUOESE = 0
      .w_SPECOR = 0
      .w_COSUNI = 0
      .w_UNIMIS = space(3)
      .w_DURBEN = 0
      .w_PERVAR = 0
      .w_QTAPRE = 0
      .w_CODI = space(20)
      .w_DESC = space(40)
      .w_ESER = space(4)
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
          if not(empty(.w_CECODCAT))
          .link_1_1('Full')
          endif
        .w_CE_SERIA = iif(!empty(.w_CE_SERIA), .w_CE_SERIA, SPACE(5))
          .DoRTCalc(3,4,.f.)
        .w_CODAZI = i_CODAZI
        .DoRTCalc(5,5,.f.)
          if not(empty(.w_CODAZI))
          .link_1_5('Full')
          endif
        .w_OBTEST = i_datsys
        .w_CECODICE = IIF(EMPTY(.w_PREFISSO) or this.cfunction='Edit',.w_CECODICE,ALLTRIM(.w_PREFISSO) + .w_CE_SERIA)
        .w_CODESE = g_CODESE
        .DoRTCalc(8,8,.f.)
          if not(empty(.w_CODESE))
          .link_1_8('Full')
          endif
          .DoRTCalc(9,10,.f.)
        .w_CETIPCES = 'CS'
        .w_CE_STATO = 'E'
        .w_CESTABEN = 'N'
        .w_CEUNIMIS = IIF(.w_CETIPCES='CQ',.w_CEUNIMIS,Space(3))
        .DoRTCalc(14,14,.f.)
          if not(empty(.w_CEUNIMIS))
          .link_1_14('Full')
          endif
        .w_CECODUBI = IIF(.w_CETIPCES $ 'CC-PC',SPACE(20),.w_CECODUBI)
        .DoRTCalc(15,15,.f.)
          if not(empty(.w_CECODUBI))
          .link_1_15('Full')
          endif
          .DoRTCalc(16,16,.f.)
        .w_CECODMAT = IIF(.w_CETIPCES $ 'CC-PC',SPACE(20),.w_CECODMAT)
        .DoRTCalc(18,18,.f.)
          if not(empty(.w_CECODFAM))
          .link_1_18('Full')
          endif
        .DoRTCalc(19,20,.f.)
          if not(empty(.w_CECODPER))
          .link_1_20('Full')
          endif
        .DoRTCalc(21,22,.f.)
          if not(empty(.w_CECODCOM))
          .link_1_22('Full')
          endif
        .DoRTCalc(23,23,.f.)
          if not(empty(.w_CECODATT))
          .link_1_23('Full')
          endif
          .DoRTCalc(24,26,.f.)
        .w_CEDATDIS = IIF(.w_CESTABEN='C' OR .w_CESTABEN='E',.w_CEDATDIS,cp_CharToDate('  -  -  '))
        .w_CEESPRIC = calceser(.w_CEDTPRIC, space(4))
        .DoRTCalc(28,28,.f.)
          if not(empty(.w_CEESPRIC))
          .link_1_28('Full')
          endif
          .DoRTCalc(29,29,.f.)
        .w_CEESPRIU = calceser(.w_CEDTPRIU, space(4))
        .DoRTCalc(30,30,.f.)
          if not(empty(.w_CEESPRIU))
          .link_1_30('Full')
          endif
        .oPgFrm.Page1.oPag.oObj_1_48.Calculate()
          .DoRTCalc(31,37,.f.)
        .w_OLDCAT = .w_CECODCAT
        .oPgFrm.Page1.oPag.oObj_1_52.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_54.Calculate()
        .w_CODAZI = i_CODAZI
        .DoRTCalc(39,39,.f.)
          if not(empty(.w_CODAZI))
          .link_2_1('Full')
          endif
          .DoRTCalc(40,40,.f.)
        .w_CODI = .w_CECODICE
        .w_DESC = .w_CEDESCRI
        .w_CETIPAMM = IIF(EMPTY(.w_CECODESE), .w_PCTIPAMM, .w_CETIPAMM)
        .DoRTCalc(44,44,.f.)
          if not(empty(.w_CECODESE))
          .link_2_6('Full')
          endif
        .DoRTCalc(45,45,.f.)
          if not(empty(.w_VALECC))
          .link_2_7('Full')
          endif
          .DoRTCalc(46,46,.f.)
        .w_CEDURCES = 0
        .w_CALCPIP = DEFPIP(.w_DECTOP)
          .DoRTCalc(49,51,.f.)
        .w_CEVALLIM = g_PERVAL
        .DoRTCalc(52,52,.f.)
          if not(empty(.w_CEVALLIM))
          .link_2_14('Full')
          endif
          .DoRTCalc(53,53,.f.)
        .w_CEFLAUMU = 'S'
        .w_CALCPIC = DEFPIC(.w_DECTOT)
          .DoRTCalc(56,56,.f.)
        .w_OK = .F.
        .oPgFrm.Page1.oPag.oObj_1_64.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_65.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_66.Calculate()
        .DoRTCalc(58,62,.f.)
          if not(empty(.w_CEVOCCEN))
          .link_2_30('Full')
          endif
        .DoRTCalc(63,63,.f.)
          if not(empty(.w_CECODCEN))
          .link_2_31('Full')
          endif
        .oPgFrm.Page2.oPag.oObj_2_41.Calculate()
          .DoRTCalc(64,66,.f.)
        .w_CEAMMIMM = IIF(EMPTY(.w_CEAMMIMM),'N', .w_CEAMMIMM)
        .w_CENSOAMM = IIF(.w_NSOAMM='S','S','N')
        .w_CENSOAMF = IIF(.w_NSOAMF='S','S','N')
        .w_CEAMMIMC = IIF(EMPTY(.w_CEAMMIMC),'N', .w_CEAMMIMC)
        .oPgFrm.Page1.oPag.oObj_1_74.Calculate()
        .oPgFrm.Page4.oPag.MOVIMENTI.Calculate()
        .oPgFrm.Page4.oPag.SALDI.Calculate()
          .DoRTCalc(71,73,.f.)
        .w_COD = .w_CECODICE
        .w_DES = .w_CEDESCRI
        .w_SERIALE = .w_MOVIMENTI.getVar('SERIALE')
        .w_ESPRI = .w_CEESPRIU
        .w_CAUSALE = .w_MOVIMENTI.getVar('MCCODCAU')
        .oPgFrm.Page4.oPag.oObj_4_14.Calculate(NOT(.w_FLPRIU='=' OR .w_FLPRIC='=') )
        .w_FLDADI = .w_MOVIMENTI.getVar('CCFLDADI')
        .w_FLPRIC = .w_MOVIMENTI.getVar('CCFLPRIC')
        .w_FLPRIU = .w_MOVIMENTI.getVar('CCFLPRIU')
          .DoRTCalc(82,90,.f.)
        .w_CODI = .w_CECODICE
        .w_DESC = .w_CEDESCRI
        .w_ESER = .w_CODESE
      endif
    endwith
    cp_BlankRecExtFlds(this,'CES_PITI')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page4.oPag.oBtn_4_10.enabled = this.oPgFrm.Page4.oPag.oBtn_4_10.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsce_ace
    * --- Caricamento Rapido Cespiti disabilitato se provengo da gsve_kda
    IF this.cFunction='Load' AND NOT EMPTY(g_CODCESP) AND g_F4RAPIDO = 'S'AND Type('This.w_OBJMASK')<>'O'
        this.NotifyEvent('CaricaRapido')
    ENDIF
    
    * Se si � in caricamento la pagina 3 � disattivata
        this.oPgFrm.Pages[3].Enabled=Not(this.cFunction='Load')
    
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CES_PITI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CES_PITI_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"SERCE","i_CODAZI,w_CECODCAT,w_CE_SERIA")
      .op_CODAZI = .w_CODAZI
      .op_CECODCAT = .w_CECODCAT
      .op_CE_SERIA = .w_CE_SERIA
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- gsce_ace
    IF thisform.cFunction='Load'
           thisform.w_MOVIMENTI.visible = .f.
           thisform.w_SALDI.visible = .f.
           thisform.oPgFrm.Page1.oPag.oCECODCAT_1_1.setfocus()
    else
          thisform.w_SALDI.visible = .t.
          thisform.w_MOVIMENTI.visible = .t.
    endif
    IF thisform.cFunction='Edit'
        thisform.oPgFrm.Page1.oPag.oCECODCAT_1_1.setfocus()
    endif
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oCECODCAT_1_1.enabled = i_bVal
      .Page1.oPag.oCECODICE_1_7.enabled = i_bVal
      .Page1.oPag.oCEDESCRI_1_9.enabled = i_bVal
      .Page1.oPag.oCEDESSUP_1_10.enabled = i_bVal
      .Page1.oPag.oCETIPCES_1_11.enabled = i_bVal
      .Page1.oPag.oCE_STATO_1_12.enabled = i_bVal
      .Page1.oPag.oCESTABEN_1_13.enabled = i_bVal
      .Page1.oPag.oCEUNIMIS_1_14.enabled = i_bVal
      .Page1.oPag.oCECODUBI_1_15.enabled = i_bVal
      .Page1.oPag.oCECODMAT_1_17.enabled = i_bVal
      .Page1.oPag.oCECODFAM_1_18.enabled = i_bVal
      .Page1.oPag.oCECODPER_1_20.enabled = i_bVal
      .Page1.oPag.oCECODCOM_1_22.enabled = i_bVal
      .Page1.oPag.oCECODATT_1_23.enabled = i_bVal
      .Page1.oPag.oCEDTPRIC_1_24.enabled = i_bVal
      .Page1.oPag.oCEDTPRIU_1_26.enabled = i_bVal
      .Page1.oPag.oCEDATDIS_1_27.enabled = i_bVal
      .Page1.oPag.oCEESPRIC_1_28.enabled = i_bVal
      .Page1.oPag.oCEFLSPEM_1_29.enabled = i_bVal
      .Page1.oPag.oCEESPRIU_1_30.enabled = i_bVal
      .Page1.oPag.oCEFLCEUS_1_31.enabled = i_bVal
      .Page1.oPag.oCEDTINVA_1_32.enabled = i_bVal
      .Page1.oPag.oCEDTOBSO_1_34.enabled = i_bVal
      .Page2.oPag.oCETIPAMM_2_5.enabled = i_bVal
      .Page2.oPag.oCEDURCES_2_9.enabled = i_bVal
      .Page2.oPag.oCECOEFIS_2_11.enabled = i_bVal
      .Page2.oPag.oCEPERDEF_2_12.enabled = i_bVal
      .Page2.oPag.oCEIMPMAX_2_13.enabled = i_bVal
      .Page2.oPag.oCEVALLIM_2_14.enabled = i_bVal
      .Page2.oPag.oCEFLAUMU_2_17.enabled = i_bVal
      .Page2.oPag.oCEVOCCEN_2_30.enabled = i_bVal
      .Page2.oPag.oCECODCEN_2_31.enabled = i_bVal
      .Page2.oPag.oCEAMMIMM_2_42.enabled = i_bVal
      .Page2.oPag.oCENSOAMM_2_44.enabled = i_bVal
      .Page2.oPag.oCENSOAMF_2_45.enabled = i_bVal
      .Page2.oPag.oCEAMMIMC_2_47.enabled = i_bVal
      .Page1.oPag.oBtn_1_56.enabled = i_bVal
      .Page4.oPag.oBtn_4_10.enabled = .Page4.oPag.oBtn_4_10.mCond()
      .Page1.oPag.oObj_1_48.enabled = i_bVal
      .Page1.oPag.oObj_1_52.enabled = i_bVal
      .Page1.oPag.oObj_1_54.enabled = i_bVal
      .Page1.oPag.oObj_1_65.enabled = i_bVal
      .Page1.oPag.oObj_1_66.enabled = i_bVal
      .Page2.oPag.oObj_2_41.enabled = i_bVal
      .Page1.oPag.oObj_1_74.enabled = i_bVal
      .Page4.oPag.oObj_4_14.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oCECODICE_1_7.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oCECODCAT_1_1.enabled = .t.
        .Page1.oPag.oCECODICE_1_7.enabled = .t.
        .Page1.oPag.oCEDESCRI_1_9.enabled = .t.
        .Page1.oPag.oCEUNIMIS_1_14.enabled = .t.
        .Page1.oPag.oCECODMAT_1_17.enabled = .t.
      endif
    endwith
    this.GSCE_MCO.SetStatus(i_cOp)
    this.GSCE_MAM.SetStatus(i_cOp)
    this.GSCE_MCC.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'CES_PITI',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSCE_MCO.SetChildrenStatus(i_cOp)
  *  this.GSCE_MAM.SetChildrenStatus(i_cOp)
  *  this.GSCE_MCC.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CES_PITI_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CECODCAT,"CECODCAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CE_SERIA,"CE_SERIA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CECODICE,"CECODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CEDESCRI,"CEDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CEDESSUP,"CEDESSUP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CETIPCES,"CETIPCES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CE_STATO,"CE_STATO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CESTABEN,"CESTABEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CEUNIMIS,"CEUNIMIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CECODUBI,"CECODUBI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CECODMAT,"CECODMAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CECODFAM,"CECODFAM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CECODPER,"CECODPER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CECODCOM,"CECODCOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CECODATT,"CECODATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CEDTPRIC,"CEDTPRIC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CEDTPRIU,"CEDTPRIU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CEDATDIS,"CEDATDIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CEESPRIC,"CEESPRIC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CEFLSPEM,"CEFLSPEM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CEESPRIU,"CEESPRIU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CEFLCEUS,"CEFLCEUS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CEDTINVA,"CEDTINVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CEDTOBSO,"CEDTOBSO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CETIPAMM,"CETIPAMM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CECODESE,"CECODESE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CEDURCES,"CEDURCES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CECOEFIS,"CECOEFIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CEPERDEF,"CEPERDEF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CEIMPMAX,"CEIMPMAX",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CEVALLIM,"CEVALLIM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CEFLAUMU,"CEFLAUMU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CEVOCCEN,"CEVOCCEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CECODCEN,"CECODCEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CEAMMIMM,"CEAMMIMM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CENSOAMM,"CENSOAMM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CENSOAMF,"CENSOAMF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CEAMMIMC,"CEAMMIMC",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.CES_PITI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CES_PITI_IDX,2])
    i_lTable = "CES_PITI"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.CES_PITI_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSCE_KCE with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CES_PITI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CES_PITI_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.CES_PITI_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"SERCE","i_CODAZI,w_CECODCAT,w_CE_SERIA")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into CES_PITI
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CES_PITI')
        i_extval=cp_InsertValODBCExtFlds(this,'CES_PITI')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(CECODCAT,CE_SERIA,CECODICE,CEDESCRI,CEDESSUP"+;
                  ",CETIPCES,CE_STATO,CESTABEN,CEUNIMIS,CECODUBI"+;
                  ",CECODMAT,CECODFAM,CECODPER,CECODCOM,CECODATT"+;
                  ",CEDTPRIC,CEDTPRIU,CEDATDIS,CEESPRIC,CEFLSPEM"+;
                  ",CEESPRIU,CEFLCEUS,CEDTINVA,CEDTOBSO,UTCC"+;
                  ",UTCV,UTDC,UTDV,CETIPAMM,CECODESE"+;
                  ",CEDURCES,CECOEFIS,CEPERDEF,CEIMPMAX,CEVALLIM"+;
                  ",CEFLAUMU,CEVOCCEN,CECODCEN,CEAMMIMM,CENSOAMM"+;
                  ",CENSOAMF,CEAMMIMC "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBCNull(this.w_CECODCAT)+;
                  ","+cp_ToStrODBC(this.w_CE_SERIA)+;
                  ","+cp_ToStrODBC(this.w_CECODICE)+;
                  ","+cp_ToStrODBC(this.w_CEDESCRI)+;
                  ","+cp_ToStrODBC(this.w_CEDESSUP)+;
                  ","+cp_ToStrODBC(this.w_CETIPCES)+;
                  ","+cp_ToStrODBC(this.w_CE_STATO)+;
                  ","+cp_ToStrODBC(this.w_CESTABEN)+;
                  ","+cp_ToStrODBCNull(this.w_CEUNIMIS)+;
                  ","+cp_ToStrODBCNull(this.w_CECODUBI)+;
                  ","+cp_ToStrODBC(this.w_CECODMAT)+;
                  ","+cp_ToStrODBCNull(this.w_CECODFAM)+;
                  ","+cp_ToStrODBCNull(this.w_CECODPER)+;
                  ","+cp_ToStrODBCNull(this.w_CECODCOM)+;
                  ","+cp_ToStrODBCNull(this.w_CECODATT)+;
                  ","+cp_ToStrODBC(this.w_CEDTPRIC)+;
                  ","+cp_ToStrODBC(this.w_CEDTPRIU)+;
                  ","+cp_ToStrODBC(this.w_CEDATDIS)+;
                  ","+cp_ToStrODBCNull(this.w_CEESPRIC)+;
                  ","+cp_ToStrODBC(this.w_CEFLSPEM)+;
                  ","+cp_ToStrODBCNull(this.w_CEESPRIU)+;
                  ","+cp_ToStrODBC(this.w_CEFLCEUS)+;
                  ","+cp_ToStrODBC(this.w_CEDTINVA)+;
                  ","+cp_ToStrODBC(this.w_CEDTOBSO)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                  ","+cp_ToStrODBC(this.w_CETIPAMM)+;
                  ","+cp_ToStrODBCNull(this.w_CECODESE)+;
                  ","+cp_ToStrODBC(this.w_CEDURCES)+;
                  ","+cp_ToStrODBC(this.w_CECOEFIS)+;
                  ","+cp_ToStrODBC(this.w_CEPERDEF)+;
                  ","+cp_ToStrODBC(this.w_CEIMPMAX)+;
                  ","+cp_ToStrODBCNull(this.w_CEVALLIM)+;
                  ","+cp_ToStrODBC(this.w_CEFLAUMU)+;
                  ","+cp_ToStrODBCNull(this.w_CEVOCCEN)+;
                  ","+cp_ToStrODBCNull(this.w_CECODCEN)+;
                  ","+cp_ToStrODBC(this.w_CEAMMIMM)+;
                  ","+cp_ToStrODBC(this.w_CENSOAMM)+;
                  ","+cp_ToStrODBC(this.w_CENSOAMF)+;
                  ","+cp_ToStrODBC(this.w_CEAMMIMC)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CES_PITI')
        i_extval=cp_InsertValVFPExtFlds(this,'CES_PITI')
        cp_CheckDeletedKey(i_cTable,0,'CECODICE',this.w_CECODICE)
        INSERT INTO (i_cTable);
              (CECODCAT,CE_SERIA,CECODICE,CEDESCRI,CEDESSUP,CETIPCES,CE_STATO,CESTABEN,CEUNIMIS,CECODUBI,CECODMAT,CECODFAM,CECODPER,CECODCOM,CECODATT,CEDTPRIC,CEDTPRIU,CEDATDIS,CEESPRIC,CEFLSPEM,CEESPRIU,CEFLCEUS,CEDTINVA,CEDTOBSO,UTCC,UTCV,UTDC,UTDV,CETIPAMM,CECODESE,CEDURCES,CECOEFIS,CEPERDEF,CEIMPMAX,CEVALLIM,CEFLAUMU,CEVOCCEN,CECODCEN,CEAMMIMM,CENSOAMM,CENSOAMF,CEAMMIMC  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_CECODCAT;
                  ,this.w_CE_SERIA;
                  ,this.w_CECODICE;
                  ,this.w_CEDESCRI;
                  ,this.w_CEDESSUP;
                  ,this.w_CETIPCES;
                  ,this.w_CE_STATO;
                  ,this.w_CESTABEN;
                  ,this.w_CEUNIMIS;
                  ,this.w_CECODUBI;
                  ,this.w_CECODMAT;
                  ,this.w_CECODFAM;
                  ,this.w_CECODPER;
                  ,this.w_CECODCOM;
                  ,this.w_CECODATT;
                  ,this.w_CEDTPRIC;
                  ,this.w_CEDTPRIU;
                  ,this.w_CEDATDIS;
                  ,this.w_CEESPRIC;
                  ,this.w_CEFLSPEM;
                  ,this.w_CEESPRIU;
                  ,this.w_CEFLCEUS;
                  ,this.w_CEDTINVA;
                  ,this.w_CEDTOBSO;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
                  ,this.w_CETIPAMM;
                  ,this.w_CECODESE;
                  ,this.w_CEDURCES;
                  ,this.w_CECOEFIS;
                  ,this.w_CEPERDEF;
                  ,this.w_CEIMPMAX;
                  ,this.w_CEVALLIM;
                  ,this.w_CEFLAUMU;
                  ,this.w_CEVOCCEN;
                  ,this.w_CECODCEN;
                  ,this.w_CEAMMIMM;
                  ,this.w_CENSOAMM;
                  ,this.w_CENSOAMF;
                  ,this.w_CEAMMIMC;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.CES_PITI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CES_PITI_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.CES_PITI_IDX,i_nConn)
      *
      * update CES_PITI
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'CES_PITI')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " CECODCAT="+cp_ToStrODBCNull(this.w_CECODCAT)+;
             ",CE_SERIA="+cp_ToStrODBC(this.w_CE_SERIA)+;
             ",CEDESCRI="+cp_ToStrODBC(this.w_CEDESCRI)+;
             ",CEDESSUP="+cp_ToStrODBC(this.w_CEDESSUP)+;
             ",CETIPCES="+cp_ToStrODBC(this.w_CETIPCES)+;
             ",CE_STATO="+cp_ToStrODBC(this.w_CE_STATO)+;
             ",CESTABEN="+cp_ToStrODBC(this.w_CESTABEN)+;
             ",CEUNIMIS="+cp_ToStrODBCNull(this.w_CEUNIMIS)+;
             ",CECODUBI="+cp_ToStrODBCNull(this.w_CECODUBI)+;
             ",CECODMAT="+cp_ToStrODBC(this.w_CECODMAT)+;
             ",CECODFAM="+cp_ToStrODBCNull(this.w_CECODFAM)+;
             ",CECODPER="+cp_ToStrODBCNull(this.w_CECODPER)+;
             ",CECODCOM="+cp_ToStrODBCNull(this.w_CECODCOM)+;
             ",CECODATT="+cp_ToStrODBCNull(this.w_CECODATT)+;
             ",CEDTPRIC="+cp_ToStrODBC(this.w_CEDTPRIC)+;
             ",CEDTPRIU="+cp_ToStrODBC(this.w_CEDTPRIU)+;
             ",CEDATDIS="+cp_ToStrODBC(this.w_CEDATDIS)+;
             ",CEESPRIC="+cp_ToStrODBCNull(this.w_CEESPRIC)+;
             ",CEFLSPEM="+cp_ToStrODBC(this.w_CEFLSPEM)+;
             ",CEESPRIU="+cp_ToStrODBCNull(this.w_CEESPRIU)+;
             ",CEFLCEUS="+cp_ToStrODBC(this.w_CEFLCEUS)+;
             ",CEDTINVA="+cp_ToStrODBC(this.w_CEDTINVA)+;
             ",CEDTOBSO="+cp_ToStrODBC(this.w_CEDTOBSO)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",CETIPAMM="+cp_ToStrODBC(this.w_CETIPAMM)+;
             ",CECODESE="+cp_ToStrODBCNull(this.w_CECODESE)+;
             ",CEDURCES="+cp_ToStrODBC(this.w_CEDURCES)+;
             ",CECOEFIS="+cp_ToStrODBC(this.w_CECOEFIS)+;
             ",CEPERDEF="+cp_ToStrODBC(this.w_CEPERDEF)+;
             ",CEIMPMAX="+cp_ToStrODBC(this.w_CEIMPMAX)+;
             ",CEVALLIM="+cp_ToStrODBCNull(this.w_CEVALLIM)+;
             ",CEFLAUMU="+cp_ToStrODBC(this.w_CEFLAUMU)+;
             ",CEVOCCEN="+cp_ToStrODBCNull(this.w_CEVOCCEN)+;
             ",CECODCEN="+cp_ToStrODBCNull(this.w_CECODCEN)+;
             ",CEAMMIMM="+cp_ToStrODBC(this.w_CEAMMIMM)+;
             ",CENSOAMM="+cp_ToStrODBC(this.w_CENSOAMM)+;
             ",CENSOAMF="+cp_ToStrODBC(this.w_CENSOAMF)+;
             ",CEAMMIMC="+cp_ToStrODBC(this.w_CEAMMIMC)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'CES_PITI')
        i_cWhere = cp_PKFox(i_cTable  ,'CECODICE',this.w_CECODICE  )
        UPDATE (i_cTable) SET;
              CECODCAT=this.w_CECODCAT;
             ,CE_SERIA=this.w_CE_SERIA;
             ,CEDESCRI=this.w_CEDESCRI;
             ,CEDESSUP=this.w_CEDESSUP;
             ,CETIPCES=this.w_CETIPCES;
             ,CE_STATO=this.w_CE_STATO;
             ,CESTABEN=this.w_CESTABEN;
             ,CEUNIMIS=this.w_CEUNIMIS;
             ,CECODUBI=this.w_CECODUBI;
             ,CECODMAT=this.w_CECODMAT;
             ,CECODFAM=this.w_CECODFAM;
             ,CECODPER=this.w_CECODPER;
             ,CECODCOM=this.w_CECODCOM;
             ,CECODATT=this.w_CECODATT;
             ,CEDTPRIC=this.w_CEDTPRIC;
             ,CEDTPRIU=this.w_CEDTPRIU;
             ,CEDATDIS=this.w_CEDATDIS;
             ,CEESPRIC=this.w_CEESPRIC;
             ,CEFLSPEM=this.w_CEFLSPEM;
             ,CEESPRIU=this.w_CEESPRIU;
             ,CEFLCEUS=this.w_CEFLCEUS;
             ,CEDTINVA=this.w_CEDTINVA;
             ,CEDTOBSO=this.w_CEDTOBSO;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             ,CETIPAMM=this.w_CETIPAMM;
             ,CECODESE=this.w_CECODESE;
             ,CEDURCES=this.w_CEDURCES;
             ,CECOEFIS=this.w_CECOEFIS;
             ,CEPERDEF=this.w_CEPERDEF;
             ,CEIMPMAX=this.w_CEIMPMAX;
             ,CEVALLIM=this.w_CEVALLIM;
             ,CEFLAUMU=this.w_CEFLAUMU;
             ,CEVOCCEN=this.w_CEVOCCEN;
             ,CECODCEN=this.w_CECODCEN;
             ,CEAMMIMM=this.w_CEAMMIMM;
             ,CENSOAMM=this.w_CENSOAMM;
             ,CENSOAMF=this.w_CENSOAMF;
             ,CEAMMIMC=this.w_CEAMMIMC;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSCE_MCO : Saving
      this.GSCE_MCO.ChangeRow(this.cRowID+'      1',0;
             ,this.w_CECODICE,"COCODICE";
             )
      this.GSCE_MCO.mReplace()
      * --- GSCE_MAM : Saving
      this.GSCE_MAM.ChangeRow(this.cRowID+'      1',0;
             ,this.w_CECODICE,"AMCODCES";
             )
      this.GSCE_MAM.mReplace()
      * --- GSCE_MCC : Saving
      this.GSCE_MCC.ChangeRow(this.cRowID+'      1',0;
             ,this.w_CECODICE,"ACCODCES";
             )
      this.GSCE_MCC.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- gsce_ace
    * valorizzo maschera documenti
    local mess
    If this.cfunction='Load'
    If Type('This.w_OBJMASK.Caption')='C'
       LOCAL l_CTRL
       This.w_OBJMASK.w_MVCODCES=this.w_CECODICE
       This.w_OBJMASK.w_DESCCESP=this.w_CEDESCRI
       This.w_OBJMASK.w_CETIPO= this.w_CETIPCES
       This.w_OBJMASK.w_STABEN= this.w_CESTABEN
       l_CTRL=This.w_OBJMASK.GETCTRL('w_MVCODCES')
       l_CTRL.CHECK()
       IF EMPTY(This.w_OBJMASK.w_MVCODCES)
         IF NOT(ah_yesno("Cespite incongruente con il documento, l'abbinamento non verr� effettuato%0Si desidera comunque mantenere il cespite?"))
           bTrsErr=.T.
          i_TrsMsg='Transazione abbandonata'
           this.ecpquit()
          ENDIF
    
       ENDIF
       This.w_OBJMASK.mcalc(.T.)
    
    Endif
    Endif
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSCE_MCO : Deleting
    this.GSCE_MCO.ChangeRow(this.cRowID+'      1',0;
           ,this.w_CECODICE,"COCODICE";
           )
    this.GSCE_MCO.mDelete()
    * --- GSCE_MAM : Deleting
    this.GSCE_MAM.ChangeRow(this.cRowID+'      1',0;
           ,this.w_CECODICE,"AMCODCES";
           )
    this.GSCE_MAM.mDelete()
    * --- GSCE_MCC : Deleting
    this.GSCE_MCC.ChangeRow(this.cRowID+'      1',0;
           ,this.w_CECODICE,"ACCODCES";
           )
    this.GSCE_MCC.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.CES_PITI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CES_PITI_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.CES_PITI_IDX,i_nConn)
      *
      * delete CES_PITI
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'CECODICE',this.w_CECODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CES_PITI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CES_PITI_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
            .w_CE_SERIA = iif(!empty(.w_CE_SERIA), .w_CE_SERIA, SPACE(5))
        .DoRTCalc(3,4,.t.)
          .link_1_5('Full')
        .DoRTCalc(6,6,.t.)
        if .o_CECODCAT<>.w_CECODCAT
            .w_CECODICE = IIF(EMPTY(.w_PREFISSO) or this.cfunction='Edit',.w_CECODICE,ALLTRIM(.w_PREFISSO) + .w_CE_SERIA)
        endif
        if .o_CECODICE<>.w_CECODICE
          .link_1_8('Full')
        endif
        .DoRTCalc(9,13,.t.)
        if .o_CETIPCES<>.w_CETIPCES.or. .o_CEUNIMIS<>.w_CEUNIMIS
            .w_CEUNIMIS = IIF(.w_CETIPCES='CQ',.w_CEUNIMIS,Space(3))
          .link_1_14('Full')
        endif
        if .o_CETIPCES<>.w_CETIPCES
            .w_CECODUBI = IIF(.w_CETIPCES $ 'CC-PC',SPACE(20),.w_CECODUBI)
          .link_1_15('Full')
        endif
        .DoRTCalc(16,16,.t.)
        if .o_CETIPCES<>.w_CETIPCES
            .w_CECODMAT = IIF(.w_CETIPCES $ 'CC-PC',SPACE(20),.w_CECODMAT)
        endif
        .DoRTCalc(18,26,.t.)
        if .o_CESTABEN<>.w_CESTABEN
            .w_CEDATDIS = IIF(.w_CESTABEN='C' OR .w_CESTABEN='E',.w_CEDATDIS,cp_CharToDate('  -  -  '))
        endif
        if .o_CEDTPRIC<>.w_CEDTPRIC
            .w_CEESPRIC = calceser(.w_CEDTPRIC, space(4))
          .link_1_28('Full')
        endif
        .DoRTCalc(29,29,.t.)
        if .o_CEDTPRIU<>.w_CEDTPRIU
            .w_CEESPRIU = calceser(.w_CEDTPRIU, space(4))
          .link_1_30('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_48.Calculate()
        .DoRTCalc(31,37,.t.)
        if .o_CECODICE<>.w_CECODICE
            .w_OLDCAT = .w_CECODCAT
        endif
        .oPgFrm.Page1.oPag.oObj_1_52.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_54.Calculate()
          .link_2_1('Full')
        .DoRTCalc(40,40,.t.)
            .w_CODI = .w_CECODICE
            .w_DESC = .w_CEDESCRI
        if .o_CECODICE<>.w_CECODICE
            .w_CETIPAMM = IIF(EMPTY(.w_CECODESE), .w_PCTIPAMM, .w_CETIPAMM)
        endif
          .link_2_6('Full')
        if .o_CECODESE<>.w_CECODESE
          .link_2_7('Full')
        endif
        .DoRTCalc(46,47,.t.)
        if .o_CECODESE<>.w_CECODESE
            .w_CALCPIP = DEFPIP(.w_DECTOP)
        endif
        .DoRTCalc(49,54,.t.)
        if .o_CEVALLIM<>.w_CEVALLIM
            .w_CALCPIC = DEFPIC(.w_DECTOT)
        endif
        .oPgFrm.Page1.oPag.oObj_1_64.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_65.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_66.Calculate()
        if  .o_FLRIDU<>.w_FLRIDU.or. .o_CECOEFIS<>.w_CECOEFIS.or. .o_TIPBEN<>.w_TIPBEN
          .WriteTo_GSCE_MAM()
        endif
        .oPgFrm.Page2.oPag.oObj_2_41.Calculate()
        .DoRTCalc(56,67,.t.)
        if .o_NSOAMM<>.w_NSOAMM.or. .o_CECODCAT<>.w_CECODCAT
            .w_CENSOAMM = IIF(.w_NSOAMM='S','S','N')
        endif
        if .o_NSOAMF<>.w_NSOAMF.or. .o_CECODCAT<>.w_CECODCAT
            .w_CENSOAMF = IIF(.w_NSOAMF='S','S','N')
        endif
        .oPgFrm.Page1.oPag.oObj_1_74.Calculate()
        .oPgFrm.Page4.oPag.MOVIMENTI.Calculate()
        .oPgFrm.Page4.oPag.SALDI.Calculate()
        .DoRTCalc(70,73,.t.)
            .w_COD = .w_CECODICE
            .w_DES = .w_CEDESCRI
            .w_SERIALE = .w_MOVIMENTI.getVar('SERIALE')
            .w_ESPRI = .w_CEESPRIU
            .w_CAUSALE = .w_MOVIMENTI.getVar('MCCODCAU')
        .oPgFrm.Page4.oPag.oObj_4_14.Calculate(NOT(.w_FLPRIU='=' OR .w_FLPRIC='=') )
            .w_FLDADI = .w_MOVIMENTI.getVar('CCFLDADI')
            .w_FLPRIC = .w_MOVIMENTI.getVar('CCFLPRIC')
            .w_FLPRIU = .w_MOVIMENTI.getVar('CCFLPRIU')
        .DoRTCalc(82,90,.t.)
            .w_CODI = .w_CECODICE
            .w_DESC = .w_CEDESCRI
            .w_ESER = .w_CODESE
        * --- Area Manuale = Calculate
        * --- gsce_ace
        LOCAL OGGETTO
        OGGETTO=THIS.GETCTRL (ah_Msgformat("\<Dettagli"))
        OGGETTO.ENABLED=NOT EMPTY(THIS.w_SERIALE)  AND NOT((THIS.w_FLPRIU='S' OR THIS.w_FLPRIC='S') AND THIS.cFunction = 'Edit') 
        * --- Fine Area Manuale
        if .op_CODAZI<>.w_CODAZI .or. .op_CECODCAT<>.w_CECODCAT
           cp_AskTableProg(this,i_nConn,"SERCE","i_CODAZI,w_CECODCAT,w_CE_SERIA")
          .op_CE_SERIA = .w_CE_SERIA
        endif
        .op_CODAZI = .w_CODAZI
        .op_CECODCAT = .w_CECODCAT
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_48.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_52.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_54.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_64.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_65.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_66.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_41.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_74.Calculate()
        .oPgFrm.Page4.oPag.MOVIMENTI.Calculate()
        .oPgFrm.Page4.oPag.SALDI.Calculate()
        .oPgFrm.Page4.oPag.oObj_4_14.Calculate(NOT(.w_FLPRIU='=' OR .w_FLPRIC='=') )
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCECODICE_1_7.enabled = this.oPgFrm.Page1.oPag.oCECODICE_1_7.mCond()
    this.oPgFrm.Page1.oPag.oCEUNIMIS_1_14.enabled = this.oPgFrm.Page1.oPag.oCEUNIMIS_1_14.mCond()
    this.oPgFrm.Page1.oPag.oCECODUBI_1_15.enabled = this.oPgFrm.Page1.oPag.oCECODUBI_1_15.mCond()
    this.oPgFrm.Page1.oPag.oCECODMAT_1_17.enabled = this.oPgFrm.Page1.oPag.oCECODMAT_1_17.mCond()
    this.oPgFrm.Page1.oPag.oCECODPER_1_20.enabled = this.oPgFrm.Page1.oPag.oCECODPER_1_20.mCond()
    this.oPgFrm.Page1.oPag.oCECODCOM_1_22.enabled = this.oPgFrm.Page1.oPag.oCECODCOM_1_22.mCond()
    this.oPgFrm.Page1.oPag.oCEDTPRIC_1_24.enabled = this.oPgFrm.Page1.oPag.oCEDTPRIC_1_24.mCond()
    this.oPgFrm.Page1.oPag.oCEDTPRIU_1_26.enabled = this.oPgFrm.Page1.oPag.oCEDTPRIU_1_26.mCond()
    this.oPgFrm.Page1.oPag.oCEDATDIS_1_27.enabled = this.oPgFrm.Page1.oPag.oCEDATDIS_1_27.mCond()
    this.oPgFrm.Page1.oPag.oCEESPRIC_1_28.enabled = this.oPgFrm.Page1.oPag.oCEESPRIC_1_28.mCond()
    this.oPgFrm.Page1.oPag.oCEESPRIU_1_30.enabled = this.oPgFrm.Page1.oPag.oCEESPRIU_1_30.mCond()
    this.oPgFrm.Page2.oPag.oCETIPAMM_2_5.enabled = this.oPgFrm.Page2.oPag.oCETIPAMM_2_5.mCond()
    this.oPgFrm.Page2.oPag.oCEDURCES_2_9.enabled = this.oPgFrm.Page2.oPag.oCEDURCES_2_9.mCond()
    this.oPgFrm.Page2.oPag.oCECOEFIS_2_11.enabled = this.oPgFrm.Page2.oPag.oCECOEFIS_2_11.mCond()
    this.oPgFrm.Page2.oPag.oCEPERDEF_2_12.enabled = this.oPgFrm.Page2.oPag.oCEPERDEF_2_12.mCond()
    this.oPgFrm.Page2.oPag.oCEIMPMAX_2_13.enabled = this.oPgFrm.Page2.oPag.oCEIMPMAX_2_13.mCond()
    this.oPgFrm.Page2.oPag.oCEVALLIM_2_14.enabled = this.oPgFrm.Page2.oPag.oCEVALLIM_2_14.mCond()
    this.oPgFrm.Page2.oPag.oCEFLAUMU_2_17.enabled = this.oPgFrm.Page2.oPag.oCEFLAUMU_2_17.mCond()
    this.oPgFrm.Page2.oPag.oCEVOCCEN_2_30.enabled = this.oPgFrm.Page2.oPag.oCEVOCCEN_2_30.mCond()
    this.oPgFrm.Page2.oPag.oCECODCEN_2_31.enabled = this.oPgFrm.Page2.oPag.oCECODCEN_2_31.mCond()
    this.oPgFrm.Page2.oPag.oCEAMMIMM_2_42.enabled = this.oPgFrm.Page2.oPag.oCEAMMIMM_2_42.mCond()
    this.oPgFrm.Page2.oPag.oCENSOAMM_2_44.enabled = this.oPgFrm.Page2.oPag.oCENSOAMM_2_44.mCond()
    this.oPgFrm.Page2.oPag.oCENSOAMF_2_45.enabled = this.oPgFrm.Page2.oPag.oCENSOAMF_2_45.mCond()
    this.oPgFrm.Page2.oPag.oCEAMMIMC_2_47.enabled = this.oPgFrm.Page2.oPag.oCEAMMIMC_2_47.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_56.enabled = this.oPgFrm.Page1.oPag.oBtn_1_56.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_10.enabled = this.oPgFrm.Page4.oPag.oBtn_4_10.mCond()
    this.oPgFrm.Page1.oPag.oLinkPC_1_55.enabled = this.oPgFrm.Page1.oPag.oLinkPC_1_55.mCond()
    this.GSCE_MCC.enabled = this.oPgFrm.Page2.oPag.oLinkPC_2_49.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oLinkPC_1_55.visible=!this.oPgFrm.Page1.oPag.oLinkPC_1_55.mHide()
    this.oPgFrm.Page2.oPag.oCEVOCCEN_2_30.visible=!this.oPgFrm.Page2.oPag.oCEVOCCEN_2_30.mHide()
    this.oPgFrm.Page2.oPag.oCECODCEN_2_31.visible=!this.oPgFrm.Page2.oPag.oCECODCEN_2_31.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_32.visible=!this.oPgFrm.Page2.oPag.oStr_2_32.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_33.visible=!this.oPgFrm.Page2.oPag.oStr_2_33.mHide()
    this.oPgFrm.Page2.oPag.oDESVOC_2_34.visible=!this.oPgFrm.Page2.oPag.oDESVOC_2_34.mHide()
    this.oPgFrm.Page2.oPag.oDESCEN_2_35.visible=!this.oPgFrm.Page2.oPag.oDESCEN_2_35.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_2.visible=!this.oPgFrm.Page4.oPag.oStr_4_2.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_3.visible=!this.oPgFrm.Page4.oPag.oStr_4_3.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_4.visible=!this.oPgFrm.Page4.oPag.oStr_4_4.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_6.visible=!this.oPgFrm.Page4.oPag.oStr_4_6.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_9.visible=!this.oPgFrm.Page4.oPag.oStr_4_9.mHide()
    this.oPgFrm.Page4.oPag.oBtn_4_10.visible=!this.oPgFrm.Page4.oPag.oBtn_4_10.mHide()
    this.oPgFrm.Page4.oPag.oESPRI_4_12.visible=!this.oPgFrm.Page4.oPag.oESPRI_4_12.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_48.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_52.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_54.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_64.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_65.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_66.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_41.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_74.Event(cEvent)
      .oPgFrm.Page4.oPag.MOVIMENTI.Event(cEvent)
      .oPgFrm.Page4.oPag.SALDI.Event(cEvent)
      .oPgFrm.Page4.oPag.oObj_4_14.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CECODCAT
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_CESP_IDX,3]
    i_lTable = "CAT_CESP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_CESP_IDX,2], .t., this.CAT_CESP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_CESP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CECODCAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCE_ACT',True,'CAT_CESP')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_CECODCAT)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCPREFIS,CCTIPBEN,CCNSOAMM,CCNSOAMF,CCFLRIDU,CCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_CECODCAT))
          select CCCODICE,CCDESCRI,CCPREFIS,CCTIPBEN,CCNSOAMM,CCNSOAMF,CCFLRIDU,CCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CECODCAT)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStrODBC(trim(this.w_CECODCAT)+"%");

            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCPREFIS,CCTIPBEN,CCNSOAMM,CCNSOAMF,CCFLRIDU,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStr(trim(this.w_CECODCAT)+"%");

            select CCCODICE,CCDESCRI,CCPREFIS,CCTIPBEN,CCNSOAMM,CCNSOAMF,CCFLRIDU,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CECODCAT) and !this.bDontReportError
            deferred_cp_zoom('CAT_CESP','*','CCCODICE',cp_AbsName(oSource.parent,'oCECODCAT_1_1'),i_cWhere,'GSCE_ACT',"Categorie cespiti",'GSCE1ACE.CAT_CESP_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCPREFIS,CCTIPBEN,CCNSOAMM,CCNSOAMF,CCFLRIDU,CCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCPREFIS,CCTIPBEN,CCNSOAMM,CCNSOAMF,CCFLRIDU,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CECODCAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCPREFIS,CCTIPBEN,CCNSOAMM,CCNSOAMF,CCFLRIDU,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_CECODCAT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_CECODCAT)
            select CCCODICE,CCDESCRI,CCPREFIS,CCTIPBEN,CCNSOAMM,CCNSOAMF,CCFLRIDU,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CECODCAT = NVL(_Link_.CCCODICE,space(15))
      this.w_DESCAT = NVL(_Link_.CCDESCRI,space(40))
      this.w_PREFISSO = NVL(_Link_.CCPREFIS,space(15))
      this.w_TIPBEN = NVL(_Link_.CCTIPBEN,space(1))
      this.w_NSOAMM = NVL(_Link_.CCNSOAMM,space(1))
      this.w_NSOAMF = NVL(_Link_.CCNSOAMF,space(1))
      this.w_FLRIDU = NVL(_Link_.CCFLRIDU,space(10))
      this.w_CCOBTEST = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CECODCAT = space(15)
      endif
      this.w_DESCAT = space(40)
      this.w_PREFISSO = space(15)
      this.w_TIPBEN = space(1)
      this.w_NSOAMM = space(1)
      this.w_NSOAMF = space(1)
      this.w_FLRIDU = space(10)
      this.w_CCOBTEST = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_CCOBTEST) OR .w_CCOBTEST>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Categoria cespite inesistente oppure obsoleta")
        endif
        this.w_CECODCAT = space(15)
        this.w_DESCAT = space(40)
        this.w_PREFISSO = space(15)
        this.w_TIPBEN = space(1)
        this.w_NSOAMM = space(1)
        this.w_NSOAMF = space(1)
        this.w_FLRIDU = space(10)
        this.w_CCOBTEST = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_CESP_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_CESP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CECODCAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 8 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAT_CESP_IDX,3] and i_nFlds+8<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAT_CESP_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_1.CCCODICE as CCCODICE101"+ ",link_1_1.CCDESCRI as CCDESCRI101"+ ",link_1_1.CCPREFIS as CCPREFIS101"+ ",link_1_1.CCTIPBEN as CCTIPBEN101"+ ",link_1_1.CCNSOAMM as CCNSOAMM101"+ ",link_1_1.CCNSOAMF as CCNSOAMF101"+ ",link_1_1.CCFLRIDU as CCFLRIDU101"+ ",link_1_1.CCDTOBSO as CCDTOBSO101"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_1 on CES_PITI.CECODCAT=link_1_1.CCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_1"
          i_cKey=i_cKey+'+" and CES_PITI.CECODCAT=link_1_1.CCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CODAZI
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_CESP_IDX,3]
    i_lTable = "PAR_CESP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_CESP_IDX,2], .t., this.PAR_CESP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_CESP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PCCODAZI,PCFLCARR";
                   +" from "+i_cTable+" "+i_lTable+" where PCCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   +" and PCCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PCCODAZI',this.w_CODAZI;
                       ,'PCCODAZI',this.w_CODAZI)
            select PCCODAZI,PCFLCARR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.PCCODAZI,space(5))
      this.w_F4RAPIDO = NVL(_Link_.PCFLCARR,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_F4RAPIDO = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_CESP_IDX,2])+'\'+cp_ToStr(_Link_.PCCODAZI,1)+'\'+cp_ToStr(_Link_.PCCODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_CESP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODESE
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SAL_CESP_IDX,3]
    i_lTable = "SAL_CESP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SAL_CESP_IDX,2], .t., this.SAL_CESP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SAL_CESP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SCCODCES,SCCODESE,SCCOSBEN,SCDURBEN,SCQUOESE,SCSPEPRE,SCPERVAR,SCSPECOR,SCUNIMIS,SCQTAPREV,SCCOSUNI";
                   +" from "+i_cTable+" "+i_lTable+" where SCCODESE="+cp_ToStrODBC(this.w_CODESE);
                   +" and SCCODCES="+cp_ToStrODBC(this.w_CECODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SCCODCES',this.w_CECODICE;
                       ,'SCCODESE',this.w_CODESE)
            select SCCODCES,SCCODESE,SCCOSBEN,SCDURBEN,SCQUOESE,SCSPEPRE,SCPERVAR,SCSPECOR,SCUNIMIS,SCQTAPREV,SCCOSUNI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODESE = NVL(_Link_.SCCODESE,space(5))
      this.w_COSBEN = NVL(_Link_.SCCOSBEN,0)
      this.w_DURBEN = NVL(_Link_.SCDURBEN,0)
      this.w_QUOESE = NVL(_Link_.SCQUOESE,0)
      this.w_SPEPRE = NVL(_Link_.SCSPEPRE,0)
      this.w_PERVAR = NVL(_Link_.SCPERVAR,0)
      this.w_SPECOR = NVL(_Link_.SCSPECOR,0)
      this.w_UNIMIS = NVL(_Link_.SCUNIMIS,space(3))
      this.w_QTAPRE = NVL(_Link_.SCQTAPREV,0)
      this.w_COSUNI = NVL(_Link_.SCCOSUNI,0)
    else
      if i_cCtrl<>'Load'
        this.w_CODESE = space(5)
      endif
      this.w_COSBEN = 0
      this.w_DURBEN = 0
      this.w_QUOESE = 0
      this.w_SPEPRE = 0
      this.w_PERVAR = 0
      this.w_SPECOR = 0
      this.w_UNIMIS = space(3)
      this.w_QTAPRE = 0
      this.w_COSUNI = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SAL_CESP_IDX,2])+'\'+cp_ToStr(_Link_.SCCODCES,1)+'\'+cp_ToStr(_Link_.SCCODESE,1)
      cp_ShowWarn(i_cKey,this.SAL_CESP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CEUNIMIS
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CEUNIMIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'UNIMIS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UMCODICE like "+cp_ToStrODBC(trim(this.w_CEUNIMIS)+"%");

          i_ret=cp_SQL(i_nConn,"select UMCODICE,UMDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UMCODICE',trim(this.w_CEUNIMIS))
          select UMCODICE,UMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CEUNIMIS)==trim(_Link_.UMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CEUNIMIS) and !this.bDontReportError
            deferred_cp_zoom('UNIMIS','*','UMCODICE',cp_AbsName(oSource.parent,'oCEUNIMIS_1_14'),i_cWhere,'',"Unit� di misura",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',oSource.xKey(1))
            select UMCODICE,UMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CEUNIMIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_CEUNIMIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_CEUNIMIS)
            select UMCODICE,UMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CEUNIMIS = NVL(_Link_.UMCODICE,space(3))
      this.w_DESUNI = NVL(_Link_.UMDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CEUNIMIS = space(3)
      endif
      this.w_DESUNI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CEUNIMIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_14(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.UNIMIS_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_14.UMCODICE as UMCODICE114"+ ",link_1_14.UMDESCRI as UMDESCRI114"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_14 on CES_PITI.CEUNIMIS=link_1_14.UMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_14"
          i_cKey=i_cKey+'+" and CES_PITI.CEUNIMIS=link_1_14.UMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CECODUBI
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UBI_CESP_IDX,3]
    i_lTable = "UBI_CESP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UBI_CESP_IDX,2], .t., this.UBI_CESP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UBI_CESP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CECODUBI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCE_AUB',True,'UBI_CESP')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UBCODICE like "+cp_ToStrODBC(trim(this.w_CECODUBI)+"%");

          i_ret=cp_SQL(i_nConn,"select UBCODICE,UBDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UBCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UBCODICE',trim(this.w_CECODUBI))
          select UBCODICE,UBDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UBCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CECODUBI)==trim(_Link_.UBCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" UBDESCRI like "+cp_ToStrODBC(trim(this.w_CECODUBI)+"%");

            i_ret=cp_SQL(i_nConn,"select UBCODICE,UBDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" UBDESCRI like "+cp_ToStr(trim(this.w_CECODUBI)+"%");

            select UBCODICE,UBDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CECODUBI) and !this.bDontReportError
            deferred_cp_zoom('UBI_CESP','*','UBCODICE',cp_AbsName(oSource.parent,'oCECODUBI_1_15'),i_cWhere,'GSCE_AUB',"Ubicazioni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODICE,UBDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where UBCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UBCODICE',oSource.xKey(1))
            select UBCODICE,UBDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CECODUBI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODICE,UBDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where UBCODICE="+cp_ToStrODBC(this.w_CECODUBI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UBCODICE',this.w_CECODUBI)
            select UBCODICE,UBDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CECODUBI = NVL(_Link_.UBCODICE,space(20))
      this.w_DESUBI = NVL(_Link_.UBDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CECODUBI = space(20)
      endif
      this.w_DESUBI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UBI_CESP_IDX,2])+'\'+cp_ToStr(_Link_.UBCODICE,1)
      cp_ShowWarn(i_cKey,this.UBI_CESP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CECODUBI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_15(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.UBI_CESP_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.UBI_CESP_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_15.UBCODICE as UBCODICE115"+ ",link_1_15.UBDESCRI as UBDESCRI115"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_15 on CES_PITI.CECODUBI=link_1_15.UBCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_15"
          i_cKey=i_cKey+'+" and CES_PITI.CECODUBI=link_1_15.UBCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CECODFAM
  func Link_1_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_CESP_IDX,3]
    i_lTable = "FAM_CESP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_CESP_IDX,2], .t., this.FAM_CESP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_CESP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CECODFAM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCE_AFC',True,'FAM_CESP')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_CECODFAM)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_CECODFAM))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CECODFAM)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CECODFAM) and !this.bDontReportError
            deferred_cp_zoom('FAM_CESP','*','FACODICE',cp_AbsName(oSource.parent,'oCECODFAM_1_18'),i_cWhere,'GSCE_AFC',"Famiglie cespiti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CECODFAM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_CECODFAM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_CECODFAM)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CECODFAM = NVL(_Link_.FACODICE,space(5))
      this.w_DESFAM = NVL(_Link_.FADESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CECODFAM = space(5)
      endif
      this.w_DESFAM = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_CESP_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_CESP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CECODFAM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_18(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.FAM_CESP_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.FAM_CESP_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_18.FACODICE as FACODICE118"+ ",link_1_18.FADESCRI as FADESCRI118"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_18 on CES_PITI.CECODFAM=link_1_18.FACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_18"
          i_cKey=i_cKey+'+" and CES_PITI.CECODFAM=link_1_18.FACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CECODPER
  func Link_1_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CES_PITI_IDX,3]
    i_lTable = "CES_PITI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CES_PITI_IDX,2], .t., this.CES_PITI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CES_PITI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CECODPER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCE_ACE',True,'CES_PITI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CECODICE like "+cp_ToStrODBC(trim(this.w_CECODPER)+"%");

          i_ret=cp_SQL(i_nConn,"select CECODICE,CEDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CECODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CECODICE',trim(this.w_CECODPER))
          select CECODICE,CEDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CECODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CECODPER)==trim(_Link_.CECODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CECODPER) and !this.bDontReportError
            deferred_cp_zoom('CES_PITI','*','CECODICE',cp_AbsName(oSource.parent,'oCECODPER_1_20'),i_cWhere,'GSCE_ACE',"Elenco cespiti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CECODICE,CEDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CECODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CECODICE',oSource.xKey(1))
            select CECODICE,CEDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CECODPER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CECODICE,CEDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CECODICE="+cp_ToStrODBC(this.w_CECODPER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CECODICE',this.w_CECODPER)
            select CECODICE,CEDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CECODPER = NVL(_Link_.CECODICE,space(20))
      this.w_DESCES = NVL(_Link_.CEDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CECODPER = space(20)
      endif
      this.w_DESCES = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CES_PITI_IDX,2])+'\'+cp_ToStr(_Link_.CECODICE,1)
      cp_ShowWarn(i_cKey,this.CES_PITI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CECODPER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_20(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CES_PITI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CES_PITI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_20.CECODICE as CECODICE120"+ ",link_1_20.CEDESCRI as CEDESCRI120"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_20 on CES_PITI.CECODPER=link_1_20.CECODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_20"
          i_cKey=i_cKey+'+" and CES_PITI.CECODPER=link_1_20.CECODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CECODCOM
  func Link_1_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CECODCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACN',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_CECODCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_CECODCOM))
          select CNCODCAN,CNDESCAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CECODCOM)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStrODBC(trim(this.w_CECODCOM)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStr(trim(this.w_CECODCOM)+"%");

            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CECODCOM) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCECODCOM_1_22'),i_cWhere,'GSAR_ACN',"Commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CECODCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_CECODCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_CECODCOM)
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CECODCOM = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCOM = NVL(_Link_.CNDESCAN,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CECODCOM = space(15)
      endif
      this.w_DESCOM = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CECODCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_22(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAN_TIER_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_22.CNCODCAN as CNCODCAN122"+ ",link_1_22.CNDESCAN as CNDESCAN122"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_22 on CES_PITI.CECODCOM=link_1_22.CNCODCAN"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_22"
          i_cKey=i_cKey+'+" and CES_PITI.CECODCOM=link_1_22.CNCODCAN(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CECODATT
  func Link_1_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIMAST_IDX,3]
    i_lTable = "ATTIMAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIMAST_IDX,2], .t., this.ATTIMAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIMAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CECODATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ATTIMAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_CECODATT)+"%");

          i_ret=cp_SQL(i_nConn,"select ATCODATT,ATDESATT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODATT',trim(this.w_CECODATT))
          select ATCODATT,ATDESATT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CECODATT)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CECODATT) and !this.bDontReportError
            deferred_cp_zoom('ATTIMAST','*','ATCODATT',cp_AbsName(oSource.parent,'oCECODATT_1_23'),i_cWhere,'',"Attivit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODATT,ATDESATT";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODATT',oSource.xKey(1))
            select ATCODATT,ATDESATT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CECODATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODATT,ATDESATT";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_CECODATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODATT',this.w_CECODATT)
            select ATCODATT,ATDESATT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CECODATT = NVL(_Link_.ATCODATT,space(5))
      this.w_DESATT = NVL(_Link_.ATDESATT,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CECODATT = space(5)
      endif
      this.w_DESATT = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIMAST_IDX,2])+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIMAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CECODATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_23(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ATTIMAST_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ATTIMAST_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_23.ATCODATT as ATCODATT123"+ ",link_1_23.ATDESATT as ATDESATT123"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_23 on CES_PITI.CECODATT=link_1_23.ATCODATT"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_23"
          i_cKey=i_cKey+'+" and CES_PITI.CECODATT=link_1_23.ATCODATT(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CEESPRIC
  func Link_1_28(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CEESPRIC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_CEESPRIC)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_CODAZI;
                     ,'ESCODESE',trim(this.w_CEESPRIC))
          select ESCODAZI,ESCODESE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CEESPRIC)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CEESPRIC) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oCEESPRIC_1_28'),i_cWhere,'',"Esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Esercizio non corrispondente")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CEESPRIC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_CEESPRIC);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_CEESPRIC)
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CEESPRIC = NVL(_Link_.ESCODESE,space(4))
    else
      if i_cCtrl<>'Load'
        this.w_CEESPRIC = space(4)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CEESPRIC=calceser(.w_CEDTPRIC, space(4)) OR calceser(.w_CEDTPRIC, "OK") ="OK"
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Esercizio non corrispondente")
        endif
        this.w_CEESPRIC = space(4)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CEESPRIC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CEESPRIU
  func Link_1_30(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CEESPRIU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_CEESPRIU)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_CODAZI;
                     ,'ESCODESE',trim(this.w_CEESPRIU))
          select ESCODAZI,ESCODESE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CEESPRIU)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CEESPRIU) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oCEESPRIU_1_30'),i_cWhere,'',"Esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Esercizio non corrispondente")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CEESPRIU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_CEESPRIU);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_CEESPRIU)
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CEESPRIU = NVL(_Link_.ESCODESE,space(4))
    else
      if i_cCtrl<>'Load'
        this.w_CEESPRIU = space(4)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CEESPRIU=calceser(.w_CEDTPRIU, space(4)) OR calceser(.w_CEDTPRIU, "OK") ="OK"
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Esercizio non corrispondente")
        endif
        this.w_CEESPRIU = space(4)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CEESPRIU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODAZI
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_CESP_IDX,3]
    i_lTable = "PAR_CESP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_CESP_IDX,2], .t., this.PAR_CESP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_CESP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PCCODAZI,PCTIPAMM";
                   +" from "+i_cTable+" "+i_lTable+" where PCCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PCCODAZI',this.w_CODAZI)
            select PCCODAZI,PCTIPAMM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.PCCODAZI,space(1))
      this.w_PCTIPAMM = NVL(_Link_.PCTIPAMM,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(1)
      endif
      this.w_PCTIPAMM = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_CESP_IDX,2])+'\'+cp_ToStr(_Link_.PCCODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_CESP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CECODESE
  func Link_2_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CECODESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CECODESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ,ESFINESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_CECODESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_CECODESE)
            select ESCODAZI,ESCODESE,ESVALNAZ,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CECODESE = NVL(_Link_.ESCODESE,space(4))
      this.w_VALECC = NVL(_Link_.ESVALNAZ,space(3))
      this.w_FINESE = NVL(cp_ToDate(_Link_.ESFINESE),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CECODESE = space(4)
      endif
      this.w_VALECC = space(3)
      this.w_FINESE = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CECODESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VALECC
  func Link_2_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALECC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALECC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALECC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALECC)
            select VACODVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALECC = NVL(_Link_.VACODVAL,space(3))
      this.w_DECTOP = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_VALECC = space(3)
      endif
      this.w_DECTOP = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALECC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CEVALLIM
  func Link_2_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CEVALLIM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_CEVALLIM)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_CEVALLIM))
          select VACODVAL,VADECTOT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CEVALLIM)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CEVALLIM) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oCEVALLIM_2_14'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CEVALLIM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_CEVALLIM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_CEVALLIM)
            select VACODVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CEVALLIM = NVL(_Link_.VACODVAL,space(3))
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_CEVALLIM = space(3)
      endif
      this.w_DECTOT = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CEVALLIM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_14(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VALUTE_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_14.VACODVAL as VACODVAL214"+ ",link_2_14.VADECTOT as VADECTOT214"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_14 on CES_PITI.CEVALLIM=link_2_14.VACODVAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_14"
          i_cKey=i_cKey+'+" and CES_PITI.CEVALLIM=link_2_14.VACODVAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CEVOCCEN
  func Link_2_30(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOC_COST_IDX,3]
    i_lTable = "VOC_COST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2], .t., this.VOC_COST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CEVOCCEN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_AVC',True,'VOC_COST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VCCODICE like "+cp_ToStrODBC(trim(this.w_CEVOCCEN)+"%");

          i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VCCODICE',trim(this.w_CEVOCCEN))
          select VCCODICE,VCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CEVOCCEN)==trim(_Link_.VCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CEVOCCEN) and !this.bDontReportError
            deferred_cp_zoom('VOC_COST','*','VCCODICE',cp_AbsName(oSource.parent,'oCEVOCCEN_2_30'),i_cWhere,'GSCA_AVC',"Voci di costo/ricavo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCCODICE',oSource.xKey(1))
            select VCCODICE,VCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CEVOCCEN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(this.w_CEVOCCEN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCCODICE',this.w_CEVOCCEN)
            select VCCODICE,VCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CEVOCCEN = NVL(_Link_.VCCODICE,space(15))
      this.w_DESVOC = NVL(_Link_.VCDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CEVOCCEN = space(15)
      endif
      this.w_DESVOC = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])+'\'+cp_ToStr(_Link_.VCCODICE,1)
      cp_ShowWarn(i_cKey,this.VOC_COST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CEVOCCEN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_30(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VOC_COST_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_30.VCCODICE as VCCODICE230"+ ",link_2_30.VCDESCRI as VCDESCRI230"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_30 on CES_PITI.CEVOCCEN=link_2_30.VCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_30"
          i_cKey=i_cKey+'+" and CES_PITI.CEVOCCEN=link_2_30.VCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CECODCEN
  func Link_2_31(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CENCOST_IDX,3]
    i_lTable = "CENCOST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2], .t., this.CENCOST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CECODCEN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_ACC',True,'CENCOST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CC_CONTO like "+cp_ToStrODBC(trim(this.w_CECODCEN)+"%");

          i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CC_CONTO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CC_CONTO',trim(this.w_CECODCEN))
          select CC_CONTO,CCDESPIA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CC_CONTO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CECODCEN)==trim(_Link_.CC_CONTO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CECODCEN) and !this.bDontReportError
            deferred_cp_zoom('CENCOST','*','CC_CONTO',cp_AbsName(oSource.parent,'oCECODCEN_2_31'),i_cWhere,'GSCA_ACC',"Centri di costo/ricavo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                     +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',oSource.xKey(1))
            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CECODCEN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                   +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(this.w_CECODCEN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',this.w_CECODCEN)
            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CECODCEN = NVL(_Link_.CC_CONTO,space(15))
      this.w_DESCEN = NVL(_Link_.CCDESPIA,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CECODCEN = space(15)
      endif
      this.w_DESCEN = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])+'\'+cp_ToStr(_Link_.CC_CONTO,1)
      cp_ShowWarn(i_cKey,this.CENCOST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CECODCEN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_31(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CENCOST_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_31.CC_CONTO as CC_CONTO231"+ ",link_2_31.CCDESPIA as CCDESPIA231"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_31 on CES_PITI.CECODCEN=link_2_31.CC_CONTO"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_31"
          i_cKey=i_cKey+'+" and CES_PITI.CECODCEN=link_2_31.CC_CONTO(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCECODCAT_1_1.value==this.w_CECODCAT)
      this.oPgFrm.Page1.oPag.oCECODCAT_1_1.value=this.w_CECODCAT
    endif
    if not(this.oPgFrm.Page1.oPag.oCECODICE_1_7.value==this.w_CECODICE)
      this.oPgFrm.Page1.oPag.oCECODICE_1_7.value=this.w_CECODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oCEDESCRI_1_9.value==this.w_CEDESCRI)
      this.oPgFrm.Page1.oPag.oCEDESCRI_1_9.value=this.w_CEDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCEDESSUP_1_10.value==this.w_CEDESSUP)
      this.oPgFrm.Page1.oPag.oCEDESSUP_1_10.value=this.w_CEDESSUP
    endif
    if not(this.oPgFrm.Page1.oPag.oCETIPCES_1_11.RadioValue()==this.w_CETIPCES)
      this.oPgFrm.Page1.oPag.oCETIPCES_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCE_STATO_1_12.RadioValue()==this.w_CE_STATO)
      this.oPgFrm.Page1.oPag.oCE_STATO_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCESTABEN_1_13.RadioValue()==this.w_CESTABEN)
      this.oPgFrm.Page1.oPag.oCESTABEN_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCEUNIMIS_1_14.value==this.w_CEUNIMIS)
      this.oPgFrm.Page1.oPag.oCEUNIMIS_1_14.value=this.w_CEUNIMIS
    endif
    if not(this.oPgFrm.Page1.oPag.oCECODUBI_1_15.value==this.w_CECODUBI)
      this.oPgFrm.Page1.oPag.oCECODUBI_1_15.value=this.w_CECODUBI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESUBI_1_16.value==this.w_DESUBI)
      this.oPgFrm.Page1.oPag.oDESUBI_1_16.value=this.w_DESUBI
    endif
    if not(this.oPgFrm.Page1.oPag.oCECODMAT_1_17.value==this.w_CECODMAT)
      this.oPgFrm.Page1.oPag.oCECODMAT_1_17.value=this.w_CECODMAT
    endif
    if not(this.oPgFrm.Page1.oPag.oCECODFAM_1_18.value==this.w_CECODFAM)
      this.oPgFrm.Page1.oPag.oCECODFAM_1_18.value=this.w_CECODFAM
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFAM_1_19.value==this.w_DESFAM)
      this.oPgFrm.Page1.oPag.oDESFAM_1_19.value=this.w_DESFAM
    endif
    if not(this.oPgFrm.Page1.oPag.oCECODPER_1_20.value==this.w_CECODPER)
      this.oPgFrm.Page1.oPag.oCECODPER_1_20.value=this.w_CECODPER
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCES_1_21.value==this.w_DESCES)
      this.oPgFrm.Page1.oPag.oDESCES_1_21.value=this.w_DESCES
    endif
    if not(this.oPgFrm.Page1.oPag.oCECODCOM_1_22.value==this.w_CECODCOM)
      this.oPgFrm.Page1.oPag.oCECODCOM_1_22.value=this.w_CECODCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oCECODATT_1_23.value==this.w_CECODATT)
      this.oPgFrm.Page1.oPag.oCECODATT_1_23.value=this.w_CECODATT
    endif
    if not(this.oPgFrm.Page1.oPag.oCEDTPRIC_1_24.value==this.w_CEDTPRIC)
      this.oPgFrm.Page1.oPag.oCEDTPRIC_1_24.value=this.w_CEDTPRIC
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCOM_1_25.value==this.w_DESCOM)
      this.oPgFrm.Page1.oPag.oDESCOM_1_25.value=this.w_DESCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oCEDTPRIU_1_26.value==this.w_CEDTPRIU)
      this.oPgFrm.Page1.oPag.oCEDTPRIU_1_26.value=this.w_CEDTPRIU
    endif
    if not(this.oPgFrm.Page1.oPag.oCEDATDIS_1_27.value==this.w_CEDATDIS)
      this.oPgFrm.Page1.oPag.oCEDATDIS_1_27.value=this.w_CEDATDIS
    endif
    if not(this.oPgFrm.Page1.oPag.oCEESPRIC_1_28.value==this.w_CEESPRIC)
      this.oPgFrm.Page1.oPag.oCEESPRIC_1_28.value=this.w_CEESPRIC
    endif
    if not(this.oPgFrm.Page1.oPag.oCEFLSPEM_1_29.RadioValue()==this.w_CEFLSPEM)
      this.oPgFrm.Page1.oPag.oCEFLSPEM_1_29.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCEESPRIU_1_30.value==this.w_CEESPRIU)
      this.oPgFrm.Page1.oPag.oCEESPRIU_1_30.value=this.w_CEESPRIU
    endif
    if not(this.oPgFrm.Page1.oPag.oCEFLCEUS_1_31.RadioValue()==this.w_CEFLCEUS)
      this.oPgFrm.Page1.oPag.oCEFLCEUS_1_31.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCEDTINVA_1_32.value==this.w_CEDTINVA)
      this.oPgFrm.Page1.oPag.oCEDTINVA_1_32.value=this.w_CEDTINVA
    endif
    if not(this.oPgFrm.Page1.oPag.oCEDTOBSO_1_34.value==this.w_CEDTOBSO)
      this.oPgFrm.Page1.oPag.oCEDTOBSO_1_34.value=this.w_CEDTOBSO
    endif
    if not(this.oPgFrm.Page2.oPag.oCODI_2_3.value==this.w_CODI)
      this.oPgFrm.Page2.oPag.oCODI_2_3.value=this.w_CODI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESC_2_4.value==this.w_DESC)
      this.oPgFrm.Page2.oPag.oDESC_2_4.value=this.w_DESC
    endif
    if not(this.oPgFrm.Page2.oPag.oCETIPAMM_2_5.RadioValue()==this.w_CETIPAMM)
      this.oPgFrm.Page2.oPag.oCETIPAMM_2_5.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCEDURCES_2_9.value==this.w_CEDURCES)
      this.oPgFrm.Page2.oPag.oCEDURCES_2_9.value=this.w_CEDURCES
    endif
    if not(this.oPgFrm.Page2.oPag.oCECOEFIS_2_11.value==this.w_CECOEFIS)
      this.oPgFrm.Page2.oPag.oCECOEFIS_2_11.value=this.w_CECOEFIS
    endif
    if not(this.oPgFrm.Page2.oPag.oCEPERDEF_2_12.value==this.w_CEPERDEF)
      this.oPgFrm.Page2.oPag.oCEPERDEF_2_12.value=this.w_CEPERDEF
    endif
    if not(this.oPgFrm.Page2.oPag.oCEIMPMAX_2_13.value==this.w_CEIMPMAX)
      this.oPgFrm.Page2.oPag.oCEIMPMAX_2_13.value=this.w_CEIMPMAX
    endif
    if not(this.oPgFrm.Page2.oPag.oCEVALLIM_2_14.RadioValue()==this.w_CEVALLIM)
      this.oPgFrm.Page2.oPag.oCEVALLIM_2_14.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCEFLAUMU_2_17.RadioValue()==this.w_CEFLAUMU)
      this.oPgFrm.Page2.oPag.oCEFLAUMU_2_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESATT_1_58.value==this.w_DESATT)
      this.oPgFrm.Page1.oPag.oDESATT_1_58.value=this.w_DESATT
    endif
    if not(this.oPgFrm.Page1.oPag.oDESUNI_1_59.value==this.w_DESUNI)
      this.oPgFrm.Page1.oPag.oDESUNI_1_59.value=this.w_DESUNI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAT_1_62.value==this.w_DESCAT)
      this.oPgFrm.Page1.oPag.oDESCAT_1_62.value=this.w_DESCAT
    endif
    if not(this.oPgFrm.Page2.oPag.oCEVOCCEN_2_30.value==this.w_CEVOCCEN)
      this.oPgFrm.Page2.oPag.oCEVOCCEN_2_30.value=this.w_CEVOCCEN
    endif
    if not(this.oPgFrm.Page2.oPag.oCECODCEN_2_31.value==this.w_CECODCEN)
      this.oPgFrm.Page2.oPag.oCECODCEN_2_31.value=this.w_CECODCEN
    endif
    if not(this.oPgFrm.Page2.oPag.oDESVOC_2_34.value==this.w_DESVOC)
      this.oPgFrm.Page2.oPag.oDESVOC_2_34.value=this.w_DESVOC
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCEN_2_35.value==this.w_DESCEN)
      this.oPgFrm.Page2.oPag.oDESCEN_2_35.value=this.w_DESCEN
    endif
    if not(this.oPgFrm.Page2.oPag.oCEAMMIMM_2_42.RadioValue()==this.w_CEAMMIMM)
      this.oPgFrm.Page2.oPag.oCEAMMIMM_2_42.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCENSOAMM_2_44.RadioValue()==this.w_CENSOAMM)
      this.oPgFrm.Page2.oPag.oCENSOAMM_2_44.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCENSOAMF_2_45.RadioValue()==this.w_CENSOAMF)
      this.oPgFrm.Page2.oPag.oCENSOAMF_2_45.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCEAMMIMC_2_47.RadioValue()==this.w_CEAMMIMC)
      this.oPgFrm.Page2.oPag.oCEAMMIMC_2_47.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFLRIDU_2_48.RadioValue()==this.w_FLRIDU)
      this.oPgFrm.Page2.oPag.oFLRIDU_2_48.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oCOD_4_7.value==this.w_COD)
      this.oPgFrm.Page4.oPag.oCOD_4_7.value=this.w_COD
    endif
    if not(this.oPgFrm.Page4.oPag.oDES_4_8.value==this.w_DES)
      this.oPgFrm.Page4.oPag.oDES_4_8.value=this.w_DES
    endif
    if not(this.oPgFrm.Page4.oPag.oESPRI_4_12.value==this.w_ESPRI)
      this.oPgFrm.Page4.oPag.oESPRI_4_12.value=this.w_ESPRI
    endif
    if not(this.oPgFrm.Page3.oPag.oCOSBEN_3_9.value==this.w_COSBEN)
      this.oPgFrm.Page3.oPag.oCOSBEN_3_9.value=this.w_COSBEN
    endif
    if not(this.oPgFrm.Page3.oPag.oSPEPRE_3_10.value==this.w_SPEPRE)
      this.oPgFrm.Page3.oPag.oSPEPRE_3_10.value=this.w_SPEPRE
    endif
    if not(this.oPgFrm.Page3.oPag.oQUOESE_3_11.value==this.w_QUOESE)
      this.oPgFrm.Page3.oPag.oQUOESE_3_11.value=this.w_QUOESE
    endif
    if not(this.oPgFrm.Page3.oPag.oSPECOR_3_12.value==this.w_SPECOR)
      this.oPgFrm.Page3.oPag.oSPECOR_3_12.value=this.w_SPECOR
    endif
    if not(this.oPgFrm.Page3.oPag.oCOSUNI_3_13.value==this.w_COSUNI)
      this.oPgFrm.Page3.oPag.oCOSUNI_3_13.value=this.w_COSUNI
    endif
    if not(this.oPgFrm.Page3.oPag.oUNIMIS_3_14.value==this.w_UNIMIS)
      this.oPgFrm.Page3.oPag.oUNIMIS_3_14.value=this.w_UNIMIS
    endif
    if not(this.oPgFrm.Page3.oPag.oDURBEN_3_15.value==this.w_DURBEN)
      this.oPgFrm.Page3.oPag.oDURBEN_3_15.value=this.w_DURBEN
    endif
    if not(this.oPgFrm.Page3.oPag.oPERVAR_3_16.value==this.w_PERVAR)
      this.oPgFrm.Page3.oPag.oPERVAR_3_16.value=this.w_PERVAR
    endif
    if not(this.oPgFrm.Page3.oPag.oQTAPRE_3_17.value==this.w_QTAPRE)
      this.oPgFrm.Page3.oPag.oQTAPRE_3_17.value=this.w_QTAPRE
    endif
    if not(this.oPgFrm.Page3.oPag.oCODI_3_19.value==this.w_CODI)
      this.oPgFrm.Page3.oPag.oCODI_3_19.value=this.w_CODI
    endif
    if not(this.oPgFrm.Page3.oPag.oDESC_3_20.value==this.w_DESC)
      this.oPgFrm.Page3.oPag.oDESC_3_20.value=this.w_DESC
    endif
    if not(this.oPgFrm.Page3.oPag.oESER_3_23.value==this.w_ESER)
      this.oPgFrm.Page3.oPag.oESER_3_23.value=this.w_ESER
    endif
    cp_SetControlsValueExtFlds(this,'CES_PITI')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case not(.w_CETIPCES<>'CQ' OR (NOT EMPTY(.w_CEUNIMIS) AND .w_CETIPCES='CQ'))
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Attenzione: cespite a quantit� e unit� di misura mancante")
          case   ((empty(.w_CECODCAT)) or not(EMPTY(.w_CCOBTEST) OR .w_CCOBTEST>.w_OBTEST))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCECODCAT_1_1.SetFocus()
            i_bnoObbl = !empty(.w_CECODCAT)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Categoria cespite inesistente oppure obsoleta")
          case   ((empty(.w_CESTABEN)) or not(.w_CESTABEN<>'N' or empty(.w_CEDTPRIC)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCESTABEN_1_13.SetFocus()
            i_bnoObbl = !empty(.w_CESTABEN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valore non ammesso: data di primo utilizzo civile impostata.")
          case   (empty(.w_CECODPER))  and (.w_CETIPCES $ 'PS-PC')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCECODPER_1_20.SetFocus()
            i_bnoObbl = !empty(.w_CECODPER)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CEDATDIS))  and (.w_CESTABEN='C' OR .w_CESTABEN='E')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCEDATDIS_1_27.SetFocus()
            i_bnoObbl = !empty(.w_CEDATDIS)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_CEESPRIC)) or not(.w_CEESPRIC=calceser(.w_CEDTPRIC, space(4)) OR calceser(.w_CEDTPRIC, "OK") ="OK"))  and (not empty(.w_CEDTPRIC) AND .w_PCTIPAMM<>'F')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCEESPRIC_1_28.SetFocus()
            i_bnoObbl = !empty(.w_CEESPRIC)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Esercizio non corrispondente")
          case   ((empty(.w_CEESPRIU)) or not(.w_CEESPRIU=calceser(.w_CEDTPRIU, space(4)) OR calceser(.w_CEDTPRIU, "OK") ="OK"))  and (not empty(.w_CEDTPRIU) AND .w_PCTIPAMM<>'C')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCEESPRIU_1_30.SetFocus()
            i_bnoObbl = !empty(.w_CEESPRIU)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Esercizio non corrispondente")
          case   not(.w_CECOEFIS=0 OR .w_CEPERDEF>0 OR .w_CETIPAMM='C')  and (.w_CETIPAMM<>'C' AND .w_PCTIPAMM<>'C' AND NOT EMPTY(.w_CEESPRIU) AND .w_CENSOAMF<>'S' AND .w_CEAMMIMM<>'S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCEPERDEF_2_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      *i_bRes = i_bRes .and. .GSCE_MCO.CheckForm()
      if i_bres
        i_bres=  .GSCE_MCO.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      *i_bRes = i_bRes .and. .GSCE_MAM.CheckForm()
      if i_bres
        i_bres=  .GSCE_MAM.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=2
        endif
      endif
      *i_bRes = i_bRes .and. .GSCE_MCC.CheckForm()
      if i_bres
        i_bres=  .GSCE_MCC.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=2
        endif
      endif
      * --- Area Manuale = Check Form
      * --- gsce_ace
      *Effettua il controllo sul campo Deducibilit� fiscale all'F10
      if .w_CEPERDEF=0 AND .w_CECOEFIS>0 AND .w_CETIPAMM<>'C'
                  i_bRes = .f.
                  i_bnoChk = .f.
                  i_cErrorMsg = ah_MsgFormat("Percentuale di deducibilit� dell'accantonamento fiscale non definita")
      Endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CECODCAT = this.w_CECODCAT
    this.o_NSOAMM = this.w_NSOAMM
    this.o_CECODICE = this.w_CECODICE
    this.o_CETIPCES = this.w_CETIPCES
    this.o_CESTABEN = this.w_CESTABEN
    this.o_CEUNIMIS = this.w_CEUNIMIS
    this.o_CEDTPRIC = this.w_CEDTPRIC
    this.o_CEDTPRIU = this.w_CEDTPRIU
    this.o_CECODESE = this.w_CECODESE
    this.o_CECOEFIS = this.w_CECOEFIS
    this.o_CEVALLIM = this.w_CEVALLIM
    this.o_TIPBEN = this.w_TIPBEN
    this.o_NSOAMF = this.w_NSOAMF
    this.o_FLRIDU = this.w_FLRIDU
    * --- GSCE_MCO : Depends On
    this.GSCE_MCO.SaveDependsOn()
    * --- GSCE_MAM : Depends On
    this.GSCE_MAM.SaveDependsOn()
    * --- GSCE_MCC : Depends On
    this.GSCE_MCC.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgsce_acePag1 as StdContainer
  Width  = 784
  height = 541
  stdWidth  = 784
  stdheight = 541
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCECODCAT_1_1 as StdField with uid="YMGQWSIDKA",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CECODCAT", cQueryName = "CECODCAT",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Categoria cespite inesistente oppure obsoleta",;
    ToolTipText = "Categoria di appartenenza del cespite",;
    HelpContextID = 17380474,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=113, Top=15, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAT_CESP", cZoomOnZoom="GSCE_ACT", oKey_1_1="CCCODICE", oKey_1_2="this.w_CECODCAT"

  func oCECODCAT_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oCECODCAT_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCECODCAT_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_CESP','*','CCCODICE',cp_AbsName(this.parent,'oCECODCAT_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCE_ACT',"Categorie cespiti",'GSCE1ACE.CAT_CESP_VZM',this.parent.oContained
  endproc
  proc oCECODCAT_1_1.mZoomOnZoom
    local i_obj
    i_obj=GSCE_ACT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_CECODCAT
     i_obj.ecpSave()
  endproc

  add object oCECODICE_1_7 as StdField with uid="NUXRTCSWHO",rtseq=7,rtrep=.f.,;
    cFormVar = "w_CECODICE", cQueryName = "CECODICE",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice del cespite",;
    HelpContextID = 118043755,;
   bGlobalFont=.t.,;
    Height=21, Width=156, Left=113, Top=47, InputMask=replicate('X',20)

  func oCECODICE_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not Empty(.w_Cecodcat))
    endwith
   endif
  endfunc

  func oCECODICE_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_CODESE)
        bRes2=.link_1_8('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oCEDESCRI_1_9 as StdField with uid="UIVCYWLIXY",rtseq=9,rtrep=.f.,;
    cFormVar = "w_CEDESCRI", cQueryName = "CECODCAT,CEDESCRI",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione del cespite",;
    HelpContextID = 32457839,;
   bGlobalFont=.t.,;
    Height=21, Width=346, Left=402, Top=47, InputMask=replicate('X',40)

  add object oCEDESSUP_1_10 as StdMemo with uid="ENVVKKOROJ",rtseq=10,rtrep=.f.,;
    cFormVar = "w_CEDESSUP", cQueryName = "CEDESSUP",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Ulteriore descrizione del cespite",;
    HelpContextID = 32457846,;
   bGlobalFont=.t.,;
    Height=57, Width=346, Left=402, Top=78


  add object oCETIPCES_1_11 as StdCombo with uid="DWRDFEGPHM",rtseq=11,rtrep=.f.,left=113,top=78,width=130,height=21;
    , ToolTipText = "Cespite singolo, cesp.con componenti, pertinenza singola, pertinenza composta";
    , HelpContextID = 29639801;
    , cFormVar="w_CETIPCES",RowSource=""+"Cespite singolo,"+"Cespite composto,"+"Pertinenza singola,"+"Pertinenza composta,"+"Gestito a quantit�", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCETIPCES_1_11.RadioValue()
    return(iif(this.value =1,'CS',;
    iif(this.value =2,'CC',;
    iif(this.value =3,'PS',;
    iif(this.value =4,'PC',;
    iif(this.value =5,'CQ',;
    space(2)))))))
  endfunc
  func oCETIPCES_1_11.GetRadio()
    this.Parent.oContained.w_CETIPCES = this.RadioValue()
    return .t.
  endfunc

  func oCETIPCES_1_11.SetRadio()
    this.Parent.oContained.w_CETIPCES=trim(this.Parent.oContained.w_CETIPCES)
    this.value = ;
      iif(this.Parent.oContained.w_CETIPCES=='CS',1,;
      iif(this.Parent.oContained.w_CETIPCES=='CC',2,;
      iif(this.Parent.oContained.w_CETIPCES=='PS',3,;
      iif(this.Parent.oContained.w_CETIPCES=='PC',4,;
      iif(this.Parent.oContained.w_CETIPCES=='CQ',5,;
      0)))))
  endfunc


  add object oCE_STATO_1_12 as StdCombo with uid="LTZTSIROBI",rtseq=12,rtrep=.f.,left=278,top=78,width=99,height=21;
    , ToolTipText = "Cespite acquisito o in futura acquisizione";
    , HelpContextID = 980085;
    , cFormVar="w_CE_STATO",RowSource=""+"Effettivo,"+"Previsionale", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCE_STATO_1_12.RadioValue()
    return(iif(this.value =1,'E',;
    iif(this.value =2,'P',;
    space(1))))
  endfunc
  func oCE_STATO_1_12.GetRadio()
    this.Parent.oContained.w_CE_STATO = this.RadioValue()
    return .t.
  endfunc

  func oCE_STATO_1_12.SetRadio()
    this.Parent.oContained.w_CE_STATO=trim(this.Parent.oContained.w_CE_STATO)
    this.value = ;
      iif(this.Parent.oContained.w_CE_STATO=='E',1,;
      iif(this.Parent.oContained.w_CE_STATO=='P',2,;
      0))
  endfunc


  add object oCESTABEN_1_13 as StdCombo with uid="LLPQDHEJWQ",rtseq=13,rtrep=.f.,left=113,top=109,width=104,height=21;
    , ToolTipText = "Stato del bene";
    , HelpContextID = 266286196;
    , cFormVar="w_CESTABEN",RowSource=""+"In uso,"+"Ceduto,"+"Eliminato,"+"Non attivato,"+"In costruzione", bObbl = .t. , nPag = 1;
    , sErrorMsg = "Valore non ammesso: data di primo utilizzo civile impostata.";
  , bGlobalFont=.t.


  func oCESTABEN_1_13.RadioValue()
    return(iif(this.value =1,'U',;
    iif(this.value =2,'C',;
    iif(this.value =3,'E',;
    iif(this.value =4,'N',;
    iif(this.value =5,'I',;
    space(1)))))))
  endfunc
  func oCESTABEN_1_13.GetRadio()
    this.Parent.oContained.w_CESTABEN = this.RadioValue()
    return .t.
  endfunc

  func oCESTABEN_1_13.SetRadio()
    this.Parent.oContained.w_CESTABEN=trim(this.Parent.oContained.w_CESTABEN)
    this.value = ;
      iif(this.Parent.oContained.w_CESTABEN=='U',1,;
      iif(this.Parent.oContained.w_CESTABEN=='C',2,;
      iif(this.Parent.oContained.w_CESTABEN=='E',3,;
      iif(this.Parent.oContained.w_CESTABEN=='N',4,;
      iif(this.Parent.oContained.w_CESTABEN=='I',5,;
      0)))))
  endfunc

  func oCESTABEN_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_CESTABEN<>'N' or empty(.w_CEDTPRIC))
    endwith
    return bRes
  endfunc

  add object oCEUNIMIS_1_14 as StdField with uid="SNXMKCCDZD",rtseq=14,rtrep=.f.,;
    cFormVar = "w_CEUNIMIS", cQueryName = "CEUNIMIS",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Unit� di misura del cespite",;
    HelpContextID = 78031751,;
   bGlobalFont=.t.,;
    Height=21, Width=49, Left=113, Top=148, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="UNIMIS", oKey_1_1="UMCODICE", oKey_1_2="this.w_CEUNIMIS"

  func oCEUNIMIS_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CETIPCES ='CQ')
    endwith
   endif
  endfunc

  func oCEUNIMIS_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oCEUNIMIS_1_14.ecpDrop(oSource)
    this.Parent.oContained.link_1_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCEUNIMIS_1_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'UNIMIS','*','UMCODICE',cp_AbsName(this.parent,'oCEUNIMIS_1_14'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Unit� di misura",'',this.parent.oContained
  endproc

  add object oCECODUBI_1_15 as StdField with uid="TVHLMGTMMW",rtseq=15,rtrep=.f.,;
    cFormVar = "w_CECODUBI", cQueryName = "CECODUBI",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice ubicazione del cespite",;
    HelpContextID = 50934895,;
   bGlobalFont=.t.,;
    Height=21, Width=156, Left=113, Top=178, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="UBI_CESP", cZoomOnZoom="GSCE_AUB", oKey_1_1="UBCODICE", oKey_1_2="this.w_CECODUBI"

  func oCECODUBI_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT .w_CETIPCES $ 'CC-PC')
    endwith
   endif
  endfunc

  func oCECODUBI_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oCECODUBI_1_15.ecpDrop(oSource)
    this.Parent.oContained.link_1_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCECODUBI_1_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'UBI_CESP','*','UBCODICE',cp_AbsName(this.parent,'oCECODUBI_1_15'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCE_AUB',"Ubicazioni",'',this.parent.oContained
  endproc
  proc oCECODUBI_1_15.mZoomOnZoom
    local i_obj
    i_obj=GSCE_AUB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_UBCODICE=this.parent.oContained.w_CECODUBI
     i_obj.ecpSave()
  endproc

  add object oDESUBI_1_16 as StdField with uid="PAJBXOXCJG",rtseq=16,rtrep=.f.,;
    cFormVar = "w_DESUBI", cQueryName = "DESUBI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 116405302,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=278, Top=178, InputMask=replicate('X',40)

  add object oCECODMAT_1_17 as StdField with uid="LUBHBYDWEL",rtseq=17,rtrep=.f.,;
    cFormVar = "w_CECODMAT", cQueryName = "CECODMAT",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice matricola di appartenenza",;
    HelpContextID = 185152634,;
   bGlobalFont=.t.,;
    Height=21, Width=156, Left=113, Top=210, InputMask=replicate('X',20)

  func oCECODMAT_1_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT .w_CETIPCES $ 'CC-PC')
    endwith
   endif
  endfunc

  add object oCECODFAM_1_18 as StdField with uid="OUPPPJPIWX",rtseq=18,rtrep=.f.,;
    cFormVar = "w_CECODFAM", cQueryName = "CECODFAM",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice famiglia di appartenenza",;
    HelpContextID = 67712115,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=113, Top=240, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_CESP", cZoomOnZoom="GSCE_AFC", oKey_1_1="FACODICE", oKey_1_2="this.w_CECODFAM"

  func oCECODFAM_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oCECODFAM_1_18.ecpDrop(oSource)
    this.Parent.oContained.link_1_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCECODFAM_1_18.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_CESP','*','FACODICE',cp_AbsName(this.parent,'oCECODFAM_1_18'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCE_AFC',"Famiglie cespiti",'',this.parent.oContained
  endproc
  proc oCECODFAM_1_18.mZoomOnZoom
    local i_obj
    i_obj=GSCE_AFC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_FACODICE=this.parent.oContained.w_CECODFAM
     i_obj.ecpSave()
  endproc

  add object oDESFAM_1_19 as StdField with uid="SYGOLPGOJW",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DESFAM", cQueryName = "DESFAM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 181482550,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=278, Top=240, InputMask=replicate('X',35)

  add object oCECODPER_1_20 as StdField with uid="MRBIOQDWJQ",rtseq=20,rtrep=.f.,;
    cFormVar = "w_CECODPER", cQueryName = "CECODPER",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice cespite di riferimento",;
    HelpContextID = 235484280,;
   bGlobalFont=.t.,;
    Height=21, Width=156, Left=113, Top=271, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="CES_PITI", cZoomOnZoom="GSCE_ACE", oKey_1_1="CECODICE", oKey_1_2="this.w_CECODPER"

  func oCECODPER_1_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CETIPCES $ 'PS-PC')
    endwith
   endif
  endfunc

  func oCECODPER_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_20('Part',this)
    endwith
    return bRes
  endfunc

  proc oCECODPER_1_20.ecpDrop(oSource)
    this.Parent.oContained.link_1_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCECODPER_1_20.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CES_PITI','*','CECODICE',cp_AbsName(this.parent,'oCECODPER_1_20'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCE_ACE',"Elenco cespiti",'',this.parent.oContained
  endproc
  proc oCECODPER_1_20.mZoomOnZoom
    local i_obj
    i_obj=GSCE_ACE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CECODICE=this.parent.oContained.w_CECODPER
     i_obj.ecpSave()
  endproc

  add object oDESCES_1_21 as StdField with uid="CAELKWILLE",rtseq=21,rtrep=.f.,;
    cFormVar = "w_DESCES", cQueryName = "DESCES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 17708086,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=278, Top=271, InputMask=replicate('X',40)

  add object oCECODCOM_1_22 as StdField with uid="YWVNMHRZHV",rtseq=22,rtrep=.f.,;
    cFormVar = "w_CECODCOM", cQueryName = "CECODCOM",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Commessa associata al cespite",;
    HelpContextID = 251054989,;
   bGlobalFont=.t.,;
    Height=21, Width=156, Left=113, Top=302, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="GSAR_ACN", oKey_1_1="CNCODCAN", oKey_1_2="this.w_CECODCOM"

  func oCECODCOM_1_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CESTABEN = 'I')
    endwith
   endif
  endfunc

  func oCECODCOM_1_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_22('Part',this)
    endwith
    return bRes
  endfunc

  proc oCECODCOM_1_22.ecpDrop(oSource)
    this.Parent.oContained.link_1_22('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCECODCOM_1_22.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCECODCOM_1_22'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACN',"Commesse",'',this.parent.oContained
  endproc
  proc oCECODCOM_1_22.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_CECODCOM
     i_obj.ecpSave()
  endproc

  add object oCECODATT_1_23 as StdField with uid="KMNAIJMFRS",rtseq=23,rtrep=.f.,;
    cFormVar = "w_CECODATT", cQueryName = "CECODATT",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice attivit� IVA dell'azienda associata al cespite",;
    HelpContextID = 252261498,;
   bGlobalFont=.t.,;
    Height=21, Width=64, Left=113, Top=333, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="ATTIMAST", oKey_1_1="ATCODATT", oKey_1_2="this.w_CECODATT"

  func oCECODATT_1_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_23('Part',this)
    endwith
    return bRes
  endfunc

  proc oCECODATT_1_23.ecpDrop(oSource)
    this.Parent.oContained.link_1_23('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCECODATT_1_23.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ATTIMAST','*','ATCODATT',cp_AbsName(this.parent,'oCECODATT_1_23'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Attivit�",'',this.parent.oContained
  endproc

  add object oCEDTPRIC_1_24 as StdField with uid="ZDWTHJQWYT",rtseq=24,rtrep=.f.,;
    cFormVar = "w_CEDTPRIC", cQueryName = "CEDTPRIC",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di inizio utilizzo civile del cespite",;
    HelpContextID = 254917527,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=113, Top=387

  func oCEDTPRIC_1_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CESTABEN $ 'UI' AND .w_PCTIPAMM<>'F')
    endwith
   endif
  endfunc

  add object oDESCOM_1_25 as StdField with uid="JHUUTCTYEU",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DESCOM", cQueryName = "DESCOM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 195966006,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=278, Top=302, InputMask=replicate('X',30)

  add object oCEDTPRIU_1_26 as StdField with uid="YSWHZSAVEX",rtseq=26,rtrep=.f.,;
    cFormVar = "w_CEDTPRIU", cQueryName = "CEDTPRIU",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di inizio utilizzo del cespite",;
    HelpContextID = 254917509,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=278, Top=387

  func oCEDTPRIU_1_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CESTABEN $ 'UI' AND .w_PCTIPAMM<>'C')
    endwith
   endif
  endfunc

  add object oCEDATDIS_1_27 as StdField with uid="SGWHHPYZAL",rtseq=27,rtrep=.f.,;
    cFormVar = "w_CEDATDIS", cQueryName = "CEDATDIS",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data dismissione del cespite",;
    HelpContextID = 218413959,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=491, Top=389

  func oCEDATDIS_1_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CESTABEN='C' OR .w_CESTABEN='E')
    endwith
   endif
  endfunc

  add object oCEESPRIC_1_28 as StdField with uid="LFDAFWWUZC",rtseq=28,rtrep=.f.,;
    cFormVar = "w_CEESPRIC", cQueryName = "CEESPRIC",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Esercizio non corrispondente",;
    ToolTipText = "Esercizio della data di inizio utilizzo civile del cespite",;
    HelpContextID = 254978967,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=113, Top=416, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_CEESPRIC"

  func oCEESPRIC_1_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_CEDTPRIC) AND .w_PCTIPAMM<>'F')
    endwith
   endif
  endfunc

  func oCEESPRIC_1_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_28('Part',this)
    endwith
    return bRes
  endfunc

  proc oCEESPRIC_1_28.ecpDrop(oSource)
    this.Parent.oContained.link_1_28('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCEESPRIC_1_28.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CODAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_CODAZI)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oCEESPRIC_1_28'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Esercizi",'',this.parent.oContained
  endproc

  add object oCEFLSPEM_1_29 as StdCheck with uid="ABOGBCLYUB",rtseq=29,rtrep=.f.,left=589, top=148, caption="Spese manutenzione",;
    ToolTipText = "Il cespite partecipa al calcolo del limite spese manutenzione deducibile",;
    HelpContextID = 251028595,;
    cFormVar="w_CEFLSPEM", bObbl = .f. , nPag = 1;
    , tabstop=.f.;
   , bGlobalFont=.t.


  func oCEFLSPEM_1_29.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCEFLSPEM_1_29.GetRadio()
    this.Parent.oContained.w_CEFLSPEM = this.RadioValue()
    return .t.
  endfunc

  func oCEFLSPEM_1_29.SetRadio()
    this.Parent.oContained.w_CEFLSPEM=trim(this.Parent.oContained.w_CEFLSPEM)
    this.value = ;
      iif(this.Parent.oContained.w_CEFLSPEM=='S',1,;
      0)
  endfunc

  add object oCEESPRIU_1_30 as StdField with uid="HKVXITXIMR",rtseq=30,rtrep=.f.,;
    cFormVar = "w_CEESPRIU", cQueryName = "CEESPRIU",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Esercizio non corrispondente",;
    ToolTipText = "Esercizio della data di inizio utilizzo fiscale del cespite",;
    HelpContextID = 254978949,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=278, Top=416, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_CEESPRIU"

  func oCEESPRIU_1_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_CEDTPRIU) AND .w_PCTIPAMM<>'C')
    endwith
   endif
  endfunc

  func oCEESPRIU_1_30.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_30('Part',this)
    endwith
    return bRes
  endfunc

  proc oCEESPRIU_1_30.ecpDrop(oSource)
    this.Parent.oContained.link_1_30('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCEESPRIU_1_30.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CODAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_CODAZI)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oCEESPRIU_1_30'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Esercizi",'',this.parent.oContained
  endproc

  add object oCEFLCEUS_1_31 as StdCheck with uid="IEBTHVQPVO",rtseq=31,rtrep=.f.,left=589, top=178, caption="Cespite usato",;
    ToolTipText = "Cespite con accantonamento anticipato limitato al primo esercizio",;
    HelpContextID = 49702009,;
    cFormVar="w_CEFLCEUS", bObbl = .f. , nPag = 1;
    , tabstop=.f.;
   , bGlobalFont=.t.


  func oCEFLCEUS_1_31.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCEFLCEUS_1_31.GetRadio()
    this.Parent.oContained.w_CEFLCEUS = this.RadioValue()
    return .t.
  endfunc

  func oCEFLCEUS_1_31.SetRadio()
    this.Parent.oContained.w_CEFLCEUS=trim(this.Parent.oContained.w_CEFLCEUS)
    this.value = ;
      iif(this.Parent.oContained.w_CEFLCEUS=='S',1,;
      0)
  endfunc

  add object oCEDTINVA_1_32 as StdField with uid="DYDNTXARPC",rtseq=32,rtrep=.f.,;
    cFormVar = "w_CEDTINVA", cQueryName = "CEDTINVA",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di entrata in funzione",;
    HelpContextID = 207504487,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=440, Top=503

  add object oCEDTOBSO_1_34 as StdField with uid="QJESJELWAE",rtseq=33,rtrep=.f.,;
    cFormVar = "w_CEDTOBSO", cQueryName = "CEDTOBSO",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di fine validit�",;
    HelpContextID = 12469365,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=669, Top=503


  add object oObj_1_48 as cp_runprogram with uid="SVLKMEZHAK",left=247, top=572, width=246,height=19,;
    caption='GSCE_BCK(D)',;
   bGlobalFont=.t.,;
    prg="GSCE_BCK('D')",;
    cEvent = "Update start,Delete start",;
    nPag=1;
    , HelpContextID = 28449073


  add object oObj_1_52 as cp_runprogram with uid="OPAPSBJNXW",left=247, top=593, width=246,height=19,;
    caption='GSCE_BCK(C)',;
   bGlobalFont=.t.,;
    prg="GSCE_BCK('C')",;
    cEvent = "w_CEDTPRIU Changed",;
    nPag=1;
    , HelpContextID = 28448817


  add object oObj_1_54 as cp_runprogram with uid="JPUYCFLMIX",left=-4, top=572, width=246,height=19,;
    caption='GSCE_BCK(F)',;
   bGlobalFont=.t.,;
    prg="GSCE_BCK('F')",;
    cEvent = "w_CEDATDIS Changed",;
    nPag=1;
    , HelpContextID = 28449585


  add object oLinkPC_1_55 as StdButton with uid="SGQHLWXZUA",left=75, top=482, width=48,height=45,;
    CpPicture="BMP\COMPONE.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per accedere ai componenti del cespite";
    , HelpContextID = 249392852;
    , caption='\<Compon.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oLinkPC_1_55.Click()
      this.Parent.oContained.GSCE_MCO.LinkPCClick()
    endproc

  func oLinkPC_1_55.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CETIPCES $ 'CC-PC')
      endwith
    endif
  endfunc

  func oLinkPC_1_55.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (NOT .w_CETIPCES $ 'CC-PC')
     endwith
    endif
  endfunc


  add object oBtn_1_56 as StdButton with uid="NZAHRZGYLN",left=23, top=481, width=48,height=45,;
    CpPicture="BMP\SALDI.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per accedere alla situazione dei saldi cespiti";
    , HelpContextID = 261037786;
    , caption='\<Saldi';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_56.Click()
      with this.Parent.oContained
        GSCE_BGS(this.Parent.oContained,"C", .w_CECODICE)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_56.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_CECODICE))
      endwith
    endif
  endfunc

  add object oDESATT_1_58 as StdField with uid="LICNNRNLRY",rtseq=58,rtrep=.f.,;
    cFormVar = "w_DESATT", cQueryName = "DESATT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 50082870,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=278, Top=333, InputMask=replicate('X',35)

  add object oDESUNI_1_59 as StdField with uid="QDSVTXLOIJ",rtseq=59,rtrep=.f.,;
    cFormVar = "w_DESUNI", cQueryName = "DESUNI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 128988214,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=278, Top=148, InputMask=replicate('X',35)

  add object oDESCAT_1_62 as StdField with uid="IDHDMUMAXZ",rtseq=61,rtrep=.f.,;
    cFormVar = "w_DESCAT", cQueryName = "DESCAT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 30290998,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=278, Top=15, InputMask=replicate('X',40)


  add object oObj_1_64 as cp_runprogram with uid="DKGMUQVJYG",left=502, top=594, width=246,height=19,;
    caption='GSCE_BCR(R)',;
   bGlobalFont=.t.,;
    prg="GSCE_BCR('R')",;
    cEvent = "CaricaRapido",;
    nPag=1;
    , HelpContextID = 28452664


  add object oObj_1_65 as cp_runprogram with uid="FYBYSSWLUT",left=503, top=573, width=246,height=19,;
    caption='GSCE_BCR(C)',;
   bGlobalFont=.t.,;
    prg="GSCE_BCR('C')",;
    cEvent = "w_CECODCAT Changed",;
    nPag=1;
    , HelpContextID = 28448824


  add object oObj_1_66 as cp_runprogram with uid="AKDEZUKEGV",left=-3, top=592, width=246,height=19,;
    caption='GSCE_BCK(I)',;
   bGlobalFont=.t.,;
    prg="GSCE_BCK('I')",;
    cEvent = "Insert start",;
    nPag=1;
    , HelpContextID = 28450353


  add object oObj_1_74 as cp_runprogram with uid="YTXHQHUALS",left=501, top=615, width=246,height=19,;
    caption='GSCE_BCK(E)',;
   bGlobalFont=.t.,;
    prg="GSCE_BCK('E')",;
    cEvent = "w_CEDTPRIC Changed",;
    nPag=1;
    , HelpContextID = 28449329

  add object oStr_1_33 as StdString with uid="VAEFYCTSAD",Visible=.t., Left=2, Top=47,;
    Alignment=1, Width=108, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_35 as StdString with uid="PHWNORHFVI",Visible=.t., Left=2, Top=78,;
    Alignment=1, Width=108, Height=15,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="RUUDRGWEHT",Visible=.t., Left=2, Top=109,;
    Alignment=1, Width=108, Height=15,;
    Caption="Stato del bene:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="VBZTYATAJY",Visible=.t., Left=2, Top=178,;
    Alignment=1, Width=108, Height=15,;
    Caption="Ubicazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="VUQZEPHOKH",Visible=.t., Left=2, Top=209,;
    Alignment=1, Width=108, Height=15,;
    Caption="Matricola:"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="KYQHYKOTPJ",Visible=.t., Left=2, Top=271,;
    Alignment=1, Width=108, Height=15,;
    Caption="Rif.cespite:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="VONVCJXCUY",Visible=.t., Left=2, Top=302,;
    Alignment=1, Width=108, Height=15,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="RGFTAXTTYR",Visible=.t., Left=336, Top=507,;
    Alignment=1, Width=100, Height=15,;
    Caption="Inizio validit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="AJZNXASTAT",Visible=.t., Left=543, Top=507,;
    Alignment=1, Width=124, Height=15,;
    Caption="Data obsolescenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_47 as StdString with uid="YOGWHVFSUY",Visible=.t., Left=203, Top=386,;
    Alignment=1, Width=71, Height=18,;
    Caption="Fiscale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_50 as StdString with uid="OYOOPOHYCF",Visible=.t., Left=2, Top=240,;
    Alignment=1, Width=108, Height=15,;
    Caption="Famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_51 as StdString with uid="KOJIEIWZWC",Visible=.t., Left=172, Top=417,;
    Alignment=1, Width=102, Height=18,;
    Caption="Esercizio fiscale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_53 as StdString with uid="GLVNJTQIXK",Visible=.t., Left=366, Top=389,;
    Alignment=1, Width=121, Height=15,;
    Caption="Data dismissione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_57 as StdString with uid="ESIRAKYAHO",Visible=.t., Left=2, Top=333,;
    Alignment=1, Width=108, Height=18,;
    Caption="Attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_60 as StdString with uid="PUEBBQEUAU",Visible=.t., Left=2, Top=148,;
    Alignment=1, Width=108, Height=18,;
    Caption="Unit� di misura:"  ;
  , bGlobalFont=.t.

  add object oStr_1_63 as StdString with uid="ETHJNTAAXO",Visible=.t., Left=2, Top=17,;
    Alignment=1, Width=108, Height=15,;
    Caption="Categoria:"  ;
  , bGlobalFont=.t.

  add object oStr_1_68 as StdString with uid="FKNGPQDASX",Visible=.t., Left=296, Top=47,;
    Alignment=1, Width=105, Height=15,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_69 as StdString with uid="BWAFONUWYR",Visible=.t., Left=17, Top=362,;
    Alignment=0, Width=134, Height=18,;
    Caption="Date primo utilizzo"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_71 as StdString with uid="WXPZZWYJOP",Visible=.t., Left=29, Top=386,;
    Alignment=1, Width=81, Height=15,;
    Caption="Civile:"  ;
  , bGlobalFont=.t.

  add object oStr_1_72 as StdString with uid="CNOWJOLIHV",Visible=.t., Left=0, Top=419,;
    Alignment=1, Width=110, Height=15,;
    Caption="Esercizio civile:"  ;
  , bGlobalFont=.t.

  add object oBox_1_70 as StdBox with uid="TMYXJEQGQU",left=14, top=376, width=347,height=2
enddefine
define class tgsce_acePag2 as StdContainer
  Width  = 784
  height = 541
  stdWidth  = 784
  stdheight = 541
  resizeYpos=284
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODI_2_3 as StdField with uid="LPQDQHMWJA",rtseq=41,rtrep=.f.,;
    cFormVar = "w_CODI", cQueryName = "CODI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 104640986,;
   bGlobalFont=.t.,;
    Height=21, Width=156, Left=92, Top=12, InputMask=replicate('X',20)

  add object oDESC_2_4 as StdField with uid="PIJKXBLXZT",rtseq=42,rtrep=.f.,;
    cFormVar = "w_DESC", cQueryName = "DESC",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 104975306,;
   bGlobalFont=.t.,;
    Height=21, Width=303, Left=253, Top=12, InputMask=replicate('X',40)


  add object oCETIPAMM_2_5 as StdCombo with uid="GFGTRTDZDV",rtseq=43,rtrep=.f.,left=641,top=74,width=111,height=21;
    , ToolTipText = "Tipo di eccezione gestita";
    , HelpContextID = 3914637;
    , cFormVar="w_CETIPAMM",RowSource=""+"Solo civile,"+"Solo fiscale,"+"Civile e fiscale", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oCETIPAMM_2_5.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    iif(this.value =3,'E',;
    space(1)))))
  endfunc
  func oCETIPAMM_2_5.GetRadio()
    this.Parent.oContained.w_CETIPAMM = this.RadioValue()
    return .t.
  endfunc

  func oCETIPAMM_2_5.SetRadio()
    this.Parent.oContained.w_CETIPAMM=trim(this.Parent.oContained.w_CETIPAMM)
    this.value = ;
      iif(this.Parent.oContained.w_CETIPAMM=='C',1,;
      iif(this.Parent.oContained.w_CETIPAMM=='F',2,;
      iif(this.Parent.oContained.w_CETIPAMM=='E',3,;
      0)))
  endfunc

  func oCETIPAMM_2_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((NOT EMPTY(.w_CEESPRIU) OR NOT EMPTY(.w_CEESPRIC) ) AND .w_PCTIPAMM='E')
    endwith
   endif
  endfunc

  add object oCEDURCES_2_9 as StdField with uid="XNNZBFEVHA",rtseq=47,rtrep=.f.,;
    cFormVar = "w_CEDURCES", cQueryName = "CEDURCES",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Durata del cespite espressa in numero di esercizi",;
    HelpContextID = 32457849,;
   bGlobalFont=.t.,;
    Height=21, Width=32, Left=137, Top=158, cSayPict='"999"', cGetPict='"999"'

  func oCEDURCES_2_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PCTIPAMM<>'F')
    endwith
   endif
  endfunc

  add object oCECOEFIS_2_11 as StdField with uid="PRYDWUAKCT",rtseq=49,rtrep=.f.,;
    cFormVar = "w_CECOEFIS", cQueryName = "CECOEFIS",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Coefficiente di ammortamento ordinario",;
    HelpContextID = 199674759,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=162, Top=294, cSayPict='"999.99"', cGetPict='"999.99"'

  func oCECOEFIS_2_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CETIPAMM<>'C' AND .w_PCTIPAMM<>'C' AND NOT EMPTY(.w_CEESPRIU) AND .w_CENSOAMF<>'S' AND .w_CEAMMIMM<>'S')
    endwith
   endif
  endfunc

  add object oCEPERDEF_2_12 as StdField with uid="PRUMJWZJAH",rtseq=50,rtrep=.f.,;
    cFormVar = "w_CEPERDEF", cQueryName = "CEPERDEF",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Percentuale di deducibilit� dell'accantonamento",;
    HelpContextID = 48235628,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=162, Top=321, cSayPict='"999.99"', cGetPict='"999.99"'

  func oCEPERDEF_2_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CETIPAMM<>'C' AND .w_PCTIPAMM<>'C' AND NOT EMPTY(.w_CEESPRIU) AND .w_CENSOAMF<>'S' AND .w_CEAMMIMM<>'S')
    endwith
   endif
  endfunc

  func oCEPERDEF_2_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_CECOEFIS=0 OR .w_CEPERDEF>0 OR .w_CETIPAMM='C')
    endwith
    return bRes
  endfunc

  add object oCEIMPMAX_2_13 as StdField with uid="ZJBYYLAOQI",rtseq=51,rtrep=.f.,;
    cFormVar = "w_CEIMPMAX", cQueryName = "CEIMPMAX",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Limite massimo importo accantonabile",;
    HelpContextID = 197629054,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=95, Top=351, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oCEIMPMAX_2_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CETIPAMM<>'C' AND .w_PCTIPAMM<>'C' AND NOT EMPTY(.w_CEESPRIU) AND .w_CENSOAMF<>'S' AND .w_CEAMMIMM<>'S')
    endwith
   endif
  endfunc


  add object oCEVALLIM_2_14 as StdCombo with uid="YTPXNNYOMR",rtseq=52,rtrep=.f.,left=95,top=376,width=125,height=21;
    , ToolTipText = "Valuta a cui � riferito l'importo massimo accantonabile";
    , HelpContextID = 92511117;
    , cFormVar="w_CEVALLIM",RowSource=""+"Valuta di conto,"+"Valuta nazionale", bObbl = .f. , nPag = 2;
    , cLinkFile="VALUTE";
  , bGlobalFont=.t.


  func oCEVALLIM_2_14.RadioValue()
    return(iif(this.value =1,ALLTRIM(g_PERVAL),;
    iif(this.value =2,ALLTRIM(g_CODLIR),;
    space(3))))
  endfunc
  func oCEVALLIM_2_14.GetRadio()
    this.Parent.oContained.w_CEVALLIM = this.RadioValue()
    return .t.
  endfunc

  func oCEVALLIM_2_14.SetRadio()
    this.Parent.oContained.w_CEVALLIM=trim(this.Parent.oContained.w_CEVALLIM)
    this.value = ;
      iif(this.Parent.oContained.w_CEVALLIM==ALLTRIM(g_PERVAL),1,;
      iif(this.Parent.oContained.w_CEVALLIM==ALLTRIM(g_CODLIR),2,;
      0))
  endfunc

  func oCEVALLIM_2_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CETIPAMM<>'C' AND .w_PCTIPAMM<>'C' AND NOT EMPTY(.w_CEESPRIU) AND .w_CENSOAMF<>'S' AND .w_CEAMMIMM<>'S')
    endwith
   endif
  endfunc

  func oCEVALLIM_2_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oCEVALLIM_2_14.ecpDrop(oSource)
    this.Parent.oContained.link_2_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  add object oCEFLAUMU_2_17 as StdCheck with uid="FEJNGRLBAJ",rtseq=54,rtrep=.f.,left=56, top=399, caption="Aut.minor uso",;
    ToolTipText = "L'accantonamento sotto il limite minimo di accant. non genera quote perse",;
    HelpContextID = 220830597,;
    cFormVar="w_CEFLAUMU", bObbl = .f. , nPag = 2;
    , tabstop=.f.;
   , bGlobalFont=.t.


  func oCEFLAUMU_2_17.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCEFLAUMU_2_17.GetRadio()
    this.Parent.oContained.w_CEFLAUMU = this.RadioValue()
    return .t.
  endfunc

  func oCEFLAUMU_2_17.SetRadio()
    this.Parent.oContained.w_CEFLAUMU=trim(this.Parent.oContained.w_CEFLAUMU)
    this.value = ;
      iif(this.Parent.oContained.w_CEFLAUMU=='S',1,;
      0)
  endfunc

  func oCEFLAUMU_2_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CETIPAMM<>'C' AND .w_PCTIPAMM<>'C' AND NOT EMPTY(.w_CEESPRIU) AND .w_CENSOAMF<>'S' AND .w_CEAMMIMM<>'S')
    endwith
   endif
  endfunc


  add object oLinkPC_2_27 as stdDynamicChildContainer with uid="FSOSVOQSDO",left=223, top=295, width=561, height=159, bOnScreen=.t.;


  add object oCEVOCCEN_2_30 as StdField with uid="TCPWTLSVPQ",rtseq=62,rtrep=.f.,;
    cFormVar = "w_CEVOCCEN", cQueryName = "CEVOCCEN",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice voce di costo associata al cespite",;
    HelpContextID = 16409716,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=107, Top=484, cSayPict="p_MCE", cGetPict="p_MCE", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="VOC_COST", cZoomOnZoom="GSCA_AVC", oKey_1_1="VCCODICE", oKey_1_2="this.w_CEVOCCEN"

  func oCEVOCCEN_2_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERCCR='S')
    endwith
   endif
  endfunc

  func oCEVOCCEN_2_30.mHide()
    with this.Parent.oContained
      return (g_PERCCR<>'S')
    endwith
  endfunc

  func oCEVOCCEN_2_30.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_30('Part',this)
    endwith
    return bRes
  endfunc

  proc oCEVOCCEN_2_30.ecpDrop(oSource)
    this.Parent.oContained.link_2_30('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCEVOCCEN_2_30.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOC_COST','*','VCCODICE',cp_AbsName(this.parent,'oCEVOCCEN_2_30'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_AVC',"Voci di costo/ricavo",'',this.parent.oContained
  endproc
  proc oCEVOCCEN_2_30.mZoomOnZoom
    local i_obj
    i_obj=GSCA_AVC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VCCODICE=this.parent.oContained.w_CEVOCCEN
     i_obj.ecpSave()
  endproc

  add object oCECODCEN_2_31 as StdField with uid="AMUTPEVOCM",rtseq=63,rtrep=.f.,;
    cFormVar = "w_CECODCEN", cQueryName = "CECODCEN",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Centro di costo/ricavo associato al cespite",;
    HelpContextID = 17380468,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=107, Top=515, cSayPict="p_CEN", cGetPict="p_CEN", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CENCOST", cZoomOnZoom="GSCA_ACC", oKey_1_1="CC_CONTO", oKey_1_2="this.w_CECODCEN"

  func oCECODCEN_2_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERCCR='S')
    endwith
   endif
  endfunc

  func oCECODCEN_2_31.mHide()
    with this.Parent.oContained
      return (g_PERCCR<>'S')
    endwith
  endfunc

  func oCECODCEN_2_31.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_31('Part',this)
    endwith
    return bRes
  endfunc

  proc oCECODCEN_2_31.ecpDrop(oSource)
    this.Parent.oContained.link_2_31('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCECODCEN_2_31.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CENCOST','*','CC_CONTO',cp_AbsName(this.parent,'oCECODCEN_2_31'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_ACC',"Centri di costo/ricavo",'',this.parent.oContained
  endproc
  proc oCECODCEN_2_31.mZoomOnZoom
    local i_obj
    i_obj=GSCA_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CC_CONTO=this.parent.oContained.w_CECODCEN
     i_obj.ecpSave()
  endproc

  add object oDESVOC_2_34 as StdField with uid="GJSZPSBOEV",rtseq=64,rtrep=.f.,;
    cFormVar = "w_DESVOC", cQueryName = "DESVOC",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 29439030,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=232, Top=484, InputMask=replicate('X',40)

  func oDESVOC_2_34.mHide()
    with this.Parent.oContained
      return (g_PERCCR<>'S')
    endwith
  endfunc

  add object oDESCEN_2_35 as StdField with uid="LQJEWCGLFB",rtseq=65,rtrep=.f.,;
    cFormVar = "w_DESCEN", cQueryName = "DESCEN",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 202257462,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=232, Top=515, InputMask=replicate('X',40)

  func oDESCEN_2_35.mHide()
    with this.Parent.oContained
      return (g_PERCCR<>'S')
    endwith
  endfunc


  add object oObj_2_41 as cp_runprogram with uid="ULARFGPCXV",left=-7, top=560, width=312,height=22,;
    caption='GSCE_BCE',;
   bGlobalFont=.t.,;
    prg="GSCE_BCE",;
    cEvent = "w_CETIPAMM Changed",;
    nPag=2;
    , HelpContextID = 28263083

  add object oCEAMMIMM_2_42 as StdCheck with uid="BOYAKJBSJF",rtseq=67,rtrep=.f.,left=344, top=100, caption="Fiscale",;
    ToolTipText = "Se attivo, effettua l'ammortamento immediato ai fini fiscali (beni di modico valore)",;
    HelpContextID = 141093773,;
    cFormVar="w_CEAMMIMM", bObbl = .f. , nPag = 2;
    , tabstop=.f.;
   , bGlobalFont=.t.


  func oCEAMMIMM_2_42.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCEAMMIMM_2_42.GetRadio()
    this.Parent.oContained.w_CEAMMIMM = this.RadioValue()
    return .t.
  endfunc

  func oCEAMMIMM_2_42.SetRadio()
    this.Parent.oContained.w_CEAMMIMM=trim(this.Parent.oContained.w_CEAMMIMM)
    this.value = ;
      iif(this.Parent.oContained.w_CEAMMIMM=='S',1,;
      0)
  endfunc

  func oCEAMMIMM_2_42.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CENSOAMF<>'S' AND .w_CETIPAMM<>'C' AND .w_PCTIPAMM<>'C' AND NOT EMPTY(.w_CEESPRIU))
    endwith
   endif
  endfunc

  add object oCENSOAMM_2_44 as StdCheck with uid="FPGKXKDXRE",rtseq=68,rtrep=.f.,left=278, top=73, caption="Civile",;
    ToolTipText = "Se attivo per il cespite non viene generato nessun accantonamento civile",;
    HelpContextID = 4332429,;
    cFormVar="w_CENSOAMM", bObbl = .f. , nPag = 2;
    , tabstop=.f.;
   , bGlobalFont=.t.


  func oCENSOAMM_2_44.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCENSOAMM_2_44.GetRadio()
    this.Parent.oContained.w_CENSOAMM = this.RadioValue()
    return .t.
  endfunc

  func oCENSOAMM_2_44.SetRadio()
    this.Parent.oContained.w_CENSOAMM=trim(this.Parent.oContained.w_CENSOAMM)
    this.value = ;
      iif(this.Parent.oContained.w_CENSOAMM=='S',1,;
      0)
  endfunc

  func oCENSOAMM_2_44.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NSOAMM<>'S' AND .w_PCTIPAMM<>'F' AND .w_CETIPAMM<>'F')
    endwith
   endif
  endfunc

  add object oCENSOAMF_2_45 as StdCheck with uid="AGQKWBYIRZ",rtseq=69,rtrep=.f.,left=344, top=73, caption="Fiscale",;
    ToolTipText = "Se attivo per il cespite non viene generato nessun accantonamento fiscale",;
    HelpContextID = 4332436,;
    cFormVar="w_CENSOAMF", bObbl = .f. , nPag = 2;
    , tabstop=.f.;
   , bGlobalFont=.t.


  func oCENSOAMF_2_45.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCENSOAMF_2_45.GetRadio()
    this.Parent.oContained.w_CENSOAMF = this.RadioValue()
    return .t.
  endfunc

  func oCENSOAMF_2_45.SetRadio()
    this.Parent.oContained.w_CENSOAMF=trim(this.Parent.oContained.w_CENSOAMF)
    this.value = ;
      iif(this.Parent.oContained.w_CENSOAMF=='S',1,;
      0)
  endfunc

  func oCENSOAMF_2_45.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NSOAMF<>'S' AND .w_PCTIPAMM<>'C' AND .w_CETIPAMM<>'C')
    endwith
   endif
  endfunc

  add object oCEAMMIMC_2_47 as StdCheck with uid="QZJAHNMIZW",rtseq=70,rtrep=.f.,left=278, top=100, caption="Civile",;
    ToolTipText = "Se attivo, effettua l'ammortamento immediato ai fini civili (beni di modico valore)",;
    HelpContextID = 141093783,;
    cFormVar="w_CEAMMIMC", bObbl = .f. , nPag = 2;
    , tabstop=.f.;
   , bGlobalFont=.t.


  func oCEAMMIMC_2_47.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCEAMMIMC_2_47.GetRadio()
    this.Parent.oContained.w_CEAMMIMC = this.RadioValue()
    return .t.
  endfunc

  func oCEAMMIMC_2_47.SetRadio()
    this.Parent.oContained.w_CEAMMIMC=trim(this.Parent.oContained.w_CEAMMIMC)
    this.value = ;
      iif(this.Parent.oContained.w_CEAMMIMC=='S',1,;
      0)
  endfunc

  func oCEAMMIMC_2_47.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CENSOAMM<>'S' AND .w_CETIPAMM<>'F' AND .w_PCTIPAMM<>'F' AND NOT EMPTY(.w_CEESPRIC))
    endwith
   endif
  endfunc

  add object oFLRIDU_2_48 as StdCheck with uid="WYEGKADMBC",rtseq=72,rtrep=.f.,left=56, top=417, caption="% Riduzione 1^ esercizio", enabled=.f.,;
    ToolTipText = "Eredita dalla categoria l'applicabilit� della %  di riduzione 1^ esercizio",;
    HelpContextID = 50604886,;
    cFormVar="w_FLRIDU", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oFLRIDU_2_48.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLRIDU_2_48.GetRadio()
    this.Parent.oContained.w_FLRIDU = this.RadioValue()
    return .t.
  endfunc

  func oFLRIDU_2_48.SetRadio()
    this.Parent.oContained.w_FLRIDU=trim(this.Parent.oContained.w_FLRIDU)
    this.value = ;
      iif(this.Parent.oContained.w_FLRIDU=='S',1,;
      0)
  endfunc


  add object oLinkPC_2_49 as stdDynamicChildContainer with uid="VBMTZNQZOT",left=247, top=158, width=448, height=112, bOnScreen=.t.;


  func oLinkPC_2_49.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CETIPAMM$'EC' AND .w_CENSOAMM<>'S' AND .w_CEAMMIMC<>'S' AND .w_PCTIPAMM<>'F')
      endwith
    endif
  endfunc

  add object oStr_2_15 as StdString with uid="VOJBUVNIJB",Visible=.t., Left=3, Top=12,;
    Alignment=1, Width=85, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_19 as StdString with uid="JXAFQSZIJG",Visible=.t., Left=6, Top=49,;
    Alignment=0, Width=203, Height=15,;
    Caption="Piano ammortamento particolare"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_23 as StdString with uid="GMZUWJEHFN",Visible=.t., Left=472, Top=75,;
    Alignment=1, Width=165, Height=15,;
    Caption="Aspetto personalizzato:"  ;
  , bGlobalFont=.t.

  add object oStr_2_24 as StdString with uid="BCYESSKZAA",Visible=.t., Left=39, Top=353,;
    Alignment=1, Width=55, Height=15,;
    Caption="Limite:"  ;
  , bGlobalFont=.t.

  add object oStr_2_25 as StdString with uid="JRQRYYKZXP",Visible=.t., Left=66, Top=324,;
    Alignment=1, Width=95, Height=15,;
    Caption="% Deducibilit�:"  ;
  , bGlobalFont=.t.

  add object oStr_2_26 as StdString with uid="PLOAPAYWFW",Visible=.t., Left=6, Top=132,;
    Alignment=0, Width=96, Height=17,;
    Caption="Aspetto civile"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_28 as StdString with uid="TIPIHGVORK",Visible=.t., Left=1, Top=296,;
    Alignment=1, Width=160, Height=15,;
    Caption="Coefficiente amm.ordinario:"  ;
  , bGlobalFont=.t.

  add object oStr_2_29 as StdString with uid="MSWWJDUGIX",Visible=.t., Left=9, Top=161,;
    Alignment=1, Width=124, Height=18,;
    Caption="Durata del cespite:"  ;
  , bGlobalFont=.t.

  add object oStr_2_32 as StdString with uid="KFRVCWERWH",Visible=.t., Left=0, Top=484,;
    Alignment=1, Width=104, Height=15,;
    Caption="Voce di costo:"  ;
  , bGlobalFont=.t.

  func oStr_2_32.mHide()
    with this.Parent.oContained
      return (g_PERCCR<>'S')
    endwith
  endfunc

  add object oStr_2_33 as StdString with uid="CMWVDEJQKB",Visible=.t., Left=0, Top=515,;
    Alignment=1, Width=104, Height=15,;
    Caption="C/Costo ricavo:"  ;
  , bGlobalFont=.t.

  func oStr_2_33.mHide()
    with this.Parent.oContained
      return (g_PERCCR<>'S')
    endwith
  endfunc

  add object oStr_2_36 as StdString with uid="VQYUAPOFZQ",Visible=.t., Left=9, Top=463,;
    Alignment=0, Width=89, Height=15,;
    Caption="Analitica"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_38 as StdString with uid="DUWVFCRSTJ",Visible=.t., Left=5, Top=272,;
    Alignment=0, Width=96, Height=17,;
    Caption="Aspetto fiscale"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_43 as StdString with uid="YTEFNGRIEZ",Visible=.t., Left=21, Top=73,;
    Alignment=1, Width=251, Height=18,;
    Caption="Non soggetto ad ammortamento:"  ;
  , bGlobalFont=.t.

  add object oStr_2_46 as StdString with uid="QHQLPRWXSG",Visible=.t., Left=21, Top=102,;
    Alignment=1, Width=251, Height=18,;
    Caption="Effettua ammortamento immediato:"  ;
  , bGlobalFont=.t.

  add object oBox_2_20 as StdBox with uid="NPYWQRICYI",left=2, top=147, width=753,height=1

  add object oBox_2_37 as StdBox with uid="QUZBAFXMLI",left=3, top=479, width=753,height=1

  add object oBox_2_39 as StdBox with uid="LIIUODSMPX",left=3, top=288, width=753,height=1

  add object oBox_2_40 as StdBox with uid="MROKXGKJAQ",left=2, top=65, width=753,height=1
  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gsce_mcc",lower(this.oContained.GSCE_MCC.class))=0
        this.oContained.GSCE_MCC.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc
enddefine
define class tgsce_acePag3 as StdContainer
  Width  = 784
  height = 541
  stdWidth  = 784
  stdheight = 541
  resizeXpos=333
  resizeYpos=402
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCOSBEN_3_9 as StdField with uid="QUDXXKKQDG",rtseq=82,rtrep=.f.,;
    cFormVar = "w_COSBEN", cQueryName = "COSBEN",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Costo gestionale del bene",;
    HelpContextID = 202194470,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=116, Top=102, cSayPict="v_PV(38+VVP)", cGetPict="v_GV(38+VVP)"

  add object oSPEPRE_3_10 as StdField with uid="ZLAVEDHAFT",rtseq=83,rtrep=.f.,;
    cFormVar = "w_SPEPRE", cQueryName = "SPEPRE",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Spese gestionale del cespite dell'esercizio precedente",;
    HelpContextID = 65691686,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=116, Top=180, cSayPict="v_PV(38+VVP)", cGetPict="v_GV(38+VVP)"

  add object oQUOESE_3_11 as StdField with uid="GEOLSUIJUF",rtseq=84,rtrep=.f.,;
    cFormVar = "w_QUOESE", cQueryName = "QUOESE",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quota accantonamento gestionale dell'esercizio",;
    HelpContextID = 66061574,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=585, Top=102, cSayPict="v_PV(38+VVP)", cGetPict="v_GV(38+VVP)"

  add object oSPECOR_3_12 as StdField with uid="UIJJXNZPFO",rtseq=85,rtrep=.f.,;
    cFormVar = "w_SPECOR", cQueryName = "SPECOR",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Spese gestionali previste per l'esercizio in corso",;
    HelpContextID = 11362342,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=585, Top=183, cSayPict="v_PV(38+VVP)", cGetPict="v_GV(38+VVP)"

  add object oCOSUNI_3_13 as StdField with uid="EJADEGOIBV",rtseq=86,rtrep=.f.,;
    cFormVar = "w_COSUNI", cQueryName = "COSUNI",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Costo unitario del cespite per unit� di misura",;
    HelpContextID = 128990758,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=585, Top=258, cSayPict="v_PV(38+VVP)", cGetPict="v_PV(38+VVP)"

  add object oUNIMIS_3_14 as StdField with uid="IBRQHUNALK",rtseq=87,rtrep=.f.,;
    cFormVar = "w_UNIMIS", cQueryName = "UNIMIS",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Unit� di misura d'impiego del cespite",;
    HelpContextID = 22519366,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=116, Top=258, InputMask=replicate('X',3)

  add object oDURBEN_3_15 as StdField with uid="WKVRZDVKUF",rtseq=88,rtrep=.f.,;
    cFormVar = "w_DURBEN", cQueryName = "DURBEN",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Durata gestionale del bene",;
    HelpContextID = 202191926,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=366, Top=102, cSayPict='"999.99"', cGetPict='"999.99"'

  add object oPERVAR_3_16 as StdField with uid="WHOADAYCXN",rtseq=89,rtrep=.f.,;
    cFormVar = "w_PERVAR", cQueryName = "PERVAR",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Variazione rispetto alle spese esercizio precedente",;
    HelpContextID = 266413302,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=366, Top=180, cSayPict='"999.99"', cGetPict='"999.99"'

  add object oQTAPRE_3_17 as StdField with uid="RFZDJYZDLQ",rtseq=90,rtrep=.f.,;
    cFormVar = "w_QTAPRE", cQueryName = "QTAPRE",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quantit� totale prevista relativa all'impiego",;
    HelpContextID = 65676294,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=366, Top=258, cSayPict="v_PQ(12)", cGetPict="v_PQ(12)"

  add object oCODI_3_19 as StdField with uid="UFAOSUKBNF",rtseq=91,rtrep=.f.,;
    cFormVar = "w_CODI", cQueryName = "CODI",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 104640986,;
   bGlobalFont=.t.,;
    Height=21, Width=156, Left=94, Top=31, InputMask=replicate('X',20)

  add object oDESC_3_20 as StdField with uid="DIZNDBSFKC",rtseq=92,rtrep=.f.,;
    cFormVar = "w_DESC", cQueryName = "DESC",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 104975306,;
   bGlobalFont=.t.,;
    Height=21, Width=303, Left=255, Top=31, InputMask=replicate('X',40)

  add object oESER_3_23 as StdField with uid="VAYBMTJRIN",rtseq=93,rtrep=.f.,;
    cFormVar = "w_ESER", cQueryName = "ESER",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(4), bMultilanguage =  .f.,;
    HelpContextID = 104046010,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=662, Top=31, InputMask=replicate('X',4)

  add object oStr_3_1 as StdString with uid="TCLJSSBYNB",Visible=.t., Left=19, Top=102,;
    Alignment=1, Width=94, Height=15,;
    Caption="Costo del bene:"  ;
  , bGlobalFont=.t.

  add object oStr_3_2 as StdString with uid="PZHTBPKYOO",Visible=.t., Left=18, Top=180,;
    Alignment=1, Width=95, Height=15,;
    Caption="Sp.es.preced.:"  ;
  , bGlobalFont=.t.

  add object oStr_3_3 as StdString with uid="EZWHENRUIB",Visible=.t., Left=26, Top=260,;
    Alignment=1, Width=87, Height=15,;
    Caption="U.M.:"  ;
  , bGlobalFont=.t.

  add object oStr_3_4 as StdString with uid="DVNNICGPFV",Visible=.t., Left=266, Top=102,;
    Alignment=1, Width=97, Height=15,;
    Caption="Durata:"  ;
  , bGlobalFont=.t.

  add object oStr_3_5 as StdString with uid="IACNTZBNSA",Visible=.t., Left=453, Top=102,;
    Alignment=1, Width=129, Height=15,;
    Caption="Quota dell'esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_3_6 as StdString with uid="OGDSHPMJSW",Visible=.t., Left=444, Top=180,;
    Alignment=1, Width=138, Height=15,;
    Caption="Sp.prev.es.in corso:"  ;
  , bGlobalFont=.t.

  add object oStr_3_7 as StdString with uid="XZHGURQQBE",Visible=.t., Left=244, Top=261,;
    Alignment=1, Width=119, Height=15,;
    Caption="Quantit� prevista:"  ;
  , bGlobalFont=.t.

  add object oStr_3_8 as StdString with uid="QXATDJARLC",Visible=.t., Left=459, Top=261,;
    Alignment=1, Width=123, Height=15,;
    Caption="Costo unitario:"  ;
  , bGlobalFont=.t.

  add object oStr_3_18 as StdString with uid="XERPRMXDQP",Visible=.t., Left=262, Top=180,;
    Alignment=1, Width=101, Height=15,;
    Caption="% Variazione:"  ;
  , bGlobalFont=.t.

  add object oStr_3_21 as StdString with uid="WQKZZGSVSN",Visible=.t., Left=5, Top=31,;
    Alignment=1, Width=85, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_22 as StdString with uid="WTOFTCJTLR",Visible=.t., Left=579, Top=31,;
    Alignment=1, Width=77, Height=15,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.
enddefine
define class tgsce_acePag4 as StdContainer
  Width  = 784
  height = 541
  stdWidth  = 784
  stdheight = 541
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object MOVIMENTI as cp_zoombox with uid="FVZJYBMGTV",left=6, top=69, width=772,height=210,;
    caption='MOVIMENTI',;
   bGlobalFont=.t.,;
    bQueryOnLoad=.f.,cZoomFile="MOVICESP",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,cTable="MOV_CESP",cMenuFile="",cZoomOnZoom="GSCE_AMC",bRetriveAllRows=.f.,bQueryOnDblClick=.t.,bNoZoomGridShape=.f.,;
    cEvent = "ActivatePage 4",;
    nPag=4;
    , HelpContextID = 208374870


  add object SALDI as cp_zoombox with uid="GKZAMHCGQM",left=6, top=333, width=772,height=153,;
    caption='SALDI',;
   bGlobalFont=.t.,;
    bQueryOnLoad=.f.,cZoomFile="SALDICESPITE",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,cTable="SAL_CESP",cMenuFile="",cZoomOnZoom="GSCE_ASC",bRetriveAllRows=.f.,bQueryOnDblClick=.t.,bNoZoomGridShape=.f.,;
    cEvent = "ActivatePage 4",;
    nPag=4;
    , HelpContextID = 28393178

  add object oCOD_4_7 as StdField with uid="NSMECPGCID",rtseq=74,rtrep=.f.,;
    cFormVar = "w_COD", cQueryName = "COD",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 109425114,;
   bGlobalFont=.t.,;
    Height=21, Width=156, Left=6, Top=19, InputMask=replicate('X',20)

  add object oDES_4_8 as StdField with uid="WDMDWOLSLJ",rtseq=75,rtrep=.f.,;
    cFormVar = "w_DES", cQueryName = "DES",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 109366218,;
   bGlobalFont=.t.,;
    Height=21, Width=303, Left=168, Top=19, InputMask=replicate('X',40)


  add object oBtn_4_10 as StdButton with uid="MFHMKLNUED",left=730, top=283, width=48,height=45,;
    CpPicture="bmp\dettagli.bmp", caption="", nPag=4;
    , ToolTipText = "Visualizza il movimento cespite associato";
    , HelpContextID = 150903649;
    , caption='\<Dettagli';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_10.Click()
      with this.Parent.oContained
        GSCE_BZC(this.Parent.oContained,.w_SERIALE)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_4_10.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_SERIALE) AND NOT((.w_FLPRIU='S' OR .w_FLPRIC='S' OR .w_FLDADI='S') AND thisform.cFunction = 'Edit') )
      endwith
    endif
  endfunc

  func oBtn_4_10.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (thisform.cFunction = 'Load')
     endwith
    endif
  endfunc

  add object oESPRI_4_12 as StdField with uid="XBVWYAUJNC",rtseq=77,rtrep=.f.,;
    cFormVar = "w_ESPRI", cQueryName = "ESPRI",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 27454906,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=702, Top=19, InputMask=replicate('X',5)

  func oESPRI_4_12.mHide()
    with this.Parent.oContained
      return (.w_CESTABEN $ 'NI')
    endwith
  endfunc


  add object oObj_4_14 as cp_runprogram with uid="CZPZNNXFJS",left=-5, top=560, width=219,height=26,;
    caption='GSCE_BZC',;
   bGlobalFont=.t.,;
    prg="GSCE_BZC(w_SERIALE)",;
    cEvent = "w_movimenti selected",;
    nPag=4;
    , HelpContextID = 240172375

  add object oStr_4_2 as StdString with uid="SFCTPGSSNH",Visible=.t., Left=6, Top=46,;
    Alignment=0, Width=155, Height=19,;
    Caption="Movimenti cespite"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_4_2.mHide()
    with this.Parent.oContained
      return (thisform.cFunction = 'Load')
    endwith
  endfunc

  add object oStr_4_3 as StdString with uid="XRHYTPTFGU",Visible=.t., Left=247, Top=304,;
    Alignment=0, Width=193, Height=18,;
    Caption="Gli importi sono tradotti in Euro"    , ForeColor=RGB(255,0,0);
  ;
  , bGlobalFont=.t.

  func oStr_4_3.mHide()
    with this.Parent.oContained
      return (thisform.cFunction = 'Load')
    endwith
  endfunc

  add object oStr_4_4 as StdString with uid="FWUANPJDLY",Visible=.t., Left=6, Top=310,;
    Alignment=0, Width=155, Height=19,;
    Caption="Saldi cespite"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_4_4.mHide()
    with this.Parent.oContained
      return (thisform.cFunction = 'Load')
    endwith
  endfunc

  add object oStr_4_6 as StdString with uid="NZSKYATEOW",Visible=.t., Left=209, Top=197,;
    Alignment=0, Width=343, Height=22,;
    Caption="Funzione non attivabile in caricamento"    , forecolor = rgb(255,0,0);
  ;
    , FontName = "Arial", FontSize = 12, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_4_6.mHide()
    with this.Parent.oContained
      return (thisform.cFunction <> 'Load')
    endwith
  endfunc

  add object oStr_4_9 as StdString with uid="GTJSOOWIMS",Visible=.t., Left=527, Top=23,;
    Alignment=1, Width=172, Height=18,;
    Caption="Esercizio di primo utilizzo:"  ;
  , bGlobalFont=.t.

  func oStr_4_9.mHide()
    with this.Parent.oContained
      return (.w_CESTABEN $ 'NI')
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsce_ace','CES_PITI','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".CECODICE=CES_PITI.CECODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
