* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_bd1                                                        *
*              Carica dettaglio componenti                                     *
*                                                                              *
*      Author: Zucchetti spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][207]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-03-17                                                      *
* Last revis.: 2001-01-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsce_bd1",oParentObject)
return(i_retval)

define class tgsce_bd1 as StdBatch
  * --- Local variables
  w_TROV = .f.
  * --- WorkFile variables
  COM_PCES_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Carica il dettaglio Componenti (da GSCE_MMC)
    this.w_TROV = .F.
    * --- Azzero il valore dei totalizzatori.
    this.oParentObject.w_TOTIMP = 0
    this.oParentObject.w_TOTIMPC = 0
    if NOT EMPTY(this.oParentObject.w_CODCES)
      * --- Azzera il Transitorio
      SELECT (this.oParentObject.cTrsName)
      GO TOP
      DELETE ALL
      vq_exec("..\CESP\EXE\QUERY\Compone.vqr",this,"Aggiorna")
      Select Aggiorna
      Go Top
      Scan for Not Empty(Nvl(Aggiorna.Cocompon," "))
      this.w_TROV = .T.
      * --- Inserisce Nuova Riga
      this.oParentObject.InitRow()
      this.oParentObject.w_MCCOMPON = Nvl(Aggiorna.Cocompon," ")
      this.oParentObject.w_MCIMPTOT = 0
      this.oParentObject.w_MCIMPTOC = 0
      this.oParentObject.w_VALUTA = Nvl(Aggiorna.Covaluta," ")
      this.oParentObject.w_MCIMPVAL = 0
      this.oParentObject.w_MCIMPVAC = 0
      * --- Aggiorna il Transitorio
      this.oParentObject.TrsFromWork()
      Endscan
    endif
    if used("Aggiorna")
      select ("Aggiorna")
      use
    endif
    if this.w_TROV=.F.
      * --- Carica Almeno la Prima Riga
      this.oParentObject.BlankRec()
    endif
    * --- Questa Parte derivata dal Metodo LoadRec
    SELECT (this.oParentObject.cTrsName)
    GO TOP
    With this.oParentObject
    .WorkFromTrs()
    .SetControlsValue()
    .ChildrenChangeRow()
    .oPgFrm.Page1.oPag.oBody.Refresh()
    .oPgFrm.Page1.oPag.oBody.nAbsRow=1
    .oPgFrm.Page1.oPag.oBody.nRelRow=1
    EndWith
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='COM_PCES'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
