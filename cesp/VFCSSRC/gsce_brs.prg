* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_brs                                                        *
*              Ricostruzione saldi cespiti                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_54]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-03-05                                                      *
* Last revis.: 2009-11-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsce_brs",oParentObject,m.pOper)
return(i_retval)

define class tgsce_brs as StdBatch
  * --- Local variables
  pOper = space(10)
  w_MCCODCES = space(20)
  w_FLGA01 = space(1)
  w_TIPCES = space(2)
  cFunction = space(20)
  w_ERROR = .f.
  w_DTPRIU = ctod("  /  /  ")
  w_ESERC = space(4)
  w_MCCODCES1 = space(20)
  w_MCDATREG = ctod("  /  /  ")
  w_IMPVAL = 0
  w_FLDADI = space(1)
  w_COMPONE = space(40)
  w_CESPITE = space(20)
  w_MCDATDIS = ctod("  /  /  ")
  w_DTPRIC = ctod("  /  /  ")
  w_CODAZI = space(5)
  w_TMPES = space(4)
  w_ERROR = .f.
  w_FINECONS = ctod("  /  /  ")
  w_IMPVAC = 0
  * --- WorkFile variables
  CES_PITI_idx=0
  SAL_CESP_idx=0
  MOV_PCES_idx=0
  COM_PCES_idx=0
  VALUTE_idx=0
  ESERCIZI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Ricostruzione Saldi Cespiti (da GSCE_KRS)
    *     - R- Esegue ricostruzione saldi
    *     - E - Determina se esercizio selezionato ultimo in lire...
    this.w_CODAZI = i_CODAZI
    if this.pOper="E"
      * --- Determino se esercizio selezionato � ultimo in lire, per farlo scorro gli esercizi 
      *     in lire in ordine di data inizio ascedente...
      if this.oParentObject.w_CODVAL=g_CODLIR
        * --- Select from ESERCIZI
        i_nConn=i_TableProp[this.ESERCIZI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2],.t.,this.ESERCIZI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select ESCODESE  from "+i_cTable+" ESERCIZI ";
              +" where ESCODAZI= "+cp_ToStrODBC(this.w_CODAZI)+" And ESVALNAZ= "+cp_ToStrODBC(this.oParentObject.w_CODVAL)+"";
              +" order by ESINIESE";
               ,"_Curs_ESERCIZI")
        else
          select ESCODESE from (i_cTable);
           where ESCODAZI= this.w_CODAZI And ESVALNAZ= this.oParentObject.w_CODVAL;
           order by ESINIESE;
            into cursor _Curs_ESERCIZI
        endif
        if used('_Curs_ESERCIZI')
          select _Curs_ESERCIZI
          locate for 1=1
          do while not(eof())
          this.w_TMPES = _Curs_ESERCIZI.ESCODESE
            select _Curs_ESERCIZI
            continue
          enddo
          use
        endif
        this.oParentObject.w_LASTLIT = ( this.oParentObject.w_COMPET = this.w_TMPES )
      else
        this.oParentObject.w_LASTLIT = .F.
      endif
      i_retcode = 'stop'
      return
    endif
    this.cFunction = "Ricos"
    * --- Verifica se l'esercizio � successivo all'esercizio di consolidamento,
    *     questo se ricostruzione "normale", se ricostruzione saldi in lire non svolge in controllo..
    * --- Leggo la data di fine esercizio consolidamento
    * --- Read from ESERCIZI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ESERCIZI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2],.t.,this.ESERCIZI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ESFINESE"+;
        " from "+i_cTable+" ESERCIZI where ";
            +"ESCODAZI = "+cp_ToStrODBC(i_CODAZI);
            +" and ESCODESE = "+cp_ToStrODBC(this.oParentObject.w_DATCON);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ESFINESE;
        from (i_cTable) where;
            ESCODAZI = i_CODAZI;
            and ESCODESE = this.oParentObject.w_DATCON;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_FINECONS = NVL(cp_ToDate(_read_.ESFINESE),cp_NullValue(_read_.ESFINESE))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if this.oParentObject.w_INIESE<=this.w_FINECONS AND NOT EMPTY(this.oParentObject.w_DATCON) And this.oParentObject.w_SALCOM<>"S"
      ah_ErrorMsg("L'esercizio selezionato deve essere maggiore dell'esercizio di consolidamento presente nei parametri cespiti [%1]",,"", this.oParentObject.w_DATCON)
      i_retcode = 'stop'
      return
    endif
    if empty(this.oParentObject.w_COMPET)
      if ! ah_yesno("I saldi di tutti gli esercizi verranno ricostruiti%0Confermi ugualmente?")
        ah_Msg("Operazione annullata")
        i_retcode = 'stop'
        return
      endif
    endif
    * --- Qui deve Bloccare l'Archivio dei Saldi!!
    * --- Try
    local bErr_03881438
    bErr_03881438=bTrsErr
    this.Try_03881438()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      ah_ErrorMsg(i_ErrMsg,,"")
    endif
    bTrsErr=bTrsErr or bErr_03881438
    * --- End
  endproc
  proc Try_03881438()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    if this.oParentObject.w_SALCOM="S"
      GSCE_BRE(this, this.oParentObject.w_INIESE ) 
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      * --- Azzera dati del Periodo
      ah_Msg("Fase azzeramento saldi periodo...")
      * --- Write into SAL_CESP
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.SAL_CESP_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SAL_CESP_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SAL_CESP_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"SCINCVAL ="+cp_NullLink(cp_ToStrODBC(0),'SAL_CESP','SCINCVAL');
        +",SCIMPONS ="+cp_NullLink(cp_ToStrODBC(0),'SAL_CESP','SCIMPONS');
        +",SCIMPRIV ="+cp_NullLink(cp_ToStrODBC(0),'SAL_CESP','SCIMPRIV');
        +",SCDECVAL ="+cp_NullLink(cp_ToStrODBC(0),'SAL_CESP','SCDECVAL');
        +",SCIMPSVA ="+cp_NullLink(cp_ToStrODBC(0),'SAL_CESP','SCIMPSVA');
        +",SCACCCIV ="+cp_NullLink(cp_ToStrODBC(0),'SAL_CESP','SCACCCIV');
        +",SCUTICIV ="+cp_NullLink(cp_ToStrODBC(0),'SAL_CESP','SCUTICIV');
        +",SCACCFIS ="+cp_NullLink(cp_ToStrODBC(0),'SAL_CESP','SCACCFIS');
        +",SCUTIFIS ="+cp_NullLink(cp_ToStrODBC(0),'SAL_CESP','SCUTIFIS');
        +",SCIMPNOA ="+cp_NullLink(cp_ToStrODBC(0),'SAL_CESP','SCIMPNOA');
        +",SCQUOPER ="+cp_NullLink(cp_ToStrODBC(0),'SAL_CESP','SCQUOPER');
        +",SCPLUSVA ="+cp_NullLink(cp_ToStrODBC(0),'SAL_CESP','SCPLUSVA');
        +",SCMINSVA ="+cp_NullLink(cp_ToStrODBC(0),'SAL_CESP','SCMINSVA');
        +",SCACCANT ="+cp_NullLink(cp_ToStrODBC(0),'SAL_CESP','SCACCANT');
        +",SCUTIANT ="+cp_NullLink(cp_ToStrODBC(0),'SAL_CESP','SCUTIANT');
        +",SCIMPRIC ="+cp_NullLink(cp_ToStrODBC(0),'SAL_CESP','SCIMPRIC');
        +",SCMINSVC ="+cp_NullLink(cp_ToStrODBC(0),'SAL_CESP','SCMINSVC');
        +",SCPLUSVC ="+cp_NullLink(cp_ToStrODBC(0),'SAL_CESP','SCPLUSVC');
        +",SCIMPSVC ="+cp_NullLink(cp_ToStrODBC(0),'SAL_CESP','SCIMPSVC');
        +",SCDECVAC ="+cp_NullLink(cp_ToStrODBC(0),'SAL_CESP','SCDECVAC');
        +",SCIMPONC ="+cp_NullLink(cp_ToStrODBC(0),'SAL_CESP','SCIMPONC');
        +",SCINCVAC ="+cp_NullLink(cp_ToStrODBC(0),'SAL_CESP','SCINCVAC');
        +",SCQTACES ="+cp_NullLink(cp_ToStrODBC(0),'SAL_CESP','SCQTACES');
            +i_ccchkf ;
        +" where ";
            +"SCCODESE = "+cp_ToStrODBC(this.oParentObject.w_COMPET);
               )
      else
        update (i_cTable) set;
            SCINCVAL = 0;
            ,SCIMPONS = 0;
            ,SCIMPRIV = 0;
            ,SCDECVAL = 0;
            ,SCIMPSVA = 0;
            ,SCACCCIV = 0;
            ,SCUTICIV = 0;
            ,SCACCFIS = 0;
            ,SCUTIFIS = 0;
            ,SCIMPNOA = 0;
            ,SCQUOPER = 0;
            ,SCPLUSVA = 0;
            ,SCMINSVA = 0;
            ,SCACCANT = 0;
            ,SCUTIANT = 0;
            ,SCIMPRIC = 0;
            ,SCMINSVC = 0;
            ,SCPLUSVC = 0;
            ,SCIMPSVC = 0;
            ,SCDECVAC = 0;
            ,SCIMPONC = 0;
            ,SCINCVAC = 0;
            ,SCQTACES = 0;
            &i_ccchkf. ;
         where;
            SCCODESE = this.oParentObject.w_COMPET;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Aggiorna Saldi del Periodo
      * --- Select from GSCE_BRS
      do vq_exec with 'GSCE_BRS',this,'_Curs_GSCE_BRS','',.f.,.t.
      if used('_Curs_GSCE_BRS')
        select _Curs_GSCE_BRS
        locate for 1=1
        do while not(eof())
        ah_Msg("Aggiorna cespite: %1",.T.,.F.,.F., _Curs_GSCE_BRS.MCCODCES+" "+_Curs_GSCE_BRS.MCCOMPET)
        * --- Try
        local bErr_03880E38
        bErr_03880E38=bTrsErr
        this.Try_03880E38()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          * --- Try
          local bErr_03880388
          bErr_03880388=bTrsErr
          this.Try_03880388()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- Raise
            i_Error="Errore scrittura archivio saldi cespiti - operazione annullata -"
            return
          endif
          bTrsErr=bTrsErr or bErr_03880388
          * --- End
        endif
        bTrsErr=bTrsErr or bErr_03880E38
        * --- End
          select _Curs_GSCE_BRS
          continue
        enddo
        use
      endif
      * --- Aggiorna Flag cespiste ceduto
      * --- Select from GSCE1BRS
      do vq_exec with 'GSCE1BRS',this,'_Curs_GSCE1BRS','',.f.,.t.
      if used('_Curs_GSCE1BRS')
        select _Curs_GSCE1BRS
        locate for 1=1
        do while not(eof())
        ah_Msg("Aggiorna cessione cespite: %1",.T.,.F.,.F., _Curs_GSCE1BRS.MCCODCES+" "+_Curs_GSCE1BRS.MCCOMPET)
        this.w_MCCODCES = _Curs_GSCE1BRS.MCCODCES
        this.w_FLGA01 = _Curs_GSCE1BRS.FLGA01
        this.w_TIPCES = _Curs_GSCE1BRS.TIPCES
        this.w_MCDATREG = Cp_Todate(_Curs_GSCE1BRS.MCDATREG)
        this.w_MCDATDIS = Cp_Todate(_Curs_GSCE1BRS.MCDATDIS)
        GSCE_BMC(this,"BRS")
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if i_RETCODE="stop" or not empty(i_Error)
          RETURN
        endif
          select _Curs_GSCE1BRS
          continue
        enddo
        use
      endif
      * --- Aggiorna data primo utilizzo ed esercizio
      * --- Select from GSCE2BRS
      do vq_exec with 'GSCE2BRS',this,'_Curs_GSCE2BRS','',.f.,.t.
      if used('_Curs_GSCE2BRS')
        select _Curs_GSCE2BRS
        locate for 1=1
        do while not(eof())
        ah_Msg("Aggiorna data utilizzo: %1",.T.,.F.,.F., _Curs_GSCE2BRS.MCCODCES+" "+_Curs_GSCE2BRS.MCCOMPET)
        this.w_MCCODCES1 = _Curs_GSCE2BRS.MCCODCES
        this.w_DTPRIU = CP_TODATE(_Curs_GSCE2BRS.MCDTPRIU)
        this.w_DTPRIC = CP_TODATE(_Curs_GSCE2BRS.MCDTPRIC)
        this.w_ESERC = _Curs_GSCE2BRS.MCCOMPET
        * --- Controllo se la data di primo utilizzo fiscale � stata valorizzata nei movimenti cespiti
        if NOT EMPTY(this.w_DTPRIU)
          * --- Write into CES_PITI
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.CES_PITI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CES_PITI_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.CES_PITI_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CEDTPRIU ="+cp_NullLink(cp_ToStrODBC(this.w_DTPRIU),'CES_PITI','CEDTPRIU');
            +",CEESPRIU ="+cp_NullLink(cp_ToStrODBC(this.w_ESERC),'CES_PITI','CEESPRIU');
                +i_ccchkf ;
            +" where ";
                +"CECODICE = "+cp_ToStrODBC(this.w_MCCODCES1);
                   )
          else
            update (i_cTable) set;
                CEDTPRIU = this.w_DTPRIU;
                ,CEESPRIU = this.w_ESERC;
                &i_ccchkf. ;
             where;
                CECODICE = this.w_MCCODCES1;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        * --- Controllo se la data di primo utilizzo civile � stata valorizzata nei movimenti cespiti
        if NOT EMPTY(this.w_DTPRIC)
          * --- Write into CES_PITI
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.CES_PITI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CES_PITI_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.CES_PITI_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CEDTPRIC ="+cp_NullLink(cp_ToStrODBC(this.w_DTPRIC),'CES_PITI','CEDTPRIC');
            +",CEESPRIC ="+cp_NullLink(cp_ToStrODBC(this.w_ESERC),'CES_PITI','CEESPRIC');
                +i_ccchkf ;
            +" where ";
                +"CECODICE = "+cp_ToStrODBC(this.w_MCCODCES1);
                   )
          else
            update (i_cTable) set;
                CEDTPRIC = this.w_DTPRIC;
                ,CEESPRIC = this.w_ESERC;
                &i_ccchkf. ;
             where;
                CECODICE = this.w_MCCODCES1;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
          select _Curs_GSCE2BRS
          continue
        enddo
        use
      endif
      * --- Aggiorno il valore dei componenti.
      vq_exec("..\CESP\EXE\QUERY\gsce2biv.vqr",this,"Aggiorna")
      Select Aggiorna
      Go Top
      Scan
      this.w_CESPITE = Aggiorna.Mccodces
      this.w_COMPONE = Aggiorna.Mccompon
      this.w_IMPVAL = Aggiorna.Mcimpval
      this.w_IMPVAC = Aggiorna.Mcimpvac
      * --- Write into COM_PCES
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.COM_PCES_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.COM_PCES_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.COM_PCES_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"COIMPTOT ="+cp_NullLink(cp_ToStrODBC(this.w_IMPVAL),'COM_PCES','COIMPTOT');
        +",COIMPTOC ="+cp_NullLink(cp_ToStrODBC(this.w_IMPVAC),'COM_PCES','COIMPTOC');
            +i_ccchkf ;
        +" where ";
            +"COCODICE = "+cp_ToStrODBC(this.w_CESPITE);
            +" and COCOMPON = "+cp_ToStrODBC(this.w_COMPONE);
               )
      else
        update (i_cTable) set;
            COIMPTOT = this.w_IMPVAL;
            ,COIMPTOC = this.w_IMPVAC;
            &i_ccchkf. ;
         where;
            COCODICE = this.w_CESPITE;
            and COCOMPON = this.w_COMPONE;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      Endscan
      if used("Aggiorna")
        select ("Aggiorna")
        use
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    ah_Msg("Ricostruzione saldi terminata")
    return
  proc Try_03880E38()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SAL_CESP
    i_nConn=i_TableProp[this.SAL_CESP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SAL_CESP_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SAL_CESP_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SCCODCES"+",SCCODESE"+",SCINCVAL"+",SCIMPONS"+",SCIMPRIV"+",SCDECVAL"+",SCIMPSVA"+",SCACCCIV"+",SCUTICIV"+",SCACCFIS"+",SCUTIFIS"+",SCIMPNOA"+",SCQUOPER"+",SCPLUSVA"+",SCMINSVA"+",SCACCANT"+",SCUTIANT"+",SCIMPRIC"+",SCMINSVC"+",SCPLUSVC"+",SCIMPSVC"+",SCDECVAC"+",SCIMPONC"+",SCINCVAC"+",SCQTACES"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(_Curs_GSCE_BRS.MCCODCES),'SAL_CESP','SCCODCES');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_GSCE_BRS.MCCOMPET),'SAL_CESP','SCCODESE');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_GSCE_BRS.MCIMPA02),'SAL_CESP','SCINCVAL');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_GSCE_BRS.MCIMPA03),'SAL_CESP','SCIMPONS');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_GSCE_BRS.MCIMPA04),'SAL_CESP','SCIMPRIV');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_GSCE_BRS.MCIMPA05),'SAL_CESP','SCDECVAL');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_GSCE_BRS.MCIMPA06),'SAL_CESP','SCIMPSVA');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_GSCE_BRS.MCIMPA07),'SAL_CESP','SCACCCIV');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_GSCE_BRS.MCIMPA08),'SAL_CESP','SCUTICIV');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_GSCE_BRS.MCIMPA09),'SAL_CESP','SCACCFIS');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_GSCE_BRS.MCIMPA10),'SAL_CESP','SCUTIFIS');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_GSCE_BRS.MCIMPA11),'SAL_CESP','SCIMPNOA');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_GSCE_BRS.MCIMPA12),'SAL_CESP','SCQUOPER');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_GSCE_BRS.MCIMPA13),'SAL_CESP','SCPLUSVA');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_GSCE_BRS.MCIMPA14),'SAL_CESP','SCMINSVA');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_GSCE_BRS.MCIMPA15),'SAL_CESP','SCACCANT');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_GSCE_BRS.MCIMPA16),'SAL_CESP','SCUTIANT');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_GSCE_BRS.MCIMPA18),'SAL_CESP','SCIMPRIC');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_GSCE_BRS.MCIMPA21),'SAL_CESP','SCMINSVC');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_GSCE_BRS.MCIMPA20),'SAL_CESP','SCPLUSVC');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_GSCE_BRS.MCIMPA19),'SAL_CESP','SCIMPSVC');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_GSCE_BRS.MCIMPA22),'SAL_CESP','SCDECVAC');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_GSCE_BRS.MCIMPA24),'SAL_CESP','SCIMPONC');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_GSCE_BRS.MCIMPA23),'SAL_CESP','SCINCVAC');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_GSCE_BRS.MCQTACES),'SAL_CESP','SCQTACES');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SCCODCES',_Curs_GSCE_BRS.MCCODCES,'SCCODESE',_Curs_GSCE_BRS.MCCOMPET,'SCINCVAL',_Curs_GSCE_BRS.MCIMPA02,'SCIMPONS',_Curs_GSCE_BRS.MCIMPA03,'SCIMPRIV',_Curs_GSCE_BRS.MCIMPA04,'SCDECVAL',_Curs_GSCE_BRS.MCIMPA05,'SCIMPSVA',_Curs_GSCE_BRS.MCIMPA06,'SCACCCIV',_Curs_GSCE_BRS.MCIMPA07,'SCUTICIV',_Curs_GSCE_BRS.MCIMPA08,'SCACCFIS',_Curs_GSCE_BRS.MCIMPA09,'SCUTIFIS',_Curs_GSCE_BRS.MCIMPA10,'SCIMPNOA',_Curs_GSCE_BRS.MCIMPA11)
      insert into (i_cTable) (SCCODCES,SCCODESE,SCINCVAL,SCIMPONS,SCIMPRIV,SCDECVAL,SCIMPSVA,SCACCCIV,SCUTICIV,SCACCFIS,SCUTIFIS,SCIMPNOA,SCQUOPER,SCPLUSVA,SCMINSVA,SCACCANT,SCUTIANT,SCIMPRIC,SCMINSVC,SCPLUSVC,SCIMPSVC,SCDECVAC,SCIMPONC,SCINCVAC,SCQTACES &i_ccchkf. );
         values (;
           _Curs_GSCE_BRS.MCCODCES;
           ,_Curs_GSCE_BRS.MCCOMPET;
           ,_Curs_GSCE_BRS.MCIMPA02;
           ,_Curs_GSCE_BRS.MCIMPA03;
           ,_Curs_GSCE_BRS.MCIMPA04;
           ,_Curs_GSCE_BRS.MCIMPA05;
           ,_Curs_GSCE_BRS.MCIMPA06;
           ,_Curs_GSCE_BRS.MCIMPA07;
           ,_Curs_GSCE_BRS.MCIMPA08;
           ,_Curs_GSCE_BRS.MCIMPA09;
           ,_Curs_GSCE_BRS.MCIMPA10;
           ,_Curs_GSCE_BRS.MCIMPA11;
           ,_Curs_GSCE_BRS.MCIMPA12;
           ,_Curs_GSCE_BRS.MCIMPA13;
           ,_Curs_GSCE_BRS.MCIMPA14;
           ,_Curs_GSCE_BRS.MCIMPA15;
           ,_Curs_GSCE_BRS.MCIMPA16;
           ,_Curs_GSCE_BRS.MCIMPA18;
           ,_Curs_GSCE_BRS.MCIMPA21;
           ,_Curs_GSCE_BRS.MCIMPA20;
           ,_Curs_GSCE_BRS.MCIMPA19;
           ,_Curs_GSCE_BRS.MCIMPA22;
           ,_Curs_GSCE_BRS.MCIMPA24;
           ,_Curs_GSCE_BRS.MCIMPA23;
           ,_Curs_GSCE_BRS.MCQTACES;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_03880388()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into SAL_CESP
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SAL_CESP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SAL_CESP_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.SAL_CESP_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"SCINCVAL ="+cp_NullLink(cp_ToStrODBC(_Curs_GSCE_BRS.MCIMPA02),'SAL_CESP','SCINCVAL');
      +",SCIMPONS ="+cp_NullLink(cp_ToStrODBC(_Curs_GSCE_BRS.MCIMPA03),'SAL_CESP','SCIMPONS');
      +",SCIMPRIV ="+cp_NullLink(cp_ToStrODBC(_Curs_GSCE_BRS.MCIMPA04),'SAL_CESP','SCIMPRIV');
      +",SCDECVAL ="+cp_NullLink(cp_ToStrODBC(_Curs_GSCE_BRS.MCIMPA05),'SAL_CESP','SCDECVAL');
      +",SCIMPSVA ="+cp_NullLink(cp_ToStrODBC(_Curs_GSCE_BRS.MCIMPA06),'SAL_CESP','SCIMPSVA');
      +",SCACCCIV ="+cp_NullLink(cp_ToStrODBC(_Curs_GSCE_BRS.MCIMPA07),'SAL_CESP','SCACCCIV');
      +",SCUTICIV ="+cp_NullLink(cp_ToStrODBC(_Curs_GSCE_BRS.MCIMPA08),'SAL_CESP','SCUTICIV');
      +",SCACCFIS ="+cp_NullLink(cp_ToStrODBC(_Curs_GSCE_BRS.MCIMPA09),'SAL_CESP','SCACCFIS');
      +",SCUTIFIS ="+cp_NullLink(cp_ToStrODBC(_Curs_GSCE_BRS.MCIMPA10),'SAL_CESP','SCUTIFIS');
      +",SCIMPNOA ="+cp_NullLink(cp_ToStrODBC(_Curs_GSCE_BRS.MCIMPA11),'SAL_CESP','SCIMPNOA');
      +",SCQUOPER ="+cp_NullLink(cp_ToStrODBC(_Curs_GSCE_BRS.MCIMPA12),'SAL_CESP','SCQUOPER');
      +",SCPLUSVA ="+cp_NullLink(cp_ToStrODBC(_Curs_GSCE_BRS.MCIMPA13),'SAL_CESP','SCPLUSVA');
      +",SCMINSVA ="+cp_NullLink(cp_ToStrODBC(_Curs_GSCE_BRS.MCIMPA14),'SAL_CESP','SCMINSVA');
      +",SCACCANT ="+cp_NullLink(cp_ToStrODBC(_Curs_GSCE_BRS.MCIMPA15),'SAL_CESP','SCACCANT');
      +",SCUTIANT ="+cp_NullLink(cp_ToStrODBC(_Curs_GSCE_BRS.MCIMPA16),'SAL_CESP','SCUTIANT');
      +",SCIMPRIC ="+cp_NullLink(cp_ToStrODBC(_Curs_GSCE_BRS.MCIMPA18),'SAL_CESP','SCIMPRIC');
      +",SCMINSVC ="+cp_NullLink(cp_ToStrODBC(_Curs_GSCE_BRS.MCIMPA21),'SAL_CESP','SCMINSVC');
      +",SCPLUSVC ="+cp_NullLink(cp_ToStrODBC(_Curs_GSCE_BRS.MCIMPA20),'SAL_CESP','SCPLUSVC');
      +",SCIMPSVC ="+cp_NullLink(cp_ToStrODBC(_Curs_GSCE_BRS.MCIMPA19),'SAL_CESP','SCIMPSVC');
      +",SCDECVAC ="+cp_NullLink(cp_ToStrODBC(_Curs_GSCE_BRS.MCIMPA22),'SAL_CESP','SCDECVAC');
      +",SCIMPONC ="+cp_NullLink(cp_ToStrODBC(_Curs_GSCE_BRS.MCIMPA24),'SAL_CESP','SCIMPONC');
      +",SCINCVAC ="+cp_NullLink(cp_ToStrODBC(_Curs_GSCE_BRS.MCIMPA23),'SAL_CESP','SCINCVAC');
      +",SCQTACES ="+cp_NullLink(cp_ToStrODBC(_Curs_GSCE_BRS.MCQTACES),'SAL_CESP','SCQTACES');
          +i_ccchkf ;
      +" where ";
          +"SCCODCES = "+cp_ToStrODBC(_Curs_GSCE_BRS.MCCODCES);
          +" and SCCODESE = "+cp_ToStrODBC(_Curs_GSCE_BRS.MCCOMPET);
             )
    else
      update (i_cTable) set;
          SCINCVAL = _Curs_GSCE_BRS.MCIMPA02;
          ,SCIMPONS = _Curs_GSCE_BRS.MCIMPA03;
          ,SCIMPRIV = _Curs_GSCE_BRS.MCIMPA04;
          ,SCDECVAL = _Curs_GSCE_BRS.MCIMPA05;
          ,SCIMPSVA = _Curs_GSCE_BRS.MCIMPA06;
          ,SCACCCIV = _Curs_GSCE_BRS.MCIMPA07;
          ,SCUTICIV = _Curs_GSCE_BRS.MCIMPA08;
          ,SCACCFIS = _Curs_GSCE_BRS.MCIMPA09;
          ,SCUTIFIS = _Curs_GSCE_BRS.MCIMPA10;
          ,SCIMPNOA = _Curs_GSCE_BRS.MCIMPA11;
          ,SCQUOPER = _Curs_GSCE_BRS.MCIMPA12;
          ,SCPLUSVA = _Curs_GSCE_BRS.MCIMPA13;
          ,SCMINSVA = _Curs_GSCE_BRS.MCIMPA14;
          ,SCACCANT = _Curs_GSCE_BRS.MCIMPA15;
          ,SCUTIANT = _Curs_GSCE_BRS.MCIMPA16;
          ,SCIMPRIC = _Curs_GSCE_BRS.MCIMPA18;
          ,SCMINSVC = _Curs_GSCE_BRS.MCIMPA21;
          ,SCPLUSVC = _Curs_GSCE_BRS.MCIMPA20;
          ,SCIMPSVC = _Curs_GSCE_BRS.MCIMPA19;
          ,SCDECVAC = _Curs_GSCE_BRS.MCIMPA22;
          ,SCIMPONC = _Curs_GSCE_BRS.MCIMPA24;
          ,SCINCVAC = _Curs_GSCE_BRS.MCIMPA23;
          ,SCQTACES = _Curs_GSCE_BRS.MCQTACES;
          &i_ccchkf. ;
       where;
          SCCODCES = _Curs_GSCE_BRS.MCCODCES;
          and SCCODESE = _Curs_GSCE_BRS.MCCOMPET;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,6)]
    this.cWorkTables[1]='CES_PITI'
    this.cWorkTables[2]='SAL_CESP'
    this.cWorkTables[3]='MOV_PCES'
    this.cWorkTables[4]='COM_PCES'
    this.cWorkTables[5]='VALUTE'
    this.cWorkTables[6]='ESERCIZI'
    return(this.OpenAllTables(6))

  proc CloseCursors()
    if used('_Curs_ESERCIZI')
      use in _Curs_ESERCIZI
    endif
    if used('_Curs_GSCE_BRS')
      use in _Curs_GSCE_BRS
    endif
    if used('_Curs_GSCE1BRS')
      use in _Curs_GSCE1BRS
    endif
    if used('_Curs_GSCE2BRS')
      use in _Curs_GSCE2BRS
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
