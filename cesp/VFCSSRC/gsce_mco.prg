* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_mco                                                        *
*              Componenti                                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30] [VRS_19]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-02-23                                                      *
* Last revis.: 2011-02-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsce_mco")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsce_mco")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsce_mco")
  return

* --- Class definition
define class tgsce_mco as StdPCForm
  Width  = 949
  Height = 289
  Top    = 156
  Left   = 3
  cComment = "Componenti"
  cPrg = "gsce_mco"
  HelpContextID=171293335
  add object cnt as tcgsce_mco
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsce_mco as PCContext
  w_COCODICE = space(20)
  w_OBTEST = space(8)
  w_COVALUTA = space(3)
  w_DECTOT = 0
  w_CALCPIC = 0
  w_COCOMPON = space(40)
  w_COIMPTOT = 0
  w_TOTVAL = 0
  w_DTOBSO = space(8)
  w_DESVAL = space(35)
  w_COIMPTOC = 0
  w_TOTVAC = 0
  proc Save(i_oFrom)
    this.w_COCODICE = i_oFrom.w_COCODICE
    this.w_OBTEST = i_oFrom.w_OBTEST
    this.w_COVALUTA = i_oFrom.w_COVALUTA
    this.w_DECTOT = i_oFrom.w_DECTOT
    this.w_CALCPIC = i_oFrom.w_CALCPIC
    this.w_COCOMPON = i_oFrom.w_COCOMPON
    this.w_COIMPTOT = i_oFrom.w_COIMPTOT
    this.w_TOTVAL = i_oFrom.w_TOTVAL
    this.w_DTOBSO = i_oFrom.w_DTOBSO
    this.w_DESVAL = i_oFrom.w_DESVAL
    this.w_COIMPTOC = i_oFrom.w_COIMPTOC
    this.w_TOTVAC = i_oFrom.w_TOTVAC
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_COCODICE = this.w_COCODICE
    i_oTo.w_OBTEST = this.w_OBTEST
    i_oTo.w_COVALUTA = this.w_COVALUTA
    i_oTo.w_DECTOT = this.w_DECTOT
    i_oTo.w_CALCPIC = this.w_CALCPIC
    i_oTo.w_COCOMPON = this.w_COCOMPON
    i_oTo.w_COIMPTOT = this.w_COIMPTOT
    i_oTo.w_TOTVAL = this.w_TOTVAL
    i_oTo.w_DTOBSO = this.w_DTOBSO
    i_oTo.w_DESVAL = this.w_DESVAL
    i_oTo.w_COIMPTOC = this.w_COIMPTOC
    i_oTo.w_TOTVAC = this.w_TOTVAC
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsce_mco as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 949
  Height = 289
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-02-25"
  HelpContextID=171293335
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=12

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  COM_PCES_IDX = 0
  COM_PUBI_IDX = 0
  VALUTE_IDX = 0
  cFile = "COM_PCES"
  cKeySelect = "COCODICE"
  cKeyWhere  = "COCODICE=this.w_COCODICE"
  cKeyDetail  = "COCODICE=this.w_COCODICE and COCOMPON=this.w_COCOMPON"
  cKeyWhereODBC = '"COCODICE="+cp_ToStrODBC(this.w_COCODICE)';

  cKeyDetailWhereODBC = '"COCODICE="+cp_ToStrODBC(this.w_COCODICE)';
      +'+" and COCOMPON="+cp_ToStrODBC(this.w_COCOMPON)';

  cKeyWhereODBCqualified = '"COM_PCES.COCODICE="+cp_ToStrODBC(this.w_COCODICE)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cPrg = "gsce_mco"
  cComment = "Componenti"
  i_nRowNum = 0
  i_nRowPerPage = 11
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_COCODICE = space(20)
  w_OBTEST = ctod('  /  /  ')
  w_COVALUTA = space(3)
  o_COVALUTA = space(3)
  w_DECTOT = 0
  w_CALCPIC = 0
  w_COCOMPON = space(40)
  w_COIMPTOT = 0
  w_TOTVAL = 0
  w_DTOBSO = ctod('  /  /  ')
  w_DESVAL = space(35)
  w_COIMPTOC = 0
  w_TOTVAC = 0

  * --- Children pointers
  GSCE_MCU = .NULL.
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    *set procedure to GSCE_MCU additive
    with this
      .Pages(1).addobject("oPag","tgsce_mcoPag1","gsce_mco",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCOVALUTA_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    *release procedure GSCE_MCU
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='COM_PUBI'
    this.cWorkTables[2]='VALUTE'
    this.cWorkTables[3]='COM_PCES'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.COM_PCES_IDX,5],7]
    this.nPostItConn=i_TableProp[this.COM_PCES_IDX,3]
  return

  function CreateChildren()
    this.GSCE_MCU = CREATEOBJECT('stdDynamicChild',this,'GSCE_MCU',this.oPgFrm.Page1.oPag.oLinkPC_2_3)
    this.GSCE_MCU.createrealchild()
    return

  procedure NewContext()
    return(createobject('tsgsce_mco'))

  function DestroyChildrenChain()
    this.oParentObject=.NULL.
    if !ISNULL(this.GSCE_MCU)
      this.GSCE_MCU.DestroyChildrenChain()
    endif
    return

  function HideChildrenChain()
    *this.Hide()
    this.bOnScreen = .f.
    this.GSCE_MCU.HideChildrenChain()
    return

  function ShowChildrenChain()
    this.bOnScreen=.t.
    this.GSCE_MCU.ShowChildrenChain()
    DoDefault()
    return
  procedure DestroyChildren()
    if !ISNULL(this.GSCE_MCU)
      this.GSCE_MCU.DestroyChildrenChain()
      this.GSCE_MCU=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_2_3')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSCE_MCU.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSCE_MCU.IsAChildUpdated(.t.)
    endif	
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSCE_MCU.NewDocument()
    return

  procedure ChildrenChangeRow()
    local i_cOldSel,i_cRow
    i_cOldSel=select()
    select (this.cTrsName)
    i_cRow=str(recno(),7,0)
    with this
      .GSCE_MCU.ChangeRow(this.cRowID+i_cRow,1;
             ,.w_COCODICE,"CUCODICE";
             ,.w_COCOMPON,"CUCOMPON";
             )
    endwith
    select (i_cOldSel)
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_3_joined
    link_1_3_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from COM_PCES where COCODICE=KeySet.COCODICE
    *                            and COCOMPON=KeySet.COCOMPON
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.COM_PCES_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.COM_PCES_IDX,2],this.bLoadRecFilter,this.COM_PCES_IDX,"gsce_mco")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('COM_PCES')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "COM_PCES.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' COM_PCES '
      link_1_3_joined=this.AddJoinedLink_1_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'COCODICE',this.w_COCODICE  )
      select * from (i_cTable) COM_PCES where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DECTOT = 0
        .w_TOTVAL = 0
        .w_DTOBSO = ctod("  /  /  ")
        .w_DESVAL = space(35)
        .w_TOTVAC = 0
        .w_COCODICE = NVL(COCODICE,space(20))
        .w_OBTEST = i_DATSYS
        .w_COVALUTA = NVL(COVALUTA,space(3))
          if link_1_3_joined
            this.w_COVALUTA = NVL(VACODVAL103,NVL(this.w_COVALUTA,space(3)))
            this.w_DECTOT = NVL(VADECTOT103,0)
            this.w_DTOBSO = NVL(cp_ToDate(VADTOBSO103),ctod("  /  /  "))
            this.w_DESVAL = NVL(VADESVAL103,space(35))
          else
          .link_1_3('Load')
          endif
        .w_CALCPIC = DEFPIC(.w_DECTOT)
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_12.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'COM_PCES')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      this.w_TOTVAL = 0
      this.w_TOTVAC = 0
      scan
        with this
          .w_COCOMPON = NVL(COCOMPON,space(40))
          .w_COIMPTOT = NVL(COIMPTOT,0)
          .w_COIMPTOC = NVL(COIMPTOC,0)
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          .w_TOTVAL = .w_TOTVAL+.w_COIMPTOT
          .w_TOTVAC = .w_TOTVAC+.w_COIMPTOC
          replace COCOMPON with .w_COCOMPON
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .w_OBTEST = i_DATSYS
        .w_CALCPIC = DEFPIC(.w_DECTOT)
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_12.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_COCODICE=space(20)
      .w_OBTEST=ctod("  /  /  ")
      .w_COVALUTA=space(3)
      .w_DECTOT=0
      .w_CALCPIC=0
      .w_COCOMPON=space(40)
      .w_COIMPTOT=0
      .w_TOTVAL=0
      .w_DTOBSO=ctod("  /  /  ")
      .w_DESVAL=space(35)
      .w_COIMPTOC=0
      .w_TOTVAC=0
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
        .w_OBTEST = i_DATSYS
        .w_COVALUTA = g_perval
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_COVALUTA))
         .link_1_3('Full')
        endif
        .DoRTCalc(4,4,.f.)
        .w_CALCPIC = DEFPIC(.w_DECTOT)
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_12.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'COM_PCES')
    this.DoRTCalc(6,12,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oCOVALUTA_1_3.enabled = i_bVal
      .Page1.oPag.oObj_1_9.enabled = i_bVal
      .Page1.oPag.oObj_1_12.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    this.GSCE_MCU.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'COM_PCES',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSCE_MCU.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.COM_PCES_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCODICE,"COCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COVALUTA,"COVALUTA",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_COCOMPON C(40);
      ,t_COIMPTOT N(18,4);
      ,t_COIMPTOC N(18,4);
      ,COCOMPON C(40);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsce_mcobodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCOCOMPON_2_1.controlsource=this.cTrsName+'.t_COCOMPON'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCOIMPTOT_2_2.controlsource=this.cTrsName+'.t_COIMPTOT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCOIMPTOC_2_4.controlsource=this.cTrsName+'.t_COIMPTOC'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(248)
    this.AddVLine(358)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOCOMPON_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.COM_PCES_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.COM_PCES_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.COM_PCES_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.COM_PCES_IDX,2])
      *
      * insert into COM_PCES
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'COM_PCES')
        i_extval=cp_InsertValODBCExtFlds(this,'COM_PCES')
        i_cFldBody=" "+;
                  "(COCODICE,COVALUTA,COCOMPON,COIMPTOT,COIMPTOC,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_COCODICE)+","+cp_ToStrODBCNull(this.w_COVALUTA)+","+cp_ToStrODBC(this.w_COCOMPON)+","+cp_ToStrODBC(this.w_COIMPTOT)+","+cp_ToStrODBC(this.w_COIMPTOC)+;
             ","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'COM_PCES')
        i_extval=cp_InsertValVFPExtFlds(this,'COM_PCES')
        cp_CheckDeletedKey(i_cTable,0,'COCODICE',this.w_COCODICE,'COCOMPON',this.w_COCOMPON)
        INSERT INTO (i_cTable) (;
                   COCODICE;
                  ,COVALUTA;
                  ,COCOMPON;
                  ,COIMPTOT;
                  ,COIMPTOC;
                  ,CPCCCHK &i_extfld.) VALUES (;
                  this.w_COCODICE;
                  ,this.w_COVALUTA;
                  ,this.w_COCOMPON;
                  ,this.w_COIMPTOT;
                  ,this.w_COIMPTOC;
                  ,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.COM_PCES_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.COM_PCES_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (t_COCOMPON<>space(40)) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'COM_PCES')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 " COVALUTA="+cp_ToStrODBCNull(this.w_COVALUTA)+;
                 ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and COCOMPON="+cp_ToStrODBC(&i_TN.->COCOMPON)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'COM_PCES')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                  COVALUTA=this.w_COVALUTA;
                 ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and COCOMPON=&i_TN.->COCOMPON;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (t_COCOMPON<>space(40)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete row children
              this.GSCE_MCU.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
                     ,this.w_COCODICE,"CUCODICE";
                     ,this.w_COCOMPON,"CUCOMPON";
                     )
              this.GSCE_MCU.mDelete()
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and COCOMPON="+cp_ToStrODBC(&i_TN.->COCOMPON)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and COCOMPON=&i_TN.->COCOMPON;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = 0
              replace COCOMPON with this.w_COCOMPON
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update COM_PCES
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'COM_PCES')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " COVALUTA="+cp_ToStrODBCNull(this.w_COVALUTA)+;
                     ",COIMPTOT="+cp_ToStrODBC(this.w_COIMPTOT)+;
                     ",COIMPTOC="+cp_ToStrODBC(this.w_COIMPTOC)+;
                     ",COCOMPON="+cp_ToStrODBC(this.w_COCOMPON)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+;
                             " and COCOMPON="+cp_ToStrODBC(COCOMPON)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'COM_PCES')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      COVALUTA=this.w_COVALUTA;
                     ,COIMPTOT=this.w_COIMPTOT;
                     ,COIMPTOC=this.w_COIMPTOC;
                     ,COCOMPON=this.w_COCOMPON;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                                      and COCOMPON=&i_TN.->COCOMPON;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
      * --- Ask to row children to save themselves
      select (this.cTrsName)
      i_TN = this.cTrsName
      scan for (t_COCOMPON<>space(40))
        * --- > Optimize children saving
        i_nRec = recno()
        this.WorkFromTrs()
        if not(deleted())
          this.GSCE_MCU.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
               ,this.w_COCODICE,"CUCODICE";
               ,this.w_COCOMPON,"CUCOMPON";
               )
          this.GSCE_MCU.mReplace()
          this.GSCE_MCU.bSaveContext=.f.
        endif
      endscan
     this.GSCE_MCU.bSaveContext=.t.
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.COM_PCES_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.COM_PCES_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (t_COCOMPON<>space(40)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_nRec = recno()
        * --- GSCE_MCU : Deleting
        this.GSCE_MCU.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
               ,this.w_COCODICE,"CUCODICE";
               ,this.w_COCOMPON,"CUCOMPON";
               )
        this.GSCE_MCU.mDelete()
        if bTrsErr
          i_nModRow = -1
          exit
        endif
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete COM_PCES
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and COCOMPON="+cp_ToStrODBC(&i_TN.->COCOMPON)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and COCOMPON=&i_TN.->COCOMPON;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (t_COCOMPON<>space(40)) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.COM_PCES_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.COM_PCES_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
          .w_OBTEST = i_DATSYS
        .DoRTCalc(3,4,.t.)
        if .o_COVALUTA<>.w_COVALUTA
          .w_CALCPIC = DEFPIC(.w_DECTOT)
        endif
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_12.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(6,12,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_12.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_9.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_12.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=COVALUTA
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COVALUTA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_COVALUTA)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT,VADTOBSO,VADESVAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_COVALUTA))
          select VACODVAL,VADECTOT,VADTOBSO,VADESVAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COVALUTA)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COVALUTA) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oCOVALUTA_1_3'),i_cWhere,'GSAR_AVL',"Valute",'VALUZOOM.VALUTE_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT,VADTOBSO,VADESVAL";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADECTOT,VADTOBSO,VADESVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COVALUTA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT,VADTOBSO,VADESVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_COVALUTA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_COVALUTA)
            select VACODVAL,VADECTOT,VADTOBSO,VADESVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COVALUTA = NVL(_Link_.VACODVAL,space(3))
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
      this.w_DTOBSO = NVL(cp_ToDate(_Link_.VADTOBSO),ctod("  /  /  "))
      this.w_DESVAL = NVL(_Link_.VADESVAL,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_COVALUTA = space(3)
      endif
      this.w_DECTOT = 0
      this.w_DTOBSO = ctod("  /  /  ")
      this.w_DESVAL = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKDTOBS(.w_DTOBSO,.w_OBTEST,"Valuta obsoleta alla data Attuale!", .T.)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_COVALUTA = space(3)
        this.w_DECTOT = 0
        this.w_DTOBSO = ctod("  /  /  ")
        this.w_DESVAL = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COVALUTA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VALUTE_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_3.VACODVAL as VACODVAL103"+ ",link_1_3.VADECTOT as VADECTOT103"+ ",link_1_3.VADTOBSO as VADTOBSO103"+ ",link_1_3.VADESVAL as VADESVAL103"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_3 on COM_PCES.COVALUTA=link_1_3.VACODVAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_3"
          i_cKey=i_cKey+'+" and COM_PCES.COVALUTA=link_1_3.VACODVAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oCOVALUTA_1_3.value==this.w_COVALUTA)
      this.oPgFrm.Page1.oPag.oCOVALUTA_1_3.value=this.w_COVALUTA
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTVAL_3_1.value==this.w_TOTVAL)
      this.oPgFrm.Page1.oPag.oTOTVAL_3_1.value=this.w_TOTVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oDESVAL_1_11.value==this.w_DESVAL)
      this.oPgFrm.Page1.oPag.oDESVAL_1_11.value=this.w_DESVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTVAC_3_3.value==this.w_TOTVAC)
      this.oPgFrm.Page1.oPag.oTOTVAC_3_3.value=this.w_TOTVAC
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOCOMPON_2_1.value==this.w_COCOMPON)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOCOMPON_2_1.value=this.w_COCOMPON
      replace t_COCOMPON with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOCOMPON_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOIMPTOT_2_2.value==this.w_COIMPTOT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOIMPTOT_2_2.value=this.w_COIMPTOT
      replace t_COIMPTOT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOIMPTOT_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOIMPTOC_2_4.value==this.w_COIMPTOC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOIMPTOC_2_4.value=this.w_COIMPTOC
      replace t_COIMPTOC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOIMPTOC_2_4.value
    endif
    cp_SetControlsValueExtFlds(this,'COM_PCES')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_COVALUTA) or not(CHKDTOBS(.w_DTOBSO,.w_OBTEST,"Valuta obsoleta alla data Attuale!", .T.)))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCOVALUTA_1_3.SetFocus()
            i_bnoObbl = !empty(.w_COVALUTA)
            i_bnoChk = .f.
            i_bRes = .f.
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
      endcase
      i_bRes = i_bRes .and. .GSCE_MCU.CheckForm()
      if .w_COCOMPON<>space(40)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_COVALUTA = this.w_COVALUTA
    * --- GSCE_MCU : Depends On
    this.GSCE_MCU.SaveDependsOn()
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(t_COCOMPON<>space(40))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_COCOMPON=space(40)
      .w_COIMPTOT=0
      .w_COIMPTOC=0
    endwith
    this.DoRTCalc(1,12,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_COCOMPON = t_COCOMPON
    this.w_COIMPTOT = t_COIMPTOT
    this.w_COIMPTOC = t_COIMPTOC
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_COCOMPON with this.w_COCOMPON
    replace t_COIMPTOT with this.w_COIMPTOT
    replace t_COIMPTOC with this.w_COIMPTOC
    if i_srv='A'
      replace COCOMPON with this.w_COCOMPON
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
     with this
        .w_TOTVAL = .w_TOTVAL-.w_coimptot
        .w_TOTVAC = .w_TOTVAC-.w_coimptoc
        .SetControlsValue()
      endwith
  EndProc
enddefine

* --- Define pages as container
define class tgsce_mcoPag1 as StdContainer
  Width  = 945
  height = 289
  stdWidth  = 945
  stdheight = 289
  resizeXpos=217
  resizeYpos=200
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCOVALUTA_1_3 as StdField with uid="DNJMCRPHYI",rtseq=3,rtrep=.f.,;
    cFormVar = "w_COVALUTA", cQueryName = "COVALUTA",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice valuta dei componenti",;
    HelpContextID = 197366169,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=195, Top=5, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_COVALUTA"

  func oCOVALUTA_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOVALUTA_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOVALUTA_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oCOVALUTA_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"Valute",'VALUZOOM.VALUTE_VZM',this.parent.oContained
  endproc
  proc oCOVALUTA_1_3.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_COVALUTA
    i_obj.ecpSave()
  endproc


  add object oObj_1_9 as cp_runprogram with uid="EEQUZWSWMC",left=245, top=304, width=224,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSCE_BCO('A')",;
    cEvent = "w_COVALUTA Changed",;
    nPag=1;
    , HelpContextID = 187579930

  add object oDESVAL_1_11 as StdField with uid="XWJURXNCMZ",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DESVAL", cQueryName = "DESVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 90098634,;
   bGlobalFont=.t.,;
    Height=21, Width=209, Left=245, Top=5, InputMask=replicate('X',35)


  add object oObj_1_12 as cp_runprogram with uid="QEDKVYAWYI",left=244, top=323, width=130,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSCE_BCO('B')",;
    cEvent = "Delete row start",;
    nPag=1;
    , HelpContextID = 187579930


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=5, top=29, width=477,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=3,Field1="COCOMPON",Label1="Componente",Field2="COIMPTOT",Label2="Valore fiscale",Field3="COIMPTOC",Label3="Valore civile",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 49486726

  add object oStr_1_4 as StdString with uid="CQQSCARULF",Visible=.t., Left=9, Top=9,;
    Alignment=0, Width=114, Height=15,;
    Caption="Componenti"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_5 as StdString with uid="ZFWPYIQCCB",Visible=.t., Left=484, Top=8,;
    Alignment=0, Width=101, Height=15,;
    Caption="Dettagli"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_6 as StdString with uid="FTVSXHLOAU",Visible=.t., Left=131, Top=5,;
    Alignment=1, Width=64, Height=15,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.
  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gsce_mcu",lower(this.oContained.GSCE_MCU.class))=0
        this.oContained.GSCE_MCU.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-5,top=48,;
    width=477+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*11*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-4,top=49,width=476+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*11*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oContained.ChildrenChangeRow()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oLinkPC_2_3 as stdDynamicChildContainer with uid="MIQKTITUTX",bOnScreen=.t.,width=447,height=224,;
   left=490, top=29;


  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1

  add object oTOTVAL_3_1 as StdField with uid="ZFJOUOEHVN",rtseq=8,rtrep=.f.,;
    cFormVar="w_TOTVAL",value=0,enabled=.f.,;
    ToolTipText = "Valore globale fiscale del componente",;
    HelpContextID = 90091722,;
    cQueryName = "TOTVAL",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=107, Left=250, Top=265, cSayPict=[v_PV(38+VVL)], cGetPict=[v_GV(38+VVL)]

  add object oTOTVAC_3_3 as StdField with uid="BMHYCTSJUT",rtseq=12,rtrep=.f.,;
    cFormVar="w_TOTVAC",value=0,enabled=.f.,;
    ToolTipText = "Valore totale civile del bene",;
    HelpContextID = 241086666,;
    cQueryName = "TOTVAC",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=107, Left=356, Top=265, cSayPict=[v_PV(38+VVL)], cGetPict=[v_GV(38+VVL)]

  add object oStr_3_2 as StdString with uid="KTXAURALKR",Visible=.t., Left=65, Top=267,;
    Alignment=1, Width=178, Height=15,;
    Caption="Valore totale del bene:"  ;
  , bGlobalFont=.t.
enddefine

* --- Defining Body row
define class tgsce_mcoBodyRow as CPBodyRowCnt
  Width=467
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCOCOMPON_2_1 as StdTrsField with uid="DKMXZPHMHU",rtseq=6,rtrep=.t.,;
    cFormVar="w_COCOMPON",value=space(40),isprimarykey=.t.,;
    HelpContextID = 10928524,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=243, Left=-2, Top=0, InputMask=replicate('X',40)

  add object oCOIMPTOT_2_2 as StdTrsField with uid="HKURYTOAWV",rtseq=7,rtrep=.t.,;
    cFormVar="w_COIMPTOT",value=0,enabled=.f.,;
    ToolTipText = "Valore globale del componente",;
    HelpContextID = 209215878,;
    cTotal = "this.Parent.oContained.w_totval", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=109, Left=241, Top=0, cSayPict=[v_PV(38+VVL)], cGetPict=[v_GV(38+VVL)]

  add object oCOIMPTOC_2_4 as StdTrsField with uid="JKUKNQZJJP",rtseq=11,rtrep=.t.,;
    cFormVar="w_COIMPTOC",value=0,enabled=.f.,;
    ToolTipText = "Valore globale civile del componente",;
    HelpContextID = 209215895,;
    cTotal = "this.Parent.oContained.w_totvac", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=109, Left=353, Top=0, cSayPict=[v_PV(38+VVL)], cGetPict=[v_GV(38+VVL)]
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oCOCOMPON_2_1.When()
    return(.t.)
  proc oCOCOMPON_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCOCOMPON_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=10
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsce_mco','COM_PCES','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".COCODICE=COM_PCES.COCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
