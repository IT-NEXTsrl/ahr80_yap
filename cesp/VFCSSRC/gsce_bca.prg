* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_bca                                                        *
*              Creazione azienda                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_49]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-11-13                                                      *
* Last revis.: 2012-09-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsce_bca",oParentObject)
return(i_retval)

define class tgsce_bca as StdBatch
  * --- Local variables
  w_CODAZI = space(5)
  w_C1 = 0
  w_C2 = 0
  w_C3 = 0
  w_C4 = 0
  w_C5 = 0
  w_C6 = 0
  w_C7 = 0
  w_C8 = ctod("  /  /  ")
  w_C9 = ctod("  /  /  ")
  w_C10 = space(4)
  w_C11 = space(30)
  w_C12 = space(30)
  w_C13 = space(30)
  w_C14 = space(30)
  w_C15 = space(30)
  w_C16 = space(30)
  w_C17 = space(30)
  w_C18 = space(30)
  w_C19 = space(30)
  w_C20 = space(30)
  w_C21 = space(1)
  w_C22 = space(1)
  w_C23 = space(1)
  w_C24 = space(1)
  w_C25 = space(20)
  w_C26 = space(1)
  w_C27 = space(1)
  w_C28 = space(1)
  w_C29 = 0
  w_C30 = space(1)
  w_C31 = space(30)
  w_C32 = space(30)
  w_C33 = space(30)
  w_C34 = space(30)
  w_C35 = space(30)
  w_C36 = space(30)
  w_C37 = space(1)
  w_C38 = space(1)
  w_DATVEN = ctod("  /  /  ")
  * --- WorkFile variables
  PAR_CESP_idx=0
  CAU_CESP_idx=0
  FOR_CESP_idx=0
  MCO_CESP_idx=0
  CES_PITI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lanciato sotto transazione da GSUT_BCA (Creazione Azienda)
    this.w_CODAZI = this.oparentObject.oParentObject.w_CODAZI
    * --- Read from PAR_CESP
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_CESP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_CESP_idx,2],.t.,this.PAR_CESP_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "*"+;
        " from "+i_cTable+" PAR_CESP where ";
            +"PCCODAZI = "+cp_ToStrODBC(i_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        *;
        from (i_cTable) where;
            PCCODAZI = i_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_C1 = NVL(cp_ToDate(_read_.PCPERANT),cp_NullValue(_read_.PCPERANT))
      this.w_C2 = NVL(cp_ToDate(_read_.PCPERRID),cp_NullValue(_read_.PCPERRID))
      this.w_C3 = NVL(cp_ToDate(_read_.PCPERESE),cp_NullValue(_read_.PCPERESE))
      this.w_C4 = NVL(cp_ToDate(_read_.PCMAXANT),cp_NullValue(_read_.PCMAXANT))
      this.w_C5 = NVL(cp_ToDate(_read_.PCAMMABU),cp_NullValue(_read_.PCAMMABU))
      this.w_C6 = NVL(cp_ToDate(_read_.UTCC),cp_NullValue(_read_.UTCC))
      this.w_C7 = NVL(cp_ToDate(_read_.UTCV),cp_NullValue(_read_.UTCV))
      this.w_C8 = NVL(cp_ToDate(_read_.UTDC),cp_NullValue(_read_.UTDC))
      this.w_C9 = NVL(cp_ToDate(_read_.UTDV),cp_NullValue(_read_.UTDV))
      this.w_C10 = NVL(cp_ToDate(_read_.PCDATCON),cp_NullValue(_read_.PCDATCON))
      this.w_C11 = NVL(cp_ToDate(_read_.PCCONC01),cp_NullValue(_read_.PCCONC01))
      this.w_C12 = NVL(cp_ToDate(_read_.PCCONC02),cp_NullValue(_read_.PCCONC02))
      this.w_C13 = NVL(cp_ToDate(_read_.PCCONC03),cp_NullValue(_read_.PCCONC03))
      this.w_C14 = NVL(cp_ToDate(_read_.PCCONC04),cp_NullValue(_read_.PCCONC04))
      this.w_C15 = NVL(cp_ToDate(_read_.PCCONC05),cp_NullValue(_read_.PCCONC05))
      this.w_C16 = NVL(cp_ToDate(_read_.PCCONC06),cp_NullValue(_read_.PCCONC06))
      this.w_C17 = NVL(cp_ToDate(_read_.PCCONC07),cp_NullValue(_read_.PCCONC07))
      this.w_C18 = NVL(cp_ToDate(_read_.PCCONC08),cp_NullValue(_read_.PCCONC08))
      this.w_C19 = NVL(cp_ToDate(_read_.PCCONC09),cp_NullValue(_read_.PCCONC09))
      this.w_C20 = NVL(cp_ToDate(_read_.PCCONC10),cp_NullValue(_read_.PCCONC10))
      this.w_C21 = NVL(cp_ToDate(_read_.PCREGCES),cp_NullValue(_read_.PCREGCES))
      this.w_C22 = NVL(cp_ToDate(_read_.PCFLSTIN),cp_NullValue(_read_.PCFLSTIN))
      this.w_C23 = NVL(cp_ToDate(_read_.PCSTAREG),cp_NullValue(_read_.PCSTAREG))
      this.w_C24 = NVL(cp_ToDate(_read_.PCSTAINT),cp_NullValue(_read_.PCSTAINT))
      this.w_C25 = NVL(cp_ToDate(_read_.PCPREFIS),cp_NullValue(_read_.PCPREFIS))
      this.w_C26 = NVL(cp_ToDate(_read_.PCTIPAMM),cp_NullValue(_read_.PCTIPAMM))
      this.w_C27 = NVL(cp_ToDate(_read_.PCFLSALP),cp_NullValue(_read_.PCFLSALP))
      this.w_C28 = NVL(cp_ToDate(_read_.PCFLCARR),cp_NullValue(_read_.PCFLCARR))
      this.w_C29 = NVL(cp_ToDate(_read_.PCPERTOL),cp_NullValue(_read_.PCPERTOL))
      this.w_C30 = NVL(cp_ToDate(_read_.PCBIMMAM),cp_NullValue(_read_.PCBIMMAM))
      this.w_C31 = NVL(cp_ToDate(_read_.PCCONC11),cp_NullValue(_read_.PCCONC11))
      this.w_C32 = NVL(cp_ToDate(_read_.PCCONC12),cp_NullValue(_read_.PCCONC12))
      this.w_C33 = NVL(cp_ToDate(_read_.PCCONC13),cp_NullValue(_read_.PCCONC13))
      this.w_C34 = NVL(cp_ToDate(_read_.PCCONC14),cp_NullValue(_read_.PCCONC14))
      this.w_C35 = NVL(cp_ToDate(_read_.PCCONC15),cp_NullValue(_read_.PCCONC15))
      this.w_C36 = NVL(cp_ToDate(_read_.PCCONC16),cp_NullValue(_read_.PCCONC16))
      this.w_C37 = NVL(cp_ToDate(_read_.PCDATCOC),cp_NullValue(_read_.PCDATCOC))
      this.w_C38 = NVL(cp_ToDate(_read_.PCIASADO),cp_NullValue(_read_.PCIASADO))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if i_Rows<>0
      * --- Try
      local bErr_038806E8
      bErr_038806E8=bTrsErr
      this.Try_038806E8()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_038806E8
      * --- End
      * --- Try
      local bErr_03881A38
      bErr_03881A38=bTrsErr
      this.Try_03881A38()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        ah_ErrorMsg("Impossibile registrare i parametri cespiti: inserire manualmente")
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_03881A38
      * --- End
    endif
    this.oParentObject.VABENE = TRASFARC(i_CODAZI,this.w_codazi,"CAU_CESP")
    this.oParentObject.VABENE = IIF(this.oParentObject.VABENE,TRASFARC(i_CODAZI,this.w_codazi,"FOR_CESP"),.F.)
    this.oParentObject.VABENE = IIF(this.oParentObject.VABENE,TRASFARC(i_CODAZI,this.w_codazi,"MCO_CESP"),.F.)
    this.oParentObject.VABENE = IIF(this.oParentObject.VABENE,TRASFARC(i_CODAZI,this.w_codazi,"GRU_COCE"),.F.)
    this.oParentObject.VABENE = IIF(this.oParentObject.VABENE,TRASFARC(i_CODAZI,this.w_codazi,"CAT_CESP"),.F.)
    this.oParentObject.VABENE = IIF(this.oParentObject.VABENE,TRASFARC(i_CODAZI,this.w_codazi,"UBI_CESP"),.F.)
    this.oParentObject.VABENE = IIF(this.oParentObject.VABENE,TRASFARC(i_CODAZI,this.w_codazi,"FAM_CESP"),.F.)
    this.oParentObject.VABENE = IIF(this.oParentObject.VABENE,TRASFARC(i_CODAZI,this.w_codazi,"CES_PITI"),.F.)
    this.oParentObject.VABENE = IIF(this.oParentObject.VABENE,TRASFARC(i_CODAZI,this.w_codazi,"COM_PCES"),.F.)
    this.oParentObject.VABENE = IIF(this.oParentObject.VABENE,TRASFARC(i_CODAZI,this.w_codazi,"COM_PUBI"),.F.)
    this.oParentObject.VABENE = IIF(this.oParentObject.VABENE,TRASFARC(i_CODAZI,this.w_codazi,"CAT_AMMO"),.F.)
    this.oParentObject.VABENE = IIF(this.oParentObject.VABENE,TRASFARC(i_CODAZI,this.w_codazi,"CAT_AMMC"),.F.)
    * --- Riscrittura di alcuni campi dopo la copia di tutta la tabella:
    *     Nei cespiti la combo stato del bene deve essere 'Non Attivato' = 'N', le date di dismissione primo utilizzo e esercizio primo utilizzo
    *     devono essere vuote. Nel caso di componenti anche la data di Vendita/dismissione deve essere vuota.
    i_nConn=i_TableProp[this.CES_PITI_idx,3]
    w_iRows = SQLExec(i_nConn,"UPDATE "+ALLTRIM(this.w_CODAZI)+"CES_PITI SET CESTABEN='N'")
    w_iRows = SQLExec(i_nConn,"UPDATE "+ALLTRIM(this.w_CODAZI)+"CES_PITI SET CEDTPRIU=NULL")
    w_iRows = SQLExec(i_nConn,"UPDATE "+ALLTRIM(this.w_CODAZI)+"CES_PITI SET CEDATDIS=NULL")
    w_iRows = SQLExec(i_nConn,"UPDATE "+ALLTRIM(this.w_CODAZI)+"CES_PITI SET CEESPRIU=''")
    w_iRows = SQLExec(i_nConn,"UPDATE "+ALLTRIM(this.w_CODAZI)+"CES_PITI SET CECOECIV= 0")
    w_iRows = SQLExec(i_nConn,"UPDATE "+ALLTRIM(this.w_CODAZI)+"CES_PITI SET CEQUOCIV=0")
    w_iRows = SQLExec(i_nConn,"UPDATE "+ALLTRIM(this.w_CODAZI)+"CES_PITI SET CECOEFI1= 0")
    w_iRows = SQLExec(i_nConn,"UPDATE "+ALLTRIM(this.w_CODAZI)+"CES_PITI SET CEQUOFIS=0")
    w_iRows = SQLExec(i_nConn,"UPDATE "+ALLTRIM(this.w_CODAZI)+"CES_PITI SET CECOEFI2= 0")
    w_iRows = SQLExec(i_nConn,"UPDATE "+ALLTRIM(this.w_CODAZI)+"CES_PITI SET CEQUOFI1=0")
    w_iRows = SQLExec(i_nConn,"UPDATE "+ALLTRIM(this.w_CODAZI)+"CES_PITI SET CEPERDEF= 0")
    w_iRows = SQLExec(i_nConn,"UPDATE "+ALLTRIM(this.w_CODAZI)+"CES_PITI SET CEIMPMAX=0")
    w_iRows = SQLExec(i_nConn,"UPDATE "+ALLTRIM(this.w_CODAZI)+"CES_PITI SET CEFLAUMU=''")
    w_iRows = SQLExec(i_nConn,"UPDATE "+ALLTRIM(this.w_CODAZI)+"CES_PITI SET CECODESE=''")
    w_iRows = SQLExec(i_nConn,"UPDATE "+ALLTRIM(this.w_CODAZI)+"CES_PITI SET CETIPAMM='E'")
    w_iRows = SQLExec(i_nConn,"UPDATE "+ALLTRIM(this.w_CODAZI)+"CES_PITI SET CEAMMIMM=''")
    w_iRows = SQLExec(i_nConn,"UPDATE "+ALLTRIM(this.w_CODAZI)+"CES_PITI SET CEDURCES=1")
    w_iRows = SQLExec(i_nConn,"UPDATE "+ALLTRIM(this.w_CODAZI)+"CES_PITI SET CEPERCIV=0")
    w_iRows = SQLExec(i_nConn,"UPDATE "+ALLTRIM(this.w_CODAZI)+"CES_PITI SET CEFLAMCI='I'")
    w_iRows = SQLExec(i_nConn,"UPDATE "+ALLTRIM(this.w_CODAZI)+"CES_PITI SET CECOEFIS=0")
    w_iRows = SQLExec(i_nConn,"UPDATE "+ALLTRIM(this.w_CODAZI)+"CES_PITI SET CEVALLIM=g_PERVAL")
    w_iRows = SQLExec(i_nConn,"UPDATE "+ALLTRIM(this.w_CODAZI)+"COM_PUBI SET CUDATVEN= NULL")
    w_iRows = SQLExec(i_nConn,"UPDATE "+ALLTRIM(this.w_CODAZI)+"COM_PCES SET COIMPTOT= 0")
  endproc
  proc Try_038806E8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PAR_CESP
    i_nConn=i_TableProp[this.PAR_CESP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_CESP_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_CESP_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PCCODAZI"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CODAZI),'PAR_CESP','PCCODAZI');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PCCODAZI',this.w_CODAZI)
      insert into (i_cTable) (PCCODAZI &i_ccchkf. );
         values (;
           this.w_CODAZI;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_03881A38()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into PAR_CESP
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PAR_CESP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_CESP_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_CESP_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PCPERANT ="+cp_NullLink(cp_ToStrODBC(this.w_C1),'PAR_CESP','PCPERANT');
      +",PCPERRID ="+cp_NullLink(cp_ToStrODBC(this.w_C2),'PAR_CESP','PCPERRID');
      +",PCPERESE ="+cp_NullLink(cp_ToStrODBC(this.w_C3),'PAR_CESP','PCPERESE');
      +",PCMAXANT ="+cp_NullLink(cp_ToStrODBC(this.w_C4),'PAR_CESP','PCMAXANT');
      +",PCAMMABU ="+cp_NullLink(cp_ToStrODBC(this.w_C5),'PAR_CESP','PCAMMABU');
      +",UTCC ="+cp_NullLink(cp_ToStrODBC(this.w_C6),'PAR_CESP','UTCC');
      +",UTCV ="+cp_NullLink(cp_ToStrODBC(this.w_C7),'PAR_CESP','UTCV');
      +",UTDC ="+cp_NullLink(cp_ToStrODBC(this.w_C8),'PAR_CESP','UTDC');
      +",UTDV ="+cp_NullLink(cp_ToStrODBC(this.w_C9),'PAR_CESP','UTDV');
      +",PCCONC01 ="+cp_NullLink(cp_ToStrODBC(this.w_C11),'PAR_CESP','PCCONC01');
      +",PCCONC02 ="+cp_NullLink(cp_ToStrODBC(this.w_C12),'PAR_CESP','PCCONC02');
      +",PCCONC03 ="+cp_NullLink(cp_ToStrODBC(this.w_C13),'PAR_CESP','PCCONC03');
      +",PCCONC04 ="+cp_NullLink(cp_ToStrODBC(this.w_C14),'PAR_CESP','PCCONC04');
      +",PCCONC05 ="+cp_NullLink(cp_ToStrODBC(this.w_C15),'PAR_CESP','PCCONC05');
      +",PCCONC06 ="+cp_NullLink(cp_ToStrODBC(this.w_C16),'PAR_CESP','PCCONC06');
      +",PCCONC07 ="+cp_NullLink(cp_ToStrODBC(this.w_C17),'PAR_CESP','PCCONC07');
      +",PCCONC08 ="+cp_NullLink(cp_ToStrODBC(this.w_C18),'PAR_CESP','PCCONC08');
      +",PCCONC09 ="+cp_NullLink(cp_ToStrODBC(this.w_C19),'PAR_CESP','PCCONC09');
      +",PCCONC10 ="+cp_NullLink(cp_ToStrODBC(this.w_C20),'PAR_CESP','PCCONC10');
      +",PCREGCES ="+cp_NullLink(cp_ToStrODBC(this.w_C21),'PAR_CESP','PCREGCES');
      +",PCFLSTIN ="+cp_NullLink(cp_ToStrODBC(this.w_C22),'PAR_CESP','PCFLSTIN');
      +",PCSTAREG ="+cp_NullLink(cp_ToStrODBC(this.w_C23),'PAR_CESP','PCSTAREG');
      +",PCSTAINT ="+cp_NullLink(cp_ToStrODBC(this.w_C24),'PAR_CESP','PCSTAINT');
      +",PCPREFIS ="+cp_NullLink(cp_ToStrODBC(this.w_C25),'PAR_CESP','PCPREFIS');
      +",PCTIPAMM ="+cp_NullLink(cp_ToStrODBC(this.w_C26),'PAR_CESP','PCTIPAMM');
      +",PCFLSALP ="+cp_NullLink(cp_ToStrODBC(this.w_C27),'PAR_CESP','PCFLSALP');
      +",PCFLCARR ="+cp_NullLink(cp_ToStrODBC(this.w_C28),'PAR_CESP','PCFLCARR');
      +",PCPERTOL ="+cp_NullLink(cp_ToStrODBC(this.w_C29),'PAR_CESP','PCPERTOL');
      +",PCBIMMAM ="+cp_NullLink(cp_ToStrODBC(this.w_C30),'PAR_CESP','PCBIMMAM');
      +",PCCONC11 ="+cp_NullLink(cp_ToStrODBC(this.w_C31),'PAR_CESP','PCCONC11');
      +",PCCONC12 ="+cp_NullLink(cp_ToStrODBC(this.w_C32),'PAR_CESP','PCCONC12');
      +",PCCONC13 ="+cp_NullLink(cp_ToStrODBC(this.w_C33),'PAR_CESP','PCCONC13');
      +",PCCONC14 ="+cp_NullLink(cp_ToStrODBC(this.w_C34),'PAR_CESP','PCCONC14');
      +",PCCONC15 ="+cp_NullLink(cp_ToStrODBC(this.w_C35),'PAR_CESP','PCCONC15');
      +",PCCONC16 ="+cp_NullLink(cp_ToStrODBC(this.w_C36),'PAR_CESP','PCCONC16');
      +",PCDATCOC ="+cp_NullLink(cp_ToStrODBC(this.w_C37),'PAR_CESP','PCDATCOC');
      +",PCIASADO ="+cp_NullLink(cp_ToStrODBC(this.w_C38),'PAR_CESP','PCIASADO');
          +i_ccchkf ;
      +" where ";
          +"PCCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
             )
    else
      update (i_cTable) set;
          PCPERANT = this.w_C1;
          ,PCPERRID = this.w_C2;
          ,PCPERESE = this.w_C3;
          ,PCMAXANT = this.w_C4;
          ,PCAMMABU = this.w_C5;
          ,UTCC = this.w_C6;
          ,UTCV = this.w_C7;
          ,UTDC = this.w_C8;
          ,UTDV = this.w_C9;
          ,PCCONC01 = this.w_C11;
          ,PCCONC02 = this.w_C12;
          ,PCCONC03 = this.w_C13;
          ,PCCONC04 = this.w_C14;
          ,PCCONC05 = this.w_C15;
          ,PCCONC06 = this.w_C16;
          ,PCCONC07 = this.w_C17;
          ,PCCONC08 = this.w_C18;
          ,PCCONC09 = this.w_C19;
          ,PCCONC10 = this.w_C20;
          ,PCREGCES = this.w_C21;
          ,PCFLSTIN = this.w_C22;
          ,PCSTAREG = this.w_C23;
          ,PCSTAINT = this.w_C24;
          ,PCPREFIS = this.w_C25;
          ,PCTIPAMM = this.w_C26;
          ,PCFLSALP = this.w_C27;
          ,PCFLCARR = this.w_C28;
          ,PCPERTOL = this.w_C29;
          ,PCBIMMAM = this.w_C30;
          ,PCCONC11 = this.w_C31;
          ,PCCONC12 = this.w_C32;
          ,PCCONC13 = this.w_C33;
          ,PCCONC14 = this.w_C34;
          ,PCCONC15 = this.w_C35;
          ,PCCONC16 = this.w_C36;
          ,PCDATCOC = this.w_C37;
          ,PCIASADO = this.w_C38;
          &i_ccchkf. ;
       where;
          PCCODAZI = this.w_CODAZI;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='PAR_CESP'
    this.cWorkTables[2]='CAU_CESP'
    this.cWorkTables[3]='FOR_CESP'
    this.cWorkTables[4]='MCO_CESP'
    this.cWorkTables[5]='CES_PITI'
    return(this.OpenAllTables(5))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
