* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_mcc                                                        *
*              Cespiti/ammortamento civili                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-02-26                                                      *
* Last revis.: 2014-10-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsce_mcc")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsce_mcc")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsce_mcc")
  return

* --- Class definition
define class tgsce_mcc as StdPCForm
  Width  = 448
  Height = 116
  Top    = 78
  Left   = 152
  cComment = "Cespiti/ammortamento civili"
  cPrg = "gsce_mcc"
  HelpContextID=97142121
  add object cnt as tcgsce_mcc
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsce_mcc as PCContext
  w_ACCODCES = space(20)
  w_ACPROESE = 0
  w_ACTIPAMM = space(1)
  w_ACPERORD = 0
  w_TOTPEORD = 0
  w_ACQUOFIS = 0
  w_ACPERORD_T = 0
  w_CODESE = space(4)
  w_CODAZI = space(10)
  w_ESPRIU = space(4)
  proc Save(i_oFrom)
    this.w_ACCODCES = i_oFrom.w_ACCODCES
    this.w_ACPROESE = i_oFrom.w_ACPROESE
    this.w_ACTIPAMM = i_oFrom.w_ACTIPAMM
    this.w_ACPERORD = i_oFrom.w_ACPERORD
    this.w_TOTPEORD = i_oFrom.w_TOTPEORD
    this.w_ACQUOFIS = i_oFrom.w_ACQUOFIS
    this.w_ACPERORD_T = i_oFrom.w_ACPERORD_T
    this.w_CODESE = i_oFrom.w_CODESE
    this.w_CODAZI = i_oFrom.w_CODAZI
    this.w_ESPRIU = i_oFrom.w_ESPRIU
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_ACCODCES = this.w_ACCODCES
    i_oTo.w_ACPROESE = this.w_ACPROESE
    i_oTo.w_ACTIPAMM = this.w_ACTIPAMM
    i_oTo.w_ACPERORD = this.w_ACPERORD
    i_oTo.w_TOTPEORD = this.w_TOTPEORD
    i_oTo.w_ACQUOFIS = this.w_ACQUOFIS
    i_oTo.w_ACPERORD_T = this.w_ACPERORD_T
    i_oTo.w_CODESE = this.w_CODESE
    i_oTo.w_CODAZI = this.w_CODAZI
    i_oTo.w_ESPRIU = this.w_ESPRIU
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsce_mcc as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 448
  Height = 116
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-10-28"
  HelpContextID=97142121
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=10

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  AMC_CESP_IDX = 0
  ESERCIZI_IDX = 0
  cFile = "AMC_CESP"
  cKeySelect = "ACCODCES"
  cKeyWhere  = "ACCODCES=this.w_ACCODCES"
  cKeyDetail  = "ACCODCES=this.w_ACCODCES and ACPROESE=this.w_ACPROESE"
  cKeyWhereODBC = '"ACCODCES="+cp_ToStrODBC(this.w_ACCODCES)';

  cKeyDetailWhereODBC = '"ACCODCES="+cp_ToStrODBC(this.w_ACCODCES)';
      +'+" and ACPROESE="+cp_ToStrODBC(this.w_ACPROESE)';

  cKeyWhereODBCqualified = '"AMC_CESP.ACCODCES="+cp_ToStrODBC(this.w_ACCODCES)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cPrg = "gsce_mcc"
  cComment = "Cespiti/ammortamento civili"
  i_nRowNum = 0
  i_nRowPerPage = 3
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_ACCODCES = space(20)
  w_ACPROESE = 0
  o_ACPROESE = 0
  w_ACTIPAMM = space(1)
  o_ACTIPAMM = space(1)
  w_ACPERORD = 0
  w_TOTPEORD = 0
  w_ACQUOFIS = 0
  w_ACPERORD_T = 0
  w_CODESE = space(4)
  w_CODAZI = space(10)
  w_ESPRIU = space(4)
  o_ESPRIU = space(4)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsce_mccPag1","gsce_mcc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='ESERCIZI'
    this.cWorkTables[2]='AMC_CESP'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.AMC_CESP_IDX,5],7]
    this.nPostItConn=i_TableProp[this.AMC_CESP_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsce_mcc'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from AMC_CESP where ACCODCES=KeySet.ACCODCES
    *                            and ACPROESE=KeySet.ACPROESE
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- gsce_mcc
      * --- Setta Ordine per Progressivo Periodo
      i_cOrder = 'order by ACPROESE '
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.AMC_CESP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.AMC_CESP_IDX,2],this.bLoadRecFilter,this.AMC_CESP_IDX,"gsce_mcc")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('AMC_CESP')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "AMC_CESP.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' AMC_CESP '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'ACCODCES',this.w_ACCODCES  )
      select * from (i_cTable) AMC_CESP where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_TOTPEORD = 0
        .w_CODAZI = i_CODAZI
        .w_ACCODCES = NVL(ACCODCES,space(20))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_ESPRIU = this.oParentObject.w_CEESPRIC
        cp_LoadRecExtFlds(this,'AMC_CESP')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      this.w_TOTPEORD = 0
      scan
        with this
          .w_ACPROESE = NVL(ACPROESE,0)
          .w_ACTIPAMM = NVL(ACTIPAMM,space(1))
          .w_ACPERORD = NVL(ACPERORD,0)
          .w_ACQUOFIS = NVL(ACQUOFIS,0)
        .w_ACPERORD_T = .w_ACPERORD
        .w_CODESE = CALCESCES(.w_ESPRIU,.w_ACPROESE)
          .link_3_3('Load')
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          .w_TOTPEORD = .w_TOTPEORD+.w_ACPERORD_T
          replace ACPROESE with .w_ACPROESE
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_CODESE = CALCESCES(.w_ESPRIU,.w_ACPROESE)
        .w_ESPRIU = this.oParentObject.w_CEESPRIC
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_ACCODCES=space(20)
      .w_ACPROESE=0
      .w_ACTIPAMM=space(1)
      .w_ACPERORD=0
      .w_TOTPEORD=0
      .w_ACQUOFIS=0
      .w_ACPERORD_T=0
      .w_CODESE=space(4)
      .w_CODAZI=space(10)
      .w_ESPRIU=space(4)
      if .cFunction<>"Filter"
        .DoRTCalc(1,2,.f.)
        .w_ACTIPAMM = 'I'
        .w_ACPERORD = 0
        .DoRTCalc(5,5,.f.)
        .w_ACQUOFIS = 0
        .w_ACPERORD_T = .w_ACPERORD
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_CODESE = CALCESCES(.w_ESPRIU,.w_ACPROESE)
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_CODESE))
         .link_3_3('Full')
        endif
        .w_CODAZI = i_CODAZI
        .w_ESPRIU = this.oParentObject.w_CEESPRIC
      endif
    endwith
    cp_BlankRecExtFlds(this,'AMC_CESP')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'AMC_CESP',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.AMC_CESP_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ACCODCES,"ACCODCES",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_ACPROESE N(3);
      ,t_ACTIPAMM N(3);
      ,t_ACPERORD N(6,2);
      ,t_ACQUOFIS N(18,4);
      ,t_CODESE C(4);
      ,ACPROESE N(3);
      ,t_ACPERORD_T N(6,2);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsce_mccbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oACPROESE_2_1.controlsource=this.cTrsName+'.t_ACPROESE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oACTIPAMM_2_2.controlsource=this.cTrsName+'.t_ACTIPAMM'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oACPERORD_2_3.controlsource=this.cTrsName+'.t_ACPERORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oACQUOFIS_2_4.controlsource=this.cTrsName+'.t_ACQUOFIS'
    this.oPgFRm.Page1.oPag.oCODESE_3_3.controlsource=this.cTrsName+'.t_CODESE'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(81)
    this.AddVLine(191)
    this.AddVLine(267)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oACPROESE_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.AMC_CESP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.AMC_CESP_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.AMC_CESP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.AMC_CESP_IDX,2])
      *
      * insert into AMC_CESP
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'AMC_CESP')
        i_extval=cp_InsertValODBCExtFlds(this,'AMC_CESP')
        i_cFldBody=" "+;
                  "(ACCODCES,ACPROESE,ACTIPAMM,ACPERORD,ACQUOFIS,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_ACCODCES)+","+cp_ToStrODBC(this.w_ACPROESE)+","+cp_ToStrODBC(this.w_ACTIPAMM)+","+cp_ToStrODBC(this.w_ACPERORD)+","+cp_ToStrODBC(this.w_ACQUOFIS)+;
             ","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'AMC_CESP')
        i_extval=cp_InsertValVFPExtFlds(this,'AMC_CESP')
        cp_CheckDeletedKey(i_cTable,0,'ACCODCES',this.w_ACCODCES,'ACPROESE',this.w_ACPROESE)
        INSERT INTO (i_cTable) (;
                   ACCODCES;
                  ,ACPROESE;
                  ,ACTIPAMM;
                  ,ACPERORD;
                  ,ACQUOFIS;
                  ,CPCCCHK &i_extfld.) VALUES (;
                  this.w_ACCODCES;
                  ,this.w_ACPROESE;
                  ,this.w_ACTIPAMM;
                  ,this.w_ACPERORD;
                  ,this.w_ACQUOFIS;
                  ,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.AMC_CESP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.AMC_CESP_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (t_ACPROESE<>0  OR (t_ACPERORD<>0 OR t_ACQUOFIS<>0 )) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'AMC_CESP')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and ACPROESE="+cp_ToStrODBC(&i_TN.->ACPROESE)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'AMC_CESP')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and ACPROESE=&i_TN.->ACPROESE;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (t_ACPROESE<>0  OR (t_ACPERORD<>0 OR t_ACQUOFIS<>0 )) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and ACPROESE="+cp_ToStrODBC(&i_TN.->ACPROESE)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and ACPROESE=&i_TN.->ACPROESE;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = 0
              replace ACPROESE with this.w_ACPROESE
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update AMC_CESP
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'AMC_CESP')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " ACTIPAMM="+cp_ToStrODBC(this.w_ACTIPAMM)+;
                     ",ACPERORD="+cp_ToStrODBC(this.w_ACPERORD)+;
                     ",ACQUOFIS="+cp_ToStrODBC(this.w_ACQUOFIS)+;
                     ",ACPROESE="+cp_ToStrODBC(this.w_ACPROESE)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+;
                             " and ACPROESE="+cp_ToStrODBC(ACPROESE)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'AMC_CESP')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      ACTIPAMM=this.w_ACTIPAMM;
                     ,ACPERORD=this.w_ACPERORD;
                     ,ACQUOFIS=this.w_ACQUOFIS;
                     ,ACPROESE=this.w_ACPROESE;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                                      and ACPROESE=&i_TN.->ACPROESE;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.AMC_CESP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.AMC_CESP_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (t_ACPROESE<>0  OR (t_ACPERORD<>0 OR t_ACQUOFIS<>0 )) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete AMC_CESP
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and ACPROESE="+cp_ToStrODBC(&i_TN.->ACPROESE)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and ACPROESE=&i_TN.->ACPROESE;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (t_ACPROESE<>0  OR (t_ACPERORD<>0 OR t_ACQUOFIS<>0 )) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.AMC_CESP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.AMC_CESP_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,6,.t.)
          .w_TOTPEORD = .w_TOTPEORD-.w_acperord_t
          .w_ACPERORD_T = .w_ACPERORD
          .w_TOTPEORD = .w_TOTPEORD+.w_acperord_t
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        if .o_ESPRIU<>.w_ESPRIU.or. .o_ACPROESE<>.w_ACPROESE
          .w_CODESE = CALCESCES(.w_ESPRIU,.w_ACPROESE)
          .link_3_3('Full')
        endif
        .DoRTCalc(9,9,.t.)
          .w_ESPRIU = this.oParentObject.w_CEESPRIC
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_ACPERORD_T with this.w_ACPERORD_T
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_2.visible=!this.oPgFrm.Page1.oPag.oStr_1_2.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODESE
  func Link_3_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_CODESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_CODESE)
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODESE = NVL(_Link_.ESCODESE,space(4))
    else
      if i_cCtrl<>'Load'
        this.w_CODESE = space(4)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oTOTPEORD_3_1.value==this.w_TOTPEORD)
      this.oPgFrm.Page1.oPag.oTOTPEORD_3_1.value=this.w_TOTPEORD
    endif
    if not(this.oPgFrm.Page1.oPag.oCODESE_3_3.value==this.w_CODESE)
      this.oPgFrm.Page1.oPag.oCODESE_3_3.value=this.w_CODESE
      replace t_CODESE with this.oPgFrm.Page1.oPag.oCODESE_3_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oACPROESE_2_1.value==this.w_ACPROESE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oACPROESE_2_1.value=this.w_ACPROESE
      replace t_ACPROESE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oACPROESE_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oACTIPAMM_2_2.RadioValue()==this.w_ACTIPAMM)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oACTIPAMM_2_2.SetRadio()
      replace t_ACTIPAMM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oACTIPAMM_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oACPERORD_2_3.value==this.w_ACPERORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oACPERORD_2_3.value=this.w_ACPERORD
      replace t_ACPERORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oACPERORD_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oACQUOFIS_2_4.value==this.w_ACQUOFIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oACQUOFIS_2_4.value=this.w_ACQUOFIS
      replace t_ACQUOFIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oACQUOFIS_2_4.value
    endif
    cp_SetControlsValueExtFlds(this,'AMC_CESP')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsce_mcc
      if this.w_TOTPEORD>100
         i_bnoChk=.f.
         i_bRes=.f.
         i_cErrorMsg=ah_MsgFormat("Il totale delle percentuali non pu� essere superiore a 100")
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(.w_ACTIPAMM='I' OR (.w_ACTIPAMM $ '5RD' AND .w_ACPROESE=1) ) and (.w_ACPROESE<>0  OR (.w_ACPERORD<>0 OR .w_ACQUOFIS<>0 ))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oACTIPAMM_2_2
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Tipo ammortamento non corretto")
      endcase
      if .w_ACPROESE<>0  OR (.w_ACPERORD<>0 OR .w_ACQUOFIS<>0 )
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_ACPROESE = this.w_ACPROESE
    this.o_ACTIPAMM = this.w_ACTIPAMM
    this.o_ESPRIU = this.w_ESPRIU
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(t_ACPROESE<>0  OR (t_ACPERORD<>0 OR t_ACQUOFIS<>0 ))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_ACPROESE=0
      .w_ACTIPAMM=space(1)
      .w_ACPERORD=0
      .w_ACQUOFIS=0
      .w_ACPERORD_T=0
      .w_CODESE=space(4)
      .DoRTCalc(1,2,.f.)
        .w_ACTIPAMM = 'I'
        .w_ACPERORD = 0
      .DoRTCalc(5,5,.f.)
        .w_ACQUOFIS = 0
        .w_ACPERORD_T = .w_ACPERORD
        .w_CODESE = CALCESCES(.w_ESPRIU,.w_ACPROESE)
      .DoRTCalc(8,8,.f.)
      if not(empty(.w_CODESE))
        .link_3_3('Full')
      endif
    endwith
    this.DoRTCalc(9,10,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_ACPROESE = t_ACPROESE
    this.w_ACTIPAMM = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oACTIPAMM_2_2.RadioValue(.t.)
    this.w_ACPERORD = t_ACPERORD
    this.w_ACQUOFIS = t_ACQUOFIS
    this.w_ACPERORD_T = t_ACPERORD_T
    this.w_CODESE = t_CODESE
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_ACPROESE with this.w_ACPROESE
    replace t_ACTIPAMM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oACTIPAMM_2_2.ToRadio()
    replace t_ACPERORD with this.w_ACPERORD
    replace t_ACQUOFIS with this.w_ACQUOFIS
    replace t_ACPERORD_T with this.w_ACPERORD_T
    replace t_CODESE with this.w_CODESE
    if i_srv='A'
      replace ACPROESE with this.w_ACPROESE
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
     with this
        .w_TOTPEORD = .w_TOTPEORD-.w_acperord_t
        .SetControlsValue()
      endwith
  EndProc
enddefine

* --- Define pages as container
define class tgsce_mccPag1 as StdContainer
  Width  = 444
  height = 116
  stdWidth  = 444
  stdheight = 116
  resizeYpos=59
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=24, top=7, width=410,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=4,Field1="ACPROESE",Label1="Prog.es.",Field2="ACTIPAMM",Label2="Tipo amm.to",Field3="ACPERORD",Label3="% Ordinario",Field4="ACQUOFIS",Label4="Quota fissa",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 218948730

  add object oStr_1_2 as StdString with uid="NHTVZRONQK",Visible=.t., Left=1, Top=119,;
    Alignment=0, Width=605, Height=19,;
    Caption="Se modificata sequenza campo actipamm controllare la condizione di riga piena"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_2.mHide()
    with this.Parent.oContained
      return (.T.)
    endwith
  endfunc

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=14,top=27,;
    width=406+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*3*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=15,top=28,width=405+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*3*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oCODESE_3_3.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1

  add object oTOTPEORD_3_1 as StdField with uid="YCNKJKGRSN",rtseq=5,rtrep=.f.,;
    cFormVar="w_TOTPEORD",value=0,enabled=.f.,;
    HelpContextID = 232476538,;
    cQueryName = "TOTPEORD",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=195, Top=89, cSayPict=["999.99"], cGetPict=["999.99"]

  add object oCODESE_3_3 as StdTrsField with uid="NKFAQSWAZP",rtseq=8,rtrep=.t.,;
    cFormVar="w_CODESE",value=space(4),enabled=.f.,;
    ToolTipText = "Codice esercizio corrispondente al'es.progressivo dell'ammortamento del cespite",;
    HelpContextID = 189837786,;
    cTotal="", bFixedPos=.t., cQueryName = "CODESE",;
    bObbl = .f. , nPag = 3, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=49, Left=24, Top=89, InputMask=replicate('X',4), cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_CODESE"

  func oCODESE_3_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oStr_3_2 as StdString with uid="RMAUMWQCRS",Visible=.t., Left=147, Top=93,;
    Alignment=1, Width=44, Height=15,;
    Caption="Totale:"  ;
  , bGlobalFont=.t.
enddefine

* --- Defining Body row
define class tgsce_mccBodyRow as CPBodyRowCnt
  Width=396
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oACPROESE_2_1 as StdTrsField with uid="CVMYHRXMWM",rtseq=2,rtrep=.t.,;
    cFormVar="w_ACPROESE",value=0,isprimarykey=.t.,;
    ToolTipText = "Progressivo esercizio",;
    HelpContextID = 75301451,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=48, Left=-2, Top=0, cSayPict=["999"], cGetPict=["999"]

  add object oACTIPAMM_2_2 as StdTrsCombo with uid="QOKPVLLJNZ",rtrep=.t.,;
    cFormVar="w_ACTIPAMM", RowSource=""+"Rateo utilizzo,"+"Ridotto 50%,"+"Intero,"+"Diverso" , ;
    ToolTipText = "Tipo ammortamento",;
    HelpContextID = 259767725,;
    Height=21, Width=97, Left=64, Top=-1,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2  , sErrorMsg = "Tipo ammortamento non corretto";
, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oACTIPAMM_2_2.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..ACTIPAMM,&i_cF..t_ACTIPAMM),this.value)
    return(iif(xVal =1,'R',;
    iif(xVal =2,'5',;
    iif(xVal =3,'I',;
    iif(xVal =4,'D',;
    space(1))))))
  endfunc
  func oACTIPAMM_2_2.GetRadio()
    this.Parent.oContained.w_ACTIPAMM = this.RadioValue()
    return .t.
  endfunc

  func oACTIPAMM_2_2.ToRadio()
    this.Parent.oContained.w_ACTIPAMM=trim(this.Parent.oContained.w_ACTIPAMM)
    return(;
      iif(this.Parent.oContained.w_ACTIPAMM=='R',1,;
      iif(this.Parent.oContained.w_ACTIPAMM=='5',2,;
      iif(this.Parent.oContained.w_ACTIPAMM=='I',3,;
      iif(this.Parent.oContained.w_ACTIPAMM=='D',4,;
      0)))))
  endfunc

  func oACTIPAMM_2_2.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oACTIPAMM_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_ACTIPAMM='I' OR (.w_ACTIPAMM $ '5RD' AND .w_ACPROESE=1) )
    endwith
    return bRes
  endfunc

  add object oACPERORD_2_3 as StdTrsField with uid="ZBXCFJLKWC",rtseq=4,rtrep=.t.,;
    cFormVar="w_ACPERORD",value=0,;
    ToolTipText = "Percentuale ammortamento ordinario",;
    HelpContextID = 245367370,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=69, Left=169, Top=0, cSayPict=["999.99"], cGetPict=["999.99"]

  add object oACQUOFIS_2_4 as StdTrsField with uid="ZFZJKNGQUN",rtseq=6,rtrep=.t.,;
    cFormVar="w_ACQUOFIS",value=0,;
    ToolTipText = "Quota per ammortamenti civili (importo espresso nella valuta di conto)",;
    HelpContextID = 92279385,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=146, Left=245, Top=-1, cSayPict=[v_PV(20)], cGetPict=[v_GV(20)]
  add object oLast as LastKeyMover
  * ---
  func oACPROESE_2_1.When()
    return(.t.)
  proc oACPROESE_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oACPROESE_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=2
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsce_mcc','AMC_CESP','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".ACCODCES=AMC_CESP.ACCODCES";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
