* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_bcm                                                        *
*              Contabilizzazione movimenti                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_174]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-03-26                                                      *
* Last revis.: 2012-09-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsce_bcm",oParentObject)
return(i_retval)

define class tgsce_bcm as StdBatch
  * --- Local variables
  w_LOOP = 0
  w_oERRORLOG = .NULL.
  w_NUDOC = 0
  w_MCVALNAZ = space(3)
  w_MCIMPA01 = 0
  w_MCNUMREG = 0
  w_OKDOC = 0
  w_MCCODVAL = space(3)
  w_MCIMPA02 = 0
  w_MCCODESE = space(4)
  w_MAXDAT = ctod("  /  /  ")
  w_MCCAOVAL = 0
  w_MCIMPA03 = 0
  IMP = 0
  w_CODAZI = space(5)
  w_MCDESMOV = space(40)
  w_MCIMPA04 = 0
  GPC = space(15)
  w_APPO = space(10)
  w_MCCODCES = space(20)
  w_MCIMPA05 = 0
  w_ACOD = space(3)
  w_OSERIAL = space(10)
  w_CECODCAT = space(15)
  w_MCIMPA06 = 0
  w_CDAR = space(3)
  w_NSERIAL = space(10)
  w_CCCAUCON = space(5)
  w_MCIMPA07 = 0
  w_CAVE = space(3)
  w_MCSERIAL = space(10)
  w_CCFLRIFE = space(1)
  w_MCIMPA08 = 0
  w_APPO1 = 0
  w_MCDATREG = ctod("  /  /  ")
  w_MCTIPCON = space(1)
  w_MCIMPA09 = 0
  w_IMPRIG = 0
  w_MCCODCON = space(15)
  w_MCIMPA10 = 0
  w_CAONAZ = 0
  w_MCCOMPET = space(4)
  w_MCNUMDOC = 0
  w_MCIMPA11 = 0
  w_DECTOP = 0
  w_MCCODCAU = space(5)
  w_MCCODCEN = space(15)
  w_MCIMPA12 = 0
  w_MCDATDOC = ctod("  /  /  ")
  w_MCVOCCEN = space(15)
  w_MCIMPA13 = 0
  w_GPCODICE = space(15)
  w_MCALFDOC = space(10)
  w_MCCODCOM = space(15)
  w_MCIMPA14 = 0
  w_MCIMPA15 = 0
  w_MCIMPA16 = 0
  w_APPVAL = 0
  w_OK1DOC = 0
  w_CCFLANAL = space(1)
  w_PNCOMPET = space(4)
  w_PNVALNAZ = space(3)
  w_PNSERIAL = space(10)
  w_APPSEZ = space(1)
  w_PNCODUTE = 0
  w_PNCODVAL = space(3)
  w_PNNUMRER = 0
  w_PNCODCON = space(15)
  w_PNDATREG = ctod("  /  /  ")
  w_PNCAOVAL = 0
  w_CPROWNUM = 0
  w_PNIMPDAR = 0
  w_PNCODCAU = space(5)
  w_PNCODCLF = space(15)
  w_CPROWORD = 0
  w_PNIMPAVE = 0
  w_PNNUMDOC = 0
  w_PNTIPCLF = space(1)
  w_PNCODESE = space(4)
  w_TOTDET = 0
  w_PNALFDOC = space(10)
  w_PNDESSUP = space(50)
  w_CODCAT = space(15)
  w_TOTDAR = 0
  w_PNDATDOC = ctod("  /  /  ")
  w_CAUCES = space(5)
  w_TOTAVE = 0
  w_FLSTAT = space(1)
  w_PNFLPROV = space(1)
  w_MCSTATUS = space(1)
  w_PNFLSALD = space(1)
  w_DATBLO = ctod("  /  /  ")
  w_MRCODICE = space(15)
  w_FLANAL = space(1)
  w_STALIG = ctod("  /  /  ")
  w_MESS = space(10)
  w_MRCODVOC = space(15)
  w_CCTAGG = space(1)
  w_CONCON = ctod("  /  /  ")
  w_FLERR = .f.
  w_MRCODCOM = space(15)
  w_RIFREG = space(10)
  w_PNPRG = space(8)
  w_GPCONC01 = space(15)
  w_GPCONC02 = space(15)
  w_GPCONC03 = space(15)
  w_GPCONC04 = space(15)
  w_GPCONC05 = space(15)
  w_GPCONC06 = space(15)
  w_GPCONC07 = space(15)
  w_GPCONC08 = space(15)
  w_GPCONC09 = space(15)
  w_GPCONC10 = space(15)
  w_GESCOM = space(1)
  w_MCIMPA18 = 0
  w_MCIMPA19 = 0
  w_MCIMPA20 = 0
  w_MCIMPA21 = 0
  w_MCIMPA22 = 0
  w_MCIMPA23 = 0
  w_MCIMPA24 = 0
  w_OBTEST = ctod("  /  /  ")
  w_DATOBSO = ctod("  /  /  ")
  w_OBSOLETO = .f.
  w_SEARCH_TABLE = space(25)
  w_SEARCH_ANTIPCON = space(1)
  w_SEARCH_ANCODICE = space(15)
  w_MESSPN = space(10)
  w_NUMMOV = 0
  w_FLPDOC = space(1)
  w_FLPPRO = space(1)
  w_PNANNDOC = space(4)
  w_PNANNPRO = space(4)
  w_PNDESRIG = space(50)
  w_CCDESRIG = space(254)
  w_CCDESSUP = space(254)
  w_CONTRO = .f.
  w_PSERIAL = space(10)
  w_APPCES = space(10)
  w_CPORDCES = 0
  * --- WorkFile variables
  AZIENDA_idx=0
  CONTI_idx=0
  ESERCIZI_idx=0
  GRU_COCE_idx=0
  MCO_CESP_idx=0
  MOVICOST_idx=0
  MOV_CESP_idx=0
  PNT_DETT_idx=0
  PNT_MAST_idx=0
  SALDICON_idx=0
  VALUTE_idx=0
  CAU_CONT_idx=0
  PNT_CESP_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Contabilizzazione Movimenti Cespiti (da GSCE_KCM)
    * --- Parametri dalla Maschera
    DIMENSION IMP[24], GPC[16], IMP1[24], ARPARAM[12,2]
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_CODAZI = i_CODAZI
    this.w_FLSTAT = IIF(this.oParentObject.w_FLPROV="S", " ", "C")
    * --- Carica i Movimenti Cespite da Contabilizzare
    ah_Msg("Ricerca movimenti da contabilizzare...")
    vq_exec("..\cesp\exe\query\GSCE_QCM.VQR",this,"CONTAMOV")
    if USED("CONTAMOV")
      * --- Inizio Aggiornamento vero e proprio
      this.w_NUDOC = 0
      this.w_OKDOC = 0
      this.w_OK1DOC = 0
      if RECCOUNT("CONTAMOV") > 0
        ah_Msg("Inizio fase di contabilizzazione...")
        * --- Fase di Blocco
        * --- Blocco la Prima Nota con data = Max (data reg.)
        * --- Calcolo il Max Data reg.
        this.w_MAXDAT = IIF(this.oParentObject.w_FLDATC="U", this.oParentObject.w_DATCON, this.oParentObject.w_DATFIN)
        * --- Try
        local bErr_03B0FD78
        bErr_03B0FD78=bTrsErr
        this.Try_03B0FD78()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          ah_ErrorMsg(i_errmsg,,"")
          this.Page_3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        bTrsErr=bTrsErr or bErr_03B0FD78
        * --- End
        * --- Stanzia l'oggetto per la gestione delle messaggistiche di errore
        this.w_oERRORLOG=createobject("AH_ErrorLog")
        * --- Genera i Movimenti da Contabilizzare
        this.Page_4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Alla Fine Rimuovo il Blocco Primanota
        * --- Try
        local bErr_03AD8EA8
        bErr_03AD8EA8=bTrsErr
        this.Try_03AD8EA8()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          ah_ErrorMsg("Impossibile rimuovere blocco primanota. Rimuoverlo dai dati azienda")
        endif
        bTrsErr=bTrsErr or bErr_03AD8EA8
        * --- End
        this.w_APPO = "Operazione completata%0N. %1 movimenti contabilizzati%0su %2 movimenti da contabilizzare"
        ah_ErrorMsg(this.w_APPO,,"", ALLTRIM(STR(this.w_OKDOC)), ALLTRIM(STR(this.w_NUDOC)) )
        * --- LOG Errori
        if this.w_NUDOC>0 AND this.w_OK1DOC<>this.w_NUDOC
          this.w_oERRORLOG.PrintLog(this,"Errori riscontrati")     
        endif
      else
        this.w_APPO = "Per l'intervallo selezionato non esistono movimenti da contabilizzare"
        ah_ErrorMsg(this.w_APPO)
      endif
      this.Page_3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  endproc
  proc Try_03B0FD78()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Leggo la data dei blocco e la data stampa del libro giornale
    this.w_STALIG = cp_CharToDate("  -  -  ")
    this.w_CONCON = cp_CharToDate("  -  -  ")
    this.w_DATBLO = cp_CharToDate("  -  -  ")
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZDATBLO,AZSTALIG,AZGESCOM"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZDATBLO,AZSTALIG,AZGESCOM;
        from (i_cTable) where;
            AZCODAZI = this.w_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DATBLO = NVL(cp_ToDate(_read_.AZDATBLO),cp_NullValue(_read_.AZDATBLO))
      this.w_STALIG = NVL(cp_ToDate(_read_.AZSTALIG),cp_NullValue(_read_.AZSTALIG))
      this.w_GESCOM = NVL(cp_ToDate(_read_.AZGESCOM),cp_NullValue(_read_.AZGESCOM))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Controllo Integrit� Prima Nota
    * --- Controllo che il documento possa essere contabilizzato
    do case
      case Not empty(this.w_datblo)
        * --- UN altro utente ha impostato il blocco - controllo concorrenza
        this.w_MESS = "Prima Nota bloccata - verificare semaforo bollati in dati azienda - impossibile contabilizzare"
        * --- Raise
        i_Error=this.w_MESS
        return
      case this.w_MAXDAT<=this.w_STALIG
        this.w_MESS = "La data di contabilizzazione � inferiore all'ultima stampa L.G."
        * --- Raise
        i_Error=this.w_MESS
        return
      case empty(this.w_datblo)
        * --- Inserisce <Blocco> per Primanota
        * --- Write into AZIENDA
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.AZIENDA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.AZIENDA_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"AZDATBLO ="+cp_NullLink(cp_ToStrODBC(this.w_MAXDAT),'AZIENDA','AZDATBLO');
              +i_ccchkf ;
          +" where ";
              +"AZCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
                 )
        else
          update (i_cTable) set;
              AZDATBLO = this.w_MAXDAT;
              &i_ccchkf. ;
           where;
              AZCODAZI = this.w_CODAZI;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
    endcase
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_03AD8EA8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into AZIENDA
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.AZIENDA_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"AZDATBLO ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'AZIENDA','AZDATBLO');
          +i_ccchkf ;
      +" where ";
          +"AZCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
             )
    else
      update (i_cTable) set;
          AZDATBLO = cp_CharToDate("  -  -  ");
          &i_ccchkf. ;
       where;
          AZCODAZI = this.w_CODAZI;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inizializza Variabili Locali
    FOR L_i = 1 TO 24
    IMP[L_i] = 0
    IMP1[L_i] = 0
    ENDFOR
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elimina Cursori di Appoggio
    if USED("CONTAMOV")
      SELECT CONTAMOV
      USE
    endif
    if USED("MessErr")
      SELECT MessErr
      USE
    endif
    if USED("__TMP__")
      SELECT __TMP__
      USE
    endif
    if USED("DettDocu")
      SELECT DettDocu
      USE
    endif
    if USED("CURS_CONTI")
      Select CURS_CONTI 
 Use
    endif
    i_retcode = 'stop'
    return
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Contabilizzazione Dei Movimenti
    this.w_OSERIAL = "###########################"
    * --- Testa il Cambio di Movimento
    SELECT CONTAMOV
    GO TOP
    SCAN FOR NOT EMPTY(NVL(MCSERIAL," ")) AND NOT EMPTY(NVL(MCDATREG, cp_CharToDate("  -  -  ")))
    this.w_FLERR = .F.
    this.w_MESSPN = ""
    this.w_MCSERIAL = MCSERIAL
    this.w_MCDATREG = MCDATREG
    this.w_MCCOMPET = NVL(MCCOMPET, SPACE(4))
    this.w_MCNUMDOC = NVL(MCNUMDOC, 0)
    this.w_MCALFDOC = NVL(MCALFDOC, Space(10))
    this.w_MCDATDOC = CP_TODATE(MCDATDOC)
    this.w_MCVALNAZ = NVL(MCVALNAZ, g_PERVAL)
    this.w_MCCODVAL = NVL(MCCODVAL, g_PERVAL)
    this.w_MCCAOVAL = NVL(MCCAOVAL, GETCAM(this.w_MCCODVAL, IIF(EMPTY(this.w_MCDATDOC),this.w_MCDATREG,this.w_MCDATDOC), 7) )
    this.w_MCCODCAU = NVL(MCCODCAU, SPACE(5))
    this.w_MCDESMOV = NVL(MCDESMOV, SPACE(40))
    this.w_MCCODCES = NVL(MCCODCES, SPACE(20))
    this.w_CECODCAT = NVL(CECODCAT, SPACE(15))
    this.w_CCCAUCON = NVL(CCCAUCON, SPACE(5))
    this.w_CCFLRIFE = NVL(CCFLRIFE, "N")
    this.w_CCFLANAL = NVL(CCFLANAL, "N")
    this.w_MCTIPCON = IIF(this.w_CCFLRIFE $ "CF", NVL(MCTIPCON, "N"), "N")
    this.w_MCCODCON = IIF(this.w_MCTIPCON $ "CF", NVL(MCCODCON, SPACE(15)), SPACE(15))
    this.w_MCSTATUS = NVL(MCSTATUS, "C")
    this.w_MCCODCEN = NVL(MCCODCEN, SPACE(15))
    this.w_MCVOCCEN = NVL(MCVOCCEN, SPACE(15))
    this.w_MCCODCOM = NVL(MCCODCOM, SPACE(15))
    this.w_MCIMPA01 = NVL(MCIMPA01, 0)
    this.w_MCIMPA02 = NVL(MCIMPA02, 0)
    this.w_MCIMPA03 = NVL(MCIMPA03, 0)
    this.w_MCIMPA04 = NVL(MCIMPA04, 0)
    this.w_MCIMPA05 = NVL(MCIMPA05, 0)
    this.w_MCIMPA06 = NVL(MCIMPA06, 0)
    this.w_MCIMPA07 = NVL(MCIMPA07, 0)
    this.w_MCIMPA08 = NVL(MCIMPA08, 0)
    this.w_MCIMPA09 = NVL(MCIMPA09, 0)
    this.w_MCIMPA10 = NVL(MCIMPA10, 0)
    this.w_MCIMPA11 = NVL(MCIMPA11, 0)
    this.w_MCIMPA12 = NVL(MCIMPA12, 0)
    this.w_MCIMPA13 = NVL(MCIMPA13, 0)
    this.w_MCIMPA14 = NVL(MCIMPA14, 0)
    this.w_MCIMPA15 = NVL(MCIMPA15, 0)
    this.w_MCIMPA16 = NVL(MCIMPA16, 0)
    this.w_MCIMPA18 = NVL(MCIMPA18, 0)
    this.w_MCIMPA19 = NVL(MCIMPA19, 0)
    this.w_MCIMPA20 = NVL(MCIMPA20, 0)
    this.w_MCIMPA21 = NVL(MCIMPA21, 0)
    this.w_MCIMPA22 = NVL(MCIMPA22, 0)
    this.w_MCIMPA23 = NVL(MCIMPA23, 0)
    this.w_MCIMPA24 = NVL(MCIMPA24, 0)
    this.w_MCNUMREG = NVL(MCNUMREG, 0)
    this.w_MCCODESE = NVL(MCCODESE, SPACE(4))
    this.w_NSERIAL = DTOC(CP_TODATE(this.w_MCDATREG))+this.w_CCCAUCON+this.w_CECODCAT
    this.w_NSERIAL = this.w_NSERIAL+this.w_MCCOMPET+this.w_MCCODVAL+STR(this.w_MCCAOVAL,12,7)+this.w_MCSTATUS
    this.w_NSERIAL = this.w_NSERIAL+this.w_MCCODCEN+this.w_MCVOCCEN+this.w_MCCODCOM
    * --- Testa Cambio Movimento
    if this.w_OSERIAL<>this.w_NSERIAL
      this.Page_5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Inizializza i dati di Testata della Nuova Registrazione P.N.
      this.w_NUMMOV = 0
      this.w_RIFREG = ah_Msgformat("movimento cespiti n: %1 del %2",ALLTRIM(STR(this.w_MCNUMREG))+"/"+this.w_MCCODESE, DTOC(this.w_MCDATREG))
      this.w_PNCOMPET = this.w_MCCOMPET
      this.w_PNCODESE = this.w_MCCOMPET
      this.w_PNCODUTE = IIF(g_UNIUTE $ "UE", i_CODUTE, 0)
      this.w_PNDATREG = IIF(this.oParentObject.w_FLDATC="U", this.oParentObject.w_DATCON, this.w_MCDATREG)
      this.w_PNPRG = IIF(g_UNIUTE $ "GE", DTOS(this.w_PNDATREG), SPACE(8))
      this.w_PNCODCAU = this.w_CCCAUCON
      this.w_PNNUMDOC = this.w_MCNUMDOC
      this.w_PNALFDOC = this.w_MCALFDOC
      this.w_PNDATDOC = this.w_MCDATDOC
      this.w_PNVALNAZ = this.w_MCVALNAZ
      this.w_PNCODVAL = this.w_MCCODVAL
      this.w_PNCAOVAL = this.w_MCCAOVAL
      this.w_PNFLPROV = IIF(this.w_MCSTATUS="P", "S", "N")
      this.w_PNCODCLF = this.w_MCCODCON
      this.w_PNTIPCLF = this.w_MCTIPCON
      this.w_PNDESSUP = this.w_MCDESMOV
      this.w_MRCODICE = this.w_MCCODCEN
      this.w_MRCODVOC = this.w_MCVOCCEN
      this.w_MRCODCOM = this.w_MCCODCOM
      this.w_FLANAL = IIF(g_PERCCR="S", this.w_CCFLANAL, "N")
      this.w_CAONAZ = GETCAM(this.w_PNVALNAZ, this.w_PNDATREG)
      this.w_DECTOP = g_PERPVL
      this.w_MESSPN = CHKINPNT(this.w_PNDATREG,this.w_FLANAL,"N",0,this.w_PNDATREG,"NO" ,"Load","SNSNN")
      if NOT EMPTY(this.w_MESSPN)
        * --- verifico la data del libro giornale
        this.w_oERRORLOG.AddMsgLog("%1", this.w_RIFREG)     
        this.w_oERRORLOG.AddMsgLog("%1", this.w_MESSPN)     
        this.w_FLERR = .T.
      endif
      * --- Se altra Valuta di Esercizio
      if this.w_PNVALNAZ<>g_PERVAL
        this.w_CAONAZ = GETCAM(this.w_PNVALNAZ, this.w_PNDATREG)
        this.w_DECTOP = GETVALUT(this.w_PNVALNAZ, "VADECTOT")
      endif
      FOR L_i = 1 TO 24
      IMP[L_i] = 0
      IMP1[L_i] = 0
      ENDFOR
      SELECT CONTAMOV
      this.w_CODCAT = this.w_CECODCAT
      this.w_CAUCES = this.w_MCCODCAU
      this.w_GPCODICE = NVL(GPCODICE, SPACE(15))
      GPC[1] = NVL(GPCONC01, SPACE(15))
      GPC[2] = NVL(GPCONC02, SPACE(15))
      GPC[3] = NVL(GPCONC03, SPACE(15))
      GPC[4] = NVL(GPCONC04, SPACE(15))
      GPC[5] = NVL(GPCONC05, SPACE(15))
      GPC[6] = NVL(GPCONC06, SPACE(15))
      GPC[7] = NVL(GPCONC07, SPACE(15))
      GPC[8] = NVL(GPCONC08, SPACE(15))
      GPC[9] = NVL(GPCONC09, SPACE(15))
      GPC[10] = NVL(GPCONC10, SPACE(15))
      GPC[11] = NVL(GPCONC11, SPACE(15))
      GPC[12] = NVL(GPCONC12, SPACE(15))
      GPC[13] = NVL(GPCONC13, SPACE(15))
      GPC[14] = NVL(GPCONC14, SPACE(15))
      GPC[15] = NVL(GPCONC15, SPACE(15))
      GPC[16] = NVL(GPCONC16, SPACE(15))
    endif
    this.w_OSERIAL = this.w_NSERIAL
    IMP[1] = IMP[1] + this.w_MCIMPA01
    IMP[2] = IMP[2] + this.w_MCIMPA02
    IMP[3] = IMP[3] + this.w_MCIMPA03
    IMP[4] = IMP[4] + this.w_MCIMPA04
    IMP[5] = IMP[5] + this.w_MCIMPA05
    IMP[6] = IMP[6] + this.w_MCIMPA06
    IMP[7] = IMP[7] + this.w_MCIMPA07
    IMP[8] = IMP[8] + this.w_MCIMPA08
    IMP[9] = IMP[9] + this.w_MCIMPA09
    IMP[10] = IMP[10] + this.w_MCIMPA10
    IMP[11] = IMP[11] + this.w_MCIMPA11
    IMP[12] = IMP[12] + this.w_MCIMPA12
    IMP[13] = IMP[13] + this.w_MCIMPA13
    IMP[14] = IMP[14] + this.w_MCIMPA14
    IMP[15] = IMP[15] + this.w_MCIMPA15
    IMP[16] = IMP[16] + this.w_MCIMPA16
    IMP[18] = IMP[18] + this.w_MCIMPA18
    IMP[19] = IMP[19] + this.w_MCIMPA19
    IMP[20] = IMP[20] + this.w_MCIMPA20
    IMP[21] = IMP[21] + this.w_MCIMPA21
    IMP[22] = IMP[22] + this.w_MCIMPA22
    IMP[23] = IMP[23] + this.w_MCIMPA23
    IMP[24] = IMP[24] + this.w_MCIMPA24
    * --- Cursore per verificare se esistono moviemnti orfani
    IMP1[1] = IMP1[1] + this.w_MCIMPA01
    IMP1[2] = IMP1[2] + this.w_MCIMPA02
    IMP1[3] = IMP1[3] + this.w_MCIMPA03
    IMP1[4] = IMP1[4] + this.w_MCIMPA04
    IMP1[5] = IMP1[5] + this.w_MCIMPA05
    IMP1[6] = IMP1[6] + this.w_MCIMPA06
    IMP1[7] = IMP1[7] + this.w_MCIMPA07
    IMP1[8] = IMP1[8] + this.w_MCIMPA08
    IMP1[9] = IMP1[9] + this.w_MCIMPA09
    IMP1[10] = IMP1[10] + this.w_MCIMPA10
    IMP1[11] = IMP1[11] + this.w_MCIMPA11
    IMP1[12] = IMP1[12] + this.w_MCIMPA12
    IMP1[13] = IMP1[13] + this.w_MCIMPA13
    IMP1[14] = IMP1[14] + this.w_MCIMPA14
    IMP1[15] = IMP1[15] + this.w_MCIMPA15
    IMP1[16] = IMP1[16] + this.w_MCIMPA16
    IMP1[18] = IMP1[18] + this.w_MCIMPA18
    IMP1[19] = IMP1[19] + this.w_MCIMPA19
    IMP1[20] = IMP1[20] + this.w_MCIMPA20
    IMP1[21] = IMP1[21] + this.w_MCIMPA21
    IMP1[22] = IMP1[22] + this.w_MCIMPA22
    IMP1[23] = IMP1[23] + this.w_MCIMPA23
    IMP1[24] = IMP1[24] + this.w_MCIMPA24
    this.w_NUDOC = this.w_NUDOC + 1
    if this.w_FLANAL="S" 
      * --- Array per la memorizzazione degli errori riguardanti il dettaglio della Registrazione
      * --- Dimension Mov_Err[ w_NUMMOV ]
      this.w_NUMMOV = this.w_NUMMOV +1
       
 Dimension Mov_Err[ this.w_NUMMOV ] 
 Mov_Err[ this.w_NUMMOV ] = Ah_Msgformat("movimento cespiti n: %1 del %2",ALLTRIM(STR(this.w_MCNUMREG))+"/"+this.w_MCCODESE, DTOC(this.w_MCDATREG) )
    endif
    INSERT INTO DettDocu (MCSERIAL, MCNUMREG, MCCODESE, MCDATREG) ;
    VALUES (this.w_MCSERIAL,this.w_MCNUMREG,this.w_MCCODESE,this.w_MCDATREG)
    SELECT CONTAMOV
    ENDSCAN
    * --- Testa l'Ultima Uscita
    this.Page_5()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Se non e' il Primo Ingresso, Scrive la Nuova Registrazione Contabile
    if LEFT(this.w_OSERIAL, 10) <> REPL("#", 10) AND RECCOUNT("DettDocu") > 0 
      * --- Inizializza Messaggistica di Errore
      this.w_FLERR = .F.
      * --- Variabile che serve per sapere se la contropartita contabile risulti obsoleta
      this.w_OBSOLETO = .F.
      * --- Calcola PNSERIAL, PNNUMRER
      this.w_PNSERIAL = SPACE(10)
      this.w_PNNUMRER = 0
      this.w_CPROWNUM = 0
      this.w_CPROWORD = 0
      this.w_TOTDET = 0
      this.w_TOTDAR = 0
      this.w_TOTAVE = 0
      * --- Read from CAU_CONT
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CAU_CONT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAU_CONT_idx,2],.t.,this.CAU_CONT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CCFLPDOC,CCFLPPRO,CCDESSUP,CCDESRIG"+;
          " from "+i_cTable+" CAU_CONT where ";
              +"CCCODICE = "+cp_ToStrODBC(this.w_PNCODCAU);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CCFLPDOC,CCFLPPRO,CCDESSUP,CCDESRIG;
          from (i_cTable) where;
              CCCODICE = this.w_PNCODCAU;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_FLPDOC = NVL(cp_ToDate(_read_.CCFLPDOC),cp_NullValue(_read_.CCFLPDOC))
        this.w_FLPPRO = NVL(cp_ToDate(_read_.CCFLPPRO),cp_NullValue(_read_.CCFLPPRO))
        this.w_CCDESSUP = NVL(cp_ToDate(_read_.CCDESSUP),cp_NullValue(_read_.CCDESSUP))
        this.w_CCDESRIG = NVL(cp_ToDate(_read_.CCDESRIG),cp_NullValue(_read_.CCDESRIG))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if Not Empty(this.w_CCDESSUP) or Not Empty(this.w_CCDESRIG)
        * --- Array elenco parametri per descrizioni di riga e testata
         
 ARPARAM[1,1]="NUMDOC" 
 ARPARAM[1,2]=ALLTRIM(STR(this.w_PNNUMDOC)) 
 ARPARAM[2,1]="ALFDOC" 
 ARPARAM[2,2]=this.w_PNALFDOC 
 ARPARAM[3,1]="DATDOC" 
 ARPARAM[3,2]=DTOC(this.w_PNDATDOC) 
 ARPARAM[4,1]="ALFPRO" 
 ARPARAM[4,2]="" 
 ARPARAM[5,1]="DATREG" 
 ARPARAM[5,2]=DTOC(this.w_PNDATREG) 
 ARPARAM[6,1]="CODCON" 
 ARPARAM[6,2]=ALLTRIM(this.w_PNCODCLF) 
 ARPARAM[7,1]="ELEDOC" 
 ARPARAM[7,2]="" 
 ARPARAM[8,1]="CODAGE" 
 ARPARAM[8,2]="" 
 ARPARAM[9,1]="NUMPRO" 
 ARPARAM[9,2]="" 
 ARPARAM[10,1]="NUMPAR" 
 ARPARAM[10,2]="" 
 ARPARAM[11,1]="DATSCA" 
 ARPARAM[11,2]="" 
 ARPARAM[12,1]="TIPCON" 
 ARPARAM[12,2]=this.w_PNTIPCLF
      endif
      this.w_PNDESSUP = IIF(EMPTY(this.w_PNDESSUP),CALDESPA(this.w_CCDESSUP,@ARPARAM),this.w_PNDESSUP)
      this.w_PNANNDOC = CALPRO(IIF(EMPTY(this.w_PNDATDOC), this.w_PNDATREG, this.w_PNDATDOC), this.w_PNCOMPET, this.w_FLPDOC)
      this.w_PNANNPRO = CALPRO(this.w_PNDATREG, this.w_PNCOMPET, this.w_FLPPRO)
      * --- Try
      local bErr_0392C230
      bErr_0392C230=bTrsErr
      this.Try_0392C230()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_0392C230
      * --- End
    endif
    * --- Azzera il Temporaneo di Appoggio
    CREATE CURSOR DettDocu (MCSERIAL C(15), MCNUMREG N(6), MCCODESE C(4), MCDATREG D(8))
  endproc
  proc Try_0392C230()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    i_Conn=i_TableProp[this.PNT_MAST_IDX, 3]
    cp_NextTableProg(this, i_Conn, "SEPNT", "i_codazi,w_PNSERIAL")
    cp_NextTableProg(this, i_Conn, "PRPNT", "i_codazi,w_PNCODESE,w_PNCODUTE,w_PNPRG,w_PNNUMRER")
    this.w_APPO = Ah_Msgformat("%1 del %2",ALLTRIM(STR(this.w_PNNUMRER)), DTOC(this.w_PNDATREG) )
    ah_Msg("Scrivo reg. n.: %1",.T.,.F.,.F., this.w_APPO)
    * --- Scrive la Testata
    this.w_OBTEST = iif(empty(this.w_PNDATDOC), this.w_PNDATREG, this.w_PNDATDOC)
    * --- Insert into PNT_MAST
    i_nConn=i_TableProp[this.PNT_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PNT_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PNALFDOC"+",PNALFPRO"+",PNANNDOC"+",PNANNPRO"+",PNCAOVAL"+",PNCODCAU"+",PNCODCLF"+",PNCODESE"+",PNCODUTE"+",PNCODVAL"+",PNCOMIVA"+",PNCOMPET"+",PNDATDOC"+",PNDATREG"+",PNDESSUP"+",PNFLIVDF"+",PNFLPROV"+",PNFLREGI"+",PNNUMDOC"+",PNNUMPRO"+",PNNUMREG"+",PNNUMRER"+",PNPRD"+",PNPRG"+",PNPRP"+",PNRIFCES"+",PNRIFDIS"+",PNRIFDOC"+",PNRIFINC"+",PNSERIAL"+",PNTIPCLF"+",PNTIPDOC"+",PNTIPREG"+",PNVALNAZ"+",UTCC"+",UTDC"+",UTCV"+",UTDV"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PNALFDOC),'PNT_MAST','PNALFDOC');
      +","+cp_NullLink(cp_ToStrODBC(Space(10)),'PNT_MAST','PNALFPRO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNANNDOC),'PNT_MAST','PNANNDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNANNPRO),'PNT_MAST','PNANNPRO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCAOVAL),'PNT_MAST','PNCAOVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCAU),'PNT_MAST','PNCODCAU');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCLF),'PNT_MAST','PNCODCLF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCOMPET),'PNT_MAST','PNCODESE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODUTE),'PNT_MAST','PNCODUTE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODVAL),'PNT_MAST','PNCODVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATREG),'PNT_MAST','PNCOMIVA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCOMPET),'PNT_MAST','PNCOMPET');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATDOC),'PNT_MAST','PNDATDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATREG),'PNT_MAST','PNDATREG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNDESSUP),'PNT_MAST','PNDESSUP');
      +","+cp_NullLink(cp_ToStrODBC(" "),'PNT_MAST','PNFLIVDF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLPROV),'PNT_MAST','PNFLPROV');
      +","+cp_NullLink(cp_ToStrODBC(" "),'PNT_MAST','PNFLREGI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMDOC),'PNT_MAST','PNNUMDOC');
      +","+cp_NullLink(cp_ToStrODBC(0),'PNT_MAST','PNNUMPRO');
      +","+cp_NullLink(cp_ToStrODBC(0),'PNT_MAST','PNNUMREG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMRER),'PNT_MAST','PNNUMRER');
      +","+cp_NullLink(cp_ToStrODBC("NN"),'PNT_MAST','PNPRD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNPRG),'PNT_MAST','PNPRG');
      +","+cp_NullLink(cp_ToStrODBC("NN"),'PNT_MAST','PNPRP');
      +","+cp_NullLink(cp_ToStrODBC("S"),'PNT_MAST','PNRIFCES');
      +","+cp_NullLink(cp_ToStrODBC(SPACE(10)),'PNT_MAST','PNRIFDIS');
      +","+cp_NullLink(cp_ToStrODBC(SPACE(10)),'PNT_MAST','PNRIFDOC');
      +","+cp_NullLink(cp_ToStrODBC(SPACE(10)),'PNT_MAST','PNRIFINC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PNT_MAST','PNSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNTIPCLF),'PNT_MAST','PNTIPCLF');
      +","+cp_NullLink(cp_ToStrODBC("  "),'PNT_MAST','PNTIPDOC');
      +","+cp_NullLink(cp_ToStrODBC("N"),'PNT_MAST','PNTIPREG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNVALNAZ),'PNT_MAST','PNVALNAZ');
      +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'PNT_MAST','UTCC');
      +","+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'PNT_MAST','UTDC');
      +","+cp_NullLink(cp_ToStrODBC(0),'PNT_MAST','UTCV');
      +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -    ")),'PNT_MAST','UTDV');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PNALFDOC',this.w_PNALFDOC,'PNALFPRO',Space(10),'PNANNDOC',this.w_PNANNDOC,'PNANNPRO',this.w_PNANNPRO,'PNCAOVAL',this.w_PNCAOVAL,'PNCODCAU',this.w_PNCODCAU,'PNCODCLF',this.w_PNCODCLF,'PNCODESE',this.w_PNCOMPET,'PNCODUTE',this.w_PNCODUTE,'PNCODVAL',this.w_PNCODVAL,'PNCOMIVA',this.w_PNDATREG,'PNCOMPET',this.w_PNCOMPET)
      insert into (i_cTable) (PNALFDOC,PNALFPRO,PNANNDOC,PNANNPRO,PNCAOVAL,PNCODCAU,PNCODCLF,PNCODESE,PNCODUTE,PNCODVAL,PNCOMIVA,PNCOMPET,PNDATDOC,PNDATREG,PNDESSUP,PNFLIVDF,PNFLPROV,PNFLREGI,PNNUMDOC,PNNUMPRO,PNNUMREG,PNNUMRER,PNPRD,PNPRG,PNPRP,PNRIFCES,PNRIFDIS,PNRIFDOC,PNRIFINC,PNSERIAL,PNTIPCLF,PNTIPDOC,PNTIPREG,PNVALNAZ,UTCC,UTDC,UTCV,UTDV &i_ccchkf. );
         values (;
           this.w_PNALFDOC;
           ,Space(10);
           ,this.w_PNANNDOC;
           ,this.w_PNANNPRO;
           ,this.w_PNCAOVAL;
           ,this.w_PNCODCAU;
           ,this.w_PNCODCLF;
           ,this.w_PNCOMPET;
           ,this.w_PNCODUTE;
           ,this.w_PNCODVAL;
           ,this.w_PNDATREG;
           ,this.w_PNCOMPET;
           ,this.w_PNDATDOC;
           ,this.w_PNDATREG;
           ,this.w_PNDESSUP;
           ," ";
           ,this.w_PNFLPROV;
           ," ";
           ,this.w_PNNUMDOC;
           ,0;
           ,0;
           ,this.w_PNNUMRER;
           ,"NN";
           ,this.w_PNPRG;
           ,"NN";
           ,"S";
           ,SPACE(10);
           ,SPACE(10);
           ,SPACE(10);
           ,this.w_PNSERIAL;
           ,this.w_PNTIPCLF;
           ,"  ";
           ,"N";
           ,this.w_PNVALNAZ;
           ,i_CODUTE;
           ,SetInfoDate( g_CALUTD );
           ,0;
           ,cp_CharToDate("  -  -    ");
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    do case
      case EMPTY(this.w_GPCODICE)
        this.w_oERRORLOG.AddMsgLog("'Verificare categoria cespite: %1 - gruppo contabile inesistente", this.w_CODCAT)     
        this.w_FLERR = .T.
      case Not Empty(CHKCONS(IIF(this.w_FLANAL="S","PC","P"),this.w_PNDATREG,"B","N"))
        * --- Esegue controllo date consolidamento
        this.w_oERRORLOG.AddMsgLog("Verificare movimento cespiti n. %1 del %2", ALLTRIM(STR(this.w_MCNUMREG)) +"/"+this.w_MCCODESE, DTOC(this.w_MCDATREG) )     
        this.w_oERRORLOG.AddMsgLog("%1", substr(CHKCONS(IIF(this.w_FLANAL="S","PC","P"),this.w_PNDATREG,"B","N"),12))     
        this.w_FLERR = .T.
      case Not Empty(this.w_MESSPN) 
        * --- verifico la data del libro giornale
        this.w_oERRORLOG.AddMsgLog("Verificare movimento cespiti n. %1 del %2", ALLTRIM(STR(this.w_MCNUMREG)) +"/"+this.w_MCCODESE, DTOC(this.w_MCDATREG) )     
        this.w_oERRORLOG.AddMsgLog("%1", this.w_MESSPN)     
        this.w_FLERR = .T.
      otherwise
        * --- Cicla Sui Modelli Contabili
        * --- Select from MCO_CESP
        i_nConn=i_TableProp[this.MCO_CESP_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MCO_CESP_idx,2],.t.,this.MCO_CESP_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" MCO_CESP ";
              +" where MCCODCAU="+cp_ToStrODBC(this.w_CAUCES)+"";
              +" order by CPROWORD";
               ,"_Curs_MCO_CESP")
        else
          select * from (i_cTable);
           where MCCODCAU=this.w_CAUCES;
           order by CPROWORD;
            into cursor _Curs_MCO_CESP
        endif
        if used('_Curs_MCO_CESP')
          select _Curs_MCO_CESP
          locate for 1=1
          do while not(eof())
          this.w_ACOD = NVL(_Curs_MCO_CESP.MCCODICE,"   ")
          this.w_CDAR = NVL(_Curs_MCO_CESP.MCCONDAR, "   ")
          this.w_CAVE = NVL(_Curs_MCO_CESP.MCCONAVE, "   ")
          if NOT EMPTY(this.w_ACOD) AND LEN(this.w_ACOD)=3
            this.w_APPO1 = VAL(RIGHT(this.w_ACOD,2))
            if this.w_APPO1>0 AND this.w_APPO1<25
              if IMP[this.w_APPO1]<>0
                * --- Importo da Scrivere
                this.w_IMPRIG = IMP[this.w_APPO1]
                IMP1[this.w_APPO1] = 0
                * --- Cerca Conto DARE.Effettuo l'operazione solo nel caso in cui la contropartita
                *     non risulti obsoleta
                if NOT EMPTY(this.w_CDAR) AND LEN(this.w_CDAR)=3 AND NOT this.w_OBSOLETO
                  this.w_APPO1 = VAL(RIGHT(this.w_CDAR,2))
                  if this.w_APPO1>0 AND this.w_APPO1<17
                    if NOT EMPTY(GPC[this.w_APPO1])
                      this.w_APPVAL = this.w_IMPRIG
                      this.w_APPSEZ = "D"
                      this.w_PNCODCON = GPC[this.w_APPO1]
                      w_WORKAREA=ALIAS()
                      this.w_SEARCH_TABLE = "CONTI"
                      this.w_SEARCH_ANTIPCON = "G"
                      this.w_SEARCH_ANCODICE = this.w_PNCODCON
                      this.Page_8()
                      if i_retcode='stop' or !empty(i_Error)
                        return
                      endif
                      this.w_DATOBSO = CP_TODATE(ANDTOBSO)
                      this.w_CCTAGG = NVL(ANCCTAGG,"N")
                      if NOT EMPTY(w_WORKAREA)
                        Select (w_WORKAREA)
                      endif
                      if this.w_DATOBSO>this.w_OBTEST OR EMPTY(this.w_DATOBSO)
                        this.Page_6()
                        if i_retcode='stop' or !empty(i_Error)
                          return
                        endif
                        this.w_TOTDET = this.w_TOTDET + 1
                        this.w_CONTRO = .F.
                      else
                        this.w_OBSOLETO = .T.
                      endif
                    else
                      this.w_CONTRO = .T.
                    endif
                  endif
                endif
                * --- Cerca Conto AVERE.Effettuo l'operazione solo nel caso in cui la contropartita
                *     non risulti obsoleta
                if NOT EMPTY(this.w_CAVE) AND LEN(this.w_CAVE)=3 AND NOT this.w_OBSOLETO
                  this.w_APPO1 = VAL(RIGHT(this.w_CAVE,2))
                  if this.w_APPO1>0 AND this.w_APPO1<17
                    if NOT EMPTY(GPC[this.w_APPO1])
                      this.w_APPVAL = this.w_IMPRIG
                      this.w_APPSEZ = "A"
                      this.w_PNCODCON = GPC[this.w_APPO1]
                      w_WORKAREA=ALIAS()
                      this.w_SEARCH_TABLE = "CONTI"
                      this.w_SEARCH_ANTIPCON = "G"
                      this.w_SEARCH_ANCODICE = this.w_PNCODCON
                      this.Page_8()
                      if i_retcode='stop' or !empty(i_Error)
                        return
                      endif
                      this.w_DATOBSO = CP_TODATE(ANDTOBSO)
                      this.w_CCTAGG = NVL(ANCCTAGG,"N")
                      if NOT EMPTY(w_WORKAREA)
                        Select (w_WORKAREA)
                      endif
                      if this.w_DATOBSO>this.w_OBTEST OR EMPTY(this.w_DATOBSO)
                        this.Page_6()
                        if i_retcode='stop' or !empty(i_Error)
                          return
                        endif
                        this.w_TOTDET = this.w_TOTDET + 1
                        this.w_CONTRO = .F.
                      else
                        this.w_OBSOLETO = .T.
                      endif
                    else
                      this.w_CONTRO = .T.
                    endif
                  endif
                endif
              endif
            endif
          endif
            select _Curs_MCO_CESP
            continue
          enddo
          use
        endif
        * --- Verifica 'Orfani'
        FOR L_i = 1 TO 24
        if IMP1[L_i]<>0
          this.w_oERRORLOG.AddMsgLog("Verificare modelli contabili causale cespite: %1", this.w_CAUCES)     
          this.w_oERRORLOG.AddMsgLogPartNoTrans(space(6), "Riferimento: A %1 non definito", RIGHT("00"+ALLTRIM(STR(L_i)), 2))     
          this.w_FLERR = .T.
        endif
        ENDFOR
        if this.w_TOTDET=0 OR this.w_OBSOLETO
          * --- Neppure un Dettaglio!
          SELECT DettDocu
          GO TOP
          SCAN FOR NOT EMPTY(NVL(MCSERIAL," "))
          this.w_oERRORLOG.AddMsgLog("Verificare movimento cespiti n. %1 del %2", STR(NVL(MCNUMREG,0))+"/"+NVL(MCCODESE,"    "), DTOC(NVL(MCDATREG,cp_CharToDate("  -  -  "))) )     
          * --- Verifico se l'errore � dovuto al fatto che la contropartita risulti obsoleta
          if this.w_OBSOLETO
            this.w_oERRORLOG.AddMsgLogPartNoTrans(space(6), "Contropartita %1 obsoleta",this.w_PNCODCON )     
          else
            this.w_oERRORLOG.AddMsgLogPartNoTrans(space(6), "Nessun importo contabilizzato")     
          endif
          SELECT DettDocu
          ENDSCAN
          this.w_FLERR = .T.
        endif
    endcase
    if this.w_FLERR=.F.
      if this.w_TOTDAR<>this.w_TOTAVE
        if g_QUADRA="S"
          this.w_oERRORLOG.AddMsgLog("Verificare %1", this.w_RIFREG)     
          this.w_oERRORLOG.AddMsgLogPartNoTrans(space(6), "Errore di quadratura")     
          this.w_FLERR = .T.
        else
          this.w_oERRORLOG.AddMsgLog("Verificare reg. contabile n. %1 del %2", STR(this.w_PNNUMRER), DTOC(this.w_PNDATREG))     
          this.w_oERRORLOG.AddMsgLogPartNoTrans(space(6), "Errore di quadratura (solo warning)")     
          * --- Diminuisco il contatore OKDOC1 per poter stampare l'errore di quadratura
          * --- nel caso in cui non ho settato il controllo sulla quadratura.
          this.w_OK1DOC = this.w_OK1DOC-1
        endif
      endif
    endif
    if this.w_FLERR
      * --- Abbandona la Registrazione
      * --- Raise
      i_Error="Errore"
      return
    else
      this.w_PSERIAL = this.w_PNSERIAL
      this.w_CPORDCES = 0
      SELECT DettDocu
      GO TOP
      SCAN FOR NOT EMPTY(NVL(MCSERIAL,SPACE(10)))
      this.w_APPCES = ALLTRIM(NVL(MCSERIAL,SPACE(10)))
      this.w_CPORDCES = this.w_CPORDCES+10
      * --- Insert into PNT_CESP
      i_nConn=i_TableProp[this.PNT_CESP_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PNT_CESP_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PNT_CESP_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"ACSERIAL"+",CPROWORD"+",ACMOVCES"+",ACTIPASS"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_PSERIAL),'PNT_CESP','ACSERIAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CPORDCES),'PNT_CESP','CPROWORD');
        +","+cp_NullLink(cp_ToStrODBC(this.w_APPCES),'PNT_CESP','ACMOVCES');
        +","+cp_NullLink(cp_ToStrODBC("C"),'PNT_CESP','ACTIPASS');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'ACSERIAL',this.w_PSERIAL,'CPROWORD',this.w_CPORDCES,'ACMOVCES',this.w_APPCES,'ACTIPASS',"C")
        insert into (i_cTable) (ACSERIAL,CPROWORD,ACMOVCES,ACTIPASS &i_ccchkf. );
           values (;
             this.w_PSERIAL;
             ,this.w_CPORDCES;
             ,this.w_APPCES;
             ,"C";
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Write into MOV_CESP
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.MOV_CESP_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MOV_CESP_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.MOV_CESP_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MCDATCON ="+cp_NullLink(cp_ToStrODBC(this.w_PNDATREG),'MOV_CESP','MCDATCON');
            +i_ccchkf ;
        +" where ";
            +"MCSERIAL = "+cp_ToStrODBC(this.w_APPCES);
               )
      else
        update (i_cTable) set;
            MCDATCON = this.w_PNDATREG;
            &i_ccchkf. ;
         where;
            MCSERIAL = this.w_APPCES;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      this.w_OKDOC = this.w_OKDOC + 1
      this.w_OK1DOC = this.w_OK1DOC-1
      SELECT DettDocu
      ENDSCAN
      * --- commit
      cp_EndTrs(.t.)
    endif
    return


  procedure Page_6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Scrive Dettaglio P.N. e Saldi
    this.w_CPROWNUM = this.w_CPROWNUM + 1
    this.w_CPROWORD = this.w_CPROWORD + 10
    if this.w_PNCODVAL<>this.w_PNVALNAZ
      * --- Se arriva un Importo in Valuta e il Documento e' in Valuta Converte in Moneta di Conto
      this.w_APPVAL = cp_ROUND(VAL2MON(this.w_APPVAL,this.w_PNCAOVAL,this.w_CAONAZ,this.w_PNDATREG,this.w_PNVALNAZ), this.w_DECTOP)
    endif
    if this.w_APPSEZ="D"
      this.w_PNIMPDAR = this.w_APPVAL
      this.w_PNIMPAVE = 0
    else
      this.w_PNIMPDAR = 0
      this.w_PNIMPAVE = this.w_APPVAL
    endif
    this.w_TOTAVE = this.w_TOTAVE + this.w_PNIMPAVE
    this.w_TOTDAR = this.w_TOTDAR + this.w_PNIMPDAR
    this.w_PNFLSALD = IIF(this.w_PNFLPROV="S", " ", "+")
    this.w_PNDESRIG = IIF(Not Empty(this.w_CCDESRIG),CALDESPA(this.w_CCDESRIG,@ARPARAM),"")
    * --- Insert into PNT_DETT
    i_nConn=i_TableProp[this.PNT_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PNT_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PNSERIAL"+",CPROWNUM"+",CPROWORD"+",PNTIPCON"+",PNCODCON"+",PNIMPDAR"+",PNIMPAVE"+",PNFLPART"+",PNFLZERO"+",PNDESRIG"+",PNCAURIG"+",PNCODPAG"+",PNFLSALD"+",PNFLSALI"+",PNFLSALF"+",PNINICOM"+",PNFINCOM"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PNT_DETT','PNSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'PNT_DETT','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'PNT_DETT','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC("G"),'PNT_DETT','PNTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCON),'PNT_DETT','PNCODCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNIMPDAR),'PNT_DETT','PNIMPDAR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNIMPAVE),'PNT_DETT','PNIMPAVE');
      +","+cp_NullLink(cp_ToStrODBC("N"),'PNT_DETT','PNFLPART');
      +","+cp_NullLink(cp_ToStrODBC(" "),'PNT_DETT','PNFLZERO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNDESRIG),'PNT_DETT','PNDESRIG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCAU),'PNT_DETT','PNCAURIG');
      +","+cp_NullLink(cp_ToStrODBC(SPACE(5)),'PNT_DETT','PNCODPAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLSALD),'PNT_DETT','PNFLSALD');
      +","+cp_NullLink(cp_ToStrODBC(" "),'PNT_DETT','PNFLSALI');
      +","+cp_NullLink(cp_ToStrODBC(" "),'PNT_DETT','PNFLSALF');
      +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'PNT_DETT','PNINICOM');
      +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'PNT_DETT','PNFINCOM');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PNSERIAL',this.w_PNSERIAL,'CPROWNUM',this.w_CPROWNUM,'CPROWORD',this.w_CPROWORD,'PNTIPCON',"G",'PNCODCON',this.w_PNCODCON,'PNIMPDAR',this.w_PNIMPDAR,'PNIMPAVE',this.w_PNIMPAVE,'PNFLPART',"N",'PNFLZERO'," ",'PNDESRIG',this.w_PNDESRIG,'PNCAURIG',this.w_PNCODCAU,'PNCODPAG',SPACE(5))
      insert into (i_cTable) (PNSERIAL,CPROWNUM,CPROWORD,PNTIPCON,PNCODCON,PNIMPDAR,PNIMPAVE,PNFLPART,PNFLZERO,PNDESRIG,PNCAURIG,PNCODPAG,PNFLSALD,PNFLSALI,PNFLSALF,PNINICOM,PNFINCOM &i_ccchkf. );
         values (;
           this.w_PNSERIAL;
           ,this.w_CPROWNUM;
           ,this.w_CPROWORD;
           ,"G";
           ,this.w_PNCODCON;
           ,this.w_PNIMPDAR;
           ,this.w_PNIMPAVE;
           ,"N";
           ," ";
           ,this.w_PNDESRIG;
           ,this.w_PNCODCAU;
           ,SPACE(5);
           ,this.w_PNFLSALD;
           ," ";
           ," ";
           ,cp_CharToDate("  -  -  ");
           ,cp_CharToDate("  -  -  ");
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    if this.w_PNFLPROV<>"S"
      * --- Aggiorna Saldo
      * --- Try
      local bErr_03BA6688
      bErr_03BA6688=bTrsErr
      this.Try_03BA6688()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_03BA6688
      * --- End
      * --- Write into SALDICON
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.SALDICON_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICON_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"SLDARPER =SLDARPER+ "+cp_ToStrODBC(this.w_PNIMPDAR);
        +",SLAVEPER =SLAVEPER+ "+cp_ToStrODBC(this.w_PNIMPAVE);
            +i_ccchkf ;
        +" where ";
            +"SLTIPCON = "+cp_ToStrODBC("G");
            +" and SLCODICE = "+cp_ToStrODBC(this.w_PNCODCON);
            +" and SLCODESE = "+cp_ToStrODBC(this.w_PNCODESE);
               )
      else
        update (i_cTable) set;
            SLDARPER = SLDARPER + this.w_PNIMPDAR;
            ,SLAVEPER = SLAVEPER + this.w_PNIMPAVE;
            &i_ccchkf. ;
         where;
            SLTIPCON = "G";
            and SLCODICE = this.w_PNCODCON;
            and SLCODESE = this.w_PNCODESE;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    if this.w_FLANAL="S" 
      * --- Se la Causale Gestisce l'Analitica
      this.w_CCTAGG = "N"
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANCCTAGG"+;
          " from "+i_cTable+" CONTI where ";
              +"ANTIPCON = "+cp_ToStrODBC("G");
              +" and ANCODICE = "+cp_ToStrODBC(this.w_PNCODCON);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANCCTAGG;
          from (i_cTable) where;
              ANTIPCON = "G";
              and ANCODICE = this.w_PNCODCON;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CCTAGG = NVL(cp_ToDate(_read_.ANCCTAGG),cp_NullValue(_read_.ANCCTAGG))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Scrive Movimento di Analitica
      if this.w_CCTAGG $ "AM"
        if this.w_GESCOM<>"S"
          * --- Se in dati azienda la gestione analitica per  commesse non � attivo non inserisce la commessa
          if NOT EMPTY(this.w_MRCODICE) AND NOT EMPTY(this.w_MRCODVOC) 
            * --- Insert into MOVICOST
            i_nConn=i_TableProp[this.MOVICOST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.MOVICOST_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MOVICOST_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"MRSERIAL"+",MRROWORD"+",CPROWNUM"+",MRCODVOC"+",MRCODICE"+",MR_SEGNO"+",MRTOTIMP"+",MRPARAME"+",MRINICOM"+",MRFINCOM"+",MRFLRIPA"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'MOVICOST','MRSERIAL');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'MOVICOST','MRROWORD');
              +","+cp_NullLink(cp_ToStrODBC(1),'MOVICOST','CPROWNUM');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MRCODVOC),'MOVICOST','MRCODVOC');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MRCODICE),'MOVICOST','MRCODICE');
              +","+cp_NullLink(cp_ToStrODBC(this.w_APPSEZ),'MOVICOST','MR_SEGNO');
              +","+cp_NullLink(cp_ToStrODBC(this.w_APPVAL),'MOVICOST','MRTOTIMP');
              +","+cp_NullLink(cp_ToStrODBC(1),'MOVICOST','MRPARAME');
              +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'MOVICOST','MRINICOM');
              +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'MOVICOST','MRFINCOM');
              +","+cp_NullLink(cp_ToStrODBC(" "),'MOVICOST','MRFLRIPA');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'MRSERIAL',this.w_PNSERIAL,'MRROWORD',this.w_CPROWNUM,'CPROWNUM',1,'MRCODVOC',this.w_MRCODVOC,'MRCODICE',this.w_MRCODICE,'MR_SEGNO',this.w_APPSEZ,'MRTOTIMP',this.w_APPVAL,'MRPARAME',1,'MRINICOM',cp_CharToDate("  -  -  "),'MRFINCOM',cp_CharToDate("  -  -  "),'MRFLRIPA'," ")
              insert into (i_cTable) (MRSERIAL,MRROWORD,CPROWNUM,MRCODVOC,MRCODICE,MR_SEGNO,MRTOTIMP,MRPARAME,MRINICOM,MRFINCOM,MRFLRIPA &i_ccchkf. );
                 values (;
                   this.w_PNSERIAL;
                   ,this.w_CPROWNUM;
                   ,1;
                   ,this.w_MRCODVOC;
                   ,this.w_MRCODICE;
                   ,this.w_APPSEZ;
                   ,this.w_APPVAL;
                   ,1;
                   ,cp_CharToDate("  -  -  ");
                   ,cp_CharToDate("  -  -  ");
                   ," ";
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          else
            this.Page_7()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_FLERR = .T.
          endif
        else
          * --- Se in dati azienda la gestione analitica per  commesse � attivo verifica se il codice commessa � vuoto
          if NOT EMPTY(this.w_MRCODICE) AND NOT EMPTY(this.w_MRCODVOC) 
            * --- Insert into MOVICOST
            i_nConn=i_TableProp[this.MOVICOST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.MOVICOST_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MOVICOST_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"MRSERIAL"+",MRROWORD"+",CPROWNUM"+",MRCODVOC"+",MRCODICE"+",MRCODCOM"+",MR_SEGNO"+",MRTOTIMP"+",MRPARAME"+",MRINICOM"+",MRFINCOM"+",MRFLRIPA"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'MOVICOST','MRSERIAL');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'MOVICOST','MRROWORD');
              +","+cp_NullLink(cp_ToStrODBC(1),'MOVICOST','CPROWNUM');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MRCODVOC),'MOVICOST','MRCODVOC');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MRCODICE),'MOVICOST','MRCODICE');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MRCODCOM),'MOVICOST','MRCODCOM');
              +","+cp_NullLink(cp_ToStrODBC(this.w_APPSEZ),'MOVICOST','MR_SEGNO');
              +","+cp_NullLink(cp_ToStrODBC(this.w_APPVAL),'MOVICOST','MRTOTIMP');
              +","+cp_NullLink(cp_ToStrODBC(1),'MOVICOST','MRPARAME');
              +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'MOVICOST','MRINICOM');
              +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'MOVICOST','MRFINCOM');
              +","+cp_NullLink(cp_ToStrODBC(" "),'MOVICOST','MRFLRIPA');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'MRSERIAL',this.w_PNSERIAL,'MRROWORD',this.w_CPROWNUM,'CPROWNUM',1,'MRCODVOC',this.w_MRCODVOC,'MRCODICE',this.w_MRCODICE,'MRCODCOM',this.w_MRCODCOM,'MR_SEGNO',this.w_APPSEZ,'MRTOTIMP',this.w_APPVAL,'MRPARAME',1,'MRINICOM',cp_CharToDate("  -  -  "),'MRFINCOM',cp_CharToDate("  -  -  "),'MRFLRIPA'," ")
              insert into (i_cTable) (MRSERIAL,MRROWORD,CPROWNUM,MRCODVOC,MRCODICE,MRCODCOM,MR_SEGNO,MRTOTIMP,MRPARAME,MRINICOM,MRFINCOM,MRFLRIPA &i_ccchkf. );
                 values (;
                   this.w_PNSERIAL;
                   ,this.w_CPROWNUM;
                   ,1;
                   ,this.w_MRCODVOC;
                   ,this.w_MRCODICE;
                   ,this.w_MRCODCOM;
                   ,this.w_APPSEZ;
                   ,this.w_APPVAL;
                   ,1;
                   ,cp_CharToDate("  -  -  ");
                   ,cp_CharToDate("  -  -  ");
                   ," ";
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
            if EMPTY(this.w_MRCODCOM)
              this.Page_7()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
          else
            if EMPTY(this.w_MRCODICE) OR EMPTY(this.w_MRCODVOC)
              this.Page_7()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              this.w_FLERR = .T.
            endif
          endif
        endif
      endif
    endif
  endproc
  proc Try_03BA6688()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDICON
    i_nConn=i_TableProp[this.SALDICON_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDICON_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SLTIPCON"+",SLCODICE"+",SLCODESE"+",SLDARPRO"+",SLAVEPRO"+",SLDARPER"+",SLAVEPER"+",SLDARINI"+",SLAVEINI"+",SLDARFIN"+",SLAVEFIN"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC("G"),'SALDICON','SLTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCON),'SALDICON','SLCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODESE),'SALDICON','SLCODESE');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARPRO');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEPRO');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARPER');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEPER');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARINI');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEINI');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARFIN');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEFIN');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SLTIPCON',"G",'SLCODICE',this.w_PNCODCON,'SLCODESE',this.w_PNCODESE,'SLDARPRO',0,'SLAVEPRO',0,'SLDARPER',0,'SLAVEPER',0,'SLDARINI',0,'SLAVEINI',0,'SLDARFIN',0,'SLAVEFIN',0)
      insert into (i_cTable) (SLTIPCON,SLCODICE,SLCODESE,SLDARPRO,SLAVEPRO,SLDARPER,SLAVEPER,SLDARINI,SLAVEINI,SLDARFIN,SLAVEFIN &i_ccchkf. );
         values (;
           "G";
           ,this.w_PNCODCON;
           ,this.w_PNCODESE;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Page_7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_LOOP = 1
    do while this.w_LOOP<=this.w_NUMMOV
      this.w_oERRORLOG.AddMsgLog("Verificare %1%0", Mov_Err[ this.w_LOOP ])     
      if EMPTY(this.w_MRCODICE) OR EMPTY(this.w_MRCODVOC)
        this.w_oERRORLOG.AddMsgLogPartNoTrans(space(6), "Centro di costo/ricavo e/o voce di costo/ricavo non definite")     
      endif
      if !this.w_FLERR AND EMPTY(this.w_MRCODCOM)
        this.w_oERRORLOG.AddMsgLogPartNoTrans(space(6), "Commessa non definita (solo warning)")     
      endif
      this.w_LOOP = this.w_LOOP + 1
    enddo
  endproc


  procedure Page_8
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if this.w_SEARCH_TABLE="CONTI"
      if USED("CURS_CONTI")
        Select CURS_CONTI 
 LOCATE FOR ANTIPCON=this.w_SEARCH_ANTIPCON AND ANCODICE=this.w_SEARCH_ANCODICE
        if NOT FOUND( ) AND NOT EMPTY(this.w_SEARCH_ANTIPCON) AND NOT EMPTY(this.w_SEARCH_ANCODICE)
          VQ_EXEC("QUERY\GSVE6BGE.VQR",this,"TMP_CURS_CONTI")
          if USED("TMP_CURS_CONTI")
            Select TMP_CURS_CONTI
            scatter memvar
            Insert into CURS_CONTI from memvar
            Select TMP_CURS_CONTI 
 Use 
 Select CURS_CONTI
          endif
        endif
      else
        VQ_EXEC("QUERY\GSVE6BGE.VQR",this,"CURS_CONTI") 
 wr_curs_conti=WRCURSOR("CURS_CONTI") 
 Select CURS_CONTI
      endif
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,13)]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='ESERCIZI'
    this.cWorkTables[4]='GRU_COCE'
    this.cWorkTables[5]='MCO_CESP'
    this.cWorkTables[6]='MOVICOST'
    this.cWorkTables[7]='MOV_CESP'
    this.cWorkTables[8]='PNT_DETT'
    this.cWorkTables[9]='PNT_MAST'
    this.cWorkTables[10]='SALDICON'
    this.cWorkTables[11]='VALUTE'
    this.cWorkTables[12]='CAU_CONT'
    this.cWorkTables[13]='PNT_CESP'
    return(this.OpenAllTables(13))

  proc CloseCursors()
    if used('_Curs_MCO_CESP')
      use in _Curs_MCO_CESP
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
