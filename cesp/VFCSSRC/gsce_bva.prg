* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_bva                                                        *
*              Visualizza associazioni pnt e doc                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_19]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-10-15                                                      *
* Last revis.: 2003-10-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsce_bva",oParentObject)
return(i_retval)

define class tgsce_bva as StdBatch
  * --- Local variables
  w_TIPASS = space(1)
  w_ACSERIAL = space(10)
  w_MVSERIAL = space(10)
  w_PADRE = .NULL.
  * --- WorkFile variables
  PNT_CESP_idx=0
  PNT_MAST_idx=0
  DOC_DETT_idx=0
  DOC_MAST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Visualizza i riferimenti della reg.primanota associata al movimento cespite
    this.w_PADRE = This.oparentobject
    * --- Verifica le associazioni per contabilizzazione con la primanota
    * --- Select from PNT_CESP
    i_nConn=i_TableProp[this.PNT_CESP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PNT_CESP_idx,2],.t.,this.PNT_CESP_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select ACSERIAL  from "+i_cTable+" PNT_CESP ";
          +" where ACMOVCES="+cp_ToStrODBC(this.oParentObject.w_MCSERIAL)+" AND ACTIPASS='C'";
           ,"_Curs_PNT_CESP")
    else
      select ACSERIAL from (i_cTable);
       where ACMOVCES=this.oParentObject.w_MCSERIAL AND ACTIPASS="C";
        into cursor _Curs_PNT_CESP
    endif
    if used('_Curs_PNT_CESP')
      select _Curs_PNT_CESP
      locate for 1=1
      do while not(eof())
      this.w_ACSERIAL = _Curs_PNT_CESP.ACSERIAL
      * --- associazione per contabilizzazione
      * --- Read from PNT_MAST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PNT_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2],.t.,this.PNT_MAST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "PNFLPROV,PNSERIAL"+;
          " from "+i_cTable+" PNT_MAST where ";
              +"PNSERIAL = "+cp_ToStrODBC(this.w_ACSERIAL);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          PNFLPROV,PNSERIAL;
          from (i_cTable) where;
              PNSERIAL = this.w_ACSERIAL;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.oParentObject.w_FLPROV = NVL(cp_ToDate(_read_.PNFLPROV),cp_NullValue(_read_.PNFLPROV))
        this.oParentObject.w_PNSERIALC = NVL(cp_ToDate(_read_.PNSERIAL),cp_NullValue(_read_.PNSERIAL))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
        select _Curs_PNT_CESP
        continue
      enddo
      use
    endif
    * --- Verifica le associazioni con la primanota e i documenti
    * --- Select from GSCE1AMC
    do vq_exec with 'GSCE1AMC',this,'_Curs_GSCE1AMC','',.f.,.t.
    if used('_Curs_GSCE1AMC')
      select _Curs_GSCE1AMC
      locate for 1=1
      do while not(eof())
      this.oParentObject.w_ABILITA = .T.
        select _Curs_GSCE1AMC
        continue
      enddo
      use
    endif
    this.w_PADRE.mHideControls()     
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='PNT_CESP'
    this.cWorkTables[2]='PNT_MAST'
    this.cWorkTables[3]='DOC_DETT'
    this.cWorkTables[4]='DOC_MAST'
    return(this.OpenAllTables(4))

  proc CloseCursors()
    if used('_Curs_PNT_CESP')
      use in _Curs_PNT_CESP
    endif
    if used('_Curs_GSCE1AMC')
      use in _Curs_GSCE1AMC
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
