* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_aac                                                        *
*              Dettagli piano di ammortamento                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_26]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-03-19                                                      *
* Last revis.: 2008-09-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsce_aac"))

* --- Class definition
define class tgsce_aac as StdForm
  Top    = 5
  Left   = 16

  * --- Standard Properties
  Width  = 645
  Height = 469+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-09-15"
  HelpContextID=143279465
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=31

  * --- Constant Properties
  CES_AMMO_IDX = 0
  CES_PIAN_IDX = 0
  ESERCIZI_IDX = 0
  CAT_CESP_IDX = 0
  CES_PITI_IDX = 0
  VALUTE_IDX = 0
  PAR_CESP_IDX = 0
  cFile = "CES_AMMO"
  cKeySelect = "ACNUMREG,ACCODESE,ACCODCES"
  cKeyWhere  = "ACNUMREG=this.w_ACNUMREG and ACCODESE=this.w_ACCODESE and ACCODCES=this.w_ACCODCES"
  cKeyWhereODBC = '"ACNUMREG="+cp_ToStrODBC(this.w_ACNUMREG)';
      +'+" and ACCODESE="+cp_ToStrODBC(this.w_ACCODESE)';
      +'+" and ACCODCES="+cp_ToStrODBC(this.w_ACCODCES)';

  cKeyWhereODBCqualified = '"CES_AMMO.ACNUMREG="+cp_ToStrODBC(this.w_ACNUMREG)';
      +'+" and CES_AMMO.ACCODESE="+cp_ToStrODBC(this.w_ACCODESE)';
      +'+" and CES_AMMO.ACCODCES="+cp_ToStrODBC(this.w_ACCODCES)';

  cPrg = "gsce_aac"
  cComment = "Dettagli piano di ammortamento"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CODAZI = space(5)
  w_TIPAMM = space(1)
  w_ACNUMREG = space(6)
  w_DATREG = ctod('  /  /  ')
  w_ACCODESE = space(4)
  o_ACCODESE = space(4)
  w_VALNAZ = space(3)
  w_DECTOT = 0
  w_CALCPIC = 0
  w_DESCRI = space(40)
  w_ACCODCES = space(20)
  w_DESCES = space(40)
  w_CODCAT = space(15)
  w_DESCAT = space(40)
  w_ACIMPV16 = 0
  w_ACIMPV03 = 0
  w_ACIMPV04 = 0
  w_ACCOECIV = 0
  w_ACIMPV08 = 0
  w_ACIMPV01 = 0
  w_ACIMPV05 = 0
  w_ACIMPV06 = 0
  w_ACIMPV14 = 0
  w_ACIMPV07 = 0
  w_ACCOEFI1 = 0
  w_ACCOEFI2 = 0
  w_ACIMPV11 = 0
  w_ACIMPV12 = 0
  w_ACIMPV09 = 0
  w_ACIMPV13 = 0
  w_ACIMPV10 = 0
  w_ACRIFMOV = space(10)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'CES_AMMO','gsce_aac')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsce_aacPag1","gsce_aac",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Piani di ammortamento")
      .Pages(1).HelpContextID = 11945840
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oACNUMREG_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[7]
    this.cWorkTables[1]='CES_PIAN'
    this.cWorkTables[2]='ESERCIZI'
    this.cWorkTables[3]='CAT_CESP'
    this.cWorkTables[4]='CES_PITI'
    this.cWorkTables[5]='VALUTE'
    this.cWorkTables[6]='PAR_CESP'
    this.cWorkTables[7]='CES_AMMO'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(7))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CES_AMMO_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CES_AMMO_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_ACNUMREG = NVL(ACNUMREG,space(6))
      .w_ACCODESE = NVL(ACCODESE,space(4))
      .w_ACCODCES = NVL(ACCODCES,space(20))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_10_joined
    link_1_10_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from CES_AMMO where ACNUMREG=KeySet.ACNUMREG
    *                            and ACCODESE=KeySet.ACCODESE
    *                            and ACCODCES=KeySet.ACCODCES
    *
    i_nConn = i_TableProp[this.CES_AMMO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CES_AMMO_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CES_AMMO')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CES_AMMO.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' CES_AMMO '
      link_1_10_joined=this.AddJoinedLink_1_10(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'ACNUMREG',this.w_ACNUMREG  ,'ACCODESE',this.w_ACCODESE  ,'ACCODCES',this.w_ACCODCES  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_CODAZI = i_CODAZI
        .w_TIPAMM = space(1)
        .w_DATREG = ctod("  /  /  ")
        .w_VALNAZ = space(3)
        .w_DECTOT = 0
        .w_DESCRI = space(40)
        .w_DESCES = space(40)
        .w_CODCAT = space(15)
        .w_DESCAT = space(40)
          .link_1_1('Load')
        .w_ACNUMREG = NVL(ACNUMREG,space(6))
          .link_1_3('Load')
        .w_ACCODESE = NVL(ACCODESE,space(4))
          .link_1_5('Load')
          .link_1_6('Load')
        .w_CALCPIC = DEFPIC(.w_DECTOT)
        .w_ACCODCES = NVL(ACCODCES,space(20))
          if link_1_10_joined
            this.w_ACCODCES = NVL(CECODICE110,NVL(this.w_ACCODCES,space(20)))
            this.w_DESCES = NVL(CEDESCRI110,space(40))
            this.w_CODCAT = NVL(CECODCAT110,space(15))
          else
          .link_1_10('Load')
          endif
          .link_1_12('Load')
        .w_ACIMPV16 = NVL(ACIMPV16,0)
        .w_ACIMPV03 = NVL(ACIMPV03,0)
        .w_ACIMPV04 = NVL(ACIMPV04,0)
        .w_ACCOECIV = NVL(ACCOECIV,0)
        .w_ACIMPV08 = NVL(ACIMPV08,0)
        .w_ACIMPV01 = NVL(ACIMPV01,0)
        .w_ACIMPV05 = NVL(ACIMPV05,0)
        .w_ACIMPV06 = NVL(ACIMPV06,0)
        .w_ACIMPV14 = NVL(ACIMPV14,0)
        .w_ACIMPV07 = NVL(ACIMPV07,0)
        .w_ACCOEFI1 = NVL(ACCOEFI1,0)
        .w_ACCOEFI2 = NVL(ACCOEFI2,0)
        .w_ACIMPV11 = NVL(ACIMPV11,0)
        .w_ACIMPV12 = NVL(ACIMPV12,0)
        .w_ACIMPV09 = NVL(ACIMPV09,0)
        .w_ACIMPV13 = NVL(ACIMPV13,0)
        .w_ACIMPV10 = NVL(ACIMPV10,0)
        .w_ACRIFMOV = NVL(ACRIFMOV,space(10))
        .oPgFrm.Page1.oPag.oObj_1_56.Calculate()
        cp_LoadRecExtFlds(this,'CES_AMMO')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_57.enabled = this.oPgFrm.Page1.oPag.oBtn_1_57.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI = space(5)
      .w_TIPAMM = space(1)
      .w_ACNUMREG = space(6)
      .w_DATREG = ctod("  /  /  ")
      .w_ACCODESE = space(4)
      .w_VALNAZ = space(3)
      .w_DECTOT = 0
      .w_CALCPIC = 0
      .w_DESCRI = space(40)
      .w_ACCODCES = space(20)
      .w_DESCES = space(40)
      .w_CODCAT = space(15)
      .w_DESCAT = space(40)
      .w_ACIMPV16 = 0
      .w_ACIMPV03 = 0
      .w_ACIMPV04 = 0
      .w_ACCOECIV = 0
      .w_ACIMPV08 = 0
      .w_ACIMPV01 = 0
      .w_ACIMPV05 = 0
      .w_ACIMPV06 = 0
      .w_ACIMPV14 = 0
      .w_ACIMPV07 = 0
      .w_ACCOEFI1 = 0
      .w_ACCOEFI2 = 0
      .w_ACIMPV11 = 0
      .w_ACIMPV12 = 0
      .w_ACIMPV09 = 0
      .w_ACIMPV13 = 0
      .w_ACIMPV10 = 0
      .w_ACRIFMOV = space(10)
      if .cFunction<>"Filter"
        .w_CODAZI = i_CODAZI
        .DoRTCalc(1,1,.f.)
          if not(empty(.w_CODAZI))
          .link_1_1('Full')
          endif
        .DoRTCalc(2,3,.f.)
          if not(empty(.w_ACNUMREG))
          .link_1_3('Full')
          endif
        .DoRTCalc(4,5,.f.)
          if not(empty(.w_ACCODESE))
          .link_1_5('Full')
          endif
        .DoRTCalc(6,6,.f.)
          if not(empty(.w_VALNAZ))
          .link_1_6('Full')
          endif
          .DoRTCalc(7,7,.f.)
        .w_CALCPIC = DEFPIC(.w_DECTOT)
        .DoRTCalc(9,10,.f.)
          if not(empty(.w_ACCODCES))
          .link_1_10('Full')
          endif
        .DoRTCalc(11,12,.f.)
          if not(empty(.w_CODCAT))
          .link_1_12('Full')
          endif
        .oPgFrm.Page1.oPag.oObj_1_56.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'CES_AMMO')
    this.DoRTCalc(13,31,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_57.enabled = this.oPgFrm.Page1.oPag.oBtn_1_57.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oACNUMREG_1_3.enabled = i_bVal
      .Page1.oPag.oACCODESE_1_5.enabled = i_bVal
      .Page1.oPag.oACCODCES_1_10.enabled = i_bVal
      .Page1.oPag.oACIMPV16_1_14.enabled = i_bVal
      .Page1.oPag.oACIMPV03_1_15.enabled = i_bVal
      .Page1.oPag.oACIMPV04_1_16.enabled = i_bVal
      .Page1.oPag.oACCOECIV_1_17.enabled = i_bVal
      .Page1.oPag.oACIMPV08_1_18.enabled = i_bVal
      .Page1.oPag.oACIMPV01_1_19.enabled = i_bVal
      .Page1.oPag.oACIMPV05_1_20.enabled = i_bVal
      .Page1.oPag.oACIMPV06_1_21.enabled = i_bVal
      .Page1.oPag.oACIMPV14_1_22.enabled = i_bVal
      .Page1.oPag.oACIMPV07_1_23.enabled = i_bVal
      .Page1.oPag.oACCOEFI1_1_24.enabled = i_bVal
      .Page1.oPag.oACCOEFI2_1_25.enabled = i_bVal
      .Page1.oPag.oACIMPV11_1_26.enabled = i_bVal
      .Page1.oPag.oACIMPV12_1_27.enabled = i_bVal
      .Page1.oPag.oACIMPV09_1_28.enabled = i_bVal
      .Page1.oPag.oACIMPV13_1_29.enabled = i_bVal
      .Page1.oPag.oACIMPV10_1_30.enabled = i_bVal
      .Page1.oPag.oBtn_1_57.enabled = .Page1.oPag.oBtn_1_57.mCond()
      .Page1.oPag.oObj_1_56.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oACNUMREG_1_3.enabled = .f.
        .Page1.oPag.oACCODESE_1_5.enabled = .f.
        .Page1.oPag.oACCODCES_1_10.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oACNUMREG_1_3.enabled = .t.
        .Page1.oPag.oACCODESE_1_5.enabled = .t.
        .Page1.oPag.oACCODCES_1_10.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'CES_AMMO',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CES_AMMO_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ACNUMREG,"ACNUMREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ACCODESE,"ACCODESE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ACCODCES,"ACCODCES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ACIMPV16,"ACIMPV16",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ACIMPV03,"ACIMPV03",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ACIMPV04,"ACIMPV04",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ACCOECIV,"ACCOECIV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ACIMPV08,"ACIMPV08",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ACIMPV01,"ACIMPV01",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ACIMPV05,"ACIMPV05",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ACIMPV06,"ACIMPV06",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ACIMPV14,"ACIMPV14",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ACIMPV07,"ACIMPV07",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ACCOEFI1,"ACCOEFI1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ACCOEFI2,"ACCOEFI2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ACIMPV11,"ACIMPV11",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ACIMPV12,"ACIMPV12",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ACIMPV09,"ACIMPV09",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ACIMPV13,"ACIMPV13",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ACIMPV10,"ACIMPV10",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ACRIFMOV,"ACRIFMOV",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.CES_AMMO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CES_AMMO_IDX,2])
    i_lTable = "CES_AMMO"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.CES_AMMO_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CES_AMMO_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CES_AMMO_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.CES_AMMO_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into CES_AMMO
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CES_AMMO')
        i_extval=cp_InsertValODBCExtFlds(this,'CES_AMMO')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(ACNUMREG,ACCODESE,ACCODCES,ACIMPV16,ACIMPV03"+;
                  ",ACIMPV04,ACCOECIV,ACIMPV08,ACIMPV01,ACIMPV05"+;
                  ",ACIMPV06,ACIMPV14,ACIMPV07,ACCOEFI1,ACCOEFI2"+;
                  ",ACIMPV11,ACIMPV12,ACIMPV09,ACIMPV13,ACIMPV10"+;
                  ",ACRIFMOV "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBCNull(this.w_ACNUMREG)+;
                  ","+cp_ToStrODBCNull(this.w_ACCODESE)+;
                  ","+cp_ToStrODBCNull(this.w_ACCODCES)+;
                  ","+cp_ToStrODBC(this.w_ACIMPV16)+;
                  ","+cp_ToStrODBC(this.w_ACIMPV03)+;
                  ","+cp_ToStrODBC(this.w_ACIMPV04)+;
                  ","+cp_ToStrODBC(this.w_ACCOECIV)+;
                  ","+cp_ToStrODBC(this.w_ACIMPV08)+;
                  ","+cp_ToStrODBC(this.w_ACIMPV01)+;
                  ","+cp_ToStrODBC(this.w_ACIMPV05)+;
                  ","+cp_ToStrODBC(this.w_ACIMPV06)+;
                  ","+cp_ToStrODBC(this.w_ACIMPV14)+;
                  ","+cp_ToStrODBC(this.w_ACIMPV07)+;
                  ","+cp_ToStrODBC(this.w_ACCOEFI1)+;
                  ","+cp_ToStrODBC(this.w_ACCOEFI2)+;
                  ","+cp_ToStrODBC(this.w_ACIMPV11)+;
                  ","+cp_ToStrODBC(this.w_ACIMPV12)+;
                  ","+cp_ToStrODBC(this.w_ACIMPV09)+;
                  ","+cp_ToStrODBC(this.w_ACIMPV13)+;
                  ","+cp_ToStrODBC(this.w_ACIMPV10)+;
                  ","+cp_ToStrODBC(this.w_ACRIFMOV)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CES_AMMO')
        i_extval=cp_InsertValVFPExtFlds(this,'CES_AMMO')
        cp_CheckDeletedKey(i_cTable,0,'ACNUMREG',this.w_ACNUMREG,'ACCODESE',this.w_ACCODESE,'ACCODCES',this.w_ACCODCES)
        INSERT INTO (i_cTable);
              (ACNUMREG,ACCODESE,ACCODCES,ACIMPV16,ACIMPV03,ACIMPV04,ACCOECIV,ACIMPV08,ACIMPV01,ACIMPV05,ACIMPV06,ACIMPV14,ACIMPV07,ACCOEFI1,ACCOEFI2,ACIMPV11,ACIMPV12,ACIMPV09,ACIMPV13,ACIMPV10,ACRIFMOV  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_ACNUMREG;
                  ,this.w_ACCODESE;
                  ,this.w_ACCODCES;
                  ,this.w_ACIMPV16;
                  ,this.w_ACIMPV03;
                  ,this.w_ACIMPV04;
                  ,this.w_ACCOECIV;
                  ,this.w_ACIMPV08;
                  ,this.w_ACIMPV01;
                  ,this.w_ACIMPV05;
                  ,this.w_ACIMPV06;
                  ,this.w_ACIMPV14;
                  ,this.w_ACIMPV07;
                  ,this.w_ACCOEFI1;
                  ,this.w_ACCOEFI2;
                  ,this.w_ACIMPV11;
                  ,this.w_ACIMPV12;
                  ,this.w_ACIMPV09;
                  ,this.w_ACIMPV13;
                  ,this.w_ACIMPV10;
                  ,this.w_ACRIFMOV;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.CES_AMMO_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CES_AMMO_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.CES_AMMO_IDX,i_nConn)
      *
      * update CES_AMMO
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'CES_AMMO')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " ACIMPV16="+cp_ToStrODBC(this.w_ACIMPV16)+;
             ",ACIMPV03="+cp_ToStrODBC(this.w_ACIMPV03)+;
             ",ACIMPV04="+cp_ToStrODBC(this.w_ACIMPV04)+;
             ",ACCOECIV="+cp_ToStrODBC(this.w_ACCOECIV)+;
             ",ACIMPV08="+cp_ToStrODBC(this.w_ACIMPV08)+;
             ",ACIMPV01="+cp_ToStrODBC(this.w_ACIMPV01)+;
             ",ACIMPV05="+cp_ToStrODBC(this.w_ACIMPV05)+;
             ",ACIMPV06="+cp_ToStrODBC(this.w_ACIMPV06)+;
             ",ACIMPV14="+cp_ToStrODBC(this.w_ACIMPV14)+;
             ",ACIMPV07="+cp_ToStrODBC(this.w_ACIMPV07)+;
             ",ACCOEFI1="+cp_ToStrODBC(this.w_ACCOEFI1)+;
             ",ACCOEFI2="+cp_ToStrODBC(this.w_ACCOEFI2)+;
             ",ACIMPV11="+cp_ToStrODBC(this.w_ACIMPV11)+;
             ",ACIMPV12="+cp_ToStrODBC(this.w_ACIMPV12)+;
             ",ACIMPV09="+cp_ToStrODBC(this.w_ACIMPV09)+;
             ",ACIMPV13="+cp_ToStrODBC(this.w_ACIMPV13)+;
             ",ACIMPV10="+cp_ToStrODBC(this.w_ACIMPV10)+;
             ",ACRIFMOV="+cp_ToStrODBC(this.w_ACRIFMOV)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'CES_AMMO')
        i_cWhere = cp_PKFox(i_cTable  ,'ACNUMREG',this.w_ACNUMREG  ,'ACCODESE',this.w_ACCODESE  ,'ACCODCES',this.w_ACCODCES  )
        UPDATE (i_cTable) SET;
              ACIMPV16=this.w_ACIMPV16;
             ,ACIMPV03=this.w_ACIMPV03;
             ,ACIMPV04=this.w_ACIMPV04;
             ,ACCOECIV=this.w_ACCOECIV;
             ,ACIMPV08=this.w_ACIMPV08;
             ,ACIMPV01=this.w_ACIMPV01;
             ,ACIMPV05=this.w_ACIMPV05;
             ,ACIMPV06=this.w_ACIMPV06;
             ,ACIMPV14=this.w_ACIMPV14;
             ,ACIMPV07=this.w_ACIMPV07;
             ,ACCOEFI1=this.w_ACCOEFI1;
             ,ACCOEFI2=this.w_ACCOEFI2;
             ,ACIMPV11=this.w_ACIMPV11;
             ,ACIMPV12=this.w_ACIMPV12;
             ,ACIMPV09=this.w_ACIMPV09;
             ,ACIMPV13=this.w_ACIMPV13;
             ,ACIMPV10=this.w_ACIMPV10;
             ,ACRIFMOV=this.w_ACRIFMOV;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.CES_AMMO_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CES_AMMO_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.CES_AMMO_IDX,i_nConn)
      *
      * delete CES_AMMO
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'ACNUMREG',this.w_ACNUMREG  ,'ACCODESE',this.w_ACCODESE  ,'ACCODCES',this.w_ACCODCES  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CES_AMMO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CES_AMMO_IDX,2])
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,5,.t.)
          .link_1_6('Full')
        .DoRTCalc(7,7,.t.)
        if .o_ACCODESE<>.w_ACCODESE
            .w_CALCPIC = DEFPIC(.w_DECTOT)
        endif
        .DoRTCalc(9,11,.t.)
          .link_1_12('Full')
        .oPgFrm.Page1.oPag.oObj_1_56.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(13,31,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_56.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oACIMPV16_1_14.enabled = this.oPgFrm.Page1.oPag.oACIMPV16_1_14.mCond()
    this.oPgFrm.Page1.oPag.oACIMPV03_1_15.enabled = this.oPgFrm.Page1.oPag.oACIMPV03_1_15.mCond()
    this.oPgFrm.Page1.oPag.oACIMPV04_1_16.enabled = this.oPgFrm.Page1.oPag.oACIMPV04_1_16.mCond()
    this.oPgFrm.Page1.oPag.oACCOECIV_1_17.enabled = this.oPgFrm.Page1.oPag.oACCOECIV_1_17.mCond()
    this.oPgFrm.Page1.oPag.oACIMPV08_1_18.enabled = this.oPgFrm.Page1.oPag.oACIMPV08_1_18.mCond()
    this.oPgFrm.Page1.oPag.oACIMPV01_1_19.enabled = this.oPgFrm.Page1.oPag.oACIMPV01_1_19.mCond()
    this.oPgFrm.Page1.oPag.oACIMPV05_1_20.enabled = this.oPgFrm.Page1.oPag.oACIMPV05_1_20.mCond()
    this.oPgFrm.Page1.oPag.oACIMPV06_1_21.enabled = this.oPgFrm.Page1.oPag.oACIMPV06_1_21.mCond()
    this.oPgFrm.Page1.oPag.oACIMPV14_1_22.enabled = this.oPgFrm.Page1.oPag.oACIMPV14_1_22.mCond()
    this.oPgFrm.Page1.oPag.oACIMPV07_1_23.enabled = this.oPgFrm.Page1.oPag.oACIMPV07_1_23.mCond()
    this.oPgFrm.Page1.oPag.oACCOEFI1_1_24.enabled = this.oPgFrm.Page1.oPag.oACCOEFI1_1_24.mCond()
    this.oPgFrm.Page1.oPag.oACCOEFI2_1_25.enabled = this.oPgFrm.Page1.oPag.oACCOEFI2_1_25.mCond()
    this.oPgFrm.Page1.oPag.oACIMPV11_1_26.enabled = this.oPgFrm.Page1.oPag.oACIMPV11_1_26.mCond()
    this.oPgFrm.Page1.oPag.oACIMPV12_1_27.enabled = this.oPgFrm.Page1.oPag.oACIMPV12_1_27.mCond()
    this.oPgFrm.Page1.oPag.oACIMPV09_1_28.enabled = this.oPgFrm.Page1.oPag.oACIMPV09_1_28.mCond()
    this.oPgFrm.Page1.oPag.oACIMPV13_1_29.enabled = this.oPgFrm.Page1.oPag.oACIMPV13_1_29.mCond()
    this.oPgFrm.Page1.oPag.oACIMPV10_1_30.enabled = this.oPgFrm.Page1.oPag.oACIMPV10_1_30.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_57.enabled = this.oPgFrm.Page1.oPag.oBtn_1_57.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_56.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_CESP_IDX,3]
    i_lTable = "PAR_CESP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_CESP_IDX,2], .t., this.PAR_CESP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_CESP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PCCODAZI,PCTIPAMM";
                   +" from "+i_cTable+" "+i_lTable+" where PCCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PCCODAZI',this.w_CODAZI)
            select PCCODAZI,PCTIPAMM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.PCCODAZI,space(5))
      this.w_TIPAMM = NVL(_Link_.PCTIPAMM,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_TIPAMM = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_CESP_IDX,2])+'\'+cp_ToStr(_Link_.PCCODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_CESP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ACNUMREG
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CES_PIAN_IDX,3]
    i_lTable = "CES_PIAN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CES_PIAN_IDX,2], .t., this.CES_PIAN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CES_PIAN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ACNUMREG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCE_APA',True,'CES_PIAN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PANUMREG like "+cp_ToStrODBC(trim(this.w_ACNUMREG)+"%");
                   +" and PACODESE="+cp_ToStrODBC(this.w_ACCODESE);

          i_ret=cp_SQL(i_nConn,"select PACODESE,PANUMREG,PADATREG,PADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PACODESE,PANUMREG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PACODESE',this.w_ACCODESE;
                     ,'PANUMREG',trim(this.w_ACNUMREG))
          select PACODESE,PANUMREG,PADATREG,PADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PACODESE,PANUMREG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ACNUMREG)==trim(_Link_.PANUMREG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ACNUMREG) and !this.bDontReportError
            deferred_cp_zoom('CES_PIAN','*','PACODESE,PANUMREG',cp_AbsName(oSource.parent,'oACNUMREG_1_3'),i_cWhere,'GSCE_APA',"Piani di ammortamento",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ACCODESE<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODESE,PANUMREG,PADATREG,PADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select PACODESE,PANUMREG,PADATREG,PADESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODESE,PANUMREG,PADATREG,PADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where PANUMREG="+cp_ToStrODBC(oSource.xKey(2));
                     +" and PACODESE="+cp_ToStrODBC(this.w_ACCODESE);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODESE',oSource.xKey(1);
                       ,'PANUMREG',oSource.xKey(2))
            select PACODESE,PANUMREG,PADATREG,PADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ACNUMREG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODESE,PANUMREG,PADATREG,PADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where PANUMREG="+cp_ToStrODBC(this.w_ACNUMREG);
                   +" and PACODESE="+cp_ToStrODBC(this.w_ACCODESE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODESE',this.w_ACCODESE;
                       ,'PANUMREG',this.w_ACNUMREG)
            select PACODESE,PANUMREG,PADATREG,PADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ACNUMREG = NVL(_Link_.PANUMREG,space(6))
      this.w_DATREG = NVL(cp_ToDate(_Link_.PADATREG),ctod("  /  /  "))
      this.w_DESCRI = NVL(_Link_.PADESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_ACNUMREG = space(6)
      endif
      this.w_DATREG = ctod("  /  /  ")
      this.w_DESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CES_PIAN_IDX,2])+'\'+cp_ToStr(_Link_.PACODESE,1)+'\'+cp_ToStr(_Link_.PANUMREG,1)
      cp_ShowWarn(i_cKey,this.CES_PIAN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ACNUMREG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ACCODESE
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ACCODESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_ACCODESE)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_CODAZI;
                     ,'ESCODESE',trim(this.w_ACCODESE))
          select ESCODAZI,ESCODESE,ESVALNAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ACCODESE)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ACCODESE) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oACCODESE_1_5'),i_cWhere,'',"Esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ACCODESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_ACCODESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_ACCODESE)
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ACCODESE = NVL(_Link_.ESCODESE,space(4))
      this.w_VALNAZ = NVL(_Link_.ESVALNAZ,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_ACCODESE = space(4)
      endif
      this.w_VALNAZ = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ACCODESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VALNAZ
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALNAZ) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALNAZ)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALNAZ);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALNAZ)
            select VACODVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALNAZ = NVL(_Link_.VACODVAL,space(3))
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_VALNAZ = space(3)
      endif
      this.w_DECTOT = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALNAZ Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ACCODCES
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CES_PITI_IDX,3]
    i_lTable = "CES_PITI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CES_PITI_IDX,2], .t., this.CES_PITI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CES_PITI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ACCODCES) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCE_ACE',True,'CES_PITI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CECODICE like "+cp_ToStrODBC(trim(this.w_ACCODCES)+"%");

          i_ret=cp_SQL(i_nConn,"select CECODICE,CEDESCRI,CECODCAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CECODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CECODICE',trim(this.w_ACCODCES))
          select CECODICE,CEDESCRI,CECODCAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CECODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ACCODCES)==trim(_Link_.CECODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ACCODCES) and !this.bDontReportError
            deferred_cp_zoom('CES_PITI','*','CECODICE',cp_AbsName(oSource.parent,'oACCODCES_1_10'),i_cWhere,'GSCE_ACE',"Cespiti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CECODICE,CEDESCRI,CECODCAT";
                     +" from "+i_cTable+" "+i_lTable+" where CECODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CECODICE',oSource.xKey(1))
            select CECODICE,CEDESCRI,CECODCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ACCODCES)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CECODICE,CEDESCRI,CECODCAT";
                   +" from "+i_cTable+" "+i_lTable+" where CECODICE="+cp_ToStrODBC(this.w_ACCODCES);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CECODICE',this.w_ACCODCES)
            select CECODICE,CEDESCRI,CECODCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ACCODCES = NVL(_Link_.CECODICE,space(20))
      this.w_DESCES = NVL(_Link_.CEDESCRI,space(40))
      this.w_CODCAT = NVL(_Link_.CECODCAT,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_ACCODCES = space(20)
      endif
      this.w_DESCES = space(40)
      this.w_CODCAT = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CES_PITI_IDX,2])+'\'+cp_ToStr(_Link_.CECODICE,1)
      cp_ShowWarn(i_cKey,this.CES_PITI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ACCODCES Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_10(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CES_PITI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CES_PITI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_10.CECODICE as CECODICE110"+ ",link_1_10.CEDESCRI as CEDESCRI110"+ ",link_1_10.CECODCAT as CECODCAT110"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_10 on CES_AMMO.ACCODCES=link_1_10.CECODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_10"
          i_cKey=i_cKey+'+" and CES_AMMO.ACCODCES=link_1_10.CECODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CODCAT
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_CESP_IDX,3]
    i_lTable = "CAT_CESP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_CESP_IDX,2], .t., this.CAT_CESP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_CESP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_CODCAT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_CODCAT)
            select CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCAT = NVL(_Link_.CCCODICE,space(15))
      this.w_DESCAT = NVL(_Link_.CCDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODCAT = space(15)
      endif
      this.w_DESCAT = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_CESP_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_CESP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oACNUMREG_1_3.value==this.w_ACNUMREG)
      this.oPgFrm.Page1.oPag.oACNUMREG_1_3.value=this.w_ACNUMREG
    endif
    if not(this.oPgFrm.Page1.oPag.oDATREG_1_4.value==this.w_DATREG)
      this.oPgFrm.Page1.oPag.oDATREG_1_4.value=this.w_DATREG
    endif
    if not(this.oPgFrm.Page1.oPag.oACCODESE_1_5.value==this.w_ACCODESE)
      this.oPgFrm.Page1.oPag.oACCODESE_1_5.value=this.w_ACCODESE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_9.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_9.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oACCODCES_1_10.value==this.w_ACCODCES)
      this.oPgFrm.Page1.oPag.oACCODCES_1_10.value=this.w_ACCODCES
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCES_1_11.value==this.w_DESCES)
      this.oPgFrm.Page1.oPag.oDESCES_1_11.value=this.w_DESCES
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCAT_1_12.value==this.w_CODCAT)
      this.oPgFrm.Page1.oPag.oCODCAT_1_12.value=this.w_CODCAT
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAT_1_13.value==this.w_DESCAT)
      this.oPgFrm.Page1.oPag.oDESCAT_1_13.value=this.w_DESCAT
    endif
    if not(this.oPgFrm.Page1.oPag.oACIMPV16_1_14.value==this.w_ACIMPV16)
      this.oPgFrm.Page1.oPag.oACIMPV16_1_14.value=this.w_ACIMPV16
    endif
    if not(this.oPgFrm.Page1.oPag.oACIMPV03_1_15.value==this.w_ACIMPV03)
      this.oPgFrm.Page1.oPag.oACIMPV03_1_15.value=this.w_ACIMPV03
    endif
    if not(this.oPgFrm.Page1.oPag.oACIMPV04_1_16.value==this.w_ACIMPV04)
      this.oPgFrm.Page1.oPag.oACIMPV04_1_16.value=this.w_ACIMPV04
    endif
    if not(this.oPgFrm.Page1.oPag.oACCOECIV_1_17.value==this.w_ACCOECIV)
      this.oPgFrm.Page1.oPag.oACCOECIV_1_17.value=this.w_ACCOECIV
    endif
    if not(this.oPgFrm.Page1.oPag.oACIMPV08_1_18.value==this.w_ACIMPV08)
      this.oPgFrm.Page1.oPag.oACIMPV08_1_18.value=this.w_ACIMPV08
    endif
    if not(this.oPgFrm.Page1.oPag.oACIMPV01_1_19.value==this.w_ACIMPV01)
      this.oPgFrm.Page1.oPag.oACIMPV01_1_19.value=this.w_ACIMPV01
    endif
    if not(this.oPgFrm.Page1.oPag.oACIMPV05_1_20.value==this.w_ACIMPV05)
      this.oPgFrm.Page1.oPag.oACIMPV05_1_20.value=this.w_ACIMPV05
    endif
    if not(this.oPgFrm.Page1.oPag.oACIMPV06_1_21.value==this.w_ACIMPV06)
      this.oPgFrm.Page1.oPag.oACIMPV06_1_21.value=this.w_ACIMPV06
    endif
    if not(this.oPgFrm.Page1.oPag.oACIMPV14_1_22.value==this.w_ACIMPV14)
      this.oPgFrm.Page1.oPag.oACIMPV14_1_22.value=this.w_ACIMPV14
    endif
    if not(this.oPgFrm.Page1.oPag.oACIMPV07_1_23.value==this.w_ACIMPV07)
      this.oPgFrm.Page1.oPag.oACIMPV07_1_23.value=this.w_ACIMPV07
    endif
    if not(this.oPgFrm.Page1.oPag.oACCOEFI1_1_24.value==this.w_ACCOEFI1)
      this.oPgFrm.Page1.oPag.oACCOEFI1_1_24.value=this.w_ACCOEFI1
    endif
    if not(this.oPgFrm.Page1.oPag.oACCOEFI2_1_25.value==this.w_ACCOEFI2)
      this.oPgFrm.Page1.oPag.oACCOEFI2_1_25.value=this.w_ACCOEFI2
    endif
    if not(this.oPgFrm.Page1.oPag.oACIMPV11_1_26.value==this.w_ACIMPV11)
      this.oPgFrm.Page1.oPag.oACIMPV11_1_26.value=this.w_ACIMPV11
    endif
    if not(this.oPgFrm.Page1.oPag.oACIMPV12_1_27.value==this.w_ACIMPV12)
      this.oPgFrm.Page1.oPag.oACIMPV12_1_27.value=this.w_ACIMPV12
    endif
    if not(this.oPgFrm.Page1.oPag.oACIMPV09_1_28.value==this.w_ACIMPV09)
      this.oPgFrm.Page1.oPag.oACIMPV09_1_28.value=this.w_ACIMPV09
    endif
    if not(this.oPgFrm.Page1.oPag.oACIMPV13_1_29.value==this.w_ACIMPV13)
      this.oPgFrm.Page1.oPag.oACIMPV13_1_29.value=this.w_ACIMPV13
    endif
    if not(this.oPgFrm.Page1.oPag.oACIMPV10_1_30.value==this.w_ACIMPV10)
      this.oPgFrm.Page1.oPag.oACIMPV10_1_30.value=this.w_ACIMPV10
    endif
    cp_SetControlsValueExtFlds(this,'CES_AMMO')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_ACCODESE = this.w_ACCODESE
    return

enddefine

* --- Define pages as container
define class tgsce_aacPag1 as StdContainer
  Width  = 641
  height = 469
  stdWidth  = 641
  stdheight = 469
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oACNUMREG_1_3 as StdField with uid="PBGOCSNHMR",rtseq=3,rtrep=.f.,;
    cFormVar = "w_ACNUMREG", cQueryName = "ACNUMREG",;
    bObbl = .f. , nPag = 1, value=space(6), bMultilanguage =  .f.,;
    ToolTipText = "Numero del piano di ammortamento selezionato",;
    HelpContextID = 245359181,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=97, Top=13, cSayPict="'999999'", cGetPict="'999999'", InputMask=replicate('X',6), bHasZoom = .t. , cLinkFile="CES_PIAN", cZoomOnZoom="GSCE_APA", oKey_1_1="PACODESE", oKey_1_2="this.w_ACCODESE", oKey_2_1="PANUMREG", oKey_2_2="this.w_ACNUMREG"

  func oACNUMREG_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oACNUMREG_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oACNUMREG_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CES_PIAN_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"PACODESE="+cp_ToStrODBC(this.Parent.oContained.w_ACCODESE)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"PACODESE="+cp_ToStr(this.Parent.oContained.w_ACCODESE)
    endif
    do cp_zoom with 'CES_PIAN','*','PACODESE,PANUMREG',cp_AbsName(this.parent,'oACNUMREG_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCE_APA',"Piani di ammortamento",'',this.parent.oContained
  endproc
  proc oACNUMREG_1_3.mZoomOnZoom
    local i_obj
    i_obj=GSCE_APA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.PACODESE=w_ACCODESE
     i_obj.w_PANUMREG=this.parent.oContained.w_ACNUMREG
     i_obj.ecpSave()
  endproc

  add object oDATREG_1_4 as StdField with uid="QAGDFJDPWH",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DATREG", cQueryName = "DATREG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 216186826,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=307, Top=13

  add object oACCODESE_1_5 as StdField with uid="GNARCHKTLB",rtseq=5,rtrep=.f.,;
    cFormVar = "w_ACCODESE", cQueryName = "ACNUMREG,ACCODESE",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Codice esercizio di riferimento",;
    HelpContextID = 17379915,;
   bGlobalFont=.t.,;
    Height=21, Width=50, Left=587, Top=13, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_ACCODESE"

  func oACCODESE_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
      if .not. empty(.w_ACNUMREG)
        bRes2=.link_1_3('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oACCODESE_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oACCODESE_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CODAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_CODAZI)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oACCODESE_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Esercizi",'',this.parent.oContained
  endproc

  add object oDESCRI_1_9 as StdField with uid="VLZVAOLXYS",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 169987018,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=97, Top=43, InputMask=replicate('X',40)

  add object oACCODCES_1_10 as StdField with uid="UDWMMHAXLN",rtseq=10,rtrep=.f.,;
    cFormVar = "w_ACCODCES", cQueryName = "ACNUMREG,ACCODESE,ACCODCES",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice del cespite",;
    HelpContextID = 252260953,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=97, Top=103, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="CES_PITI", cZoomOnZoom="GSCE_ACE", oKey_1_1="CECODICE", oKey_1_2="this.w_ACCODCES"

  func oACCODCES_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oACCODCES_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oACCODCES_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CES_PITI','*','CECODICE',cp_AbsName(this.parent,'oACCODCES_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCE_ACE',"Cespiti",'',this.parent.oContained
  endproc
  proc oACCODCES_1_10.mZoomOnZoom
    local i_obj
    i_obj=GSCE_ACE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CECODICE=this.parent.oContained.w_ACCODCES
     i_obj.ecpSave()
  endproc

  add object oDESCES_1_11 as StdField with uid="CNALBLNFCF",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DESCES", cQueryName = "DESCES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 15846346,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=234, Top=103, InputMask=replicate('X',40)

  add object oCODCAT_1_12 as StdField with uid="GJWVCFLINV",rtseq=12,rtrep=.f.,;
    cFormVar = "w_CODCAT", cQueryName = "CODCAT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 3322330,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=97, Top=73, InputMask=replicate('X',15), cLinkFile="CAT_CESP", cZoomOnZoom="GSCE_ACT", oKey_1_1="CCCODICE", oKey_1_2="this.w_CODCAT"

  func oCODCAT_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDESCAT_1_13 as StdField with uid="MMGFMSEVSP",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DESCAT", cQueryName = "DESCAT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 3263434,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=234, Top=73, InputMask=replicate('X',40)

  add object oACIMPV16_1_14 as StdField with uid="IMSLIHWFVO",rtseq=14,rtrep=.f.,;
    cFormVar = "w_ACIMPV16", cQueryName = "ACIMPV16",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Valore del bene civile",;
    HelpContextID = 46633532,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=160, Top=166, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oACIMPV16_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPAMM<>'F')
    endwith
   endif
  endfunc

  add object oACIMPV03_1_15 as StdField with uid="ILSTNNCAES",rtseq=15,rtrep=.f.,;
    cFormVar = "w_ACIMPV03", cQueryName = "ACIMPV03",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale accantonato civile",;
    HelpContextID = 221801927,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=160, Top=193, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oACIMPV03_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPAMM<>'F')
    endwith
   endif
  endfunc

  add object oACIMPV04_1_16 as StdField with uid="WSBZDKBXFA",rtseq=16,rtrep=.f.,;
    cFormVar = "w_ACIMPV04", cQueryName = "ACIMPV04",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Residuo da ammortamento civile",;
    HelpContextID = 221801926,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=516, Top=193, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oACIMPV04_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPAMM<>'F')
    endwith
   endif
  endfunc

  add object oACCOECIV_1_17 as StdField with uid="BMPLZDPQTT",rtseq=17,rtrep=.f.,;
    cFormVar = "w_ACCOECIV", cQueryName = "ACCOECIV",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Coefficiente civilistico",;
    HelpContextID = 253309532,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=160, Top=223, cSayPict='"999.99"', cGetPict='"999.99"'

  func oACCOECIV_1_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPAMM<>'F')
    endwith
   endif
  endfunc

  add object oACIMPV08_1_18 as StdField with uid="LSNJICSMJN",rtseq=18,rtrep=.f.,;
    cFormVar = "w_ACIMPV08", cQueryName = "ACIMPV08",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quota di accantonamento civile",;
    HelpContextID = 221801922,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=516, Top=223, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oACIMPV08_1_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPAMM<>'F')
    endwith
   endif
  endfunc

  add object oACIMPV01_1_19 as StdField with uid="RCPRBMMVUH",rtseq=19,rtrep=.f.,;
    cFormVar = "w_ACIMPV01", cQueryName = "ACIMPV01",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Valore del bene fiscale",;
    HelpContextID = 221801929,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=172, Top=272, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oACIMPV01_1_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPAMM<>'C')
    endwith
   endif
  endfunc

  add object oACIMPV05_1_20 as StdField with uid="CGVEXAMZDN",rtseq=20,rtrep=.f.,;
    cFormVar = "w_ACIMPV05", cQueryName = "ACIMPV05",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Valore non ammortizzabile",;
    HelpContextID = 221801925,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=172, Top=301, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oACIMPV05_1_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPAMM<>'C')
    endwith
   endif
  endfunc

  add object oACIMPV06_1_21 as StdField with uid="ECTJFROQZE",rtseq=21,rtrep=.f.,;
    cFormVar = "w_ACIMPV06", cQueryName = "ACIMPV06",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale accantonato fiscale",;
    HelpContextID = 221801924,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=172, Top=328, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oACIMPV06_1_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPAMM<>'C')
    endwith
   endif
  endfunc

  add object oACIMPV14_1_22 as StdField with uid="BHVHYARPWA",rtseq=22,rtrep=.f.,;
    cFormVar = "w_ACIMPV14", cQueryName = "ACIMPV14",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 46633530,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=172, Top=357, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oACIMPV14_1_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPAMM<>'C')
    endwith
   endif
  endfunc

  add object oACIMPV07_1_23 as StdField with uid="MPNQKCSBXV",rtseq=23,rtrep=.f.,;
    cFormVar = "w_ACIMPV07", cQueryName = "ACIMPV07",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Residuo da ammortamento fiscale",;
    HelpContextID = 221801923,;
   bGlobalFont=.t.,;
    Height=21, Width=124, Left=516, Top=328, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oACIMPV07_1_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPAMM<>'C')
    endwith
   endif
  endfunc

  add object oACCOEFI1_1_24 as StdField with uid="URFMUDQUHH",rtseq=24,rtrep=.f.,;
    cFormVar = "w_ACCOEFI1", cQueryName = "ACCOEFI1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "1^Coefficiente di ammortamento fiscale",;
    HelpContextID = 35205687,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=172, Top=386, cSayPict='"999.99"', cGetPict='"999.99"'

  func oACCOEFI1_1_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPAMM<>'C')
    endwith
   endif
  endfunc

  add object oACCOEFI2_1_25 as StdField with uid="NDEIVQBECS",rtseq=25,rtrep=.f.,;
    cFormVar = "w_ACCOEFI2", cQueryName = "ACCOEFI2",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "2^Coefficiente di ammortamento fiscale",;
    HelpContextID = 35205688,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=245, Top=386, cSayPict='"999.99"', cGetPict='"999.99"'

  func oACCOEFI2_1_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPAMM<>'C')
    endwith
   endif
  endfunc

  add object oACIMPV11_1_26 as StdField with uid="PCZSWVFDHC",rtseq=26,rtrep=.f.,;
    cFormVar = "w_ACIMPV11", cQueryName = "ACIMPV11",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Percentuale di deducibilitÓ",;
    HelpContextID = 46633527,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=396, Top=357, cSayPict='"999.99"', cGetPict='"999.99"'

  func oACIMPV11_1_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPAMM<>'C')
    endwith
   endif
  endfunc

  add object oACIMPV12_1_27 as StdField with uid="LZUMXZDOHO",rtseq=27,rtrep=.f.,;
    cFormVar = "w_ACIMPV12", cQueryName = "ACIMPV12",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo limite",;
    HelpContextID = 46633528,;
   bGlobalFont=.t.,;
    Height=21, Width=123, Left=516, Top=357, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oACIMPV12_1_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPAMM<>'C')
    endwith
   endif
  endfunc

  add object oACIMPV09_1_28 as StdField with uid="AIHPLMPSRJ",rtseq=28,rtrep=.f.,;
    cFormVar = "w_ACIMPV09", cQueryName = "ACIMPV09",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quota di accantonamento ordinario",;
    HelpContextID = 221801921,;
   bGlobalFont=.t.,;
    Height=21, Width=123, Left=516, Top=386, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oACIMPV09_1_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPAMM<>'C')
    endwith
   endif
  endfunc

  add object oACIMPV13_1_29 as StdField with uid="RRWOYUCDLV",rtseq=29,rtrep=.f.,;
    cFormVar = "w_ACIMPV13", cQueryName = "ACIMPV13",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quota accantonamento anticipato",;
    HelpContextID = 46633529,;
   bGlobalFont=.t.,;
    Height=21, Width=123, Left=516, Top=415, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oACIMPV13_1_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPAMM<>'C')
    endwith
   endif
  endfunc

  add object oACIMPV10_1_30 as StdField with uid="LMNXJEZLWZ",rtseq=30,rtrep=.f.,;
    cFormVar = "w_ACIMPV10", cQueryName = "ACIMPV10",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quota persa",;
    HelpContextID = 46633526,;
   bGlobalFont=.t.,;
    Height=21, Width=123, Left=516, Top=444, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oACIMPV10_1_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPAMM<>'C')
    endwith
   endif
  endfunc


  add object oObj_1_56 as cp_runprogram with uid="QSRUKEWPEX",left=0, top=483, width=163,height=23,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSCE_BAD',;
    cEvent = "Delete start,Update start",;
    nPag=1;
    , HelpContextID = 34718182


  add object oBtn_1_57 as StdButton with uid="BUJHPMFBQD",left=12, top=423, width=48,height=45,;
    CpPicture="bmp\mcesp.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per accedere al movimento cespite collegato";
    , HelpContextID = 206137814;
    , caption='\<Mov.cesp';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_57.Click()
      with this.Parent.oContained
        GSCE_BGS(this.Parent.oContained,"M", .w_ACRIFMOV)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_57.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_ACRIFMOV))
      endwith
    endif
  endfunc

  add object oStr_1_31 as StdString with uid="ZEUDPXXJZN",Visible=.t., Left=4, Top=13,;
    Alignment=1, Width=89, Height=15,;
    Caption="Numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="CHDCDAMTHV",Visible=.t., Left=216, Top=13,;
    Alignment=1, Width=87, Height=15,;
    Caption="Data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="RUFGEDMOBW",Visible=.t., Left=506, Top=13,;
    Alignment=1, Width=78, Height=15,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="LHHPKUACXC",Visible=.t., Left=4, Top=103,;
    Alignment=1, Width=89, Height=15,;
    Caption="Cespite:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="JHNAGHZSVQ",Visible=.t., Left=4, Top=43,;
    Alignment=1, Width=89, Height=15,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="YGIMSHHOPS",Visible=.t., Left=4, Top=73,;
    Alignment=1, Width=89, Height=15,;
    Caption="Categoria:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="WCJIEHOPGH",Visible=.t., Left=17, Top=272,;
    Alignment=1, Width=152, Height=18,;
    Caption="Valore del bene:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="VEBZHVZTIU",Visible=.t., Left=8, Top=140,;
    Alignment=0, Width=624, Height=15,;
    Caption="Aspetto civilistico"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_39 as StdString with uid="TLCPXZWJRA",Visible=.t., Left=8, Top=247,;
    Alignment=0, Width=619, Height=15,;
    Caption="Aspetto fiscale"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_40 as StdString with uid="EXTNIOKEAN",Visible=.t., Left=2, Top=193,;
    Alignment=1, Width=154, Height=15,;
    Caption="Totale accantonato civile:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="TGUKWRMVHV",Visible=.t., Left=295, Top=193,;
    Alignment=1, Width=218, Height=15,;
    Caption="Residuo da ammortamento civile:"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="HJLAZNJYTL",Visible=.t., Left=295, Top=223,;
    Alignment=1, Width=218, Height=15,;
    Caption="Quota accantonamento civile:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="BXHMSCEGAU",Visible=.t., Left=5, Top=223,;
    Alignment=1, Width=151, Height=15,;
    Caption="Coefficiente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="MHVXQGNNHF",Visible=.t., Left=16, Top=389,;
    Alignment=1, Width=153, Height=15,;
    Caption="Coefficienti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="DHYJDIPDYW",Visible=.t., Left=233, Top=386,;
    Alignment=0, Width=11, Height=15,;
    Caption="+"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="NCXHFVEBCB",Visible=.t., Left=1, Top=304,;
    Alignment=1, Width=168, Height=18,;
    Caption="Valore non ammortizzabile:"  ;
  , bGlobalFont=.t.

  add object oStr_1_47 as StdString with uid="DZFRZWRTMM",Visible=.t., Left=1, Top=331,;
    Alignment=1, Width=168, Height=18,;
    Caption="Totale accantonato ordinario:"  ;
  , bGlobalFont=.t.

  add object oStr_1_48 as StdString with uid="SWUTPPMIRW",Visible=.t., Left=300, Top=331,;
    Alignment=1, Width=213, Height=15,;
    Caption="Residuo da ammortamento fiscale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_49 as StdString with uid="IFCBRWGHEK",Visible=.t., Left=296, Top=360,;
    Alignment=1, Width=98, Height=15,;
    Caption="% DeducibilitÓ:"  ;
  , bGlobalFont=.t.

  add object oStr_1_50 as StdString with uid="FILGNUUZOT",Visible=.t., Left=462, Top=360,;
    Alignment=1, Width=51, Height=15,;
    Caption="Limite:"  ;
  , bGlobalFont=.t.

  add object oStr_1_51 as StdString with uid="GYJTOXUDZS",Visible=.t., Left=300, Top=389,;
    Alignment=1, Width=213, Height=18,;
    Caption="Quota accantonamento ordinario:"  ;
  , bGlobalFont=.t.

  add object oStr_1_52 as StdString with uid="CXEKSZWRDS",Visible=.t., Left=321, Top=449,;
    Alignment=1, Width=192, Height=15,;
    Caption="Quota persa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_58 as StdString with uid="TKXKNGCKWZ",Visible=.t., Left=300, Top=420,;
    Alignment=1, Width=213, Height=19,;
    Caption="Quota accantonamento anticipato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_59 as StdString with uid="QDBIMWLMAF",Visible=.t., Left=1, Top=360,;
    Alignment=1, Width=168, Height=18,;
    Caption="Totale accantonato anticipato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_60 as StdString with uid="CPOUBGOKTU",Visible=.t., Left=4, Top=166,;
    Alignment=1, Width=152, Height=18,;
    Caption="Valore del bene:"  ;
  , bGlobalFont=.t.

  add object oBox_1_53 as StdBox with uid="NZQEDNDHAG",left=1, top=265, width=633,height=1

  add object oBox_1_54 as StdBox with uid="QBEMVJZCPY",left=1, top=158, width=633,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsce_aac','CES_AMMO','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".ACNUMREG=CES_AMMO.ACNUMREG";
  +" and "+i_cAliasName2+".ACCODESE=CES_AMMO.ACCODESE";
  +" and "+i_cAliasName2+".ACCODCES=CES_AMMO.ACCODCES";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
