* --- Container for functions
* --- START CALCESCES
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: calcesces                                                       *
*              Calcesces                                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_7]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-11-26                                                      *
* Last revis.: 2004-11-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func calcesces
param pESPRIU,pPROESE

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_CODESE
  m.w_CODESE=space(4)
  private w_PROEFFE
  m.w_PROEFFE=0
  private w_FINESE
  m.w_FINESE=ctod("  /  /  ")
  private w_CODAZI
  m.w_CODAZI=space(5)
* --- WorkFile variables
  private ESERCIZI_idx
  ESERCIZI_idx=0
  private i_nOpenedTables
  i_nOpenedTables=0
  private i_cWorkTables
  dimension i_cWorkTables[1]
  private i_nTrs
  i_nTrs=iif(type("nTrsConnCnt")="U",0,iif(nTrsConnCnt=0,0,1))
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "calcesces"
if vartype(__calcesces_hook__)='O'
  __calcesces_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'calcesces('+Transform(pESPRIU)+','+Transform(pPROESE)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'calcesces')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
if calcesces_OpenTables()
  calcesces_Pag1()
endif
cp_CloseFuncTables()
if used('_Curs_ESERCIZI')
  use in _Curs_ESERCIZI
endif
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'calcesces('+Transform(pESPRIU)+','+Transform(pPROESE)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'calcesces')
Endif
*--- Activity log
if vartype(__calcesces_hook__)='O'
  __calcesces_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure calcesces_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Questa funzione restituisce il codice esercizio nell'ammortamento particolare fiscale del cespite,
  *     partendo dall'esercizio di primo utilizzo e dal  progressivo esercizio 
  * --- Parametri
  * --- Variabili
  * --- Sottrae di 1 perch� nel primo anno progressivo l'esercizio corrisponde a se stesso
  m.w_CODAZI = i_CODAZI
  m.w_PROEFFE = m.pPROESE-1
  * --- Read from ESERCIZI
  i_nOldArea=select()
  if used('_read_')
    select _read_
    use
  endif
  i_nConn=i_TableProp[ESERCIZI_idx,3]
  i_cTable=cp_SetAzi(i_TableProp[ESERCIZI_idx,2],.t.,ESERCIZI_idx)
  if i_nConn<>0
    cp_sqlexec(i_nConn,"select "+;
      "ESFINESE"+;
      " from "+i_cTable+" ESERCIZI where ";
          +"ESCODAZI = "+cp_ToStrODBC(m.w_CODAZI);
          +" and ESCODESE = "+cp_ToStrODBC(m.pESPRIU);
           ,"_read_")
    i_Rows=iif(used('_read_'),reccount(),0)
  else
    select;
      ESFINESE;
      from (i_cTable) where;
          ESCODAZI = m.w_CODAZI;
          and ESCODESE = m.pESPRIU;
       into cursor _read_
    i_Rows=_tally
  endif
  if used('_read_')
    locate for 1=1
    m.w_FINESE = NVL(cp_ToDate(_read_.ESFINESE),cp_NullValue(_read_.ESFINESE))
    use
  else
    * --- Error: sql sentence error.
    i_Error = MSG_READ_ERROR
    return
  endif
  select (i_nOldArea)
  if m.w_PROEFFE=0
    m.w_CODESE = m.pESPRIU
  else
    * --- Select from ESERCIZI
    i_nConn=i_TableProp[ESERCIZI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[ESERCIZI_idx,2],.t.,ESERCIZI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select ESCODESE from "+i_cTable+" ESERCIZI ";
          +" where ESINIESE>"+cp_ToStrODBC(m.w_FINESE)+" and ESCODAZI="+cp_ToStrODBC(m.w_CODAZI)+"";
          +" order by ESINIESE";
           ,"_Curs_ESERCIZI")
    else
      select ESCODESE from (i_cTable);
       where ESINIESE>m.w_FINESE and ESCODAZI=m.w_CODAZI;
       order by ESINIESE;
        into cursor _Curs_ESERCIZI
    endif
    if used('_Curs_ESERCIZI')
      select _Curs_ESERCIZI
      locate for 1=1
      do while not(eof())
      * --- Decrementa fino ad arrivare all'esercizio che corrisponde al progressivo
      m.w_PROEFFE = m.w_PROEFFE-1
      if m.w_PROEFFE=0
        m.w_CODESE = _Curs_ESERCIZI.ESCODESE
        EXIT
      endif
        select _Curs_ESERCIZI
        continue
      enddo
      use
    endif
  endif
  i_retcode = 'stop'
  i_retval = m.w_CODESE
  return
endproc


  function calcesces_OpenTables()
    dimension i_cWorkTables[max(1,1)]
    i_cWorkTables[1]='ESERCIZI'
    return(cp_OpenFuncTables(1))
* --- END CALCESCES
* --- START CALCPROCES
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: calcproces                                                      *
*              Calcproces                                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_11]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-11-26                                                      *
* Last revis.: 2004-11-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func calcproces
param pESPRIU,pCOMPET

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_PROESE
  m.w_PROESE=0
  private w_FINESE1
  m.w_FINESE1=ctod("  /  /  ")
  private w_FINESE2
  m.w_FINESE2=ctod("  /  /  ")
  private w_CODAZI
  m.w_CODAZI=space(5)
* --- WorkFile variables
  private ESERCIZI_idx
  ESERCIZI_idx=0
  private i_nOpenedTables
  i_nOpenedTables=0
  private i_cWorkTables
  dimension i_cWorkTables[1]
  private i_nTrs
  i_nTrs=iif(type("nTrsConnCnt")="U",0,iif(nTrsConnCnt=0,0,1))
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "calcproces"
if vartype(__calcproces_hook__)='O'
  __calcproces_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'calcproces('+Transform(pESPRIU)+','+Transform(pCOMPET)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'calcproces')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
if calcproces_OpenTables()
  calcproces_Pag1()
endif
cp_CloseFuncTables()
if used('_Curs_ESERCIZI')
  use in _Curs_ESERCIZI
endif
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'calcproces('+Transform(pESPRIU)+','+Transform(pCOMPET)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'calcproces')
Endif
*--- Activity log
if vartype(__calcproces_hook__)='O'
  __calcproces_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure calcproces_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Questa funzione restituisce il numero progressivo dell'esercizio partendo dall'esercizio di primo utilizzo e dall'esercizio di competenza
  * --- Parametri
  * --- Variabili
  m.w_CODAZI = i_CODAZI
  m.w_PROESE = 0
  * --- Read from ESERCIZI
  i_nOldArea=select()
  if used('_read_')
    select _read_
    use
  endif
  i_nConn=i_TableProp[ESERCIZI_idx,3]
  i_cTable=cp_SetAzi(i_TableProp[ESERCIZI_idx,2],.t.,ESERCIZI_idx)
  if i_nConn<>0
    cp_sqlexec(i_nConn,"select "+;
      "ESFINESE"+;
      " from "+i_cTable+" ESERCIZI where ";
          +"ESCODAZI = "+cp_ToStrODBC(m.w_CODAZI);
          +" and ESCODESE = "+cp_ToStrODBC(m.pESPRIU);
           ,"_read_")
    i_Rows=iif(used('_read_'),reccount(),0)
  else
    select;
      ESFINESE;
      from (i_cTable) where;
          ESCODAZI = m.w_CODAZI;
          and ESCODESE = m.pESPRIU;
       into cursor _read_
    i_Rows=_tally
  endif
  if used('_read_')
    locate for 1=1
    m.w_FINESE1 = NVL(cp_ToDate(_read_.ESFINESE),cp_NullValue(_read_.ESFINESE))
    use
  else
    * --- Error: sql sentence error.
    i_Error = MSG_READ_ERROR
    return
  endif
  select (i_nOldArea)
  * --- Read from ESERCIZI
  i_nOldArea=select()
  if used('_read_')
    select _read_
    use
  endif
  i_nConn=i_TableProp[ESERCIZI_idx,3]
  i_cTable=cp_SetAzi(i_TableProp[ESERCIZI_idx,2],.t.,ESERCIZI_idx)
  if i_nConn<>0
    cp_sqlexec(i_nConn,"select "+;
      "ESFINESE"+;
      " from "+i_cTable+" ESERCIZI where ";
          +"ESCODAZI = "+cp_ToStrODBC(m.w_CODAZI);
          +" and ESCODESE = "+cp_ToStrODBC(m.pCOMPET);
           ,"_read_")
    i_Rows=iif(used('_read_'),reccount(),0)
  else
    select;
      ESFINESE;
      from (i_cTable) where;
          ESCODAZI = m.w_CODAZI;
          and ESCODESE = m.pCOMPET;
       into cursor _read_
    i_Rows=_tally
  endif
  if used('_read_')
    locate for 1=1
    m.w_FINESE2 = NVL(cp_ToDate(_read_.ESFINESE),cp_NullValue(_read_.ESFINESE))
    use
  else
    * --- Error: sql sentence error.
    i_Error = MSG_READ_ERROR
    return
  endif
  select (i_nOldArea)
  * --- Select from ESERCIZI
  i_nConn=i_TableProp[ESERCIZI_idx,3]
  i_cTable=cp_SetAzi(i_TableProp[ESERCIZI_idx,2],.t.,ESERCIZI_idx)
  if i_nConn<>0
    cp_sqlexec(i_nConn,"select COUNT(*) AS CONTA from "+i_cTable+" ESERCIZI ";
        +" where  ESFINESE>="+cp_ToStrODBC(m.w_FINESE1)+" AND ESFINESE<="+cp_ToStrODBC(m.w_FINESE2)+" and ESCODAZI="+cp_ToStrODBC(m.w_CODAZI)+"";
         ,"_Curs_ESERCIZI")
  else
    select COUNT(*) AS CONTA from (i_cTable);
     where  ESFINESE>=m.w_FINESE1 AND ESFINESE<=m.w_FINESE2 and ESCODAZI=m.w_CODAZI;
      into cursor _Curs_ESERCIZI
  endif
  if used('_Curs_ESERCIZI')
    select _Curs_ESERCIZI
    locate for 1=1
    do while not(eof())
    m.w_PROESE = CONTA
      select _Curs_ESERCIZI
      continue
    enddo
    use
  endif
  i_retcode = 'stop'
  i_retval = m.w_PROESE
  return
endproc


  function calcproces_OpenTables()
    dimension i_cWorkTables[max(1,1)]
    i_cWorkTables[1]='ESERCIZI'
    return(cp_OpenFuncTables(1))
* --- END CALCPROCES
