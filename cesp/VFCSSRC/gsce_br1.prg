* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_br1                                                        *
*              STAMPA REGISTRO CESPITI X CESPITE                               *
*                                                                              *
*      Author: ZUCCHETTI TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_93]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-12-27                                                      *
* Last revis.: 2014-11-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsce_br1",oParentObject)
return(i_retval)

define class tgsce_br1 as StdBatch
  * --- Local variables
  w_COMPET = space(4)
  w_DECTOT = 0
  w_SIMVAL = space(5)
  w_DATFIN = ctod("  /  /  ")
  w_RESAMMO = 0
  w_MESS = space(255)
  w_OK = .f.
  w_CECODICE = space(20)
  w_APPO = 0
  w_VALCALBE = 0
  w_ACCFISCA = 0
  w_VALAMMO = 0
  w_VALBENE = 0
  w_INTLIG = space(1)
  w_PREFIS = space(20)
  w_TOTPARZ = 0
  w_TOTPARZC = 0
  w_RESAMCIV = 0
  w_ACCCIV = 0
  w_VALBENEC = 0
  w_APPOC = 0
  w_FILTCATE = space(1)
  w_OK = .f.
  w_LCODATT = space(5)
  w_CONFER = space(1)
  * --- WorkFile variables
  VALUTE_idx=0
  PAR_CESP_idx=0
  AZIENDA_idx=0
  TMP_LIB_CESP_idx=0
  ATT_ALTE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcola i saldi iniziali ed i movimenti del registro cespiti (da GSCE_SRC)
    * --- Evidenzio anche ammortamento non deducibile su Registro Cespiti
    * --- Contiene il valore del campo PAR_CESP.PCREGCES
    * --- Leggo i decimali da stampare sul report
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VADECTOT,VASIMVAL"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(this.oParentObject.w_VALNAZ);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VADECTOT,VASIMVAL;
        from (i_cTable) where;
            VACODVAL = this.oParentObject.w_VALNAZ;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DECTOT = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
      this.w_SIMVAL = NVL(cp_ToDate(_read_.VASIMVAL),cp_NullValue(_read_.VASIMVAL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Leggo sui parametri cespiti il flag 'Stampa tutti i movimenti'
    * --- Questo flag serve per poter verificare se stampare solo i cespiti moviemntati nell'esercizio oppure
    * --- anche quelli non movimentati.
    * --- Read from PAR_CESP
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_CESP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_CESP_idx,2],.t.,this.PAR_CESP_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PCREGCES,PCSTAINT,PCPREFIS"+;
        " from "+i_cTable+" PAR_CESP where ";
            +"PCCODAZI = "+cp_ToStrODBC(I_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PCREGCES,PCSTAINT,PCPREFIS;
        from (i_cTable) where;
            PCCODAZI = I_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_FILTCATE = NVL(cp_ToDate(_read_.PCREGCES),cp_NullValue(_read_.PCREGCES))
      this.w_INTLIG = NVL(cp_ToDate(_read_.PCSTAINT),cp_NullValue(_read_.PCSTAINT))
      this.w_PREFIS = NVL(cp_ToDate(_read_.PCPREFIS),cp_NullValue(_read_.PCPREFIS))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_FILTCATE = nvl(this.w_FILTCATE,"N")
    this.w_COMPET = this.oParentObject.w_CODESE
    this.w_DATFIN = this.oParentObject.w_FINESE
    * --- Controllo se simulata o definitiva se ho dei mov. provvisori nell'intervallo
    *     Nel caso di ristampa non controllo ovviamente se nell'intervallo ho dei provvisori
    if this.oParentObject.w_TIPSTAM<>"R"
      this.w_OK = .F.
      * --- Select from GSCE3BKRC
      do vq_exec with 'GSCE3BKRC',this,'_Curs_GSCE3BKRC','',.f.,.t.
      if used('_Curs_GSCE3BKRC')
        select _Curs_GSCE3BKRC
        locate for 1=1
        do while not(eof())
        this.w_OK = Nvl( _Curs_GSCE3BKRC.CONTA , 0 ) <>0
          select _Curs_GSCE3BKRC
          continue
        enddo
        use
      endif
      if this.w_OK
        this.w_MESS = "Esistono per il periodo selezionato delle registrazioni provvisorie che verranno ignorate%0Proseguire con l'elaborazione?"
        if NOT ah_YesNo(this.w_MESS)
          i_retcode = 'stop'
          return
        endif
      endif
      * --- Se stampa simulata o definitiva l'esercizio deve superare quello di stampa definitiva
      if this.oParentObject.w_INIESE<=this.oParentObject.w_ULTDAT
        this.w_MESS = "L'esercizio di stampa deve essere maggiore di quello di stampa definitiva"
        ah_ErrorMsg(this.w_MESS)
        i_retcode = 'stop'
        return
      endif
    else
      * --- Se ristampa l'esercizio NON deve superare quello di stampa definitiva
      if this.oParentObject.w_INIESE>this.oParentObject.w_ULTDAT OR EMPTY(this.oParentObject.w_ULTDAT)
        this.w_MESS = "L'esercizio di ristampa supera quello di stampa definitiva"
        ah_ErrorMsg(this.w_MESS)
        i_retcode = 'stop'
        return
      endif
    endif
    * --- Recupero i saldi esercizi precedenti....
    *     ===================================
    * --- Controllo se ho settato il check 'Cespiti non movimentati nella stampa Registro Cespiti'
    *     Saldi Cespiti - Group by: categoria,valuta; Filter: competenza (<), azienda(=)
    * --- Se il tipo ammortamento � civile w_TIPAMM='C'
    *     ATTENZIONE: la query restituisce una struttura con alias identici a quelli per tipo fiscale
    *     ma i campi estratti riguardano l'ammortamento civile
    if this.w_FILTCATE="S"
      * --- Carico nella tabella temporanea l'elenco dei cespiti che devo considerare.
      *     Se w_FILTCATE='S' stampo tutti i Cespiti anche quelli non movimentati 
      *     altrimenti solo quelli con almeno un movimento confermato nell'esercizio.
      *     ATTENZIONE: I saldi importati da ad hoc Windows vanno cmq considerati,
      *     quindi includo anche cespiti mai movimentati sul database che hanno un saldo.
      * --- Create temporary table TMP_LIB_CESP
      i_nIdx=cp_AddTableDef('TMP_LIB_CESP') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('GSCET1BRC',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.TMP_LIB_CESP_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      vq_exec("..\CESP\EXE\QUERY\GSCE4KR1.VQR", this, "saldi")
      * --- Drop temporary table TMP_LIB_CESP
      i_nIdx=cp_GetTableDefIdx('TMP_LIB_CESP')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('TMP_LIB_CESP')
      endif
    else
      vq_exec("..\CESP\EXE\QUERY\GSCE11KR1.VQR", this, "saldi")
    endif
    * --- Recupero i movimenti nell'esercizio di competenza
    *     ==========================================
    *     Controllo se ho settato il check 'Cespiti non movimentati nella stampa Registro Cespiti 
    *     (w_FLTCATE) per filtrare nella query i cespiti
    *     La query utilizza w_TIPAMM (civile/fiscale) per capire quali
    *     importi prelevare.
    *     
    *     Movimenti Cespiti - Filter:competenza (=)
    vq_exec("..\CESP\EXE\QUERY\GSCE1KR1.VQR", this, "movime")
    * --- Verifico se il cursore saldi contiene almeno un record
    if used("Saldi") and reccount("saldi")>0
       
 SELECT cecodice as cecodice, cccodice as cccodice,Max(mcvalnaz) as mcvalnaz, Max(cedescri) as cedescri, ; 
 Max(descrcat) as descrcat,Max(mcdatreg) as mcdatreg, 999999 as mcnumreg, space(10) as mcserial, ; 
 Max(ceespriu) as ceespriu, Sum(mcimpa02) as mcimpa02, Sum(mcimpa03) as mcimpa03 , ; 
 Sum(mcimpa04) as mcimpa04, Sum(mcimpa06) as mcimpa06, Sum(scaccfis) as scaccfis, ; 
 Max(mcimpa09) as mcimpa09, Sum(scincval) as scincval, Sum(scimpnoa) as scimpnoa , ; 
 Sum (scquoper) as scquoper, Max(cedatdis) as cedatdis,space(40) as ccdescri,0 as mcimpa01,; 
 0 as mcimpa13, Sum(mcimpa10) as mcimpa10,Max(Mccoefis) as Mccoefis,; 
 Max (Mccoefi1) as Mccoefi1,0 as Mcimpa15,0 as Resammo, Sum(Scaccant ) as Scaccant,; 
 Sum (Scimpons) as Scimpons,Sum(Scimpriv) as Scimpriv,Sum(Scdecval) as Scdecval,Sum(Scimpsva) as Scimpsva,; 
 0 as Mcimpa14,Sum(Mcimpa16) as Mcimpa16,Space(40) as Andescri,Space(8) as An___cap,Space(30) as Anlocali,; 
 Space(2) as Anprovin,999999999999999 as Mcnumdoc,Space(10) as Mcalfdoc,Max(Cedatdis) as Mcdatdoc,Space(40) as Mcdesmov,; 
 Space(1) as Antipcon, Max(Ccgrucon) as Ccgrucon, ; 
 Max(Ceespric) as Ceespric,Sum(Mcimpa23) as Mcimpa23,Sum(Mcimpa24) as Mcimpa24,Sum(Mcimpa18) as Mcimpa18,; 
 Sum(Mcimpa19) as Mcimpa19,Sum(Scaccciv) as Scaccciv,Sum(Mcimpa07) as Mcimpa07,Sum(Scincvac) as Scincvac,; 
 Sum(Mcimpa02-Mcimpa02) as Mcimpa20,Sum(Mcimpa08) as Mcimpa08,Max(Mccoeciv) as Mccoeciv,; 
 Sum(Mcimpa02-Mcimpa02) as Resamciv,Sum(Scimponc) as Scimponc, Sum(Scimpric) as Scimpric,; 
 Sum(Scdecvac) as Scdecvac,Sum(Scimpsvc) as Scimpsvc,; 
 Sum(Mcimpa02-Mcimpa02) as Mcimpa21,Sum(Mcimpa02-Mcimpa02 ) as Resamcivc, ; 
 Sum(ScArrAmm) As ScArrAmm, Sum(ScArrFon) As ScArrFon ; 
 From Saldi GROUP BY cccodice,cecodice into cursor stampa
      * --- Chiudo il cursore 'Saldi'
      if used("Saldi")
        select ("Saldi")
        use
      endif
      * --- Unisce i dati (saldi iniziali + movimenti)
       
 select movime.*, 0*McImpa14 As ScArrAmm , 0*McImpa14 As ScArrFon , 1 as ordmov, 0*McImpa14 As Totparz, 0*McImpa14 As TotParzC from movime ; 
 UNION ALL select Stampa.*,0 as ordmov, 0*McImpa14 As Totparz, 0*McImpa14 As TotParzC from Stampa ; 
 ORDER BY 2,1, ordmov, 5, 6,7 into cursor TEMP
    else
      * --- Calcolo il residuo da ammortizzare
       
 select movime.*, 0*McImpa14 As ScArrAmm , 0*McImpa14 As ScArrFon ,1 as ordmov, 0*McImpa14 As Totparz, 0*McImpa14 As TotParzC from movime ; 
 order by 2,1,5,6,7 into cursor TEMP
    endif
    * --- Calcolo il residuo da ammortizzare.Effettuo questa operazione
    *     all'interno del batch perch� non riesco ad effettuare i calcoli all'interno del report.
    Select TEMP
    =Wrcursor("TEMP")
    this.w_CECODICE = SPACE(20)
    Go Top
    Scan
    * --- Controllo il codice cespite se � variato.
    if this.w_Cecodice<>Temp.Cecodice
      this.w_RESAMMO = 0
      this.w_ACCFISCA = 0
      this.w_OK = .F.
      this.w_VALBENE = 0
      this.w_APPO = 0
      this.w_RESAMCIV = 0
      this.w_ACCCIV = 0
      this.w_VALBENEC = 0
      this.w_APPOC = 0
      this.w_TOTPARZ = 0
      this.w_TOTPARZC = 0
      * --- Temp.ordmov vale 0 se la riga deriva dai saldi mentre vale 1 se la riga deriva dai movimenti
      if Temp.ordmov=0
        this.w_VALBENE = (Temp.Scincval+Temp.Scimpons+Temp.Scimpriv)-(Temp.Scdecval+Temp.Scimpsva)
        this.w_ACCFISCA = (Temp.Scaccant+Temp.Scaccfis-Temp.Mcimpa10-Temp.Mcimpa16)
        this.w_RESAMMO = (this.w_Valbene-Temp.Scimpnoa-Temp.Scquoper-this.w_Accfisca) + Temp.ScArrAmm
        this.w_VALBENEC = (Temp.Scincvac+Temp.Scimponc+Temp.Scimpric)-(Temp.Scdecvac+Temp.Scimpsvc)
        this.w_ACCCIV = Temp.Scaccciv-Temp.Mcimpa08
        this.w_RESAMCIV = this.w_Valbenec-this.w_Accciv
        this.w_TOTPARZ = IIF(this.oParentObject.w_TIPAMM="F", this.w_RESAMMO, this.w_RESAMCIV )
        this.w_TOTPARZC = this.w_RESAMCIV
        this.w_OK = .T.
      else
        this.w_APPO = (Temp.Mcimpa09+Temp.Mcimpa15)-(Temp.Mcimpa10+Temp.Mcimpa16)
        this.w_VALBENE = (Temp.Scincval+Temp.Scimpons+Temp.Scimpriv)-(Temp.Scdecval+Temp.Scimpsva)
        this.w_RESAMMO = this.w_Valbene-(this.w_Appo)-Temp.Scquoper-Temp.Scimpnoa
        this.w_VALBENEC = (Temp.Scincvac+Temp.Scimponc+Temp.Scimpric)-(Temp.Scdecvac+Temp.Scimpsvc)
        this.w_APPOC = Temp.Mcimpa07-Temp.Mcimpa08
        this.w_RESAMCIV = this.w_Valbenec-this.w_Appoc
        this.w_TOTPARZ = IIF(this.oParentObject.w_TIPAMM="F", this.w_RESAMMO, this.w_RESAMCIV )
        this.w_TOTPARZC = this.w_RESAMCIV
      endif
      Replace Resammo with this.w_Resammo
      Replace Resamciv with this.w_Resamciv
      Replace Totparz with this.w_Totparz
      Replace Totparzc with this.w_Totparzc
    else
      this.w_VALBENE = (Temp.Scincval+Temp.Scimpons+Temp.Scimpriv)-(Temp.Scdecval+Temp.Scimpsva)
      this.w_APPO = (Temp.Mcimpa09+Temp.Mcimpa15-Temp.Mcimpa10-Temp.Mcimpa16)
      this.w_RESAMMO = this.w_Resammo+this.w_Valbene-(this.w_Appo)-Temp.Scquoper-Temp.Scimpnoa 
      this.w_VALBENEC = (Temp.Scincvac+Temp.Scimponc+Temp.Scimpric)-(Temp.Scdecvac+Temp.Scimpsvc)
      this.w_APPOC = (Temp.Mcimpa07-Temp.Mcimpa08)
      this.w_RESAMCIV = this.w_Resamciv+(this.w_Valbenec-this.w_Appoc)
      this.w_TOTPARZ = IIF(this.oParentObject.w_TIPAMM="F",this.w_Valbene-(this.w_Appo),this.w_Valbenec-(this.w_Appoc))-Temp.Scquoper-Temp.Scimpnoa 
      this.w_TOTPARZC = this.w_Valbenec-(this.w_Appoc)
      Replace Resammo with this.w_Resammo
      Replace Resamciv with this.w_Resamciv
      Replace Totparz with this.w_Totparz
      Replace Totparzc with this.w_Totparzc
    endif
    this.w_CECODICE = Temp.Cecodice
    Endscan
    * --- Passo il risultato del cursore Temp al cursore __TMP__
    if this.oParentObject.w_STAREG="C"
      Select * from Temp order by 2,1,65,6,7 into cursor __TMP__
    else
      Select * from Temp order by 44,2,1,65,6,7 into cursor __TMP__
    endif
    * --- Chiude i cursori
    if used("movime")
      select ("movime")
      use
    endif
    if used("Saldi")
      select ("Saldi")
      use
    endif
    if used("Temp")
      select ("Temp")
      use
    endif
    * --- Parametri della query passati al report
    l_compet = this.w_COMPET
    l_iniese = this.oParentObject.w_INIESE
    l_finese = this.oParentObject.w_FINESE
    l_valnaz = this.oParentObject.w_VALNAZ
    l_dectot = this.w_DECTOT
    l_simval=this.w_Simval
    * --- Applico Codice Attivit� alternativo
    *     Esiste procedura di conversione che crea record di default cod codice attivit�
    *     pari al codice attivit� padre
    if Not Empty(this.oParentObject.w_CODATT)
      this.w_LCODATT = this.oParentObject.w_CODATT
      * --- Select from ATT_ALTE
      i_nConn=i_TableProp[this.ATT_ALTE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ATT_ALTE_idx,2],.t.,this.ATT_ALTE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select CAATTIVI,CADESCRI  from "+i_cTable+" ATT_ALTE ";
            +" where CADATATT<="+cp_ToStrODBC(this.oParentObject.w_INIESE)+" AND CACODATT="+cp_ToStrODBC(this.oParentObject.w_CODATT)+"";
            +" order by CADATATT Desc";
             ,"_Curs_ATT_ALTE")
      else
        select CAATTIVI,CADESCRI from (i_cTable);
         where CADATATT<=this.oParentObject.w_INIESE AND CACODATT=this.oParentObject.w_CODATT;
         order by CADATATT Desc;
          into cursor _Curs_ATT_ALTE
      endif
      if used('_Curs_ATT_ALTE')
        select _Curs_ATT_ALTE
        locate for 1=1
        do while not(eof())
        this.w_LCODATT = Nvl(_Curs_ATT_ALTE.CAATTIVI,Space(5))
        Exit
          select _Curs_ATT_ALTE
          continue
        enddo
        use
      endif
    endif
    l_codatt= this.w_LCODATT
    l_catini= this.oParentObject.w_CATINI
    l_catfin= this.oParentObject.w_CATFIN
    CEFLSTDT = this.oParentObject.w_PCFLSTIN
    * --- PER NUMERAZIONE PAGINE IN TESTATA
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZINDAZI,AZLOCAZI,AZCAPAZI,AZPROAZI,AZPIVAZI,AZCOFAZI"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZINDAZI,AZLOCAZI,AZCAPAZI,AZPROAZI,AZPIVAZI,AZCOFAZI;
        from (i_cTable) where;
            AZCODAZI = this.oParentObject.w_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      w_INDAZI = NVL(cp_ToDate(_read_.AZINDAZI),cp_NullValue(_read_.AZINDAZI))
      w_LOCAZI = NVL(cp_ToDate(_read_.AZLOCAZI),cp_NullValue(_read_.AZLOCAZI))
      w_CAPAZI = NVL(cp_ToDate(_read_.AZCAPAZI),cp_NullValue(_read_.AZCAPAZI))
      w_PROAZI = NVL(cp_ToDate(_read_.AZPROAZI),cp_NullValue(_read_.AZPROAZI))
      w_PIVAZI = NVL(cp_ToDate(_read_.AZPIVAZI),cp_NullValue(_read_.AZPIVAZI))
      w_COFAZI = NVL(cp_ToDate(_read_.AZCOFAZI),cp_NullValue(_read_.AZCOFAZI))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    L_INTLIG=this.w_INTLIG
    L_PREFIS=this.w_PREFIS
    L_INDAZI=w_INDAZI
    L_LOCAZI=w_LOCAZI
    L_CAPAZI=w_CAPAZI
    L_PROAZI=w_PROAZI
    L_COFAZI=w_COFAZI
    L_PIVAZI=w_PIVAZI
    * --- Lascio variabile perch� gi� impostata nella stampa
    select ("__TMP__")
    if !(this.oParentObject.w_TIPAMM="F" AND this.oParentObject.w_INFAGG="S")
      WRCURSOR("__TMP__")
      if this.oParentObject.w_TIPAMM="F"
        delete FOR EMPTY (NVL (CEESPRIU,"")) 
      else
        delete FOR EMPTY (NVL (CEESPRIC,"")) 
      endif
      Go top
    endif
    if this.oParentObject.w_TOTCAT="S"
      if this.oParentObject.w_TIPAMM="F"
        if this.oParentObject.w_INFAGG="S"
          CP_CHPRN("..\CESP\EXE\QUERY\GSCE90KRC.FRX", " ", this)
        else
          CP_CHPRN("..\CESP\EXE\QUERY\GSCE91KRC.FRX", " ", this)
        endif
      else
        CP_CHPRN("..\CESP\EXE\QUERY\GSCE92KRC.FRX", " ", this)
      endif
    else
      if this.oParentObject.w_FLSALP="S"
        if this.oParentObject.w_TIPAMM="F"
          if this.oParentObject.w_INFAGG="S"
            CP_CHPRN("..\CESP\EXE\QUERY\GSCE77KRC.FRX", " ", this)
          else
            CP_CHPRN("..\CESP\EXE\QUERY\GSCE5KRC.FRX", " ", this)
          endif
        else
          CP_CHPRN("..\CESP\EXE\QUERY\GSCE7KRC.FRX", " ", this)
        endif
      else
        if this.oParentObject.w_STAREG="G"
          if this.oParentObject.w_TIPAMM="F"
            if this.oParentObject.w_INFAGG="S"
              CP_CHPRN("..\CESP\EXE\QUERY\GSCE99KRC.FRX", " ", this)
            else
              CP_CHPRN("..\CESP\EXE\QUERY\GSCE9KRC.FRX", " ", this)
            endif
          else
            CP_CHPRN("..\CESP\EXE\QUERY\GSCE10KRC.FRX", " ", this)
          endif
        else
          if this.oParentObject.w_TIPAMM="F"
            if this.oParentObject.w_INFAGG="S"
              CP_CHPRN("..\CESP\EXE\QUERY\GSCE46KRC.FRX", " ", this)
            else
              CP_CHPRN("..\CESP\EXE\QUERY\GSCE4KRC.FRX", " ", this)
            endif
          else
            CP_CHPRN("..\CESP\EXE\QUERY\GSCE6KRC.FRX", " ", this)
          endif
        endif
      endif
    endif
    * --- Aggiorna l'esercizio di stampa definitiva
    if this.oParentObject.w_TIPSTAM="D" AND ((this.oParentObject.w_PCTIPAMM="E" AND this.oParentObject.w_TIPAMM="F") OR (this.oParentObject.w_PCTIPAMM="C" AND this.oParentObject.w_TIPAMM="C") OR (this.oParentObject.w_PCTIPAMM="F" AND this.oParentObject.w_TIPAMM="F"))
      do GSCE_S2R with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if this.w_CONFER="S"
        * --- Write into PAR_CESP
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.PAR_CESP_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_CESP_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_CESP_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PCDATCON ="+cp_NullLink(cp_ToStrODBC(this.w_COMPET),'PAR_CESP','PCDATCON');
              +i_ccchkf ;
          +" where ";
              +"PCCODAZI = "+cp_ToStrODBC(I_CODAZI);
                 )
        else
          update (i_cTable) set;
              PCDATCON = this.w_COMPET;
              &i_ccchkf. ;
           where;
              PCCODAZI = I_CODAZI;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='VALUTE'
    this.cWorkTables[2]='PAR_CESP'
    this.cWorkTables[3]='AZIENDA'
    this.cWorkTables[4]='*TMP_LIB_CESP'
    this.cWorkTables[5]='ATT_ALTE'
    return(this.OpenAllTables(5))

  proc CloseCursors()
    if used('_Curs_GSCE3BKRC')
      use in _Curs_GSCE3BKRC
    endif
    if used('_Curs_ATT_ALTE')
      use in _Curs_ATT_ALTE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
