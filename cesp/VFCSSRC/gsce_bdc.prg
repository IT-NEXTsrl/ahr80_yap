* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_bdc                                                        *
*              Elimina/conferma riferimento cespite                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_22]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-05-18                                                      *
* Last revis.: 1999-05-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsce_bdc",oParentObject)
return(i_retval)

define class tgsce_bdc as StdBatch
  * --- Local variables
  w_SERIAL = space(10)
  w_APPO = space(10)
  w_Conf = .f.
  * --- WorkFile variables
  MOV_CESP_idx=0
  PNT_CESP_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questo Batch viene eseguito dalla procedura GSCG_BCK del Modulo COGE
    * --- E' necessario che avvwenga dentro tale Modulo poiche' non verrebbe gestito dalla sua Analisi
    this.w_SERIAL = this.oParentObject.oParentObject.w_PNSERIAL
    this.w_Conf = this.oParentObject.w_ConfRif
    if NOT EMPTY(this.w_SERIAL)
      if this.w_Conf=.T.
        ah_Msg("Confermo movimenti cespite associati...")
        * --- Select from PNT_CESP
        i_nConn=i_TableProp[this.PNT_CESP_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PNT_CESP_idx,2],.t.,this.PNT_CESP_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select ACMOVCES  from "+i_cTable+" PNT_CESP ";
              +" where ACSERIAL="+cp_ToStrODBC(this.w_SERIAL)+" AND ACTIPASS='C'";
               ,"_Curs_PNT_CESP")
        else
          select ACMOVCES from (i_cTable);
           where ACSERIAL=this.w_SERIAL AND ACTIPASS="C";
            into cursor _Curs_PNT_CESP
        endif
        if used('_Curs_PNT_CESP')
          select _Curs_PNT_CESP
          locate for 1=1
          do while not(eof())
          this.w_APPO = NVL(_Curs_PNT_CESP.ACMOVCES,SPACE(10))
          if NOT EMPTY(this.w_APPO)
            * --- Write into MOV_CESP
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.MOV_CESP_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.MOV_CESP_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.MOV_CESP_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"MCSTATUS ="+cp_NullLink(cp_ToStrODBC("C"),'MOV_CESP','MCSTATUS');
                  +i_ccchkf ;
              +" where ";
                  +"MCSERIAL = "+cp_ToStrODBC(this.w_APPO);
                     )
            else
              update (i_cTable) set;
                  MCSTATUS = "C";
                  &i_ccchkf. ;
               where;
                  MCSERIAL = this.w_APPO;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
            select _Curs_PNT_CESP
            continue
          enddo
          use
        endif
      else
        * --- Eseguo la select dal transitorio perch� in cancellazione � sempre istanziato e
        *     da esso posso ricavare il seriale del movimento cespite
        if type("THIS.OPARENTOBJECT.OPARENTOBJECT.GSCG_MAC.CNT")="O"
          ah_Msg("Elimino riferimenti cespiti...")
          SELECT * FROM (THIS.OPARENTOBJECT.OPARENTOBJECT.GSCG_MAC.CNT.CTRSNAME) INTO CURSOR APPO WHERE t_ACTIPASS="C"
          if RECCOUNT("APPO")>0
            SELECT APPO
            GO TOP 
 SCAN
            this.w_APPO = NVL(ACMOVCES,SPACE(10))
            if NOT EMPTY(this.w_APPO)
              * --- Write into MOV_CESP
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.MOV_CESP_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.MOV_CESP_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.MOV_CESP_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"MCDATCON ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'MOV_CESP','MCDATCON');
                    +i_ccchkf ;
                +" where ";
                    +"MCSERIAL = "+cp_ToStrODBC(this.w_APPO);
                       )
              else
                update (i_cTable) set;
                    MCDATCON = cp_CharToDate("  -  -  ");
                    &i_ccchkf. ;
                 where;
                    MCSERIAL = this.w_APPO;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
            ENDSCAN
          endif
          if used("APPO")
            SELECT APPO
            USE
          endif
        endif
      endif
      WAIT CLEAR
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='MOV_CESP'
    this.cWorkTables[2]='PNT_CESP'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_PNT_CESP')
      use in _Curs_PNT_CESP
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
