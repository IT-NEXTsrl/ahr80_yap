* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_bcv                                                        *
*              Calcoli valori cespiti                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_247]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-03-01                                                      *
* Last revis.: 2017-07-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_Padre
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsce_bcv",oParentObject,m.w_Padre)
return(i_retval)

define class tgsce_bcv as StdBatch
  * --- Local variables
  w_Padre = space(1)
  w_CEPROESE = 0
  w_CEPROESC = 0
  w_PROESC = 0
  w_PERTOL = 0
  w_ECPERORD = 0
  w_QUOFISOK = .f.
  w_COEFFCIV = 0
  w_DURCESFIS = 0
  w_MSG = .f.
  w_CCCOEORD = 0
  w_QUOPEORD = 0
  w_AMMEFFET = 0
  w_PERC09 = 0
  w_PERC15 = 0
  w_SCIMPONS = 0
  w_SCIMPRIV = 0
  w_SCINCVAL = 0
  w_SCDECVAL = 0
  w_SCIMPSVA = 0
  w_SCQUOPER = 0
  w_RIFCAL1 = 0
  w_AMMABU = space(2)
  w_DURCES = 0
  w_APPO = 0
  w_APPO1 = space(10)
  w_APPO2 = ctod("  /  /  ")
  w_SCACCCIV = 0
  w_SCUTICIV = 0
  w_SCIMPNOA = 0
  w_SCACCFIS = 0
  w_SCUTIFIS = 0
  w_VALLIM = space(3)
  w_PERRID = 0
  w_SCACCANT = 0
  w_PERQUOTE = 0
  w_EATIPAMM = space(1)
  w_EAPERORD = 0
  w_EAPERANT = 0
  w_CCCOECIV = 0
  w_CCPERCIV = 0
  w_CCFLAMCI = space(1)
  w_CCCOEFIS = 0
  w_PROESE = 0
  w_QUOTEPER = 0
  w_MESS = space(50)
  w_RESIMPV7 = 0
  w_SCUTIANT = 0
  w_RIFCAL = 0
  w_GIORNI = 0
  w_FINESPRE = ctod("  /  /  ")
  w_INESPRE = ctod("  /  /  ")
  w_DATINI = ctod("  /  /  ")
  w_DATFIN = ctod("  /  /  ")
  w_ANNO1 = 0
  w_ANNO2 = 0
  w_VALUTA1 = space(3)
  w_VALUTA2 = space(3)
  w_OLDAN1 = 0
  w_OLDAN2 = 0
  w_OLDVAL1 = space(3)
  w_Appoggio = 0
  w_MAXANT = 0
  w_PCTIPAMM = space(1)
  w_SCQTACES = 0
  w_SCINCVAC = 0
  w_SCIMPONC = 0
  w_SCDECVAC = 0
  w_SCIMPRIC = 0
  w_SCIMPSVC = 0
  w_MESS1 = space(100)
  * --- WorkFile variables
  CAT_AMMO_idx=0
  CAT_CESP_idx=0
  PAR_CESP_idx=0
  ESERCIZI_idx=0
  CES_AMMO_idx=0
  MOV_CESP_idx=0
  CES_PITI_idx=0
  AMM_CESP_idx=0
  AMC_CESP_idx=0
  CAT_AMMC_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcola Valori (V01..V12)  nei Movimenti Cespiti e nel Piano di Ammortamento (da GSCE_AMC e GSCE_BPA)
    * --- Vale "A" se lanciato da GSCE_BPA mentre vale "M" se lanciato da movimenti manuali 
    * --- Controllo se il batch viene lanciato da movimenti manuali.
    * --- In questo caso devo verificare che il cespite non sia ceduto.
    if this.w_Padre="M"
      do case
        case this.oParentObject.w_STABEN="C"
          this.w_MESS = "Attenzione:%0Il cespite risulta dismesso"
          ah_ErrorMsg(this.w_MESS,"!","")
          this.oParentObject.w_MCCODCES = SPACE(20)
          this.oParentObject.w_DESCES = SPACE(40)
          i_retcode = 'stop'
          return
        case this.oParentObject.w_STABEN="E"
          this.w_MESS = "Attenzione:%0Il cespite risulta eliminato"
          ah_ErrorMsg(this.w_MESS,"!","")
          this.oParentObject.w_MCCODCES = SPACE(20)
          this.oParentObject.w_DESCES = SPACE(40)
          i_retcode = 'stop'
          return
      endcase
    endif
    this.w_DATFIN = this.oParentObject.w_FINESE
    if EMPTY(this.oParentObject.w_MCCODCES) OR EMPTY(this.oParentObject.w_MCCOMPET)
      i_retcode = 'stop'
      return
    endif
    this.oParentObject.w_IMPV01 = 0
    this.oParentObject.w_IMPV07 = 0
    this.oParentObject.w_IMPV02 = 0
    this.oParentObject.w_IMPV08 = 0
    this.oParentObject.w_IMPV03 = 0
    this.oParentObject.w_IMPV09 = 0
    this.oParentObject.w_IMPV04 = 0
    this.oParentObject.w_IMPV10 = 0
    this.oParentObject.w_IMPV05 = 0
    this.oParentObject.w_IMPV11 = 100
    this.oParentObject.w_IMPV06 = 0
    this.oParentObject.w_IMPV12 = 0
    this.oParentObject.w_IMPV13 = 0
    this.oParentObject.w_IMPV14 = 0
    this.oParentObject.w_IMPRIV = 0
    this.oParentObject.w_IMPV16 = 0
    this.oParentObject.w_IMPV17 = 0
    * --- Valori letti solo nel caso di caricamento dei dati dalla movimentazione cespiti
    if this.w_Padre="M"
      this.oParentObject.w_IMQTACES = 0
      * --- legge i valori dell'ammortamento particolare del cespite per l'esercizio in corso
      this.w_CEPROESE = CALCPROCES(this.oParentObject.w_CEESPRIU,this.oParentObject.w_MCCOMPET)
      this.w_CEPROESC = CALCPROCES(this.oParentObject.w_CEESPRIC,this.oParentObject.w_MCCOMPET)
      * --- Read from AMM_CESP
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.AMM_CESP_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AMM_CESP_idx,2],.t.,this.AMM_CESP_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "AMPERORD,AMPERANT,AMQUOFIS,AMQUOFI1,AMTIPAMM"+;
          " from "+i_cTable+" AMM_CESP where ";
              +"AMCODCES = "+cp_ToStrODBC(this.oParentObject.w_MCCODCES);
              +" and AMPROESE = "+cp_ToStrODBC(this.w_CEPROESE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          AMPERORD,AMPERANT,AMQUOFIS,AMQUOFI1,AMTIPAMM;
          from (i_cTable) where;
              AMCODCES = this.oParentObject.w_MCCODCES;
              and AMPROESE = this.w_CEPROESE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.oParentObject.w_COEFI1 = NVL(cp_ToDate(_read_.AMPERORD),cp_NullValue(_read_.AMPERORD))
        this.oParentObject.w_COEFI2 = NVL(cp_ToDate(_read_.AMPERANT),cp_NullValue(_read_.AMPERANT))
        this.oParentObject.w_QUOFIS = NVL(cp_ToDate(_read_.AMQUOFIS),cp_NullValue(_read_.AMQUOFIS))
        this.oParentObject.w_QUOFI1 = NVL(cp_ToDate(_read_.AMQUOFI1),cp_NullValue(_read_.AMQUOFI1))
        this.oParentObject.w_AMTIPAMM = NVL(cp_ToDate(_read_.AMTIPAMM),cp_NullValue(_read_.AMTIPAMM))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if i_rows<>0
        this.oParentObject.w_CODESF = CALCESCES(this.oParentObject.w_CEESPRIU,this.w_CEPROESE)
      endif
      * --- Read from AMC_CESP
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.AMC_CESP_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AMC_CESP_idx,2],.t.,this.AMC_CESP_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ACTIPAMM,ACPERORD,ACQUOFIS"+;
          " from "+i_cTable+" AMC_CESP where ";
              +"ACCODCES = "+cp_ToStrODBC(this.oParentObject.w_MCCODCES);
              +" and ACPROESE = "+cp_ToStrODBC(this.w_CEPROESC);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ACTIPAMM,ACPERORD,ACQUOFIS;
          from (i_cTable) where;
              ACCODCES = this.oParentObject.w_MCCODCES;
              and ACPROESE = this.w_CEPROESC;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.oParentObject.w_CEFLAMCI = NVL(cp_ToDate(_read_.ACTIPAMM),cp_NullValue(_read_.ACTIPAMM))
        this.oParentObject.w_COECIV = NVL(cp_ToDate(_read_.ACPERORD),cp_NullValue(_read_.ACPERORD))
        this.oParentObject.w_QUOCIV = NVL(cp_ToDate(_read_.ACQUOFIS),cp_NullValue(_read_.ACQUOFIS))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if i_rows<>0
        this.oParentObject.w_CODESC = CALCESCES(this.oParentObject.w_CEESPRIC,this.w_CEPROESC)
      endif
    endif
    * --- Eccezioni lette sul Cespite
    this.oParentObject.w_TCOECIV = iif(this.oParentObject.w_Okcivile=.F.,this.oParentObject.w_Coeciv,this.oParentObject.w_Tcoeciv)
    this.oParentObject.w_TCOEFI1 = iif(this.oParentObject.w_Okfisord=.F.,this.oParentObject.w_Coefi1,this.oParentObject.w_Tcoefi1)
    this.oParentObject.w_TCOEFI2 = iif(this.oParentObject.w_Okfisant=.F.,this.oParentObject.w_Coefi2,this.oParentObject.w_Tcoefi2)
    * --- Utilizzata per notificare che il cespite � gi� ammortizzato nell'esercizio corrente.
    this.w_APPO = 0
    this.w_QUOTEPER = 0
    * --- Calcola dai Saldi sino all'Esercizio di Competenza
    * --- Select from GSCE_BCV
    do vq_exec with 'GSCE_BCV',this,'_Curs_GSCE_BCV','',.f.,.t.
    if used('_Curs_GSCE_BCV')
      select _Curs_GSCE_BCV
      locate for 1=1
      do while not(eof())
      this.w_APPO1 = NVL(_Curs_GSCE_BCV.ESVALNAZ, this.oParentObject.w_MCVALNAZ)
      this.w_SCINCVAL = NVL(_Curs_GSCE_BCV.SCINCVAL, 0)
      this.w_SCIMPONS = NVL(_Curs_GSCE_BCV.SCIMPONS, 0)
      this.w_SCINCVAC = NVL(_Curs_GSCE_BCV.SCINCVAC, 0)
      this.w_SCIMPONC = NVL(_Curs_GSCE_BCV.SCIMPONC, 0)
      this.w_SCIMPRIV = NVL(_Curs_GSCE_BCV.SCIMPRIV, 0)
      this.w_SCDECVAL = NVL(_Curs_GSCE_BCV.SCDECVAL, 0)
      this.w_SCDECVAC = NVL(_Curs_GSCE_BCV.SCDECVAC, 0)
      this.w_SCIMPSVA = NVL(_Curs_GSCE_BCV.SCIMPSVA, 0)
      this.w_SCACCCIV = NVL(_Curs_GSCE_BCV.SCACCCIV, 0)
      this.w_SCUTICIV = NVL(_Curs_GSCE_BCV.SCUTICIV, 0)
      this.w_SCIMPNOA = NVL(_Curs_GSCE_BCV.SCIMPNOA, 0)
      this.w_SCACCFIS = NVL(_Curs_GSCE_BCV.SCACCFIS, 0)
      this.w_SCUTIFIS = NVL(_Curs_GSCE_BCV.SCUTIFIS, 0)
      this.w_SCACCANT = NVL(_Curs_GSCE_BCV.SCACCANT, 0)
      this.w_SCUTIANT = NVL(_Curs_GSCE_BCV.SCUTIANT, 0)
      this.w_SCQUOPER = NVL(_Curs_GSCE_BCV.SCQUOPER, 0)
      this.w_SCQTACES = NVL(_Curs_GSCE_BCV.SCQTACES, 0)
      this.w_SCIMPRIC = NVL(_Curs_GSCE_BCV.SCIMPRIC, 0)
      this.w_SCIMPSVC = NVL(_Curs_GSCE_BCV.SCIMPSVC, 0)
      this.oParentObject.w_IMPV01 = this.oParentObject.w_IMPV01 + ((this.w_SCINCVAL+this.w_SCIMPONS+this.w_SCIMPRIV)-(this.w_SCDECVAL+this.w_SCIMPSVA))
      this.oParentObject.w_IMPV16 = this.oParentObject.w_IMPV16 + ((this.w_SCINCVAC+this.w_SCIMPONC+this.w_SCIMPRIC)-(this.w_SCDECVAC+this.w_SCIMPSVC))
      this.oParentObject.w_IMPV03 = this.oParentObject.w_IMPV03 + (this.w_SCACCCIV-this.w_SCUTICIV)
      this.oParentObject.w_IMPV05 = this.oParentObject.w_IMPV05 + this.w_SCIMPNOA
      this.oParentObject.w_IMPV06 = this.oParentObject.w_IMPV06 + (this.w_SCACCFIS-this.w_SCUTIFIS)
      this.oParentObject.w_IMPV14 = this.oParentObject.w_IMPV14 + (this.w_SCACCANT-this.w_SCUTIANT)
      this.w_APPO = this.w_APPO + this.w_SCQUOPER
      this.w_QUOTEPER = this.w_QUOTEPER+this.w_SCQUOPER
      this.oParentObject.w_IMPRIV = this.oParentObject.w_IMPRIV + this.w_SCIMPRIV
      if this.w_Padre="M"
        this.oParentObject.w_IMQTACES = this.oParentObject.w_IMQTACES+this.w_SCQTACES
      endif
        select _Curs_GSCE_BCV
        continue
      enddo
      use
    endif
    this.oParentObject.w_IMPV04 = this.oParentObject.w_IMPV16 - this.oParentObject.w_IMPV03
    this.oParentObject.w_IMPV07 = this.oParentObject.w_IMPV01 - (this.oParentObject.w_IMPV05 + this.oParentObject.w_IMPV06 +this.oParentObject.w_IMPV14+ this.w_APPO)
    * --- Dati dalla Categoria Cespite
    this.w_CCFLAMCI = " "
    this.w_CCCOECIV = 0
    this.w_CCCOEFIS = 0
    this.w_PROESE = 1
    this.w_DURCES = 0
    * --- Read from CAT_CESP
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CAT_CESP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAT_CESP_idx,2],.t.,this.CAT_CESP_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CCCOEFIS,CCPERDEF,CCIMPMAX,CCVALLIM,CCDURCES,CCCOECIV"+;
        " from "+i_cTable+" CAT_CESP where ";
            +"CCCODICE = "+cp_ToStrODBC(this.oParentObject.w_CODCAT);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CCCOEFIS,CCPERDEF,CCIMPMAX,CCVALLIM,CCDURCES,CCCOECIV;
        from (i_cTable) where;
            CCCODICE = this.oParentObject.w_CODCAT;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CCCOEFIS = NVL(cp_ToDate(_read_.CCCOEFIS),cp_NullValue(_read_.CCCOEFIS))
      this.oParentObject.w_IMPV11 = NVL(cp_ToDate(_read_.CCPERDEF),cp_NullValue(_read_.CCPERDEF))
      this.oParentObject.w_IMPV12 = NVL(cp_ToDate(_read_.CCIMPMAX),cp_NullValue(_read_.CCIMPMAX))
      this.w_VALLIM = NVL(cp_ToDate(_read_.CCVALLIM),cp_NullValue(_read_.CCVALLIM))
      this.w_DURCES = NVL(cp_ToDate(_read_.CCDURCES),cp_NullValue(_read_.CCDURCES))
      this.w_CCCOECIV = NVL(cp_ToDate(_read_.CCCOECIV),cp_NullValue(_read_.CCCOECIV))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Se Eccezione: % Deducibilita', Limite Massimo, Valuta di riferimento dell'Importo Massimo Accantonabile letta dal Cespite
    * --- Se presenti, legge i valori dell'ammortamento particolare del cespite per l'esercizio in corso
    if this.oParentObject.w_TIPAMM<>"C" AND this.oParentObject.w_PERDEF<>0
      this.oParentObject.w_IMPV11 = this.oParentObject.w_PERDEF
      this.oParentObject.w_IMPV12 = this.oParentObject.w_IMPMAX
      this.w_VALLIM = IIF(NOT EMPTY(this.oParentObject.w_CEVALLIM),this.oParentObject.w_CEVALLIM,this.w_VALLIM)
    endif
    * --- Se Eccezione: Primo Esercizio e Percentuale di Ammortamento Civilistico Diverso letto dal Cespite
    if this.oParentObject.w_TIPAMM<>"F" AND this.oParentObject.w_CODESE=this.oParentObject.w_MCCOMPET
      this.w_DURCES = IIF(this.oParentObject.w_CEDURCES<>0, this.oParentObject.w_CEDURCES,this.w_DURCES)
    endif
    if EMPTY(NVL(this.w_VALLIM,""))
      this.w_VALLIM = g_perval
    endif
    if this.oParentObject.w_IMPV12<>0 AND this.oParentObject.w_MCVALNAZ<>this.w_VALLIM 
      this.oParentObject.w_IMPV12 = cp_ROUND(VALCAM(this.oParentObject.w_IMPV12, this.w_VALLIM, this.oParentObject.w_MCVALNAZ, this.oParentObject.w_FINESE, 0), this.oParentObject.w_DECTOT)
    endif
    * --- Effettuo una lettura sulla tabella ESERCIZI  per contare il numero 
    *     di esercizio che intercorrono tra la data di entrata in funzione del bene 
    *     e la data nella quale si effettua l'ammortamento.
    this.w_GIORNI = 0
    * --- fiscale
    this.w_PROESE = 0
    this.w_PROESE = CALCPROCES(this.oParentObject.w_CEESPRIU,this.oParentObject.w_MCCOMPET)
    * --- civile
    this.w_PROESC = 0
    this.w_PROESC = CALCPROCES(this.oParentObject.w_CEESPRIC,this.oParentObject.w_MCCOMPET)
    * --- QUOFISOK controlla se esiste una quota fissa per il civile.
    *     In questo caso non deve fare gli altri calcoli e deve considerare solo la quota fissa
    this.w_QUOFISOK = .F.
    * --- Leggo dai parametri cespiti il limite minimo % amm. ordinario per il calcolo delle Quote Perse
    * --- Leggo anche il campo contenente il Numero Ammortamenti Anticipati per Beni Usati
    *     e il numero max ammortamenti anticipati e la percentuale di tolleranza per l'ultimo anno di ammortamento civile
    * --- Read from PAR_CESP
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_CESP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_CESP_idx,2],.t.,this.PAR_CESP_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PCAMMABU,PCPERRID,PCTIPAMM,PCMAXANT,PCPERTOL"+;
        " from "+i_cTable+" PAR_CESP where ";
            +"PCCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PCAMMABU,PCPERRID,PCTIPAMM,PCMAXANT,PCPERTOL;
        from (i_cTable) where;
            PCCODAZI = this.oParentObject.w_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_AMMABU = NVL(cp_ToDate(_read_.PCAMMABU),cp_NullValue(_read_.PCAMMABU))
      this.w_PERRID = NVL(cp_ToDate(_read_.PCPERRID),cp_NullValue(_read_.PCPERRID))
      this.w_PCTIPAMM = NVL(cp_ToDate(_read_.PCTIPAMM),cp_NullValue(_read_.PCTIPAMM))
      this.w_MAXANT = NVL(cp_ToDate(_read_.PCMAXANT),cp_NullValue(_read_.PCMAXANT))
      this.w_PERTOL = NVL(cp_ToDate(_read_.PCPERTOL),cp_NullValue(_read_.PCPERTOL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if this.w_PCTIPAMM$"EC" 
      * --- V08 - Quota Accantonamento Civile
      if this.oParentObject.w_Ccnsoamm<>"S" And this.oParentObject.w_Censoamm<>"S"
        if this.oParentObject.w_AMMIMC="S"
          this.oParentObject.w_IMPV08 = this.oParentObject.w_IMPV04
          this.oParentObject.w_TCOECIV = 100
        else
          * --- Effettuo una lettura nella tabella Esercizi/Ammortamento civile.
          *     In base all'esercizio in cui mi trovo leggo la relativa percentuale e la tipologia
          *     di ammortamento.
          * --- Read from CAT_AMMC
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CAT_AMMC_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAT_AMMC_idx,2],.t.,this.CAT_AMMC_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ECTIPAMM,ECPERORD"+;
              " from "+i_cTable+" CAT_AMMC where ";
                  +"ECCODCAT = "+cp_ToStrODBC(this.oParentObject.w_CODCAT);
                  +" and ECPROESE = "+cp_ToStrODBC(this.w_PROESC);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ECTIPAMM,ECPERORD;
              from (i_cTable) where;
                  ECCODCAT = this.oParentObject.w_CODCAT;
                  and ECPROESE = this.w_PROESC;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CCFLAMCI = NVL(cp_ToDate(_read_.ECTIPAMM),cp_NullValue(_read_.ECTIPAMM))
            this.w_ECPERORD = NVL(cp_ToDate(_read_.ECPERORD),cp_NullValue(_read_.ECPERORD))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_CCFLAMCI = IIF(EMPTY(Nvl(this.w_CCFLAMCI,"")),"I",this.w_CCFLAMCI)
          if this.oParentObject.w_TIPAMM<>"F" AND this.oParentObject.w_CODESC=this.oParentObject.w_MCCOMPET
            this.w_CCFLAMCI = this.oParentObject.w_CEFLAMCI
            * --- Prende le Eccezioni dalla Scheda del Cespite
            if this.oParentObject.w_QUOCIV<>0 and this.oParentObject.w_Okcivile=.F.
              * --- Quota Fissa (se Esiste)
              this.oParentObject.w_IMPV08 = this.oParentObject.w_QUOCIV
              this.w_QUOFISOK = .T.
            else
              this.oParentObject.w_IMPV08 = cp_ROUND((this.oParentObject.w_TCOECIV * this.oParentObject.w_IMPV16) / 100, this.oParentObject.w_DECTOT)
            endif
          else
            if this.oParentObject.w_Okcivile=.F.
              * --- calcola dalla Categoria...
              * --- w_CCCOECIV:Coefficiente Civilistico Categorie Cespiti.
              this.oParentObject.w_TCOECIV = IIF(this.w_ECPERORD<>0,this.w_ECPERORD,this.w_CCCOECIV)
            endif
          endif
          * --- Se non esiste quota fissa vado a considerare gli altri metodi di calcolo
          if Not this.w_QUOFISOK
            if this.w_PROESC= 1
              * --- Primo Esercizio di Utilizzo
              do case
                case this.w_CCFLAMCI $ "ID"
                  * --- Intero
                  this.oParentObject.w_IMPV08 = cp_ROUND((this.oParentObject.w_TCOECIV * this.oParentObject.w_IMPV16) / 100, this.oParentObject.w_DECTOT)
                case this.w_CCFLAMCI="5"
                  * --- Nel caso di amm.to specificato nel piano devo applicare la riduzione per il 
                  *     primo esercizio
                  if this.oParentObject.w_Okcivile Or (this.oParentObject.w_Tipamm<>"F" And this.oParentObject.w_Codesc=this.oParentObject.w_Mccompet)
                    this.oParentObject.w_TCOECIV = cp_ROUND(this.oParentObject.w_TCOECIV/2, 2)
                  endif
                  * --- 50%
                  this.oParentObject.w_IMPV08 = cp_ROUND((this.oParentObject.w_TCOECIV * this.oParentObject.w_IMPV16)/100 , this.oParentObject.w_DECTOT)
                case this.w_CCFLAMCI="R"
                  this.w_GIORNI = CP_TODATE(this.oParentObject.w_FINESE)-CP_TODATE(this.oParentObject.w_INIESE)+1
                  if this.w_GIORNI>=365
                    * --- Rateo Esercizio (riferito a w_MCCOMPET)
                    this.w_APPO = ((this.oParentObject.w_FINESE+1) - this.oParentObject.w_DTPRIC) / ((this.oParentObject.w_FINESE+1) - this.oParentObject.w_INIESE)
                  else
                    this.w_FINESPRE = this.oParentObject.w_INIESE-1
                    * --- Leggo la data iniziale dell'esercizio precedente
                    * --- Read from ESERCIZI
                    i_nOldArea=select()
                    if used('_read_')
                      select _read_
                      use
                    endif
                    i_nConn=i_TableProp[this.ESERCIZI_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2],.t.,this.ESERCIZI_idx)
                    if i_nConn<>0
                      cp_sqlexec(i_nConn,"select "+;
                        "ESINIESE"+;
                        " from "+i_cTable+" ESERCIZI where ";
                            +"ESCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
                            +" and ESFINESE = "+cp_ToStrODBC(this.w_FINESPRE);
                             ,"_read_")
                      i_Rows=iif(used('_read_'),reccount(),0)
                    else
                      select;
                        ESINIESE;
                        from (i_cTable) where;
                            ESCODAZI = this.oParentObject.w_CODAZI;
                            and ESFINESE = this.w_FINESPRE;
                         into cursor _read_
                      i_Rows=_tally
                    endif
                    if used('_read_')
                      locate for 1=1
                      this.w_INESPRE = NVL(cp_ToDate(_read_.ESINIESE),cp_NullValue(_read_.ESINIESE))
                      use
                    else
                      * --- Error: sql sentence error.
                      i_Error = MSG_READ_ERROR
                      return
                    endif
                    select (i_nOldArea)
                    this.w_GIORNI = CP_TODATE(this.oParentObject.w_FINESE)-CP_TODATE(this.w_INESPRE)+1
                    if this.w_GIORNI=365
                      this.w_APPO = ((this.oParentObject.w_FINESE+1) - this.oParentObject.w_DTPRIC) / ((this.oParentObject.w_FINESE+1) - this.w_INESPRE)
                    else
                      this.w_APPO = ((this.oParentObject.w_FINESE+1) - this.oParentObject.w_DTPRIC) / ((this.oParentObject.w_FINESE+1) - this.oParentObject.w_INIESE)
                    endif
                  endif
                  this.oParentObject.w_IMPV08 = cp_ROUND(this.w_APPO * ((this.oParentObject.w_TCOECIV * this.oParentObject.w_IMPV16) / 100), this.oParentObject.w_DECTOT)
              endcase
            else
              * --- Anche se � stato superato l'ultimo esercizio nella categoria, se esiste un residuo civile da ammortizzare, continua l'ammortamento anche negli esercizi successivi
              * --- Se non  primo Esercizio di utilizzo:Coefficiente Civilistico Categorie Cespiti.
              if this.oParentObject.w_Okcivile=.F. and this.w_PROESC>=this.w_DURCES
                this.oParentObject.w_IMPV08 = this.oParentObject.w_IMPV16-this.oParentObject.w_IMPV03
                if this.oParentObject.w_IMPV16<>0 
                  * --- effettuo la divisione  solo se w_impv16 � diverso da 0 
                  this.w_COEFFCIV = cp_ROUND((100*this.oParentObject.w_IMPV08)/this.oParentObject.w_IMPV16,2)
                endif
                if this.w_COEFFCIV>=this.oParentObject.w_TCOECIV AND (this.w_COEFFCIV - this.oParentObject.w_TCOECIV) >this.w_PERTOL
                  * --- Se la percentuale ricavata dal residuo da ammortizzare � superiore a quella della categoria 
                  *     e la differenza fra le due percentuali supera la percentuale di tolleranza impostata nei parametri
                  *     calcola l'ammortamento sulla percentuale della categoria 
                  this.oParentObject.w_IMPV08 = cp_ROUND((this.oParentObject.w_TCOECIV * this.oParentObject.w_IMPV16) / 100, this.oParentObject.w_DECTOT)
                else
                  this.oParentObject.w_TCOECIV = this.w_COEFFCIV
                endif
              else
                this.oParentObject.w_IMPV08 = cp_ROUND((this.oParentObject.w_TCOECIV * this.oParentObject.w_IMPV16) / 100, this.oParentObject.w_DECTOT)
              endif
            endif
          endif
        endif
        * --- Controllo che il bene sia in uso altrimenti non calcola l'accantonamento civile
        * --- Verifico che il valore calcolato non superi il Residuo da Amm.Civile
        * --- In tal Caso prende il Valore di Quest'Ultimo (V04)
        if this.oParentObject.w_AMMIMC<>"S"
          if this.oParentObject.w_STABEN="U" 
            * --- Calcolo l'importo dell'Accantonamento Civile solo per il periodo specificato.
            this.oParentObject.w_IMPV08 = IIF(this.oParentObject.w_IMPV08 > this.oParentObject.w_IMPV04, this.oParentObject.w_IMPV04, this.oParentObject.w_IMPV08)
          else
            this.oParentObject.w_IMPV08 = 0
          endif
        endif
        * --- Verifico se il residuo da ammortamento civile � diverso da 0
        if this.oParentObject.w_IMPV04=0
          this.oParentObject.w_DaGenera = .F.
        endif
      else
        this.oParentObject.w_IMPV08 = 0
        this.oParentObject.w_TCOECIV = 0
      endif
    else
      this.oParentObject.w_IMPV03 = 0
      this.oParentObject.w_IMPV04 = 0
      this.oParentObject.w_IMPV08 = 0
      this.oParentObject.w_TCOECIV = 0
    endif
    * --- V09 - Quota Accantonamento Fiscale
    if this.w_PCTIPAMM$"EF" 
      if Not empty(this.oParentObject.w_Dtpriu) And (this.oParentObject.w_Dtpriu<=this.w_Datfin) And this.oParentObject.w_Ccnsoamf<>"S" And this.oParentObject.w_Censoamf<>"S"
        * --- V09 - Quota Accantonamento Fiscale
        this.w_PERC09 = 0
        this.w_PERC15 = 0
        this.w_EATIPAMM = " "
        this.w_EAPERORD = 0
        this.w_EAPERANT = 0
        * --- Prende il Riferimento Per il Calcolo (Usato  per le Quote perse)
        this.w_RIFCAL = this.oParentObject.w_IMPV01
        * --- Prende il Riferimento Per il Calcolo (Usato  per Importo non ammortizzabile)
        this.w_RIFCAL1 = (this.oParentObject.w_IMPV01 - this.oParentObject.w_IMPV05)
        if this.oParentObject.w_AMMIMM="S"
          this.oParentObject.w_IMPV09 = this.oParentObject.w_IMPV07
          this.oParentObject.w_IMPV13 = 0
          this.oParentObject.w_TCOEFI1 = 100
          this.oParentObject.w_TCOEFI2 = 0
        else
          if this.oParentObject.w_TIPAMM<>"C" AND this.oParentObject.w_CODESF=this.oParentObject.w_MCCOMPET
            * --- Prende le Eccezioni dalla Scheda del Cespite
            if this.oParentObject.w_QUOFIS<>0
              * --- Quota Fissa (se Esiste)
              this.oParentObject.w_IMPV09 = iif(this.oParentObject.w_Okfisord=.F.,this.oParentObject.w_QUOFIS,0)
              if this.w_PROESE<=this.w_MAXANT
                * --- Controlla se l'anno progressivo di ammortamento non � maggiore del numero massimo consentito
                *     per gli ammortamenti anticipati 
                this.oParentObject.w_IMPV13 = iif(this.oParentObject.w_Okfisant=.F.,this.oParentObject.w_QUOFI1,0)
              else
                this.oParentObject.w_IMPV13 = 0
              endif
              this.w_PERC09 = iif(this.oParentObject.w_Okfisord=.F.,cp_ROUND((this.oParentObject.w_IMPV09 * 100) / this.w_RIFCAL1, 2),this.oParentObject.w_Tcoefi1)
              * --- Calcola la % da utilizzare in gestione delle Quote Fisse.Utilizzata nel calcolo delle quote perse.
              this.w_PERQUOTE = iif(this.oParentObject.w_Okfisord=.F.,cp_ROUND((this.oParentObject.w_IMPV09 * 100) / this.w_RIFCAL1, 2),this.oParentObject.w_Tcoefi1)
            else
              this.w_PERQUOTE = iif(this.oParentObject.w_Okfisord=.F.,this.oParentObject.w_Coefi1,this.oParentObject.w_Tcoefi1)
              this.w_PERC09 = iif(this.oParentObject.w_Okfisord=.F.,this.oParentObject.w_Coefi1,this.oParentObject.w_Tcoefi1)
              * --- Imposto APPO a 1 altrimenti nel caso in cui non � rateo utilizzo moltiplica per 0
              this.w_APPO = 1
              if this.w_PROESE<=this.w_MAXANT 
                if this.w_PROESE = 1 And this.oParentObject.w_AMTIPAMM = "R"
                  this.w_GIORNI = CP_TODATE(this.oParentObject.w_FINESE)-CP_TODATE(this.oParentObject.w_INIESE)+1
                  if this.w_GIORNI>=365
                    * --- Rateo Esercizio (riferito a w_MCCOMPET)
                    this.w_APPO = ((this.oParentObject.w_FINESE+1) - this.oParentObject.w_DTPRIU) / ((this.oParentObject.w_FINESE+1) - this.oParentObject.w_INIESE)
                  else
                    this.w_FINESPRE = this.oParentObject.w_INIESE-1
                    * --- Leggo la data iniziale dell'esercizio precedente
                    * --- Read from ESERCIZI
                    i_nOldArea=select()
                    if used('_read_')
                      select _read_
                      use
                    endif
                    i_nConn=i_TableProp[this.ESERCIZI_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2],.t.,this.ESERCIZI_idx)
                    if i_nConn<>0
                      cp_sqlexec(i_nConn,"select "+;
                        "ESINIESE"+;
                        " from "+i_cTable+" ESERCIZI where ";
                            +"ESCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
                            +" and ESFINESE = "+cp_ToStrODBC(this.w_FINESPRE);
                             ,"_read_")
                      i_Rows=iif(used('_read_'),reccount(),0)
                    else
                      select;
                        ESINIESE;
                        from (i_cTable) where;
                            ESCODAZI = this.oParentObject.w_CODAZI;
                            and ESFINESE = this.w_FINESPRE;
                         into cursor _read_
                      i_Rows=_tally
                    endif
                    if used('_read_')
                      locate for 1=1
                      this.w_INESPRE = NVL(cp_ToDate(_read_.ESINIESE),cp_NullValue(_read_.ESINIESE))
                      use
                    else
                      * --- Error: sql sentence error.
                      i_Error = MSG_READ_ERROR
                      return
                    endif
                    select (i_nOldArea)
                    this.w_GIORNI = CP_TODATE(this.oParentObject.w_FINESE)-CP_TODATE(this.w_INESPRE)+1
                    if this.w_GIORNI=365
                      this.w_APPO = ((this.oParentObject.w_FINESE+1) - this.oParentObject.w_DTPRIU) / ((this.oParentObject.w_FINESE+1) - this.w_INESPRE)
                    else
                      this.w_APPO = ((this.oParentObject.w_FINESE+1) - this.oParentObject.w_DTPRIU) / ((this.oParentObject.w_FINESE+1) - this.oParentObject.w_INIESE)
                    endif
                  endif
                  * --- -se reteo utilizzo non considero l'anticipato
                  this.w_PERC15 = 0
                else
                  * --- Controlla se l'anno progressivo di ammortamento non � maggiore del numero massimo consentito
                  *     per gli ammortamenti anticipati 
                  this.w_PERC15 = iif(this.oParentObject.w_Okfisant=.F., this.oParentObject.w_Coefi2, this.oParentObject.w_Tcoefi2)
                endif
              else
                this.w_PERC15 = 0
              endif
              this.oParentObject.w_IMPV09 = cp_ROUND(this.w_APPO * (this.w_PERC09 * this.w_RIFCAL1) / 100, this.oParentObject.w_DECTOT)
              this.oParentObject.w_IMPV13 = cp_ROUND((this.w_PERC15 * this.w_RIFCAL1) / 100, this.oParentObject.w_DECTOT)
            endif
          else
            * --- calcola dalla Categoria...
            * --- Legge il record Corrispondente all'esercizio da calcolare
            * --- Read from CAT_AMMO
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.CAT_AMMO_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CAT_AMMO_idx,2],.t.,this.CAT_AMMO_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "EATIPAMM,EAPERORD,EAPERANT"+;
                " from "+i_cTable+" CAT_AMMO where ";
                    +"EACODCAT = "+cp_ToStrODBC(this.oParentObject.w_CODCAT);
                    +" and EAPROESE = "+cp_ToStrODBC(this.w_PROESE);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                EATIPAMM,EAPERORD,EAPERANT;
                from (i_cTable) where;
                    EACODCAT = this.oParentObject.w_CODCAT;
                    and EAPROESE = this.w_PROESE;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_EATIPAMM = NVL(cp_ToDate(_read_.EATIPAMM),cp_NullValue(_read_.EATIPAMM))
              this.w_EAPERORD = NVL(cp_ToDate(_read_.EAPERORD),cp_NullValue(_read_.EAPERORD))
              this.w_EAPERANT = NVL(cp_ToDate(_read_.EAPERANT),cp_NullValue(_read_.EAPERANT))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.w_PERQUOTE = iif(this.oParentObject.w_Okfisord=.F.,this.w_Eaperord,this.oParentObject.w_Tcoefi1)
            this.w_PERC09 = iif(this.oParentObject.w_okfisord=.F.,this.w_EAPERORD,this.oParentObject.w_Tcoefi1)
            * --- Imposto APPO a 1 altrimenti nel caso in cui non � rateo utilizzo moltiplica per 0
            this.w_APPO = 1
            if this.w_PROESE<=this.w_MAXANT
              if this.w_PROESE = 1 And this.w_EATIPAMM = "R"
                this.w_GIORNI = CP_TODATE(this.oParentObject.w_FINESE)-CP_TODATE(this.oParentObject.w_INIESE)+1
                if this.w_GIORNI>=365
                  * --- Rateo Esercizio (riferito a w_MCCOMPET)
                  this.w_APPO = ((this.oParentObject.w_FINESE+1) - this.oParentObject.w_DTPRIU) / ((this.oParentObject.w_FINESE+1) - this.oParentObject.w_INIESE)
                else
                  this.w_FINESPRE = this.oParentObject.w_INIESE-1
                  * --- Leggo la data iniziale dell'esercizio precedente
                  * --- Read from ESERCIZI
                  i_nOldArea=select()
                  if used('_read_')
                    select _read_
                    use
                  endif
                  i_nConn=i_TableProp[this.ESERCIZI_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2],.t.,this.ESERCIZI_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select "+;
                      "ESINIESE"+;
                      " from "+i_cTable+" ESERCIZI where ";
                          +"ESCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
                          +" and ESFINESE = "+cp_ToStrODBC(this.w_FINESPRE);
                           ,"_read_")
                    i_Rows=iif(used('_read_'),reccount(),0)
                  else
                    select;
                      ESINIESE;
                      from (i_cTable) where;
                          ESCODAZI = this.oParentObject.w_CODAZI;
                          and ESFINESE = this.w_FINESPRE;
                       into cursor _read_
                    i_Rows=_tally
                  endif
                  if used('_read_')
                    locate for 1=1
                    this.w_INESPRE = NVL(cp_ToDate(_read_.ESINIESE),cp_NullValue(_read_.ESINIESE))
                    use
                  else
                    * --- Error: sql sentence error.
                    i_Error = MSG_READ_ERROR
                    return
                  endif
                  select (i_nOldArea)
                  this.w_GIORNI = CP_TODATE(this.oParentObject.w_FINESE)-CP_TODATE(this.w_INESPRE)+1
                  if this.w_GIORNI=365
                    this.w_APPO = ((this.oParentObject.w_FINESE+1) - this.oParentObject.w_DTPRIU) / ((this.oParentObject.w_FINESE+1) - this.w_INESPRE)
                  else
                    this.w_APPO = ((this.oParentObject.w_FINESE+1) - this.oParentObject.w_DTPRIU) / ((this.oParentObject.w_FINESE+1) - this.oParentObject.w_INIESE)
                  endif
                endif
                * --- se rateo utilizzo non considero ammortamento anticipato
                this.w_PERC15 = 0
                this.oParentObject.w_TCOEFI2 = 0
              else
                * --- Controlla se l'anno progressivo di ammortamento non � maggiore del numero massimo consentito
                *     per gli ammortamenti anticipati 
                this.w_PERC15 = iif(this.oParentObject.w_Okfisant=.F., IIF(this.w_EATIPAMM="A" AND (this.oParentObject.w_FLCEUS<>"S" OR this.w_PROESE<=this.w_AMMABU), this.w_EAPERANT, 0),this.oParentObject.w_Tcoefi2)
                this.oParentObject.w_TCOEFI2 = iif(this.oParentObject.w_Okfisant=.F., IIF(this.w_EATIPAMM="A" AND (this.oParentObject.w_FLCEUS<>"S" OR this.w_PROESE<=this.w_AMMABU), this.w_EAPERANT, 0),this.oParentObject.w_Tcoefi2)
              endif
            else
              this.w_PERC15 = 0
              this.oParentObject.w_TCOEFI2 = 0
            endif
            this.oParentObject.w_IMPV09 = cp_ROUND(this.w_APPO * (this.w_PERC09 * this.w_RIFCAL1) / 100, this.oParentObject.w_DECTOT)
            this.oParentObject.w_IMPV13 = cp_ROUND((this.w_PERC15 * this.w_RIFCAL1) / 100, this.oParentObject.w_DECTOT)
            this.oParentObject.w_TCOEFI1 = iif(this.oParentObject.w_okfisord=.F.,this.w_EAPERORD,this.oParentObject.w_Tcoefi1)
            * --- Calcolo la durata massima per gli accantonamenti fiscali.
            *     Devo prendere il valore massimo della tabella Categoria - Ammortamenti.
            * --- Select from CAT_AMMO
            i_nConn=i_TableProp[this.CAT_AMMO_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CAT_AMMO_idx,2],.t.,this.CAT_AMMO_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select Max(EAPROESE) as EAPROESE  from "+i_cTable+" CAT_AMMO ";
                  +" where EACODCAT="+cp_ToStrODBC(this.oParentObject.w_CODCAT)+"";
                  +" group by EACODCAT";
                   ,"_Curs_CAT_AMMO")
            else
              select Max(EAPROESE) as EAPROESE from (i_cTable);
               where EACODCAT=this.oParentObject.w_CODCAT;
               group by EACODCAT;
                into cursor _Curs_CAT_AMMO
            endif
            if used('_Curs_CAT_AMMO')
              select _Curs_CAT_AMMO
              locate for 1=1
              do while not(eof())
              this.w_DURCESFIS = EAPROESE
                select _Curs_CAT_AMMO
                continue
              enddo
              use
            endif
            * --- Se sono nell'ultimo esercizio e non ho specificato un ammortamento particolare
            *     ed il coefficiente anticipato � uguale a zero verifico se il residuo da 
            *     ammortizzare � maggiore della quota di accantonamento 
            *     calcolata, se la condizione � verificata dobbiamo utilizzare 
            *     come coefficiente di ammortamento  quello specificato nel campo coefficiente ammortamento ordinario
            if this.w_Durcesfis=this.w_Proese And this.oParentObject.w_Okfisord=.F. And this.w_PERC15=0 And (this.oParentObject.w_IMPV07 - this.oParentObject.w_IMPV09 >0 )
              this.oParentObject.w_IMPV09 = cp_ROUND((this.w_CCCOEFIS * this.w_RIFCAL1) / 100, this.oParentObject.w_DECTOT)
              * --- Verifico se la quota di accantonamento ordinaria � maggiore del residuo
              *     da ammortizzare
              this.oParentObject.w_IMPV09 = IIF(this.oParentObject.w_IMPV09 > this.oParentObject.w_IMPV07, this.oParentObject.w_IMPV07, this.oParentObject.w_IMPV09)
              this.oParentObject.w_TCOEFI1 = cp_ROUND((100*this.oParentObject.w_IMPV09)/this.w_RIFCAL1,2)
            endif
          endif
        endif
        * --- Verifico che il valore calcolato non superi il Residuo da Amm.Fiscale
        * --- In tal Caso prende il Valore di Quest'Ultimo (V07)
        if this.oParentObject.w_AMMIMM<>"S"
          if this.oParentObject.w_IMPV09+this.oParentObject.w_IMPV13>this.oParentObject.w_IMPV07
            if this.oParentObject.w_IMPV09>this.oParentObject.w_IMPV07
              this.oParentObject.w_IMPV09 = this.oParentObject.w_IMPV07
              this.oParentObject.w_IMPV13 = 0
            else
              this.oParentObject.w_IMPV13 = this.oParentObject.w_IMPV07-this.oParentObject.w_IMPV09
            endif
          endif
        endif
        * --- Verifico se il valore da ammortizzare per quel determinato cespite esiste.
        if this.oParentObject.w_IMPV07=0 and this.oParentObject.w_DaGenera=.F.
          this.oParentObject.w_DaScartare = .f.
        else
          this.oParentObject.w_DaGenera = .T.
          if this.oParentObject.w_IMPV07<>0 And this.oParentObject.w_AMMIMM<>"S"
            * --- Valore Restituito a GSCE_BPA
            * --- Select from CAT_AMMO
            i_nConn=i_TableProp[this.CAT_AMMO_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CAT_AMMO_idx,2],.t.,this.CAT_AMMO_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select EACODCAT  from "+i_cTable+" CAT_AMMO ";
                  +" where EAPROESE="+cp_ToStrODBC(this.w_PROESE)+" and EACODCAT="+cp_ToStrODBC(this.oParentObject.w_CODCAT)+"";
                   ,"_Curs_CAT_AMMO")
            else
              select EACODCAT from (i_cTable);
               where EAPROESE=this.w_PROESE and EACODCAT=this.oParentObject.w_CODCAT;
                into cursor _Curs_CAT_AMMO
            endif
            if used('_Curs_CAT_AMMO')
              select _Curs_CAT_AMMO
              locate for 1=1
              do while not(eof())
              this.oParentObject.w_DaScartare = .f.
                select _Curs_CAT_AMMO
                continue
              enddo
              use
            endif
            * --- Anche se non esiste pi� ammortamento fiscale, controllo se sul cespite ne esiste uno personalizzato 
            *     per l' esercizio di elaborazione, altrimenti emette messaggio di errore.
            if this.oParentObject.w_TIPAMM<>"C" AND this.oParentObject.w_CODESF=this.oParentObject.w_MCCOMPET
              this.oParentObject.w_DaScartare = .f.
            endif
            if this.oParentObject.w_DaScartare=.t. and this.w_Padre="A"
              * --- Controllo se ho specificato nel piano la categoria, in questo caso l'ammortamento
              *     � gi� stato calcolato correttamente.
              if this.oParentObject.w_OKFISORD
                * --- Genero l'accantonamento anche se nella categoria per l'esercizio in corso
                *     non ho specificato nessun ammortamento
                this.w_MESS1 = AH_MSGFORMAT ("Ammortamento effettuato con le percentuali specificate nel piano,%0anche se non � presente una percentuale di ammortamento per l'esercizio in corso%0")
                insert into ResocCespi (categoria, cespite,messaggio);
                values (this.oParentObject.w_CODCAT,this.oParentObject.w_MCCODCES,this.w_MESS1)
              else
                this.w_MSG = .F.
                * --- Devo verificare se l'utente nei parametri cespiti ha selezionato il flag limite ammortamenti fiscali
                this.oParentObject.w_IMPV09 = cp_ROUND((this.w_CCCOEFIS * this.w_RIFCAL1) / 100, this.oParentObject.w_DECTOT)
                this.oParentObject.w_TCOEFI1 = this.w_CCCOEFIS
                * --- Devo verificare se la quota di ammortamento � maggiore della quota di 
                *     residuo da ammortizzare
                if this.oParentObject.w_IMPV09>this.oParentObject.w_IMPV07
                  this.oParentObject.w_IMPV09 = this.oParentObject.w_IMPV07
                  this.w_MSG = .T.
                endif
                * --- Genero l'accantonamento anche se nella categoria per l'esercizio in corso
                *     non ho specificato nessun ammortamento
                if this.w_MSG
                  this.w_MESS1 = AH_MSGFORMAT ("Ammortamento effettuato con l'importo residuo del cespite%0")
                else
                  this.w_MESS1 = AH_MSGFORMAT ("Ammortamento effettuato con la percentuale specificata nella categoria del cespite,%0anche se non � presente una percentuale di ammortamento per l'esercizio in corso%0")
                endif
                insert into ResocCespi (categoria, cespite,messaggio);
                values (this.oParentObject.w_CODCAT,this.oParentObject.w_MCCODCES,this.w_MESS1)
              endif
            endif
          else
            this.oParentObject.w_DaScartare = .F.
          endif
        endif
        * --- V10 - Quote Perse
        *     Calcola la % legata alla Quota Persa
        *     Solo se no Aut.Minor Uso e V09 < della meta' (Parametri cespiti impostati a 50) del Coefficiente di Ammortam.Fiscale
        *     La calcolo se:
        *     Cespite in uso (w_STABEN='U')
        *     Flag Aut. minor Uso disattivo (w_FLAUMU<>'S')
        *     Nel caso di movimento manuale (w_Padre='M') verifico se la
        *     causale cespite ha il flag "Quote perse"
        if this.oParentObject.w_STABEN="U" And this.oParentObject.w_FLAUMU<>"S" And ( (this.w_Padre="M" And this.oParentObject.w_FLGA12="S") or this.w_Padre="A")
          * --- Nel caso di movimenti manuali diversi da accantonamenti fiscali le quote perse vengono lette dai saldi 
          if this.w_Padre="M" And this.oParentObject.w_FLGA09<>"S"
            this.oParentObject.w_IMPV10 = this.w_QUOTEPER
          else
            * --- Leggo la percentuale di accantonamento fiscale
            *     in  base all'esercizio in cui mi trovo.
            this.w_CCCOEORD = 0
            * --- Read from CAT_AMMO
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.CAT_AMMO_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CAT_AMMO_idx,2],.t.,this.CAT_AMMO_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "EAPERORD"+;
                " from "+i_cTable+" CAT_AMMO where ";
                    +"EACODCAT = "+cp_ToStrODBC(this.oParentObject.w_CODCAT);
                    +" and EAPROESE = "+cp_ToStrODBC(this.w_PROESE);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                EAPERORD;
                from (i_cTable) where;
                    EACODCAT = this.oParentObject.w_CODCAT;
                    and EAPROESE = this.w_PROESE;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_CCCOEORD = NVL(cp_ToDate(_read_.EAPERORD),cp_NullValue(_read_.EAPERORD))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            * --- La quota persa va calcolata quando la percentuale utilizzata  (w_TCOEFI1)
            *     e la percentuale ordinaria (w_CCCOEORD) sono in rapporto minori della 
            *     percentuale limite minimo  (w_PERRID); quindi
            *     w_TCOEFI1 / w_CCCOEORD < w_PERRID/100
            if this.w_CCCOEORD<> 0 And (this.oParentObject.w_TCOEFI1 / this.w_CCCOEORD) < ( this.w_PERRID / 100)
              * --- Calcolo l'ammortamento ordinario in base alla quota specificata nella
              *     tabella CAT_AMMO.
              this.w_QUOPEORD = cp_ROUND((this.w_CCCOEORD * this.w_RIFCAL1) / 100, this.oParentObject.w_DECTOT)
              * --- Ammortamento calcolato dalle percentuali specificate sulla maschera.
              *     E' l'ammortamento reale.
              this.w_AMMEFFET = this.oParentObject.w_IMPV09+this.oParentObject.w_IMPV13
              * --- Calcolo il valore della quota persa.
              this.oParentObject.w_IMPV10 = cp_ROUND((this.w_QUOPEORD*(100-this.w_PERRID)/100)- this.w_AMMEFFET, this.oParentObject.w_DECTOT)
              * --- Verifico che il valore calcolato non superi la Differenza tra il  Residuo da Amm.Fiscale e la Quota di Accantonam.Fiscale
              *     In tal Caso prende tale Valore (V07-V09)
              if this.oParentObject.w_IMPV10>0
                this.oParentObject.w_IMPV10 = IIF(this.oParentObject.w_IMPV10 > (this.oParentObject.w_IMPV07-this.w_AMMEFFET), (this.oParentObject.w_IMPV07-this.w_AMMEFFET), this.oParentObject.w_IMPV10)
              else
                this.oParentObject.w_IMPV10 = 0
              endif
            endif
          endif
        endif
        * --- Verifico se il residuo da ammortamento fiscale (anticipato e ordinario) � diverso da 0
        *     oppure se non ha un residuo da ammortamento civile
        if this.oParentObject.w_IMPV09=0 and this.oParentObject.w_IMPV13=0 and (this.w_PCTIPAMM="F" or this.oParentObject.w_IMPV04=0)
          this.oParentObject.w_DaGenera = .F.
        endif
      else
        this.oParentObject.w_IMPV09 = 0
        this.oParentObject.w_IMPV13 = 0
        this.oParentObject.w_TCOEFI1 = 0
        this.oParentObject.w_TCOEFI2 = 0
      endif
    else
      this.oParentObject.w_IMPV05 = 0
      this.oParentObject.w_IMPV06 = 0
      this.oParentObject.w_IMPV07 = 0
      this.oParentObject.w_IMPV09 = 0
      this.oParentObject.w_IMPV11 = 0
      this.oParentObject.w_IMPV13 = 0
      this.oParentObject.w_IMPV14 = 0
      this.oParentObject.w_TCOEFI1 = 0
      this.oParentObject.w_TCOEFI2 = 0
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inizializza variabili di Lavoro
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,w_Padre)
    this.w_Padre=w_Padre
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,10)]
    this.cWorkTables[1]='CAT_AMMO'
    this.cWorkTables[2]='CAT_CESP'
    this.cWorkTables[3]='PAR_CESP'
    this.cWorkTables[4]='ESERCIZI'
    this.cWorkTables[5]='CES_AMMO'
    this.cWorkTables[6]='MOV_CESP'
    this.cWorkTables[7]='CES_PITI'
    this.cWorkTables[8]='AMM_CESP'
    this.cWorkTables[9]='AMC_CESP'
    this.cWorkTables[10]='CAT_AMMC'
    return(this.OpenAllTables(10))

  proc CloseCursors()
    if used('_Curs_GSCE_BCV')
      use in _Curs_GSCE_BCV
    endif
    if used('_Curs_CAT_AMMO')
      use in _Curs_CAT_AMMO
    endif
    if used('_Curs_CAT_AMMO')
      use in _Curs_CAT_AMMO
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_Padre"
endproc
