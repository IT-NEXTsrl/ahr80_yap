* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_kcm                                                        *
*              Contabilizzazione movimenti cespiti                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-03-26                                                      *
* Last revis.: 2009-10-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsce_kcm",oParentObject))

* --- Class definition
define class tgsce_kcm as StdForm
  Top    = 21
  Left   = 34

  * --- Standard Properties
  Width  = 382
  Height = 131
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-10-15"
  HelpContextID=169196183
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=4

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsce_kcm"
  cComment = "Contabilizzazione movimenti cespiti"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_DATFIN = ctod('  /  /  ')
  w_FLDATC = space(1)
  w_DATCON = ctod('  /  /  ')
  w_FLPROV = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsce_kcmPag1","gsce_kcm",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDATFIN_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do GSCE_BCM with this
      endwith
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_DATFIN=ctod("  /  /  ")
      .w_FLDATC=space(1)
      .w_DATCON=ctod("  /  /  ")
      .w_FLPROV=space(1)
        .w_DATFIN = i_datsys
        .w_FLDATC = 'U'
        .w_DATCON = i_datsys
        .w_FLPROV = 'N'
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,4,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oDATCON_1_3.enabled = this.oPgFrm.Page1.oPag.oDATCON_1_3.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oDATCON_1_3.visible=!this.oPgFrm.Page1.oPag.oDATCON_1_3.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_1.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_1.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oFLDATC_1_2.RadioValue()==this.w_FLDATC)
      this.oPgFrm.Page1.oPag.oFLDATC_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDATCON_1_3.value==this.w_DATCON)
      this.oPgFrm.Page1.oPag.oDATCON_1_3.value=this.w_DATCON
    endif
    if not(this.oPgFrm.Page1.oPag.oFLPROV_1_4.RadioValue()==this.w_FLPROV)
      this.oPgFrm.Page1.oPag.oFLPROV_1_4.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_DATFIN)) or not(.w_DATCON>=.w_DATFIN OR .w_FLDATC<>'U'))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFIN_1_1.SetFocus()
            i_bnoObbl = !empty(.w_DATFIN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data di selezione maggiore della data di contabilizzazione")
          case   ((empty(.w_DATCON)) or not(.w_DATCON>=.w_DATFIN))  and not(.w_FLDATC<>'U')  and (.w_FLDATC='U')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATCON_1_3.SetFocus()
            i_bnoObbl = !empty(.w_DATCON)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data di contabilizzazione minore della data di selezione movimenti")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsce_kcmPag1 as StdContainer
  Width  = 378
  height = 131
  stdWidth  = 378
  stdheight = 131
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDATFIN_1_1 as StdField with uid="SJTOPLMBNQ",rtseq=1,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data di selezione maggiore della data di contabilizzazione",;
    ToolTipText = "Data di fine selezione dei movimenti da contabilizzare",;
    HelpContextID = 51298250,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=150, Top=13

  func oDATFIN_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATCON>=.w_DATFIN OR .w_FLDATC<>'U')
    endwith
    return bRes
  endfunc


  add object oFLDATC_1_2 as StdCombo with uid="PPWABLUYXG",rtseq=2,rtrep=.f.,left=150,top=44,width=135,height=21;
    , ToolTipText = "Data di registrazione P.N.: unica o uguale alla data reg. del movimento cespite";
    , HelpContextID = 224703658;
    , cFormVar="w_FLDATC",RowSource=""+"Unica,"+"Data registrazione", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLDATC_1_2.RadioValue()
    return(iif(this.value =1,'U',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oFLDATC_1_2.GetRadio()
    this.Parent.oContained.w_FLDATC = this.RadioValue()
    return .t.
  endfunc

  func oFLDATC_1_2.SetRadio()
    this.Parent.oContained.w_FLDATC=trim(this.Parent.oContained.w_FLDATC)
    this.value = ;
      iif(this.Parent.oContained.w_FLDATC=='U',1,;
      iif(this.Parent.oContained.w_FLDATC=='D',2,;
      0))
  endfunc

  add object oDATCON_1_3 as StdField with uid="BSNZCHZTLP",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DATCON", cQueryName = "DATCON",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data di contabilizzazione minore della data di selezione movimenti",;
    ToolTipText = "Data di contabilizzazione dei movimenti selezionati (se unica)",;
    HelpContextID = 45203402,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=293, Top=44

  func oDATCON_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLDATC='U')
    endwith
   endif
  endfunc

  func oDATCON_1_3.mHide()
    with this.Parent.oContained
      return (.w_FLDATC<>'U')
    endwith
  endfunc

  func oDATCON_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATCON>=.w_DATFIN)
    endwith
    return bRes
  endfunc

  add object oFLPROV_1_4 as StdCheck with uid="LYDYASUNJY",rtseq=4,rtrep=.f.,left=19, top=85, caption="Movimenti provvisori",;
    ToolTipText = "Se attivo: contabilizza anche i movimenti cespite provvisori (come reg.provvisor",;
    HelpContextID = 178451626,;
    cFormVar="w_FLPROV", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLPROV_1_4.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLPROV_1_4.GetRadio()
    this.Parent.oContained.w_FLPROV = this.RadioValue()
    return .t.
  endfunc

  func oFLPROV_1_4.SetRadio()
    this.Parent.oContained.w_FLPROV=trim(this.Parent.oContained.w_FLPROV)
    this.value = ;
      iif(this.Parent.oContained.w_FLPROV=='S',1,;
      0)
  endfunc


  add object oBtn_1_7 as StdButton with uid="ZMIZZNTUDJ",left=272, top=79, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per effettuare la contabilizzazione";
    , HelpContextID = 44084202;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_7.Click()
      with this.Parent.oContained
        do GSCE_BCM with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_7.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_DATCON>=.w_DATFIN OR .w_FLDATC<>'U')
      endwith
    endif
  endfunc


  add object oBtn_1_8 as StdButton with uid="JWIBHYOYEX",left=324, top=79, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 176513606;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_8.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_5 as StdString with uid="TNNUVBUKJW",Visible=.t., Left=8, Top=13,;
    Alignment=1, Width=138, Height=15,;
    Caption="Movimenti fino al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="EMQRMIETVC",Visible=.t., Left=4, Top=44,;
    Alignment=1, Width=142, Height=15,;
    Caption="Data contabilizzazione:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsce_kcm','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
