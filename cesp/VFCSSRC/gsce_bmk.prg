* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_bmk                                                        *
*              Controlli movimenti cespiti                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_56]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-03-18                                                      *
* Last revis.: 2013-04-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsce_bmk",oParentObject)
return(i_retval)

define class tgsce_bmk as StdBatch
  * --- Local variables
  w_TCOM = 0
  w_TCOMC = 0
  w_MESS = space(10)
  w_OK = .f.
  w_CODAZI = space(5)
  w_CODESE = space(4)
  w_MESS1 = space(10)
  w_TROVA = .f.
  w_oMESS = .NULL.
  w_oPART = .NULL.
  w_MVDATDOC = ctod("  /  /  ")
  w_MVNUMDOC = 0
  w_MVALFDOC = space(10)
  w_MVCODCON = space(15)
  w_MVTIPCON = space(15)
  w_PNDATREG = ctod("  /  /  ")
  w_PNNUMRER = 0
  * --- WorkFile variables
  MOV_PCES_idx=0
  PNT_CESP_idx=0
  DOC_DETT_idx=0
  DOC_MAST_idx=0
  COM_PUBI_idx=0
  MOV_CESP_idx=0
  ESERCIZI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controlli Finali Movimenti Cespiti (da GSCE_AMC) in A.M. Replace End
    this.w_OK = .T.
    this.w_MESS = ah_Msgformat("Transazione abbandonata")
    if this.oParentObject.w_MCIMPA01=0 AND this.oParentObject.w_MCIMPA02=0 AND this.oParentObject.w_MCIMPA03=0 AND this.oParentObject.w_MCIMPA04=0 AND this.oParentObject.w_MCIMPA05=0
      if this.oParentObject.w_MCIMPA06=0 AND this.oParentObject.w_MCIMPA07=0 AND this.oParentObject.w_MCIMPA08=0 AND this.oParentObject.w_MCIMPA09=0 AND this.oParentObject.w_MCIMPA10=0 AND this.oParentObject.w_MCIMPA11=0
        if this.oParentObject.w_MCIMPA12=0 AND this.oParentObject.w_MCIMPA13=0 AND this.oParentObject.w_MCIMPA14=0 AND this.oParentObject.w_MCIMPA15=0 AND this.oParentObject.w_MCIMPA16=0 AND this.oParentObject.w_MCIMPA18=0
          if this.oParentObject.w_MCIMPA19=0 AND this.oParentObject.w_MCIMPA20=0 AND this.oParentObject.w_MCIMPA21=0 AND this.oParentObject.w_MCIMPA22=0 AND this.oParentObject.w_MCIMPA23=0 AND this.oParentObject.w_MCIMPA24=0
            if EMPTY(Cp_Todate(this.oParentObject.w_MCDTPRIU)) AND EMPTY(Cp_Todate(this.oParentObject.w_MCDATDIS)) AND EMPTY(Cp_Todate(this.oParentObject.w_MCDTPRIC))
              this.w_MESS = ah_Msgformat("Movimento cespite non valorizzato")
              this.w_OK = .F.
            endif
          endif
        endif
      endif
    endif
    * --- Controllo che lo stato del bene del cespite non sia 'In uso' se il movimento � di tipo acquisto.
    if (this.oParentObject.w_STABEN="U" and this.oParentObject.w_FLGA02="S") AND this.w_OK
      if ah_YesNo("Cespite gi� in uso %0Si intende proseguire?")
        this.w_OK = .T.
      else
        this.w_MESS = ah_Msgformat("Cespite in uso")
        this.w_OK = .F.
      endif
    endif
    * --- Verifica che siano stati fatti gli ammortamenti in tutti gli esercizi del cespite
    if this.oParentObject.w_FLDADI="S" AND this.w_OK
      this.w_MESS1 = ""
      this.w_CODAZI = i_CODAZI
      * --- Select from ESERCIZI
      i_nConn=i_TableProp[this.ESERCIZI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2],.t.,this.ESERCIZI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select ESCODESE  from "+i_cTable+" ESERCIZI ";
            +" where ESCODAZI="+cp_ToStrODBC(this.w_CODAZI)+" AND ESFINESE>="+cp_ToStrODBC(this.oParentObject.w_DTPRIU)+" AND ESFINESE<="+cp_ToStrODBC(this.oParentObject.w_FINESE)+"";
            +" order by ESCODESE";
             ,"_Curs_ESERCIZI")
      else
        select ESCODESE from (i_cTable);
         where ESCODAZI=this.w_CODAZI AND ESFINESE>=this.oParentObject.w_DTPRIU AND ESFINESE<=this.oParentObject.w_FINESE;
         order by ESCODESE;
          into cursor _Curs_ESERCIZI
      endif
      if used('_Curs_ESERCIZI')
        select _Curs_ESERCIZI
        locate for 1=1
        do while not(eof())
        this.w_TROVA = .F.
        this.w_CODESE = _Curs_ESERCIZI.ESCODESE
        * --- Select from MOV_CESP
        i_nConn=i_TableProp[this.MOV_CESP_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MOV_CESP_idx,2],.t.,this.MOV_CESP_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" MOV_CESP ";
              +" where MCCOMPET="+cp_ToStrODBC(this.w_CODESE)+" AND MCCODCES="+cp_ToStrODBC(this.oParentObject.w_MCCODCES)+" AND (MCIMPA07<>0 OR MCIMPA09<>0 OR MCIMPA15<>0) AND MCSTATUS='C'";
               ,"_Curs_MOV_CESP")
        else
          select * from (i_cTable);
           where MCCOMPET=this.w_CODESE AND MCCODCES=this.oParentObject.w_MCCODCES AND (MCIMPA07<>0 OR MCIMPA09<>0 OR MCIMPA15<>0) AND MCSTATUS="C";
            into cursor _Curs_MOV_CESP
        endif
        if used('_Curs_MOV_CESP')
          select _Curs_MOV_CESP
          locate for 1=1
          do while not(eof())
          this.w_TROVA = .T.
            select _Curs_MOV_CESP
            continue
          enddo
          use
        endif
        if  !this.w_TROVA
          this.w_MESS1 = this.w_MESS1 + this.w_CODESE + CHR(13)
        endif
          select _Curs_ESERCIZI
          continue
        enddo
        use
      endif
      if NOT EMPTY(this.w_MESS1)
        ah_ErrorMsg("Manca l'ammortamento per i seguenti esercizi (solo warning):%0%1",,"", this.w_MESS1)
      endif
      * --- Controllo presenza dela data di dismissione del cespite
      if empty(this.oParentObject.w_MCDATDIS)
        ah_ErrorMsg("Data di dismissione cespite non valorizzata, ma richiesta dalla causale.%0Verr� inizializzata in automatico dalla procedura, verificare se la data immessa risulta corretta.",,"")
        * --- Blocco il savataggio
        this.w_OK = .F.
      endif
    endif
    if this.w_OK=.T. AND this.oParentObject.w_TIPCES $ "CC-PC"
      * --- Cespite gestito a Componenti
      this.oParentObject.w_TCAR = (this.oParentObject.w_MCIMPA02+this.oParentObject.w_MCIMPA03+this.oParentObject.w_MCIMPA04)-this.oParentObject.w_MCIMPA06
      do case
        case this.oParentObject.w_FLCAR
          * --- Acquisto
          this.w_TCOM = 0
          * --- Select from MOV_PCES
          i_nConn=i_TableProp[this.MOV_PCES_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MOV_PCES_idx,2],.t.,this.MOV_PCES_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select MCIMPTOT  from "+i_cTable+" MOV_PCES ";
                +" where MCSERIAL="+cp_ToStrODBC(this.oParentObject.w_MCSERIAL)+"";
                 ,"_Curs_MOV_PCES")
          else
            select MCIMPTOT from (i_cTable);
             where MCSERIAL=this.oParentObject.w_MCSERIAL;
              into cursor _Curs_MOV_PCES
          endif
          if used('_Curs_MOV_PCES')
            select _Curs_MOV_PCES
            locate for 1=1
            do while not(eof())
            this.w_TCOM = this.w_TCOM + NVL(_Curs_MOV_PCES.MCIMPTOT, 0)
              select _Curs_MOV_PCES
              continue
            enddo
            use
          endif
          if this.w_TCOM<>this.oParentObject.w_TCAR
            this.w_MESS = ah_Msgformat("L'importo del movimento cespite non � congruente con gli importi dei componenti valorizzati%0Importo movimento:%1%0Importi componenti:%2", TRANSFORM(this.oParentObject.w_TCAR,"@Z"+v_pv(20*(this.oParentObject.w_dectot+2))), TRANSFORM(this.w_TCOM,"@Z"+v_pv(20*(this.oParentObject.w_dectot+2))) )
            this.w_OK = .F.
          endif
        case this.oParentObject.w_FLSCA
          if this.oParentObject.w_FLDADI="S"
            * --- Dismissione
            * --- Select from COM_PUBI
            i_nConn=i_TableProp[this.COM_PUBI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.COM_PUBI_idx,2],.t.,this.COM_PUBI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select CUCOMPON  from "+i_cTable+" COM_PUBI ";
                  +" where CUCODICE="+cp_ToStrODBC(this.oParentObject.w_MCCODCES)+" AND CUDATVEN IS NULL";
                   ,"_Curs_COM_PUBI")
            else
              select CUCOMPON from (i_cTable);
               where CUCODICE=this.oParentObject.w_MCCODCES AND CUDATVEN IS NULL;
                into cursor _Curs_COM_PUBI
            endif
            if used('_Curs_COM_PUBI')
              select _Curs_COM_PUBI
              locate for 1=1
              do while not(eof())
              if  NOT EMPTY(NVL(_Curs_COM_PUBI.CUCOMPON,""))
                this.w_MESS = ah_Msgformat("Il componente %1 non � stato selezionato dalla lista dei componenti", alltrim(_Curs_COM_PUBI.CUCOMPON) )
                this.w_OK = .F.
                exit
              endif
                select _Curs_COM_PUBI
                continue
              enddo
              use
            endif
          else
            * --- Istanzio Oggetto Messaggio Incrementale
            this.w_oMESS=createobject("ah_message")
            * --- Vendita
            if this.oParentObject.w_IMPV02<>this.oParentObject.w_MCIMPA05
              this.w_oMESS.addmsgpartNL("L'importo relativo al decremento di valore � incongruente con il totale degli importi dei componenti selezionati")     
              this.w_oPART = this.w_oMESS.addmsgpartNL("Importo decremento valore:%1%0Importi componenti selezionati:%2%0Si intende proseguire?")
              this.w_oPART.addParam(TRANSFORM(this.oParentObject.w_MCIMPA05,"@Z"+v_pv(20*(this.oParentObject.w_dectot+2))))     
              this.w_oPART.addParam(TRANSFORM(this.oParentObject.w_IMPV02,"@Z"+v_pv(20*(this.oParentObject.w_dectot+2))))     
              this.w_OK = .F.
              if this.w_oMess.ah_YesNo()
                this.w_OK = .T.
              endif
            endif
          endif
      endcase
    endif
    * --- Controlla la congruenza delle impostazioni fra mov.cespite  e documento associato: N: Doc, Alfa Doc, Data Doc,Cli/For, Quantit�
    * --- Select from DOC_DETT
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select DOC_DETT.MVSERIAL, MVQTAMOV  from "+i_cTable+" DOC_DETT ";
          +" where DOC_DETT.MVCESSER = "+cp_ToStrODBC(this.oParentObject.w_MCSERIAL)+"";
           ,"_Curs_DOC_DETT")
    else
      select DOC_DETT.MVSERIAL, MVQTAMOV from (i_cTable);
       where DOC_DETT.MVCESSER = this.oParentObject.w_MCSERIAL;
        into cursor _Curs_DOC_DETT
    endif
    if used('_Curs_DOC_DETT')
      select _Curs_DOC_DETT
      locate for 1=1
      do while not(eof())
      * --- Recupero gli estremi del documento...
      * --- Read from DOC_MAST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.DOC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MVDATDOC,MVNUMDOC,MVALFDOC,MVCODCON,MVTIPCON"+;
          " from "+i_cTable+" DOC_MAST where ";
              +"MVSERIAL = "+cp_ToStrODBC(_Curs_DOC_DETT.MVSERIAL);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MVDATDOC,MVNUMDOC,MVALFDOC,MVCODCON,MVTIPCON;
          from (i_cTable) where;
              MVSERIAL = _Curs_DOC_DETT.MVSERIAL;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_MVDATDOC = NVL(cp_ToDate(_read_.MVDATDOC),cp_NullValue(_read_.MVDATDOC))
        this.w_MVNUMDOC = NVL(cp_ToDate(_read_.MVNUMDOC),cp_NullValue(_read_.MVNUMDOC))
        this.w_MVALFDOC = NVL(cp_ToDate(_read_.MVALFDOC),cp_NullValue(_read_.MVALFDOC))
        this.w_MVCODCON = NVL(cp_ToDate(_read_.MVCODCON),cp_NullValue(_read_.MVCODCON))
        this.w_MVTIPCON = NVL(cp_ToDate(_read_.MVTIPCON),cp_NullValue(_read_.MVTIPCON))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.oParentObject.w_MCNUMDOC<>this.w_MVNUMDOC OR this.oParentObject.w_MCALFDOC<>this.w_MVALFDOC OR this.oParentObject.w_MCDATDOC<>this.w_MVDATDOC OR this.oParentObject.w_MCCODCON<>this.w_MVCODCON OR this.oParentObject.w_MCTIPCON<>this.w_MVTIPCON OR abs(this.oParentObject.w_MCQTACES)<>abs(NVL(_Curs_DOC_DETT.MVQTAMOV ,0) )
        if ah_YesNo("Numero, data, serie, cliente/fornitore, o quantit� del documento associato n�%1 del %2 non congruente%0Confermi l'elaborazione?","", ALLTRIM(STR(this.w_MVNUMDOC,15))+IIF(NOT EMPTY(this.w_MVALFDOC),"/","")+Alltrim(this.w_MVALFDOC), DTOC(this.w_MVDATDOC) )
        else
          this.w_MESS = ah_Msgformat("Elaborazione interrotta")
          this.w_OK = .F.
        endif
      else
        this.w_MESS = ""
      endif
        select _Curs_DOC_DETT
        continue
      enddo
      use
    endif
    * --- Controlla la congruenza delle impostazioni fra mov.cespite e reg.primanota  associata: N.doc, Alfa Doc, Data Doc,Cli/For, 
    * --- Select from GSCE1BMK
    do vq_exec with 'GSCE1BMK',this,'_Curs_GSCE1BMK','',.f.,.t.
    if used('_Curs_GSCE1BMK')
      select _Curs_GSCE1BMK
      locate for 1=1
      do while not(eof())
      this.w_PNDATREG = NVL(_Curs_GSCE1BMK.PNDATREG,cp_CharToDate("  -  -    "))
      this.w_PNNUMRER = NVL(_Curs_GSCE1BMK.PNNUMRER,0)
      if this.oParentObject.w_MCNUMDOC<>NVL(_Curs_GSCE1BMK.PNNUMDOC,0) OR this.oParentObject.w_MCALFDOC<>NVL(_Curs_GSCE1BMK.PNALFDOC,SPACE(2)) OR this.oParentObject.w_MCDATDOC<>NVL(_Curs_GSCE1BMK.PNDATDOC,cp_CharToDate("  -  -    ")) OR this.oParentObject.w_MCCODCON<>NVL(_Curs_GSCE1BMK.PNCODCLF,SPACE(15)) OR this.oParentObject.w_MCTIPCON<>NVL(_Curs_GSCE1BMK.PNTIPCLF," ")
        if ah_YesNo("Numero, data, serie o cliente/fornitore della registrazione di primanota associata n�%1 del %2 non congruente%0Confermi l'elaborazione?","", ALLTRIM(STR(this.w_PNNUMRER)), DTOC(this.w_PNDATREG) )
        else
          this.w_MESS = ah_Msgformat("Elaborazione interrotta")
          this.w_OK = .F.
        endif
      else
        this.w_MESS = ""
      endif
        select _Curs_GSCE1BMK
        continue
      enddo
      use
    endif
    if Not this.w_OK
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=this.w_MESS
      this.oParentObject.w_BLOCCA = .T.
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,7)]
    this.cWorkTables[1]='MOV_PCES'
    this.cWorkTables[2]='PNT_CESP'
    this.cWorkTables[3]='DOC_DETT'
    this.cWorkTables[4]='DOC_MAST'
    this.cWorkTables[5]='COM_PUBI'
    this.cWorkTables[6]='MOV_CESP'
    this.cWorkTables[7]='ESERCIZI'
    return(this.OpenAllTables(7))

  proc CloseCursors()
    if used('_Curs_ESERCIZI')
      use in _Curs_ESERCIZI
    endif
    if used('_Curs_MOV_CESP')
      use in _Curs_MOV_CESP
    endif
    if used('_Curs_MOV_PCES')
      use in _Curs_MOV_PCES
    endif
    if used('_Curs_COM_PUBI')
      use in _Curs_COM_PUBI
    endif
    if used('_Curs_DOC_DETT')
      use in _Curs_DOC_DETT
    endif
    if used('_Curs_GSCE1BMK')
      use in _Curs_GSCE1BMK
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
