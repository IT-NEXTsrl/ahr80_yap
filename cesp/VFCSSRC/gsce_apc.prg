* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_apc                                                        *
*              Parametri cespiti                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30] [VRS_70]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-04-26                                                      *
* Last revis.: 2009-12-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsce_apc"))

* --- Class definition
define class tgsce_apc as StdForm
  Top    = 0
  Left   = 18

  * --- Standard Properties
  Width  = 484
  Height = 475+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-12-14"
  HelpContextID=160056681
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=58

  * --- Constant Properties
  PAR_CESP_IDX = 0
  AZIENDA_IDX = 0
  ESERCIZI_IDX = 0
  cFile = "PAR_CESP"
  cKeySelect = "PCCODAZI"
  cKeyWhere  = "PCCODAZI=this.w_PCCODAZI"
  cKeyWhereODBC = '"PCCODAZI="+cp_ToStrODBC(this.w_PCCODAZI)';

  cKeyWhereODBCqualified = '"PAR_CESP.PCCODAZI="+cp_ToStrODBC(this.w_PCCODAZI)';

  cPrg = "gsce_apc"
  cComment = "Parametri cespiti"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_PCCODAZI = space(5)
  w_RAGAZI = space(40)
  w_PCPERESE = 0
  w_PCPERRID = 0
  w_PCPERANT = 0
  w_PCAMMABU = 0
  w_PCMAXANT = 0
  w_PCDATCON = space(4)
  w_PCPERTOL = 0
  w_PCSTAREG = space(1)
  o_PCSTAREG = space(1)
  w_PCTIPAMM = space(1)
  w_PCDATCOC = space(1)
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_OBTEST = ctod('  /  /  ')
  w_PCREGCES = space(1)
  o_PCREGCES = space(1)
  w_PCFLSTIN = space(1)
  w_PCSTAINT = space(1)
  o_PCSTAINT = space(1)
  w_PCPREFIS = space(20)
  w_PCCONC01 = space(30)
  w_PCCONC02 = space(30)
  w_PCCONC03 = space(30)
  w_PCCONC04 = space(30)
  w_PCCONC05 = space(30)
  w_PCCONC06 = space(30)
  w_PCCONC07 = space(30)
  w_PCCONC08 = space(30)
  w_PCCONC09 = space(30)
  w_PCCONC10 = space(30)
  w_PCCONC11 = space(30)
  w_PCCONC12 = space(30)
  w_PCCONC13 = space(30)
  w_PCCONC14 = space(30)
  w_PCCONC15 = space(30)
  w_PCCONC16 = space(30)
  w_PCTIPC01 = space(1)
  w_PCTIPC02 = space(1)
  w_PCTIPC03 = space(1)
  w_PCTIPC04 = space(1)
  w_PCTIPC05 = space(1)
  w_PCTIPC06 = space(1)
  w_PCTIPC07 = space(1)
  w_PCTIPC08 = space(1)
  w_PCTIPC09 = space(1)
  w_PCTIPC10 = space(1)
  w_PCFLSALP = space(1)
  w_PCFLCARR = space(1)
  w_PCTIPC11 = space(1)
  w_PCTIPC12 = space(1)
  w_PCTIPC13 = space(1)
  w_PCTIPC14 = space(1)
  w_PCTIPC15 = space(1)
  w_PCTIPC16 = space(1)
  w_PCBIMMAM = space(1)
  w_IASADO = space(1)
  w_PCIASADO = space(1)
  * --- Area Manuale = Declare Variables
  * --- gsce_apc
  * --- Disabilita il Caricamento e la Cancellazione sulla Toolbar
  proc SetCPToolBar()
          doDefault()
          oCpToolBar.b3.enabled=.f.
          oCpToolBar.b5.enabled=.f.
  endproc
  * ---- Disattiva i metodi Load e Delete (posso solo variare)
  proc ecpLoad()
      * ----
  endproc
  proc ecpDelete()
      * ----
  endproc
  proc ecpEdit()
        * ----
        DoDefault()
        this.oPgFrm.Pages(3).caption=''
        this.oPgFrm.Pages(3).enabled=.f.
  endproc
  proc ecpQuery()
        * ----
        DoDefault()
        this.oPgFrm.Pages(3).caption=''
        this.oPgFrm.Pages(3).enabled=.f.
  endproc
  
  
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'PAR_CESP','gsce_apc')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsce_apcPag1","gsce_apc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Parametri")
      .Pages(1).HelpContextID = 44976120
      .Pages(2).addobject("oPag","tgsce_apcPag2","gsce_apc",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Contropartite")
      .Pages(2).HelpContextID = 64756135
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- gsce_apc
    * --- Per non visualizzare le Tabs
    this.Tabs=.t.
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='ESERCIZI'
    this.cWorkTables[3]='PAR_CESP'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.PAR_CESP_IDX,5],7]
    this.nPostItConn=i_TableProp[this.PAR_CESP_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_PCCODAZI = NVL(PCCODAZI,space(5))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_1_joined
    link_1_1_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- gsce_apc
    this.w_PCCODAZI=i_codazi
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from PAR_CESP where PCCODAZI=KeySet.PCCODAZI
    *
    i_nConn = i_TableProp[this.PAR_CESP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_CESP_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('PAR_CESP')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "PAR_CESP.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' PAR_CESP '
      link_1_1_joined=this.AddJoinedLink_1_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'PCCODAZI',this.w_PCCODAZI  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_RAGAZI = space(40)
        .w_OBTEST = i_datsys
        .w_IASADO = space(1)
        .w_PCCODAZI = NVL(PCCODAZI,space(5))
          if link_1_1_joined
            this.w_PCCODAZI = NVL(AZCODAZI101,NVL(this.w_PCCODAZI,space(5)))
            this.w_RAGAZI = NVL(AZRAGAZI101,space(40))
          else
          .link_1_1('Load')
          endif
        .w_PCPERESE = NVL(PCPERESE,0)
        .w_PCPERRID = NVL(PCPERRID,0)
        .w_PCPERANT = NVL(PCPERANT,0)
        .w_PCAMMABU = NVL(PCAMMABU,0)
        .w_PCMAXANT = NVL(PCMAXANT,0)
        .w_PCDATCON = NVL(PCDATCON,space(4))
          * evitabile
          *.link_1_8('Load')
        .w_PCPERTOL = NVL(PCPERTOL,0)
        .w_PCSTAREG = NVL(PCSTAREG,space(1))
        .w_PCTIPAMM = NVL(PCTIPAMM,space(1))
        .w_PCDATCOC = NVL(PCDATCOC,space(1))
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .w_PCREGCES = NVL(PCREGCES,space(1))
        .w_PCFLSTIN = NVL(PCFLSTIN,space(1))
        .w_PCSTAINT = NVL(PCSTAINT,space(1))
        .w_PCPREFIS = NVL(PCPREFIS,space(20))
        .w_PCCONC01 = NVL(PCCONC01,space(30))
        .w_PCCONC02 = NVL(PCCONC02,space(30))
        .w_PCCONC03 = NVL(PCCONC03,space(30))
        .w_PCCONC04 = NVL(PCCONC04,space(30))
        .w_PCCONC05 = NVL(PCCONC05,space(30))
        .w_PCCONC06 = NVL(PCCONC06,space(30))
        .w_PCCONC07 = NVL(PCCONC07,space(30))
        .w_PCCONC08 = NVL(PCCONC08,space(30))
        .w_PCCONC09 = NVL(PCCONC09,space(30))
        .w_PCCONC10 = NVL(PCCONC10,space(30))
        .w_PCCONC11 = NVL(PCCONC11,space(30))
        .w_PCCONC12 = NVL(PCCONC12,space(30))
        .w_PCCONC13 = NVL(PCCONC13,space(30))
        .w_PCCONC14 = NVL(PCCONC14,space(30))
        .w_PCCONC15 = NVL(PCCONC15,space(30))
        .w_PCCONC16 = NVL(PCCONC16,space(30))
        .w_PCTIPC01 = NVL(PCTIPC01,space(1))
        .w_PCTIPC02 = NVL(PCTIPC02,space(1))
        .w_PCTIPC03 = NVL(PCTIPC03,space(1))
        .w_PCTIPC04 = NVL(PCTIPC04,space(1))
        .w_PCTIPC05 = NVL(PCTIPC05,space(1))
        .w_PCTIPC06 = NVL(PCTIPC06,space(1))
        .w_PCTIPC07 = NVL(PCTIPC07,space(1))
        .w_PCTIPC08 = NVL(PCTIPC08,space(1))
        .w_PCTIPC09 = NVL(PCTIPC09,space(1))
        .w_PCTIPC10 = NVL(PCTIPC10,space(1))
        .w_PCFLSALP = NVL(PCFLSALP,space(1))
        .w_PCFLCARR = NVL(PCFLCARR,space(1))
        .w_PCTIPC11 = NVL(PCTIPC11,space(1))
        .w_PCTIPC12 = NVL(PCTIPC12,space(1))
        .w_PCTIPC13 = NVL(PCTIPC13,space(1))
        .w_PCTIPC14 = NVL(PCTIPC14,space(1))
        .w_PCTIPC15 = NVL(PCTIPC15,space(1))
        .w_PCTIPC16 = NVL(PCTIPC16,space(1))
        .w_PCBIMMAM = NVL(PCBIMMAM,space(1))
        .w_PCIASADO = NVL(PCIASADO,space(1))
        cp_LoadRecExtFlds(this,'PAR_CESP')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gsce_apc
    This.w_IASADO = This.w_PCIASADO
    if this.w_PCCODAZI<>i_codazi and not empty(this.w_PCCODAZI)
       this.blankrec()
       this.w_PCCODAZI=""
       ah_ErrorMsg("Manutenzione consentita alla sola azienda corrente (%1)",,'',i_CODAZI)
    endif
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PCCODAZI = space(5)
      .w_RAGAZI = space(40)
      .w_PCPERESE = 0
      .w_PCPERRID = 0
      .w_PCPERANT = 0
      .w_PCAMMABU = 0
      .w_PCMAXANT = 0
      .w_PCDATCON = space(4)
      .w_PCPERTOL = 0
      .w_PCSTAREG = space(1)
      .w_PCTIPAMM = space(1)
      .w_PCDATCOC = space(1)
      .w_UTCC = 0
      .w_UTCV = 0
      .w_UTDC = ctot("")
      .w_UTDV = ctot("")
      .w_OBTEST = ctod("  /  /  ")
      .w_PCREGCES = space(1)
      .w_PCFLSTIN = space(1)
      .w_PCSTAINT = space(1)
      .w_PCPREFIS = space(20)
      .w_PCCONC01 = space(30)
      .w_PCCONC02 = space(30)
      .w_PCCONC03 = space(30)
      .w_PCCONC04 = space(30)
      .w_PCCONC05 = space(30)
      .w_PCCONC06 = space(30)
      .w_PCCONC07 = space(30)
      .w_PCCONC08 = space(30)
      .w_PCCONC09 = space(30)
      .w_PCCONC10 = space(30)
      .w_PCCONC11 = space(30)
      .w_PCCONC12 = space(30)
      .w_PCCONC13 = space(30)
      .w_PCCONC14 = space(30)
      .w_PCCONC15 = space(30)
      .w_PCCONC16 = space(30)
      .w_PCTIPC01 = space(1)
      .w_PCTIPC02 = space(1)
      .w_PCTIPC03 = space(1)
      .w_PCTIPC04 = space(1)
      .w_PCTIPC05 = space(1)
      .w_PCTIPC06 = space(1)
      .w_PCTIPC07 = space(1)
      .w_PCTIPC08 = space(1)
      .w_PCTIPC09 = space(1)
      .w_PCTIPC10 = space(1)
      .w_PCFLSALP = space(1)
      .w_PCFLCARR = space(1)
      .w_PCTIPC11 = space(1)
      .w_PCTIPC12 = space(1)
      .w_PCTIPC13 = space(1)
      .w_PCTIPC14 = space(1)
      .w_PCTIPC15 = space(1)
      .w_PCTIPC16 = space(1)
      .w_PCBIMMAM = space(1)
      .w_IASADO = space(1)
      .w_PCIASADO = space(1)
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
          if not(empty(.w_PCCODAZI))
          .link_1_1('Full')
          endif
        .DoRTCalc(2,8,.f.)
          if not(empty(.w_PCDATCON))
          .link_1_8('Full')
          endif
          .DoRTCalc(9,9,.f.)
        .w_PCSTAREG = 'C'
        .w_PCTIPAMM = 'E'
          .DoRTCalc(12,16,.f.)
        .w_OBTEST = i_datsys
          .DoRTCalc(18,20,.f.)
        .w_PCPREFIS = IIF(.w_PCSTAINT<>'S',SPACE(20),.w_PCPREFIS)
          .DoRTCalc(22,37,.f.)
        .w_PCTIPC01 = 'A'
        .w_PCTIPC02 = 'A'
        .w_PCTIPC03 = 'A'
        .w_PCTIPC04 = 'A'
        .w_PCTIPC05 = 'A'
        .w_PCTIPC06 = 'A'
        .w_PCTIPC07 = 'A'
        .w_PCTIPC08 = 'A'
        .w_PCTIPC09 = 'A'
        .w_PCTIPC10 = 'A'
        .w_PCFLSALP = IIF(.w_PCSTAREG<>'C','N',.w_PCFLSALP)
        .w_PCFLCARR = 'N'
        .w_PCTIPC11 = 'A'
        .w_PCTIPC12 = 'A'
        .w_PCTIPC13 = 'A'
        .w_PCTIPC14 = 'A'
        .w_PCTIPC15 = 'A'
        .w_PCTIPC16 = 'A'
        .w_PCBIMMAM = IIF(.w_PCREGCES='S','S','N')
          .DoRTCalc(57,57,.f.)
        .w_PCIASADO = 'N'
      endif
    endwith
    cp_BlankRecExtFlds(this,'PAR_CESP')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oPCPERESE_1_3.enabled = i_bVal
      .Page1.oPag.oPCPERRID_1_4.enabled = i_bVal
      .Page1.oPag.oPCPERANT_1_5.enabled = i_bVal
      .Page1.oPag.oPCAMMABU_1_6.enabled = i_bVal
      .Page1.oPag.oPCMAXANT_1_7.enabled = i_bVal
      .Page1.oPag.oPCDATCON_1_8.enabled = i_bVal
      .Page1.oPag.oPCPERTOL_1_9.enabled = i_bVal
      .Page1.oPag.oPCSTAREG_1_10.enabled = i_bVal
      .Page1.oPag.oPCTIPAMM_1_11.enabled = i_bVal
      .Page1.oPag.oPCDATCOC_1_12.enabled = i_bVal
      .Page1.oPag.oPCREGCES_1_24.enabled = i_bVal
      .Page1.oPag.oPCFLSTIN_1_25.enabled = i_bVal
      .Page1.oPag.oPCSTAINT_1_27.enabled = i_bVal
      .Page1.oPag.oPCPREFIS_1_28.enabled = i_bVal
      .Page2.oPag.oPCCONC01_2_3.enabled = i_bVal
      .Page2.oPag.oPCCONC02_2_4.enabled = i_bVal
      .Page2.oPag.oPCCONC03_2_5.enabled = i_bVal
      .Page2.oPag.oPCCONC04_2_6.enabled = i_bVal
      .Page2.oPag.oPCCONC05_2_7.enabled = i_bVal
      .Page2.oPag.oPCCONC06_2_8.enabled = i_bVal
      .Page2.oPag.oPCCONC07_2_9.enabled = i_bVal
      .Page2.oPag.oPCCONC08_2_10.enabled = i_bVal
      .Page2.oPag.oPCCONC09_2_11.enabled = i_bVal
      .Page2.oPag.oPCCONC10_2_12.enabled = i_bVal
      .Page2.oPag.oPCCONC11_2_13.enabled = i_bVal
      .Page2.oPag.oPCCONC12_2_14.enabled = i_bVal
      .Page2.oPag.oPCCONC13_2_15.enabled = i_bVal
      .Page2.oPag.oPCCONC14_2_16.enabled = i_bVal
      .Page2.oPag.oPCCONC15_2_17.enabled = i_bVal
      .Page2.oPag.oPCCONC16_2_18.enabled = i_bVal
      .Page2.oPag.oPCTIPC01_2_29.enabled = i_bVal
      .Page2.oPag.oPCTIPC02_2_30.enabled = i_bVal
      .Page2.oPag.oPCTIPC03_2_31.enabled = i_bVal
      .Page2.oPag.oPCTIPC04_2_32.enabled = i_bVal
      .Page2.oPag.oPCTIPC05_2_33.enabled = i_bVal
      .Page2.oPag.oPCTIPC06_2_34.enabled = i_bVal
      .Page2.oPag.oPCTIPC07_2_35.enabled = i_bVal
      .Page2.oPag.oPCTIPC08_2_36.enabled = i_bVal
      .Page2.oPag.oPCTIPC09_2_37.enabled = i_bVal
      .Page2.oPag.oPCTIPC10_2_38.enabled = i_bVal
      .Page1.oPag.oPCFLSALP_1_30.enabled = i_bVal
      .Page1.oPag.oPCFLCARR_1_31.enabled = i_bVal
      .Page2.oPag.oPCTIPC11_2_39.enabled = i_bVal
      .Page2.oPag.oPCTIPC12_2_40.enabled = i_bVal
      .Page2.oPag.oPCTIPC13_2_41.enabled = i_bVal
      .Page2.oPag.oPCTIPC14_2_42.enabled = i_bVal
      .Page2.oPag.oPCTIPC15_2_43.enabled = i_bVal
      .Page2.oPag.oPCTIPC16_2_44.enabled = i_bVal
      .Page1.oPag.oPCBIMMAM_1_34.enabled = i_bVal
      .Page1.oPag.oPCIASADO_1_38.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'PAR_CESP',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.PAR_CESP_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCCODAZI,"PCCODAZI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCPERESE,"PCPERESE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCPERRID,"PCPERRID",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCPERANT,"PCPERANT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCAMMABU,"PCAMMABU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCMAXANT,"PCMAXANT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCDATCON,"PCDATCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCPERTOL,"PCPERTOL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCSTAREG,"PCSTAREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCTIPAMM,"PCTIPAMM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCDATCOC,"PCDATCOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCREGCES,"PCREGCES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCFLSTIN,"PCFLSTIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCSTAINT,"PCSTAINT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCPREFIS,"PCPREFIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCCONC01,"PCCONC01",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCCONC02,"PCCONC02",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCCONC03,"PCCONC03",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCCONC04,"PCCONC04",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCCONC05,"PCCONC05",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCCONC06,"PCCONC06",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCCONC07,"PCCONC07",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCCONC08,"PCCONC08",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCCONC09,"PCCONC09",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCCONC10,"PCCONC10",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCCONC11,"PCCONC11",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCCONC12,"PCCONC12",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCCONC13,"PCCONC13",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCCONC14,"PCCONC14",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCCONC15,"PCCONC15",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCCONC16,"PCCONC16",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCTIPC01,"PCTIPC01",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCTIPC02,"PCTIPC02",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCTIPC03,"PCTIPC03",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCTIPC04,"PCTIPC04",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCTIPC05,"PCTIPC05",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCTIPC06,"PCTIPC06",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCTIPC07,"PCTIPC07",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCTIPC08,"PCTIPC08",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCTIPC09,"PCTIPC09",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCTIPC10,"PCTIPC10",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCFLSALP,"PCFLSALP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCFLCARR,"PCFLCARR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCTIPC11,"PCTIPC11",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCTIPC12,"PCTIPC12",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCTIPC13,"PCTIPC13",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCTIPC14,"PCTIPC14",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCTIPC15,"PCTIPC15",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCTIPC16,"PCTIPC16",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCBIMMAM,"PCBIMMAM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCIASADO,"PCIASADO",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.PAR_CESP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_CESP_IDX,2])
    i_lTable = "PAR_CESP"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.PAR_CESP_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.PAR_CESP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_CESP_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.PAR_CESP_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into PAR_CESP
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'PAR_CESP')
        i_extval=cp_InsertValODBCExtFlds(this,'PAR_CESP')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(PCCODAZI,PCPERESE,PCPERRID,PCPERANT,PCAMMABU"+;
                  ",PCMAXANT,PCDATCON,PCPERTOL,PCSTAREG,PCTIPAMM"+;
                  ",PCDATCOC,UTCC,UTCV,UTDC,UTDV"+;
                  ",PCREGCES,PCFLSTIN,PCSTAINT,PCPREFIS,PCCONC01"+;
                  ",PCCONC02,PCCONC03,PCCONC04,PCCONC05,PCCONC06"+;
                  ",PCCONC07,PCCONC08,PCCONC09,PCCONC10,PCCONC11"+;
                  ",PCCONC12,PCCONC13,PCCONC14,PCCONC15,PCCONC16"+;
                  ",PCTIPC01,PCTIPC02,PCTIPC03,PCTIPC04,PCTIPC05"+;
                  ",PCTIPC06,PCTIPC07,PCTIPC08,PCTIPC09,PCTIPC10"+;
                  ",PCFLSALP,PCFLCARR,PCTIPC11,PCTIPC12,PCTIPC13"+;
                  ",PCTIPC14,PCTIPC15,PCTIPC16,PCBIMMAM,PCIASADO "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBCNull(this.w_PCCODAZI)+;
                  ","+cp_ToStrODBC(this.w_PCPERESE)+;
                  ","+cp_ToStrODBC(this.w_PCPERRID)+;
                  ","+cp_ToStrODBC(this.w_PCPERANT)+;
                  ","+cp_ToStrODBC(this.w_PCAMMABU)+;
                  ","+cp_ToStrODBC(this.w_PCMAXANT)+;
                  ","+cp_ToStrODBCNull(this.w_PCDATCON)+;
                  ","+cp_ToStrODBC(this.w_PCPERTOL)+;
                  ","+cp_ToStrODBC(this.w_PCSTAREG)+;
                  ","+cp_ToStrODBC(this.w_PCTIPAMM)+;
                  ","+cp_ToStrODBC(this.w_PCDATCOC)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                  ","+cp_ToStrODBC(this.w_PCREGCES)+;
                  ","+cp_ToStrODBC(this.w_PCFLSTIN)+;
                  ","+cp_ToStrODBC(this.w_PCSTAINT)+;
                  ","+cp_ToStrODBC(this.w_PCPREFIS)+;
                  ","+cp_ToStrODBC(this.w_PCCONC01)+;
                  ","+cp_ToStrODBC(this.w_PCCONC02)+;
                  ","+cp_ToStrODBC(this.w_PCCONC03)+;
                  ","+cp_ToStrODBC(this.w_PCCONC04)+;
                  ","+cp_ToStrODBC(this.w_PCCONC05)+;
                  ","+cp_ToStrODBC(this.w_PCCONC06)+;
                  ","+cp_ToStrODBC(this.w_PCCONC07)+;
                  ","+cp_ToStrODBC(this.w_PCCONC08)+;
                  ","+cp_ToStrODBC(this.w_PCCONC09)+;
                  ","+cp_ToStrODBC(this.w_PCCONC10)+;
                  ","+cp_ToStrODBC(this.w_PCCONC11)+;
                  ","+cp_ToStrODBC(this.w_PCCONC12)+;
                  ","+cp_ToStrODBC(this.w_PCCONC13)+;
                  ","+cp_ToStrODBC(this.w_PCCONC14)+;
                  ","+cp_ToStrODBC(this.w_PCCONC15)+;
                  ","+cp_ToStrODBC(this.w_PCCONC16)+;
                  ","+cp_ToStrODBC(this.w_PCTIPC01)+;
                  ","+cp_ToStrODBC(this.w_PCTIPC02)+;
                  ","+cp_ToStrODBC(this.w_PCTIPC03)+;
                  ","+cp_ToStrODBC(this.w_PCTIPC04)+;
                  ","+cp_ToStrODBC(this.w_PCTIPC05)+;
                  ","+cp_ToStrODBC(this.w_PCTIPC06)+;
                  ","+cp_ToStrODBC(this.w_PCTIPC07)+;
                  ","+cp_ToStrODBC(this.w_PCTIPC08)+;
                  ","+cp_ToStrODBC(this.w_PCTIPC09)+;
                  ","+cp_ToStrODBC(this.w_PCTIPC10)+;
                  ","+cp_ToStrODBC(this.w_PCFLSALP)+;
                  ","+cp_ToStrODBC(this.w_PCFLCARR)+;
                  ","+cp_ToStrODBC(this.w_PCTIPC11)+;
                  ","+cp_ToStrODBC(this.w_PCTIPC12)+;
                  ","+cp_ToStrODBC(this.w_PCTIPC13)+;
                  ","+cp_ToStrODBC(this.w_PCTIPC14)+;
                  ","+cp_ToStrODBC(this.w_PCTIPC15)+;
                  ","+cp_ToStrODBC(this.w_PCTIPC16)+;
                  ","+cp_ToStrODBC(this.w_PCBIMMAM)+;
                  ","+cp_ToStrODBC(this.w_PCIASADO)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'PAR_CESP')
        i_extval=cp_InsertValVFPExtFlds(this,'PAR_CESP')
        cp_CheckDeletedKey(i_cTable,0,'PCCODAZI',this.w_PCCODAZI)
        INSERT INTO (i_cTable);
              (PCCODAZI,PCPERESE,PCPERRID,PCPERANT,PCAMMABU,PCMAXANT,PCDATCON,PCPERTOL,PCSTAREG,PCTIPAMM,PCDATCOC,UTCC,UTCV,UTDC,UTDV,PCREGCES,PCFLSTIN,PCSTAINT,PCPREFIS,PCCONC01,PCCONC02,PCCONC03,PCCONC04,PCCONC05,PCCONC06,PCCONC07,PCCONC08,PCCONC09,PCCONC10,PCCONC11,PCCONC12,PCCONC13,PCCONC14,PCCONC15,PCCONC16,PCTIPC01,PCTIPC02,PCTIPC03,PCTIPC04,PCTIPC05,PCTIPC06,PCTIPC07,PCTIPC08,PCTIPC09,PCTIPC10,PCFLSALP,PCFLCARR,PCTIPC11,PCTIPC12,PCTIPC13,PCTIPC14,PCTIPC15,PCTIPC16,PCBIMMAM,PCIASADO  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_PCCODAZI;
                  ,this.w_PCPERESE;
                  ,this.w_PCPERRID;
                  ,this.w_PCPERANT;
                  ,this.w_PCAMMABU;
                  ,this.w_PCMAXANT;
                  ,this.w_PCDATCON;
                  ,this.w_PCPERTOL;
                  ,this.w_PCSTAREG;
                  ,this.w_PCTIPAMM;
                  ,this.w_PCDATCOC;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
                  ,this.w_PCREGCES;
                  ,this.w_PCFLSTIN;
                  ,this.w_PCSTAINT;
                  ,this.w_PCPREFIS;
                  ,this.w_PCCONC01;
                  ,this.w_PCCONC02;
                  ,this.w_PCCONC03;
                  ,this.w_PCCONC04;
                  ,this.w_PCCONC05;
                  ,this.w_PCCONC06;
                  ,this.w_PCCONC07;
                  ,this.w_PCCONC08;
                  ,this.w_PCCONC09;
                  ,this.w_PCCONC10;
                  ,this.w_PCCONC11;
                  ,this.w_PCCONC12;
                  ,this.w_PCCONC13;
                  ,this.w_PCCONC14;
                  ,this.w_PCCONC15;
                  ,this.w_PCCONC16;
                  ,this.w_PCTIPC01;
                  ,this.w_PCTIPC02;
                  ,this.w_PCTIPC03;
                  ,this.w_PCTIPC04;
                  ,this.w_PCTIPC05;
                  ,this.w_PCTIPC06;
                  ,this.w_PCTIPC07;
                  ,this.w_PCTIPC08;
                  ,this.w_PCTIPC09;
                  ,this.w_PCTIPC10;
                  ,this.w_PCFLSALP;
                  ,this.w_PCFLCARR;
                  ,this.w_PCTIPC11;
                  ,this.w_PCTIPC12;
                  ,this.w_PCTIPC13;
                  ,this.w_PCTIPC14;
                  ,this.w_PCTIPC15;
                  ,this.w_PCTIPC16;
                  ,this.w_PCBIMMAM;
                  ,this.w_PCIASADO;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.PAR_CESP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_CESP_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.PAR_CESP_IDX,i_nConn)
      *
      * update PAR_CESP
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'PAR_CESP')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " PCPERESE="+cp_ToStrODBC(this.w_PCPERESE)+;
             ",PCPERRID="+cp_ToStrODBC(this.w_PCPERRID)+;
             ",PCPERANT="+cp_ToStrODBC(this.w_PCPERANT)+;
             ",PCAMMABU="+cp_ToStrODBC(this.w_PCAMMABU)+;
             ",PCMAXANT="+cp_ToStrODBC(this.w_PCMAXANT)+;
             ",PCDATCON="+cp_ToStrODBCNull(this.w_PCDATCON)+;
             ",PCPERTOL="+cp_ToStrODBC(this.w_PCPERTOL)+;
             ",PCSTAREG="+cp_ToStrODBC(this.w_PCSTAREG)+;
             ",PCTIPAMM="+cp_ToStrODBC(this.w_PCTIPAMM)+;
             ",PCDATCOC="+cp_ToStrODBC(this.w_PCDATCOC)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",PCREGCES="+cp_ToStrODBC(this.w_PCREGCES)+;
             ",PCFLSTIN="+cp_ToStrODBC(this.w_PCFLSTIN)+;
             ",PCSTAINT="+cp_ToStrODBC(this.w_PCSTAINT)+;
             ",PCPREFIS="+cp_ToStrODBC(this.w_PCPREFIS)+;
             ",PCCONC01="+cp_ToStrODBC(this.w_PCCONC01)+;
             ",PCCONC02="+cp_ToStrODBC(this.w_PCCONC02)+;
             ",PCCONC03="+cp_ToStrODBC(this.w_PCCONC03)+;
             ",PCCONC04="+cp_ToStrODBC(this.w_PCCONC04)+;
             ",PCCONC05="+cp_ToStrODBC(this.w_PCCONC05)+;
             ",PCCONC06="+cp_ToStrODBC(this.w_PCCONC06)+;
             ",PCCONC07="+cp_ToStrODBC(this.w_PCCONC07)+;
             ",PCCONC08="+cp_ToStrODBC(this.w_PCCONC08)+;
             ",PCCONC09="+cp_ToStrODBC(this.w_PCCONC09)+;
             ",PCCONC10="+cp_ToStrODBC(this.w_PCCONC10)+;
             ",PCCONC11="+cp_ToStrODBC(this.w_PCCONC11)+;
             ",PCCONC12="+cp_ToStrODBC(this.w_PCCONC12)+;
             ",PCCONC13="+cp_ToStrODBC(this.w_PCCONC13)+;
             ",PCCONC14="+cp_ToStrODBC(this.w_PCCONC14)+;
             ",PCCONC15="+cp_ToStrODBC(this.w_PCCONC15)+;
             ",PCCONC16="+cp_ToStrODBC(this.w_PCCONC16)+;
             ",PCTIPC01="+cp_ToStrODBC(this.w_PCTIPC01)+;
             ",PCTIPC02="+cp_ToStrODBC(this.w_PCTIPC02)+;
             ",PCTIPC03="+cp_ToStrODBC(this.w_PCTIPC03)+;
             ",PCTIPC04="+cp_ToStrODBC(this.w_PCTIPC04)+;
             ",PCTIPC05="+cp_ToStrODBC(this.w_PCTIPC05)+;
             ",PCTIPC06="+cp_ToStrODBC(this.w_PCTIPC06)+;
             ",PCTIPC07="+cp_ToStrODBC(this.w_PCTIPC07)+;
             ",PCTIPC08="+cp_ToStrODBC(this.w_PCTIPC08)+;
             ",PCTIPC09="+cp_ToStrODBC(this.w_PCTIPC09)+;
             ",PCTIPC10="+cp_ToStrODBC(this.w_PCTIPC10)+;
             ",PCFLSALP="+cp_ToStrODBC(this.w_PCFLSALP)+;
             ",PCFLCARR="+cp_ToStrODBC(this.w_PCFLCARR)+;
             ",PCTIPC11="+cp_ToStrODBC(this.w_PCTIPC11)+;
             ",PCTIPC12="+cp_ToStrODBC(this.w_PCTIPC12)+;
             ",PCTIPC13="+cp_ToStrODBC(this.w_PCTIPC13)+;
             ",PCTIPC14="+cp_ToStrODBC(this.w_PCTIPC14)+;
             ",PCTIPC15="+cp_ToStrODBC(this.w_PCTIPC15)+;
             ",PCTIPC16="+cp_ToStrODBC(this.w_PCTIPC16)+;
             ",PCBIMMAM="+cp_ToStrODBC(this.w_PCBIMMAM)+;
             ",PCIASADO="+cp_ToStrODBC(this.w_PCIASADO)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'PAR_CESP')
        i_cWhere = cp_PKFox(i_cTable  ,'PCCODAZI',this.w_PCCODAZI  )
        UPDATE (i_cTable) SET;
              PCPERESE=this.w_PCPERESE;
             ,PCPERRID=this.w_PCPERRID;
             ,PCPERANT=this.w_PCPERANT;
             ,PCAMMABU=this.w_PCAMMABU;
             ,PCMAXANT=this.w_PCMAXANT;
             ,PCDATCON=this.w_PCDATCON;
             ,PCPERTOL=this.w_PCPERTOL;
             ,PCSTAREG=this.w_PCSTAREG;
             ,PCTIPAMM=this.w_PCTIPAMM;
             ,PCDATCOC=this.w_PCDATCOC;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             ,PCREGCES=this.w_PCREGCES;
             ,PCFLSTIN=this.w_PCFLSTIN;
             ,PCSTAINT=this.w_PCSTAINT;
             ,PCPREFIS=this.w_PCPREFIS;
             ,PCCONC01=this.w_PCCONC01;
             ,PCCONC02=this.w_PCCONC02;
             ,PCCONC03=this.w_PCCONC03;
             ,PCCONC04=this.w_PCCONC04;
             ,PCCONC05=this.w_PCCONC05;
             ,PCCONC06=this.w_PCCONC06;
             ,PCCONC07=this.w_PCCONC07;
             ,PCCONC08=this.w_PCCONC08;
             ,PCCONC09=this.w_PCCONC09;
             ,PCCONC10=this.w_PCCONC10;
             ,PCCONC11=this.w_PCCONC11;
             ,PCCONC12=this.w_PCCONC12;
             ,PCCONC13=this.w_PCCONC13;
             ,PCCONC14=this.w_PCCONC14;
             ,PCCONC15=this.w_PCCONC15;
             ,PCCONC16=this.w_PCCONC16;
             ,PCTIPC01=this.w_PCTIPC01;
             ,PCTIPC02=this.w_PCTIPC02;
             ,PCTIPC03=this.w_PCTIPC03;
             ,PCTIPC04=this.w_PCTIPC04;
             ,PCTIPC05=this.w_PCTIPC05;
             ,PCTIPC06=this.w_PCTIPC06;
             ,PCTIPC07=this.w_PCTIPC07;
             ,PCTIPC08=this.w_PCTIPC08;
             ,PCTIPC09=this.w_PCTIPC09;
             ,PCTIPC10=this.w_PCTIPC10;
             ,PCFLSALP=this.w_PCFLSALP;
             ,PCFLCARR=this.w_PCFLCARR;
             ,PCTIPC11=this.w_PCTIPC11;
             ,PCTIPC12=this.w_PCTIPC12;
             ,PCTIPC13=this.w_PCTIPC13;
             ,PCTIPC14=this.w_PCTIPC14;
             ,PCTIPC15=this.w_PCTIPC15;
             ,PCTIPC16=this.w_PCTIPC16;
             ,PCBIMMAM=this.w_PCBIMMAM;
             ,PCIASADO=this.w_PCIASADO;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.PAR_CESP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_CESP_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.PAR_CESP_IDX,i_nConn)
      *
      * delete PAR_CESP
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'PCCODAZI',this.w_PCCODAZI  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.PAR_CESP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_CESP_IDX,2])
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,20,.t.)
        if .o_PCSTAINT<>.w_PCSTAINT
            .w_PCPREFIS = IIF(.w_PCSTAINT<>'S',SPACE(20),.w_PCPREFIS)
        endif
        .DoRTCalc(22,47,.t.)
        if .o_PCSTAREG<>.w_PCSTAREG
            .w_PCFLSALP = IIF(.w_PCSTAREG<>'C','N',.w_PCFLSALP)
        endif
        .DoRTCalc(49,55,.t.)
        if .o_PCREGCES<>.w_PCREGCES
            .w_PCBIMMAM = IIF(.w_PCREGCES='S','S','N')
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(57,58,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_MRJQRLTZFI()
    with this
          * --- Warning
          AVVERTI(this;
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oPCPREFIS_1_28.enabled = this.oPgFrm.Page1.oPag.oPCPREFIS_1_28.mCond()
    this.oPgFrm.Page1.oPag.oPCFLSALP_1_30.enabled = this.oPgFrm.Page1.oPag.oPCFLSALP_1_30.mCond()
    this.oPgFrm.Page1.oPag.oPCBIMMAM_1_34.enabled = this.oPgFrm.Page1.oPag.oPCBIMMAM_1_34.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("w_PCTIPAMM Changed")
          .Calculate_MRJQRLTZFI()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsce_apc
    * --- Inserito controllo nel caso in cui l'utente abbia disattivato il flag
    * --- Ias Adopter
    If cEvent = 'w_PCIASADO Changed' 
         if this.w_IASADO='S' And this.w_PCIASADO='N'
           ah_Errormsg("Verificare i criteri di ammortamento presenti nelle categorie cespiti ")
         endif
    Endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PCCODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PCCODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PCCODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZRAGAZI";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_PCCODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_PCCODAZI)
            select AZCODAZI,AZRAGAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PCCODAZI = NVL(_Link_.AZCODAZI,space(5))
      this.w_RAGAZI = NVL(_Link_.AZRAGAZI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_PCCODAZI = space(5)
      endif
      this.w_RAGAZI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PCCODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.AZIENDA_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_1.AZCODAZI as AZCODAZI101"+ ",link_1_1.AZRAGAZI as AZRAGAZI101"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_1 on PAR_CESP.PCCODAZI=link_1_1.AZCODAZI"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_1"
          i_cKey=i_cKey+'+" and PAR_CESP.PCCODAZI=link_1_1.AZCODAZI(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PCDATCON
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PCDATCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_PCDATCON)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_PCCODAZI);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_PCCODAZI;
                     ,'ESCODESE',trim(this.w_PCDATCON))
          select ESCODAZI,ESCODESE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PCDATCON)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PCDATCON) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oPCDATCON_1_8'),i_cWhere,'',"Esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_PCCODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_PCCODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PCDATCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_PCDATCON);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_PCCODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_PCCODAZI;
                       ,'ESCODESE',this.w_PCDATCON)
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PCDATCON = NVL(_Link_.ESCODESE,space(4))
    else
      if i_cCtrl<>'Load'
        this.w_PCDATCON = space(4)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PCDATCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oPCPERESE_1_3.value==this.w_PCPERESE)
      this.oPgFrm.Page1.oPag.oPCPERESE_1_3.value=this.w_PCPERESE
    endif
    if not(this.oPgFrm.Page1.oPag.oPCPERRID_1_4.value==this.w_PCPERRID)
      this.oPgFrm.Page1.oPag.oPCPERRID_1_4.value=this.w_PCPERRID
    endif
    if not(this.oPgFrm.Page1.oPag.oPCPERANT_1_5.value==this.w_PCPERANT)
      this.oPgFrm.Page1.oPag.oPCPERANT_1_5.value=this.w_PCPERANT
    endif
    if not(this.oPgFrm.Page1.oPag.oPCAMMABU_1_6.value==this.w_PCAMMABU)
      this.oPgFrm.Page1.oPag.oPCAMMABU_1_6.value=this.w_PCAMMABU
    endif
    if not(this.oPgFrm.Page1.oPag.oPCMAXANT_1_7.value==this.w_PCMAXANT)
      this.oPgFrm.Page1.oPag.oPCMAXANT_1_7.value=this.w_PCMAXANT
    endif
    if not(this.oPgFrm.Page1.oPag.oPCDATCON_1_8.value==this.w_PCDATCON)
      this.oPgFrm.Page1.oPag.oPCDATCON_1_8.value=this.w_PCDATCON
    endif
    if not(this.oPgFrm.Page1.oPag.oPCPERTOL_1_9.value==this.w_PCPERTOL)
      this.oPgFrm.Page1.oPag.oPCPERTOL_1_9.value=this.w_PCPERTOL
    endif
    if not(this.oPgFrm.Page1.oPag.oPCSTAREG_1_10.RadioValue()==this.w_PCSTAREG)
      this.oPgFrm.Page1.oPag.oPCSTAREG_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPCTIPAMM_1_11.RadioValue()==this.w_PCTIPAMM)
      this.oPgFrm.Page1.oPag.oPCTIPAMM_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPCDATCOC_1_12.RadioValue()==this.w_PCDATCOC)
      this.oPgFrm.Page1.oPag.oPCDATCOC_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPCREGCES_1_24.RadioValue()==this.w_PCREGCES)
      this.oPgFrm.Page1.oPag.oPCREGCES_1_24.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPCFLSTIN_1_25.RadioValue()==this.w_PCFLSTIN)
      this.oPgFrm.Page1.oPag.oPCFLSTIN_1_25.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPCSTAINT_1_27.RadioValue()==this.w_PCSTAINT)
      this.oPgFrm.Page1.oPag.oPCSTAINT_1_27.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPCPREFIS_1_28.value==this.w_PCPREFIS)
      this.oPgFrm.Page1.oPag.oPCPREFIS_1_28.value=this.w_PCPREFIS
    endif
    if not(this.oPgFrm.Page2.oPag.oPCCONC01_2_3.value==this.w_PCCONC01)
      this.oPgFrm.Page2.oPag.oPCCONC01_2_3.value=this.w_PCCONC01
    endif
    if not(this.oPgFrm.Page2.oPag.oPCCONC02_2_4.value==this.w_PCCONC02)
      this.oPgFrm.Page2.oPag.oPCCONC02_2_4.value=this.w_PCCONC02
    endif
    if not(this.oPgFrm.Page2.oPag.oPCCONC03_2_5.value==this.w_PCCONC03)
      this.oPgFrm.Page2.oPag.oPCCONC03_2_5.value=this.w_PCCONC03
    endif
    if not(this.oPgFrm.Page2.oPag.oPCCONC04_2_6.value==this.w_PCCONC04)
      this.oPgFrm.Page2.oPag.oPCCONC04_2_6.value=this.w_PCCONC04
    endif
    if not(this.oPgFrm.Page2.oPag.oPCCONC05_2_7.value==this.w_PCCONC05)
      this.oPgFrm.Page2.oPag.oPCCONC05_2_7.value=this.w_PCCONC05
    endif
    if not(this.oPgFrm.Page2.oPag.oPCCONC06_2_8.value==this.w_PCCONC06)
      this.oPgFrm.Page2.oPag.oPCCONC06_2_8.value=this.w_PCCONC06
    endif
    if not(this.oPgFrm.Page2.oPag.oPCCONC07_2_9.value==this.w_PCCONC07)
      this.oPgFrm.Page2.oPag.oPCCONC07_2_9.value=this.w_PCCONC07
    endif
    if not(this.oPgFrm.Page2.oPag.oPCCONC08_2_10.value==this.w_PCCONC08)
      this.oPgFrm.Page2.oPag.oPCCONC08_2_10.value=this.w_PCCONC08
    endif
    if not(this.oPgFrm.Page2.oPag.oPCCONC09_2_11.value==this.w_PCCONC09)
      this.oPgFrm.Page2.oPag.oPCCONC09_2_11.value=this.w_PCCONC09
    endif
    if not(this.oPgFrm.Page2.oPag.oPCCONC10_2_12.value==this.w_PCCONC10)
      this.oPgFrm.Page2.oPag.oPCCONC10_2_12.value=this.w_PCCONC10
    endif
    if not(this.oPgFrm.Page2.oPag.oPCCONC11_2_13.value==this.w_PCCONC11)
      this.oPgFrm.Page2.oPag.oPCCONC11_2_13.value=this.w_PCCONC11
    endif
    if not(this.oPgFrm.Page2.oPag.oPCCONC12_2_14.value==this.w_PCCONC12)
      this.oPgFrm.Page2.oPag.oPCCONC12_2_14.value=this.w_PCCONC12
    endif
    if not(this.oPgFrm.Page2.oPag.oPCCONC13_2_15.value==this.w_PCCONC13)
      this.oPgFrm.Page2.oPag.oPCCONC13_2_15.value=this.w_PCCONC13
    endif
    if not(this.oPgFrm.Page2.oPag.oPCCONC14_2_16.value==this.w_PCCONC14)
      this.oPgFrm.Page2.oPag.oPCCONC14_2_16.value=this.w_PCCONC14
    endif
    if not(this.oPgFrm.Page2.oPag.oPCCONC15_2_17.value==this.w_PCCONC15)
      this.oPgFrm.Page2.oPag.oPCCONC15_2_17.value=this.w_PCCONC15
    endif
    if not(this.oPgFrm.Page2.oPag.oPCCONC16_2_18.value==this.w_PCCONC16)
      this.oPgFrm.Page2.oPag.oPCCONC16_2_18.value=this.w_PCCONC16
    endif
    if not(this.oPgFrm.Page2.oPag.oPCTIPC01_2_29.RadioValue()==this.w_PCTIPC01)
      this.oPgFrm.Page2.oPag.oPCTIPC01_2_29.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPCTIPC02_2_30.RadioValue()==this.w_PCTIPC02)
      this.oPgFrm.Page2.oPag.oPCTIPC02_2_30.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPCTIPC03_2_31.RadioValue()==this.w_PCTIPC03)
      this.oPgFrm.Page2.oPag.oPCTIPC03_2_31.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPCTIPC04_2_32.RadioValue()==this.w_PCTIPC04)
      this.oPgFrm.Page2.oPag.oPCTIPC04_2_32.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPCTIPC05_2_33.RadioValue()==this.w_PCTIPC05)
      this.oPgFrm.Page2.oPag.oPCTIPC05_2_33.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPCTIPC06_2_34.RadioValue()==this.w_PCTIPC06)
      this.oPgFrm.Page2.oPag.oPCTIPC06_2_34.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPCTIPC07_2_35.RadioValue()==this.w_PCTIPC07)
      this.oPgFrm.Page2.oPag.oPCTIPC07_2_35.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPCTIPC08_2_36.RadioValue()==this.w_PCTIPC08)
      this.oPgFrm.Page2.oPag.oPCTIPC08_2_36.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPCTIPC09_2_37.RadioValue()==this.w_PCTIPC09)
      this.oPgFrm.Page2.oPag.oPCTIPC09_2_37.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPCTIPC10_2_38.RadioValue()==this.w_PCTIPC10)
      this.oPgFrm.Page2.oPag.oPCTIPC10_2_38.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPCFLSALP_1_30.RadioValue()==this.w_PCFLSALP)
      this.oPgFrm.Page1.oPag.oPCFLSALP_1_30.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPCFLCARR_1_31.RadioValue()==this.w_PCFLCARR)
      this.oPgFrm.Page1.oPag.oPCFLCARR_1_31.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPCTIPC11_2_39.RadioValue()==this.w_PCTIPC11)
      this.oPgFrm.Page2.oPag.oPCTIPC11_2_39.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPCTIPC12_2_40.RadioValue()==this.w_PCTIPC12)
      this.oPgFrm.Page2.oPag.oPCTIPC12_2_40.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPCTIPC13_2_41.RadioValue()==this.w_PCTIPC13)
      this.oPgFrm.Page2.oPag.oPCTIPC13_2_41.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPCTIPC14_2_42.RadioValue()==this.w_PCTIPC14)
      this.oPgFrm.Page2.oPag.oPCTIPC14_2_42.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPCTIPC15_2_43.RadioValue()==this.w_PCTIPC15)
      this.oPgFrm.Page2.oPag.oPCTIPC15_2_43.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPCTIPC16_2_44.RadioValue()==this.w_PCTIPC16)
      this.oPgFrm.Page2.oPag.oPCTIPC16_2_44.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPCBIMMAM_1_34.RadioValue()==this.w_PCBIMMAM)
      this.oPgFrm.Page1.oPag.oPCBIMMAM_1_34.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPCIASADO_1_38.RadioValue()==this.w_PCIASADO)
      this.oPgFrm.Page1.oPag.oPCIASADO_1_38.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'PAR_CESP')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PCSTAREG = this.w_PCSTAREG
    this.o_PCREGCES = this.w_PCREGCES
    this.o_PCSTAINT = this.w_PCSTAINT
    return

enddefine

* --- Define pages as container
define class tgsce_apcPag1 as StdContainer
  Width  = 480
  height = 475
  stdWidth  = 480
  stdheight = 475
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPCPERESE_1_3 as StdField with uid="BQIBMGAJJQ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_PCPERESE", cQueryName = "PCPERESE",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Riduzione 1^ esercizio per ammortamento fiscale",;
    HelpContextID = 14680891,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=190, Top=20, cSayPict='"999.99"', cGetPict='"999.99"'

  add object oPCPERRID_1_4 as StdField with uid="KUALONDWBY",rtseq=4,rtrep=.f.,;
    cFormVar = "w_PCPERRID", cQueryName = "PCPERRID",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Limite minimo % amm. ordinario per il calcolo delle quote perse",;
    HelpContextID = 35650758,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=418, Top=20, cSayPict='"999.99"', cGetPict='"999.99"'

  add object oPCPERANT_1_5 as StdField with uid="GEXXEJYFUK",rtseq=5,rtrep=.f.,;
    cFormVar = "w_PCPERANT", cQueryName = "PCPERANT",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Limite massimo % amm. anticipato rispetto alla % ordinaria",;
    HelpContextID = 52427958,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=190, Top=51, cSayPict='"999.99"', cGetPict='"999.99"'

  add object oPCAMMABU_1_6 as StdField with uid="FOBIOODWPT",rtseq=6,rtrep=.f.,;
    cFormVar = "w_PCAMMABU", cQueryName = "PCAMMABU",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero ammortamenti anticipati per beni usati",;
    HelpContextID = 211227467,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=418, Top=51, cSayPict='"99"', cGetPict='"99"'

  add object oPCMAXANT_1_7 as StdField with uid="NUYGAAODCS",rtseq=7,rtrep=.f.,;
    cFormVar = "w_PCMAXANT", cQueryName = "PCMAXANT",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Massimo numero di ammortamenti anticipati",;
    HelpContextID = 46410934,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=190, Top=82, cSayPict='"99"', cGetPict='"99"'

  add object oPCDATCON_1_8 as StdField with uid="IJZFPTHJGW",rtseq=8,rtrep=.f.,;
    cFormVar = "w_PCDATCON", cQueryName = "PCDATCON",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Esercizio di consolidamento dei movimenti cespiti",;
    HelpContextID = 17087676,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=418, Top=82, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_PCCODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_PCDATCON"

  func oPCDATCON_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oPCDATCON_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPCDATCON_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_PCCODAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_PCCODAZI)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oPCDATCON_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Esercizi",'',this.parent.oContained
  endproc

  add object oPCPERTOL_1_9 as StdField with uid="SWOYOIYYTM",rtseq=9,rtrep=.f.,;
    cFormVar = "w_PCPERTOL", cQueryName = "PCPERTOL",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Percentuale di tolleranza per l'ultimo anno di ammortamento civile",;
    HelpContextID = 2096318,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=418, Top=112, cSayPict='"999.99"', cGetPict='"999.99"'


  add object oPCSTAREG_1_10 as StdCombo with uid="ZTOFYALKDP",rtseq=10,rtrep=.f.,left=318,top=153,width=133,height=21;
    , ToolTipText = "Stampa registro cespiti ragruppata per cespite oppure per categoria";
    , HelpContextID = 215954237;
    , cFormVar="w_PCSTAREG",RowSource=""+"Cespite,"+"Categoria,"+"Gruppo contabile", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPCSTAREG_1_10.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'A',;
    iif(this.value =3,'G',;
    space(1)))))
  endfunc
  func oPCSTAREG_1_10.GetRadio()
    this.Parent.oContained.w_PCSTAREG = this.RadioValue()
    return .t.
  endfunc

  func oPCSTAREG_1_10.SetRadio()
    this.Parent.oContained.w_PCSTAREG=trim(this.Parent.oContained.w_PCSTAREG)
    this.value = ;
      iif(this.Parent.oContained.w_PCSTAREG=='C',1,;
      iif(this.Parent.oContained.w_PCSTAREG=='A',2,;
      iif(this.Parent.oContained.w_PCSTAREG=='G',3,;
      0)))
  endfunc


  add object oPCTIPAMM_1_11 as StdCombo with uid="FWVQRWGPGD",rtseq=11,rtrep=.f.,left=318,top=187,width=133,height=21;
    , ToolTipText = "Tipo di ammortamento";
    , HelpContextID = 54246589;
    , cFormVar="w_PCTIPAMM",RowSource=""+"Civile e fiscale,"+"Solo civile,"+"Solo fiscale", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPCTIPAMM_1_11.RadioValue()
    return(iif(this.value =1,'E',;
    iif(this.value =2,'C',;
    iif(this.value =3,'F',;
    space(1)))))
  endfunc
  func oPCTIPAMM_1_11.GetRadio()
    this.Parent.oContained.w_PCTIPAMM = this.RadioValue()
    return .t.
  endfunc

  func oPCTIPAMM_1_11.SetRadio()
    this.Parent.oContained.w_PCTIPAMM=trim(this.Parent.oContained.w_PCTIPAMM)
    this.value = ;
      iif(this.Parent.oContained.w_PCTIPAMM=='E',1,;
      iif(this.Parent.oContained.w_PCTIPAMM=='C',2,;
      iif(this.Parent.oContained.w_PCTIPAMM=='F',3,;
      0)))
  endfunc


  add object oPCDATCOC_1_12 as StdCombo with uid="HJTSWNPZJT",rtseq=12,rtrep=.f.,left=318,top=221,width=133,height=21;
    , ToolTipText = "Modalit� di calcolo giorni per costo complementare";
    , HelpContextID = 17087687;
    , cFormVar="w_PCDATCOC",RowSource=""+"Data primo utilizzo,"+"Data registrazione", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPCDATCOC_1_12.RadioValue()
    return(iif(this.value =1,'U',;
    iif(this.value =2,'R',;
    space(1))))
  endfunc
  func oPCDATCOC_1_12.GetRadio()
    this.Parent.oContained.w_PCDATCOC = this.RadioValue()
    return .t.
  endfunc

  func oPCDATCOC_1_12.SetRadio()
    this.Parent.oContained.w_PCDATCOC=trim(this.Parent.oContained.w_PCDATCOC)
    this.value = ;
      iif(this.Parent.oContained.w_PCDATCOC=='U',1,;
      iif(this.Parent.oContained.w_PCDATCOC=='R',2,;
      0))
  endfunc

  add object oPCREGCES_1_24 as StdCheck with uid="UQDEJGDVBY",rtseq=18,rtrep=.f.,left=17, top=264, caption="Stampa tutti i cespiti",;
    ToolTipText = "Se attivato, sul libro cespiti, stampa anche quelli non movimentati",;
    HelpContextID = 238035785,;
    cFormVar="w_PCREGCES", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPCREGCES_1_24.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPCREGCES_1_24.GetRadio()
    this.Parent.oContained.w_PCREGCES = this.RadioValue()
    return .t.
  endfunc

  func oPCREGCES_1_24.SetRadio()
    this.Parent.oContained.w_PCREGCES=trim(this.Parent.oContained.w_PCREGCES)
    this.value = ;
      iif(this.Parent.oContained.w_PCREGCES=='S',1,;
      0)
  endfunc

  add object oPCFLSTIN_1_25 as StdCheck with uid="GQEHXPPYJE",rtseq=19,rtrep=.f.,left=17, top=292, caption="Evidenzia anche ammortamento non deducibile su registro cespiti",;
    ToolTipText = "Evidenzia ammortamento non deducibile nella stampa registro cespiti",;
    HelpContextID = 629948,;
    cFormVar="w_PCFLSTIN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPCFLSTIN_1_25.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPCFLSTIN_1_25.GetRadio()
    this.Parent.oContained.w_PCFLSTIN = this.RadioValue()
    return .t.
  endfunc

  func oPCFLSTIN_1_25.SetRadio()
    this.Parent.oContained.w_PCFLSTIN=trim(this.Parent.oContained.w_PCFLSTIN)
    this.value = ;
      iif(this.Parent.oContained.w_PCFLSTIN=='S',1,;
      0)
  endfunc

  add object oPCSTAINT_1_27 as StdCheck with uid="USEQOKLUPQ",rtseq=20,rtrep=.f.,left=17, top=320, caption="Stampa intestazione nel registro",;
    ToolTipText = "Attivato: stampa intestazione e numerazione pagine nel registro cespiti",;
    HelpContextID = 203476150,;
    cFormVar="w_PCSTAINT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPCSTAINT_1_27.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oPCSTAINT_1_27.GetRadio()
    this.Parent.oContained.w_PCSTAINT = this.RadioValue()
    return .t.
  endfunc

  func oPCSTAINT_1_27.SetRadio()
    this.Parent.oContained.w_PCSTAINT=trim(this.Parent.oContained.w_PCSTAINT)
    this.value = ;
      iif(this.Parent.oContained.w_PCSTAINT=='S',1,;
      0)
  endfunc

  add object oPCPREFIS_1_28 as StdField with uid="PQCCZJCSQJ",rtseq=21,rtrep=.f.,;
    cFormVar = "w_PCPREFIS", cQueryName = "PCPREFIS",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Prefisso per numerazione pagine",;
    HelpContextID = 249756855,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=318, Top=348, InputMask=replicate('X',20)

  func oPCPREFIS_1_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PCSTAINT='S')
    endwith
   endif
  endfunc

  add object oPCFLSALP_1_30 as StdCheck with uid="MWIHAVCAQX",rtseq=48,rtrep=.f.,left=17, top=380, caption="Salto pagina a cambio categoria su registro cespiti (stampa per cespiti)",;
    ToolTipText = "Effettua salto pagina al cambio della categoria",;
    HelpContextID = 50961594,;
    cFormVar="w_PCFLSALP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPCFLSALP_1_30.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPCFLSALP_1_30.GetRadio()
    this.Parent.oContained.w_PCFLSALP = this.RadioValue()
    return .t.
  endfunc

  func oPCFLSALP_1_30.SetRadio()
    this.Parent.oContained.w_PCFLSALP=trim(this.Parent.oContained.w_PCFLSALP)
    this.value = ;
      iif(this.Parent.oContained.w_PCFLSALP=='S',1,;
      0)
  endfunc

  func oPCFLSALP_1_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PCSTAREG = 'C')
    endwith
   endif
  endfunc

  add object oPCFLCARR_1_31 as StdCheck with uid="QUUAABBXFL",rtseq=49,rtrep=.f.,left=17, top=405, caption="Abilita il caricamento rapido cespiti",;
    ToolTipText = "Abilita il caricamento rapido cespiti",;
    HelpContextID = 200696648,;
    cFormVar="w_PCFLCARR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPCFLCARR_1_31.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPCFLCARR_1_31.GetRadio()
    this.Parent.oContained.w_PCFLCARR = this.RadioValue()
    return .t.
  endfunc

  func oPCFLCARR_1_31.SetRadio()
    this.Parent.oContained.w_PCFLCARR=trim(this.Parent.oContained.w_PCFLCARR)
    this.value = ;
      iif(this.Parent.oContained.w_PCFLCARR=='S',1,;
      0)
  endfunc

  add object oPCBIMMAM_1_34 as StdCheck with uid="APYZUPPHFQ",rtseq=56,rtrep=.f.,left=17, top=430, caption="Stampa beni immateriali",;
    ToolTipText = "Se attivo stampa i beni immateriali non movimentati nell'esercizio",;
    HelpContextID = 143860547,;
    cFormVar="w_PCBIMMAM", bObbl = .f. , nPag = 1;
    , tabstop=.F.;
   , bGlobalFont=.t.


  func oPCBIMMAM_1_34.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPCBIMMAM_1_34.GetRadio()
    this.Parent.oContained.w_PCBIMMAM = this.RadioValue()
    return .t.
  endfunc

  func oPCBIMMAM_1_34.SetRadio()
    this.Parent.oContained.w_PCBIMMAM=trim(this.Parent.oContained.w_PCBIMMAM)
    this.value = ;
      iif(this.Parent.oContained.w_PCBIMMAM=='S',1,;
      0)
  endfunc

  func oPCBIMMAM_1_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PCREGCES='S')
    endwith
   endif
  endfunc

  add object oPCIASADO_1_38 as StdCheck with uid="UHESOXRLHM",rtseq=58,rtrep=.f.,left=17, top=455, caption="Ias Adopter",;
    ToolTipText = "Se attivo per i beni materiali � possibile impostare come tipo ammortamento rateo utilizzo",;
    HelpContextID = 216765253,;
    cFormVar="w_PCIASADO", bObbl = .f. , nPag = 1;
    , tabstop=.F.;
   , bGlobalFont=.t.


  func oPCIASADO_1_38.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPCIASADO_1_38.GetRadio()
    this.Parent.oContained.w_PCIASADO = this.RadioValue()
    return .t.
  endfunc

  func oPCIASADO_1_38.SetRadio()
    this.Parent.oContained.w_PCIASADO=trim(this.Parent.oContained.w_PCIASADO)
    this.value = ;
      iif(this.Parent.oContained.w_PCIASADO=='S',1,;
      0)
  endfunc

  add object oStr_1_13 as StdString with uid="NQSITSNGGJ",Visible=.t., Left=4, Top=20,;
    Alignment=1, Width=184, Height=15,;
    Caption="% Riduzione 1^ esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="HIGYNQTOKT",Visible=.t., Left=4, Top=51,;
    Alignment=1, Width=184, Height=15,;
    Caption="% Ammortamento anticipato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="OGAFAUQLIS",Visible=.t., Left=4, Top=82,;
    Alignment=1, Width=184, Height=15,;
    Caption="N.max.ammortamenti anticipati:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="MYJLRQNOID",Visible=.t., Left=252, Top=51,;
    Alignment=1, Width=164, Height=15,;
    Caption="N.amm.antic.beni usati:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="QMBPVTUVFN",Visible=.t., Left=252, Top=82,;
    Alignment=1, Width=164, Height=15,;
    Caption="Esercizio consolidamento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="IKWNPJMVAP",Visible=.t., Left=272, Top=20,;
    Alignment=1, Width=144, Height=15,;
    Caption="% Limite minimo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="FPWXYGWQDA",Visible=.t., Left=59, Top=157,;
    Alignment=1, Width=256, Height=18,;
    Caption="Stampa registro cespiti raggruppata per:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="XKSPIADXBU",Visible=.t., Left=135, Top=349,;
    Alignment=1, Width=180, Height=18,;
    Caption="Prefisso per num. pagine:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="AHQRVLTBSZ",Visible=.t., Left=155, Top=189,;
    Alignment=1, Width=160, Height=18,;
    Caption="Tipo di ammortamento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="HHWTYURYWB",Visible=.t., Left=252, Top=113,;
    Alignment=1, Width=164, Height=18,;
    Caption="Percentuale di tolleranza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="IEASRYCTMA",Visible=.t., Left=41, Top=222,;
    Alignment=1, Width=274, Height=18,;
    Caption="Data calcolo giorni per costo complementare:"  ;
  , bGlobalFont=.t.
enddefine
define class tgsce_apcPag2 as StdContainer
  Width  = 480
  height = 475
  stdWidth  = 480
  stdheight = 475
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPCCONC01_2_3 as StdField with uid="ZDIVIHESGW",rtseq=22,rtrep=.f.,;
    cFormVar = "w_PCCONC01", cQueryName = "PCCONC01",;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Decodifica contropartita contabile",;
    HelpContextID = 22465753,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=56, Top=49, InputMask=replicate('X',30)

  add object oPCCONC02_2_4 as StdField with uid="UMWYWGVOYH",rtseq=23,rtrep=.f.,;
    cFormVar = "w_PCCONC02", cQueryName = "PCCONC02",;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Decodifica contropartita contabile",;
    HelpContextID = 22465752,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=56, Top=75, InputMask=replicate('X',30)

  add object oPCCONC03_2_5 as StdField with uid="XQTTZPWONE",rtseq=24,rtrep=.f.,;
    cFormVar = "w_PCCONC03", cQueryName = "PCCONC03",;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Decodifica contropartita contabile",;
    HelpContextID = 22465751,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=56, Top=101, InputMask=replicate('X',30)

  add object oPCCONC04_2_6 as StdField with uid="GEIOOGYIWA",rtseq=25,rtrep=.f.,;
    cFormVar = "w_PCCONC04", cQueryName = "PCCONC04",;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Decodifica contropartita contabile",;
    HelpContextID = 22465750,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=56, Top=127, InputMask=replicate('X',30)

  add object oPCCONC05_2_7 as StdField with uid="DMBKWILYWV",rtseq=26,rtrep=.f.,;
    cFormVar = "w_PCCONC05", cQueryName = "PCCONC05",;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Decodifica contropartita contabile",;
    HelpContextID = 22465749,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=56, Top=153, InputMask=replicate('X',30)

  add object oPCCONC06_2_8 as StdField with uid="SSTLFAXWXJ",rtseq=27,rtrep=.f.,;
    cFormVar = "w_PCCONC06", cQueryName = "PCCONC06",;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Decodifica contropartita contabile",;
    HelpContextID = 22465748,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=56, Top=179, InputMask=replicate('X',30)

  add object oPCCONC07_2_9 as StdField with uid="QLLYHJTRBL",rtseq=28,rtrep=.f.,;
    cFormVar = "w_PCCONC07", cQueryName = "PCCONC07",;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Decodifica contropartita contabile",;
    HelpContextID = 22465747,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=56, Top=205, InputMask=replicate('X',30)

  add object oPCCONC08_2_10 as StdField with uid="MKWNTGNNPN",rtseq=29,rtrep=.f.,;
    cFormVar = "w_PCCONC08", cQueryName = "PCCONC08",;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Decodifica contropartita contabile",;
    HelpContextID = 22465746,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=56, Top=231, InputMask=replicate('X',30)

  add object oPCCONC09_2_11 as StdField with uid="HZEFVYHNJJ",rtseq=30,rtrep=.f.,;
    cFormVar = "w_PCCONC09", cQueryName = "PCCONC09",;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Decodifica contropartita contabile",;
    HelpContextID = 22465745,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=56, Top=257, InputMask=replicate('X',30)

  add object oPCCONC10_2_12 as StdField with uid="FNLTZCLGTC",rtseq=31,rtrep=.f.,;
    cFormVar = "w_PCCONC10", cQueryName = "PCCONC10",;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Decodifica contropartita contabile",;
    HelpContextID = 22465754,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=56, Top=283, InputMask=replicate('X',30)

  add object oPCCONC11_2_13 as StdField with uid="UITQRKDJQO",rtseq=32,rtrep=.f.,;
    cFormVar = "w_PCCONC11", cQueryName = "PCCONC11",;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Decodifica contropartita contabile",;
    HelpContextID = 22465753,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=56, Top=309, InputMask=replicate('X',30)

  add object oPCCONC12_2_14 as StdField with uid="MHFSAAHGVY",rtseq=33,rtrep=.f.,;
    cFormVar = "w_PCCONC12", cQueryName = "PCCONC12",;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Decodifica contropartita contabile",;
    HelpContextID = 22465752,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=56, Top=335, InputMask=replicate('X',30)

  add object oPCCONC13_2_15 as StdField with uid="YHQDUIOLMC",rtseq=34,rtrep=.f.,;
    cFormVar = "w_PCCONC13", cQueryName = "PCCONC13",;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Decodifica contropartita contabile",;
    HelpContextID = 22465751,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=56, Top=361, InputMask=replicate('X',30)

  add object oPCCONC14_2_16 as StdField with uid="GMHPCNXPQA",rtseq=35,rtrep=.f.,;
    cFormVar = "w_PCCONC14", cQueryName = "PCCONC14",;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Decodifica contropartita contabile",;
    HelpContextID = 22465750,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=56, Top=387, InputMask=replicate('X',30)

  add object oPCCONC15_2_17 as StdField with uid="BQYGUFUORU",rtseq=36,rtrep=.f.,;
    cFormVar = "w_PCCONC15", cQueryName = "PCCONC15",;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Decodifica contropartita contabile",;
    HelpContextID = 22465749,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=56, Top=413, InputMask=replicate('X',30)

  add object oPCCONC16_2_18 as StdField with uid="TCORBPYWOQ",rtseq=37,rtrep=.f.,;
    cFormVar = "w_PCCONC16", cQueryName = "PCCONC16",;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Decodifica contropartita contabile",;
    HelpContextID = 22465748,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=56, Top=439, InputMask=replicate('X',30)


  add object oPCTIPC01_2_29 as StdCombo with uid="QGKRTMYYYN",rtseq=38,rtrep=.f.,left=281,top=49,width=169,height=21;
    , ToolTipText = "Rappresenta la sezione di bilancio per la stampa verifica contabile";
    , HelpContextID = 20692185;
    , cFormVar="w_PCTIPC01",RowSource=""+"Immobilizzazioni,"+"Fondo amm. civile,"+"Fondo amm. fiscale,"+"Altro", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oPCTIPC01_2_29.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'C',;
    iif(this.value =3,'F',;
    iif(this.value =4,'A',;
    space(1))))))
  endfunc
  func oPCTIPC01_2_29.GetRadio()
    this.Parent.oContained.w_PCTIPC01 = this.RadioValue()
    return .t.
  endfunc

  func oPCTIPC01_2_29.SetRadio()
    this.Parent.oContained.w_PCTIPC01=trim(this.Parent.oContained.w_PCTIPC01)
    this.value = ;
      iif(this.Parent.oContained.w_PCTIPC01=='I',1,;
      iif(this.Parent.oContained.w_PCTIPC01=='C',2,;
      iif(this.Parent.oContained.w_PCTIPC01=='F',3,;
      iif(this.Parent.oContained.w_PCTIPC01=='A',4,;
      0))))
  endfunc


  add object oPCTIPC02_2_30 as StdCombo with uid="ZRBZKKDZXZ",rtseq=39,rtrep=.f.,left=281,top=75,width=169,height=21;
    , ToolTipText = "Rappresenta la sezione di bilancio per la stampa verifica contabile";
    , HelpContextID = 20692184;
    , cFormVar="w_PCTIPC02",RowSource=""+"Immobilizzazioni,"+"Fondo amm. civile,"+"Fondo amm. fiscale,"+"Altro", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oPCTIPC02_2_30.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'C',;
    iif(this.value =3,'F',;
    iif(this.value =4,'A',;
    space(1))))))
  endfunc
  func oPCTIPC02_2_30.GetRadio()
    this.Parent.oContained.w_PCTIPC02 = this.RadioValue()
    return .t.
  endfunc

  func oPCTIPC02_2_30.SetRadio()
    this.Parent.oContained.w_PCTIPC02=trim(this.Parent.oContained.w_PCTIPC02)
    this.value = ;
      iif(this.Parent.oContained.w_PCTIPC02=='I',1,;
      iif(this.Parent.oContained.w_PCTIPC02=='C',2,;
      iif(this.Parent.oContained.w_PCTIPC02=='F',3,;
      iif(this.Parent.oContained.w_PCTIPC02=='A',4,;
      0))))
  endfunc


  add object oPCTIPC03_2_31 as StdCombo with uid="TIWGSFSHOO",rtseq=40,rtrep=.f.,left=281,top=101,width=169,height=21;
    , ToolTipText = "Rappresenta la sezione di bilancio per la stampa verifica contabile";
    , HelpContextID = 20692183;
    , cFormVar="w_PCTIPC03",RowSource=""+"Immobilizzazioni,"+"Fondo amm. civile,"+"Fondo amm. fiscale,"+"Altro", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oPCTIPC03_2_31.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'C',;
    iif(this.value =3,'F',;
    iif(this.value =4,'A',;
    space(1))))))
  endfunc
  func oPCTIPC03_2_31.GetRadio()
    this.Parent.oContained.w_PCTIPC03 = this.RadioValue()
    return .t.
  endfunc

  func oPCTIPC03_2_31.SetRadio()
    this.Parent.oContained.w_PCTIPC03=trim(this.Parent.oContained.w_PCTIPC03)
    this.value = ;
      iif(this.Parent.oContained.w_PCTIPC03=='I',1,;
      iif(this.Parent.oContained.w_PCTIPC03=='C',2,;
      iif(this.Parent.oContained.w_PCTIPC03=='F',3,;
      iif(this.Parent.oContained.w_PCTIPC03=='A',4,;
      0))))
  endfunc


  add object oPCTIPC04_2_32 as StdCombo with uid="PYVHGOOFGE",rtseq=41,rtrep=.f.,left=281,top=127,width=169,height=21;
    , ToolTipText = "Rappresenta la sezione di bilancio per la stampa verifica contabile";
    , HelpContextID = 20692182;
    , cFormVar="w_PCTIPC04",RowSource=""+"Immobilizzazioni,"+"Fondo amm. civile,"+"Fondo amm. fiscale,"+"Altro", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oPCTIPC04_2_32.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'C',;
    iif(this.value =3,'F',;
    iif(this.value =4,'A',;
    space(1))))))
  endfunc
  func oPCTIPC04_2_32.GetRadio()
    this.Parent.oContained.w_PCTIPC04 = this.RadioValue()
    return .t.
  endfunc

  func oPCTIPC04_2_32.SetRadio()
    this.Parent.oContained.w_PCTIPC04=trim(this.Parent.oContained.w_PCTIPC04)
    this.value = ;
      iif(this.Parent.oContained.w_PCTIPC04=='I',1,;
      iif(this.Parent.oContained.w_PCTIPC04=='C',2,;
      iif(this.Parent.oContained.w_PCTIPC04=='F',3,;
      iif(this.Parent.oContained.w_PCTIPC04=='A',4,;
      0))))
  endfunc


  add object oPCTIPC05_2_33 as StdCombo with uid="JYHTZBTGHW",rtseq=42,rtrep=.f.,left=281,top=153,width=169,height=21;
    , ToolTipText = "Rappresenta la sezione di bilancio per la stampa verifica contabile";
    , HelpContextID = 20692181;
    , cFormVar="w_PCTIPC05",RowSource=""+"Immobilizzazioni,"+"Fondo amm. civile,"+"Fondo amm. fiscale,"+"Altro", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oPCTIPC05_2_33.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'C',;
    iif(this.value =3,'F',;
    iif(this.value =4,'A',;
    space(1))))))
  endfunc
  func oPCTIPC05_2_33.GetRadio()
    this.Parent.oContained.w_PCTIPC05 = this.RadioValue()
    return .t.
  endfunc

  func oPCTIPC05_2_33.SetRadio()
    this.Parent.oContained.w_PCTIPC05=trim(this.Parent.oContained.w_PCTIPC05)
    this.value = ;
      iif(this.Parent.oContained.w_PCTIPC05=='I',1,;
      iif(this.Parent.oContained.w_PCTIPC05=='C',2,;
      iif(this.Parent.oContained.w_PCTIPC05=='F',3,;
      iif(this.Parent.oContained.w_PCTIPC05=='A',4,;
      0))))
  endfunc


  add object oPCTIPC06_2_34 as StdCombo with uid="PGAKWHSSIK",rtseq=43,rtrep=.f.,left=281,top=179,width=169,height=21;
    , ToolTipText = "Rappresenta la sezione di bilancio per la stampa verifica contabile";
    , HelpContextID = 20692180;
    , cFormVar="w_PCTIPC06",RowSource=""+"Immobilizzazioni,"+"Fondo amm. civile,"+"Fondo amm. fiscale,"+"Altro", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oPCTIPC06_2_34.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'C',;
    iif(this.value =3,'F',;
    iif(this.value =4,'A',;
    space(1))))))
  endfunc
  func oPCTIPC06_2_34.GetRadio()
    this.Parent.oContained.w_PCTIPC06 = this.RadioValue()
    return .t.
  endfunc

  func oPCTIPC06_2_34.SetRadio()
    this.Parent.oContained.w_PCTIPC06=trim(this.Parent.oContained.w_PCTIPC06)
    this.value = ;
      iif(this.Parent.oContained.w_PCTIPC06=='I',1,;
      iif(this.Parent.oContained.w_PCTIPC06=='C',2,;
      iif(this.Parent.oContained.w_PCTIPC06=='F',3,;
      iif(this.Parent.oContained.w_PCTIPC06=='A',4,;
      0))))
  endfunc


  add object oPCTIPC07_2_35 as StdCombo with uid="HIYDPAVKNN",rtseq=44,rtrep=.f.,left=281,top=205,width=169,height=21;
    , ToolTipText = "Rappresenta la sezione di bilancio per la stampa verifica contabile";
    , HelpContextID = 20692179;
    , cFormVar="w_PCTIPC07",RowSource=""+"Immobilizzazioni,"+"Fondo amm. civile,"+"Fondo amm. fiscale,"+"Altro", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oPCTIPC07_2_35.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'C',;
    iif(this.value =3,'F',;
    iif(this.value =4,'A',;
    space(1))))))
  endfunc
  func oPCTIPC07_2_35.GetRadio()
    this.Parent.oContained.w_PCTIPC07 = this.RadioValue()
    return .t.
  endfunc

  func oPCTIPC07_2_35.SetRadio()
    this.Parent.oContained.w_PCTIPC07=trim(this.Parent.oContained.w_PCTIPC07)
    this.value = ;
      iif(this.Parent.oContained.w_PCTIPC07=='I',1,;
      iif(this.Parent.oContained.w_PCTIPC07=='C',2,;
      iif(this.Parent.oContained.w_PCTIPC07=='F',3,;
      iif(this.Parent.oContained.w_PCTIPC07=='A',4,;
      0))))
  endfunc


  add object oPCTIPC08_2_36 as StdCombo with uid="BVOGPWKJBM",rtseq=45,rtrep=.f.,left=281,top=231,width=169,height=21;
    , ToolTipText = "Rappresenta la sezione di bilancio per la stampa verifica contabile";
    , HelpContextID = 20692178;
    , cFormVar="w_PCTIPC08",RowSource=""+"Immobilizzazioni,"+"Fondo amm. civile,"+"Fondo amm. fiscale,"+"Altro", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oPCTIPC08_2_36.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'C',;
    iif(this.value =3,'F',;
    iif(this.value =4,'A',;
    space(1))))))
  endfunc
  func oPCTIPC08_2_36.GetRadio()
    this.Parent.oContained.w_PCTIPC08 = this.RadioValue()
    return .t.
  endfunc

  func oPCTIPC08_2_36.SetRadio()
    this.Parent.oContained.w_PCTIPC08=trim(this.Parent.oContained.w_PCTIPC08)
    this.value = ;
      iif(this.Parent.oContained.w_PCTIPC08=='I',1,;
      iif(this.Parent.oContained.w_PCTIPC08=='C',2,;
      iif(this.Parent.oContained.w_PCTIPC08=='F',3,;
      iif(this.Parent.oContained.w_PCTIPC08=='A',4,;
      0))))
  endfunc


  add object oPCTIPC09_2_37 as StdCombo with uid="YMIUHEDLHS",rtseq=46,rtrep=.f.,left=281,top=257,width=169,height=21;
    , ToolTipText = "Rappresenta la sezione di bilancio per la stampa verifica contabile";
    , HelpContextID = 20692177;
    , cFormVar="w_PCTIPC09",RowSource=""+"Immobilizzazioni,"+"Fondo amm. civile,"+"Fondo amm. fiscale,"+"Altro", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oPCTIPC09_2_37.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'C',;
    iif(this.value =3,'F',;
    iif(this.value =4,'A',;
    space(1))))))
  endfunc
  func oPCTIPC09_2_37.GetRadio()
    this.Parent.oContained.w_PCTIPC09 = this.RadioValue()
    return .t.
  endfunc

  func oPCTIPC09_2_37.SetRadio()
    this.Parent.oContained.w_PCTIPC09=trim(this.Parent.oContained.w_PCTIPC09)
    this.value = ;
      iif(this.Parent.oContained.w_PCTIPC09=='I',1,;
      iif(this.Parent.oContained.w_PCTIPC09=='C',2,;
      iif(this.Parent.oContained.w_PCTIPC09=='F',3,;
      iif(this.Parent.oContained.w_PCTIPC09=='A',4,;
      0))))
  endfunc


  add object oPCTIPC10_2_38 as StdCombo with uid="OHECQLTWHS",rtseq=47,rtrep=.f.,left=281,top=283,width=169,height=21;
    , ToolTipText = "Rappresenta la sezione di bilancio per la stampa verifica contabile";
    , HelpContextID = 20692186;
    , cFormVar="w_PCTIPC10",RowSource=""+"Immobilizzazioni,"+"Fondo amm. civile,"+"Fondo amm. fiscale,"+"Altro", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oPCTIPC10_2_38.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'C',;
    iif(this.value =3,'F',;
    iif(this.value =4,'A',;
    space(1))))))
  endfunc
  func oPCTIPC10_2_38.GetRadio()
    this.Parent.oContained.w_PCTIPC10 = this.RadioValue()
    return .t.
  endfunc

  func oPCTIPC10_2_38.SetRadio()
    this.Parent.oContained.w_PCTIPC10=trim(this.Parent.oContained.w_PCTIPC10)
    this.value = ;
      iif(this.Parent.oContained.w_PCTIPC10=='I',1,;
      iif(this.Parent.oContained.w_PCTIPC10=='C',2,;
      iif(this.Parent.oContained.w_PCTIPC10=='F',3,;
      iif(this.Parent.oContained.w_PCTIPC10=='A',4,;
      0))))
  endfunc


  add object oPCTIPC11_2_39 as StdCombo with uid="HARIRGQAMI",rtseq=50,rtrep=.f.,left=281,top=309,width=169,height=21;
    , ToolTipText = "Rappresenta la sezione di bilancio per la stampa verifica contabile";
    , HelpContextID = 20692185;
    , cFormVar="w_PCTIPC11",RowSource=""+"Immobilizzazioni,"+"Fondo amm. civile,"+"Fondo amm. fiscale,"+"Altro", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oPCTIPC11_2_39.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'C',;
    iif(this.value =3,'F',;
    iif(this.value =4,'A',;
    space(1))))))
  endfunc
  func oPCTIPC11_2_39.GetRadio()
    this.Parent.oContained.w_PCTIPC11 = this.RadioValue()
    return .t.
  endfunc

  func oPCTIPC11_2_39.SetRadio()
    this.Parent.oContained.w_PCTIPC11=trim(this.Parent.oContained.w_PCTIPC11)
    this.value = ;
      iif(this.Parent.oContained.w_PCTIPC11=='I',1,;
      iif(this.Parent.oContained.w_PCTIPC11=='C',2,;
      iif(this.Parent.oContained.w_PCTIPC11=='F',3,;
      iif(this.Parent.oContained.w_PCTIPC11=='A',4,;
      0))))
  endfunc


  add object oPCTIPC12_2_40 as StdCombo with uid="EZGVXIUXST",rtseq=51,rtrep=.f.,left=281,top=335,width=169,height=21;
    , ToolTipText = "Rappresenta la sezione di bilancio per la stampa verifica contabile";
    , HelpContextID = 20692184;
    , cFormVar="w_PCTIPC12",RowSource=""+"Immobilizzazioni,"+"Fondo amm. civile,"+"Fondo amm. fiscale,"+"Altro", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oPCTIPC12_2_40.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'C',;
    iif(this.value =3,'F',;
    iif(this.value =4,'A',;
    space(1))))))
  endfunc
  func oPCTIPC12_2_40.GetRadio()
    this.Parent.oContained.w_PCTIPC12 = this.RadioValue()
    return .t.
  endfunc

  func oPCTIPC12_2_40.SetRadio()
    this.Parent.oContained.w_PCTIPC12=trim(this.Parent.oContained.w_PCTIPC12)
    this.value = ;
      iif(this.Parent.oContained.w_PCTIPC12=='I',1,;
      iif(this.Parent.oContained.w_PCTIPC12=='C',2,;
      iif(this.Parent.oContained.w_PCTIPC12=='F',3,;
      iif(this.Parent.oContained.w_PCTIPC12=='A',4,;
      0))))
  endfunc


  add object oPCTIPC13_2_41 as StdCombo with uid="PYOAKTCIEG",rtseq=52,rtrep=.f.,left=281,top=361,width=169,height=21;
    , ToolTipText = "Rappresenta la sezione di bilancio per la stampa verifica contabile";
    , HelpContextID = 20692183;
    , cFormVar="w_PCTIPC13",RowSource=""+"Immobilizzazioni,"+"Fondo amm. civile,"+"Fondo amm. fiscale,"+"Altro", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oPCTIPC13_2_41.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'C',;
    iif(this.value =3,'F',;
    iif(this.value =4,'A',;
    space(1))))))
  endfunc
  func oPCTIPC13_2_41.GetRadio()
    this.Parent.oContained.w_PCTIPC13 = this.RadioValue()
    return .t.
  endfunc

  func oPCTIPC13_2_41.SetRadio()
    this.Parent.oContained.w_PCTIPC13=trim(this.Parent.oContained.w_PCTIPC13)
    this.value = ;
      iif(this.Parent.oContained.w_PCTIPC13=='I',1,;
      iif(this.Parent.oContained.w_PCTIPC13=='C',2,;
      iif(this.Parent.oContained.w_PCTIPC13=='F',3,;
      iif(this.Parent.oContained.w_PCTIPC13=='A',4,;
      0))))
  endfunc


  add object oPCTIPC14_2_42 as StdCombo with uid="ZBSAJKURJM",rtseq=53,rtrep=.f.,left=281,top=387,width=169,height=21;
    , ToolTipText = "Rappresenta la sezione di bilancio per la stampa verifica contabile";
    , HelpContextID = 20692182;
    , cFormVar="w_PCTIPC14",RowSource=""+"Immobilizzazioni,"+"Fondo amm. civile,"+"Fondo amm. fiscale,"+"Altro", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oPCTIPC14_2_42.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'C',;
    iif(this.value =3,'F',;
    iif(this.value =4,'A',;
    space(1))))))
  endfunc
  func oPCTIPC14_2_42.GetRadio()
    this.Parent.oContained.w_PCTIPC14 = this.RadioValue()
    return .t.
  endfunc

  func oPCTIPC14_2_42.SetRadio()
    this.Parent.oContained.w_PCTIPC14=trim(this.Parent.oContained.w_PCTIPC14)
    this.value = ;
      iif(this.Parent.oContained.w_PCTIPC14=='I',1,;
      iif(this.Parent.oContained.w_PCTIPC14=='C',2,;
      iif(this.Parent.oContained.w_PCTIPC14=='F',3,;
      iif(this.Parent.oContained.w_PCTIPC14=='A',4,;
      0))))
  endfunc


  add object oPCTIPC15_2_43 as StdCombo with uid="LTULSDLOGC",rtseq=54,rtrep=.f.,left=281,top=413,width=169,height=21;
    , ToolTipText = "Rappresenta la sezione di bilancio per la stampa verifica contabile";
    , HelpContextID = 20692181;
    , cFormVar="w_PCTIPC15",RowSource=""+"Immobilizzazioni,"+"Fondo amm. civile,"+"Fondo amm. fiscale,"+"Altro", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oPCTIPC15_2_43.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'C',;
    iif(this.value =3,'F',;
    iif(this.value =4,'A',;
    space(1))))))
  endfunc
  func oPCTIPC15_2_43.GetRadio()
    this.Parent.oContained.w_PCTIPC15 = this.RadioValue()
    return .t.
  endfunc

  func oPCTIPC15_2_43.SetRadio()
    this.Parent.oContained.w_PCTIPC15=trim(this.Parent.oContained.w_PCTIPC15)
    this.value = ;
      iif(this.Parent.oContained.w_PCTIPC15=='I',1,;
      iif(this.Parent.oContained.w_PCTIPC15=='C',2,;
      iif(this.Parent.oContained.w_PCTIPC15=='F',3,;
      iif(this.Parent.oContained.w_PCTIPC15=='A',4,;
      0))))
  endfunc


  add object oPCTIPC16_2_44 as StdCombo with uid="OFDYPVCYBJ",rtseq=55,rtrep=.f.,left=281,top=439,width=169,height=21;
    , ToolTipText = "Rappresenta la sezione di bilancio per la stampa verifica contabile";
    , HelpContextID = 20692180;
    , cFormVar="w_PCTIPC16",RowSource=""+"Immobilizzazioni,"+"Fondo amm. civile,"+"Fondo amm. fiscale,"+"Altro", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oPCTIPC16_2_44.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'C',;
    iif(this.value =3,'F',;
    iif(this.value =4,'A',;
    space(1))))))
  endfunc
  func oPCTIPC16_2_44.GetRadio()
    this.Parent.oContained.w_PCTIPC16 = this.RadioValue()
    return .t.
  endfunc

  func oPCTIPC16_2_44.SetRadio()
    this.Parent.oContained.w_PCTIPC16=trim(this.Parent.oContained.w_PCTIPC16)
    this.value = ;
      iif(this.Parent.oContained.w_PCTIPC16=='I',1,;
      iif(this.Parent.oContained.w_PCTIPC16=='C',2,;
      iif(this.Parent.oContained.w_PCTIPC16=='F',3,;
      iif(this.Parent.oContained.w_PCTIPC16=='A',4,;
      0))))
  endfunc

  add object oStr_2_1 as StdString with uid="TVNOUGPXYP",Visible=.t., Left=10, Top=18,;
    Alignment=0, Width=433, Height=18,;
    Caption="Descrizioni contropartite contabili e sezione di bilancio"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_2 as StdString with uid="ZTDESTMAUR",Visible=.t., Left=5, Top=49,;
    Alignment=1, Width=47, Height=15,;
    Caption="C01:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_19 as StdString with uid="NOXNODXXMC",Visible=.t., Left=5, Top=75,;
    Alignment=1, Width=47, Height=15,;
    Caption="C02:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_20 as StdString with uid="JDNYCJENDN",Visible=.t., Left=5, Top=101,;
    Alignment=1, Width=47, Height=15,;
    Caption="C03:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_21 as StdString with uid="UQEOHTOLRZ",Visible=.t., Left=5, Top=127,;
    Alignment=1, Width=47, Height=15,;
    Caption="C04:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_22 as StdString with uid="UELXHTIAIC",Visible=.t., Left=5, Top=153,;
    Alignment=1, Width=47, Height=15,;
    Caption="C05:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_23 as StdString with uid="MLZTPGMXLC",Visible=.t., Left=5, Top=179,;
    Alignment=1, Width=47, Height=15,;
    Caption="C06:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_24 as StdString with uid="MKHAIFJFPX",Visible=.t., Left=5, Top=205,;
    Alignment=1, Width=47, Height=15,;
    Caption="C07:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_25 as StdString with uid="NPWQIISIGZ",Visible=.t., Left=5, Top=231,;
    Alignment=1, Width=47, Height=15,;
    Caption="C08:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_26 as StdString with uid="QMVRZPITZZ",Visible=.t., Left=5, Top=257,;
    Alignment=1, Width=47, Height=15,;
    Caption="C09:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_27 as StdString with uid="TRTVQOGMSA",Visible=.t., Left=5, Top=283,;
    Alignment=1, Width=47, Height=15,;
    Caption="C10:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_45 as StdString with uid="QYTLYEOEGE",Visible=.t., Left=27, Top=309,;
    Alignment=1, Width=25, Height=15,;
    Caption="C11:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_46 as StdString with uid="FUEFFAVXUK",Visible=.t., Left=27, Top=335,;
    Alignment=1, Width=25, Height=15,;
    Caption="C12:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_47 as StdString with uid="OMSBTTUZMB",Visible=.t., Left=27, Top=361,;
    Alignment=1, Width=25, Height=15,;
    Caption="C13:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_48 as StdString with uid="CQJWVCGTBC",Visible=.t., Left=27, Top=387,;
    Alignment=1, Width=25, Height=15,;
    Caption="C14:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_49 as StdString with uid="TSWBIQAUXK",Visible=.t., Left=27, Top=413,;
    Alignment=1, Width=25, Height=15,;
    Caption="C15:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_50 as StdString with uid="GSQXFLZJEU",Visible=.t., Left=27, Top=439,;
    Alignment=1, Width=25, Height=15,;
    Caption="C16:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_2_28 as StdBox with uid="KGIZKPFBDI",left=8, top=38, width=466,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsce_apc','PAR_CESP','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".PCCODAZI=PAR_CESP.PCCODAZI";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsce_apc
proc avverti(pParent)
ah_ErrorMsg("Verificare che il valore da impostare sia congruente con gli ammortamenti eseguiti",,'')
endproc
* --- Fine Area Manuale
