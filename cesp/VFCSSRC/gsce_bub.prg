* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_bub                                                        *
*              Cancellazione ubicazioni                                        *
*                                                                              *
*      Author: Zucchetti spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][207]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-05-08                                                      *
* Last revis.: 2005-12-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsce_bub",oParentObject)
return(i_retval)

define class tgsce_bub as StdBatch
  * --- Local variables
  w_CODICE = space(20)
  w_MESS = space(100)
  * --- WorkFile variables
  CES_PITI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questo batch serve per poter verificare se � possibile cancellare l'ubicazione.
    *     Devo controllare se l'ubicazione i questione � presente nell'anagrafica cespiti,
    *     se � presente non devo dare la possibilit� di cancellarla.
    * --- Select from CES_PITI
    i_nConn=i_TableProp[this.CES_PITI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CES_PITI_idx,2],.t.,this.CES_PITI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select CECODICE  from "+i_cTable+" CES_PITI ";
          +" where CECODUBI="+cp_ToStrODBC(this.oParentObject.w_UBCODICE)+"";
           ,"_Curs_CES_PITI")
    else
      select CECODICE from (i_cTable);
       where CECODUBI=this.oParentObject.w_UBCODICE;
        into cursor _Curs_CES_PITI
    endif
    if used('_Curs_CES_PITI')
      select _Curs_CES_PITI
      locate for 1=1
      do while not(eof())
      this.w_CODICE = _Curs_CES_PITI.CECODICE
        select _Curs_CES_PITI
        continue
      enddo
      use
    endif
    if Not Empty(this.w_Codice)
      this.w_MESS = AH_MSGFORMAT ("Impossibile eliminare l'ubicazione.%0Esistono cespiti che utilizzano l'ubicazione che si intende cancellare ")
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=this.w_MESS
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='CES_PITI'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_CES_PITI')
      use in _Curs_CES_PITI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
