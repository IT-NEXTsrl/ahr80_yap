* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_ase                                                        *
*              Saldi cespiti pre-Euro                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30] [VRS_19]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-04-26                                                      *
* Last revis.: 2008-09-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsce_ase"))

* --- Class definition
define class tgsce_ase as StdForm
  Top    = 4
  Left   = 11

  * --- Standard Properties
  Width  = 725
  Height = 465+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-09-15"
  HelpContextID=109725033
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=35

  * --- Constant Properties
  SAL_CESE_IDX = 0
  CES_PITI_IDX = 0
  PAR_CESP_IDX = 0
  cFile = "SAL_CESE"
  cKeySelect = "SCCODCES"
  cKeyWhere  = "SCCODCES=this.w_SCCODCES"
  cKeyWhereODBC = '"SCCODCES="+cp_ToStrODBC(this.w_SCCODCES)';

  cKeyWhereODBCqualified = '"SAL_CESE.SCCODCES="+cp_ToStrODBC(this.w_SCCODCES)';

  cPrg = "gsce_ase"
  cComment = "Saldi cespiti pre-Euro"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_SCCODCES = space(20)
  w_SCCOSBEN = 0
  w_SCDURBEN = 0
  w_SCQUOESE = 0
  w_SCSPEPRE = 0
  w_SCPERVAR = 0
  w_SCSPECOR = 0
  w_SCUNIMIS = space(3)
  w_SCQTAPREV = 0
  w_SCCOSUNI = 0
  w_DESCES = space(40)
  w_SCINCVAC = 0
  w_SCIMPONC = 0
  w_SCIMPRIC = 0
  w_SCDECVAC = 0
  w_SCIMPSVC = 0
  w_SCACCCIV = 0
  w_SCUTICIV = 0
  w_SCARRFOC = 0
  w_SCARRAMC = 0
  w_SCINCVAL = 0
  w_SCIMPONS = 0
  w_SCIMPRIV = 0
  w_SCDECVAL = 0
  w_SCIMPSVA = 0
  w_SCIMPNOA = 0
  w_SCQUOPER = 0
  w_SCACCFIS = 0
  w_SCUTIFIS = 0
  w_SCACCANT = 0
  w_SCUTIANT = 0
  w_SCARRFON = 0
  w_SCARRAMM = 0
  w_CODAZI = space(5)
  w_PCTIPAMM = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'SAL_CESE','gsce_ase')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsce_asePag1","gsce_ase",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Saldi valuta nazionale")
      .Pages(1).HelpContextID = 218231633
      .Pages(2).addobject("oPag","tgsce_asePag2","gsce_ase",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Aspetto gestionale")
      .Pages(2).HelpContextID = 80856649
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSCCODCES_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='CES_PITI'
    this.cWorkTables[2]='PAR_CESP'
    this.cWorkTables[3]='SAL_CESE'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.SAL_CESE_IDX,5],7]
    this.nPostItConn=i_TableProp[this.SAL_CESE_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_SCCODCES = NVL(SCCODCES,space(20))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_1_joined
    link_1_1_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from SAL_CESE where SCCODCES=KeySet.SCCODCES
    *
    i_nConn = i_TableProp[this.SAL_CESE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SAL_CESE_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('SAL_CESE')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "SAL_CESE.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' SAL_CESE '
      link_1_1_joined=this.AddJoinedLink_1_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'SCCODCES',this.w_SCCODCES  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DESCES = space(40)
        .w_CODAZI = i_codazi
        .w_PCTIPAMM = space(1)
        .w_SCCODCES = NVL(SCCODCES,space(20))
          if link_1_1_joined
            this.w_SCCODCES = NVL(CECODICE101,NVL(this.w_SCCODCES,space(20)))
            this.w_DESCES = NVL(CEDESCRI101,space(40))
          else
          .link_1_1('Load')
          endif
        .w_SCCOSBEN = NVL(SCCOSBEN,0)
        .w_SCDURBEN = NVL(SCDURBEN,0)
        .w_SCQUOESE = NVL(SCQUOESE,0)
        .w_SCSPEPRE = NVL(SCSPEPRE,0)
        .w_SCPERVAR = NVL(SCPERVAR,0)
        .w_SCSPECOR = NVL(SCSPECOR,0)
        .w_SCUNIMIS = NVL(SCUNIMIS,space(3))
        .w_SCQTAPREV = NVL(SCQTAPREV,0)
        .w_SCCOSUNI = NVL(SCCOSUNI,0)
        .w_SCINCVAC = NVL(SCINCVAC,0)
        .w_SCIMPONC = NVL(SCIMPONC,0)
        .w_SCIMPRIC = NVL(SCIMPRIC,0)
        .w_SCDECVAC = NVL(SCDECVAC,0)
        .w_SCIMPSVC = NVL(SCIMPSVC,0)
        .w_SCACCCIV = NVL(SCACCCIV,0)
        .w_SCUTICIV = NVL(SCUTICIV,0)
        .w_SCARRFOC = NVL(SCARRFOC,0)
        .w_SCARRAMC = NVL(SCARRAMC,0)
        .w_SCINCVAL = NVL(SCINCVAL,0)
        .w_SCIMPONS = NVL(SCIMPONS,0)
        .w_SCIMPRIV = NVL(SCIMPRIV,0)
        .w_SCDECVAL = NVL(SCDECVAL,0)
        .w_SCIMPSVA = NVL(SCIMPSVA,0)
        .w_SCIMPNOA = NVL(SCIMPNOA,0)
        .w_SCQUOPER = NVL(SCQUOPER,0)
        .w_SCACCFIS = NVL(SCACCFIS,0)
        .w_SCUTIFIS = NVL(SCUTIFIS,0)
        .w_SCACCANT = NVL(SCACCANT,0)
        .w_SCUTIANT = NVL(SCUTIANT,0)
        .w_SCARRFON = NVL(SCARRFON,0)
        .w_SCARRAMM = NVL(SCARRAMM,0)
          .link_1_54('Load')
        cp_LoadRecExtFlds(this,'SAL_CESE')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_SCCODCES = space(20)
      .w_SCCOSBEN = 0
      .w_SCDURBEN = 0
      .w_SCQUOESE = 0
      .w_SCSPEPRE = 0
      .w_SCPERVAR = 0
      .w_SCSPECOR = 0
      .w_SCUNIMIS = space(3)
      .w_SCQTAPREV = 0
      .w_SCCOSUNI = 0
      .w_DESCES = space(40)
      .w_SCINCVAC = 0
      .w_SCIMPONC = 0
      .w_SCIMPRIC = 0
      .w_SCDECVAC = 0
      .w_SCIMPSVC = 0
      .w_SCACCCIV = 0
      .w_SCUTICIV = 0
      .w_SCARRFOC = 0
      .w_SCARRAMC = 0
      .w_SCINCVAL = 0
      .w_SCIMPONS = 0
      .w_SCIMPRIV = 0
      .w_SCDECVAL = 0
      .w_SCIMPSVA = 0
      .w_SCIMPNOA = 0
      .w_SCQUOPER = 0
      .w_SCACCFIS = 0
      .w_SCUTIFIS = 0
      .w_SCACCANT = 0
      .w_SCUTIANT = 0
      .w_SCARRFON = 0
      .w_SCARRAMM = 0
      .w_CODAZI = space(5)
      .w_PCTIPAMM = space(1)
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
          if not(empty(.w_SCCODCES))
          .link_1_1('Full')
          endif
          .DoRTCalc(2,33,.f.)
        .w_CODAZI = i_codazi
        .DoRTCalc(34,34,.f.)
          if not(empty(.w_CODAZI))
          .link_1_54('Full')
          endif
      endif
    endwith
    cp_BlankRecExtFlds(this,'SAL_CESE')
    this.DoRTCalc(35,35,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oSCCODCES_1_1.enabled = i_bVal
      .Page2.oPag.oSCCOSBEN_2_1.enabled = i_bVal
      .Page2.oPag.oSCDURBEN_2_2.enabled = i_bVal
      .Page2.oPag.oSCQUOESE_2_3.enabled = i_bVal
      .Page2.oPag.oSCSPEPRE_2_4.enabled = i_bVal
      .Page2.oPag.oSCPERVAR_2_5.enabled = i_bVal
      .Page2.oPag.oSCSPECOR_2_6.enabled = i_bVal
      .Page2.oPag.oSCUNIMIS_2_7.enabled = i_bVal
      .Page2.oPag.oSCQTAPREV_2_8.enabled = i_bVal
      .Page2.oPag.oSCCOSUNI_2_9.enabled = i_bVal
      .Page1.oPag.oSCINCVAC_1_13.enabled = i_bVal
      .Page1.oPag.oSCIMPONC_1_15.enabled = i_bVal
      .Page1.oPag.oSCIMPRIC_1_17.enabled = i_bVal
      .Page1.oPag.oSCDECVAC_1_18.enabled = i_bVal
      .Page1.oPag.oSCIMPSVC_1_20.enabled = i_bVal
      .Page1.oPag.oSCACCCIV_1_22.enabled = i_bVal
      .Page1.oPag.oSCUTICIV_1_23.enabled = i_bVal
      .Page1.oPag.oSCARRFOC_1_25.enabled = i_bVal
      .Page1.oPag.oSCARRAMC_1_27.enabled = i_bVal
      .Page1.oPag.oSCINCVAL_1_29.enabled = i_bVal
      .Page1.oPag.oSCIMPONS_1_31.enabled = i_bVal
      .Page1.oPag.oSCIMPRIV_1_32.enabled = i_bVal
      .Page1.oPag.oSCDECVAL_1_34.enabled = i_bVal
      .Page1.oPag.oSCIMPSVA_1_36.enabled = i_bVal
      .Page1.oPag.oSCIMPNOA_1_38.enabled = i_bVal
      .Page1.oPag.oSCQUOPER_1_39.enabled = i_bVal
      .Page1.oPag.oSCACCFIS_1_40.enabled = i_bVal
      .Page1.oPag.oSCUTIFIS_1_41.enabled = i_bVal
      .Page1.oPag.oSCACCANT_1_42.enabled = i_bVal
      .Page1.oPag.oSCUTIANT_1_43.enabled = i_bVal
      .Page1.oPag.oSCARRFON_1_45.enabled = i_bVal
      .Page1.oPag.oSCARRAMM_1_47.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oSCCODCES_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oSCCODCES_1_1.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'SAL_CESE',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.SAL_CESE_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCCODCES,"SCCODCES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCCOSBEN,"SCCOSBEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCDURBEN,"SCDURBEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCQUOESE,"SCQUOESE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCSPEPRE,"SCSPEPRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCPERVAR,"SCPERVAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCSPECOR,"SCSPECOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCUNIMIS,"SCUNIMIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCQTAPREV,"SCQTAPREV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCCOSUNI,"SCCOSUNI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCINCVAC,"SCINCVAC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCIMPONC,"SCIMPONC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCIMPRIC,"SCIMPRIC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCDECVAC,"SCDECVAC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCIMPSVC,"SCIMPSVC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCACCCIV,"SCACCCIV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCUTICIV,"SCUTICIV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCARRFOC,"SCARRFOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCARRAMC,"SCARRAMC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCINCVAL,"SCINCVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCIMPONS,"SCIMPONS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCIMPRIV,"SCIMPRIV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCDECVAL,"SCDECVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCIMPSVA,"SCIMPSVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCIMPNOA,"SCIMPNOA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCQUOPER,"SCQUOPER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCACCFIS,"SCACCFIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCUTIFIS,"SCUTIFIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCACCANT,"SCACCANT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCUTIANT,"SCUTIANT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCARRFON,"SCARRFON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCARRAMM,"SCARRAMM",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.SAL_CESE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SAL_CESE_IDX,2])
    i_lTable = "SAL_CESE"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.SAL_CESE_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.SAL_CESE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.SAL_CESE_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.SAL_CESE_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into SAL_CESE
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'SAL_CESE')
        i_extval=cp_InsertValODBCExtFlds(this,'SAL_CESE')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(SCCODCES,SCCOSBEN,SCDURBEN,SCQUOESE,SCSPEPRE"+;
                  ",SCPERVAR,SCSPECOR,SCUNIMIS,SCQTAPREV,SCCOSUNI"+;
                  ",SCINCVAC,SCIMPONC,SCIMPRIC,SCDECVAC,SCIMPSVC"+;
                  ",SCACCCIV,SCUTICIV,SCARRFOC,SCARRAMC,SCINCVAL"+;
                  ",SCIMPONS,SCIMPRIV,SCDECVAL,SCIMPSVA,SCIMPNOA"+;
                  ",SCQUOPER,SCACCFIS,SCUTIFIS,SCACCANT,SCUTIANT"+;
                  ",SCARRFON,SCARRAMM "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBCNull(this.w_SCCODCES)+;
                  ","+cp_ToStrODBC(this.w_SCCOSBEN)+;
                  ","+cp_ToStrODBC(this.w_SCDURBEN)+;
                  ","+cp_ToStrODBC(this.w_SCQUOESE)+;
                  ","+cp_ToStrODBC(this.w_SCSPEPRE)+;
                  ","+cp_ToStrODBC(this.w_SCPERVAR)+;
                  ","+cp_ToStrODBC(this.w_SCSPECOR)+;
                  ","+cp_ToStrODBC(this.w_SCUNIMIS)+;
                  ","+cp_ToStrODBC(this.w_SCQTAPREV)+;
                  ","+cp_ToStrODBC(this.w_SCCOSUNI)+;
                  ","+cp_ToStrODBC(this.w_SCINCVAC)+;
                  ","+cp_ToStrODBC(this.w_SCIMPONC)+;
                  ","+cp_ToStrODBC(this.w_SCIMPRIC)+;
                  ","+cp_ToStrODBC(this.w_SCDECVAC)+;
                  ","+cp_ToStrODBC(this.w_SCIMPSVC)+;
                  ","+cp_ToStrODBC(this.w_SCACCCIV)+;
                  ","+cp_ToStrODBC(this.w_SCUTICIV)+;
                  ","+cp_ToStrODBC(this.w_SCARRFOC)+;
                  ","+cp_ToStrODBC(this.w_SCARRAMC)+;
                  ","+cp_ToStrODBC(this.w_SCINCVAL)+;
                  ","+cp_ToStrODBC(this.w_SCIMPONS)+;
                  ","+cp_ToStrODBC(this.w_SCIMPRIV)+;
                  ","+cp_ToStrODBC(this.w_SCDECVAL)+;
                  ","+cp_ToStrODBC(this.w_SCIMPSVA)+;
                  ","+cp_ToStrODBC(this.w_SCIMPNOA)+;
                  ","+cp_ToStrODBC(this.w_SCQUOPER)+;
                  ","+cp_ToStrODBC(this.w_SCACCFIS)+;
                  ","+cp_ToStrODBC(this.w_SCUTIFIS)+;
                  ","+cp_ToStrODBC(this.w_SCACCANT)+;
                  ","+cp_ToStrODBC(this.w_SCUTIANT)+;
                  ","+cp_ToStrODBC(this.w_SCARRFON)+;
                  ","+cp_ToStrODBC(this.w_SCARRAMM)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'SAL_CESE')
        i_extval=cp_InsertValVFPExtFlds(this,'SAL_CESE')
        cp_CheckDeletedKey(i_cTable,0,'SCCODCES',this.w_SCCODCES)
        INSERT INTO (i_cTable);
              (SCCODCES,SCCOSBEN,SCDURBEN,SCQUOESE,SCSPEPRE,SCPERVAR,SCSPECOR,SCUNIMIS,SCQTAPREV,SCCOSUNI,SCINCVAC,SCIMPONC,SCIMPRIC,SCDECVAC,SCIMPSVC,SCACCCIV,SCUTICIV,SCARRFOC,SCARRAMC,SCINCVAL,SCIMPONS,SCIMPRIV,SCDECVAL,SCIMPSVA,SCIMPNOA,SCQUOPER,SCACCFIS,SCUTIFIS,SCACCANT,SCUTIANT,SCARRFON,SCARRAMM  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_SCCODCES;
                  ,this.w_SCCOSBEN;
                  ,this.w_SCDURBEN;
                  ,this.w_SCQUOESE;
                  ,this.w_SCSPEPRE;
                  ,this.w_SCPERVAR;
                  ,this.w_SCSPECOR;
                  ,this.w_SCUNIMIS;
                  ,this.w_SCQTAPREV;
                  ,this.w_SCCOSUNI;
                  ,this.w_SCINCVAC;
                  ,this.w_SCIMPONC;
                  ,this.w_SCIMPRIC;
                  ,this.w_SCDECVAC;
                  ,this.w_SCIMPSVC;
                  ,this.w_SCACCCIV;
                  ,this.w_SCUTICIV;
                  ,this.w_SCARRFOC;
                  ,this.w_SCARRAMC;
                  ,this.w_SCINCVAL;
                  ,this.w_SCIMPONS;
                  ,this.w_SCIMPRIV;
                  ,this.w_SCDECVAL;
                  ,this.w_SCIMPSVA;
                  ,this.w_SCIMPNOA;
                  ,this.w_SCQUOPER;
                  ,this.w_SCACCFIS;
                  ,this.w_SCUTIFIS;
                  ,this.w_SCACCANT;
                  ,this.w_SCUTIANT;
                  ,this.w_SCARRFON;
                  ,this.w_SCARRAMM;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.SAL_CESE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.SAL_CESE_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.SAL_CESE_IDX,i_nConn)
      *
      * update SAL_CESE
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'SAL_CESE')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " SCCOSBEN="+cp_ToStrODBC(this.w_SCCOSBEN)+;
             ",SCDURBEN="+cp_ToStrODBC(this.w_SCDURBEN)+;
             ",SCQUOESE="+cp_ToStrODBC(this.w_SCQUOESE)+;
             ",SCSPEPRE="+cp_ToStrODBC(this.w_SCSPEPRE)+;
             ",SCPERVAR="+cp_ToStrODBC(this.w_SCPERVAR)+;
             ",SCSPECOR="+cp_ToStrODBC(this.w_SCSPECOR)+;
             ",SCUNIMIS="+cp_ToStrODBC(this.w_SCUNIMIS)+;
             ",SCQTAPREV="+cp_ToStrODBC(this.w_SCQTAPREV)+;
             ",SCCOSUNI="+cp_ToStrODBC(this.w_SCCOSUNI)+;
             ",SCINCVAC="+cp_ToStrODBC(this.w_SCINCVAC)+;
             ",SCIMPONC="+cp_ToStrODBC(this.w_SCIMPONC)+;
             ",SCIMPRIC="+cp_ToStrODBC(this.w_SCIMPRIC)+;
             ",SCDECVAC="+cp_ToStrODBC(this.w_SCDECVAC)+;
             ",SCIMPSVC="+cp_ToStrODBC(this.w_SCIMPSVC)+;
             ",SCACCCIV="+cp_ToStrODBC(this.w_SCACCCIV)+;
             ",SCUTICIV="+cp_ToStrODBC(this.w_SCUTICIV)+;
             ",SCARRFOC="+cp_ToStrODBC(this.w_SCARRFOC)+;
             ",SCARRAMC="+cp_ToStrODBC(this.w_SCARRAMC)+;
             ",SCINCVAL="+cp_ToStrODBC(this.w_SCINCVAL)+;
             ",SCIMPONS="+cp_ToStrODBC(this.w_SCIMPONS)+;
             ",SCIMPRIV="+cp_ToStrODBC(this.w_SCIMPRIV)+;
             ",SCDECVAL="+cp_ToStrODBC(this.w_SCDECVAL)+;
             ",SCIMPSVA="+cp_ToStrODBC(this.w_SCIMPSVA)+;
             ",SCIMPNOA="+cp_ToStrODBC(this.w_SCIMPNOA)+;
             ",SCQUOPER="+cp_ToStrODBC(this.w_SCQUOPER)+;
             ",SCACCFIS="+cp_ToStrODBC(this.w_SCACCFIS)+;
             ",SCUTIFIS="+cp_ToStrODBC(this.w_SCUTIFIS)+;
             ",SCACCANT="+cp_ToStrODBC(this.w_SCACCANT)+;
             ",SCUTIANT="+cp_ToStrODBC(this.w_SCUTIANT)+;
             ",SCARRFON="+cp_ToStrODBC(this.w_SCARRFON)+;
             ",SCARRAMM="+cp_ToStrODBC(this.w_SCARRAMM)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'SAL_CESE')
        i_cWhere = cp_PKFox(i_cTable  ,'SCCODCES',this.w_SCCODCES  )
        UPDATE (i_cTable) SET;
              SCCOSBEN=this.w_SCCOSBEN;
             ,SCDURBEN=this.w_SCDURBEN;
             ,SCQUOESE=this.w_SCQUOESE;
             ,SCSPEPRE=this.w_SCSPEPRE;
             ,SCPERVAR=this.w_SCPERVAR;
             ,SCSPECOR=this.w_SCSPECOR;
             ,SCUNIMIS=this.w_SCUNIMIS;
             ,SCQTAPREV=this.w_SCQTAPREV;
             ,SCCOSUNI=this.w_SCCOSUNI;
             ,SCINCVAC=this.w_SCINCVAC;
             ,SCIMPONC=this.w_SCIMPONC;
             ,SCIMPRIC=this.w_SCIMPRIC;
             ,SCDECVAC=this.w_SCDECVAC;
             ,SCIMPSVC=this.w_SCIMPSVC;
             ,SCACCCIV=this.w_SCACCCIV;
             ,SCUTICIV=this.w_SCUTICIV;
             ,SCARRFOC=this.w_SCARRFOC;
             ,SCARRAMC=this.w_SCARRAMC;
             ,SCINCVAL=this.w_SCINCVAL;
             ,SCIMPONS=this.w_SCIMPONS;
             ,SCIMPRIV=this.w_SCIMPRIV;
             ,SCDECVAL=this.w_SCDECVAL;
             ,SCIMPSVA=this.w_SCIMPSVA;
             ,SCIMPNOA=this.w_SCIMPNOA;
             ,SCQUOPER=this.w_SCQUOPER;
             ,SCACCFIS=this.w_SCACCFIS;
             ,SCUTIFIS=this.w_SCUTIFIS;
             ,SCACCANT=this.w_SCACCANT;
             ,SCUTIANT=this.w_SCUTIANT;
             ,SCARRFON=this.w_SCARRFON;
             ,SCARRAMM=this.w_SCARRAMM;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.SAL_CESE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.SAL_CESE_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.SAL_CESE_IDX,i_nConn)
      *
      * delete SAL_CESE
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'SCCODCES',this.w_SCCODCES  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.SAL_CESE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SAL_CESE_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,33,.t.)
          .link_1_54('Full')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(35,35,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oSCINCVAC_1_13.enabled = this.oPgFrm.Page1.oPag.oSCINCVAC_1_13.mCond()
    this.oPgFrm.Page1.oPag.oSCIMPONC_1_15.enabled = this.oPgFrm.Page1.oPag.oSCIMPONC_1_15.mCond()
    this.oPgFrm.Page1.oPag.oSCIMPRIC_1_17.enabled = this.oPgFrm.Page1.oPag.oSCIMPRIC_1_17.mCond()
    this.oPgFrm.Page1.oPag.oSCDECVAC_1_18.enabled = this.oPgFrm.Page1.oPag.oSCDECVAC_1_18.mCond()
    this.oPgFrm.Page1.oPag.oSCIMPSVC_1_20.enabled = this.oPgFrm.Page1.oPag.oSCIMPSVC_1_20.mCond()
    this.oPgFrm.Page1.oPag.oSCACCCIV_1_22.enabled = this.oPgFrm.Page1.oPag.oSCACCCIV_1_22.mCond()
    this.oPgFrm.Page1.oPag.oSCUTICIV_1_23.enabled = this.oPgFrm.Page1.oPag.oSCUTICIV_1_23.mCond()
    this.oPgFrm.Page1.oPag.oSCARRFOC_1_25.enabled = this.oPgFrm.Page1.oPag.oSCARRFOC_1_25.mCond()
    this.oPgFrm.Page1.oPag.oSCARRAMC_1_27.enabled = this.oPgFrm.Page1.oPag.oSCARRAMC_1_27.mCond()
    this.oPgFrm.Page1.oPag.oSCINCVAL_1_29.enabled = this.oPgFrm.Page1.oPag.oSCINCVAL_1_29.mCond()
    this.oPgFrm.Page1.oPag.oSCIMPONS_1_31.enabled = this.oPgFrm.Page1.oPag.oSCIMPONS_1_31.mCond()
    this.oPgFrm.Page1.oPag.oSCIMPRIV_1_32.enabled = this.oPgFrm.Page1.oPag.oSCIMPRIV_1_32.mCond()
    this.oPgFrm.Page1.oPag.oSCDECVAL_1_34.enabled = this.oPgFrm.Page1.oPag.oSCDECVAL_1_34.mCond()
    this.oPgFrm.Page1.oPag.oSCIMPSVA_1_36.enabled = this.oPgFrm.Page1.oPag.oSCIMPSVA_1_36.mCond()
    this.oPgFrm.Page1.oPag.oSCIMPNOA_1_38.enabled = this.oPgFrm.Page1.oPag.oSCIMPNOA_1_38.mCond()
    this.oPgFrm.Page1.oPag.oSCQUOPER_1_39.enabled = this.oPgFrm.Page1.oPag.oSCQUOPER_1_39.mCond()
    this.oPgFrm.Page1.oPag.oSCACCFIS_1_40.enabled = this.oPgFrm.Page1.oPag.oSCACCFIS_1_40.mCond()
    this.oPgFrm.Page1.oPag.oSCUTIFIS_1_41.enabled = this.oPgFrm.Page1.oPag.oSCUTIFIS_1_41.mCond()
    this.oPgFrm.Page1.oPag.oSCACCANT_1_42.enabled = this.oPgFrm.Page1.oPag.oSCACCANT_1_42.mCond()
    this.oPgFrm.Page1.oPag.oSCUTIANT_1_43.enabled = this.oPgFrm.Page1.oPag.oSCUTIANT_1_43.mCond()
    this.oPgFrm.Page1.oPag.oSCARRFON_1_45.enabled = this.oPgFrm.Page1.oPag.oSCARRFON_1_45.mCond()
    this.oPgFrm.Page1.oPag.oSCARRAMM_1_47.enabled = this.oPgFrm.Page1.oPag.oSCARRAMM_1_47.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=SCCODCES
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CES_PITI_IDX,3]
    i_lTable = "CES_PITI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CES_PITI_IDX,2], .t., this.CES_PITI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CES_PITI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SCCODCES) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCE_ACE',True,'CES_PITI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CECODICE like "+cp_ToStrODBC(trim(this.w_SCCODCES)+"%");

          i_ret=cp_SQL(i_nConn,"select CECODICE,CEDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CECODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CECODICE',trim(this.w_SCCODCES))
          select CECODICE,CEDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CECODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SCCODCES)==trim(_Link_.CECODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CEDESCRI like "+cp_ToStrODBC(trim(this.w_SCCODCES)+"%");

            i_ret=cp_SQL(i_nConn,"select CECODICE,CEDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CEDESCRI like "+cp_ToStr(trim(this.w_SCCODCES)+"%");

            select CECODICE,CEDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_SCCODCES) and !this.bDontReportError
            deferred_cp_zoom('CES_PITI','*','CECODICE',cp_AbsName(oSource.parent,'oSCCODCES_1_1'),i_cWhere,'GSCE_ACE',"Cespite",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CECODICE,CEDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CECODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CECODICE',oSource.xKey(1))
            select CECODICE,CEDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SCCODCES)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CECODICE,CEDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CECODICE="+cp_ToStrODBC(this.w_SCCODCES);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CECODICE',this.w_SCCODCES)
            select CECODICE,CEDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SCCODCES = NVL(_Link_.CECODICE,space(20))
      this.w_DESCES = NVL(_Link_.CEDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_SCCODCES = space(20)
      endif
      this.w_DESCES = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CES_PITI_IDX,2])+'\'+cp_ToStr(_Link_.CECODICE,1)
      cp_ShowWarn(i_cKey,this.CES_PITI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SCCODCES Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CES_PITI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CES_PITI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_1.CECODICE as CECODICE101"+ ",link_1_1.CEDESCRI as CEDESCRI101"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_1 on SAL_CESE.SCCODCES=link_1_1.CECODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_1"
          i_cKey=i_cKey+'+" and SAL_CESE.SCCODCES=link_1_1.CECODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CODAZI
  func Link_1_54(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_CESP_IDX,3]
    i_lTable = "PAR_CESP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_CESP_IDX,2], .t., this.PAR_CESP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_CESP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PCCODAZI,PCTIPAMM";
                   +" from "+i_cTable+" "+i_lTable+" where PCCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PCCODAZI',this.w_CODAZI)
            select PCCODAZI,PCTIPAMM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.PCCODAZI,space(5))
      this.w_PCTIPAMM = NVL(_Link_.PCTIPAMM,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_PCTIPAMM = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_CESP_IDX,2])+'\'+cp_ToStr(_Link_.PCCODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_CESP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSCCODCES_1_1.value==this.w_SCCODCES)
      this.oPgFrm.Page1.oPag.oSCCODCES_1_1.value=this.w_SCCODCES
    endif
    if not(this.oPgFrm.Page2.oPag.oSCCOSBEN_2_1.value==this.w_SCCOSBEN)
      this.oPgFrm.Page2.oPag.oSCCOSBEN_2_1.value=this.w_SCCOSBEN
    endif
    if not(this.oPgFrm.Page2.oPag.oSCDURBEN_2_2.value==this.w_SCDURBEN)
      this.oPgFrm.Page2.oPag.oSCDURBEN_2_2.value=this.w_SCDURBEN
    endif
    if not(this.oPgFrm.Page2.oPag.oSCQUOESE_2_3.value==this.w_SCQUOESE)
      this.oPgFrm.Page2.oPag.oSCQUOESE_2_3.value=this.w_SCQUOESE
    endif
    if not(this.oPgFrm.Page2.oPag.oSCSPEPRE_2_4.value==this.w_SCSPEPRE)
      this.oPgFrm.Page2.oPag.oSCSPEPRE_2_4.value=this.w_SCSPEPRE
    endif
    if not(this.oPgFrm.Page2.oPag.oSCPERVAR_2_5.value==this.w_SCPERVAR)
      this.oPgFrm.Page2.oPag.oSCPERVAR_2_5.value=this.w_SCPERVAR
    endif
    if not(this.oPgFrm.Page2.oPag.oSCSPECOR_2_6.value==this.w_SCSPECOR)
      this.oPgFrm.Page2.oPag.oSCSPECOR_2_6.value=this.w_SCSPECOR
    endif
    if not(this.oPgFrm.Page2.oPag.oSCUNIMIS_2_7.value==this.w_SCUNIMIS)
      this.oPgFrm.Page2.oPag.oSCUNIMIS_2_7.value=this.w_SCUNIMIS
    endif
    if not(this.oPgFrm.Page2.oPag.oSCQTAPREV_2_8.value==this.w_SCQTAPREV)
      this.oPgFrm.Page2.oPag.oSCQTAPREV_2_8.value=this.w_SCQTAPREV
    endif
    if not(this.oPgFrm.Page2.oPag.oSCCOSUNI_2_9.value==this.w_SCCOSUNI)
      this.oPgFrm.Page2.oPag.oSCCOSUNI_2_9.value=this.w_SCCOSUNI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCES_1_3.value==this.w_DESCES)
      this.oPgFrm.Page1.oPag.oDESCES_1_3.value=this.w_DESCES
    endif
    if not(this.oPgFrm.Page1.oPag.oSCINCVAC_1_13.value==this.w_SCINCVAC)
      this.oPgFrm.Page1.oPag.oSCINCVAC_1_13.value=this.w_SCINCVAC
    endif
    if not(this.oPgFrm.Page1.oPag.oSCIMPONC_1_15.value==this.w_SCIMPONC)
      this.oPgFrm.Page1.oPag.oSCIMPONC_1_15.value=this.w_SCIMPONC
    endif
    if not(this.oPgFrm.Page1.oPag.oSCIMPRIC_1_17.value==this.w_SCIMPRIC)
      this.oPgFrm.Page1.oPag.oSCIMPRIC_1_17.value=this.w_SCIMPRIC
    endif
    if not(this.oPgFrm.Page1.oPag.oSCDECVAC_1_18.value==this.w_SCDECVAC)
      this.oPgFrm.Page1.oPag.oSCDECVAC_1_18.value=this.w_SCDECVAC
    endif
    if not(this.oPgFrm.Page1.oPag.oSCIMPSVC_1_20.value==this.w_SCIMPSVC)
      this.oPgFrm.Page1.oPag.oSCIMPSVC_1_20.value=this.w_SCIMPSVC
    endif
    if not(this.oPgFrm.Page1.oPag.oSCACCCIV_1_22.value==this.w_SCACCCIV)
      this.oPgFrm.Page1.oPag.oSCACCCIV_1_22.value=this.w_SCACCCIV
    endif
    if not(this.oPgFrm.Page1.oPag.oSCUTICIV_1_23.value==this.w_SCUTICIV)
      this.oPgFrm.Page1.oPag.oSCUTICIV_1_23.value=this.w_SCUTICIV
    endif
    if not(this.oPgFrm.Page1.oPag.oSCARRFOC_1_25.value==this.w_SCARRFOC)
      this.oPgFrm.Page1.oPag.oSCARRFOC_1_25.value=this.w_SCARRFOC
    endif
    if not(this.oPgFrm.Page1.oPag.oSCARRAMC_1_27.value==this.w_SCARRAMC)
      this.oPgFrm.Page1.oPag.oSCARRAMC_1_27.value=this.w_SCARRAMC
    endif
    if not(this.oPgFrm.Page1.oPag.oSCINCVAL_1_29.value==this.w_SCINCVAL)
      this.oPgFrm.Page1.oPag.oSCINCVAL_1_29.value=this.w_SCINCVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oSCIMPONS_1_31.value==this.w_SCIMPONS)
      this.oPgFrm.Page1.oPag.oSCIMPONS_1_31.value=this.w_SCIMPONS
    endif
    if not(this.oPgFrm.Page1.oPag.oSCIMPRIV_1_32.value==this.w_SCIMPRIV)
      this.oPgFrm.Page1.oPag.oSCIMPRIV_1_32.value=this.w_SCIMPRIV
    endif
    if not(this.oPgFrm.Page1.oPag.oSCDECVAL_1_34.value==this.w_SCDECVAL)
      this.oPgFrm.Page1.oPag.oSCDECVAL_1_34.value=this.w_SCDECVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oSCIMPSVA_1_36.value==this.w_SCIMPSVA)
      this.oPgFrm.Page1.oPag.oSCIMPSVA_1_36.value=this.w_SCIMPSVA
    endif
    if not(this.oPgFrm.Page1.oPag.oSCIMPNOA_1_38.value==this.w_SCIMPNOA)
      this.oPgFrm.Page1.oPag.oSCIMPNOA_1_38.value=this.w_SCIMPNOA
    endif
    if not(this.oPgFrm.Page1.oPag.oSCQUOPER_1_39.value==this.w_SCQUOPER)
      this.oPgFrm.Page1.oPag.oSCQUOPER_1_39.value=this.w_SCQUOPER
    endif
    if not(this.oPgFrm.Page1.oPag.oSCACCFIS_1_40.value==this.w_SCACCFIS)
      this.oPgFrm.Page1.oPag.oSCACCFIS_1_40.value=this.w_SCACCFIS
    endif
    if not(this.oPgFrm.Page1.oPag.oSCUTIFIS_1_41.value==this.w_SCUTIFIS)
      this.oPgFrm.Page1.oPag.oSCUTIFIS_1_41.value=this.w_SCUTIFIS
    endif
    if not(this.oPgFrm.Page1.oPag.oSCACCANT_1_42.value==this.w_SCACCANT)
      this.oPgFrm.Page1.oPag.oSCACCANT_1_42.value=this.w_SCACCANT
    endif
    if not(this.oPgFrm.Page1.oPag.oSCUTIANT_1_43.value==this.w_SCUTIANT)
      this.oPgFrm.Page1.oPag.oSCUTIANT_1_43.value=this.w_SCUTIANT
    endif
    if not(this.oPgFrm.Page1.oPag.oSCARRFON_1_45.value==this.w_SCARRFON)
      this.oPgFrm.Page1.oPag.oSCARRFON_1_45.value=this.w_SCARRFON
    endif
    if not(this.oPgFrm.Page1.oPag.oSCARRAMM_1_47.value==this.w_SCARRAMM)
      this.oPgFrm.Page1.oPag.oSCARRAMM_1_47.value=this.w_SCARRAMM
    endif
    cp_SetControlsValueExtFlds(this,'SAL_CESE')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsce_asePag1 as StdContainer
  Width  = 727
  height = 465
  stdWidth  = 727
  stdheight = 465
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSCCODCES_1_1 as StdField with uid="AENREOGANE",rtseq=1,rtrep=.f.,;
    cFormVar = "w_SCCODCES", cQueryName = "SCCODCES",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice cespite",;
    HelpContextID = 17380217,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=132, Left=70, Top=17, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="CES_PITI", cZoomOnZoom="GSCE_ACE", oKey_1_1="CECODICE", oKey_1_2="this.w_SCCODCES"

  func oSCCODCES_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oSCCODCES_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSCCODCES_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CES_PITI','*','CECODICE',cp_AbsName(this.parent,'oSCCODCES_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCE_ACE',"Cespite",'',this.parent.oContained
  endproc
  proc oSCCODCES_1_1.mZoomOnZoom
    local i_obj
    i_obj=GSCE_ACE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CECODICE=this.parent.oContained.w_SCCODCES
     i_obj.ecpSave()
  endproc

  add object oDESCES_1_3 as StdField with uid="KXPXSGLBWD",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DESCES", cQueryName = "DESCES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione cespite",;
    HelpContextID = 17708086,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=209, Top=17, InputMask=replicate('X',40)

  add object oSCINCVAC_1_13 as StdField with uid="HEVXBDFXNZ",rtseq=12,rtrep=.f.,;
    cFormVar = "w_SCINCVAC", cQueryName = "SCINCVAC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Incrementi valore civile del bene",;
    HelpContextID = 66622313,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=171, Top=76, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oSCINCVAC_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PCTIPAMM<>'F')
    endwith
   endif
  endfunc

  add object oSCIMPONC_1_15 as StdField with uid="BZSWBBFRSW",rtseq=13,rtrep=.f.,;
    cFormVar = "w_SCIMPONC", cQueryName = "SCIMPONC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Oneri e spese civili",;
    HelpContextID = 37252247,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=171, Top=106, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oSCIMPONC_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PCTIPAMM<>'F')
    endwith
   endif
  endfunc

  add object oSCIMPRIC_1_17 as StdField with uid="DOFGOVQGYQ",rtseq=14,rtrep=.f.,;
    cFormVar = "w_SCIMPRIC", cQueryName = "SCIMPRIC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Rivalutazioni civili",;
    HelpContextID = 255356055,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=171, Top=136, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oSCIMPRIC_1_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PCTIPAMM<>'F')
    endwith
   endif
  endfunc

  add object oSCDECVAC_1_18 as StdField with uid="KWMUUXLMYX",rtseq=15,rtrep=.f.,;
    cFormVar = "w_SCDECVAC", cQueryName = "SCDECVAC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Decrementi di valore civile",;
    HelpContextID = 66012009,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=171, Top=166, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oSCDECVAC_1_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PCTIPAMM<>'F')
    endwith
   endif
  endfunc

  add object oSCIMPSVC_1_20 as StdField with uid="KKWKDTAGOO",rtseq=16,rtrep=.f.,;
    cFormVar = "w_SCIMPSVC", cQueryName = "SCIMPSVC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Svalutazioni civili",;
    HelpContextID = 238578839,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=171, Top=196, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oSCIMPSVC_1_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PCTIPAMM<>'F')
    endwith
   endif
  endfunc

  add object oSCACCCIV_1_22 as StdField with uid="GJPIDTWUQP",rtseq=17,rtrep=.f.,;
    cFormVar = "w_SCACCCIV", cQueryName = "SCACCCIV",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Accantonamento civile",;
    HelpContextID = 252898436,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=171, Top=226, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oSCACCCIV_1_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PCTIPAMM<>'F')
    endwith
   endif
  endfunc

  add object oSCUTICIV_1_23 as StdField with uid="HJJMUZHMOI",rtseq=18,rtrep=.f.,;
    cFormVar = "w_SCUTICIV", cQueryName = "SCUTICIV",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Utilizzazione civile",;
    HelpContextID = 245410948,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=171, Top=256, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oSCUTICIV_1_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PCTIPAMM<>'F')
    endwith
   endif
  endfunc

  add object oSCARRFOC_1_25 as StdField with uid="ZBGQSXUKUF",rtseq=19,rtrep=.f.,;
    cFormVar = "w_SCARRFOC", cQueryName = "SCARRFOC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Arrotondamento da applicare al fondo ammortamento civile",;
    HelpContextID = 82580329,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=171, Top=410, cSayPict='"99999999999999.9999"', cGetPict='"99999999999999.9999"'

  func oSCARRFOC_1_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PCTIPAMM<>'F')
    endwith
   endif
  endfunc

  add object oSCARRAMC_1_27 as StdField with uid="GEPPCDJSCN",rtseq=20,rtrep=.f.,;
    cFormVar = "w_SCARRAMC", cQueryName = "SCARRAMC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Arrotondamento da applicare al residuo da ammortizzare civile",;
    HelpContextID = 1305751,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=171, Top=440, cSayPict='"99999999999999.9999"', cGetPict='"99999999999999.9999"'

  func oSCARRAMC_1_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PCTIPAMM<>'F')
    endwith
   endif
  endfunc

  add object oSCINCVAL_1_29 as StdField with uid="QUYQKRQHIA",rtseq=21,rtrep=.f.,;
    cFormVar = "w_SCINCVAL", cQueryName = "SCINCVAL",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Incrementi valore fiscale del bene",;
    HelpContextID = 66622322,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=535, Top=76, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oSCINCVAL_1_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PCTIPAMM<>'C')
    endwith
   endif
  endfunc

  add object oSCIMPONS_1_31 as StdField with uid="OYCUYMJEAH",rtseq=22,rtrep=.f.,;
    cFormVar = "w_SCIMPONS", cQueryName = "SCIMPONS",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Oneri e spese fiscali",;
    HelpContextID = 37252231,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=535, Top=106, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oSCIMPONS_1_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PCTIPAMM<>'C')
    endwith
   endif
  endfunc

  add object oSCIMPRIV_1_32 as StdField with uid="IQUVPBYXVF",rtseq=23,rtrep=.f.,;
    cFormVar = "w_SCIMPRIV", cQueryName = "SCIMPRIV",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Rivalutazioni fiscali",;
    HelpContextID = 255356036,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=535, Top=136, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oSCIMPRIV_1_32.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PCTIPAMM<>'C')
    endwith
   endif
  endfunc

  add object oSCDECVAL_1_34 as StdField with uid="NKTRZHWHFG",rtseq=24,rtrep=.f.,;
    cFormVar = "w_SCDECVAL", cQueryName = "SCDECVAL",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Decrementi di valore fiscale",;
    HelpContextID = 66012018,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=535, Top=166, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oSCDECVAL_1_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PCTIPAMM<>'C')
    endwith
   endif
  endfunc

  add object oSCIMPSVA_1_36 as StdField with uid="SEVSEZRIJX",rtseq=25,rtrep=.f.,;
    cFormVar = "w_SCIMPSVA", cQueryName = "SCIMPSVA",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Svalutazioni fiscali",;
    HelpContextID = 238578841,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=535, Top=196, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oSCIMPSVA_1_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PCTIPAMM<>'C')
    endwith
   endif
  endfunc

  add object oSCIMPNOA_1_38 as StdField with uid="GBNAJMBOVT",rtseq=26,rtrep=.f.,;
    cFormVar = "w_SCIMPNOA", cQueryName = "SCIMPNOA",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo non ammortizzabile fiscale",;
    HelpContextID = 214405991,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=535, Top=226, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oSCIMPNOA_1_38.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PCTIPAMM<>'C')
    endwith
   endif
  endfunc

  add object oSCQUOPER_1_39 as StdField with uid="VFPSMPUEMN",rtseq=27,rtrep=.f.,;
    cFormVar = "w_SCQUOPER", cQueryName = "SCQUOPER",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quote perse",;
    HelpContextID = 247468920,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=535, Top=256, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oSCQUOPER_1_39.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PCTIPAMM<>'C')
    endwith
   endif
  endfunc

  add object oSCACCFIS_1_40 as StdField with uid="YETMDUNMUX",rtseq=28,rtrep=.f.,;
    cFormVar = "w_SCACCFIS", cQueryName = "SCACCFIS",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Accantonamento ordinario",;
    HelpContextID = 202566791,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=536, Top=286, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oSCACCFIS_1_40.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PCTIPAMM<>'C')
    endwith
   endif
  endfunc

  add object oSCUTIFIS_1_41 as StdField with uid="TRKERZMNUK",rtseq=29,rtrep=.f.,;
    cFormVar = "w_SCUTIFIS", cQueryName = "SCUTIFIS",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Utilizzazione ordinaria",;
    HelpContextID = 195079303,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=536, Top=316, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oSCUTIFIS_1_41.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PCTIPAMM<>'C')
    endwith
   endif
  endfunc

  add object oSCACCANT_1_42 as StdField with uid="QDKUJZBANE",rtseq=30,rtrep=.f.,;
    cFormVar = "w_SCACCANT", cQueryName = "SCACCANT",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Accantonamento anticipato",;
    HelpContextID = 18017414,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=536, Top=346, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oSCACCANT_1_42.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PCTIPAMM<>'C')
    endwith
   endif
  endfunc

  add object oSCUTIANT_1_43 as StdField with uid="BBJZFXYJYZ",rtseq=31,rtrep=.f.,;
    cFormVar = "w_SCUTIANT", cQueryName = "SCUTIANT",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Utilizzazione anticipata",;
    HelpContextID = 10529926,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=536, Top=376, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oSCUTIANT_1_43.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PCTIPAMM<>'C')
    endwith
   endif
  endfunc

  add object oSCARRFON_1_45 as StdField with uid="XUVUSFYXPM",rtseq=32,rtrep=.f.,;
    cFormVar = "w_SCARRFON", cQueryName = "SCARRFON",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Arrotondamento da applicare al fondo ammortamento fiscale",;
    HelpContextID = 82580340,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=536, Top=410, cSayPict='"99999999999999.9999"', cGetPict='"99999999999999.9999"'

  func oSCARRFON_1_45.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PCTIPAMM<>'C')
    endwith
   endif
  endfunc

  add object oSCARRAMM_1_47 as StdField with uid="LTHHSCQZST",rtseq=33,rtrep=.f.,;
    cFormVar = "w_SCARRAMM", cQueryName = "SCARRAMM",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Arrotondamento da applicare al residuo da ammortizzare fiscale",;
    HelpContextID = 1305741,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=536, Top=440, cSayPict='"99999999999999.9999"', cGetPict='"99999999999999.9999"'

  func oSCARRAMM_1_47.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PCTIPAMM<>'C')
    endwith
   endif
  endfunc

  add object oStr_1_2 as StdString with uid="USEPFIXVUS",Visible=.t., Left=38, Top=48,;
    Alignment=0, Width=126, Height=18,;
    Caption="Aspetto civile"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_4 as StdString with uid="LMWSLGMMWS",Visible=.t., Left=6, Top=17,;
    Alignment=1, Width=62, Height=18,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_5 as StdString with uid="VJFHOTCPBY",Visible=.t., Left=372, Top=440,;
    Alignment=1, Width=163, Height=18,;
    Caption="Residuo da ammortizzare:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="SDNJNKHQIU",Visible=.t., Left=387, Top=409,;
    Alignment=1, Width=148, Height=18,;
    Caption="Ammortamento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="TITEWJHFJO",Visible=.t., Left=20, Top=409,;
    Alignment=1, Width=148, Height=18,;
    Caption="Ammortamento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="NBLCWAWTCF",Visible=.t., Left=38, Top=366,;
    Alignment=0, Width=151, Height=18,;
    Caption="Arrotondamenti"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_9 as StdString with uid="OBOCIXQHFW",Visible=.t., Left=38, Top=385,;
    Alignment=0, Width=324, Height=17,;
    Caption="(Per problemi di calcolo numerico su stampe registri/saldi)"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_10 as StdString with uid="XVHIKXQRQM",Visible=.t., Left=5, Top=440,;
    Alignment=1, Width=163, Height=18,;
    Caption="Residuo da ammortizzare:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="RCQLKFMUFY",Visible=.t., Left=402, Top=48,;
    Alignment=0, Width=155, Height=15,;
    Caption="Aspetto fiscale"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_14 as StdString with uid="DVUKTAZFNG",Visible=.t., Left=38, Top=76,;
    Alignment=1, Width=130, Height=15,;
    Caption="Increm. valore bene:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="ZLBRYCBYIF",Visible=.t., Left=38, Top=106,;
    Alignment=1, Width=130, Height=15,;
    Caption="Oneri e spese:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="XQUQNTVTGL",Visible=.t., Left=38, Top=166,;
    Alignment=1, Width=130, Height=15,;
    Caption="Decrementi valore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="VJUTRANOOT",Visible=.t., Left=38, Top=196,;
    Alignment=1, Width=130, Height=15,;
    Caption="Svalutazioni:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="KDCGAWEKNP",Visible=.t., Left=38, Top=226,;
    Alignment=1, Width=130, Height=15,;
    Caption="Accantonamento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="HNVZSHIUKA",Visible=.t., Left=38, Top=256,;
    Alignment=1, Width=130, Height=15,;
    Caption="Utilizzazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="NUZVIKAEMJ",Visible=.t., Left=38, Top=136,;
    Alignment=1, Width=130, Height=15,;
    Caption="Rivalutazioni:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="FVXOWSKYDB",Visible=.t., Left=402, Top=76,;
    Alignment=1, Width=130, Height=15,;
    Caption="Increm. valore bene:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="HZYFGRXGSU",Visible=.t., Left=402, Top=106,;
    Alignment=1, Width=130, Height=15,;
    Caption="Oneri e spese:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="UAJXUHSOBN",Visible=.t., Left=402, Top=166,;
    Alignment=1, Width=130, Height=15,;
    Caption="Decrementi valore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="KLNYHOJPMB",Visible=.t., Left=402, Top=196,;
    Alignment=1, Width=130, Height=15,;
    Caption="Svalutazioni:"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="XXJXWZDOCB",Visible=.t., Left=398, Top=226,;
    Alignment=1, Width=134, Height=15,;
    Caption="Importo non ammort.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="EFHTROTBHY",Visible=.t., Left=402, Top=256,;
    Alignment=1, Width=130, Height=15,;
    Caption="Quote perse:"  ;
  , bGlobalFont=.t.

  add object oStr_1_48 as StdString with uid="SEQTFHJBQG",Visible=.t., Left=342, Top=286,;
    Alignment=1, Width=191, Height=18,;
    Caption="Accantonamento ordinario:"  ;
  , bGlobalFont=.t.

  add object oStr_1_49 as StdString with uid="VHNBIUAAFA",Visible=.t., Left=403, Top=316,;
    Alignment=1, Width=130, Height=15,;
    Caption="Utilizzazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_50 as StdString with uid="ZRJDEJCBZH",Visible=.t., Left=342, Top=346,;
    Alignment=1, Width=191, Height=18,;
    Caption="Accantonamento anticipato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_51 as StdString with uid="HOUKMZJGRH",Visible=.t., Left=403, Top=376,;
    Alignment=1, Width=130, Height=15,;
    Caption="Utilizzazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_52 as StdString with uid="EMUMDTHGMG",Visible=.t., Left=402, Top=136,;
    Alignment=1, Width=130, Height=15,;
    Caption="Rivalutazioni:"  ;
  , bGlobalFont=.t.

  add object oBox_1_11 as StdBox with uid="XNTPGRZHAC",left=2, top=66, width=725,height=1

  add object oBox_1_53 as StdBox with uid="EVKRKDWFMC",left=2, top=403, width=725,height=1
enddefine
define class tgsce_asePag2 as StdContainer
  Width  = 727
  height = 465
  stdWidth  = 727
  stdheight = 465
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSCCOSBEN_2_1 as StdField with uid="JURFQUTXKT",rtseq=2,rtrep=.f.,;
    cFormVar = "w_SCCOSBEN", cQueryName = "SCCOSBEN",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Costo gestionale del bene",;
    HelpContextID = 16331636,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=121, Top=31, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  add object oSCDURBEN_2_2 as StdField with uid="IYAKNZWXCQ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_SCDURBEN", cQueryName = "SCDURBEN",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Durata gestionale del bene",;
    HelpContextID = 15680372,;
   bGlobalFont=.t.,;
    Height=21, Width=56, Left=341, Top=31, cSayPict='"999.99"', cGetPict='"999.99"'

  add object oSCQUOESE_2_3 as StdField with uid="FTDFSZSUQK",rtseq=4,rtrep=.f.,;
    cFormVar = "w_SCQUOESE", cQueryName = "SCQUOESE",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quota accantonamento gestionale dell'esercizio",;
    HelpContextID = 62919531,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=572, Top=31, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  add object oSCSPEPRE_2_4 as StdField with uid="KSUBRJVIVH",rtseq=5,rtrep=.f.,;
    cFormVar = "w_SCSPEPRE", cQueryName = "SCSPEPRE",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Spese gestionali del cespite dell'esercizio precedente",;
    HelpContextID = 236663659,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=121, Top=97, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  add object oSCPERVAR_2_5 as StdField with uid="SPAUFUOECK",rtseq=6,rtrep=.f.,;
    cFormVar = "w_SCPERVAR", cQueryName = "SCPERVAR",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Variazioni rispetto alla spese dell'esercizio precedente",;
    HelpContextID = 81789816,;
   bGlobalFont=.t.,;
    Height=21, Width=56, Left=341, Top=97, cSayPict='"999.99"', cGetPict='"999.99"'

  add object oSCSPECOR_2_6 as StdField with uid="VEBRCRDSMP",rtseq=7,rtrep=.f.,;
    cFormVar = "w_SCSPECOR", cQueryName = "SCSPECOR",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Spese gestionali previste per l'esercizio in corso",;
    HelpContextID = 18559864,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=572, Top=97, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  add object oSCUNIMIS_2_7 as StdField with uid="JMUBDBISJW",rtseq=8,rtrep=.f.,;
    cFormVar = "w_SCUNIMIS", cQueryName = "SCUNIMIS",;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Unit� di misura di impiego del cespite",;
    HelpContextID = 78032007,;
   bGlobalFont=.t.,;
    Height=21, Width=32, Left=121, Top=163, InputMask=replicate('X',3)

  add object oSCQTAPREV_2_8 as StdField with uid="GGYHWOIUIG",rtseq=9,rtrep=.f.,;
    cFormVar = "w_SCQTAPREV", cQueryName = "SCQTAPREV",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quantit� totale prevista relativa all'impiego",;
    HelpContextID = 232724683,;
   bGlobalFont=.t.,;
    Height=21, Width=104, Left=341, Top=163, cSayPict="v_PQ(12)", cGetPict="v_GQ(12)"

  add object oSCCOSUNI_2_9 as StdField with uid="EIMXHGTZVP",rtseq=10,rtrep=.f.,;
    cFormVar = "w_SCCOSUNI", cQueryName = "SCCOSUNI",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Costo unitario del cespite per unit� di misura",;
    HelpContextID = 66663279,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=572, Top=163, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  add object oStr_2_10 as StdString with uid="QRULIBTGTN",Visible=.t., Left=260, Top=31,;
    Alignment=1, Width=76, Height=15,;
    Caption="Durata:"  ;
  , bGlobalFont=.t.

  add object oStr_2_11 as StdString with uid="KMYRSIMHJC",Visible=.t., Left=409, Top=31,;
    Alignment=1, Width=159, Height=15,;
    Caption="Quota dell'esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_2_12 as StdString with uid="LWSTNZPEON",Visible=.t., Left=10, Top=97,;
    Alignment=1, Width=109, Height=15,;
    Caption="Spese es. preced.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_13 as StdString with uid="TGRIKPCZYE",Visible=.t., Left=262, Top=97,;
    Alignment=1, Width=74, Height=15,;
    Caption="Variaz. %:"  ;
  , bGlobalFont=.t.

  add object oStr_2_14 as StdString with uid="KDYUYTKYPH",Visible=.t., Left=403, Top=97,;
    Alignment=1, Width=165, Height=15,;
    Caption="Spese prev. es. in corso:"  ;
  , bGlobalFont=.t.

  add object oStr_2_15 as StdString with uid="JQYYIJQJRD",Visible=.t., Left=10, Top=163,;
    Alignment=1, Width=107, Height=15,;
    Caption="Unit� di misura:"  ;
  , bGlobalFont=.t.

  add object oStr_2_16 as StdString with uid="NSAKMAIOLM",Visible=.t., Left=161, Top=163,;
    Alignment=1, Width=175, Height=15,;
    Caption="Quantit� prevista:"  ;
  , bGlobalFont=.t.

  add object oStr_2_17 as StdString with uid="FPFXPEELUI",Visible=.t., Left=449, Top=163,;
    Alignment=1, Width=119, Height=15,;
    Caption="Costo unitario:"  ;
  , bGlobalFont=.t.

  add object oStr_2_18 as StdString with uid="LPPTRDKOLH",Visible=.t., Left=11, Top=31,;
    Alignment=1, Width=108, Height=15,;
    Caption="Costo del bene:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsce_ase','SAL_CESE','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".SCCODCES=SAL_CESE.SCCODCES";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
