* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_moc                                                        *
*              Modelli contabili                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-03-24                                                      *
* Last revis.: 2011-02-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsce_moc")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsce_moc")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsce_moc")
  return

* --- Class definition
define class tgsce_moc as StdPCForm
  Width  = 690
  Height = 243
  Top    = 124
  Left   = 10
  cComment = "Modelli contabili"
  cPrg = "gsce_moc"
  HelpContextID=164250985
  add object cnt as tcgsce_moc
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsce_moc as PCContext
  w_MCCODCAU = space(5)
  w_CODAZI = space(5)
  w_CONC01 = space(30)
  w_CONC02 = space(30)
  w_CONC03 = space(30)
  w_CONC04 = space(30)
  w_CONC05 = space(30)
  w_CONC06 = space(30)
  w_CONC07 = space(30)
  w_CONC08 = space(30)
  w_CONC09 = space(30)
  w_CONC10 = space(30)
  w_CONC11 = space(30)
  w_CONC12 = space(30)
  w_CONC13 = space(30)
  w_CONC14 = space(30)
  w_CONC15 = space(30)
  w_CONC16 = space(30)
  w_ALLCO1 = space(10)
  w_ALLCO2 = space(10)
  w_ALLCO3 = space(10)
  w_ALLCO4 = space(10)
  w_ALLCO5 = space(10)
  w_ALLCO6 = space(10)
  w_ALLCO7 = space(10)
  w_ALLCO8 = space(10)
  w_ALLCOX = space(10)
  w_CPROWORD = 0
  w_MCCODICE = space(3)
  w_MCCONDAR = space(3)
  w_MCCONAVE = space(3)
  w_DESA = space(25)
  w_DESCD = space(25)
  w_DESCA = space(25)
  proc Save(i_oFrom)
    this.w_MCCODCAU = i_oFrom.w_MCCODCAU
    this.w_CODAZI = i_oFrom.w_CODAZI
    this.w_CONC01 = i_oFrom.w_CONC01
    this.w_CONC02 = i_oFrom.w_CONC02
    this.w_CONC03 = i_oFrom.w_CONC03
    this.w_CONC04 = i_oFrom.w_CONC04
    this.w_CONC05 = i_oFrom.w_CONC05
    this.w_CONC06 = i_oFrom.w_CONC06
    this.w_CONC07 = i_oFrom.w_CONC07
    this.w_CONC08 = i_oFrom.w_CONC08
    this.w_CONC09 = i_oFrom.w_CONC09
    this.w_CONC10 = i_oFrom.w_CONC10
    this.w_CONC11 = i_oFrom.w_CONC11
    this.w_CONC12 = i_oFrom.w_CONC12
    this.w_CONC13 = i_oFrom.w_CONC13
    this.w_CONC14 = i_oFrom.w_CONC14
    this.w_CONC15 = i_oFrom.w_CONC15
    this.w_CONC16 = i_oFrom.w_CONC16
    this.w_ALLCO1 = i_oFrom.w_ALLCO1
    this.w_ALLCO2 = i_oFrom.w_ALLCO2
    this.w_ALLCO3 = i_oFrom.w_ALLCO3
    this.w_ALLCO4 = i_oFrom.w_ALLCO4
    this.w_ALLCO5 = i_oFrom.w_ALLCO5
    this.w_ALLCO6 = i_oFrom.w_ALLCO6
    this.w_ALLCO7 = i_oFrom.w_ALLCO7
    this.w_ALLCO8 = i_oFrom.w_ALLCO8
    this.w_ALLCOX = i_oFrom.w_ALLCOX
    this.w_CPROWORD = i_oFrom.w_CPROWORD
    this.w_MCCODICE = i_oFrom.w_MCCODICE
    this.w_MCCONDAR = i_oFrom.w_MCCONDAR
    this.w_MCCONAVE = i_oFrom.w_MCCONAVE
    this.w_DESA = i_oFrom.w_DESA
    this.w_DESCD = i_oFrom.w_DESCD
    this.w_DESCA = i_oFrom.w_DESCA
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_MCCODCAU = this.w_MCCODCAU
    i_oTo.w_CODAZI = this.w_CODAZI
    i_oTo.w_CONC01 = this.w_CONC01
    i_oTo.w_CONC02 = this.w_CONC02
    i_oTo.w_CONC03 = this.w_CONC03
    i_oTo.w_CONC04 = this.w_CONC04
    i_oTo.w_CONC05 = this.w_CONC05
    i_oTo.w_CONC06 = this.w_CONC06
    i_oTo.w_CONC07 = this.w_CONC07
    i_oTo.w_CONC08 = this.w_CONC08
    i_oTo.w_CONC09 = this.w_CONC09
    i_oTo.w_CONC10 = this.w_CONC10
    i_oTo.w_CONC11 = this.w_CONC11
    i_oTo.w_CONC12 = this.w_CONC12
    i_oTo.w_CONC13 = this.w_CONC13
    i_oTo.w_CONC14 = this.w_CONC14
    i_oTo.w_CONC15 = this.w_CONC15
    i_oTo.w_CONC16 = this.w_CONC16
    i_oTo.w_ALLCO1 = this.w_ALLCO1
    i_oTo.w_ALLCO2 = this.w_ALLCO2
    i_oTo.w_ALLCO3 = this.w_ALLCO3
    i_oTo.w_ALLCO4 = this.w_ALLCO4
    i_oTo.w_ALLCO5 = this.w_ALLCO5
    i_oTo.w_ALLCO6 = this.w_ALLCO6
    i_oTo.w_ALLCO7 = this.w_ALLCO7
    i_oTo.w_ALLCO8 = this.w_ALLCO8
    i_oTo.w_ALLCOX = this.w_ALLCOX
    i_oTo.w_CPROWORD = this.w_CPROWORD
    i_oTo.w_MCCODICE = this.w_MCCODICE
    i_oTo.w_MCCONDAR = this.w_MCCONDAR
    i_oTo.w_MCCONAVE = this.w_MCCONAVE
    i_oTo.w_DESA = this.w_DESA
    i_oTo.w_DESCD = this.w_DESCD
    i_oTo.w_DESCA = this.w_DESCA
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsce_moc as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 690
  Height = 243
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-02-28"
  HelpContextID=164250985
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=34

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  MCO_CESP_IDX = 0
  PAR_CESP_IDX = 0
  cFile = "MCO_CESP"
  cKeySelect = "MCCODCAU"
  cKeyWhere  = "MCCODCAU=this.w_MCCODCAU"
  cKeyDetail  = "MCCODCAU=this.w_MCCODCAU"
  cKeyWhereODBC = '"MCCODCAU="+cp_ToStrODBC(this.w_MCCODCAU)';

  cKeyDetailWhereODBC = '"MCCODCAU="+cp_ToStrODBC(this.w_MCCODCAU)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"MCO_CESP.MCCODCAU="+cp_ToStrODBC(this.w_MCCODCAU)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'MCO_CESP.CPROWORD '
  cPrg = "gsce_moc"
  cComment = "Modelli contabili"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_MCCODCAU = space(5)
  w_CODAZI = space(5)
  w_CONC01 = space(30)
  w_CONC02 = space(30)
  w_CONC03 = space(30)
  w_CONC04 = space(30)
  w_CONC05 = space(30)
  w_CONC06 = space(30)
  w_CONC07 = space(30)
  w_CONC08 = space(30)
  w_CONC09 = space(30)
  w_CONC10 = space(30)
  w_CONC11 = space(30)
  w_CONC12 = space(30)
  w_CONC13 = space(30)
  w_CONC14 = space(30)
  w_CONC15 = space(30)
  w_CONC16 = space(30)
  w_ALLCO1 = space(10)
  w_ALLCO2 = space(10)
  w_ALLCO3 = space(10)
  w_ALLCO4 = space(10)
  w_ALLCO5 = space(10)
  w_ALLCO6 = space(10)
  w_ALLCO7 = space(10)
  w_ALLCO8 = space(10)
  w_ALLCOX = space(10)
  w_CPROWORD = 0
  w_MCCODICE = space(3)
  w_MCCONDAR = space(3)
  w_MCCONAVE = space(3)
  w_DESA = space(25)
  w_DESCD = space(25)
  w_DESCA = space(25)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsce_mocPag1","gsce_moc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='PAR_CESP'
    this.cWorkTables[2]='MCO_CESP'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.MCO_CESP_IDX,5],7]
    this.nPostItConn=i_TableProp[this.MCO_CESP_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsce_moc'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from MCO_CESP where MCCODCAU=KeySet.MCCODCAU
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.MCO_CESP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MCO_CESP_IDX,2],this.bLoadRecFilter,this.MCO_CESP_IDX,"gsce_moc")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('MCO_CESP')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "MCO_CESP.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' MCO_CESP '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'MCCODCAU',this.w_MCCODCAU  )
      select * from (i_cTable) MCO_CESP where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_CODAZI = i_CODAZI
        .w_CONC01 = space(30)
        .w_CONC02 = space(30)
        .w_CONC03 = space(30)
        .w_CONC04 = space(30)
        .w_CONC05 = space(30)
        .w_CONC06 = space(30)
        .w_CONC07 = space(30)
        .w_CONC08 = space(30)
        .w_CONC09 = space(30)
        .w_CONC10 = space(30)
        .w_CONC11 = space(30)
        .w_CONC12 = space(30)
        .w_CONC13 = space(30)
        .w_CONC14 = space(30)
        .w_CONC15 = space(30)
        .w_CONC16 = space(30)
        .w_MCCODCAU = NVL(MCCODCAU,space(5))
          .link_1_2('Load')
        .w_ALLCO1 = LEFT(ALLTRIM(.w_CONC01)+SPACE(30),30)+LEFT(ALLTRIM(.w_CONC02)+SPACE(30),30)
        .w_ALLCO2 = LEFT(ALLTRIM(.w_CONC03)+SPACE(30),30)+LEFT(ALLTRIM(.w_CONC04)+SPACE(30),30)
        .w_ALLCO3 = LEFT(ALLTRIM(.w_CONC05)+SPACE(30),30)+LEFT(ALLTRIM(.w_CONC06)+SPACE(30),30)
        .w_ALLCO4 = LEFT(ALLTRIM(.w_CONC07)+SPACE(30),30)+LEFT(ALLTRIM(.w_CONC08)+SPACE(30),30)
        .w_ALLCO5 = LEFT(ALLTRIM(.w_CONC09)+SPACE(30),30)+LEFT(ALLTRIM(.w_CONC10)+SPACE(30),30)
        .w_ALLCO6 = LEFT(ALLTRIM(.w_CONC11)+SPACE(30),30)+LEFT(ALLTRIM(.w_CONC12)+SPACE(30),30)
        .w_ALLCO7 = LEFT(ALLTRIM(.w_CONC13)+SPACE(30),30)+LEFT(ALLTRIM(.w_CONC14)+SPACE(30),30)
        .w_ALLCO8 = LEFT(ALLTRIM(.w_CONC15)+SPACE(30),30)+LEFT(ALLTRIM(.w_CONC16)+SPACE(30),30)
        .w_ALLCOX = .w_ALLCO1+.w_ALLCO2+.w_ALLCO3+.w_ALLCO4+.w_ALLCO5+.w_ALLCO6+.w_ALLCO7+.w_ALLCO8
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'MCO_CESP')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_MCCODICE = NVL(MCCODICE,space(3))
          .w_MCCONDAR = NVL(MCCONDAR,space(3))
          .w_MCCONAVE = NVL(MCCONAVE,space(3))
        .w_DESA = CHKEXMO(.w_MCCODICE, 2)
        .w_DESCD = CHKEXMO(LEFT(ALLTRIM(.w_MCCONDAR)+SPACE(3),3)+.w_ALLCOX, 3)
        .w_DESCA = CHKEXMO(LEFT(ALLTRIM(.w_MCCONAVE)+SPACE(3),3)+.w_ALLCOX, 3)
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .w_ALLCO1 = LEFT(ALLTRIM(.w_CONC01)+SPACE(30),30)+LEFT(ALLTRIM(.w_CONC02)+SPACE(30),30)
        .w_ALLCO2 = LEFT(ALLTRIM(.w_CONC03)+SPACE(30),30)+LEFT(ALLTRIM(.w_CONC04)+SPACE(30),30)
        .w_ALLCO3 = LEFT(ALLTRIM(.w_CONC05)+SPACE(30),30)+LEFT(ALLTRIM(.w_CONC06)+SPACE(30),30)
        .w_ALLCO4 = LEFT(ALLTRIM(.w_CONC07)+SPACE(30),30)+LEFT(ALLTRIM(.w_CONC08)+SPACE(30),30)
        .w_ALLCO5 = LEFT(ALLTRIM(.w_CONC09)+SPACE(30),30)+LEFT(ALLTRIM(.w_CONC10)+SPACE(30),30)
        .w_ALLCO6 = LEFT(ALLTRIM(.w_CONC11)+SPACE(30),30)+LEFT(ALLTRIM(.w_CONC12)+SPACE(30),30)
        .w_ALLCO7 = LEFT(ALLTRIM(.w_CONC13)+SPACE(30),30)+LEFT(ALLTRIM(.w_CONC14)+SPACE(30),30)
        .w_ALLCO8 = LEFT(ALLTRIM(.w_CONC15)+SPACE(30),30)+LEFT(ALLTRIM(.w_CONC16)+SPACE(30),30)
        .w_ALLCOX = .w_ALLCO1+.w_ALLCO2+.w_ALLCO3+.w_ALLCO4+.w_ALLCO5+.w_ALLCO6+.w_ALLCO7+.w_ALLCO8
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_MCCODCAU=space(5)
      .w_CODAZI=space(5)
      .w_CONC01=space(30)
      .w_CONC02=space(30)
      .w_CONC03=space(30)
      .w_CONC04=space(30)
      .w_CONC05=space(30)
      .w_CONC06=space(30)
      .w_CONC07=space(30)
      .w_CONC08=space(30)
      .w_CONC09=space(30)
      .w_CONC10=space(30)
      .w_CONC11=space(30)
      .w_CONC12=space(30)
      .w_CONC13=space(30)
      .w_CONC14=space(30)
      .w_CONC15=space(30)
      .w_CONC16=space(30)
      .w_ALLCO1=space(10)
      .w_ALLCO2=space(10)
      .w_ALLCO3=space(10)
      .w_ALLCO4=space(10)
      .w_ALLCO5=space(10)
      .w_ALLCO6=space(10)
      .w_ALLCO7=space(10)
      .w_ALLCO8=space(10)
      .w_ALLCOX=space(10)
      .w_CPROWORD=10
      .w_MCCODICE=space(3)
      .w_MCCONDAR=space(3)
      .w_MCCONAVE=space(3)
      .w_DESA=space(25)
      .w_DESCD=space(25)
      .w_DESCA=space(25)
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
        .w_CODAZI = i_CODAZI
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODAZI))
         .link_1_2('Full')
        endif
        .DoRTCalc(3,18,.f.)
        .w_ALLCO1 = LEFT(ALLTRIM(.w_CONC01)+SPACE(30),30)+LEFT(ALLTRIM(.w_CONC02)+SPACE(30),30)
        .w_ALLCO2 = LEFT(ALLTRIM(.w_CONC03)+SPACE(30),30)+LEFT(ALLTRIM(.w_CONC04)+SPACE(30),30)
        .w_ALLCO3 = LEFT(ALLTRIM(.w_CONC05)+SPACE(30),30)+LEFT(ALLTRIM(.w_CONC06)+SPACE(30),30)
        .w_ALLCO4 = LEFT(ALLTRIM(.w_CONC07)+SPACE(30),30)+LEFT(ALLTRIM(.w_CONC08)+SPACE(30),30)
        .w_ALLCO5 = LEFT(ALLTRIM(.w_CONC09)+SPACE(30),30)+LEFT(ALLTRIM(.w_CONC10)+SPACE(30),30)
        .w_ALLCO6 = LEFT(ALLTRIM(.w_CONC11)+SPACE(30),30)+LEFT(ALLTRIM(.w_CONC12)+SPACE(30),30)
        .w_ALLCO7 = LEFT(ALLTRIM(.w_CONC13)+SPACE(30),30)+LEFT(ALLTRIM(.w_CONC14)+SPACE(30),30)
        .w_ALLCO8 = LEFT(ALLTRIM(.w_CONC15)+SPACE(30),30)+LEFT(ALLTRIM(.w_CONC16)+SPACE(30),30)
        .w_ALLCOX = .w_ALLCO1+.w_ALLCO2+.w_ALLCO3+.w_ALLCO4+.w_ALLCO5+.w_ALLCO6+.w_ALLCO7+.w_ALLCO8
        .DoRTCalc(28,31,.f.)
        .w_DESA = CHKEXMO(.w_MCCODICE, 2)
        .w_DESCD = CHKEXMO(LEFT(ALLTRIM(.w_MCCONDAR)+SPACE(3),3)+.w_ALLCOX, 3)
        .w_DESCA = CHKEXMO(LEFT(ALLTRIM(.w_MCCONAVE)+SPACE(3),3)+.w_ALLCOX, 3)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'MCO_CESP')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'MCO_CESP',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.MCO_CESP_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCCODCAU,"MCCODCAU",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(3);
      ,t_MCCODICE C(3);
      ,t_MCCONDAR C(3);
      ,t_MCCONAVE C(3);
      ,t_DESA C(25);
      ,t_DESCD C(25);
      ,t_DESCA C(25);
      ,CPROWNUM N(10);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsce_mocbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMCCODICE_2_2.controlsource=this.cTrsName+'.t_MCCODICE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMCCONDAR_2_3.controlsource=this.cTrsName+'.t_MCCONDAR'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMCCONAVE_2_4.controlsource=this.cTrsName+'.t_MCCONAVE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDESA_2_5.controlsource=this.cTrsName+'.t_DESA'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDESCD_2_6.controlsource=this.cTrsName+'.t_DESCD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDESCA_2_7.controlsource=this.cTrsName+'.t_DESCA'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(37)
    this.AddVLine(79)
    this.AddVLine(247)
    this.AddVLine(289)
    this.AddVLine(457)
    this.AddVLine(499)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.MCO_CESP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MCO_CESP_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.MCO_CESP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MCO_CESP_IDX,2])
      *
      * insert into MCO_CESP
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'MCO_CESP')
        i_extval=cp_InsertValODBCExtFlds(this,'MCO_CESP')
        i_cFldBody=" "+;
                  "(MCCODCAU,CPROWORD,MCCODICE,MCCONDAR,MCCONAVE,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_MCCODCAU)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBC(this.w_MCCODICE)+","+cp_ToStrODBC(this.w_MCCONDAR)+","+cp_ToStrODBC(this.w_MCCONAVE)+;
             ","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'MCO_CESP')
        i_extval=cp_InsertValVFPExtFlds(this,'MCO_CESP')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'MCCODCAU',this.w_MCCODCAU)
        INSERT INTO (i_cTable) (;
                   MCCODCAU;
                  ,CPROWORD;
                  ,MCCODICE;
                  ,MCCONDAR;
                  ,MCCONAVE;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_MCCODCAU;
                  ,this.w_CPROWORD;
                  ,this.w_MCCODICE;
                  ,this.w_MCCONDAR;
                  ,this.w_MCCONAVE;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.MCO_CESP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MCO_CESP_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (t_CPROWORD<>0 AND NOT EMPTY(t_MCCODICE)) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'MCO_CESP')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'MCO_CESP')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (t_CPROWORD<>0 AND NOT EMPTY(t_MCCODICE)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update MCO_CESP
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'MCO_CESP')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",MCCODICE="+cp_ToStrODBC(this.w_MCCODICE)+;
                     ",MCCONDAR="+cp_ToStrODBC(this.w_MCCONDAR)+;
                     ",MCCONAVE="+cp_ToStrODBC(this.w_MCCONAVE)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'MCO_CESP')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,MCCODICE=this.w_MCCODICE;
                     ,MCCONDAR=this.w_MCCONDAR;
                     ,MCCONAVE=this.w_MCCONAVE;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.MCO_CESP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MCO_CESP_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (t_CPROWORD<>0 AND NOT EMPTY(t_MCCODICE)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete MCO_CESP
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (t_CPROWORD<>0 AND NOT EMPTY(t_MCCODICE)) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.MCO_CESP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MCO_CESP_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
          .link_1_2('Full')
        .DoRTCalc(3,18,.t.)
          .w_ALLCO1 = LEFT(ALLTRIM(.w_CONC01)+SPACE(30),30)+LEFT(ALLTRIM(.w_CONC02)+SPACE(30),30)
          .w_ALLCO2 = LEFT(ALLTRIM(.w_CONC03)+SPACE(30),30)+LEFT(ALLTRIM(.w_CONC04)+SPACE(30),30)
          .w_ALLCO3 = LEFT(ALLTRIM(.w_CONC05)+SPACE(30),30)+LEFT(ALLTRIM(.w_CONC06)+SPACE(30),30)
          .w_ALLCO4 = LEFT(ALLTRIM(.w_CONC07)+SPACE(30),30)+LEFT(ALLTRIM(.w_CONC08)+SPACE(30),30)
          .w_ALLCO5 = LEFT(ALLTRIM(.w_CONC09)+SPACE(30),30)+LEFT(ALLTRIM(.w_CONC10)+SPACE(30),30)
          .w_ALLCO6 = LEFT(ALLTRIM(.w_CONC11)+SPACE(30),30)+LEFT(ALLTRIM(.w_CONC12)+SPACE(30),30)
          .w_ALLCO7 = LEFT(ALLTRIM(.w_CONC13)+SPACE(30),30)+LEFT(ALLTRIM(.w_CONC14)+SPACE(30),30)
          .w_ALLCO8 = LEFT(ALLTRIM(.w_CONC15)+SPACE(30),30)+LEFT(ALLTRIM(.w_CONC16)+SPACE(30),30)
          .w_ALLCOX = .w_ALLCO1+.w_ALLCO2+.w_ALLCO3+.w_ALLCO4+.w_ALLCO5+.w_ALLCO6+.w_ALLCO7+.w_ALLCO8
        .DoRTCalc(28,31,.t.)
          .w_DESA = CHKEXMO(.w_MCCODICE, 2)
          .w_DESCD = CHKEXMO(LEFT(ALLTRIM(.w_MCCONDAR)+SPACE(3),3)+.w_ALLCOX, 3)
          .w_DESCA = CHKEXMO(LEFT(ALLTRIM(.w_MCCONAVE)+SPACE(3),3)+.w_ALLCOX, 3)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_CESP_IDX,3]
    i_lTable = "PAR_CESP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_CESP_IDX,2], .t., this.PAR_CESP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_CESP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PCCODAZI,PCCONC01,PCCONC02,PCCONC03,PCCONC04,PCCONC05,PCCONC06,PCCONC07,PCCONC08,PCCONC09,PCCONC10,PCCONC11,PCCONC12,PCCONC13,PCCONC14,PCCONC15,PCCONC16";
                   +" from "+i_cTable+" "+i_lTable+" where PCCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PCCODAZI',this.w_CODAZI)
            select PCCODAZI,PCCONC01,PCCONC02,PCCONC03,PCCONC04,PCCONC05,PCCONC06,PCCONC07,PCCONC08,PCCONC09,PCCONC10,PCCONC11,PCCONC12,PCCONC13,PCCONC14,PCCONC15,PCCONC16;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.PCCODAZI,space(5))
      this.w_CONC01 = NVL(_Link_.PCCONC01,space(30))
      this.w_CONC02 = NVL(_Link_.PCCONC02,space(30))
      this.w_CONC03 = NVL(_Link_.PCCONC03,space(30))
      this.w_CONC04 = NVL(_Link_.PCCONC04,space(30))
      this.w_CONC05 = NVL(_Link_.PCCONC05,space(30))
      this.w_CONC06 = NVL(_Link_.PCCONC06,space(30))
      this.w_CONC07 = NVL(_Link_.PCCONC07,space(30))
      this.w_CONC08 = NVL(_Link_.PCCONC08,space(30))
      this.w_CONC09 = NVL(_Link_.PCCONC09,space(30))
      this.w_CONC10 = NVL(_Link_.PCCONC10,space(30))
      this.w_CONC11 = NVL(_Link_.PCCONC11,space(30))
      this.w_CONC12 = NVL(_Link_.PCCONC12,space(30))
      this.w_CONC13 = NVL(_Link_.PCCONC13,space(30))
      this.w_CONC14 = NVL(_Link_.PCCONC14,space(30))
      this.w_CONC15 = NVL(_Link_.PCCONC15,space(30))
      this.w_CONC16 = NVL(_Link_.PCCONC16,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_CONC01 = space(30)
      this.w_CONC02 = space(30)
      this.w_CONC03 = space(30)
      this.w_CONC04 = space(30)
      this.w_CONC05 = space(30)
      this.w_CONC06 = space(30)
      this.w_CONC07 = space(30)
      this.w_CONC08 = space(30)
      this.w_CONC09 = space(30)
      this.w_CONC10 = space(30)
      this.w_CONC11 = space(30)
      this.w_CONC12 = space(30)
      this.w_CONC13 = space(30)
      this.w_CONC14 = space(30)
      this.w_CONC15 = space(30)
      this.w_CONC16 = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_CESP_IDX,2])+'\'+cp_ToStr(_Link_.PCCODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_CESP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMCCODICE_2_2.value==this.w_MCCODICE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMCCODICE_2_2.value=this.w_MCCODICE
      replace t_MCCODICE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMCCODICE_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMCCONDAR_2_3.value==this.w_MCCONDAR)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMCCONDAR_2_3.value=this.w_MCCONDAR
      replace t_MCCONDAR with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMCCONDAR_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMCCONAVE_2_4.value==this.w_MCCONAVE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMCCONAVE_2_4.value=this.w_MCCONAVE
      replace t_MCCONAVE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMCCONAVE_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESA_2_5.value==this.w_DESA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESA_2_5.value=this.w_DESA
      replace t_DESA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESA_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESCD_2_6.value==this.w_DESCD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESCD_2_6.value=this.w_DESCD
      replace t_DESCD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESCD_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESCA_2_7.value==this.w_DESCA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESCA_2_7.value=this.w_DESCA
      replace t_DESCA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESCA_2_7.value
    endif
    cp_SetControlsValueExtFlds(this,'MCO_CESP')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(CHKEXMO(.w_MCCODICE, 0)) and (.w_CPROWORD<>0 AND NOT EMPTY(.w_MCCODICE))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMCCODICE_2_2
          i_bRes = .f.
          i_bnoChk = .f.
        case   not(CHKEXMO(.w_MCCONDAR, 1)) and (.w_CPROWORD<>0 AND NOT EMPTY(.w_MCCODICE))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMCCONDAR_2_3
          i_bRes = .f.
          i_bnoChk = .f.
        case   not(CHKEXMO(.w_MCCONAVE, 1)) and (.w_CPROWORD<>0 AND NOT EMPTY(.w_MCCODICE))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMCCONAVE_2_4
          i_bRes = .f.
          i_bnoChk = .f.
      endcase
      if .w_CPROWORD<>0 AND NOT EMPTY(.w_MCCODICE)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(t_CPROWORD<>0 AND NOT EMPTY(t_MCCODICE))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(999,cp_maxroword()+10)
      .w_MCCODICE=space(3)
      .w_MCCONDAR=space(3)
      .w_MCCONAVE=space(3)
      .w_DESA=space(25)
      .w_DESCD=space(25)
      .w_DESCA=space(25)
      .DoRTCalc(1,31,.f.)
        .w_DESA = CHKEXMO(.w_MCCODICE, 2)
        .w_DESCD = CHKEXMO(LEFT(ALLTRIM(.w_MCCONDAR)+SPACE(3),3)+.w_ALLCOX, 3)
        .w_DESCA = CHKEXMO(LEFT(ALLTRIM(.w_MCCONAVE)+SPACE(3),3)+.w_ALLCOX, 3)
    endwith
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_MCCODICE = t_MCCODICE
    this.w_MCCONDAR = t_MCCONDAR
    this.w_MCCONAVE = t_MCCONAVE
    this.w_DESA = t_DESA
    this.w_DESCD = t_DESCD
    this.w_DESCA = t_DESCA
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_MCCODICE with this.w_MCCODICE
    replace t_MCCONDAR with this.w_MCCONDAR
    replace t_MCCONAVE with this.w_MCCONAVE
    replace t_DESA with this.w_DESA
    replace t_DESCD with this.w_DESCD
    replace t_DESCA with this.w_DESCA
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsce_mocPag1 as StdContainer
  Width  = 686
  height = 243
  stdWidth  = 686
  stdheight = 243
  resizeXpos=209
  resizeYpos=143
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=3, top=24, width=679,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=7,Field1="CPROWORD",Label1="Seq.",Field2="MCCODICE",Label2="",Field3="DESA",Label3="Valore",Field4="MCCONDAR",Label4="",Field5="DESCD",Label5="Conto dare",Field6="MCCONAVE",Label6="",Field7="DESCA",Label7="Conto avere",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 151839866

  add object oStr_1_3 as StdString with uid="NZAXNIBVJS",Visible=.t., Left=6, Top=5,;
    Alignment=0, Width=126, Height=15,;
    Caption="Modelli contabili"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-7,top=43,;
    width=675+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-6,top=44,width=674+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsce_mocBodyRow as CPBodyRowCnt
  Width=665
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="UTQQKCGYJE",rtseq=28,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    ToolTipText = "Sequenza di contabilizzazione",;
    HelpContextID = 184168298,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=32, Left=-2, Top=0, cSayPict=["999"], cGetPict=["999"]

  add object oMCCODICE_2_2 as StdTrsField with uid="QGYLDNWCNN",rtseq=29,rtrep=.t.,;
    cFormVar="w_MCCODICE",value=space(3),;
    ToolTipText = "Codice del campo associato al movimento contabile",;
    HelpContextID = 63517451,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=37, Left=35, Top=0, cSayPict=['!99'], cGetPict=['!99'], InputMask=replicate('X',3)

  func oMCCODICE_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (CHKEXMO(.w_MCCODICE, 0))
    endwith
    return bRes
  endfunc

  add object oMCCONDAR_2_3 as StdTrsField with uid="VWEMHYADEJ",rtseq=30,rtrep=.t.,;
    cFormVar="w_MCCONDAR",value=space(3),;
    ToolTipText = "Codice della contropartita di contabilizzazione",;
    HelpContextID = 9882856,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=37, Left=245, Top=0, cSayPict=['!99'], cGetPict=['!99'], InputMask=replicate('X',3)

  func oMCCONDAR_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (CHKEXMO(.w_MCCONDAR, 1))
    endwith
    return bRes
  endfunc

  add object oMCCONAVE_2_4 as StdTrsField with uid="FWUHTLKLBP",rtseq=31,rtrep=.t.,;
    cFormVar="w_MCCONAVE",value=space(3),;
    ToolTipText = "Codice della contropartita di contabilizzazione",;
    HelpContextID = 208220939,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=37, Left=455, Top=0, cSayPict=['!99'], cGetPict=['!99'], InputMask=replicate('X',3)

  func oMCCONAVE_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (CHKEXMO(.w_MCCONAVE, 1))
    endwith
    return bRes
  endfunc

  add object oDESA_2_5 as StdTrsField with uid="HKWGQUEYBV",rtseq=32,rtrep=.t.,;
    cFormVar="w_DESA",value=space(25),enabled=.f.,;
    HelpContextID = 159632330,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=163, Left=77, Top=0, InputMask=replicate('X',25)

  add object oDESCD_2_6 as StdTrsField with uid="YXFAKKMGQJ",rtseq=33,rtrep=.t.,;
    cFormVar="w_DESCD",value=space(25),enabled=.f.,;
    HelpContextID = 88198090,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=163, Left=287, Top=0, InputMask=replicate('X',25)

  add object oDESCA_2_7 as StdTrsField with uid="YSSATMKEPM",rtseq=34,rtrep=.t.,;
    cFormVar="w_DESCA",value=space(25),enabled=.f.,;
    HelpContextID = 91343818,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=163, Left=497, Top=0, InputMask=replicate('X',25)
  add object oLast as LastKeyMover
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsce_moc','MCO_CESP','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".MCCODCAU=MCO_CESP.MCCODCAU";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
