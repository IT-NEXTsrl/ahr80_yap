* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_bm1                                                        *
*              Aggiornamento importo                                           *
*                                                                              *
*      Author: ZUCCHETTI TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_8]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-06-20                                                      *
* Last revis.: 2000-06-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsce_bm1",oParentObject)
return(i_retval)

define class tgsce_bm1 as StdBatch
  * --- Local variables
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch utilizzato per poter calcolare il valore corretto dell'importo
    * --- del valore movimentato da inserire nella movimentazione dei COMPONENTI.
    * --- Controllo se il movimento � nella stessa valuta specificata nei COMPONENTI.
    if this.oParentObject.w_VALUTA<>this.oParentObject.w_VALMOV
      this.oParentObject.w_VALIMPTOT = cp_ROUND(VALCAM(NVL(this.oParentObject.w_MCIMPTOT,0), this.oParentObject.w_VALMOV, this.oParentObject.W_VALUTA, this.oParentObject.w_DATREG, 0), this.oParentObject.W_DECTOT)
    else
      this.oParentObject.w_VALIMPTOT = this.oParentObject.w_MCIMPTOT
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
