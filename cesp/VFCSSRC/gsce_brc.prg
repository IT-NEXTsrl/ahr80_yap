* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_brc                                                        *
*              Stampa registro cespiti                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_332]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-04-06                                                      *
* Last revis.: 2012-01-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsce_brc",oParentObject)
return(i_retval)

define class tgsce_brc as StdBatch
  * --- Local variables
  w_COMPET = space(4)
  w_DATFIN = ctod("  /  /  ")
  w_MESS = space(255)
  w_CECODICE = space(15)
  w_VALCALBE = 0
  w_VALAMMO = 0
  w_CODCESP = space(20)
  w_DECTOT = 0
  w_DATAREG = ctod("  /  /  ")
  w_NOIMPAMM = 0
  w_CESPITE = space(20)
  w_VALAMMES = 0
  w_VALUTA = space(3)
  w_MCDATREG = ctod("  /  /  ")
  w_FILTCATE = space(1)
  w_SIMVAL = space(5)
  w_INTLIG = space(1)
  w_PREFIS = space(20)
  w_VALAMMESC = 0
  w_OK = .f.
  w_LCODATT = space(5)
  w_CONFER = space(1)
  * --- WorkFile variables
  PAR_CESP_idx=0
  VALUTE_idx=0
  AZIENDA_idx=0
  TMP_LIB_CESP_idx=0
  ATT_ALTE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcola i saldi iniziali ed i movimenti del registro cespiti (da GSCE_SRC)
    * --- Evidenzio anche ammortamento non deducibile su Registro Cespiti
    * --- Leggo i decimali da stampare sul report
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VADECTOT,VASIMVAL"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(this.oParentObject.w_VALNAZ);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VADECTOT,VASIMVAL;
        from (i_cTable) where;
            VACODVAL = this.oParentObject.w_VALNAZ;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DECTOT = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
      this.w_SIMVAL = NVL(cp_ToDate(_read_.VASIMVAL),cp_NullValue(_read_.VASIMVAL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Contiene il valore del campo PAR_CESP.PCREGCES
    * --- Read from PAR_CESP
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_CESP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_CESP_idx,2],.t.,this.PAR_CESP_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PCREGCES,PCSTAINT,PCPREFIS"+;
        " from "+i_cTable+" PAR_CESP where ";
            +"PCCODAZI = "+cp_ToStrODBC(i_codazi);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PCREGCES,PCSTAINT,PCPREFIS;
        from (i_cTable) where;
            PCCODAZI = i_codazi;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_FILTCATE = NVL(cp_ToDate(_read_.PCREGCES),cp_NullValue(_read_.PCREGCES))
      this.w_INTLIG = NVL(cp_ToDate(_read_.PCSTAINT),cp_NullValue(_read_.PCSTAINT))
      this.w_PREFIS = NVL(cp_ToDate(_read_.PCPREFIS),cp_NullValue(_read_.PCPREFIS))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_FILTCATE = nvl(this.w_FILTCATE,"N")
    this.w_COMPET = this.oParentObject.w_CODESE
    this.w_DATFIN = this.oParentObject.w_FINESE
    * --- Controllo se simulata o definitiva se ho dei mov. provvisori nell'intervallo
    *     Nel caso di ristampa non controllo ovviamente se nell'intervallo ho dei provvisori
    if this.oParentObject.w_TIPSTAM<>"R"
      this.w_OK = .F.
      * --- Select from GSCE3BKRC
      do vq_exec with 'GSCE3BKRC',this,'_Curs_GSCE3BKRC','',.f.,.t.
      if used('_Curs_GSCE3BKRC')
        select _Curs_GSCE3BKRC
        locate for 1=1
        do while not(eof())
        this.w_OK = Nvl( _Curs_GSCE3BKRC.CONTA , 0 ) <>0
          select _Curs_GSCE3BKRC
          continue
        enddo
        use
      endif
      if this.w_OK
        this.w_MESS = "Esistono per il periodo selezionato delle registrazioni provvisorie che verranno ignorate%0Proseguire con l'elaborazione?"
        if NOT ah_YesNo(this.w_MESS)
          i_retcode = 'stop'
          return
        endif
      endif
      * --- Se stampa simulata o definitiva l'esercizio deve superare quello di stampa definitiva
      if this.oParentObject.w_INIESE<=this.oParentObject.w_ULTDAT
        this.w_MESS = "L'esercizio di stampa deve essere maggiore di quello di stampa definitiva"
        ah_ErrorMsg(this.w_MESS)
        i_retcode = 'stop'
        return
      endif
    else
      * --- Se ristampa l'esercizio NON deve superare quello di stampa definitiva
      if this.oParentObject.w_INIESE>this.oParentObject.w_ULTDAT OR EMPTY(this.oParentObject.w_ULTDAT)
        this.w_MESS = "L'esercizio di ristampa supera quello di stampa definitiva"
        ah_ErrorMsg(this.w_MESS)
        i_retcode = 'stop'
        return
      endif
    endif
    * --- Recupero i saldi esercizi precedenti....
    *     ===================================
    * --- Controllo se ho settato il check 'Cespiti non movimentati nella stampa Registro Cespiti'
    *     Saldi Cespiti - Group by: categoria,valuta; Filter: competenza (<), azienda(=)
    * --- Se il tipo ammortamento � civile w_TIPAMM='C'
    *     ATTENZIONE: la query restituisce una struttura con alias identici a quelli per tipo fiscale
    *     ma i campi estratti riguardano l'ammortamento civile
    * --- Nella query si Sub Filter GSCE5KRC viene utilizzata la variabile w_INFAGG (Informazioni Aggiuntive)
    *     per decidere se i filtri debbano escludere i cespiti che non hanno movimenti fiscali o meno
    *     Con check Attivo vengono inclusi anche i valori Civilistici e quindi la query considera tutti i cespiti che hanno
    *     almeno un movimento confermato.
    *     Con check Disattivo vengono invece presi solo i cespiti che hanno almeno un movimento fiscale confermato
    if this.w_FILTCATE="S"
      * --- Carico nella tabella temporanea l'elenco dei cespiti che devo considerare.
      *     Se w_FILTCATE='S' stampo tutti i Cespiti anche quelli non movimentati 
      *     altrimenti solo quelli con almeno un movimento confermato nell'esercizio.
      *     ATTENZIONE: I saldi importati da ad hoc Windows vanno cmq considerati,
      *     quindi includo anche cespiti mai movimentati sul database che hanno un saldo.
      * --- Create temporary table TMP_LIB_CESP
      i_nIdx=cp_AddTableDef('TMP_LIB_CESP') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('GSCET1BRC',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.TMP_LIB_CESP_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      vq_exec("..\CESP\EXE\QUERY\GSCE4KRC.VQR", this, "mycrs_salcat")
      * --- Drop temporary table TMP_LIB_CESP
      i_nIdx=cp_GetTableDefIdx('TMP_LIB_CESP')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('TMP_LIB_CESP')
      endif
    else
      vq_exec("..\CESP\EXE\QUERY\GSCE11KRC.VQR", this, "mycrs_salcat")
    endif
    * --- Recupero i movimenti nell'esercizio di competenza
    *     ==========================================
    *     Controllo se ho settato il check 'Cespiti non movimentati nella stampa Registro Cespiti'
    *     Entrambe le query utilizzano w_TIPAMM (civile/fiscale) per capire quali
    *     importi prelevare
    *     
    *     Movimenti Cespiti - Filter:competenza (=)
    if this.w_FILTCATE="S"
      vq_exec("..\CESP\EXE\QUERY\GSCE1KRC.VQR", this, "mycrs_movces")
    else
      vq_exec("..\CESP\EXE\QUERY\GSCE9KRC.VQR", this, "mycrs_movces")
    endif
    * --- Controllo se esistono saldi di esercizi precedenti.
    if RECCOUNT("MYCRS_SALCAT")>0
      * --- Nel caso siano presenti saldi di esercizi precedenti calcolo il Valore da ammortizzare
      *     dei periodi precedenti. La query in base a w_TIPAMM determina gli importi
      *     civili o fiscali
      *     
      *     Se valuta di conto Eur recupero i saldi in lire "compressi"
      vq_exec("..\CESP\EXE\QUERY\GSCE13KRC.VQR", this, "saldiprec")
      * --- Ragruppo per codice cespite.Il motivo � perch� potrei avere pi� righe del medesimo cespite.
       
 Select Cecodice as Cecodice, Sum(Scincval+Scimpons+Scimpriv-Scdecval-Scimpsva-Impnoamm) as Valoammo,; 
 Sum(Impnoamm) as Impnoamm,Sum(Scincvac+Scimponc+Scimpric-Scdecvac-Scimpsvc) as Valoammoc; 
 from saldiprec into cursor saldiprec nofilter group by cecodice
      * --- Verifico se l'utente ha selezionato nella maschera di stampa il flag informazioni
      *     aggiuntive.
      if this.oParentObject.w_INFAGG="S"
        * --- Metto in Join il cursore 'Mycrs_Movces' con 'Saldiprec'
        Select Mycrs_Movces.*,Nvl(Saldiprec.Valoammo,0) as Valoammo,Nvl(Saldiprec.Impnoamm,0) as Impnoamm,; 
 Nvl(Saldiprec.Valoammoc,0) as Valoammoc from Mycrs_Movces; 
 left outer join Saldiprec on Saldiprec.cecodice=Mycrs_Movces.Cecodice and; 
 (Mycrs_Movces.mcaccant<>0 or Mycrs_Movces.Scaccciv<>0) ; 
 order by 15,2,32,36 into cursor Movime nofilter
      else
        * --- Metto in Join il cursore 'Mycrs_Movces' con 'Saldiprec'
        Select Mycrs_Movces.*,Nvl(Saldiprec.Valoammo,0) as Valoammo,Nvl(Saldiprec.Impnoamm,0) as Impnoamm,; 
 Nvl(Saldiprec.Valoammoc,0) as Valoammoc from Mycrs_Movces; 
 left outer join Saldiprec on Saldiprec.cecodice=Mycrs_Movces.Cecodice and Mycrs_Movces.mcaccant<>0 ; 
 order by 15,2,32,36 into cursor Movime nofilter
      endif
    else
      * --- Non esistono saldi di esercizi precedenti.Creo il cursore Movime basandomi solo sul cursore Mycrs_Movces.
      Select Mycrs_movces.*,0 as valoammo,0 as Impnoamm,0 as valoammoc from Mycrs_movces; 
 order by 15,2,32,36 into cursor Movime nofilter
    endif
    * --- Creo un cursore dove inserir� il valore ammortizzabile del periodo per ogni singolo cespite.
    *     Questa operazione � necessaria nel caso in cui il cespite venga ceduto nel periodo che stampo.
    * --- Questa operazione � necessaria nel caso in cui il cespite venga ceduto nel periodo che stampo.
     
 Create cursor VALAMM (CECODICE C(20), VALAMMES N(18,4),NOIMPAMM N(18,4), ; 
 MCVALNAZ C(3), MCDATREG D(8),VALAMMESC N(18,4))
    * --- Seleziono il cursore contenente i movimenti.
    *     Per ogni movimento determino il totale incremento valore per i movimenti
    *     precedenti ed l'importo non ammortizzabile (se fiscale)
    Select Movime
    Go Top
    Scan for Mcaccant<>0 or (Scaccciv<>0 and this.oParentObject.w_Infagg="S")
    this.w_DATAREG = CP_TODATE(MCDATREG)
    this.w_CODCESP = NVL(cecodice,"")
    * --- A seconda di w_TIPAMM (F/C) recupera i dati fiscali o civili...
    vq_exec("..\CESP\EXE\QUERY\GSCE14KRC.VQR", this, "saldiese")
    Select Saldiese
    Go Top
    this.w_CESPITE = NVL(CECODICE,"")
    this.w_VALAMMES = NVL(VALAMMES,0)
    this.w_VALAMMESC = NVL(VALAMMESC,0)
    this.w_NOIMPAMM = NVL(NOIMPAMM,0)
    this.w_VALUTA = NVL(MCVALNAZ,"")
    this.w_MCDATREG = CP_TODATE(MCDATREG)
     
 INSERT INTO VALAMM (CECODICE, VALAMMES,NOIMPAMM,MCVALNAZ,MCDATREG,VALAMMESC) ; 
 VALUES (this.w_CESPITE, this.w_VALAMMES,this.w_NOIMPAMM, this.w_VALUTA,this.w_MCDATREG,this.w_VALAMMESC)
    Select Movime
    Endscan
    * --- Metto in Join il cursore 'MOVIMENTI' con 'SALDIESE'
    if this.oParentObject.w_INFAGG="S"
      Select distinct Movime.*,Valamm.Valammes,Valamm.Noimpamm,Valamm.Valammesc from Movime; 
 left outer join Valamm on Valamm.cecodice=Movime.Cecodice and (Movime.mcaccant<>0 or Movime.Scaccciv<>0) and; 
 Movime.mcdatreg=Valamm.mcdatreg order by 15,2,32,36 into cursor Stmovim nofilter
    else
      Select distinct Movime.*,Valamm.Valammes,Valamm.Noimpamm,Valamm.Valammesc from Movime; 
 left outer join Valamm on Valamm.cecodice=Movime.Cecodice and Movime.mcaccant<>0 and; 
 Movime.mcdatreg=Valamm.mcdatreg order by 15,2,32,36 into cursor Stmovim nofilter
    endif
    * --- Ciclo il cursore per eliminare tutti i campi nulli (Prodotti dalla Lef Outer Join sopra)
    Wrcursor("Stmovim")
     
 Select Stmovim 
 Go Top
    Scan
     
 Replace Valammes with Nvl(Valammes,0) 
 Replace Valammesc with Nvl(Valammesc,0) 
 Replace Noimpamm with Nvl(Noimpamm,0)
    Endscan
    * --- Devo verificare i movimenti se hanno il campo Raggsing='S' oppure no.
    *     Se Raggsing='S' non devo effettuare nessun raggruppamento.
     
 Select * from Stmovim where Stmovim.raggsing="S"; 
 order by 15,2,32,36 into cursor Movime1 nofilter
    * --- Movimenti raggruppati.Raggsing='R'
    *     Raggruppo i cespiti in base alla data di registrazione,causale,codice cespite,esercizio di utilizzo.
    if this.oParentObject.w_INFAGG="S"
       
 SELECT mccompet,mcdatreg, mcnumdoc,mcalfdoc, mcdatdoc,mcvalnaz,; 
 scincval, iif(mcaccant<>0,mccoefis,mccoefis-mccoefis) as mccoefis, mcaccant,scuticiv,ceperdef,scimpnoa,; 
 Cecodice, Cedescri, cccodice,cccodice1,ccdescri1, mcdesmov, mcdesmo2,; 
 antipcon,andescri,anindiri,an___cap,anlocali, anprovin,ceespriu,mcdtpriu, mcflpriu,; 
 "R" as raggsing,mcimpa13, mcimpa01,mcnumreg,ceimpmax,mcimpa10,mcserial,; 
 Mcimpa11,Mcaccan1,iif(Mcaccan1<>0,mccoefi1,mccoefi1-mccoefi1) as Mccoefi1,; 
 Scimpons,Scimpriv,Scdecval,Scimpsva,Scutifis,Scutiant,Scquoper,Mcimpa14,; 
 Scincvac,Scaccciv,Ceespric,Mcdtpric,Mcflpric,Mcimpa20,iif(Scaccciv<>0,Mccoeciv,mccoeciv-mccoeciv) as Mccoeciv,; 
 Scimponc,Scimpric,Scdecvac,Scimpsvc,Mcimpa21,Valoammo,Impnoamm,; 
 Valoammoc,Valammes,Noimpamm,Valammesc; 
 FROM Stmovim into cursor Appoggio where Raggsing="R"
       
 SELECT Max(mccompet) as mccompet,mcdatreg, Max(mcnumdoc) as mcnumdoc,; 
 Max(mcalfdoc) as mcalfdoc, Max(mcdatdoc) as mcdatdoc,Max(mcvalnaz) as mcvalnaz,; 
 SUM(scincval) as scincval, mccoefis, SUM(mcaccant) as mcaccant, ; 
 SUM(scuticiv) as scuticiv, Max(ceperdef) as ceperdef, SUM(scimpnoa) as scimpnoa,; 
 Max(cecodice) as cecodice, Max(cedescri) as cedescri, cccodice,; 
 cccodice1, Max(ccdescri1) as ccdescri1, Max(mcdesmov) as mcdesmov, Max(mcdesmo2) as mcdesmo2, ; 
 Max(antipcon) as antipcon, Max(andescri) as andescri, Max(anindiri) as anindiri, Max(an___cap) as an___cap, ; 
 Max(anlocali) as anlocali, Max(anprovin) as anprovin, ceespriu,Max(mcdtpriu) as mcdtpriu, Max(mcflpriu) as mcflpriu, ; 
 "R" as raggsing,SUM(mcimpa13) as mcimpa13, Sum(Mcimpa01) as mcimpa01,Max(mcnumreg) as mcnumreg,; 
 Max(ceimpmax) as ceimpmax,Sum(mcimpa10) as mcimpa10,Max(mcserial) as mcserial,; 
 Min(Mcimpa11) as Mcimpa11, Sum(Mcaccan1) as Mcaccan1,mccoefi1,Sum(scimpons) as Scimpons,; 
 Sum(Scimpriv) as Scimpriv,Sum(Scdecval) as Scdecval,Sum(Scimpsva) as Scimpsva,Sum(Scutifis) as Scutifis,; 
 Sum(Scutiant) as Scutiant,Sum(Scquoper) as Scquoper,Sum(Mcimpa14) as Mcimpa14, ; 
 Sum(Scincvac) as Scincvac,Sum(Scaccciv) as Scaccciv,Ceespric,Max(Mcdtpric) as Mcdtpric,; 
 Max(Mcflpric) as Mcflpric,Sum(Mcimpa20) as Mcimpa20,Mccoeciv,Sum(Scimponc) as Scimponc, Sum(Scimpric) as Scimpric,; 
 Sum(Scdecvac) as Scdecvac,Sum(Scimpsvc) as Scimpsvc,Sum(Mcimpa21) as Mcimpa21,; 
 Sum(Valoammo) as Valoammo,Sum(Impnoamm) as Impnoamm,Sum(Valoammoc) as Valoammoc,; 
 Sum(Valammes) as Valammes,Sum(Noimpamm) as Noimpamm,Sum(Valammesc) as Valammesc; 
 FROM Appoggio GROUP BY mcdatreg,cccodice1,cccodice,ceespriu,ceespric,mccoefis,mccoefi1,mccoeciv; 
 into cursor Movime2 where Raggsing="R"
    else
       
 SELECT Max(mccompet) as mccompet,mcdatreg, Max(mcnumdoc) as mcnumdoc,; 
 Max(mcalfdoc) as mcalfdoc, Max(mcdatdoc) as mcdatdoc,Max(mcvalnaz) as mcvalnaz,; 
 SUM(scincval) as scincval, mccoefis, SUM(mcaccant) as mcaccant, ; 
 SUM(scuticiv) as scuticiv, Max(ceperdef) as ceperdef, SUM(scimpnoa) as scimpnoa,; 
 Max(cecodice) as cecodice, Max(cedescri) as cedescri, cccodice,; 
 cccodice1, Max(ccdescri1) as ccdescri1, Max(mcdesmov) as mcdesmov, Max(mcdesmo2) as mcdesmo2, ; 
 Max(antipcon) as antipcon, Max(andescri) as andescri, Max(anindiri) as anindiri, Max(an___cap) as an___cap, ; 
 Max(anlocali) as anlocali, Max(anprovin) as anprovin, ceespriu,Max(mcdtpriu) as mcdtpriu, Max(mcflpriu) as mcflpriu, ; 
 "R" as raggsing,SUM(mcimpa13) as mcimpa13, Sum(Mcimpa01) as mcimpa01,Max(mcnumreg) as mcnumreg,; 
 Max(ceimpmax) as ceimpmax,Sum(mcimpa10) as mcimpa10,Max(mcserial) as mcserial,; 
 Min(Mcimpa11) as Mcimpa11, Sum(Mcaccan1) as Mcaccan1,mccoefi1,Sum(scimpons) as Scimpons,; 
 Sum(Scimpriv) as Scimpriv,Sum(Scdecval) as Scdecval,Sum(Scimpsva) as Scimpsva,Sum(Scutifis) as Scutifis,; 
 Sum(Scutiant) as Scutiant,Sum(Scquoper) as Scquoper,Sum(Mcimpa14) as Mcimpa14,; 
 Sum(Scincvac) as Scincvac,Sum(Scaccciv) as Scaccciv,Max(Ceespric) as Ceespric,Max(Mcdtpric) as Mcdtpric,; 
 Max(Mcflpric) as Mcflpric,Sum(Mcimpa20) as Mcimpa20,Max(Mccoeciv) as Mccoeciv,Sum(Scimponc) as Scimponc, Sum(Scimpric) as Scimpric,; 
 Sum(Scdecvac) as Scdecvac,Sum(Scimpsvc) as Scimpsvc,Sum(Mcimpa21) as Mcimpa21,; 
 Sum(Valoammo) as Valoammo,Sum(Impnoamm) as Impnoamm,Sum(Valoammoc) as Valoammoc,; 
 Sum(Valammes) as Valammes,Sum(Noimpamm) as Noimpamm,Sum(Valammesc) as Valammesc; 
 FROM Stmovim GROUP BY mcdatreg,cccodice1,cccodice,ceespriu,mccoefis,mccoefi1 into cursor Movime2 where Raggsing="R"
    endif
    * --- Creo un unico cursore dei movimenti.
     
 Select * from Movime1 ; 
 Union All ; 
 Select * from Movime2 ; 
 order by 15,2,32,36 into cursor Movimenti nofilter
    if Reccount("mycrs_salcat")>0
      SELECT space(4) as mccompet, mcdatreg, -99999999999999 as mcnumdoc, Space(10) as mcalfdoc, mcdatdoc, ; 
 mcvalnaz, SUM(scincval) as scincval, mccoefis, SUM(mcaccant) as mcaccant, ; 
 SUM(scuticiv) as scuticiv, ceperdef, SUM(scimpnoa) as scimpnoa, cecodice, cedescri, cccodice, ; 
 space(5) as cccodice1, space(40) as ccdescri1, space(40) as mcdesmov, space(40) as mcdesmo2, ; 
 " " as antipcon, space(40) as andescri, space(35) as anindiri, space(9) as an___cap, ; 
 space(30) as anlocali, "  " as anprovin, space(4) as ceespriu, mcdatreg as mcdtpriu, " " as mcflpriu, "R" as raggsing, ; 
 0 as mcimpa13, 0 as mcimpa01, 999999 as mcnumreg, ceimpmax,; 
 0 as mcimpa10,space(10) as mcserial,0 as Mcimpa11,Sum(Mcaccan1) as Mcaccan1,mccoefi1,; 
 Sum(Scimpons) as Scimpons,Sum(Scimpriv) as Scimpriv,Sum(Scdecval) as Scdecval,Sum(Scimpsva) as Scimpsva,; 
 Sum(Scutifis) as Scutifis,Sum(Scutiant) as Scutiant,Sum(Scquoper) as Scquoper,0 as Mcimpa14, ; 
 Sum(Scincvac) as Scincvac,Sum(Scaccciv) as Scaccciv,space(4) as ceespric,mcdatreg as mcdtpric,; 
 " " as mcflpric,0 as Mcimpa20,mccoeciv,Sum(Scimponc) as Scimponc, Sum(Scimpric) as Scimpric, ; 
 Sum(Scdecvac) as Scdecvac,Sum(Scimpsvc) as Scimpsvc,0 as Mcimpa21, ; 
 0 as valoammo,0 as Impnoamm,0 as valoammoc,0 as valammes, 0 as Noimpamm, 0 as valammesc, ; 
 Sum(ScArrAmm) As ScArrAmm, Sum(ScArrFon) As ScArrFon ; 
 FROM mycrs_Salcat GROUP BY cccodice into cursor mycrs_salctg
      * --- Unisce i dati (saldi iniziali + movimenti)
       
 Select movimenti.*,scincval*0 As ScArrAmm, scincval*0 As ScArrFon,1 as ordmov, scincval*0 as valcalbe from movimenti ; 
 UNION select mycrs_salctg.*,0 as ordmov, scincval*0 as valcalbe; 
 from mycrs_salctg ORDER BY 15, ordmov, 2, 32,36 into cursor __TMP__
    else
       
 select movimenti.*,scincval*0 As ScArrAmm, scincval*0 As ScArrFon,1 as ordmov, scincval*0 as valcalbe from movimenti ; 
 order by 15,2,32,36 into cursor __tmp__
    endif
    * --- Parametri della query passati al report
    l_compet = this.w_COMPET
    l_iniese = this.oParentObject.w_INIESE
    l_finese = this.oParentObject.w_FINESE
    l_valnaz = this.oParentObject.w_VALNAZ
    l_dectot = this.w_DECTOT
    l_simval=this.w_Simval
    * --- Applico Codice Attivit� alternativo
    *     Esiste procedura di conversione che crea record di default cod codice attivit�
    *     pari al codice attivit� padre
    if Not Empty(this.oParentObject.w_CODATT)
      this.w_LCODATT = this.oParentObject.w_CODATT
      * --- Select from ATT_ALTE
      i_nConn=i_TableProp[this.ATT_ALTE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ATT_ALTE_idx,2],.t.,this.ATT_ALTE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select CAATTIVI,CADESCRI  from "+i_cTable+" ATT_ALTE ";
            +" where CADATATT<="+cp_ToStrODBC(this.oParentObject.w_INIESE)+" AND CACODATT="+cp_ToStrODBC(this.oParentObject.w_CODATT)+"";
            +" order by CADATATT Desc";
             ,"_Curs_ATT_ALTE")
      else
        select CAATTIVI,CADESCRI from (i_cTable);
         where CADATATT<=this.oParentObject.w_INIESE AND CACODATT=this.oParentObject.w_CODATT;
         order by CADATATT Desc;
          into cursor _Curs_ATT_ALTE
      endif
      if used('_Curs_ATT_ALTE')
        select _Curs_ATT_ALTE
        locate for 1=1
        do while not(eof())
        this.w_LCODATT = Nvl(_Curs_ATT_ALTE.CAATTIVI,Space(5))
        Exit
          select _Curs_ATT_ALTE
          continue
        enddo
        use
      endif
    endif
    l_codatt= this.w_LCODATT
    l_catini= this.oParentObject.w_CATINI
    l_catfin= this.oParentObject.w_CATFIN
    CEFLSTDT = this.oParentObject.w_PCFLSTIN
    * --- PER NUMERAZIONE PAGINE IN TESTATA
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZINDAZI,AZLOCAZI,AZCAPAZI,AZPROAZI,AZPIVAZI,AZCOFAZI"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZINDAZI,AZLOCAZI,AZCAPAZI,AZPROAZI,AZPIVAZI,AZCOFAZI;
        from (i_cTable) where;
            AZCODAZI = this.oParentObject.w_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      w_INDAZI = NVL(cp_ToDate(_read_.AZINDAZI),cp_NullValue(_read_.AZINDAZI))
      w_LOCAZI = NVL(cp_ToDate(_read_.AZLOCAZI),cp_NullValue(_read_.AZLOCAZI))
      w_CAPAZI = NVL(cp_ToDate(_read_.AZCAPAZI),cp_NullValue(_read_.AZCAPAZI))
      w_PROAZI = NVL(cp_ToDate(_read_.AZPROAZI),cp_NullValue(_read_.AZPROAZI))
      w_PIVAZI = NVL(cp_ToDate(_read_.AZPIVAZI),cp_NullValue(_read_.AZPIVAZI))
      w_COFAZI = NVL(cp_ToDate(_read_.AZCOFAZI),cp_NullValue(_read_.AZCOFAZI))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    L_INTLIG=this.w_INTLIG
    L_PREFIS=this.w_PREFIS
    L_INDAZI=w_INDAZI
    L_LOCAZI=w_LOCAZI
    L_CAPAZI=w_CAPAZI
    L_PROAZI=w_PROAZI
    L_COFAZI=w_COFAZI
    L_PIVAZI=w_PIVAZI
    * --- Lascio variabile perch� gi� impostata nella stampa
    select ("__TMP__")
    if !(this.oParentObject.w_TIPAMM="F" AND this.oParentObject.w_INFAGG="S")
      WRCURSOR("__TMP__")
      if this.oParentObject.w_TIPAMM="F"
        delete FOR EMPTY (NVL (CEESPRIU,"")) AND NOT EMPTY(NVL(MCCOMPET, SPACE(4)))
      else
        delete FOR EMPTY (NVL (CEESPRIC,"")) AND NOT EMPTY(NVL(MCCOMPET, SPACE(4)))
      endif
      Go Top
    endif
    if this.oParentObject.w_TESTO="S"
      if this.oParentObject.w_TIPAMM="F"
        if this.oParentObject.w_INFAGG="S"
          CP_CHPRN("..\CESP\EXE\QUERY\GSCE12KRC.FRX", " ", this)
        else
          CP_CHPRN("..\CESP\EXE\QUERY\GSCE2KRC.FRX", " ", this)
        endif
      else
        CP_CHPRN("..\CESP\EXE\QUERY\GSCE8KRC.FRX", " ", this)
      endif
    else
      if this.oParentObject.w_TIPAMM="F"
        if this.oParentObject.w_INFAGG="S"
          CP_CHPRN("..\CESP\EXE\QUERY\GSCE11KRC.FRX", " ", this)
        else
          CP_CHPRN("..\CESP\EXE\QUERY\GSCE1KRC.FRX", " ", this)
        endif
      else
        CP_CHPRN("..\CESP\EXE\QUERY\GSCE3KRC.FRX", " ", this)
      endif
    endif
    * --- Chiudo i cursori
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Aggiorna l'esercizio di stampa definitiva
    if this.oParentObject.w_TIPSTAM="D" AND ((this.oParentObject.w_PCTIPAMM="E" AND this.oParentObject.w_TIPAMM="F") OR (this.oParentObject.w_PCTIPAMM="C" AND this.oParentObject.w_TIPAMM="C") OR (this.oParentObject.w_PCTIPAMM="F" AND this.oParentObject.w_TIPAMM="F"))
      do GSCE_S2R with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if this.w_CONFER="S"
        * --- Write into PAR_CESP
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.PAR_CESP_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_CESP_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_CESP_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PCDATCON ="+cp_NullLink(cp_ToStrODBC(this.w_COMPET),'PAR_CESP','PCDATCON');
              +i_ccchkf ;
          +" where ";
              +"PCCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
                 )
        else
          update (i_cTable) set;
              PCDATCON = this.w_COMPET;
              &i_ccchkf. ;
           where;
              PCCODAZI = this.oParentObject.w_CODAZI;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Chiude i cursori
    if used("mycrs_movces")
      select ("mycrs_movces")
      use
    endif
    if used("mycrs_salctg")
      select ("mycrs_salctg")
      use
    endif
    if used("mycrs_salcat")
      select ("mycrs_salcat")
      use
    endif
    if used("Saldiese")
      select ("Saldiese")
      use
    endif
    if used("Saldiprec")
      select ("Saldiprec")
      use
    endif
    if used("Movime")
      select ("movime")
      use
    endif
    if used("Valamm")
      select ("Valamm")
      use
    endif
    if used("Stmovim")
      select ("Stmovim")
      use
    endif
    if used("Movime1")
      select ("Movime1")
      use
    endif
    if used("Movime2")
      select ("Movime2")
      use
    endif
    if used("Movimenti")
      select ("Movimenti")
      use
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='PAR_CESP'
    this.cWorkTables[2]='VALUTE'
    this.cWorkTables[3]='AZIENDA'
    this.cWorkTables[4]='*TMP_LIB_CESP'
    this.cWorkTables[5]='ATT_ALTE'
    return(this.OpenAllTables(5))

  proc CloseCursors()
    if used('_Curs_GSCE3BKRC')
      use in _Curs_GSCE3BKRC
    endif
    if used('_Curs_ATT_ALTE')
      use in _Curs_ATT_ALTE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
