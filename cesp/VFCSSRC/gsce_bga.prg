* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_bga                                                        *
*              Generazione accantonamenti                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_48]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-03-25                                                      *
* Last revis.: 2010-05-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsce_bga",oParentObject)
return(i_retval)

define class tgsce_bga as StdBatch
  * --- Local variables
  w_MCSERIAL = space(10)
  w_MCCOMPET = space(4)
  w_MCCODESE = space(4)
  w_MCNUMREG = 0
  w_MCCODCES = space(20)
  w_MCCOEFIS = 0
  w_TOT = 0
  w_GEN = 0
  w_MESS = space(10)
  w_INIESE = ctod("  /  /  ")
  w_FINCOM = ctod("  /  /  ")
  w_MCVALNAZ = space(3)
  w_MCCAOVAL = 0
  w_MCIMPA07 = 0
  w_MCIMPA09 = 0
  w_MCIMPA12 = 0
  w_MCCOECIV = 0
  w_MCCODCEN = space(15)
  w_MCVOCCEN = space(15)
  w_MCCODCOM = space(15)
  w_CODAZI = space(5)
  w_COMPRC = space(4)
  w_MCIMPA15 = 0
  w_MCCOEFI1 = 0
  w_MESS1 = space(100)
  w_MESS2 = space(100)
  w_oERRORLOG = .NULL.
  * --- WorkFile variables
  CES_AMMO_idx=0
  MOV_CESP_idx=0
  CES_PIAN_idx=0
  ESERCIZI_idx=0
  CES_PITI_idx=0
  SAL_CESP_idx=0
  PAR_CESP_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Generazione Movimenti di Accantonamento (da GSCE_KGA)
    this.w_CODAZI = i_CODAZI
    this.w_MCCOMPET = this.oParentObject.w_CODESE
    this.w_MCCODESE = this.oParentObject.w_CODESE
    do case
      case EMPTY(this.oParentObject.w_CODCAU)
        ah_ErrorMsg("Causale cespite non definita")
        i_retcode = 'stop'
        return
      case EMPTY(this.oParentObject.w_NUMREG) OR EMPTY(this.oParentObject.w_CODESE)
        ah_ErrorMsg("Piano di ammortamento non definito")
        i_retcode = 'stop'
        return
    endcase
    * --- Stanzia l'oggetto per la gestione delle messaggistiche di errore
    this.w_oERRORLOG=createobject("AH_ErrorLog")
    this.w_MESS1 = Ah_Msgformat("Verificare piano numero: %1 del %2", this.oParentObject.w_NUMREG, DTOC(this.oParentObject.w_DATREG))
    * --- Verifica date di Consolidamento
    this.w_INIESE = cp_CharToDate("  -  -  ")
    this.w_FINCOM = cp_CharToDate("  -  -  ")
    this.w_COMPRC = SPACE(4)
    * --- Read from ESERCIZI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ESERCIZI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2],.t.,this.ESERCIZI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ESINIESE"+;
        " from "+i_cTable+" ESERCIZI where ";
            +"ESCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
            +" and ESCODESE = "+cp_ToStrODBC(this.oParentObject.w_CODESE);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ESINIESE;
        from (i_cTable) where;
            ESCODAZI = this.w_CODAZI;
            and ESCODESE = this.oParentObject.w_CODESE;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_INIESE = NVL(cp_ToDate(_read_.ESINIESE),cp_NullValue(_read_.ESINIESE))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from PAR_CESP
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_CESP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_CESP_idx,2],.t.,this.PAR_CESP_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PCDATCON"+;
        " from "+i_cTable+" PAR_CESP where ";
            +"PCCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PCDATCON;
        from (i_cTable) where;
            PCCODAZI = this.w_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_COMPRC = NVL(cp_ToDate(_read_.PCDATCON),cp_NullValue(_read_.PCDATCON))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if NOT EMPTY(this.w_COMPRC)
      * --- Read from ESERCIZI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ESERCIZI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2],.t.,this.ESERCIZI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ESFINESE"+;
          " from "+i_cTable+" ESERCIZI where ";
              +"ESCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
              +" and ESCODESE = "+cp_ToStrODBC(this.w_COMPRC);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ESFINESE;
          from (i_cTable) where;
              ESCODAZI = this.w_CODAZI;
              and ESCODESE = this.w_COMPRC;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_FINCOM = NVL(cp_ToDate(_read_.ESFINESE),cp_NullValue(_read_.ESFINESE))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_FINCOM>=this.w_INIESE
        ah_ErrorMsg("Esercizio di competenza compreso entro la data di consolidamento")
        i_retcode = 'stop'
        return
      endif
    endif
    * --- Controlla se esiste un Movimento di Accantonamento per i cespiti del piano di ammortamaneto selezionato
    VQ_EXEC("GSCE_BGA",this,"CursAmmort")
    if RECCOUNT("CursAmmort")>0
      this.w_MESS = "Sono gi� presenti dei movimenti di accantonamento%0per i cespiti del piano di ammortamento selezionato"
      ah_ErrorMsg(this.w_MESS,"STOP","")
      if ah_YesNo("Si vuole generare la stampa di controllo dei movimenti?")
        SELECT * FROM CursAmmort INTO CURSOR __TMP__
        l_NUMREG = this.oParentObject.w_NUMREG
        CP_CHPRN("..\CESP\EXE\QUERY\GSCE_BGA.FRX", " ", this)
      endif
      if used("CursAmmort")
        SELECT CursAmmort
        USE
      endif
      i_retcode = 'stop'
      return
    endif
    if used("CursAmmort")
      SELECT CursAmmort
      USE
    endif
    * --- Valuta Nazionale
    * --- Read from ESERCIZI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ESERCIZI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2],.t.,this.ESERCIZI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ESVALNAZ"+;
        " from "+i_cTable+" ESERCIZI where ";
            +"ESCODAZI = "+cp_ToStrODBC(i_CODAZI);
            +" and ESCODESE = "+cp_ToStrODBC(this.oParentObject.w_CODESE);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ESVALNAZ;
        from (i_cTable) where;
            ESCODAZI = i_CODAZI;
            and ESCODESE = this.oParentObject.w_CODESE;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_MCVALNAZ = NVL(cp_ToDate(_read_.ESVALNAZ),cp_NullValue(_read_.ESVALNAZ))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_MCCAOVAL = GETCAM(this.w_MCVALNAZ, this.oParentObject.w_DATMOV, 0)
    this.w_TOT = 0
    this.w_GEN = 0
    * --- Select from CES_AMMO
    i_nConn=i_TableProp[this.CES_AMMO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CES_AMMO_idx,2],.t.,this.CES_AMMO_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" CES_AMMO ";
          +" where ACNUMREG="+cp_ToStrODBC(this.oParentObject.w_NUMREG)+" AND ACCODESE="+cp_ToStrODBC(this.oParentObject.w_CODESE)+"";
           ,"_Curs_CES_AMMO")
    else
      select * from (i_cTable);
       where ACNUMREG=this.oParentObject.w_NUMREG AND ACCODESE=this.oParentObject.w_CODESE;
        into cursor _Curs_CES_AMMO
    endif
    if used('_Curs_CES_AMMO')
      select _Curs_CES_AMMO
      locate for 1=1
      do while not(eof())
      this.w_MCCODCES = NVL(_Curs_CES_AMMO.ACCODCES," ")
      this.w_MCIMPA07 = NVL(_Curs_CES_AMMO.ACIMPV08, 0)
      this.w_MCIMPA09 = NVL(_Curs_CES_AMMO.ACIMPV09, 0)
      this.w_MCIMPA12 = IIF(this.oParentObject.w_FLGA12="S",NVL(_Curs_CES_AMMO.ACIMPV10, 0),0)
      this.w_MCCOECIV = NVL(_Curs_CES_AMMO.ACCOECIV, 0)
      this.w_MCCOEFIS = NVL(_Curs_CES_AMMO.ACCOEFI1, 0)
      this.w_MCCOEFI1 = NVL(_Curs_CES_AMMO.ACCOEFI2, 0)
      this.w_MCIMPA15 = NVL(_Curs_CES_AMMO.ACIMPV13, 0)
      this.w_TOT = this.w_TOT+1
      if NOT EMPTY(this.w_MCCODCES) AND (this.w_MCIMPA07<>0 OR this.w_MCIMPA09<>0 OR this.w_MCIMPA12<>0)
        * --- Legge i Dati di Analitica dal Cespite
        this.w_MCCODCEN = SPACE(15)
        this.w_MCVOCCEN = SPACE(15)
        this.w_MCCODCOM = SPACE(15)
        * --- Read from CES_PITI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CES_PITI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CES_PITI_idx,2],.t.,this.CES_PITI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CECODCOM,CEVOCCEN,CECODCEN"+;
            " from "+i_cTable+" CES_PITI where ";
                +"CECODICE = "+cp_ToStrODBC(this.w_MCCODCES);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CECODCOM,CEVOCCEN,CECODCEN;
            from (i_cTable) where;
                CECODICE = this.w_MCCODCES;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_MCCODCOM = NVL(cp_ToDate(_read_.CECODCOM),cp_NullValue(_read_.CECODCOM))
          this.w_MCVOCCEN = NVL(cp_ToDate(_read_.CEVOCCEN),cp_NullValue(_read_.CEVOCCEN))
          this.w_MCCODCEN = NVL(cp_ToDate(_read_.CECODCEN),cp_NullValue(_read_.CECODCEN))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        ah_Msg("Generazione movimento cespite: %1",.T.,.F.,.F., this.w_MCCODCES)
        * --- Try
        local bErr_03A4A160
        bErr_03A4A160=bTrsErr
        this.Try_03A4A160()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- Errore non compreso nei controlli
          this.w_oERRORLOG.AddMsgLogNoTranslate(this.w_MESS1)     
          this.w_oERRORLOG.AddMsgLogNoTranslate(SPACE(50))     
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
        endif
        bTrsErr=bTrsErr or bErr_03A4A160
        * --- End
      else
        this.w_oERRORLOG.AddMsgLog("Ammortamento non generato per il codice cespite: %1 non presente nessun importo accantonato", Alltrim(this.w_Mccodces))     
      endif
        select _Curs_CES_AMMO
        continue
      enddo
      use
    endif
    this.w_MESS = "Generazione completata%0Num.movimenti cespiti inseriti: %1%0Su un totale di: %2"
    ah_ErrorMsg(this.w_MESS,,"", ALLTRIM(STR(this.w_GEN)), ALLTRIM(STR(this.w_TOT)) )
    this.w_oERRORLOG.PrintLog(this,"Errori riscontrati")     
  endproc
  proc Try_03A4A160()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    this.w_MCSERIAL = SPACE(10)
    this.w_MCNUMREG = 0
    i_Conn=i_TableProp[this.MOV_CESP_IDX, 3]
    cp_NextTableProg(this, i_Conn, "SEMCE", "i_codazi,w_MCSERIAL")
    cp_NextTableProg(this, i_Conn, "PRMCE", "i_codazi,w_MCCODESE,w_MCNUMREG")
    * --- Insert into MOV_CESP
    i_nConn=i_TableProp[this.MOV_CESP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOV_CESP_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MOV_CESP_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"MCSERIAL"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_MCSERIAL),'MOV_CESP','MCSERIAL');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'MCSERIAL',this.w_MCSERIAL)
      insert into (i_cTable) (MCSERIAL &i_ccchkf. );
         values (;
           this.w_MCSERIAL;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Write into MOV_CESP
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.MOV_CESP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOV_CESP_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.MOV_CESP_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MCCODESE ="+cp_NullLink(cp_ToStrODBC(this.w_MCCODESE),'MOV_CESP','MCCODESE');
      +",MCCOMPET ="+cp_NullLink(cp_ToStrODBC(this.w_MCCOMPET),'MOV_CESP','MCCOMPET');
      +",MCNUMREG ="+cp_NullLink(cp_ToStrODBC(this.w_MCNUMREG),'MOV_CESP','MCNUMREG');
      +",MCDATREG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DATMOV),'MOV_CESP','MCDATREG');
      +",MCSTATUS ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_STATUS),'MOV_CESP','MCSTATUS');
      +",MCNUMDOC ="+cp_NullLink(cp_ToStrODBC(0),'MOV_CESP','MCNUMDOC');
      +",MCALFDOC ="+cp_NullLink(cp_ToStrODBC(Space(10)),'MOV_CESP','MCALFDOC');
      +",MCDATDOC ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'MOV_CESP','MCDATDOC');
      +",MCVALNAZ ="+cp_NullLink(cp_ToStrODBC(this.w_MCVALNAZ),'MOV_CESP','MCVALNAZ');
      +",MCCODVAL ="+cp_NullLink(cp_ToStrODBC(this.w_MCVALNAZ),'MOV_CESP','MCCODVAL');
      +",MCCAOVAL ="+cp_NullLink(cp_ToStrODBC(this.w_MCCAOVAL),'MOV_CESP','MCCAOVAL');
      +",MCCODCAU ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODCAU),'MOV_CESP','MCCODCAU');
      +",MCDESMOV ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DESCRI),'MOV_CESP','MCDESMOV');
      +",MCDESMO2 ="+cp_NullLink(cp_ToStrODBC(SPACE(40)),'MOV_CESP','MCDESMO2');
      +",MCTIPCON ="+cp_NullLink(cp_ToStrODBC("N"),'MOV_CESP','MCTIPCON');
      +",MCCODCON ="+cp_NullLink(cp_ToStrODBC(SPACE(15)),'MOV_CESP','MCCODCON');
      +",MCCODCES ="+cp_NullLink(cp_ToStrODBC(this.w_MCCODCES),'MOV_CESP','MCCODCES');
      +",MCIMPA07 ="+cp_NullLink(cp_ToStrODBC(this.w_MCIMPA07),'MOV_CESP','MCIMPA07');
      +",MCIMPA09 ="+cp_NullLink(cp_ToStrODBC(this.w_MCIMPA09),'MOV_CESP','MCIMPA09');
      +",MCIMPA12 ="+cp_NullLink(cp_ToStrODBC(this.w_MCIMPA12),'MOV_CESP','MCIMPA12');
      +",MCDATCON ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'MOV_CESP','MCDATCON');
      +",MCSTABEN ="+cp_NullLink(cp_ToStrODBC("U"),'MOV_CESP','MCSTABEN');
      +",MCCOECIV ="+cp_NullLink(cp_ToStrODBC(this.w_MCCOECIV),'MOV_CESP','MCCOECIV');
      +",MCCOEFIS ="+cp_NullLink(cp_ToStrODBC(this.w_MCCOEFIS),'MOV_CESP','MCCOEFIS');
      +",MCVOCCEN ="+cp_NullLink(cp_ToStrODBC(this.w_MCVOCCEN),'MOV_CESP','MCVOCCEN');
      +",MCCODCEN ="+cp_NullLink(cp_ToStrODBC(this.w_MCCODCEN),'MOV_CESP','MCCODCEN');
      +",MCCODCOM ="+cp_NullLink(cp_ToStrODBC(this.w_MCCODCOM),'MOV_CESP','MCCODCOM');
      +",MCFLPRIU ="+cp_NullLink(cp_ToStrODBC(" "),'MOV_CESP','MCFLPRIU');
      +",MCIMPA15 ="+cp_NullLink(cp_ToStrODBC(this.w_MCIMPA15),'MOV_CESP','MCIMPA15');
      +",MCCOEFI1 ="+cp_NullLink(cp_ToStrODBC(this.w_MCCOEFI1),'MOV_CESP','MCCOEFI1');
      +",MCFLPRIC ="+cp_NullLink(cp_ToStrODBC(" "),'MOV_CESP','MCFLPRIC');
          +i_ccchkf ;
      +" where ";
          +"MCSERIAL = "+cp_ToStrODBC(this.w_MCSERIAL);
             )
    else
      update (i_cTable) set;
          MCCODESE = this.w_MCCODESE;
          ,MCCOMPET = this.w_MCCOMPET;
          ,MCNUMREG = this.w_MCNUMREG;
          ,MCDATREG = this.oParentObject.w_DATMOV;
          ,MCSTATUS = this.oParentObject.w_STATUS;
          ,MCNUMDOC = 0;
          ,MCALFDOC = Space(10);
          ,MCDATDOC = cp_CharToDate("  -  -  ");
          ,MCVALNAZ = this.w_MCVALNAZ;
          ,MCCODVAL = this.w_MCVALNAZ;
          ,MCCAOVAL = this.w_MCCAOVAL;
          ,MCCODCAU = this.oParentObject.w_CODCAU;
          ,MCDESMOV = this.oParentObject.w_DESCRI;
          ,MCDESMO2 = SPACE(40);
          ,MCTIPCON = "N";
          ,MCCODCON = SPACE(15);
          ,MCCODCES = this.w_MCCODCES;
          ,MCIMPA07 = this.w_MCIMPA07;
          ,MCIMPA09 = this.w_MCIMPA09;
          ,MCIMPA12 = this.w_MCIMPA12;
          ,MCDATCON = cp_CharToDate("  -  -  ");
          ,MCSTABEN = "U";
          ,MCCOECIV = this.w_MCCOECIV;
          ,MCCOEFIS = this.w_MCCOEFIS;
          ,MCVOCCEN = this.w_MCVOCCEN;
          ,MCCODCEN = this.w_MCCODCEN;
          ,MCCODCOM = this.w_MCCODCOM;
          ,MCFLPRIU = " ";
          ,MCIMPA15 = this.w_MCIMPA15;
          ,MCCOEFI1 = this.w_MCCOEFI1;
          ,MCFLPRIC = " ";
          &i_ccchkf. ;
       where;
          MCSERIAL = this.w_MCSERIAL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Aggiorna Saldi
    * --- Try
    local bErr_038803B8
    bErr_038803B8=bTrsErr
    this.Try_038803B8()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_038803B8
    * --- End
    * --- Write into SAL_CESP
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SAL_CESP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SAL_CESP_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.SAL_CESP_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"SCACCCIV =SCACCCIV+ "+cp_ToStrODBC(this.w_MCIMPA07);
      +",SCACCFIS =SCACCFIS+ "+cp_ToStrODBC(this.w_MCIMPA09);
      +",SCQUOPER =SCQUOPER+ "+cp_ToStrODBC(this.w_MCIMPA12);
      +",SCACCANT =SCACCANT+ "+cp_ToStrODBC(this.w_MCIMPA15);
          +i_ccchkf ;
      +" where ";
          +"SCCODCES = "+cp_ToStrODBC(this.w_MCCODCES);
          +" and SCCODESE = "+cp_ToStrODBC(this.w_MCCOMPET);
             )
    else
      update (i_cTable) set;
          SCACCCIV = SCACCCIV + this.w_MCIMPA07;
          ,SCACCFIS = SCACCFIS + this.w_MCIMPA09;
          ,SCQUOPER = SCQUOPER + this.w_MCIMPA12;
          ,SCACCANT = SCACCANT + this.w_MCIMPA15;
          &i_ccchkf. ;
       where;
          SCCODCES = this.w_MCCODCES;
          and SCCODESE = this.w_MCCOMPET;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Inserisco Riferimento Mov.Cespite
    * --- Write into CES_AMMO
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.CES_AMMO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CES_AMMO_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.CES_AMMO_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"ACRIFMOV ="+cp_NullLink(cp_ToStrODBC(this.w_MCSERIAL),'CES_AMMO','ACRIFMOV');
          +i_ccchkf ;
      +" where ";
          +"ACNUMREG = "+cp_ToStrODBC(this.oParentObject.w_NUMREG);
          +" and ACCODESE = "+cp_ToStrODBC(this.oParentObject.w_CODESE);
          +" and ACCODCES = "+cp_ToStrODBC(this.w_MCCODCES);
             )
    else
      update (i_cTable) set;
          ACRIFMOV = this.w_MCSERIAL;
          &i_ccchkf. ;
       where;
          ACNUMREG = this.oParentObject.w_NUMREG;
          and ACCODESE = this.oParentObject.w_CODESE;
          and ACCODCES = this.w_MCCODCES;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Inserisco Flag Definitiva
    * --- Write into CES_PIAN
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.CES_PIAN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CES_PIAN_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.CES_PIAN_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PAFLDEFI ="+cp_NullLink(cp_ToStrODBC("D"),'CES_PIAN','PAFLDEFI');
          +i_ccchkf ;
      +" where ";
          +"PANUMREG = "+cp_ToStrODBC(this.oParentObject.w_NUMREG);
          +" and PACODESE = "+cp_ToStrODBC(this.oParentObject.w_CODESE);
             )
    else
      update (i_cTable) set;
          PAFLDEFI = "D";
          &i_ccchkf. ;
       where;
          PANUMREG = this.oParentObject.w_NUMREG;
          and PACODESE = this.oParentObject.w_CODESE;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    this.w_GEN = this.w_GEN + 1
    return
  proc Try_038803B8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SAL_CESP
    i_nConn=i_TableProp[this.SAL_CESP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SAL_CESP_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SAL_CESP_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SCCODCES"+",SCCODESE"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_MCCODCES),'SAL_CESP','SCCODCES');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MCCOMPET),'SAL_CESP','SCCODESE');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SCCODCES',this.w_MCCODCES,'SCCODESE',this.w_MCCOMPET)
      insert into (i_cTable) (SCCODCES,SCCODESE &i_ccchkf. );
         values (;
           this.w_MCCODCES;
           ,this.w_MCCOMPET;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,7)]
    this.cWorkTables[1]='CES_AMMO'
    this.cWorkTables[2]='MOV_CESP'
    this.cWorkTables[3]='CES_PIAN'
    this.cWorkTables[4]='ESERCIZI'
    this.cWorkTables[5]='CES_PITI'
    this.cWorkTables[6]='SAL_CESP'
    this.cWorkTables[7]='PAR_CESP'
    return(this.OpenAllTables(7))

  proc CloseCursors()
    if used('_Curs_CES_AMMO')
      use in _Curs_CES_AMMO
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
