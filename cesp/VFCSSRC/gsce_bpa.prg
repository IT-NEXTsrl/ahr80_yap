* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_bpa                                                        *
*              Elabora piano di ammortamento                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_147]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-03-19                                                      *
* Last revis.: 2014-11-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipOpe
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsce_bpa",oParentObject,m.pTipOpe)
return(i_retval)

define class tgsce_bpa as StdBatch
  * --- Local variables
  pTipOpe = space(1)
  w_STATO = space(1)
  w_GIORNI = 0
  w_PROESE = 0
  w_DATINI = ctod("  /  /  ")
  w_DATFIN = ctod("  /  /  ")
  w_ANNO1 = 0
  w_ANNO2 = 0
  w_VALUTA1 = space(3)
  w_VALUTA2 = space(3)
  w_OLDAN1 = 0
  w_OLDAN2 = 0
  w_OLDVAL1 = space(3)
  w_DaScartare = .f.
  w_DaGenera = .f.
  w_AMMIMM = space(1)
  w_AMMIMC = space(1)
  w_CCNSOAMM = space(1)
  w_CCNSOAMF = space(1)
  w_CENSOAMM = space(1)
  w_CENSOAMF = space(1)
  w_FLRIDU = space(1)
  w_GIORESER = 0
  w_GIORELAB = 0
  w_GIORESER_C = 0
  w_GIORELAB_C = 0
  w_IMPV01 = 0
  w_IMPV02 = 0
  w_IMPV03 = 0
  w_IMPV04 = 0
  w_IMPV05 = 0
  w_IMPV06 = 0
  w_IMPV07 = 0
  w_IMPV08 = 0
  w_IMPV09 = 0
  w_IMPV10 = 0
  w_IMPV11 = 0
  w_IMPV12 = 0
  w_IMPV13 = 0
  w_IMPV14 = 0
  w_IMPRIV = 0
  w_PERDEF = 0
  w_IMPMAX = 0
  w_TOTACCIV = 0
  w_RESACCIV = 0
  w_TOTACCFI = 0
  w_RESACCFI = 0
  w_TOTACCAN = 0
  w_IMPV01S = 0
  w_IMPV16S = 0
  w_IMPRIV2 = 0
  w_IMPV16 = 0
  w_IMPV17 = 0
  w_DTPRIC = ctod("  /  /  ")
  w_FISTROVC = .f.
  w_CODESC = space(4)
  w_MCCOMPET = space(4)
  w_INIESE = ctod("  /  /  ")
  w_MCVALNAZ = space(3)
  w_FINESE = ctod("  /  /  ")
  w_DECTOT = 0
  w_CODAZI = space(5)
  w_TCOECIV = 0
  w_DATAGG = space(1)
  w_TCOEFI1 = 0
  w_MCCODCAU = space(5)
  w_TCOEFI2 = 0
  w_STALIG = ctod("  /  /  ")
  w_OKCIVILE = .f.
  w_OKFISORD = .f.
  w_OKFISANT = .f.
  w_DATINI = ctod("  /  /  ")
  w_DATFIN = ctod("  /  /  ")
  w_ANNO1 = 0
  w_ANNO2 = 0
  w_MCCODCES = space(20)
  w_QUOFIS = 0
  w_MOVRIF = space(10)
  w_COECIV = 0
  w_QUOCIV = 0
  w_MCIMPA07 = 0
  w_COEFI1 = 0
  w_CODESE = space(4)
  w_COEFI2 = 0
  w_MCIMPA09 = 0
  w_TIPAMM = space(1)
  w_AMTIPAMM = space(1)
  w_MCIMPA12 = 0
  w_IMPV11 = 0
  w_FLAUMU = space(1)
  w_MESS = space(10)
  w_IMPV12 = 0
  w_DTPRIU = ctod("  /  /  ")
  w_OK = .f.
  w_CODCAT = space(15)
  w_RIFCON = space(10)
  w_STABEN = space(1)
  w_FLCEUS = space(1)
  w_MCIMPA15 = 0
  w_QUOFI1 = 0
  w_TCODESE = space(4)
  w_CEPROESE = 0
  w_CEPROESC = 0
  w_CEFLAMCI = space(1)
  w_CEPERCIV = 0
  w_CODESF = space(4)
  w_CEVALLIM = space(3)
  w_CEESPRIU = space(4)
  w_CEESPRIC = space(4)
  w_CECOEFI1 = 0
  w_CECOEFI2 = 0
  w_CEQUOFIS = 0
  w_CEQUOFI1 = 0
  w_CEDURCES = 0
  w_FISTROV = .f.
  w_COMPRC = space(4)
  w_FINCOM = ctod("  /  /  ")
  w_TIPBEN = space(1)
  w_TRANMSG = space(100)
  w_CODESE = space(4)
  w_PNFLSALI = space(1)
  w_PNFLSALF = space(1)
  w_DATPNT = ctod("  /  /  ")
  w_PNFLSALD = space(1)
  w_ROWNUM = 0
  w_PNTIPCON = space(1)
  w_PNCODCON = space(15)
  w_PNIMPDAR = 0
  w_PNIMPAVE = 0
  w_OKCES = .f.
  * --- WorkFile variables
  AZIENDA_idx=0
  CES_AMMO_idx=0
  ESERCIZI_idx=0
  MOV_CESP_idx=0
  MOVICOST_idx=0
  PNT_DETT_idx=0
  PNT_MAST_idx=0
  SAL_CESP_idx=0
  SALDICON_idx=0
  PNT_CESP_idx=0
  AMM_CESP_idx=0
  CAT_CESP_idx=0
  PAR_CESP_idx=0
  CES_PIAN_idx=0
  AMC_CESP_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elabora il Piano di Ammortamento (da GSCE_APA)
    * --- Tipo Operazione: I =Record Inserted, U=Record Updated, D=Record Deleted
    * --- Notifica se il Cespite non ha un ammortamento definito per l'esercizio
    * --- Variabili utilizzate per verificare se calcolare l'ammortamento civile oppure fiscale
    * --- Variabili utilizzate per il calcolo degli accantonamenti per "Dietimi"
    * --- Variabili ritornate dal Batch di Calcolo Valori Cespite (GSCE_BCV)
    * --- Inizializza variabili di Lavoro
    this.w_STATO = IIF(this.oParentObject.w_PA_STATO $ "EP", this.oParentObject.w_PA_STATO, " ")
    * --- Resoconto Cespiti non ammortizzabili
    create cursor ResocCespi (categoria C(20), cespite C(20),messaggio C(200))
    =wrcursor("ResocCespi")
    * --- Variabili da Passare al Batch di Calcolo Valori Cespite che devono essere inizializzate (GSCE_BCV)
    * --- Letti dal Cespite
    * --- Var. lette dal cespite 
    this.w_TCODESE = this.oParentObject.w_PACODESE
    * --- inizializzati dall'Anagrafica
    this.w_MCCOMPET = this.oParentObject.w_PACODESE
    this.w_MCVALNAZ = this.oParentObject.w_VALNAZ
    this.w_INIESE = this.oParentObject.w_ESEINI
    this.w_FINESE = this.oParentObject.w_ESEFIN
    this.w_DECTOT = this.oParentObject.w_TOTDEC
    this.w_CODAZI = i_CODAZI
    this.w_STALIG = cp_CharToDate("  -  -  ")
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZSTALIG"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZSTALIG;
        from (i_cTable) where;
            AZCODAZI = this.w_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_STALIG = NVL(cp_ToDate(_read_.AZSTALIG),cp_NullValue(_read_.AZSTALIG))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_OK = .T.
    this.w_MESS = ah_Msgformat("Transazione abbandonata")
    * --- Non Gestiti
    this.w_DATAGG = "U"
    this.w_MCCODCAU = SPACE(5)
    if this.oParentObject.w_OFLDEFI="D" AND this.pTipOpe $ "UDE" AND this.oParentObject.w_ELABORA
      * --- Controllo se il piano che si intende variare oppure cancellare ha la data di fine
      *     elaborazione all'interno dell'esercizio di consolidamento impostato nei parametri cespiti.
      * --- Read from PAR_CESP
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PAR_CESP_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_CESP_idx,2],.t.,this.PAR_CESP_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "PCDATCON"+;
          " from "+i_cTable+" PAR_CESP where ";
              +"PCCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          PCDATCON;
          from (i_cTable) where;
              PCCODAZI = this.w_CODAZI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_COMPRC = NVL(cp_ToDate(_read_.PCDATCON),cp_NullValue(_read_.PCDATCON))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if NOT EMPTY(this.w_COMPRC)
        * --- Read from ESERCIZI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ESERCIZI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2],.t.,this.ESERCIZI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ESFINESE"+;
            " from "+i_cTable+" ESERCIZI where ";
                +"ESCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
                +" and ESCODESE = "+cp_ToStrODBC(this.w_COMPRC);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ESFINESE;
            from (i_cTable) where;
                ESCODAZI = this.w_CODAZI;
                and ESCODESE = this.w_COMPRC;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_FINCOM = NVL(cp_ToDate(_read_.ESFINESE),cp_NullValue(_read_.ESFINESE))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_FINCOM>=this.oParentObject.w_PADATINI
          if this.pTipOpe$ "DE"
            this.w_MESS = "Esercizio di competenza compreso entro la data di consolidamento"
          else
            this.w_MESS = "Esercizio di competenza compreso entro la data di consolidamento%0Transazione abbandonata"
          endif
          this.w_OK = .F.
        endif
      endif
      if this.w_OK=.T.
        * --- Se generati Movimenti da Accantonamento li Elimina
        if this.pTipOpe$ "UD"
          if this.pTipOpe="D"
            this.w_MESS = "Attenzione:%0Eliminando il piano di ammortamento definitivo%0verranno eliminati tutti i movimenti cespiti associati%0e le eventuali registrazioni contabili associate%0Proseguo?"
          else
            this.w_MESS = "Attenzione:%0Variando il piano di ammortamento definitivo verranno eliminati%0tutti i movimenti cespiti associati e le eventuali registrazioni contabili%0Proseguo?"
          endif
        else
          this.w_MESS = "Si vuole procedere con l'eliminazione dei movimenti cespiti%0associati al piano e delle eventuali registrazioni contabili?"
        endif
        this.w_OK = ah_YesNo(this.w_MESS)
        if this.pTipOpe="E"
          this.w_MESS = "Operazione abbandonata"
        else
          this.w_MESS = "Transazione abbandonata"
        endif
      endif
    endif
    * --- Se Vario o Cancello Elimina gli Ammortamenti Cespiti gia' Inseriti
    if this.w_OK=.T. AND this.pTipOpe $ "UDE" AND this.oParentObject.w_ELABORA
      if this.pTipOpe="E"
        * --- Try
        local bErr_02FB7318
        bErr_02FB7318=bTrsErr
        this.Try_02FB7318()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
        endif
        bTrsErr=bTrsErr or bErr_02FB7318
        * --- End
      else
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    if this.w_OK=.T. AND this.pTipOpe $ "UI" AND this.oParentObject.w_ELABORA
      * --- Controllo se ho specificato percentuali diverse nell'elaborazione del piano.
      * --- Il codice categoria deve essere presente per poter specificare percentuali differenti.
      this.w_OKCIVILE = .F.
      this.w_OKFISORD = .F.
      this.w_OKFISANT = .F.
      * --- Select from GSCE_BPA
      do vq_exec with 'GSCE_BPA',this,'_Curs_GSCE_BPA','',.f.,.t.
      if used('_Curs_GSCE_BPA')
        select _Curs_GSCE_BPA
        locate for 1=1
        do while not(eof())
        * --- inizializza
        this.w_IMPV01 = 0
        this.w_IMPV02 = 0
        this.w_IMPV03 = 0
        this.w_IMPV04 = 0
        this.w_IMPV05 = 0
        this.w_IMPV06 = 0
        this.w_IMPV07 = 0
        this.w_IMPV08 = 0
        this.w_IMPV09 = 0
        this.w_IMPV10 = 0
        this.w_IMPV11 = 0
        this.w_IMPV12 = 0
        this.w_IMPV13 = 0
        this.w_IMPV14 = 0
        this.w_IMPV16 = 0
        this.w_IMPV17 = 0
        this.w_IMPRIV = 0
        this.w_IMPRIV2 = 0
        this.w_QUOFIS = 0
        this.w_QUOCIV = 0
        this.w_TOTACCIV = 0
        this.w_RESACCIV = 0
        this.w_TOTACCFI = 0
        this.w_RESACCFI = 0
        this.w_QUOFI1 = 0
        this.w_TOTACCAN = 0
        * --- Letti dal Cespite
        this.w_MCCODCES = NVL(_Curs_GSCE_BPA.CECODICE," ")
        this.w_PERDEF = NVL(_Curs_GSCE_BPA.CEPERDEF, 0)
        this.w_IMPMAX = NVL(_Curs_GSCE_BPA.CEIMPMAX, 0)
        this.w_CODCAT = NVL(_Curs_GSCE_BPA.CECODCAT, SPACE(15))
        this.w_CODESE = NVL(_Curs_GSCE_BPA.CECODESE, SPACE(4))
        this.w_TIPAMM = NVL(_Curs_GSCE_BPA.CETIPAMM, " ")
        this.w_DTPRIU = CP_TODATE(_Curs_GSCE_BPA.CEDTPRIU)
        this.w_DTPRIC = CP_TODATE(_Curs_GSCE_BPA.CEDTPRIC)
        this.w_STABEN = NVL(_Curs_GSCE_BPA.CESTABEN,"")
        this.w_FLAUMU = NVL(_Curs_GSCE_BPA.CEFLAUMU, " ")
        this.w_FLCEUS = NVL(_Curs_GSCE_BPA.CEFLCEUS,"")
        this.w_CEESPRIU = NVL(_Curs_GSCE_BPA.CEESPRIU,SPACE(4))
        this.w_CEESPRIC = NVL(_Curs_GSCE_BPA.CEESPRIC,SPACE(4))
        this.w_CEVALLIM = NVL(_Curs_GSCE_BPA.CEVALLIM, SPACE(3))
        this.w_CEDURCES = NVL(_Curs_GSCE_BPA.CEDURCES, SPACE(3))
        this.w_AMMIMM = NVL(_Curs_GSCE_BPA.CEAMMIMM,"N")
        this.w_AMMIMC = NVL(_Curs_GSCE_BPA.CEAMMIMC, "N")
        this.w_CCNSOAMM = NVL(_Curs_GSCE_BPA.CCNSOAMM, "N")
        this.w_CCNSOAMF = NVL(_Curs_GSCE_BPA.CCNSOAMF, "N")
        this.w_CENSOAMM = NVL(_Curs_GSCE_BPA.CENSOAMM, "N")
        this.w_CENSOAMF = NVL(_Curs_GSCE_BPA.CENSOAMF, "N")
        this.w_FLRIDU = NVL(_Curs_GSCE_BPA.CCFLRIDU, "N")
        this.w_CEPROESE = CALCPROCES(this.w_CEESPRIU,this.w_MCCOMPET)
        this.w_CEPROESC = CALCPROCES(this.w_CEESPRIC,this.w_MCCOMPET)
        this.w_FISTROV = .F.
        this.w_FISTROVC = .F.
        * --- sbianco le variabili prima della nuova lettura
        this.w_COEFI1 = 0
        this.w_QUOFIS = 0
        this.w_COEFI2 = 0
        this.w_QUOFI1 = 0
        this.w_AMTIPAMM = " "
        this.w_CEFLAMCI = " "
        this.w_CEPERCIV = 0
        this.w_COECIV = 0
        this.w_QUOCIV = 0
        this.w_CODESF = space(4)
        this.w_CODESC = space(4)
        * --- Select from AMM_CESP
        i_nConn=i_TableProp[this.AMM_CESP_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.AMM_CESP_idx,2],.t.,this.AMM_CESP_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select AMPERORD,AMPERANT,AMQUOFIS,AMQUOFI1,AMTIPAMM  from "+i_cTable+" AMM_CESP ";
              +" where AMCODCES="+cp_ToStrODBC(this.w_MCCODCES)+" AND AMPROESE="+cp_ToStrODBC(this.w_CEPROESE)+"";
               ,"_Curs_AMM_CESP")
        else
          select AMPERORD,AMPERANT,AMQUOFIS,AMQUOFI1,AMTIPAMM from (i_cTable);
           where AMCODCES=this.w_MCCODCES AND AMPROESE=this.w_CEPROESE;
            into cursor _Curs_AMM_CESP
        endif
        if used('_Curs_AMM_CESP')
          select _Curs_AMM_CESP
          locate for 1=1
          do while not(eof())
          this.w_COEFI1 = NVL(_Curs_AMM_CESP.AMPERORD, 0)
          this.w_QUOFIS = NVL(_Curs_AMM_CESP.AMQUOFIS, 0)
          this.w_COEFI2 = NVL(_Curs_AMM_CESP.AMPERANT, 0)
          this.w_QUOFI1 = NVL(_Curs_AMM_CESP.AMQUOFI1,0)
          this.w_AMTIPAMM = Nvl(_Curs_AMM_CESP.AMTIPAMM,"O")
          this.w_CODESF = CALCESCES(this.w_CEESPRIU,this.w_CEPROESE)
          this.w_FISTROV = .T.
            select _Curs_AMM_CESP
            continue
          enddo
          use
        endif
        * --- Select from AMC_CESP
        i_nConn=i_TableProp[this.AMC_CESP_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.AMC_CESP_idx,2],.t.,this.AMC_CESP_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select ACPERORD,ACQUOFIS,ACTIPAMM  from "+i_cTable+" AMC_CESP ";
              +" where ACCODCES="+cp_ToStrODBC(this.w_MCCODCES)+" AND ACPROESE="+cp_ToStrODBC(this.w_CEPROESC)+"";
               ,"_Curs_AMC_CESP")
        else
          select ACPERORD,ACQUOFIS,ACTIPAMM from (i_cTable);
           where ACCODCES=this.w_MCCODCES AND ACPROESE=this.w_CEPROESC;
            into cursor _Curs_AMC_CESP
        endif
        if used('_Curs_AMC_CESP')
          select _Curs_AMC_CESP
          locate for 1=1
          do while not(eof())
          this.w_CEFLAMCI = Nvl(_Curs_AMC_CESP.ACTIPAMM,"I")
          this.w_COECIV = NVL(_Curs_AMC_CESP.ACPERORD, 0)
          this.w_QUOCIV = NVL(_Curs_AMC_CESP.ACQUOFIS, 0)
          this.w_CODESC = CALCESCES(this.w_CEESPRIC,this.w_CEPROESC)
          this.w_FISTROVC = .T.
            select _Curs_AMC_CESP
            continue
          enddo
          use
        endif
        * --- Controllo se nell'elaborazione del piano ho specificato la categoria.
        *     Se ho specificato la categoria devo verificare se ho introdotto percentuali
        *     per i vari tipi di ammortamento.
        if Not empty(this.oParentObject.w_Pacodcat)
          * --- Verifico se ho specificato una percentuale per l'accantonamento civile
          if this.oParentObject.w_Pacoeciv<>0 AND !this.w_FISTROVC
            this.w_COECIV = Nvl(this.oParentObject.w_Pacoeciv,0)
            this.w_QUOCIV = 0
            this.w_OKCIVILE = .T.
          endif
          * --- Applica la percentuale della maschera se non ha trovato ammortamenti fiscali cespiti particolari
          * --- Controllo se la data di primo utilizzo fiscale � stata inserita nella tabella
          *     Cespiti ed inoltre che sia minore della data di elaborazione del piano
          if Not empty(this.w_Dtpriu) And ( (this.w_Dtpriu<=this.oParentObject.w_Padatreg And this.oParentObject.w_Patipgen="D") or (this.w_Dtpriu<=this.oParentObject.w_Padatfin And this.oParentObject.w_Patipgen="X") )
            if this.oParentObject.w_Pacoefi1<>0 AND !this.w_FISTROV
              * --- Effettuo una lettura sulla tabella ESERCIZI  per contare il numero 
              *     di esercizio che intercorrono tra la data di entrata in funzione del bene 
              *     e la data nella quale si effettua l'ammortanto.
              this.w_PROESE = 0
              this.w_PROESE = CALCPROCES(this.w_CEESPRIU,this.oParentObject.w_PACODESE)
              * --- Se sono nel primo esercizio devo verificare la percentuale di riduzione
              * --- dei coefficienti fiscali specificata nei parametri cespiti
              * --- Read from CAT_CESP
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.CAT_CESP_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.CAT_CESP_idx,2],.t.,this.CAT_CESP_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "CCTIPBEN"+;
                  " from "+i_cTable+" CAT_CESP where ";
                      +"CCCODICE = "+cp_ToStrODBC(this.w_CODCAT);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  CCTIPBEN;
                  from (i_cTable) where;
                      CCCODICE = this.w_CODCAT;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_TIPBEN = NVL(cp_ToDate(_read_.CCTIPBEN),cp_NullValue(_read_.CCTIPBEN))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              * --- Applico la riduzione solo se il tipo bene della categoria � materiale
              if this.w_Proese=1 AND this.w_TIPBEN="M" AND this.w_FLRIDU="S" AND this.oParentObject.w_PERRID<>0
                this.w_COEFI1 = cp_Round(Nvl(this.oParentObject.w_Pacoefi1,0)*(this.oParentObject.w_PERRID/100),2)
                this.w_QUOFIS = 0
                this.w_OKFISORD = .T.
              else
                this.w_COEFI1 = Nvl(this.oParentObject.w_Pacoefi1,0)
                this.w_QUOFIS = 0
                this.w_OKFISORD = .T.
              endif
              * --- Verifico se ho specificato una percentuale diversa per l'ammortamento anticipato.
              * --- In questo caso devo verificare anche se per il cespite ho superato il numero massimo di ammortamenti anticipati
              if this.oParentObject.w_Pacoefi2<>0
                this.w_COEFI2 = Nvl(this.oParentObject.w_Pacoefi2,0)
                do Gsce_Bp1 with this
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
                this.w_QUOFI1 = 0
                this.w_OKFISANT = iif(this.w_Coefi2<>0,.T.,.F.)
              else
                this.w_COEFI2 = 0
                this.w_QUOFI1 = 0
                this.w_OKFISANT = .T.
              endif
            endif
          else
            this.w_COEFI1 = 0
            this.w_QUOFIS = 0
            this.w_COEFI2 = 0
            this.w_QUOFI1 = 0
          endif
        endif
        if NOT EMPTY(this.w_MCCODCES)
          ah_Msg("Elaborazione cespite: %1",.T.,.F.,.F., this.w_MCCODCES)
          if NOT EMPTY(this.w_QUOFIS)
            this.w_OKFISORD = .F.
          endif
          if NOT EMPTY(this.w_QUOFI1)
            this.w_OKFISANT = .F.
          endif
          if NOT EMPTY(this.w_QUOCIV)
            this.w_OKCIVILE = .F.
          endif
          this.w_TCOECIV = this.w_COECIV
          this.w_TCOEFI1 = this.w_COEFI1
          this.w_TCOEFI2 = this.w_COEFI2
          * --- Lancia Batch dei Calcoli
          this.w_DaScartare = .t.
          this.w_DaGenera = .t.
          * --- Calcolo le date di elaborazione da utilizzare nel calcolo degli accantonamenti
          if this.oParentObject.w_PATIPGEN="X"
            if Not empty(this.w_Dtpriu) And (this.w_Dtpriu<=this.oParentObject.w_Padatfin)
              * --- Data di primo utilizzo fiscale al di fuori dell'esercizio di elaborazione
              do case
                case this.w_Dtpriu<this.oParentObject.w_Eseini
                  this.w_GIORESER = (this.oParentObject.w_Esefin-this.oParentObject.w_Eseini)+1
                  this.w_GIORELAB = (this.oParentObject.w_Padatfin-this.oParentObject.w_Padatini)+1
                  * --- Data di primo utilizzo fiscale all'interno dell'esercizio di elaborazione ma
                  *     data di primo utilizzo fiscale inferiore alla data di inizio elaborazione
                case this.w_Dtpriu>=this.oParentObject.w_Eseini And this.w_Dtpriu<this.oParentObject.w_Padatini
                  this.w_GIORESER = (this.oParentObject.w_Esefin-this.w_Dtpriu)+1
                  this.w_GIORELAB = (this.oParentObject.w_Padatfin-this.oParentObject.w_Padatini)+1
                  * --- Data di primo utilizzo fiscale all'interno dell'esercizio di elaborazione e
                  *     data di primo utilizzo fiscale superiore alla data di inizio elaborazione
                case this.w_Dtpriu>=this.oParentObject.w_Eseini And this.w_Dtpriu>=this.oParentObject.w_Padatini
                  this.w_GIORESER = (this.oParentObject.w_Esefin-this.w_Dtpriu)+1
                  this.w_GIORELAB = (this.oParentObject.w_Padatfin-this.w_Dtpriu)+1
              endcase
            else
              this.w_GIORESER = 0
              this.w_GIORELAB = 0
            endif
            do case
              case this.w_Dtpric<this.oParentObject.w_Eseini
                this.w_GIORESER_C = (this.oParentObject.w_Esefin-this.oParentObject.w_Eseini)+1
                this.w_GIORELAB_C = (this.oParentObject.w_Padatfin-this.oParentObject.w_Padatini)+1
                * --- Data di primo utilizzo civile all'interno dell'esercizio di elaborazione ma
                *     data di primo utilizzo civile inferiore alla data di inizio elaborazione
              case this.w_Dtpric>=this.oParentObject.w_Eseini And this.w_Dtpric<this.oParentObject.w_Padatini
                this.w_GIORESER_C = (this.oParentObject.w_Esefin-this.w_Dtpric)+1
                this.w_GIORELAB_C = (this.oParentObject.w_Padatfin-this.oParentObject.w_Padatini)+1
                * --- Data di primo utilizzo fiscale all'interno dell'esercizio di elaborazione e
                *     data di primo utilizzo fiscale superiore alla data di inizio elaborazione
              case this.w_Dtpric>=this.oParentObject.w_Eseini And this.w_Dtpric>=this.oParentObject.w_Padatini
                this.w_GIORESER_C = (this.oParentObject.w_Esefin-this.w_Dtpric)+1
                this.w_GIORELAB_C = (this.oParentObject.w_Padatfin-this.w_Dtpric)+1
            endcase
          endif
          GSCE_BCV(this,"A")
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Riassegna dopo i calcoli
          this.w_COECIV = this.w_TCOECIV
          this.w_COEFI1 = this.w_TCOEFI1
          this.w_COEFI2 = this.w_TCOEFI2
          if this.w_DaGenera=.T.
            * --- Verifichiamo la tipologia di ammortamento
            do case
              case this.oParentObject.w_PATIPGEN="D"
                if this.oParentObject.w_PANUMFRA<>this.oParentObject.w_PANUMMES
                  * --- Importi da Frazionare
                  this.w_IMPV08 = cp_ROUND((this.w_IMPV08*this.oParentObject.w_PANUMFRA)/this.oParentObject.w_PANUMMES, this.w_DECTOT)
                  this.w_IMPV09 = cp_ROUND((this.w_IMPV09*this.oParentObject.w_PANUMFRA)/this.oParentObject.w_PANUMMES, this.w_DECTOT)
                  this.w_IMPV10 = cp_ROUND((this.w_IMPV10*this.oParentObject.w_PANUMFRA)/this.oParentObject.w_PANUMMES, this.w_DECTOT)
                  this.w_IMPV13 = cp_ROUND((this.w_IMPV13*this.oParentObject.w_PANUMFRA)/this.oParentObject.w_PANUMMES, this.w_DECTOT)
                  * --- Parte del valore del bene che fa parte dell'esercizio utilizzata per ricalcolare il coefficente
                  this.w_IMPV01S = cp_ROUND((this.w_IMPV01*this.oParentObject.w_PANUMFRA)/this.oParentObject.w_PANUMMES, this.w_DECTOT)
                  this.w_IMPV16S = cp_ROUND((this.w_IMPV16*this.oParentObject.w_PANUMFRA)/this.oParentObject.w_PANUMMES, this.w_DECTOT)
                  if this.w_IMPV16S<>0 
                    * --- effettuo la divisione  solo se w_impv01S � diverso da 0 
                    this.w_COECIV = cp_ROUND(((this.w_IMPV08*100)/this.w_IMPV16S),2)
                  endif
                  if this.w_IMPV01S<>0 
                    * --- effettuo la divisione  solo se w_impv01S � diverso da 0 
                    this.w_COEFI1 = cp_ROUND(((this.w_IMPV09*100)/this.w_IMPV01S),2)
                    this.w_COEFI2 = cp_ROUND(((this.w_IMPV13*100)/this.w_IMPV01S),2)
                  endif
                else
                  * --- Rieseguo un'arrotondamento sui valori per problema Fox Pro 
                  *     che tiene decimali oltre la sesta posizione. In alcuni casi viene memorizzato sul
                  *     database un valore sbagliato
                  this.w_IMPV08 = cp_ROUND(this.w_IMPV08, this.w_DECTOT)
                  this.w_IMPV09 = cp_ROUND(this.w_IMPV09, this.w_DECTOT)
                  this.w_IMPV10 = cp_ROUND(this.w_IMPV10, this.w_DECTOT)
                  this.w_IMPV13 = cp_ROUND(this.w_IMPV13, this.w_DECTOT)
                  if this.w_IMPV16<>0
                    * --- effettuo la divisione  solo se w_impv16 � diverso da 0 
                    this.w_COECIV = cp_ROUND(((this.w_IMPV08*100)/this.w_IMPV16),2)
                  endif
                  if this.w_IMPV01<>0 
 
                    * --- effettuo la divisione  solo se w_impv01S � diverso da 0 
                    this.w_COEFI1 = cp_ROUND(((this.w_IMPV09*100)/this.w_IMPV01),2)
                    this.w_COEFI2 = cp_ROUND(((this.w_IMPV13*100)/this.w_IMPV01),2)
                  endif
                endif
                * --- Calcolo il valore del Totale Accantonato Civile, Residuo da ammortamento Civile,
                * --- il Totale Accantonamento Fiscale ed il Residuo da Accantonamento Fiscale.
                this.w_TOTACCIV = (this.w_IMPV03+this.w_IMPV08)
                this.w_RESACCIV = (this.w_IMPV04-this.w_IMPV08)
                this.w_TOTACCFI = (this.w_IMPV06+this.w_IMPV09)
                this.w_TOTACCAN = (this.w_IMPV14+this.w_IMPV13)
                this.w_RESACCFI = (this.w_IMPV07-this.w_IMPV09-this.w_IMPV13)
              case this.oParentObject.w_PATIPGEN="X"
                * --- Importi da ripartire per data
                if this.w_GIORELAB_C<>this.w_GIORESER_C
                  this.w_IMPV08 = cp_ROUND((this.w_IMPV08*this.w_GIORELAB_C)/this.w_GIORESER_C, this.w_DECTOT)
                  this.w_IMPV16S = cp_ROUND((this.w_IMPV16*this.w_GIORELAB_C)/this.w_GIORESER_C, this.w_DECTOT)
                else
                  this.w_IMPV08 = cp_ROUND(this.w_IMPV08,this.w_DECTOT)
                  this.w_IMPV16S = cp_ROUND(this.w_IMPV16,this.w_DECTOT)
                endif
                if this.w_GIORELAB<>this.w_GIORESER
                  this.w_IMPV09 = cp_ROUND((this.w_IMPV09*this.w_GIORELAB)/this.w_GIORESER, this.w_DECTOT)
                  this.w_IMPV10 = cp_ROUND((this.w_IMPV10*this.w_GIORELAB)/this.w_GIORESER, this.w_DECTOT)
                  this.w_IMPV13 = cp_ROUND((this.w_IMPV13*this.w_GIORELAB)/this.w_GIORESER, this.w_DECTOT)
                  * --- Parte del valore del bene che fa parte dell'esercizio utilizzata per ricalcolare il coefficente
                  this.w_IMPV01S = cp_ROUND((this.w_IMPV01*this.w_GIORELAB)/this.w_GIORESER, this.w_DECTOT)
                else
                  this.w_IMPV09 = cp_ROUND(this.w_IMPV09,this.w_DECTOT)
                  this.w_IMPV10 = cp_ROUND(this.w_IMPV10,this.w_DECTOT)
                  this.w_IMPV13 = cp_ROUND(this.w_IMPV13,this.w_DECTOT)
                  * --- Parte del valore del bene che fa parte dell'esercizio utilizzata per ricalcolare il coefficente
                  this.w_IMPV01S = cp_ROUND(this.w_IMPV01,this.w_DECTOT)
                endif
                if this.w_IMPV16S<>0 
                  * --- effettuo la divisione  solo se w_impv01S � diverso da 0 
                  this.w_COECIV = cp_ROUND(((this.w_IMPV08*100)/this.w_IMPV16S),2)
                endif
                if this.w_IMPV01S<>0 
                  * --- effettuo la divisione  solo se w_impv01S � diverso da 0 
                  this.w_COEFI1 = cp_ROUND(((this.w_IMPV09*100)/this.w_IMPV01S),2)
                  this.w_COEFI2 = cp_ROUND(((this.w_IMPV13*100)/this.w_IMPV01S),2)
                endif
                * --- Calcolo il valore del Totale Accantonato Civile, Residuo da ammortamento Civile,
                * --- il Totale Accantonamento Fiscale ed il Residuo da Accantonamento Fiscale.
                this.w_TOTACCIV = (this.w_IMPV03+this.w_IMPV08)
                this.w_RESACCIV = (this.w_IMPV04-this.w_IMPV08)
                this.w_TOTACCFI = (this.w_IMPV06+this.w_IMPV09)
                this.w_TOTACCAN = (this.w_IMPV14+this.w_IMPV13)
                this.w_RESACCFI = (this.w_IMPV07-this.w_IMPV09-this.w_IMPV13)
            endcase
            * --- Scrive Ammortamento Cespite
            * --- Try
            local bErr_02F443E8
            bErr_02F443E8=bTrsErr
            this.Try_02F443E8()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- accept error
              bTrsErr=.f.
              * --- Write into CES_AMMO
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.CES_AMMO_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.CES_AMMO_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.CES_AMMO_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"ACIMPV01 ="+cp_NullLink(cp_ToStrODBC(this.w_IMPV01),'CES_AMMO','ACIMPV01');
                +",ACIMPV03 ="+cp_NullLink(cp_ToStrODBC(this.w_TOTACCIV),'CES_AMMO','ACIMPV03');
                +",ACIMPV04 ="+cp_NullLink(cp_ToStrODBC(this.w_RESACCIV),'CES_AMMO','ACIMPV04');
                +",ACIMPV05 ="+cp_NullLink(cp_ToStrODBC(this.w_IMPV05),'CES_AMMO','ACIMPV05');
                +",ACIMPV06 ="+cp_NullLink(cp_ToStrODBC(this.w_TOTACCFI),'CES_AMMO','ACIMPV06');
                +",ACIMPV07 ="+cp_NullLink(cp_ToStrODBC(this.w_RESACCFI),'CES_AMMO','ACIMPV07');
                +",ACIMPV08 ="+cp_NullLink(cp_ToStrODBC(this.w_IMPV08),'CES_AMMO','ACIMPV08');
                +",ACIMPV09 ="+cp_NullLink(cp_ToStrODBC(this.w_IMPV09),'CES_AMMO','ACIMPV09');
                +",ACIMPV10 ="+cp_NullLink(cp_ToStrODBC(this.w_IMPV10),'CES_AMMO','ACIMPV10');
                +",ACCOEFI1 ="+cp_NullLink(cp_ToStrODBC(this.w_COEFI1),'CES_AMMO','ACCOEFI1');
                +",ACCOEFI2 ="+cp_NullLink(cp_ToStrODBC(this.w_COEFI2),'CES_AMMO','ACCOEFI2');
                +",ACIMPV11 ="+cp_NullLink(cp_ToStrODBC(this.w_IMPV11),'CES_AMMO','ACIMPV11');
                +",ACIMPV12 ="+cp_NullLink(cp_ToStrODBC(this.w_IMPV12),'CES_AMMO','ACIMPV12');
                +",ACCOECIV ="+cp_NullLink(cp_ToStrODBC(this.w_COECIV),'CES_AMMO','ACCOECIV');
                +",ACRIFMOV ="+cp_NullLink(cp_ToStrODBC(SPACE(10)),'CES_AMMO','ACRIFMOV');
                +",ACIMPV13 ="+cp_NullLink(cp_ToStrODBC(this.w_IMPV13),'CES_AMMO','ACIMPV13');
                +",ACIMPV14 ="+cp_NullLink(cp_ToStrODBC(this.w_TOTACCAN),'CES_AMMO','ACIMPV14');
                +",ACIMPV16 ="+cp_NullLink(cp_ToStrODBC(this.w_IMPV16),'CES_AMMO','ACIMPV16');
                    +i_ccchkf ;
                +" where ";
                    +"ACNUMREG = "+cp_ToStrODBC(this.oParentObject.w_PANUMREG);
                    +" and ACCODESE = "+cp_ToStrODBC(this.oParentObject.w_PACODESE);
                    +" and ACCODCES = "+cp_ToStrODBC(this.w_MCCODCES);
                       )
              else
                update (i_cTable) set;
                    ACIMPV01 = this.w_IMPV01;
                    ,ACIMPV03 = this.w_TOTACCIV;
                    ,ACIMPV04 = this.w_RESACCIV;
                    ,ACIMPV05 = this.w_IMPV05;
                    ,ACIMPV06 = this.w_TOTACCFI;
                    ,ACIMPV07 = this.w_RESACCFI;
                    ,ACIMPV08 = this.w_IMPV08;
                    ,ACIMPV09 = this.w_IMPV09;
                    ,ACIMPV10 = this.w_IMPV10;
                    ,ACCOEFI1 = this.w_COEFI1;
                    ,ACCOEFI2 = this.w_COEFI2;
                    ,ACIMPV11 = this.w_IMPV11;
                    ,ACIMPV12 = this.w_IMPV12;
                    ,ACCOECIV = this.w_COECIV;
                    ,ACRIFMOV = SPACE(10);
                    ,ACIMPV13 = this.w_IMPV13;
                    ,ACIMPV14 = this.w_TOTACCAN;
                    ,ACIMPV16 = this.w_IMPV16;
                    &i_ccchkf. ;
                 where;
                    ACNUMREG = this.oParentObject.w_PANUMREG;
                    and ACCODESE = this.oParentObject.w_PACODESE;
                    and ACCODCES = this.w_MCCODCES;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
            bTrsErr=bTrsErr or bErr_02F443E8
            * --- End
          else
            * --- Il cespite � stato scartato
          endif
        endif
          select _Curs_GSCE_BPA
          continue
        enddo
        use
      endif
    endif
    if this.w_OK=.F. AND this.pTipOpe $ "UDE" AND this.oParentObject.w_ELABORA
      * --- Nel caso abbia premuto il bottone "Elimina" visualizzo a video un messaggio
      *     di errore
      if this.pTipOpe="E"
        Ah_ErrorMsg(this.w_Mess)
      else
        * --- Solo Update e Delete sono sotto Transazione
        this.w_TRANMSG = AH_MSGFORMAT (this.w_MESS)
        * --- transaction error
        bTrsErr=.t.
        i_TrsMsg=this.w_TRANMSG
      endif
    else
      if this.pTipOpe $ "IU" AND this.oParentObject.w_ELABORA
        ah_ErrorMsg("Elaborazione terminata",,"")
      endif
    endif
    if used("ResocCespi")
      if reccount("ResocCespi")>0
        select * from ResocCespi into cursor __TMP__
        cp_chprn("..\CESP\EXE\QUERY\gsce_srs.frx", " ", this)
        select __TMP__
        use
      endif
      select ResocCespi
      use
    endif
  endproc
  proc Try_02FB7318()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_02F443E8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into CES_AMMO
    i_nConn=i_TableProp[this.CES_AMMO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CES_AMMO_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CES_AMMO_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"ACNUMREG"+",ACCODESE"+",ACCODCES"+",ACIMPV01"+",ACIMPV03"+",ACIMPV04"+",ACIMPV05"+",ACIMPV06"+",ACIMPV07"+",ACIMPV08"+",ACIMPV09"+",ACIMPV10"+",ACCOEFI1"+",ACCOEFI2"+",ACIMPV11"+",ACIMPV12"+",ACCOECIV"+",ACRIFMOV"+",ACIMPV13"+",ACIMPV14"+",ACIMPV16"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PANUMREG),'CES_AMMO','ACNUMREG');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PACODESE),'CES_AMMO','ACCODESE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MCCODCES),'CES_AMMO','ACCODCES');
      +","+cp_NullLink(cp_ToStrODBC(this.w_IMPV01),'CES_AMMO','ACIMPV01');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TOTACCIV),'CES_AMMO','ACIMPV03');
      +","+cp_NullLink(cp_ToStrODBC(this.w_RESACCIV),'CES_AMMO','ACIMPV04');
      +","+cp_NullLink(cp_ToStrODBC(this.w_IMPV05),'CES_AMMO','ACIMPV05');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TOTACCFI),'CES_AMMO','ACIMPV06');
      +","+cp_NullLink(cp_ToStrODBC(this.w_RESACCFI),'CES_AMMO','ACIMPV07');
      +","+cp_NullLink(cp_ToStrODBC(this.w_IMPV08),'CES_AMMO','ACIMPV08');
      +","+cp_NullLink(cp_ToStrODBC(this.w_IMPV09),'CES_AMMO','ACIMPV09');
      +","+cp_NullLink(cp_ToStrODBC(this.w_IMPV10),'CES_AMMO','ACIMPV10');
      +","+cp_NullLink(cp_ToStrODBC(this.w_COEFI1),'CES_AMMO','ACCOEFI1');
      +","+cp_NullLink(cp_ToStrODBC(this.w_COEFI2),'CES_AMMO','ACCOEFI2');
      +","+cp_NullLink(cp_ToStrODBC(this.w_IMPV11),'CES_AMMO','ACIMPV11');
      +","+cp_NullLink(cp_ToStrODBC(this.w_IMPV12),'CES_AMMO','ACIMPV12');
      +","+cp_NullLink(cp_ToStrODBC(this.w_COECIV),'CES_AMMO','ACCOECIV');
      +","+cp_NullLink(cp_ToStrODBC(SPACE(10)),'CES_AMMO','ACRIFMOV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_IMPV13),'CES_AMMO','ACIMPV13');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TOTACCAN),'CES_AMMO','ACIMPV14');
      +","+cp_NullLink(cp_ToStrODBC(this.w_IMPV16),'CES_AMMO','ACIMPV16');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'ACNUMREG',this.oParentObject.w_PANUMREG,'ACCODESE',this.oParentObject.w_PACODESE,'ACCODCES',this.w_MCCODCES,'ACIMPV01',this.w_IMPV01,'ACIMPV03',this.w_TOTACCIV,'ACIMPV04',this.w_RESACCIV,'ACIMPV05',this.w_IMPV05,'ACIMPV06',this.w_TOTACCFI,'ACIMPV07',this.w_RESACCFI,'ACIMPV08',this.w_IMPV08,'ACIMPV09',this.w_IMPV09,'ACIMPV10',this.w_IMPV10)
      insert into (i_cTable) (ACNUMREG,ACCODESE,ACCODCES,ACIMPV01,ACIMPV03,ACIMPV04,ACIMPV05,ACIMPV06,ACIMPV07,ACIMPV08,ACIMPV09,ACIMPV10,ACCOEFI1,ACCOEFI2,ACIMPV11,ACIMPV12,ACCOECIV,ACRIFMOV,ACIMPV13,ACIMPV14,ACIMPV16 &i_ccchkf. );
         values (;
           this.oParentObject.w_PANUMREG;
           ,this.oParentObject.w_PACODESE;
           ,this.w_MCCODCES;
           ,this.w_IMPV01;
           ,this.w_TOTACCIV;
           ,this.w_RESACCIV;
           ,this.w_IMPV05;
           ,this.w_TOTACCFI;
           ,this.w_RESACCFI;
           ,this.w_IMPV08;
           ,this.w_IMPV09;
           ,this.w_IMPV10;
           ,this.w_COEFI1;
           ,this.w_COEFI2;
           ,this.w_IMPV11;
           ,this.w_IMPV12;
           ,this.w_COECIV;
           ,SPACE(10);
           ,this.w_IMPV13;
           ,this.w_TOTACCAN;
           ,this.w_IMPV16;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elimina la Reg.Contabile Associata Al Movimento Cespite
    this.w_DATPNT = cp_CharToDate("  -  -  ")
    this.w_CODESE = g_CODESE
    * --- Read from PNT_MAST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PNT_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2],.t.,this.PNT_MAST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PNDATREG,PNCOMPET"+;
        " from "+i_cTable+" PNT_MAST where ";
            +"PNSERIAL = "+cp_ToStrODBC(this.w_RIFCON);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PNDATREG,PNCOMPET;
        from (i_cTable) where;
            PNSERIAL = this.w_RIFCON;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DATPNT = NVL(cp_ToDate(_read_.PNDATREG),cp_NullValue(_read_.PNDATREG))
      this.w_CODESE = NVL(cp_ToDate(_read_.PNCOMPET),cp_NullValue(_read_.PNCOMPET))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    do case
      case this.w_DATPNT<=this.w_STALIG AND NOT EMPTY(this.w_STALIG)
        this.w_MESS = ah_Msgformat("Reg. contabile gi� stampata su L.G.; impossibile variare o eliminare")
        this.w_OK = .F.
    endcase
    * --- Verifica se ci sono altri mov.cespiti abbinati alla primanota
    this.w_OKCES = .T.
    * --- Select from PNT_CESP
    i_nConn=i_TableProp[this.PNT_CESP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PNT_CESP_idx,2],.t.,this.PNT_CESP_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select ACMOVCES  from "+i_cTable+" PNT_CESP ";
          +" where ACSERIAL="+cp_ToStrODBC(this.w_RIFCON)+" AND ACTIPASS<>'C'";
           ,"_Curs_PNT_CESP")
    else
      select ACMOVCES from (i_cTable);
       where ACSERIAL=this.w_RIFCON AND ACTIPASS<>"C";
        into cursor _Curs_PNT_CESP
    endif
    if used('_Curs_PNT_CESP')
      select _Curs_PNT_CESP
      locate for 1=1
      do while not(eof())
      this.w_OKCES = .F.
        select _Curs_PNT_CESP
        continue
      enddo
      use
    endif
    if this.w_OKCES=.F.
      this.w_MESS = ah_Msgformat("Reg. contabile abbinata ad altri movimenti cespiti; impossibile eliminare")
      this.w_OK = .F.
    endif
    if this.w_OK=.T.
      * --- Quindi elimina...
      * --- Select from PNT_DETT
      i_nConn=i_TableProp[this.PNT_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2],.t.,this.PNT_DETT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" PNT_DETT ";
            +" where PNSERIAL="+cp_ToStrODBC(this.w_RIFCON)+"";
             ,"_Curs_PNT_DETT")
      else
        select * from (i_cTable);
         where PNSERIAL=this.w_RIFCON;
          into cursor _Curs_PNT_DETT
      endif
      if used('_Curs_PNT_DETT')
        select _Curs_PNT_DETT
        locate for 1=1
        do while not(eof())
        this.w_ROWNUM = CPROWNUM
        if NVL(this.w_ROWNUM, 0)<>0
          this.w_PNTIPCON = _Curs_PNT_DETT.PNTIPCON
          this.w_PNCODCON = _Curs_PNT_DETT.PNCODCON
          this.w_PNIMPDAR = NVL(_Curs_PNT_DETT.PNIMPDAR, 0)
          this.w_PNIMPAVE = NVL(_Curs_PNT_DETT.PNIMPAVE, 0)
          * --- Aggiorno i saldi legati alla registrazione di Prima Nota
          this.w_PNFLSALD = NVL(_Curs_PNT_DETT.PNFLSALD,"=")
          this.w_PNFLSALI = NVL(_Curs_PNT_DETT.PNFLSALI,"=")
          this.w_PNFLSALF = NVL(_Curs_PNT_DETT.PNFLSALF,"=")
          * --- Inverte i Flag per Storno Saldi
          this.w_PNFLSALD = IIF(this.w_PNFLSALD="-", "+", IIF(this.w_PNFLSALD="+", "-", " "))
          this.w_PNFLSALI = IIF(this.w_PNFLSALI="-", "+", IIF(this.w_PNFLSALI="+", "-", " "))
          this.w_PNFLSALF = IIF(this.w_PNFLSALF="-", "+", IIF(this.w_PNFLSALF="+", "-", " "))
          * --- Aggiorna Saldi
          * --- Write into SALDICON
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.SALDICON_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2])
            i_cOp1=cp_SetTrsOp(this.w_PNFLSALD,'SLDARPER','this.w_PNIMPDAR',this.w_PNIMPDAR,'update',i_nConn)
            i_cOp2=cp_SetTrsOp(this.w_PNFLSALD,'SLAVEPER','this.w_PNIMPAVE',this.w_PNIMPAVE,'update',i_nConn)
            i_cOp3=cp_SetTrsOp(this.w_PNFLSALI,'SLDARINI','this.w_PNIMPDAR',this.w_PNIMPDAR,'update',i_nConn)
            i_cOp4=cp_SetTrsOp(this.w_PNFLSALI,'SLAVEINI','this.w_PNIMPAVE',this.w_PNIMPAVE,'update',i_nConn)
            i_cOp5=cp_SetTrsOp(this.w_PNFLSALF,'SLDARFIN','this.w_PNIMPDAR',this.w_PNIMPDAR,'update',i_nConn)
            i_cOp6=cp_SetTrsOp(this.w_PNFLSALF,'SLAVEFIN','this.w_PNIMPAVE',this.w_PNIMPAVE,'update',i_nConn)
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICON_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SLDARPER ="+cp_NullLink(i_cOp1,'SALDICON','SLDARPER');
            +",SLAVEPER ="+cp_NullLink(i_cOp2,'SALDICON','SLAVEPER');
            +",SLDARINI ="+cp_NullLink(i_cOp3,'SALDICON','SLDARINI');
            +",SLAVEINI ="+cp_NullLink(i_cOp4,'SALDICON','SLAVEINI');
            +",SLDARFIN ="+cp_NullLink(i_cOp5,'SALDICON','SLDARFIN');
            +",SLAVEFIN ="+cp_NullLink(i_cOp6,'SALDICON','SLAVEFIN');
                +i_ccchkf ;
            +" where ";
                +"SLTIPCON = "+cp_ToStrODBC(this.w_PNTIPCON);
                +" and SLCODICE = "+cp_ToStrODBC(this.w_PNCODCON);
                +" and SLCODESE = "+cp_ToStrODBC(this.w_CODESE);
                   )
          else
            update (i_cTable) set;
                SLDARPER = &i_cOp1.;
                ,SLAVEPER = &i_cOp2.;
                ,SLDARINI = &i_cOp3.;
                ,SLAVEINI = &i_cOp4.;
                ,SLDARFIN = &i_cOp5.;
                ,SLAVEFIN = &i_cOp6.;
                &i_ccchkf. ;
             where;
                SLTIPCON = this.w_PNTIPCON;
                and SLCODICE = this.w_PNCODCON;
                and SLCODESE = this.w_CODESE;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          * --- Elimina i Centri di Costo Associati alla Reg.di P.N.
          * --- (Attenzione non posso fare tutto in un colpo solo xche' MRSERIAL Potrebbe Riferire a Mov.Manuali
          * --- Delete from MOVICOST
          i_nConn=i_TableProp[this.MOVICOST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MOVICOST_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"MRSERIAL = "+cp_ToStrODBC(this.w_RIFCON);
                  +" and MRROWORD = "+cp_ToStrODBC(this.w_ROWNUM);
                   )
          else
            delete from (i_cTable) where;
                  MRSERIAL = this.w_RIFCON;
                  and MRROWORD = this.w_ROWNUM;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
        endif
          select _Curs_PNT_DETT
          continue
        enddo
        use
      endif
      * --- Elimina la Reg.Contabile Associata
      * --- Delete from PNT_CESP
      i_nConn=i_TableProp[this.PNT_CESP_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PNT_CESP_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"ACSERIAL = "+cp_ToStrODBC(this.w_RIFCON);
               )
      else
        delete from (i_cTable) where;
              ACSERIAL = this.w_RIFCON;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Delete from PNT_DETT
      i_nConn=i_TableProp[this.PNT_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"PNSERIAL = "+cp_ToStrODBC(this.w_RIFCON);
               )
      else
        delete from (i_cTable) where;
              PNSERIAL = this.w_RIFCON;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Delete from PNT_MAST
      i_nConn=i_TableProp[this.PNT_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"PNSERIAL = "+cp_ToStrODBC(this.w_RIFCON);
               )
      else
        delete from (i_cTable) where;
              PNSERIAL = this.w_RIFCON;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Cicla sui Cespiti Selezionati
    * --- Select from CES_AMMO
    i_nConn=i_TableProp[this.CES_AMMO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CES_AMMO_idx,2],.t.,this.CES_AMMO_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" CES_AMMO ";
          +" where ACNUMREG="+cp_ToStrODBC(this.oParentObject.w_PANUMREG)+" AND ACCODESE="+cp_ToStrODBC(this.oParentObject.w_PACODESE)+"";
           ,"_Curs_CES_AMMO")
    else
      select * from (i_cTable);
       where ACNUMREG=this.oParentObject.w_PANUMREG AND ACCODESE=this.oParentObject.w_PACODESE;
        into cursor _Curs_CES_AMMO
    endif
    if used('_Curs_CES_AMMO')
      select _Curs_CES_AMMO
      locate for 1=1
      do while not(eof())
      this.w_MOVRIF = NVL(_Curs_CES_AMMO.ACRIFMOV, " ")
      if this.w_OK = .T. AND NOT EMPTY(this.w_MOVRIF)
        if g_COGE="S" AND this.oParentObject.w_OFLDEFI="D"
          * --- Se Definitiva cerca l'eventuale Reg.Contabile Associata e la Elimina
          this.w_RIFCON = SPACE(10)
          * --- Read from PNT_CESP
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.PNT_CESP_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PNT_CESP_idx,2],.t.,this.PNT_CESP_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ACSERIAL"+;
              " from "+i_cTable+" PNT_CESP where ";
                  +"ACMOVCES = "+cp_ToStrODBC(this.w_MOVRIF);
                  +" and ACTIPASS = "+cp_ToStrODBC("C");
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ACSERIAL;
              from (i_cTable) where;
                  ACMOVCES = this.w_MOVRIF;
                  and ACTIPASS = "C";
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_RIFCON = NVL(cp_ToDate(_read_.ACSERIAL),cp_NullValue(_read_.ACSERIAL))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if NOT EMPTY(this.w_RIFCON)
            this.Page_2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        endif
        if this.w_OK=.T.
          this.w_MCCODCES = NVL(_Curs_CES_AMMO.ACCODCES," ")
          this.w_MCIMPA07 = NVL(_Curs_CES_AMMO.ACIMPV08, 0)
          this.w_MCIMPA09 = NVL(_Curs_CES_AMMO.ACIMPV09, 0)
          this.w_MCIMPA12 = NVL(_Curs_CES_AMMO.ACIMPV10, 0)
          this.w_MCIMPA15 = NVL(_Curs_CES_AMMO.ACIMPV13,0)
          * --- Delete from MOV_CESP
          i_nConn=i_TableProp[this.MOV_CESP_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MOV_CESP_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"MCSERIAL = "+cp_ToStrODBC(this.w_MOVRIF);
                   )
          else
            delete from (i_cTable) where;
                  MCSERIAL = this.w_MOVRIF;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
          if i_Rows<>0
            * --- Se il Movimento e' gia' stato eliminato dalla manutenzione non deve stornare i Saldi...
            if NOT EMPTY(this.w_MCCODCES) AND NOT EMPTY(this.w_MCCOMPET)
              * --- Storna i Saldi
              * --- Try
              local bErr_03073C08
              bErr_03073C08=bTrsErr
              this.Try_03073C08()
              * --- Catch
              if !empty(i_Error)
                i_ErrMsg=i_Error
                i_Error=''
                * --- accept error
                bTrsErr=.f.
              endif
              bTrsErr=bTrsErr or bErr_03073C08
              * --- End
            endif
          endif
        endif
      endif
        select _Curs_CES_AMMO
        continue
      enddo
      use
    endif
    if this.w_OK=.T.
      * --- Ripristina Provvisorio
      this.oParentObject.w_PAFLDEFI = "P"
      this.oParentObject.w_OFLDEFI = "P"
      if this.pTipOpe $ "UD"
        * --- Elimina gli Ammortamenti eseguiti per il Piano Eliminato
        * --- Delete from CES_AMMO
        i_nConn=i_TableProp[this.CES_AMMO_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CES_AMMO_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"ACNUMREG = "+cp_ToStrODBC(this.oParentObject.w_PANUMREG);
                +" and ACCODESE = "+cp_ToStrODBC(this.oParentObject.w_PACODESE);
                 )
        else
          delete from (i_cTable) where;
                ACNUMREG = this.oParentObject.w_PANUMREG;
                and ACCODESE = this.oParentObject.w_PACODESE;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
      else
        * --- Nel caso l'utente abbia premuto il pulsante "Elimina" devo riportare lo
        *     stato del piano a "Provvisorio" ed inoltre devo sbiancare il campo
        *     "Acrifmov" sulla tabella "Ces_Ammo"
        * --- Write into CES_AMMO
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.CES_AMMO_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CES_AMMO_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CES_AMMO_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ACRIFMOV ="+cp_NullLink(cp_ToStrODBC(SPACE(10)),'CES_AMMO','ACRIFMOV');
              +i_ccchkf ;
          +" where ";
              +"ACNUMREG = "+cp_ToStrODBC(this.oParentObject.w_PANUMREG);
              +" and ACCODESE = "+cp_ToStrODBC(this.oParentObject.w_PACODESE);
                 )
        else
          update (i_cTable) set;
              ACRIFMOV = SPACE(10);
              &i_ccchkf. ;
           where;
              ACNUMREG = this.oParentObject.w_PANUMREG;
              and ACCODESE = this.oParentObject.w_PACODESE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Write into CES_PIAN
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.CES_PIAN_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CES_PIAN_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CES_PIAN_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PAFLDEFI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PAFLDEFI),'CES_PIAN','PAFLDEFI');
              +i_ccchkf ;
          +" where ";
              +"PANUMREG = "+cp_ToStrODBC(this.oParentObject.w_PANUMREG);
              +" and PACODESE = "+cp_ToStrODBC(this.oParentObject.w_PACODESE);
                 )
        else
          update (i_cTable) set;
              PAFLDEFI = this.oParentObject.w_PAFLDEFI;
              &i_ccchkf. ;
           where;
              PANUMREG = this.oParentObject.w_PANUMREG;
              and PACODESE = this.oParentObject.w_PACODESE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        this.oParentObject.mEnableControls()
      endif
    endif
  endproc
  proc Try_03073C08()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into SAL_CESP
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SAL_CESP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SAL_CESP_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.SAL_CESP_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"SCACCCIV =SCACCCIV- "+cp_ToStrODBC(this.w_MCIMPA07);
      +",SCACCFIS =SCACCFIS- "+cp_ToStrODBC(this.w_MCIMPA09);
      +",SCQUOPER =SCQUOPER- "+cp_ToStrODBC(this.w_MCIMPA12);
      +",SCACCANT =SCACCANT- "+cp_ToStrODBC(this.w_MCIMPA15);
          +i_ccchkf ;
      +" where ";
          +"SCCODCES = "+cp_ToStrODBC(this.w_MCCODCES);
          +" and SCCODESE = "+cp_ToStrODBC(this.w_MCCOMPET);
             )
    else
      update (i_cTable) set;
          SCACCCIV = SCACCCIV - this.w_MCIMPA07;
          ,SCACCFIS = SCACCFIS - this.w_MCIMPA09;
          ,SCQUOPER = SCQUOPER - this.w_MCIMPA12;
          ,SCACCANT = SCACCANT - this.w_MCIMPA15;
          &i_ccchkf. ;
       where;
          SCCODCES = this.w_MCCODCES;
          and SCCODESE = this.w_MCCOMPET;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pTipOpe)
    this.pTipOpe=pTipOpe
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,15)]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='CES_AMMO'
    this.cWorkTables[3]='ESERCIZI'
    this.cWorkTables[4]='MOV_CESP'
    this.cWorkTables[5]='MOVICOST'
    this.cWorkTables[6]='PNT_DETT'
    this.cWorkTables[7]='PNT_MAST'
    this.cWorkTables[8]='SAL_CESP'
    this.cWorkTables[9]='SALDICON'
    this.cWorkTables[10]='PNT_CESP'
    this.cWorkTables[11]='AMM_CESP'
    this.cWorkTables[12]='CAT_CESP'
    this.cWorkTables[13]='PAR_CESP'
    this.cWorkTables[14]='CES_PIAN'
    this.cWorkTables[15]='AMC_CESP'
    return(this.OpenAllTables(15))

  proc CloseCursors()
    if used('_Curs_GSCE_BPA')
      use in _Curs_GSCE_BPA
    endif
    if used('_Curs_AMM_CESP')
      use in _Curs_AMM_CESP
    endif
    if used('_Curs_AMC_CESP')
      use in _Curs_AMC_CESP
    endif
    if used('_Curs_PNT_CESP')
      use in _Curs_PNT_CESP
    endif
    if used('_Curs_PNT_DETT')
      use in _Curs_PNT_DETT
    endif
    if used('_Curs_CES_AMMO')
      use in _Curs_CES_AMMO
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipOpe"
endproc
