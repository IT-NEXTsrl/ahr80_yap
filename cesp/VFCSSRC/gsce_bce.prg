* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_bce                                                        *
*              Aggiornamento cespiti                                           *
*                                                                              *
*      Author: ZUCCHETTI TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_63]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-06-23                                                      *
* Last revis.: 2012-09-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsce_bce",oParentObject)
return(i_retval)

define class tgsce_bce as StdBatch
  * --- Local variables
  w_CCVALLIM = space(3)
  * --- WorkFile variables
  CAT_AMMO_idx=0
  CAT_CESP_idx=0
  VALUTE_idx=0
  PAR_CESP_idx=0
  ESERCIZI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch utilizzato per valorizzare la percentuale di deducibilitÓ ed il limite.
    * --- Verifico se l'aspetto impostato nell'anagrafica cespiti Ŕ uguale a fiscale oppure
    *     entrambi.
    if this.oParentObject.w_CETIPAMM$"EF"
      * --- Leggo dalla categoria del cespite la percentuale di deducibilitÓ,il limite e la
      *     valuta.
      * --- Read from CAT_CESP
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CAT_CESP_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAT_CESP_idx,2],.t.,this.CAT_CESP_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CCPERDEF,CCIMPMAX,CCVALLIM"+;
          " from "+i_cTable+" CAT_CESP where ";
              +"CCCODICE = "+cp_ToStrODBC(this.oParentObject.w_CECODCAT);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CCPERDEF,CCIMPMAX,CCVALLIM;
          from (i_cTable) where;
              CCCODICE = this.oParentObject.w_CECODCAT;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.oParentObject.w_CEPERDEF = NVL(cp_ToDate(_read_.CCPERDEF),cp_NullValue(_read_.CCPERDEF))
        this.oParentObject.w_CEIMPMAX = NVL(cp_ToDate(_read_.CCIMPMAX),cp_NullValue(_read_.CCIMPMAX))
        this.w_CCVALLIM = NVL(cp_ToDate(_read_.CCVALLIM),cp_NullValue(_read_.CCVALLIM))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Importo limite in altra valuta
      if this.oParentObject.w_CEIMPMAX<>0 AND this.w_CCVALLIM<>this.oParentObject.w_CEVALLIM
        this.oParentObject.w_CEIMPMAX = cp_ROUND(VALCAM(this.oParentObject.w_CEIMPMAX, this.w_CCVALLIM, this.oParentObject.w_CEVALLIM, g_FINESE, 0), g_PERPVL)
      endif
    else
      this.oParentObject.w_CEPERDEF = 0
      this.oParentObject.w_CEIMPMAX = 0
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='CAT_AMMO'
    this.cWorkTables[2]='CAT_CESP'
    this.cWorkTables[3]='VALUTE'
    this.cWorkTables[4]='PAR_CESP'
    this.cWorkTables[5]='ESERCIZI'
    return(this.OpenAllTables(5))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
