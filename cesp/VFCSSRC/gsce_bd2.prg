* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_bd2                                                        *
*              Carica dettaglio componenti                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_15]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-03-18                                                      *
* Last revis.: 2000-07-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsce_bd2",oParentObject)
return(i_retval)

define class tgsce_bd2 as StdBatch
  * --- Local variables
  w_TROV = .f.
  w_APPO = space(40)
  w_APPO1 = 0
  w_APPO2 = space(3)
  w_APPO4 = 0
  w_APPO5 = 0
  * --- WorkFile variables
  COM_PCES_idx=0
  COM_PUBI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Carica il dettaglio Componenti (da GSCE_MMS)
    this.w_TROV = .F.
    this.oParentObject.w_IMPSEL = 0
    this.oParentObject.w_IMPSEC = 0
    if NOT EMPTY(this.oParentObject.w_CODCES)
      * --- Azzera il Transitorio
      SELECT (this.oParentObject.cTrsName)
      GO TOP
      DELETE ALL
      * --- Select from COM_PCES
      i_nConn=i_TableProp[this.COM_PCES_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.COM_PCES_idx,2],.t.,this.COM_PCES_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select COCOMPON,COIMPTOT,COVALUTA,COIMPTOC  from "+i_cTable+" COM_PCES ";
            +" where COCODICE="+cp_ToStrODBC(this.oParentObject.w_CODCES)+"";
             ,"_Curs_COM_PCES")
      else
        select COCOMPON,COIMPTOT,COVALUTA,COIMPTOC from (i_cTable);
         where COCODICE=this.oParentObject.w_CODCES;
          into cursor _Curs_COM_PCES
      endif
      if used('_Curs_COM_PCES')
        select _Curs_COM_PCES
        locate for 1=1
        do while not(eof())
        if NOT EMPTY(NVL(_Curs_COM_PCES.COCOMPON," "))
          this.w_APPO = _Curs_COM_PCES.COCOMPON
          this.w_APPO1 = NVL(_Curs_COM_PCES.COIMPTOT,0)
          this.w_APPO2 = NVL(_Curs_COM_PCES.COVALUTA,"")
          this.w_APPO4 = NVL(_Curs_COM_PCES.COIMPTOC,0)
          * --- Per ogni Componente cicla sulle Matricole/Ubicazioni
          * --- Select from COM_PUBI
          i_nConn=i_TableProp[this.COM_PUBI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.COM_PUBI_idx,2],.t.,this.COM_PUBI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select * from "+i_cTable+" COM_PUBI ";
                +" where CUCODICE="+cp_ToStrODBC(this.oParentObject.w_CODCES)+" AND CUCOMPON="+cp_ToStrODBC(this.w_APPO)+"";
                 ,"_Curs_COM_PUBI")
          else
            select * from (i_cTable);
             where CUCODICE=this.oParentObject.w_CODCES AND CUCOMPON=this.w_APPO;
              into cursor _Curs_COM_PUBI
          endif
          if used('_Curs_COM_PUBI')
            select _Curs_COM_PUBI
            locate for 1=1
            do while not(eof())
            if NVL(_Curs_COM_PUBI.CPROWNUM,0)<>0 AND NVL(_Curs_COM_PUBI.CUQTACOM,0)<>0 AND EMPTY(NVL(_Curs_COM_PUBI.CUDATVEN,""))
              this.w_TROV = .T.
              * --- Inserisce Nuova Riga
              this.oParentObject.InitRow()
              this.oParentObject.w_SCCOMPON = this.w_APPO
              this.oParentObject.w_SCROWNUM = _Curs_COM_PUBI.CPROWNUM
              this.oParentObject.w_QTACOM = _Curs_COM_PUBI.CUQTACOM
              this.oParentObject.w_CODMAT = NVL(_Curs_COM_PUBI.CUCODMAT, SPACE(20))
              this.oParentObject.w_CODUBI = NVL(_Curs_COM_PUBI.CUCODUBI, SPACE(20))
              this.oParentObject.w_DTVEND = NVL(_Curs_COM_PUBI.CUDATVEN, cp_CharToDate("  -  -  "))
              this.oParentObject.w_SCFLSELE = " "
              this.oParentObject.w_VALUTA = this.w_APPO2
              if this.oParentObject.w_VALMOV<>this.oParentObject.w_VALUTA
                this.oParentObject.w_IMPTOT = cp_ROUND(VALCAM(NVL(this.w_APPO1,0), this.oParentObject.w_VALUTA, this.oParentObject.W_VALMOV, this.oParentObject.w_DATVEN, 0), this.oParentObject.W_DECTOT1)
                this.oParentObject.w_IMPTOC = cp_ROUND(VALCAM(NVL(this.w_APPO5,0), this.oParentObject.w_VALUTA, this.oParentObject.W_VALMOV, this.oParentObject.w_DATVEN, 0), this.oParentObject.W_DECTOT1)
              else
                this.oParentObject.w_IMPTOT = this.w_APPO1
                this.oParentObject.w_IMPTOC = this.w_APPO4
              endif
              * --- Aggiorna il Transitorio
              this.oParentObject.TrsFromWork()
            endif
              select _Curs_COM_PUBI
              continue
            enddo
            use
          endif
        endif
          select _Curs_COM_PCES
          continue
        enddo
        use
      endif
    endif
    if this.w_TROV=.F.
      * --- Carica Almeno la Prima Riga
      this.oParentObject.BlankRec()
    endif
    * --- Questa Parte derivata dal Metodo LoadRec
    SELECT (this.oParentObject.cTrsName)
    GO TOP
    With this.oParentObject
    .WorkFromTrs()
    .SetControlsValue()
    .ChildrenChangeRow()
    .oPgFrm.Page1.oPag.oBody.Refresh()
    .oPgFrm.Page1.oPag.oBody.nAbsRow=1
    .oPgFrm.Page1.oPag.oBody.nRelRow=1
    EndWith
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='COM_PCES'
    this.cWorkTables[2]='COM_PUBI'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_COM_PCES')
      use in _Curs_COM_PCES
    endif
    if used('_Curs_COM_PUBI')
      use in _Curs_COM_PUBI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
