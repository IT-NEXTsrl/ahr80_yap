* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_bss                                                        *
*              Stampa saldi cespiti                                            *
*                                                                              *
*      Author: TAM Software Srl                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_68]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-03-18                                                      *
* Last revis.: 2000-07-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsce_bss",oParentObject)
return(i_retval)

define class tgsce_bss as StdBatch
  * --- Local variables
  W_CODAZI = space(5)
  W_INIESE = ctod("  /  /  ")
  W_CODVAL = space(3)
  W_FINESE = ctod("  /  /  ")
  w_IMPTOT = 0
  w_IMPTOC = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampa saldi Cespiti
    *     La procedura recupera i cespiti movimentanti nell'esercizio richiesto pi�
    *     eventuali cespiti non movimentanti con saldi negli esercizi precedenti
    *     ancora attivi.
    this.W_INIESE = this.oParentObject.w_INIES
    this.W_CODAZI = this.oParentObject.W_CODAZ
    this.W_CODVAL = this.oParentObject.W_CODVA
    this.W_FINESE = this.oParentObject.W_FINES
    * --- Due Query in Union la prima recupera i cespiti con saldi nell'esercizio mentre
    *     la seconda i cepsiti che non hanno saldi ancora attivi.
    *     
    *     Entrambe utilizzano GSCE1TSC in join per determinare il totale dei saldi
    *     relativi agli esercizi precedenti.
    vq_exec("..\CESP\EXE\QUERY\GSCE1KSC.VQR",this,"mycurs_cesp")
     
 Select MYCURS_CESP 
 =WrCursor("MYCURS_CESP")
    if RecCount ("mycurs_cesp")>0
      * --- Tramuto il valore dei componenti nella valuta di stampa dei saldi
       
 Select MYCURS_CESP 
 Go Top
      Scan For COVALUTA <>this.w_CODVAL
      this.w_IMPTOT = VALCAM(NVL(COIMPTOT,0), COVALUTA, this.W_CODVAL, this.w_FINESE, 0)
      this.w_IMPTOC = VALCAM(NVL(COIMPTOC,0), COVALUTA, this.W_CODVAL, this.w_FINESE, 0)
      Replace COIMPTOT With this.w_IMPTOT
      Replace COIMPTOC With this.w_IMPTOC
      EndScan
      * --- Invio alla stampa ordinato per categoria, cespite e componente...
       
 Select * From MYCURS_CESP Order by cecodcat,ScCodCes,CoCompon Into Cursor __TMP__ NoFilter
      select("__TMP__")
      * --- Variabili usate nel report
      l_dectot=this.oParentObject.W_DECTO
      l_eserc=this.oParentObject.w_ESERC
      l_cespite=this.oParentObject.W_CESPIT
      l_catego=this.oParentObject.W_CATEGO
      l_ubicaz=this.oParentObject.W_UBICAZ
      l_staben=this.oParentObject.W_STABEN
      if this.oParentObject.W_DETRAG="D"
        CP_CHPRN("..\CESP\EXE\QUERY\GSCE_SSC.FRX", " ", this)
      else
        CP_CHPRN("..\CESP\EXE\QUERY\GSCE1SSC.FRX", " ", this)
      endif
    else
      ah_ErrorMsg("Non ci sono dati da stampare","!","")
    endif
    * --- Chiude i cursori
    if used("mycurs_cesp")
       
 Select("mycurs_cesp") 
 Use
      use
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
