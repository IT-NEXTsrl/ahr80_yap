* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_bma                                                        *
*              Verifica progressivo                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_11]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-12-03                                                      *
* Last revis.: 2004-12-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsce_bma",oParentObject,m.pOPER)
return(i_retval)

define class tgsce_bma as StdBatch
  * --- Local variables
  pOPER = space(2)
  w_PROG = 0
  w_CONTA = 0
  w_ERR = .f.
  w_MESS = space(0)
  w_MEMPROG = space(0)
  w_PADRE = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- da GSCE_MAM,GSCE_MEA
    * --- Verifica che il progressivo non abbia buchi di numerazione
    this.w_CONTA = 0
    this.w_PADRE = THIS.OPARENTOBJECT
    do case
      case this.pOPER="CT"
         
 proese="t_EAPROESE" 
 perord="t_EAPERORD" 
 perant="t_EAPERANT"
      case this.pOPER="CE"
         
 proese="t_AMPROESE" 
 perord="t_AMPERORD" 
 perant="t_AMPERANT"
    endcase
    SELECT * FROM (this.w_PADRE.cTrsName) Order By &proese INTO CURSOR CONTR
    SELECT CONTR
    GO TOP
    SCAN FOR NVL(&proese,0)>0 OR NVL(&perord,0)<>0 OR NVL(&perant,0)<>0
    this.w_CONTA = this.w_CONTA +1
    this.w_PROG = NVL(&proese,0)
    if this.w_PROG<>this.w_CONTA
      this.w_ERR = .T.
      this.w_MEMPROG = this.w_MEMPROG + " " + alltrim(STR(this.w_PROG))
    endif
    ENDSCAN
    if this.w_ERR=.T.
      if this.pOPER="CT"
        * --- Per categoria Bloccante
        this.w_MESS = "Impossibile confermare: i seguenti progressivi non sono in sequenza%0%0%1"
      else
        * --- Per cespite solo Warning
        this.w_MESS = "Attenzione: i seguenti progressivi non sono in sequenza%0%0%1"
      endif
      ah_ErrorMsg(this.w_MESS,"OK","", this.w_MEMPROG)
      if this.pOper = "CT"
        * --- transaction error
        bTrsErr=.t.
        i_TrsMsg=MSG_TRANSACTION_ERROR
        i_retcode = 'stop'
        return
      endif
    endif
    if Used("CONTR")
       
 SELECT CONTR 
 Use
    endif
  endproc


  proc Init(oParentObject,pOPER)
    this.pOPER=pOPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER"
endproc
