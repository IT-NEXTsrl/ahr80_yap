* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_bcs                                                        *
*              Cancellazione saldi                                             *
*                                                                              *
*      Author: Zucchetti spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][207]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-06-09                                                      *
* Last revis.: 2000-06-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsce_bcs",oParentObject)
return(i_retval)

define class tgsce_bcs as StdBatch
  * --- Local variables
  w_SERIAL = space(10)
  w_OK = .f.
  w_MESS = space(50)
  w_PASSO = .f.
  * --- WorkFile variables
  MOV_CESP_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch lanciato dall'anagrafica dei saldi.GSCE_ASC.
    * --- Questo batch controlla che il codice del cespite sia presente nei movimenti cespiti.
    this.w_OK = .T.
    this.w_PASSO = .F.
    * --- Select from MOV_CESP
    i_nConn=i_TableProp[this.MOV_CESP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOV_CESP_idx,2],.t.,this.MOV_CESP_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select MCSERIAL  from "+i_cTable+" MOV_CESP ";
          +" where MCCODESE="+cp_ToStrODBC(this.oParentObject.w_SCCODESE)+" AND MCCODCES="+cp_ToStrODBC(this.oParentObject.w_SCCODCES)+"";
           ,"_Curs_MOV_CESP")
    else
      select MCSERIAL from (i_cTable);
       where MCCODESE=this.oParentObject.w_SCCODESE AND MCCODCES=this.oParentObject.w_SCCODCES;
        into cursor _Curs_MOV_CESP
    endif
    if used('_Curs_MOV_CESP')
      select _Curs_MOV_CESP
      locate for 1=1
      do while not(eof())
      * --- Controllo se esiste almeno un movimento cespite
      this.w_SERIAL = _Curs_MOV_CESP.MCSERIAL
      if NOT EMPTY(NVL(this.w_SERIAL,SPACE(10)))
        this.w_MESS = "Attenzione: cespite movimentato%0Cancellare comunque?"
        this.w_PASSO = .T.
      endif
        select _Curs_MOV_CESP
        continue
      enddo
      use
    endif
    if this.w_PASSO
      this.w_OK = ah_YESNO(this.w_MESS)
      if this.w_OK=.F.
        * --- transaction error
        bTrsErr=.t.
        i_TrsMsg=MSG_TRANSACTION_ERROR
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='MOV_CESP'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_MOV_CESP')
      use in _Curs_MOV_CESP
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
