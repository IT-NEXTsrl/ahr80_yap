* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_bd3                                                        *
*              Calcola dettaglio componenti                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_37]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-03-18                                                      *
* Last revis.: 2000-07-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,PTIPO
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsce_bd3",oParentObject,m.PTIPO)
return(i_retval)

define class tgsce_bd3 as StdBatch
  * --- Local variables
  PTIPO = space(3)
  w_OREC = 0
  w_IMPORTO = 0
  w_IMPRIG = 0
  w_IMPRIGC = 0
  w_QTATOT = 0
  w_COMPON = space(40)
  w_PADRE = .NULL.
  w_OBJ = .NULL.
  w_OREC = 0
  w_nAbsRow = 0
  w_nRelRow = 0
  * --- WorkFile variables
  COM_PUBI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcola il Totale dei Componenti Selezionati (da GSCE_MMS)
    this.w_OREC = 0
    this.w_IMPORTO = 0
    this.w_IMPRIG = 0
    this.w_IMPRIGC = 0
    this.w_QTATOT = 0
    this.w_COMPON = "#####"
    do case
      case this.PTIPO="CAL"
        * --- Cicla sul temporaneo
        SELECT (this.oParentObject.cTrsName)
        this.w_OREC = RECNO()
        GO TOP
        this.oParentObject.w_IMPSEL = 0
        this.oParentObject.w_IMPSEC = 0
        SCAN FOR NOT EMPTY(NVL(t_SCCOMPON," ")) AND NVL(t_QTACOM,0)<>0 AND (NVL(t_IMPTOT,0)<>0 OR NVL(t_IMPTOC,0)<>0) AND t_SCFLSELE=1
        if t_SCCOMPON<>this.w_COMPON OR this.w_QTATOT=0
          * --- Legge il Totale del Quantita' per il Componente
          this.w_COMPON = t_SCCOMPON
          this.w_QTATOT = 0
          * --- Select from COM_PUBI
          i_nConn=i_TableProp[this.COM_PUBI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.COM_PUBI_idx,2],.t.,this.COM_PUBI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select CUQTACOM  from "+i_cTable+" COM_PUBI ";
                +" where CUCODICE="+cp_ToStrODBC(this.oParentObject.w_CODCES)+" AND CUCOMPON="+cp_ToStrODBC(this.w_COMPON)+"";
                 ,"_Curs_COM_PUBI")
          else
            select CUQTACOM from (i_cTable);
             where CUCODICE=this.oParentObject.w_CODCES AND CUCOMPON=this.w_COMPON;
              into cursor _Curs_COM_PUBI
          endif
          if used('_Curs_COM_PUBI')
            select _Curs_COM_PUBI
            locate for 1=1
            do while not(eof())
            this.w_QTATOT = this.w_QTATOT + NVL(_Curs_COM_PUBI.CUQTACOM, 0)
              select _Curs_COM_PUBI
              continue
            enddo
            use
          endif
          SELECT (this.oParentObject.cTrsName)
        endif
        this.w_IMPRIG = 0
        this.w_IMPRIGC = 0
        if this.w_QTATOT<>0 
          if t_QTACOM<>this.w_QTATOT
            this.w_IMPRIG = cp_ROUND((t_IMPTOT * t_QTACOM) / this.w_QTATOT, this.oParentObject.oParentObject.w_DECTOT)
            this.w_IMPRIGC = cp_ROUND((t_IMPTOC * t_QTACOM) / this.w_QTATOT, this.oParentObject.oParentObject.w_DECTOT)
          else
            this.w_IMPRIG = t_IMPTOT 
            this.w_IMPRIGC = t_IMPTOC
          endif
        endif
        this.oParentObject.w_IMPSEL = this.oParentObject.w_IMPSEL + this.w_IMPRIG
        this.oParentObject.w_IMPSEC = this.oParentObject.w_IMPSEC + this.w_IMPRIGC
        ENDSCAN
        if this.w_OREC<>0 AND this.w_OREC<=RECCOUNT()
          GOTO this.w_OREC
        else
          GO TOP
        endif
        * --- Totale Componenti Selezionati dai Movimenti Cespiti
        * --- Controllo se la valuta del movimento � identica a quella dei Componenti.
        this.oParentObject.oParentObject.w_IMPV02 = this.oParentObject.w_IMPSEL
        this.oParentObject.oParentObject.w_IMPV17 = this.oParentObject.w_IMPSEC
      case this.PTIPO="SEL"
        this.w_PADRE = THIS.OPARENTOBJECT
        SELECT (this.w_PADRE.cTrsName)
        this.w_OREC = IIF(Eof(),RECNO(this.w_PADRE.cTrsname)-1,RECNO(this.w_PADRE.cTrsname))
        this.w_nAbsRow = this.w_PADRE.oPgFrm.Page1.oPag.oBody.nAbsRow
        this.w_nRelRow = this.w_PADRE.oPgFrm.Page1.oPag.oBody.nRelRow
        GO TOP
        SCAN FOR NOT EMPTY(NVL( t_SCCOMPON,""))
        if this.oParentObject.w_SELEZ1="S"
          REPLACE t_SCFLSELE WITH 1 
          REPLACE t_SCROWSAL WITH t_SCROWNUM
        else
          REPLACE t_SCFLSELE WITH 0 
          REPLACE t_SCROWSAL WITH 0
        endif
        ENDSCAN
        * --- Questa Parte derivata dal Metodo LoadRec
        if this.w_OREC>0 AND this.w_OREC<=RECCOUNT()
          GOTO this.w_OREC
        else
          GO TOP
        endif
        With this.oParentObject
        .WorkFromTrs()
        .SaveDependsOn()
        .SetControlsValue()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=this.w_NABSROW
        .oPgFrm.Page1.oPag.oBody.nRelRow=this.w_NRELROW
        EndWith
        this.w_PADRE.NotifyEvent("w_SCFLSELE Changed")     
        this.w_PADRE.mCalc(.T.)     
    endcase
  endproc


  proc Init(oParentObject,PTIPO)
    this.PTIPO=PTIPO
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='COM_PUBI'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_COM_PUBI')
      use in _Curs_COM_PUBI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="PTIPO"
endproc
