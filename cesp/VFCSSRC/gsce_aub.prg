* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_aub                                                        *
*              Ubicazioni                                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-02-05                                                      *
* Last revis.: 2008-09-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsce_aub"))

* --- Class definition
define class tgsce_aub as StdForm
  Top    = 18
  Left   = 28

  * --- Standard Properties
  Width  = 427
  Height = 70+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-09-15"
  HelpContextID=76170601
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=2

  * --- Constant Properties
  UBI_CESP_IDX = 0
  cFile = "UBI_CESP"
  cKeySelect = "UBCODICE"
  cKeyWhere  = "UBCODICE=this.w_UBCODICE"
  cKeyWhereODBC = '"UBCODICE="+cp_ToStrODBC(this.w_UBCODICE)';

  cKeyWhereODBCqualified = '"UBI_CESP.UBCODICE="+cp_ToStrODBC(this.w_UBCODICE)';

  cPrg = "gsce_aub"
  cComment = "Ubicazioni"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_UBCODICE = space(20)
  w_UBDESCRI = space(40)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'UBI_CESP','gsce_aub')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsce_aubPag1","gsce_aub",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Ubicazione")
      .Pages(1).HelpContextID = 68149611
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oUBCODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='UBI_CESP'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.UBI_CESP_IDX,5],7]
    this.nPostItConn=i_TableProp[this.UBI_CESP_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_UBCODICE = NVL(UBCODICE,space(20))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from UBI_CESP where UBCODICE=KeySet.UBCODICE
    *
    i_nConn = i_TableProp[this.UBI_CESP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.UBI_CESP_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('UBI_CESP')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "UBI_CESP.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' UBI_CESP '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'UBCODICE',this.w_UBCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_UBCODICE = NVL(UBCODICE,space(20))
        .w_UBDESCRI = NVL(UBDESCRI,space(40))
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
        cp_LoadRecExtFlds(this,'UBI_CESP')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_UBCODICE = space(20)
      .w_UBDESCRI = space(40)
      if .cFunction<>"Filter"
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'UBI_CESP')
    this.DoRTCalc(1,2,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oUBCODICE_1_1.enabled = i_bVal
      .Page1.oPag.oUBDESCRI_1_2.enabled = i_bVal
      .Page1.oPag.oObj_1_5.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oUBCODICE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oUBCODICE_1_1.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'UBI_CESP',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.UBI_CESP_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UBCODICE,"UBCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UBDESCRI,"UBDESCRI",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.UBI_CESP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.UBI_CESP_IDX,2])
    i_lTable = "UBI_CESP"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.UBI_CESP_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      vx_exec("..\CESP\EXE\QUERY\GSCE_SUC.VQR",this)
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.UBI_CESP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.UBI_CESP_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.UBI_CESP_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into UBI_CESP
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'UBI_CESP')
        i_extval=cp_InsertValODBCExtFlds(this,'UBI_CESP')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(UBCODICE,UBDESCRI "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_UBCODICE)+;
                  ","+cp_ToStrODBC(this.w_UBDESCRI)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'UBI_CESP')
        i_extval=cp_InsertValVFPExtFlds(this,'UBI_CESP')
        cp_CheckDeletedKey(i_cTable,0,'UBCODICE',this.w_UBCODICE)
        INSERT INTO (i_cTable);
              (UBCODICE,UBDESCRI  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_UBCODICE;
                  ,this.w_UBDESCRI;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.UBI_CESP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.UBI_CESP_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.UBI_CESP_IDX,i_nConn)
      *
      * update UBI_CESP
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'UBI_CESP')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " UBDESCRI="+cp_ToStrODBC(this.w_UBDESCRI)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'UBI_CESP')
        i_cWhere = cp_PKFox(i_cTable  ,'UBCODICE',this.w_UBCODICE  )
        UPDATE (i_cTable) SET;
              UBDESCRI=this.w_UBDESCRI;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.UBI_CESP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.UBI_CESP_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.UBI_CESP_IDX,i_nConn)
      *
      * delete UBI_CESP
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'UBCODICE',this.w_UBCODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.UBI_CESP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.UBI_CESP_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,2,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_5.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oUBCODICE_1_1.value==this.w_UBCODICE)
      this.oPgFrm.Page1.oPag.oUBCODICE_1_1.value=this.w_UBCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oUBDESCRI_1_2.value==this.w_UBDESCRI)
      this.oPgFrm.Page1.oPag.oUBDESCRI_1_2.value=this.w_UBDESCRI
    endif
    cp_SetControlsValueExtFlds(this,'UBI_CESP')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsce_aubPag1 as StdContainer
  Width  = 423
  height = 70
  stdWidth  = 423
  stdheight = 70
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oUBCODICE_1_1 as StdField with uid="IIFJQIKUPH",rtseq=1,rtrep=.f.,;
    cFormVar = "w_UBCODICE", cQueryName = "UBCODICE",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 151597707,;
   bGlobalFont=.t.,;
    Height=21, Width=168, Left=91, Top=13, InputMask=replicate('X',20)

  add object oUBDESCRI_1_2 as StdField with uid="YPHGOHDMSY",rtseq=2,rtrep=.f.,;
    cFormVar = "w_UBDESCRI", cQueryName = "UBDESCRI",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 66011791,;
   bGlobalFont=.t.,;
    Height=21, Width=328, Left=91, Top=42, InputMask=replicate('X',40)


  add object oObj_1_5 as cp_runprogram with uid="ZSGBOEDQVX",left=1, top=84, width=232,height=25,;
    caption='GSCE_BUB',;
   bGlobalFont=.t.,;
    prg="GSCE_BUB",;
    cEvent = "Delete start",;
    nPag=1;
    , HelpContextID = 61817512

  add object oStr_1_3 as StdString with uid="NWOXURESJG",Visible=.t., Left=44, Top=13,;
    Alignment=1, Width=42, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_4 as StdString with uid="FZVLWDUSFY",Visible=.t., Left=4, Top=42,;
    Alignment=1, Width=82, Height=15,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsce_aub','UBI_CESP','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".UBCODICE=UBI_CESP.UBCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
