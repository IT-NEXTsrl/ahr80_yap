* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_ba1                                                        *
*              Controllo percentuali amm.                                      *
*                                                                              *
*      Author: ZUCCHETTI TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_6]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-01-29                                                      *
* Last revis.: 2010-05-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsce_ba1",oParentObject)
return(i_retval)

define class tgsce_ba1 as StdBatch
  * --- Local variables
  w_MESS = space(10)
  w_DAESERC = space(4)
  w_AESERC = space(4)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch lanciato dall'anagrafica Gsce_Apa (Elabora piano ammortamento).
    * --- Batch che effettua controlli sulle percentuali civili e fiscali.
    * --- Se la tipologia di ammortamento � in dietimi effettuo alcuni controlli sulle
    *     date impostate
    if this.oParentObject.w_PATIPGEN="X"
      this.w_DAESERC = calceser(this.oParentObject.w_PADATINI, space(4))
      this.w_AESERC = calceser(this.oParentObject.w_PADATFIN, space(4))
      if this.w_DAESERC<>this.oParentObject.w_PACODESE
        this.w_MESS = AH_MsgFormat("Data di inizio elaborazione non compresa nell'esercizio")
        * --- transaction error
        bTrsErr=.t.
        i_TrsMsg=this.w_Mess
        this.oParentObject.w_ELABORA = .F.
      endif
      if this.w_AESERC<>this.oParentObject.w_PACODESE
        this.w_MESS = AH_MsgFormat("Data di fine elaborazione non compresa nell'esercizio")
        * --- transaction error
        bTrsErr=.t.
        i_TrsMsg=this.w_Mess
        this.oParentObject.w_ELABORA = .F.
      endif
    endif
    if this.oParentObject.w_PACOECIV>100
      this.w_MESS = ah_MsgFormat("Percentuale ammortamento civile superiore al 100%")
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=this.w_MESS
      this.oParentObject.w_ELABORA = .F.
    else
      if this.oParentObject.w_PACOEFI1+this.oParentObject.w_PACOEFI2>100
        this.w_MESS = ah_MsgFormat("La somma delle percentuali fiscali � superiore al 100%")
        * --- transaction error
        bTrsErr=.t.
        i_TrsMsg=this.w_MESS
        this.oParentObject.w_ELABORA = .F.
      else
        if this.oParentObject.w_PACOEFI2>this.oParentObject.w_PERCANT
          this.w_MESS = ah_MsgFormat("Superato limite massimo amm. anticipato rispetto ad amm. ordinario")
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_MESS
          this.oParentObject.w_ELABORA = .F.
        else
          this.oParentObject.w_ELABORA = .T.
        endif
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
