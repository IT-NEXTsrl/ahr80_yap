* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_bre                                                        *
*              Ricostruzione saldi Lire (sal_cese)                             *
*                                                                              *
*      Author: Nicola Gorlandi                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_11]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-06-03                                                      *
* Last revis.: 2008-03-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_INIESE
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsce_bre",oParentObject,m.w_INIESE)
return(i_retval)

define class tgsce_bre as StdBatch
  * --- Local variables
  w_INIESE = ctod("  /  /  ")
  w_MCIMPA02 = 0
  w_MCIMPA03 = 0
  w_MCIMPA04 = 0
  w_MCIMPA05 = 0
  w_MCIMPA06 = 0
  w_MCIMPA07 = 0
  w_MCIMPA08 = 0
  w_MCIMPA09 = 0
  w_MCIMPA10 = 0
  w_MCIMPA11 = 0
  w_MCIMPA12 = 0
  w_MCIMPA13 = 0
  w_MCIMPA14 = 0
  w_MCIMPA15 = 0
  w_MCIMPA16 = 0
  w_MCIMPA18 = 0
  w_MCIMPA19 = 0
  w_MCIMPA22 = 0
  w_MCIMPA23 = 0
  w_MCIMPA24 = 0
  w_MCIMPA20 = 0
  w_MCIMPA21 = 0
  w_CODVAL = space(3)
  w_VALFIN = space(3)
  w_DATFIN = ctod("  /  /  ")
  w_DECTOT = 0
  w_SCCODCES = space(15)
  w_CODCES = space(20)
  * --- WorkFile variables
  SAL_CESE_idx=0
  VALUTE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_CODVAL = g_CODLIR
    * --- Write into SAL_CESE
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SAL_CESE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SAL_CESE_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.SAL_CESE_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"SCINCVAL ="+cp_NullLink(cp_ToStrODBC(0),'SAL_CESE','SCINCVAL');
      +",SCIMPONS ="+cp_NullLink(cp_ToStrODBC(0),'SAL_CESE','SCIMPONS');
      +",SCIMPRIV ="+cp_NullLink(cp_ToStrODBC(0),'SAL_CESE','SCIMPRIV');
      +",SCDECVAL ="+cp_NullLink(cp_ToStrODBC(0),'SAL_CESE','SCDECVAL');
      +",SCIMPSVA ="+cp_NullLink(cp_ToStrODBC(0),'SAL_CESE','SCIMPSVA');
      +",SCACCCIV ="+cp_NullLink(cp_ToStrODBC(0),'SAL_CESE','SCACCCIV');
      +",SCUTICIV ="+cp_NullLink(cp_ToStrODBC(0),'SAL_CESE','SCUTICIV');
      +",SCACCFIS ="+cp_NullLink(cp_ToStrODBC(0),'SAL_CESE','SCACCFIS');
      +",SCUTIFIS ="+cp_NullLink(cp_ToStrODBC(0),'SAL_CESE','SCUTIFIS');
      +",SCIMPNOA ="+cp_NullLink(cp_ToStrODBC(0),'SAL_CESE','SCIMPNOA');
      +",SCQUOPER ="+cp_NullLink(cp_ToStrODBC(0),'SAL_CESE','SCQUOPER');
      +",SCACCANT ="+cp_NullLink(cp_ToStrODBC(0),'SAL_CESE','SCACCANT');
      +",SCUTIANT ="+cp_NullLink(cp_ToStrODBC(0),'SAL_CESE','SCUTIANT');
      +",SCIMPRIC ="+cp_NullLink(cp_ToStrODBC(0),'SAL_CESE','SCIMPRIC');
      +",SCIMPSVC ="+cp_NullLink(cp_ToStrODBC(0),'SAL_CESE','SCIMPSVC');
      +",SCDECVAC ="+cp_NullLink(cp_ToStrODBC(0),'SAL_CESE','SCDECVAC');
      +",SCIMPONC ="+cp_NullLink(cp_ToStrODBC(0),'SAL_CESE','SCIMPONC');
      +",SCINCVAC ="+cp_NullLink(cp_ToStrODBC(0),'SAL_CESE','SCINCVAC');
          +i_ccchkf ;
      +" where ";
          +"1 = "+cp_ToStrODBC(1);
             )
    else
      update (i_cTable) set;
          SCINCVAL = 0;
          ,SCIMPONS = 0;
          ,SCIMPRIV = 0;
          ,SCDECVAL = 0;
          ,SCIMPSVA = 0;
          ,SCACCCIV = 0;
          ,SCUTICIV = 0;
          ,SCACCFIS = 0;
          ,SCUTIFIS = 0;
          ,SCIMPNOA = 0;
          ,SCQUOPER = 0;
          ,SCACCANT = 0;
          ,SCUTIANT = 0;
          ,SCIMPRIC = 0;
          ,SCIMPSVC = 0;
          ,SCDECVAC = 0;
          ,SCIMPONC = 0;
          ,SCINCVAC = 0;
          &i_ccchkf. ;
       where;
          1 = 1;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- la query GSCE_TSC � utilizzata anche in GSCE_BSC.
    *     In questo batch deve comprendere anche il saldo dell'ultimo
    *     esercizio in lire mentre in GSCE_BSC no.
    this.w_INIESE = this.w_INIESE + 1
    * --- Variabile bianca da passare alla query GSCE_TSC
    * --- Leggo decimali Euro...
    this.w_VALFIN = GETVALUT(this.w_CODVAL, "VAUNIVAL")
    this.w_DATFIN = GETVALUT(this.w_CODVAL, "VADATEUR")
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VADECTOT"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(this.w_VALFIN);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VADECTOT;
        from (i_cTable) where;
            VACODVAL = this.w_VALFIN;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DECTOT = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Riempio il saldo con il totale dei saldi precedenti....
    * --- Select from GSCE_TSC
    do vq_exec with 'GSCE_TSC',this,'_Curs_GSCE_TSC','',.f.,.t.
    if used('_Curs_GSCE_TSC')
      select _Curs_GSCE_TSC
      locate for 1=1
      do while not(eof())
      this.w_MCIMPA02 = cp_Round ( VALCAM(NVL(_Curs_GSCE_TSC.SCINCVAL,0), this.w_CODVAL , this.w_VALFIN , this.w_DATFIN , 0) , this.w_DECTOT )
      this.w_MCIMPA03 = cp_Round ( VALCAM(NVL(_Curs_GSCE_TSC.SCIMPONS,0), this.w_CODVAL , this.w_VALFIN , this.w_DATFIN , 0) , this.w_DECTOT )
      this.w_MCIMPA04 = cp_Round ( VALCAM(NVL(_Curs_GSCE_TSC.SCIMPRIV,0), this.w_CODVAL , this.w_VALFIN , this.w_DATFIN , 0) , this.w_DECTOT )
      this.w_MCIMPA05 = cp_Round ( VALCAM(NVL(_Curs_GSCE_TSC.SCDECVAL,0), this.w_CODVAL , this.w_VALFIN , this.w_DATFIN , 0) , this.w_DECTOT )
      this.w_MCIMPA06 = cp_Round ( VALCAM(NVL(_Curs_GSCE_TSC.SCIMPSVA,0), this.w_CODVAL , this.w_VALFIN , this.w_DATFIN , 0) , this.w_DECTOT )
      this.w_MCIMPA07 = cp_Round ( VALCAM(NVL(_Curs_GSCE_TSC.SCACCCIV,0), this.w_CODVAL , this.w_VALFIN , this.w_DATFIN , 0) , this.w_DECTOT )
      this.w_MCIMPA08 = cp_Round ( VALCAM(NVL(_Curs_GSCE_TSC.SCUTICIV,0), this.w_CODVAL , this.w_VALFIN , this.w_DATFIN , 0) , this.w_DECTOT )
      this.w_MCIMPA09 = cp_Round ( VALCAM(NVL(_Curs_GSCE_TSC.SCACCFIS,0), this.w_CODVAL , this.w_VALFIN , this.w_DATFIN , 0) , this.w_DECTOT )
      this.w_MCIMPA10 = cp_Round ( VALCAM(NVL(_Curs_GSCE_TSC.SCUTIFIS,0), this.w_CODVAL , this.w_VALFIN , this.w_DATFIN , 0) , this.w_DECTOT )
      this.w_MCIMPA11 = cp_Round ( VALCAM(NVL(_Curs_GSCE_TSC.SCIMPNOA,0), this.w_CODVAL , this.w_VALFIN , this.w_DATFIN , 0) , this.w_DECTOT )
      this.w_MCIMPA12 = cp_Round ( VALCAM(NVL(_Curs_GSCE_TSC.SCQUOPER,0), this.w_CODVAL , this.w_VALFIN , this.w_DATFIN , 0) , this.w_DECTOT )
      this.w_MCIMPA18 = cp_Round ( VALCAM(NVL(_Curs_GSCE_TSC.SCIMPRIC,0), this.w_CODVAL , this.w_VALFIN , this.w_DATFIN , 0) , this.w_DECTOT )
      this.w_MCIMPA19 = cp_Round ( VALCAM(NVL(_Curs_GSCE_TSC.SCIMPSVC,0), this.w_CODVAL , this.w_VALFIN , this.w_DATFIN , 0) , this.w_DECTOT )
      this.w_MCIMPA22 = cp_Round ( VALCAM(NVL(_Curs_GSCE_TSC.SCDECVAC,0), this.w_CODVAL , this.w_VALFIN , this.w_DATFIN , 0) , this.w_DECTOT )
      this.w_MCIMPA23 = cp_Round ( VALCAM(NVL(_Curs_GSCE_TSC.SCINCVAC,0), this.w_CODVAL , this.w_VALFIN , this.w_DATFIN , 0) , this.w_DECTOT )
      this.w_MCIMPA24 = cp_Round ( VALCAM(NVL(_Curs_GSCE_TSC.SCIMPONC,0), this.w_CODVAL , this.w_VALFIN , this.w_DATFIN , 0) , this.w_DECTOT )
      * --- Minusvalenza e plusvalenza non sono memorizzate
      this.w_MCIMPA13 = 0
      this.w_MCIMPA14 = 0
      this.w_MCIMPA20 = 0
      this.w_MCIMPA21 = 0
      this.w_MCIMPA15 = cp_Round ( VALCAM(NVL(_Curs_GSCE_TSC.SCACCANT,0), this.w_CODVAL , this.w_VALFIN , this.w_DATFIN , 0) , this.w_DECTOT )
      this.w_MCIMPA16 = cp_Round ( VALCAM(NVL(_Curs_GSCE_TSC.SCUTIANT,0), this.w_CODVAL , this.w_VALFIN , this.w_DATFIN , 0) , this.w_DECTOT )
      this.w_CODCES = _Curs_GSCE_TSC.SCCODCES
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
        select _Curs_GSCE_TSC
        continue
      enddo
      use
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Riempio anagrafica riassunto saldi in Lire...
    * --- Try
    local bErr_03918F70
    bErr_03918F70=bTrsErr
    this.Try_03918F70()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
      * --- Try
      local bErr_039109C0
      bErr_039109C0=bTrsErr
      this.Try_039109C0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- Raise
        i_Error="Errore scrittura archivio saldi cespiti - operazione annullata -"
        return
      endif
      bTrsErr=bTrsErr or bErr_039109C0
      * --- End
    endif
    bTrsErr=bTrsErr or bErr_03918F70
    * --- End
  endproc
  proc Try_03918F70()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SAL_CESE
    i_nConn=i_TableProp[this.SAL_CESE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SAL_CESE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SAL_CESE_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SCCODCES"+",SCINCVAL"+",SCIMPONS"+",SCIMPRIV"+",SCDECVAL"+",SCIMPSVA"+",SCACCCIV"+",SCUTICIV"+",SCACCFIS"+",SCUTIFIS"+",SCIMPNOA"+",SCQUOPER"+",SCACCANT"+",SCUTIANT"+",SCIMPRIC"+",SCIMPSVC"+",SCDECVAC"+",SCIMPONC"+",SCINCVAC"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CODCES),'SAL_CESE','SCCODCES');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MCIMPA02),'SAL_CESE','SCINCVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MCIMPA03),'SAL_CESE','SCIMPONS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MCIMPA04),'SAL_CESE','SCIMPRIV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MCIMPA05),'SAL_CESE','SCDECVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MCIMPA06),'SAL_CESE','SCIMPSVA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MCIMPA07),'SAL_CESE','SCACCCIV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MCIMPA08),'SAL_CESE','SCUTICIV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MCIMPA09),'SAL_CESE','SCACCFIS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MCIMPA10),'SAL_CESE','SCUTIFIS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MCIMPA11),'SAL_CESE','SCIMPNOA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MCIMPA12),'SAL_CESE','SCQUOPER');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MCIMPA15),'SAL_CESE','SCACCANT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MCIMPA16),'SAL_CESE','SCUTIANT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MCIMPA18),'SAL_CESE','SCIMPRIC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MCIMPA19),'SAL_CESE','SCIMPSVC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MCIMPA22),'SAL_CESE','SCDECVAC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MCIMPA24),'SAL_CESE','SCIMPONC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MCIMPA23),'SAL_CESE','SCINCVAC');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SCCODCES',this.w_CODCES,'SCINCVAL',this.w_MCIMPA02,'SCIMPONS',this.w_MCIMPA03,'SCIMPRIV',this.w_MCIMPA04,'SCDECVAL',this.w_MCIMPA05,'SCIMPSVA',this.w_MCIMPA06,'SCACCCIV',this.w_MCIMPA07,'SCUTICIV',this.w_MCIMPA08,'SCACCFIS',this.w_MCIMPA09,'SCUTIFIS',this.w_MCIMPA10,'SCIMPNOA',this.w_MCIMPA11,'SCQUOPER',this.w_MCIMPA12)
      insert into (i_cTable) (SCCODCES,SCINCVAL,SCIMPONS,SCIMPRIV,SCDECVAL,SCIMPSVA,SCACCCIV,SCUTICIV,SCACCFIS,SCUTIFIS,SCIMPNOA,SCQUOPER,SCACCANT,SCUTIANT,SCIMPRIC,SCIMPSVC,SCDECVAC,SCIMPONC,SCINCVAC &i_ccchkf. );
         values (;
           this.w_CODCES;
           ,this.w_MCIMPA02;
           ,this.w_MCIMPA03;
           ,this.w_MCIMPA04;
           ,this.w_MCIMPA05;
           ,this.w_MCIMPA06;
           ,this.w_MCIMPA07;
           ,this.w_MCIMPA08;
           ,this.w_MCIMPA09;
           ,this.w_MCIMPA10;
           ,this.w_MCIMPA11;
           ,this.w_MCIMPA12;
           ,this.w_MCIMPA15;
           ,this.w_MCIMPA16;
           ,this.w_MCIMPA18;
           ,this.w_MCIMPA19;
           ,this.w_MCIMPA22;
           ,this.w_MCIMPA24;
           ,this.w_MCIMPA23;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_039109C0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into SAL_CESE
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SAL_CESE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SAL_CESE_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.SAL_CESE_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"SCINCVAL =SCINCVAL+ "+cp_ToStrODBC(this.w_MCIMPA02);
      +",SCIMPONS =SCIMPONS+ "+cp_ToStrODBC(this.w_MCIMPA03);
      +",SCIMPRIV =SCIMPRIV+ "+cp_ToStrODBC(this.w_MCIMPA04);
      +",SCDECVAL =SCDECVAL+ "+cp_ToStrODBC(this.w_MCIMPA05);
      +",SCIMPSVA =SCIMPSVA+ "+cp_ToStrODBC(this.w_MCIMPA06);
      +",SCACCCIV =SCACCCIV+ "+cp_ToStrODBC(this.w_MCIMPA07);
      +",SCUTICIV =SCUTICIV+ "+cp_ToStrODBC(this.w_MCIMPA08);
      +",SCACCFIS =SCACCFIS+ "+cp_ToStrODBC(this.w_MCIMPA09);
      +",SCUTIFIS =SCUTIFIS+ "+cp_ToStrODBC(this.w_MCIMPA10);
      +",SCIMPNOA =SCIMPNOA+ "+cp_ToStrODBC(this.w_MCIMPA11);
      +",SCQUOPER =SCQUOPER+ "+cp_ToStrODBC(this.w_MCIMPA12);
      +",SCACCANT =SCACCANT+ "+cp_ToStrODBC(this.w_MCIMPA15);
      +",SCUTIANT =SCUTIANT+ "+cp_ToStrODBC(this.w_MCIMPA16);
      +",SCIMPRIC =SCIMPRIC+ "+cp_ToStrODBC(this.w_MCIMPA18);
      +",SCIMPSVC =SCIMPSVC+ "+cp_ToStrODBC(this.w_MCIMPA19);
      +",SCDECVAC =SCDECVAC+ "+cp_ToStrODBC(this.w_MCIMPA22);
      +",SCIMPONC =SCIMPONC+ "+cp_ToStrODBC(this.w_MCIMPA24);
      +",SCINCVAC =SCINCVAC+ "+cp_ToStrODBC(this.w_MCIMPA23);
          +i_ccchkf ;
      +" where ";
          +"SCCODCES = "+cp_ToStrODBC(this.w_CODCES);
             )
    else
      update (i_cTable) set;
          SCINCVAL = SCINCVAL + this.w_MCIMPA02;
          ,SCIMPONS = SCIMPONS + this.w_MCIMPA03;
          ,SCIMPRIV = SCIMPRIV + this.w_MCIMPA04;
          ,SCDECVAL = SCDECVAL + this.w_MCIMPA05;
          ,SCIMPSVA = SCIMPSVA + this.w_MCIMPA06;
          ,SCACCCIV = SCACCCIV + this.w_MCIMPA07;
          ,SCUTICIV = SCUTICIV + this.w_MCIMPA08;
          ,SCACCFIS = SCACCFIS + this.w_MCIMPA09;
          ,SCUTIFIS = SCUTIFIS + this.w_MCIMPA10;
          ,SCIMPNOA = SCIMPNOA + this.w_MCIMPA11;
          ,SCQUOPER = SCQUOPER + this.w_MCIMPA12;
          ,SCACCANT = SCACCANT + this.w_MCIMPA15;
          ,SCUTIANT = SCUTIANT + this.w_MCIMPA16;
          ,SCIMPRIC = SCIMPRIC + this.w_MCIMPA18;
          ,SCIMPSVC = SCIMPSVC + this.w_MCIMPA19;
          ,SCDECVAC = SCDECVAC + this.w_MCIMPA22;
          ,SCIMPONC = SCIMPONC + this.w_MCIMPA24;
          ,SCINCVAC = SCINCVAC + this.w_MCIMPA23;
          &i_ccchkf. ;
       where;
          SCCODCES = this.w_CODCES;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  proc Init(oParentObject,w_INIESE)
    this.w_INIESE=w_INIESE
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='SAL_CESE'
    this.cWorkTables[2]='VALUTE'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_GSCE_TSC')
      use in _Curs_GSCE_TSC
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_INIESE"
endproc
