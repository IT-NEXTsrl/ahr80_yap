* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_acc                                                        *
*              Causali cespiti                                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30] [VRS_52]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-02-23                                                      *
* Last revis.: 2012-11-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- gsce_acc
L_Descri3='Utilizzazioni Ordinarie'
L_Descri4='Utilizzazioni Anticipate'
* --- Fine Area Manuale
return(createobject("tgsce_acc"))

* --- Class definition
define class tgsce_acc as StdForm
  Top    = 1
  Left   = 4

  * --- Standard Properties
  Width  = 769
  Height = 474+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-11-05"
  HelpContextID=109725033
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=52

  * --- Constant Properties
  CAU_CESP_IDX = 0
  CAU_CONT_IDX = 0
  FOR_CESP_IDX = 0
  MCO_CESP_IDX = 0
  PAR_CESP_IDX = 0
  cFile = "CAU_CESP"
  cKeySelect = "CCCODICE"
  cKeyWhere  = "CCCODICE=this.w_CCCODICE"
  cKeyWhereODBC = '"CCCODICE="+cp_ToStrODBC(this.w_CCCODICE)';

  cKeyWhereODBCqualified = '"CAU_CESP.CCCODICE="+cp_ToStrODBC(this.w_CCCODICE)';

  cPrg = "gsce_acc"
  cComment = "Causali cespiti"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CODAZI = space(5)
  w_TIPAMM = space(1)
  w_CCCODICE = space(5)
  w_CCDESCRI = space(40)
  w_CCFLRIFE = space(1)
  w_CCFLRAGG = space(1)
  w_CCFLGA01 = space(1)
  w_CCFLPRIU = space(1)
  w_CCFLDADI = space(1)
  w_CCFLGA23 = space(1)
  w_CCFLGA24 = space(1)
  w_CCFLGA18 = space(1)
  w_CCFLONFI = space(1)
  w_CCFLGA22 = space(1)
  w_CCFLGA19 = space(1)
  w_CCFLGA20 = space(1)
  w_CCFLGA21 = space(1)
  w_CCFLGA07 = space(1)
  w_CCFLGA08 = space(1)
  w_CCFLGA17 = space(1)
  w_CCFLGOPE = space(1)
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_OBTEST = ctod('  /  /  ')
  w_CODI = space(5)
  w_DESC = space(40)
  w_CCDATAGG = space(1)
  w_CODI = space(5)
  w_DESC = space(40)
  w_CCFLCONT = space(1)
  o_CCFLCONT = space(1)
  w_CCCAUCON = space(5)
  w_DESCAU = space(35)
  w_CCRIVALU = space(1)
  w_CCFLGA03 = space(1)
  w_CCFLGA02 = space(1)
  w_CCFLGA04 = space(1)
  o_CCFLGA04 = space(1)
  w_CCFLGA05 = space(1)
  w_CCFLGA06 = space(1)
  w_CCFLONFF = space(1)
  w_CCFLGA11 = space(1)
  w_CCFLGA12 = space(1)
  w_CCFLGA09 = space(1)
  w_CCFLGA15 = space(1)
  w_CCFLPRIC = space(1)
  w_CCFLGA10 = space(1)
  w_CCFLGA13 = space(1)
  w_CCFLGA16 = space(1)
  w_CCFLGA14 = space(1)
  w_CCDTINVA = ctod('  /  /  ')
  w_CCDTOBSO = ctod('  /  /  ')

  * --- Children pointers
  GSCE_MFO = .NULL.
  GSCE_MOC = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=4, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'CAU_CESP','gsce_acc')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsce_accPag1","gsce_acc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Causale")
      .Pages(1).HelpContextID = 201355302
      .Pages(2).addobject("oPag","tgsce_accPag2","gsce_acc",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Formule")
      .Pages(2).HelpContextID = 221924950
      .Pages(3).addobject("oPag","tgsce_accPag3","gsce_acc",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Modelli")
      .Pages(3).HelpContextID = 56529210
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCCCODICE_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='CAU_CONT'
    this.cWorkTables[2]='FOR_CESP'
    this.cWorkTables[3]='MCO_CESP'
    this.cWorkTables[4]='PAR_CESP'
    this.cWorkTables[5]='CAU_CESP'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(5))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CAU_CESP_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CAU_CESP_IDX,3]
  return

  function CreateChildren()
    this.GSCE_MFO = CREATEOBJECT('stdDynamicChild',this,'GSCE_MFO',this.oPgFrm.Page2.oPag.oLinkPC_2_4)
    this.GSCE_MFO.createrealchild()
    this.GSCE_MOC = CREATEOBJECT('stdDynamicChild',this,'GSCE_MOC',this.oPgFrm.Page3.oPag.oLinkPC_3_8)
    this.GSCE_MOC.createrealchild()
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSCE_MFO)
      this.GSCE_MFO.DestroyChildrenChain()
      this.GSCE_MFO=.NULL.
    endif
    this.oPgFrm.Page2.oPag.RemoveObject('oLinkPC_2_4')
    if !ISNULL(this.GSCE_MOC)
      this.GSCE_MOC.DestroyChildrenChain()
      this.GSCE_MOC=.NULL.
    endif
    this.oPgFrm.Page3.oPag.RemoveObject('oLinkPC_3_8')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSCE_MFO.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSCE_MOC.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSCE_MFO.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSCE_MOC.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSCE_MFO.NewDocument()
    this.GSCE_MOC.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSCE_MFO.SetKey(;
            .w_CCCODICE,"FOCODCAU";
            )
      this.GSCE_MOC.SetKey(;
            .w_CCCODICE,"MCCODCAU";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSCE_MFO.ChangeRow(this.cRowID+'      1',1;
             ,.w_CCCODICE,"FOCODCAU";
             )
      .GSCE_MOC.ChangeRow(this.cRowID+'      1',1;
             ,.w_CCCODICE,"MCCODCAU";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSCE_MFO)
        i_f=.GSCE_MFO.BuildFilter()
        if !(i_f==.GSCE_MFO.cQueryFilter)
          i_fnidx=.GSCE_MFO.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSCE_MFO.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSCE_MFO.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSCE_MFO.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSCE_MFO.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSCE_MOC)
        i_f=.GSCE_MOC.BuildFilter()
        if !(i_f==.GSCE_MOC.cQueryFilter)
          i_fnidx=.GSCE_MOC.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSCE_MOC.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSCE_MOC.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSCE_MOC.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSCE_MOC.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_CCCODICE = NVL(CCCODICE,space(5))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_3_5_joined
    link_3_5_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from CAU_CESP where CCCODICE=KeySet.CCCODICE
    *
    i_nConn = i_TableProp[this.CAU_CESP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CESP_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CAU_CESP')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CAU_CESP.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' CAU_CESP '
      link_3_5_joined=this.AddJoinedLink_3_5(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'CCCODICE',this.w_CCCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_CODAZI = i_CODAZI
        .w_TIPAMM = space(1)
        .w_OBTEST = i_datsys
        .w_DESCAU = space(35)
          .link_1_1('Load')
        .w_CCCODICE = NVL(CCCODICE,space(5))
        .w_CCDESCRI = NVL(CCDESCRI,space(40))
        .w_CCFLRIFE = NVL(CCFLRIFE,space(1))
        .w_CCFLRAGG = NVL(CCFLRAGG,space(1))
        .w_CCFLGA01 = NVL(CCFLGA01,space(1))
        .w_CCFLPRIU = NVL(CCFLPRIU,space(1))
        .w_CCFLDADI = NVL(CCFLDADI,space(1))
        .w_CCFLGA23 = NVL(CCFLGA23,space(1))
        .w_CCFLGA24 = NVL(CCFLGA24,space(1))
        .w_CCFLGA18 = NVL(CCFLGA18,space(1))
        .w_CCFLONFI = NVL(CCFLONFI,space(1))
        .w_CCFLGA22 = NVL(CCFLGA22,space(1))
        .w_CCFLGA19 = NVL(CCFLGA19,space(1))
        .w_CCFLGA20 = NVL(CCFLGA20,space(1))
        .w_CCFLGA21 = NVL(CCFLGA21,space(1))
        .w_CCFLGA07 = NVL(CCFLGA07,space(1))
        .w_CCFLGA08 = NVL(CCFLGA08,space(1))
        .w_CCFLGA17 = NVL(CCFLGA17,space(1))
        .w_CCFLGOPE = NVL(CCFLGOPE,space(1))
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
        .w_CODI = .w_CCCODICE
        .w_DESC = .w_CCDESCRI
        .w_CCDATAGG = NVL(CCDATAGG,space(1))
        .w_CODI = .w_CCCODICE
        .w_DESC = .w_CCDESCRI
        .w_CCFLCONT = NVL(CCFLCONT,space(1))
        .w_CCCAUCON = NVL(CCCAUCON,space(5))
          if link_3_5_joined
            this.w_CCCAUCON = NVL(CCCODICE305,NVL(this.w_CCCAUCON,space(5)))
            this.w_DESCAU = NVL(CCDESCRI305,space(35))
          else
          .link_3_5('Load')
          endif
        .w_CCRIVALU = NVL(CCRIVALU,space(1))
        .w_CCFLGA03 = NVL(CCFLGA03,space(1))
        .w_CCFLGA02 = NVL(CCFLGA02,space(1))
        .w_CCFLGA04 = NVL(CCFLGA04,space(1))
        .w_CCFLGA05 = NVL(CCFLGA05,space(1))
        .w_CCFLGA06 = NVL(CCFLGA06,space(1))
        .w_CCFLONFF = NVL(CCFLONFF,space(1))
        .w_CCFLGA11 = NVL(CCFLGA11,space(1))
        .w_CCFLGA12 = NVL(CCFLGA12,space(1))
        .w_CCFLGA09 = NVL(CCFLGA09,space(1))
        .w_CCFLGA15 = NVL(CCFLGA15,space(1))
        .w_CCFLPRIC = NVL(CCFLPRIC,space(1))
        .w_CCFLGA10 = NVL(CCFLGA10,space(1))
        .w_CCFLGA13 = NVL(CCFLGA13,space(1))
        .w_CCFLGA16 = NVL(CCFLGA16,space(1))
        .w_CCFLGA14 = NVL(CCFLGA14,space(1))
        .w_CCDTINVA = NVL(cp_ToDate(CCDTINVA),ctod("  /  /  "))
        .w_CCDTOBSO = NVL(cp_ToDate(CCDTOBSO),ctod("  /  /  "))
        .oPgFrm.Page1.oPag.oObj_1_60.Calculate()
        cp_LoadRecExtFlds(this,'CAU_CESP')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI = space(5)
      .w_TIPAMM = space(1)
      .w_CCCODICE = space(5)
      .w_CCDESCRI = space(40)
      .w_CCFLRIFE = space(1)
      .w_CCFLRAGG = space(1)
      .w_CCFLGA01 = space(1)
      .w_CCFLPRIU = space(1)
      .w_CCFLDADI = space(1)
      .w_CCFLGA23 = space(1)
      .w_CCFLGA24 = space(1)
      .w_CCFLGA18 = space(1)
      .w_CCFLONFI = space(1)
      .w_CCFLGA22 = space(1)
      .w_CCFLGA19 = space(1)
      .w_CCFLGA20 = space(1)
      .w_CCFLGA21 = space(1)
      .w_CCFLGA07 = space(1)
      .w_CCFLGA08 = space(1)
      .w_CCFLGA17 = space(1)
      .w_CCFLGOPE = space(1)
      .w_UTCC = 0
      .w_UTCV = 0
      .w_UTDC = ctot("")
      .w_UTDV = ctot("")
      .w_OBTEST = ctod("  /  /  ")
      .w_CODI = space(5)
      .w_DESC = space(40)
      .w_CCDATAGG = space(1)
      .w_CODI = space(5)
      .w_DESC = space(40)
      .w_CCFLCONT = space(1)
      .w_CCCAUCON = space(5)
      .w_DESCAU = space(35)
      .w_CCRIVALU = space(1)
      .w_CCFLGA03 = space(1)
      .w_CCFLGA02 = space(1)
      .w_CCFLGA04 = space(1)
      .w_CCFLGA05 = space(1)
      .w_CCFLGA06 = space(1)
      .w_CCFLONFF = space(1)
      .w_CCFLGA11 = space(1)
      .w_CCFLGA12 = space(1)
      .w_CCFLGA09 = space(1)
      .w_CCFLGA15 = space(1)
      .w_CCFLPRIC = space(1)
      .w_CCFLGA10 = space(1)
      .w_CCFLGA13 = space(1)
      .w_CCFLGA16 = space(1)
      .w_CCFLGA14 = space(1)
      .w_CCDTINVA = ctod("  /  /  ")
      .w_CCDTOBSO = ctod("  /  /  ")
      if .cFunction<>"Filter"
        .w_CODAZI = i_CODAZI
        .DoRTCalc(1,1,.f.)
          if not(empty(.w_CODAZI))
          .link_1_1('Full')
          endif
          .DoRTCalc(2,4,.f.)
        .w_CCFLRIFE = 'N'
        .w_CCFLRAGG = 'N'
          .DoRTCalc(7,20,.f.)
        .w_CCFLGOPE = '+'
          .DoRTCalc(22,25,.f.)
        .w_OBTEST = i_datsys
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
        .w_CODI = .w_CCCODICE
        .w_DESC = .w_CCDESCRI
        .w_CCDATAGG = 'U'
        .w_CODI = .w_CCCODICE
        .w_DESC = .w_CCDESCRI
        .w_CCFLCONT = 'N'
        .w_CCCAUCON = IIF(.w_CCFLCONT<>'N', .w_CCCAUCON, '')
        .DoRTCalc(33,33,.f.)
          if not(empty(.w_CCCAUCON))
          .link_3_5('Full')
          endif
          .DoRTCalc(34,34,.f.)
        .w_CCRIVALU = 'C'
        .oPgFrm.Page1.oPag.oObj_1_60.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'CAU_CESP')
    this.DoRTCalc(36,52,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oCCCODICE_1_3.enabled = i_bVal
      .Page1.oPag.oCCDESCRI_1_4.enabled = i_bVal
      .Page1.oPag.oCCFLRIFE_1_5.enabled = i_bVal
      .Page1.oPag.oCCFLRAGG_1_6.enabled = i_bVal
      .Page1.oPag.oCCFLGA01_1_7.enabled = i_bVal
      .Page1.oPag.oCCFLPRIU_1_8.enabled = i_bVal
      .Page1.oPag.oCCFLDADI_1_9.enabled = i_bVal
      .Page1.oPag.oCCFLGA23_1_12.enabled = i_bVal
      .Page1.oPag.oCCFLGA24_1_13.enabled = i_bVal
      .Page1.oPag.oCCFLGA18_1_14.enabled = i_bVal
      .Page1.oPag.oCCFLONFI_1_15.enabled = i_bVal
      .Page1.oPag.oCCFLGA22_1_16.enabled = i_bVal
      .Page1.oPag.oCCFLGA19_1_17.enabled = i_bVal
      .Page1.oPag.oCCFLGA20_1_18.enabled = i_bVal
      .Page1.oPag.oCCFLGA21_1_19.enabled = i_bVal
      .Page1.oPag.oCCFLGA07_1_20.enabled = i_bVal
      .Page1.oPag.oCCFLGA08_1_21.enabled = i_bVal
      .Page1.oPag.oCCFLGA17_1_22.enabled = i_bVal
      .Page1.oPag.oCCFLGOPE_1_23.enabled = i_bVal
      .Page2.oPag.oCCDATAGG_2_3.enabled = i_bVal
      .Page3.oPag.oCCFLCONT_3_3.enabled_(i_bVal)
      .Page3.oPag.oCCCAUCON_3_5.enabled = i_bVal
      .Page1.oPag.oCCFLGA03_1_39.enabled = i_bVal
      .Page1.oPag.oCCFLGA02_1_40.enabled = i_bVal
      .Page1.oPag.oCCFLGA04_1_41.enabled = i_bVal
      .Page1.oPag.oCCFLGA05_1_42.enabled = i_bVal
      .Page1.oPag.oCCFLGA06_1_43.enabled = i_bVal
      .Page1.oPag.oCCFLONFF_1_44.enabled = i_bVal
      .Page1.oPag.oCCFLGA11_1_45.enabled = i_bVal
      .Page1.oPag.oCCFLGA12_1_47.enabled = i_bVal
      .Page1.oPag.oCCFLGA09_1_49.enabled = i_bVal
      .Page1.oPag.oCCFLGA15_1_51.enabled = i_bVal
      .Page1.oPag.oCCFLPRIC_1_52.enabled = i_bVal
      .Page1.oPag.oCCFLGA10_1_53.enabled = i_bVal
      .Page1.oPag.oCCFLGA13_1_54.enabled = i_bVal
      .Page1.oPag.oCCFLGA16_1_55.enabled = i_bVal
      .Page1.oPag.oCCFLGA14_1_56.enabled = i_bVal
      .Page1.oPag.oCCDTINVA_1_58.enabled = i_bVal
      .Page1.oPag.oCCDTOBSO_1_59.enabled = i_bVal
      .Page1.oPag.oObj_1_34.enabled = i_bVal
      .Page1.oPag.oObj_1_60.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oCCCODICE_1_3.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oCCCODICE_1_3.enabled = .t.
        .Page1.oPag.oCCDESCRI_1_4.enabled = .t.
      endif
    endwith
    this.GSCE_MFO.SetStatus(i_cOp)
    this.GSCE_MOC.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'CAU_CESP',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSCE_MFO.SetChildrenStatus(i_cOp)
  *  this.GSCE_MOC.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CAU_CESP_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCCODICE,"CCCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCDESCRI,"CCDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCFLRIFE,"CCFLRIFE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCFLRAGG,"CCFLRAGG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCFLGA01,"CCFLGA01",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCFLPRIU,"CCFLPRIU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCFLDADI,"CCFLDADI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCFLGA23,"CCFLGA23",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCFLGA24,"CCFLGA24",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCFLGA18,"CCFLGA18",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCFLONFI,"CCFLONFI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCFLGA22,"CCFLGA22",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCFLGA19,"CCFLGA19",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCFLGA20,"CCFLGA20",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCFLGA21,"CCFLGA21",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCFLGA07,"CCFLGA07",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCFLGA08,"CCFLGA08",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCFLGA17,"CCFLGA17",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCFLGOPE,"CCFLGOPE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCDATAGG,"CCDATAGG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCFLCONT,"CCFLCONT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCCAUCON,"CCCAUCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCRIVALU,"CCRIVALU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCFLGA03,"CCFLGA03",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCFLGA02,"CCFLGA02",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCFLGA04,"CCFLGA04",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCFLGA05,"CCFLGA05",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCFLGA06,"CCFLGA06",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCFLONFF,"CCFLONFF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCFLGA11,"CCFLGA11",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCFLGA12,"CCFLGA12",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCFLGA09,"CCFLGA09",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCFLGA15,"CCFLGA15",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCFLPRIC,"CCFLPRIC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCFLGA10,"CCFLGA10",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCFLGA13,"CCFLGA13",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCFLGA16,"CCFLGA16",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCFLGA14,"CCFLGA14",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCDTINVA,"CCDTINVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCDTOBSO,"CCDTOBSO",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.CAU_CESP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CESP_IDX,2])
    i_lTable = "CAU_CESP"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.CAU_CESP_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSCE_KCC with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CAU_CESP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CESP_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.CAU_CESP_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into CAU_CESP
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CAU_CESP')
        i_extval=cp_InsertValODBCExtFlds(this,'CAU_CESP')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(CCCODICE,CCDESCRI,CCFLRIFE,CCFLRAGG,CCFLGA01"+;
                  ",CCFLPRIU,CCFLDADI,CCFLGA23,CCFLGA24,CCFLGA18"+;
                  ",CCFLONFI,CCFLGA22,CCFLGA19,CCFLGA20,CCFLGA21"+;
                  ",CCFLGA07,CCFLGA08,CCFLGA17,CCFLGOPE,UTCC"+;
                  ",UTCV,UTDC,UTDV,CCDATAGG,CCFLCONT"+;
                  ",CCCAUCON,CCRIVALU,CCFLGA03,CCFLGA02,CCFLGA04"+;
                  ",CCFLGA05,CCFLGA06,CCFLONFF,CCFLGA11,CCFLGA12"+;
                  ",CCFLGA09,CCFLGA15,CCFLPRIC,CCFLGA10,CCFLGA13"+;
                  ",CCFLGA16,CCFLGA14,CCDTINVA,CCDTOBSO "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_CCCODICE)+;
                  ","+cp_ToStrODBC(this.w_CCDESCRI)+;
                  ","+cp_ToStrODBC(this.w_CCFLRIFE)+;
                  ","+cp_ToStrODBC(this.w_CCFLRAGG)+;
                  ","+cp_ToStrODBC(this.w_CCFLGA01)+;
                  ","+cp_ToStrODBC(this.w_CCFLPRIU)+;
                  ","+cp_ToStrODBC(this.w_CCFLDADI)+;
                  ","+cp_ToStrODBC(this.w_CCFLGA23)+;
                  ","+cp_ToStrODBC(this.w_CCFLGA24)+;
                  ","+cp_ToStrODBC(this.w_CCFLGA18)+;
                  ","+cp_ToStrODBC(this.w_CCFLONFI)+;
                  ","+cp_ToStrODBC(this.w_CCFLGA22)+;
                  ","+cp_ToStrODBC(this.w_CCFLGA19)+;
                  ","+cp_ToStrODBC(this.w_CCFLGA20)+;
                  ","+cp_ToStrODBC(this.w_CCFLGA21)+;
                  ","+cp_ToStrODBC(this.w_CCFLGA07)+;
                  ","+cp_ToStrODBC(this.w_CCFLGA08)+;
                  ","+cp_ToStrODBC(this.w_CCFLGA17)+;
                  ","+cp_ToStrODBC(this.w_CCFLGOPE)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                  ","+cp_ToStrODBC(this.w_CCDATAGG)+;
                  ","+cp_ToStrODBC(this.w_CCFLCONT)+;
                  ","+cp_ToStrODBCNull(this.w_CCCAUCON)+;
                  ","+cp_ToStrODBC(this.w_CCRIVALU)+;
                  ","+cp_ToStrODBC(this.w_CCFLGA03)+;
                  ","+cp_ToStrODBC(this.w_CCFLGA02)+;
                  ","+cp_ToStrODBC(this.w_CCFLGA04)+;
                  ","+cp_ToStrODBC(this.w_CCFLGA05)+;
                  ","+cp_ToStrODBC(this.w_CCFLGA06)+;
                  ","+cp_ToStrODBC(this.w_CCFLONFF)+;
                  ","+cp_ToStrODBC(this.w_CCFLGA11)+;
                  ","+cp_ToStrODBC(this.w_CCFLGA12)+;
                  ","+cp_ToStrODBC(this.w_CCFLGA09)+;
                  ","+cp_ToStrODBC(this.w_CCFLGA15)+;
                  ","+cp_ToStrODBC(this.w_CCFLPRIC)+;
                  ","+cp_ToStrODBC(this.w_CCFLGA10)+;
                  ","+cp_ToStrODBC(this.w_CCFLGA13)+;
                  ","+cp_ToStrODBC(this.w_CCFLGA16)+;
                  ","+cp_ToStrODBC(this.w_CCFLGA14)+;
                  ","+cp_ToStrODBC(this.w_CCDTINVA)+;
                  ","+cp_ToStrODBC(this.w_CCDTOBSO)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CAU_CESP')
        i_extval=cp_InsertValVFPExtFlds(this,'CAU_CESP')
        cp_CheckDeletedKey(i_cTable,0,'CCCODICE',this.w_CCCODICE)
        INSERT INTO (i_cTable);
              (CCCODICE,CCDESCRI,CCFLRIFE,CCFLRAGG,CCFLGA01,CCFLPRIU,CCFLDADI,CCFLGA23,CCFLGA24,CCFLGA18,CCFLONFI,CCFLGA22,CCFLGA19,CCFLGA20,CCFLGA21,CCFLGA07,CCFLGA08,CCFLGA17,CCFLGOPE,UTCC,UTCV,UTDC,UTDV,CCDATAGG,CCFLCONT,CCCAUCON,CCRIVALU,CCFLGA03,CCFLGA02,CCFLGA04,CCFLGA05,CCFLGA06,CCFLONFF,CCFLGA11,CCFLGA12,CCFLGA09,CCFLGA15,CCFLPRIC,CCFLGA10,CCFLGA13,CCFLGA16,CCFLGA14,CCDTINVA,CCDTOBSO  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_CCCODICE;
                  ,this.w_CCDESCRI;
                  ,this.w_CCFLRIFE;
                  ,this.w_CCFLRAGG;
                  ,this.w_CCFLGA01;
                  ,this.w_CCFLPRIU;
                  ,this.w_CCFLDADI;
                  ,this.w_CCFLGA23;
                  ,this.w_CCFLGA24;
                  ,this.w_CCFLGA18;
                  ,this.w_CCFLONFI;
                  ,this.w_CCFLGA22;
                  ,this.w_CCFLGA19;
                  ,this.w_CCFLGA20;
                  ,this.w_CCFLGA21;
                  ,this.w_CCFLGA07;
                  ,this.w_CCFLGA08;
                  ,this.w_CCFLGA17;
                  ,this.w_CCFLGOPE;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
                  ,this.w_CCDATAGG;
                  ,this.w_CCFLCONT;
                  ,this.w_CCCAUCON;
                  ,this.w_CCRIVALU;
                  ,this.w_CCFLGA03;
                  ,this.w_CCFLGA02;
                  ,this.w_CCFLGA04;
                  ,this.w_CCFLGA05;
                  ,this.w_CCFLGA06;
                  ,this.w_CCFLONFF;
                  ,this.w_CCFLGA11;
                  ,this.w_CCFLGA12;
                  ,this.w_CCFLGA09;
                  ,this.w_CCFLGA15;
                  ,this.w_CCFLPRIC;
                  ,this.w_CCFLGA10;
                  ,this.w_CCFLGA13;
                  ,this.w_CCFLGA16;
                  ,this.w_CCFLGA14;
                  ,this.w_CCDTINVA;
                  ,this.w_CCDTOBSO;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.CAU_CESP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CESP_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.CAU_CESP_IDX,i_nConn)
      *
      * update CAU_CESP
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'CAU_CESP')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " CCDESCRI="+cp_ToStrODBC(this.w_CCDESCRI)+;
             ",CCFLRIFE="+cp_ToStrODBC(this.w_CCFLRIFE)+;
             ",CCFLRAGG="+cp_ToStrODBC(this.w_CCFLRAGG)+;
             ",CCFLGA01="+cp_ToStrODBC(this.w_CCFLGA01)+;
             ",CCFLPRIU="+cp_ToStrODBC(this.w_CCFLPRIU)+;
             ",CCFLDADI="+cp_ToStrODBC(this.w_CCFLDADI)+;
             ",CCFLGA23="+cp_ToStrODBC(this.w_CCFLGA23)+;
             ",CCFLGA24="+cp_ToStrODBC(this.w_CCFLGA24)+;
             ",CCFLGA18="+cp_ToStrODBC(this.w_CCFLGA18)+;
             ",CCFLONFI="+cp_ToStrODBC(this.w_CCFLONFI)+;
             ",CCFLGA22="+cp_ToStrODBC(this.w_CCFLGA22)+;
             ",CCFLGA19="+cp_ToStrODBC(this.w_CCFLGA19)+;
             ",CCFLGA20="+cp_ToStrODBC(this.w_CCFLGA20)+;
             ",CCFLGA21="+cp_ToStrODBC(this.w_CCFLGA21)+;
             ",CCFLGA07="+cp_ToStrODBC(this.w_CCFLGA07)+;
             ",CCFLGA08="+cp_ToStrODBC(this.w_CCFLGA08)+;
             ",CCFLGA17="+cp_ToStrODBC(this.w_CCFLGA17)+;
             ",CCFLGOPE="+cp_ToStrODBC(this.w_CCFLGOPE)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",CCDATAGG="+cp_ToStrODBC(this.w_CCDATAGG)+;
             ",CCFLCONT="+cp_ToStrODBC(this.w_CCFLCONT)+;
             ",CCCAUCON="+cp_ToStrODBCNull(this.w_CCCAUCON)+;
             ",CCRIVALU="+cp_ToStrODBC(this.w_CCRIVALU)+;
             ",CCFLGA03="+cp_ToStrODBC(this.w_CCFLGA03)+;
             ",CCFLGA02="+cp_ToStrODBC(this.w_CCFLGA02)+;
             ",CCFLGA04="+cp_ToStrODBC(this.w_CCFLGA04)+;
             ",CCFLGA05="+cp_ToStrODBC(this.w_CCFLGA05)+;
             ",CCFLGA06="+cp_ToStrODBC(this.w_CCFLGA06)+;
             ",CCFLONFF="+cp_ToStrODBC(this.w_CCFLONFF)+;
             ",CCFLGA11="+cp_ToStrODBC(this.w_CCFLGA11)+;
             ",CCFLGA12="+cp_ToStrODBC(this.w_CCFLGA12)+;
             ",CCFLGA09="+cp_ToStrODBC(this.w_CCFLGA09)+;
             ",CCFLGA15="+cp_ToStrODBC(this.w_CCFLGA15)+;
             ",CCFLPRIC="+cp_ToStrODBC(this.w_CCFLPRIC)+;
             ",CCFLGA10="+cp_ToStrODBC(this.w_CCFLGA10)+;
             ",CCFLGA13="+cp_ToStrODBC(this.w_CCFLGA13)+;
             ",CCFLGA16="+cp_ToStrODBC(this.w_CCFLGA16)+;
             ",CCFLGA14="+cp_ToStrODBC(this.w_CCFLGA14)+;
             ",CCDTINVA="+cp_ToStrODBC(this.w_CCDTINVA)+;
             ",CCDTOBSO="+cp_ToStrODBC(this.w_CCDTOBSO)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'CAU_CESP')
        i_cWhere = cp_PKFox(i_cTable  ,'CCCODICE',this.w_CCCODICE  )
        UPDATE (i_cTable) SET;
              CCDESCRI=this.w_CCDESCRI;
             ,CCFLRIFE=this.w_CCFLRIFE;
             ,CCFLRAGG=this.w_CCFLRAGG;
             ,CCFLGA01=this.w_CCFLGA01;
             ,CCFLPRIU=this.w_CCFLPRIU;
             ,CCFLDADI=this.w_CCFLDADI;
             ,CCFLGA23=this.w_CCFLGA23;
             ,CCFLGA24=this.w_CCFLGA24;
             ,CCFLGA18=this.w_CCFLGA18;
             ,CCFLONFI=this.w_CCFLONFI;
             ,CCFLGA22=this.w_CCFLGA22;
             ,CCFLGA19=this.w_CCFLGA19;
             ,CCFLGA20=this.w_CCFLGA20;
             ,CCFLGA21=this.w_CCFLGA21;
             ,CCFLGA07=this.w_CCFLGA07;
             ,CCFLGA08=this.w_CCFLGA08;
             ,CCFLGA17=this.w_CCFLGA17;
             ,CCFLGOPE=this.w_CCFLGOPE;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             ,CCDATAGG=this.w_CCDATAGG;
             ,CCFLCONT=this.w_CCFLCONT;
             ,CCCAUCON=this.w_CCCAUCON;
             ,CCRIVALU=this.w_CCRIVALU;
             ,CCFLGA03=this.w_CCFLGA03;
             ,CCFLGA02=this.w_CCFLGA02;
             ,CCFLGA04=this.w_CCFLGA04;
             ,CCFLGA05=this.w_CCFLGA05;
             ,CCFLGA06=this.w_CCFLGA06;
             ,CCFLONFF=this.w_CCFLONFF;
             ,CCFLGA11=this.w_CCFLGA11;
             ,CCFLGA12=this.w_CCFLGA12;
             ,CCFLGA09=this.w_CCFLGA09;
             ,CCFLGA15=this.w_CCFLGA15;
             ,CCFLPRIC=this.w_CCFLPRIC;
             ,CCFLGA10=this.w_CCFLGA10;
             ,CCFLGA13=this.w_CCFLGA13;
             ,CCFLGA16=this.w_CCFLGA16;
             ,CCFLGA14=this.w_CCFLGA14;
             ,CCDTINVA=this.w_CCDTINVA;
             ,CCDTOBSO=this.w_CCDTOBSO;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSCE_MFO : Saving
      this.GSCE_MFO.ChangeRow(this.cRowID+'      1',0;
             ,this.w_CCCODICE,"FOCODCAU";
             )
      this.GSCE_MFO.mReplace()
      * --- GSCE_MOC : Saving
      this.GSCE_MOC.ChangeRow(this.cRowID+'      1',0;
             ,this.w_CCCODICE,"MCCODCAU";
             )
      this.GSCE_MOC.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- gsce_acc
    this.NotifyEvent('ControlliFinali')
    * valorizzo maschera documenti
    If Type('This.w_OBJMASK')='O'
       This.w_OBJMASK.w_MVCESSER=this.w_MCSERIAL
       This.w_OBJMASK.mcalc(.T.)
       This.w_OBJMASK.Notifyevent('Valorizzacespiti')
    Endif
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSCE_MFO : Deleting
    this.GSCE_MFO.ChangeRow(this.cRowID+'      1',0;
           ,this.w_CCCODICE,"FOCODCAU";
           )
    this.GSCE_MFO.mDelete()
    * --- GSCE_MOC : Deleting
    this.GSCE_MOC.ChangeRow(this.cRowID+'      1',0;
           ,this.w_CCCODICE,"MCCODCAU";
           )
    this.GSCE_MOC.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.CAU_CESP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CESP_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.CAU_CESP_IDX,i_nConn)
      *
      * delete CAU_CESP
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'CCCODICE',this.w_CCCODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CAU_CESP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CESP_IDX,2])
    if i_bUpd
      with this
          .link_1_1('Full')
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
        .DoRTCalc(2,26,.t.)
            .w_CODI = .w_CCCODICE
            .w_DESC = .w_CCDESCRI
        .DoRTCalc(29,29,.t.)
            .w_CODI = .w_CCCODICE
            .w_DESC = .w_CCDESCRI
        .DoRTCalc(32,32,.t.)
        if .o_CCFLCONT<>.w_CCFLCONT
            .w_CCCAUCON = IIF(.w_CCFLCONT<>'N', .w_CCCAUCON, '')
          .link_3_5('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_60.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(34,52,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_60.Calculate()
    endwith
  return

  proc Calculate_JKXASVASEZ()
    with this
          * --- Check cambio obsolescenza gsce_bcc (O)
          GSCE_BCC(this;
              ,'O';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCCFLPRIU_1_8.enabled = this.oPgFrm.Page1.oPag.oCCFLPRIU_1_8.mCond()
    this.oPgFrm.Page1.oPag.oCCFLGA23_1_12.enabled = this.oPgFrm.Page1.oPag.oCCFLGA23_1_12.mCond()
    this.oPgFrm.Page1.oPag.oCCFLGA24_1_13.enabled = this.oPgFrm.Page1.oPag.oCCFLGA24_1_13.mCond()
    this.oPgFrm.Page1.oPag.oCCFLGA18_1_14.enabled = this.oPgFrm.Page1.oPag.oCCFLGA18_1_14.mCond()
    this.oPgFrm.Page1.oPag.oCCFLONFI_1_15.enabled = this.oPgFrm.Page1.oPag.oCCFLONFI_1_15.mCond()
    this.oPgFrm.Page1.oPag.oCCFLGA22_1_16.enabled = this.oPgFrm.Page1.oPag.oCCFLGA22_1_16.mCond()
    this.oPgFrm.Page1.oPag.oCCFLGA19_1_17.enabled = this.oPgFrm.Page1.oPag.oCCFLGA19_1_17.mCond()
    this.oPgFrm.Page1.oPag.oCCFLGA20_1_18.enabled = this.oPgFrm.Page1.oPag.oCCFLGA20_1_18.mCond()
    this.oPgFrm.Page1.oPag.oCCFLGA21_1_19.enabled = this.oPgFrm.Page1.oPag.oCCFLGA21_1_19.mCond()
    this.oPgFrm.Page1.oPag.oCCFLGA07_1_20.enabled = this.oPgFrm.Page1.oPag.oCCFLGA07_1_20.mCond()
    this.oPgFrm.Page1.oPag.oCCFLGA08_1_21.enabled = this.oPgFrm.Page1.oPag.oCCFLGA08_1_21.mCond()
    this.oPgFrm.Page3.oPag.oCCCAUCON_3_5.enabled = this.oPgFrm.Page3.oPag.oCCCAUCON_3_5.mCond()
    this.oPgFrm.Page1.oPag.oCCFLGA03_1_39.enabled = this.oPgFrm.Page1.oPag.oCCFLGA03_1_39.mCond()
    this.oPgFrm.Page1.oPag.oCCFLGA02_1_40.enabled = this.oPgFrm.Page1.oPag.oCCFLGA02_1_40.mCond()
    this.oPgFrm.Page1.oPag.oCCFLGA04_1_41.enabled = this.oPgFrm.Page1.oPag.oCCFLGA04_1_41.mCond()
    this.oPgFrm.Page1.oPag.oCCFLGA05_1_42.enabled = this.oPgFrm.Page1.oPag.oCCFLGA05_1_42.mCond()
    this.oPgFrm.Page1.oPag.oCCFLGA06_1_43.enabled = this.oPgFrm.Page1.oPag.oCCFLGA06_1_43.mCond()
    this.oPgFrm.Page1.oPag.oCCFLONFF_1_44.enabled = this.oPgFrm.Page1.oPag.oCCFLONFF_1_44.mCond()
    this.oPgFrm.Page1.oPag.oCCFLGA11_1_45.enabled = this.oPgFrm.Page1.oPag.oCCFLGA11_1_45.mCond()
    this.oPgFrm.Page1.oPag.oCCFLGA12_1_47.enabled = this.oPgFrm.Page1.oPag.oCCFLGA12_1_47.mCond()
    this.oPgFrm.Page1.oPag.oCCFLGA09_1_49.enabled = this.oPgFrm.Page1.oPag.oCCFLGA09_1_49.mCond()
    this.oPgFrm.Page1.oPag.oCCFLGA15_1_51.enabled = this.oPgFrm.Page1.oPag.oCCFLGA15_1_51.mCond()
    this.oPgFrm.Page1.oPag.oCCFLPRIC_1_52.enabled = this.oPgFrm.Page1.oPag.oCCFLPRIC_1_52.mCond()
    this.oPgFrm.Page1.oPag.oCCFLGA10_1_53.enabled = this.oPgFrm.Page1.oPag.oCCFLGA10_1_53.mCond()
    this.oPgFrm.Page1.oPag.oCCFLGA13_1_54.enabled = this.oPgFrm.Page1.oPag.oCCFLGA13_1_54.mCond()
    this.oPgFrm.Page1.oPag.oCCFLGA16_1_55.enabled = this.oPgFrm.Page1.oPag.oCCFLGA16_1_55.mCond()
    this.oPgFrm.Page1.oPag.oCCFLGA14_1_56.enabled = this.oPgFrm.Page1.oPag.oCCFLGA14_1_56.mCond()
    this.GSCE_MOC.enabled = this.oPgFrm.Page3.oPag.oLinkPC_3_8.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_34.Event(cEvent)
        if lower(cEvent)==lower("w_CCDTOBSO Changed")
          .Calculate_JKXASVASEZ()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_60.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_CESP_IDX,3]
    i_lTable = "PAR_CESP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_CESP_IDX,2], .t., this.PAR_CESP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_CESP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PCCODAZI,PCTIPAMM";
                   +" from "+i_cTable+" "+i_lTable+" where PCCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PCCODAZI',this.w_CODAZI)
            select PCCODAZI,PCTIPAMM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.PCCODAZI,space(5))
      this.w_TIPAMM = NVL(_Link_.PCTIPAMM,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_TIPAMM = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_CESP_IDX,2])+'\'+cp_ToStr(_Link_.PCCODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_CESP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CCCAUCON
  func Link_3_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CCCAUCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_CCCAUCON)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_CCCAUCON))
          select CCCODICE,CCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CCCAUCON)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CCCAUCON) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oCCCAUCON_3_5'),i_cWhere,'GSCG_ACC',"Causali contabili",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CCCAUCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_CCCAUCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_CCCAUCON)
            select CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CCCAUCON = NVL(_Link_.CCCODICE,space(5))
      this.w_DESCAU = NVL(_Link_.CCDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CCCAUCON = space(5)
      endif
      this.w_DESCAU = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CCCAUCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_3_5(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAU_CONT_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
      i_cNewSel = i_cSel+ ",link_3_5.CCCODICE as CCCODICE305"+ ",link_3_5.CCDESCRI as CCDESCRI305"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_3_5 on CAU_CESP.CCCAUCON=link_3_5.CCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_3_5"
          i_cKey=i_cKey+'+" and CAU_CESP.CCCAUCON=link_3_5.CCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCCCODICE_1_3.value==this.w_CCCODICE)
      this.oPgFrm.Page1.oPag.oCCCODICE_1_3.value=this.w_CCCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oCCDESCRI_1_4.value==this.w_CCDESCRI)
      this.oPgFrm.Page1.oPag.oCCDESCRI_1_4.value=this.w_CCDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCCFLRIFE_1_5.RadioValue()==this.w_CCFLRIFE)
      this.oPgFrm.Page1.oPag.oCCFLRIFE_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCFLRAGG_1_6.RadioValue()==this.w_CCFLRAGG)
      this.oPgFrm.Page1.oPag.oCCFLRAGG_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCFLGA01_1_7.RadioValue()==this.w_CCFLGA01)
      this.oPgFrm.Page1.oPag.oCCFLGA01_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCFLPRIU_1_8.RadioValue()==this.w_CCFLPRIU)
      this.oPgFrm.Page1.oPag.oCCFLPRIU_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCFLDADI_1_9.RadioValue()==this.w_CCFLDADI)
      this.oPgFrm.Page1.oPag.oCCFLDADI_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCFLGA23_1_12.RadioValue()==this.w_CCFLGA23)
      this.oPgFrm.Page1.oPag.oCCFLGA23_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCFLGA24_1_13.RadioValue()==this.w_CCFLGA24)
      this.oPgFrm.Page1.oPag.oCCFLGA24_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCFLGA18_1_14.RadioValue()==this.w_CCFLGA18)
      this.oPgFrm.Page1.oPag.oCCFLGA18_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCFLONFI_1_15.RadioValue()==this.w_CCFLONFI)
      this.oPgFrm.Page1.oPag.oCCFLONFI_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCFLGA22_1_16.RadioValue()==this.w_CCFLGA22)
      this.oPgFrm.Page1.oPag.oCCFLGA22_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCFLGA19_1_17.RadioValue()==this.w_CCFLGA19)
      this.oPgFrm.Page1.oPag.oCCFLGA19_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCFLGA20_1_18.RadioValue()==this.w_CCFLGA20)
      this.oPgFrm.Page1.oPag.oCCFLGA20_1_18.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCFLGA21_1_19.RadioValue()==this.w_CCFLGA21)
      this.oPgFrm.Page1.oPag.oCCFLGA21_1_19.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCFLGA07_1_20.RadioValue()==this.w_CCFLGA07)
      this.oPgFrm.Page1.oPag.oCCFLGA07_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCFLGA08_1_21.RadioValue()==this.w_CCFLGA08)
      this.oPgFrm.Page1.oPag.oCCFLGA08_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCFLGA17_1_22.RadioValue()==this.w_CCFLGA17)
      this.oPgFrm.Page1.oPag.oCCFLGA17_1_22.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCFLGOPE_1_23.RadioValue()==this.w_CCFLGOPE)
      this.oPgFrm.Page1.oPag.oCCFLGOPE_1_23.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCODI_2_1.value==this.w_CODI)
      this.oPgFrm.Page2.oPag.oCODI_2_1.value=this.w_CODI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESC_2_2.value==this.w_DESC)
      this.oPgFrm.Page2.oPag.oDESC_2_2.value=this.w_DESC
    endif
    if not(this.oPgFrm.Page2.oPag.oCCDATAGG_2_3.RadioValue()==this.w_CCDATAGG)
      this.oPgFrm.Page2.oPag.oCCDATAGG_2_3.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oCODI_3_1.value==this.w_CODI)
      this.oPgFrm.Page3.oPag.oCODI_3_1.value=this.w_CODI
    endif
    if not(this.oPgFrm.Page3.oPag.oDESC_3_2.value==this.w_DESC)
      this.oPgFrm.Page3.oPag.oDESC_3_2.value=this.w_DESC
    endif
    if not(this.oPgFrm.Page3.oPag.oCCFLCONT_3_3.RadioValue()==this.w_CCFLCONT)
      this.oPgFrm.Page3.oPag.oCCFLCONT_3_3.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oCCCAUCON_3_5.value==this.w_CCCAUCON)
      this.oPgFrm.Page3.oPag.oCCCAUCON_3_5.value=this.w_CCCAUCON
    endif
    if not(this.oPgFrm.Page3.oPag.oDESCAU_3_6.value==this.w_DESCAU)
      this.oPgFrm.Page3.oPag.oDESCAU_3_6.value=this.w_DESCAU
    endif
    if not(this.oPgFrm.Page1.oPag.oCCFLGA03_1_39.RadioValue()==this.w_CCFLGA03)
      this.oPgFrm.Page1.oPag.oCCFLGA03_1_39.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCFLGA02_1_40.RadioValue()==this.w_CCFLGA02)
      this.oPgFrm.Page1.oPag.oCCFLGA02_1_40.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCFLGA04_1_41.RadioValue()==this.w_CCFLGA04)
      this.oPgFrm.Page1.oPag.oCCFLGA04_1_41.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCFLGA05_1_42.RadioValue()==this.w_CCFLGA05)
      this.oPgFrm.Page1.oPag.oCCFLGA05_1_42.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCFLGA06_1_43.RadioValue()==this.w_CCFLGA06)
      this.oPgFrm.Page1.oPag.oCCFLGA06_1_43.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCFLONFF_1_44.RadioValue()==this.w_CCFLONFF)
      this.oPgFrm.Page1.oPag.oCCFLONFF_1_44.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCFLGA11_1_45.RadioValue()==this.w_CCFLGA11)
      this.oPgFrm.Page1.oPag.oCCFLGA11_1_45.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCFLGA12_1_47.RadioValue()==this.w_CCFLGA12)
      this.oPgFrm.Page1.oPag.oCCFLGA12_1_47.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCFLGA09_1_49.RadioValue()==this.w_CCFLGA09)
      this.oPgFrm.Page1.oPag.oCCFLGA09_1_49.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCFLGA15_1_51.RadioValue()==this.w_CCFLGA15)
      this.oPgFrm.Page1.oPag.oCCFLGA15_1_51.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCFLPRIC_1_52.RadioValue()==this.w_CCFLPRIC)
      this.oPgFrm.Page1.oPag.oCCFLPRIC_1_52.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCFLGA10_1_53.RadioValue()==this.w_CCFLGA10)
      this.oPgFrm.Page1.oPag.oCCFLGA10_1_53.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCFLGA13_1_54.RadioValue()==this.w_CCFLGA13)
      this.oPgFrm.Page1.oPag.oCCFLGA13_1_54.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCFLGA16_1_55.RadioValue()==this.w_CCFLGA16)
      this.oPgFrm.Page1.oPag.oCCFLGA16_1_55.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCFLGA14_1_56.RadioValue()==this.w_CCFLGA14)
      this.oPgFrm.Page1.oPag.oCCFLGA14_1_56.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCDTINVA_1_58.value==this.w_CCDTINVA)
      this.oPgFrm.Page1.oPag.oCCDTINVA_1_58.value=this.w_CCDTINVA
    endif
    if not(this.oPgFrm.Page1.oPag.oCCDTOBSO_1_59.value==this.w_CCDTOBSO)
      this.oPgFrm.Page1.oPag.oCCDTOBSO_1_59.value=this.w_CCDTOBSO
    endif
    cp_SetControlsValueExtFlds(this,'CAU_CESP')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CCCODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCCCODICE_1_3.SetFocus()
            i_bnoObbl = !empty(.w_CCCODICE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CCCAUCON))  and (g_COGE='S' AND .w_CCFLCONT='S')
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oCCCAUCON_3_5.SetFocus()
            i_bnoObbl = !empty(.w_CCCAUCON)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      *i_bRes = i_bRes .and. .GSCE_MFO.CheckForm()
      if i_bres
        i_bres=  .GSCE_MFO.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=2
        endif
      endif
      *i_bRes = i_bRes .and. .GSCE_MOC.CheckForm()
      if i_bres
        i_bres=  .GSCE_MOC.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=3
        endif
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CCFLCONT = this.w_CCFLCONT
    this.o_CCFLGA04 = this.w_CCFLGA04
    * --- GSCE_MFO : Depends On
    this.GSCE_MFO.SaveDependsOn()
    * --- GSCE_MOC : Depends On
    this.GSCE_MOC.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgsce_accPag1 as StdContainer
  Width  = 765
  height = 477
  stdWidth  = 765
  stdheight = 477
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCCCODICE_1_3 as StdField with uid="WVDMGOANRA",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CCCODICE", cQueryName = "CCCODICE",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice della causale di movimentazione cespite",;
    HelpContextID = 118043243,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=78, Top=13, cSayPict="'!!!!!'", cGetPict="'!!!!!'", InputMask=replicate('X',5)

  add object oCCDESCRI_1_4 as StdField with uid="XCAPPJUVVW",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CCDESCRI", cQueryName = "CCDESCRI",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione della causale",;
    HelpContextID = 32457327,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=149, Top=13, InputMask=replicate('X',40)


  add object oCCFLRIFE_1_5 as StdCombo with uid="OGRCTCYUSA",rtseq=5,rtrep=.f.,left=78,top=43,width=103,height=21;
    , ToolTipText = "Eventuale riferimento cli/for per la contabilizzazione";
    , HelpContextID = 132538987;
    , cFormVar="w_CCFLRIFE",RowSource=""+"Nessuno,"+"Cliente,"+"Fornitore", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCCFLRIFE_1_5.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'C',;
    iif(this.value =3,'F',;
    space(1)))))
  endfunc
  func oCCFLRIFE_1_5.GetRadio()
    this.Parent.oContained.w_CCFLRIFE = this.RadioValue()
    return .t.
  endfunc

  func oCCFLRIFE_1_5.SetRadio()
    this.Parent.oContained.w_CCFLRIFE=trim(this.Parent.oContained.w_CCFLRIFE)
    this.value = ;
      iif(this.Parent.oContained.w_CCFLRIFE=='N',1,;
      iif(this.Parent.oContained.w_CCFLRIFE=='C',2,;
      iif(this.Parent.oContained.w_CCFLRIFE=='F',3,;
      0)))
  endfunc

  add object oCCFLRAGG_1_6 as StdCheck with uid="BJJHDYAAZU",rtseq=6,rtrep=.f.,left=382, top=40, caption="Raggruppa categorie",;
    ToolTipText = "Se attivo: raggruppa per categoria, i movimenti durante la stampa libro cespiti",;
    HelpContextID = 266756717,;
    cFormVar="w_CCFLRAGG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCCFLRAGG_1_6.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCCFLRAGG_1_6.GetRadio()
    this.Parent.oContained.w_CCFLRAGG = this.RadioValue()
    return .t.
  endfunc

  func oCCFLRAGG_1_6.SetRadio()
    this.Parent.oContained.w_CCFLRAGG=trim(this.Parent.oContained.w_CCFLRAGG)
    this.value = ;
      iif(this.Parent.oContained.w_CCFLRAGG=='S',1,;
      0)
  endfunc

  add object oCCFLGA01_1_7 as StdCheck with uid="PFPLYBGHVP",rtseq=7,rtrep=.f.,left=15, top=76, caption="Importo vendita",;
    ToolTipText = "Se attivo: abilita la gestione del campo durante i movimenti riferiti alla causa",;
    HelpContextID = 13213097,;
    cFormVar="w_CCFLGA01", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCCFLGA01_1_7.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCCFLGA01_1_7.GetRadio()
    this.Parent.oContained.w_CCFLGA01 = this.RadioValue()
    return .t.
  endfunc

  func oCCFLGA01_1_7.SetRadio()
    this.Parent.oContained.w_CCFLGA01=trim(this.Parent.oContained.w_CCFLGA01)
    this.value = ;
      iif(this.Parent.oContained.w_CCFLGA01=='S',1,;
      0)
  endfunc

  add object oCCFLPRIU_1_8 as StdCheck with uid="RXSCMCKKNM",rtseq=8,rtrep=.f.,left=543, top=142, caption="Data primo utilizzo fiscale",;
    ToolTipText = "Se attivo: aggiorna la data di primo utilizzo fiscale del cespite",;
    HelpContextID = 255434117,;
    cFormVar="w_CCFLPRIU", bObbl = .f. , nPag = 1;
    , tabstop=.f.;
   , bGlobalFont=.t.


  func oCCFLPRIU_1_8.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCCFLPRIU_1_8.GetRadio()
    this.Parent.oContained.w_CCFLPRIU = this.RadioValue()
    return .t.
  endfunc

  func oCCFLPRIU_1_8.SetRadio()
    this.Parent.oContained.w_CCFLPRIU=trim(this.Parent.oContained.w_CCFLPRIU)
    this.value = ;
      iif(this.Parent.oContained.w_CCFLPRIU=='S',1,;
      0)
  endfunc

  func oCCFLPRIU_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPAMM<>'C')
    endwith
   endif
  endfunc

  add object oCCFLDADI_1_9 as StdCheck with uid="WOZEHIKCLU",rtseq=9,rtrep=.f.,left=543, top=164, caption="Data dismissione",;
    ToolTipText = "Se attivo: aggiorna la data di dismissione del cespite",;
    HelpContextID = 252076655,;
    cFormVar="w_CCFLDADI", bObbl = .f. , nPag = 1;
    , tabstop=.f.;
   , bGlobalFont=.t.


  func oCCFLDADI_1_9.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCCFLDADI_1_9.GetRadio()
    this.Parent.oContained.w_CCFLDADI = this.RadioValue()
    return .t.
  endfunc

  func oCCFLDADI_1_9.SetRadio()
    this.Parent.oContained.w_CCFLDADI=trim(this.Parent.oContained.w_CCFLDADI)
    this.value = ;
      iif(this.Parent.oContained.w_CCFLDADI=='S',1,;
      0)
  endfunc

  add object oCCFLGA23_1_12 as StdCheck with uid="UYXDLFROQC",rtseq=10,rtrep=.f.,left=15, top=119, caption="Incrementi di valore",;
    ToolTipText = "Se attivo: abilita la gestione del campo durante i movimenti riferiti alla causale",;
    HelpContextID = 13213095,;
    cFormVar="w_CCFLGA23", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCCFLGA23_1_12.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCCFLGA23_1_12.GetRadio()
    this.Parent.oContained.w_CCFLGA23 = this.RadioValue()
    return .t.
  endfunc

  func oCCFLGA23_1_12.SetRadio()
    this.Parent.oContained.w_CCFLGA23=trim(this.Parent.oContained.w_CCFLGA23)
    this.value = ;
      iif(this.Parent.oContained.w_CCFLGA23=='S',1,;
      0)
  endfunc

  func oCCFLGA23_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPAMM<>'F')
    endwith
   endif
  endfunc

  add object oCCFLGA24_1_13 as StdCheck with uid="PQHQDVXXDA",rtseq=11,rtrep=.f.,left=15, top=142, caption="Oneri e spese",;
    ToolTipText = "Se attivo: abilita la gestione del campo durante i movimenti riferiti alla causale",;
    HelpContextID = 13213094,;
    cFormVar="w_CCFLGA24", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCCFLGA24_1_13.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCCFLGA24_1_13.GetRadio()
    this.Parent.oContained.w_CCFLGA24 = this.RadioValue()
    return .t.
  endfunc

  func oCCFLGA24_1_13.SetRadio()
    this.Parent.oContained.w_CCFLGA24=trim(this.Parent.oContained.w_CCFLGA24)
    this.value = ;
      iif(this.Parent.oContained.w_CCFLGA24=='S',1,;
      0)
  endfunc

  func oCCFLGA24_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPAMM<>'F')
    endwith
   endif
  endfunc

  add object oCCFLGA18_1_14 as StdCheck with uid="JZSXUARMAZ",rtseq=12,rtrep=.f.,left=15, top=164, caption="Rivalutazioni",;
    ToolTipText = "Se attivo: abilita la gestione del campo durante i movimenti riferiti alla causale",;
    HelpContextID = 13213090,;
    cFormVar="w_CCFLGA18", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCCFLGA18_1_14.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCCFLGA18_1_14.GetRadio()
    this.Parent.oContained.w_CCFLGA18 = this.RadioValue()
    return .t.
  endfunc

  func oCCFLGA18_1_14.SetRadio()
    this.Parent.oContained.w_CCFLGA18=trim(this.Parent.oContained.w_CCFLGA18)
    this.value = ;
      iif(this.Parent.oContained.w_CCFLGA18=='S',1,;
      0)
  endfunc

  func oCCFLGA18_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPAMM<>'F')
    endwith
   endif
  endfunc

  add object oCCFLONFI_1_15 as StdCheck with uid="UHMCILLOMK",rtseq=13,rtrep=.f.,left=146, top=142, caption="Oneri finanziari",;
    ToolTipText = "Se attivo: oneri finanziari per la stampa della nota integrativa",;
    HelpContextID = 213279343,;
    cFormVar="w_CCFLONFI", bObbl = .f. , nPag = 1;
    , tabstop=.f.;
   , bGlobalFont=.t.


  func oCCFLONFI_1_15.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCCFLONFI_1_15.GetRadio()
    this.Parent.oContained.w_CCFLONFI = this.RadioValue()
    return .t.
  endfunc

  func oCCFLONFI_1_15.SetRadio()
    this.Parent.oContained.w_CCFLONFI=trim(this.Parent.oContained.w_CCFLONFI)
    this.value = ;
      iif(this.Parent.oContained.w_CCFLONFI=='S',1,;
      0)
  endfunc

  func oCCFLONFI_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CCFLGA24='S' AND .w_TIPAMM<>'F')
    endwith
   endif
  endfunc

  add object oCCFLGA22_1_16 as StdCheck with uid="ZISSPPTWMZ",rtseq=14,rtrep=.f.,left=15, top=188, caption="Decrementi di valore",;
    ToolTipText = "Se attivo: abilita la gestione del campo durante i movimenti riferiti alla causale",;
    HelpContextID = 13213096,;
    cFormVar="w_CCFLGA22", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCCFLGA22_1_16.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCCFLGA22_1_16.GetRadio()
    this.Parent.oContained.w_CCFLGA22 = this.RadioValue()
    return .t.
  endfunc

  func oCCFLGA22_1_16.SetRadio()
    this.Parent.oContained.w_CCFLGA22=trim(this.Parent.oContained.w_CCFLGA22)
    this.value = ;
      iif(this.Parent.oContained.w_CCFLGA22=='S',1,;
      0)
  endfunc

  func oCCFLGA22_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPAMM<>'F')
    endwith
   endif
  endfunc

  add object oCCFLGA19_1_17 as StdCheck with uid="OVHNDTOMJU",rtseq=15,rtrep=.f.,left=15, top=211, caption="Svalutazioni",;
    ToolTipText = "Se attivo: abilita la gestione del campo durante i movimenti riferiti alla causale",;
    HelpContextID = 13213089,;
    cFormVar="w_CCFLGA19", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCCFLGA19_1_17.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCCFLGA19_1_17.GetRadio()
    this.Parent.oContained.w_CCFLGA19 = this.RadioValue()
    return .t.
  endfunc

  func oCCFLGA19_1_17.SetRadio()
    this.Parent.oContained.w_CCFLGA19=trim(this.Parent.oContained.w_CCFLGA19)
    this.value = ;
      iif(this.Parent.oContained.w_CCFLGA19=='S',1,;
      0)
  endfunc

  func oCCFLGA19_1_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPAMM<>'F')
    endwith
   endif
  endfunc

  add object oCCFLGA20_1_18 as StdCheck with uid="CXVGBFRDSU",rtseq=16,rtrep=.f.,left=15, top=259, caption="Plusvalenza",;
    ToolTipText = "Se attivo: abilita la gestione del campo durante i movimenti riferiti alla causale",;
    HelpContextID = 13213098,;
    cFormVar="w_CCFLGA20", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCCFLGA20_1_18.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCCFLGA20_1_18.GetRadio()
    this.Parent.oContained.w_CCFLGA20 = this.RadioValue()
    return .t.
  endfunc

  func oCCFLGA20_1_18.SetRadio()
    this.Parent.oContained.w_CCFLGA20=trim(this.Parent.oContained.w_CCFLGA20)
    this.value = ;
      iif(this.Parent.oContained.w_CCFLGA20=='S',1,;
      0)
  endfunc

  func oCCFLGA20_1_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPAMM<>'F')
    endwith
   endif
  endfunc

  add object oCCFLGA21_1_19 as StdCheck with uid="IPYYAXAZJB",rtseq=17,rtrep=.f.,left=15, top=280, caption="Minusvalenza",;
    ToolTipText = "Se attivo: abilita la gestione del campo durante i movimenti riferiti alla causale",;
    HelpContextID = 13213097,;
    cFormVar="w_CCFLGA21", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCCFLGA21_1_19.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCCFLGA21_1_19.GetRadio()
    this.Parent.oContained.w_CCFLGA21 = this.RadioValue()
    return .t.
  endfunc

  func oCCFLGA21_1_19.SetRadio()
    this.Parent.oContained.w_CCFLGA21=trim(this.Parent.oContained.w_CCFLGA21)
    this.value = ;
      iif(this.Parent.oContained.w_CCFLGA21=='S',1,;
      0)
  endfunc

  func oCCFLGA21_1_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPAMM<>'F')
    endwith
   endif
  endfunc

  add object oCCFLGA07_1_20 as StdCheck with uid="VAZQMUHRKC",rtseq=18,rtrep=.f.,left=15, top=332, caption="Accantonamenti",;
    ToolTipText = "Se attivo: abilita la gestione del campo durante i movimenti riferiti alla causale",;
    HelpContextID = 13213091,;
    cFormVar="w_CCFLGA07", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCCFLGA07_1_20.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCCFLGA07_1_20.GetRadio()
    this.Parent.oContained.w_CCFLGA07 = this.RadioValue()
    return .t.
  endfunc

  func oCCFLGA07_1_20.SetRadio()
    this.Parent.oContained.w_CCFLGA07=trim(this.Parent.oContained.w_CCFLGA07)
    this.value = ;
      iif(this.Parent.oContained.w_CCFLGA07=='S',1,;
      0)
  endfunc

  func oCCFLGA07_1_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPAMM<>'F')
    endwith
   endif
  endfunc

  add object oCCFLGA08_1_21 as StdCheck with uid="ROUOHVHJAN",rtseq=19,rtrep=.f.,left=15, top=356, caption="Utilizzazioni civili",;
    ToolTipText = "Se attivo: abilita la gestione del campo durante i movimenti riferiti alla causale",;
    HelpContextID = 13213090,;
    cFormVar="w_CCFLGA08", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCCFLGA08_1_21.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCCFLGA08_1_21.GetRadio()
    this.Parent.oContained.w_CCFLGA08 = this.RadioValue()
    return .t.
  endfunc

  func oCCFLGA08_1_21.SetRadio()
    this.Parent.oContained.w_CCFLGA08=trim(this.Parent.oContained.w_CCFLGA08)
    this.value = ;
      iif(this.Parent.oContained.w_CCFLGA08=='S',1,;
      0)
  endfunc

  func oCCFLGA08_1_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPAMM<>'F')
    endwith
   endif
  endfunc

  add object oCCFLGA17_1_22 as StdCheck with uid="NUXTMCEZXG",rtseq=20,rtrep=.f.,left=287, top=74, caption="Movimenta quantit�",;
    ToolTipText = "Se attivo: campo gestito durante i movimenti riferiti alla causale",;
    HelpContextID = 13213091,;
    cFormVar="w_CCFLGA17", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCCFLGA17_1_22.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCCFLGA17_1_22.GetRadio()
    this.Parent.oContained.w_CCFLGA17 = this.RadioValue()
    return .t.
  endfunc

  func oCCFLGA17_1_22.SetRadio()
    this.Parent.oContained.w_CCFLGA17=trim(this.Parent.oContained.w_CCFLGA17)
    this.value = ;
      iif(this.Parent.oContained.w_CCFLGA17=='S',1,;
      0)
  endfunc


  add object oCCFLGOPE_1_23 as StdCombo with uid="KUYWHGXKVZ",rtseq=21,rtrep=.f.,left=444,top=76,width=90,height=21;
    , ToolTipText = "Se � gestita la quantit�, specifica come devono essere aggiornati i saldi";
    , HelpContextID = 46767509;
    , cFormVar="w_CCFLGOPE",RowSource=""+"Aggiunge,"+"Sottrae", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCCFLGOPE_1_23.RadioValue()
    return(iif(this.value =1,'+',;
    iif(this.value =2,'-',;
    space(1))))
  endfunc
  func oCCFLGOPE_1_23.GetRadio()
    this.Parent.oContained.w_CCFLGOPE = this.RadioValue()
    return .t.
  endfunc

  func oCCFLGOPE_1_23.SetRadio()
    this.Parent.oContained.w_CCFLGOPE=trim(this.Parent.oContained.w_CCFLGOPE)
    this.value = ;
      iif(this.Parent.oContained.w_CCFLGOPE=='+',1,;
      iif(this.Parent.oContained.w_CCFLGOPE=='-',2,;
      0))
  endfunc


  add object oObj_1_34 as cp_runprogram with uid="HVRNDLPMWI",left=-1, top=487, width=211,height=20,;
    caption='GSCE_BCC(C)',;
   bGlobalFont=.t.,;
    prg="GSCE_BCC('C')",;
    cEvent = "ControlliFinali",;
    nPag=1;
    , HelpContextID = 28448809

  add object oCCFLGA03_1_39 as StdCheck with uid="KHQYMREPMB",rtseq=36,rtrep=.f.,left=287, top=142, caption="Oneri e spese",;
    ToolTipText = "Se attivo: abilita la gestione del campo durante i movimenti riferiti alla causale",;
    HelpContextID = 13213095,;
    cFormVar="w_CCFLGA03", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCCFLGA03_1_39.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCCFLGA03_1_39.GetRadio()
    this.Parent.oContained.w_CCFLGA03 = this.RadioValue()
    return .t.
  endfunc

  func oCCFLGA03_1_39.SetRadio()
    this.Parent.oContained.w_CCFLGA03=trim(this.Parent.oContained.w_CCFLGA03)
    this.value = ;
      iif(this.Parent.oContained.w_CCFLGA03=='S',1,;
      0)
  endfunc

  func oCCFLGA03_1_39.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPAMM<>'C')
    endwith
   endif
  endfunc

  add object oCCFLGA02_1_40 as StdCheck with uid="XPOSISYCBS",rtseq=37,rtrep=.f.,left=287, top=119, caption="Incrementi di valore",;
    ToolTipText = "Se attivo: abilita la gestione del campo durante i movimenti riferiti alla causale",;
    HelpContextID = 13213096,;
    cFormVar="w_CCFLGA02", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCCFLGA02_1_40.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCCFLGA02_1_40.GetRadio()
    this.Parent.oContained.w_CCFLGA02 = this.RadioValue()
    return .t.
  endfunc

  func oCCFLGA02_1_40.SetRadio()
    this.Parent.oContained.w_CCFLGA02=trim(this.Parent.oContained.w_CCFLGA02)
    this.value = ;
      iif(this.Parent.oContained.w_CCFLGA02=='S',1,;
      0)
  endfunc

  func oCCFLGA02_1_40.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPAMM<>'C')
    endwith
   endif
  endfunc

  add object oCCFLGA04_1_41 as StdCheck with uid="AQVRCRLTXL",rtseq=38,rtrep=.f.,left=287, top=164, caption="Rivalutazioni",;
    ToolTipText = "Se attivo: abilita la gestione del campo durante i movimenti riferiti alla causale",;
    HelpContextID = 13213094,;
    cFormVar="w_CCFLGA04", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCCFLGA04_1_41.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCCFLGA04_1_41.GetRadio()
    this.Parent.oContained.w_CCFLGA04 = this.RadioValue()
    return .t.
  endfunc

  func oCCFLGA04_1_41.SetRadio()
    this.Parent.oContained.w_CCFLGA04=trim(this.Parent.oContained.w_CCFLGA04)
    this.value = ;
      iif(this.Parent.oContained.w_CCFLGA04=='S',1,;
      0)
  endfunc

  func oCCFLGA04_1_41.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPAMM<>'C')
    endwith
   endif
  endfunc

  add object oCCFLGA05_1_42 as StdCheck with uid="AERKGRYLSL",rtseq=39,rtrep=.f.,left=287, top=188, caption="Decrementi di valore",;
    ToolTipText = "Se attivo: abilita la gestione del campo durante i movimenti riferiti alla causale",;
    HelpContextID = 13213093,;
    cFormVar="w_CCFLGA05", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCCFLGA05_1_42.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCCFLGA05_1_42.GetRadio()
    this.Parent.oContained.w_CCFLGA05 = this.RadioValue()
    return .t.
  endfunc

  func oCCFLGA05_1_42.SetRadio()
    this.Parent.oContained.w_CCFLGA05=trim(this.Parent.oContained.w_CCFLGA05)
    this.value = ;
      iif(this.Parent.oContained.w_CCFLGA05=='S',1,;
      0)
  endfunc

  func oCCFLGA05_1_42.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPAMM<>'C')
    endwith
   endif
  endfunc

  add object oCCFLGA06_1_43 as StdCheck with uid="SYMMBWTLHI",rtseq=40,rtrep=.f.,left=287, top=211, caption="Svalutazioni",;
    ToolTipText = "Se attivo: abilita la gestione del campo durante i movimenti riferiti alla causale",;
    HelpContextID = 13213092,;
    cFormVar="w_CCFLGA06", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCCFLGA06_1_43.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCCFLGA06_1_43.GetRadio()
    this.Parent.oContained.w_CCFLGA06 = this.RadioValue()
    return .t.
  endfunc

  func oCCFLGA06_1_43.SetRadio()
    this.Parent.oContained.w_CCFLGA06=trim(this.Parent.oContained.w_CCFLGA06)
    this.value = ;
      iif(this.Parent.oContained.w_CCFLGA06=='S',1,;
      0)
  endfunc

  func oCCFLGA06_1_43.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPAMM<>'C')
    endwith
   endif
  endfunc

  add object oCCFLONFF_1_44 as StdCheck with uid="NZIFIPNIZM",rtseq=41,rtrep=.f.,left=415, top=142, caption="Oneri finanziari",;
    ToolTipText = "Se attivo: oneri finanziari per la stampa della nota integrativa",;
    HelpContextID = 213279340,;
    cFormVar="w_CCFLONFF", bObbl = .f. , nPag = 1;
    , tabstop=.f.;
   , bGlobalFont=.t.


  func oCCFLONFF_1_44.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCCFLONFF_1_44.GetRadio()
    this.Parent.oContained.w_CCFLONFF = this.RadioValue()
    return .t.
  endfunc

  func oCCFLONFF_1_44.SetRadio()
    this.Parent.oContained.w_CCFLONFF=trim(this.Parent.oContained.w_CCFLONFF)
    this.value = ;
      iif(this.Parent.oContained.w_CCFLONFF=='S',1,;
      0)
  endfunc

  func oCCFLONFF_1_44.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CCFLGA03='S' AND .w_TIPAMM<>'C')
    endwith
   endif
  endfunc

  add object oCCFLGA11_1_45 as StdCheck with uid="AILODLRAER",rtseq=42,rtrep=.f.,left=287, top=259, caption="Importo non amm.le",;
    ToolTipText = "Se attivo: abilita la gestione del campo durante i movimenti riferiti alla causale",;
    HelpContextID = 13213097,;
    cFormVar="w_CCFLGA11", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCCFLGA11_1_45.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCCFLGA11_1_45.GetRadio()
    this.Parent.oContained.w_CCFLGA11 = this.RadioValue()
    return .t.
  endfunc

  func oCCFLGA11_1_45.SetRadio()
    this.Parent.oContained.w_CCFLGA11=trim(this.Parent.oContained.w_CCFLGA11)
    this.value = ;
      iif(this.Parent.oContained.w_CCFLGA11=='S',1,;
      0)
  endfunc

  func oCCFLGA11_1_45.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPAMM<>'C')
    endwith
   endif
  endfunc

  add object oCCFLGA12_1_47 as StdCheck with uid="NGAOQBXNRM",rtseq=43,rtrep=.f.,left=287, top=280, caption="Quote perse",;
    ToolTipText = "Se attivo: abilita la gestione del campo durante i movimenti riferiti alla causale",;
    HelpContextID = 13213096,;
    cFormVar="w_CCFLGA12", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCCFLGA12_1_47.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCCFLGA12_1_47.GetRadio()
    this.Parent.oContained.w_CCFLGA12 = this.RadioValue()
    return .t.
  endfunc

  func oCCFLGA12_1_47.SetRadio()
    this.Parent.oContained.w_CCFLGA12=trim(this.Parent.oContained.w_CCFLGA12)
    this.value = ;
      iif(this.Parent.oContained.w_CCFLGA12=='S',1,;
      0)
  endfunc

  func oCCFLGA12_1_47.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPAMM<>'C')
    endwith
   endif
  endfunc

  add object oCCFLGA09_1_49 as StdCheck with uid="FNSDWXWHCB",rtseq=44,rtrep=.f.,left=287, top=332, caption="Accantonamenti ordinari",;
    ToolTipText = "Se attivo: abilita la gestione del campo durante i movimenti riferiti alla causale",;
    HelpContextID = 13213089,;
    cFormVar="w_CCFLGA09", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCCFLGA09_1_49.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCCFLGA09_1_49.GetRadio()
    this.Parent.oContained.w_CCFLGA09 = this.RadioValue()
    return .t.
  endfunc

  func oCCFLGA09_1_49.SetRadio()
    this.Parent.oContained.w_CCFLGA09=trim(this.Parent.oContained.w_CCFLGA09)
    this.value = ;
      iif(this.Parent.oContained.w_CCFLGA09=='S',1,;
      0)
  endfunc

  func oCCFLGA09_1_49.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPAMM<>'C')
    endwith
   endif
  endfunc

  add object oCCFLGA15_1_51 as StdCheck with uid="CLGUBHTCKX",rtseq=45,rtrep=.f.,left=287, top=356, caption="Accantonamenti anticipati",;
    ToolTipText = "Se attivo: abilita la gestione del campo durante i movimenti riferiti alla causale",;
    HelpContextID = 13213093,;
    cFormVar="w_CCFLGA15", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCCFLGA15_1_51.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCCFLGA15_1_51.GetRadio()
    this.Parent.oContained.w_CCFLGA15 = this.RadioValue()
    return .t.
  endfunc

  func oCCFLGA15_1_51.SetRadio()
    this.Parent.oContained.w_CCFLGA15=trim(this.Parent.oContained.w_CCFLGA15)
    this.value = ;
      iif(this.Parent.oContained.w_CCFLGA15=='S',1,;
      0)
  endfunc

  func oCCFLGA15_1_51.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPAMM<>'C')
    endwith
   endif
  endfunc

  add object oCCFLPRIC_1_52 as StdCheck with uid="GOBHGKRHRH",rtseq=46,rtrep=.f.,left=543, top=119, caption="Data primo utilizzo civile",;
    ToolTipText = "Se attivo: aggiorna la data di primo utilizzo civile del cespite",;
    HelpContextID = 255434135,;
    cFormVar="w_CCFLPRIC", bObbl = .f. , nPag = 1;
    , tabstop=.f.;
   , bGlobalFont=.t.


  func oCCFLPRIC_1_52.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCCFLPRIC_1_52.GetRadio()
    this.Parent.oContained.w_CCFLPRIC = this.RadioValue()
    return .t.
  endfunc

  func oCCFLPRIC_1_52.SetRadio()
    this.Parent.oContained.w_CCFLPRIC=trim(this.Parent.oContained.w_CCFLPRIC)
    this.value = ;
      iif(this.Parent.oContained.w_CCFLPRIC=='S',1,;
      0)
  endfunc

  func oCCFLPRIC_1_52.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPAMM<>'F')
    endwith
   endif
  endfunc

  add object oCCFLGA10_1_53 as StdCheck with uid="CVATAUVFNY",rtseq=47,rtrep=.f.,left=287, top=386, caption="Utilizzazioni ordinarie",;
    ToolTipText = "Se attivo: abilita la gestione del campo durante i movimenti riferiti alla causale",;
    HelpContextID = 13213098,;
    cFormVar="w_CCFLGA10", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCCFLGA10_1_53.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCCFLGA10_1_53.GetRadio()
    this.Parent.oContained.w_CCFLGA10 = this.RadioValue()
    return .t.
  endfunc

  func oCCFLGA10_1_53.SetRadio()
    this.Parent.oContained.w_CCFLGA10=trim(this.Parent.oContained.w_CCFLGA10)
    this.value = ;
      iif(this.Parent.oContained.w_CCFLGA10=='S',1,;
      0)
  endfunc

  func oCCFLGA10_1_53.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPAMM<>'C')
    endwith
   endif
  endfunc

  add object oCCFLGA13_1_54 as StdCheck with uid="LHORNAMHJL",rtseq=48,rtrep=.f.,left=543, top=386, caption="Plusvalenza",;
    ToolTipText = "Se attivo: campo gestito durante i movimenti riferiti alla causale",;
    HelpContextID = 13213095,;
    cFormVar="w_CCFLGA13", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCCFLGA13_1_54.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCCFLGA13_1_54.GetRadio()
    this.Parent.oContained.w_CCFLGA13 = this.RadioValue()
    return .t.
  endfunc

  func oCCFLGA13_1_54.SetRadio()
    this.Parent.oContained.w_CCFLGA13=trim(this.Parent.oContained.w_CCFLGA13)
    this.value = ;
      iif(this.Parent.oContained.w_CCFLGA13=='S',1,;
      0)
  endfunc

  func oCCFLGA13_1_54.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPAMM<>'C')
    endwith
   endif
  endfunc

  add object oCCFLGA16_1_55 as StdCheck with uid="UFFBQKQEFW",rtseq=49,rtrep=.f.,left=287, top=410, caption="Utilizzazioni anticipate",;
    ToolTipText = "Se attivo: abilita la gestione del campo durante i movimenti riferiti alla causale",;
    HelpContextID = 13213092,;
    cFormVar="w_CCFLGA16", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCCFLGA16_1_55.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCCFLGA16_1_55.GetRadio()
    this.Parent.oContained.w_CCFLGA16 = this.RadioValue()
    return .t.
  endfunc

  func oCCFLGA16_1_55.SetRadio()
    this.Parent.oContained.w_CCFLGA16=trim(this.Parent.oContained.w_CCFLGA16)
    this.value = ;
      iif(this.Parent.oContained.w_CCFLGA16=='S',1,;
      0)
  endfunc

  func oCCFLGA16_1_55.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPAMM<>'C')
    endwith
   endif
  endfunc

  add object oCCFLGA14_1_56 as StdCheck with uid="SUPEPTQOXK",rtseq=50,rtrep=.f.,left=543, top=410, caption="Minusvalenza",;
    ToolTipText = "Se attivo: campo gestito durante i movimenti riferiti alla causale",;
    HelpContextID = 13213094,;
    cFormVar="w_CCFLGA14", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCCFLGA14_1_56.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCCFLGA14_1_56.GetRadio()
    this.Parent.oContained.w_CCFLGA14 = this.RadioValue()
    return .t.
  endfunc

  func oCCFLGA14_1_56.SetRadio()
    this.Parent.oContained.w_CCFLGA14=trim(this.Parent.oContained.w_CCFLGA14)
    this.value = ;
      iif(this.Parent.oContained.w_CCFLGA14=='S',1,;
      0)
  endfunc

  func oCCFLGA14_1_56.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPAMM<>'C')
    endwith
   endif
  endfunc

  add object oCCDTINVA_1_58 as StdField with uid="OXOGCKNCFQ",rtseq=51,rtrep=.f.,;
    cFormVar = "w_CCDTINVA", cQueryName = "CCDTINVA",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di inizio validit�",;
    HelpContextID = 207503975,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=399, Top=451

  add object oCCDTOBSO_1_59 as StdField with uid="JTDSCVJBHX",rtseq=52,rtrep=.f.,;
    cFormVar = "w_CCDTOBSO", cQueryName = "CCDTOBSO",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di fine validit�",;
    HelpContextID = 12468853,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=623, Top=451


  add object oObj_1_60 as cp_runprogram with uid="EJJUAANWCQ",left=1, top=510, width=211,height=20,;
    caption='GSCE_BCC(D)',;
   bGlobalFont=.t.,;
    prg="GSCE_BCC('D')",;
    cEvent = "Delete start",;
    nPag=1;
    , HelpContextID = 28449065

  add object oStr_1_10 as StdString with uid="HWJAIMFFAT",Visible=.t., Left=15, Top=13,;
    Alignment=1, Width=59, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_11 as StdString with uid="ZURIGNSIOF",Visible=.t., Left=16, Top=43,;
    Alignment=1, Width=58, Height=15,;
    Caption="Riferim.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="EGTNAFBHLS",Visible=.t., Left=218, Top=455,;
    Alignment=1, Width=177, Height=15,;
    Caption="Inizio validit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="WVEDVTNTSZ",Visible=.t., Left=498, Top=454,;
    Alignment=1, Width=122, Height=15,;
    Caption="Data obsolescenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="AUIOZBGIKK",Visible=.t., Left=5, Top=98,;
    Alignment=0, Width=245, Height=18,;
    Caption="Valore del bene e variazioni civili"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_31 as StdString with uid="PEQOANOAPL",Visible=.t., Left=5, Top=311,;
    Alignment=0, Width=254, Height=18,;
    Caption="Accantonamenti e utilizzazioni civili"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_32 as StdString with uid="GDELJDMJFT",Visible=.t., Left=268, Top=234,;
    Alignment=0, Width=249, Height=18,;
    Caption="Importo non ammortizzabile e quote perse"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_35 as StdString with uid="MHKRDAGXYH",Visible=.t., Left=195, Top=43,;
    Alignment=1, Width=175, Height=15,;
    Caption="Test stampa libro cespiti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="UXSAWIUCMX",Visible=.t., Left=268, Top=98,;
    Alignment=0, Width=236, Height=18,;
    Caption="Valore del bene e variazioni fiscali"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_46 as StdString with uid="UIOFNSZUBE",Visible=.t., Left=5, Top=234,;
    Alignment=0, Width=208, Height=18,;
    Caption="Plusvalenze e minusvalenze civili"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_48 as StdString with uid="ZDXQBFIGUU",Visible=.t., Left=268, Top=311,;
    Alignment=0, Width=281, Height=18,;
    Caption="Accantonamenti e utilizzazioni fiscali"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_50 as StdString with uid="NUTDVMPHGN",Visible=.t., Left=533, Top=98,;
    Alignment=0, Width=171, Height=18,;
    Caption="Date di utilizzo\dismissione"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_57 as StdString with uid="WLSYGYHQRN",Visible=.t., Left=533, Top=356,;
    Alignment=0, Width=216, Height=18,;
    Caption="Plusvalenze e minusvalenze fiscali"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
enddefine
define class tgsce_accPag2 as StdContainer
  Width  = 765
  height = 477
  stdWidth  = 765
  stdheight = 477
  resizeXpos=654
  resizeYpos=439
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODI_2_1 as StdField with uid="CASYBPJPEG",rtseq=27,rtrep=.f.,;
    cFormVar = "w_CODI", cQueryName = "CODI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice della causale di movimentazione cespite",;
    HelpContextID = 104640986,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=65, Top=13, InputMask=replicate('X',5)

  add object oDESC_2_2 as StdField with uid="JHOQPSRCWH",rtseq=28,rtrep=.f.,;
    cFormVar = "w_DESC", cQueryName = "DESC",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione della causale",;
    HelpContextID = 104975306,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=126, Top=13, InputMask=replicate('X',40)


  add object oCCDATAGG_2_3 as StdCombo with uid="XESUMCEHYP",rtseq=29,rtrep=.f.,left=545,top=13,width=128,height=21;
    , HelpContextID = 268124781;
    , cFormVar="w_CCDATAGG",RowSource=""+"Ultima operazione,"+"Eserc. precedente", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oCCDATAGG_2_3.RadioValue()
    return(iif(this.value =1,'U',;
    iif(this.value =2,'E',;
    space(1))))
  endfunc
  func oCCDATAGG_2_3.GetRadio()
    this.Parent.oContained.w_CCDATAGG = this.RadioValue()
    return .t.
  endfunc

  func oCCDATAGG_2_3.SetRadio()
    this.Parent.oContained.w_CCDATAGG=trim(this.Parent.oContained.w_CCDATAGG)
    this.value = ;
      iif(this.Parent.oContained.w_CCDATAGG=='U',1,;
      iif(this.Parent.oContained.w_CCDATAGG=='E',2,;
      0))
  endfunc


  add object oLinkPC_2_4 as stdDynamicChildContainer with uid="UOPENIBIVL",left=1, top=293, width=672, height=184, bOnScreen=.t.;


  add object oStr_2_5 as StdString with uid="RAVYCNGWHM",Visible=.t., Left=421, Top=13,;
    Alignment=1, Width=120, Height=15,;
    Caption="Dati aggiornati a:"  ;
  , bGlobalFont=.t.

  add object oStr_2_6 as StdString with uid="MMZBJRJWWA",Visible=.t., Left=4, Top=57,;
    Alignment=0, Width=22, Height=15,;
    Caption="V01"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_7 as StdString with uid="TXGMLNBSZA",Visible=.t., Left=4, Top=77,;
    Alignment=0, Width=22, Height=15,;
    Caption="V02"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_8 as StdString with uid="ALYEGVPKAP",Visible=.t., Left=4, Top=97,;
    Alignment=0, Width=22, Height=15,;
    Caption="V03"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_9 as StdString with uid="KFJVZYIXDF",Visible=.t., Left=4, Top=117,;
    Alignment=0, Width=22, Height=15,;
    Caption="V04"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_10 as StdString with uid="VTTFZECZNF",Visible=.t., Left=4, Top=137,;
    Alignment=0, Width=22, Height=15,;
    Caption="V05"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_11 as StdString with uid="ADZEKSCJBD",Visible=.t., Left=257, Top=57,;
    Alignment=0, Width=22, Height=15,;
    Caption="V06"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_12 as StdString with uid="KJJUFKRZVJ",Visible=.t., Left=257, Top=97,;
    Alignment=0, Width=22, Height=15,;
    Caption="V07"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_13 as StdString with uid="MNKQIVUOSO",Visible=.t., Left=257, Top=117,;
    Alignment=0, Width=22, Height=15,;
    Caption="V08"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_14 as StdString with uid="UAEOJFCAGZ",Visible=.t., Left=257, Top=137,;
    Alignment=0, Width=22, Height=15,;
    Caption="V09"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_15 as StdString with uid="UCPWKCDIEA",Visible=.t., Left=483, Top=57,;
    Alignment=0, Width=24, Height=15,;
    Caption="V10"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_16 as StdString with uid="PPANRHOYUE",Visible=.t., Left=33, Top=57,;
    Alignment=0, Width=168, Height=18,;
    Caption="Valore del bene fiscale"  ;
  , bGlobalFont=.t.

  add object oStr_2_17 as StdString with uid="BZEZWIHUOH",Visible=.t., Left=33, Top=77,;
    Alignment=0, Width=212, Height=18,;
    Caption="Valore fiscale componenti selezionati"  ;
  , bGlobalFont=.t.

  add object oStr_2_18 as StdString with uid="HTSIWSCDPA",Visible=.t., Left=33, Top=97,;
    Alignment=0, Width=187, Height=15,;
    Caption="Totale accantonato civile"  ;
  , bGlobalFont=.t.

  add object oStr_2_19 as StdString with uid="QTOXBNBKUT",Visible=.t., Left=33, Top=137,;
    Alignment=0, Width=187, Height=15,;
    Caption="Valore non ammortizzabile"  ;
  , bGlobalFont=.t.

  add object oStr_2_20 as StdString with uid="PXFWNZINEG",Visible=.t., Left=286, Top=57,;
    Alignment=0, Width=187, Height=18,;
    Caption="Totale accantonato ordinario"  ;
  , bGlobalFont=.t.

  add object oStr_2_21 as StdString with uid="MLEXTUWLKH",Visible=.t., Left=512, Top=57,;
    Alignment=0, Width=170, Height=15,;
    Caption="Quota persa"  ;
  , bGlobalFont=.t.

  add object oStr_2_22 as StdString with uid="KZUTVYFTRZ",Visible=.t., Left=33, Top=117,;
    Alignment=0, Width=187, Height=15,;
    Caption="Residuo da ammortam.civile"  ;
  , bGlobalFont=.t.

  add object oStr_2_23 as StdString with uid="YIAAETKFQZ",Visible=.t., Left=286, Top=97,;
    Alignment=0, Width=187, Height=15,;
    Caption="Residuo da ammortam.fiscale"  ;
  , bGlobalFont=.t.

  add object oStr_2_24 as StdString with uid="QMBVXJMSQT",Visible=.t., Left=286, Top=117,;
    Alignment=0, Width=187, Height=15,;
    Caption="Quota accantonam.civile"  ;
  , bGlobalFont=.t.

  add object oStr_2_25 as StdString with uid="VBVXZAFPIQ",Visible=.t., Left=286, Top=137,;
    Alignment=0, Width=187, Height=18,;
    Caption="Quota accantonam.ordinaria"  ;
  , bGlobalFont=.t.

  add object oStr_2_26 as StdString with uid="DVHRLKATTO",Visible=.t., Left=2, Top=13,;
    Alignment=1, Width=59, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_27 as StdString with uid="QRUCEXXDZO",Visible=.t., Left=3, Top=35,;
    Alignment=0, Width=97, Height=15,;
    Caption="Saldi cespiti"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_29 as StdString with uid="QAYPLKWHRQ",Visible=.t., Left=3, Top=158,;
    Alignment=0, Width=137, Height=15,;
    Caption="Movimenti cespiti"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_31 as StdString with uid="RGVCVNYPSS",Visible=.t., Left=483, Top=77,;
    Alignment=0, Width=22, Height=15,;
    Caption="V11"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_32 as StdString with uid="DGASCKXMNI",Visible=.t., Left=512, Top=77,;
    Alignment=0, Width=170, Height=15,;
    Caption="% Deducibilit�"  ;
  , bGlobalFont=.t.

  add object oStr_2_33 as StdString with uid="SFMZXDWOKQ",Visible=.t., Left=483, Top=97,;
    Alignment=0, Width=22, Height=15,;
    Caption="V12"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_34 as StdString with uid="PHKEMRDMYU",Visible=.t., Left=512, Top=97,;
    Alignment=0, Width=170, Height=15,;
    Caption="Limite deducibilit�"  ;
  , bGlobalFont=.t.

  add object oStr_2_35 as StdString with uid="XBUQJLROGC",Visible=.t., Left=257, Top=77,;
    Alignment=0, Width=26, Height=18,;
    Caption="V14"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_36 as StdString with uid="LVWFCLZDHZ",Visible=.t., Left=286, Top=77,;
    Alignment=0, Width=187, Height=18,;
    Caption="Totale accantonato anticipato"  ;
  , bGlobalFont=.t.

  add object oStr_2_37 as StdString with uid="DNEUZXRDIH",Visible=.t., Left=483, Top=117,;
    Alignment=0, Width=22, Height=18,;
    Caption="V13"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_38 as StdString with uid="YSBRYHVVQB",Visible=.t., Left=510, Top=117,;
    Alignment=0, Width=170, Height=15,;
    Caption="Quota accantonam.anticipata"  ;
  , bGlobalFont=.t.

  add object oStr_2_39 as StdString with uid="GCWIPILETM",Visible=.t., Left=483, Top=137,;
    Alignment=0, Width=22, Height=18,;
    Caption="V15"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_40 as StdString with uid="HCRMURFLAG",Visible=.t., Left=510, Top=137,;
    Alignment=0, Width=170, Height=15,;
    Caption="Quantit� residua"  ;
  , bGlobalFont=.t.

  add object oStr_2_41 as StdString with uid="GAHDCCWBMX",Visible=.t., Left=257, Top=156,;
    Alignment=0, Width=22, Height=18,;
    Caption="V16"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_42 as StdString with uid="NSHOEVZPPJ",Visible=.t., Left=286, Top=157,;
    Alignment=0, Width=187, Height=16,;
    Caption="Valore del bene civile"  ;
  , bGlobalFont=.t.

  add object oStr_2_43 as StdString with uid="VXAXJLWWAI",Visible=.t., Left=483, Top=156,;
    Alignment=0, Width=22, Height=15,;
    Caption="V17"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_44 as StdString with uid="YPYEYGKENO",Visible=.t., Left=509, Top=156,;
    Alignment=0, Width=192, Height=15,;
    Caption="Valore civile componenti selez."  ;
  , bGlobalFont=.t.

  add object oStr_2_45 as StdString with uid="MIVMYDYNYU",Visible=.t., Left=27, Top=178,;
    Alignment=0, Width=89, Height=15,;
    Caption="Importo vendita"  ;
  , bGlobalFont=.t.

  add object oStr_2_46 as StdString with uid="BFOGXUWBDC",Visible=.t., Left=309, Top=198,;
    Alignment=0, Width=118, Height=15,;
    Caption="Incrementi di valore"  ;
  , bGlobalFont=.t.

  add object oStr_2_47 as StdString with uid="BCHCGIWIUU",Visible=.t., Left=309, Top=218,;
    Alignment=0, Width=100, Height=15,;
    Caption="Oneri e spese"  ;
  , bGlobalFont=.t.

  add object oStr_2_48 as StdString with uid="CJIHDGBNNL",Visible=.t., Left=309, Top=258,;
    Alignment=0, Width=76, Height=15,;
    Caption="Rivalutazioni"  ;
  , bGlobalFont=.t.

  add object oStr_2_49 as StdString with uid="APZFOEJZYV",Visible=.t., Left=309, Top=238,;
    Alignment=0, Width=124, Height=15,;
    Caption="Decrementi di valore"  ;
  , bGlobalFont=.t.

  add object oStr_2_50 as StdString with uid="EWNNPQBUVU",Visible=.t., Left=309, Top=278,;
    Alignment=0, Width=84, Height=15,;
    Caption="Svalutazioni"  ;
  , bGlobalFont=.t.

  add object oStr_2_51 as StdString with uid="QGQZJMHZYN",Visible=.t., Left=183, Top=258,;
    Alignment=0, Width=95, Height=15,;
    Caption="Accantonamenti"  ;
  , bGlobalFont=.t.

  add object oStr_2_52 as StdString with uid="OBPIPKJWCT",Visible=.t., Left=183, Top=278,;
    Alignment=0, Width=95, Height=15,;
    Caption="Utilizzazioni"  ;
  , bGlobalFont=.t.

  add object oStr_2_53 as StdString with uid="VPAZVLMGTK",Visible=.t., Left=466, Top=258,;
    Alignment=0, Width=142, Height=15,;
    Caption="Accantonamenti ordinari"  ;
  , bGlobalFont=.t.

  add object oStr_2_54 as StdString with uid="AWLAAGBDCX",Visible=.t., Left=466, Top=278,;
    Alignment=0, Width=147, Height=15,;
    Caption="Utilizzazioni ordinarie"  ;
  , bGlobalFont=.t.

  add object oStr_2_55 as StdString with uid="RCWDDJZKPS",Visible=.t., Left=466, Top=238,;
    Alignment=0, Width=79, Height=15,;
    Caption="Quote perse"  ;
  , bGlobalFont=.t.

  add object oStr_2_56 as StdString with uid="URADKBTKAX",Visible=.t., Left=466, Top=178,;
    Alignment=0, Width=75, Height=15,;
    Caption="Plusvalenza"  ;
  , bGlobalFont=.t.

  add object oStr_2_57 as StdString with uid="HKFZJBHDVJ",Visible=.t., Left=466, Top=198,;
    Alignment=0, Width=117, Height=15,;
    Caption="Minusvalenza"  ;
  , bGlobalFont=.t.

  add object oStr_2_58 as StdString with uid="DLKNBESYWH",Visible=.t., Left=2, Top=178,;
    Alignment=0, Width=22, Height=15,;
    Caption="A01"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_59 as StdString with uid="BFAMNDKIVS",Visible=.t., Left=283, Top=198,;
    Alignment=0, Width=22, Height=15,;
    Caption="A02"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_60 as StdString with uid="CNNIONNBGJ",Visible=.t., Left=283, Top=218,;
    Alignment=0, Width=22, Height=15,;
    Caption="A03"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_61 as StdString with uid="NHAWTJAXQT",Visible=.t., Left=283, Top=258,;
    Alignment=0, Width=22, Height=15,;
    Caption="A04"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_62 as StdString with uid="CAMBGKVWDD",Visible=.t., Left=283, Top=238,;
    Alignment=0, Width=22, Height=15,;
    Caption="A05"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_63 as StdString with uid="LSWUQYPQMS",Visible=.t., Left=283, Top=278,;
    Alignment=0, Width=22, Height=15,;
    Caption="A06"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_64 as StdString with uid="BXUFKPEHWE",Visible=.t., Left=156, Top=258,;
    Alignment=0, Width=22, Height=15,;
    Caption="A07"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_65 as StdString with uid="ZVOQMGNXZY",Visible=.t., Left=156, Top=278,;
    Alignment=0, Width=22, Height=15,;
    Caption="A08"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_66 as StdString with uid="UZMYBAVFNT",Visible=.t., Left=440, Top=258,;
    Alignment=0, Width=22, Height=15,;
    Caption="A09"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_67 as StdString with uid="MNETITPPFC",Visible=.t., Left=440, Top=278,;
    Alignment=0, Width=22, Height=15,;
    Caption="A10"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_68 as StdString with uid="WTJPODLPTH",Visible=.t., Left=440, Top=218,;
    Alignment=0, Width=22, Height=15,;
    Caption="A11"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_69 as StdString with uid="YLGGEPPTFM",Visible=.t., Left=440, Top=238,;
    Alignment=0, Width=22, Height=15,;
    Caption="A12"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_70 as StdString with uid="YJTEIPWKUY",Visible=.t., Left=440, Top=178,;
    Alignment=0, Width=22, Height=15,;
    Caption="A13"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_71 as StdString with uid="ZBPNAKRIPR",Visible=.t., Left=440, Top=198,;
    Alignment=0, Width=22, Height=15,;
    Caption="A14"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_72 as StdString with uid="VXBWZDTNPE",Visible=.t., Left=466, Top=218,;
    Alignment=0, Width=174, Height=15,;
    Caption="Importo non amm.le"  ;
  , bGlobalFont=.t.

  add object oStr_2_73 as StdString with uid="TXLKPXBFHM",Visible=.t., Left=613, Top=178,;
    Alignment=0, Width=148, Height=15,;
    Caption="Accantonamenti anticipati"  ;
  , bGlobalFont=.t.

  add object oStr_2_74 as StdString with uid="GERERSIVLE",Visible=.t., Left=586, Top=178,;
    Alignment=0, Width=22, Height=15,;
    Caption="A15"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_75 as StdString with uid="RFIMDTMDAC",Visible=.t., Left=586, Top=198,;
    Alignment=0, Width=22, Height=15,;
    Caption="A16"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_76 as StdString with uid="YHGSJBJXZF",Visible=.t., Left=613, Top=198,;
    Alignment=0, Width=148, Height=15,;
    Caption="Utilizzazioni anticipate"  ;
  , bGlobalFont=.t.

  add object oStr_2_77 as StdString with uid="MXVHHHULWB",Visible=.t., Left=1, Top=198,;
    Alignment=0, Width=22, Height=15,;
    Caption="A17"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_78 as StdString with uid="RSHCBTNTSY",Visible=.t., Left=27, Top=198,;
    Alignment=0, Width=125, Height=15,;
    Caption="Quantit� movimentata"  ;
  , bGlobalFont=.t.

  add object oStr_2_79 as StdString with uid="JJCTWQJHJQ",Visible=.t., Left=183, Top=178,;
    Alignment=0, Width=95, Height=15,;
    Caption="Rivalutazioni"  ;
  , bGlobalFont=.t.

  add object oStr_2_80 as StdString with uid="FFNBGODFEP",Visible=.t., Left=156, Top=178,;
    Alignment=0, Width=22, Height=15,;
    Caption="A18"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_81 as StdString with uid="WYGVYLGMSC",Visible=.t., Left=183, Top=198,;
    Alignment=0, Width=95, Height=15,;
    Caption="Svalutazioni"  ;
  , bGlobalFont=.t.

  add object oStr_2_82 as StdString with uid="RDBBWOSLJP",Visible=.t., Left=155, Top=198,;
    Alignment=0, Width=22, Height=15,;
    Caption="A19"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_83 as StdString with uid="ZMGGCLWYAM",Visible=.t., Left=183, Top=218,;
    Alignment=0, Width=95, Height=15,;
    Caption="Plusvalenza"  ;
  , bGlobalFont=.t.

  add object oStr_2_84 as StdString with uid="GORHBWXNCP",Visible=.t., Left=156, Top=218,;
    Alignment=0, Width=22, Height=15,;
    Caption="A20"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_85 as StdString with uid="KRYMKRFNKV",Visible=.t., Left=183, Top=238,;
    Alignment=0, Width=95, Height=15,;
    Caption="Minusvalenza"  ;
  , bGlobalFont=.t.

  add object oStr_2_86 as StdString with uid="OVTCNTABEC",Visible=.t., Left=156, Top=238,;
    Alignment=0, Width=22, Height=15,;
    Caption="A21"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_87 as StdString with uid="WOMUWVIUHM",Visible=.t., Left=1, Top=278,;
    Alignment=0, Width=22, Height=15,;
    Caption="A22"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_88 as StdString with uid="AQGLRHLZPA",Visible=.t., Left=27, Top=278,;
    Alignment=0, Width=124, Height=15,;
    Caption="Decrementi di valore"  ;
  , bGlobalFont=.t.

  add object oStr_2_89 as StdString with uid="UDWMRDGTUW",Visible=.t., Left=2, Top=217,;
    Alignment=0, Width=100, Height=15,;
    Caption="Valori civili"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_90 as StdString with uid="MUXDENWHLS",Visible=.t., Left=27, Top=238,;
    Alignment=0, Width=118, Height=15,;
    Caption="Incrementi di valore"  ;
  , bGlobalFont=.t.

  add object oStr_2_91 as StdString with uid="YPGWVZQBMT",Visible=.t., Left=1, Top=238,;
    Alignment=0, Width=22, Height=15,;
    Caption="A23"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_92 as StdString with uid="DKHGCVFXXT",Visible=.t., Left=27, Top=258,;
    Alignment=0, Width=118, Height=15,;
    Caption="Oneri e spese"  ;
  , bGlobalFont=.t.

  add object oStr_2_93 as StdString with uid="SLOQFKBQHY",Visible=.t., Left=1, Top=258,;
    Alignment=0, Width=22, Height=15,;
    Caption="A24"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_94 as StdString with uid="SSGBMLZYWQ",Visible=.t., Left=283, Top=178,;
    Alignment=0, Width=100, Height=15,;
    Caption="Valori fiscali"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_2_28 as StdBox with uid="FIULEIFINN",left=-1, top=53, width=764,height=1

  add object oBox_2_30 as StdBox with uid="WKBMBMYMOR",left=-1, top=175, width=764,height=1
enddefine
define class tgsce_accPag3 as StdContainer
  Width  = 765
  height = 477
  stdWidth  = 765
  stdheight = 477
  resizeXpos=434
  resizeYpos=327
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODI_3_1 as StdField with uid="DOXOTGOLPQ",rtseq=30,rtrep=.f.,;
    cFormVar = "w_CODI", cQueryName = "CODI",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice della causale di movimentazione cespite",;
    HelpContextID = 104640986,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=65, Top=13, InputMask=replicate('X',5)

  add object oDESC_3_2 as StdField with uid="VRXSMCXUOD",rtseq=31,rtrep=.f.,;
    cFormVar = "w_DESC", cQueryName = "DESC",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione della causale",;
    HelpContextID = 104975306,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=136, Top=13, InputMask=replicate('X',40)

  add object oCCFLCONT_3_3 as StdRadio with uid="LSXNRGBSRN",rtseq=32,rtrep=.f.,left=152, top=56, width=53,height=59;
    , ToolTipText = "Abilita o meno la contabilizzazione dei movimenti registrati con la causale cesp";
    , cFormVar="w_CCFLCONT", ButtonCount=2, bObbl=.f., nPag=3;
  , bGlobalFont=.t.

    proc oCCFLCONT_3_3.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Si"
      this.Buttons(1).HelpContextID = 50961798
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="No"
      this.Buttons(2).HelpContextID = 50961798
      this.Buttons(2).Top=28
      this.SetAll("Width",51)
      this.SetAll("Height",30)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Abilita o meno la contabilizzazione dei movimenti registrati con la causale cesp")
      StdRadio::init()
    endproc

  func oCCFLCONT_3_3.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    space(1))))
  endfunc
  func oCCFLCONT_3_3.GetRadio()
    this.Parent.oContained.w_CCFLCONT = this.RadioValue()
    return .t.
  endfunc

  func oCCFLCONT_3_3.SetRadio()
    this.Parent.oContained.w_CCFLCONT=trim(this.Parent.oContained.w_CCFLCONT)
    this.value = ;
      iif(this.Parent.oContained.w_CCFLCONT=='S',1,;
      iif(this.Parent.oContained.w_CCFLCONT=='N',2,;
      0))
  endfunc

  add object oCCCAUCON_3_5 as StdField with uid="HJCIQFUMFD",rtseq=33,rtrep=.f.,;
    cFormVar = "w_CCCAUCON", cQueryName = "CCCAUCON",;
    bObbl = .t. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale di contabilit� generale utilizzata per la contabilizzazione del movimen",;
    HelpContextID = 234147212,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=353, Top=56, cSayPict="'!!!!!'", cGetPict="'!!!!!'", InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_CCCAUCON"

  func oCCCAUCON_3_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_COGE='S' AND .w_CCFLCONT='S')
    endwith
   endif
  endfunc

  func oCCCAUCON_3_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oCCCAUCON_3_5.ecpDrop(oSource)
    this.Parent.oContained.link_3_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCCCAUCON_3_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oCCCAUCON_3_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"Causali contabili",'',this.parent.oContained
  endproc
  proc oCCCAUCON_3_5.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_CCCAUCON
     i_obj.ecpSave()
  endproc

  add object oDESCAU_3_6 as StdField with uid="PPDXGJGAUW",rtseq=34,rtrep=.f.,;
    cFormVar = "w_DESCAU", cQueryName = "DESCAU",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 221367242,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=420, Top=56, InputMask=replicate('X',35)


  add object oLinkPC_3_8 as stdDynamicChildContainer with uid="KOEZZKGOXU",left=3, top=114, width=684, height=245, bOnScreen=.t.;


  func oLinkPC_3_8.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CCFLCONT='S')
      endwith
    endif
  endfunc

  add object oStr_3_4 as StdString with uid="UCEAHKEXIK",Visible=.t., Left=1, Top=13,;
    Alignment=1, Width=60, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_7 as StdString with uid="HNCVGFRPBL",Visible=.t., Left=214, Top=56,;
    Alignment=1, Width=135, Height=15,;
    Caption="Causale contabile:"  ;
  , bGlobalFont=.t.

  add object oStr_3_9 as StdString with uid="SJCNEKNEPC",Visible=.t., Left=39, Top=56,;
    Alignment=1, Width=109, Height=15,;
    Caption="Contabilizza:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsce_acc','CAU_CESP','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".CCCODICE=CAU_CESP.CCCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
