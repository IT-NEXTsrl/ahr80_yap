* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_bco                                                        *
*              Valuta componenti                                               *
*                                                                              *
*      Author: ZUCCHETTI TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_20]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-09-06                                                      *
* Last revis.: 2000-09-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsce_bco",oParentObject,m.pOper)
return(i_retval)

define class tgsce_bco as StdBatch
  * --- Local variables
  pOper = space(1)
  w_SERIAL = space(10)
  w_CODCES = space(20)
  w_OK = .f.
  w_MESS = space(50)
  w_PASSO = .f.
  * --- WorkFile variables
  MOV_PCES_idx=0
  MOV_CESP_idx=0
  MOV_SCES_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Messaggistica di errore nel caso venga cambiata la valuta all'interno dei componenti.
    this.w_OK = .T.
    this.w_PASSO = .F.
    do case
      case this.pOper="A"
        * --- Controllo di essere in variazione.
        if This.Oparentobject.Oparentobject.cFunction="Edit"
          this.w_MESS = "Il valore dei componenti non verr� riconvertito"
          ah_ErrorMsg(this.w_MESS)
        endif
      case this.pOper="B"
        * --- Select from MOV_PCES
        i_nConn=i_TableProp[this.MOV_PCES_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MOV_PCES_idx,2],.t.,this.MOV_PCES_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select MCSERIAL  from "+i_cTable+" MOV_PCES ";
              +" where MCCOMPON="+cp_ToStrODBC(this.oParentObject.w_COCOMPON)+"";
               ,"_Curs_MOV_PCES")
        else
          select MCSERIAL from (i_cTable);
           where MCCOMPON=this.oParentObject.w_COCOMPON;
            into cursor _Curs_MOV_PCES
        endif
        if used('_Curs_MOV_PCES')
          select _Curs_MOV_PCES
          locate for 1=1
          do while not(eof())
          * --- Controllo se esiste almeno un movimento di carico del componente
          this.w_SERIAL = _Curs_MOV_PCES.MCSERIAL
          if NOT EMPTY(NVL(this.w_SERIAL,SPACE(10)))
            * --- Read from MOV_CESP
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.MOV_CESP_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.MOV_CESP_idx,2],.t.,this.MOV_CESP_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "MCCODCES"+;
                " from "+i_cTable+" MOV_CESP where ";
                    +"MCSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                MCCODCES;
                from (i_cTable) where;
                    MCSERIAL = this.w_SERIAL;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_CODCES = NVL(cp_ToDate(_read_.MCCODCES),cp_NullValue(_read_.MCCODCES))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if this.w_CODCES=this.oParentObject.w_COCODICE
              this.w_PASSO = .T.
            endif
          endif
            select _Curs_MOV_PCES
            continue
          enddo
          use
        endif
        if this.w_PASSO=.F.
          * --- Select from MOV_SCES
          i_nConn=i_TableProp[this.MOV_SCES_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MOV_SCES_idx,2],.t.,this.MOV_SCES_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select SCSERIAL  from "+i_cTable+" MOV_SCES ";
                +" where SCCOMPON="+cp_ToStrODBC(this.oParentObject.w_COCOMPON)+"";
                 ,"_Curs_MOV_SCES")
          else
            select SCSERIAL from (i_cTable);
             where SCCOMPON=this.oParentObject.w_COCOMPON;
              into cursor _Curs_MOV_SCES
          endif
          if used('_Curs_MOV_SCES')
            select _Curs_MOV_SCES
            locate for 1=1
            do while not(eof())
            * --- Controllo se esiste almeno un movimento di scarico del componente
            this.w_SERIAL = _Curs_MOV_SCES.SCSERIAL
            if NOT EMPTY(NVL(this.w_SERIAL,SPACE(10)))
              * --- Read from MOV_CESP
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.MOV_CESP_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.MOV_CESP_idx,2],.t.,this.MOV_CESP_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "MCCODCES"+;
                  " from "+i_cTable+" MOV_CESP where ";
                      +"MCSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  MCCODCES;
                  from (i_cTable) where;
                      MCSERIAL = this.w_SERIAL;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_CODCES = NVL(cp_ToDate(_read_.MCCODCES),cp_NullValue(_read_.MCCODCES))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              if this.w_CODCES=this.oParentObject.w_COCODICE
                this.w_PASSO = .T.
              endif
            endif
              select _Curs_MOV_SCES
              continue
            enddo
            use
          endif
        endif
        if this.w_PASSO=.T.
          this.w_MESS = "Attenzione: componente movimentato%0Impossibile cancellare"
          ah_ErrorMsg(this.w_MESS)
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=MSG_TRANSACTION_ERROR
        endif
    endcase
  endproc


  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='MOV_PCES'
    this.cWorkTables[2]='MOV_CESP'
    this.cWorkTables[3]='MOV_SCES'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_MOV_PCES')
      use in _Curs_MOV_PCES
    endif
    if used('_Curs_MOV_SCES')
      use in _Curs_MOV_SCES
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
