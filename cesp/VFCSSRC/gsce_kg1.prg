* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_kg1                                                        *
*              Numero piano di accantonamento                                  *
*                                                                              *
*      Author: ZUCCHETTI TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_20]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-06-01                                                      *
* Last revis.: 2010-05-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsce_kg1",oParentObject))

* --- Class definition
define class tgsce_kg1 as StdForm
  Top    = 50
  Left   = 100

  * --- Standard Properties
  Width  = 366
  Height = 181
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2010-05-24"
  HelpContextID=236305047
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=11

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsce_kg1"
  cComment = "Numero piano di accantonamento"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_PANUMREG = space(6)
  w_PACODESE = space(4)
  w_PADATREG = ctod('  /  /  ')
  w_PADESCRI = space(40)
  w_PASTATO = space(1)
  w_PAFLDEFI = space(1)
  w_PANUMFRA = 0
  w_PACODCAT = space(15)
  w_PATIPGEN = space(1)
  w_PADATINI = ctod('  /  /  ')
  w_PADATFIN = ctod('  /  /  ')
  w_ragru = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsce_kg1Pag1","gsce_kg1",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ragru = this.oPgFrm.Pages(1).oPag.ragru
    DoDefault()
    proc Destroy()
      this.w_ragru = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PANUMREG=space(6)
      .w_PACODESE=space(4)
      .w_PADATREG=ctod("  /  /  ")
      .w_PADESCRI=space(40)
      .w_PASTATO=space(1)
      .w_PAFLDEFI=space(1)
      .w_PANUMFRA=0
      .w_PACODCAT=space(15)
      .w_PATIPGEN=space(1)
      .w_PADATINI=ctod("  /  /  ")
      .w_PADATFIN=ctod("  /  /  ")
      .w_PANUMREG=oParentObject.w_PANUMREG
      .w_PACODESE=oParentObject.w_PACODESE
      .w_PADATREG=oParentObject.w_PADATREG
      .w_PADESCRI=oParentObject.w_PADESCRI
      .w_PASTATO=oParentObject.w_PASTATO
      .w_PAFLDEFI=oParentObject.w_PAFLDEFI
      .w_PANUMFRA=oParentObject.w_PANUMFRA
      .w_PACODCAT=oParentObject.w_PACODCAT
      .w_PATIPGEN=oParentObject.w_PATIPGEN
      .w_PADATINI=oParentObject.w_PADATINI
      .w_PADATFIN=oParentObject.w_PADATFIN
      .oPgFrm.Page1.oPag.ragru.Calculate(.F.)
        .w_PANUMREG = NVL(.w_ragru.getVar('PANUMREG'), SPACE(6))
        .w_PACODESE = NVL(.w_ragru.getVar('PACODESE'), SPACE(4))
        .w_PADATREG = Nvl( .w_ragru.getVar('PADATREG') , cp_CharToDate('  /  /    '))
        .w_PADESCRI = nvl(.w_ragru.getVar('PADESCRI'), SPACE(10))
      .oPgFrm.Page1.oPag.oObj_1_6.Calculate()
        .w_PASTATO = NVL(.w_ragru.getVar('PA_STATO'), ' ')
        .w_PAFLDEFI = NVL(.w_ragru.getVar('PAFLDEFI'), ' ')
        .w_PANUMFRA = NVL(.w_ragru.getVar('PANUMFRA'), 0)
        .w_PACODCAT = NVL(.w_ragru.getVar('PACODCAT'), SPACE(15))
        .w_PATIPGEN = NVL(.w_ragru.getVar('PATIPGEN'), 'D')
        .w_PADATINI = Nvl( .w_ragru.getVar('PADATINI') , cp_CharToDate('  /  /    '))
        .w_PADATFIN = Nvl( .w_ragru.getVar('PADATFIN') , cp_CharToDate('  /  /    '))
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_PANUMREG=.w_PANUMREG
      .oParentObject.w_PACODESE=.w_PACODESE
      .oParentObject.w_PADATREG=.w_PADATREG
      .oParentObject.w_PADESCRI=.w_PADESCRI
      .oParentObject.w_PASTATO=.w_PASTATO
      .oParentObject.w_PAFLDEFI=.w_PAFLDEFI
      .oParentObject.w_PANUMFRA=.w_PANUMFRA
      .oParentObject.w_PACODCAT=.w_PACODCAT
      .oParentObject.w_PATIPGEN=.w_PATIPGEN
      .oParentObject.w_PADATINI=.w_PADATINI
      .oParentObject.w_PADATFIN=.w_PADATFIN
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.ragru.Calculate(.F.)
            .w_PANUMREG = NVL(.w_ragru.getVar('PANUMREG'), SPACE(6))
            .w_PACODESE = NVL(.w_ragru.getVar('PACODESE'), SPACE(4))
            .w_PADATREG = Nvl( .w_ragru.getVar('PADATREG') , cp_CharToDate('  /  /    '))
            .w_PADESCRI = nvl(.w_ragru.getVar('PADESCRI'), SPACE(10))
        .oPgFrm.Page1.oPag.oObj_1_6.Calculate()
            .w_PASTATO = NVL(.w_ragru.getVar('PA_STATO'), ' ')
            .w_PAFLDEFI = NVL(.w_ragru.getVar('PAFLDEFI'), ' ')
            .w_PANUMFRA = NVL(.w_ragru.getVar('PANUMFRA'), 0)
            .w_PACODCAT = NVL(.w_ragru.getVar('PACODCAT'), SPACE(15))
            .w_PATIPGEN = NVL(.w_ragru.getVar('PATIPGEN'), 'D')
            .w_PADATINI = Nvl( .w_ragru.getVar('PADATINI') , cp_CharToDate('  /  /    '))
            .w_PADATFIN = Nvl( .w_ragru.getVar('PADATFIN') , cp_CharToDate('  /  /    '))
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ragru.Calculate(.F.)
        .oPgFrm.Page1.oPag.oObj_1_6.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ragru.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_6.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsce_kg1Pag1 as StdContainer
  Width  = 362
  height = 181
  stdWidth  = 362
  stdheight = 181
  resizeXpos=211
  resizeYpos=116
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object ragru as cp_zoombox with uid="TYJAADILOE",left=2, top=3, width=357,height=177,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable='CES_PIAN',cZoomFile='GSCE_KG1',bOptions=.F.,bAdvOptions=.F.,;
    nPag=1;
    , HelpContextID = 122568218


  add object oObj_1_6 as cp_runprogram with uid="FEMCRBOSZH",left=-1, top=186, width=257,height=18,;
    caption='GSCE_BOO(X)',;
   bGlobalFont=.t.,;
    prg="GSCE_BOO('X')",;
    cEvent = "w_ragru selected",;
    nPag=1;
    , HelpContextID = 162386635
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsce_kg1','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
