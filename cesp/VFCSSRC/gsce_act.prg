* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_act                                                        *
*              Categorie cespiti                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30] [VRS_46]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-02-22                                                      *
* Last revis.: 2012-09-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsce_act"))

* --- Class definition
define class tgsce_act as StdForm
  Top    = 6
  Left   = 10

  * --- Standard Properties
  Width  = 605
  Height = 428+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-09-17"
  HelpContextID=109725033
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=38

  * --- Constant Properties
  CAT_CESP_IDX = 0
  GRU_COCE_IDX = 0
  CONTI_IDX = 0
  CAT_AMMO_IDX = 0
  VALUTE_IDX = 0
  PAR_CESP_IDX = 0
  cFile = "CAT_CESP"
  cKeySelect = "CCCODICE"
  cKeyWhere  = "CCCODICE=this.w_CCCODICE"
  cKeyWhereODBC = '"CCCODICE="+cp_ToStrODBC(this.w_CCCODICE)';

  cKeyWhereODBCqualified = '"CAT_CESP.CCCODICE="+cp_ToStrODBC(this.w_CCCODICE)';

  cPrg = "gsce_act"
  cComment = "Categorie cespiti"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CCCODICE = space(15)
  o_CCCODICE = space(15)
  w_TIPCON = space(1)
  w_CCDESCRI = space(40)
  w_OLD_SOAMM = space(1)
  w_OLD_SOAMF = space(1)
  w_CCGRUCON = space(15)
  w_DESGRU = space(40)
  w_CCTIPBEN = space(1)
  o_CCTIPBEN = space(1)
  w_CCDURCES = 0
  o_CCDURCES = 0
  w_CCFLANSI = space(1)
  w_CCPERSPE = 0
  o_CCPERSPE = 0
  w_CCCONMAN = space(15)
  w_DESCON = space(40)
  w_CCDTINVA = ctod('  /  /  ')
  w_CCDTOBSO = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  w_CODAZI = space(1)
  w_CODI = space(15)
  w_PCTIPAMM = space(1)
  w_DESC = space(40)
  w_CCCOECIV = 0
  w_CCFLAMCI = space(1)
  w_CCPERCIV = 0
  w_CCCOEFIS = 0
  o_CCCOEFIS = 0
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_CCPERDEF = 0
  w_CCIMPMAX = 0
  w_CCVALLIM = space(3)
  w_DECTOP = 0
  w_CALCPIC = 0
  w_CCPREFIS = space(15)
  w_VALFIN = space(3)
  w_CCNSOAMM = space(1)
  o_CCNSOAMM = space(1)
  w_CCNSOAMF = space(1)
  o_CCNSOAMF = space(1)
  w_CCFLRIDU = space(1)
  o_CCFLRIDU = space(1)

  * --- Children pointers
  GSCE_MEA = .NULL.
  GSCE_MCA = .NULL.
  w_SALDI = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=4, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'CAT_CESP','gsce_act')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsce_actPag1","gsce_act",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Categoria")
      .Pages(1).HelpContextID = 257058463
      .Pages(2).addobject("oPag","tgsce_actPag2","gsce_act",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Piani ammortamento")
      .Pages(2).HelpContextID = 200915487
      .Pages(3).addobject("oPag","tgsce_actPag3","gsce_act",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Saldi")
      .Pages(3).HelpContextID = 261037786
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCCCODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
    proc Init()
      this.w_SALDI = this.oPgFrm.Pages(3).oPag.SALDI
      DoDefault()
    proc Destroy()
      this.w_SALDI = .NULL.
      DoDefault()

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='GRU_COCE'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='CAT_AMMO'
    this.cWorkTables[4]='VALUTE'
    this.cWorkTables[5]='PAR_CESP'
    this.cWorkTables[6]='CAT_CESP'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(6))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CAT_CESP_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CAT_CESP_IDX,3]
  return

  function CreateChildren()
    this.GSCE_MEA = CREATEOBJECT('stdDynamicChild',this,'GSCE_MEA',this.oPgFrm.Page2.oPag.oLinkPC_2_22)
    this.GSCE_MEA.createrealchild()
    this.GSCE_MCA = CREATEOBJECT('stdDynamicChild',this,'GSCE_MCA',this.oPgFrm.Page2.oPag.oLinkPC_2_32)
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSCE_MEA)
      this.GSCE_MEA.DestroyChildrenChain()
      this.GSCE_MEA=.NULL.
    endif
    this.oPgFrm.Page2.oPag.RemoveObject('oLinkPC_2_22')
    if !ISNULL(this.GSCE_MCA)
      this.GSCE_MCA.DestroyChildrenChain()
      this.GSCE_MCA=.NULL.
    endif
    this.oPgFrm.Page2.oPag.RemoveObject('oLinkPC_2_32')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSCE_MEA.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSCE_MCA.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSCE_MEA.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSCE_MCA.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSCE_MEA.NewDocument()
    this.GSCE_MCA.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSCE_MEA.SetKey(;
            .w_CCCODICE,"EACODCAT";
            )
      this.GSCE_MCA.SetKey(;
            .w_CCCODICE,"ECCODCAT";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSCE_MEA.ChangeRow(this.cRowID+'      1',1;
             ,.w_CCCODICE,"EACODCAT";
             )
      .WriteTo_GSCE_MEA()
      .GSCE_MCA.ChangeRow(this.cRowID+'      1',1;
             ,.w_CCCODICE,"ECCODCAT";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSCE_MEA)
        i_f=.GSCE_MEA.BuildFilter()
        if !(i_f==.GSCE_MEA.cQueryFilter)
          i_fnidx=.GSCE_MEA.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSCE_MEA.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSCE_MEA.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSCE_MEA.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSCE_MEA.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSCE_MCA)
        i_f=.GSCE_MCA.BuildFilter()
        if !(i_f==.GSCE_MCA.cQueryFilter)
          i_fnidx=.GSCE_MCA.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSCE_MCA.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSCE_MCA.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSCE_MCA.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSCE_MCA.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

procedure WriteTo_GSCE_MEA()
  if at('gsce_mea',lower(this.GSCE_MEA.class))<>0
    if this.GSCE_MEA.w_TIPBEN<>this.w_CCTIPBEN or this.GSCE_MEA.w_COEFIS<>this.w_CCCOEFIS
      this.GSCE_MEA.w_TIPBEN = this.w_CCTIPBEN
      this.GSCE_MEA.w_COEFIS = this.w_CCCOEFIS
      this.GSCE_MEA.mCalc(.t.)
    endif
  endif
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_CCCODICE = NVL(CCCODICE,space(15))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_6_joined
    link_1_6_joined=.f.
    local link_2_25_joined
    link_2_25_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from CAT_CESP where CCCODICE=KeySet.CCCODICE
    *
    i_nConn = i_TableProp[this.CAT_CESP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_CESP_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CAT_CESP')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CAT_CESP.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' CAT_CESP '
      link_1_6_joined=this.AddJoinedLink_1_6(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_25_joined=this.AddJoinedLink_2_25(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'CCCODICE',this.w_CCCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_TIPCON = 'G'
        .w_OLD_SOAMM = space(1)
        .w_OLD_SOAMF = space(1)
        .w_DESGRU = space(40)
        .w_DESCON = space(40)
        .w_OBTEST = i_datsys
        .w_CODAZI = i_CODAZI
        .w_PCTIPAMM = space(1)
        .w_DECTOP = 0
        .w_VALFIN = GETVALUT(g_PERVAL, "VAUNIVAL")
        .w_CCCODICE = NVL(CCCODICE,space(15))
        .w_CCDESCRI = NVL(CCDESCRI,space(40))
        .w_CCGRUCON = NVL(CCGRUCON,space(15))
          if link_1_6_joined
            this.w_CCGRUCON = NVL(GPCODICE106,NVL(this.w_CCGRUCON,space(15)))
            this.w_DESGRU = NVL(GPDESCRI106,space(40))
            this.w_CCTIPBEN = NVL(GPTIPBEN106,space(1))
          else
          .link_1_6('Load')
          endif
        .w_CCTIPBEN = NVL(CCTIPBEN,space(1))
        .w_CCDURCES = NVL(CCDURCES,0)
        .w_CCFLANSI = NVL(CCFLANSI,space(1))
        .w_CCPERSPE = NVL(CCPERSPE,0)
        .w_CCCONMAN = NVL(CCCONMAN,space(15))
          .link_1_12('Load')
        .w_CCDTINVA = NVL(cp_ToDate(CCDTINVA),ctod("  /  /  "))
        .w_CCDTOBSO = NVL(cp_ToDate(CCDTOBSO),ctod("  /  /  "))
          .link_2_1('Load')
        .w_CODI = .w_CCCODICE
        .w_DESC = .w_CCDESCRI
        .w_CCCOECIV = NVL(CCCOECIV,0)
        .w_CCFLAMCI = NVL(CCFLAMCI,space(1))
        .w_CCPERCIV = NVL(CCPERCIV,0)
        .w_CCCOEFIS = NVL(CCCOEFIS,0)
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .w_CCPERDEF = NVL(CCPERDEF,0)
        .w_CCIMPMAX = NVL(CCIMPMAX,0)
        .w_CCVALLIM = NVL(CCVALLIM,space(3))
          if link_2_25_joined
            this.w_CCVALLIM = NVL(VACODVAL225,NVL(this.w_CCVALLIM,space(3)))
            this.w_DECTOP = NVL(VADECTOT225,0)
          else
          .link_2_25('Load')
          endif
        .w_CALCPIC = DEFPIC(.w_DECTOP)
        .w_CCPREFIS = NVL(CCPREFIS,space(15))
        .oPgFrm.Page3.oPag.SALDI.Calculate()
        .w_CCNSOAMM = NVL(CCNSOAMM,space(1))
        .w_CCNSOAMF = NVL(CCNSOAMF,space(1))
        .w_CCFLRIDU = NVL(CCFLRIDU,space(1))
        cp_LoadRecExtFlds(this,'CAT_CESP')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gsce_act
    This.w_Old_Soamm=This.w_Ccnsoamm
    This.w_Old_Soamf=This.w_Ccnsoamf
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CCCODICE = space(15)
      .w_TIPCON = space(1)
      .w_CCDESCRI = space(40)
      .w_OLD_SOAMM = space(1)
      .w_OLD_SOAMF = space(1)
      .w_CCGRUCON = space(15)
      .w_DESGRU = space(40)
      .w_CCTIPBEN = space(1)
      .w_CCDURCES = 0
      .w_CCFLANSI = space(1)
      .w_CCPERSPE = 0
      .w_CCCONMAN = space(15)
      .w_DESCON = space(40)
      .w_CCDTINVA = ctod("  /  /  ")
      .w_CCDTOBSO = ctod("  /  /  ")
      .w_OBTEST = ctod("  /  /  ")
      .w_CODAZI = space(1)
      .w_CODI = space(15)
      .w_PCTIPAMM = space(1)
      .w_DESC = space(40)
      .w_CCCOECIV = 0
      .w_CCFLAMCI = space(1)
      .w_CCPERCIV = 0
      .w_CCCOEFIS = 0
      .w_UTCC = 0
      .w_UTCV = 0
      .w_UTDC = ctot("")
      .w_UTDV = ctot("")
      .w_CCPERDEF = 0
      .w_CCIMPMAX = 0
      .w_CCVALLIM = space(3)
      .w_DECTOP = 0
      .w_CALCPIC = 0
      .w_CCPREFIS = space(15)
      .w_VALFIN = space(3)
      .w_CCNSOAMM = space(1)
      .w_CCNSOAMF = space(1)
      .w_CCFLRIDU = space(1)
      if .cFunction<>"Filter"
          .DoRTCalc(1,1,.f.)
        .w_TIPCON = 'G'
        .DoRTCalc(3,6,.f.)
          if not(empty(.w_CCGRUCON))
          .link_1_6('Full')
          endif
          .DoRTCalc(7,7,.f.)
        .w_CCTIPBEN = 'M'
        .w_CCDURCES = 1
          .DoRTCalc(10,11,.f.)
        .w_CCCONMAN = IIF(.w_CCPERSPE=0,space(15),.w_CCCONMAN)
        .DoRTCalc(12,12,.f.)
          if not(empty(.w_CCCONMAN))
          .link_1_12('Full')
          endif
          .DoRTCalc(13,15,.f.)
        .w_OBTEST = i_datsys
        .w_CODAZI = i_CODAZI
        .DoRTCalc(17,17,.f.)
          if not(empty(.w_CODAZI))
          .link_2_1('Full')
          endif
        .w_CODI = .w_CCCODICE
          .DoRTCalc(19,19,.f.)
        .w_DESC = .w_CCDESCRI
        .w_CCCOECIV = cp_ROUND(100/.w_CCDURCES,2)
        .w_CCFLAMCI = 'I'
        .w_CCPERCIV = 0
          .DoRTCalc(24,28,.f.)
        .w_CCPERDEF = 100
          .DoRTCalc(30,30,.f.)
        .w_CCVALLIM = g_PERVAL
        .DoRTCalc(31,31,.f.)
          if not(empty(.w_CCVALLIM))
          .link_2_25('Full')
          endif
          .DoRTCalc(32,32,.f.)
        .w_CALCPIC = DEFPIC(.w_DECTOP)
        .w_CCPREFIS = IIF(EMPTY(.w_CCPREFIS), LEFT(ALLTRIM(.w_CCCODICE),15),.w_CCPREFIS)
        .w_VALFIN = GETVALUT(g_PERVAL, "VAUNIVAL")
        .oPgFrm.Page3.oPag.SALDI.Calculate()
        .w_CCNSOAMM = 'N'
        .w_CCNSOAMF = 'N'
        .w_CCFLRIDU = 'N'
      endif
    endwith
    cp_BlankRecExtFlds(this,'CAT_CESP')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsce_act
    * Se si � in caricamento la pagina 3 � disattivata
        this.oPgFrm.Pages[3].Enabled=Not(this.cFunction='Load')
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- gsce_act
    IF thisform.cFunction='Load'
       thisform.w_SALDI.visible = .f.
       this.oPgFrm.Pages(this.oPgFrm.PageCount-2).enabled=.t.
    else
       thisform.w_SALDI.visible = .t.
    endif
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oCCCODICE_1_1.enabled = i_bVal
      .Page1.oPag.oCCDESCRI_1_3.enabled = i_bVal
      .Page1.oPag.oCCGRUCON_1_6.enabled = i_bVal
      .Page1.oPag.oCCTIPBEN_1_8.enabled = i_bVal
      .Page1.oPag.oCCDURCES_1_9.enabled = i_bVal
      .Page1.oPag.oCCFLANSI_1_10.enabled = i_bVal
      .Page1.oPag.oCCPERSPE_1_11.enabled = i_bVal
      .Page1.oPag.oCCCONMAN_1_12.enabled = i_bVal
      .Page1.oPag.oCCDTINVA_1_14.enabled = i_bVal
      .Page1.oPag.oCCDTOBSO_1_15.enabled = i_bVal
      .Page2.oPag.oCCCOECIV_2_5.enabled = i_bVal
      .Page2.oPag.oCCCOEFIS_2_8.enabled = i_bVal
      .Page2.oPag.oCCPERDEF_2_23.enabled = i_bVal
      .Page2.oPag.oCCIMPMAX_2_24.enabled = i_bVal
      .Page2.oPag.oCCVALLIM_2_25.enabled = i_bVal
      .Page1.oPag.oCCPREFIS_1_25.enabled = i_bVal
      .Page1.oPag.oCCNSOAMM_1_28.enabled = i_bVal
      .Page1.oPag.oCCNSOAMF_1_29.enabled = i_bVal
      .Page2.oPag.oCCFLRIDU_2_30.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oCCCODICE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oCCCODICE_1_1.enabled = .t.
        .Page1.oPag.oCCDESCRI_1_3.enabled = .t.
      endif
    endwith
    this.GSCE_MEA.SetStatus(i_cOp)
    this.GSCE_MCA.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'CAT_CESP',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSCE_MEA.SetChildrenStatus(i_cOp)
  *  this.GSCE_MCA.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CAT_CESP_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCCODICE,"CCCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCDESCRI,"CCDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCGRUCON,"CCGRUCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCTIPBEN,"CCTIPBEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCDURCES,"CCDURCES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCFLANSI,"CCFLANSI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCPERSPE,"CCPERSPE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCCONMAN,"CCCONMAN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCDTINVA,"CCDTINVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCDTOBSO,"CCDTOBSO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCCOECIV,"CCCOECIV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCFLAMCI,"CCFLAMCI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCPERCIV,"CCPERCIV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCCOEFIS,"CCCOEFIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCPERDEF,"CCPERDEF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCIMPMAX,"CCIMPMAX",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCVALLIM,"CCVALLIM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCPREFIS,"CCPREFIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCNSOAMM,"CCNSOAMM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCNSOAMF,"CCNSOAMF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCFLRIDU,"CCFLRIDU",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.CAT_CESP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_CESP_IDX,2])
    i_lTable = "CAT_CESP"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.CAT_CESP_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSCE_KCA with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CAT_CESP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_CESP_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.CAT_CESP_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into CAT_CESP
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CAT_CESP')
        i_extval=cp_InsertValODBCExtFlds(this,'CAT_CESP')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(CCCODICE,CCDESCRI,CCGRUCON,CCTIPBEN,CCDURCES"+;
                  ",CCFLANSI,CCPERSPE,CCCONMAN,CCDTINVA,CCDTOBSO"+;
                  ",CCCOECIV,CCFLAMCI,CCPERCIV,CCCOEFIS,UTCC"+;
                  ",UTCV,UTDC,UTDV,CCPERDEF,CCIMPMAX"+;
                  ",CCVALLIM,CCPREFIS,CCNSOAMM,CCNSOAMF,CCFLRIDU "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_CCCODICE)+;
                  ","+cp_ToStrODBC(this.w_CCDESCRI)+;
                  ","+cp_ToStrODBCNull(this.w_CCGRUCON)+;
                  ","+cp_ToStrODBC(this.w_CCTIPBEN)+;
                  ","+cp_ToStrODBC(this.w_CCDURCES)+;
                  ","+cp_ToStrODBC(this.w_CCFLANSI)+;
                  ","+cp_ToStrODBC(this.w_CCPERSPE)+;
                  ","+cp_ToStrODBCNull(this.w_CCCONMAN)+;
                  ","+cp_ToStrODBC(this.w_CCDTINVA)+;
                  ","+cp_ToStrODBC(this.w_CCDTOBSO)+;
                  ","+cp_ToStrODBC(this.w_CCCOECIV)+;
                  ","+cp_ToStrODBC(this.w_CCFLAMCI)+;
                  ","+cp_ToStrODBC(this.w_CCPERCIV)+;
                  ","+cp_ToStrODBC(this.w_CCCOEFIS)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                  ","+cp_ToStrODBC(this.w_CCPERDEF)+;
                  ","+cp_ToStrODBC(this.w_CCIMPMAX)+;
                  ","+cp_ToStrODBCNull(this.w_CCVALLIM)+;
                  ","+cp_ToStrODBC(this.w_CCPREFIS)+;
                  ","+cp_ToStrODBC(this.w_CCNSOAMM)+;
                  ","+cp_ToStrODBC(this.w_CCNSOAMF)+;
                  ","+cp_ToStrODBC(this.w_CCFLRIDU)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CAT_CESP')
        i_extval=cp_InsertValVFPExtFlds(this,'CAT_CESP')
        cp_CheckDeletedKey(i_cTable,0,'CCCODICE',this.w_CCCODICE)
        INSERT INTO (i_cTable);
              (CCCODICE,CCDESCRI,CCGRUCON,CCTIPBEN,CCDURCES,CCFLANSI,CCPERSPE,CCCONMAN,CCDTINVA,CCDTOBSO,CCCOECIV,CCFLAMCI,CCPERCIV,CCCOEFIS,UTCC,UTCV,UTDC,UTDV,CCPERDEF,CCIMPMAX,CCVALLIM,CCPREFIS,CCNSOAMM,CCNSOAMF,CCFLRIDU  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_CCCODICE;
                  ,this.w_CCDESCRI;
                  ,this.w_CCGRUCON;
                  ,this.w_CCTIPBEN;
                  ,this.w_CCDURCES;
                  ,this.w_CCFLANSI;
                  ,this.w_CCPERSPE;
                  ,this.w_CCCONMAN;
                  ,this.w_CCDTINVA;
                  ,this.w_CCDTOBSO;
                  ,this.w_CCCOECIV;
                  ,this.w_CCFLAMCI;
                  ,this.w_CCPERCIV;
                  ,this.w_CCCOEFIS;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
                  ,this.w_CCPERDEF;
                  ,this.w_CCIMPMAX;
                  ,this.w_CCVALLIM;
                  ,this.w_CCPREFIS;
                  ,this.w_CCNSOAMM;
                  ,this.w_CCNSOAMF;
                  ,this.w_CCFLRIDU;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.CAT_CESP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_CESP_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.CAT_CESP_IDX,i_nConn)
      *
      * update CAT_CESP
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'CAT_CESP')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " CCDESCRI="+cp_ToStrODBC(this.w_CCDESCRI)+;
             ",CCGRUCON="+cp_ToStrODBCNull(this.w_CCGRUCON)+;
             ",CCTIPBEN="+cp_ToStrODBC(this.w_CCTIPBEN)+;
             ",CCDURCES="+cp_ToStrODBC(this.w_CCDURCES)+;
             ",CCFLANSI="+cp_ToStrODBC(this.w_CCFLANSI)+;
             ",CCPERSPE="+cp_ToStrODBC(this.w_CCPERSPE)+;
             ",CCCONMAN="+cp_ToStrODBCNull(this.w_CCCONMAN)+;
             ",CCDTINVA="+cp_ToStrODBC(this.w_CCDTINVA)+;
             ",CCDTOBSO="+cp_ToStrODBC(this.w_CCDTOBSO)+;
             ",CCCOECIV="+cp_ToStrODBC(this.w_CCCOECIV)+;
             ",CCFLAMCI="+cp_ToStrODBC(this.w_CCFLAMCI)+;
             ",CCPERCIV="+cp_ToStrODBC(this.w_CCPERCIV)+;
             ",CCCOEFIS="+cp_ToStrODBC(this.w_CCCOEFIS)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",CCPERDEF="+cp_ToStrODBC(this.w_CCPERDEF)+;
             ",CCIMPMAX="+cp_ToStrODBC(this.w_CCIMPMAX)+;
             ",CCVALLIM="+cp_ToStrODBCNull(this.w_CCVALLIM)+;
             ",CCPREFIS="+cp_ToStrODBC(this.w_CCPREFIS)+;
             ",CCNSOAMM="+cp_ToStrODBC(this.w_CCNSOAMM)+;
             ",CCNSOAMF="+cp_ToStrODBC(this.w_CCNSOAMF)+;
             ",CCFLRIDU="+cp_ToStrODBC(this.w_CCFLRIDU)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'CAT_CESP')
        i_cWhere = cp_PKFox(i_cTable  ,'CCCODICE',this.w_CCCODICE  )
        UPDATE (i_cTable) SET;
              CCDESCRI=this.w_CCDESCRI;
             ,CCGRUCON=this.w_CCGRUCON;
             ,CCTIPBEN=this.w_CCTIPBEN;
             ,CCDURCES=this.w_CCDURCES;
             ,CCFLANSI=this.w_CCFLANSI;
             ,CCPERSPE=this.w_CCPERSPE;
             ,CCCONMAN=this.w_CCCONMAN;
             ,CCDTINVA=this.w_CCDTINVA;
             ,CCDTOBSO=this.w_CCDTOBSO;
             ,CCCOECIV=this.w_CCCOECIV;
             ,CCFLAMCI=this.w_CCFLAMCI;
             ,CCPERCIV=this.w_CCPERCIV;
             ,CCCOEFIS=this.w_CCCOEFIS;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             ,CCPERDEF=this.w_CCPERDEF;
             ,CCIMPMAX=this.w_CCIMPMAX;
             ,CCVALLIM=this.w_CCVALLIM;
             ,CCPREFIS=this.w_CCPREFIS;
             ,CCNSOAMM=this.w_CCNSOAMM;
             ,CCNSOAMF=this.w_CCNSOAMF;
             ,CCFLRIDU=this.w_CCFLRIDU;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSCE_MEA : Saving
      this.GSCE_MEA.ChangeRow(this.cRowID+'      1',0;
             ,this.w_CCCODICE,"EACODCAT";
             )
      this.GSCE_MEA.mReplace()
      * --- GSCE_MCA : Saving
      this.GSCE_MCA.ChangeRow(this.cRowID+'      1',0;
             ,this.w_CCCODICE,"ECCODCAT";
             )
      this.GSCE_MCA.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSCE_MEA : Deleting
    this.GSCE_MEA.ChangeRow(this.cRowID+'      1',0;
           ,this.w_CCCODICE,"EACODCAT";
           )
    this.GSCE_MEA.mDelete()
    * --- GSCE_MCA : Deleting
    this.GSCE_MCA.ChangeRow(this.cRowID+'      1',0;
           ,this.w_CCCODICE,"ECCODCAT";
           )
    this.GSCE_MCA.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.CAT_CESP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_CESP_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.CAT_CESP_IDX,i_nConn)
      *
      * delete CAT_CESP
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'CCCODICE',this.w_CCCODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CAT_CESP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_CESP_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,11,.t.)
        if .o_CCPERSPE<>.w_CCPERSPE
            .w_CCCONMAN = IIF(.w_CCPERSPE=0,space(15),.w_CCCONMAN)
          .link_1_12('Full')
        endif
        .DoRTCalc(13,16,.t.)
          .link_2_1('Full')
            .w_CODI = .w_CCCODICE
        .DoRTCalc(19,19,.t.)
            .w_DESC = .w_CCDESCRI
        if .o_CCDURCES<>.w_CCDURCES
            .w_CCCOECIV = cp_ROUND(100/.w_CCDURCES,2)
        endif
        if  .o_CCTIPBEN<>.w_CCTIPBEN.or. .o_CCCOEFIS<>.w_CCCOEFIS
          .WriteTo_GSCE_MEA()
        endif
        .DoRTCalc(22,32,.t.)
            .w_CALCPIC = DEFPIC(.w_DECTOP)
        if .o_CCCODICE<>.w_CCCODICE
            .w_CCPREFIS = IIF(EMPTY(.w_CCPREFIS), LEFT(ALLTRIM(.w_CCCODICE),15),.w_CCPREFIS)
        endif
        .oPgFrm.Page3.oPag.SALDI.Calculate()
        if .o_CCNSOAMM<>.w_CCNSOAMM.or. .o_CCNSOAMF<>.w_CCNSOAMF
          .Calculate_DEWNHNUWUK()
        endif
        if .o_CCFLRIDU<>.w_CCFLRIDU
          .Calculate_SHVSADSLTN()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(35,38,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page3.oPag.SALDI.Calculate()
    endwith
  return

  proc Calculate_DEWNHNUWUK()
    with this
          * --- Non soggetto ad ammortamento
          GSCE_BSA(this;
              ,'A';
             )
    endwith
  endproc
  proc Calculate_SHVSADSLTN()
    with this
          * --- Flag riduzione 1^esercizio
          GSCE_BSA(this;
              ,'R';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCCGRUCON_1_6.enabled = this.oPgFrm.Page1.oPag.oCCGRUCON_1_6.mCond()
    this.oPgFrm.Page1.oPag.oCCTIPBEN_1_8.enabled = this.oPgFrm.Page1.oPag.oCCTIPBEN_1_8.mCond()
    this.oPgFrm.Page1.oPag.oCCPERSPE_1_11.enabled = this.oPgFrm.Page1.oPag.oCCPERSPE_1_11.mCond()
    this.oPgFrm.Page1.oPag.oCCCONMAN_1_12.enabled = this.oPgFrm.Page1.oPag.oCCCONMAN_1_12.mCond()
    this.oPgFrm.Page2.oPag.oCCCOECIV_2_5.enabled = this.oPgFrm.Page2.oPag.oCCCOECIV_2_5.mCond()
    this.oPgFrm.Page2.oPag.oCCCOEFIS_2_8.enabled = this.oPgFrm.Page2.oPag.oCCCOEFIS_2_8.mCond()
    this.oPgFrm.Page2.oPag.oCCPERDEF_2_23.enabled = this.oPgFrm.Page2.oPag.oCCPERDEF_2_23.mCond()
    this.oPgFrm.Page2.oPag.oCCIMPMAX_2_24.enabled = this.oPgFrm.Page2.oPag.oCCIMPMAX_2_24.mCond()
    this.oPgFrm.Page2.oPag.oCCVALLIM_2_25.enabled = this.oPgFrm.Page2.oPag.oCCVALLIM_2_25.mCond()
    this.oPgFrm.Page1.oPag.oCCNSOAMM_1_28.enabled = this.oPgFrm.Page1.oPag.oCCNSOAMM_1_28.mCond()
    this.oPgFrm.Page1.oPag.oCCNSOAMF_1_29.enabled = this.oPgFrm.Page1.oPag.oCCNSOAMF_1_29.mCond()
    this.GSCE_MEA.enabled = this.oPgFrm.Page2.oPag.oLinkPC_2_22.mCond()
    this.GSCE_MCA.enabled = this.oPgFrm.Page2.oPag.oLinkPC_2_32.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page3.oPag.oStr_3_3.visible=!this.oPgFrm.Page3.oPag.oStr_3_3.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page3.oPag.SALDI.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CCGRUCON
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRU_COCE_IDX,3]
    i_lTable = "GRU_COCE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRU_COCE_IDX,2], .t., this.GRU_COCE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRU_COCE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CCGRUCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCE_AGP',True,'GRU_COCE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GPCODICE like "+cp_ToStrODBC(trim(this.w_CCGRUCON)+"%");

          i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI,GPTIPBEN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GPCODICE',trim(this.w_CCGRUCON))
          select GPCODICE,GPDESCRI,GPTIPBEN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CCGRUCON)==trim(_Link_.GPCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" GPDESCRI like "+cp_ToStrODBC(trim(this.w_CCGRUCON)+"%");

            i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI,GPTIPBEN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" GPDESCRI like "+cp_ToStr(trim(this.w_CCGRUCON)+"%");

            select GPCODICE,GPDESCRI,GPTIPBEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CCGRUCON) and !this.bDontReportError
            deferred_cp_zoom('GRU_COCE','*','GPCODICE',cp_AbsName(oSource.parent,'oCCGRUCON_1_6'),i_cWhere,'GSCE_AGP',"Gruppi contabili",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI,GPTIPBEN";
                     +" from "+i_cTable+" "+i_lTable+" where GPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GPCODICE',oSource.xKey(1))
            select GPCODICE,GPDESCRI,GPTIPBEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CCGRUCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI,GPTIPBEN";
                   +" from "+i_cTable+" "+i_lTable+" where GPCODICE="+cp_ToStrODBC(this.w_CCGRUCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GPCODICE',this.w_CCGRUCON)
            select GPCODICE,GPDESCRI,GPTIPBEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CCGRUCON = NVL(_Link_.GPCODICE,space(15))
      this.w_DESGRU = NVL(_Link_.GPDESCRI,space(40))
      this.w_CCTIPBEN = NVL(_Link_.GPTIPBEN,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CCGRUCON = space(15)
      endif
      this.w_DESGRU = space(40)
      this.w_CCTIPBEN = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRU_COCE_IDX,2])+'\'+cp_ToStr(_Link_.GPCODICE,1)
      cp_ShowWarn(i_cKey,this.GRU_COCE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CCGRUCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_6(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.GRU_COCE_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.GRU_COCE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_6.GPCODICE as GPCODICE106"+ ",link_1_6.GPDESCRI as GPDESCRI106"+ ",link_1_6.GPTIPBEN as GPTIPBEN106"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_6 on CAT_CESP.CCGRUCON=link_1_6.GPCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_6"
          i_cKey=i_cKey+'+" and CAT_CESP.CCGRUCON=link_1_6.GPCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CCCONMAN
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CCCONMAN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CCCONMAN)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_CCCONMAN))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CCCONMAN)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CCCONMAN) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCCCONMAN_1_12'),i_cWhere,'GSAR_BZC',"Conti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CCCONMAN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CCCONMAN);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_CCCONMAN)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CCCONMAN = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCON = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CCCONMAN = space(15)
      endif
      this.w_DESCON = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CCCONMAN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODAZI
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_CESP_IDX,3]
    i_lTable = "PAR_CESP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_CESP_IDX,2], .t., this.PAR_CESP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_CESP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PCCODAZI,PCTIPAMM";
                   +" from "+i_cTable+" "+i_lTable+" where PCCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PCCODAZI',this.w_CODAZI)
            select PCCODAZI,PCTIPAMM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.PCCODAZI,space(1))
      this.w_PCTIPAMM = NVL(_Link_.PCTIPAMM,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(1)
      endif
      this.w_PCTIPAMM = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_CESP_IDX,2])+'\'+cp_ToStr(_Link_.PCCODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_CESP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CCVALLIM
  func Link_2_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CCVALLIM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_CCVALLIM)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_CCVALLIM))
          select VACODVAL,VADECTOT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CCVALLIM)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CCVALLIM) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oCCVALLIM_2_25'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CCVALLIM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_CCVALLIM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_CCVALLIM)
            select VACODVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CCVALLIM = NVL(_Link_.VACODVAL,space(3))
      this.w_DECTOP = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_CCVALLIM = space(3)
      endif
      this.w_DECTOP = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CCVALLIM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_25(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VALUTE_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_25.VACODVAL as VACODVAL225"+ ",link_2_25.VADECTOT as VADECTOT225"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_25 on CAT_CESP.CCVALLIM=link_2_25.VACODVAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_25"
          i_cKey=i_cKey+'+" and CAT_CESP.CCVALLIM=link_2_25.VACODVAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCCCODICE_1_1.value==this.w_CCCODICE)
      this.oPgFrm.Page1.oPag.oCCCODICE_1_1.value=this.w_CCCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oCCDESCRI_1_3.value==this.w_CCDESCRI)
      this.oPgFrm.Page1.oPag.oCCDESCRI_1_3.value=this.w_CCDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCCGRUCON_1_6.value==this.w_CCGRUCON)
      this.oPgFrm.Page1.oPag.oCCGRUCON_1_6.value=this.w_CCGRUCON
    endif
    if not(this.oPgFrm.Page1.oPag.oDESGRU_1_7.value==this.w_DESGRU)
      this.oPgFrm.Page1.oPag.oDESGRU_1_7.value=this.w_DESGRU
    endif
    if not(this.oPgFrm.Page1.oPag.oCCTIPBEN_1_8.RadioValue()==this.w_CCTIPBEN)
      this.oPgFrm.Page1.oPag.oCCTIPBEN_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCDURCES_1_9.value==this.w_CCDURCES)
      this.oPgFrm.Page1.oPag.oCCDURCES_1_9.value=this.w_CCDURCES
    endif
    if not(this.oPgFrm.Page1.oPag.oCCFLANSI_1_10.RadioValue()==this.w_CCFLANSI)
      this.oPgFrm.Page1.oPag.oCCFLANSI_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCPERSPE_1_11.value==this.w_CCPERSPE)
      this.oPgFrm.Page1.oPag.oCCPERSPE_1_11.value=this.w_CCPERSPE
    endif
    if not(this.oPgFrm.Page1.oPag.oCCCONMAN_1_12.value==this.w_CCCONMAN)
      this.oPgFrm.Page1.oPag.oCCCONMAN_1_12.value=this.w_CCCONMAN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCON_1_13.value==this.w_DESCON)
      this.oPgFrm.Page1.oPag.oDESCON_1_13.value=this.w_DESCON
    endif
    if not(this.oPgFrm.Page1.oPag.oCCDTINVA_1_14.value==this.w_CCDTINVA)
      this.oPgFrm.Page1.oPag.oCCDTINVA_1_14.value=this.w_CCDTINVA
    endif
    if not(this.oPgFrm.Page1.oPag.oCCDTOBSO_1_15.value==this.w_CCDTOBSO)
      this.oPgFrm.Page1.oPag.oCCDTOBSO_1_15.value=this.w_CCDTOBSO
    endif
    if not(this.oPgFrm.Page2.oPag.oCODI_2_2.value==this.w_CODI)
      this.oPgFrm.Page2.oPag.oCODI_2_2.value=this.w_CODI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESC_2_4.value==this.w_DESC)
      this.oPgFrm.Page2.oPag.oDESC_2_4.value=this.w_DESC
    endif
    if not(this.oPgFrm.Page2.oPag.oCCCOECIV_2_5.value==this.w_CCCOECIV)
      this.oPgFrm.Page2.oPag.oCCCOECIV_2_5.value=this.w_CCCOECIV
    endif
    if not(this.oPgFrm.Page2.oPag.oCCCOEFIS_2_8.value==this.w_CCCOEFIS)
      this.oPgFrm.Page2.oPag.oCCCOEFIS_2_8.value=this.w_CCCOEFIS
    endif
    if not(this.oPgFrm.Page2.oPag.oCCPERDEF_2_23.value==this.w_CCPERDEF)
      this.oPgFrm.Page2.oPag.oCCPERDEF_2_23.value=this.w_CCPERDEF
    endif
    if not(this.oPgFrm.Page2.oPag.oCCIMPMAX_2_24.value==this.w_CCIMPMAX)
      this.oPgFrm.Page2.oPag.oCCIMPMAX_2_24.value=this.w_CCIMPMAX
    endif
    if not(this.oPgFrm.Page2.oPag.oCCVALLIM_2_25.RadioValue()==this.w_CCVALLIM)
      this.oPgFrm.Page2.oPag.oCCVALLIM_2_25.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCPREFIS_1_25.value==this.w_CCPREFIS)
      this.oPgFrm.Page1.oPag.oCCPREFIS_1_25.value=this.w_CCPREFIS
    endif
    if not(this.oPgFrm.Page1.oPag.oCCNSOAMM_1_28.RadioValue()==this.w_CCNSOAMM)
      this.oPgFrm.Page1.oPag.oCCNSOAMM_1_28.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCNSOAMF_1_29.RadioValue()==this.w_CCNSOAMF)
      this.oPgFrm.Page1.oPag.oCCNSOAMF_1_29.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCCFLRIDU_2_30.RadioValue()==this.w_CCFLRIDU)
      this.oPgFrm.Page2.oPag.oCCFLRIDU_2_30.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'CAT_CESP')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CCCODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCCCODICE_1_1.SetFocus()
            i_bnoObbl = !empty(.w_CCCODICE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CCGRUCON))  and (g_COGE='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCCGRUCON_1_6.SetFocus()
            i_bnoObbl = !empty(.w_CCGRUCON)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CCDURCES>0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCCDURCES_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CCPERDEF))  and (.w_PCTIPAMM<>'C' and .w_CCNSOAMF<>'S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCCPERDEF_2_23.SetFocus()
            i_bnoObbl = !empty(.w_CCPERDEF)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      *i_bRes = i_bRes .and. .GSCE_MEA.CheckForm()
      if i_bres
        i_bres=  .GSCE_MEA.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=2
        endif
      endif
      *i_bRes = i_bRes .and. .GSCE_MCA.CheckForm()
      if i_bres
        i_bres=  .GSCE_MCA.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=2
        endif
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CCCODICE = this.w_CCCODICE
    this.o_CCTIPBEN = this.w_CCTIPBEN
    this.o_CCDURCES = this.w_CCDURCES
    this.o_CCPERSPE = this.w_CCPERSPE
    this.o_CCCOEFIS = this.w_CCCOEFIS
    this.o_CCNSOAMM = this.w_CCNSOAMM
    this.o_CCNSOAMF = this.w_CCNSOAMF
    this.o_CCFLRIDU = this.w_CCFLRIDU
    * --- GSCE_MEA : Depends On
    this.GSCE_MEA.SaveDependsOn()
    * --- GSCE_MCA : Depends On
    this.GSCE_MCA.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgsce_actPag1 as StdContainer
  Width  = 601
  height = 428
  stdWidth  = 601
  stdheight = 428
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCCCODICE_1_1 as StdField with uid="LYLPNKFCGJ",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CCCODICE", cQueryName = "CCCODICE",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice della categoria dei cespiti",;
    HelpContextID = 118043243,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=132, Top=22, InputMask=replicate('X',15)

  add object oCCDESCRI_1_3 as StdField with uid="CXQJHXEFGZ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CCDESCRI", cQueryName = "CCDESCRI",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione categoria cespiti",;
    HelpContextID = 32457327,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=273, Top=22, InputMask=replicate('X',40)

  add object oCCGRUCON_1_6 as StdField with uid="VJURTWYJPR",rtseq=6,rtrep=.f.,;
    cFormVar = "w_CCGRUCON", cQueryName = "CCGRUCON",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Gruppo contabile associato",;
    HelpContextID = 233016716,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=132, Top=78, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="GRU_COCE", cZoomOnZoom="GSCE_AGP", oKey_1_1="GPCODICE", oKey_1_2="this.w_CCGRUCON"

  func oCCGRUCON_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_COGE='S')
    endwith
   endif
  endfunc

  func oCCGRUCON_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oCCGRUCON_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCCGRUCON_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRU_COCE','*','GPCODICE',cp_AbsName(this.parent,'oCCGRUCON_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCE_AGP',"Gruppi contabili",'',this.parent.oContained
  endproc
  proc oCCGRUCON_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSCE_AGP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GPCODICE=this.parent.oContained.w_CCGRUCON
     i_obj.ecpSave()
  endproc

  add object oDESGRU_1_7 as StdField with uid="HHTNHYHEBH",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DESGRU", cQueryName = "DESGRU",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 65156150,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=273, Top=78, InputMask=replicate('X',40)


  add object oCCTIPBEN_1_8 as StdCombo with uid="SCANLVBTSA",rtseq=8,rtrep=.f.,left=132,top=139,width=116,height=21;
    , ToolTipText = "Tipologia dei beni rappresentati dalla categoria";
    , HelpContextID = 12862068;
    , cFormVar="w_CCTIPBEN",RowSource=""+"Materiali,"+"Immateriali", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCCTIPBEN_1_8.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'I',;
    space(1))))
  endfunc
  func oCCTIPBEN_1_8.GetRadio()
    this.Parent.oContained.w_CCTIPBEN = this.RadioValue()
    return .t.
  endfunc

  func oCCTIPBEN_1_8.SetRadio()
    this.Parent.oContained.w_CCTIPBEN=trim(this.Parent.oContained.w_CCTIPBEN)
    this.value = ;
      iif(this.Parent.oContained.w_CCTIPBEN=='M',1,;
      iif(this.Parent.oContained.w_CCTIPBEN=='I',2,;
      0))
  endfunc

  func oCCTIPBEN_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_COGE<>'S')
    endwith
   endif
  endfunc

  add object oCCDURCES_1_9 as StdField with uid="XNNZBFEVHA",rtseq=9,rtrep=.f.,;
    cFormVar = "w_CCDURCES", cQueryName = "CCDURCES",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Durata dei cespiti espressa in numero di esercizi",;
    HelpContextID = 32457337,;
   bGlobalFont=.t.,;
    Height=21, Width=32, Left=376, Top=139, cSayPict='"999"', cGetPict='"999"'

  func oCCDURCES_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_CCDURCES>0)
    endwith
    return bRes
  endfunc

  add object oCCFLANSI_1_10 as StdCheck with uid="OKTYBIETBP",rtseq=10,rtrep=.f.,left=418, top=139, caption="Annotazione singola",;
    ToolTipText = "Se attivo: per beni immobili o iscritti a pubblici registri",;
    HelpContextID = 198599279,;
    cFormVar="w_CCFLANSI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCCFLANSI_1_10.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCCFLANSI_1_10.GetRadio()
    this.Parent.oContained.w_CCFLANSI = this.RadioValue()
    return .t.
  endfunc

  func oCCFLANSI_1_10.SetRadio()
    this.Parent.oContained.w_CCFLANSI=trim(this.Parent.oContained.w_CCFLANSI)
    this.value = ;
      iif(this.Parent.oContained.w_CCFLANSI=='S',1,;
      0)
  endfunc

  add object oCCPERSPE_1_11 as StdField with uid="ZGBPGVGBUF",rtseq=11,rtrep=.f.,;
    cFormVar = "w_CCPERSPE", cQueryName = "CCPERSPE",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Percentuale di spese di manutenzione deducibili",;
    HelpContextID = 31457899,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=433, Top=205, cSayPict='"999.99"', cGetPict='"999.99"'

  func oCCPERSPE_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CCTIPBEN='M' AND g_COGE='S')
    endwith
   endif
  endfunc

  add object oCCCONMAN_1_12 as StdField with uid="QBKSSIVUAF",rtseq=12,rtrep=.f.,;
    cFormVar = "w_CCCONMAN", cQueryName = "CCCONMAN",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Conto sul quale addebitare le spese di manutenzione della categoria cespiti",;
    HelpContextID = 195637876,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=132, Top=267, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CCCONMAN"

  func oCCCONMAN_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CCTIPBEN='M' AND .w_CCPERSPE<>0 AND g_COGE='S')
    endwith
   endif
  endfunc

  func oCCCONMAN_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oCCCONMAN_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCCCONMAN_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCCCONMAN_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti",'',this.parent.oContained
  endproc
  proc oCCCONMAN_1_12.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CCCONMAN
     i_obj.ecpSave()
  endproc

  add object oDESCON_1_13 as StdField with uid="VNAUXKZDWS",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DESCON", cQueryName = "DESCON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 55692234,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=272, Top=267, InputMask=replicate('X',40)

  add object oCCDTINVA_1_14 as StdField with uid="PBGFZWYDCQ",rtseq=14,rtrep=.f.,;
    cFormVar = "w_CCDTINVA", cQueryName = "CCDTINVA",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di inizio validit�",;
    HelpContextID = 207503975,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=260, Top=402

  add object oCCDTOBSO_1_15 as StdField with uid="YRYZFYZASD",rtseq=15,rtrep=.f.,;
    cFormVar = "w_CCDTOBSO", cQueryName = "CCDTOBSO",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di fine validit�",;
    HelpContextID = 12468853,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=485, Top=402

  add object oCCPREFIS_1_25 as StdField with uid="XVJESXKCJR",rtseq=34,rtrep=.f.,;
    cFormVar = "w_CCPREFIS", cQueryName = "CCPREFIS",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Prefisso per caricamento automatico codice cespiti",;
    HelpContextID = 199425415,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=132, Top=205, InputMask=replicate('X',15)

  add object oCCNSOAMM_1_28 as StdCheck with uid="FPGKXKDXRE",rtseq=36,rtrep=.f.,left=226, top=337, caption="Civile",;
    ToolTipText = "Se attivo per la categoria non viene generato nessun accantonamento civile",;
    HelpContextID = 4332941,;
    cFormVar="w_CCNSOAMM", bObbl = .f. , nPag = 1;
    , tabstop=.f.;
   , bGlobalFont=.t.


  func oCCNSOAMM_1_28.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCCNSOAMM_1_28.GetRadio()
    this.Parent.oContained.w_CCNSOAMM = this.RadioValue()
    return .t.
  endfunc

  func oCCNSOAMM_1_28.SetRadio()
    this.Parent.oContained.w_CCNSOAMM=trim(this.Parent.oContained.w_CCNSOAMM)
    this.value = ;
      iif(this.Parent.oContained.w_CCNSOAMM=='S',1,;
      0)
  endfunc

  func oCCNSOAMM_1_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PCTIPAMM<>'F')
    endwith
   endif
  endfunc

  add object oCCNSOAMF_1_29 as StdCheck with uid="AGQKWBYIRZ",rtseq=37,rtrep=.f.,left=293, top=337, caption="Fiscale",;
    ToolTipText = "Se attivo per la categoria non viene generato nessun accantonamento fiscale",;
    HelpContextID = 4332948,;
    cFormVar="w_CCNSOAMF", bObbl = .f. , nPag = 1;
    , tabstop=.f.;
   , bGlobalFont=.t.


  func oCCNSOAMF_1_29.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCCNSOAMF_1_29.GetRadio()
    this.Parent.oContained.w_CCNSOAMF = this.RadioValue()
    return .t.
  endfunc

  func oCCNSOAMF_1_29.SetRadio()
    this.Parent.oContained.w_CCNSOAMF=trim(this.Parent.oContained.w_CCNSOAMF)
    this.value = ;
      iif(this.Parent.oContained.w_CCNSOAMF=='S',1,;
      0)
  endfunc

  func oCCNSOAMF_1_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PCTIPAMM<>'C')
    endwith
   endif
  endfunc

  add object oStr_1_16 as StdString with uid="BOMJWBYQBK",Visible=.t., Left=122, Top=402,;
    Alignment=1, Width=133, Height=15,;
    Caption="Inizio validit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="VGTIGSWUVE",Visible=.t., Left=356, Top=402,;
    Alignment=1, Width=124, Height=15,;
    Caption="Data obsolescenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="GERTRDBTRB",Visible=.t., Left=34, Top=22,;
    Alignment=1, Width=94, Height=15,;
    Caption="Categoria:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_19 as StdString with uid="URJUHNJLOU",Visible=.t., Left=12, Top=139,;
    Alignment=1, Width=117, Height=15,;
    Caption="Tipo beni:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="MSWWJDUGIX",Visible=.t., Left=250, Top=139,;
    Alignment=1, Width=120, Height=15,;
    Caption="Durata dei cespiti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="RSSZYFEXIE",Visible=.t., Left=1, Top=78,;
    Alignment=1, Width=127, Height=15,;
    Caption="Gruppo contabile:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="SKUFZMTHJC",Visible=.t., Left=1, Top=267,;
    Alignment=1, Width=127, Height=15,;
    Caption="Conto manutenzione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="JZLWAKTAGU",Visible=.t., Left=309, Top=205,;
    Alignment=1, Width=120, Height=15,;
    Caption="% Sp.manut.ded.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="XKGTQVVETK",Visible=.t., Left=52, Top=205,;
    Alignment=1, Width=77, Height=18,;
    Caption="Prefisso:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="YTEFNGRIEZ",Visible=.t., Left=10, Top=336,;
    Alignment=1, Width=212, Height=18,;
    Caption="Non soggetto ad ammortamento:"  ;
  , bGlobalFont=.t.
enddefine
define class tgsce_actPag2 as StdContainer
  Width  = 601
  height = 428
  stdWidth  = 601
  stdheight = 428
  resizeYpos=244
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODI_2_2 as StdField with uid="OOPVKGZSVP",rtseq=18,rtrep=.f.,;
    cFormVar = "w_CODI", cQueryName = "CODI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 104640986,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=86, Top=11, InputMask=replicate('X',15)

  add object oDESC_2_4 as StdField with uid="XMTNMQATWJ",rtseq=20,rtrep=.f.,;
    cFormVar = "w_DESC", cQueryName = "DESC",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 104975306,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=229, Top=11, InputMask=replicate('X',40)

  add object oCCCOECIV_2_5 as StdField with uid="OQGCUKHODE",rtseq=21,rtrep=.f.,;
    cFormVar = "w_CCCOECIV", cQueryName = "CCCOECIV",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Coefficiente di ammortamento civile",;
    HelpContextID = 250006916,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=163, Top=83, cSayPict='"999.99"', cGetPict='"999.99"'

  func oCCCOECIV_2_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PCTIPAMM<>'F' and .w_CCNSOAMM<>'S')
    endwith
   endif
  endfunc

  add object oCCCOEFIS_2_8 as StdField with uid="PRYDWUAKCT",rtseq=24,rtrep=.f.,;
    cFormVar = "w_CCCOEFIS", cQueryName = "CCCOEFIS",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Coefficiente di ammortamento ordinario",;
    HelpContextID = 199675271,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=162, Top=256, cSayPict='"999.99"', cGetPict='"999.99"'

  func oCCCOEFIS_2_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PCTIPAMM<>'C' and .w_CCNSOAMF<>'S')
    endwith
   endif
  endfunc


  add object oLinkPC_2_22 as stdDynamicChildContainer with uid="BZPAMOUKTD",left=227, top=256, width=362, height=165, bOnScreen=.t.;


  func oLinkPC_2_22.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CCNSOAMF<>'S')
      endwith
    endif
  endfunc

  add object oCCPERDEF_2_23 as StdField with uid="OWSWVXVDOB",rtseq=29,rtrep=.f.,;
    cFormVar = "w_CCPERDEF", cQueryName = "CCPERDEF",;
    bObbl = .t. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Percentuale di deducibilit� dell'accantonamento",;
    HelpContextID = 48235116,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=162, Top=289, cSayPict='"999.99"', cGetPict='"999.99"'

  func oCCPERDEF_2_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PCTIPAMM<>'C' and .w_CCNSOAMF<>'S')
    endwith
   endif
  endfunc

  add object oCCIMPMAX_2_24 as StdField with uid="DZEGNDLDTT",rtseq=30,rtrep=.f.,;
    cFormVar = "w_CCIMPMAX", cQueryName = "CCIMPMAX",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Limite massimo importo accantonabile",;
    HelpContextID = 197628542,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=95, Top=322, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oCCIMPMAX_2_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PCTIPAMM<>'C' and .w_CCNSOAMF<>'S')
    endwith
   endif
  endfunc


  add object oCCVALLIM_2_25 as StdCombo with uid="YTPXNNYOMR",rtseq=31,rtrep=.f.,left=96,top=348,width=124,height=21;
    , ToolTipText = "Valuta a cui � riferito l'importo massimo accantonabile";
    , HelpContextID = 92511629;
    , cFormVar="w_CCVALLIM",RowSource=""+"Valuta di conto,"+"Valuta nazionale", bObbl = .f. , nPag = 2;
    , cLinkFile="VALUTE";
  , bGlobalFont=.t.


  func oCCVALLIM_2_25.RadioValue()
    return(iif(this.value =1,ALLTRIM(g_PERVAL),;
    iif(this.value =2,ALLTRIM(g_CODLIR),;
    space(3))))
  endfunc
  func oCCVALLIM_2_25.GetRadio()
    this.Parent.oContained.w_CCVALLIM = this.RadioValue()
    return .t.
  endfunc

  func oCCVALLIM_2_25.SetRadio()
    this.Parent.oContained.w_CCVALLIM=trim(this.Parent.oContained.w_CCVALLIM)
    this.value = ;
      iif(this.Parent.oContained.w_CCVALLIM==ALLTRIM(g_PERVAL),1,;
      iif(this.Parent.oContained.w_CCVALLIM==ALLTRIM(g_CODLIR),2,;
      0))
  endfunc

  func oCCVALLIM_2_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PCTIPAMM<>'C' and .w_CCNSOAMF<>'S')
    endwith
   endif
  endfunc

  func oCCVALLIM_2_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_25('Part',this)
    endwith
    return bRes
  endfunc

  proc oCCVALLIM_2_25.ecpDrop(oSource)
    this.Parent.oContained.link_2_25('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  add object oCCFLRIDU_2_30 as StdCheck with uid="IFMCRPCWVD",rtseq=38,rtrep=.f.,left=51, top=372, caption="% Riduzione 1^ esercizio",;
    ToolTipText = "Se attivo, al cespite viene applicata la %  di riduzione 1^ esercizio",;
    HelpContextID = 132539003,;
    cFormVar="w_CCFLRIDU", bObbl = .f. , nPag = 2;
    , tabstop=.f.;
   , bGlobalFont=.t.


  func oCCFLRIDU_2_30.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCCFLRIDU_2_30.GetRadio()
    this.Parent.oContained.w_CCFLRIDU = this.RadioValue()
    return .t.
  endfunc

  func oCCFLRIDU_2_30.SetRadio()
    this.Parent.oContained.w_CCFLRIDU=trim(this.Parent.oContained.w_CCFLRIDU)
    this.value = ;
      iif(this.Parent.oContained.w_CCFLRIDU=='S',1,;
      0)
  endfunc


  add object oLinkPC_2_32 as stdDynamicChildContainer with uid="AOFMECNIAO",left=227, top=83, width=362, height=131, bOnScreen=.t.;


  func oLinkPC_2_32.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_PCTIPAMM<>'F' and .w_CCNSOAMM<>'S')
      endwith
    endif
  endfunc

  add object oStr_2_13 as StdString with uid="HARGFSRPFW",Visible=.t., Left=2, Top=11,;
    Alignment=1, Width=79, Height=15,;
    Caption="Categoria:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_14 as StdString with uid="KUCICCSSYD",Visible=.t., Left=5, Top=54,;
    Alignment=0, Width=215, Height=15,;
    Caption="Aspetto civile"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_15 as StdString with uid="HDRMBQPJPY",Visible=.t., Left=4, Top=231,;
    Alignment=0, Width=220, Height=15,;
    Caption="Aspetto fiscale"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_16 as StdString with uid="ONKVYPVDGH",Visible=.t., Left=6, Top=83,;
    Alignment=1, Width=152, Height=15,;
    Caption="Coefficiente:"  ;
  , bGlobalFont=.t.

  add object oStr_2_18 as StdString with uid="TIPIHGVORK",Visible=.t., Left=0, Top=256,;
    Alignment=1, Width=160, Height=15,;
    Caption="Coefficiente amm.ordinario:"  ;
  , bGlobalFont=.t.

  add object oStr_2_20 as StdString with uid="WEDUUKQGMM",Visible=.t., Left=224, Top=231,;
    Alignment=2, Width=334, Height=15,;
    Caption="Ammortamenti"  ;
  , bGlobalFont=.t.

  add object oStr_2_21 as StdString with uid="DAFQSPVUNV",Visible=.t., Left=224, Top=54,;
    Alignment=2, Width=334, Height=15,;
    Caption="Ammortamenti"  ;
  , bGlobalFont=.t.

  add object oStr_2_28 as StdString with uid="IOXHDLFIOG",Visible=.t., Left=1, Top=289,;
    Alignment=1, Width=159, Height=15,;
    Caption="% Deducibilit�:"  ;
  , bGlobalFont=.t.

  add object oStr_2_29 as StdString with uid="NYJRMGDQWC",Visible=.t., Left=27, Top=322,;
    Alignment=1, Width=67, Height=15,;
    Caption="Limite:"  ;
  , bGlobalFont=.t.

  add object oBox_2_17 as StdBox with uid="IOOBOXZAGZ",left=4, top=250, width=561,height=1

  add object oBox_2_19 as StdBox with uid="SHOSUOXUAY",left=4, top=73, width=559,height=1
  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gsce_mca",lower(this.oContained.GSCE_MCA.class))=0
        this.oContained.GSCE_MCA.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc
enddefine
define class tgsce_actPag3 as StdContainer
  Width  = 601
  height = 428
  stdWidth  = 601
  stdheight = 428
  resizeXpos=351
  resizeYpos=178
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object SALDI as cp_zoombox with uid="UVYCRQLBCH",left=9, top=11, width=541,height=286,;
    caption='SALDI',;
   bGlobalFont=.t.,;
    bQueryOnLoad=.f.,cZoomFile="GSCE_VSC",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,cTable="SAL_CESP",cZoomOnZoom="GSCE_ASC",cMenuFile="",bRetriveAllRows=.f.,;
    cEvent = "ActivatePage 3",;
    nPag=3;
    , HelpContextID = 28393178

  add object oStr_3_3 as StdString with uid="NZSKYATEOW",Visible=.t., Left=163, Top=138,;
    Alignment=0, Width=343, Height=22,;
    Caption="Funzione non attivabile in caricamento"    , forecolor = rgb(255,0,0);
  ;
    , FontName = "Arial", FontSize = 12, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_3_3.mHide()
    with this.Parent.oContained
      return (thisform.cFunction <> 'Load')
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsce_act','CAT_CESP','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".CCCODICE=CAT_CESP.CCCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
