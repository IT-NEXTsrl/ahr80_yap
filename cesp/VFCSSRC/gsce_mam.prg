* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_mam                                                        *
*              Cespite/ammortamenti                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30] [VRS_92]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-02-26                                                      *
* Last revis.: 2012-09-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsce_mam")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsce_mam")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsce_mam")
  return

* --- Class definition
define class tgsce_mam as StdPCForm
  Width  = 567
  Height = 155
  Top    = 78
  Left   = 158
  cComment = "Cespite/ammortamenti"
  cPrg = "gsce_mam"
  HelpContextID=137738903
  add object cnt as tcgsce_mam
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsce_mam as PCContext
  w_AMCODCES = space(20)
  w_CODAZI = space(10)
  w_AMMANT = 0
  w_MAXANT = 0
  w_PERRID1 = 0
  w_TIPBEN = space(1)
  w_AMPROESE = 0
  w_AMTIPAMM = space(1)
  w_MAXPEORD = 0
  w_COEFIS = 0
  w_AMPERORD = 0
  w_MAXPEANT = 0
  w_AMPERANT = 0
  w_TOTPEORD = 0
  w_TOTPEANT = 0
  w_TOTALE = 0
  w_TIPAMM = space(1)
  w_IASADO = space(1)
  w_CODESE = space(4)
  w_VALNAZ = space(3)
  w_ESPRIU = space(4)
  w_DECTOT = 0
  w_CALCPIP = 0
  w_AMQUOFIS = 0
  w_AMQUOFI1 = 0
  w_NSOAMF = space(1)
  w_AMMIMM = space(1)
  w_AMPERORD_T = 0
  w_TOTRIGA = 0
  w_FLRIDU = space(1)
  w_PMAXPEORD = 0
  proc Save(i_oFrom)
    this.w_AMCODCES = i_oFrom.w_AMCODCES
    this.w_CODAZI = i_oFrom.w_CODAZI
    this.w_AMMANT = i_oFrom.w_AMMANT
    this.w_MAXANT = i_oFrom.w_MAXANT
    this.w_PERRID1 = i_oFrom.w_PERRID1
    this.w_TIPBEN = i_oFrom.w_TIPBEN
    this.w_AMPROESE = i_oFrom.w_AMPROESE
    this.w_AMTIPAMM = i_oFrom.w_AMTIPAMM
    this.w_MAXPEORD = i_oFrom.w_MAXPEORD
    this.w_COEFIS = i_oFrom.w_COEFIS
    this.w_AMPERORD = i_oFrom.w_AMPERORD
    this.w_MAXPEANT = i_oFrom.w_MAXPEANT
    this.w_AMPERANT = i_oFrom.w_AMPERANT
    this.w_TOTPEORD = i_oFrom.w_TOTPEORD
    this.w_TOTPEANT = i_oFrom.w_TOTPEANT
    this.w_TOTALE = i_oFrom.w_TOTALE
    this.w_TIPAMM = i_oFrom.w_TIPAMM
    this.w_IASADO = i_oFrom.w_IASADO
    this.w_CODESE = i_oFrom.w_CODESE
    this.w_VALNAZ = i_oFrom.w_VALNAZ
    this.w_ESPRIU = i_oFrom.w_ESPRIU
    this.w_DECTOT = i_oFrom.w_DECTOT
    this.w_CALCPIP = i_oFrom.w_CALCPIP
    this.w_AMQUOFIS = i_oFrom.w_AMQUOFIS
    this.w_AMQUOFI1 = i_oFrom.w_AMQUOFI1
    this.w_NSOAMF = i_oFrom.w_NSOAMF
    this.w_AMMIMM = i_oFrom.w_AMMIMM
    this.w_AMPERORD_T = i_oFrom.w_AMPERORD_T
    this.w_TOTRIGA = i_oFrom.w_TOTRIGA
    this.w_FLRIDU = i_oFrom.w_FLRIDU
    this.w_PMAXPEORD = i_oFrom.w_PMAXPEORD
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_AMCODCES = this.w_AMCODCES
    i_oTo.w_CODAZI = this.w_CODAZI
    i_oTo.w_AMMANT = this.w_AMMANT
    i_oTo.w_MAXANT = this.w_MAXANT
    i_oTo.w_PERRID1 = this.w_PERRID1
    i_oTo.w_TIPBEN = this.w_TIPBEN
    i_oTo.w_AMPROESE = this.w_AMPROESE
    i_oTo.w_AMTIPAMM = this.w_AMTIPAMM
    i_oTo.w_MAXPEORD = this.w_MAXPEORD
    i_oTo.w_COEFIS = this.w_COEFIS
    i_oTo.w_AMPERORD = this.w_AMPERORD
    i_oTo.w_MAXPEANT = this.w_MAXPEANT
    i_oTo.w_AMPERANT = this.w_AMPERANT
    i_oTo.w_TOTPEORD = this.w_TOTPEORD
    i_oTo.w_TOTPEANT = this.w_TOTPEANT
    i_oTo.w_TOTALE = this.w_TOTALE
    i_oTo.w_TIPAMM = this.w_TIPAMM
    i_oTo.w_IASADO = this.w_IASADO
    i_oTo.w_CODESE = this.w_CODESE
    i_oTo.w_VALNAZ = this.w_VALNAZ
    i_oTo.w_ESPRIU = this.w_ESPRIU
    i_oTo.w_DECTOT = this.w_DECTOT
    i_oTo.w_CALCPIP = this.w_CALCPIP
    i_oTo.w_AMQUOFIS = this.w_AMQUOFIS
    i_oTo.w_AMQUOFI1 = this.w_AMQUOFI1
    i_oTo.w_NSOAMF = this.w_NSOAMF
    i_oTo.w_AMMIMM = this.w_AMMIMM
    i_oTo.w_AMPERORD_T = this.w_AMPERORD_T
    i_oTo.w_TOTRIGA = this.w_TOTRIGA
    i_oTo.w_FLRIDU = this.w_FLRIDU
    i_oTo.w_PMAXPEORD = this.w_PMAXPEORD
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsce_mam as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 567
  Height = 155
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-09-13"
  HelpContextID=137738903
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=31

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  AMM_CESP_IDX = 0
  PAR_CESP_IDX = 0
  ESERCIZI_IDX = 0
  VALUTE_IDX = 0
  cFile = "AMM_CESP"
  cKeySelect = "AMCODCES"
  cKeyWhere  = "AMCODCES=this.w_AMCODCES"
  cKeyDetail  = "AMCODCES=this.w_AMCODCES and AMPROESE=this.w_AMPROESE"
  cKeyWhereODBC = '"AMCODCES="+cp_ToStrODBC(this.w_AMCODCES)';

  cKeyDetailWhereODBC = '"AMCODCES="+cp_ToStrODBC(this.w_AMCODCES)';
      +'+" and AMPROESE="+cp_ToStrODBC(this.w_AMPROESE)';

  cKeyWhereODBCqualified = '"AMM_CESP.AMCODCES="+cp_ToStrODBC(this.w_AMCODCES)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cPrg = "gsce_mam"
  cComment = "Cespite/ammortamenti"
  i_nRowNum = 0
  i_nRowPerPage = 5
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_AMCODCES = space(20)
  w_CODAZI = space(10)
  w_AMMANT = 0
  w_MAXANT = 0
  w_PERRID1 = 0
  w_TIPBEN = space(1)
  w_AMPROESE = 0
  o_AMPROESE = 0
  w_AMTIPAMM = space(1)
  o_AMTIPAMM = space(1)
  w_MAXPEORD = 0
  o_MAXPEORD = 0
  w_COEFIS = 0
  o_COEFIS = 0
  w_AMPERORD = 0
  o_AMPERORD = 0
  w_MAXPEANT = 0
  o_MAXPEANT = 0
  w_AMPERANT = 0
  w_TOTPEORD = 0
  w_TOTPEANT = 0
  w_TOTALE = 0
  w_TIPAMM = space(1)
  w_IASADO = space(1)
  w_CODESE = space(4)
  o_CODESE = space(4)
  w_VALNAZ = space(3)
  w_ESPRIU = space(4)
  o_ESPRIU = space(4)
  w_DECTOT = 0
  w_CALCPIP = 0
  w_AMQUOFIS = 0
  w_AMQUOFI1 = 0
  w_NSOAMF = space(1)
  w_AMMIMM = space(1)
  w_AMPERORD_T = 0
  w_TOTRIGA = 0
  w_FLRIDU = space(1)
  o_FLRIDU = space(1)
  w_PMAXPEORD = 0
  o_PMAXPEORD = 0
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsce_mamPag1","gsce_mam",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='PAR_CESP'
    this.cWorkTables[2]='ESERCIZI'
    this.cWorkTables[3]='VALUTE'
    this.cWorkTables[4]='AMM_CESP'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(4))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.AMM_CESP_IDX,5],7]
    this.nPostItConn=i_TableProp[this.AMM_CESP_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsce_mam'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from AMM_CESP where AMCODCES=KeySet.AMCODCES
    *                            and AMPROESE=KeySet.AMPROESE
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- gsce_mam
      * --- Setta Ordine per Progressivo Periodo
      i_cOrder = 'order by AMPROESE '
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.AMM_CESP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.AMM_CESP_IDX,2],this.bLoadRecFilter,this.AMM_CESP_IDX,"gsce_mam")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('AMM_CESP')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "AMM_CESP.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' AMM_CESP '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'AMCODCES',this.w_AMCODCES  )
      select * from (i_cTable) AMM_CESP where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_CODAZI = i_CODAZI
        .w_AMMANT = 0
        .w_MAXANT = 0
        .w_PERRID1 = 0
        .w_TIPBEN = space(1)
        .w_COEFIS = 0
        .w_TOTPEORD = 0
        .w_TOTPEANT = 0
        .w_TOTALE = 0
        .w_TIPAMM = space(1)
        .w_IASADO = space(1)
        .w_FLRIDU = space(1)
        .w_AMCODCES = NVL(AMCODCES,space(20))
          .link_1_2('Load')
        .w_ESPRIU = this.oParentObject.w_CEESPRIU
        .w_NSOAMF = this.oParentObject.w_CENSOAMF
        .w_AMMIMM = This.oParentObject.w_CEAMMIMM
        .w_PMAXPEORD = cp_ROUND(.w_COEFIS*iif( .w_TIPBEN='M' AND .w_FLRIDU='S' AND .w_PERRID1<>0 , .w_PERRID1/100, 1),2)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'AMM_CESP')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      this.w_TOTPEORD = 0
      this.w_TOTPEANT = 0
      this.w_TOTALE = 0
      scan
        with this
          .w_VALNAZ = space(3)
          .w_DECTOT = 0
          .w_AMPROESE = NVL(AMPROESE,0)
          .w_AMTIPAMM = NVL(AMTIPAMM,space(1))
        .w_MAXPEORD = iif(.w_AMPROESE=1, .w_PMAXPEORD, .w_COEFIS)
          .w_AMPERORD = NVL(AMPERORD,0)
        .w_MAXPEANT = cp_ROUND(IIF(.w_TIPBEN='I',0,(.w_AMPERORD*(.w_AMMANT/100))),2)
          .w_AMPERANT = NVL(AMPERANT,0)
        .w_CODESE = CALCESCES(.w_ESPRIU,.w_AMPROESE)
          .link_2_7('Load')
          .link_2_8('Load')
        .w_CALCPIP = IIF(Empty(.w_CODESE),0,DEFPIP(.w_DECTOT))
          .w_AMQUOFIS = NVL(AMQUOFIS,0)
          .w_AMQUOFI1 = NVL(AMQUOFI1,0)
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_13.Calculate()
        .w_AMPERORD_T = IIF(.w_AMTIPAMM='R',0,.w_AMPERORD)
        .w_TOTRIGA = .w_AMPERORD_T+.w_AMPERANT
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          .w_TOTPEORD = .w_TOTPEORD+.w_AMPERORD_T
          .w_TOTPEANT = .w_TOTPEANT+.w_AMPERANT
          .w_TOTALE = .w_TOTALE+.w_TOTRIGA
          replace AMPROESE with .w_AMPROESE
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .w_ESPRIU = this.oParentObject.w_CEESPRIU
        .w_NSOAMF = this.oParentObject.w_CENSOAMF
        .w_AMMIMM = This.oParentObject.w_CEAMMIMM
        .w_PMAXPEORD = cp_ROUND(.w_COEFIS*iif( .w_TIPBEN='M' AND .w_FLRIDU='S' AND .w_PERRID1<>0 , .w_PERRID1/100, 1),2)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_AMCODCES=space(20)
      .w_CODAZI=space(10)
      .w_AMMANT=0
      .w_MAXANT=0
      .w_PERRID1=0
      .w_TIPBEN=space(1)
      .w_AMPROESE=0
      .w_AMTIPAMM=space(1)
      .w_MAXPEORD=0
      .w_COEFIS=0
      .w_AMPERORD=0
      .w_MAXPEANT=0
      .w_AMPERANT=0
      .w_TOTPEORD=0
      .w_TOTPEANT=0
      .w_TOTALE=0
      .w_TIPAMM=space(1)
      .w_IASADO=space(1)
      .w_CODESE=space(4)
      .w_VALNAZ=space(3)
      .w_ESPRIU=space(4)
      .w_DECTOT=0
      .w_CALCPIP=0
      .w_AMQUOFIS=0
      .w_AMQUOFI1=0
      .w_NSOAMF=space(1)
      .w_AMMIMM=space(1)
      .w_AMPERORD_T=0
      .w_TOTRIGA=0
      .w_FLRIDU=space(1)
      .w_PMAXPEORD=0
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
        .w_CODAZI = i_CODAZI
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODAZI))
         .link_1_2('Full')
        endif
        .DoRTCalc(3,7,.f.)
        .w_AMTIPAMM = 'O'
        .w_MAXPEORD = iif(.w_AMPROESE=1, .w_PMAXPEORD, .w_COEFIS)
        .DoRTCalc(10,10,.f.)
        .w_AMPERORD = INT(iif(.w_AMPROESE=1, .w_PMAXPEORD, .w_COEFIS) * iif(.w_AMPROESE>0,1,0)*100)/100
        .w_MAXPEANT = cp_ROUND(IIF(.w_TIPBEN='I',0,(.w_AMPERORD*(.w_AMMANT/100))),2)
        .w_AMPERANT = INT(iif(.w_AMTIPAMM='A', .w_MAXPEANT, 0) * iif(.w_AMPROESE>0,1,0)*100)/100
        .DoRTCalc(14,18,.f.)
        .w_CODESE = CALCESCES(.w_ESPRIU,.w_AMPROESE)
        .DoRTCalc(19,19,.f.)
        if not(empty(.w_CODESE))
         .link_2_7('Full')
        endif
        .DoRTCalc(20,20,.f.)
        if not(empty(.w_VALNAZ))
         .link_2_8('Full')
        endif
        .w_ESPRIU = this.oParentObject.w_CEESPRIU
        .DoRTCalc(22,22,.f.)
        .w_CALCPIP = IIF(Empty(.w_CODESE),0,DEFPIP(.w_DECTOT))
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_13.Calculate()
        .DoRTCalc(24,25,.f.)
        .w_NSOAMF = this.oParentObject.w_CENSOAMF
        .w_AMMIMM = This.oParentObject.w_CEAMMIMM
        .w_AMPERORD_T = IIF(.w_AMTIPAMM='R',0,.w_AMPERORD)
        .w_TOTRIGA = .w_AMPERORD_T+.w_AMPERANT
        .DoRTCalc(30,30,.f.)
        .w_PMAXPEORD = cp_ROUND(.w_COEFIS*iif( .w_TIPBEN='M' AND .w_FLRIDU='S' AND .w_PERRID1<>0 , .w_PERRID1/100, 1),2)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'AMM_CESP')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oBody.oBodyCol.oRow.oObj_2_13.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'AMM_CESP',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.AMM_CESP_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AMCODCES,"AMCODCES",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_AMPROESE N(3);
      ,t_AMTIPAMM N(3);
      ,t_AMPERORD N(6,2);
      ,t_AMPERANT N(6,2);
      ,t_CODESE C(4);
      ,t_AMQUOFIS N(18,4);
      ,t_AMQUOFI1 N(18,4);
      ,AMPROESE N(3);
      ,t_MAXPEORD N(6,2);
      ,t_MAXPEANT N(6,2);
      ,t_VALNAZ C(3);
      ,t_DECTOT N(1);
      ,t_CALCPIP N(1);
      ,t_AMPERORD_T N(6,2);
      ,t_TOTRIGA N(6,2);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsce_mambodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oAMPROESE_2_1.controlsource=this.cTrsName+'.t_AMPROESE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oAMTIPAMM_2_2.controlsource=this.cTrsName+'.t_AMTIPAMM'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oAMPERORD_2_4.controlsource=this.cTrsName+'.t_AMPERORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oAMPERANT_2_6.controlsource=this.cTrsName+'.t_AMPERANT'
    this.oPgFRm.Page1.oPag.oCODESE_2_7.controlsource=this.cTrsName+'.t_CODESE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oAMQUOFIS_2_11.controlsource=this.cTrsName+'.t_AMQUOFIS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oAMQUOFI1_2_12.controlsource=this.cTrsName+'.t_AMQUOFI1'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(60)
    this.AddVLine(181)
    this.AddVLine(258)
    this.AddVLine(333)
    this.AddVLine(438)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAMPROESE_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.AMM_CESP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.AMM_CESP_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.AMM_CESP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.AMM_CESP_IDX,2])
      *
      * insert into AMM_CESP
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'AMM_CESP')
        i_extval=cp_InsertValODBCExtFlds(this,'AMM_CESP')
        i_cFldBody=" "+;
                  "(AMCODCES,AMPROESE,AMTIPAMM,AMPERORD,AMPERANT"+;
                  ",AMQUOFIS,AMQUOFI1,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_AMCODCES)+","+cp_ToStrODBC(this.w_AMPROESE)+","+cp_ToStrODBC(this.w_AMTIPAMM)+","+cp_ToStrODBC(this.w_AMPERORD)+","+cp_ToStrODBC(this.w_AMPERANT)+;
             ","+cp_ToStrODBC(this.w_AMQUOFIS)+","+cp_ToStrODBC(this.w_AMQUOFI1)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'AMM_CESP')
        i_extval=cp_InsertValVFPExtFlds(this,'AMM_CESP')
        cp_CheckDeletedKey(i_cTable,0,'AMCODCES',this.w_AMCODCES,'AMPROESE',this.w_AMPROESE)
        INSERT INTO (i_cTable) (;
                   AMCODCES;
                  ,AMPROESE;
                  ,AMTIPAMM;
                  ,AMPERORD;
                  ,AMPERANT;
                  ,AMQUOFIS;
                  ,AMQUOFI1;
                  ,CPCCCHK &i_extfld.) VALUES (;
                  this.w_AMCODCES;
                  ,this.w_AMPROESE;
                  ,this.w_AMTIPAMM;
                  ,this.w_AMPERORD;
                  ,this.w_AMPERANT;
                  ,this.w_AMQUOFIS;
                  ,this.w_AMQUOFI1;
                  ,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.AMM_CESP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.AMM_CESP_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (t_AMPROESE<>0) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'AMM_CESP')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and AMPROESE="+cp_ToStrODBC(&i_TN.->AMPROESE)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'AMM_CESP')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and AMPROESE=&i_TN.->AMPROESE;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (t_AMPROESE<>0) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and AMPROESE="+cp_ToStrODBC(&i_TN.->AMPROESE)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and AMPROESE=&i_TN.->AMPROESE;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = 0
              replace AMPROESE with this.w_AMPROESE
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update AMM_CESP
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'AMM_CESP')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " AMTIPAMM="+cp_ToStrODBC(this.w_AMTIPAMM)+;
                     ",AMPERORD="+cp_ToStrODBC(this.w_AMPERORD)+;
                     ",AMPERANT="+cp_ToStrODBC(this.w_AMPERANT)+;
                     ",AMQUOFIS="+cp_ToStrODBC(this.w_AMQUOFIS)+;
                     ",AMQUOFI1="+cp_ToStrODBC(this.w_AMQUOFI1)+;
                     ",AMPROESE="+cp_ToStrODBC(this.w_AMPROESE)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+;
                             " and AMPROESE="+cp_ToStrODBC(AMPROESE)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'AMM_CESP')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      AMTIPAMM=this.w_AMTIPAMM;
                     ,AMPERORD=this.w_AMPERORD;
                     ,AMPERANT=this.w_AMPERANT;
                     ,AMQUOFIS=this.w_AMQUOFIS;
                     ,AMQUOFI1=this.w_AMQUOFI1;
                     ,AMPROESE=this.w_AMPROESE;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                                      and AMPROESE=&i_TN.->AMPROESE;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- gsce_mam
    *Controlla la sequenza dei progressivi
    this.NotifyEvent('Controlli')
    
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.AMM_CESP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.AMM_CESP_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (t_AMPROESE<>0) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete AMM_CESP
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and AMPROESE="+cp_ToStrODBC(&i_TN.->AMPROESE)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and AMPROESE=&i_TN.->AMPROESE;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (t_AMPROESE<>0) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.AMM_CESP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.AMM_CESP_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
          .link_1_2('Full')
        .DoRTCalc(3,8,.t.)
        if .o_AMPROESE<>.w_AMPROESE.or. .o_AMTIPAMM<>.w_AMTIPAMM.or. .o_FLRIDU<>.w_FLRIDU.or. .o_COEFIS<>.w_COEFIS.or. .o_PMAXPEORD<>.w_PMAXPEORD.or. .o_AMPERORD<>.w_AMPERORD
          .w_MAXPEORD = iif(.w_AMPROESE=1, .w_PMAXPEORD, .w_COEFIS)
        endif
        .DoRTCalc(10,10,.t.)
        if .o_MAXPEORD<>.w_MAXPEORD.or. .o_AMPROESE<>.w_AMPROESE.or. .o_AMTIPAMM<>.w_AMTIPAMM
          .w_AMPERORD = INT(iif(.w_AMPROESE=1, .w_PMAXPEORD, .w_COEFIS) * iif(.w_AMPROESE>0,1,0)*100)/100
        endif
        if .o_AMPROESE<>.w_AMPROESE.or. .o_AMTIPAMM<>.w_AMTIPAMM
          .w_MAXPEANT = cp_ROUND(IIF(.w_TIPBEN='I',0,(.w_AMPERORD*(.w_AMMANT/100))),2)
        endif
        if .o_MAXPEANT<>.w_MAXPEANT.or. .o_AMPROESE<>.w_AMPROESE.or. .o_AMTIPAMM<>.w_AMTIPAMM
          .w_TOTPEANT = .w_TOTPEANT-.w_amperant
          .w_AMPERANT = INT(iif(.w_AMTIPAMM='A', .w_MAXPEANT, 0) * iif(.w_AMPROESE>0,1,0)*100)/100
          .w_TOTPEANT = .w_TOTPEANT+.w_amperant
        endif
        .DoRTCalc(14,18,.t.)
        if .o_ESPRIU<>.w_ESPRIU.or. .o_AMPROESE<>.w_AMPROESE
          .w_CODESE = CALCESCES(.w_ESPRIU,.w_AMPROESE)
          .link_2_7('Full')
        endif
          .link_2_8('Full')
          .w_ESPRIU = this.oParentObject.w_CEESPRIU
        .DoRTCalc(22,22,.t.)
        if .o_AMPROESE<>.w_AMPROESE.or. .o_CODESE<>.w_CODESE
          .w_CALCPIP = IIF(Empty(.w_CODESE),0,DEFPIP(.w_DECTOT))
        endif
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_13.Calculate()
        .DoRTCalc(24,25,.t.)
          .w_NSOAMF = this.oParentObject.w_CENSOAMF
          .w_AMMIMM = This.oParentObject.w_CEAMMIMM
        if .o_AMPERORD<>.w_AMPERORD.or. .o_AMTIPAMM<>.w_AMTIPAMM
          .w_TOTPEORD = .w_TOTPEORD-.w_amperord_t
          .w_AMPERORD_T = IIF(.w_AMTIPAMM='R',0,.w_AMPERORD)
          .w_TOTPEORD = .w_TOTPEORD+.w_amperord_t
        endif
          .w_TOTALE = .w_TOTALE-.w_totriga
          .w_TOTRIGA = .w_AMPERORD_T+.w_AMPERANT
          .w_TOTALE = .w_TOTALE+.w_totriga
        .DoRTCalc(30,30,.t.)
        if .o_AMTIPAMM<>.w_AMTIPAMM.or. .o_FLRIDU<>.w_FLRIDU.or. .o_COEFIS<>.w_COEFIS.or. .o_AMPROESE<>.w_AMPROESE
          .w_PMAXPEORD = cp_ROUND(.w_COEFIS*iif( .w_TIPBEN='M' AND .w_FLRIDU='S' AND .w_PERRID1<>0 , .w_PERRID1/100, 1),2)
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_MAXPEORD with this.w_MAXPEORD
      replace t_MAXPEANT with this.w_MAXPEANT
      replace t_VALNAZ with this.w_VALNAZ
      replace t_DECTOT with this.w_DECTOT
      replace t_CALCPIP with this.w_CALCPIP
      replace t_AMPERORD_T with this.w_AMPERORD_T
      replace t_TOTRIGA with this.w_TOTRIGA
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_13.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_13.Calculate()
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oAMTIPAMM_2_2.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oAMTIPAMM_2_2.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oAMPERORD_2_4.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oAMPERORD_2_4.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oAMPERANT_2_6.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oAMPERANT_2_6.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oAMQUOFIS_2_11.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oAMQUOFIS_2_11.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oAMQUOFI1_2_12.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oAMQUOFI1_2_12.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsce_mam
    * --- Messaggio di avviso per Rateo utilizzo
    
    If cEvent = "w_AMTIPAMM Changed" And this.w_AMTIPAMM='R'
       Ah_ErrorMsg("Attenzione! La percentuale impostata su riga con tipo ammortamento Rateo Utilizzo non verr� considerata nel totale")
    Endif
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_13.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_CESP_IDX,3]
    i_lTable = "PAR_CESP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_CESP_IDX,2], .t., this.PAR_CESP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_CESP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PCCODAZI,PCPERANT,PCMAXANT,PCPERESE,PCTIPAMM,PCIASADO";
                   +" from "+i_cTable+" "+i_lTable+" where PCCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PCCODAZI',this.w_CODAZI)
            select PCCODAZI,PCPERANT,PCMAXANT,PCPERESE,PCTIPAMM,PCIASADO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.PCCODAZI,space(10))
      this.w_AMMANT = NVL(_Link_.PCPERANT,0)
      this.w_MAXANT = NVL(_Link_.PCMAXANT,0)
      this.w_PERRID1 = NVL(_Link_.PCPERESE,0)
      this.w_TIPAMM = NVL(_Link_.PCTIPAMM,space(1))
      this.w_IASADO = NVL(_Link_.PCIASADO,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(10)
      endif
      this.w_AMMANT = 0
      this.w_MAXANT = 0
      this.w_PERRID1 = 0
      this.w_TIPAMM = space(1)
      this.w_IASADO = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_CESP_IDX,2])+'\'+cp_ToStr(_Link_.PCCODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_CESP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODESE
  func Link_2_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_CODESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_CODESE)
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODESE = NVL(_Link_.ESCODESE,space(4))
      this.w_VALNAZ = NVL(_Link_.ESVALNAZ,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_CODESE = space(4)
      endif
      this.w_VALNAZ = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VALNAZ
  func Link_2_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALNAZ) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALNAZ)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALNAZ);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALNAZ)
            select VACODVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALNAZ = NVL(_Link_.VACODVAL,space(3))
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_VALNAZ = space(3)
      endif
      this.w_DECTOT = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALNAZ Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oTOTPEORD_3_1.value==this.w_TOTPEORD)
      this.oPgFrm.Page1.oPag.oTOTPEORD_3_1.value=this.w_TOTPEORD
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTPEANT_3_2.value==this.w_TOTPEANT)
      this.oPgFrm.Page1.oPag.oTOTPEANT_3_2.value=this.w_TOTPEANT
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTALE_3_3.value==this.w_TOTALE)
      this.oPgFrm.Page1.oPag.oTOTALE_3_3.value=this.w_TOTALE
    endif
    if not(this.oPgFrm.Page1.oPag.oCODESE_2_7.value==this.w_CODESE)
      this.oPgFrm.Page1.oPag.oCODESE_2_7.value=this.w_CODESE
      replace t_CODESE with this.oPgFrm.Page1.oPag.oCODESE_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAMPROESE_2_1.value==this.w_AMPROESE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAMPROESE_2_1.value=this.w_AMPROESE
      replace t_AMPROESE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAMPROESE_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAMTIPAMM_2_2.RadioValue()==this.w_AMTIPAMM)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAMTIPAMM_2_2.SetRadio()
      replace t_AMTIPAMM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAMTIPAMM_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAMPERORD_2_4.value==this.w_AMPERORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAMPERORD_2_4.value=this.w_AMPERORD
      replace t_AMPERORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAMPERORD_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAMPERANT_2_6.value==this.w_AMPERANT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAMPERANT_2_6.value=this.w_AMPERANT
      replace t_AMPERANT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAMPERANT_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAMQUOFIS_2_11.value==this.w_AMQUOFIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAMQUOFIS_2_11.value=this.w_AMQUOFIS
      replace t_AMQUOFIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAMQUOFIS_2_11.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAMQUOFI1_2_12.value==this.w_AMQUOFI1)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAMQUOFI1_2_12.value=this.w_AMQUOFI1
      replace t_AMQUOFI1 with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAMQUOFI1_2_12.value
    endif
    cp_SetControlsValueExtFlds(this,'AMM_CESP')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsce_mam
      * --Controllo totale percentuali: non pu� essere superiore a 100
      if this.w_TOTALE > 100 AND this.w_TOTALE<>0
         i_bnoChk=.f.
         i_bRes=.f.
         i_cErrorMsg=ah_MsgFormat("Il totale delle percentuali non pu� essere superiore a 100")
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(.w_AMTIPAMM='O' OR (.w_TIPBEN='M' AND .w_AMTIPAMM='A' AND .w_AMPROESE<=.w_MAXANT) OR ( (.w_TIPBEN='I' OR (.w_IASADO='S' and .w_TIPBEN='M') ) AND .w_AMTIPAMM='R' AND .w_AMPROESE=1)) and (.w_TIPAMM<>'C' AND NOT EMPTY(.w_ESPRIU) AND .w_NSOAMF<>'S' AND .w_AMMIMM<>'S') and (.w_AMPROESE<>0)
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAMTIPAMM_2_2
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Tipo ammortamento non corretto")
        case   not((.w_AMPERORD>0 AND .w_AMPERORD<=iif(.w_AMPROESE=1, .w_PMAXPEORD, .w_COEFIS)) OR .w_AMPROESE=0 OR (.w_AMPERORD=100 AND .w_COEFIS=100)) and (.w_TIPAMM<>'C' AND NOT EMPTY(.w_ESPRIU) AND .w_NSOAMF<>'S' AND .w_AMMIMM<>'S') and (.w_AMPROESE<>0)
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAMPERORD_2_4
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Valore errato")
        case   not((.w_AMPERANT>=0 AND .w_AMPERANT<=.w_MAXPEANT) OR .w_AMPROESE=0) and (.w_AMTIPAMM='A' AND .w_TIPAMM<>'C' AND NOT EMPTY(.w_ESPRIU) AND .w_NSOAMF<>'S' AND .w_AMMIMM<>'S') and (.w_AMPROESE<>0)
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAMPERANT_2_6
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Valore errato")
      endcase
      if .w_AMPROESE<>0
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_AMPROESE = this.w_AMPROESE
    this.o_AMTIPAMM = this.w_AMTIPAMM
    this.o_MAXPEORD = this.w_MAXPEORD
    this.o_COEFIS = this.w_COEFIS
    this.o_AMPERORD = this.w_AMPERORD
    this.o_MAXPEANT = this.w_MAXPEANT
    this.o_CODESE = this.w_CODESE
    this.o_ESPRIU = this.w_ESPRIU
    this.o_FLRIDU = this.w_FLRIDU
    this.o_PMAXPEORD = this.w_PMAXPEORD
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(t_AMPROESE<>0)
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_AMPROESE=0
      .w_AMTIPAMM=space(1)
      .w_MAXPEORD=0
      .w_AMPERORD=0
      .w_MAXPEANT=0
      .w_AMPERANT=0
      .w_CODESE=space(4)
      .w_VALNAZ=space(3)
      .w_DECTOT=0
      .w_CALCPIP=0
      .w_AMQUOFIS=0
      .w_AMQUOFI1=0
      .w_AMPERORD_T=0
      .w_TOTRIGA=0
      .DoRTCalc(1,7,.f.)
        .w_AMTIPAMM = 'O'
        .w_MAXPEORD = iif(.w_AMPROESE=1, .w_PMAXPEORD, .w_COEFIS)
      .DoRTCalc(10,10,.f.)
        .w_AMPERORD = INT(iif(.w_AMPROESE=1, .w_PMAXPEORD, .w_COEFIS) * iif(.w_AMPROESE>0,1,0)*100)/100
        .w_MAXPEANT = cp_ROUND(IIF(.w_TIPBEN='I',0,(.w_AMPERORD*(.w_AMMANT/100))),2)
        .w_AMPERANT = INT(iif(.w_AMTIPAMM='A', .w_MAXPEANT, 0) * iif(.w_AMPROESE>0,1,0)*100)/100
      .DoRTCalc(14,18,.f.)
        .w_CODESE = CALCESCES(.w_ESPRIU,.w_AMPROESE)
      .DoRTCalc(19,19,.f.)
      if not(empty(.w_CODESE))
        .link_2_7('Full')
      endif
      .DoRTCalc(20,20,.f.)
      if not(empty(.w_VALNAZ))
        .link_2_8('Full')
      endif
      .DoRTCalc(21,22,.f.)
        .w_CALCPIP = IIF(Empty(.w_CODESE),0,DEFPIP(.w_DECTOT))
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_13.Calculate()
      .DoRTCalc(24,27,.f.)
        .w_AMPERORD_T = IIF(.w_AMTIPAMM='R',0,.w_AMPERORD)
        .w_TOTRIGA = .w_AMPERORD_T+.w_AMPERANT
    endwith
    this.DoRTCalc(30,31,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_AMPROESE = t_AMPROESE
    this.w_AMTIPAMM = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAMTIPAMM_2_2.RadioValue(.t.)
    this.w_MAXPEORD = t_MAXPEORD
    this.w_AMPERORD = t_AMPERORD
    this.w_MAXPEANT = t_MAXPEANT
    this.w_AMPERANT = t_AMPERANT
    this.w_CODESE = t_CODESE
    this.w_VALNAZ = t_VALNAZ
    this.w_DECTOT = t_DECTOT
    this.w_CALCPIP = t_CALCPIP
    this.w_AMQUOFIS = t_AMQUOFIS
    this.w_AMQUOFI1 = t_AMQUOFI1
    this.w_AMPERORD_T = t_AMPERORD_T
    this.w_TOTRIGA = t_TOTRIGA
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_AMPROESE with this.w_AMPROESE
    replace t_AMTIPAMM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAMTIPAMM_2_2.ToRadio()
    replace t_MAXPEORD with this.w_MAXPEORD
    replace t_AMPERORD with this.w_AMPERORD
    replace t_MAXPEANT with this.w_MAXPEANT
    replace t_AMPERANT with this.w_AMPERANT
    replace t_CODESE with this.w_CODESE
    replace t_VALNAZ with this.w_VALNAZ
    replace t_DECTOT with this.w_DECTOT
    replace t_CALCPIP with this.w_CALCPIP
    replace t_AMQUOFIS with this.w_AMQUOFIS
    replace t_AMQUOFI1 with this.w_AMQUOFI1
    replace t_AMPERORD_T with this.w_AMPERORD_T
    replace t_TOTRIGA with this.w_TOTRIGA
    if i_srv='A'
      replace AMPROESE with this.w_AMPROESE
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
     with this
        .w_TOTPEANT = .w_TOTPEANT-.w_amperant
        .w_TOTPEORD = .w_TOTPEORD-.w_amperord_t
        .w_TOTALE = .w_TOTALE-.w_totriga
        .SetControlsValue()
      endwith
  EndProc
enddefine

* --- Define pages as container
define class tgsce_mamPag1 as StdContainer
  Width  = 563
  height = 155
  stdWidth  = 563
  stdheight = 155
  resizeXpos=535
  resizeYpos=102
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=6, top=5, width=551,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=6,Field1="AMPROESE",Label1="Prog.es.",Field2="AMTIPAMM",Label2="Tipo amm.to",Field3="AMPERORD",Label3="% Ordinario",Field4="AMPERANT",Label4="% Anticipato",Field5="AMQUOFIS",Label5="Q.fissa amm.ord.",Field6="AMQUOFI1",Label6="Q.fissa amm.ant.",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 83041158

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-3,top=24,;
    width=546+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*5*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-2,top=25,width=545+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*5*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oCODESE_2_7.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc


  add object oCODESE_2_7 as StdTrsField with uid="NKFAQSWAZP",rtseq=19,rtrep=.t.,;
    cFormVar="w_CODESE",value=space(4),enabled=.f.,;
    ToolTipText = "Codice esercizio corrispondente al'es.progressivo dell'ammortamento del cespite",;
    HelpContextID = 223392218,;
    cTotal="", bFixedPos=.t., cQueryName = "CODESE",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=49, Left=5, Top=126, InputMask=replicate('X',4), cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_CODESE"

  func oCODESE_2_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oBeginFooter as BodyKeyMover with nDirection=-1

  add object oTOTPEORD_3_1 as StdField with uid="ELBYGNGHIN",rtseq=14,rtrep=.f.,;
    cFormVar="w_TOTPEORD",value=0,enabled=.f.,;
    HelpContextID = 69513350,;
    cQueryName = "TOTPEORD",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=186, Top=126, cSayPict=["999.99"], cGetPict=["999.99"]

  add object oTOTPEANT_3_2 as StdField with uid="TIWSAYCQQO",rtseq=15,rtrep=.f.,;
    cFormVar="w_TOTPEANT",value=0,enabled=.f.,;
    HelpContextID = 232476554,;
    cQueryName = "TOTPEANT",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=261, Top=126, cSayPict=["999.99"], cGetPict=["999.99"]

  add object oTOTALE_3_3 as StdField with uid="YWFOHMELCC",rtseq=16,rtrep=.f.,;
    cFormVar="w_TOTALE",value=0,enabled=.f.,;
    HelpContextID = 230928586,;
    cQueryName = "TOTALE",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=113, Top=126, cSayPict=["999.99"], cGetPict=["999.99"]

  add object oStr_3_4 as StdString with uid="PKNJBSEHRU",Visible=.t., Left=59, Top=126,;
    Alignment=1, Width=53, Height=15,;
    Caption="Totale:"  ;
  , bGlobalFont=.t.
enddefine

* --- Defining Body row
define class tgsce_mamBodyRow as CPBodyRowCnt
  Width=536
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oAMPROESE_2_1 as StdTrsField with uid="YDKCJNNRVJ",rtseq=7,rtrep=.t.,;
    cFormVar="w_AMPROESE",value=0,isprimarykey=.t.,;
    ToolTipText = "Progressivo esercizio",;
    HelpContextID = 226685877,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=48, Left=-2, Top=0, cSayPict=["999"], cGetPict=["999"]

  add object oAMTIPAMM_2_2 as StdTrsCombo with uid="AMUJLXLDUR",rtrep=.t.,;
    cFormVar="w_AMTIPAMM", RowSource=""+"Anticipato,"+"Ordinario,"+"Rateo utilizzo" , ;
    ToolTipText = "Tipo ammortamento",;
    HelpContextID = 243551315,;
    Height=21, Width=110, Left=57, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2  , sErrorMsg = "Tipo ammortamento non corretto";
, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oAMTIPAMM_2_2.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..AMTIPAMM,&i_cF..t_AMTIPAMM),this.value)
    return(iif(xVal =1,'A',;
    iif(xVal =2,'O',;
    iif(xVal =3,'R',;
    space(1)))))
  endfunc
  func oAMTIPAMM_2_2.GetRadio()
    this.Parent.oContained.w_AMTIPAMM = this.RadioValue()
    return .t.
  endfunc

  func oAMTIPAMM_2_2.ToRadio()
    this.Parent.oContained.w_AMTIPAMM=trim(this.Parent.oContained.w_AMTIPAMM)
    return(;
      iif(this.Parent.oContained.w_AMTIPAMM=='A',1,;
      iif(this.Parent.oContained.w_AMTIPAMM=='O',2,;
      iif(this.Parent.oContained.w_AMTIPAMM=='R',3,;
      0))))
  endfunc

  func oAMTIPAMM_2_2.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oAMTIPAMM_2_2.mCond()
    with this.Parent.oContained
      return (.w_TIPAMM<>'C' AND NOT EMPTY(.w_ESPRIU) AND .w_NSOAMF<>'S' AND .w_AMMIMM<>'S')
    endwith
  endfunc

  func oAMTIPAMM_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_AMTIPAMM='O' OR (.w_TIPBEN='M' AND .w_AMTIPAMM='A' AND .w_AMPROESE<=.w_MAXANT) OR ( (.w_TIPBEN='I' OR (.w_IASADO='S' and .w_TIPBEN='M') ) AND .w_AMTIPAMM='R' AND .w_AMPROESE=1))
    endwith
    return bRes
  endfunc

  add object oAMPERORD_2_4 as StdTrsField with uid="DAZWDRADGM",rtseq=11,rtrep=.t.,;
    cFormVar="w_AMPERORD",value=0,;
    ToolTipText = "Percentuale ammortamento ordinario",;
    HelpContextID = 56619958,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Valore errato",;
   bGlobalFont=.t.,;
    Height=17, Width=69, Left=177, Top=0, cSayPict=["999.99"], cGetPict=["999.99"]

  func oAMPERORD_2_4.mCond()
    with this.Parent.oContained
      return (.w_TIPAMM<>'C' AND NOT EMPTY(.w_ESPRIU) AND .w_NSOAMF<>'S' AND .w_AMMIMM<>'S')
    endwith
  endfunc

  func oAMPERORD_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_AMPERORD>0 AND .w_AMPERORD<=iif(.w_AMPROESE=1, .w_PMAXPEORD, .w_COEFIS)) OR .w_AMPROESE=0 OR (.w_AMPERORD=100 AND .w_COEFIS=100))
    endwith
    return bRes
  endfunc

  add object oAMPERANT_2_6 as StdTrsField with uid="YYLVJSAKLV",rtseq=13,rtrep=.t.,;
    cFormVar="w_AMPERANT",value=0,;
    ToolTipText = "Percentuale ammortamento anticipato",;
    HelpContextID = 245369946,;
    cTotal = "this.Parent.oContained.w_totpeant", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Valore errato",;
   bGlobalFont=.t.,;
    Height=17, Width=69, Left=252, Top=0, cSayPict=["999.99"], cGetPict=["999.99"]

  func oAMPERANT_2_6.mCond()
    with this.Parent.oContained
      return (.w_AMTIPAMM='A' AND .w_TIPAMM<>'C' AND NOT EMPTY(.w_ESPRIU) AND .w_NSOAMF<>'S' AND .w_AMMIMM<>'S')
    endwith
  endfunc

  func oAMPERANT_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_AMPERANT>=0 AND .w_AMPERANT<=.w_MAXPEANT) OR .w_AMPROESE=0)
    endwith
    return bRes
  endfunc

  add object oAMQUOFIS_2_11 as StdTrsField with uid="CTSZQTPCSW",rtseq=24,rtrep=.t.,;
    cFormVar="w_AMQUOFIS",value=0,;
    HelpContextID = 58727513,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=100, Left=328, Top=0, cSayPict=[v_PV(38+VVP)], cGetPict=[v_GV(38+VVP)]

  func oAMQUOFIS_2_11.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_ESPRIU) AND .w_NSOAMF<>'S' AND .w_AMMIMM<>'S' AND .w_TIPAMM<>'C')
    endwith
  endfunc

  add object oAMQUOFI1_2_12 as StdTrsField with uid="EEUXYZXCHU",rtseq=25,rtrep=.t.,;
    cFormVar="w_AMQUOFI1",value=0,;
    HelpContextID = 58727479,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=100, Left=431, Top=0, cSayPict=[v_PV(38+VVP)], cGetPict=[v_GV(38+VVP)]

  func oAMQUOFI1_2_12.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_ESPRIU) AND .w_NSOAMF<>'S' AND .w_AMMIMM<>'S' AND .w_TIPAMM<>'C')
    endwith
  endfunc

  add object oObj_2_13 as cp_runprogram with uid="ZNESPFWTXH",width=143,height=24,;
   left=131, top=149,;
    caption='GSCE_BMA(CE)',;
   bGlobalFont=.t.,;
    prg="GSCE_BMA('CE')",;
    cEvent = "Controlli",;
    nPag=2;
    , HelpContextID = 10278951
  add object oLast as LastKeyMover
  * ---
  func oAMPROESE_2_1.When()
    return(.t.)
  proc oAMPROESE_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oAMPROESE_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=4
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsce_mam','AMM_CESP','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".AMCODCES=AMM_CESP.AMCODCES";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
