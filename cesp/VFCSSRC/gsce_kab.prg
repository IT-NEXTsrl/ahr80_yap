* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_kab                                                        *
*              Abbinamento con primanota                                       *
*                                                                              *
*      Author: TAM Software Srl                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-04-20                                                      *
* Last revis.: 2012-12-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsce_kab",oParentObject))

* --- Class definition
define class tgsce_kab as StdForm
  Top    = 15
  Left   = 57

  * --- Standard Properties
  Width  = 629
  Height = 256
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-12-14"
  HelpContextID=132793705
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=16

  * --- Constant Properties
  _IDX = 0
  ESERCIZI_IDX = 0
  cPrg = "gsce_kab"
  cComment = "Abbinamento con primanota"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CODAZI = space(5)
  w_MCCODCON = space(15)
  w_MCRIFCON = space(10)
  w_MCNUMDOC = 0
  w_MCALFDOC = space(10)
  w_MCDATDOC = ctod('  /  /  ')
  w_MCCODESE = space(4)
  w_data1 = ctod('  /  /  ')
  w_data2 = ctod('  /  /  ')
  w_ESE = space(4)
  w_SENZRIFE = space(1)
  w_PNSERL = space(10)
  w_PNNUMD = 0
  w_PNDATA = ctod('  /  /  ')
  w_PNALFD = space(2)
  w_PNCLFO = space(15)
  w_ZOOM = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsce_kabPag1","gsce_kab",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.odata1_1_8
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZOOM = this.oPgFrm.Pages(1).oPag.ZOOM
    DoDefault()
    proc Destroy()
      this.w_ZOOM = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='ESERCIZI'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI=space(5)
      .w_MCCODCON=space(15)
      .w_MCRIFCON=space(10)
      .w_MCNUMDOC=0
      .w_MCALFDOC=space(10)
      .w_MCDATDOC=ctod("  /  /  ")
      .w_MCCODESE=space(4)
      .w_data1=ctod("  /  /  ")
      .w_data2=ctod("  /  /  ")
      .w_ESE=space(4)
      .w_SENZRIFE=space(1)
      .w_PNSERL=space(10)
      .w_PNNUMD=0
      .w_PNDATA=ctod("  /  /  ")
      .w_PNALFD=space(2)
      .w_PNCLFO=space(15)
      .w_MCCODCON=oParentObject.w_MCCODCON
      .w_MCRIFCON=oParentObject.w_MCRIFCON
      .w_MCNUMDOC=oParentObject.w_MCNUMDOC
      .w_MCALFDOC=oParentObject.w_MCALFDOC
      .w_MCDATDOC=oParentObject.w_MCDATDOC
      .w_MCCODESE=oParentObject.w_MCCODESE
        .w_CODAZI = i_codazi
          .DoRTCalc(2,9,.f.)
        .w_ESE = .w_MCCODESE
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_ESE))
          .link_1_10('Full')
        endif
        .w_SENZRIFE = 'N'
      .oPgFrm.Page1.oPag.ZOOM.Calculate()
        .w_PNSERL = Nvl(.w_Zoom.getVar('PNSERIAL'),Space(10))
        .w_PNNUMD = Nvl(.w_Zoom.getVar('PNNUMDOC'),0 )
        .w_PNDATA = Nvl(.w_Zoom.getVar('PNDATDOC'),cp_CharToDate('  /  /    '))
        .w_PNALFD = Nvl(.w_Zoom.getVar('PNALFDOC'),Space(2))
        .w_PNCLFO = Nvl(.w_Zoom.getVar('PNCODCLF'),Space(15))
      .oPgFrm.Page1.oPag.oObj_1_23.Calculate()
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_MCCODCON=.w_MCCODCON
      .oParentObject.w_MCRIFCON=.w_MCRIFCON
      .oParentObject.w_MCNUMDOC=.w_MCNUMDOC
      .oParentObject.w_MCALFDOC=.w_MCALFDOC
      .oParentObject.w_MCDATDOC=.w_MCDATDOC
      .oParentObject.w_MCCODESE=.w_MCCODESE
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.ZOOM.Calculate()
        .DoRTCalc(1,11,.t.)
            .w_PNSERL = Nvl(.w_Zoom.getVar('PNSERIAL'),Space(10))
            .w_PNNUMD = Nvl(.w_Zoom.getVar('PNNUMDOC'),0 )
            .w_PNDATA = Nvl(.w_Zoom.getVar('PNDATDOC'),cp_CharToDate('  /  /    '))
            .w_PNALFD = Nvl(.w_Zoom.getVar('PNALFDOC'),Space(2))
            .w_PNCLFO = Nvl(.w_Zoom.getVar('PNCODCLF'),Space(15))
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZOOM.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZOOM.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_23.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=ESE
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_ESE)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_CODAZI;
                     ,'ESCODESE',trim(this.w_ESE))
          select ESCODAZI,ESCODESE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ESE)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ESE) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oESE_1_10'),i_cWhere,'',"Esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_ESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_ESE)
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ESE = NVL(_Link_.ESCODESE,space(4))
    else
      if i_cCtrl<>'Load'
        this.w_ESE = space(4)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.odata1_1_8.value==this.w_data1)
      this.oPgFrm.Page1.oPag.odata1_1_8.value=this.w_data1
    endif
    if not(this.oPgFrm.Page1.oPag.odata2_1_9.value==this.w_data2)
      this.oPgFrm.Page1.oPag.odata2_1_9.value=this.w_data2
    endif
    if not(this.oPgFrm.Page1.oPag.oESE_1_10.value==this.w_ESE)
      this.oPgFrm.Page1.oPag.oESE_1_10.value=this.w_ESE
    endif
    if not(this.oPgFrm.Page1.oPag.oSENZRIFE_1_11.RadioValue()==this.w_SENZRIFE)
      this.oPgFrm.Page1.oPag.oSENZRIFE_1_11.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((.w_data2>=.w_data1 OR EMPTY(.w_data2)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.odata1_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data iniziale minore della data finale filtro")
          case   not((.w_data2>=.w_data1 OR EMPTY(.w_data2)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.odata2_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data iniziale minore della data finale filtro")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsce_kabPag1 as StdContainer
  Width  = 625
  height = 256
  stdWidth  = 625
  stdheight = 256
  resizeXpos=308
  resizeYpos=97
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object odata1_1_8 as StdField with uid="AHXSZMJFYQ",rtseq=8,rtrep=.f.,;
    cFormVar = "w_data1", cQueryName = "data1",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data iniziale minore della data finale filtro",;
    ToolTipText = "Data inizio filtro",;
    HelpContextID = 74554826,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=90, Top=7, cSayPict='"99/99/9999"', cGetPict='"99/99/9999"'

  func odata1_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_data2>=.w_data1 OR EMPTY(.w_data2)))
    endwith
    return bRes
  endfunc

  add object odata2_1_9 as StdField with uid="XALXKTAZJE",rtseq=9,rtrep=.f.,;
    cFormVar = "w_data2", cQueryName = "data2",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data iniziale minore della data finale filtro",;
    ToolTipText = "Data fine filtro",;
    HelpContextID = 73506250,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=258, Top=7, cSayPict='"99/99/9999"', cGetPict='"99/99/9999"'

  func odata2_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_data2>=.w_data1 OR EMPTY(.w_data2)))
    endwith
    return bRes
  endfunc

  add object oESE_1_10 as StdField with uid="YKECSNJHJJ",rtseq=10,rtrep=.f.,;
    cFormVar = "w_ESE", cQueryName = "ESE",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Filtro esercizio documenti",;
    HelpContextID = 132488634,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=428, Top=7, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_ESE"

  func oESE_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oESE_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oESE_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CODAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_CODAZI)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oESE_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Esercizi",'',this.parent.oContained
  endproc


  add object oSENZRIFE_1_11 as StdCombo with uid="GHPHHISZMW",rtseq=11,rtrep=.f.,left=490,top=6,width=125,height=21;
    , ToolTipText = "Scelta documenti da visualizzare";
    , HelpContextID = 110421355;
    , cFormVar="w_SENZRIFE",RowSource=""+"Senza riferimento,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSENZRIFE_1_11.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'T',;
    space(1))))
  endfunc
  func oSENZRIFE_1_11.GetRadio()
    this.Parent.oContained.w_SENZRIFE = this.RadioValue()
    return .t.
  endfunc

  func oSENZRIFE_1_11.SetRadio()
    this.Parent.oContained.w_SENZRIFE=trim(this.Parent.oContained.w_SENZRIFE)
    this.value = ;
      iif(this.Parent.oContained.w_SENZRIFE=='N',1,;
      iif(this.Parent.oContained.w_SENZRIFE=='T',2,;
      0))
  endfunc


  add object oBtn_1_12 as StdButton with uid="QFRVJEHILV",left=516, top=205, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per impostare l'abbinamento";
    , HelpContextID = 41847786;
    , caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_12.Click()
      with this.Parent.oContained
        GSCE_BGS(this.Parent.oContained,"A", .w_PNSERL)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_13 as StdButton with uid="IKBMOUVYHI",left=568, top=205, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 125476282;
    , caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_13.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object ZOOM as cp_zoombox with uid="NBRNJKHHPZ",left=1, top=30, width=622,height=170,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable='pnt_mast',cZoomFile='gsce_zpn',bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnDblClick=.f.,;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 45203942


  add object oObj_1_23 as cp_runprogram with uid="KUOZWQWSRW",left=2, top=307, width=459,height=19,;
    caption='GSCE_BGS(R)',;
   bGlobalFont=.t.,;
    prg='GSCE_BGS("R", "   ")',;
    cEvent = "w_data2 Changed,w_ESE Changed,w_SENZRIFE Changed,w_data1 Changed",;
    nPag=1;
    , HelpContextID = 5383993

  add object oStr_1_20 as StdString with uid="IIQLCOBLAD",Visible=.t., Left=14, Top=7,;
    Alignment=1, Width=70, Height=15,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="DKSOHNOPIK",Visible=.t., Left=190, Top=7,;
    Alignment=1, Width=62, Height=15,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="LUTMJUKGZP",Visible=.t., Left=353, Top=7,;
    Alignment=1, Width=68, Height=15,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsce_kab','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
