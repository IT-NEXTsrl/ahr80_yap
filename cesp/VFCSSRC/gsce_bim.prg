* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_bim                                                        *
*              Import archivi cespiti                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_44]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-09-27                                                      *
* Last revis.: 2012-04-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsce_bim",oParentObject)
return(i_retval)

define class tgsce_bim as StdBatch
  * --- Local variables
  w_CODAZI = space(5)
  w_TIPAMM = space(1)
  w_TIPCON = space(1)
  * --- WorkFile variables
  CAT_CESP_idx=0
  CES_PITI_idx=0
  SAL_CESP_idx=0
  ESERCIZI_idx=0
  CONTI_idx=0
  PAR_CESP_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lanciato dalla routine GSIM_BAC per l'importazione degli Archivi Cespiti
    * --- Modello di segnalazione
    * --- Modello di segnalazione
    * --- Contatore delle righe scritte
    * --- Tipo segnalazione
    * --- Dettaglio segnalazione
    * --- Numero errori riscontrati
    * --- Record corretto S/N
    * --- Sigla archivio destinazione
    * --- Codice utente
    * --- Data importazione
    * --- Esercizio di Consolidamento Saldi Cespiti
    * --- Contatore per Saldi Cespiti
    * --- Codice Azienda
    this.w_CODAZI = i_CODAZI
    if this.oParentObject.w_Destinaz = "CS"
      * --- Read from PAR_CESP
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PAR_CESP_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_CESP_idx,2],.t.,this.PAR_CESP_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "PCTIPAMM"+;
          " from "+i_cTable+" PAR_CESP where ";
              +"PCCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          PCTIPAMM;
          from (i_cTable) where;
              PCCODAZI = this.w_CODAZI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_TIPAMM = NVL(cp_ToDate(_read_.PCTIPAMM),cp_NullValue(_read_.PCTIPAMM))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_TIPAMM="C" and this.oParentObject.w_CECOUNT=1 and w_EXTIPSRC="W3"
        ah_ErrorMsg("Attenzione, ad hoc Windows non gestisce l'ammortamento civile")
      endif
    endif
    * --- Tipo conto (valorizzata a G=Generico) per controllo Conto Manutenzione
    * --- Aggiornamento archivi
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiornamento database
    do case
      case this.oParentObject.w_Destinaz $ this.oParentObject.oParentObject.w_Gestiti
        * --- Inserimento nuovi records
        * --- Try
        local bErr_038F6360
        bErr_038F6360=bTrsErr
        this.Try_038F6360()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_038F6360
        * --- End
        * --- Aggiornamento records
        i_rows = 0
        * --- Try
        local bErr_038806E8
        bErr_038806E8=bTrsErr
        this.Try_038806E8()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          this.oParentObject.w_Corretto = .F.
        endif
        bTrsErr=bTrsErr or bErr_038806E8
        * --- End
        * --- Se fallisce la insert (trigger failed) la procedura prosegue e poi le write non vengono fatte perch� il serial non esiste
        if i_rows=0
          this.oParentObject.w_Corretto = .F.
        endif
        if this.oParentObject.w_Corretto .and. this.oParentObject.w_ResoDett <> ah_Msgformat("<SENZA RESOCONTO>")
          * --- Scrittura resoconto
          this.oParentObject.w_ResoMode = "SCRITTURA"
          * --- Aggiornamento archivio resoconti
          this.oParentObject.Pag4()
        endif
      case .T.
        * --- Scrittura resoconto per archivi non supportati
        this.oParentObject.w_ResoMode = "NONSUPP"
        * --- Aggiornamento archivio resoconti
        this.oParentObject.Pag4()
    endcase
  endproc
  proc Try_038F6360()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    do case
      case this.oParentObject.w_Destinaz = "CR"
        * --- Segnalazione di errore se il conto manutenzione della categoria non � presente in Conti
        i_rows = 0
        this.w_TIPCON = "G"
        * --- Read from CONTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "*"+;
            " from "+i_cTable+" CONTI where ";
                +"ANTIPCON = "+cp_ToStrODBC(this.w_TIPCON);
                +" and ANCODICE = "+cp_ToStrODBC(w_CCCONMAN);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            *;
            from (i_cTable) where;
                ANTIPCON = this.w_TIPCON;
                and ANCODICE = w_CCCONMAN;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if i_rows=0 AND NOT EMPTY(w_CCCONMAN)
          * --- Se l'esercizio di primo utilizzo � vuoto, EMPTY(w_CEESPRIU), il cespite deve essere importato con il valore vuoto.
          this.oParentObject.w_ResoMode = "NOCMANUT"
          this.oParentObject.w_Corretto = .F.
          this.oParentObject.Pag4()
        else
          * --- Categorie Cespiti
          w_CCCODICE = left( ltrim(w_CCCODICE), 15)
          * --- Insert into CAT_CESP
          i_nConn=i_TableProp[this.CAT_CESP_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAT_CESP_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CAT_CESP_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"CCCODICE"+",UTCC"+",UTDC"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(w_CCCODICE),'CAT_CESP','CCCODICE');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CreVarUte),'CAT_CESP','UTCC');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CreVarDat),'CAT_CESP','UTDC');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'CCCODICE',w_CCCODICE,'UTCC',this.oParentObject.w_CreVarUte,'UTDC',this.oParentObject.w_CreVarDat)
            insert into (i_cTable) (CCCODICE,UTCC,UTDC &i_ccchkf. );
               values (;
                 w_CCCODICE;
                 ,this.oParentObject.w_CreVarUte;
                 ,this.oParentObject.w_CreVarDat;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        endif
      case this.oParentObject.w_Destinaz = "CT"
        * --- Segnalazione di errore se l'esercizio di primo utilizzo del cespite non � presente nella tabella degli esercizi
        i_rows = 0
        * --- Read from ESERCIZI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ESERCIZI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2],.t.,this.ESERCIZI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "*"+;
            " from "+i_cTable+" ESERCIZI where ";
                +"ESCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
                +" and ESCODESE = "+cp_ToStrODBC(w_CEESPRIU);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            *;
            from (i_cTable) where;
                ESCODAZI = this.w_CODAZI;
                and ESCODESE = w_CEESPRIU;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if i_rows=0 AND NOT EMPTY(w_CEESPRIU)
          * --- Se l'esercizio di primo utilizzo � vuoto, EMPTY(w_CEESPRIU), il cespite deve essere importato con il valore vuoto.
          this.oParentObject.w_ResoMode = "NOESEANA"
          this.oParentObject.w_Corretto = .F.
          this.oParentObject.Pag4()
        else
          * --- Anagrafica Cespiti
          w_CECODICE = left( ltrim(w_CECODICE), 20)
          * --- Insert into CES_PITI
          i_nConn=i_TableProp[this.CES_PITI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CES_PITI_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CES_PITI_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"CECODICE"+",UTCC"+",UTDC"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(w_CECODICE),'CES_PITI','CECODICE');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CreVarUte),'CES_PITI','UTCC');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CreVarDat),'CES_PITI','UTDC');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'CECODICE',w_CECODICE,'UTCC',this.oParentObject.w_CreVarUte,'UTDC',this.oParentObject.w_CreVarDat)
            insert into (i_cTable) (CECODICE,UTCC,UTDC &i_ccchkf. );
               values (;
                 w_CECODICE;
                 ,this.oParentObject.w_CreVarUte;
                 ,this.oParentObject.w_CreVarDat;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        endif
      case this.oParentObject.w_Destinaz = "CS"
        * --- Segnalazione di errore se l'esercizio deI saldi cespiti non � presente nella tabella degli esercizi
        i_rows = 0
        * --- Read from ESERCIZI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ESERCIZI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2],.t.,this.ESERCIZI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "*"+;
            " from "+i_cTable+" ESERCIZI where ";
                +"ESCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
                +" and ESCODESE = "+cp_ToStrODBC(w_SCCODESE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            *;
            from (i_cTable) where;
                ESCODAZI = this.w_CODAZI;
                and ESCODESE = w_SCCODESE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if i_rows=0
          this.oParentObject.w_ResoMode = "NOESESAL"
          this.oParentObject.w_Corretto = .F.
          this.oParentObject.Pag4()
        else
          * --- Saldi Cespiti
          w_SCCODCES = left( ltrim(w_SCCODCES), 20)
          w_SCCODESE = left( ltrim(w_SCCODESE), 4)
          if VAL(w_SCCODESE)>VAL(this.oParentObject.w_DATCON)
            this.oParentObject.w_DATCON = w_SCCODESE
          endif
          * --- Insert into SAL_CESP
          i_nConn=i_TableProp[this.SAL_CESP_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SAL_CESP_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SAL_CESP_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"SCCODCES"+",SCCODESE"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(w_SCCODCES),'SAL_CESP','SCCODCES');
            +","+cp_NullLink(cp_ToStrODBC(w_SCCODESE),'SAL_CESP','SCCODESE');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'SCCODCES',w_SCCODCES,'SCCODESE',w_SCCODESE)
            insert into (i_cTable) (SCCODCES,SCCODESE &i_ccchkf. );
               values (;
                 w_SCCODCES;
                 ,w_SCCODESE;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        endif
    endcase
    return
  proc Try_038806E8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    do case
      case this.oParentObject.w_Destinaz="CR"
        * --- Categorie Cespiti
        this.oParentObject.w_ResoDett = trim(w_CCCODICE)
        * --- Write into CAT_CESP
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.CAT_CESP_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAT_CESP_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CAT_CESP_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CCDESCRI ="+cp_NullLink(cp_ToStrODBC(w_CCDESCRI),'CAT_CESP','CCDESCRI');
          +",CCTIPBEN ="+cp_NullLink(cp_ToStrODBC(w_CCTIPBEN),'CAT_CESP','CCTIPBEN');
          +",CCDURCES ="+cp_NullLink(cp_ToStrODBC(w_CCDURCES),'CAT_CESP','CCDURCES');
          +",CCPERSPE ="+cp_NullLink(cp_ToStrODBC(w_CCPERSPE),'CAT_CESP','CCPERSPE');
          +",CCCONMAN ="+cp_NullLink(cp_ToStrODBC(w_CCCONMAN),'CAT_CESP','CCCONMAN');
          +",CCCOECIV ="+cp_NullLink(cp_ToStrODBC(w_CCCOECIV),'CAT_CESP','CCCOECIV');
          +",CCFLAMCI ="+cp_NullLink(cp_ToStrODBC(w_CCFLAMCI),'CAT_CESP','CCFLAMCI');
          +",CCPERCIV ="+cp_NullLink(cp_ToStrODBC(w_CCPERCIV),'CAT_CESP','CCPERCIV');
          +",CCFLANSI ="+cp_NullLink(cp_ToStrODBC(w_CCFLANSI),'CAT_CESP','CCFLANSI');
          +",CCGRUCON ="+cp_NullLink(cp_ToStrODBC(w_CCGRUCON),'CAT_CESP','CCGRUCON');
          +",CCCOEFIS ="+cp_NullLink(cp_ToStrODBC(w_CCCOEFIS),'CAT_CESP','CCCOEFIS');
          +",CCPERDEF ="+cp_NullLink(cp_ToStrODBC(w_CCPERDEF),'CAT_CESP','CCPERDEF');
          +",CCIMPMAX ="+cp_NullLink(cp_ToStrODBC(w_CCIMPMAX),'CAT_CESP','CCIMPMAX');
          +",CCDTOBSO ="+cp_NullLink(cp_ToStrODBC(w_CCDTOBSO),'CAT_CESP','CCDTOBSO');
          +",CCDTINVA ="+cp_NullLink(cp_ToStrODBC(w_CCDTINVA),'CAT_CESP','CCDTINVA');
          +",UTCV ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CreVarUte),'CAT_CESP','UTCV');
          +",UTDV ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CreVarDat),'CAT_CESP','UTDV');
          +",CCVALLIM ="+cp_NullLink(cp_ToStrODBC(w_CCVALLIM),'CAT_CESP','CCVALLIM');
              +i_ccchkf ;
          +" where ";
              +"CCCODICE = "+cp_ToStrODBC(w_CCCODICE);
                 )
        else
          update (i_cTable) set;
              CCDESCRI = w_CCDESCRI;
              ,CCTIPBEN = w_CCTIPBEN;
              ,CCDURCES = w_CCDURCES;
              ,CCPERSPE = w_CCPERSPE;
              ,CCCONMAN = w_CCCONMAN;
              ,CCCOECIV = w_CCCOECIV;
              ,CCFLAMCI = w_CCFLAMCI;
              ,CCPERCIV = w_CCPERCIV;
              ,CCFLANSI = w_CCFLANSI;
              ,CCGRUCON = w_CCGRUCON;
              ,CCCOEFIS = w_CCCOEFIS;
              ,CCPERDEF = w_CCPERDEF;
              ,CCIMPMAX = w_CCIMPMAX;
              ,CCDTOBSO = w_CCDTOBSO;
              ,CCDTINVA = w_CCDTINVA;
              ,UTCV = this.oParentObject.w_CreVarUte;
              ,UTDV = this.oParentObject.w_CreVarDat;
              ,CCVALLIM = w_CCVALLIM;
              &i_ccchkf. ;
           where;
              CCCODICE = w_CCCODICE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz="CT"
        * --- Anagrafica Cespiti
        this.oParentObject.w_ResoDett = trim(w_CECODICE)
        do case
          case this.w_TIPAMM="F"
            w_CEESPRIC = SPACE(4)
            w_CEDTPRIC = cp_CharToDate("  -  -    ")
          case this.w_TIPAMM="C"
            w_CEESPRIU = SPACE(4)
            w_CEDTPRIU = cp_CharToDate("  -  -    ")
        endcase
        * --- Write into CES_PITI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.CES_PITI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CES_PITI_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CES_PITI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CEDESCRI ="+cp_NullLink(cp_ToStrODBC(w_CEDESCRI),'CES_PITI','CEDESCRI');
          +",CEDESSUP ="+cp_NullLink(cp_ToStrODBC(w_CEDESSUP),'CES_PITI','CEDESSUP');
          +",CECODCAT ="+cp_NullLink(cp_ToStrODBC(w_CECODCAT),'CES_PITI','CECODCAT');
          +",CETIPCES ="+cp_NullLink(cp_ToStrODBC(w_CETIPCES),'CES_PITI','CETIPCES');
          +",CE_STATO ="+cp_NullLink(cp_ToStrODBC(w_CE_STATO),'CES_PITI','CE_STATO');
          +",CEESPRIU ="+cp_NullLink(cp_ToStrODBC(w_CEESPRIU),'CES_PITI','CEESPRIU');
          +",CESTABEN ="+cp_NullLink(cp_ToStrODBC(w_CESTABEN),'CES_PITI','CESTABEN');
          +",CEDTPRIU ="+cp_NullLink(cp_ToStrODBC(w_CEDTPRIU),'CES_PITI','CEDTPRIU');
          +",CEFLSPEM ="+cp_NullLink(cp_ToStrODBC(w_CEFLSPEM),'CES_PITI','CEFLSPEM');
          +",CEDATDIS ="+cp_NullLink(cp_ToStrODBC(w_CEDATDIS),'CES_PITI','CEDATDIS');
          +",CEFLAUMU ="+cp_NullLink(cp_ToStrODBC(w_CEFLAUMU),'CES_PITI','CEFLAUMU');
          +",CEFLCEUS ="+cp_NullLink(cp_ToStrODBC(w_CEFLCEUS),'CES_PITI','CEFLCEUS');
          +",CECODESE ="+cp_NullLink(cp_ToStrODBC(w_CECODESE),'CES_PITI','CECODESE');
          +",CETIPAMM ="+cp_NullLink(cp_ToStrODBC(w_CETIPAMM),'CES_PITI','CETIPAMM');
          +",CECOECIV ="+cp_NullLink(cp_ToStrODBC(w_CECOECIV),'CES_PITI','CECOECIV');
          +",CEPERDEF ="+cp_NullLink(cp_ToStrODBC(w_CEPERDEF),'CES_PITI','CEPERDEF');
          +",CEIMPMAX ="+cp_NullLink(cp_ToStrODBC(w_CEIMPMAX),'CES_PITI','CEIMPMAX');
          +",CECODUBI ="+cp_NullLink(cp_ToStrODBC(w_CECODUBI),'CES_PITI','CECODUBI');
          +",CECODMAT ="+cp_NullLink(cp_ToStrODBC(w_CECODMAT),'CES_PITI','CECODMAT');
          +",CECODFAM ="+cp_NullLink(cp_ToStrODBC(w_CECODFAM),'CES_PITI','CECODFAM');
          +",CECODPER ="+cp_NullLink(cp_ToStrODBC(w_CECODPER),'CES_PITI','CECODPER');
          +",CECODCOM ="+cp_NullLink(cp_ToStrODBC(w_CECODCOM),'CES_PITI','CECODCOM');
          +",CEQUOCIV ="+cp_NullLink(cp_ToStrODBC(w_CEQUOCIV),'CES_PITI','CEQUOCIV');
          +",CECOEFI1 ="+cp_NullLink(cp_ToStrODBC(w_CECOEFI1),'CES_PITI','CECOEFI1');
          +",CECOEFI2 ="+cp_NullLink(cp_ToStrODBC(w_CECOEFI2),'CES_PITI','CECOEFI2');
          +",CEQUOFIS ="+cp_NullLink(cp_ToStrODBC(w_CEQUOFIS),'CES_PITI','CEQUOFIS');
          +",CEVOCCEN ="+cp_NullLink(cp_ToStrODBC(w_CEVOCCEN),'CES_PITI','CEVOCCEN');
          +",CECODCEN ="+cp_NullLink(cp_ToStrODBC(w_CECODCEN),'CES_PITI','CECODCEN');
          +",CEDTOBSO ="+cp_NullLink(cp_ToStrODBC(w_CEDTOBSO),'CES_PITI','CEDTOBSO');
          +",CEDTINVA ="+cp_NullLink(cp_ToStrODBC(w_CEDTINVA),'CES_PITI','CEDTINVA');
          +",UTCV ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CreVarUte),'CES_PITI','UTCV');
          +",UTDV ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CreVarDat),'CES_PITI','UTDV');
          +",CEQUOFI1 ="+cp_NullLink(cp_ToStrODBC(w_CEQUOFI1),'CES_PITI','CEQUOFI1');
          +",CEESPRIC ="+cp_NullLink(cp_ToStrODBC(w_CEESPRIC),'CES_PITI','CEESPRIC');
          +",CEDTPRIC ="+cp_NullLink(cp_ToStrODBC(w_CEDTPRIC),'CES_PITI','CEDTPRIC');
              +i_ccchkf ;
          +" where ";
              +"CECODICE = "+cp_ToStrODBC(w_CECODICE);
                 )
        else
          update (i_cTable) set;
              CEDESCRI = w_CEDESCRI;
              ,CEDESSUP = w_CEDESSUP;
              ,CECODCAT = w_CECODCAT;
              ,CETIPCES = w_CETIPCES;
              ,CE_STATO = w_CE_STATO;
              ,CEESPRIU = w_CEESPRIU;
              ,CESTABEN = w_CESTABEN;
              ,CEDTPRIU = w_CEDTPRIU;
              ,CEFLSPEM = w_CEFLSPEM;
              ,CEDATDIS = w_CEDATDIS;
              ,CEFLAUMU = w_CEFLAUMU;
              ,CEFLCEUS = w_CEFLCEUS;
              ,CECODESE = w_CECODESE;
              ,CETIPAMM = w_CETIPAMM;
              ,CECOECIV = w_CECOECIV;
              ,CEPERDEF = w_CEPERDEF;
              ,CEIMPMAX = w_CEIMPMAX;
              ,CECODUBI = w_CECODUBI;
              ,CECODMAT = w_CECODMAT;
              ,CECODFAM = w_CECODFAM;
              ,CECODPER = w_CECODPER;
              ,CECODCOM = w_CECODCOM;
              ,CEQUOCIV = w_CEQUOCIV;
              ,CECOEFI1 = w_CECOEFI1;
              ,CECOEFI2 = w_CECOEFI2;
              ,CEQUOFIS = w_CEQUOFIS;
              ,CEVOCCEN = w_CEVOCCEN;
              ,CECODCEN = w_CECODCEN;
              ,CEDTOBSO = w_CEDTOBSO;
              ,CEDTINVA = w_CEDTINVA;
              ,UTCV = this.oParentObject.w_CreVarUte;
              ,UTDV = this.oParentObject.w_CreVarDat;
              ,CEQUOFI1 = w_CEQUOFI1;
              ,CEESPRIC = w_CEESPRIC;
              ,CEDTPRIC = w_CEDTPRIC;
              &i_ccchkf. ;
           where;
              CECODICE = w_CECODICE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz="CS"
        * --- Saldi Cespiti
        this.oParentObject.w_ResoDett = trim(w_SCCODCES+w_SCCODESE)
        do case
          case this.w_TIPAMM = "F"
            w_SCACCCIV = 0
            w_SCUTICIV = 0
            w_SCINCVAC =0
            w_SCIMPONC =0
            w_SCIMPRIC=0
            w_SCDECVAC =0
            w_SCIMPSVC =0
            w_SCPLUSVC =0
            w_SCMINSVC =0
          case this.w_TIPAMM = "E" AND w_EXTIPSRC="W3"
            w_SCACCCIV = w_SCACCFIS + w_SCACCANT
            w_SCUTICIV = w_SCUTIFIS+w_SCUTIANT
          case this.w_TIPAMM = "C"
            if w_EXTIPSRC="W3"
              w_SCACCCIV = w_SCACCFIS + w_SCACCANT
              w_SCUTICIV = w_SCUTIFIS+w_SCUTIANT
            endif
            w_SCACCFIS = 0
            w_SCUTIFIS = 0
            w_SCACCANT = 0
            w_SCUTIANT = 0
            w_SCINCVAL=0
            w_SCIMPONS =0
            w_SCIMPRIV=0
            w_SCDECVAL =0
            w_SCIMPSVA =0
            w_SCPLUSVA =0
            w_SCMINSVA=0
        endcase
        * --- Write into SAL_CESP
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.SAL_CESP_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SAL_CESP_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.SAL_CESP_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SCINCVAL ="+cp_NullLink(cp_ToStrODBC(w_SCINCVAL),'SAL_CESP','SCINCVAL');
          +",SCIMPONS ="+cp_NullLink(cp_ToStrODBC(w_SCIMPONS),'SAL_CESP','SCIMPONS');
          +",SCIMPRIV ="+cp_NullLink(cp_ToStrODBC(w_SCIMPRIV),'SAL_CESP','SCIMPRIV');
          +",SCDECVAL ="+cp_NullLink(cp_ToStrODBC(w_SCDECVAL),'SAL_CESP','SCDECVAL');
          +",SCIMPSVA ="+cp_NullLink(cp_ToStrODBC(w_SCIMPSVA),'SAL_CESP','SCIMPSVA');
          +",SCACCCIV ="+cp_NullLink(cp_ToStrODBC(w_SCACCCIV),'SAL_CESP','SCACCCIV');
          +",SCIMPNOA ="+cp_NullLink(cp_ToStrODBC(w_SCIMPNOA),'SAL_CESP','SCIMPNOA');
          +",SCQUOPER ="+cp_NullLink(cp_ToStrODBC(w_SCQUOPER),'SAL_CESP','SCQUOPER');
          +",SCACCFIS ="+cp_NullLink(cp_ToStrODBC(w_SCACCFIS),'SAL_CESP','SCACCFIS');
          +",SCUTIFIS ="+cp_NullLink(cp_ToStrODBC(w_SCUTIFIS),'SAL_CESP','SCUTIFIS');
          +",SCACCANT ="+cp_NullLink(cp_ToStrODBC(w_SCACCANT),'SAL_CESP','SCACCANT');
          +",SCUTIANT ="+cp_NullLink(cp_ToStrODBC(w_SCUTIANT),'SAL_CESP','SCUTIANT');
          +",SCPLUSVA ="+cp_NullLink(cp_ToStrODBC(w_SCPLUSVA),'SAL_CESP','SCPLUSVA');
          +",SCMINSVA ="+cp_NullLink(cp_ToStrODBC(w_SCMINSVA),'SAL_CESP','SCMINSVA');
          +",SCUTICIV ="+cp_NullLink(cp_ToStrODBC(w_SCUTICIV),'SAL_CESP','SCUTICIV');
          +",SCCOSBEN ="+cp_NullLink(cp_ToStrODBC(w_SCCOSBEN),'SAL_CESP','SCCOSBEN');
          +",SCDURBEN ="+cp_NullLink(cp_ToStrODBC(w_SCDURBEN),'SAL_CESP','SCDURBEN');
          +",SCQUOESE ="+cp_NullLink(cp_ToStrODBC(w_SCQUOESE),'SAL_CESP','SCQUOESE');
          +",SCSPEPRE ="+cp_NullLink(cp_ToStrODBC(w_SCSPEPRE),'SAL_CESP','SCSPEPRE');
          +",SCPERVAR ="+cp_NullLink(cp_ToStrODBC(w_SCPERVAR),'SAL_CESP','SCPERVAR');
          +",SCSPECOR ="+cp_NullLink(cp_ToStrODBC(w_SCSPECOR),'SAL_CESP','SCSPECOR');
          +",SCUNIMIS ="+cp_NullLink(cp_ToStrODBC(w_SCUNIMIS),'SAL_CESP','SCUNIMIS');
          +",SCQTAPREV ="+cp_NullLink(cp_ToStrODBC(w_SCQTAPREV),'SAL_CESP','SCQTAPREV');
          +",SCCOSUNI ="+cp_NullLink(cp_ToStrODBC(w_SCCOSUNI),'SAL_CESP','SCCOSUNI');
          +",SCINCVAC ="+cp_NullLink(cp_ToStrODBC(w_SCINCVAC),'SAL_CESP','SCINCVAC');
          +",SCIMPONC ="+cp_NullLink(cp_ToStrODBC(w_SCIMPONC),'SAL_CESP','SCIMPONC');
          +",SCIMPRIC ="+cp_NullLink(cp_ToStrODBC(w_SCIMPRIC),'SAL_CESP','SCIMPRIC');
          +",SCDECVAC ="+cp_NullLink(cp_ToStrODBC(w_SCDECVAC),'SAL_CESP','SCDECVAC');
          +",SCIMPSVC ="+cp_NullLink(cp_ToStrODBC(w_SCIMPSVC),'SAL_CESP','SCIMPSVC');
          +",SCPLUSVC ="+cp_NullLink(cp_ToStrODBC(w_SCPLUSVC),'SAL_CESP','SCPLUSVC');
          +",SCMINSVC ="+cp_NullLink(cp_ToStrODBC(w_SCMINSVC),'SAL_CESP','SCMINSVC');
              +i_ccchkf ;
          +" where ";
              +"SCCODCES = "+cp_ToStrODBC(w_SCCODCES);
              +" and SCCODESE = "+cp_ToStrODBC(w_SCCODESE);
                 )
        else
          update (i_cTable) set;
              SCINCVAL = w_SCINCVAL;
              ,SCIMPONS = w_SCIMPONS;
              ,SCIMPRIV = w_SCIMPRIV;
              ,SCDECVAL = w_SCDECVAL;
              ,SCIMPSVA = w_SCIMPSVA;
              ,SCACCCIV = w_SCACCCIV;
              ,SCIMPNOA = w_SCIMPNOA;
              ,SCQUOPER = w_SCQUOPER;
              ,SCACCFIS = w_SCACCFIS;
              ,SCUTIFIS = w_SCUTIFIS;
              ,SCACCANT = w_SCACCANT;
              ,SCUTIANT = w_SCUTIANT;
              ,SCPLUSVA = w_SCPLUSVA;
              ,SCMINSVA = w_SCMINSVA;
              ,SCUTICIV = w_SCUTICIV;
              ,SCCOSBEN = w_SCCOSBEN;
              ,SCDURBEN = w_SCDURBEN;
              ,SCQUOESE = w_SCQUOESE;
              ,SCSPEPRE = w_SCSPEPRE;
              ,SCPERVAR = w_SCPERVAR;
              ,SCSPECOR = w_SCSPECOR;
              ,SCUNIMIS = w_SCUNIMIS;
              ,SCQTAPREV = w_SCQTAPREV;
              ,SCCOSUNI = w_SCCOSUNI;
              ,SCINCVAC = w_SCINCVAC;
              ,SCIMPONC = w_SCIMPONC;
              ,SCIMPRIC = w_SCIMPRIC;
              ,SCDECVAC = w_SCDECVAC;
              ,SCIMPSVC = w_SCIMPSVC;
              ,SCPLUSVC = w_SCPLUSVC;
              ,SCMINSVC = w_SCMINSVC;
              &i_ccchkf. ;
           where;
              SCCODCES = w_SCCODCES;
              and SCCODESE = w_SCCODESE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
    endcase
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,6)]
    this.cWorkTables[1]='CAT_CESP'
    this.cWorkTables[2]='CES_PITI'
    this.cWorkTables[3]='SAL_CESP'
    this.cWorkTables[4]='ESERCIZI'
    this.cWorkTables[5]='CONTI'
    this.cWorkTables[6]='PAR_CESP'
    return(this.OpenAllTables(6))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
