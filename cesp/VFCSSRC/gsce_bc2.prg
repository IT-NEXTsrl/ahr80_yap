* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsce_bc2                                                        *
*              Calcola da formule                                              *
*                                                                              *
*      Author: Zucchetti spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][207]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-03-02                                                      *
* Last revis.: 2000-12-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipo
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsce_bc2",oParentObject,m.pTipo)
return(i_retval)

define class tgsce_bc2 as StdBatch
  * --- Local variables
  pTipo = space(1)
  w_MESS = space(10)
  w_RIGA = 0
  w_oERRORLOG = .NULL.
  * --- WorkFile variables
  FOR_CESP_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcola valori Movimenti Cespiti dalle Formule (da GSCE_AMC)
    * --- Stanzia l'oggetto per la gestione delle messaggistiche di errore
    this.w_oERRORLOG=createobject("AH_ErrorLog")
    this.w_MESS = SPACE(10)
    this.w_RIGA = 0
    * --- Variabili utilzzate nelle Formule
    PRIVATE V01, V02, V03, V04, V05, V06, V07, V08, V09, V10, V11, V12,V13,V14,V15,V16,V17
    PRIVATE A01, A02, A03, A04, A05, A06, A07, A08, A09, A10, A11, A12, A13, A14,A15,A16,A17,A18,A19,A20,A21,A22,A23,A24
    PRIVATE RISULT, FORMUL, CONDIZ, OLDERR, NUMERR
    if this.pTipo="C"
      * --- Inizializza valori
      A01 = this.oParentObject.w_MCIMPA01
      A02 = this.oParentObject.w_MCIMPA02
      A03 = this.oParentObject.w_MCIMPA03
      A04 = this.oParentObject.w_MCIMPA04
      A05 = this.oParentObject.w_MCIMPA05
      A06 = this.oParentObject.w_MCIMPA06
      A07 = this.oParentObject.w_MCIMPA07
      A08 = this.oParentObject.w_MCIMPA08
      A09 = this.oParentObject.w_MCIMPA09
      A10 = this.oParentObject.w_MCIMPA10
      A11 = this.oParentObject.w_MCIMPA11
      A12 = this.oParentObject.w_MCIMPA12
      A13 = this.oParentObject.w_MCIMPA13
      A14 = this.oParentObject.w_MCIMPA14
      A15 = this.oParentObject.w_MCIMPA15
      A16 = this.oParentObject.w_MCIMPA16
      A17 = this.oParentObject.w_MCQTACES
      A18 = this.oParentObject.w_MCIMPA18
      A19 = this.oParentObject.w_MCIMPA19
      A20 = this.oParentObject.w_MCIMPA20
      A21 = this.oParentObject.w_MCIMPA21
      A22 = this.oParentObject.w_MCIMPA22
      A23 = this.oParentObject.w_MCIMPA23
      A24 = this.oParentObject.w_MCIMPA24
      V01 = this.oParentObject.w_IMPV01
      V02 = this.oParentObject.w_IMPV02
      V03 = this.oParentObject.w_IMPV03
      V04 = this.oParentObject.w_IMPV04
      V05 = this.oParentObject.w_IMPV05
      V06 = this.oParentObject.w_IMPV06
      V07 = this.oParentObject.w_IMPV07
      V08 = this.oParentObject.w_IMPV08
      V09 = this.oParentObject.w_IMPV09
      V10 = this.oParentObject.w_IMPV10
      V11 = this.oParentObject.w_IMPV11
      V12 = this.oParentObject.w_IMPV12
      V13 = this.oParentObject.w_IMPV13
      V14 = this.oParentObject.w_IMPV14
      V15 = this.oParentObject.w_IMQTACES
      V16 = this.oParentObject.w_IMPV16
      V17 = this.oParentObject.w_IMPV17
      NUMERR=0
      OLDERR = ON("ERROR")
      ON ERROR NUMERR=ERROR()
      * --- Legge Formule Cespiti
      * --- Select from FOR_CESP
      i_nConn=i_TableProp[this.FOR_CESP_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.FOR_CESP_idx,2],.t.,this.FOR_CESP_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" FOR_CESP ";
            +" where FOCODCAU="+cp_ToStrODBC(this.oParentObject.w_MCCODCAU)+"";
            +" order by CPROWORD";
             ,"_Curs_FOR_CESP")
      else
        select * from (i_cTable);
         where FOCODCAU=this.oParentObject.w_MCCODCAU;
         order by CPROWORD;
          into cursor _Curs_FOR_CESP
      endif
      if used('_Curs_FOR_CESP')
        select _Curs_FOR_CESP
        locate for 1=1
        do while not(eof())
        RISULT = ALLTRIM(NVL(_Curs_FOR_CESP.FORISULT, " "))
        FORMUL = ALLTRIM(NVL(_Curs_FOR_CESP.FOFORMUL, " "))
        CONDIZ = ALLTRIM(NVL(_Curs_FOR_CESP.FOCONDIZ, " "))
        this.w_RIGA = _Curs_FOR_CESP.CPROWORD
        if LEN(RISULT)=3 AND LEFT(RISULT, 1)="A" AND NOT EMPTY(FORMUL)
          NUMERR=0
          if NOT EMPTY(CONDIZ)
            if &CONDIZ
              &RISULT = &FORMUL
            endif
          else
            &RISULT = &FORMUL
          endif
          if NUMERR<>0
            do case
              case NUMERR=10
                this.w_MESS = "Verificare valore formula: %1 - riga: %2%0Espressione incongruente"
              case NUMERR=12
                this.w_MESS = "Verificare valore formula: %1 - riga: %2%0Variabile inesistente"
              otherwise
                this.w_MESS = "Verificare valore formula: %1 - riga: %2%"
            endcase
            * --- Aggiungo l'errore nella stampa log
            this.w_oERRORLOG.AddMsgLog(this.w_MESS, RISULT, STR(this.w_RIGA,3,0))     
          endif
          * --- Controllo congruenza valore sestituito. Nel caso la formula restituisca un 0/0
          *     non rientra negli errori ERROR() restituisce 0. Controllo quindi che in generale il 
          *     valore restituito dalla formula sia compreso tra il valore massimo e minimo di Revolution
          if Not (&RISULT>= i_ininum and &RISULT <= i_finnum)
            this.w_MESS = "Verificare valore formula: %1 - riga: %2%0La formula restituisce un valore incongruente (possibile divisione per 0)"
            this.w_oERRORLOG.AddMsgLog(this.w_MESS, RISULT, STR(this.w_RIGA,3,0))     
            &RISULT = 0
          endif
        endif
          select _Curs_FOR_CESP
          continue
        enddo
        use
      endif
      ON ERROR &OLDERR
      * --- Riassegno i Valori Calcolati
      this.oParentObject.w_MCIMPA01 = cp_ROUND(A01, this.oParentObject.w_DECTOT)
      this.oParentObject.w_MCIMPA02 = cp_ROUND(A02, this.oParentObject.w_DECTOT)
      this.oParentObject.w_MCIMPA03 = cp_ROUND(A03, this.oParentObject.w_DECTOT)
      this.oParentObject.w_MCIMPA04 = cp_ROUND(A04, this.oParentObject.w_DECTOT)
      this.oParentObject.w_MCIMPA05 = cp_ROUND(A05, this.oParentObject.w_DECTOT)
      this.oParentObject.w_MCIMPA06 = cp_ROUND(A06, this.oParentObject.w_DECTOT)
      this.oParentObject.w_MCIMPA07 = cp_ROUND(A07, this.oParentObject.w_DECTOT)
      this.oParentObject.w_MCIMPA08 = cp_ROUND(A08, this.oParentObject.w_DECTOT)
      this.oParentObject.w_MCIMPA09 = cp_ROUND(A09, this.oParentObject.w_DECTOT)
      this.oParentObject.w_MCIMPA10 = cp_ROUND(A10, this.oParentObject.w_DECTOT)
      this.oParentObject.w_MCIMPA11 = cp_ROUND(A11, this.oParentObject.w_DECTOT)
      this.oParentObject.w_MCIMPA12 = cp_ROUND(A12, this.oParentObject.w_DECTOT)
      this.oParentObject.w_MCIMPA13 = cp_ROUND(A13, this.oParentObject.w_DECTOT)
      this.oParentObject.w_MCIMPA14 = cp_ROUND(A14, this.oParentObject.w_DECTOT)
      this.oParentObject.w_MCIMPA15 = cp_ROUND(A15, this.oParentObject.w_DECTOT)
      this.oParentObject.w_MCIMPA16 = cp_ROUND(A16, this.oParentObject.w_DECTOT)
      this.oParentObject.w_MCQTACES = A17
      this.oParentObject.w_MCIMPA18 = cp_ROUND(A18, this.oParentObject.w_DECTOT)
      this.oParentObject.w_MCIMPA19 = cp_ROUND(A19, this.oParentObject.w_DECTOT)
      this.oParentObject.w_MCIMPA20 = cp_ROUND(A20, this.oParentObject.w_DECTOT)
      this.oParentObject.w_MCIMPA21 = cp_ROUND(A21, this.oParentObject.w_DECTOT)
      this.oParentObject.w_MCIMPA22 = cp_ROUND(A22, this.oParentObject.w_DECTOT)
      this.oParentObject.w_MCIMPA23 = cp_ROUND(A23, this.oParentObject.w_DECTOT)
      this.oParentObject.w_MCIMPA24 = cp_ROUND(A24, this.oParentObject.w_DECTOT)
    else
      * --- Azzero il valore dei campi da MCIMPA01 a MCIMPA22
      * --- Effettuo questa operazione per poter evitare il caso di trovarmi visualizzato
      * --- nello stesso movimento una plusvalenza ed una minusvalenza.
      this.oParentObject.w_MCIMPA02 = 0
      this.oParentObject.w_MCIMPA03 = 0
      this.oParentObject.w_MCIMPA04 = 0
      this.oParentObject.w_MCIMPA05 = 0
      this.oParentObject.w_MCIMPA06 = 0
      this.oParentObject.w_MCIMPA07 = 0
      this.oParentObject.w_MCIMPA08 = 0
      this.oParentObject.w_MCIMPA09 = 0
      this.oParentObject.w_MCIMPA10 = 0
      this.oParentObject.w_MCIMPA11 = 0
      this.oParentObject.w_MCIMPA12 = 0
      this.oParentObject.w_MCIMPA13 = 0
      this.oParentObject.w_MCIMPA14 = 0
      this.oParentObject.w_MCIMPA15 = 0
      this.oParentObject.w_MCIMPA16 = 0
      this.oParentObject.w_MCIMPA18 = 0
      this.oParentObject.w_MCIMPA19 = 0
      this.oParentObject.w_MCIMPA20 = 0
      this.oParentObject.w_MCIMPA21 = 0
      this.oParentObject.w_MCIMPA22 = 0
      this.oParentObject.w_MCIMPA23 = 0
      this.oParentObject.w_MCIMPA24 = 0
    endif
    if this.w_oERRORLOG.IsFullLog()
      this.w_oERRORLOG.PrintLog(this.oParentObject,"Errori riscontrati nella valutazione formule")     
    endif
  endproc


  proc Init(oParentObject,pTipo)
    this.pTipo=pTipo
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='FOR_CESP'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_FOR_CESP')
      use in _Curs_FOR_CESP
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipo"
endproc
