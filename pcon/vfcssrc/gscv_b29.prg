* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscv_b29                                                        *
*              2001.1.0 - aggiornamento chiave moviripa                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_239]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-07-12                                                      *
* Last revis.: 2001-07-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscv_b29",oParentObject,m.w_NHF)
return(i_retval)

define class tgscv_b29 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_MRCODCOM = space(15)
  w_FRASE = space(200)
  w_NCONN = 0
  * --- WorkFile variables
  MOVIRIPA_idx=0
  RIPATMP1_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- FIle di LOG
    * --- Try
    local bErr_0360CD10
    bErr_0360CD10=bTrsErr
    this.Try_0360CD10()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = this.oParentObject.w_PMSG
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_0360CD10
    * --- End
  endproc
  proc Try_0360CD10()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    i_nConn=i_TableProp[this.MOVIRIPA_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.MOVIRIPA_idx,2])
    if i_nConn<>0
      do case
        case upper(CP_DBTYPE)="SQLSERVER"
          * --- Caso SQLServer
          this.w_NCONN = i_TableProp[this.MOVIRIPA_idx,3]
           
 i_cTable=cp_SetAzi(i_TableProp[this.MOVIRIPA_idx,2])
          this.w_FRASE = "UPDATE "+i_cTable+" SET MRCODCOM='' WHERE MRCODCOM IS NULL"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_FRASE = "ALTER TABLE "+i_cTable+" ALTER COLUMN MRCODCOM char(15) NOT NULL"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_FRASE = "UPDATE xxxMOVIRIPA"+" SET MRCODCOM='' WHERE MRCODCOM IS NULL"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_FRASE = "ALTER TABLE xxxMOVIRIPA"+" ALTER COLUMN MRCODCOM char(15) NOT NULL"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_FRASE = "UPDATE "+i_cTable+" SET MR_SEGNO='' WHERE MR_SEGNO IS NULL"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_FRASE = "ALTER TABLE "+i_cTable+" ALTER COLUMN MR_SEGNO char(1) NOT NULL"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_FRASE = "UPDATE xxxMOVIRIPA"+" SET MR_SEGNO='' WHERE MR_SEGNO IS NULL"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_FRASE = "ALTER TABLE xxxMOVIRIPA"+" ALTER COLUMN MR_SEGNO char(1) NOT NULL"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_FRASE = "UPDATE "+i_cTable+" SET MRSTORNO=1 WHERE MRSTORNO IS NULL"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_FRASE = "ALTER TABLE "+i_cTable+" ALTER COLUMN MRSTORNO int NOT NULL"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_FRASE = "UPDATE xxxMOVIRIPA"+" SET MRSTORNO=1 WHERE MRSTORNO IS NULL"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_FRASE = "ALTER TABLE xxxMOVIRIPA"+" ALTER COLUMN MRSTORNO int NOT NULL"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        case upper(CP_DBTYPE)="ORACLE"
          * --- Caso ORACLE
          this.w_NCONN = i_TableProp[this.MOVIRIPA_idx,3]
           
 i_cTable=cp_SetAzi(i_TableProp[this.MOVIRIPA_idx,2])
          this.w_FRASE = "UPDATE "+i_cTable+" SET MRCODCOM='               ' WHERE MRCODCOM IS NULL"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_FRASE = "ALTER TABLE "+i_cTable+" MODIFY(MRCODCOM NOT NULL)"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_FRASE = "UPDATE xxxMOVIRIPA"+" SET MRCODCOM='               ' WHERE MRCODCOM IS NULL"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_FRASE = "ALTER TABLE xxxMOVIRIPA"+" MODIFY(MRCODCOM NOT NULL)"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_FRASE = "UPDATE "+i_cTable+" SET MR_SEGNO=' ' WHERE MR_SEGNO IS NULL"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_FRASE = "ALTER TABLE "+i_cTable+" MODIFY(MR_SEGNO NOT NULL)"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_FRASE = "UPDATE xxxMOVIRIPA"+" SET MR_SEGNO=' ' WHERE MR_SEGNO IS NULL"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_FRASE = "ALTER TABLE xxxMOVIRIPA"+" MODIFY(MR_SEGNO NOT NULL)"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_FRASE = "UPDATE "+i_cTable+" SET MRSTORNO=1 WHERE MRSTORNO IS NULL"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_FRASE = "ALTER TABLE "+i_cTable+" MODIFY(MRSTORNO NOT NULL)"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_FRASE = "UPDATE xxxMOVIRIPA"+" SET MRSTORNO=1 WHERE MRSTORNO IS NULL"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_FRASE = "ALTER TABLE xxxMOVIRIPA"+" MODIFY(MRSTORNO NOT NULL)"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        case upper(CP_DBTYPE)="DB2"
          * --- Caso DB2
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
      endcase
      if bTrsErr
        * --- Raise
        i_Error=MESSAGE()
        return
      else
        if this.w_NHF>=0
          if upper(CP_DBTYPE)="DB2"
            this.w_TMPC = ah_Msgformat("Elaborazione terminata con successo")
          else
            this.w_TMPC = ah_Msgformat("Adeguamento chiave primaria tabella MOVIRIPA eseguito correttamente%0ATTENZIONE: occorre andare in MANUTENZIONE DATABASE, selezionare la tabella%0MOVIRIPA e premere <OK>")
          endif
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    if upper(CP_DBTYPE)="DB2"
      this.oParentObject.w_PMSG = ah_Msgformat("Elaborazione terminata con successo")
    else
      this.w_TMPC = ah_Msgformat("Adeguamento chiave primaria tabella MOVIRIPA eseguito correttamente%0ATTENZIONE: occorre andare in MANUTENZIONE DATABASE, selezionare la tabella%0MOVIRIPA e premere <OK>")
      this.oParentObject.w_PMSG = this.w_TMPC
    endif
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella DB2
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.MOVIRIPA_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.MOVIRIPA_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
          )
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    * --- Elimino la tabella
    if Not GSCV_BDT( this, "MOVIRIPA" , i_nConn )
      * --- Raise
      i_Error="Error"
      return
    endif
    * --- Ricostruisco il database per la tabella
    GSCV_BRT(this, "MOVIRIPA" , i_nConn , .T.)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Select from RIPATMP1
    i_nConn=i_TableProp[this.RIPATMP1_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" RIPATMP1 ";
           ,"_Curs_RIPATMP1")
    else
      select * from (i_cTable);
        into cursor _Curs_RIPATMP1
    endif
    if used('_Curs_RIPATMP1')
      select _Curs_RIPATMP1
      locate for 1=1
      do while not(eof())
      this.w_MRCODCOM = NVL(_Curs_RIPATMP1.MRCODCOM,"")
      * --- Try
      local bErr_035EF7B0
      bErr_035EF7B0=bTrsErr
      this.Try_035EF7B0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_035EF7B0
      * --- End
        select _Curs_RIPATMP1
        continue
      enddo
      use
    endif
    ah_Msg("Ripristinati valori di origine",.T.)
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    ah_Msg("Eliminata tabella temporanea tmp",.T.)
  endproc
  proc Try_035EF7B0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into MOVIRIPA
    i_nConn=i_TableProp[this.MOVIRIPA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOVIRIPA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MOVIRIPA_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"MRPERIOD"+",MRTIPREG"+",MRFLGMOV"+",MRNUMRIG"+",MRCODESE"+",MRDATREG"+",MRCODVAL"+",MRCODVOC"+",MRCODICE"+",MRCODCOM"+",MR_SEGNO"+",MRTOTIMP"+",MRINICOM"+",MRFINCOM"+",MRFLRIPA"+",MRSERRIF"+",MRORDRIF"+",MRNUMRIF"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MRPERIOD),'MOVIRIPA','MRPERIOD');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MRTIPREG),'MOVIRIPA','MRTIPREG');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MRFLGMOV),'MOVIRIPA','MRFLGMOV');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MRNUMRIG),'MOVIRIPA','MRNUMRIG');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MRCODESE),'MOVIRIPA','MRCODESE');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MRDATREG),'MOVIRIPA','MRDATREG');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MRCODVAL),'MOVIRIPA','MRCODVAL');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MRCODVOC),'MOVIRIPA','MRCODVOC');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MRCODICE),'MOVIRIPA','MRCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MRCODCOM),'MOVIRIPA','MRCODCOM');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MR_SEGNO),'MOVIRIPA','MR_SEGNO');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MRTOTIMP),'MOVIRIPA','MRTOTIMP');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MRINICOM),'MOVIRIPA','MRINICOM');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MRFINCOM),'MOVIRIPA','MRFINCOM');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MRFLRIPA),'MOVIRIPA','MRFLRIPA');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MRSERRIF),'MOVIRIPA','MRSERRIF');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MRORDRIF),'MOVIRIPA','MRORDRIF');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MRNUMRIF),'MOVIRIPA','MRNUMRIF');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'MRPERIOD',_Curs_RIPATMP1.MRPERIOD,'MRTIPREG',_Curs_RIPATMP1.MRTIPREG,'MRFLGMOV',_Curs_RIPATMP1.MRFLGMOV,'MRNUMRIG',_Curs_RIPATMP1.MRNUMRIG,'MRCODESE',_Curs_RIPATMP1.MRCODESE,'MRDATREG',_Curs_RIPATMP1.MRDATREG,'MRCODVAL',_Curs_RIPATMP1.MRCODVAL,'MRCODVOC',_Curs_RIPATMP1.MRCODVOC,'MRCODICE',_Curs_RIPATMP1.MRCODICE,'MRCODCOM',this.w_MRCODCOM,'MR_SEGNO',_Curs_RIPATMP1.MR_SEGNO,'MRTOTIMP',_Curs_RIPATMP1.MRTOTIMP)
      insert into (i_cTable) (MRPERIOD,MRTIPREG,MRFLGMOV,MRNUMRIG,MRCODESE,MRDATREG,MRCODVAL,MRCODVOC,MRCODICE,MRCODCOM,MR_SEGNO,MRTOTIMP,MRINICOM,MRFINCOM,MRFLRIPA,MRSERRIF,MRORDRIF,MRNUMRIF &i_ccchkf. );
         values (;
           _Curs_RIPATMP1.MRPERIOD;
           ,_Curs_RIPATMP1.MRTIPREG;
           ,_Curs_RIPATMP1.MRFLGMOV;
           ,_Curs_RIPATMP1.MRNUMRIG;
           ,_Curs_RIPATMP1.MRCODESE;
           ,_Curs_RIPATMP1.MRDATREG;
           ,_Curs_RIPATMP1.MRCODVAL;
           ,_Curs_RIPATMP1.MRCODVOC;
           ,_Curs_RIPATMP1.MRCODICE;
           ,this.w_MRCODCOM;
           ,_Curs_RIPATMP1.MR_SEGNO;
           ,_Curs_RIPATMP1.MRTOTIMP;
           ,_Curs_RIPATMP1.MRINICOM;
           ,_Curs_RIPATMP1.MRFINCOM;
           ,_Curs_RIPATMP1.MRFLRIPA;
           ,_Curs_RIPATMP1.MRSERRIF;
           ,_Curs_RIPATMP1.MRORDRIF;
           ,_Curs_RIPATMP1.MRNUMRIF;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Errore nel trasferimento dati'
      return
    endif
    return


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if SQLExec(this.w_nConn,this.w_FRASE)=-1
      * --- Raise
      i_Error=ah_Msgformat("Errore %1 %2",this.w_FRASE,message())
      return
    endif
  endproc


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='MOVIRIPA'
    this.cWorkTables[2]='*RIPATMP1'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_RIPATMP1')
      use in _Curs_RIPATMP1
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
