* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvbb16                                                        *
*              Ripartisce sconti                                               *
*                                                                              *
*      Author: Nicola Gorlandi                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: TRCF][111][VRS_33]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-03-31                                                      *
* Last revis.: 2005-03-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvbb16",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvbb16 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_MESS = space(100)
  w_NUMAGG = 0
  w_CLADOC = space(2)
  w_DATMOD = ctod("  /  /  ")
  w_oERRORLOG = .NULL.
  w_SERIAL = space(10)
  w_BCOMM = .f.
  w_BMAGA = .f.
  w_ROWNUM = 0
  w_NUMRIF = 0
  w_VALULT = 0
  w_IMPSCO = 0
  w_IMPCOM = 0
  w_VALMAG = 0
  w_IMPNAZ = 0
  w_IMPACC_IVA = 0
  w_OK = .f.
  w_SEREVA = space(10)
  w_CLADOCEVA = space(2)
  w_ROWORD = 0
  * --- WorkFile variables
  AHEPATCH_idx=0
  DOC_DETT_idx=0
  DOC_MAST_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Riparte gli sconti di piede se il documento non � stato
    *     oggetto della ripartizione degli sconti.
    * --- FIle di LOG
    * --- Try
    local bErr_0375B270
    bErr_0375B270=bTrsErr
    this.Try_0375B270()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE GENERICO - impossibile aggiornare documenti %1", this.oParentObject.w_PMSG)
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_0375B270
    * --- End
    if used("CESP")
       
 select CESP 
 use
    endif
  endproc
  proc Try_0375B270()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Calcolo la data di installazione della Fast Pacth 89 4_0
    * --- Read from AHEPATCH
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AHEPATCH_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AHEPATCH_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PTDATINS"+;
        " from "+i_cTable+" AHEPATCH where ";
            +"PTRELEAS  = "+cp_ToStrODBC("4.0");
            +" and PTNUMPAT = "+cp_ToStrODBC(89);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PTDATINS;
        from (i_cTable) where;
            PTRELEAS  = "4.0";
            and PTNUMPAT = 89;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DATMOD = NVL(cp_ToDate(_read_.PTDATINS),cp_NullValue(_read_.PTDATINS))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Se installata path 89 allora procedo con l'aggiornamento
    if i_Rows>0
      * --- Stanzia l'oggetto per la gestione delle messaggistiche di errore
      this.w_oERRORLOG=createobject("AH_ErrorLog")
      * --- begin transaction
      cp_BeginTrs()
      * --- Scorro ogni documento ed aggiorno il dettaglio...
      * --- Select from GSCVBB16
      do vq_exec with 'GSCVBB16',this,'_Curs_GSCVBB16','',.f.,.t.
      if used('_Curs_GSCVBB16')
        select _Curs_GSCVBB16
        locate for 1=1
        do while not(eof())
        this.w_SERIAL = _Curs_GSCVBB16.MVSERIAL
        this.w_CLADOC = _Curs_GSCVBB16.MVCLADOC
        * --- Costruisco il cursore da passare alla routine di ricalcolo della ripartizione dei
        *     soli sconti
        VQ_EXEC("..\PCON\EXE\QUERY\GSCVBB16b.VQR", this , "GENEAPP" )
        =WrCursor( "GENEAPP" )
        * --- L'anomalia sia solo nel caso in cui le spese di testata sono tutte a 0!
        *     (non gestisco errori di ritorno)
        this.w_MESS = GSAR_BRS(this,"B", "GENEAPP", _Curs_GSCVBB16.MVFLVEAC, _Curs_GSCVBB16.MVFLSCOR, 0 , Nvl(_Curs_GSCVBB16.SCONTI,0), Nvl(_Curs_GSCVBB16.TOTMERCE,0), Nvl(_Curs_GSCVBB16.MVCODIVE,""), nvl(_Curs_GSCVBB16.MVCAOVAL,0), _Curs_GSCVBB16.DATCOM, nvl(_Curs_GSCVBB16.CAONAZ,0), nvl(_Curs_GSCVBB16.MVVALNAZ,""), Nvl(_Curs_GSCVBB16.MVCODVAL,""), 0 , This )
        if _Curs_GSCVBB16.MVFLVEAC="V"
          this.w_oERRORLOG.AddMsgLog("Ciclo vendite causale %1 registrazione num: %2 del:%3:", _Curs_GSCVBB16.MVTIPDOC, STR(_Curs_GSCVBB16.MVNUMREG,6,0), DTOC(_Curs_GSCVBB16.MVDATREG) )     
        else
          this.w_oERRORLOG.AddMsgLog("Ciclo acquisti causale %1 registrazione num: %2 del:%3:", _Curs_GSCVBB16.MVTIPDOC, STR(_Curs_GSCVBB16.MVNUMREG,6,0), DTOC(_Curs_GSCVBB16.MVDATREG) )     
        endif
        this.w_BCOMM = .T.
        this.w_BMAGA = .T.
        * --- Scorro il cursore con i nuovi valori ed aggiorno il database..
         
 Select "GENEAPP" 
 Go Top
        do while Not Eof( "GENEAPP" )
          * --- Alla prima riga valida
          if this.w_BCOMM And Not Empty ( Nvl( t_MVCODCOS , "" )) And ( Not Empty ( Nvl( t_MVFLORCO , "" )) Or Not Empty ( Nvl( t_MVFLCOCO , "" )) )
            this.w_oERRORLOG.AddMsgLog("Occorre ricostruire saldi di commessa")     
            this.w_BCOMM = .F.
          endif
          if this.w_BMAGA And t_FLINVE="S" And ((Nvl(t_FLELGM,"")="S" And Nvl(t_FLAVAL,"")$"AVCS") Or ((Nvl(t_FLELGM1,"")="S" And Nvl(t_FLAVAL1,"")$"AVCS")))
            this.w_oERRORLOG.AddMsgLog("Occorre verificare eventuali inventari nel periodo")     
            this.w_BMAGA = .F.
          endif
          this.w_ROWNUM = CPROWNUM
          this.w_NUMRIF = MVNUMRIF
          this.w_OK = .T.
          if this.w_CLADOC$"DT-DI" And t_MVQTAEV1<>0
            * --- Verifico il tipo documento del documento che evade...
            this.w_ROWORD = Nvl( t_CPROWORD , 0 )
            * --- Read from DOC_DETT
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.DOC_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "MVSERIAL"+;
                " from "+i_cTable+" DOC_DETT where ";
                    +"MVSERRIF = "+cp_ToStrODBC(this.w_SERIAL);
                    +" and MVROWRIF = "+cp_ToStrODBC(this.w_ROWNUM);
                    +" and MVNUMRIF = "+cp_ToStrODBC(this.w_NUMRIF);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                MVSERIAL;
                from (i_cTable) where;
                    MVSERRIF = this.w_SERIAL;
                    and MVROWRIF = this.w_ROWNUM;
                    and MVNUMRIF = this.w_NUMRIF;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_SEREVA = NVL(cp_ToDate(_read_.MVSERIAL),cp_NullValue(_read_.MVSERIAL))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            * --- Read from DOC_MAST
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.DOC_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "MVCLADOC"+;
                " from "+i_cTable+" DOC_MAST where ";
                    +"MVSERIAL = "+cp_ToStrODBC(this.w_SEREVA);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                MVCLADOC;
                from (i_cTable) where;
                    MVSERIAL = this.w_SEREVA;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_CLADOCEVA = NVL(cp_ToDate(_read_.MVCLADOC),cp_NullValue(_read_.MVCLADOC))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            * --- NON VERIFICO EVENTUALE RIPARTIZIONE SPESE!
            if this.w_CLADOCEVA$"FA-NC"
              this.w_oERRORLOG.AddMsgLog("Impossibile rivalorizzare riga %1 rivalorizzata da fattura o nota di credito", Alltrim(Str( this.w_ROWORD )) )     
              this.w_OK = .F.
            endif
          endif
          if this.w_OK
            this.w_VALULT = t_MVVALULT
            this.w_IMPSCO = t_MVIMPSCO
            this.w_IMPCOM = t_MVIMPCOM
            this.w_VALMAG = t_MVVALMAG
            this.w_IMPNAZ = t_MVIMPNAZ
            * --- Write into DOC_DETT
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.DOC_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"MVVALULT ="+cp_NullLink(cp_ToStrODBC(this.w_VALULT),'DOC_DETT','MVVALULT');
              +",MVIMPSCO ="+cp_NullLink(cp_ToStrODBC(this.w_IMPSCO),'DOC_DETT','MVIMPSCO');
              +",MVIMPCOM ="+cp_NullLink(cp_ToStrODBC(this.w_IMPCOM),'DOC_DETT','MVIMPCOM');
              +",MVVALMAG ="+cp_NullLink(cp_ToStrODBC(this.w_VALMAG),'DOC_DETT','MVVALMAG');
              +",MVIMPNAZ ="+cp_NullLink(cp_ToStrODBC(this.w_IMPNAZ),'DOC_DETT','MVIMPNAZ');
                  +i_ccchkf ;
              +" where ";
                  +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWNUM);
                  +" and MVNUMRIF = "+cp_ToStrODBC(this.w_NUMRIF);
                     )
            else
              update (i_cTable) set;
                  MVVALULT = this.w_VALULT;
                  ,MVIMPSCO = this.w_IMPSCO;
                  ,MVIMPCOM = this.w_IMPCOM;
                  ,MVVALMAG = this.w_VALMAG;
                  ,MVIMPNAZ = this.w_IMPNAZ;
                  &i_ccchkf. ;
               where;
                  MVSERIAL = this.w_SERIAL;
                  and CPROWNUM = this.w_ROWNUM;
                  and MVNUMRIF = this.w_NUMRIF;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            this.w_NUMAGG = this.w_NUMAGG + 1
          endif
           
 Select "GENEAPP" 
 Skip
        enddo
         
 Use in "GENEAPP"
          select _Curs_GSCVBB16
          continue
        enddo
        use
      endif
      * --- commit
      cp_EndTrs(.t.)
      this.w_oERRORLOG.PrintLog(this.oParentObject,"Errori riscontrati",.F.)     
    endif
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("- Aggiornamento dettaglio documenti%0Variati %1 righe%0", ALLTRIM(STR(this.w_NUMAGG)) )
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='AHEPATCH'
    this.cWorkTables[2]='DOC_DETT'
    this.cWorkTables[3]='DOC_MAST'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_GSCVBB16')
      use in _Curs_GSCVBB16
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
