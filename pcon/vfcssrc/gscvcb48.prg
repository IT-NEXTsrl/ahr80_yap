* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcb48                                                        *
*              Aggiornamento flag  pratica riservata                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-11-14                                                      *
* Last revis.: 2009-06-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcb48",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcb48 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_OKCONV = .f.
  w_NOPRAT = .f.
  w_CNFLRISE = space(1)
  * --- WorkFile variables
  CAN_TIER_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna l' esercizio di competenza sui movimenti di analitica
    * --- Eseguo conversione se ho una regola attiva sulle pratiche e restrizioni sugli utenti\gruppi
    * --- Select from GSCVCB48
    do vq_exec with 'GSCVCB48',this,'_Curs_GSCVCB48','',.f.,.t.
    if used('_Curs_GSCVCB48')
      select _Curs_GSCVCB48
      locate for 1=1
      do while not(eof())
      this.w_OKCONV = .T.
        select _Curs_GSCVCB48
        continue
      enddo
      use
    endif
    if Isalt()
      * --- Se esitono delle pratiche gi� riservate chiedo se si desidera cmq eseguire conversione
      * --- Read from CAN_TIER
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CAN_TIER_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CNFLRISE"+;
          " from "+i_cTable+" CAN_TIER where ";
              +"CNFLRISE = "+cp_ToStrODBC("S");
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CNFLRISE;
          from (i_cTable) where;
              CNFLRISE = "S";
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CNFLRISE = NVL(cp_ToDate(_read_.CNFLRISE),cp_NullValue(_read_.CNFLRISE))
        use
        if i_Rows=0
          this.w_NOPRAT = .T.
        endif
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    if IsAlt() AND this.w_OKCONV AND (this.w_NOPRAT or (!this.w_NOPRAT and Ah_Yesno("Esistono delle pratiche gi� riservate, si desidera  procedere comunque a rendere tutte le pratiche riservate?")))
      * --- Try
      local bErr_037BE540
      bErr_037BE540=bTrsErr
      this.Try_037BE540()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        * --- Gestisce log errori
        this.oParentObject.w_PMSG = Message()
        this.oParentObject.w_PESEOK = .F.
        if this.w_NHF>=0
          this.w_TMPC = ah_Msgformat("Errore generico - impossibile eseguire aggiornamento richiesto")
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
      bTrsErr=bTrsErr or bErr_037BE540
      * --- End
    else
      * --- Esecuzione ok
      this.oParentObject.w_PMSG = ah_Msgformat("Elaborazione terminata con successo")
      this.oParentObject.w_PESEOK = .T.
      this.w_TMPC = ah_Msgformat("Procedura di conversione eseguita con successo")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
  endproc
  proc Try_037BE540()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Aggiorno il campo CMCOMPET
    * --- Write into CAN_TIER
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.CAN_TIER_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.CAN_TIER_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"CNFLRISE ="+cp_NullLink(cp_ToStrODBC("S"),'CAN_TIER','CNFLRISE');
          +i_ccchkf ;
      +" where ";
          +"1 = "+cp_ToStrODBC(1);
             )
    else
      update (i_cTable) set;
          CNFLRISE = "S";
          &i_ccchkf. ;
       where;
          1 = 1;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    this.w_TMPC = ah_Msgformat("Aggiornamento campo CNFLRISE eseguita con successo")
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = this.w_TMPC
    * --- commit
    cp_EndTrs(.t.)
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='CAN_TIER'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_GSCVCB48')
      use in _Curs_GSCVCB48
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
