* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvab14                                                        *
*              Aggiornamento pezzi x conf.                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_345]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-10-18                                                      *
* Last revis.: 2007-05-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvab14",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvab14 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_NomeCon = space(50)
  w_Result = 0
  * --- WorkFile variables
  RIPATMP1_idx=0
  ART_ICOL_idx=0
  KEY_ARTI_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- FIle di LOG
    * --- Try
    local bErr_0360A0A0
    bErr_0360A0A0=bTrsErr
    this.Try_0360A0A0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = this.oParentObject.w_PMSG
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_0360A0A0
    * --- End
  endproc
  proc Try_0360A0A0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
     
 i_nConn=i_TableProp[this.ART_ICOL_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
    if i_nConn<>0
      do case
        case upper(CP_DBTYPE)="SQLSERVER"
          * --- Caso SQLServer
          w_iRows = SQLExec(i_nConn,"ALTER TABLE "+i_cTable+; 
 " ALTER COLUMN ARPZCONF DECIMAL(9,3)")
          if w_iRows=-1
             
 ErrMess=Alltrim(Message()) 
 Archivio=i_cTable 
 Campo="ARPZCONF"
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          w_iRows = SQLExec(i_nConn,"ALTER TABLE xxxART_ICOL"+; 
 " ALTER COLUMN ARPZCONF DECIMAL(9,3)")
          if w_iRows=-1
             
 ErrMess=Alltrim(Message()) 
 Archivio="xxxART_ICOL" 
 Campo="ARPZCONF"
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          w_iRows = SQLExec(i_nConn,"ALTER TABLE "+i_cTable+; 
 " ALTER COLUMN ARPZCON2 DECIMAL(9,3)")
          if w_iRows=-1
             
 ErrMess=Alltrim(Message()) 
 Archivio=i_cTable 
 Campo="ARPZCON2"
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          w_iRows = SQLExec(i_nConn,"ALTER TABLE xxxART_ICOL"+; 
 " ALTER COLUMN ARPZCON2 DECIMAL(9,3) ")
          if w_iRows=-1
             
 ErrMess=Alltrim(Message()) 
 Archivio="xxxART_ICOL" 
 Campo="ARPZCON2"
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
           
 i_nConn=i_TableProp[this.KEY_ARTI_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2])
          w_iRows = SQLExec(i_nConn,"ALTER TABLE "+i_cTable+; 
 " ALTER COLUMN CAPZCON3 DECIMAL(9,3)")
          if w_iRows=-1
             
 ErrMess=Alltrim(Message()) 
 Archivio=i_cTable 
 Campo="CAPZCON3"
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          w_iRows = SQLExec(i_nConn,"ALTER TABLE xxxKEY_ARTI"+; 
 " ALTER COLUMN CAPZCON3 DECIMAL(9,3) ")
          if w_iRows=-1
             
 ErrMess=Alltrim(Message()) 
 Archivio="xxxKEY_ARTI" 
 Campo="CAPZCON3"
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        case upper(CP_DBTYPE)="ORACLE"
          * --- Caso ORACLE
          this.w_TMPC = ah_Msgformat("Procedura di conversione solo per SQL Server e DB2")
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        case upper(CP_DBTYPE)="DB2"
          * --- Caso DB2
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
      endcase
      if bTrsErr
        * --- Raise
        i_Error=MESSAGE()
        return
      else
        if this.w_NHF>=0
          this.w_TMPC = ah_Msgformat("Adeguamento campi ARPZCONF e ARPZCON2 tabella ART_ICOL,%0CAPZCON3 tabella Key_ARTI eseguito correttamente%0Occorre ripetere la procedura su ogni azienda")
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.w_TMPC = ah_Msgformat("Adeguamento campi ARPZCONF e ARPZCON2 tabella ART_ICOL,%0CAPZCON3 tabella Key_ARTI eseguito correttamente%0Occorre ripetere la procedura su ogni azienda")
    this.oParentObject.w_PMSG = this.w_TMPC
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella DB2
    pName="ART_ICOL"
    pDatabaseType=i_ServerConn[1,6]
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.ART_ICOL_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
          )
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    * --- Elimino la tabella
    if Not GSCV_BDT( this, "ART_ICOL" , i_nConn )
      * --- Raise
      i_Error="Error"
      return
    endif
    * --- Ricostruisco il database per la tabella
    GSCV_BRT(this, "ART_ICOL" , i_nConn , .T.)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into ART_ICOL
    i_nConn=i_TableProp[this.ART_ICOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.ART_ICOL_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_Msg("Ripristinati valori di origine",.T.)
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    ah_Msg("Eliminata tabella temporanea tmp",.T.)
    * --- Converte tabella DB2
    pName="KEY_ARTI"
    pDatabaseType=i_ServerConn[1,6]
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.KEY_ARTI_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
          )
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    * --- Elimino la tabella
    if Not GSCV_BDT( this, "KEY_ARTI" , i_nConn )
      * --- Raise
      i_Error="Error"
      return
    endif
    * --- Ricostruisco il database per la tabella
    GSCV_BRT(this, "KEY_ARTI" , i_nConn , .T.)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into KEY_ARTI
    i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.KEY_ARTI_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_Msg("Ripristinati valori di origine",.T.)
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    ah_Msg("Eliminata tabella temporanea tmp",.T.)
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- provo a cancellare il constrants dei valori di default
    *     a seconda se SQL Server 2000 o SQL Server 7 ho 2 messaggi di risposta differenti
    *     in SQL Server 2000: The object 'DF__TAMSWSALD__SLDAR__5CACADF9' is dependent on column 'sldarfin'.
    *     quindi leggo la stringa tra i primi 2 apici mentre in SQL Server 7:
    *     ALTER TABLE ALTER COLUMN AZDENMAG failed because DEFAULT CONSTRAINT DF__AZIENDA__AZDENMA__17036CC0 accesses this column
    *     cerco DEFAULT CONSTRAINT e prendo fino a accesses.
    *     comincio da SQL Server 2000 (se trovo l'apice)
    *     cerco il primo apice
    i_nConn=i_TableProp[this.ART_ICOL_idx,3]
     
 Scale=3 
 Lunghezza=9 
 DisAllowNull=.f. 
 DefCon=""
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- cancello tutti i Constraints
    do while !Empty(this.w_NomeCon)
      this.w_Result=SqlExec(i_nConn, "Alter table "+archivio+" DROP CONSTRAINT "+this.w_NomeCon)
      * --- esco con errore, non riesco a cancellare il constraints
      if this.w_Result=-1
        this.w_NomeCon = ""
      else
        if Upper(Left(this.w_NomeCon,2))="DF" 
           
 Valoredef="S" 
 DefCon=this.w_NomeCon
        endif
        this.w_Result=SqlExec(i_nConn, "Alter table "+archivio+" ALTER COLUMN "+CAMPO+" DECIMAL("+alltrim(str(lunghezza))+","+Alltrim(Str(Scale))+")  " )
        if this.w_Result=-1
           
 ErrMess=Message()
          this.Pag4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          this.w_NomeCon = ""
        endif
      endif
    enddo
    if ValoreDef="S"
      * --- Rimetto il valore di Default
      this.w_Result=SqlExec(i_nConn, "Alter table "+archivio+" ADD CONSTRAINT "+DefCon+" DEFAULT 0 FOR "+CAMPO )
    endif
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Dal messaggio di errore cerco di capire che tipo di costraint devo eliminare
     
 Local Mess, Pos 
 Mess2=ErrMess 
 Pos=At("'" , Mess2 )
    if pos<>0
      * --- leggo il nome del constrains che da il problema
      Mess2=Right(Mess2,Len(Mess2)-Pos)
      * --- cerco l'altro apice e in ErrMess dovrei trovarmi il nome del contraint da droppare
      Pos=At("'" , Mess2 )
      this.w_NomeCon = Alltrim(Left(Mess2,Pos-1))
    else
      * --- caso SQL Server 7.0 non trovo l'apice cerco CONSTRAINT non riesco a gestire l'errore
      Pos=At("CONSTRAINT" , Mess2 )
      if Pos<>0
        * --- L'ho trovato:cerco accesses
         
 Mess2=Right(Mess2,Len(Mess2)-Pos-10) 
 Pos=At("accesses" , Mess2 )
        * --- cerco accesses e in ErrMess dovrei trovarmi il nome del contraint da droppare
        this.w_NomeCon = Alltrim(Left(Mess2,Pos-1))
      else
        * --- Dal messaggio non riesco a capire il contraint
        this.w_NomeCon = ""
      endif
    endif
  endproc


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='*RIPATMP1'
    this.cWorkTables[2]='ART_ICOL'
    this.cWorkTables[3]='KEY_ARTI'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
