* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvbb83                                                        *
*              Eliminazione campi nei dati azienda obsoleti                    *
*                                                                              *
*      Author: Zucchetti TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_77]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-05-07                                                      *
* Last revis.: 2008-02-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvbb83",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvbb83 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_ROWS = 0
  w_AZPATOFF = space(200)
  w_CODAZI = space(5)
  w_AZCOACOG = space(1)
  w_OK = .f.
  w_OK1 = .f.
  w_LOOP = space(1)
  * --- WorkFile variables
  AZIENDA_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- FIle di LOG
    *     5.0 - Elimina campi obsoleti tabella AZIENDA
    *     AZMASOFF
    *     AZPEROFF
    *     AZPATOFF
    *     AZFILOFF
    *     AZMODOFF
    this.w_OK1 = .T.
    this.w_CODAZI = i_CODAZI
    this.w_OK = .T.
    if upper(CP_DBTYPE)<>"DB2"
      * --- Try
      local bErr_035D1B00
      bErr_035D1B00=bTrsErr
      this.Try_035D1B00()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_OK = .F.
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_035D1B00
      * --- End
       
 Dimension ARFIELD(5) 
 ARFIELD[1]="AZMASOFF" 
 ARFIELD[2]="AZPEROFF" 
 ARFIELD[3]="AZPATOFF" 
 ARFIELD[4]="AZFILOFF" 
 ARFIELD[5]="AZMODOFF"
    else
      this.w_OK = .F.
      this.w_OK1 = .F.
    endif
    * --- Try
    local bErr_037E9B10
    bErr_037E9B10=bTrsErr
    this.Try_037E9B10()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_msgformat("ERRORE GENERICO - Impossibile eseguire aggiornamento richiesto")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_037E9B10
    * --- End
  endproc
  proc Try_035D1B00()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZPATOFF"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZPATOFF;
        from (i_cTable) where;
            AZCODAZI = this.w_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_AZPATOFF = NVL(cp_ToDate(_read_.AZPATOFF),cp_NullValue(_read_.AZPATOFF))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    return
  proc Try_037E9B10()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.w_LOOP = 0
    do while this.w_LOOP<5
      this.w_ROWS = 0
      this.w_LOOP = this.w_LOOP + 1
      if this.w_OK
        i_nConn=i_TableProp[this.AZIENDA_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
        this.w_ROWS = SQLExec(i_nConn,"ALTER TABLE "+i_cTable+" DROP COLUMN " + ARFIELD[this.w_LOOP] )
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      else
        * --- Esecuzione ok
        this.oParentObject.w_PESEOK = .T.
        this.w_TMPC = ah_msgformat("Eliminazione campi da tabella AZIENDA eseguita con successo%0")
        this.oParentObject.w_PMSG = this.w_TMPC
        this.w_LOOP = 6
      endif
    enddo
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if this.w_ROWS<0
      * --- Raise
      i_Error=Message()
      return
    else
      * --- Esecuzione ok
      this.oParentObject.w_PESEOK = .T.
      this.w_TMPC = ah_msgformat("Eliminazione campo %1 eseguita con successo%0",arfield[this.w_loop])
      this.oParentObject.w_PMSG = this.oParentObject.w_PMSG + this.w_TMPC
    endif
  endproc


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='AZIENDA'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
