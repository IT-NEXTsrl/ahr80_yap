* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvab68                                                        *
*              Nuova dichiarazione di intenti                                  *
*                                                                              *
*      Author: Zucchetti TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_96]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-05-07                                                      *
* Last revis.: 2002-05-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvab68",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvab68 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_DISERIAL = space(15)
  w_DICODCON = space(15)
  w_DITIPCON = space(1)
  w_DI__ANNO = space(4)
  w_DINUMDIC = 0
  w_DITIPIVA = space(1)
  w_DIDATLET = ctod("  /  /  ")
  w_DINUMDOC = 0
  * --- WorkFile variables
  ESP_ABIT_idx=0
  DIC_INTE_idx=0
  DOC_MAST_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Popola la nuova tabella DIC_INTE da ESP_ABIT (Lettere di Intento)
    * --- Try
    local bErr_038BA278
    bErr_038BA278=bTrsErr
    this.Try_038BA278()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE GENERICO - impossibile inserire le lettere di intento")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_038BA278
    * --- End
  endproc
  proc Try_038BA278()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    i_Conn=i_TableProp[this.DIC_INTE_IDX, 3]
    * --- Dalla precedente tabella ESP_ABIT che contiene le lettere di intento,
    *     popolo la nuova tabella DIC_INTE
    ah_Msg("Aggiornamento lettere di intento...",.T.)
    * --- Select from ESP_ABIT
    i_nConn=i_TableProp[this.ESP_ABIT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ESP_ABIT_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select DICODCON,DITIPCON,DI__ANNO  from "+i_cTable+" ESP_ABIT ";
          +" order by DITIPCON";
           ,"_Curs_ESP_ABIT")
    else
      select DICODCON,DITIPCON,DI__ANNO from (i_cTable);
       order by DITIPCON;
        into cursor _Curs_ESP_ABIT
    endif
    if used('_Curs_ESP_ABIT')
      select _Curs_ESP_ABIT
      locate for 1=1
      do while not(eof())
      this.w_DICODCON = _Curs_ESP_ABIT.DICODCON
      this.w_DITIPCON = _Curs_ESP_ABIT.DITIPCON
      this.w_DI__ANNO = _Curs_ESP_ABIT.DI__ANNO
      this.w_DISERIAL = Space(15)
      ah_Msg("Inserimento lettera di intento %1",.T.,.F.,.F., Alltrim(this.w_DICODCON) )
      cp_NextTableProg(this, i_Conn, "SEDIN", "i_codazi,w_DISERIAL")
      * --- Inserimento Dichiarazioni di intento nella nuova tabella
      * --- Insert into DIC_INTE
      i_nConn=i_TableProp[this.DIC_INTE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DIC_INTE_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"gscvab68",this.DIC_INTE_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Modifico la chiave esterna sui documenti che punta alla nuova tabella delle lettere di intento
      * --- Write into DOC_MAST
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DOC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="MVSERIAL"
        do vq_exec with 'gscva168',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="DOC_MAST.MVSERIAL = _t2.MVSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MVRIFDIC ="+cp_NullLink(cp_ToStrODBC(this.w_DISERIAL),'DOC_MAST','MVRIFDIC');
            +i_ccchkf;
            +" from "+i_cTable+" DOC_MAST, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="DOC_MAST.MVSERIAL = _t2.MVSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_MAST, "+i_cQueryTable+" _t2 set ";
        +"DOC_MAST.MVRIFDIC ="+cp_NullLink(cp_ToStrODBC(this.w_DISERIAL),'DOC_MAST','MVRIFDIC');
            +Iif(Empty(i_ccchkf),"",",DOC_MAST.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="DOC_MAST.MVSERIAL = t2.MVSERIAL";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_MAST set (";
            +"MVRIFDIC";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +cp_NullLink(cp_ToStrODBC(this.w_DISERIAL),'DOC_MAST','MVRIFDIC')+"";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="DOC_MAST.MVSERIAL = _t2.MVSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_MAST set ";
        +"MVRIFDIC ="+cp_NullLink(cp_ToStrODBC(this.w_DISERIAL),'DOC_MAST','MVRIFDIC');
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MVRIFDIC ="+cp_NullLink(cp_ToStrODBC(this.w_DISERIAL),'DOC_MAST','MVRIFDIC');
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Esecuzione ok
      this.oParentObject.w_PESEOK = .T.
      this.oParentObject.w_PMSG = this.w_TMPC
        select _Curs_ESP_ABIT
        continue
      enddo
      use
    endif
    if this.oParentObject.w_PESEOK=.F.
      this.oParentObject.w_PESEOK = .T.
      this.oParentObject.w_PMSG = ah_Msgformat("Non ci sono lettere di intento da riportare")
    endif
    * --- Select from DIC_INTE
    i_nConn=i_TableProp[this.DIC_INTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DIC_INTE_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" DIC_INTE ";
          +" where DITIPCON = 'F'";
           ,"_Curs_DIC_INTE")
    else
      select * from (i_cTable);
       where DITIPCON = "F";
        into cursor _Curs_DIC_INTE
    endif
    if used('_Curs_DIC_INTE')
      select _Curs_DIC_INTE
      locate for 1=1
      do while not(eof())
      * --- Nelle lettere a fornitori aggiorno il Numero documento e l'anno
      this.w_DIDATLET = _Curs_DIC_INTE.DIDATDIC
      this.w_DITIPIVA = _Curs_DIC_INTE.DITIPIVA
      this.w_DITIPCON = _Curs_DIC_INTE.DITIPCON
      this.w_DI__ANNO = _Curs_DIC_INTE.DI__ANNO
      this.w_DINUMDOC = 0
      this.w_DISERIAL = _Curs_DIC_INTE.DISERIAL
      cp_NextTableProg(this, i_Conn, "PRDID", "i_codazi,w_DI__ANNO,w_DITIPCON,w_DITIPIVA,w_DINUMDOC")
      * --- Write into DIC_INTE
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DIC_INTE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DIC_INTE_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DIC_INTE_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"DIDATLET ="+cp_NullLink(cp_ToStrODBC(this.w_DIDATLET),'DIC_INTE','DIDATLET');
        +",DINUMDOC ="+cp_NullLink(cp_ToStrODBC(this.w_DINUMDOC),'DIC_INTE','DINUMDOC');
            +i_ccchkf ;
        +" where ";
            +"DISERIAL = "+cp_ToStrODBC(this.w_DISERIAL);
               )
      else
        update (i_cTable) set;
            DIDATLET = this.w_DIDATLET;
            ,DINUMDOC = this.w_DINUMDOC;
            &i_ccchkf. ;
         where;
            DISERIAL = this.w_DISERIAL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
        select _Curs_DIC_INTE
        continue
      enddo
      use
    endif
    * --- Select from gscva268
    do vq_exec with 'gscva268',this,'_Curs_gscva268','',.f.,.t.
    if used('_Curs_gscva268')
      select _Curs_gscva268
      locate for 1=1
      do while not(eof())
      * --- Forzo il nuovo progressivo in base a quello vecchio
      *     Eseguo una MAX del Numero Protocollo per Tipo conto (Cliente/Fornitore) e anno
      this.w_DITIPCON = _Curs_gscva268.TIPCON
      this.w_DI__ANNO = _Curs_gscva268.ANNO
      this.w_DINUMDIC = _Curs_gscva268.NUMDIC
      cp_NextTableProg(this, i_Conn, "PRDIN", "i_codazi,w_DI__ANNO,w_DITIPCON,w_DINUMDIC")
        select _Curs_gscva268
        continue
      enddo
      use
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='ESP_ABIT'
    this.cWorkTables[2]='DIC_INTE'
    this.cWorkTables[3]='DOC_MAST'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_ESP_ABIT')
      use in _Curs_ESP_ABIT
    endif
    if used('_Curs_DIC_INTE')
      use in _Curs_DIC_INTE
    endif
    if used('_Curs_gscva268')
      use in _Curs_gscva268
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
