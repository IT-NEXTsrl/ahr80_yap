* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcb28                                                        *
*              Predisposizione tabella dei Tipi riserva                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-01-21                                                      *
* Last revis.: 2011-01-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcb28",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcb28 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_NCONN = 0
  w_TROVATA = .f.
  w_CodRel = space(15)
  w_SERIALE_ATT = space(20)
  * --- WorkFile variables
  TIP_RISE_idx=0
  OFF_ATTI_idx=0
  CONVERSI_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inserimento di 7 records nella tabella dei Tipi riserva ed aggiornamento campo Riserva nelle Attivit�
    this.w_NCONN = i_TableProp[this.OFF_ATTI_idx,3] 
    this.w_CodRel = "7.0-M5522"
    * --- Try
    local bErr_03769E20
    bErr_03769E20=bTrsErr
    this.Try_03769E20()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_03769E20
    * --- End
    if IsAlt() AND NOT this.w_TROVATA
      * --- Try
      local bErr_037A1310
      bErr_037A1310=bTrsErr
      this.Try_037A1310()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
      endif
      bTrsErr=bTrsErr or bErr_037A1310
      * --- End
      * --- Try
      local bErr_035EDEF0
      bErr_035EDEF0=bTrsErr
      this.Try_035EDEF0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
      endif
      bTrsErr=bTrsErr or bErr_035EDEF0
      * --- End
      * --- Try
      local bErr_035EC450
      bErr_035EC450=bTrsErr
      this.Try_035EC450()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
      endif
      bTrsErr=bTrsErr or bErr_035EC450
      * --- End
      * --- Try
      local bErr_037A5720
      bErr_037A5720=bTrsErr
      this.Try_037A5720()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
      endif
      bTrsErr=bTrsErr or bErr_037A5720
      * --- End
      * --- Try
      local bErr_035F47A0
      bErr_035F47A0=bTrsErr
      this.Try_035F47A0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
      endif
      bTrsErr=bTrsErr or bErr_035F47A0
      * --- End
      * --- Try
      local bErr_03771410
      bErr_03771410=bTrsErr
      this.Try_03771410()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
      endif
      bTrsErr=bTrsErr or bErr_03771410
      * --- End
      * --- Try
      local bErr_03782400
      bErr_03782400=bTrsErr
      this.Try_03782400()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
      endif
      bTrsErr=bTrsErr or bErr_03782400
      * --- End
    endif
    if NOT this.w_TROVATA
      * --- Try
      local bErr_036087E0
      bErr_036087E0=bTrsErr
      this.Try_036087E0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
      endif
      bTrsErr=bTrsErr or bErr_036087E0
      * --- End
      * --- Try
      local bErr_0376F0A0
      bErr_0376F0A0=bTrsErr
      this.Try_0376F0A0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
      endif
      bTrsErr=bTrsErr or bErr_0376F0A0
      * --- End
    endif
    * --- Resoconto - sempre positivo
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Elaborazione terminata con successo")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = ah_Msgformat("Elaborazione terminata con successo")
  endproc
  proc Try_03769E20()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Legge se eseguita la conversione  7.0-M5522
    * --- Read from CONVERSI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CONVERSI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONVERSI_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "*"+;
        " from "+i_cTable+" CONVERSI where ";
            +"COCODREL = "+cp_ToStrODBC(this.w_CodRel);
            +" and COESEGUI = "+cp_ToStrODBC("S");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        *;
        from (i_cTable) where;
            COCODREL = this.w_CodRel;
            and COESEGUI = "S";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_TROVATA = i_Rows>0
    return
  proc Try_037A1310()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Inserimento tipo Riserva
    * --- Insert into TIP_RISE
    i_nConn=i_TableProp[this.TIP_RISE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TIP_RISE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TIP_RISE_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"TRCODICE"+",TRDESTIP"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC("R"),'TIP_RISE','TRCODICE');
      +","+cp_NullLink(cp_ToStrODBC("Riserva"),'TIP_RISE','TRDESTIP');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'TRCODICE',"R",'TRDESTIP',"Riserva")
      insert into (i_cTable) (TRCODICE,TRDESTIP &i_ccchkf. );
         values (;
           "R";
           ,"Riserva";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Inserito tipo Riserva")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    return
  proc Try_035EDEF0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Inserimento tipo Decisione
    * --- Insert into TIP_RISE
    i_nConn=i_TableProp[this.TIP_RISE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TIP_RISE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TIP_RISE_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"TRCODICE"+",TRDESTIP"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC("D"),'TIP_RISE','TRCODICE');
      +","+cp_NullLink(cp_ToStrODBC("Decisione"),'TIP_RISE','TRDESTIP');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'TRCODICE',"D",'TRDESTIP',"Decisione")
      insert into (i_cTable) (TRCODICE,TRDESTIP &i_ccchkf. );
         values (;
           "D";
           ,"Decisione";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Inserito tipo Decisione")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    return
  proc Try_035EC450()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Inserimento tipo Congelata
    * --- Insert into TIP_RISE
    i_nConn=i_TableProp[this.TIP_RISE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TIP_RISE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TIP_RISE_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"TRCODICE"+",TRDESTIP"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC("C"),'TIP_RISE','TRCODICE');
      +","+cp_NullLink(cp_ToStrODBC("Congelata"),'TIP_RISE','TRDESTIP');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'TRCODICE',"C",'TRDESTIP',"Congelata")
      insert into (i_cTable) (TRCODICE,TRDESTIP &i_ccchkf. );
         values (;
           "C";
           ,"Congelata";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Inserito tipo Congelata")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    return
  proc Try_037A5720()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Inserimento tipo Sospesa
    * --- Insert into TIP_RISE
    i_nConn=i_TableProp[this.TIP_RISE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TIP_RISE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TIP_RISE_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"TRCODICE"+",TRDESTIP"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC("S"),'TIP_RISE','TRCODICE');
      +","+cp_NullLink(cp_ToStrODBC("Sospesa"),'TIP_RISE','TRDESTIP');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'TRCODICE',"S",'TRDESTIP',"Sospesa")
      insert into (i_cTable) (TRCODICE,TRDESTIP &i_ccchkf. );
         values (;
           "S";
           ,"Sospesa";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Inserito tipo Sospesa")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    return
  proc Try_035F47A0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Inserimento tipo Interrotta
    * --- Insert into TIP_RISE
    i_nConn=i_TableProp[this.TIP_RISE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TIP_RISE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TIP_RISE_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"TRCODICE"+",TRDESTIP"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC("I"),'TIP_RISE','TRCODICE');
      +","+cp_NullLink(cp_ToStrODBC("Interrotta"),'TIP_RISE','TRDESTIP');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'TRCODICE',"I",'TRDESTIP',"Interrotta")
      insert into (i_cTable) (TRCODICE,TRDESTIP &i_ccchkf. );
         values (;
           "I";
           ,"Interrotta";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Inserito tipo Interrotta")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    return
  proc Try_03771410()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Inserimento tipo Transatta
    * --- Insert into TIP_RISE
    i_nConn=i_TableProp[this.TIP_RISE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TIP_RISE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TIP_RISE_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"TRCODICE"+",TRDESTIP"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC("T"),'TIP_RISE','TRCODICE');
      +","+cp_NullLink(cp_ToStrODBC("Transatta"),'TIP_RISE','TRDESTIP');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'TRCODICE',"T",'TRDESTIP',"Transatta")
      insert into (i_cTable) (TRCODICE,TRDESTIP &i_ccchkf. );
         values (;
           "T";
           ,"Transatta";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Inserito tipo Transatta")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    return
  proc Try_03782400()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Inserimento tipo Cancellata
    * --- Insert into TIP_RISE
    i_nConn=i_TableProp[this.TIP_RISE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TIP_RISE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TIP_RISE_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"TRCODICE"+",TRDESTIP"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC("F"),'TIP_RISE','TRCODICE');
      +","+cp_NullLink(cp_ToStrODBC("Cancellata"),'TIP_RISE','TRDESTIP');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'TRCODICE',"F",'TRDESTIP',"Cancellata")
      insert into (i_cTable) (TRCODICE,TRDESTIP &i_ccchkf. );
         values (;
           "F";
           ,"Cancellata";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Inserito tipo Cancellata")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    return
  proc Try_036087E0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Aggiornamento campo Riserva delle Attivit�: nel caso sia pari ad 'N'  viene posto a .NULL.
    * --- Select from OFF_ATTI
    i_nConn=i_TableProp[this.OFF_ATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select ATSERIAL,ATTIPRIS  from "+i_cTable+" OFF_ATTI ";
          +" where ATTIPRIS='N'";
           ,"_Curs_OFF_ATTI")
    else
      select ATSERIAL,ATTIPRIS from (i_cTable);
       where ATTIPRIS="N";
        into cursor _Curs_OFF_ATTI
    endif
    if used('_Curs_OFF_ATTI')
      select _Curs_OFF_ATTI
      locate for 1=1
      do while not(eof())
      this.w_SERIALE_ATT = ATSERIAL
      * --- Write into OFF_ATTI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.OFF_ATTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_ATTI_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"ATTIPRIS ="+cp_NullLink(cp_ToStrODBC(.NULL.),'OFF_ATTI','ATTIPRIS');
            +i_ccchkf ;
        +" where ";
            +"ATSERIAL = "+cp_ToStrODBC(this.w_SERIALE_ATT);
               )
      else
        update (i_cTable) set;
            ATTIPRIS = .NULL.;
            &i_ccchkf. ;
         where;
            ATSERIAL = this.w_SERIALE_ATT;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
        select _Curs_OFF_ATTI
        continue
      enddo
      use
    endif
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Azzerato campo Riserva")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    return
  proc Try_0376F0A0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Ricostruisce integrit� referenziali della tabella OFF_ATTI (Attivit�)
    GSCV_BRT(this, "OFF_ATTI" , this.w_NCONN )
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Ricostruite integrit� referenziali della tabella Attivit�")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='TIP_RISE'
    this.cWorkTables[2]='OFF_ATTI'
    this.cWorkTables[3]='CONVERSI'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_OFF_ATTI')
      use in _Curs_OFF_ATTI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
