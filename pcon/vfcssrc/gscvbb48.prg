* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvbb48                                                        *
*              Conversione gestfile - docm (creazione classi)                  *
*                                                                              *
*      Author: Zucchetti                                                       *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][116]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-12-02                                                      *
* Last revis.: 2008-04-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvbb48",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvbb48 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_KRIC = .f.
  w_CODCLAS = space(15)
  w_DESCLAS = space(50)
  w_NOMTAB = space(20)
  w_DATRAG = space(20)
  w_CODATT = space(15)
  w_DESATT = space(40)
  w_CAMCUR = space(20)
  w_PATSTD = space(100)
  w_VOLUME = space(10)
  w_OLDPAT = space(100)
  w_CURPAT = space(100)
  w_CURFILE = space(100)
  w_TMPN = 0
  w_TMPNF = 0
  w_TMPC = space(100)
  w_CODAZI = space(5)
  w_COUNTDIR = 0
  w_COUNTFILE = 0
  w_IDSERIAL = space(15)
  w_ESTENSIONE = space(100)
  w_DATARAGGR = ctod("  /  /  ")
  w_TIPORAGG = space(1)
  w_VALATT = space(100)
  w_DESCLASK = space(50)
  w_ARCHIVIO = space(2)
  w_PATH = space(254)
  w_TIPRAGR = space(1)
  w_CLASSEDOC = space(15)
  w_PATHCOPI = space(100)
  w_PATHCOLL = space(100)
  w_NOMEFILE = space(254)
  w_TIPOALL = space(5)
  w_CLASALL = space(5)
  w_INPATTIP = space(1)
  w_INPATCLA = space(1)
  w_DESCR = space(50)
  w_RAGGRU = space(1)
  w_DATARAG = ctod("  /  /  ")
  w_PATHABS = space(254)
  w_CONVROUT = space(50)
  w_CODREL = space(15)
  w_MSGLOG = space(20)
  * --- WorkFile variables
  LIB_IMMA_idx=0
  PROMCLAS_idx=0
  PRODCLAS_idx=0
  ART_ICOL_idx=0
  GESTFILE_idx=0
  CLA_ALLE_idx=0
  TIP_ALLE_idx=0
  CONVERSI_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Conversione GESTFILE >> DOCMAN
    * --- FIle di LOG
    * --- Creo una classe documentale per ogni archivio gestito e non
    * --- Try
    local bErr_047116B0
    bErr_047116B0=bTrsErr
    this.Try_047116B0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_msgformat("Errore generico durante la conversione - %1",Message())
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_047116B0
    * --- End
  endproc
  proc Try_047116B0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if UPPER( G_APPLICATION ) # "AD HOC ENTERPRISE"
      * --- Controllo se � stata eseguita e correttamente la conversione GSCVAB84 (3.0-M3162) - Prerequisito all'esecuzione della presente
      this.w_CONVROUT = "GSCVAB84"
      this.w_CODREL = "3.0-M3162"
    else
      * --- Controllo se � stata eseguita e correttamente la conversione GSCV_BDP (20060600--00000) - Prerequisito all'esecuzione della presente
      this.w_CONVROUT = "GSCV_BDP"
      this.w_CODREL = "20060600--00000"
    endif
    * --- Read from CONVERSI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CONVERSI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONVERSI_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "COESEGUI,COERRORE"+;
        " from "+i_cTable+" CONVERSI where ";
            +"CONOMPRO = "+cp_ToStrODBC(this.w_CONVROUT);
            +" and COCODREL = "+cp_ToStrODBC(this.w_CODREL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        COESEGUI,COERRORE;
        from (i_cTable) where;
            CONOMPRO = this.w_CONVROUT;
            and COCODREL = this.w_CODREL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      w_ESEGUITA = NVL(cp_ToDate(_read_.COESEGUI),cp_NullValue(_read_.COESEGUI))
      w_ERRORE = NVL(cp_ToDate(_read_.COERRORE),cp_NullValue(_read_.COERRORE))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    do case
      case w_ESEGUITA # "S"
        this.w_TMPC = Ah_msgformat("Procedura di conversione %1 non eseguita. Elaborazione interrotta!",this.w_CONVROUT)
        this.oParentObject.w_PESEOK = .F.
        this.oParentObject.w_pMSG = this.w_TMPC
        i_retcode = 'stop'
        return
      case w_ESEGUITA = "S"
        if not ("corrett" $ w_ERRORE or "success" $ w_ERRORE or empty(w_ERRORE))
          this.w_TMPC = Ah_msgformat("Procedura di conversione %1 eseguita con errori. Elaborazione interrotta!",this.w_CONVROUT)
          this.oParentObject.w_PESEOK = .F.
          this.oParentObject.w_pMSG = this.w_TMPC
          i_retcode = 'stop'
          return
        endif
    endcase
    * --- Prima nota
    this.w_ARCHIVIO = "PN"
    this.w_CODCLAS = "GESTFILE_PN"
    this.w_DESCLAS = "Conversione primanota GESTFILE (primanota)"
    this.w_NOMTAB = "PNT_MAST"
    this.w_DATRAG = "PNDATREG"
    this.w_CODATT = "SERIALE"
    this.w_DESATT = "SERIALE REGISTRAZIONE"
    this.w_CAMCUR = "PNSERIAL"
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Documenti
    this.w_ARCHIVIO = "DO"
    this.w_CODCLAS = "GESTFILE_DO"
    this.w_DESCLAS = "Conversione documenti GESTFILE (documenti)"
    this.w_NOMTAB = "DOC_MAST"
    this.w_DATRAG = "MVDATDOC"
    this.w_CODATT = "SERIALE"
    this.w_DESATT = "SERIALE DOCUMENTO"
    this.w_CAMCUR = "MVSERIAL"
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Articoli
    this.w_ARCHIVIO = "AR"
    this.w_CODCLAS = "GESTFILE_AR"
    this.w_DESCLAS = "Conversione articoli GESTFILE (articoli)"
    this.w_NOMTAB = "ART_ICOL"
    this.w_DATRAG = "UTDC"
    this.w_CODATT = "CODICE"
    this.w_DESATT = "CODICE ARTICOLO"
    this.w_CAMCUR = "ARCODART"
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Clienti
    this.w_ARCHIVIO = "CL"
    this.w_CODCLAS = "GESTFILE_CL"
    this.w_DESCLAS = "Conversione clienti GESTFILE (clienti)"
    this.w_NOMTAB = "CONTI"
    this.w_DATRAG = "UTDC"
    this.w_CODATT = "CODICE"
    this.w_DESATT = "CODICE CLIENTE"
    this.w_CAMCUR = "ANTIPCON+ANCODICE"
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Fornitori
    this.w_ARCHIVIO = "FR"
    this.w_CODCLAS = "GESTFILE_FR"
    this.w_DESCLAS = "Conversione fornitori GESTFILE (fornitori)"
    this.w_NOMTAB = "CONTI"
    this.w_DATRAG = "UTDC"
    this.w_CODATT = "CODICE"
    this.w_DESATT = "CODICE FORNITORE"
    this.w_CAMCUR = "ANTIPCON+ANCODICE"
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Categorie omogenee
    this.w_ARCHIVIO = "CO"
    this.w_CODCLAS = "GESTFILE_CO"
    this.w_DESCLAS = "Conversione categorie omogenee GESTFILE (cat.omo)"
    this.w_NOMTAB = "CATEGOMO"
    this.w_DATRAG = " "
    this.w_CODATT = "CODICE"
    this.w_DESATT = "CODICE CATEGORIA"
    this.w_CAMCUR = "OMCODICE"
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Famiglie
    this.w_ARCHIVIO = "FA"
    this.w_CODCLAS = "GESTFILE_FA"
    this.w_DESCLAS = "Conversione famiglie GESTFILE (famiglie)"
    this.w_NOMTAB = "FAM_ARTI"
    this.w_DATRAG = " "
    this.w_CODATT = "CODICE"
    this.w_DESATT = "CODICE FAMIGLIA"
    this.w_CAMCUR = "FACODICE"
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Marchi
    this.w_ARCHIVIO = "MA"
    this.w_CODCLAS = "GESTFILE_MA"
    this.w_DESCLAS = "Conversione marchi GESTFILE (marchi)"
    this.w_NOMTAB = "MARCHI"
    this.w_DATRAG = " "
    this.w_CODATT = "CODICE"
    this.w_DESATT = "CODICE MARCHIO"
    this.w_CAMCUR = "MACODICE"
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Gruppi merceologici
    this.w_ARCHIVIO = "GM"
    this.w_CODCLAS = "GESTFILE_GM"
    this.w_DESCLAS = "Conversione gruppi merceologici GESTFILE (gr. mer)"
    this.w_NOMTAB = "GRUMERC"
    this.w_DATRAG = " "
    this.w_CODATT = "CODICE"
    this.w_DESATT = "CODICE GRUPPO"
    this.w_CAMCUR = "GMCODICE"
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if UPPER( G_APPLICATION ) = "AD HOC ENTERPRISE"
      * --- Codici di ricerca
      this.w_ARCHIVIO = "CA"
      this.w_CODCLAS = "GESTFILE_KA"
      this.w_DESCLAS = "Conversione codici di ricerca GESTFILE (cod. ric)"
      this.w_NOMTAB = "KEY_ARTI"
      this.w_DATRAG = "UTDC"
      this.w_CODATT = "CODICE"
      this.w_DESATT = "CODICE DI RICERCA"
      this.w_CAMCUR = "CACODICE"
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.w_NHF>=0
      this.w_MSGLOG = iif(g_DOCM="S","classi documentali","classi librerie allegati")
      this.w_TMPC = Ah_msgformat("Creazione %1 completata con successo.",alltrim(this.w_MSGLOG))
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = ah_msgformat("Conversione eseguita correttamente")
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inserisco una nuova classe documentale
    this.w_PATH = " "
    this.w_INPATTIP = " "
    this.w_INPATCLA = " "
    * --- Verifico se gestita direttamente dall'utente
    * --- Read from LIB_IMMA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.LIB_IMMA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.LIB_IMMA_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "LIPATIMG,LIDATRIF,LITIPALL,LICLAALL,LIFL_TIP,LIFL_CLA"+;
        " from "+i_cTable+" LIB_IMMA where ";
            +"LICODICE = "+cp_ToStrODBC("GESTFILE");
            +" and LITIPARC = "+cp_ToStrODBC(this.w_ARCHIVIO);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        LIPATIMG,LIDATRIF,LITIPALL,LICLAALL,LIFL_TIP,LIFL_CLA;
        from (i_cTable) where;
            LICODICE = "GESTFILE";
            and LITIPARC = this.w_ARCHIVIO;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PATH = NVL(cp_ToDate(_read_.LIPATIMG),cp_NullValue(_read_.LIPATIMG))
      this.w_TIPRAGR = NVL(cp_ToDate(_read_.LIDATRIF),cp_NullValue(_read_.LIDATRIF))
      this.w_TIPOALL = NVL(cp_ToDate(_read_.LITIPALL),cp_NullValue(_read_.LITIPALL))
      this.w_CLASALL = NVL(cp_ToDate(_read_.LICLAALL),cp_NullValue(_read_.LICLAALL))
      this.w_INPATTIP = NVL(cp_ToDate(_read_.LIFL_TIP),cp_NullValue(_read_.LIFL_TIP))
      this.w_INPATCLA = NVL(cp_ToDate(_read_.LIFL_CLA),cp_NullValue(_read_.LIFL_CLA))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    do case
      case i_ROWS > 1
        Ah_ErrorMsg("Attenzione: Per l'archivio %1 sono presenti pi� percorsi di memorizzazione, verr� preso il primo incontrato",48,"",this.w_ARCHIVIO)
      case i_ROWS = 0
        * --- Assegno valori di default alle librerie non gestite
        this.w_DATRAG = " "
        this.w_PATH = SYS(5)+SYS(2003)+"\Default_Immagini"
        this.w_TIPRAGR = "N"
        this.w_TIPOALL = "ALTRO"
        this.w_CLASALL = "ALTRO"
    endcase
    * --- Verifico la valorizzazione dei campi relativi alla tipologia e classe allegato
    if empty(this.w_TIPOALL)
      this.w_TIPOALL = "ALTRO"
    endif
    if empty(this.w_CLASALL)
      this.w_CLASALL = "ALTRO"
    endif
    if empty(this.w_INPATTIP)
      this.w_INPATTIP = "N"
    endif
    if empty(this.w_INPATCLA)
      this.w_INPATCLA = "N"
    endif
    * --- Verifico  se non � stato assegnato un tipo di raggruppamento, in questo caso resetto il campo
    *     su cui raggruppare
    if this.w_TIPRAGR = "N"
      this.w_DATRAG = " "
    endif
    * --- Testata classe documentale
    * --- Try
    local bErr_046DDD50
    bErr_046DDD50=bTrsErr
    this.Try_046DDD50()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
      * --- Try
      local bErr_046EA890
      bErr_046EA890=bTrsErr
      this.Try_046EA890()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_046EA890
      * --- End
    endif
    bTrsErr=bTrsErr or bErr_046DDD50
    * --- End
    * --- Dettaglio classe documentale
    * --- Try
    local bErr_046F7040
    bErr_046F7040=bTrsErr
    this.Try_046F7040()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_046F7040
    * --- End
  endproc
  proc Try_046DDD50()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PROMCLAS
    i_nConn=i_TableProp[this.PROMCLAS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PROMCLAS_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PROMCLAS_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CDCODCLA"+",CDDESCLA"+",CDMOTARC"+",CDCLAAWE"+",CDPATSTD"+",CDTIPRAG"+",CDCAMRAG"+",CDRIFTAB"+",CDTIPARC"+",CDRIFDEF"+",CDTIPALL"+",CDCLAALL"+",CDCOMTIP"+",CDCOMCLA"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CODCLAS),'PROMCLAS','CDCODCLA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DESCLAS),'PROMCLAS','CDDESCLA');
      +","+cp_NullLink(cp_ToStrODBC("G"),'PROMCLAS','CDMOTARC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODCLAS),'PROMCLAS','CDCLAAWE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PATH),'PROMCLAS','CDPATSTD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TIPRAGR),'PROMCLAS','CDTIPRAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DATRAG),'PROMCLAS','CDCAMRAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NOMTAB),'PROMCLAS','CDRIFTAB');
      +","+cp_NullLink(cp_ToStrODBC("M"),'PROMCLAS','CDTIPARC');
      +","+cp_NullLink(cp_ToStrODBC("S"),'PROMCLAS','CDRIFDEF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TIPOALL),'PROMCLAS','CDTIPALL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CLASALL),'PROMCLAS','CDCLAALL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_INPATTIP),'PROMCLAS','CDCOMTIP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_INPATCLA),'PROMCLAS','CDCOMCLA');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CDCODCLA',this.w_CODCLAS,'CDDESCLA',this.w_DESCLAS,'CDMOTARC',"G",'CDCLAAWE',this.w_CODCLAS,'CDPATSTD',this.w_PATH,'CDTIPRAG',this.w_TIPRAGR,'CDCAMRAG',this.w_DATRAG,'CDRIFTAB',this.w_NOMTAB,'CDTIPARC',"M",'CDRIFDEF',"S",'CDTIPALL',this.w_TIPOALL,'CDCLAALL',this.w_CLASALL)
      insert into (i_cTable) (CDCODCLA,CDDESCLA,CDMOTARC,CDCLAAWE,CDPATSTD,CDTIPRAG,CDCAMRAG,CDRIFTAB,CDTIPARC,CDRIFDEF,CDTIPALL,CDCLAALL,CDCOMTIP,CDCOMCLA &i_ccchkf. );
         values (;
           this.w_CODCLAS;
           ,this.w_DESCLAS;
           ,"G";
           ,this.w_CODCLAS;
           ,this.w_PATH;
           ,this.w_TIPRAGR;
           ,this.w_DATRAG;
           ,this.w_NOMTAB;
           ,"M";
           ,"S";
           ,this.w_TIPOALL;
           ,this.w_CLASALL;
           ,this.w_INPATTIP;
           ,this.w_INPATCLA;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_046EA890()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Aggiorno la classe
    * --- Write into PROMCLAS
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PROMCLAS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PROMCLAS_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PROMCLAS_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"CDDESCLA ="+cp_NullLink(cp_ToStrODBC(this.w_DESCLAS),'PROMCLAS','CDDESCLA');
      +",CDMOTARC ="+cp_NullLink(cp_ToStrODBC("G"),'PROMCLAS','CDMOTARC');
      +",CDCLAAWE ="+cp_NullLink(cp_ToStrODBC(this.w_CODCLAS),'PROMCLAS','CDCLAAWE');
      +",CDPATSTD ="+cp_NullLink(cp_ToStrODBC(this.w_PATH),'PROMCLAS','CDPATSTD');
      +",CDTIPRAG ="+cp_NullLink(cp_ToStrODBC(this.w_TIPRAGR),'PROMCLAS','CDTIPRAG');
      +",CDCAMRAG ="+cp_NullLink(cp_ToStrODBC(this.w_DATRAG),'PROMCLAS','CDCAMRAG');
      +",CDRIFTAB ="+cp_NullLink(cp_ToStrODBC(this.w_NOMTAB),'PROMCLAS','CDRIFTAB');
      +",CDTIPARC ="+cp_NullLink(cp_ToStrODBC("M"),'PROMCLAS','CDTIPARC');
      +",CDRIFDEF ="+cp_NullLink(cp_ToStrODBC("S"),'PROMCLAS','CDRIFDEF');
      +",CDTIPALL ="+cp_NullLink(cp_ToStrODBC(this.w_TIPOALL),'PROMCLAS','CDTIPALL');
      +",CDCLAALL ="+cp_NullLink(cp_ToStrODBC(this.w_CLASALL),'PROMCLAS','CDCLAALL');
      +",CDCOMTIP ="+cp_NullLink(cp_ToStrODBC(this.w_INPATTIP),'PROMCLAS','CDCOMTIP');
      +",CDCOMCLA ="+cp_NullLink(cp_ToStrODBC(this.w_INPATCLA),'PROMCLAS','CDCOMCLA');
          +i_ccchkf ;
      +" where ";
          +"CDCODCLA = "+cp_ToStrODBC(this.w_CODCLAS);
             )
    else
      update (i_cTable) set;
          CDDESCLA = this.w_DESCLAS;
          ,CDMOTARC = "G";
          ,CDCLAAWE = this.w_CODCLAS;
          ,CDPATSTD = this.w_PATH;
          ,CDTIPRAG = this.w_TIPRAGR;
          ,CDCAMRAG = this.w_DATRAG;
          ,CDRIFTAB = this.w_NOMTAB;
          ,CDTIPARC = "M";
          ,CDRIFDEF = "S";
          ,CDTIPALL = this.w_TIPOALL;
          ,CDCLAALL = this.w_CLASALL;
          ,CDCOMTIP = this.w_INPATTIP;
          ,CDCOMCLA = this.w_INPATCLA;
          &i_ccchkf. ;
       where;
          CDCODCLA = this.w_CODCLAS;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_046F7040()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PRODCLAS
    i_nConn=i_TableProp[this.PRODCLAS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRODCLAS_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PRODCLAS_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CDCODCLA"+",CPROWNUM"+",CPROWORD"+",CDCODATT"+",CDDESATT"+",CDCHKOBB"+",CDTIPATT"+",CDVLPRED"+",CDCAMCUR"+",CDTABKEY"+",CDNATAWE"+",CDKEYAWE"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CODCLAS),'PRODCLAS','CDCODCLA');
      +","+cp_NullLink(cp_ToStrODBC(1),'PRODCLAS','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(10),'PRODCLAS','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODATT),'PRODCLAS','CDCODATT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DESATT),'PRODCLAS','CDDESATT');
      +","+cp_NullLink(cp_ToStrODBC("S"),'PRODCLAS','CDCHKOBB');
      +","+cp_NullLink(cp_ToStrODBC("C"),'PRODCLAS','CDTIPATT');
      +","+cp_NullLink(cp_ToStrODBC(" "),'PRODCLAS','CDVLPRED');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CAMCUR),'PRODCLAS','CDCAMCUR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NOMTAB),'PRODCLAS','CDTABKEY');
      +","+cp_NullLink(cp_ToStrODBC(" "),'PRODCLAS','CDNATAWE');
      +","+cp_NullLink(cp_ToStrODBC(" "),'PRODCLAS','CDKEYAWE');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CDCODCLA',this.w_CODCLAS,'CPROWNUM',1,'CPROWORD',10,'CDCODATT',this.w_CODATT,'CDDESATT',this.w_DESATT,'CDCHKOBB',"S",'CDTIPATT',"C",'CDVLPRED'," ",'CDCAMCUR',this.w_CAMCUR,'CDTABKEY',this.w_NOMTAB,'CDNATAWE'," ",'CDKEYAWE'," ")
      insert into (i_cTable) (CDCODCLA,CPROWNUM,CPROWORD,CDCODATT,CDDESATT,CDCHKOBB,CDTIPATT,CDVLPRED,CDCAMCUR,CDTABKEY,CDNATAWE,CDKEYAWE &i_ccchkf. );
         values (;
           this.w_CODCLAS;
           ,1;
           ,10;
           ,this.w_CODATT;
           ,this.w_DESATT;
           ,"S";
           ,"C";
           ," ";
           ,this.w_CAMCUR;
           ,this.w_NOMTAB;
           ," ";
           ," ";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,8)]
    this.cWorkTables[1]='LIB_IMMA'
    this.cWorkTables[2]='PROMCLAS'
    this.cWorkTables[3]='PRODCLAS'
    this.cWorkTables[4]='ART_ICOL'
    this.cWorkTables[5]='GESTFILE'
    this.cWorkTables[6]='CLA_ALLE'
    this.cWorkTables[7]='TIP_ALLE'
    this.cWorkTables[8]='CONVERSI'
    return(this.OpenAllTables(8))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
