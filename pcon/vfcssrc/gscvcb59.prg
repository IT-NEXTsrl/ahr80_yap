* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcb59                                                        *
*              Modifica tabella cpwarn per aziende con meno di 5 char          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-05-12                                                      *
* Last revis.: 2009-10-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcb59",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcb59 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_MESS = space(10)
  w_LOOP = 0
  w_WARNCODE = space(10)
  w_TABLECODE = space(50)
  w_CONTA = 0
  w_AZIENDA = space(5)
  * --- WorkFile variables
  cpwarn_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Try
    local bErr_03769E20
    bErr_03769E20=bTrsErr
    this.Try_03769E20()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = this.oParentObject.w_PMSG
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_03769E20
    * --- End
  endproc
  proc Try_03769E20()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Nel caso di azienda con meno di 5 caratteri di codice abbiamo problemi
    *     nella determinazione del progressivo
    * --- Recupero i progressivi dell'azienda corrente....
    this.w_AZIENDA = i_CODAZI
    this.w_CONTA = 0
    * --- Select from cpwarn
    i_nConn=i_TableProp[this.cpwarn_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.cpwarn_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" cpwarn ";
          +" where warncode= 'xxx'";
           ,"_Curs_cpwarn")
    else
      select * from (i_cTable);
       where warncode= "xxx";
        into cursor _Curs_cpwarn
    endif
    if used('_Curs_cpwarn')
      select _Curs_cpwarn
      locate for 1=1
      do while not(eof())
      this.w_TABLECODE = _Curs_cpwarn.tablecode
      if NOT EMPTY(SUBSTR(this.w_tablecode ,13,5)) AND SUBSTR(this.w_tablecode ,13,5)= i_CODAZI
        * --- Preparo le variabili per l'UPDATE
        this.w_WARNCODE = i_CODAZI
        * --- Analizzo il progressivo che � sempre:
        *     
        *     prog\<identificativo>\'<codice azienda>'\....
        *     per cui ricerco la seconda e la terza occorrenza del carattere \
        *     prendo quanto trovo a sinistra della seconda occorrenza, metto i_codazi 
        *     e recupero il pezzo che va dalla terza alla fine..
        this.w_TABLECODE = LEFT( this.w_tablecode, AT("\",this.w_tablecode,2))+"'"+ trim(i_CODAZI) +"'"+SUBSTR(this.w_TABLECODE,AT("\",this.W_tablecode,3),100)
        if Alltrim(this.w_TABLECODE)<>Alltrim(_Curs_cpwarn.tablecode)
          * --- Se il progressivo � gia presente lo devo prima cancellare...
          *     (ho due progressivi identici con la differenza degli spazi nel codice azienda...
          *     Mi darebbe errore la WRITE per chiave duplicata...
          * --- Delete from cpwarn
          i_nConn=i_TableProp[this.cpwarn_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.cpwarn_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"tablecode = "+cp_ToStrODBC(this.w_TABLECODE);
                   )
          else
            delete from (i_cTable) where;
                  tablecode = this.w_TABLECODE;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
        endif
        * --- Passo ad aggiornarlo...
        if Alltrim(this.w_TABLECODE)<>Alltrim(_Curs_cpwarn.tablecode) Or ( this.w_WARNCODE<>_Curs_cpwarn.warncode )
          * --- Write into cpwarn
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.cpwarn_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.cpwarn_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.cpwarn_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"warncode ="+cp_NullLink(cp_ToStrODBC(this.w_WARNCODE),'cpwarn','warncode');
            +",tablecode ="+cp_NullLink(cp_ToStrODBC(this.w_TABLECODE),'cpwarn','tablecode');
                +i_ccchkf ;
            +" where ";
                +"tablecode = "+cp_ToStrODBC(_Curs_cpwarn.tablecode);
                   )
          else
            update (i_cTable) set;
                warncode = this.w_WARNCODE;
                ,tablecode = this.w_TABLECODE;
                &i_ccchkf. ;
             where;
                tablecode = _Curs_cpwarn.tablecode;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          if Alltrim(this.w_TABLECODE)<>Alltrim(_Curs_cpwarn.tablecode) Or ( Alltrim(this.w_WARNCODE)<>Alltrim(_Curs_cpwarn.warncode) )
            this.w_CONTA = this.w_CONTA + 1
          endif
        endif
      endif
        select _Curs_cpwarn
        continue
      enddo
      use
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.w_TMPC = ah_Msgformat("Modifica tabella progressivi CPWARN eseguito correttamente su %1 progressivi" , Alltrim( Str( this.w_CONTA ) ))
    this.oParentObject.w_PMSG = this.w_TMPC
    if this.w_NHF>=0
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='cpwarn'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_cpwarn')
      use in _Curs_cpwarn
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
