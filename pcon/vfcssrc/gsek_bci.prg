* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsek_bci                                                        *
*              Verifica inventario precedente Euro kit                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_33]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-06-04                                                      *
* Last revis.: 2001-06-04                                                      *
*                                                                              *
* Implementa il check su INNUMPRE in GSMA_AIN                                  *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsek_bci",oParentObject)
return(i_retval)

define class tgsek_bci as StdBatch
  * --- Local variables
  w_MESS = space(254)
  * --- WorkFile variables
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Implementa il check su INNUMPRE in GSEK_AIN (Euro KIT)
    * --- Risultato da restituire all'esterno
    * --- Verifica che l'inventario precedente coincida con la fine dell'esercizio nel caso in cui gli esercizi siano differenti
    if not Empty(this.oParentObject.w_INNUMPRE) and this.oParentObject.w_INCODESE<>this.oParentObject.w_INESEPRE
      this.oParentObject.w_CHECK = this.oParentObject.w_PRDATINV=this.oParentObject.w_PRINIESE
      if NOT this.oParentObject.w_CHECK
        this.oParentObject.w_INNUMPRE = SPACE(6)
        this.oParentObject.w_PRDATINV = cp_CharToDate("  -  -  ")
        ah_ErrorMsg("La data dell'inventario precedente deve essere uguale alla data%0di apertura di quell'esercizio",,"")
        i_retcode = 'stop'
        return
      endif
    endif
    * --- Verifiche di congruenza nel caso di inventario precedente
    this.oParentObject.w_CHECK = (NVL(this.oParentObject.w_INCATOMO,SPACE(5))=NVL(this.oParentObject.w_PRCATOMO,SPACE(5)))
    this.oParentObject.w_CHECK = this.oParentObject.w_CHECK and (NVL(this.oParentObject.w_INGRUMER,SPACE(5))=NVL(this.oParentObject.w_PRGRUMER,SPACE(5)))
    this.oParentObject.w_CHECK = this.oParentObject.w_CHECK and (NVL(this.oParentObject.w_INCODFAM,SPACE(5))=NVL(this.oParentObject.w_PRCODFAM,SPACE(5)))
    this.oParentObject.w_CHECK = this.oParentObject.w_CHECK and (NVL(this.oParentObject.w_INCODMAG,SPACE(5))=NVL(this.oParentObject.w_PRCODMAG,SPACE(5)))
    * --- Check gi� esistente prima dell'introduzione del batch
    this.oParentObject.w_CHECK = Empty(this.oParentObject.w_INNUMPRE) Or ((this.oParentObject.w_INNUMPRE<>this.oParentObject.w_INNUMINV Or this.oParentObject.w_INCODESE<>this.oParentObject.w_INESEPRE) And this.oParentObject.w_PRSTAINV $ "SD" and this.oParentObject.w_CHECK)
    if NOT this.oParentObject.w_CHECK
      this.oParentObject.w_PRDATINV = cp_CharToDate("  -  -  ")
      this.oParentObject.w_INNUMPRE = SPACE(6)
      this.oParentObject.w_INESEPRE = SPACE(4)
      ah_ErrorMsg("Utilizzare lo zoom (F9) per una lista di inventari validi","!","")
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
