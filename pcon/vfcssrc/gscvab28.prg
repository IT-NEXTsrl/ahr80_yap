* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvab28                                                        *
*              Aggiorna tabella fatture raggruppate (rag_fatt)                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_73]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-02-27                                                      *
* Last revis.: 2006-12-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvab28",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvab28 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_MESS = space(100)
  * --- WorkFile variables
  DOC_DETT_idx=0
  FAT_RAGG_idx=0
  RAG_FATT_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questa procedura Aggiorna:
    *     La tabella Fatture Raggruppate - Collegametno Righe DDT
    *     La vecchia tabella FAT_RAGG � stata resa Obsoleta. I suoi dati devono essere riportati in RAG_FATT.
    *     Inoltre nella nuova tabella devono essere riempiti i campi FRQTAIMP e FRQTAIM1 che sono le quantit� di ogni singolo DDT
    *     che vengono riportate nella Fattura differita Raggruppata.
    * --- FIle di LOG
    * --- Try
    local bErr_04A95D90
    bErr_04A95D90=bTrsErr
    this.Try_04A95D90()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE GENERICO - impossibile aggiornare tabella fatture raggruppate")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_04A95D90
    * --- End
  endproc
  proc Try_04A95D90()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Prima di Inserire i nuovi record cancello tutto il contenuto della tabella.
    *     In questo modo la procedura di conversione porebbe essere ripetuta.
    * --- Delete from RAG_FATT
    i_nConn=i_TableProp[this.RAG_FATT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.RAG_FATT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"1 = "+cp_ToStrODBC(1);
             )
    else
      delete from (i_cTable) where;
            1 = 1;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Inserisco i Record in RAG_FATT prendendo quelli nella tabella obsoleta FAT_RAGG
    * --- Insert into RAG_FATT
    i_nConn=i_TableProp[this.RAG_FATT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.RAG_FATT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"gscvab28",this.RAG_FATT_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    this.oParentObject.w_PMSG = ah_Msgformat("Inserimento %1 record nella Tabella RAG_FATT eseguito correttamente", Alltrim(Str( i_Rows )) )
    * --- Nella tabella RAG_FATT aggiorno le quantit� evase dalla fattura raggruppata
    * --- Write into RAG_FATT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.RAG_FATT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.RAG_FATT_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="FRSERFAT,FRROWFAT,FRNUMFAT,FRSERDDT,FRROWDDT,FRNUMDDT"
      do vq_exec with 'GSC1AB28',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.RAG_FATT_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="RAG_FATT.FRSERFAT = _t2.FRSERFAT";
              +" and "+"RAG_FATT.FRROWFAT = _t2.FRROWFAT";
              +" and "+"RAG_FATT.FRNUMFAT = _t2.FRNUMFAT";
              +" and "+"RAG_FATT.FRSERDDT = _t2.FRSERDDT";
              +" and "+"RAG_FATT.FRROWDDT = _t2.FRROWDDT";
              +" and "+"RAG_FATT.FRNUMDDT = _t2.FRNUMDDT";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"FRQTAIMP = _t2.MVQTAMOV ";
          +",FRQTAIM1 = _t2.MVQTAUM1";
          +i_ccchkf;
          +" from "+i_cTable+" RAG_FATT, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="RAG_FATT.FRSERFAT = _t2.FRSERFAT";
              +" and "+"RAG_FATT.FRROWFAT = _t2.FRROWFAT";
              +" and "+"RAG_FATT.FRNUMFAT = _t2.FRNUMFAT";
              +" and "+"RAG_FATT.FRSERDDT = _t2.FRSERDDT";
              +" and "+"RAG_FATT.FRROWDDT = _t2.FRROWDDT";
              +" and "+"RAG_FATT.FRNUMDDT = _t2.FRNUMDDT";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" RAG_FATT, "+i_cQueryTable+" _t2 set ";
          +"RAG_FATT.FRQTAIMP = _t2.MVQTAMOV ";
          +",RAG_FATT.FRQTAIM1 = _t2.MVQTAUM1";
          +Iif(Empty(i_ccchkf),"",",RAG_FATT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="RAG_FATT.FRSERFAT = t2.FRSERFAT";
              +" and "+"RAG_FATT.FRROWFAT = t2.FRROWFAT";
              +" and "+"RAG_FATT.FRNUMFAT = t2.FRNUMFAT";
              +" and "+"RAG_FATT.FRSERDDT = t2.FRSERDDT";
              +" and "+"RAG_FATT.FRROWDDT = t2.FRROWDDT";
              +" and "+"RAG_FATT.FRNUMDDT = t2.FRNUMDDT";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" RAG_FATT set (";
          +"FRQTAIMP,";
          +"FRQTAIM1";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.MVQTAMOV ,";
          +"t2.MVQTAUM1";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="RAG_FATT.FRSERFAT = _t2.FRSERFAT";
              +" and "+"RAG_FATT.FRROWFAT = _t2.FRROWFAT";
              +" and "+"RAG_FATT.FRNUMFAT = _t2.FRNUMFAT";
              +" and "+"RAG_FATT.FRSERDDT = _t2.FRSERDDT";
              +" and "+"RAG_FATT.FRROWDDT = _t2.FRROWDDT";
              +" and "+"RAG_FATT.FRNUMDDT = _t2.FRNUMDDT";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" RAG_FATT set ";
          +"FRQTAIMP = _t2.MVQTAMOV ";
          +",FRQTAIM1 = _t2.MVQTAUM1";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".FRSERFAT = "+i_cQueryTable+".FRSERFAT";
              +" and "+i_cTable+".FRROWFAT = "+i_cQueryTable+".FRROWFAT";
              +" and "+i_cTable+".FRNUMFAT = "+i_cQueryTable+".FRNUMFAT";
              +" and "+i_cTable+".FRSERDDT = "+i_cQueryTable+".FRSERDDT";
              +" and "+i_cTable+".FRROWDDT = "+i_cQueryTable+".FRROWDDT";
              +" and "+i_cTable+".FRNUMDDT = "+i_cQueryTable+".FRNUMDDT";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"FRQTAIMP = (select MVQTAMOV  from "+i_cQueryTable+" where "+i_cWhere+")";
          +",FRQTAIM1 = (select MVQTAUM1 from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = this.oParentObject.w_PMSG +ah_Msgformat("%1Aggiornamento qta fatturata in fatture raggruppate eseguito correttamente su %1 record", Alltrim(Str( i_Rows )) )
    * --- commit
    cp_EndTrs(.t.)
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='DOC_DETT'
    this.cWorkTables[2]='FAT_RAGG'
    this.cWorkTables[3]='RAG_FATT'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
