* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcb10                                                        *
*              UNIFICAZIONE TABELLA REPARTI/RISORSE                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-01-14                                                      *
* Last revis.: 2008-07-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcb10",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcb10 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TMPC = space(10)
  w_TEMP = space(10)
  w_RECODICE = space(15)
  w_REDESCRI = space(40)
  * --- WorkFile variables
  TAB_REPA_idx=0
  RIS_ORSE_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Copia i dati dalla vecchia tabella TAB_REPA nella nuova tabella
    *     RIS_ORSE, procedura di convesrione associata 5.0-M1538
    * --- Try
    local bErr_03752990
    bErr_03752990=bTrsErr
    this.Try_03752990()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = this.oParentObject.w_PMSG
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_03752990
    * --- End
  endproc
  proc Try_03752990()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Select from TAB_REPA
    i_nConn=i_TableProp[this.TAB_REPA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TAB_REPA_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select RECODICE,REDESCRI  from "+i_cTable+" TAB_REPA ";
           ,"_Curs_TAB_REPA")
    else
      select RECODICE,REDESCRI from (i_cTable);
        into cursor _Curs_TAB_REPA
    endif
    if used('_Curs_TAB_REPA')
      select _Curs_TAB_REPA
      locate for 1=1
      do while not(eof())
      this.w_RECODICE = NVL(_Curs_TAB_REPA.RECODICE, " ")
      this.w_REDESCRI = NVL(_Curs_TAB_REPA.REDESCRI, " ")
      * --- Insert into RIS_ORSE
      i_nConn=i_TableProp[this.RIS_ORSE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.RIS_ORSE_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.RIS_ORSE_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"RLCODICE"+",RLDESCRI"+",RL__TIPO"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_RECODICE),'RIS_ORSE','RLCODICE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_REDESCRI),'RIS_ORSE','RLDESCRI');
        +","+cp_NullLink(cp_ToStrODBC("RE"),'RIS_ORSE','RL__TIPO');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'RLCODICE',this.w_RECODICE,'RLDESCRI',this.w_REDESCRI,'RL__TIPO',"RE")
        insert into (i_cTable) (RLCODICE,RLDESCRI,RL__TIPO &i_ccchkf. );
           values (;
             this.w_RECODICE;
             ,this.w_REDESCRI;
             ,"RE";
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
        select _Curs_TAB_REPA
        continue
      enddo
      use
    endif
    * --- Esecuzione ok
    * --- commit
    cp_EndTrs(.t.)
    this.oParentObject.w_PESEOK = .T.
    this.w_TMPC = ah_Msgformat("Dati tabella reparti copiati correttamente")
    this.oParentObject.w_PMSG = ah_Msgformat("Conversione eseguita correttamente")
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='TAB_REPA'
    this.cWorkTables[2]='RIS_ORSE'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_TAB_REPA')
      use in _Curs_TAB_REPA
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
