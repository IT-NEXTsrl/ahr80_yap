* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcb44                                                        *
*              Aggiornamento campo DRKEYULO tabella RILEVAZI                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_11]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-05-18                                                      *
* Last revis.: 2009-05-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcb44",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcb44 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_MIRROR = space(50)
  w_CMDSQL = space(254)
  w_RESULT = 0
  w_CONN = 0
  w_DBTYPE = space(50)
  w_TMPC = space(10)
  w_TEMP = space(10)
  w_ERRORFLAG = .f.
  w_DRKEYULO = space(50)
  * --- WorkFile variables
  RILEVAZI_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiornamento campo LPTELFAX tabella PRO_LOGP
    * --- FIle di LOG
    this.w_ERRORFLAG = .F.
    * --- Try
    local bErr_037A3A70
    bErr_037A3A70=bTrsErr
    this.Try_037A3A70()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = AH_MsgFormat("ERRORE GENERICO - %1", this.oParentObject.w_PMSG)
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_037A3A70
    * --- End
  endproc
  proc Try_037A3A70()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Select from gscvcb44
    do vq_exec with 'gscvcb44',this,'_Curs_gscvcb44','',.f.,.t.
    if used('_Curs_gscvcb44')
      select _Curs_gscvcb44
      locate for 1=1
      do while not(eof())
      this.w_DRKEYULO = iif(empty(nvl(_Curs_gscvcb44.DRUBICAZ ," ")),repl("#",20),_Curs_gscvcb44.DRUBICAZ )+iif(empty(nvl(_Curs_gscvcb44.DR_LOTTO," ")),repl("#",20),_Curs_gscvcb44.DR_LOTTO)
      * --- Write into RILEVAZI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.RILEVAZI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.RILEVAZI_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.RILEVAZI_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"DRKEYULO ="+cp_NullLink(cp_ToStrODBC(this.w_DRKEYULO),'RILEVAZI','DRKEYULO');
            +i_ccchkf ;
        +" where ";
            +"DRSERIAL = "+cp_ToStrODBC(_Curs_gscvcb44.DRSERIAL);
               )
      else
        update (i_cTable) set;
            DRKEYULO = this.w_DRKEYULO;
            &i_ccchkf. ;
         where;
            DRSERIAL = _Curs_gscvcb44.DRSERIAL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
        select _Curs_gscvcb44
        continue
      enddo
      use
    endif
    if this.w_NHF>=0
      this.w_TMPC = AH_MsgFormat("Aggiornamento tabella RILEVAZI completato con successo")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    * --- commit
    cp_EndTrs(.t.)
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = AH_MsgFormat("Conversione eseguita correttamente")
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='RILEVAZI'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_gscvcb44')
      use in _Curs_gscvcb44
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
