* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvab93                                                        *
*              Aggiornamento cptsrvr                                           *
*                                                                              *
*      Author: Nicola Gorlandi                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_361]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-10-18                                                      *
* Last revis.: 2001-10-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvab93",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvab93 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_NRES = 0
  * --- WorkFile variables
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Procedura di conversione per DB2 per aggiornare CPTSRVR impostando
    *     il tipo VARCHAR ai campi di tipo CHAR per eludere l'errore nella 
    *     cp_CreateSystemTables
    * --- FIle di LOG
    * --- Try
    local bErr_0361B900
    bErr_0361B900=bTrsErr
    this.Try_0361B900()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = this.oParentObject.w_PMSG
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_0361B900
    * --- End
    * --- Rimuovo temporaneo VFP..
    Use in Select("__cptsrvr__")
  endproc
  proc Try_0361B900()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Salvo il contenuto della tabella cptsrvr in un cursore VFP
    *     Non ho la tabella in analisi non posso passarla lato Server
    cp_Sql(i_ServerConn[1,2],"select * from cptsrvr","__cptsrvr__")
    * --- Distruggo la tabella sul DB
    this.w_NRES = Sqlexec(i_ServerConn[1,2],"drop table cptsrvr")
    if this.w_NRES=-1
      * --- Raise
      i_Error=MESSAGE()
      return
    else
      * --- Ricostruisco la tabella sul DB
      *     (rimuovo l'occrenza di cptsrvr per simulare la creazione - inganno cp_existtable)
       
 i_existenttbls=Strtran( i_existenttbls,"cptsrvr" , "" ) 
 cp_CreateSystemTables(i_ServerConn[1,2],i_ServerConn[1,6])
      * --- La ripopolo con i dati in VFP precedentemente copiati..
      Select ("__cptsrvr__") 
 Go Top
      do while Not Eof( "__cptsrvr__" )
        this.w_NRES = cp_SQL(i_ServerConn[1,2],"insert into cptsrvr (servername,ServerDesc,ODBCDataSource,DatabaseType,WhenConn,PostIT) values ("+cp_ToStr(Nvl(__cptsrvr__.servername,""))+","+cp_ToStr(Nvl(__cptsrvr__.ServerDesc,""))+","+cp_ToStr(Nvl(__cptsrvr__.ODBCDataSource,""))+","+cp_ToStr(Nvl(__cptsrvr__.DatabaseType,""))+","+cp_ToStr(Nvl(__cptsrvr__.WhenConn,0))+","+cp_ToStr(Nvl(__cptsrvr__.PostIT,"N"))+")")
        if this.w_NRES=-1
          * --- Raise
          i_Error=MESSAGE()
          return
        endif
        Select ("__cptsrvr__") 
 Skip
      enddo
    endif
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Adeguamento tabella di sistema CPTSRVR%0Eseguito correttamente%0")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = this.w_TMPC
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
