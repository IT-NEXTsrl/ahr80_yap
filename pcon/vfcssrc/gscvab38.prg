* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvab38                                                        *
*              Eliminazione output utente non utilizzati                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_9]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-07-28                                                      *
* Last revis.: 2008-07-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvab38",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvab38 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_CODE = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_OK = .f.
  * --- WorkFile variables
  OUT_PUTS_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elimina gli Output Utente non pi� utilizzati:
    *     GSVE_SBD - Stampa Sintetica Doc. da contabilizzare
    *     GSAR_SCL
    *     GSAR_SFR
    this.w_OK = .T.
    * --- Try
    local bErr_038BA6C8
    bErr_038BA6C8=bTrsErr
    this.Try_038BA6C8()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      this.w_OK = .F.
    endif
    bTrsErr=bTrsErr or bErr_038BA6C8
    * --- End
    if this.w_OK
      * --- Esecuzione ok
      this.oParentObject.w_PESEOK = .T.
      this.w_TMPC = ah_Msgformat("Eliminazione eseguita con successo")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      this.oParentObject.w_PMSG = this.w_TMPC
    else
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE GENERICO - impossibile eseguire l'eliminazione richiesta")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
  endproc
  proc Try_038BA6C8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Elimino GSVE_SBD - Stampa Sintetica Doc. da contabilizzare
    * --- Delete from OUT_PUTS
    i_nConn=i_TableProp[this.OUT_PUTS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OUT_PUTS_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"OUNOMQUE = "+cp_ToStrODBC("QUERY\GSVE2SBD.VQR");
            +" and OUNOMREP = "+cp_ToStrODBC("QUERY\GSVE2SBD.FRX");
             )
    else
      delete from (i_cTable) where;
            OUNOMQUE = "QUERY\GSVE2SBD.VQR";
            and OUNOMREP = "QUERY\GSVE2SBD.FRX";

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Elimino GSAR_SCL/GSAR_SFR
    * --- Delete from OUT_PUTS
    i_nConn=i_TableProp[this.OUT_PUTS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OUT_PUTS_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      local i_cQueryTable,i_cWhere
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_cWhere=i_cTable+".OUNOMPRG = "+i_cQueryTable+".OUNOMPRG";
            +" and "+i_cTable+".OUROWNUM = "+i_cQueryTable+".OUROWNUM";
    
      do vq_exec with '..\PCON\EXE\QUERY\GSCVAB38',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
            +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='OUT_PUTS'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
