* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscv_bdc                                                        *
*              DROP CONSTRAINT SQLServer                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-12-15                                                      *
* Last revis.: 2010-06-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pNomeazi,pTabella,pColonna
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscv_bdc",oParentObject,m.pNomeazi,m.pTabella,m.pColonna)
return(i_retval)

define class tgscv_bdc as StdBatch
  * --- Local variables
  pNomeazi = space(5)
  pTabella = space(10)
  pColonna = space(10)
  w_NOMEAZI = space(5)
  w_TABELLA = space(10)
  w_COLONNA = space(10)
  w_FRASE = space(1)
  w_iNConn = 0
  w_IDXTABLE = 0
  w_PHNAME = space(50)
  * --- WorkFile variables
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questa procedura serve per individuare ed eliminare i constraint di una tabella
    if UPPER(CP_DBTYPE)="SQLSERVER"
      * --- Controllo i parametri passati
      this.w_NOMEAZI = Alltrim(IIF(TYPE("this.pNOMEAZI")<>"C", i_CODAZI , this.pNOMEAZI))
      this.w_TABELLA = Alltrim(IIF(TYPE("this.pTABELLA")<>"C", "" , alltrim(this.pTABELLA)))
      this.w_IDXTABLE = cp_OpenTable( Alltrim(this.pTABELLA) ,.T.)
      this.w_PHNAME = cp_SetAzi(i_TableProp[ this.w_IDXTABLE ,2]) 
      if alltrim(this.w_PHNAME)=alltrim(this.pTABELLA)
        * --- la tabella non ha il prefisso aziendale
      else
        this.w_TABELLA = Alltrim(IIF(TYPE("this.pTABELLA")<>"C", "" , this.w_NOMEAZI+alltrim(this.pTABELLA)))
      endif
      this.w_COLONNA = Alltrim(IIF(TYPE("this.pCOLONNA")<>"C", "" , alltrim(this.pCOLONNA)))
      * --- Aggiungo all'XDC il nome della tabella di sistema SYSOBJECTS
      i_nIdx=cp_AddTableDef("SYSOBJECTS") && aggiunge la definizione nella lista delle tabelle
      i_TableProp[i_nIdx,2] = "SYSOBJECTS" && cambio il nome della tabella non � una tabella temporanea
      i_TableProp[i_nIdx,4]=1
      * --- Aggiungo all'XDC il nome della tabella di sistema SYSCOLUMNS
      i_nIdx=cp_AddTableDef("SYSCOLUMNS") && aggiunge la definizione nella lista delle tabelle
      i_TableProp[i_nIdx,2] = "SYSCOLUMNS" && cambio il nome della tabella non � una tabella temporanea
      i_TableProp[i_nIdx,4]=1
      * --- Se esiste la devo filtrare anche per l'azienda xxx
      * --- Select from GSCV_BDC
      do vq_exec with 'GSCV_BDC',this,'_Curs_GSCV_BDC','',.f.,.t.
      if used('_Curs_GSCV_BDC')
        select _Curs_GSCV_BDC
        locate for 1=1
        do while not(eof())
        * --- Try
        local bErr_03018B08
        bErr_03018B08=bTrsErr
        this.Try_03018B08()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_03018B08
        * --- End
          select _Curs_GSCV_BDC
          continue
        enddo
        use
      endif
      cp_RemoveTableDef("SYSOBJECTS")
       
 cp_RemoveTableDef("SYSCOLUMNS") 
 cp_CloseTable( Alltrim(this.pTABELLA)) 
 cp_CloseTable("SYSCOLUMNS") 
 cp_CloseTable("SYSOBJECTS")
    endif
  endproc
  proc Try_03018B08()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.w_FRASE = ALLTRIM(_Curs_GSCV_BDC.FRASE)
    * --- Eseguo solo se il campo passato come parametro  ha un effettivo costraint o se devo togliere tutti i constraint
    this.w_COLONNA = left(this.w_COLONNA,7) 
    if this.w_COLONNA$this.w_FRASE OR EMPTY(this.w_COLONNA)
      this.w_iNConn = i_ServerConn[1,2] && i_TableProp[ i_nIdx ,3]
      w_iRows = CP_SQLEXEC(this.w_iNConn, rtrim(this.w_FRASE))
    endif
    return


  proc Init(oParentObject,pNomeazi,pTabella,pColonna)
    this.pNomeazi=pNomeazi
    this.pTabella=pTabella
    this.pColonna=pColonna
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  proc CloseCursors()
    if used('_Curs_GSCV_BDC')
      use in _Curs_GSCV_BDC
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pNomeazi,pTabella,pColonna"
endproc
