* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcb69                                                        *
*              Allargamento chiave tabelle lis_tini e lis_scag                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_239]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-07-12                                                      *
* Last revis.: 2011-01-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcb69",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcb69 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_TROVATA = .f.
  w_CodRel = space(15)
  * --- WorkFile variables
  LIS_SCAG_idx=0
  TMP_LISTINI_idx=0
  LIS_TINI_idx=0
  TMP_LISCAG_idx=0
  CONVERSI_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- FIle di LOG
    this.w_CodRel = "7.0-E9360"
    * --- Try
    local bErr_035FA000
    bErr_035FA000=bTrsErr
    this.Try_035FA000()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = this.oParentObject.w_PMSG
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_035FA000
    * --- End
    * --- 
  endproc
  proc Try_035FA000()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Legge se eseguita la conversione  7.0-E9360
    * --- Read from CONVERSI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CONVERSI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONVERSI_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "*"+;
        " from "+i_cTable+" CONVERSI where ";
            +"COCODREL = "+cp_ToStrODBC(this.w_CodRel);
            +" and COESEGUI = "+cp_ToStrODBC("S");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        *;
        from (i_cTable) where;
            COCODREL = this.w_CodRel;
            and COESEGUI = "S";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_TROVATA = i_Rows>0
    * --- begin transaction
    cp_BeginTrs()
    * --- Esecuzione ok
    if upper(CP_DBTYPE)="DB2" AND NOT this.w_TROVATA
      * --- Create temporary table TMP_LISCAG
      i_nIdx=cp_AddTableDef('TMP_LISCAG') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      i_nConn=i_TableProp[this.LIS_SCAG_idx,3] && recupera la connessione
      i_cTable=cp_SetAzi(i_TableProp[this.LIS_SCAG_idx,2])
      cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
            )
      this.TMP_LISCAG_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      * --- Create temporary table TMP_LISTINI
      i_nIdx=cp_AddTableDef('TMP_LISTINI') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      i_nConn=i_TableProp[this.LIS_TINI_idx,3] && recupera la connessione
      i_cTable=cp_SetAzi(i_TableProp[this.LIS_TINI_idx,2])
      cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
            )
      this.TMP_LISTINI_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      ah_Msg("Create tabelle temporanee di appoggio",.T.)
      * --- Elimino la tabella
      if Not GSCV_BDT( this, "LIS_SCAG" , i_nConn )
        * --- Raise
        i_Error="Error"
        return
      endif
      if Not GSCV_BDT( this, "LIS_TINI" , i_nConn )
        * --- Raise
        i_Error="Error"
        return
      endif
      * --- Ricostruisco il database per la tabella
      GSCV_BRT(this, "LIS_TINI" , i_nConn , .T.)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      GSCV_BRT(this, "LIS_SCAG" , i_nConn , .T.)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Insert into LIS_TINI
      i_nConn=i_TableProp[this.LIS_TINI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.LIS_TINI_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_cTempTable=cp_SetAzi(i_TableProp[this.TMP_LISTINI_idx,2])
        i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.LIS_TINI_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Insert into LIS_SCAG
      i_nConn=i_TableProp[this.LIS_SCAG_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.LIS_SCAG_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_cTempTable=cp_SetAzi(i_TableProp[this.TMP_LISCAG_idx,2])
        i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.LIS_SCAG_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      ah_Msg("Ripristinati valori di origine",.T.)
      * --- Elimina tabella TMP
      * --- Drop temporary table TMP_LISCAG
      i_nIdx=cp_GetTableDefIdx('TMP_LISCAG')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('TMP_LISCAG')
      endif
      * --- Drop temporary table TMP_LISTINI
      i_nIdx=cp_GetTableDefIdx('TMP_LISTINI')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('TMP_LISTINI')
      endif
      ah_Msg("Eliminate tabelle temporanee",.T.)
    endif
    * --- commit
    cp_EndTrs(.t.)
    this.oParentObject.w_PESEOK = .T.
    if this.w_NHF>=0
      if upper(CP_DBTYPE)="DB2"
        this.w_TMPC = ah_Msgformat("Adeguamento chiavi primarie tabelle LIS_TINI e LIS_SCAG eseguito correttamente%0ATTENZIONE: occorre andare in MANUTENZIONE DATABASE, selezionare le tabelle%0LIS_TINI e LIS_SCAG e premere <OK>")
      else
        this.w_TMPC = ah_Msgformat("Procedura eseguita correttamente")
      endif
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='LIS_SCAG'
    this.cWorkTables[2]='*TMP_LISTINI'
    this.cWorkTables[3]='LIS_TINI'
    this.cWorkTables[4]='*TMP_LISCAG'
    this.cWorkTables[5]='CONVERSI'
    return(this.OpenAllTables(5))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
