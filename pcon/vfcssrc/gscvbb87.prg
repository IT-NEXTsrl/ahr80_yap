* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvbb87                                                        *
*              Aggiornamento anagrafica cap                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-05-12                                                      *
* Last revis.: 2009-02-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvbb87",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvbb87 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_OK = .f.
  w_CPCODCAP = space(9)
  w_CPCODLOC = space(10)
  w_CPDESLOC = space(50)
  w_CPDESFRA = space(50)
  w_CPDESIND = space(154)
  w_CPCODPRO = space(2)
  w_CPDESPRO = space(30)
  w_CPUFFGIU = space(1)
  w_CPCODFIS = space(4)
  * --- WorkFile variables
  ANAG_CAP_idx=0
  RIPATMP1_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_CPCODCAP = SPACE(9)
    this.w_CPCODLOC = SPACE(10)
    this.w_CPDESLOC = SPACE(50)
    this.w_CPDESFRA = SPACE(50)
    this.w_CPDESIND = SPACE(154)
    this.w_CPCODPRO = SPACE(2)
    this.w_CPDESPRO = SPACE(30)
    this.w_CPUFFGIU = SPACE(1)
    this.w_CPCODFIS = SPACE(4)
    * --- Try
    local bErr_047112C0
    bErr_047112C0=bTrsErr
    this.Try_047112C0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = this.oParentObject.w_PMSG
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_047112C0
    * --- End
  endproc
  proc Try_047112C0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if Not Isalt()
      i_nConn=i_TableProp[this.ANAG_CAP_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.ANAG_CAP_idx,2])
      if i_nConn<>0
        pName="ANAG_CAP"
        pDatabaseType=i_ServerConn[1,6]
        * --- Create temporary table RIPATMP1
        i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        i_nConn=i_TableProp[this.ANAG_CAP_idx,3] && recupera la connessione
        i_cTable=cp_SetAzi(i_TableProp[this.ANAG_CAP_idx,2])
        cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
              )
        this.RIPATMP1_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        ah_Msg("Creata tabella temporanea di appoggio",.T.)
        * --- Elimino la tabella
        do case
          case g_APPLICATION = "ADHOC REVOLUTION" 
            if Not GSCV2BDT( this, "ANAG_CAP" , i_nConn )
              * --- Raise
              i_Error="Error"
              return
            endif
            * --- Ricostruisco il database per la tabella
            GSCV_BRT(this, "ANAG_CAP" , i_nConn , .T.)
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          case g_APPLICATION = "ad hoc ENTERPRISE"
            * --- rimuove la tabella
            =cp_ReadXdc()
            pName="ANAG_CAP"
            pDatabaseType=i_ServerConn[1,6]
            p=i_dcx.GetPhTable(i_dcx.GetTableIdx(pName),"")
            * --- Eseguo direttamente la Drop avvalendomi del fatto che DB2 elimina eventuali
            *     Link presenti sulla tabella.
            if cp_SQL(i_ServerConn[1,2],"drop table "+p) < 0
              cMsg="Errore cancellazione tabella %1"
              AH_ERRORMSG(cMsg,48,,p)
              * --- Raise
              i_Error=cMsg
              return
            else
              * --- Toglie dalla stringa delle tabelle aperte la tabella cancellata
              * --- tabella origine
              if at(p,i_existenttbls)<>0
                cTmp=substr(i_existenttbls,at(p,i_existenttbls)-1)
                if rat("|",cTmp)<>1
                  cTmp=left(cTmp,at("|",cTmp,2)-1)
                endif
                i_existenttbls=strtran(i_existenttbls,cTmp,"")
              endif
            endif
            ah_msg( "Cancellata tabella di origine" )
            cTable=pName
            for i=1 to i_dcx.GetTablesCount()
            n=i_dcx.GetTable(i)
            if alltrim(n)=alltrim(cTable) and len(n)=len(cTable)
              cPhTable=i_dcx.GetPhTable(i,"")
            endif
            next
            * --- Costruzione tabella origine
            cMsg=""
            public msgwnd 
 msgwnd=createobject("mkdbmsg") 
 msgwnd.show()
            ah_msg( "Ricostruisco tabella di origine" )
            if NOT cp_MakeTable(cTable,cPhTable,i_nConn,i_dcx, pDatabaseType, "")
              cMsg=MSG_REBUILDING_TABLE
              AH_ERRORMSG(cMsg)
              * --- Raise
              i_Error=cMsg
              return
            else
              msgwnd.End("Ricostruzione tabella di origine completata")
              * --- Costruzione indici tabella origine
              ah_msg( "Ricostruisco indici tabella di origine" )
              if NOT cp_MakeIndexes(cTable,cPhTable,i_nConn,i_dcx, pDatabaseType, "")
                cMsg=MSG_INDEXES
                AH_ERRORMSG(cMsg)
                * --- Raise
                i_Error=cMsg
                return
              else
                msgwnd.End("Ricostruzione indici tabella di origine completata")
              endif
            endif
            ah_msg( "Ricostruzione tabella completata" )
        endcase
        * --- Select from RIPATMP1
        i_nConn=i_TableProp[this.RIPATMP1_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" RIPATMP1 ";
               ,"_Curs_RIPATMP1")
        else
          select * from (i_cTable);
            into cursor _Curs_RIPATMP1
        endif
        if used('_Curs_RIPATMP1')
          select _Curs_RIPATMP1
          locate for 1=1
          do while not(eof())
          this.w_CPCODCAP = _Curs_RIPATMP1.CPCODCAP
          this.w_CPCODLOC = SPACE(10)
          i_Conn=i_TableProp[this.ANAG_CAP_IDX, 3]
          cp_NextTableProg(this, i_Conn, "NUCAP", "w_CPCODLOC")
          this.w_CPDESLOC = _Curs_RIPATMP1.CPDESLOC
          this.w_CPCODPRO = _Curs_RIPATMP1.CPCODPRO
          this.w_CPDESPRO = _Curs_RIPATMP1.CPDESPRO
          this.w_CPUFFGIU = _Curs_RIPATMP1.CPUFFGIU
          this.w_CPCODFIS = _Curs_RIPATMP1.CPCODFIS
          * --- Insert into ANAG_CAP
          i_nConn=i_TableProp[this.ANAG_CAP_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ANAG_CAP_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ANAG_CAP_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"CPCODCAP"+",CPCODLOC"+",CPDESLOC"+",CPDESFRA"+",CPDESIND"+",CPDESPRO"+",CPCODPRO"+",CPUFFGIU"+",CPCODFIS"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_CPCODCAP),'ANAG_CAP','CPCODCAP');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CPCODLOC),'ANAG_CAP','CPCODLOC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CPDESLOC),'ANAG_CAP','CPDESLOC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CPDESFRA),'ANAG_CAP','CPDESFRA');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CPDESIND),'ANAG_CAP','CPDESIND');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CPDESPRO),'ANAG_CAP','CPDESPRO');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CPCODPRO),'ANAG_CAP','CPCODPRO');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CPUFFGIU),'ANAG_CAP','CPUFFGIU');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CPCODFIS),'ANAG_CAP','CPCODFIS');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'CPCODCAP',this.w_CPCODCAP,'CPCODLOC',this.w_CPCODLOC,'CPDESLOC',this.w_CPDESLOC,'CPDESFRA',this.w_CPDESFRA,'CPDESIND',this.w_CPDESIND,'CPDESPRO',this.w_CPDESPRO,'CPCODPRO',this.w_CPCODPRO,'CPUFFGIU',this.w_CPUFFGIU,'CPCODFIS',this.w_CPCODFIS)
            insert into (i_cTable) (CPCODCAP,CPCODLOC,CPDESLOC,CPDESFRA,CPDESIND,CPDESPRO,CPCODPRO,CPUFFGIU,CPCODFIS &i_ccchkf. );
               values (;
                 this.w_CPCODCAP;
                 ,this.w_CPCODLOC;
                 ,this.w_CPDESLOC;
                 ,this.w_CPDESFRA;
                 ,this.w_CPDESIND;
                 ,this.w_CPDESPRO;
                 ,this.w_CPCODPRO;
                 ,this.w_CPUFFGIU;
                 ,this.w_CPCODFIS;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
            select _Curs_RIPATMP1
            continue
          enddo
          use
        endif
        * --- Elimina tabella TMP
        * --- Drop temporary table RIPATMP1
        i_nIdx=cp_GetTableDefIdx('RIPATMP1')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('RIPATMP1')
        endif
        ah_Msg("Eliminata tabella temporanea tmp",.T.)
        if bTrsErr
          * --- Raise
          i_Error=MESSAGE()
          return
        else
          if this.w_NHF>=0
            this.w_TMPC = ah_Msgformat("Elaborazione terminata con successo")
            this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
          endif
        endif
      endif
    else
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("Elaborazione terminata con successo")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = ah_Msgformat("Elaborazione terminata con successo")
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='ANAG_CAP'
    this.cWorkTables[2]='*RIPATMP1'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_RIPATMP1')
      use in _Curs_RIPATMP1
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
