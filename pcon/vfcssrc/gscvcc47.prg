* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcc47                                                        *
*              Inizializzazione tabella Distinte alternative ART_DIST          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-10-30                                                      *
* Last revis.: 2014-11-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcc47",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcc47 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  * --- WorkFile variables
  ART_DIST_idx=0
  CCF_MAST_idx=0
  CCF_DETT_idx=0
  PAR_PROD_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- 8.0 - Inizializzazione tabella Distinte alternative ART_DIST e CLassi codice di fase CCF_MAST e CCF_DETT
    * --- FIle di LOG
    * --- Try
    local bErr_03939EE8
    bErr_03939EE8=bTrsErr
    this.Try_03939EE8()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("Errore generico - impossibile eseguire aggiornamento richiesto")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_03939EE8
    * --- End
  endproc
  proc Try_03939EE8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    ah_msg("Aggiornamento flag preferenziale in corso...." )
    * --- Rendo le (eventuali) righe gi� presenti nella tabella non preferenziali
    * --- Write into ART_DIST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.ART_DIST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_DIST_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.ART_DIST_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"DIFLPREF ="+cp_NullLink(cp_ToStrODBC("N"),'ART_DIST','DIFLPREF');
          +i_ccchkf ;
          +" where 1=1")
    else
      update (i_cTable) set;
          DIFLPREF = "N";
          &i_ccchkf. ;
          where 1=1

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    ah_msg("Inizializzazione ART_DIST in corso..." )
    * --- Insert into ART_DIST
    i_nConn=i_TableProp[this.ART_DIST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_DIST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"gscvcc47",this.ART_DIST_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_msg("Inizializzazione CCF_MAST in corso..." )
    * --- Inizializzo CCF_MAST e CCF_DETT (Classe codice di fase)
    * --- Insert into CCF_MAST
    i_nConn=i_TableProp[this.CCF_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CCF_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CCF_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CFCODICE"+",CFDESCRI"+",CFINCREM"+",CFSEPARA"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC("DEFA"),'CCF_MAST','CFCODICE');
      +","+cp_NullLink(cp_ToStrODBC("Default"),'CCF_MAST','CFDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(0),'CCF_MAST','CFINCREM');
      +","+cp_NullLink(cp_ToStrODBC("."),'CCF_MAST','CFSEPARA');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CFCODICE',"DEFA",'CFDESCRI',"Default",'CFINCREM',0,'CFSEPARA',".")
      insert into (i_cTable) (CFCODICE,CFDESCRI,CFINCREM,CFSEPARA &i_ccchkf. );
         values (;
           "DEFA";
           ,"Default";
           ,0;
           ,".";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_msg("Inizializzazione CCF_DETT in corso..." )
    * --- Insert into CCF_DETT
    i_nConn=i_TableProp[this.CCF_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CCF_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CCF_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CFCODICE"+",CPROWNUM"+",CFETICHE"+",CFPOSIZI"+",CFLUNGHE"+",CF_START"+",CF__FUNC"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC("DEFA"),'CCF_DETT','CFCODICE');
      +","+cp_NullLink(cp_ToStrODBC(1),'CCF_DETT','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC("Articolo"),'CCF_DETT','CFETICHE');
      +","+cp_NullLink(cp_ToStrODBC(10),'CCF_DETT','CFPOSIZI');
      +","+cp_NullLink(cp_ToStrODBC(20),'CCF_DETT','CFLUNGHE');
      +","+cp_NullLink(cp_ToStrODBC(" "),'CCF_DETT','CF_START');
      +","+cp_NullLink(cp_ToStrODBC("A"),'CCF_DETT','CF__FUNC');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CFCODICE',"DEFA",'CPROWNUM',1,'CFETICHE',"Articolo",'CFPOSIZI',10,'CFLUNGHE',20,'CF_START'," ",'CF__FUNC',"A")
      insert into (i_cTable) (CFCODICE,CPROWNUM,CFETICHE,CFPOSIZI,CFLUNGHE,CF_START,CF__FUNC &i_ccchkf. );
         values (;
           "DEFA";
           ,1;
           ,"Articolo";
           ,10;
           ,20;
           ," ";
           ,"A";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Insert into CCF_DETT
    i_nConn=i_TableProp[this.CCF_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CCF_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CCF_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CFCODICE"+",CPROWNUM"+",CFETICHE"+",CFPOSIZI"+",CFLUNGHE"+",CF_START"+",CF__FUNC"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC("DEFA"),'CCF_DETT','CFCODICE');
      +","+cp_NullLink(cp_ToStrODBC(2),'CCF_DETT','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC("Distinta base"),'CCF_DETT','CFETICHE');
      +","+cp_NullLink(cp_ToStrODBC(20),'CCF_DETT','CFPOSIZI');
      +","+cp_NullLink(cp_ToStrODBC(20),'CCF_DETT','CFLUNGHE');
      +","+cp_NullLink(cp_ToStrODBC(" "),'CCF_DETT','CF_START');
      +","+cp_NullLink(cp_ToStrODBC("D"),'CCF_DETT','CF__FUNC');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CFCODICE',"DEFA",'CPROWNUM',2,'CFETICHE',"Distinta base",'CFPOSIZI',20,'CFLUNGHE',20,'CF_START'," ",'CF__FUNC',"D")
      insert into (i_cTable) (CFCODICE,CPROWNUM,CFETICHE,CFPOSIZI,CFLUNGHE,CF_START,CF__FUNC &i_ccchkf. );
         values (;
           "DEFA";
           ,2;
           ,"Distinta base";
           ,20;
           ,20;
           ," ";
           ,"D";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Insert into CCF_DETT
    i_nConn=i_TableProp[this.CCF_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CCF_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CCF_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CFCODICE"+",CPROWNUM"+",CFETICHE"+",CFPOSIZI"+",CFLUNGHE"+",CF_START"+",CF__FUNC"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC("DEFA"),'CCF_DETT','CFCODICE');
      +","+cp_NullLink(cp_ToStrODBC(3),'CCF_DETT','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC("Codice ciclo"),'CCF_DETT','CFETICHE');
      +","+cp_NullLink(cp_ToStrODBC(30),'CCF_DETT','CFPOSIZI');
      +","+cp_NullLink(cp_ToStrODBC(20),'CCF_DETT','CFLUNGHE');
      +","+cp_NullLink(cp_ToStrODBC(" "),'CCF_DETT','CF_START');
      +","+cp_NullLink(cp_ToStrODBC("C"),'CCF_DETT','CF__FUNC');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CFCODICE',"DEFA",'CPROWNUM',3,'CFETICHE',"Codice ciclo",'CFPOSIZI',30,'CFLUNGHE',20,'CF_START'," ",'CF__FUNC',"C")
      insert into (i_cTable) (CFCODICE,CPROWNUM,CFETICHE,CFPOSIZI,CFLUNGHE,CF_START,CF__FUNC &i_ccchkf. );
         values (;
           "DEFA";
           ,3;
           ,"Codice ciclo";
           ,30;
           ,20;
           ," ";
           ,"C";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Insert into CCF_DETT
    i_nConn=i_TableProp[this.CCF_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CCF_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CCF_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CFCODICE"+",CPROWNUM"+",CFETICHE"+",CFPOSIZI"+",CFLUNGHE"+",CF_START"+",CF__FUNC"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC("DEFA"),'CCF_DETT','CFCODICE');
      +","+cp_NullLink(cp_ToStrODBC(4),'CCF_DETT','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC("Fase"),'CCF_DETT','CFETICHE');
      +","+cp_NullLink(cp_ToStrODBC(40),'CCF_DETT','CFPOSIZI');
      +","+cp_NullLink(cp_ToStrODBC(3),'CCF_DETT','CFLUNGHE');
      +","+cp_NullLink(cp_ToStrODBC(" "),'CCF_DETT','CF_START');
      +","+cp_NullLink(cp_ToStrODBC("F"),'CCF_DETT','CF__FUNC');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CFCODICE',"DEFA",'CPROWNUM',4,'CFETICHE',"Fase",'CFPOSIZI',40,'CFLUNGHE',3,'CF_START'," ",'CF__FUNC',"F")
      insert into (i_cTable) (CFCODICE,CPROWNUM,CFETICHE,CFPOSIZI,CFLUNGHE,CF_START,CF__FUNC &i_ccchkf. );
         values (;
           "DEFA";
           ,4;
           ,"Fase";
           ,40;
           ,3;
           ," ";
           ,"F";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Inserisco la Classe codice di fase di default in PAR_PROD
    * --- Write into PAR_PROD
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PAR_PROD_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROD_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_PROD_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PPCLAFAS ="+cp_NullLink(cp_ToStrODBC("DEFA"),'PAR_PROD','PPCLAFAS');
      +",PP_CICLI ="+cp_NullLink(cp_ToStrODBC("N"),'PAR_PROD','PP_CICLI');
          +i_ccchkf ;
      +" where ";
          +"PPCODICE = "+cp_ToStrODBC("PP");
             )
    else
      update (i_cTable) set;
          PPCLAFAS = "DEFA";
          ,PP_CICLI = "N";
          &i_ccchkf. ;
       where;
          PPCODICE = "PP";

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    this.w_TMPC = ah_Msgformat("Aggiornamento eseguito con successo")
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = this.w_TMPC
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='ART_DIST'
    this.cWorkTables[2]='CCF_MAST'
    this.cWorkTables[3]='CCF_DETT'
    this.cWorkTables[4]='PAR_PROD'
    return(this.OpenAllTables(4))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
