* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscv_b83                                                        *
*              Aggiornamento importo mancati pagamenti                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_15]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-11-19                                                      *
* Last revis.: 2006-12-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscv_b83",oParentObject,m.w_NHF)
return(i_retval)

define class tgscv_b83 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_SERIAL = space(10)
  w_PTSERIAL = space(10)
  w_PTTOTIMP = 0
  w_PTROWORD = 0
  w_CPROWNUM = 0
  w_TOTIMP = 0
  * --- WorkFile variables
  CONDTENZ_idx=0
  CON_TENZ_idx=0
  PAR_TITE_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- FIle di LOG
    * --- Try
    local bErr_0375AD30
    bErr_0375AD30=bTrsErr
    this.Try_0375AD30()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE - impossibile aggiornare COTOTIMP su tabella CONDTENZ" )
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_0375AD30
    * --- End
  endproc
  proc Try_0375AD30()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Aggiornamento  AZCODPRO
    * --- Select from CON_TENZ
    i_nConn=i_TableProp[this.CON_TENZ_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CON_TENZ_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" CON_TENZ ";
          +" where CO__TIPO ='M'";
           ,"_Curs_CON_TENZ")
    else
      select * from (i_cTable);
       where CO__TIPO ="M";
        into cursor _Curs_CON_TENZ
    endif
    if used('_Curs_CON_TENZ')
      select _Curs_CON_TENZ
      locate for 1=1
      do while not(eof())
      this.w_SERIAL = _Curs_CON_TENZ.COSERIAL
      * --- Read from CONDTENZ
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONDTENZ_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONDTENZ_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "COORDRIF,COSERRIF,CONUMRIF,COTOTIMP"+;
          " from "+i_cTable+" CONDTENZ where ";
              +"COSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          COORDRIF,COSERRIF,CONUMRIF,COTOTIMP;
          from (i_cTable) where;
              COSERIAL = this.w_SERIAL;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PTROWORD = NVL(cp_ToDate(_read_.COORDRIF),cp_NullValue(_read_.COORDRIF))
        this.w_PTSERIAL = NVL(cp_ToDate(_read_.COSERRIF),cp_NullValue(_read_.COSERRIF))
        this.w_CPROWNUM = NVL(cp_ToDate(_read_.CONUMRIF),cp_NullValue(_read_.CONUMRIF))
        this.w_TOTIMP = NVL(cp_ToDate(_read_.COTOTIMP),cp_NullValue(_read_.COTOTIMP))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Read from PAR_TITE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PAR_TITE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "PTTOTIMP"+;
          " from "+i_cTable+" PAR_TITE where ";
              +"PTSERIAL = "+cp_ToStrODBC(this.w_PTSERIAL);
              +" and PTROWORD = "+cp_ToStrODBC(this.w_PTROWORD);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          PTTOTIMP;
          from (i_cTable) where;
              PTSERIAL = this.w_PTSERIAL;
              and PTROWORD = this.w_PTROWORD;
              and CPROWNUM = this.w_CPROWNUM;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PTTOTIMP = NVL(cp_ToDate(_read_.PTTOTIMP),cp_NullValue(_read_.PTTOTIMP))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_TOTIMP=0
        * --- Write into CONDTENZ
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.CONDTENZ_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONDTENZ_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CONDTENZ_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"COTOTIMP ="+cp_NullLink(cp_ToStrODBC(this.w_PTTOTIMP),'CONDTENZ','COTOTIMP');
              +i_ccchkf ;
          +" where ";
              +"COSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                 )
        else
          update (i_cTable) set;
              COTOTIMP = this.w_PTTOTIMP;
              &i_ccchkf. ;
           where;
              COSERIAL = this.w_SERIAL;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
        select _Curs_CON_TENZ
        continue
      enddo
      use
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Aggiornamento campo COTOTIMP su tabella CONDTENZ eseguito con successo")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = this.oParentObject.w_PMSG + this.w_TMPC 
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='CONDTENZ'
    this.cWorkTables[2]='CON_TENZ'
    this.cWorkTables[3]='PAR_TITE'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_CON_TENZ')
      use in _Curs_CON_TENZ
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
