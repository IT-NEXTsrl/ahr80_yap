* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcc36                                                        *
*              Altri avvocati in nota di iscrizione                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2013-12-27                                                      *
* Last revis.: 2014-04-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcc36",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcc36 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_NISERIAL = space(10)
  w_QryInsert = space(1)
  * --- WorkFile variables
  ISC_AVVO_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- FIle di LOG
    if IsAlt()
      * --- Solo per AeTop
      * --- Try
      local bErr_038B3168
      bErr_038B3168=bTrsErr
      this.Try_038B3168()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        * --- Gestisce log errori
        this.oParentObject.w_PMSG = Message()
        this.oParentObject.w_PESEOK = .F.
        if this.w_NHF>=0
          this.w_TMPC = this.oParentObject.w_PMSG
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
      bTrsErr=bTrsErr or bErr_038B3168
      * --- End
    else
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("Procedura di conversione eseguita con successo")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
  endproc
  proc Try_038B3168()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- in GSCVCC36 la query GSAL3BXL viene utilizzata per sostituire il -1 in CPROWNUM di ISC_AVVO con il CPROWNUM della persona
    this.w_QryInsert = "S"
    * --- GSAL3BXL determina il cprownum della prima parte o prima controparte a seconda della tipologia della busta - per utilizzarla abbiamo bisogno di NISERIAL
    *     A sua volta utilizza GSAL0BXL
    * --- Nella fase di insert escludiamo i seriali delle note che hanno gi� gli avvocati figli di riga presenti in GSCVCC36_0
    * --- Insert into ISC_AVVO
    i_nConn=i_TableProp[this.ISC_AVVO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ISC_AVVO_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\PCON\EXE\QUERY\GSCVCC36",this.ISC_AVVO_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Adesso rimuoviamo i record con -1 ottenuti dalla query
    this.w_QryInsert = "N"
    * --- Delete from ISC_AVVO
    i_nConn=i_TableProp[this.ISC_AVVO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ISC_AVVO_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      local i_cQueryTable,i_cWhere
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_cWhere=i_cTable+".AASERIAL = "+i_cQueryTable+".AASERIAL";
            +" and "+i_cTable+".AARIFPAD = "+i_cQueryTable+".AARIFPAD";
    
      do vq_exec with '..\PCON\EXE\QUERY\GSCVCC36',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
            +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    this.oParentObject.w_PESEOK = .T.
    this.w_TMPC = AH_MsgFormat("Aggiornamento avvocati in nota di iscrizione a ruolo eseguito correttamente")
    this.oParentObject.w_PMSG = this.w_TMPC
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='ISC_AVVO'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
