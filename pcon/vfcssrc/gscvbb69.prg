* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvbb69                                                        *
*              Ripartisce sconti                                               *
*                                                                              *
*      Author: Nicola Gorlandi                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-03-31                                                      *
* Last revis.: 2007-03-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvbb69",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvbb69 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_MESS = space(100)
  w_NUMAGG = 0
  w_CLADOC = space(2)
  w_MVSCONTI = 0
  w_TOTMERCE = 0
  w_MVSCOCL1 = 0
  w_MVSCOCL2 = 0
  w_MVSCOPAG = 0
  w_DECTOT = 0
  w_oERRORLOG = .NULL.
  w_Errori = .f.
  w_Stop = .f.
  w_DATMOD = ctod("  /  /  ")
  w_SERIAL = space(10)
  w_BCOMM = .f.
  w_BMAGA = .f.
  w_ROWNUM = 0
  w_NUMRIF = 0
  w_VALULT = 0
  w_IMPSCO = 0
  w_IMPCOM = 0
  w_VALMAG = 0
  w_IMPNAZ = 0
  w_IMPACC_IVA = 0
  w_OK = .f.
  w_SEREVA = space(10)
  w_CLADOCEVA = space(2)
  w_ROWORD = 0
  pDOCINFO = .NULL.
  PAR = .NULL.
  w_RET = .NULL.
  w_contrate = 0
  * --- WorkFile variables
  AHEPATCH_idx=0
  DOC_DETT_idx=0
  DOC_MAST_idx=0
  DOC_RATE_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Ricalcola gli sconti e li ripartisce nuovamente sul documento nel caso in cui il documento
    *     sia stato creato da una importazione da altro ciclo con ricalcolo prezzi.
    *     In questo caso il documento assume un totale che � non comprensivo 
    *     degli sconti finali
    *     Deve essere quindi ricalcolato il giusto sconto e ripartito nuovamente sul documento
    * --- FIle di LOG
    this.w_Errori = .f.
    this.w_Stop = .F.
    this.w_oERRORLOG=createobject("AH_ErrorLog")
    * --- Try
    local bErr_035F3330
    bErr_035F3330=bTrsErr
    this.Try_035F3330()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = Ah_MsgFormat("ERRORE GENERICO - Impossibile aggiornare Documenti %1", this.oParentObject.w_PMSG)
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_035F3330
    * --- End
    if used("CESP")
       
 select CESP 
 use
    endif
  endproc
  proc Try_035F3330()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    do GSCV_K69 with this
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Se installata path 282 allora procedo con l'aggiornamento
    this.w_oERRORLOG.AddMsgLog("I seguenti documenti verranno aggiornati")     
    * --- Select from GSCVBB69
    do vq_exec with 'GSCVBB69',this,'_Curs_GSCVBB69','',.f.,.t.
    if used('_Curs_GSCVBB69')
      select _Curs_GSCVBB69
      locate for 1=1
      do while not(eof())
      this.w_oERRORLOG.AddMsgLog("Ciclo %1 causale %2 registrazione num: %3 del: %4", iif( _Curs_GSCVBB69.MVFLVEAC="V", "Vendite","Acquisti"), _Curs_GSCVBB69.MVTIPDOC, Alltrim(STR(_Curs_GSCVBB69.MVNUMREG,15,0)), DTOC(_Curs_GSCVBB69.MVDATREG))     
      this.w_Errori = .T.
        select _Curs_GSCVBB69
        continue
      enddo
      use
    endif
    this.w_oERRORLOG.PrintLog(this.oParentObject,"Verifica documenti",.t.,"Stampo situazione documenti errati?")     
    * --- Pulisco il cursore di stampa
    this.w_oERRORLOG.ClearLog()     
    if this.w_Errori And Ah_YesNo("Si vuole continuare con l'aggiornamento dei documenti indicati nella stampa?")
      * --- begin transaction
      cp_BeginTrs()
      * --- Scorro ogni documento ed aggiorno il dettaglio...
      * --- Select from GSCVBB69
      do vq_exec with 'GSCVBB69',this,'_Curs_GSCVBB69','',.f.,.t.
      if used('_Curs_GSCVBB69')
        select _Curs_GSCVBB69
        locate for 1=1
        do while not(eof())
        if _Curs_GSCVBB69.MVFLSCAF="S"
          this.w_oERRORLOG.AddMsgLog("Ciclo %1 causale %2 registrazione num: %3 del: %4", iif( _Curs_GSCVBB69.MVFLVEAC="V", "Vendite","Acquisti"), _Curs_GSCVBB69.MVTIPDOC, Alltrim(STR(_Curs_GSCVBB69.MVNUMREG,15,0)), DTOC(_Curs_GSCVBB69.MVDATREG))     
          this.w_oERRORLOG.AddMsgLog("Impossibile aggiornare documento ha le scadenze confermate.")     
        else
          this.w_SERIAL = _Curs_GSCVBB69.MVSERIAL
          this.w_CLADOC = _Curs_GSCVBB69.MVCLADOC
          this.w_TOTMERCE = Nvl(_Curs_GSCVBB69.TOTMERCE, 0)
          this.w_MVSCOCL1 = Nvl(_Curs_GSCVBB69.MVSCOCL1, 0)
          this.w_MVSCOCL2 = Nvl(_Curs_GSCVBB69.MVSCOCL2, 0)
          this.w_MVSCOPAG = Nvl(_Curs_GSCVBB69.MVSCOPAG, 0)
          this.w_DECTOT = Nvl(_Curs_GSCVBB69.DECTOT, 0)
          * --- Ricalcolo gli sconti nel modo corretto
          this.w_MVSCONTI = Calsco(this.w_TOTMERCE, this.w_MVSCOCL1, this.w_MVSCOCL2, this.w_MVSCOPAG, this.w_DECTOT)
          * --- Write into DOC_MAST
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.DOC_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MVSCONTI ="+cp_NullLink(cp_ToStrODBC(this.w_MVSCONTI),'DOC_MAST','MVSCONTI');
                +i_ccchkf ;
            +" where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                   )
          else
            update (i_cTable) set;
                MVSCONTI = this.w_MVSCONTI;
                &i_ccchkf. ;
             where;
                MVSERIAL = this.w_SERIAL;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          * --- Costruisco il cursore da passare alla routine di ricalcolo della ripartizione dei
          *     soli sconti
          VQ_EXEC("..\PCON\EXE\QUERY\GSCVBB69b.VQR", this , "GENEAPP" )
          =WrCursor( "GENEAPP" )
          * --- L'anomalia sia solo nel caso in cui le spese di testata sono tutte a 0!
          *     (non gestisco errori di ritorno)
          this.w_MESS = GSAR_BRS(this,"B", "GENEAPP", _Curs_GSCVBB69.MVFLVEAC, _Curs_GSCVBB69.MVFLSCOR, 0 , this.w_MVSCONTI, Nvl(_Curs_GSCVBB69.TOTMERCE,0), Nvl(_Curs_GSCVBB69.MVCODIVE,""), nvl(_Curs_GSCVBB69.MVCAOVAL,0), _Curs_GSCVBB69.DATCOM, nvl(_Curs_GSCVBB69.CAONAZ,0), nvl(_Curs_GSCVBB69.MVVALNAZ,""), Nvl(_Curs_GSCVBB69.MVCODVAL,""), 0 , This )
          this.w_oERRORLOG.AddMsgLog("Ciclo %1 causale %2 registrazione num: %3 del: %4", iif( _Curs_GSCVBB69.MVFLVEAC="V", "Vendite","Acquisti"), _Curs_GSCVBB69.MVTIPDOC, Alltrim(STR(_Curs_GSCVBB69.MVNUMREG,15,0)), DTOC(_Curs_GSCVBB69.MVDATREG))     
          this.w_BCOMM = .T.
          this.w_BMAGA = .T.
          * --- Scorro il cursore con i nuovi valori ed aggiorno il database..
           
 Select "GENEAPP" 
 Go Top
          do while Not Eof( "GENEAPP" )
            * --- Alla prima riga valida
            if this.w_BCOMM And Not Empty ( Nvl( t_MVCODCOS , "" )) And ( Not Empty ( Nvl( t_MVFLORCO , "" )) Or Not Empty ( Nvl( t_MVFLCOCO , "" )) )
              this.w_oERRORLOG.AddMsgLog("Occorre ricostruire saldi di commessa")     
              this.w_BCOMM = .F.
            endif
            if this.w_BMAGA And t_FLINVE="S" And ((Nvl(t_FLELGM,"")="S" And Nvl(t_FLAVAL,"")$"AVCS") Or ((Nvl(t_FLELGM1,"")="S" And Nvl(t_FLAVAL1,"")$"AVCS")))
              this.w_oERRORLOG.AddMsgLog("Occorre verificare eventuali inventari nel periodo")     
              this.w_BMAGA = .F.
            endif
            this.w_ROWNUM = CPROWNUM
            this.w_NUMRIF = MVNUMRIF
            this.w_OK = .T.
            if this.w_CLADOC$"DT-DI" And t_MVQTAEV1<>0
              * --- Verifico il tipo documento del documento che evade...
              this.w_ROWORD = Nvl( t_CPROWORD , 0 )
              * --- Read from DOC_DETT
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.DOC_DETT_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "MVSERIAL"+;
                  " from "+i_cTable+" DOC_DETT where ";
                      +"MVSERRIF = "+cp_ToStrODBC(this.w_SERIAL);
                      +" and MVROWRIF = "+cp_ToStrODBC(this.w_ROWNUM);
                      +" and MVNUMRIF = "+cp_ToStrODBC(this.w_NUMRIF);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  MVSERIAL;
                  from (i_cTable) where;
                      MVSERRIF = this.w_SERIAL;
                      and MVROWRIF = this.w_ROWNUM;
                      and MVNUMRIF = this.w_NUMRIF;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_SEREVA = NVL(cp_ToDate(_read_.MVSERIAL),cp_NullValue(_read_.MVSERIAL))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              * --- Read from DOC_MAST
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.DOC_MAST_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "MVCLADOC"+;
                  " from "+i_cTable+" DOC_MAST where ";
                      +"MVSERIAL = "+cp_ToStrODBC(this.w_SEREVA);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  MVCLADOC;
                  from (i_cTable) where;
                      MVSERIAL = this.w_SEREVA;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_CLADOCEVA = NVL(cp_ToDate(_read_.MVCLADOC),cp_NullValue(_read_.MVCLADOC))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              * --- NON VERIFICO EVENTUALE RIPARTIZIONE SPESE!
              if this.w_CLADOCEVA$"FA-NC"
                this.w_oERRORLOG.AddMsgLog("Impossibile rivalorizzare riga %1 rivalorizzata da fattura o nota di credito", Alltrim(Str( this.w_ROWORD )))     
                this.w_OK = .F.
              endif
            endif
            if this.w_OK
              this.w_VALULT = t_MVVALULT
              this.w_IMPSCO = t_MVIMPSCO
              this.w_IMPCOM = t_MVIMPCOM
              this.w_VALMAG = t_MVVALMAG
              this.w_IMPNAZ = t_MVIMPNAZ
              * --- Write into DOC_DETT
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.DOC_DETT_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"MVVALULT ="+cp_NullLink(cp_ToStrODBC(this.w_VALULT),'DOC_DETT','MVVALULT');
                +",MVIMPSCO ="+cp_NullLink(cp_ToStrODBC(this.w_IMPSCO),'DOC_DETT','MVIMPSCO');
                +",MVIMPCOM ="+cp_NullLink(cp_ToStrODBC(this.w_IMPCOM),'DOC_DETT','MVIMPCOM');
                +",MVVALMAG ="+cp_NullLink(cp_ToStrODBC(this.w_VALMAG),'DOC_DETT','MVVALMAG');
                +",MVIMPNAZ ="+cp_NullLink(cp_ToStrODBC(this.w_IMPNAZ),'DOC_DETT','MVIMPNAZ');
                    +i_ccchkf ;
                +" where ";
                    +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                    +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWNUM);
                    +" and MVNUMRIF = "+cp_ToStrODBC(this.w_NUMRIF);
                       )
              else
                update (i_cTable) set;
                    MVVALULT = this.w_VALULT;
                    ,MVIMPSCO = this.w_IMPSCO;
                    ,MVIMPCOM = this.w_IMPCOM;
                    ,MVVALMAG = this.w_VALMAG;
                    ,MVIMPNAZ = this.w_IMPNAZ;
                    &i_ccchkf. ;
                 where;
                    MVSERIAL = this.w_SERIAL;
                    and CPROWNUM = this.w_ROWNUM;
                    and MVNUMRIF = this.w_NUMRIF;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
              this.w_NUMAGG = this.w_NUMAGG + 1
            endif
             
 Select "GENEAPP" 
 Skip
          enddo
          * --- Ricalcolo castelletto IVA e scadenze...
          this.PAR=createobject("PARAMETRO")
          this.pDOCINFO=createobject("DOCOBJ",this.par)
          this.pDOCINFO.cMVSERIAL = this.w_SERIAL
          * --- Rileggo il documento dal database...
          this.pDOCINFO.cRead = "A"
          this.pDOCINFO.cAzione = "Load"
          this.w_RET = CASTIVARATE(.null., this.pDOCINFO)
          * --- Aggiorno le rate...
          this.w_contrate = 1
          this.w_RET.mcRate.gotop()     
          * --- Per le rate aggiorno solo gli importi (la modifica dello sconto non pu� modificare altro di fatto)
          do while not this.w_RET.mcRate.eof()
            * --- Write into DOC_RATE
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.DOC_RATE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DOC_RATE_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_RATE_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"RSIMPRAT ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcRate.IMPNET + this.w_RET.mcRate.IMPIVA + this.w_RET.mcRate.IMPSPE),'DOC_RATE','RSIMPRAT');
                  +i_ccchkf ;
              +" where ";
                  +"RSSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                  +" and RSNUMRAT = "+cp_ToStrODBC(this.w_contrate);
                     )
            else
              update (i_cTable) set;
                  RSIMPRAT = this.w_RET.mcRate.IMPNET + this.w_RET.mcRate.IMPIVA + this.w_RET.mcRate.IMPSPE;
                  &i_ccchkf. ;
               where;
                  RSSERIAL = this.w_SERIAL;
                  and RSNUMRAT = this.w_contrate;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            if i_ROWS=0
              * --- Insert into DOC_RATE
              i_nConn=i_TableProp[this.DOC_RATE_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DOC_RATE_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_ccchkf=''
              i_ccchkv=''
              this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DOC_RATE_idx,i_nConn)
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                            " ("+"RSSERIAL"+",RSNUMRAT"+",RSDATRAT"+",RSIMPRAT"+",RSMODPAG"+",RSFLSOSP"+i_ccchkf+") values ("+;
                cp_NullLink(cp_ToStrODBC(this.w_SERIAL),'DOC_RATE','RSSERIAL');
                +","+cp_NullLink(cp_ToStrODBC(this.w_contrate),'DOC_RATE','RSNUMRAT');
                +","+cp_NullLink(cp_ToStrODBC(this.w_RET.mcRate.DATRAT),'DOC_RATE','RSDATRAT');
                +","+cp_NullLink(cp_ToStrODBC(this.w_RET.mcRate.IMPNET + this.w_RET.mcRate.IMPIVA + this.w_RET.mcRate.IMPSPE),'DOC_RATE','RSIMPRAT');
                +","+cp_NullLink(cp_ToStrODBC(this.w_RET.mcRate.MODPAG),'DOC_RATE','RSMODPAG');
                +","+cp_NullLink(cp_ToStrODBC(this.w_RET.mcRate.FLPROV),'DOC_RATE','RSFLSOSP');
                     +i_ccchkv+")")
              else
                cp_CheckDeletedKey(i_cTable,0,'RSSERIAL',this.w_SERIAL,'RSNUMRAT',this.w_contrate,'RSDATRAT',this.w_RET.mcRate.DATRAT,'RSIMPRAT',this.w_RET.mcRate.IMPNET + this.w_RET.mcRate.IMPIVA + this.w_RET.mcRate.IMPSPE,'RSMODPAG',this.w_RET.mcRate.MODPAG,'RSFLSOSP',this.w_RET.mcRate.FLPROV)
                insert into (i_cTable) (RSSERIAL,RSNUMRAT,RSDATRAT,RSIMPRAT,RSMODPAG,RSFLSOSP &i_ccchkf. );
                   values (;
                     this.w_SERIAL;
                     ,this.w_contrate;
                     ,this.w_RET.mcRate.DATRAT;
                     ,this.w_RET.mcRate.IMPNET + this.w_RET.mcRate.IMPIVA + this.w_RET.mcRate.IMPSPE;
                     ,this.w_RET.mcRate.MODPAG;
                     ,this.w_RET.mcRate.FLPROV;
                     &i_ccchkv. )
                i_Rows=iif(bTrsErr,0,1)
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if i_Rows<0 or bTrsErr
                * --- Error: insert not accepted
                i_Error=MSG_INSERT_ERROR
                return
              endif
            endif
            this.w_contrate = this.w_contrate+1
            this.w_RET.mcRate.Next()     
          enddo
          * --- Aggiorno i totali
          this.w_RET.mcCastiva.gotop()     
          this.w_contrate = 1
          do while not this.w_RET.mcCastiva.eof()
            do case
              case this.w_contrate=1
                * --- Write into DOC_MAST
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.DOC_MAST_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"MVAIMPN1 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.IMPON),'DOC_MAST','MVAIMPN1');
                  +",MVAIMPS1 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.IMPOS),'DOC_MAST','MVAIMPS1');
                      +i_ccchkf ;
                  +" where ";
                      +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                         )
                else
                  update (i_cTable) set;
                      MVAIMPN1 = this.w_RET.mcCastiva.IMPON;
                      ,MVAIMPS1 = this.w_RET.mcCastiva.IMPOS;
                      &i_ccchkf. ;
                   where;
                      MVSERIAL = this.w_SERIAL;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              case this.w_contrate=2
                * --- Write into DOC_MAST
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.DOC_MAST_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"MVAIMPN2 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.IMPON),'DOC_MAST','MVAIMPN2');
                  +",MVAIMPS2 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.IMPOS),'DOC_MAST','MVAIMPS2');
                      +i_ccchkf ;
                  +" where ";
                      +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                         )
                else
                  update (i_cTable) set;
                      MVAIMPN2 = this.w_RET.mcCastiva.IMPON;
                      ,MVAIMPS2 = this.w_RET.mcCastiva.IMPOS;
                      &i_ccchkf. ;
                   where;
                      MVSERIAL = this.w_SERIAL;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              case this.w_contrate=3
                * --- Write into DOC_MAST
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.DOC_MAST_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"MVAIMPN3 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.IMPON),'DOC_MAST','MVAIMPN3');
                  +",MVAIMPS3 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.IMPOS),'DOC_MAST','MVAIMPS3');
                      +i_ccchkf ;
                  +" where ";
                      +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                         )
                else
                  update (i_cTable) set;
                      MVAIMPN3 = this.w_RET.mcCastiva.IMPON;
                      ,MVAIMPS3 = this.w_RET.mcCastiva.IMPOS;
                      &i_ccchkf. ;
                   where;
                      MVSERIAL = this.w_SERIAL;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              case this.w_contrate=4
                * --- Write into DOC_MAST
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.DOC_MAST_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"MVAIMPN4 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.IMPON),'DOC_MAST','MVAIMPN4');
                  +",MVAIMPS4 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.IMPOS),'DOC_MAST','MVAIMPS4');
                      +i_ccchkf ;
                  +" where ";
                      +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                         )
                else
                  update (i_cTable) set;
                      MVAIMPN4 = this.w_RET.mcCastiva.IMPON;
                      ,MVAIMPS4 = this.w_RET.mcCastiva.IMPOS;
                      &i_ccchkf. ;
                   where;
                      MVSERIAL = this.w_SERIAL;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              case this.w_contrate=5
                * --- Write into DOC_MAST
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.DOC_MAST_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"MVAIMPN5 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.IMPON),'DOC_MAST','MVAIMPN5');
                  +",MVAIMPS5 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.IMPOS),'DOC_MAST','MVAIMPS5');
                      +i_ccchkf ;
                  +" where ";
                      +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                         )
                else
                  update (i_cTable) set;
                      MVAIMPN5 = this.w_RET.mcCastiva.IMPON;
                      ,MVAIMPS5 = this.w_RET.mcCastiva.IMPOS;
                      &i_ccchkf. ;
                   where;
                      MVSERIAL = this.w_SERIAL;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              case this.w_contrate=6
                * --- Write into DOC_MAST
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.DOC_MAST_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"MVAIMPN6 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.IMPON),'DOC_MAST','MVAIMPN6');
                  +",MVAIMPS6 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.IMPOS),'DOC_MAST','MVAIMPS6');
                      +i_ccchkf ;
                  +" where ";
                      +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                         )
                else
                  update (i_cTable) set;
                      MVAIMPN6 = this.w_RET.mcCastiva.IMPON;
                      ,MVAIMPS6 = this.w_RET.mcCastiva.IMPOS;
                      &i_ccchkf. ;
                   where;
                      MVSERIAL = this.w_SERIAL;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
            endcase
            this.w_contrate = this.w_contrate+1
            this.w_RET.mcCastiva.Next()     
          enddo
           
 Use in "GENEAPP"
        endif
          select _Curs_GSCVBB69
          continue
        enddo
        use
      endif
      * --- commit
      cp_EndTrs(.t.)
      this.w_oERRORLOG.PrintLog(this.oParentObject,"Errori",.t.,"Stampo verifiche da effettuare su documenti?")     
    else
      this.w_Stop = .t.
    endif
    if this.w_Stop
      if this.w_NHF>=0
        this.w_TMPC = Ah_MsgFormat("Procedura interrotta dall'utente")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
      * --- Esecuzione ok
      this.oParentObject.w_PESEOK = .F.
    else
      if this.w_NHF>=0
        this.w_TMPC = "- Aggiornamento dettaglio documenti."+chr(13)+chr(10)
        this.w_TMPC = this.w_TMPC+"Variati "+ALLTRIM(STR(this.w_NUMAGG))+" righe"+chr(13)+chr(10)
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
      * --- Esecuzione ok
      this.oParentObject.w_PESEOK = .T.
    endif
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='AHEPATCH'
    this.cWorkTables[2]='DOC_DETT'
    this.cWorkTables[3]='DOC_MAST'
    this.cWorkTables[4]='DOC_RATE'
    return(this.OpenAllTables(4))

  proc CloseCursors()
    if used('_Curs_GSCVBB69')
      use in _Curs_GSCVBB69
    endif
    if used('_Curs_GSCVBB69')
      use in _Curs_GSCVBB69
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
