* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvbb75                                                        *
*              Inserimento allegati in tabella indici                          *
*                                                                              *
*      Author: Zucchetti S.p.A.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-06-22                                                      *
* Last revis.: 2009-03-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvbb75",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvbb75 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_KRIC = .f.
  w_NOMTAB = space(20)
  w_CODATT = space(15)
  w_DESATT = space(40)
  w_CAMCUR = space(20)
  w_TMPC = space(100)
  w_CODAZI = space(5)
  w_IDSERIAL = space(15)
  w_DATARAGGR = ctod("  /  /  ")
  w_VALATT = space(100)
  w_PATH = space(254)
  w_CLASSEDOC = space(15)
  w_PATHCOPI = space(100)
  w_PATHCOLL = space(100)
  w_NOMEFILE = space(254)
  w_DESCR = space(50)
  w_RAGGRU = space(1)
  w_DATARAG = ctod("  /  /  ")
  w_PATHABS = space(254)
  w_CONVROUT = space(50)
  w_ERRORE = space(0)
  w_ESEGUITA = space(1)
  w_INFPROC = space(20)
  w_CODREL = space(15)
  w_MESSAGGIO = space(20)
  * --- WorkFile variables
  GESTFILE_idx=0
  PROMINDI_idx=0
  PRODINDI_idx=0
  CONVERSI_idx=0
  PROMCLAS_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Conversione GESTFILE >> DOCMAN   (Inserimento allegati nell'archivio indici)
    * --- FIle di LOG
    * --- Try
    local bErr_04751840
    bErr_04751840=bTrsErr
    this.Try_04751840()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_msgformat("Errore generico durante la conversione - %1",Message())
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_04751840
    * --- End
  endproc
  proc Try_04751840()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if UPPER( G_APPLICATION ) # "AD HOC ENTERPRISE"
      this.w_CONVROUT = "GSCVAB84"
      this.w_INFPROC = "3.0-M3162"
    else
      this.w_CONVROUT = "GSCV_BDP"
      this.w_INFPROC = "20060600--00000"
    endif
    * --- Controllo se � stata eseguita e correttamente la conversione GSCVAB84 - Prerequisito all'esecuzione della presente
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_CONVROUT = "GSCVBB48"
    if UPPER( G_APPLICATION ) # "AD HOC ENTERPRISE"
      this.w_INFPROC = "4.0-M3837"
    else
      this.w_INFPROC = "20090700--00000"
    endif
    * --- Controllo se � stata eseguita e correttamente la conversione GSCVBB48 - Prerequisito all'esecuzione della presente
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Conversione archivio dati GESTFILE in indici Document Management
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if this.w_NHF>=0
      this.w_MESSAGGIO = iif(g_DOCM="S","indici documenti","indici allegati")
      this.w_TMPC = Ah_msgformat("Conversione GESTFILE e creazione %1 completata con successo.",alltrim(this.w_MESSAGGIO))
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = ah_msgformat("Conversione eseguita correttamente")
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inserimento indici
    * --- Try
    local bErr_04710F60
    bErr_04710F60=bTrsErr
    this.Try_04710F60()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
    endif
    bTrsErr=bTrsErr or bErr_04710F60
    * --- End
    * --- ************************************************************************************************
  endproc
  proc Try_04710F60()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Select from GESTFILE
    i_nConn=i_TableProp[this.GESTFILE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.GESTFILE_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select *  from "+i_cTable+" GESTFILE ";
           ,"_Curs_GESTFILE")
    else
      select * from (i_cTable);
        into cursor _Curs_GESTFILE
    endif
    if used('_Curs_GESTFILE')
      select _Curs_GESTFILE
      locate for 1=1
      do while not(eof())
      * --- Determino la classe da associare al file
      do case
        case _Curs_GESTFILE.GFCODPAD = "C"
          do case
            case _Curs_GESTFILE.GFTIPCON = "C"
              this.w_CLASSEDOC = "GESTFILE_CL"
              this.w_NOMTAB = "CONTI"
              this.w_CODATT = "CODICE"
              this.w_DESATT = "CODICE CLIENTE"
            case _Curs_GESTFILE.GFTIPCON = "F"
              this.w_CLASSEDOC = "GESTFILE_FR"
              this.w_NOMTAB = "CONTI"
              this.w_CODATT = "CODICE"
              this.w_DESATT = "CODICE FORNITORE"
            case _Curs_GESTFILE.GFTIPCON = "G"
              this.w_CLASSEDOC = "GESTFILE_GE"
              this.w_NOMTAB = "CONTI"
              this.w_CODATT = "CODICE"
              this.w_DESATT = "CODICE CONTO"
          endcase
          this.w_VALATT = _Curs_GESTFILE.GFTIPCON + _Curs_GESTFILE.GFCODCON
        case _Curs_GESTFILE.GFCODPAD = "D"
          this.w_CLASSEDOC = "GESTFILE_DO"
          this.w_NOMTAB = "DOC_MAST"
          this.w_CODATT = "SERIALE"
          this.w_DESATT = "SERIALE DOCUMENTO"
          this.w_VALATT = _Curs_GESTFILE.GFSERDOC
        case _Curs_GESTFILE.GFCODPAD = "A"
          this.w_CLASSEDOC = "GESTFILE_AR"
          this.w_NOMTAB = "ART_ICOL"
          this.w_CODATT = "CODICE"
          this.w_DESATT = "CODICE ARTICOLO"
          this.w_VALATT = _Curs_GESTFILE.GFCODART
        case _Curs_GESTFILE.GFCODPAD = "P"
          this.w_CLASSEDOC = "GESTFILE_PN"
          this.w_NOMTAB = "PNT_MAST"
          this.w_CODATT = "SERIALE"
          this.w_DESATT = "SERIALE REGISTRAZIONE"
          this.w_VALATT = _Curs_GESTFILE.GFSERPNT
        case _Curs_GESTFILE.GFCODPAD = "M"
          this.w_CLASSEDOC = "GESTFILE_MA"
          this.w_NOMTAB = "MARCHI"
          this.w_CODATT = "CODICE"
          this.w_DESATT = "CODICE MARCHIO"
          this.w_VALATT = _Curs_GESTFILE.GFCODMAR
        case _Curs_GESTFILE.GFCODPAD = "O"
          this.w_CLASSEDOC = "GESTFILE_CO"
          this.w_NOMTAB = "CATEGOMO"
          this.w_CODATT = "CODICE"
          this.w_DESATT = "CODICE CATEGORIA"
          this.w_VALATT = _Curs_GESTFILE.GFCODCAT
        case _Curs_GESTFILE.GFCODPAD = "F"
          this.w_CLASSEDOC = "GESTFILE_FA"
          this.w_NOMTAB = "FAM_ARTI"
          this.w_CODATT = "CODICE"
          this.w_DESATT = "CODICE FAMIGLIA"
          this.w_VALATT = _Curs_GESTFILE.GFCODFAM
        case _Curs_GESTFILE.GFCODPAD = "G"
          this.w_CLASSEDOC = "GESTFILE_GM"
          this.w_NOMTAB = "MARCHI"
          this.w_CODATT = "CODICE"
          this.w_DESATT = "CODICE GRUPPO"
          this.w_VALATT = _Curs_GESTFILE.GFCODGRU
        case _Curs_GESTFILE.GFCODPAD = "R"
          this.w_CLASSEDOC = "GESTFILE_KA"
          this.w_NOMTAB = "KEY_ARTI"
          this.w_CODATT = "CODICE"
          this.w_DESATT = "CODICE DI RICERCA"
          this.w_VALATT = _Curs_GESTFILE.GFCODICE
      endcase
      * --- Valorizzo i paths in base alla modalit� di associazione file
      if _Curs_GESTFILE.GFMODALL = "F"
        this.w_PATHCOPI = justpath(_Curs_GESTFILE.GF__PATH)
        this.w_PATHCOLL = "  "
      else
        this.w_PATHCOPI = "  "
        this.w_PATHCOLL = left(alltrim(_Curs_GESTFILE.GF__PATH),250)
      endif
      * --- Costruisco il nome del file catturato
      this.w_NOMEFILE = alltrim(substr(_Curs_GESTFILE.GF__PATH ,RAT("\", _Curs_GESTFILE.GF__PATH)+1))
      this.w_PATHABS = left(substr(_Curs_GESTFILE.GF__PATH ,1,RAT("\", _Curs_GESTFILE.GF__PATH)),100)
      this.w_IDSERIAL = space(20)
      * --- Calcolo il nuovo progressivo
      this.w_IDSERIAL = cp_GetProg("PROMINDI","SEIDO",this.w_IDSERIAL,i_CODAZI)
      * --- Verifico se � stato usato un raggruppamento
      * --- Read from PROMCLAS
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PROMCLAS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PROMCLAS_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CDTIPRAG"+;
          " from "+i_cTable+" PROMCLAS where ";
              +"CDCODCLA = "+cp_ToStrODBC(this.w_CLASSEDOC);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CDTIPRAG;
          from (i_cTable) where;
              CDCODCLA = this.w_CLASSEDOC;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_RAGGRU = NVL(cp_ToDate(_read_.CDTIPRAG),cp_NullValue(_read_.CDTIPRAG))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_RAGGRU <> "N"
        * --- Inserisco la data, che verr� utilizzata per il calcolo del path di archiviazione dell'allegato
        this.w_DATARAG = _Curs_GESTFILE.GFDATREG
      else
        this.w_DATARAG = ctod("  -  -    ")
      endif
      * --- Insert into PROMINDI
      i_nConn=i_TableProp[this.PROMINDI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PROMINDI_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"IDSERIAL"+",IDCLADOC"+",IDTIPALL"+",IDCLAALL"+",IDOGGETT"+",ID_TESTO"+",IDDATCRE"+",IDDATINI"+",IDDATOBS"+",IDORIGIN"+",IDNOMFIL"+",IDORIFIL"+",IDALLPRI"+",IDPUBWEB"+",IDDATRAG"+",IDMODALL"+",IDPATALL"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_IDSERIAL),'PROMINDI','IDSERIAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CLASSEDOC),'PROMINDI','IDCLADOC');
        +","+cp_NullLink(cp_ToStrODBC(_Curs_GESTFILE.GFTIPALL),'PROMINDI','IDTIPALL');
        +","+cp_NullLink(cp_ToStrODBC(_Curs_GESTFILE.GFCLAALL),'PROMINDI','IDCLAALL');
        +","+cp_NullLink(cp_ToStrODBC(_Curs_GESTFILE.GFDESCRI),'PROMINDI','IDOGGETT');
        +","+cp_NullLink(cp_ToStrODBC(_Curs_GESTFILE.GF__NOTE),'PROMINDI','ID_TESTO');
        +","+cp_NullLink(cp_ToStrODBC(_Curs_GESTFILE.GFDATREG),'PROMINDI','IDDATCRE');
        +","+cp_NullLink(cp_ToStrODBC(_Curs_GESTFILE.GFDATINI),'PROMINDI','IDDATINI');
        +","+cp_NullLink(cp_ToStrODBC(_Curs_GESTFILE.GFDATOBS),'PROMINDI','IDDATOBS');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PATHCOPI),'PROMINDI','IDORIGIN');
        +","+cp_NullLink(cp_ToStrODBC(this.w_NOMEFILE),'PROMINDI','IDNOMFIL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PATHCOLL),'PROMINDI','IDORIFIL');
        +","+cp_NullLink(cp_ToStrODBC(_Curs_GESTFILE.GFFLPRIN),'PROMINDI','IDALLPRI');
        +","+cp_NullLink(cp_ToStrODBC(_Curs_GESTFILE.GFPUBWEB),'PROMINDI','IDPUBWEB');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DATARAG),'PROMINDI','IDDATRAG');
        +","+cp_NullLink(cp_ToStrODBC(_Curs_GESTFILE.GFMODALL),'PROMINDI','IDMODALL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PATHABS),'PROMINDI','IDPATALL');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'IDSERIAL',this.w_IDSERIAL,'IDCLADOC',this.w_CLASSEDOC,'IDTIPALL',_Curs_GESTFILE.GFTIPALL,'IDCLAALL',_Curs_GESTFILE.GFCLAALL,'IDOGGETT',_Curs_GESTFILE.GFDESCRI,'ID_TESTO',_Curs_GESTFILE.GF__NOTE,'IDDATCRE',_Curs_GESTFILE.GFDATREG,'IDDATINI',_Curs_GESTFILE.GFDATINI,'IDDATOBS',_Curs_GESTFILE.GFDATOBS,'IDORIGIN',this.w_PATHCOPI,'IDNOMFIL',this.w_NOMEFILE,'IDORIFIL',this.w_PATHCOLL)
        insert into (i_cTable) (IDSERIAL,IDCLADOC,IDTIPALL,IDCLAALL,IDOGGETT,ID_TESTO,IDDATCRE,IDDATINI,IDDATOBS,IDORIGIN,IDNOMFIL,IDORIFIL,IDALLPRI,IDPUBWEB,IDDATRAG,IDMODALL,IDPATALL &i_ccchkf. );
           values (;
             this.w_IDSERIAL;
             ,this.w_CLASSEDOC;
             ,_Curs_GESTFILE.GFTIPALL;
             ,_Curs_GESTFILE.GFCLAALL;
             ,_Curs_GESTFILE.GFDESCRI;
             ,_Curs_GESTFILE.GF__NOTE;
             ,_Curs_GESTFILE.GFDATREG;
             ,_Curs_GESTFILE.GFDATINI;
             ,_Curs_GESTFILE.GFDATOBS;
             ,this.w_PATHCOPI;
             ,this.w_NOMEFILE;
             ,this.w_PATHCOLL;
             ,_Curs_GESTFILE.GFFLPRIN;
             ,_Curs_GESTFILE.GFPUBWEB;
             ,this.w_DATARAG;
             ,_Curs_GESTFILE.GFMODALL;
             ,this.w_PATHABS;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Insert into PRODINDI
      i_nConn=i_TableProp[this.PRODINDI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PRODINDI_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PRODINDI_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"IDSERIAL"+",CPROWNUM"+",CPROWORD"+",IDCODATT"+",IDTIPATT"+",IDCHKOBB"+",IDVALATT"+",IDTABKEY"+",IDDESATT"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_IDSERIAL),'PRODINDI','IDSERIAL');
        +","+cp_NullLink(cp_ToStrODBC(1),'PRODINDI','CPROWNUM');
        +","+cp_NullLink(cp_ToStrODBC(10),'PRODINDI','CPROWORD');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CODATT),'PRODINDI','IDCODATT');
        +","+cp_NullLink(cp_ToStrODBC("C"),'PRODINDI','IDTIPATT');
        +","+cp_NullLink(cp_ToStrODBC("N"),'PRODINDI','IDCHKOBB');
        +","+cp_NullLink(cp_ToStrODBC(this.w_VALATT),'PRODINDI','IDVALATT');
        +","+cp_NullLink(cp_ToStrODBC(this.w_NOMTAB),'PRODINDI','IDTABKEY');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DESATT),'PRODINDI','IDDESATT');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'IDSERIAL',this.w_IDSERIAL,'CPROWNUM',1,'CPROWORD',10,'IDCODATT',this.w_CODATT,'IDTIPATT',"C",'IDCHKOBB',"N",'IDVALATT',this.w_VALATT,'IDTABKEY',this.w_NOMTAB,'IDDESATT',this.w_DESATT)
        insert into (i_cTable) (IDSERIAL,CPROWNUM,CPROWORD,IDCODATT,IDTIPATT,IDCHKOBB,IDVALATT,IDTABKEY,IDDESATT &i_ccchkf. );
           values (;
             this.w_IDSERIAL;
             ,1;
             ,10;
             ,this.w_CODATT;
             ,"C";
             ,"N";
             ,this.w_VALATT;
             ,this.w_NOMTAB;
             ,this.w_DESATT;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
        select _Curs_GESTFILE
        continue
      enddo
      use
    endif
    * --- Aggiorno le date di inivio validit� ed obsolescenza per gli allegati in cui sono valorizzate a null.
    *     Questo per evitare di essere scartati in fase di ricerca o visualizzazione allegati.
    * --- Sostiutisco gli eventuali valori null presenti nel campo IDDATINI con i_INIDAT (che rappresenta l�estremo inferiore dell�intervallo di validit� delle date)
    * --- Write into PROMINDI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PROMINDI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PROMINDI_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"IDDATINI ="+cp_NullLink(cp_ToStrODBC(i_INIDAT),'PROMINDI','IDDATINI');
          +i_ccchkf ;
      +" where ";
          +"IDDATINI is null ";
             )
    else
      update (i_cTable) set;
          IDDATINI = i_INIDAT;
          &i_ccchkf. ;
       where;
          IDDATINI is null;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Sostiutisco gli eventuali valori null presenti nel campo IDDATOBS con i_FINDAT (che rappresenta l�estremo superiore dell�intervallo di validit� delle date)
    * --- Write into PROMINDI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PROMINDI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PROMINDI_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"IDDATOBS ="+cp_NullLink(cp_ToStrODBC(i_FINDAT),'PROMINDI','IDDATOBS');
          +i_ccchkf ;
      +" where ";
          +"IDDATOBS is null ";
             )
    else
      update (i_cTable) set;
          IDDATOBS = i_FINDAT;
          &i_ccchkf. ;
       where;
          IDDATOBS is null;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Chiedo se si vuole cancellare il contenuto della tabella GESTFILE
    if Ah_YesNo("Si vuole cancellare l'archivio GESTFILE?")
      * --- Delete from GESTFILE
      i_nConn=i_TableProp[this.GESTFILE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.GESTFILE_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"1 = "+cp_ToStrODBC(1);
               )
      else
        delete from (i_cTable) where;
              1 = 1;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Verifica della corretta esecuzione dell'operazione preliminare
    * --- Read from CONVERSI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CONVERSI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONVERSI_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "COESEGUI,COERRORE"+;
        " from "+i_cTable+" CONVERSI where ";
            +"CONOMPRO = "+cp_ToStrODBC(this.w_CONVROUT);
            +" and COCODREL = "+cp_ToStrODBC(this.w_INFPROC);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        COESEGUI,COERRORE;
        from (i_cTable) where;
            CONOMPRO = this.w_CONVROUT;
            and COCODREL = this.w_INFPROC;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_ESEGUITA = NVL(cp_ToDate(_read_.COESEGUI),cp_NullValue(_read_.COESEGUI))
      this.w_ERRORE = NVL(cp_ToDate(_read_.COERRORE),cp_NullValue(_read_.COERRORE))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_INFPROC = alltrim(this.w_INFPROC)
    do case
      case this.w_ESEGUITA # "S"
        this.w_TMPC = Ah_msgformat("Procedura di conversione %1 non eseguita.  Elaborazione interrotta!",this.w_INFPROC)
        this.oParentObject.w_pMSG = this.w_TMPC
        this.oParentObject.w_PESEOK = .F.
        i_retcode = 'stop'
        return
      case this.w_ESEGUITA = "S"
        if not ("corrett" $ this.w_ERRORE or "success" $ this.w_ERRORE or empty(this.w_ERRORE))
          this.w_TMPC = Ah_msgformat("Procedura di conversione %1 eseguita con errori. Elaborazione interrotta!",this.w_INFPROC)
          this.oParentObject.w_pMSG = this.w_TMPC
          this.oParentObject.w_PESEOK = .F.
          i_retcode = 'stop'
          return
        endif
    endcase
  endproc


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='GESTFILE'
    this.cWorkTables[2]='PROMINDI'
    this.cWorkTables[3]='PRODINDI'
    this.cWorkTables[4]='CONVERSI'
    this.cWorkTables[5]='PROMCLAS'
    return(this.OpenAllTables(5))

  proc CloseCursors()
    if used('_Curs_GESTFILE')
      use in _Curs_GESTFILE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
