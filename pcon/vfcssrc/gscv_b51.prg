* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscv_b51                                                        *
*              Valorizza data competenza plafond                               *
*                                                                              *
*      Author: Zucchetti TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_18]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-09-05                                                      *
* Last revis.: 2001-09-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscv_b51",oParentObject,m.w_NHF)
return(i_retval)

define class tgscv_b51 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TMPC = space(10)
  w_TEMP = space(10)
  * --- WorkFile variables
  DOC_MAST_idx=0
  PNT_MAST_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Valorizza con la Data di registrazione il nuovo campo Data competenza Plafond 
    *     nella Prima Nota e nei Documenti 
    * --- FIle di LOG
    * --- Try
    local bErr_035ED3E0
    bErr_035ED3E0=bTrsErr
    this.Try_035ED3E0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE GENERICO - %1", Message() )
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_035ED3E0
    * --- End
  endproc
  proc Try_035ED3E0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Aggiorno Prima Nota
    * --- Select from PNT_MAST
    i_nConn=i_TableProp[this.PNT_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select PNSERIAL,PNDATREG  from "+i_cTable+" PNT_MAST ";
          +" where PNTIPREG='A'";
           ,"_Curs_PNT_MAST")
    else
      select PNSERIAL,PNDATREG from (i_cTable);
       where PNTIPREG="A";
        into cursor _Curs_PNT_MAST
    endif
    if used('_Curs_PNT_MAST')
      select _Curs_PNT_MAST
      locate for 1=1
      do while not(eof())
      * --- Write into PNT_MAST
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.PNT_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.PNT_MAST_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"PNDATPLA ="+cp_NullLink(cp_ToStrODBC(_Curs_PNT_MAST.PNDATREG),'PNT_MAST','PNDATPLA');
            +i_ccchkf ;
        +" where ";
            +"PNSERIAL = "+cp_ToStrODBC(_Curs_PNT_MAST.PNSERIAL);
               )
      else
        update (i_cTable) set;
            PNDATPLA = _Curs_PNT_MAST.PNDATREG;
            &i_ccchkf. ;
         where;
            PNSERIAL = _Curs_PNT_MAST.PNSERIAL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
        select _Curs_PNT_MAST
        continue
      enddo
      use
    endif
    * --- Aggiorno Documenti
    * --- Select from DOC_MAST
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select MVSERIAL, MVDATREG  from "+i_cTable+" DOC_MAST ";
          +" where MVFLVEAC='A'";
           ,"_Curs_DOC_MAST")
    else
      select MVSERIAL, MVDATREG from (i_cTable);
       where MVFLVEAC="A";
        into cursor _Curs_DOC_MAST
    endif
    if used('_Curs_DOC_MAST')
      select _Curs_DOC_MAST
      locate for 1=1
      do while not(eof())
      * --- Write into DOC_MAST
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DOC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MVDATPLA ="+cp_NullLink(cp_ToStrODBC(_Curs_DOC_MAST.MVDATREG),'DOC_MAST','MVDATPLA');
            +i_ccchkf ;
        +" where ";
            +"MVSERIAL = "+cp_ToStrODBC(_Curs_DOC_MAST.MVSERIAL);
               )
      else
        update (i_cTable) set;
            MVDATPLA = _Curs_DOC_MAST.MVDATREG;
            &i_ccchkf. ;
         where;
            MVSERIAL = _Curs_DOC_MAST.MVSERIAL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
        select _Curs_DOC_MAST
        continue
      enddo
      use
    endif
    * --- commit
    cp_EndTrs(.t.)
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Aggiornamento date competenza Plafond terminato con successo")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = ah_Msgformat("Conversione eseguita correttamente")
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='DOC_MAST'
    this.cWorkTables[2]='PNT_MAST'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_PNT_MAST')
      use in _Curs_PNT_MAST
    endif
    if used('_Curs_DOC_MAST')
      use in _Curs_DOC_MAST
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
