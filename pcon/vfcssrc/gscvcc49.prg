* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcc49                                                        *
*              Modifica chiave primaria Tabella ciclo di lavorazione           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-10-30                                                      *
* Last revis.: 2014-11-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcc49",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcc49 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_FraseSQL = space(200)
  * --- WorkFile variables
  ODL_CICL_idx=0
  ODLMRISO_idx=0
  ODL_RISO_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- 8.0 - Inizializzazione tabella Distinte alternative ART_DIST e CLassi codice di fase CCF_MAST e CCF_DETT
    * --- FIle di LOG
    * --- Try
    local bErr_03684BE8
    bErr_03684BE8=bTrsErr
    this.Try_03684BE8()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("Errore generico - impossibile eseguire aggiornamento richiesto")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_03684BE8
    * --- End
  endproc
  proc Try_03684BE8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    if this.w_NHF>=0
      this.w_TMPC = ah_msgformat("Eliminazione constraint tabelle ODL_RISO, ODL_CICL")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    ah_msg("Aggiornamento in corso..." )
    GSCV_BDC(this,"xxx", "ODL_RISO")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    GSCV_BDC(this,"xxx", "ODL_CICL")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    GSCV_BDC(this,i_CODAZI, "ODL_RISO")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    GSCV_BDC(this,i_CODAZI, "ODL_CICL")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- --------------------------------------------------------------------------------------
    if UPPER(CP_DBTYPE)="SQLSERVER"
      if this.w_NHF>=0
        this.w_TMPC = ah_msgformat("Aggiornamento CPROWNUM per cambio chiave su tabelle ODL_CICL e ODLSCICL")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
      ah_msg("Aggiornamento in corso..." )
      * --- Devo mettere not null il campo CPROWNUM altrimenti non riesce a ricreare la chiave
      i_nConn=i_TableProp[this.ODL_CICL_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.ODL_CICL_idx,2])
      this.w_FraseSQL = "ALTER TABLE xxxODL_CICL ALTER COLUMN CPROWNUM INT NOT NULL"
      w_iRows = CP_SQLEXEC(i_nConn, rtrim(this.w_FraseSQL))
      this.w_FraseSQL = "ALTER TABLE " + i_cTable + " ALTER COLUMN CPROWNUM INT NOT NULL"
      w_iRows = CP_SQLEXEC(i_nConn, rtrim(this.w_FraseSQL))
      if bTrsErr
        if this.w_NHF>=0
          this.w_TMPC = Message()
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
        * --- Raise
        i_Error="Impossibile assegnare la proprietÓ NOT NULL alla colonna CPROWNUM della tabella ODL_CICL"
        return
      else
        if this.w_NHF>=0
          this.w_TMPC = AH_MsgFormat("Adeguamento chiave primaria tabella ODL_CICL eseguito correttamente.")
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
      * --- --------------------------------------------------------------------------------------
      i_nConn=i_TableProp[this.ODLMRISO_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.ODLMRISO_idx,2])
      this.w_FraseSQL = "ALTER TABLE xxxODLMRISO ALTER COLUMN RLROWNUM INT NOT NULL"
      w_iRows = SQLExec(i_nConn, rtrim(this.w_FraseSQL))
      this.w_FraseSQL = "ALTER TABLE " + i_cTable + " ALTER COLUMN RLROWNUM INT NOT NULL"
      w_iRows = SQLExec(i_nConn, rtrim(this.w_FraseSQL))
      if bTrsErr
        if this.w_NHF>=0
          this.w_TMPC = Message()
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
        * --- Raise
        i_Error="Impossibile assegnare la proprietÓ NOT NULL alla colonna RLROWNUM della tabella ODLMRISO"
        return
      else
        if this.w_NHF>=0
          this.w_TMPC = AH_MsgFormat("Adeguamento chiave primaria tabella ODLMRISO eseguito correttamente.")
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
    endif
    * --- --------------------------------------------------------------------------------------
    * --- commit
    cp_EndTrs(.t.)
    i_nConn=i_TableProp[this.ODL_CICL_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.ODL_CICL_idx,2])
    GSCV_BRT(this,"ODL_CICL" , i_nConn , .T. , "all", "N")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    i_nConn=i_TableProp[this.ODLMRISO_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.ODLMRISO_idx,2])
    GSCV_BRT(this,"ODLMRISO" , i_nConn , .T. , "all", "N")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    i_nConn=i_TableProp[this.ODL_RISO_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.ODL_RISO_idx,2])
    GSCV_BRT(this,"ODL_RISO" , i_nConn , .T. , "all", "N")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_TMPC = ah_Msgformat("Aggiornamento eseguito con successo")
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = this.w_TMPC
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='ODL_CICL'
    this.cWorkTables[2]='ODLMRISO'
    this.cWorkTables[3]='ODL_RISO'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
