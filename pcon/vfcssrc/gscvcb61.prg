* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcb61                                                        *
*              Normalizzazione dati addon con dati licenza                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-01-14                                                      *
* Last revis.: 2009-11-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcb61",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcb61 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TMPC = space(10)
  w_TEMP = space(10)
  * --- WorkFile variables
  ACTKEY_idx=0
  ADDON_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Normalizzazione dati addon con dati licenza
    * --- Try
    local bErr_03015418
    bErr_03015418=bTrsErr
    this.Try_03015418()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = this.oParentObject.w_PMSG
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_03015418
    * --- End
  endproc
  proc Try_03015418()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Azzero vecchi addon
    * --- Write into ACTKEY
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.ACTKEY_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ACTKEY_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.ACTKEY_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MOADD001 ="+cp_NullLink(cp_ToStrODBC(""),'ACTKEY','MOADD001');
      +",MOADD002 ="+cp_NullLink(cp_ToStrODBC(""),'ACTKEY','MOADD002');
      +",MOADD003 ="+cp_NullLink(cp_ToStrODBC(""),'ACTKEY','MOADD003');
      +",MOADD004 ="+cp_NullLink(cp_ToStrODBC(""),'ACTKEY','MOADD004');
      +",MOADD005 ="+cp_NullLink(cp_ToStrODBC(""),'ACTKEY','MOADD005');
      +",MOADD006 ="+cp_NullLink(cp_ToStrODBC(""),'ACTKEY','MOADD006');
      +",MOADD007 ="+cp_NullLink(cp_ToStrODBC(""),'ACTKEY','MOADD007');
      +",MOADD008 ="+cp_NullLink(cp_ToStrODBC(""),'ACTKEY','MOADD008');
      +",MOADD009 ="+cp_NullLink(cp_ToStrODBC(""),'ACTKEY','MOADD009');
      +",MOADD010 ="+cp_NullLink(cp_ToStrODBC(""),'ACTKEY','MOADD010');
      +",MOADD011 ="+cp_NullLink(cp_ToStrODBC(""),'ACTKEY','MOADD011');
      +",MOADD012 ="+cp_NullLink(cp_ToStrODBC(""),'ACTKEY','MOADD012');
      +",MOADD013 ="+cp_NullLink(cp_ToStrODBC(""),'ACTKEY','MOADD013');
      +",MOADD014 ="+cp_NullLink(cp_ToStrODBC(""),'ACTKEY','MOADD014');
      +",MOADD015 ="+cp_NullLink(cp_ToStrODBC(""),'ACTKEY','MOADD015');
          +i_ccchkf ;
      +" where ";
          +"MOCODICE = "+cp_ToStrODBC("0000000001");
             )
    else
      update (i_cTable) set;
          MOADD001 = "";
          ,MOADD002 = "";
          ,MOADD003 = "";
          ,MOADD004 = "";
          ,MOADD005 = "";
          ,MOADD006 = "";
          ,MOADD007 = "";
          ,MOADD008 = "";
          ,MOADD009 = "";
          ,MOADD010 = "";
          ,MOADD011 = "";
          ,MOADD012 = "";
          ,MOADD013 = "";
          ,MOADD014 = "";
          ,MOADD015 = "";
          &i_ccchkf. ;
       where;
          MOCODICE = "0000000001";

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Delete from ADDON
    i_nConn=i_TableProp[this.ADDON_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ADDON_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"1 = "+cp_ToStrODBC(1);
             )
    else
      delete from (i_cTable) where;
            1 = 1;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Esecuzione ok
    * --- commit
    cp_EndTrs(.t.)
    this.oParentObject.w_PESEOK = .T.
    this.w_TMPC = ah_Msgformat("Normalizzazione dati addon con dati licenza avvenuta correttamente")
    this.oParentObject.w_PMSG = ah_Msgformat("Conversione eseguita correttamente%0� necessario uscire e rientrare dalla procedura per rendere effettivi i cambiamenti apportati.")
    if this.w_NHF>=0
      this.w_TMPC = this.oParentObject.w_PMSG
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='ACTKEY'
    this.cWorkTables[2]='ADDON'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
