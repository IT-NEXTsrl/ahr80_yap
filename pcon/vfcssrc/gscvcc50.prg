* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcc50                                                        *
*              Flag elimina spazi nel dettaglio formati                        *
*                                                                              *
*      Author: ZUCCHETTI S.p.A.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_32]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-12-01                                                      *
* Last revis.: 2014-12-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcc50",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcc50 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  * --- WorkFile variables
  VADETTFO_idx=0
  CONTI_idx=0
  RIPATMP1_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna dettaglio formati flag elimina alltrim
    * --- FIle di LOG
    * --- Try
    local bErr_00D98110
    bErr_00D98110=bTrsErr
    this.Try_00D98110()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = this.oParentObject.w_PMSG
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_00D98110
    * --- End
  endproc
  proc Try_00D98110()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Write into VADETTFO
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.VADETTFO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VADETTFO_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="DFCODSTR,DFCODFOR,CPROWNUM"
      do vq_exec with 'GSCVCC50',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.VADETTFO_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="VADETTFO.DFCODSTR = _t2.DFCODSTR";
              +" and "+"VADETTFO.DFCODFOR = _t2.DFCODFOR";
              +" and "+"VADETTFO.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"DFFLESAL ="+cp_NullLink(cp_ToStrODBC("S"),'VADETTFO','DFFLESAL');
          +i_ccchkf;
          +" from "+i_cTable+" VADETTFO, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="VADETTFO.DFCODSTR = _t2.DFCODSTR";
              +" and "+"VADETTFO.DFCODFOR = _t2.DFCODFOR";
              +" and "+"VADETTFO.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" VADETTFO, "+i_cQueryTable+" _t2 set ";
      +"VADETTFO.DFFLESAL ="+cp_NullLink(cp_ToStrODBC("S"),'VADETTFO','DFFLESAL');
          +Iif(Empty(i_ccchkf),"",",VADETTFO.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="VADETTFO.DFCODSTR = t2.DFCODSTR";
              +" and "+"VADETTFO.DFCODFOR = t2.DFCODFOR";
              +" and "+"VADETTFO.CPROWNUM = t2.CPROWNUM";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" VADETTFO set (";
          +"DFFLESAL";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +cp_NullLink(cp_ToStrODBC("S"),'VADETTFO','DFFLESAL')+"";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="VADETTFO.DFCODSTR = _t2.DFCODSTR";
              +" and "+"VADETTFO.DFCODFOR = _t2.DFCODFOR";
              +" and "+"VADETTFO.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" VADETTFO set ";
      +"DFFLESAL ="+cp_NullLink(cp_ToStrODBC("S"),'VADETTFO','DFFLESAL');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".DFCODSTR = "+i_cQueryTable+".DFCODSTR";
              +" and "+i_cTable+".DFCODFOR = "+i_cQueryTable+".DFCODFOR";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"DFFLESAL ="+cp_NullLink(cp_ToStrODBC("S"),'VADETTFO','DFFLESAL');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.w_TMPC = Ah_Msgformat("Adeguamento campo DFFLESAL in VADETTFO eseguito con successo")
    this.oParentObject.w_PMSG = this.w_TMPC
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella DB2
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.CONTI_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"* "," from "+i_cTable;
          )
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    * --- Elimino la tabella
    if Not GSCV_BDT( this, "CONTI" , i_nConn )
      * --- Raise
      i_Error="Error"
      return
    endif
    * --- Ricostruisco il database per la tabella
    GSCV_BRT(this, "CONTI" , i_nConn , .T.)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into CONTI
    i_nConn=i_TableProp[this.CONTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.CONTI_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_Msg("Ripristinati valori di origine",.T.)
    GSCV_BRI(this, "CONTI" , i_nConn )
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    ah_Msg("Eliminata tabella temporanea tmp",.T.)
  endproc


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='VADETTFO'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='*RIPATMP1'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
