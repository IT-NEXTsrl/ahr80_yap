* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvbb30                                                        *
*              Copia kit in distinta                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_122]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-03-17                                                      *
* Last revis.: 2007-05-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvbb30",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvbb30 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TMPC = space(10)
  w_TEMP = space(10)
  w_KICODART = space(20)
  w_EXCODART = space(20)
  w_KIDESCRI = space(40)
  w_KI__NOTE = space(0)
  w_KIPERSCO = 0
  w_KIVALSCO = 0
  w_KICODCOM = space(20)
  w_CPROWORD = 0
  w_CPROWNUM = 0
  w_KIARTCOM = space(20)
  w_KIUNIMIS = space(3)
  w_KIQTAMOV = 0
  w_KIPERSCR = 0
  w_KIVALSCR = 0
  w_KIDATINI = ctod("  /  /  ")
  w_KIDATFIN = ctod("  /  /  ")
  w_KIFLOMAG = space(1)
  w_KIQTAUM1 = 0
  w_FOUND = .f.
  w_Progre = 0
  w_DESART = space(40)
  w_Errore = .f.
  w_NKIT = 0
  w_CODDIS = space(20)
  w_TEST = space(20)
  w_ORIKIT = space(20)
  w_oERRORLOG = .NULL.
  w_Loop = .f.
  w_DISKIT = space(1)
  * --- WorkFile variables
  KIT_MAST_idx=0
  DISMBASE_idx=0
  KIT_DETT_idx=0
  DISTBASE_idx=0
  ART_ICOL_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Trasporta i dati da KIT_MAST a DISMBASE e 
    *     KIT_DETT a DISTBASE
    *     Gli articoli kit verranno quindi gestiti come distinte sui documenti
    * --- FIle di LOG
    this.w_oERRORLOG=createobject("AH_ErrorLog")
    this.w_Errore = .F.
    this.w_NKIT = 0
    * --- Select from KIT_MAST
    i_nConn=i_TableProp[this.KIT_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.KIT_MAST_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" KIT_MAST ";
           ,"_Curs_KIT_MAST")
    else
      select * from (i_cTable);
        into cursor _Curs_KIT_MAST
    endif
    if used('_Curs_KIT_MAST')
      select _Curs_KIT_MAST
      locate for 1=1
      do while not(eof())
      * --- Per ogni Kit riparto con il progressivo da 1
      this.w_Progre = 1
      * --- Found: per controllare che non esista gi� una distinta con lo stesso codice del Kit
      *     In questo caso devo calcolare un codice progressivo
      this.w_FOUND = .T.
      this.w_CODDIS = ""
      this.w_KICODART = _Curs_KIT_MAST.KICODART
      this.w_ORIKIT = _Curs_KIT_MAST.KICODART
      this.w_EXCODART = this.w_KICODART
      this.w_KIDESCRI = Nvl(_Curs_KIT_MAST.KIDESCRI,Space(40))
      this.w_KI__NOTE = Nvl(_Curs_KIT_MAST.KI__NOTE, "")
      this.w_KIPERSCO = Nvl(_Curs_KIT_MAST.KIPERSCO,0)
      this.w_KIVALSCO = Nvl(_Curs_KIT_MAST.KIVALSCO, 0)
      * --- Read from DISMBASE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.DISMBASE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DISMBASE_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "DBCODICE"+;
          " from "+i_cTable+" DISMBASE where ";
              +"DBCODICE = "+cp_ToStrODBC(this.w_KICODART);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          DBCODICE;
          from (i_cTable) where;
              DBCODICE = this.w_KICODART;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_TEST = NVL(cp_ToDate(_read_.DBCODICE),cp_NullValue(_read_.DBCODICE))
        use
        if i_Rows=0
          * --- Non c'� distinta con stesso nome quindi posso creare la distinta 
          *     con lo stesso codice del Kit
          this.w_FOUND = .F.
        endif
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Se trovo una distinta con lo stesso nome devo cambiarlo
      if this.w_Found
        this.w_Loop = .T.
        * --- Se c'� gi� con lo stesso nome ricalcolo il codice con progressivo
        this.w_KICODART = Alltrim(Left(this.w_KICODART,18) ) + Right("00"+ Alltrim(Str(this.w_Progre,2,0)), 2 )
        this.w_Progre = this.w_Progre + 1
        do while this.w_Loop
          * --- Ciclo fino a che non trovo un codice distinta non gi� presente
          *     La casistica � praticamente impossibile ma deve essere gestita
          * --- Read from DISMBASE
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DISMBASE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DISMBASE_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "DBCODICE"+;
              " from "+i_cTable+" DISMBASE where ";
                  +"DBCODICE = "+cp_ToStrODBC(this.w_KICODART);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              DBCODICE;
              from (i_cTable) where;
                  DBCODICE = this.w_KICODART;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_TEST = NVL(cp_ToDate(_read_.DBCODICE),cp_NullValue(_read_.DBCODICE))
            use
            if i_Rows=0
              * --- Non c'� distinta con stesso nome quindi posso creare la distinta 
              *     con lo stesso codice del Kit
              this.w_Loop = .F.
            endif
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if i_Rows <>0
            this.w_Loop = .T.
            this.w_KICODART = Alltrim(Left(this.w_KICODART,18) ) + Right("00"+ Alltrim(Str(this.w_Progre,2,0)), 2 )
            this.w_Progre = this.w_Progre + 1
          endif
        enddo
      endif
      * --- Read from ART_ICOL
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ARCODDIS"+;
          " from "+i_cTable+" ART_ICOL where ";
              +"ARCODART = "+cp_ToStrODBC(this.w_EXCODART);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ARCODDIS;
          from (i_cTable) where;
              ARCODART = this.w_EXCODART;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CODDIS = NVL(cp_ToDate(_read_.ARCODDIS),cp_NullValue(_read_.ARCODDIS))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if Empty(this.w_CODDIS)
        * --- Try
        local bErr_0360EA50
        bErr_0360EA50=bTrsErr
        this.Try_0360EA50()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          * --- Gestisce log errori
          this.oParentObject.w_PMSG = Message()
          this.w_Errore = .T.
          if this.w_NHF>=0
            this.w_TMPC = ah_Msgformat("ERRORE GENERICO - impossibile inserire kit %1%0%2", Alltrim(this.w_KICODART), Alltrim(this.oParentObject.w_PMSG))
            this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
          endif
        endif
        bTrsErr=bTrsErr or bErr_0360EA50
        * --- End
      else
        * --- Read from DISMBASE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DISMBASE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DISMBASE_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "DBDISKIT"+;
            " from "+i_cTable+" DISMBASE where ";
                +"DBCODICE = "+cp_ToStrODBC(this.w_CODDIS);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            DBDISKIT;
            from (i_cTable) where;
                DBCODICE = this.w_CODDIS;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DISKIT = NVL(cp_ToDate(_read_.DBDISKIT),cp_NullValue(_read_.DBDISKIT))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Se ho gi� associato un Kit va bene ma non sovrascrivo
        if this.w_DISKIT<>"K"
          this.w_oERRORLOG.AddMsgLog("L'articolo %1 ha gi� una distinta associata. Impossibile associare kit commerciale: %2", Alltrim(this.w_EXCODART), Alltrim(this.w_ORIKIT) )     
          this.w_Errore = .T.
          if this.w_NHF>=0
            this.w_TMPC = ah_Msgformat("L'articolo %1 ha gi� una distinta associata", Alltrim(this.w_EXCODART) )
            this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
          endif
        endif
      endif
        select _Curs_KIT_MAST
        continue
      enddo
      use
    endif
    if this.w_Errore
      this.w_oERRORLOG.PrintLog(this.oParentObject,"Errori riscontrati",.t., "Stampo situazione articoli da verificare?")     
    endif
    this.oParentObject.w_PESEOK = Not this.w_Errore
    this.oParentObject.w_PMSG = ah_Msgformat("Conversione eseguita correttamente per %1 kit", Alltrim(Str(this.w_NKIT,5,0) ) )
    if this.w_NHF>=0
      this.w_TMPC = this.oParentObject.w_PMSG
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
  endproc
  proc Try_0360EA50()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Insert into DISMBASE
    i_nConn=i_TableProp[this.DISMBASE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DISMBASE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DISMBASE_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"DBCODICE"+",DBDESCRI"+",UTCC"+",UTCV"+",UTDC"+",UTDV"+",DBNOTAGG"+",DBFLSTAT"+",DBPERSCO"+",DBVALSCO"+",DBDISKIT"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_KICODART),'DISMBASE','DBCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_KIDESCRI),'DISMBASE','DBDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'DISMBASE','UTCC');
      +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -    ")),'DISMBASE','UTCV');
      +","+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'DISMBASE','UTDC');
      +","+cp_NullLink(cp_ToStrODBC(0),'DISMBASE','UTDV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_KI__NOTE),'DISMBASE','DBNOTAGG');
      +","+cp_NullLink(cp_ToStrODBC("S"),'DISMBASE','DBFLSTAT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_KIPERSCO),'DISMBASE','DBPERSCO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_KIVALSCO),'DISMBASE','DBVALSCO');
      +","+cp_NullLink(cp_ToStrODBC("K"),'DISMBASE','DBDISKIT');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'DBCODICE',this.w_KICODART,'DBDESCRI',this.w_KIDESCRI,'UTCC',i_CODUTE,'UTCV',cp_CharToDate("  -  -    "),'UTDC',SetInfoDate( g_CALUTD ),'UTDV',0,'DBNOTAGG',this.w_KI__NOTE,'DBFLSTAT',"S",'DBPERSCO',this.w_KIPERSCO,'DBVALSCO',this.w_KIVALSCO,'DBDISKIT',"K")
      insert into (i_cTable) (DBCODICE,DBDESCRI,UTCC,UTCV,UTDC,UTDV,DBNOTAGG,DBFLSTAT,DBPERSCO,DBVALSCO,DBDISKIT &i_ccchkf. );
         values (;
           this.w_KICODART;
           ,this.w_KIDESCRI;
           ,i_CODUTE;
           ,cp_CharToDate("  -  -    ");
           ,SetInfoDate( g_CALUTD );
           ,0;
           ,this.w_KI__NOTE;
           ,"S";
           ,this.w_KIPERSCO;
           ,this.w_KIVALSCO;
           ,"K";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Select from KIT_DETT
    i_nConn=i_TableProp[this.KIT_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.KIT_DETT_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" KIT_DETT ";
          +" where KICODART = "+cp_ToStrODBC(this.w_EXCODART)+"";
           ,"_Curs_KIT_DETT")
    else
      select * from (i_cTable);
       where KICODART = this.w_EXCODART;
        into cursor _Curs_KIT_DETT
    endif
    if used('_Curs_KIT_DETT')
      select _Curs_KIT_DETT
      locate for 1=1
      do while not(eof())
      * --- Leggo il dettaglio del Kit appena copiato
      this.w_KICODCOM = _Curs_KIT_DETT.KICODCOM
      this.w_CPROWORD = _Curs_KIT_DETT.CPROWORD
      this.w_CPROWNUM = _Curs_KIT_DETT.CPROWNUM
      this.w_KIARTCOM = _Curs_KIT_DETT.KIARTCOM
      * --- Read from ART_ICOL
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ARDESART"+;
          " from "+i_cTable+" ART_ICOL where ";
              +"ARCODART = "+cp_ToStrODBC(this.w_KIARTCOM);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ARDESART;
          from (i_cTable) where;
              ARCODART = this.w_KIARTCOM;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_DESART = NVL(cp_ToDate(_read_.ARDESART),cp_NullValue(_read_.ARDESART))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_KIUNIMIS = _Curs_KIT_DETT.KIUNIMIS
      this.w_KIQTAMOV = Nvl(_Curs_KIT_DETT.KIQTAMOV, 0)
      this.w_KIPERSCR = Nvl(_Curs_KIT_DETT.KIPERSCR, 0)
      this.w_KIVALSCR = Nvl(_Curs_KIT_DETT.KIVALSCR,0)
      * --- Se le date sono vuote devo riempirle con una data poich� nei kit nuovi sono obbligatorie
      this.w_KIDATINI = IIF(Empty(Nvl(_Curs_KIT_DETT.KIDATINI, Cp_CharToDate("  -  -  "))), Cp_CharToDate("01-01-2000"), _Curs_KIT_DETT.KIDATINI )
      this.w_KIDATFIN = IIF(Empty(Nvl(_Curs_KIT_DETT.KIDATFIN, Cp_CharToDate("  -  -  "))), Cp_CharToDate("31-12-2020"), _Curs_KIT_DETT.KIDATFIN )
      this.w_KIFLOMAG = _Curs_KIT_DETT.KIFLOMAG
      this.w_KIQTAUM1 = Nvl(_Curs_KIT_DETT.KIQTAUM1, 0)
      * --- Insert into DISTBASE
      i_nConn=i_TableProp[this.DISTBASE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DISTBASE_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DISTBASE_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"DBCODICE"+",CPROWNUM"+",CPROWORD"+",DBFLVARI"+",DBCODCOM"+",DBDESCOM"+",DBUNIMIS"+",DBQTADIS"+",DBPERSCA"+",DBRECSCA"+",DBPERSFR"+",DBRECSFR"+",DBPERRIC"+",DBFLESPL"+",DBFLVARC"+",DBINIVAL"+",DBFINVAL"+",DBARTCOM"+",DBCOEUM1"+",DBPERSCR"+",DBVALSCR"+",DBFLOMAG"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_KICODART),'DISTBASE','DBCODICE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'DISTBASE','CPROWNUM');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'DISTBASE','CPROWORD');
        +","+cp_NullLink(cp_ToStrODBC(" "),'DISTBASE','DBFLVARI');
        +","+cp_NullLink(cp_ToStrODBC(this.w_KICODCOM),'DISTBASE','DBCODCOM');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DESART),'DISTBASE','DBDESCOM');
        +","+cp_NullLink(cp_ToStrODBC(this.w_KIUNIMIS),'DISTBASE','DBUNIMIS');
        +","+cp_NullLink(cp_ToStrODBC(this.w_KIQTAMOV),'DISTBASE','DBQTADIS');
        +","+cp_NullLink(cp_ToStrODBC(0),'DISTBASE','DBPERSCA');
        +","+cp_NullLink(cp_ToStrODBC(0),'DISTBASE','DBRECSCA');
        +","+cp_NullLink(cp_ToStrODBC(0),'DISTBASE','DBPERSFR');
        +","+cp_NullLink(cp_ToStrODBC(0),'DISTBASE','DBRECSFR');
        +","+cp_NullLink(cp_ToStrODBC(0),'DISTBASE','DBPERRIC');
        +","+cp_NullLink(cp_ToStrODBC(" "),'DISTBASE','DBFLESPL');
        +","+cp_NullLink(cp_ToStrODBC(" "),'DISTBASE','DBFLVARC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_KIDATINI),'DISTBASE','DBINIVAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_KIDATFIN),'DISTBASE','DBFINVAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_KIARTCOM),'DISTBASE','DBARTCOM');
        +","+cp_NullLink(cp_ToStrODBC(this.w_KIQTAUM1),'DISTBASE','DBCOEUM1');
        +","+cp_NullLink(cp_ToStrODBC(this.w_KIPERSCR),'DISTBASE','DBPERSCR');
        +","+cp_NullLink(cp_ToStrODBC(this.w_KIVALSCR),'DISTBASE','DBVALSCR');
        +","+cp_NullLink(cp_ToStrODBC(this.w_KIFLOMAG),'DISTBASE','DBFLOMAG');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'DBCODICE',this.w_KICODART,'CPROWNUM',this.w_CPROWNUM,'CPROWORD',this.w_CPROWORD,'DBFLVARI'," ",'DBCODCOM',this.w_KICODCOM,'DBDESCOM',this.w_DESART,'DBUNIMIS',this.w_KIUNIMIS,'DBQTADIS',this.w_KIQTAMOV,'DBPERSCA',0,'DBRECSCA',0,'DBPERSFR',0,'DBRECSFR',0)
        insert into (i_cTable) (DBCODICE,CPROWNUM,CPROWORD,DBFLVARI,DBCODCOM,DBDESCOM,DBUNIMIS,DBQTADIS,DBPERSCA,DBRECSCA,DBPERSFR,DBRECSFR,DBPERRIC,DBFLESPL,DBFLVARC,DBINIVAL,DBFINVAL,DBARTCOM,DBCOEUM1,DBPERSCR,DBVALSCR,DBFLOMAG &i_ccchkf. );
           values (;
             this.w_KICODART;
             ,this.w_CPROWNUM;
             ,this.w_CPROWORD;
             ," ";
             ,this.w_KICODCOM;
             ,this.w_DESART;
             ,this.w_KIUNIMIS;
             ,this.w_KIQTAMOV;
             ,0;
             ,0;
             ,0;
             ,0;
             ,0;
             ," ";
             ," ";
             ,this.w_KIDATINI;
             ,this.w_KIDATFIN;
             ,this.w_KIARTCOM;
             ,this.w_KIQTAUM1;
             ,this.w_KIPERSCR;
             ,this.w_KIVALSCR;
             ,this.w_KIFLOMAG;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
        select _Curs_KIT_DETT
        continue
      enddo
      use
    endif
    * --- Write into ART_ICOL
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.ART_ICOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.ART_ICOL_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"ARCODDIS ="+cp_NullLink(cp_ToStrODBC(this.w_KICODART),'ART_ICOL','ARCODDIS');
      +",ARFLCOMP ="+cp_NullLink(cp_ToStrODBC("S"),'ART_ICOL','ARFLCOMP');
          +i_ccchkf ;
      +" where ";
          +"ARCODART = "+cp_ToStrODBC(this.w_EXCODART);
             )
    else
      update (i_cTable) set;
          ARCODDIS = this.w_KICODART;
          ,ARFLCOMP = "S";
          &i_ccchkf. ;
       where;
          ARCODART = this.w_EXCODART;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    this.w_NKIT = this.w_NKIT +1
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='KIT_MAST'
    this.cWorkTables[2]='DISMBASE'
    this.cWorkTables[3]='KIT_DETT'
    this.cWorkTables[4]='DISTBASE'
    this.cWorkTables[5]='ART_ICOL'
    return(this.OpenAllTables(5))

  proc CloseCursors()
    if used('_Curs_KIT_MAST')
      use in _Curs_KIT_MAST
    endif
    if used('_Curs_KIT_DETT')
      use in _Curs_KIT_DETT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
