* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvab75                                                        *
*              Aggiorna campo trcotrif (trib_uti)                              *
*                                                                              *
*      Author: Zucchetti S.p.A.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_51]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-06-18                                                      *
* Last revis.: 2001-06-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvab75",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvab75 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_CODICE = space(15)
  w_CODTRF24 = space(4)
  w_RIGATEST = 0
  w_MESS = space(100)
  * --- WorkFile variables
  CATMLIST_idx=0
  TRI_BUTI_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questa procedura valorizza il campo 'TRCOTRIF' della tabella TRI_BUTI, dove � nullo o vuoto a 'N'
    * --- FIle di LOG
    * --- Try
    local bErr_036008A0
    bErr_036008A0=bTrsErr
    this.Try_036008A0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE GENERICO - impossibile aggiornare il campo TRCOTRIF tabella TRIB_UTI")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_036008A0
    * --- End
  endproc
  proc Try_036008A0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if not ah_YesNo("Questa conversione richiede che siano stati caricati%0i codici tributo per il modello F24%0Sono stati caricati?")
      * --- Uscita dalla procedura di conversione
      ah_ErrorMsg("Conversione interrotta per mancanza codici tributi modello F24","!","")
      this.oParentObject.w_PESEOK = .F.
      this.w_TMPC = ah_Msgformat("Conversione volontariamente interrotta")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      i_retcode = 'stop'
      return
    endif
    * --- begin transaction
    cp_BeginTrs()
    * --- Vado a leggere nella tabella TRIB_UTI I TRIBUTI che soddisfano la seguente relazione 'TRFLACON='R'', cio� quelli la
    *     cui tipologia tributo � 'IRPEF'.
    *     Per questi imposto il campo TRCOTRIF a SUBSTR(TRCODTRI,1,4)
    *     
    vq_exec("..\pcon\exe\query\gscvab75.vqr",this,"TribLis")
    if USED("TribLis")
      if Reccount ("TribLis")>0
        Select TribLis
        Go Top
        Scan
        this.w_CODICE = ALLTRIM(NVL(TribLis.TRCODTRI,""))
        this.w_CODTRF24 = SUBSTR(this.w_CODICE,1,4)
        this.w_RIGATEST = 1
        * --- Effettuo l'aggiornamento nella tabella TRIB_UTI
        * --- Write into TRI_BUTI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.TRI_BUTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TRI_BUTI_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TRI_BUTI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"TRCOTRIF ="+cp_NullLink(cp_ToStrODBC(this.w_CODTRF24),'TRI_BUTI','TRCOTRIF');
              +i_ccchkf ;
          +" where ";
              +"TRCODTRI = "+cp_ToStrODBC(this.w_CODICE);
                 )
        else
          update (i_cTable) set;
              TRCOTRIF = this.w_CODTRF24;
              &i_ccchkf. ;
           where;
              TRCODTRI = this.w_CODICE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        Endscan
        * --- Esecuzione ok
        if this.w_NHF>=0
          this.w_TMPC = ah_Msgformat("Aggiornamento campo TRCOTRIF eseguito correttamente")
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
        this.oParentObject.w_PESEOK = .T.
        this.oParentObject.w_PMSG = ah_Msgformat("Conversione eseguita correttamente")
      else
        * --- Gestisce log errori
        if this.w_NHF>=0
          this.w_TMPC = ah_Msgformat("ATTENZIONE: non esistono dati da convertire")
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
        * --- Esecuzione ok
        this.oParentObject.w_PESEOK = .T.
        this.oParentObject.w_PMSG = this.w_TMPC
      endif
      * --- Chiude cursore
      select TribLis
      use
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='CATMLIST'
    this.cWorkTables[2]='TRI_BUTI'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
