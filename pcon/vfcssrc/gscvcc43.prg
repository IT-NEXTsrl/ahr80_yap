* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcc43                                                        *
*              Aggiornamento chiave stumpiac                                   *
*                                                                              *
*      Author: Zucchetti S.p.A.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_271]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-10-18                                                      *
* Last revis.: 2014-11-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcc43",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcc43 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  * --- WorkFile variables
  DOCFINIM_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Modifica chiave alla tabela DOCFINIM (solo per Sql Server)
    *     aggiungendo il campo DFCHIAVE 
    * --- Try
    local bErr_02A73158
    bErr_02A73158=bTrsErr
    this.Try_02A73158()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = this.oParentObject.w_PMSG
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_02A73158
    * --- End
  endproc
  proc Try_02A73158()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    i_nConn=i_TableProp[this.DOCFINIM_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.DOCFINIM_idx,2])
    if i_nConn<>0
      do case
        case upper(CP_DBTYPE)="SQLSERVER"
          w_iRows = SQLExec(i_nConn,"ALTER TABLE "+i_cTable+; 
 " DROP CONSTRAINT pk_DOCFINIM")
          * --- Se la chiave non esiste non � un errore
          * --- accept error
          bTrsErr=.f.
          w_iRows = SQLExec(i_nConn,"ALTER TABLE "+i_cTable+; 
 " ALTER COLUMN DFCHIAVE char(6) NOT NULL")
          w_iRows = SQLExec(i_nConn,"ALTER TABLE "+i_cTable+; 
 " ADD CONSTRAINT PK_DOCFINIM PRIMARY KEY (DFCODAZI,DFNUMERA,DF__ANNO,DFMOVIME,DFDATREG,DFCONTRO,DFPIANOC,DFCHIAVE,CPROWNUM)")
      endcase
      if bTrsErr
        * --- Raise
        i_Error=MESSAGE()
        return
      else
        if this.w_NHF>=0
          this.w_TMPC = ah_Msgformat("Adeguamento chiave primaria tabelle DOCFINIM eseguito correttamente%0")
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.w_TMPC = ah_Msgformat("Adeguamento chiave primaria tabelle DOCFINIM eseguito correttamente%0")
    this.oParentObject.w_PMSG = this.w_TMPC
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='DOCFINIM'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
