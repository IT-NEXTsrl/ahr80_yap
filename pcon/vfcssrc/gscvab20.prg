* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvab20                                                        *
*              Aggiornamento cpprgsec                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_367]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-10-18                                                      *
* Last revis.: 2001-10-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvab20",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvab20 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_CODCAP = space(8)
  * --- WorkFile variables
  cpprgsec_idx=0
  CPPRGSEC_idx=0
  RIPATMP1_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiornamento archivio CPPRGSEC per DB2 causa cambio tipo
    *     campi SEC1,SEC2,SEC3,SEC4 e modifica della chiave...
    * --- FIle di LOG
    * --- Try
    local bErr_03724170
    bErr_03724170=bTrsErr
    this.Try_03724170()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = this.oParentObject.w_PMSG
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_03724170
    * --- End
  endproc
  proc Try_03724170()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.w_TMPC = ah_Msgformat("Adeguamento campi e chiave primaria tabella CPPRGSEC eseguito correttamente%0TTENZIONE: occorre andare in MANUTENZIONE DATABASE, selezionare la tabella%0CPPRGSEC e premere <OK>")
    this.oParentObject.w_PMSG = this.w_TMPC
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella DB2
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('..\PCON\EXE\QUERY\GSCGAB20',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    if cp_SQL(i_ServerConn[1,2],"Drop table cpprgsec")=-1
      * --- Raise
      i_Error=ah_Msgformat("Errore cancellazione %1", Message() )
      return
    endif
    if cp_SQL(i_ServerConn[1,2],"create table cpprgsec (progname Char(50) not null, grpcode "+db_FieldType("DB2","N",6,0)+" not null, usrcode "+db_FieldType("DB2","N",6,0)+" not null, sec1 "+db_FieldType("DB2","N",6,0)+",sec2 "+db_FieldType("DB2","N",6,0)+",sec3 "+db_FieldType("DB2","N",6,0)+",sec4 "+db_FieldType("DB2","N",6,0)+db_InlinePrimaryKey("cpprgsec","cpprgsec","progname,grpcode,usrcode","DB2")+")")=-1
      * --- Raise
      i_Error=ah_Msgformat("Errore create %1", Message() )
      return
    else
      db_CreatePrimaryKey(i_ServerConn[1,2],"cpprgsec","cpprgsec","progname,grpcode,usrcode","DB2")
      if cp_SQL(i_ServerConn[1,2],"create index cpprgsec1 on cpprgsec(progname)")=-1
        * --- Raise
        i_Error=ah_Msgformat("Errore create index 1 %1", Message() )
        return
      else
        if cp_SQL(i_ServerConn[1,2],"create index cpprgsec2 on cpprgsec(grpcode)")=-1
          * --- Raise
          i_Error=ah_Msgformat("Errore create index 2 %1", Message() )
          return
        else
          if cp_SQL(i_ServerConn[1,2],"create index cpprgsec3 on cpprgsec(usrcode)")=-1
            * --- Raise
            i_Error=ah_Msgformat("Errore create index 3 %1", Message() )
            return
          endif
        endif
      endif
    endif
    * --- problema CPCCHK...
    i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
    if cp_SQL(i_ServerConn[1,2],"Insert into cpprgsec ( Select progname,grpcode,usrcode,Sec1,Sec2,Sec3,Sec4 From "+i_cTempTable+" ) ")=-1
      * --- Raise
      i_Error=ah_Msgformat("Errore inserzione %1",Message() )
      return
    else
      ah_Msg("Ripristinati valori di origine",.T.)
    endif
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    ah_Msg("Eliminata tabella temporanea tmp",.T.)
  endproc


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='cpprgsec'
    this.cWorkTables[2]='CPPRGSEC'
    this.cWorkTables[3]='*RIPATMP1'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
