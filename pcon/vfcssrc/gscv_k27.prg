* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscv_k27                                                        *
*              Aggiorna qta importata/evasa documenti                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_10]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-07-23                                                      *
* Last revis.: 2008-10-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscv_k27",oParentObject))

* --- Class definition
define class tgscv_k27 as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 459
  Height = 219
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-10-07"
  HelpContextID=115946857
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=4

  * --- Constant Properties
  _IDX = 0
  cPrg = "gscv_k27"
  cComment = "Aggiorna qta importata/evasa documenti"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  w_OKCONV = .F.
  w_MESSAGGIO = space(0)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscv_k27Pag1","gscv_k27",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDATINI_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_OKCONV=.f.
      .w_MESSAGGIO=space(0)
      .w_DATINI=oParentObject.w_DATINI
      .w_DATFIN=oParentObject.w_DATFIN
      .w_OKCONV=oParentObject.w_OKCONV
          .DoRTCalc(1,2,.f.)
        .w_OKCONV = .T.
        .w_MESSAGGIO = NoteUtilizzo()
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_DATINI=.w_DATINI
      .oParentObject.w_DATFIN=.w_DATFIN
      .oParentObject.w_OKCONV=.w_OKCONV
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
            .w_OKCONV = .T.
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(4,4,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_1.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_1.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_2.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_2.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oMESSAGGIO_1_13.value==this.w_MESSAGGIO)
      this.oPgFrm.Page1.oPag.oMESSAGGIO_1_13.value=this.w_MESSAGGIO
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgscv_k27Pag1 as StdContainer
  Width  = 455
  height = 219
  stdWidth  = 455
  stdheight = 219
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDATINI_1_1 as StdField with uid="QBMIJLXQOQ",rtseq=1,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data reg. iniziale dei documenti per i quali la conversione deve essere eseguita",;
    HelpContextID = 146452426,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=148, Top=17

  add object oDATFIN_1_2 as StdField with uid="HFSVTVOFVY",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data reg. finale dei documenti per i quali la conversione deve essere eseguita",;
    HelpContextID = 68005834,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=370, Top=17


  add object oBtn_1_8 as StdButton with uid="ATTFGKAQEJ",left=350, top=169, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , HelpContextID = 115918106;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_8.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_9 as StdButton with uid="KYQESPJULE",left=400, top=169, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 108629434;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oMESSAGGIO_1_13 as StdMemo with uid="DJAKEKBCXW",rtseq=4,rtrep=.f.,;
    cFormVar = "w_MESSAGGIO", cQueryName = "MESSAGGIO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 75450879,;
   bGlobalFont=.t.,;
    Height=122, Width=436, Left=10, Top=42

  add object oStr_1_3 as StdString with uid="VHRWPAARHO",Visible=.t., Left=9, Top=17,;
    Alignment=1, Width=135, Height=18,;
    Caption="Data inizio selezione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="MKVZIUHLOQ",Visible=.t., Left=231, Top=17,;
    Alignment=1, Width=135, Height=18,;
    Caption="Data fine selezione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="ZTEPYPLWAP",Visible=.t., Left=503, Top=52,;
    Alignment=0, Width=428, Height=18,;
    Caption="Attenzione, la procedura di conversione verr� eseguita esclusivamente"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_6 as StdString with uid="TYXKJPGSAA",Visible=.t., Left=503, Top=71,;
    Alignment=0, Width=428, Height=18,;
    Caption="per quei documenti con data registrazione compresa nell'intervallo di date."  ;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_7 as StdString with uid="KZOFIVMZJG",Visible=.t., Left=503, Top=109,;
    Alignment=0, Width=428, Height=18,;
    Caption="Impostare in maniera corretta tale intervallo in modo che i documenti"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_11 as StdString with uid="MCOVPBPNWR",Visible=.t., Left=503, Top=128,;
    Alignment=0, Width=428, Height=18,;
    Caption="importati dal modulo import vengano esclusi."  ;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_12 as StdString with uid="MCBZZNEFHW",Visible=.t., Left=503, Top=90,;
    Alignment=0, Width=428, Height=18,;
    Caption="Non indicando nessun filtro verranno considerati tutti i documenti."  ;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscv_k27','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gscv_k27
func NoteUtilizzo()
local L_MESSAGGIO
L_MESSAGGIO = AH_MSGFORMAT("Attenzione, la procedura di conversione verr� eseguita esclusivamente per quei documenti con data registrazione compresa nell'intervallo di date")
L_MESSAGGIO = L_MESSAGGIO + ". " + AH_MSGFORMAT("Non indicando nessun filtro verranno considerati tutti i documenti")
L_MESSAGGIO = L_MESSAGGIO + ". " + AH_MSGFORMAT("Impostare in maniera corretta tale intervallo in modo che i documenti importati dal modulo import vengano esclusi")
RETURN L_MESSAGGIO
endfunc
* --- Fine Area Manuale
