* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcc62                                                        *
*              Inizializzazione Classe codifica articolo di fase               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2015-09-14                                                      *
* Last revis.: 2015-10-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcc62",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcc62 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_PPMGARFS = space(1)
  * --- WorkFile variables
  ODL_MAST_idx=0
  ODL_CICL_idx=0
  DIC_PROD_idx=0
  CCF_MAST_idx=0
  CCF_DETT_idx=0
  CIC_MAST_idx=0
  PAR_PROD_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- 8.0 - Inizializzazione CLassi codice di fase CCF_MAST e CCF_DETT
    * --- FIle di LOG
    * --- Try
    local bErr_0389B6C8
    bErr_0389B6C8=bTrsErr
    this.Try_0389B6C8()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("Errore generico - impossibile eseguire aggiornamento richiesto")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_0389B6C8
    * --- End
  endproc
  proc Try_0389B6C8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    ah_msg("Inizializzazione CCF_MAST in corso..." )
    * --- Inizializzo CCF_MAST e CCF_DETT (Classe codice di fase)
    * --- Insert into CCF_MAST
    i_nConn=i_TableProp[this.CCF_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CCF_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CCF_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CFCODICE"+",CFDESCRI"+",CFINCREM"+",CFSEPARA"+",CFTIPCLA"+",CFSEPASN"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC("ZZART"),'CCF_MAST','CFCODICE');
      +","+cp_NullLink(cp_ToStrODBC("Codifica articolo di fase"),'CCF_MAST','CFDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(0),'CCF_MAST','CFINCREM');
      +","+cp_NullLink(cp_ToStrODBC("."),'CCF_MAST','CFSEPARA');
      +","+cp_NullLink(cp_ToStrODBC("A"),'CCF_MAST','CFTIPCLA');
      +","+cp_NullLink(cp_ToStrODBC("S"),'CCF_MAST','CFSEPASN');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CFCODICE',"ZZART",'CFDESCRI',"Codifica articolo di fase",'CFINCREM',0,'CFSEPARA',".",'CFTIPCLA',"A",'CFSEPASN',"S")
      insert into (i_cTable) (CFCODICE,CFDESCRI,CFINCREM,CFSEPARA,CFTIPCLA,CFSEPASN &i_ccchkf. );
         values (;
           "ZZART";
           ,"Codifica articolo di fase";
           ,0;
           ,".";
           ,"A";
           ,"S";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_msg("Inizializzazione CCF_DETT in corso..." )
    * --- Insert into CCF_DETT
    i_nConn=i_TableProp[this.CCF_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CCF_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CCF_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CFCODICE"+",CPROWNUM"+",CFETICHE"+",CFPOSIZI"+",CFLUNGHE"+",CF_START"+",CF__FUNC"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC("ZZART"),'CCF_DETT','CFCODICE');
      +","+cp_NullLink(cp_ToStrODBC(1),'CCF_DETT','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC("Articolo"),'CCF_DETT','CFETICHE');
      +","+cp_NullLink(cp_ToStrODBC(10),'CCF_DETT','CFPOSIZI');
      +","+cp_NullLink(cp_ToStrODBC(12),'CCF_DETT','CFLUNGHE');
      +","+cp_NullLink(cp_ToStrODBC(" "),'CCF_DETT','CF_START');
      +","+cp_NullLink(cp_ToStrODBC("A"),'CCF_DETT','CF__FUNC');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CFCODICE',"ZZART",'CPROWNUM',1,'CFETICHE',"Articolo",'CFPOSIZI',10,'CFLUNGHE',12,'CF_START'," ",'CF__FUNC',"A")
      insert into (i_cTable) (CFCODICE,CPROWNUM,CFETICHE,CFPOSIZI,CFLUNGHE,CF_START,CF__FUNC &i_ccchkf. );
         values (;
           "ZZART";
           ,1;
           ,"Articolo";
           ,10;
           ,12;
           ," ";
           ,"A";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Insert into CCF_DETT
    i_nConn=i_TableProp[this.CCF_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CCF_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CCF_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CFCODICE"+",CPROWNUM"+",CFETICHE"+",CFPOSIZI"+",CFLUNGHE"+",CF_START"+",CF__FUNC"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC("ZZART"),'CCF_DETT','CFCODICE');
      +","+cp_NullLink(cp_ToStrODBC(2),'CCF_DETT','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC("Codice ciclo"),'CCF_DETT','CFETICHE');
      +","+cp_NullLink(cp_ToStrODBC(20),'CCF_DETT','CFPOSIZI');
      +","+cp_NullLink(cp_ToStrODBC(3),'CCF_DETT','CFLUNGHE');
      +","+cp_NullLink(cp_ToStrODBC(" "),'CCF_DETT','CF_START');
      +","+cp_NullLink(cp_ToStrODBC("C"),'CCF_DETT','CF__FUNC');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CFCODICE',"ZZART",'CPROWNUM',2,'CFETICHE',"Codice ciclo",'CFPOSIZI',20,'CFLUNGHE',3,'CF_START'," ",'CF__FUNC',"C")
      insert into (i_cTable) (CFCODICE,CPROWNUM,CFETICHE,CFPOSIZI,CFLUNGHE,CF_START,CF__FUNC &i_ccchkf. );
         values (;
           "ZZART";
           ,2;
           ,"Codice ciclo";
           ,20;
           ,3;
           ," ";
           ,"C";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Insert into CCF_DETT
    i_nConn=i_TableProp[this.CCF_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CCF_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CCF_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CFCODICE"+",CPROWNUM"+",CFETICHE"+",CFPOSIZI"+",CFLUNGHE"+",CF_START"+",CF__FUNC"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC("ZZART"),'CCF_DETT','CFCODICE');
      +","+cp_NullLink(cp_ToStrODBC(3),'CCF_DETT','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC("Fase"),'CCF_DETT','CFETICHE');
      +","+cp_NullLink(cp_ToStrODBC(30),'CCF_DETT','CFPOSIZI');
      +","+cp_NullLink(cp_ToStrODBC(3),'CCF_DETT','CFLUNGHE');
      +","+cp_NullLink(cp_ToStrODBC(" "),'CCF_DETT','CF_START');
      +","+cp_NullLink(cp_ToStrODBC("F"),'CCF_DETT','CF__FUNC');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CFCODICE',"ZZART",'CPROWNUM',3,'CFETICHE',"Fase",'CFPOSIZI',30,'CFLUNGHE',3,'CF_START'," ",'CF__FUNC',"F")
      insert into (i_cTable) (CFCODICE,CPROWNUM,CFETICHE,CFPOSIZI,CFLUNGHE,CF_START,CF__FUNC &i_ccchkf. );
         values (;
           "ZZART";
           ,3;
           ,"Fase";
           ,30;
           ,3;
           ," ";
           ,"F";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Verifico la presenza di almeno un codice di fase generato
    * --- Se � presente almeno un codice di fase continuo ad utilizzare il prefisso specificato nei parametri di produzione
    this.w_PPMGARFS = "A"
    * --- Select from GSCVCC62
    do vq_exec with 'GSCVCC62',this,'_Curs_GSCVCC62','',.f.,.t.
    if used('_Curs_GSCVCC62')
      select _Curs_GSCVCC62
      locate for 1=1
      do while not(eof())
      this.w_PPMGARFS = "C"
        select _Curs_GSCVCC62
        continue
      enddo
      use
    endif
    * --- Write into CIC_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.CIC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CIC_MAST_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="CLSERIAL"
      do vq_exec with 'GSCVCC622',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.CIC_MAST_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="CIC_MAST.CLSERIAL = _t2.CLSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CLMGARFS = _t2.CLMGARFS";
          +",CLPREFAS = _t2.CLPREFAS";
          +",CLFASSEP = _t2.CLFASSEP";
          +i_ccchkf;
          +" from "+i_cTable+" CIC_MAST, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="CIC_MAST.CLSERIAL = _t2.CLSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CIC_MAST, "+i_cQueryTable+" _t2 set ";
          +"CIC_MAST.CLMGARFS = _t2.CLMGARFS";
          +",CIC_MAST.CLPREFAS = _t2.CLPREFAS";
          +",CIC_MAST.CLFASSEP = _t2.CLFASSEP";
          +Iif(Empty(i_ccchkf),"",",CIC_MAST.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="CIC_MAST.CLSERIAL = t2.CLSERIAL";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CIC_MAST set (";
          +"CLMGARFS,";
          +"CLPREFAS,";
          +"CLFASSEP";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.CLMGARFS,";
          +"t2.CLPREFAS,";
          +"t2.CLFASSEP";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="CIC_MAST.CLSERIAL = _t2.CLSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CIC_MAST set ";
          +"CLMGARFS = _t2.CLMGARFS";
          +",CLPREFAS = _t2.CLPREFAS";
          +",CLFASSEP = _t2.CLFASSEP";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".CLSERIAL = "+i_cQueryTable+".CLSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CLMGARFS = (select CLMGARFS from "+i_cQueryTable+" where "+i_cWhere+")";
          +",CLPREFAS = (select CLPREFAS from "+i_cQueryTable+" where "+i_cWhere+")";
          +",CLFASSEP = (select CLFASSEP from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Inserisco la Classe codice di fase di default in PAR_PROD
    *     Aggiorno PPMGARFS (Modalit� di generazione dei codifci di fase)
    * --- Write into PAR_PROD
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PAR_PROD_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROD_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_PROD_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PPCLAART ="+cp_NullLink(cp_ToStrODBC("ZZART"),'PAR_PROD','PPCLAART');
      +",PPMGARFS ="+cp_NullLink(cp_ToStrODBC(this.w_PPMGARFS),'PAR_PROD','PPMGARFS');
          +i_ccchkf ;
      +" where ";
          +"PPCODICE = "+cp_ToStrODBC("PP");
             )
    else
      update (i_cTable) set;
          PPCLAART = "ZZART";
          ,PPMGARFS = this.w_PPMGARFS;
          &i_ccchkf. ;
       where;
          PPCODICE = "PP";

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    this.w_TMPC = ah_Msgformat("Aggiornamento eseguito con successo")
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = this.w_TMPC
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,7)]
    this.cWorkTables[1]='ODL_MAST'
    this.cWorkTables[2]='ODL_CICL'
    this.cWorkTables[3]='DIC_PROD'
    this.cWorkTables[4]='CCF_MAST'
    this.cWorkTables[5]='CCF_DETT'
    this.cWorkTables[6]='CIC_MAST'
    this.cWorkTables[7]='PAR_PROD'
    return(this.OpenAllTables(7))

  proc CloseCursors()
    if used('_Curs_GSCVCC62')
      use in _Curs_GSCVCC62
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
