* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvab81                                                        *
*              Aggiorna campo mvcodag2 (doc_mast)                              *
*                                                                              *
*      Author: Zucchetti S.p.A.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_55]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-06-18                                                      *
* Last revis.: 2001-06-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvab81",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvab81 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_SERIALE = space(10)
  w_CODAGENT = space(5)
  w_MVCODAG2 = space(5)
  w_MESS = space(100)
  * --- WorkFile variables
  DOC_MAST_idx=0
  AGENTI_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questa procedura valorizza il campo 'MVCODAG2' della tabella DOC_MAST per gli ordini provenienti dal Web
    * --- FIle di LOG
    * --- Try
    local bErr_035F47A0
    bErr_035F47A0=bTrsErr
    this.Try_035F47A0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE GENERICO - impossibile aggiornare il campo MVCODAG2 tabella DOC_MAST")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_035F47A0
    * --- End
  endproc
  proc Try_035F47A0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Vado a estrarre dala tabella DOC_MAST gli ordini WEB che hanno il campo 'Capo Area' vuoto e l'agente valorizzato
    *     
    vq_exec("GSCVAB81.vqr",this,"OrdiniWeb")
    if USED("OrdiniWeb")
      if Reccount ("OrdiniWeb")>0
        Select OrdiniWeb
        Go Top
        Scan
        this.w_SERIALE = ALLTRIM(NVL(OrdiniWeb.MVSERIAL,""))
        this.w_CODAGENT = ALLTRIM(NVL(OrdiniWeb.MVCODAGE," "))
        * --- Leggo il capo area associato all'agente
        * --- Read from AGENTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.AGENTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.AGENTI_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "AGCZOAGE"+;
            " from "+i_cTable+" AGENTI where ";
                +"AGCODAGE = "+cp_ToStrODBC(this.w_CODAGENT);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            AGCZOAGE;
            from (i_cTable) where;
                AGCODAGE = this.w_CODAGENT;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_MVCODAG2 = NVL(cp_ToDate(_read_.AGCZOAGE),cp_NullValue(_read_.AGCZOAGE))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if NOT EMPTY(this.w_MVCODAG2)
          * --- Effettuo l'aggiornamento nella tabella DOC_MAST
          * --- Write into DOC_MAST
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.DOC_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MVCODAG2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODAG2),'DOC_MAST','MVCODAG2');
                +i_ccchkf ;
            +" where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIALE);
                   )
          else
            update (i_cTable) set;
                MVCODAG2 = this.w_MVCODAG2;
                &i_ccchkf. ;
             where;
                MVSERIAL = this.w_SERIALE;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        Endscan
        * --- Esecuzione ok
        if this.w_NHF>=0
          this.w_TMPC = ah_Msgformat("Aggiornamento campo MVCODAG2 eseguito correttamente")
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
        this.oParentObject.w_PESEOK = .T.
        this.oParentObject.w_PMSG = ah_Msgformat("Conversione eseguita correttamente")
      else
        * --- Gestisce log errori
        if this.w_NHF>=0
          this.w_TMPC = ah_Msgformat("ATTENZIONE: non esistono dati da convertire")
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
        * --- Esecuzione ok
        this.oParentObject.w_PESEOK = .T.
        this.oParentObject.w_PMSG = this.w_TMPC
      endif
      * --- Chiude cursore
      select OrdiniWeb
      use
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='DOC_MAST'
    this.cWorkTables[2]='AGENTI'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
