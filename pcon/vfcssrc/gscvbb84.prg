* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvbb84                                                        *
*              Perdispone archivi EDI per utilizzo tabelle piatte              *
*                                                                              *
*      Author: Zucchetti TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_77]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-05-07                                                      *
* Last revis.: 2008-07-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvbb84",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvbb84 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_FTCODICE = space(15)
  w_FTPHNAME = space(30)
  w_FTDESCRI = space(50)
  w_FTCODENT = space(15)
  w_CONTA = 0
  w_CAMPO = space(30)
  w_CODTAB = space(30)
  w_INDICE = 0
  w_FTFLDTYP = space(1)
  w_FTFLDDIM = 0
  w_FTFLDDEC = 0
  w_FTFLDDES = space(50)
  w_OKSTRUT = .f.
  w_OLDTAB = space(30)
  w_POSKEY = 0
  w_KEY_FLDS = space(100)
  w_EXPRES = space(254)
  w_INDICE = 0
  * --- WorkFile variables
  AZIENDA_idx=0
  FLATMAST_idx=0
  FLATDETT_idx=0
  VASTRUTT_idx=0
  TMPZ_idx=0
  XDC_FIELDS_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Perdispone archivi EDI per utilizzo tabelle piatte
    this.w_CONTA = 0
    this.w_OKSTRUT = .F.
    * --- Try
    local bErr_03775BB0
    bErr_03775BB0=bTrsErr
    this.Try_03775BB0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Gestisce log errori
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_msgformat("ERRORE GENERICO - Impossibile eseguire aggiornamento richiesto")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_03775BB0
    * --- End
    * --- Esecuzione ok
    * --- Lancio aggiornamento tabelle
    * --- Try
    local bErr_0361D8E0
    bErr_0361D8E0=bTrsErr
    this.Try_0361D8E0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_msgformat("ERRORE GENERICO - Impossibile eseguire aggiornamento richiesto")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_0361D8E0
    * --- End
    * --- Drop temporary table TMPZ
    i_nIdx=cp_GetTableDefIdx('TMPZ')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPZ')
    endif
  endproc
  proc Try_03775BB0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Cancellazione tabelle clone
    * --- begin transaction
    cp_BeginTrs()
    * --- Verifico presenza strutture
    * --- Select from VASTRUTT
    i_nConn=i_TableProp[this.VASTRUTT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VASTRUTT_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select STCODICE  from "+i_cTable+" VASTRUTT ";
           ,"_Curs_VASTRUTT")
    else
      select STCODICE from (i_cTable);
        into cursor _Curs_VASTRUTT
    endif
    if used('_Curs_VASTRUTT')
      select _Curs_VASTRUTT
      locate for 1=1
      do while not(eof())
      this.w_OKSTRUT = .T.
      exit
        select _Curs_VASTRUTT
        continue
      enddo
      use
    endif
    if this.w_OKSTRUT
      * --- Select from gscvbb84_2
      do vq_exec with 'gscvbb84_2',this,'_Curs_gscvbb84_2','',.f.,.t.
      if used('_Curs_gscvbb84_2')
        select _Curs_gscvbb84_2
        locate for 1=1
        do while not(eof())
        this.w_CODTAB = GETCLONENAME(_Curs_gscvbb84_2.ENTABLE)
        if cp_ExistTable(Alltrim(this.w_CODTAB),i_ServerConn[1,2])
          if Cp_Sql(i_ServerConn[1,2],"Drop Table " + this.w_CODTAB)<0
            * --- Raise
            i_Error="Errore nella cancellazione"
            return
            if this.w_NHF>=0
              this.w_TMPC = ah_msgformat("Errore nella cancellazione tabella clone %1",Alltrim(this.w_CODTAB))
              this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
            endif
          endif
        endif
          select _Curs_gscvbb84_2
          continue
        enddo
        use
      endif
      if type("i_dcx")="U"
        * --- Lettura dizionario dati da XDC
        cp_ReadXDC()
      endif
      * --- Creo definizione tabella piatta
      this.w_FTCODICE = "TABDOC"
      this.w_FTPHNAME = GetFTPhName("xxx"+ALLTRIM(this.w_FTCODICE)) 
      this.w_FTDESCRI = "Tabella di appoggio documenti"
      this.w_FTCODENT = "DOCUM_VEFA"
      * --- Insert into FLATMAST
      i_nConn=i_TableProp[this.FLATMAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.FLATMAST_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.FLATMAST_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"FTCODICE"+",FTDESCRI"+",FTCODENT"+",FTFL_AZI"+",FT___UID"+",FTOBJUID"+",FTROWORD"+",FTSTUCID"+",FTSTATUS"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_FTCODICE),'FLATMAST','FTCODICE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_FTDESCRI),'FLATMAST','FTDESCRI');
        +","+cp_NullLink(cp_ToStrODBC(this.w_FTCODENT),'FLATMAST','FTCODENT');
        +","+cp_NullLink(cp_ToStrODBC("S"),'FLATMAST','FTFL_AZI');
        +","+cp_NullLink(cp_ToStrODBC("FT___UID"),'FLATMAST','FT___UID');
        +","+cp_NullLink(cp_ToStrODBC("FTOBJUID"),'FLATMAST','FTOBJUID');
        +","+cp_NullLink(cp_ToStrODBC("FTROWORD"),'FLATMAST','FTROWORD');
        +","+cp_NullLink(cp_ToStrODBC("FTSTUCID"),'FLATMAST','FTSTUCID');
        +","+cp_NullLink(cp_ToStrODBC("FTSTATUS"),'FLATMAST','FTSTATUS');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'FTCODICE',this.w_FTCODICE,'FTDESCRI',this.w_FTDESCRI,'FTCODENT',this.w_FTCODENT,'FTFL_AZI',"S",'FT___UID',"FT___UID",'FTOBJUID',"FTOBJUID",'FTROWORD',"FTROWORD",'FTSTUCID',"FTSTUCID",'FTSTATUS',"FTSTATUS")
        insert into (i_cTable) (FTCODICE,FTDESCRI,FTCODENT,FTFL_AZI,FT___UID,FTOBJUID,FTROWORD,FTSTUCID,FTSTATUS &i_ccchkf. );
           values (;
             this.w_FTCODICE;
             ,this.w_FTDESCRI;
             ,this.w_FTCODENT;
             ,"S";
             ,"FT___UID";
             ,"FTOBJUID";
             ,"FTROWORD";
             ,"FTSTUCID";
             ,"FTSTATUS";
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      this.w_OLDTAB = Repl("@",30)
      * --- Create temporary table TMPZ
      i_nIdx=cp_AddTableDef('TMPZ') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('GSCVBB84',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.TMPZ_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      * --- Select from GSCVBB84_3
      do vq_exec with 'GSCVBB84_3',this,'_Curs_GSCVBB84_3','',.f.,.t.
      if used('_Curs_GSCVBB84_3')
        select _Curs_GSCVBB84_3
        locate for 1=1
        do while not(eof())
        this.w_CODTAB = _Curs_GSCVBB84_3.CODTAB
        if this.w_OLDTAB<>this.w_CODTAB
          this.w_KEY_FLDS = cp_KeyToSQL ( I_DCX.GetIdxDef( this.w_CODTAB ,1 ) )
          this.w_KEY_FLDS = ALLTRIM(this.w_KEY_FLDS)
          this.w_POSKEY = iif(At( "," , this.w_KEY_FLDS )>0,At( "," , this.w_KEY_FLDS ),1)
          do while  this.w_POSKEY<>0
            * --- Inserisco campi chiave Tabella
            this.w_CONTA = this.w_CONTA + 1
            this.w_POSKEY = At( "," , this.w_KEY_FLDS )
            if this.w_POSKEY>0
              this.w_CAMPO = Left( this.w_KEY_FLDS , this.w_POSKEY -1 )
            else
              this.w_CAMPO = Alltrim( this.w_KEY_FLDS )
            endif
            * --- Read from XDC_FIELDS
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.XDC_FIELDS_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.XDC_FIELDS_idx,2])
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "FLNAME"+;
                " from "+i_cTable+" XDC_FIELDS where ";
                    +"TBNAME = "+cp_ToStrODBC(this.w_CODTAB);
                    +" and FLNAME = "+cp_ToStrODBC(this.w_CAMPO);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                FLNAME;
                from (i_cTable) where;
                    TBNAME = this.w_CODTAB;
                    and FLNAME = this.w_CAMPO;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_CAMPO = NVL(cp_ToDate(_read_.FLNAME),cp_NullValue(_read_.FLNAME))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if i_ROWS<>0
              * --- Insert into TMPZ
              i_nConn=i_TableProp[this.TMPZ_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.TMPZ_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_ccchkf=''
              i_ccchkv=''
              this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMPZ_idx,i_nConn)
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                            " ("+"CODTAB"+",CAMPO"+i_ccchkf+") values ("+;
                cp_NullLink(cp_ToStrODBC(this.w_CODTAB),'TMPZ','CODTAB');
                +","+cp_NullLink(cp_ToStrODBC(this.w_CAMPO),'TMPZ','CAMPO');
                     +i_ccchkv+")")
              else
                cp_CheckDeletedKey(i_cTable,0,'CODTAB',this.w_CODTAB,'CAMPO',this.w_CAMPO)
                insert into (i_cTable) (CODTAB,CAMPO &i_ccchkf. );
                   values (;
                     this.w_CODTAB;
                     ,this.w_CAMPO;
                     &i_ccchkv. )
                i_Rows=iif(bTrsErr,0,1)
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if i_Rows<0 or bTrsErr
                * --- Error: insert not accepted
                i_Error=MSG_INSERT_ERROR
                return
              endif
            endif
            this.w_INDICE = cp_ROUND(ASCAN(i_dcx.FLDS,Alltrim(this.w_CAMPO))/3,0)+1
            this.w_KEY_FLDS = SUBSTR(this.w_KEY_FLDS,this.w_POSKEY+1,LEN(this.w_KEY_FLDS)-this.w_POSKEY)
          enddo
        endif
        * --- Select from GSCV_BVS
        do vq_exec with 'GSCV_BVS',this,'_Curs_GSCV_BVS','',.f.,.t.
        if used('_Curs_GSCV_BVS')
          select _Curs_GSCV_BVS
          locate for 1=1
          do while not(eof())
          this.w_EXPRES = _Curs_GSCV_BVS.ELEXPRES
          this.w_INDICE = 0
          do while occurs(ALLTRIM(this.w_CODTAB)+".",this.w_EXPRES)>0 AND this.w_INDICE<100
            this.w_CAMPO = SUBSTR(this.w_EXPRES,rat(this.w_CODTAB,this.w_EXPRES)+LEN(this.w_CODTAB)+1,8)
            * --- Verifico esistenza del campo calcolato
            * --- Read from XDC_FIELDS
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.XDC_FIELDS_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.XDC_FIELDS_idx,2])
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "FLNAME"+;
                " from "+i_cTable+" XDC_FIELDS where ";
                    +"TBNAME = "+cp_ToStrODBC(this.w_CODTAB);
                    +" and FLNAME = "+cp_ToStrODBC(this.w_CAMPO);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                FLNAME;
                from (i_cTable) where;
                    TBNAME = this.w_CODTAB;
                    and FLNAME = this.w_CAMPO;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_CAMPO = NVL(cp_ToDate(_read_.FLNAME),cp_NullValue(_read_.FLNAME))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if i_ROWS<>0
              * --- Insert into TMPZ
              i_nConn=i_TableProp[this.TMPZ_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.TMPZ_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_ccchkf=''
              i_ccchkv=''
              this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMPZ_idx,i_nConn)
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                            " ("+"CODTAB"+",CAMPO"+i_ccchkf+") values ("+;
                cp_NullLink(cp_ToStrODBC(this.w_CODTAB),'TMPZ','CODTAB');
                +","+cp_NullLink(cp_ToStrODBC(this.w_CAMPO),'TMPZ','CAMPO');
                     +i_ccchkv+")")
              else
                cp_CheckDeletedKey(i_cTable,0,'CODTAB',this.w_CODTAB,'CAMPO',this.w_CAMPO)
                insert into (i_cTable) (CODTAB,CAMPO &i_ccchkf. );
                   values (;
                     this.w_CODTAB;
                     ,this.w_CAMPO;
                     &i_ccchkv. )
                i_Rows=iif(bTrsErr,0,1)
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if i_Rows<0 or bTrsErr
                * --- Error: insert not accepted
                i_Error=MSG_INSERT_ERROR
                return
              endif
            endif
            this.w_EXPRES = STRTRAN(this.w_EXPRES,Alltrim(this.w_CODTAB)+"."+Alltrim(this.w_CAMPO),"")
            this.w_INDICE = this.w_INDICE + 1
          enddo
            select _Curs_GSCV_BVS
            continue
          enddo
          use
        endif
        this.w_OLDTAB = this.w_CODTAB
          select _Curs_GSCVBB84_3
          continue
        enddo
        use
      endif
      * --- Select from GSCVBB84_4
      do vq_exec with 'GSCVBB84_4',this,'_Curs_GSCVBB84_4','',.f.,.t.
      if used('_Curs_GSCVBB84_4')
        select _Curs_GSCVBB84_4
        locate for 1=1
        do while not(eof())
        this.w_CONTA = this.w_CONTA + 1
        this.w_CAMPO = _Curs_GSCVBB84_4.CAMPO
        this.w_CODTAB = _Curs_GSCVBB84_4.CODTAB
        this.w_INDICE = cp_ROUND(ASCAN(i_dcx.FLDS,Alltrim(this.w_CAMPO))/3,0)+1
        this.w_FTFLDTYP = i_dcx.flds[this.w_INDICE,2]
        this.w_FTFLDDIM = i_dcx.fldsLen[this.w_INDICE]
        this.w_FTFLDDEC = i_dcx.fldsDec[this.w_INDICE]
        this.w_FTFLDDES = i_dcx.fldscomment[this.w_INDICE]
        * --- Insert into FLATDETT
        i_nConn=i_TableProp[this.FLATDETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.FLATDETT_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.FLATDETT_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"FTCODICE"+",FTTABNAM"+",FTFLDNAM"+",FTFLDTYP"+",FTFLDDIM"+",FTFLDDEC"+",FTFLDDES"+",CPROWORD"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_FTCODICE),'FLATDETT','FTCODICE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CODTAB),'FLATDETT','FTTABNAM');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CAMPO),'FLATDETT','FTFLDNAM');
          +","+cp_NullLink(cp_ToStrODBC(this.w_FTFLDTYP),'FLATDETT','FTFLDTYP');
          +","+cp_NullLink(cp_ToStrODBC(this.w_FTFLDDIM),'FLATDETT','FTFLDDIM');
          +","+cp_NullLink(cp_ToStrODBC(this.w_FTFLDDEC),'FLATDETT','FTFLDDEC');
          +","+cp_NullLink(cp_ToStrODBC(this.w_FTFLDDES),'FLATDETT','FTFLDDES');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CONTA*10),'FLATDETT','CPROWORD');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'FTCODICE',this.w_FTCODICE,'FTTABNAM',this.w_CODTAB,'FTFLDNAM',this.w_CAMPO,'FTFLDTYP',this.w_FTFLDTYP,'FTFLDDIM',this.w_FTFLDDIM,'FTFLDDEC',this.w_FTFLDDEC,'FTFLDDES',this.w_FTFLDDES,'CPROWORD',this.w_CONTA*10)
          insert into (i_cTable) (FTCODICE,FTTABNAM,FTFLDNAM,FTFLDTYP,FTFLDDIM,FTFLDDEC,FTFLDDES,CPROWORD &i_ccchkf. );
             values (;
               this.w_FTCODICE;
               ,this.w_CODTAB;
               ,this.w_CAMPO;
               ,this.w_FTFLDTYP;
               ,this.w_FTFLDDIM;
               ,this.w_FTFLDDEC;
               ,this.w_FTFLDDES;
               ,this.w_CONTA*10;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
          select _Curs_GSCVBB84_4
          continue
        enddo
        use
      endif
      * --- Aggiorno campi nuovi strutture
      * --- Write into VASTRUTT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.VASTRUTT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VASTRUTT_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.VASTRUTT_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"STBACIMP ="+cp_NullLink(cp_ToStrODBC("GSVA_BCD"),'VASTRUTT','STBACIMP');
        +",STBACXML ="+cp_NullLink(cp_ToStrODBC("GSVA_BXM"),'VASTRUTT','STBACXML');
        +",STDRIVER ="+cp_NullLink(cp_ToStrODBC("GSVA_BDR "),'VASTRUTT','STDRIVER');
        +",STFUNRIC ="+cp_NullLink(cp_ToStrODBC("GETOBJXML"),'VASTRUTT','STFUNRIC');
        +",STMODMSK ="+cp_NullLink(cp_ToStrODBC("GSVA_BI3"),'VASTRUTT','STMODMSK');
        +",STFLATAB ="+cp_NullLink(cp_ToStrODBC("TABDOC"),'VASTRUTT','STFLATAB');
            +i_ccchkf ;
        +" where ";
            +"1 = "+cp_ToStrODBC(1);
               )
      else
        update (i_cTable) set;
            STBACIMP = "GSVA_BCD";
            ,STBACXML = "GSVA_BXM";
            ,STDRIVER = "GSVA_BDR ";
            ,STFUNRIC = "GETOBJXML";
            ,STMODMSK = "GSVA_BI3";
            ,STFLATAB = "TABDOC";
            &i_ccchkf. ;
         where;
            1 = 1;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_0361D8E0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    gsva_bcx(this,this.w_FTCODICE)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    cp_readxdc()
    vq_exec("..\PCON\EXE\QUERY\GSCV_KFT",this,"TABELLE")
    gsva_bft(this,"SAVE","FC",.T.)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    vq_exec("..\PCON\EXE\QUERY\GSCVEKFT",this,"TABELLE")
    gsva_bft(this,"SAVE","EC",.T.)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if Used("TABELLE")
       
 Select Tabelle 
 Use
    endif
    this.oParentObject.w_PESEOK = .T.
    this.w_TMPC = ah_msgformat("Aggiornamenti eseguiti con successo")
    this.oParentObject.w_PMSG = this.w_TMPC
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if w_ROWS<0
      * --- Raise
      i_Error=Message()
      return
    else
      * --- Esecuzione ok
      this.oParentObject.w_PESEOK = .T.
      this.w_TMPC = ah_msgformat("Eliminazione campo %1 eseguita con successo%0",arfield[w_loop])
      this.oParentObject.w_PMSG = this.oParentObject.w_PMSG + this.w_TMPC
    endif
  endproc


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,6)]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='FLATMAST'
    this.cWorkTables[3]='FLATDETT'
    this.cWorkTables[4]='VASTRUTT'
    this.cWorkTables[5]='*TMPZ'
    this.cWorkTables[6]='XDC_FIELDS'
    return(this.OpenAllTables(6))

  proc CloseCursors()
    if used('_Curs_VASTRUTT')
      use in _Curs_VASTRUTT
    endif
    if used('_Curs_gscvbb84_2')
      use in _Curs_gscvbb84_2
    endif
    if used('_Curs_GSCVBB84_3')
      use in _Curs_GSCVBB84_3
    endif
    if used('_Curs_GSCV_BVS')
      use in _Curs_GSCV_BVS
    endif
    if used('_Curs_GSCVBB84_4')
      use in _Curs_GSCVBB84_4
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
