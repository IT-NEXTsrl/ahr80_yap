* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsek_bce                                                        *
*              Chiusura conto economico                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_21]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-04                                                      *
* Last revis.: 2007-05-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsek_bce",oParentObject)
return(i_retval)

define class tgsek_bce as StdBatch
  * --- Local variables
  w_STALIG = ctod("  /  /  ")
  w_DATBLO = ctod("  /  /  ")
  w_PNSERIAL = space(10)
  w_PNDATREG = ctod("  /  /  ")
  w_PNCODESE = space(4)
  w_PNCODUTE = 0
  w_PNNUMRER = 0
  w_PNCODVAL = space(3)
  w_PNVALNAZ = space(3)
  w_PNCODCAU = space(5)
  w_PNTIPREG = space(2)
  w_PNCOMPET = space(4)
  w_PNFLZERO = space(1)
  w_UTCC = 0
  w_PNCAOVAL = 0
  w_PNDESSUP = space(50)
  w_UTDV = ctod("  /  /  ")
  w_SLDARPER = 0
  w_INIREG = .f.
  w_UTCV = 0
  w_CONSUP = space(15)
  w_PNTIPCON = space(1)
  w_UTDC = ctod("  /  /  ")
  w_SLCODICE = space(15)
  w_SLAVEPRO = 0
  w_PNIMPDAR = 0
  w_SLTIPCON = space(1)
  w_SLDARPRO = 0
  w_PNCAURIG = space(5)
  w_PNFLSALF = space(1)
  w_SLAVEPER = 0
  w_APPO = 0
  w_TOTRIG = 0
  w_PNFLSALI = space(1)
  w_PNCODCON = space(15)
  w_PNDESRIG = space(50)
  w_SALDO = 0
  w_PNIMPAVE = 0
  w_CPROWORD = 0
  w_PNPRG = space(8)
  w_PNFLSALD = space(1)
  w_CPROWNUM = 0
  w_MESS = space(50)
  * --- WorkFile variables
  AZIENDA_idx=0
  CONTI_idx=0
  ESERCIZI_idx=0
  PNT_DETT_idx=0
  PNT_MAST_idx=0
  SALDICON_idx=0
  VALUTE_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Chiusura Conto Economico per Euro - KIT (da GSEK_KCE)
    * --- Eliminata la Fase di Aggiornamento rilevazione Utile/Perdita 
    * --- Variabili per l'Utile Perdita Esercizio
    * --- Variabili Locali
    * --- Controllo se non vi siano altre registrazioni con la causale scelta per la chiusura
    * --- nell'esercizio - La Solita Query � utilizzata nell'Apertura di Esercizio
    * --- Select from GSCG_BCE
    do vq_exec with 'GSCG_BCE',this,'_Curs_GSCG_BCE','',.f.,.t.
    if used('_Curs_GSCG_BCE')
      select _Curs_GSCG_BCE
      locate for 1=1
      do while not(eof())
      if NOT EMPTY(NVL(_Curs_GSCG_BCE.PNSERIAL,""))
        this.w_PNSERIAL = _Curs_GSCG_BCE.PNSERIAL
        * --- Select from PNT_DETT
        i_nConn=i_TableProp[this.PNT_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select PNTIPCON,PNCODCON  from "+i_cTable+" PNT_DETT ";
              +" where PNSERIAL="+cp_ToStrODBC(this.w_PNSERIAL)+"";
               ,"_Curs_PNT_DETT")
        else
          select PNTIPCON,PNCODCON from (i_cTable);
           where PNSERIAL=this.w_PNSERIAL;
            into cursor _Curs_PNT_DETT
        endif
        if used('_Curs_PNT_DETT')
          select _Curs_PNT_DETT
          locate for 1=1
          do while not(eof())
          this.w_PNTIPCON = NVL(_Curs_PNT_DETT.PNTIPCON, " ")
          this.w_PNCODCON = NVL(_Curs_PNT_DETT.PNCODCON," ")
          if (NOT EMPTY(this.w_PNTIPCON)) AND (NOT EMPTY(this.w_PNCODCON))
            * --- Verifica se Conto Economico  (SEZBIL C o R)
            this.w_CONSUP = SPACE(15)
            * --- Read from CONTI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.CONTI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "ANCONSUP"+;
                " from "+i_cTable+" CONTI where ";
                    +"ANTIPCON = "+cp_ToStrODBC(this.w_PNTIPCON);
                    +" and ANCODICE = "+cp_ToStrODBC(this.w_PNCODCON);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                ANCONSUP;
                from (i_cTable) where;
                    ANTIPCON = this.w_PNTIPCON;
                    and ANCODICE = this.w_PNCODCON;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_CONSUP = NVL(cp_ToDate(_read_.ANCONSUP),cp_NullValue(_read_.ANCONSUP))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if CALCSEZ(this.w_CONSUP) $ "CR"
              ah_ErrorMsg("In prima nota, per l'esecizio da chiudere, sono gi� presenti registrazioni di chiusura c.economici%0� necessario annullarle prima di procedere",,"")
              i_retcode = 'stop'
              return
            endif
          endif
            select _Curs_PNT_DETT
            continue
          enddo
          use
        endif
      endif
        select _Curs_GSCG_BCE
        continue
      enddo
      use
    endif
    * --- blocco Prima Nota
    * --- Try
    local bErr_0361A7C0
    bErr_0361A7C0=bTrsErr
    this.Try_0361A7C0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      do case
        case i_ErrMsg="data"
          ah_ErrorMsg("Data registrazione inferiore o uguale a ultima stampa libro giornale",,"")
        case i_ErrMsg="giornale"
          ah_ErrorMsg("Prima nota bloccata - semaforo bollati in dati azienda -. Operazione annullata",,"")
        case i_ErrMsg=""
          ah_ErrorMsg("Impossibile bloccare la prima nota - semaforo bollati in dati azienda -. Operazione annullata",,"")
      endcase
      i_retcode = 'stop'
      return
    endif
    bTrsErr=bTrsErr or bErr_0361A7C0
    * --- End
    * --- Aggiorna Variabili Primanota
    this.w_PNCAOVAL = g_CAOVAL
    this.w_PNVALNAZ = this.oParentObject.w_VALNAZ
    this.w_PNCOMPET = this.oParentObject.w_CODESE
    this.w_PNDESSUP = this.oParentObject.w_DESSUP
    this.w_PNDATREG = this.oParentObject.w_DATREG
    this.w_UTCC = i_CODUTE
    this.w_UTDC = SetInfoDate( g_CALUTD )
    this.w_UTCV = 0
    this.w_UTDV = cp_CharToDate("  -  -    ")
    this.w_PNCODCAU = this.oParentObject.w_CODCAU
    this.w_PNTIPREG = "N"
    this.w_PNCODVAL = this.oParentObject.w_VALNAZ
    this.w_PNFLSALD = "+"
    this.w_PNCODESE = this.oParentObject.w_CODESE
    this.w_PNCODUTE = IIF(g_UNIUTE $ "UE", i_CODUTE, 0)
    this.w_PNPRG = IIF(g_UNIUTE $ "GE", DTOS(this.w_PNDATREG), SPACE(8))
    * --- Testa se effettua registrazioni
    this.w_INIREG = .F.
    this.w_TOTRIG = 0
    this.w_CPROWNUM = 0
    this.w_CPROWORD = 0
    * --- Inizia la transazione...
    * --- Try
    local bErr_037E6EA0
    bErr_037E6EA0=bTrsErr
    this.Try_037E6EA0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      ah_ErrorMsg(i_errmsg,,"")
    endif
    bTrsErr=bTrsErr or bErr_037E6EA0
    * --- End
    * --- Sblocca Primanota
    * --- Try
    local bErr_0360A0A0
    bErr_0360A0A0=bTrsErr
    this.Try_0360A0A0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      ah_ErrorMsg("Impossibile sbloccare la prima nota - semaforo bollati in dati azienda -. Sbloccarlo manualmente",,"")
    endif
    bTrsErr=bTrsErr or bErr_0360A0A0
    * --- End
  endproc
  proc Try_0361A7C0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Controlli Preliminari
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZSTALIG,AZDATBLO"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(i_codazi);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZSTALIG,AZDATBLO;
        from (i_cTable) where;
            AZCODAZI = i_codazi;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_STALIG = NVL(cp_ToDate(_read_.AZSTALIG),cp_NullValue(_read_.AZSTALIG))
      this.w_DATBLO = NVL(cp_ToDate(_read_.AZDATBLO),cp_NullValue(_read_.AZDATBLO))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    do case
      case NOT this.oParentObject.w_DATREG > this.w_STALIG
        * --- Raise
        i_Error="data"
        return
      case this.oParentObject.w_DATREG <= this.w_DATBLO AND NOT EMPTY(this.w_DATBLO)
        * --- Raise
        i_Error="giornale"
        return
    endcase
    * --- Blocca Primanota
    * --- Write into AZIENDA
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.AZIENDA_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"AZDATBLO ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_INIESE),'AZIENDA','AZDATBLO');
          +i_ccchkf ;
      +" where ";
          +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
             )
    else
      update (i_cTable) set;
          AZDATBLO = this.oParentObject.w_INIESE;
          &i_ccchkf. ;
       where;
          AZCODAZI = i_CODAZI;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_037E6EA0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VACAOVAL"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(this.oParentObject.w_VALNAZ);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VACAOVAL;
        from (i_cTable) where;
            VACODVAL = this.oParentObject.w_VALNAZ;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PNCAOVAL = NVL(cp_ToDate(_read_.VACAOVAL),cp_NullValue(_read_.VACAOVAL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Legge Saldi Conti Economici <> 0
    * --- Select from SALDICON
    i_nConn=i_TableProp[this.SALDICON_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select SLTIPCON,SLCODICE,SLDARPRO,SLAVEPRO,SLDARPER,SLAVEPER  from "+i_cTable+" SALDICON ";
          +" where SLTIPCON='G' AND SLCODESE="+cp_ToStrODBC(this.oParentObject.w_CODESE)+" AND (SLDARPRO+SLDARPER)-(SLAVEPRO+SLAVEPER)<>0";
          +" order by SLTIPCON,SLCODICE";
           ,"_Curs_SALDICON")
    else
      select SLTIPCON,SLCODICE,SLDARPRO,SLAVEPRO,SLDARPER,SLAVEPER from (i_cTable);
       where SLTIPCON="G" AND SLCODESE=this.oParentObject.w_CODESE AND (SLDARPRO+SLDARPER)-(SLAVEPRO+SLAVEPER)<>0;
       order by SLTIPCON,SLCODICE;
        into cursor _Curs_SALDICON
    endif
    if used('_Curs_SALDICON')
      select _Curs_SALDICON
      locate for 1=1
      do while not(eof())
      this.w_PNTIPCON = NVL(_Curs_SALDICON.SLTIPCON, " ")
      this.w_PNCODCON = NVL(_Curs_SALDICON.SLCODICE, SPACE(15))
      this.w_CONSUP = SPACE(15)
      if (NOT EMPTY(this.w_PNTIPCON)) AND (NOT EMPTY(this.w_PNCODCON))
        * --- Verifica se Conto Economico  (SEZBIL C o R)
        * --- Read from CONTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ANCONSUP"+;
            " from "+i_cTable+" CONTI where ";
                +"ANTIPCON = "+cp_ToStrODBC(this.w_PNTIPCON);
                +" and ANCODICE = "+cp_ToStrODBC(this.w_PNCODCON);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ANCONSUP;
            from (i_cTable) where;
                ANTIPCON = this.w_PNTIPCON;
                and ANCODICE = this.w_PNCODCON;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CONSUP = NVL(cp_ToDate(_read_.ANCONSUP),cp_NullValue(_read_.ANCONSUP))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if CALCSEZ(this.w_CONSUP) $ "CR"
          ah_Msg("Elabora conto: %1",.T.,.F.,.F., this.w_PNCODCON)
          this.w_APPO = (NVL(SLDARPRO,0)+NVL(SLDARPER,0))-(NVL(SLAVEPRO,0)+NVL(SLAVEPER,0))
          this.w_TOTRIG = this.w_TOTRIG + this.w_APPO
          this.w_PNIMPDAR = IIF(this.w_APPO<0, ABS(this.w_APPO), 0)
          this.w_PNIMPAVE = IIF(this.w_APPO>0, this.w_APPO, 0)
          this.w_PNFLSALF = "+"
          this.w_PNDESRIG = this.oParentObject.w_DESSUP
          this.w_PNCAURIG = this.oParentObject.w_CODCAU
          * --- Scrive Primanota (Anagrafica)
          if this.w_INIREG=.F.
            * --- Calcola PNSERIAL e PNNUMRER
            this.w_PNSERIAL = SPACE(10)
            this.w_PNNUMRER = 0
            this.w_CPROWNUM = 0
            this.w_CPROWORD = 0
            i_Conn=i_TableProp[this.PNT_MAST_IDX, 3]
            cp_NextTableProg(this, i_Conn, "SEPNT", "i_codazi,w_PNSERIAL")
            cp_NextTableProg(this, i_Conn, "PRPNT", "i_codazi,w_PNCODESE,w_PNCODUTE,w_PNPRG,w_PNNUMRER")
            * --- Try
            local bErr_035FF940
            bErr_035FF940=bTrsErr
            this.Try_035FF940()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- Raise
              i_Error="Impossibile creare doc. prima nota"
              return
            endif
            bTrsErr=bTrsErr or bErr_035FF940
            * --- End
            * --- Write into PNT_MAST
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.PNT_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.PNT_MAST_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"PNDATREG ="+cp_NullLink(cp_ToStrODBC(this.w_PNDATREG),'PNT_MAST','PNDATREG');
              +",PNNUMRER ="+cp_NullLink(cp_ToStrODBC(this.w_PNNUMRER),'PNT_MAST','PNNUMRER');
              +",PNCODCAU ="+cp_NullLink(cp_ToStrODBC(this.w_PNCODCAU),'PNT_MAST','PNCODCAU');
              +",PNTIPREG ="+cp_NullLink(cp_ToStrODBC(this.w_PNTIPREG),'PNT_MAST','PNTIPREG');
              +",PNCODVAL ="+cp_NullLink(cp_ToStrODBC(this.w_PNCODVAL),'PNT_MAST','PNCODVAL');
              +",PNPRG ="+cp_NullLink(cp_ToStrODBC(this.w_PNPRG),'PNT_MAST','PNPRG');
              +",PNCAOVAL ="+cp_NullLink(cp_ToStrODBC(this.w_PNCAOVAL),'PNT_MAST','PNCAOVAL');
              +",PNDESSUP ="+cp_NullLink(cp_ToStrODBC(this.w_PNDESSUP),'PNT_MAST','PNDESSUP');
              +",PNCOMPET ="+cp_NullLink(cp_ToStrODBC(this.w_PNCOMPET),'PNT_MAST','PNCOMPET');
              +",PNVALNAZ ="+cp_NullLink(cp_ToStrODBC(this.w_PNVALNAZ),'PNT_MAST','PNVALNAZ');
              +",UTCC ="+cp_NullLink(cp_ToStrODBC(this.w_UTCC),'PNT_MAST','UTCC');
              +",UTCV ="+cp_NullLink(cp_ToStrODBC(this.w_UTCV),'PNT_MAST','UTCV');
              +",UTDC ="+cp_NullLink(cp_ToStrODBC(this.w_UTDC),'PNT_MAST','UTDC');
              +",UTDV ="+cp_NullLink(cp_ToStrODBC(this.w_UTDV),'PNT_MAST','UTDV');
              +",PNCODESE ="+cp_NullLink(cp_ToStrODBC(this.w_PNCODESE),'PNT_MAST','PNCODESE');
              +",PNCODUTE ="+cp_NullLink(cp_ToStrODBC(this.w_PNCODUTE),'PNT_MAST','PNCODUTE');
              +",PNFLPROV ="+cp_NullLink(cp_ToStrODBC("N"),'PNT_MAST','PNFLPROV');
                  +i_ccchkf ;
              +" where ";
                  +"PNSERIAL = "+cp_ToStrODBC(this.w_PNSERIAL);
                     )
            else
              update (i_cTable) set;
                  PNDATREG = this.w_PNDATREG;
                  ,PNNUMRER = this.w_PNNUMRER;
                  ,PNCODCAU = this.w_PNCODCAU;
                  ,PNTIPREG = this.w_PNTIPREG;
                  ,PNCODVAL = this.w_PNCODVAL;
                  ,PNPRG = this.w_PNPRG;
                  ,PNCAOVAL = this.w_PNCAOVAL;
                  ,PNDESSUP = this.w_PNDESSUP;
                  ,PNCOMPET = this.w_PNCOMPET;
                  ,PNVALNAZ = this.w_PNVALNAZ;
                  ,UTCC = this.w_UTCC;
                  ,UTCV = this.w_UTCV;
                  ,UTDC = this.w_UTDC;
                  ,UTDV = this.w_UTDV;
                  ,PNCODESE = this.w_PNCODESE;
                  ,PNCODUTE = this.w_PNCODUTE;
                  ,PNFLPROV = "N";
                  &i_ccchkf. ;
               where;
                  PNSERIAL = this.w_PNSERIAL;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            this.w_INIREG = .T.
          endif
          * --- Scrive Reg.Contabili (Movimentazione)
          this.w_CPROWNUM = this.w_CPROWNUM + 1
          this.w_CPROWORD = this.w_CPROWORD + 10
          this.w_PNFLZERO = IIF(this.w_PNIMPAVE=0 AND this.w_PNIMPDAR=0, "S", " ")
          * --- Try
          local bErr_0360ABE0
          bErr_0360ABE0=bTrsErr
          this.Try_0360ABE0()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- Raise
            i_Error="Impossibile creare dett. prima nota"
            return
          endif
          bTrsErr=bTrsErr or bErr_0360ABE0
          * --- End
          * --- Aggiorna Saldo
          * --- Try
          local bErr_0376F5E0
          bErr_0376F5E0=bTrsErr
          this.Try_0376F5E0()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_0376F5E0
          * --- End
          * --- Write into SALDICON
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.SALDICON_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICON_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SLDARPER =SLDARPER+ "+cp_ToStrODBC(this.w_PNIMPDAR);
            +",SLAVEPER =SLAVEPER+ "+cp_ToStrODBC(this.w_PNIMPAVE);
            +",SLAVEFIN =SLAVEFIN+ "+cp_ToStrODBC(this.w_PNIMPDAR);
            +",SLDARFIN =SLDARFIN+ "+cp_ToStrODBC(this.w_PNIMPAVE);
                +i_ccchkf ;
            +" where ";
                +"SLTIPCON = "+cp_ToStrODBC(this.w_PNTIPCON);
                +" and SLCODICE = "+cp_ToStrODBC(this.w_PNCODCON);
                +" and SLCODESE = "+cp_ToStrODBC(this.oParentObject.w_CODESE);
                   )
          else
            update (i_cTable) set;
                SLDARPER = SLDARPER + this.w_PNIMPDAR;
                ,SLAVEPER = SLAVEPER + this.w_PNIMPAVE;
                ,SLAVEFIN = SLAVEFIN + this.w_PNIMPDAR;
                ,SLDARFIN = SLDARFIN + this.w_PNIMPAVE;
                &i_ccchkf. ;
             where;
                SLTIPCON = this.w_PNTIPCON;
                and SLCODICE = this.w_PNCODCON;
                and SLCODESE = this.oParentObject.w_CODESE;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      endif
      if this.w_CPROWORD>1000
        * --- Scrive Movimento di Chiusura (se w_TOTRIG<>0)
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Quindi scrive una nuova Registrazione
        this.w_INIREG = .F.
        this.w_CPROWNUM = 0
        this.w_CPROWORD = 0
      endif
        select _Curs_SALDICON
        continue
      enddo
      use
    endif
    * --- Scrive Movimento di Chiusura (se w_TOTRIG<>0)
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    ah_Msg("CHIUSURA CONTO ECONOMICO COMPLETATA",.T.)
    * --- Conferma la Transazione
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_0360A0A0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into AZIENDA
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.AZIENDA_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"AZDATBLO ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'AZIENDA','AZDATBLO');
          +i_ccchkf ;
      +" where ";
          +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
             )
    else
      update (i_cTable) set;
          AZDATBLO = cp_CharToDate("  -  -  ");
          &i_ccchkf. ;
       where;
          AZCODAZI = i_CODAZI;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_035FF940()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PNT_MAST
    i_nConn=i_TableProp[this.PNT_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PNT_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PNSERIAL"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PNT_MAST','PNSERIAL');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PNSERIAL',this.w_PNSERIAL)
      insert into (i_cTable) (PNSERIAL &i_ccchkf. );
         values (;
           this.w_PNSERIAL;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_0360ABE0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PNT_DETT
    i_nConn=i_TableProp[this.PNT_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PNT_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PNSERIAL"+",CPROWNUM"+",CPROWORD"+",PNTIPCON"+",PNCODCON"+",PNIMPDAR"+",PNIMPAVE"+",PNFLSALF"+",PNDESRIG"+",PNCAURIG"+",PNFLSALD"+",PNFLZERO"+",PNFLSALI"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PNT_DETT','PNSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'PNT_DETT','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'PNT_DETT','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNTIPCON),'PNT_DETT','PNTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCON),'PNT_DETT','PNCODCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNIMPDAR),'PNT_DETT','PNIMPDAR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNIMPAVE),'PNT_DETT','PNIMPAVE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLSALF),'PNT_DETT','PNFLSALF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNDESRIG),'PNT_DETT','PNDESRIG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCAURIG),'PNT_DETT','PNCAURIG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLSALD),'PNT_DETT','PNFLSALD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLZERO),'PNT_DETT','PNFLZERO');
      +","+cp_NullLink(cp_ToStrODBC(" "),'PNT_DETT','PNFLSALI');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PNSERIAL',this.w_PNSERIAL,'CPROWNUM',this.w_CPROWNUM,'CPROWORD',this.w_CPROWORD,'PNTIPCON',this.w_PNTIPCON,'PNCODCON',this.w_PNCODCON,'PNIMPDAR',this.w_PNIMPDAR,'PNIMPAVE',this.w_PNIMPAVE,'PNFLSALF',this.w_PNFLSALF,'PNDESRIG',this.w_PNDESRIG,'PNCAURIG',this.w_PNCAURIG,'PNFLSALD',this.w_PNFLSALD,'PNFLZERO',this.w_PNFLZERO)
      insert into (i_cTable) (PNSERIAL,CPROWNUM,CPROWORD,PNTIPCON,PNCODCON,PNIMPDAR,PNIMPAVE,PNFLSALF,PNDESRIG,PNCAURIG,PNFLSALD,PNFLZERO,PNFLSALI &i_ccchkf. );
         values (;
           this.w_PNSERIAL;
           ,this.w_CPROWNUM;
           ,this.w_CPROWORD;
           ,this.w_PNTIPCON;
           ,this.w_PNCODCON;
           ,this.w_PNIMPDAR;
           ,this.w_PNIMPAVE;
           ,this.w_PNFLSALF;
           ,this.w_PNDESRIG;
           ,this.w_PNCAURIG;
           ,this.w_PNFLSALD;
           ,this.w_PNFLZERO;
           ," ";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_0376F5E0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDICON
    i_nConn=i_TableProp[this.SALDICON_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDICON_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SLTIPCON"+",SLCODICE"+",SLCODESE"+",SLDARPRO"+",SLAVEPRO"+",SLDARPER"+",SLAVEPER"+",SLDARINI"+",SLAVEINI"+",SLDARFIN"+",SLAVEFIN"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PNTIPCON),'SALDICON','SLTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCON),'SALDICON','SLCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODESE),'SALDICON','SLCODESE');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARPRO');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEPRO');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARPER');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEPER');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARINI');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEINI');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARFIN');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEFIN');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SLTIPCON',this.w_PNTIPCON,'SLCODICE',this.w_PNCODCON,'SLCODESE',this.oParentObject.w_CODESE,'SLDARPRO',0,'SLAVEPRO',0,'SLDARPER',0,'SLAVEPER',0,'SLDARINI',0,'SLAVEINI',0,'SLDARFIN',0,'SLAVEFIN',0)
      insert into (i_cTable) (SLTIPCON,SLCODICE,SLCODESE,SLDARPRO,SLAVEPRO,SLDARPER,SLAVEPER,SLDARINI,SLAVEINI,SLDARFIN,SLAVEFIN &i_ccchkf. );
         values (;
           this.w_PNTIPCON;
           ,this.w_PNCODCON;
           ,this.oParentObject.w_CODESE;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcolo Utile / Perdita di Esercizio
    * --- Questa Fase non vene gestita nelle Operazione relative all'EURO kit
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Scrive Riga Finale di Chiusura
    if this.w_TOTRIG <> 0
      ah_Msg("Scrivo riga chiusura registrazione",.T.)
      this.w_PNTIPCON = this.oParentObject.w_TIPCON
      this.w_PNCODCON = this.oParentObject.w_CODCON
      this.w_PNIMPDAR = IIF(this.w_TOTRIG>0, this.w_TOTRIG, 0)
      this.w_PNIMPAVE = IIF(this.w_TOTRIG<0, ABS(this.w_TOTRIG), 0)
      * --- Scrive Reg.Contabili (Movimentazione)
      this.w_CPROWNUM = this.w_CPROWNUM + 1
      this.w_CPROWORD = this.w_CPROWORD + 10
      this.w_PNFLZERO = IIF(this.w_PNIMPAVE=0 AND this.w_PNIMPDAR=0, "S", " ")
      * --- Try
      local bErr_037C4F00
      bErr_037C4F00=bTrsErr
      this.Try_037C4F00()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- Raise
        i_Error="Impossibile creare dett. prima nota"
        return
      endif
      bTrsErr=bTrsErr or bErr_037C4F00
      * --- End
      * --- Write into PNT_DETT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.PNT_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.PNT_DETT_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"PNTIPCON ="+cp_NullLink(cp_ToStrODBC(this.w_PNTIPCON),'PNT_DETT','PNTIPCON');
        +",PNCODCON ="+cp_NullLink(cp_ToStrODBC(this.w_PNCODCON),'PNT_DETT','PNCODCON');
        +",PNIMPDAR ="+cp_NullLink(cp_ToStrODBC(this.w_PNIMPDAR),'PNT_DETT','PNIMPDAR');
        +",PNIMPAVE ="+cp_NullLink(cp_ToStrODBC(this.w_PNIMPAVE),'PNT_DETT','PNIMPAVE');
        +",PNFLSALF ="+cp_NullLink(cp_ToStrODBC(this.w_PNFLSALF),'PNT_DETT','PNFLSALF');
        +",PNDESRIG ="+cp_NullLink(cp_ToStrODBC(this.w_PNDESRIG),'PNT_DETT','PNDESRIG');
        +",PNCAURIG ="+cp_NullLink(cp_ToStrODBC(this.w_PNCAURIG),'PNT_DETT','PNCAURIG');
        +",CPROWORD ="+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'PNT_DETT','CPROWORD');
        +",PNFLSALD ="+cp_NullLink(cp_ToStrODBC(this.w_PNFLSALD),'PNT_DETT','PNFLSALD');
        +",PNFLSALI ="+cp_NullLink(cp_ToStrODBC(" "),'PNT_DETT','PNFLSALI');
        +",PNFLZERO ="+cp_NullLink(cp_ToStrODBC(this.w_PNFLZERO),'PNT_DETT','PNFLZERO');
            +i_ccchkf ;
        +" where ";
            +"PNSERIAL = "+cp_ToStrODBC(this.w_PNSERIAL);
            +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
               )
      else
        update (i_cTable) set;
            PNTIPCON = this.w_PNTIPCON;
            ,PNCODCON = this.w_PNCODCON;
            ,PNIMPDAR = this.w_PNIMPDAR;
            ,PNIMPAVE = this.w_PNIMPAVE;
            ,PNFLSALF = this.w_PNFLSALF;
            ,PNDESRIG = this.w_PNDESRIG;
            ,PNCAURIG = this.w_PNCAURIG;
            ,CPROWORD = this.w_CPROWORD;
            ,PNFLSALD = this.w_PNFLSALD;
            ,PNFLSALI = " ";
            ,PNFLZERO = this.w_PNFLZERO;
            &i_ccchkf. ;
         where;
            PNSERIAL = this.w_PNSERIAL;
            and CPROWNUM = this.w_CPROWNUM;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Aggiorna Conto Profitti e Perdite
      * --- Try
      local bErr_037C6580
      bErr_037C6580=bTrsErr
      this.Try_037C6580()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_037C6580
      * --- End
      * --- Write into SALDICON
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.SALDICON_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICON_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"SLDARPER =SLDARPER+ "+cp_ToStrODBC(this.w_PNIMPDAR);
        +",SLAVEPER =SLAVEPER+ "+cp_ToStrODBC(this.w_PNIMPAVE);
        +",SLAVEFIN =SLAVEFIN+ "+cp_ToStrODBC(this.w_PNIMPDAR);
        +",SLDARFIN =SLDARFIN+ "+cp_ToStrODBC(this.w_PNIMPAVE);
            +i_ccchkf ;
        +" where ";
            +"SLTIPCON = "+cp_ToStrODBC(this.oParentObject.w_TIPCON);
            +" and SLCODICE = "+cp_ToStrODBC(this.oParentObject.w_CODCON);
            +" and SLCODESE = "+cp_ToStrODBC(this.oParentObject.w_CODESE);
               )
      else
        update (i_cTable) set;
            SLDARPER = SLDARPER + this.w_PNIMPDAR;
            ,SLAVEPER = SLAVEPER + this.w_PNIMPAVE;
            ,SLAVEFIN = SLAVEFIN + this.w_PNIMPDAR;
            ,SLDARFIN = SLDARFIN + this.w_PNIMPAVE;
            &i_ccchkf. ;
         where;
            SLTIPCON = this.oParentObject.w_TIPCON;
            and SLCODICE = this.oParentObject.w_CODCON;
            and SLCODESE = this.oParentObject.w_CODESE;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      this.w_TOTRIG = 0
      WAIT CLEAR
    endif
  endproc
  proc Try_037C4F00()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PNT_DETT
    i_nConn=i_TableProp[this.PNT_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PNT_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PNSERIAL"+",CPROWNUM"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PNT_DETT','PNSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'PNT_DETT','CPROWNUM');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PNSERIAL',this.w_PNSERIAL,'CPROWNUM',this.w_CPROWNUM)
      insert into (i_cTable) (PNSERIAL,CPROWNUM &i_ccchkf. );
         values (;
           this.w_PNSERIAL;
           ,this.w_CPROWNUM;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_037C6580()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDICON
    i_nConn=i_TableProp[this.SALDICON_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDICON_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SLTIPCON"+",SLCODICE"+",SLCODESE"+",SLDARPRO"+",SLAVEPRO"+",SLDARPER"+",SLAVEPER"+",SLDARINI"+",SLAVEINI"+",SLDARFIN"+",SLAVEFIN"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PNTIPCON),'SALDICON','SLTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCON),'SALDICON','SLCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODESE),'SALDICON','SLCODESE');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARPRO');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEPRO');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARPER');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEPER');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARINI');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEINI');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARFIN');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEFIN');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SLTIPCON',this.w_PNTIPCON,'SLCODICE',this.w_PNCODCON,'SLCODESE',this.oParentObject.w_CODESE,'SLDARPRO',0,'SLAVEPRO',0,'SLDARPER',0,'SLAVEPER',0,'SLDARINI',0,'SLAVEINI',0,'SLDARFIN',0,'SLAVEFIN',0)
      insert into (i_cTable) (SLTIPCON,SLCODICE,SLCODESE,SLDARPRO,SLAVEPRO,SLDARPER,SLAVEPER,SLDARINI,SLAVEINI,SLDARFIN,SLAVEFIN &i_ccchkf. );
         values (;
           this.w_PNTIPCON;
           ,this.w_PNCODCON;
           ,this.oParentObject.w_CODESE;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,7)]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='ESERCIZI'
    this.cWorkTables[4]='PNT_DETT'
    this.cWorkTables[5]='PNT_MAST'
    this.cWorkTables[6]='SALDICON'
    this.cWorkTables[7]='VALUTE'
    return(this.OpenAllTables(7))

  proc CloseCursors()
    if used('_Curs_GSCG_BCE')
      use in _Curs_GSCG_BCE
    endif
    if used('_Curs_PNT_DETT')
      use in _Curs_PNT_DETT
    endif
    if used('_Curs_SALDICON')
      use in _Curs_SALDICON
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
