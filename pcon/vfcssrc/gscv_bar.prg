* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscv_bar                                                        *
*              Aggiorna slqtapro su saldi articolo                             *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_124]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-10-23                                                      *
* Last revis.: 2005-12-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscv_bar",oParentObject,m.w_NHF)
return(i_retval)

define class tgscv_bar as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TMPC = space(10)
  w_TEMP = space(10)
  * --- WorkFile variables
  SALDIART_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Procedura obbligatoria che aggiorna il campo SLQTAPRO come (MVQTAUM1-MVQTASAL)
    *     di  DOCSDETT 
    * --- FIle di LOG
    * --- Try
    local bErr_04A9FDE0
    bErr_04A9FDE0=bTrsErr
    this.Try_04A9FDE0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_pMSG = Message()
      this.oParentObject.w_pESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_MsgFormat("Errore generico - impossibile aggiornare campo SLQTAPRO tabella SALDIART")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_04A9FDE0
    * --- End
  endproc
  proc Try_04A9FDE0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Aggiornamento campo SLQTAPRO
    * --- Write into SALDIART
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SALDIART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="SLCODICE,SLCODMAG"
      do vq_exec with 'GSCV2BAR',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="SALDIART.SLCODICE = _t2.SLCODICE";
              +" and "+"SALDIART.SLCODMAG = _t2.SLCODMAG";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SLQTAPRO = SALDIART.SLQTAPRO+_t2.SLQTAPRO";
          +i_ccchkf;
          +" from "+i_cTable+" SALDIART, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="SALDIART.SLCODICE = _t2.SLCODICE";
              +" and "+"SALDIART.SLCODMAG = _t2.SLCODMAG";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDIART, "+i_cQueryTable+" _t2 set ";
          +"SALDIART.SLQTAPRO = SALDIART.SLQTAPRO+_t2.SLQTAPRO";
          +Iif(Empty(i_ccchkf),"",",SALDIART.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="SALDIART.SLCODICE = t2.SLCODICE";
              +" and "+"SALDIART.SLCODMAG = t2.SLCODMAG";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDIART set (";
          +"SLQTAPRO";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"SLQTAPRO+t2.SLQTAPRO";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="SALDIART.SLCODICE = _t2.SLCODICE";
              +" and "+"SALDIART.SLCODMAG = _t2.SLCODMAG";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDIART set ";
          +"SLQTAPRO = SALDIART.SLQTAPRO+_t2.SLQTAPRO";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".SLCODICE = "+i_cQueryTable+".SLCODICE";
              +" and "+i_cTable+".SLCODMAG = "+i_cQueryTable+".SLCODMAG";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SLQTAPRO = (select "+i_cTable+".SLQTAPRO+SLQTAPRO from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    if this.w_NHF>=0
      this.w_TMPC = ah_MsgFormat("Aggiornamento campo SLQTAPRO tabella SALDIART completato correttamente")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    this.oParentObject.w_pMSG = ah_MsgFormat("Aggiornamento eseguito correttamente")
    this.oParentObject.w_pESEOK = .T.
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='SALDIART'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
