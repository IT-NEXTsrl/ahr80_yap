* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvbb10                                                        *
*              Conversione campi tabella stu_caus/stu_para                     *
*                                                                              *
*      Author: ZUCCHETTI S.p.A.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_43]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-12-01                                                      *
* Last revis.: 2007-05-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvbb10",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvbb10 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_GOON = .f.
  w_oMess = .NULL.
  * --- WorkFile variables
  RIPATMP1_idx=0
  STU_CAUS_idx=0
  STU_PARA_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- SOLO PER DB2
    * --- FIle di LOG
    this.w_GOON = .T.
    * --- Try
    local bErr_037E6FC0
    bErr_037E6FC0=bTrsErr
    this.Try_037E6FC0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Recupero il Nome fisico della tabella temporanea 
      *     per indicarla nel Log in caso di errore
      if this.w_GOON
        this.w_TMPC = cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
        * --- Gestisce log errori
        this.oParentObject.w_PMSG = ah_Msgformat("%1%0Tabella temporanea contenente i dati: %2", Message() , Alltrim(this.w_TMPC) )
        this.oParentObject.w_PESEOK = .F.
        if this.w_NHF>=0
          this.w_TMPC = this.oParentObject.w_PMSG
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      else
        if this.w_NHF>=0
          this.w_TMPC = ah_Msgformat("Si � verificato il seguente errore: %1", message() )
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
    endif
    bTrsErr=bTrsErr or bErr_037E6FC0
    * --- End
  endproc
  proc Try_037E6FC0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Conversione campi tabella STU_CAUS
     
 i_nConn=i_TableProp[this.STU_CAUS_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.STU_CAUS_idx,2])
    if i_nConn<>0
      * --- Caso DB2
      * --- Verifico se i campi sono gi� aggiornati
      L_A=SQLEXEC(i_nConn,"Select LMINFAVE,LMINFDAR,LMSUPAVE,LMSUPDAR from "+i_cTable,"CURS")
      afields(DATI,"CURS")
      if DATI(1,3)=6 and DATI(2,3)=6 and DATI(3,3)=6 and DATI(4,3)=6
        this.w_GOON = .F.
      endif
      * --- Elimino il cursore
      if Used("CURS")
        Select ("CURS") 
 use
      endif
      * --- Aggiorno la tabella
      if this.w_GOON
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      if bTrsErr
        * --- Raise
        i_Error=MESSAGE()
        return
      else
        if this.w_NHF>=0
          this.w_TMPC = ah_Msgformat("Adeguamento campi LMINFAVE, LMINFDAR, LMSUPAVE, LMSUPDAR tabella STU_CAUS eseguito correttamente%0Occorre ripetere la procedura su ogni azienda")
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
    endif
    * --- Conversione campi tabella STU_PARA
     
 i_nConn=i_TableProp[this.STU_PARA_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.STU_PARA_idx,2])
    if i_nConn<>0
      * --- Caso DB2
      * --- Verifico se i campi sono gi� aggiornati
      A=SQLEXEC(i_nConn,"Select LMPROCLI from "+i_cTable,"CURS")
      afields(DATI,"CURS")
      if DATI(1,3)=6 
        this.w_GOON = .F.
      endif
      * --- Elimino il cursore
      if Used("CURS")
        Select ("CURS") 
 use
      endif
      * --- Aggiorno la tabella
      if this.w_GOON
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      if bTrsErr
        * --- Raise
        i_Error=MESSAGE()
        return
      else
        if this.w_NHF>=0
          this.w_TMPC = ah_Msgformat("Adeguamento campi LMPROCLI tabella STU_PARA eseguito correttamente%0Occorre ripetere la procedura su ogni azienda")
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    * --- Oggetto per messaggi incrementali
    this.w_oMess=createobject("Ah_Message")
    this.w_oMess.AddMsgPartNL("Adeguamento campi LMINFAVE, LMINFDAR, LMSUPAVE, LMSUPDAR tabella STU_CAUS eseguito correttamente")     
    this.w_oMess.AddMsgPart("Adeguamento campi LMPROCLI tabella STU_PARA eseguito correttamente%0Occorre ripetere la procedura su ogni azienda")     
    this.w_TMPC = this.w_oMess.ComposeMessage()
    this.oParentObject.w_PMSG = this.w_TMPC
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella DB2
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.STU_CAUS_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.STU_CAUS_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"* "," from "+i_cTable;
          )
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    * --- Elimino la tabella
    if Not GSCV_BDT( this, "STU_CAUS" , i_nConn )
      * --- Raise
      i_Error="Error"
      return
    endif
    * --- Ricostruisco il database per la tabella
    GSCV_BRT(this, "STU_CAUS" , i_nConn , .T.)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into STU_CAUS
    i_nConn=i_TableProp[this.STU_CAUS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STU_CAUS_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.STU_CAUS_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_Msg("Ripristinati valori di origine",.T.)
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    ah_Msg("Eliminata tabella temporanea tmp",.T.)
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella DB2
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.STU_PARA_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.STU_PARA_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"* "," from "+i_cTable;
          )
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    * --- Elimino la tabella
    if Not GSCV_BDT( this, "STU_PARA" , i_nConn )
      * --- Raise
      i_Error="Error"
      return
    endif
    * --- Ricostruisco il database per la tabella
    GSCV_BRT(this, "STU_PARA" , i_nConn , .T.)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into STU_PARA
    i_nConn=i_TableProp[this.STU_PARA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STU_PARA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.STU_PARA_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_Msg("Ripristinati valori di origine",.T.)
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    ah_Msg("Eliminata tabella temporanea tmp",.T.)
  endproc


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='*RIPATMP1'
    this.cWorkTables[2]='STU_CAUS'
    this.cWorkTables[3]='STU_PARA'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
