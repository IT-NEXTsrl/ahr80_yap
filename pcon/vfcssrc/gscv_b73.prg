* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscv_b73                                                        *
*              Aggiornamento campi numerici per DB2                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_295]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-07-18                                                      *
* Last revis.: 2012-04-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscv_b73",oParentObject,m.w_NHF)
return(i_retval)

define class tgscv_b73 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_CODAZI = space(5)
  w_NOMARC = space(10)
  iNConn = 0
  w_NOMTBL = space(10)
  i_NT = 0
  w_TYPFLD = space(10)
  w_TIPOPE = space(255)
  w_NOMFLD = space(10)
  w_LENFLD = 0
  w_MESS = space(10)
  w_DECFLD = 0
  w_OK = .f.
  w_OPERAZ = space(10)
  w_NUMDEC = 0
  * --- WorkFile variables
  AZIENDA_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- FIle di LOG
    * --- Test sul tipo Database
    if upper(CP_DBTYPE) <> "DB2" 
      ah_ErrorMsg("Conversione prevista solo per database IBM DB2",,"")
      this.oParentObject.w_PESEOK = .T.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("Elaborazione terminata con successo")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    else
      if NOT cp_ReadXdc()
        ah_ErrorMsg("Impossibile leggere il dizionario dati",,"")
        i_retcode = 'stop'
        return
      endif
      * --- Legge Aziende
      i_i = cp_SQL(i_ServerConn[1,2],"select AZCODAZI from AZIENDA","ZOOMAZI")
      if NOT USED("ZOOMAZI")
        ah_ErrorMsg("Non ci sono aziende nel database",,"")
        i_retcode = 'stop'
        return
      else
        SELECT ZOOMAZI
        GO TOP
        SCAN FOR NOT EMPTY(NVL(AZCODAZI," "))
        this.w_CODAZI = UPPER(ALLTRIM(AZCODAZI))
        * --- Legge Tabella Aziende
        i_i = cp_SQL(i_ServerConn[1,2],"select PHNAME, FILENAME from cpttbls","ZOOMTBL")
        if USED("ZOOMTBL")
          SELECT ZOOMTBL
          GO TOP
          SCAN FOR AT("xxx", NVL(PHNAME, " "))<>0 AND NOT EMPTY(NVL(FILENAME," "))
          this.w_NOMARC = UPPER(ALLTRIM(FILENAME))
          * --- Try
          local bErr_03791790
          bErr_03791790=bTrsErr
          this.Try_03791790()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            ah_Msg("Errore: scansione azienda: %1%0Scansione tabella: %2%0Campo: %3", this.w_CODAZI, this.w_NOMARC,this.w_NOMFLD,.T.,.F.,.F., )
            * --- rollback
            bTrsErr=.t.
            cp_EndTrs(.t.)
            * --- Gestisce log errori
            this.oParentObject.w_PMSG = Message()
            this.oParentObject.w_PESEOK = .F.
            if this.w_NHF>=0
              this.w_TMPC = this.oParentObject.w_PMSG
              this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
            endif
          endif
          bTrsErr=bTrsErr or bErr_03791790
          * --- End
          SELECT ZOOMTBL
          ENDSCAN
          SELECT ZOOMTBL
          USE
        endif
        SELECT ZOOMAZI
        ENDSCAN
        SELECT ZOOMAZI
        USE
        ah_Msg("Scansione tabelle completata",.T.)
      endif
    endif
  endproc
  proc Try_03791790()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    this.i_NT = cp_OpenTable(this.w_NOMARC, .T.)
    if this.i_NT<>0
      this.iNConn = i_TableProp[this.i_NT,3]
      if this.iNConn<>0
        FOR L_j = 1 TO i_dcx.GetFieldsCount(this.w_NOMARC)
        this.w_NOMFLD = ALLTRIM(i_dcx.GetFieldName(this.w_NOMARC,L_j))
        this.w_TYPFLD = ALLTRIM(i_dcx.GetFieldType(this.w_NOMARC,L_j))
        this.w_LENFLD = i_dcx.GetFieldLen(this.w_NOMARC,L_j)
        this.w_DECFLD = i_dcx.GetFieldDec(this.w_NOMARC,L_j)
        this.w_NUMDEC = IIF(this.w_DECFLD=0,0,this.w_DECFLD-1)
        if this.w_TYPFLD="N" 
          ah_Msg("Scansione azienda: %1%0Scansione tabella: %2%0Campo: %3",.T.,.F.,.F., this.w_CODAZI, this.w_NOMARC, this.w_NOMFLD)
          this.w_NOMTBL = ALLTRIM(this.w_CODAZI)+this.w_NOMARC
          this.w_TIPOPE = this.w_NOMFLD + " = ROUND(" + this.w_NOMFLD + "," + str(this.w_NUMDEC) + ")"
          this.w_OPERAZ = "UPDATE " + this.w_NOMTBL + " SET " + this.w_TIPOPE + " WHERE " + this.w_NOMFLD + " IS NOT NULL"
          this.w_OK = (cp_TrsSQL(this.iNConn, this.w_OPERAZ))
        endif
        NEXT
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = ah_Msgformat("Aggiornamento campi numerici per DB2 effettuato correttamente")
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='AZIENDA'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
