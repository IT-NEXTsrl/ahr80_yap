* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvbb63                                                        *
*              Aggiorna chiave tabella trascodifiche                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_6]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-03-23                                                      *
* Last revis.: 2007-07-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvbb63",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvbb63 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_MESS = space(100)
  w_CPROWNUM = 0
  w_OLDCOD = space(21)
  * --- WorkFile variables
  RIPATMP1_idx=0
  TRS_DETT_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questa procedura di conversione esegue:
    *     aggiornamento chiave primaria trascidifiche (aggiungendo CPROWNUM)
    *     
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Try
    local bErr_03628690
    bErr_03628690=bTrsErr
    this.Try_03628690()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = this.oParentObject.w_PMSG
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_03628690
    * --- End
  endproc
  proc Try_03628690()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Converte tabella DB2
    pName="TRS_DETT"
    pDatabaseType=i_ServerConn[1,6]
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.TRS_DETT_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.TRS_DETT_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
          )
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    * --- Elimino la tabella
    if Not GSCV_BDT( this, "TRS_DETT" , i_nConn )
      * --- Raise
      i_Error="Error"
      return
    endif
    * --- Ricostruisco il database per la tabella
    GSCV_BRT(this, "TRS_DETT" , i_nConn , .T.)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into TRS_DETT
    i_nConn=i_TableProp[this.TRS_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TRS_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.TRS_DETT_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    ah_Msg("Eliminata tabella temporanea tmp",.T.)
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    if upper(CP_DBTYPE)="DB2"
      this.oParentObject.w_PMSG = ah_Msgformat("Elaborazione terminata con successo")
    else
      this.w_TMPC = ah_Msgformat("Adeguamento chiave primaria tabella TRS_DETT eseguito correttamente%0ATTENZIONE: occorre andare in MANUTENZIONE DATABASE, selezionare la tabella%0TRS_DETT e premere <OK>")
      this.oParentObject.w_PMSG = this.w_TMPC
    endif
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questa Procedura aggiorna il cprownum della GESTIONE TRASCODIFICHE
    this.w_OLDCOD = Repl("@",21)
    * --- Try
    local bErr_0376AD20
    bErr_0376AD20=bTrsErr
    this.Try_0376AD20()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE %1 -impossibile aggiornare la gestione trascodifiche", this.oParentObject.w_PMSG )
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_0376AD20
    * --- End
  endproc
  proc Try_0376AD20()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    ah_Msg("Aggiornamento CPROWNUM",.T.)
    * --- Select from GSCVBB63
    do vq_exec with 'GSCVBB63',this,'_Curs_GSCVBB63','',.f.,.t.
    if used('_Curs_GSCVBB63')
      select _Curs_GSCVBB63
      locate for 1=1
      do while not(eof())
      if _Curs_GSCVBB63.TRCODICE <> this.w_OLDCOD
        this.w_CPROWNUM = 0
      endif
      this.w_CPROWNUM = this.w_CPROWNUM + 1
      * --- Write into TRS_DETT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TRS_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TRS_DETT_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TRS_DETT_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"CPROWNUM ="+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'TRS_DETT','CPROWNUM');
            +i_ccchkf ;
        +" where ";
            +"TRCODICE = "+cp_ToStrODBC(_Curs_GSCVBB63.TRCODICE);
            +" and TRCODINT = "+cp_ToStrODBC(_Curs_GSCVBB63.TRCODINT);
               )
      else
        update (i_cTable) set;
            CPROWNUM = this.w_CPROWNUM;
            &i_ccchkf. ;
         where;
            TRCODICE = _Curs_GSCVBB63.TRCODICE;
            and TRCODINT = _Curs_GSCVBB63.TRCODINT;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      this.w_OLDCOD = _Curs_GSCVBB63.TRCODICE
        select _Curs_GSCVBB63
        continue
      enddo
      use
    endif
    if bTrsErr
      * --- Raise
      i_Error=i_TrsMsg
      return
    endif
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = ah_Msgformat("Conversione eseguita correttamente")
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Conversione eseguita correttamente")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='*RIPATMP1'
    this.cWorkTables[2]='TRS_DETT'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_GSCVBB63')
      use in _Curs_GSCVBB63
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
