* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscv_b77                                                        *
*              Aggiornamento nuova UM di riga in distinta base                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_479]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-11-18                                                      *
* Last revis.: 2002-11-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscv_b77",oParentObject,m.w_NHF)
return(i_retval)

define class tgscv_b77 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_CODDIS = space(20)
  w_NURIG = 0
  w_RIGA = 0
  w_QTAMOV = 0
  * --- WorkFile variables
  DISTBASE_idx=0
  COM_VARI_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- FIle di LOG
    * --- Try
    local bErr_037A4E80
    bErr_037A4E80=bTrsErr
    this.Try_037A4E80()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = this.oParentObject.w_PMSG
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_037A4E80
    * --- End
  endproc
  proc Try_037A4E80()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Select from DISTBASE
    i_nConn=i_TableProp[this.DISTBASE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DISTBASE_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select DBCODICE,CPROWNUM,DBQTAMOV  from "+i_cTable+" DISTBASE ";
          +" where DBQTADIS=0";
          +" order by DBCODICE";
           ,"_Curs_DISTBASE")
    else
      select DBCODICE,CPROWNUM,DBQTAMOV from (i_cTable);
       where DBQTADIS=0;
       order by DBCODICE;
        into cursor _Curs_DISTBASE
    endif
    if used('_Curs_DISTBASE')
      select _Curs_DISTBASE
      locate for 1=1
      do while not(eof())
      this.w_CODDIS = _Curs_DISTBASE.DBCODICE
      this.w_NURIG = _Curs_DISTBASE.CPROWNUM
      this.w_QTAMOV = _Curs_DISTBASE.DBQTAMOV
      ah_Msg("Elaborazione distinta: %1",.T.,.F.,.F., this.w_CODDIS)
      * --- Write into DISTBASE
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DISTBASE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DISTBASE_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DISTBASE_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"DBQTADIS ="+cp_NullLink(cp_ToStrODBC(this.w_QTAMOV),'DISTBASE','DBQTADIS');
            +i_ccchkf ;
        +" where ";
            +"DBCODICE = "+cp_ToStrODBC(this.w_CODDIS);
            +" and CPROWNUM = "+cp_ToStrODBC(this.w_NURIG);
               )
      else
        update (i_cTable) set;
            DBQTADIS = this.w_QTAMOV;
            &i_ccchkf. ;
         where;
            DBCODICE = this.w_CODDIS;
            and CPROWNUM = this.w_NURIG;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
        select _Curs_DISTBASE
        continue
      enddo
      use
    endif
    * --- Select from COM_VARI
    i_nConn=i_TableProp[this.COM_VARI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.COM_VARI_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select CVCODICE,CVNUMRIF,CPROWNUM,CVQTAMOV  from "+i_cTable+" COM_VARI ";
          +" where CVQTADIS=0";
          +" order by CVCODICE";
           ,"_Curs_COM_VARI")
    else
      select CVCODICE,CVNUMRIF,CPROWNUM,CVQTAMOV from (i_cTable);
       where CVQTADIS=0;
       order by CVCODICE;
        into cursor _Curs_COM_VARI
    endif
    if used('_Curs_COM_VARI')
      select _Curs_COM_VARI
      locate for 1=1
      do while not(eof())
      this.w_CODDIS = _Curs_COM_VARI.CVCODICE
      this.w_NURIG = _Curs_COM_VARI.CVNUMRIF
      this.w_RIGA = _Curs_COM_VARI.CPROWNUM
      this.w_QTAMOV = _Curs_COM_VARI.CVQTAMOV
      ah_Msg("Elaborazione componente distinta: %1",.T.,.F.,.F., this.w_CODDIS)
      * --- Write into COM_VARI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.COM_VARI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.COM_VARI_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.COM_VARI_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"CVQTADIS ="+cp_NullLink(cp_ToStrODBC(this.w_QTAMOV),'COM_VARI','CVQTADIS');
            +i_ccchkf ;
        +" where ";
            +"CVCODICE = "+cp_ToStrODBC(this.w_CODDIS);
            +" and CVNUMRIF = "+cp_ToStrODBC(this.w_NURIG);
            +" and CPROWNUM = "+cp_ToStrODBC(this.w_RIGA);
               )
      else
        update (i_cTable) set;
            CVQTADIS = this.w_QTAMOV;
            &i_ccchkf. ;
         where;
            CVCODICE = this.w_CODDIS;
            and CVNUMRIF = this.w_NURIG;
            and CPROWNUM = this.w_RIGA;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
        select _Curs_COM_VARI
        continue
      enddo
      use
    endif
    if bTrsErr
      * --- Raise
      i_Error=MESSAGE()
      return
    else
      if this.w_NHF>=0
        this.w_TMPC = Ah_Msgformat("Elaborazione terminata con successo")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = ah_Msgformat("Elaborazione terminata con successo")
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='DISTBASE'
    this.cWorkTables[2]='COM_VARI'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_DISTBASE')
      use in _Curs_DISTBASE
    endif
    if used('_Curs_COM_VARI')
      use in _Curs_COM_VARI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
