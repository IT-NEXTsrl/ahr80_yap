* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvbb89                                                        *
*              Predisposizione archivi di base per la contabilit� avvocati     *
*                                                                              *
*      Author: Zucchetti TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_77]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-05-07                                                      *
* Last revis.: 2008-07-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvbb89",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvbb89 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_C1CODICE = space(5)
  w_C1DESCRI = space(35)
  w_ARCODART = space(20)
  w_ARCATCON = space(5)
  w_CVCODART = space(5)
  w_CVCODCLI = space(5)
  w_MVSERIAL = space(10)
  w_MVROWNUM = 0
  w_MVNUMRIF = 0
  w_MVCODART = space(20)
  w_MVCATCON = space(5)
  w_ARTIPART = space(2)
  w_ARPRESTA = space(1)
  w_ANTIPCON = space(1)
  w_ANCODICE = space(15)
  w_ANDESCRI = space(40)
  w_ANCONSUP = space(15)
  w_ANTIPSOT = space(1)
  w_ANCCTAGG = space(1)
  w_UTCC = 0
  w_UTDC = ctod("  /  /  ")
  w_CVTIPCON = space(1)
  w_CVCONRIC = space(15)
  w_TRCODAZI = space(5)
  w_TRTIPRIT = space(1)
  w_CPROWNUM = 0
  w_TRFLGST1 = space(1)
  w_TRFLGSTO = space(1)
  w_COCODAZI = space(5)
  w_COCONIMB = space(15)
  w_COCONTRA = space(15)
  w_COCACINC = space(5)
  w_COCOCINC = space(15)
  w_COSAABBA = space(15)
  w_COSAABBP = space(15)
  w_COSADIFC = space(15)
  w_COSADIFN = space(15)
  w_CODIFCON = space(15)
  w_COSAPAGA = space(5)
  w_CCCODICE = space(5)
  w_CCDESCRI = space(35)
  w_CCFLPART = space(1)
  w_CCFLANAL = space(1)
  w_CCFLSALI = space(1)
  w_CCFLSALF = space(1)
  w_CCFLCOMP = space(1)
  w_CCFLRIFE = space(1)
  w_CCTIPDOC = space(2)
  w_CCTIPREG = space(1)
  w_CCNUMREG = 0
  w_CCNUMDOC = space(1)
  w_CCFLIVDF = space(1)
  w_CCFLPDIF = space(1)
  w_CCFLSTDA = space(1)
  w_CCCALDOC = space(1)
  w_CCTESDOC = space(1)
  w_CCFLRITE = space(1)
  w_CCSERDOC = space(10)
  w_CCFLPDOC = space(1)
  w_CCSERPRO = space(10)
  w_CCFLPPRO = space(1)
  w_CCCFDAVE = space(1)
  w_CCFLBESE = space(1)
  w_CCFLMOVC = space(1)
  w_CCFLAUTR = space(1)
  w_CCFLINSO = space(1)
  w_CCMOVCES = space(1)
  w_CCDESSUP = space(254)
  w_CCDESRIG = space(254)
  w_COCAUSLD = space(5)
  w_COEVICRE = space(15)
  w_Pia_cron = space(10)
  w_Cro_nolo = space(10)
  w_CCCODICE = space(15)
  w_CCDESCRI = space(40)
  w_CCCODCON = space(15)
  w_CACODICE = space(20)
  * --- WorkFile variables
  CACOARTI_idx=0
  ART_ICOL_idx=0
  CONTVEAC_idx=0
  CONTI_idx=0
  TAB_RITE_idx=0
  CONTROPA_idx=0
  CAU_CONT_idx=0
  COLCRONO_idx=0
  DOC_DETT_idx=0
  CAUMATTI_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Contabilit� avvocati
    if IsAlt()
      * --- 1) caricare le categorie contabili ARTICOLI
      * --- ANT - Anticipazioni art 15
      this.w_C1CODICE = "ANT"
      this.w_C1DESCRI = "Anticipazioni art 15"
      * --- Try
      local bErr_0360D640
      bErr_0360D640=bTrsErr
      this.Try_0360D640()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- Write into CACOARTI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.CACOARTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CACOARTI_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CACOARTI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"C1DESCRI ="+cp_NullLink(cp_ToStrODBC(this.w_C1DESCRI),'CACOARTI','C1DESCRI');
              +i_ccchkf ;
          +" where ";
              +"C1CODICE = "+cp_ToStrODBC(this.w_C1CODICE);
                 )
        else
          update (i_cTable) set;
              C1DESCRI = this.w_C1DESCRI;
              &i_ccchkf. ;
           where;
              C1CODICE = this.w_C1CODICE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        this.w_TMPC = ah_Msgformat("Modificata categoria contabile ANT")
      endif
      bTrsErr=bTrsErr or bErr_0360D640
      * --- End
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      * --- ONO - Onorari e diritti
      this.w_C1CODICE = "ONO"
      this.w_C1DESCRI = "Onorari e diritti"
      * --- Try
      local bErr_0372D770
      bErr_0372D770=bTrsErr
      this.Try_0372D770()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- Write into CACOARTI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.CACOARTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CACOARTI_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CACOARTI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"C1DESCRI ="+cp_NullLink(cp_ToStrODBC(this.w_C1DESCRI),'CACOARTI','C1DESCRI');
              +i_ccchkf ;
          +" where ";
              +"C1CODICE = "+cp_ToStrODBC(this.w_C1CODICE);
                 )
        else
          update (i_cTable) set;
              C1DESCRI = this.w_C1DESCRI;
              &i_ccchkf. ;
           where;
              C1CODICE = this.w_C1CODICE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        this.w_TMPC = ah_Msgformat("Modificata categoria contabile ONO")
      endif
      bTrsErr=bTrsErr or bErr_0372D770
      * --- End
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      * --- SPE - Spese imponibili
      this.w_C1CODICE = "SPE"
      this.w_C1DESCRI = "Spese imponibili"
      * --- Try
      local bErr_0372FCF0
      bErr_0372FCF0=bTrsErr
      this.Try_0372FCF0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- Write into CACOARTI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.CACOARTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CACOARTI_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CACOARTI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"C1DESCRI ="+cp_NullLink(cp_ToStrODBC(this.w_C1DESCRI),'CACOARTI','C1DESCRI');
              +i_ccchkf ;
          +" where ";
              +"C1CODICE = "+cp_ToStrODBC(this.w_C1CODICE);
                 )
        else
          update (i_cTable) set;
              C1DESCRI = this.w_C1DESCRI;
              &i_ccchkf. ;
           where;
              C1CODICE = this.w_C1CODICE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        this.w_TMPC = ah_Msgformat("Modificata categoria contabile SPE")
      endif
      bTrsErr=bTrsErr or bErr_0372FCF0
      * --- End
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      * --- 2) nelle prestazioni (art_ticol) caricare categoria contabile
      * --- Spese
      this.w_ARCATCON = "SPE"
      * --- Select from ART_ICOL
      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select ARCODART  from "+i_cTable+" ART_ICOL ";
            +" where ARTIPART<>'DE' AND ARPRESTA='S'";
             ,"_Curs_ART_ICOL")
      else
        select ARCODART from (i_cTable);
         where ARTIPART<>"DE" AND ARPRESTA="S";
          into cursor _Curs_ART_ICOL
      endif
      if used('_Curs_ART_ICOL')
        select _Curs_ART_ICOL
        locate for 1=1
        do while not(eof())
        this.w_ARCODART = _Curs_ART_ICOL.ARCODART
        * --- Write into ART_ICOL
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ART_ICOL_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ARCATCON ="+cp_NullLink(cp_ToStrODBC(this.w_ARCATCON),'ART_ICOL','ARCATCON');
              +i_ccchkf ;
          +" where ";
              +"ARCODART = "+cp_ToStrODBC(this.w_ARCODART);
                 )
        else
          update (i_cTable) set;
              ARCATCON = this.w_ARCATCON;
              &i_ccchkf. ;
           where;
              ARCODART = this.w_ARCODART;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
          select _Curs_ART_ICOL
          continue
        enddo
        use
      endif
      this.w_TMPC = ah_Msgformat("Modificati articoli con assegnamento categoria contabile SPE")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      * --- Anticipazioni
      this.w_ARCATCON = "ANT"
      * --- Select from ART_ICOL
      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select ARCODART  from "+i_cTable+" ART_ICOL ";
            +" where ARTIPART<>'DE' AND ARPRESTA='A'";
             ,"_Curs_ART_ICOL")
      else
        select ARCODART from (i_cTable);
         where ARTIPART<>"DE" AND ARPRESTA="A";
          into cursor _Curs_ART_ICOL
      endif
      if used('_Curs_ART_ICOL')
        select _Curs_ART_ICOL
        locate for 1=1
        do while not(eof())
        this.w_ARCODART = _Curs_ART_ICOL.ARCODART
        * --- Write into ART_ICOL
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ART_ICOL_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ARCATCON ="+cp_NullLink(cp_ToStrODBC(this.w_ARCATCON),'ART_ICOL','ARCATCON');
              +i_ccchkf ;
          +" where ";
              +"ARCODART = "+cp_ToStrODBC(this.w_ARCODART);
                 )
        else
          update (i_cTable) set;
              ARCATCON = this.w_ARCATCON;
              &i_ccchkf. ;
           where;
              ARCODART = this.w_ARCODART;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
          select _Curs_ART_ICOL
          continue
        enddo
        use
      endif
      this.w_TMPC = ah_Msgformat("Modificati articoli con assegnamento categoria contabile ANT")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      * --- Onorari
      this.w_ARCATCON = "ONO"
      * --- Select from ART_ICOL
      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select ARCODART  from "+i_cTable+" ART_ICOL ";
            +" where ARTIPART<>'DE' AND ARPRESTA<>'S' AND ARPRESTA<>'A'";
             ,"_Curs_ART_ICOL")
      else
        select ARCODART from (i_cTable);
         where ARTIPART<>"DE" AND ARPRESTA<>"S" AND ARPRESTA<>"A";
          into cursor _Curs_ART_ICOL
      endif
      if used('_Curs_ART_ICOL')
        select _Curs_ART_ICOL
        locate for 1=1
        do while not(eof())
        this.w_ARCODART = _Curs_ART_ICOL.ARCODART
        * --- Write into ART_ICOL
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ART_ICOL_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ARCATCON ="+cp_NullLink(cp_ToStrODBC(this.w_ARCATCON),'ART_ICOL','ARCATCON');
              +i_ccchkf ;
          +" where ";
              +"ARCODART = "+cp_ToStrODBC(this.w_ARCODART);
                 )
        else
          update (i_cTable) set;
              ARCATCON = this.w_ARCATCON;
              &i_ccchkf. ;
           where;
              ARCODART = this.w_ARCODART;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
          select _Curs_ART_ICOL
          continue
        enddo
        use
      endif
      this.w_TMPC = ah_Msgformat("Modificati articoli con assegnamento categoria contabile ONO")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      * --- 3) cancellare l'unica contropartita caricata in contropartite parcellazione
      this.w_CVCODART = "SER"
      this.w_CVCODCLI = "ITA"
      * --- Try
      local bErr_035D1E30
      bErr_035D1E30=bTrsErr
      this.Try_035D1E30()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
      endif
      bTrsErr=bTrsErr or bErr_035D1E30
      * --- End
      * --- 4) nei documenti (doc_dett) caricare categoria contabile
      * --- Select from DOC_DETT
      i_nConn=i_TableProp[this.DOC_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select MVSERIAL,CPROWNUM,MVNUMRIF,MVCODART  from "+i_cTable+" DOC_DETT ";
             ,"_Curs_DOC_DETT")
      else
        select MVSERIAL,CPROWNUM,MVNUMRIF,MVCODART from (i_cTable);
          into cursor _Curs_DOC_DETT
      endif
      if used('_Curs_DOC_DETT')
        select _Curs_DOC_DETT
        locate for 1=1
        do while not(eof())
        this.w_MVSERIAL = _Curs_DOC_DETT.MVSERIAL
        this.w_MVROWNUM = _Curs_DOC_DETT.CPROWNUM
        this.w_MVNUMRIF = _Curs_DOC_DETT.MVNUMRIF
        this.w_MVCODART = _Curs_DOC_DETT.MVCODART
        * --- Read from ART_ICOL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ARTIPART,ARPRESTA"+;
            " from "+i_cTable+" ART_ICOL where ";
                +"ARCODART = "+cp_ToStrODBC(this.w_MVCODART);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ARTIPART,ARPRESTA;
            from (i_cTable) where;
                ARCODART = this.w_MVCODART;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_ARTIPART = NVL(cp_ToDate(_read_.ARTIPART),cp_NullValue(_read_.ARTIPART))
          this.w_ARPRESTA = NVL(cp_ToDate(_read_.ARPRESTA),cp_NullValue(_read_.ARPRESTA))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_ARTIPART<>"DE"
          do case
            case this.w_ARPRESTA="S"
              this.w_MVCATCON = "SPE"
            case this.w_ARPRESTA="A"
              this.w_MVCATCON = "ANT"
            otherwise
              this.w_MVCATCON = "ONO"
          endcase
          * --- Write into DOC_DETT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.DOC_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MVCATCON ="+cp_NullLink(cp_ToStrODBC(this.w_MVCATCON),'DOC_DETT','MVCATCON');
                +i_ccchkf ;
            +" where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
                +" and CPROWNUM = "+cp_ToStrODBC(this.w_MVROWNUM);
                +" and MVNUMRIF = "+cp_ToStrODBC(this.w_MVNUMRIF);
                   )
          else
            update (i_cTable) set;
                MVCATCON = this.w_MVCATCON;
                &i_ccchkf. ;
             where;
                MVSERIAL = this.w_MVSERIAL;
                and CPROWNUM = this.w_MVROWNUM;
                and MVNUMRIF = this.w_MVNUMRIF;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
          select _Curs_DOC_DETT
          continue
        enddo
        use
      endif
      this.w_TMPC = ah_Msgformat("Modificati dettagli documenti con assegnamento categoria contabile")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      * --- 5) cancellare la categoria contabile articoli SER
      this.w_C1CODICE = "SER"
      * --- Try
      local bErr_03773720
      bErr_03773720=bTrsErr
      this.Try_03773720()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
      endif
      bTrsErr=bTrsErr or bErr_03773720
      * --- End
      * --- 6) caricare conto contabile 0401009
      this.w_ANTIPCON = "G"
      this.w_ANCCTAGG = "E"
      this.w_UTCC = 1
      this.w_UTDC = CTOD("25/09/2007")
      * --- caricare nuovo conto 0401009  SPESE IMPONIBILI SOSTENUTE       
      *     MASTRO RAGGRUPPAMENTO 0401000     
      *     TIPO CONTO = CONTROPARTITE VENDITE
      this.w_ANCODICE = "0401009"
      this.w_ANDESCRI = "SPESE IMPONIBILI SOSTENUTE"
      this.w_ANCONSUP = "0401000"
      this.w_ANTIPSOT = "V"
      * --- Try
      local bErr_03608990
      bErr_03608990=bTrsErr
      this.Try_03608990()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- Write into CONTI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ANDESCRI ="+cp_NullLink(cp_ToStrODBC(this.w_ANDESCRI),'CONTI','ANDESCRI');
          +",ANCONSUP ="+cp_NullLink(cp_ToStrODBC(this.w_ANCONSUP),'CONTI','ANCONSUP');
          +",ANTIPSOT ="+cp_NullLink(cp_ToStrODBC(this.w_ANTIPSOT),'CONTI','ANTIPSOT');
          +",ANCCTAGG ="+cp_NullLink(cp_ToStrODBC(this.w_ANCCTAGG),'CONTI','ANCCTAGG');
          +",UTCC ="+cp_NullLink(cp_ToStrODBC(this.w_UTCC),'CONTI','UTCC');
          +",UTDC ="+cp_NullLink(cp_ToStrODBC(this.w_UTDC),'CONTI','UTDC');
              +i_ccchkf ;
          +" where ";
              +"ANTIPCON = "+cp_ToStrODBC(this.w_ANTIPCON);
              +" and ANCODICE = "+cp_ToStrODBC(this.w_ANCODICE);
                 )
        else
          update (i_cTable) set;
              ANDESCRI = this.w_ANDESCRI;
              ,ANCONSUP = this.w_ANCONSUP;
              ,ANTIPSOT = this.w_ANTIPSOT;
              ,ANCCTAGG = this.w_ANCCTAGG;
              ,UTCC = this.w_UTCC;
              ,UTDC = this.w_UTDC;
              &i_ccchkf. ;
           where;
              ANTIPCON = this.w_ANTIPCON;
              and ANCODICE = this.w_ANCODICE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        this.w_TMPC = ah_Msgformat("Modificato conto contabile 0401009")
      endif
      bTrsErr=bTrsErr or bErr_03608990
      * --- End
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      * --- 7) caricare le contropartite parcellazione
      this.w_CVCODCLI = "ITA"
      this.w_CVTIPCON = "G"
      * --- categoria contabile articolo ANT
      *     categoria cli/for ITA
      *     conto ricavi 0105004
      this.w_CVCODART = "ANT"
      this.w_CVCONRIC = "0105004"
      * --- Try
      local bErr_0375D2E0
      bErr_0375D2E0=bTrsErr
      this.Try_0375D2E0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- Write into CONTVEAC
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.CONTVEAC_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTVEAC_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTVEAC_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CVTIPCON ="+cp_NullLink(cp_ToStrODBC(this.w_CVTIPCON),'CONTVEAC','CVTIPCON');
          +",CVCONRIC ="+cp_NullLink(cp_ToStrODBC(this.w_CVCONRIC),'CONTVEAC','CVCONRIC');
              +i_ccchkf ;
          +" where ";
              +"CVCODART = "+cp_ToStrODBC(this.w_CVCODART);
              +" and CVCODCLI = "+cp_ToStrODBC(this.w_CVCODCLI);
                 )
        else
          update (i_cTable) set;
              CVTIPCON = this.w_CVTIPCON;
              ,CVCONRIC = this.w_CVCONRIC;
              &i_ccchkf. ;
           where;
              CVCODART = this.w_CVCODART;
              and CVCODCLI = this.w_CVCODCLI;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        this.w_TMPC = ah_Msgformat("Modificata contropartita parcellazione ITA-ANT")
      endif
      bTrsErr=bTrsErr or bErr_0375D2E0
      * --- End
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      * --- categoria contabile articolo ONO
      *     categoria cli/for ITA
      *     conto ricavi 0401001     
      this.w_CVCODART = "ONO"
      this.w_CVCONRIC = "0401001"
      * --- Try
      local bErr_035D1200
      bErr_035D1200=bTrsErr
      this.Try_035D1200()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- Write into CONTVEAC
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.CONTVEAC_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTVEAC_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTVEAC_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CVTIPCON ="+cp_NullLink(cp_ToStrODBC(this.w_CVTIPCON),'CONTVEAC','CVTIPCON');
          +",CVCONRIC ="+cp_NullLink(cp_ToStrODBC(this.w_CVCONRIC),'CONTVEAC','CVCONRIC');
              +i_ccchkf ;
          +" where ";
              +"CVCODART = "+cp_ToStrODBC(this.w_CVCODART);
              +" and CVCODCLI = "+cp_ToStrODBC(this.w_CVCODCLI);
                 )
        else
          update (i_cTable) set;
              CVTIPCON = this.w_CVTIPCON;
              ,CVCONRIC = this.w_CVCONRIC;
              &i_ccchkf. ;
           where;
              CVCODART = this.w_CVCODART;
              and CVCODCLI = this.w_CVCODCLI;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        this.w_TMPC = ah_Msgformat("Modificata contropartita parcellazione ITA-ONO")
      endif
      bTrsErr=bTrsErr or bErr_035D1200
      * --- End
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      * --- categoria contabile articolo SPE
      *     categoria cli/for ITA
      *     conto ricavi 0401006 
      this.w_CVCODART = "SPE"
      this.w_CVCONRIC = "0401009"
      * --- Try
      local bErr_03617E50
      bErr_03617E50=bTrsErr
      this.Try_03617E50()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- Write into CONTVEAC
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.CONTVEAC_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTVEAC_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTVEAC_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CVTIPCON ="+cp_NullLink(cp_ToStrODBC(this.w_CVTIPCON),'CONTVEAC','CVTIPCON');
          +",CVCONRIC ="+cp_NullLink(cp_ToStrODBC(this.w_CVCONRIC),'CONTVEAC','CVCONRIC');
              +i_ccchkf ;
          +" where ";
              +"CVCODART = "+cp_ToStrODBC(this.w_CVCODART);
              +" and CVCODCLI = "+cp_ToStrODBC(this.w_CVCODCLI);
                 )
        else
          update (i_cTable) set;
              CVTIPCON = this.w_CVTIPCON;
              ,CVCONRIC = this.w_CVCONRIC;
              &i_ccchkf. ;
           where;
              CVCODART = this.w_CVCODART;
              and CVCODCLI = this.w_CVCODCLI;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        this.w_TMPC = ah_Msgformat("Modificata contropartita parcellazione ITA-SPE")
      endif
      bTrsErr=bTrsErr or bErr_03617E50
      * --- End
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      * --- 8) Piano dei conti
      this.w_ANTIPCON = "G"
      * --- modificare descrizione conto 0401001 in "ONORARI E DIRITTI "
      this.w_ANCODICE = "0401001"
      this.w_ANDESCRI = "ONORARI E DIRITTI"
      * --- Write into CONTI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTI_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"ANDESCRI ="+cp_NullLink(cp_ToStrODBC(this.w_ANDESCRI),'CONTI','ANDESCRI');
            +i_ccchkf ;
        +" where ";
            +"ANTIPCON = "+cp_ToStrODBC(this.w_ANTIPCON);
            +" and ANCODICE = "+cp_ToStrODBC(this.w_ANCODICE);
               )
      else
        update (i_cTable) set;
            ANDESCRI = this.w_ANDESCRI;
            &i_ccchkf. ;
         where;
            ANTIPCON = this.w_ANTIPCON;
            and ANCODICE = this.w_ANCODICE;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      this.w_TMPC = ah_Msgformat("Modificato conto contabile 0401001")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      * --- conto 0105004
      *     modificare 
      *     TIPO CONTO = CONTROPARTITE VENDITE
      this.w_ANCODICE = "0105004"
      this.w_ANTIPSOT = "V"
      * --- Write into CONTI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTI_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"ANTIPSOT ="+cp_NullLink(cp_ToStrODBC(this.w_ANTIPSOT),'CONTI','ANTIPSOT');
            +i_ccchkf ;
        +" where ";
            +"ANTIPCON = "+cp_ToStrODBC(this.w_ANTIPCON);
            +" and ANCODICE = "+cp_ToStrODBC(this.w_ANCODICE);
               )
      else
        update (i_cTable) set;
            ANTIPSOT = this.w_ANTIPSOT;
            &i_ccchkf. ;
         where;
            ANTIPCON = this.w_ANTIPCON;
            and ANCODICE = this.w_ANCODICE;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      this.w_TMPC = ah_Msgformat("Modificato conto contabile 0105004")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      * --- tutti conti di ricavo (quelli che iniziano con 04) modificare 
      *     TIPO CONTO = CONTROPARTITE VENDITE
      this.w_ANTIPSOT = "V"
      * --- Select from CONTI
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select ANCODICE  from "+i_cTable+" CONTI ";
            +" where ANTIPCON="+cp_ToStrODBC(this.w_ANTIPCON)+" AND LEFT(ANCODICE,2)='04'";
             ,"_Curs_CONTI")
      else
        select ANCODICE from (i_cTable);
         where ANTIPCON=this.w_ANTIPCON AND LEFT(ANCODICE,2)="04";
          into cursor _Curs_CONTI
      endif
      if used('_Curs_CONTI')
        select _Curs_CONTI
        locate for 1=1
        do while not(eof())
        this.w_ANCODICE = _Curs_CONTI.ANCODICE
        * --- Write into CONTI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ANTIPSOT ="+cp_NullLink(cp_ToStrODBC(this.w_ANTIPSOT),'CONTI','ANTIPSOT');
              +i_ccchkf ;
          +" where ";
              +"ANTIPCON = "+cp_ToStrODBC(this.w_ANTIPCON);
              +" and ANCODICE = "+cp_ToStrODBC(this.w_ANCODICE);
                 )
        else
          update (i_cTable) set;
              ANTIPSOT = this.w_ANTIPSOT;
              &i_ccchkf. ;
           where;
              ANTIPCON = this.w_ANTIPCON;
              and ANCODICE = this.w_ANCODICE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
          select _Curs_CONTI
          continue
        enddo
        use
      endif
      this.w_TMPC = ah_Msgformat("Modificati conti con assegnamento tipo sottoconto Contropartite vendite")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      * --- 9) TABELLE RITENUTE - togliere in entrambe le tabelle il check storno immediato
      this.w_TRCODAZI = i_CODAZI
      this.w_TRFLGST1 = "N"
      this.w_TRFLGSTO = "N"
      this.w_TRTIPRIT = "A"
      * --- Select from TAB_RITE
      i_nConn=i_TableProp[this.TAB_RITE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TAB_RITE_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select CPROWNUM  from "+i_cTable+" TAB_RITE ";
            +" where TRCODAZI="+cp_ToStrODBC(this.w_TRCODAZI)+" AND TRTIPRIT="+cp_ToStrODBC(this.w_TRTIPRIT)+"";
             ,"_Curs_TAB_RITE")
      else
        select CPROWNUM from (i_cTable);
         where TRCODAZI=this.w_TRCODAZI AND TRTIPRIT=this.w_TRTIPRIT;
          into cursor _Curs_TAB_RITE
      endif
      if used('_Curs_TAB_RITE')
        select _Curs_TAB_RITE
        locate for 1=1
        do while not(eof())
        this.w_CPROWNUM = _Curs_TAB_RITE.CPROWNUM
        * --- Write into TAB_RITE
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.TAB_RITE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TAB_RITE_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TAB_RITE_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"TRFLGSTO ="+cp_NullLink(cp_ToStrODBC(this.w_TRFLGSTO),'TAB_RITE','TRFLGSTO');
              +i_ccchkf ;
          +" where ";
              +"TRCODAZI = "+cp_ToStrODBC(this.w_TRCODAZI);
              +" and TRTIPRIT = "+cp_ToStrODBC(this.w_TRTIPRIT);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
                 )
        else
          update (i_cTable) set;
              TRFLGSTO = this.w_TRFLGSTO;
              &i_ccchkf. ;
           where;
              TRCODAZI = this.w_TRCODAZI;
              and TRTIPRIT = this.w_TRTIPRIT;
              and CPROWNUM = this.w_CPROWNUM;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
          select _Curs_TAB_RITE
          continue
        enddo
        use
      endif
      this.w_TRTIPRIT = "V"
      * --- Select from TAB_RITE
      i_nConn=i_TableProp[this.TAB_RITE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TAB_RITE_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select CPROWNUM  from "+i_cTable+" TAB_RITE ";
            +" where TRCODAZI="+cp_ToStrODBC(this.w_TRCODAZI)+" AND TRTIPRIT="+cp_ToStrODBC(this.w_TRTIPRIT)+"";
             ,"_Curs_TAB_RITE")
      else
        select CPROWNUM from (i_cTable);
         where TRCODAZI=this.w_TRCODAZI AND TRTIPRIT=this.w_TRTIPRIT;
          into cursor _Curs_TAB_RITE
      endif
      if used('_Curs_TAB_RITE')
        select _Curs_TAB_RITE
        locate for 1=1
        do while not(eof())
        this.w_CPROWNUM = _Curs_TAB_RITE.CPROWNUM
        * --- Write into TAB_RITE
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.TAB_RITE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TAB_RITE_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TAB_RITE_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"TRFLGST1 ="+cp_NullLink(cp_ToStrODBC(this.w_TRFLGST1),'TAB_RITE','TRFLGST1');
              +i_ccchkf ;
          +" where ";
              +"TRCODAZI = "+cp_ToStrODBC(this.w_TRCODAZI);
              +" and TRTIPRIT = "+cp_ToStrODBC(this.w_TRTIPRIT);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
                 )
        else
          update (i_cTable) set;
              TRFLGST1 = this.w_TRFLGST1;
              &i_ccchkf. ;
           where;
              TRCODAZI = this.w_TRCODAZI;
              and TRTIPRIT = this.w_TRTIPRIT;
              and CPROWNUM = this.w_CPROWNUM;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
          select _Curs_TAB_RITE
          continue
        enddo
        use
      endif
      this.w_TMPC = ah_Msgformat("Modificate tabelle ritenute togliendo il check storno immediato")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      * --- 10) PARAMETRI PARCELLAZIONE
      *     -spese generali 0401006     12,5 SPESE 
      *     -cassa previdenza 0205011  cpa da versare
      *     -causale acconti incassati 901 
      *     -conto acconti incassati 0101001   CASSA
      this.w_COCODAZI = i_CODAZI
      this.w_COCONIMB = "0401006"
      this.w_COCONTRA = "0205011"
      this.w_COCACINC = "901"
      this.w_COCOCINC = "0101001"
      * --- Write into CONTROPA
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.CONTROPA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTROPA_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTROPA_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"COCONIMB ="+cp_NullLink(cp_ToStrODBC(this.w_COCONIMB),'CONTROPA','COCONIMB');
        +",COCONTRA ="+cp_NullLink(cp_ToStrODBC(this.w_COCONTRA),'CONTROPA','COCONTRA');
        +",COCACINC ="+cp_NullLink(cp_ToStrODBC(this.w_COCACINC),'CONTROPA','COCACINC');
        +",COCOCINC ="+cp_NullLink(cp_ToStrODBC(this.w_COCOCINC),'CONTROPA','COCOCINC');
            +i_ccchkf ;
        +" where ";
            +"COCODAZI = "+cp_ToStrODBC(this.w_COCODAZI);
               )
      else
        update (i_cTable) set;
            COCONIMB = this.w_COCONIMB;
            ,COCONTRA = this.w_COCONTRA;
            ,COCACINC = this.w_COCACINC;
            ,COCOCINC = this.w_COCOCINC;
            &i_ccchkf. ;
         where;
            COCODAZI = this.w_COCODAZI;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      this.w_TMPC = ah_Msgformat("Modificati parametri parcellazione per spese generali, cassa previdenza, causali acconti incassati e conto acconti incassati")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      * --- 11) caricare tabella PARAMETRI DIFFERENZE E ABBUONI
      *     ABBUONI ATTIVI 0403002     
      *     abbuoni passivi 0307007    
      *     diff cambi attiva 0403006   
      *     diff cambi negative 0307008      
      *     diff conversione 0205012    
      *     pagamento abbuoni anticipi RD   
      this.w_COCODAZI = i_CODAZI
      this.w_COSAABBA = "0403002"
      this.w_COSAABBP = "0307007"
      this.w_COSADIFC = "0403006"
      this.w_COSADIFN = "0307008"
      this.w_CODIFCON = "0205012"
      this.w_COSAPAGA = "RD"
      * --- Write into CONTROPA
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.CONTROPA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTROPA_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTROPA_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"COSAABBA ="+cp_NullLink(cp_ToStrODBC(this.w_COSAABBA),'CONTROPA','COSAABBA');
        +",COSAABBP ="+cp_NullLink(cp_ToStrODBC(this.w_COSAABBP),'CONTROPA','COSAABBP');
        +",COSADIFC ="+cp_NullLink(cp_ToStrODBC(this.w_COSADIFC),'CONTROPA','COSADIFC');
        +",COSADIFN ="+cp_NullLink(cp_ToStrODBC(this.w_COSADIFN),'CONTROPA','COSADIFN');
        +",CODIFCON ="+cp_NullLink(cp_ToStrODBC(this.w_CODIFCON),'CONTROPA','CODIFCON');
        +",COSAPAGA ="+cp_NullLink(cp_ToStrODBC(this.w_COSAPAGA),'CONTROPA','COSAPAGA');
            +i_ccchkf ;
        +" where ";
            +"COCODAZI = "+cp_ToStrODBC(this.w_COCODAZI);
               )
      else
        update (i_cTable) set;
            COSAABBA = this.w_COSAABBA;
            ,COSAABBP = this.w_COSAABBP;
            ,COSADIFC = this.w_COSADIFC;
            ,COSADIFN = this.w_COSADIFN;
            ,CODIFCON = this.w_CODIFCON;
            ,COSAPAGA = this.w_COSAPAGA;
            &i_ccchkf. ;
         where;
            COCODAZI = this.w_COCODAZI;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      this.w_TMPC = ah_Msgformat("Modificati parametri differenze e abbuoni per abbuoni attivi, abbuoni passivi, differenza cambi passiva, differenza cambi attiva, differenza conversione e pagamento abbuoni anticipi")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      * --- 12) Causale contabile XGIRR
      *     Giroconto ritenute
      *     Salda partite
      this.w_CCCODICE = "XGIRR"
      this.w_CCDESCRI = "Giroconto ritenute"
      this.w_CCFLPART = "S"
      this.w_CCFLANAL = " "
      this.w_CCFLSALI = "N"
      this.w_CCFLSALF = "N"
      this.w_CCFLCOMP = "N"
      this.w_CCFLRIFE = "N"
      this.w_CCTIPDOC = "NO"
      this.w_CCTIPREG = "N"
      this.w_CCNUMREG = 0
      this.w_CCNUMDOC = " "
      this.w_CCFLIVDF = " "
      this.w_CCFLPDIF = " "
      this.w_CCFLSTDA = " "
      this.w_CCCALDOC = "N"
      this.w_CCTESDOC = " "
      this.w_CCFLRITE = " "
      this.w_CCSERDOC = "          "
      this.w_CCFLPDOC = "N"
      this.w_CCSERPRO = "          "
      this.w_CCFLPPRO = "N"
      this.w_CCCFDAVE = " "
      this.w_CCFLBESE = "B"
      this.w_CCFLMOVC = " "
      this.w_CCFLAUTR = " "
      this.w_CCFLINSO = " "
      this.w_CCMOVCES = " "
      this.w_CCDESSUP = SPACE(254)
      this.w_CCDESRIG = SPACE(254)
      this.w_UTCC = 1
      this.w_UTDC = CTOD("25/09/2007")
      * --- Try
      local bErr_0360EA50
      bErr_0360EA50=bTrsErr
      this.Try_0360EA50()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- Write into CAU_CONT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.CAU_CONT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAU_CONT_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CAU_CONT_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CCDESCRI ="+cp_NullLink(cp_ToStrODBC(this.w_CCDESCRI),'CAU_CONT','CCDESCRI');
          +",CCFLPART ="+cp_NullLink(cp_ToStrODBC(this.w_CCFLPART),'CAU_CONT','CCFLPART');
          +",CCFLANAL ="+cp_NullLink(cp_ToStrODBC(this.w_CCFLANAL),'CAU_CONT','CCFLANAL');
          +",CCFLSALI ="+cp_NullLink(cp_ToStrODBC(this.w_CCFLSALI),'CAU_CONT','CCFLSALI');
          +",CCFLSALF ="+cp_NullLink(cp_ToStrODBC(this.w_CCFLSALF),'CAU_CONT','CCFLSALF');
          +",CCFLCOMP ="+cp_NullLink(cp_ToStrODBC(this.w_CCFLCOMP),'CAU_CONT','CCFLCOMP');
          +",CCFLRIFE ="+cp_NullLink(cp_ToStrODBC(this.w_CCFLRIFE),'CAU_CONT','CCFLRIFE');
          +",CCTIPDOC ="+cp_NullLink(cp_ToStrODBC(this.w_CCTIPDOC),'CAU_CONT','CCTIPDOC');
          +",CCTIPREG ="+cp_NullLink(cp_ToStrODBC(this.w_CCTIPREG),'CAU_CONT','CCTIPREG');
          +",CCNUMREG ="+cp_NullLink(cp_ToStrODBC(this.w_CCNUMREG),'CAU_CONT','CCNUMREG');
          +",CCNUMDOC ="+cp_NullLink(cp_ToStrODBC(this.w_CCNUMDOC),'CAU_CONT','CCNUMDOC');
          +",CCFLIVDF ="+cp_NullLink(cp_ToStrODBC(this.w_CCFLIVDF),'CAU_CONT','CCFLIVDF');
          +",CCFLPDIF ="+cp_NullLink(cp_ToStrODBC(this.w_CCFLPDIF),'CAU_CONT','CCFLPDIF');
          +",CCFLSTDA ="+cp_NullLink(cp_ToStrODBC(this.w_CCFLSTDA),'CAU_CONT','CCFLSTDA');
          +",CCCALDOC ="+cp_NullLink(cp_ToStrODBC(this.w_CCCALDOC),'CAU_CONT','CCCALDOC');
          +",CCTESDOC ="+cp_NullLink(cp_ToStrODBC(this.w_CCTESDOC),'CAU_CONT','CCTESDOC');
          +",CCFLRITE ="+cp_NullLink(cp_ToStrODBC(this.w_CCFLRITE),'CAU_CONT','CCFLRITE');
          +",CCSERDOC ="+cp_NullLink(cp_ToStrODBC(this.w_CCSERDOC),'CAU_CONT','CCSERDOC');
          +",CCFLPDOC ="+cp_NullLink(cp_ToStrODBC(this.w_CCFLPDOC),'CAU_CONT','CCFLPDOC');
          +",CCSERPRO ="+cp_NullLink(cp_ToStrODBC(this.w_CCSERPRO),'CAU_CONT','CCSERPRO');
          +",CCFLPPRO ="+cp_NullLink(cp_ToStrODBC(this.w_CCFLPPRO),'CAU_CONT','CCFLPPRO');
          +",CCCFDAVE ="+cp_NullLink(cp_ToStrODBC(this.w_CCCFDAVE),'CAU_CONT','CCCFDAVE');
          +",UTCC ="+cp_NullLink(cp_ToStrODBC(this.w_UTCC),'CAU_CONT','UTCC');
          +",UTDC ="+cp_NullLink(cp_ToStrODBC(this.w_UTDC),'CAU_CONT','UTDC');
          +",CCFLBESE ="+cp_NullLink(cp_ToStrODBC(this.w_CCFLBESE),'CAU_CONT','CCFLBESE');
          +",CCFLMOVC ="+cp_NullLink(cp_ToStrODBC(this.w_CCFLMOVC),'CAU_CONT','CCFLMOVC');
          +",CCFLAUTR ="+cp_NullLink(cp_ToStrODBC(this.w_CCFLAUTR),'CAU_CONT','CCFLAUTR');
          +",CCFLINSO ="+cp_NullLink(cp_ToStrODBC(this.w_CCFLINSO),'CAU_CONT','CCFLINSO');
          +",CCMOVCES ="+cp_NullLink(cp_ToStrODBC(this.w_CCMOVCES),'CAU_CONT','CCMOVCES');
          +",CCDESSUP ="+cp_NullLink(cp_ToStrODBC(this.w_CCDESSUP),'CAU_CONT','CCDESSUP');
          +",CCDESRIG ="+cp_NullLink(cp_ToStrODBC(this.w_CCDESRIG),'CAU_CONT','CCDESRIG');
              +i_ccchkf ;
          +" where ";
              +"CCCODICE = "+cp_ToStrODBC(this.w_CCCODICE);
                 )
        else
          update (i_cTable) set;
              CCDESCRI = this.w_CCDESCRI;
              ,CCFLPART = this.w_CCFLPART;
              ,CCFLANAL = this.w_CCFLANAL;
              ,CCFLSALI = this.w_CCFLSALI;
              ,CCFLSALF = this.w_CCFLSALF;
              ,CCFLCOMP = this.w_CCFLCOMP;
              ,CCFLRIFE = this.w_CCFLRIFE;
              ,CCTIPDOC = this.w_CCTIPDOC;
              ,CCTIPREG = this.w_CCTIPREG;
              ,CCNUMREG = this.w_CCNUMREG;
              ,CCNUMDOC = this.w_CCNUMDOC;
              ,CCFLIVDF = this.w_CCFLIVDF;
              ,CCFLPDIF = this.w_CCFLPDIF;
              ,CCFLSTDA = this.w_CCFLSTDA;
              ,CCCALDOC = this.w_CCCALDOC;
              ,CCTESDOC = this.w_CCTESDOC;
              ,CCFLRITE = this.w_CCFLRITE;
              ,CCSERDOC = this.w_CCSERDOC;
              ,CCFLPDOC = this.w_CCFLPDOC;
              ,CCSERPRO = this.w_CCSERPRO;
              ,CCFLPPRO = this.w_CCFLPPRO;
              ,CCCFDAVE = this.w_CCCFDAVE;
              ,UTCC = this.w_UTCC;
              ,UTDC = this.w_UTDC;
              ,CCFLBESE = this.w_CCFLBESE;
              ,CCFLMOVC = this.w_CCFLMOVC;
              ,CCFLAUTR = this.w_CCFLAUTR;
              ,CCFLINSO = this.w_CCFLINSO;
              ,CCMOVCES = this.w_CCMOVCES;
              ,CCDESSUP = this.w_CCDESSUP;
              ,CCDESRIG = this.w_CCDESRIG;
              &i_ccchkf. ;
           where;
              CCCODICE = this.w_CCCODICE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        this.w_TMPC = ah_Msgformat("Modificata causale contabile XGIRR")
      endif
      bTrsErr=bTrsErr or bErr_0360EA50
      * --- End
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      * --- 13) PARAMETRI RITENUTE
      *     -causale saldaconto XGIRR
      *     -credito vs erario 0105002    
      this.w_COCODAZI = i_CODAZI
      this.w_COCAUSLD = "XGIRR"
      this.w_COEVICRE = "0105002"
      * --- Write into CONTROPA
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.CONTROPA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTROPA_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTROPA_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"COCAUSLD ="+cp_NullLink(cp_ToStrODBC(this.w_COCAUSLD),'CONTROPA','COCAUSLD');
        +",COEVICRE ="+cp_NullLink(cp_ToStrODBC(this.w_COEVICRE),'CONTROPA','COEVICRE');
            +i_ccchkf ;
        +" where ";
            +"COCODAZI = "+cp_ToStrODBC(this.w_COCODAZI);
               )
      else
        update (i_cTable) set;
            COCAUSLD = this.w_COCAUSLD;
            ,COEVICRE = this.w_COEVICRE;
            &i_ccchkf. ;
         where;
            COCODAZI = this.w_COCODAZI;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      this.w_TMPC = ah_Msgformat("Modificati parametri ritenute per causale saldaconto e credito verso erario")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      * --- 13) COLONNE CRONOLOGICO
      *     I DBF devono essere contenuti nella cartella ALTE\exe\Files_x_Import
      * --- Se esistono i DBF da cui attingere, si estraggono i dati in un cursore e si inseriscono nella tabella COLCRONO
      this.w_Pia_cron = "..\ALTE\exe\Files_x_Import\PIA_CRON.DBF"
      this.w_Cro_nolo = "..\ALTE\exe\Files_x_Import\CRO_NOLO.DBF"
      if cp_FileExist(this.w_Pia_cron) AND cp_FileExist(this.w_Cro_nolo)
        SELECT CRO_NOLO.CRNUMCOL, CRO_NOLO.CRDESCRI, PIA_CRON.PC_CONTO FROM (this.w_Cro_nolo) AS CRO_NOLO INNER JOIN (this.w_Pia_cron) AS PIA_CRON ON CRO_NOLO.CRNUMCOL=PIA_CRON.CRNUMCOL INTO CURSOR TMP_CRONOLO
        if USED("TMP_CRONOLO")
          SELECT TMP_CRONOLO
          this.w_ANTIPCON = "G"
          SCAN
          * --- Try
          local bErr_035FAC60
          bErr_035FAC60=bTrsErr
          this.Try_035FAC60()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
          endif
          bTrsErr=bTrsErr or bErr_035FAC60
          * --- End
          ENDSCAN
          USE
        endif
      endif
      * --- 14) classificazione Acconto onorari
      *     modificare il valore del campo classificazione da "onorario civile" a "diritto civile" relativamente alla prestazione con codice "Acconto onorari"
      this.w_ARCODART = "Acconto onorari"
      this.w_ARPRESTA = "C"
      * --- Write into ART_ICOL
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ART_ICOL_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"ARPRESTA ="+cp_NullLink(cp_ToStrODBC(this.w_ARPRESTA),'ART_ICOL','ARPRESTA');
            +i_ccchkf ;
        +" where ";
            +"ARCODART = "+cp_ToStrODBC(this.w_ARCODART);
               )
      else
        update (i_cTable) set;
            ARPRESTA = this.w_ARPRESTA;
            &i_ccchkf. ;
         where;
            ARCODART = this.w_ARCODART;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      this.w_TMPC = ah_Msgformat("Modificata prestazione acconto onorari")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      * --- 15)  TIPI ATTIVITA'
      *     La categoria 'Appuntamento fuori sede' ('F') deve essere convertita in 'Appuntamento' ('S') .
      * --- Select from CAUMATTI
      i_nConn=i_TableProp[this.CAUMATTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAUMATTI_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select CACODICE  from "+i_cTable+" CAUMATTI ";
            +" where CARAGGST='F'";
             ,"_Curs_CAUMATTI")
      else
        select CACODICE from (i_cTable);
         where CARAGGST="F";
          into cursor _Curs_CAUMATTI
      endif
      if used('_Curs_CAUMATTI')
        select _Curs_CAUMATTI
        locate for 1=1
        do while not(eof())
        this.w_CACODICE = _Curs_CAUMATTI.CACODICE
        * --- Pone ad 'S' (Appuntamento) il campo relativo alla Categoria
        * --- Write into CAUMATTI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.CAUMATTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAUMATTI_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CAUMATTI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CARAGGST ="+cp_NullLink(cp_ToStrODBC("S"),'CAUMATTI','CARAGGST');
              +i_ccchkf ;
          +" where ";
              +"CACODICE = "+cp_ToStrODBC(this.w_CACODICE);
              +" and CARAGGST = "+cp_ToStrODBC("F");
                 )
        else
          update (i_cTable) set;
              CARAGGST = "S";
              &i_ccchkf. ;
           where;
              CACODICE = this.w_CACODICE;
              and CARAGGST = "F";

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
          select _Curs_CAUMATTI
          continue
        enddo
        use
      endif
      this.w_TMPC = ah_Msgformat("Nei tipi attivit� convertita la categoria Appuntamento fuori sede con Appuntamento")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    else
      this.w_TMPC = ah_Msgformat("Procedura di conversione eseguita con successo")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    * --- Resoconto - sempre positivo (da gestire eventuali errori)
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Elaborazione terminata con successo")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = ah_Msgformat("Elaborazione terminata con successo")
  endproc
  proc Try_0360D640()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into CACOARTI
    i_nConn=i_TableProp[this.CACOARTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CACOARTI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CACOARTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"C1CODICE"+",C1DESCRI"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_C1CODICE),'CACOARTI','C1CODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_C1DESCRI),'CACOARTI','C1DESCRI');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'C1CODICE',this.w_C1CODICE,'C1DESCRI',this.w_C1DESCRI)
      insert into (i_cTable) (C1CODICE,C1DESCRI &i_ccchkf. );
         values (;
           this.w_C1CODICE;
           ,this.w_C1DESCRI;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    this.w_TMPC = ah_Msgformat("Inserita categoria contabile ANT")
    return
  proc Try_0372D770()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into CACOARTI
    i_nConn=i_TableProp[this.CACOARTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CACOARTI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CACOARTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"C1CODICE"+",C1DESCRI"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_C1CODICE),'CACOARTI','C1CODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_C1DESCRI),'CACOARTI','C1DESCRI');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'C1CODICE',this.w_C1CODICE,'C1DESCRI',this.w_C1DESCRI)
      insert into (i_cTable) (C1CODICE,C1DESCRI &i_ccchkf. );
         values (;
           this.w_C1CODICE;
           ,this.w_C1DESCRI;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    this.w_TMPC = ah_Msgformat("Inserita categoria contabile ONO")
    return
  proc Try_0372FCF0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into CACOARTI
    i_nConn=i_TableProp[this.CACOARTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CACOARTI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CACOARTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"C1CODICE"+",C1DESCRI"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_C1CODICE),'CACOARTI','C1CODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_C1DESCRI),'CACOARTI','C1DESCRI');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'C1CODICE',this.w_C1CODICE,'C1DESCRI',this.w_C1DESCRI)
      insert into (i_cTable) (C1CODICE,C1DESCRI &i_ccchkf. );
         values (;
           this.w_C1CODICE;
           ,this.w_C1DESCRI;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    this.w_TMPC = ah_Msgformat("Inserita categoria contabile SPE")
    return
  proc Try_035D1E30()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Delete from CONTVEAC
    i_nConn=i_TableProp[this.CONTVEAC_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTVEAC_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"CVCODART = "+cp_ToStrODBC(this.w_CVCODART);
            +" and CVCODCLI = "+cp_ToStrODBC(this.w_CVCODCLI);
             )
    else
      delete from (i_cTable) where;
            CVCODART = this.w_CVCODART;
            and CVCODCLI = this.w_CVCODCLI;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Sembra che questa cancellazione sia eseguita con successo;
    *     in realt� la successiva cancellazione di 'SER' dalle categorie contabili articoli segnala l'errore dovuto alla presenza di 'SER' nella contropartita parcellazione 'SER-CLI'
    this.w_TMPC = ah_Msgformat("Cancellata contropartita parcellazione SER-ITA")
    this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    return
  proc Try_03773720()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Delete from CACOARTI
    i_nConn=i_TableProp[this.CACOARTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CACOARTI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"C1CODICE = "+cp_ToStrODBC(this.w_C1CODICE);
             )
    else
      delete from (i_cTable) where;
            C1CODICE = this.w_C1CODICE;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    this.w_TMPC = ah_Msgformat("Cancellata categoria contabile SER")
    this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    return
  proc Try_03608990()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into CONTI
    i_nConn=i_TableProp[this.CONTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CONTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"ANTIPCON"+",ANCODICE"+",ANDESCRI"+",ANCONSUP"+",ANTIPSOT"+",ANCCTAGG"+",UTCC"+",UTDC"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_ANTIPCON),'CONTI','ANTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ANCODICE),'CONTI','ANCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ANDESCRI),'CONTI','ANDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ANCONSUP),'CONTI','ANCONSUP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ANTIPSOT),'CONTI','ANTIPSOT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ANCCTAGG),'CONTI','ANCCTAGG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_UTCC),'CONTI','UTCC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_UTDC),'CONTI','UTDC');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'ANTIPCON',this.w_ANTIPCON,'ANCODICE',this.w_ANCODICE,'ANDESCRI',this.w_ANDESCRI,'ANCONSUP',this.w_ANCONSUP,'ANTIPSOT',this.w_ANTIPSOT,'ANCCTAGG',this.w_ANCCTAGG,'UTCC',this.w_UTCC,'UTDC',this.w_UTDC)
      insert into (i_cTable) (ANTIPCON,ANCODICE,ANDESCRI,ANCONSUP,ANTIPSOT,ANCCTAGG,UTCC,UTDC &i_ccchkf. );
         values (;
           this.w_ANTIPCON;
           ,this.w_ANCODICE;
           ,this.w_ANDESCRI;
           ,this.w_ANCONSUP;
           ,this.w_ANTIPSOT;
           ,this.w_ANCCTAGG;
           ,this.w_UTCC;
           ,this.w_UTDC;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    this.w_TMPC = ah_Msgformat("Inserito conto contabile 0401009")
    return
  proc Try_0375D2E0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into CONTVEAC
    i_nConn=i_TableProp[this.CONTVEAC_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTVEAC_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CONTVEAC_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CVCODART"+",CVCODCLI"+",CVTIPCON"+",CVCONRIC"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CVCODART),'CONTVEAC','CVCODART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CVCODCLI),'CONTVEAC','CVCODCLI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CVTIPCON),'CONTVEAC','CVTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CVCONRIC),'CONTVEAC','CVCONRIC');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CVCODART',this.w_CVCODART,'CVCODCLI',this.w_CVCODCLI,'CVTIPCON',this.w_CVTIPCON,'CVCONRIC',this.w_CVCONRIC)
      insert into (i_cTable) (CVCODART,CVCODCLI,CVTIPCON,CVCONRIC &i_ccchkf. );
         values (;
           this.w_CVCODART;
           ,this.w_CVCODCLI;
           ,this.w_CVTIPCON;
           ,this.w_CVCONRIC;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    this.w_TMPC = ah_Msgformat("Inserita contropartita parcellazione ITA-ANT")
    return
  proc Try_035D1200()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into CONTVEAC
    i_nConn=i_TableProp[this.CONTVEAC_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTVEAC_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CONTVEAC_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CVCODART"+",CVCODCLI"+",CVTIPCON"+",CVCONRIC"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CVCODART),'CONTVEAC','CVCODART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CVCODCLI),'CONTVEAC','CVCODCLI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CVTIPCON),'CONTVEAC','CVTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CVCONRIC),'CONTVEAC','CVCONRIC');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CVCODART',this.w_CVCODART,'CVCODCLI',this.w_CVCODCLI,'CVTIPCON',this.w_CVTIPCON,'CVCONRIC',this.w_CVCONRIC)
      insert into (i_cTable) (CVCODART,CVCODCLI,CVTIPCON,CVCONRIC &i_ccchkf. );
         values (;
           this.w_CVCODART;
           ,this.w_CVCODCLI;
           ,this.w_CVTIPCON;
           ,this.w_CVCONRIC;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    this.w_TMPC = ah_Msgformat("Inserita contropartita parcellazione ITA-ONO")
    return
  proc Try_03617E50()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into CONTVEAC
    i_nConn=i_TableProp[this.CONTVEAC_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTVEAC_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CONTVEAC_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CVCODART"+",CVCODCLI"+",CVTIPCON"+",CVCONRIC"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CVCODART),'CONTVEAC','CVCODART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CVCODCLI),'CONTVEAC','CVCODCLI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CVTIPCON),'CONTVEAC','CVTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CVCONRIC),'CONTVEAC','CVCONRIC');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CVCODART',this.w_CVCODART,'CVCODCLI',this.w_CVCODCLI,'CVTIPCON',this.w_CVTIPCON,'CVCONRIC',this.w_CVCONRIC)
      insert into (i_cTable) (CVCODART,CVCODCLI,CVTIPCON,CVCONRIC &i_ccchkf. );
         values (;
           this.w_CVCODART;
           ,this.w_CVCODCLI;
           ,this.w_CVTIPCON;
           ,this.w_CVCONRIC;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    this.w_TMPC = ah_Msgformat("Inserita contropartita parcellazione ITA-SPE")
    return
  proc Try_0360EA50()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into CAU_CONT
    i_nConn=i_TableProp[this.CAU_CONT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAU_CONT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CAU_CONT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CCCODICE"+",CCDESCRI"+",CCFLPART"+",CCFLANAL"+",CCFLSALI"+",CCFLSALF"+",CCFLCOMP"+",CCFLRIFE"+",CCTIPDOC"+",CCTIPREG"+",CCNUMREG"+",CCNUMDOC"+",CCFLIVDF"+",CCFLPDIF"+",CCFLSTDA"+",CCCALDOC"+",CCTESDOC"+",CCFLRITE"+",CCSERDOC"+",CCFLPDOC"+",CCSERPRO"+",CCFLPPRO"+",CCCFDAVE"+",UTCC"+",UTDC"+",CCFLBESE"+",CCFLMOVC"+",CCFLAUTR"+",CCFLINSO"+",CCMOVCES"+",CCDESSUP"+",CCDESRIG"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CCCODICE),'CAU_CONT','CCCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCDESCRI),'CAU_CONT','CCDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLPART),'CAU_CONT','CCFLPART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLANAL),'CAU_CONT','CCFLANAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLSALI),'CAU_CONT','CCFLSALI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLSALF),'CAU_CONT','CCFLSALF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLCOMP),'CAU_CONT','CCFLCOMP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLRIFE),'CAU_CONT','CCFLRIFE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCTIPDOC),'CAU_CONT','CCTIPDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCTIPREG),'CAU_CONT','CCTIPREG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCNUMREG),'CAU_CONT','CCNUMREG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCNUMDOC),'CAU_CONT','CCNUMDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLIVDF),'CAU_CONT','CCFLIVDF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLPDIF),'CAU_CONT','CCFLPDIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLSTDA),'CAU_CONT','CCFLSTDA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCCALDOC),'CAU_CONT','CCCALDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCTESDOC),'CAU_CONT','CCTESDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLRITE),'CAU_CONT','CCFLRITE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCSERDOC),'CAU_CONT','CCSERDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLPDOC),'CAU_CONT','CCFLPDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCSERPRO),'CAU_CONT','CCSERPRO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLPPRO),'CAU_CONT','CCFLPPRO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCCFDAVE),'CAU_CONT','CCCFDAVE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_UTCC),'CAU_CONT','UTCC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_UTDC),'CAU_CONT','UTDC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLBESE),'CAU_CONT','CCFLBESE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLMOVC),'CAU_CONT','CCFLMOVC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLAUTR),'CAU_CONT','CCFLAUTR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLINSO),'CAU_CONT','CCFLINSO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCMOVCES),'CAU_CONT','CCMOVCES');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCDESSUP),'CAU_CONT','CCDESSUP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCDESRIG),'CAU_CONT','CCDESRIG');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CCCODICE',this.w_CCCODICE,'CCDESCRI',this.w_CCDESCRI,'CCFLPART',this.w_CCFLPART,'CCFLANAL',this.w_CCFLANAL,'CCFLSALI',this.w_CCFLSALI,'CCFLSALF',this.w_CCFLSALF,'CCFLCOMP',this.w_CCFLCOMP,'CCFLRIFE',this.w_CCFLRIFE,'CCTIPDOC',this.w_CCTIPDOC,'CCTIPREG',this.w_CCTIPREG,'CCNUMREG',this.w_CCNUMREG,'CCNUMDOC',this.w_CCNUMDOC)
      insert into (i_cTable) (CCCODICE,CCDESCRI,CCFLPART,CCFLANAL,CCFLSALI,CCFLSALF,CCFLCOMP,CCFLRIFE,CCTIPDOC,CCTIPREG,CCNUMREG,CCNUMDOC,CCFLIVDF,CCFLPDIF,CCFLSTDA,CCCALDOC,CCTESDOC,CCFLRITE,CCSERDOC,CCFLPDOC,CCSERPRO,CCFLPPRO,CCCFDAVE,UTCC,UTDC,CCFLBESE,CCFLMOVC,CCFLAUTR,CCFLINSO,CCMOVCES,CCDESSUP,CCDESRIG &i_ccchkf. );
         values (;
           this.w_CCCODICE;
           ,this.w_CCDESCRI;
           ,this.w_CCFLPART;
           ,this.w_CCFLANAL;
           ,this.w_CCFLSALI;
           ,this.w_CCFLSALF;
           ,this.w_CCFLCOMP;
           ,this.w_CCFLRIFE;
           ,this.w_CCTIPDOC;
           ,this.w_CCTIPREG;
           ,this.w_CCNUMREG;
           ,this.w_CCNUMDOC;
           ,this.w_CCFLIVDF;
           ,this.w_CCFLPDIF;
           ,this.w_CCFLSTDA;
           ,this.w_CCCALDOC;
           ,this.w_CCTESDOC;
           ,this.w_CCFLRITE;
           ,this.w_CCSERDOC;
           ,this.w_CCFLPDOC;
           ,this.w_CCSERPRO;
           ,this.w_CCFLPPRO;
           ,this.w_CCCFDAVE;
           ,this.w_UTCC;
           ,this.w_UTDC;
           ,this.w_CCFLBESE;
           ,this.w_CCFLMOVC;
           ,this.w_CCFLAUTR;
           ,this.w_CCFLINSO;
           ,this.w_CCMOVCES;
           ,this.w_CCDESSUP;
           ,this.w_CCDESRIG;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    this.w_TMPC = ah_Msgformat("Inserita causale contabile XGIRR")
    return
  proc Try_035FAC60()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.w_CCCODICE = CRNUMCOL
    this.w_CCDESCRI = LEFT(ALLTRIM(CRDESCRI),40)
    this.w_CCCODCON = PC_CONTO
    * --- Insert into COLCRONO
    i_nConn=i_TableProp[this.COLCRONO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.COLCRONO_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.COLCRONO_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CCCODICE"+",CCDESCRI"+",CCTIPCON"+",CCCODCON"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CCCODICE),'COLCRONO','CCCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCDESCRI),'COLCRONO','CCDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ANTIPCON),'COLCRONO','CCTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCCODCON),'COLCRONO','CCCODCON');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CCCODICE',this.w_CCCODICE,'CCDESCRI',this.w_CCDESCRI,'CCTIPCON',this.w_ANTIPCON,'CCCODCON',this.w_CCCODCON)
      insert into (i_cTable) (CCCODICE,CCDESCRI,CCTIPCON,CCCODCON &i_ccchkf. );
         values (;
           this.w_CCCODICE;
           ,this.w_CCDESCRI;
           ,this.w_ANTIPCON;
           ,this.w_CCCODCON;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    this.w_TMPC = ah_Msgformat("Inserita colonna cronologico %1 %2", this.w_CCCODICE, this.w_CCCODCON)
    this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,10)]
    this.cWorkTables[1]='CACOARTI'
    this.cWorkTables[2]='ART_ICOL'
    this.cWorkTables[3]='CONTVEAC'
    this.cWorkTables[4]='CONTI'
    this.cWorkTables[5]='TAB_RITE'
    this.cWorkTables[6]='CONTROPA'
    this.cWorkTables[7]='CAU_CONT'
    this.cWorkTables[8]='COLCRONO'
    this.cWorkTables[9]='DOC_DETT'
    this.cWorkTables[10]='CAUMATTI'
    return(this.OpenAllTables(10))

  proc CloseCursors()
    if used('_Curs_ART_ICOL')
      use in _Curs_ART_ICOL
    endif
    if used('_Curs_ART_ICOL')
      use in _Curs_ART_ICOL
    endif
    if used('_Curs_ART_ICOL')
      use in _Curs_ART_ICOL
    endif
    if used('_Curs_DOC_DETT')
      use in _Curs_DOC_DETT
    endif
    if used('_Curs_CONTI')
      use in _Curs_CONTI
    endif
    if used('_Curs_TAB_RITE')
      use in _Curs_TAB_RITE
    endif
    if used('_Curs_TAB_RITE')
      use in _Curs_TAB_RITE
    endif
    if used('_Curs_CAUMATTI')
      use in _Curs_CAUMATTI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
