* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcb30                                                        *
*              Ricostruzione integrit� referenziali OFF_ATTI e ALL_EGAT        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_11]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-05-18                                                      *
* Last revis.: 2015-03-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcb30",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcb30 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TMPC = space(10)
  w_TEMP = space(10)
  w_CODREL = space(10)
  w_NCONN = 0
  w_NCONN2 = 0
  w_iRows = 0
  w_iRows2 = 0
  w_PK = .f.
  * --- WorkFile variables
  OFF_ATTI_idx=0
  ALL_EGAT_idx=0
  CONVERSI_idx=0
  runtime_filters = 2

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- I Campi ATOPPOFF e  ATCODNOM della tabella OFF_ATTI non possono contenere valori BLANK poich� su tali campi sono definiti dei vincoli di integrit�. 
    *     La procedura sostituisce il contenuto di tali campi con NULL nel caso in cui contengano na stringa vuota (blanck, ossia spazi)
    *     La procedura ridimensiona anche  i campi xxxOFF_ATTI->ATCODNOM e xxxALL_EGAT->ALRIFNOM che a causa di un errore della procedura gscvbb90 (la prima versione, prima della patch 35)  non venivano ridimensionati
    *     L errore sulle tabelle xxxOFF_ATTI e xxxALL_EGAT trovandosi su tabelle che non appartengono a nessuna azienda non procura alcun problema funzionale ma la correzzione serve solo per non vedere gli errrori nel log della ricostruzione DB
    *     L errore sulle tabelle XXX si trova solo per SQLServer 
    * --- FIle di LOG
    * --- Read from CONVERSI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CONVERSI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONVERSI_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "COCODREL"+;
        " from "+i_cTable+" CONVERSI where ";
            +"CONOMPRO = "+cp_ToStrODBC("GSCVCB30");
            +" and COESEGUI = "+cp_ToStrODBC("S");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        COCODREL;
        from (i_cTable) where;
            CONOMPRO = "GSCVCB30";
            and COESEGUI = "S";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CODREL = NVL(cp_ToDate(_read_.COCODREL),cp_NullValue(_read_.COCODREL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if EMPTY(this.w_CODREL)
      * --- Try
      local bErr_02E28EA8
      bErr_02E28EA8=bTrsErr
      this.Try_02E28EA8()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- Gestisce log errori
        this.oParentObject.w_PMSG = Message()
        this.oParentObject.w_PESEOK = .F.
        if this.w_NHF>=0
          this.w_TMPC = AH_MsgFormat("ERRORE GENERICO - %1", this.oParentObject.w_PMSG)
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
      bTrsErr=bTrsErr or bErr_02E28EA8
      * --- End
    else
      if this.w_NHF>=0
        this.w_TMPC = AH_MsgFormat("Aggiornamento terminato con successo")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
      this.oParentObject.w_PESEOK = .T.
    endif
  endproc
  proc Try_02E28EA8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Le operazioni di ricostruzioni del database non sono supportate
    *     da tutti i database in modo transazionale (DB2 e Oracle) per cui le
    *     escludo dalla transazione
    * --- Ridimensiona i campi OFF_ATTI.ATCODNOM e ALL_EGAT.ALRIFNOM
    this.w_NCONN = i_TableProp[this.OFF_ATTI_idx,3] 
    this.w_NCONN2 = i_TableProp[this.ALL_EGAT_idx,3] 
    this.w_PK = .t.
    GSCV_BIN(this, "xxx", "OFF_ATTI" , this.w_NHF, IIF( this.w_PK , "S" ,"N") )
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_PK = upper(CP_DBTYPE)="ORACLE"
    GSCV_BIN(this, "xxx", "ALL_EGAT" , this.w_NHF, IIF( this.w_PK , "S" ,"N") )
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    do case
      case upper(CP_DBTYPE)="SQLSERVER"
        this.w_iRows = cp_sqlexec( this.w_NCONN , "ALTER TABLE xxxOFF_ATTI ALTER COLUMN ATCODNOM char(15)")
        this.w_iRows2 = cp_sqlexec( this.w_NCONN2 , "ALTER TABLE xxxALL_EGAT ALTER COLUMN ALRIFNOM char(15) ")
      case upper(CP_DBTYPE)="ORACLE"
        this.w_iRows = cp_sqlexec( this.w_NCONN , "ALTER TABLE xxxOFF_ATTI MODIFY (ATCODNOM char(15))")
        this.w_iRows2 = cp_sqlexec( this.w_NCONN2 , "ALTER TABLE xxxALL_EGAT MODIFY (ALRIFNOM char(15)) ")
      case upper(CP_DBTYPE)="DB2"
        this.w_iRows = cp_sqlexec( this.w_NCONN , "DROP  TABLE xxxOFF_ATTI")
        this.w_iRows2 = cp_sqlexec( this.w_NCONN2 , "DROP TABLE xxxALL_EGAT")
    endcase
    if this.w_iRows<0 Or this.w_iRows2<0 
      * --- Raise
      i_Error=Message()
      return
    endif
    * --- Sostituisce i valori BLANK con NULL per il campo OFF_ATTI->ATOPPOFF
    * --- Write into OFF_ATTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.OFF_ATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="ATSERIAL"
      do vq_exec with '..\pcon\exe\query\GSCVCB30a',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_ATTI_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="OFF_ATTI.ATSERIAL = _t2.ATSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"ATOPPOFF ="+cp_NullLink(cp_ToStrODBC(NULL),'OFF_ATTI','ATOPPOFF');
          +i_ccchkf;
          +" from "+i_cTable+" OFF_ATTI, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="OFF_ATTI.ATSERIAL = _t2.ATSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OFF_ATTI, "+i_cQueryTable+" _t2 set ";
      +"OFF_ATTI.ATOPPOFF ="+cp_NullLink(cp_ToStrODBC(NULL),'OFF_ATTI','ATOPPOFF');
          +Iif(Empty(i_ccchkf),"",",OFF_ATTI.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="OFF_ATTI.ATSERIAL = t2.ATSERIAL";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OFF_ATTI set (";
          +"ATOPPOFF";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +cp_NullLink(cp_ToStrODBC(NULL),'OFF_ATTI','ATOPPOFF')+"";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="OFF_ATTI.ATSERIAL = _t2.ATSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OFF_ATTI set ";
      +"ATOPPOFF ="+cp_NullLink(cp_ToStrODBC(NULL),'OFF_ATTI','ATOPPOFF');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".ATSERIAL = "+i_cQueryTable+".ATSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"ATOPPOFF ="+cp_NullLink(cp_ToStrODBC(NULL),'OFF_ATTI','ATOPPOFF');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Sostituisce i valori BLANK con NULL per il campo OFF_ATTI->ATCODNOM
    * --- Write into OFF_ATTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.OFF_ATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="ATSERIAL"
      do vq_exec with '..\pcon\exe\query\GSCVCB30B',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_ATTI_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="OFF_ATTI.ATSERIAL = _t2.ATSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"ATCODNOM ="+cp_NullLink(cp_ToStrODBC(NULL),'OFF_ATTI','ATCODNOM');
          +i_ccchkf;
          +" from "+i_cTable+" OFF_ATTI, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="OFF_ATTI.ATSERIAL = _t2.ATSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OFF_ATTI, "+i_cQueryTable+" _t2 set ";
      +"OFF_ATTI.ATCODNOM ="+cp_NullLink(cp_ToStrODBC(NULL),'OFF_ATTI','ATCODNOM');
          +Iif(Empty(i_ccchkf),"",",OFF_ATTI.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="OFF_ATTI.ATSERIAL = t2.ATSERIAL";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OFF_ATTI set (";
          +"ATCODNOM";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +cp_NullLink(cp_ToStrODBC(NULL),'OFF_ATTI','ATCODNOM')+"";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="OFF_ATTI.ATSERIAL = _t2.ATSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OFF_ATTI set ";
      +"ATCODNOM ="+cp_NullLink(cp_ToStrODBC(NULL),'OFF_ATTI','ATCODNOM');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".ATSERIAL = "+i_cQueryTable+".ATSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"ATCODNOM ="+cp_NullLink(cp_ToStrODBC(NULL),'OFF_ATTI','ATCODNOM');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Per ogni archivio occorre determinare l'elenco degli archivi che
    *     da esso dipendono, e per ognuno occorre ricostruire il database...
    * --- Elenco archivi:
    *     ALL_EGAT
    *     OFF_ATTI
    GSCV_BRT(this, "OFF_ATTI" , this.w_NCONN , .T. , "all")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    GSCV_BRT(this, "ALL_EGAT" , this.w_NCONN2 , .T. , "all")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if this.w_NHF>=0
      this.w_TMPC = AH_MsgFormat("Aggiornamento terminato con successo")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = AH_MsgFormat("Conversione eseguita correttamente")
    return

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='OFF_ATTI'
    this.cWorkTables[2]='ALL_EGAT'
    this.cWorkTables[3]='CONVERSI'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
