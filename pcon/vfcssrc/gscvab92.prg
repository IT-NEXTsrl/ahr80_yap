* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvab92                                                        *
*              Aggiorna struttura dati cespiti                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_29]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-05-12                                                      *
* Last revis.: 2005-05-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvab92",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvab92 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_NUMAGG = 0
  w_NUMAGG1 = 0
  w_NUMAGG2 = 0
  w_MCSERIAL = space(10)
  w_MCIMPA02 = 0
  w_MCIMPA03 = 0
  w_MCIMPA04 = 0
  w_MCIMPA05 = 0
  w_MCIMPA06 = 0
  w_MCIMPA13 = 0
  w_MCIMPA14 = 0
  w_MCIMPA17 = 0
  w_MCDTPRIU = ctod("  /  /  ")
  w_CEESPRIU = space(4)
  w_MCFLPRIU = space(1)
  w_TIPO = space(1)
  w_SCCODCES = space(20)
  w_SCCODESE = space(4)
  w_COCODICE = space(20)
  w_COCOMPON = space(40)
  w_COIMPTOC = 0
  w_MCIMPVAC = 0
  w_ORDINA = space(1)
  w_ACNUMREG = space(6)
  w_ACCODESE = space(4)
  w_ACCODCES = space(20)
  w_ACIMPV01 = 0
  w_oMess = .NULL.
  w_oPart = .NULL.
  * --- WorkFile variables
  MOV_CESP_idx=0
  SAL_CESP_idx=0
  CES_PITI_idx=0
  COM_PCES_idx=0
  MOV_PCES_idx=0
  CES_AMMO_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questa procedura di conversione aggiorna la tabella Movimenti Cespiti e
    *     la tabella Saldi Cespiti.
    *     Vengono aggiornati i nuovi campi inseriti in analisi.
    * --- FIle di LOG
    if g_CESP = "S"
      * --- Try
      local bErr_03775BB0
      bErr_03775BB0=bTrsErr
      this.Try_03775BB0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        * --- Gestisce log errori
        this.oParentObject.w_PMSG = Message()
        this.oParentObject.w_PESEOK = .F.
        if this.w_NHF>=0
          this.w_TMPC = ah_Msgformat("ERRORE - impossibile eseguire aggiornamento richiesto")
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
      bTrsErr=bTrsErr or bErr_03775BB0
      * --- End
      if used("AGGCESP")
        select AGGCESP
        use
      endif
      if used("AGGCOMP")
        select AGGCOMP
        use
      endif
      if used("AGGAMM")
        select AGGAMM
        use
      endif
    else
      this.w_TMPC = Ah_Msgformat("Cespiti non presenti")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
  endproc
  proc Try_03775BB0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    VQ_EXEC("..\PCON\EXE\QUERY\GSCV_BCP.VQR",this,"AGGCESP")
    if RECCOUNT("AGGCESP")>0
      this.w_NUMAGG = 0
      this.w_NUMAGG1 = 0
      this.w_NUMAGG2 = 0
      Select AGGCESP 
 scan
      this.w_MCSERIAL = AGGCESP.MCSERIAL
      this.w_MCIMPA17 = NVL(AGGCESP.MCIMPA17,0)
      this.w_MCIMPA02 = NVL(AGGCESP.MCIMPA02,0)
      this.w_MCIMPA03 = NVL(AGGCESP.MCIMPA03,0)
      this.w_MCIMPA04 = NVL(AGGCESP.MCIMPA04,0)
      this.w_MCIMPA05 = NVL(AGGCESP.MCIMPA05,0)
      this.w_MCIMPA06 = NVL(AGGCESP.MCIMPA06,0)
      this.w_MCIMPA13 = NVL(AGGCESP.MCIMPA13,0)
      this.w_MCIMPA14 = NVL(AGGCESP.MCIMPA14,0)
      this.w_MCDTPRIU = CP_TODATE(AGGCESP.MCDTPRIU)
      this.w_CEESPRIU = NVL(AGGCESP.CEESPRIU,"    ")
      this.w_MCFLPRIU = NVL(AGGCESP.MCFLPRIU," ")
      this.w_TIPO = NVL(AGGCESP.TIPO,"M")
      this.w_SCCODCES = NVL(AGGCESP.SCCODCES,space(20))
      this.w_SCCODESE = NVL(AGGCESP.SCCODESE,space(4))
      ah_Msg("Aggiornamento cespite: %1",.T.,.F.,.F., Alltrim(this.w_SCCODCES) )
      * --- Verifico se il record deriva dai movimenti cespiti oppure dai saldi
      do case
        case this.w_TIPO="M"
          * --- Write into MOV_CESP
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.MOV_CESP_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MOV_CESP_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.MOV_CESP_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MCIMPA18 ="+cp_NullLink(cp_ToStrODBC(this.w_MCIMPA04),'MOV_CESP','MCIMPA18');
            +",MCIMPA22 ="+cp_NullLink(cp_ToStrODBC(this.w_MCIMPA05),'MOV_CESP','MCIMPA22');
            +",MCIMPA19 ="+cp_NullLink(cp_ToStrODBC(this.w_MCIMPA06),'MOV_CESP','MCIMPA19');
            +",MCIMPA20 ="+cp_NullLink(cp_ToStrODBC(this.w_MCIMPA13),'MOV_CESP','MCIMPA20');
            +",MCIMPA21 ="+cp_NullLink(cp_ToStrODBC(this.w_MCIMPA14),'MOV_CESP','MCIMPA21');
            +",MCDTPRIC ="+cp_NullLink(cp_ToStrODBC(this.w_MCDTPRIU),'MOV_CESP','MCDTPRIC');
            +",MCFLPRIC ="+cp_NullLink(cp_ToStrODBC(this.w_MCFLPRIU),'MOV_CESP','MCFLPRIC');
            +",MCIMPA23 ="+cp_NullLink(cp_ToStrODBC(this.w_MCIMPA02),'MOV_CESP','MCIMPA23');
            +",MCIMPA24 ="+cp_NullLink(cp_ToStrODBC(this.w_MCIMPA03),'MOV_CESP','MCIMPA24');
            +",MCIMPA04 ="+cp_NullLink(cp_ToStrODBC(this.w_MCIMPA17),'MOV_CESP','MCIMPA04');
                +i_ccchkf ;
            +" where ";
                +"MCSERIAL = "+cp_ToStrODBC(this.w_MCSERIAL);
                   )
          else
            update (i_cTable) set;
                MCIMPA18 = this.w_MCIMPA04;
                ,MCIMPA22 = this.w_MCIMPA05;
                ,MCIMPA19 = this.w_MCIMPA06;
                ,MCIMPA20 = this.w_MCIMPA13;
                ,MCIMPA21 = this.w_MCIMPA14;
                ,MCDTPRIC = this.w_MCDTPRIU;
                ,MCFLPRIC = this.w_MCFLPRIU;
                ,MCIMPA23 = this.w_MCIMPA02;
                ,MCIMPA24 = this.w_MCIMPA03;
                ,MCIMPA04 = this.w_MCIMPA17;
                &i_ccchkf. ;
             where;
                MCSERIAL = this.w_MCSERIAL;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          this.w_NUMAGG = this.w_NUMAGG+1
        case this.w_TIPO="S"
          * --- Write into SAL_CESP
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.SAL_CESP_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SAL_CESP_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.SAL_CESP_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SCIMPRIC ="+cp_NullLink(cp_ToStrODBC(this.w_MCIMPA04),'SAL_CESP','SCIMPRIC');
            +",SCDECVAC ="+cp_NullLink(cp_ToStrODBC(this.w_MCIMPA05),'SAL_CESP','SCDECVAC');
            +",SCIMPSVC ="+cp_NullLink(cp_ToStrODBC(this.w_MCIMPA06),'SAL_CESP','SCIMPSVC');
            +",SCPLUSVC ="+cp_NullLink(cp_ToStrODBC(this.w_MCIMPA13),'SAL_CESP','SCPLUSVC');
            +",SCMINSVC ="+cp_NullLink(cp_ToStrODBC(this.w_MCIMPA14),'SAL_CESP','SCMINSVC');
            +",SCINCVAC ="+cp_NullLink(cp_ToStrODBC(this.w_MCIMPA02),'SAL_CESP','SCINCVAC');
            +",SCIMPONC  ="+cp_NullLink(cp_ToStrODBC(this.w_MCIMPA03),'SAL_CESP','SCIMPONC ');
            +",SCIMPRIV ="+cp_NullLink(cp_ToStrODBC(this.w_MCIMPA17),'SAL_CESP','SCIMPRIV');
                +i_ccchkf ;
            +" where ";
                +"SCCODCES = "+cp_ToStrODBC(this.w_SCCODCES);
                +" and SCCODESE = "+cp_ToStrODBC(this.w_SCCODESE);
                   )
          else
            update (i_cTable) set;
                SCIMPRIC = this.w_MCIMPA04;
                ,SCDECVAC = this.w_MCIMPA05;
                ,SCIMPSVC = this.w_MCIMPA06;
                ,SCPLUSVC = this.w_MCIMPA13;
                ,SCMINSVC = this.w_MCIMPA14;
                ,SCINCVAC = this.w_MCIMPA02;
                ,SCIMPONC  = this.w_MCIMPA03;
                ,SCIMPRIV = this.w_MCIMPA17;
                &i_ccchkf. ;
             where;
                SCCODCES = this.w_SCCODCES;
                and SCCODESE = this.w_SCCODESE;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          this.w_NUMAGG1 = this.w_NUMAGG1+1
        case this.w_TIPO="C"
          * --- Write into CES_PITI
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.CES_PITI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CES_PITI_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.CES_PITI_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CEDTPRIC ="+cp_NullLink(cp_ToStrODBC(this.w_MCDTPRIU),'CES_PITI','CEDTPRIC');
            +",CEESPRIC ="+cp_NullLink(cp_ToStrODBC(this.w_CEESPRIU),'CES_PITI','CEESPRIC');
                +i_ccchkf ;
            +" where ";
                +"CECODICE = "+cp_ToStrODBC(this.w_SCCODCES);
                   )
          else
            update (i_cTable) set;
                CEDTPRIC = this.w_MCDTPRIU;
                ,CEESPRIC = this.w_CEESPRIU;
                &i_ccchkf. ;
             where;
                CECODICE = this.w_SCCODCES;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          this.w_NUMAGG2 = this.w_NUMAGG2+1
      endcase
      Endscan
    endif
    VQ_EXEC("..\PCON\EXE\QUERY\GSCV3BCP.VQR",this,"AGGCOMP")
    if RECCOUNT("AGGCOMP")>0
      Select AGGCOMP 
 scan
      this.w_MCSERIAL = NVL(AGGCOMP.MCSERIAL,SPACE(10))
      this.w_COCODICE = NVL(AGGCOMP.COCODICE,SPACE(20))
      this.w_COCOMPON = NVL(AGGCOMP.COCOMPON,SPACE(40))
      this.w_COIMPTOC = NVL(AGGCOMP.COIMPTOC,0)
      this.w_MCIMPVAC = NVL(AGGCOMP.MCIMPVAC,0)
      this.w_ORDINA = NVL(AGGCOMP.ORDINA,"A")
      ah_Msg("Aggiornamento componente: %1",.T.,.F.,.F., Alltrim(this.w_COCOMPON) )
      do case
        case this.w_ORDINA="A"
          * --- Write into COM_PCES
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.COM_PCES_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.COM_PCES_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.COM_PCES_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"COIMPTOC ="+cp_NullLink(cp_ToStrODBC(this.w_COIMPTOC),'COM_PCES','COIMPTOC');
                +i_ccchkf ;
            +" where ";
                +"COCODICE = "+cp_ToStrODBC(this.w_COCODICE);
                +" and COCOMPON = "+cp_ToStrODBC(this.w_COCOMPON);
                   )
          else
            update (i_cTable) set;
                COIMPTOC = this.w_COIMPTOC;
                &i_ccchkf. ;
             where;
                COCODICE = this.w_COCODICE;
                and COCOMPON = this.w_COCOMPON;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        case this.w_ORDINA="B"
          * --- Write into MOV_PCES
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.MOV_PCES_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MOV_PCES_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.MOV_PCES_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MCIMPTOC ="+cp_NullLink(cp_ToStrODBC(this.w_COIMPTOC),'MOV_PCES','MCIMPTOC');
            +",MCIMPVAC ="+cp_NullLink(cp_ToStrODBC(this.w_MCIMPVAC),'MOV_PCES','MCIMPVAC');
                +i_ccchkf ;
            +" where ";
                +"MCSERIAL = "+cp_ToStrODBC(this.w_MCSERIAL);
                +" and MCCOMPON = "+cp_ToStrODBC(this.w_COCOMPON);
                   )
          else
            update (i_cTable) set;
                MCIMPTOC = this.w_COIMPTOC;
                ,MCIMPVAC = this.w_MCIMPVAC;
                &i_ccchkf. ;
             where;
                MCSERIAL = this.w_MCSERIAL;
                and MCCOMPON = this.w_COCOMPON;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
      endcase
      Endscan
    endif
    VQ_EXEC("..\PCON\EXE\QUERY\GSCV5BCP.VQR",this,"AGGAMM")
    if RECCOUNT("AGGAMM")>0
      Select AGGAMM 
 scan
      this.w_ACNUMREG = AGGAMM.ACNUMREG
      this.w_ACCODESE = AGGAMM.ACCODESE
      this.w_ACCODCES = AGGAMM.ACCODCES
      this.w_ACIMPV01 = NVL(AGGAMM.ACIMPV01,0)
      * --- Write into CES_AMMO
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.CES_AMMO_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CES_AMMO_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.CES_AMMO_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"ACIMPV16 ="+cp_NullLink(cp_ToStrODBC(this.w_ACIMPV01),'CES_AMMO','ACIMPV16');
            +i_ccchkf ;
        +" where ";
            +"ACNUMREG = "+cp_ToStrODBC(this.w_ACNUMREG);
            +" and ACCODESE = "+cp_ToStrODBC(this.w_ACCODESE);
            +" and ACCODCES = "+cp_ToStrODBC(this.w_ACCODCES);
               )
      else
        update (i_cTable) set;
            ACIMPV16 = this.w_ACIMPV01;
            &i_ccchkf. ;
         where;
            ACNUMREG = this.w_ACNUMREG;
            and ACCODESE = this.w_ACCODESE;
            and ACCODCES = this.w_ACCODCES;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      Endscan
    endif
    if this.w_NHF>=0
      * --- Oggetto per messaggi incrementali
      this.w_oMess=createobject("Ah_Message")
      this.w_oPart = this.w_oMess.AddMsgPartNL("- Aggiornamento tabella MOV_CESP eseguito con successo%0Variati %1 records")
      this.w_oPart.AddParam(ALLTRIM(STR(this.w_NUMAGG)))     
      this.w_oPart = this.w_oMess.AddMsgPartNL("- Aggiornamento tabella SAL_CESP eseguito con successo%0Variati %1 records")
      this.w_oPart.AddParam(ALLTRIM(STR(this.w_NUMAGG1)))     
      this.w_oPart = this.w_oMess.AddMsgPartNL("- Aggiornamento tabella CES_PITI eseguito con successo%0Variati %1 records")
      this.w_oPart.AddParam(ALLTRIM(STR(this.w_NUMAGG2)))     
      this.w_TMPC = this.w_oMess.ComposeMessage()
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = this.w_TMPC
    * --- commit
    cp_EndTrs(.t.)
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,6)]
    this.cWorkTables[1]='MOV_CESP'
    this.cWorkTables[2]='SAL_CESP'
    this.cWorkTables[3]='CES_PITI'
    this.cWorkTables[4]='COM_PCES'
    this.cWorkTables[5]='MOV_PCES'
    this.cWorkTables[6]='CES_AMMO'
    return(this.OpenAllTables(6))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
