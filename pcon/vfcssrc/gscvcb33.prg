* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcb33                                                        *
*              Aggiornamento campo LPTELFAX tabella PRO_LOGP                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_11]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-05-18                                                      *
* Last revis.: 2010-06-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcb33",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcb33 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_MIRROR = space(50)
  w_CMDSQL = space(254)
  w_RESULT = 0
  w_CONN = 0
  w_DBTYPE = space(50)
  w_TMPC = space(10)
  w_TEMP = space(10)
  w_ERRORFLAG = .f.
  * --- WorkFile variables
  SUENMAST_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiornamento campo LPTELFAX tabella PRO_LOGP
    * --- FIle di LOG
    this.w_ERRORFLAG = .F.
    * --- Try
    local bErr_037A0A70
    bErr_037A0A70=bTrsErr
    this.Try_037A0A70()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = "Occorre verificare l' eventuale generazione delle tabelle di mirror."
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = AH_MsgFormat("ERRORE GENERICO - %1", this.oParentObject.w_PMSG)
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_037A0A70
    * --- End
  endproc
  proc Try_037A0A70()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Cicla su tutte le super entit� per recupere il nome del mirror e aggiungere il campo DELETE_PU alla tabella di mirror
    * --- Nella logistica remota si presume che le tabelle della logistica remota siano tutte sullo stesso DB
    this.w_CONN = i_TableProp[ cp_OpenTable( "SUENMAST" ,.T.) , 3 ] 
    this.w_DBTYPE = cp_GetDatabaseType( this.w_CONN )
    * --- begin transaction
    cp_BeginTrs()
    * --- Select from SUENMAST
    i_nConn=i_TableProp[this.SUENMAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SUENMAST_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select SUCODICE  from "+i_cTable+" SUENMAST ";
           ,"_Curs_SUENMAST")
    else
      select SUCODICE from (i_cTable);
        into cursor _Curs_SUENMAST
    endif
    if used('_Curs_SUENMAST')
      select _Curs_SUENMAST
      locate for 1=1
      do while not(eof())
      this.w_MIRROR = Alltrim(GETMIRRORNAME( _Curs_SUENMAST.SUCODICE))
      this.w_CMDSQL = "ALTER TABLE "+this.w_MIRROR+" ADD DELETE_PU "+ db_FieldType( this.w_DBTYPE , "N", 1, 0)
      this.w_RESULT = cp_SQLexec ( this.w_CONN , this.w_CMDSQL ) 
      if this.w_RESULT<0
        * --- Se il campo esisteva allora si accetta l errore
        this.w_CMDSQL = "SELECT  DELETE_PU FROM "+this.w_MIRROR
        this.w_RESULT = cp_SQLexec ( this.w_CONN , this.w_CMDSQL ) 
        if this.w_RESULT<0
          * --- C' � stato un errore e il campo non � stto aggiunto
          this.w_ERRORFLAG = .T.
          if this.w_NHF>=0
            this.w_TMPC = AH_MSGFORMAT ("Errore in aggiornamento tabella di mirror %1. Verificare che le tabelle di mirror siano state generate.",this.w_MIRROR)
            this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
          endif
          * --- Raise
          i_Error="errore in aggiornamento tabella di mirror"
          return
        endif
        * --- accept error
        bTrsErr=.f.
      else
        * --- Se si � aggiunto il campo alla tabella di mirror allroa bisogna anche inizializzarlo a 0
        this.w_CMDSQL = "UPDATE  "+this.w_MIRROR+" SET DELETE_PU=0"
        this.w_RESULT = cp_SQLexec ( this.w_CONN , this.w_CMDSQL ) 
      endif
        select _Curs_SUENMAST
        continue
      enddo
      use
    endif
    if bTrsErr OR this.w_ERRORFLAG
      this.w_TMPC = AH_MsgFormat("Impossibile modificare la tabella di mirror")
      * --- Raise
      i_Error=this.w_TMPC
      return
    else
      if this.w_NHF>=0
        this.w_TMPC = AH_MsgFormat("Aggiornamento tabelle di mirror completato con successo")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = AH_MsgFormat("Conversione eseguita correttamente")
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='SUENMAST'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_SUENMAST')
      use in _Curs_SUENMAST
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
