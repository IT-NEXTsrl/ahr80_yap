* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvab54                                                        *
*              Apertura movimenti Fidelity                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_15]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-12-06                                                      *
* Last revis.: 2004-12-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvab54",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvab54 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_MESS = space(100)
  w_CODCAU = space(5)
  w_DESCAU = space(40)
  w_TIPMOV = space(1)
  w_PUNPRE = space(1)
  w_CODFID = space(20)
  w_IMPPRE = 0
  w_MFSERIAL = space(10)
  w_DATSYS = ctod("  /  /  ")
  w_STATO = space(1)
  * --- WorkFile variables
  CAU_FIDE_idx=0
  MOV_FIDE_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questa procedura Aggiorna il campo Data di Registrazione nelle Partite.
    * --- FIle di LOG
    * --- Try
    local bErr_037239F0
    bErr_037239F0=bTrsErr
    this.Try_037239F0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_STATO $"A-B"
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, i_ErrMsg)
      else
        if this.w_NHF>=0
          this.w_TMPC = ah_Msgformat("%1 ERRORE GENERICO - impossibile aggiornare i movimenti Fidelity", this.oParentObject.w_PMSG )
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
    endif
    bTrsErr=bTrsErr or bErr_037239F0
    * --- End
  endproc
  proc Try_037239F0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.w_STATO = " "
    this.w_DATSYS = i_DATSYS
    * --- begin transaction
    cp_BeginTrs()
    * --- Read from MOV_FIDE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.MOV_FIDE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOV_FIDE_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "*"+;
        " from "+i_cTable+" MOV_FIDE where ";
            +"1 = "+cp_ToStrODBC(1);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        *;
        from (i_cTable) where;
            1 = 1;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if i_Rows > 0
      this.w_STATO = "A"
      this.w_TMPC = ah_Msgformat("Sono gi� presenti movimenti Fidelity. Eliminare tutti i movimenti e rieseguire la conversione")
      * --- Raise
      i_Error=this.w_TMPC
      return
    endif
    this.w_CODCAU = "APE+"
    this.w_DESCAU = ah_Msgformat("Movimento di apertura di carico")
    this.w_TIPMOV = "+"
    this.w_PUNPRE = "I"
    * --- Inserimento Causali
    * --- Insert into CAU_FIDE
    i_nConn=i_TableProp[this.CAU_FIDE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAU_FIDE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CAU_FIDE_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CFCODICE"+",CFDESCRI"+",CFTIPMOV"+",CFPUNPRE"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CODCAU),'CAU_FIDE','CFCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DESCAU),'CAU_FIDE','CFDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TIPMOV),'CAU_FIDE','CFTIPMOV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PUNPRE),'CAU_FIDE','CFPUNPRE');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CFCODICE',this.w_CODCAU,'CFDESCRI',this.w_DESCAU,'CFTIPMOV',this.w_TIPMOV,'CFPUNPRE',this.w_PUNPRE)
      insert into (i_cTable) (CFCODICE,CFDESCRI,CFTIPMOV,CFPUNPRE &i_ccchkf. );
         values (;
           this.w_CODCAU;
           ,this.w_DESCAU;
           ,this.w_TIPMOV;
           ,this.w_PUNPRE;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    this.w_DESCAU = this.w_DESCAU + ah_Msgformat("%1Questo movimento non deve essere eliminato")
    * --- Select from GSCVAB54_1
    do vq_exec with 'GSCVAB54_1',this,'_Curs_GSCVAB54_1','',.f.,.t.
    if used('_Curs_GSCVAB54_1')
      select _Curs_GSCVAB54_1
      locate for 1=1
      do while not(eof())
      this.w_MFSERIAL = Space(10)
      this.w_CODFID = Nvl(_Curs_GSCVAB54_1.MDCODFID,Space(20) )
      this.w_IMPPRE = Nvl(_Curs_GSCVAB54_1.MDIMPPRE,0)
      i_Conn=i_TableProp[this.MOV_FIDE_IDX, 3]
      cp_NextTableProg(this, i_Conn, "SERMOVFID", "i_codazi,w_MFSERIAL")
      * --- Insert into MOV_FIDE
      i_nConn=i_TableProp[this.MOV_FIDE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MOV_FIDE_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MOV_FIDE_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"MFSERIAL"+",MFCAUFID"+",MFCODFID"+",MF_PREPA"+",MFDATMOV"+",MFDESMOV"+",MFFLRESI"+",MFFLPUNT"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_MFSERIAL),'MOV_FIDE','MFSERIAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CODCAU),'MOV_FIDE','MFCAUFID');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CODFID),'MOV_FIDE','MFCODFID');
        +","+cp_NullLink(cp_ToStrODBC(this.w_IMPPRE),'MOV_FIDE','MF_PREPA');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DATSYS),'MOV_FIDE','MFDATMOV');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DESCAU),'MOV_FIDE','MFDESMOV');
        +","+cp_NullLink(cp_ToStrODBC(this.w_TIPMOV),'MOV_FIDE','MFFLRESI');
        +","+cp_NullLink(cp_ToStrODBC(" "),'MOV_FIDE','MFFLPUNT');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'MFSERIAL',this.w_MFSERIAL,'MFCAUFID',this.w_CODCAU,'MFCODFID',this.w_CODFID,'MF_PREPA',this.w_IMPPRE,'MFDATMOV',this.w_DATSYS,'MFDESMOV',this.w_DESCAU,'MFFLRESI',this.w_TIPMOV,'MFFLPUNT'," ")
        insert into (i_cTable) (MFSERIAL,MFCAUFID,MFCODFID,MF_PREPA,MFDATMOV,MFDESMOV,MFFLRESI,MFFLPUNT &i_ccchkf. );
           values (;
             this.w_MFSERIAL;
             ,this.w_CODCAU;
             ,this.w_CODFID;
             ,this.w_IMPPRE;
             ,this.w_DATSYS;
             ,this.w_DESCAU;
             ,this.w_TIPMOV;
             ," ";
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
        select _Curs_GSCVAB54_1
        continue
      enddo
      use
    endif
    if bTrsErr
      this.w_STATO = "B"
      * --- Raise
      i_Error=MESSAGE()
      return
    else
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("Elaborazione terminata con successo")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='CAU_FIDE'
    this.cWorkTables[2]='MOV_FIDE'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_GSCVAB54_1')
      use in _Curs_GSCVAB54_1
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
