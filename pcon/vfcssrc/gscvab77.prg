* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvab77                                                        *
*              Conversione gest. autorizzazioni InfoPublisher                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_4]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-04-08                                                      *
* Last revis.: 2005-04-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvab77",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvab77 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_QACODQUE = space(20)
  w_QAUTEGRP = 0
  w_QAROWPRG = 0
  w_QAFLUTGR = space(1)
  * --- WorkFile variables
  INF_AUQU_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte la vecchia gestione autorizzazioni InfoPublisher nella nuova gestione
    * --- Try
    local bErr_0361E390
    bErr_0361E390=bTrsErr
    this.Try_0361E390()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE GENERICO - impossibile eseguire aggiornamento richiesto")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_0361E390
    * --- End
  endproc
  proc Try_0361E390()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Seleziono tutte le autorizzazioni
    * --- Select from GSCVAB77
    do vq_exec with 'GSCVAB77',this,'_Curs_GSCVAB77','',.f.,.t.
    if used('_Curs_GSCVAB77')
      select _Curs_GSCVAB77
      locate for 1=1
      do while not(eof())
      this.w_QACODQUE = QACODQUE
      this.w_QAROWPRG = QAROWPRG
      this.w_QAFLUTGR = QAFLUTGR
      this.w_QAUTEGRP = QAUTEGRP
      * --- Try
      local bErr_037BDDC0
      bErr_037BDDC0=bTrsErr
      this.Try_037BDDC0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- Se � gi� presente eseguo l'update
        * --- accept error
        bTrsErr=.f.
        * --- Write into INF_AUQU
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.INF_AUQU_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.INF_AUQU_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.INF_AUQU_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"QAFLUTGR ="+cp_NullLink(cp_ToStrODBC(this.w_QAFLUTGR),'INF_AUQU','QAFLUTGR');
          +",QAUTEGRP ="+cp_NullLink(cp_ToStrODBC(this.w_QAUTEGRP),'INF_AUQU','QAUTEGRP');
              +i_ccchkf ;
          +" where ";
              +"QACODQUE = "+cp_ToStrODBC(this.w_QACODQUE);
              +" and QAROWPRG = "+cp_ToStrODBC(this.w_QAROWPRG);
                 )
        else
          update (i_cTable) set;
              QAFLUTGR = this.w_QAFLUTGR;
              ,QAUTEGRP = this.w_QAUTEGRP;
              &i_ccchkf. ;
           where;
              QACODQUE = this.w_QACODQUE;
              and QAROWPRG = this.w_QAROWPRG;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
      bTrsErr=bTrsErr or bErr_037BDDC0
      * --- End
        select _Curs_GSCVAB77
        continue
      enddo
      use
    endif
    this.w_TMPC = ah_Msgformat("Aggiornamento autorizzazioni InfoPublisher eseguito con successo%0")
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = this.w_TMPC
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_037BDDC0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Inserisco l' autorizzazione
    * --- Insert into INF_AUQU
    i_nConn=i_TableProp[this.INF_AUQU_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INF_AUQU_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.INF_AUQU_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"QACODQUE"+",QAROWPRG"+",QAFLUTGR"+",QAUTEGRP"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_QACODQUE),'INF_AUQU','QACODQUE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_QAROWPRG),'INF_AUQU','QAROWPRG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_QAFLUTGR),'INF_AUQU','QAFLUTGR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_QAUTEGRP),'INF_AUQU','QAUTEGRP');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'QACODQUE',this.w_QACODQUE,'QAROWPRG',this.w_QAROWPRG,'QAFLUTGR',this.w_QAFLUTGR,'QAUTEGRP',this.w_QAUTEGRP)
      insert into (i_cTable) (QACODQUE,QAROWPRG,QAFLUTGR,QAUTEGRP &i_ccchkf. );
         values (;
           this.w_QACODQUE;
           ,this.w_QAROWPRG;
           ,this.w_QAFLUTGR;
           ,this.w_QAUTEGRP;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='INF_AUQU'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_GSCVAB77')
      use in _Curs_GSCVAB77
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
