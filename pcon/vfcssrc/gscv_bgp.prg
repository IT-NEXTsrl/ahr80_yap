* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscv_bgp                                                        *
*              Controllo allegati cliente fornitore -dati comunicazione        *
*                                                                              *
*      Author: Zucchetti - AT                                                  *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-07-13                                                      *
* Last revis.: 2007-09-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscv_bgp",oParentObject,m.w_NHF)
return(i_retval)

define class tgscv_bgp as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_PATHXDC = space(10)
  w_NUMTAB = 0
  w_ORIXDC = space(10)
  w_TOTVOCISTR = space(5)
  w_DATESTR = space(10)
  w_CONTA = 0
  w_NUMFIELD = 0
  w_PRIMKEY = space(10)
  w_CONTAINDICI = 0
  w_KEY1 = space(10)
  w_KEY2 = space(10)
  w_KEY3 = space(10)
  w_KEY4 = space(10)
  w_KEY5 = space(10)
  w_KL1 = .f.
  w_KL2 = .f.
  w_KL3 = .f.
  w_KL4 = .f.
  w_KL5 = .f.
  w_KLES = .f.
  w_LINKES = space(10)
  w_INIETXDC = space(10)
  w_BLANK = space(3)
  w_NUM = 0
  w_OKXDC = .f.
  * --- WorkFile variables
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- CONTROLLO ALLEGATI CLIENTE FORNITORE - DATI COMUNICAZIONE
    * --- FIle di LOG
    * --- Inserimento in PCON.XDC
    if upper(g_application)="ADHOC REVOLUTION"
      this.w_PATHXDC = "..\DISB\EXE\DISB.XDC"
    else
      this.w_PATHXDC = "..\PCON\EXE\PCON.XDC"
    endif
    COPY FILE (this.w_PATHXDC) TO this.w_PATHXDC +".bak"
     
 DECLARE ARRTABLE(6,3) 
 ARRTABLE (1,1) = "PAELCLFT" 
 ARRTABLE (1,2) = "Parametri elenchi clienti / fornitori" 
 ARRTABLE (1,3) = "GSCG_APE" 
 ARRTABLE (2,1) = "PAELCLFO" 
 ARRTABLE (2,2) = "Parametri elenchi clienti / fornitori" 
 ARRTABLE (2,3) = "GSCG_MPE" 
 ARRTABLE (3,1) = "DAELCLFO" 
 ARRTABLE (3,2) = "Dati comunicazione elenchi clienti / fornitori (Master)" 
 ARRTABLE (3,3) = "GSCG_MEL" 
 ARRTABLE (4,1) = "DAELCLFD" 
 ARRTABLE (4,2) = "Dati comunicazione elenchi clienti / fornitori (Detail)" 
 ARRTABLE (4,3) = "GSCG_MEL" 
 ARRTABLE (5,1) = "DAELGENE" 
 ARRTABLE (5,2) = "Generazione file allegati clienti/fornitori" 
 ARRTABLE (5,3) = "GSCG_AGE" 
 ARRTABLE (6,1) = "PAELCLFM" 
 ARRTABLE (6,2) = "Parametri elenchi clienti / fornitori mastri da escludere" 
 ARRTABLE (6,3) = "GSCG_MPX" 
 
    Create Cursor CURFIELD (TABELLA C(10), NOME C(10), TIPO C(1), LINT N(4), LDEC N(4), DESCRI C(80), CONTROLLO C(10), VALDEFA C(10), INDICE N(2))
    * --- Anagrafica Parametri elenchi clienti / fornitori
     
 Insert into CURFIELD Values ("PAELCLFT","PE__ANNO","C",4,0,"Anno di comunicazione","","",1) 
 Insert into CURFIELD Values ("PAELCLFT","PETIPCOM","C",1,0,"Tipo comunicazione clienti / fronitori","","",1) 
 Insert into CURFIELD Values ("PAELCLFT","PEFLGISO","C",1,0,"Esportazioni / importazione da nazione intestatario","","",0)
    * --- Movimentazione Parametri elenchi clienti / fornitori
     
 Insert into CURFIELD Values ("PAELCLFO","PE__ANNO","C",4,0,"Anno di comunicazione","","",1) 
 Insert into CURFIELD Values ("PAELCLFO","PETIPCOM","C",1,0,"Tipo comunicazione clienti / fronitori","","",1) 
 Insert into CURFIELD Values ("PAELCLFO","CPROWNUM","N",6,0,"","","",1) 
 Insert into CURFIELD Values ("PAELCLFO","PECAUCON","C",5,0,"Causale contabile","","",2) 
 Insert into CURFIELD Values ("PAELCLFO","PECODIVA","C",5,0,"Codice IVA","","",2) 
 Insert into CURFIELD Values ("PAELCLFO","PETIPREG","C",1,0,"Tipo registro IVA","","",0) 
 Insert into CURFIELD Values ("PAELCLFO","PECONDIZ","C",1,0,"Condizione parametro: escludi, attribuisci","","",0) 
 Insert into CURFIELD Values ("PAELCLFO","PETIPOPE","C",3,0,"Tipo operazione","","",0) 
 Insert into CURFIELD Values ("PAELCLFO","PECNDIZI","C",1,0,"Condizione per importo","","",0) 
 Insert into CURFIELD Values ("PAELCLFO","PEIMPCON","N",18,4,"Importo condizione","","",0) 
 Insert into CURFIELD Values ("PAELCLFO","PEIMPDCO","C",1,0,"Tipo importo da comunicare","","",0)
    * --- Generazione file allegati clienti/fornitori
     
 Insert into CURFIELD Values ("DAELGENE","GESERIAL","C",10,0,"Seriale comunicazione","","",1) 
 Insert into CURFIELD Values ("DAELGENE","GE__ANNO","C",4,0,"Anno di imposta","","",2) 
 Insert into CURFIELD Values ("DAELGENE","GEDATINV","D",8,0,"Data invio","","",0) 
 Insert into CURFIELD Values ("DAELGENE","GECODFIS","C",16,0,"Codice fiscale","","",0) 
 Insert into CURFIELD Values ("DAELGENE","GEPARIVA","C",11,0,"Partita IVA","","",0) 
 Insert into CURFIELD Values ("DAELGENE","GECOGNOM","C",26,0,"Cognome","","",0) 
 Insert into CURFIELD Values ("DAELGENE","GE__NOME","C",25,0,"Nome","","",0) 
 Insert into CURFIELD Values ("DAELGENE","GE_SESSO","C",1,0,"Sesso","","",0) 
 Insert into CURFIELD Values ("DAELGENE","GEDATNAS","D",8,0,"Data di nascita","","",0) 
 Insert into CURFIELD Values ("DAELGENE","GECOMUNE","C",40,0,"Comune o stato estero di nascita / della sede legale","","",0) 
 Insert into CURFIELD Values ("DAELGENE","GEPROVIN","C",2,0,"Provincia di nascita / della sede legale","","",0) 
 Insert into CURFIELD Values ("DAELGENE","GERAGSOC","C",70,0,"Ragione sociale","","",0) 
 Insert into CURFIELD Values ("DAELGENE","GECODSOG","C",16,0,"Codice fiscale del soggetto","","",0) 
 Insert into CURFIELD Values ("DAELGENE","GEPROGIN","N",4,0,"Progressivo invio telematico","","",0) 
 Insert into CURFIELD Values ("DAELGENE","GECODINT","C",16,0,"Codice fiscale intermediario che effettua la trasmissione","","",0) 
 Insert into CURFIELD Values ("DAELGENE","GECODCAF","C",5,0,"Numero di iscrizione all'albo del C.A.F.","","",0) 
 Insert into CURFIELD Values ("DAELGENE","GEIMPTRA","C",1,0,"Impegno a trasmettere in via telematica la comunicazione","","",0) 
 Insert into CURFIELD Values ("DAELGENE","GEDATIMP","D",8,0,"Data dell'impegno","","",0) 
 Insert into CURFIELD Values ("DAELGENE","GENOMFIL","C",254,0,"Nome file e percorso per inoltro telematico","","",0)
    * --- Dati comunicazione elenchi clienti / fornitori-Master
     
 Insert into CURFIELD Values ("DAELCLFO","DESERIAL","C",10,0,"Seriale","","",1) 
 Insert into CURFIELD Values ("DAELCLFO","DE__ANNO","C",4,0,"Anno riferimento","","",0) 
 Insert into CURFIELD Values ("DAELCLFO","DETIPSOG","C",1,0,"Tipo soggetto (cliente / fornitore)","","",0) 
 Insert into CURFIELD Values ("DAELCLFO","DEPARIVA","C",12,0,"Partita IVA","","",0) 
 Insert into CURFIELD Values ("DAELCLFO","DECODFIS","C",16,0,"Codice fiscale","","",0) 
 Insert into CURFIELD Values ("DAELCLFO","DETIPCON","C",1,0,"Tipo conto (cliente / fornitore)","","",0) 
 Insert into CURFIELD Values ("DAELCLFO","DECODCON","C",15,0,"Codice conto","","",0) 
 Insert into CURFIELD Values ("DAELCLFO","DEDESCON","C",50,0,"Descrizione","","",0) 
 Insert into CURFIELD Values ("DAELCLFO","DECONPLU","C",1,0,"Flag codifica non univoca","","",0) 
 Insert into CURFIELD Values ("DAELCLFO","DEESCGEN","C",1,0,"Flag escludi da generazione","","",0)
    * --- Dati comunicazione elenchi clienti / fornitori-Detail
     
 Insert into CURFIELD Values ("DAELCLFD","DESERIAL","C",10,0,"Seriale","","",1) 
 Insert into CURFIELD Values ("DAELCLFD","CPROWNUM","N",6,0,"","","",1) 
 Insert into CURFIELD Values ("DAELCLFD","DEIMPIMP","N",18,4,"Importo operazioni imponibili","","",0) 
 Insert into CURFIELD Values ("DAELCLFD","DEIMPAFF","N",18,4,"Importo imposta afferente","","",0) 
 Insert into CURFIELD Values ("DAELCLFD","DEIMPNOI","N",18,4,"Importo operazioni non imponibili","","",0) 
 Insert into CURFIELD Values ("DAELCLFD","DEIMPESE","N",18,4,"Importo operazioni esenti","","",0) 
 Insert into CURFIELD Values ("DAELCLFD","DEIMPNIN","N",18,4,"Importo operazioni imponibile IVA non esposta","","",0) 
 Insert into CURFIELD Values ("DAELCLFD","DEIMPLOR","N",18,4,"Imposta operazioni imponibili comprensive imposta afferente","","",0) 
 Insert into CURFIELD Values ("DAELCLFD","DERIFPNT","C",10,0,"Riferimento registrazione di primanota","","",0)
    * --- Parametri elenchi clienti / fornitori mastri da escludere
     
 Insert into CURFIELD Values ("PAELCLFM","PE__ANNO","C",4,0,"Anno di comunicazione","","",1) 
 Insert into CURFIELD Values ("PAELCLFM","PETIPCOM","C",1,0,"Tipo comunicazione clienti / fronitori","","",1) 
 Insert into CURFIELD Values ("PAELCLFM","CPROWNUM","N",6,0,"","","",1) 
 Insert into CURFIELD Values ("PAELCLFM","PEMASTRO","C",15,0,"Mastro da escludere nell estrazione","","",0) 
 Insert into CURFIELD Values ("PAELCLFM","PECATCON","C",5,0,"Categoria contabile da escludere nell estrazione","","",0) 
 Insert into CURFIELD Values ("PAELCLFM","PECATCOM","C",3,0,"Categ.commerciale da escludere nell estrazione","","",0)
    * --- Carico pcon.xdc in una stringa per poterlo scomporre
    this.w_ORIXDC = FILETOSTR(this.w_PATHXDC)
    * --- Occorre aggiornare la stringa della data
    this.w_DATESTR = SUBSTR(this.w_ORIXDC, 1, AT(CHR(13)+CHR(10), this.w_ORIXDC, 1)+2) + TTOC(datetime(),1) +'"'+CHR(13)+CHR(10)
    this.w_NUMTAB = 1
    do while this.w_NUMTAB <=6
      if AT(ARRTABLE (this.w_NUMTAB,1),this.w_ORIXDC) =0
        this.w_OKXDC = .t.
        ah_msg( "Inserimento nuove tabelle in corso..")
        Select * from CURFIELD Into Cursor ELAB Where TABELLA=alltrim(ARRTABLE (this.w_NUMTAB,1))
        COUNT to this.w_NUMFIELD
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      this.w_NUMTAB = this.w_NUMTAB + 1
    enddo
    if this.w_OKXDC
      this.w_NUM = STRTOFILE(this.w_ORIXDC,this.w_PATHXDC)
    endif
    if this.w_NHF>=0
      this.w_TMPC = AH_MsgFormat("- Aggiornamento plan eseguito con successo.%0Per rendere operative le modifiche, uscire e rientrare nella procedura.")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    if USED ("CURFIELD")
      SELECT CURFIELD
      USE
    endif
    if USED ( "ELAB" )
      SELECT ELAB
      USE
    endif
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = this.w_TMPC
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorno il progressivo
    this.w_TOTVOCISTR = SUBSTR(this.w_ORIXDC, AT(CHR(13)+CHR(10), this.w_ORIXDC, 2)+2,(AT(CHR(13)+CHR(10), this.w_ORIXDC, 3)-AT(CHR(13)+CHR(10), this.w_ORIXDC, 2)-2) )
    this.w_TOTVOCISTR = " "+alltrim(str(VAL(alltrim(this.w_TOTVOCISTR))+1))
    * --- Ricomposizione xdc con nuove informazioni di testata aggiunte
    this.w_ORIXDC = this.w_DATESTR + this.w_TOTVOCISTR + SUBSTR(this.w_ORIXDC, AT(CHR(13)+CHR(10),this.w_ORIXDC,3))
    this.w_INIETXDC = '"'+ARRTABLE (this.w_NUMTAB,1)+'"'+CHR(13)+CHR(10)+ ".F."+CHR(13)+CHR(10)+ ".F."+CHR(13)+CHR(10)
    this.w_INIETXDC = this.w_INIETXDC + ".T."+CHR(13)+CHR(10)+'"'+"xxx"+ ARRTABLE(this.w_NUMTAB,1)+'"'+CHR(13)+CHR(10)+" "+alltrim(str(this.w_NUMFIELD))+CHR(13)+CHR(10)
    this.w_CONTA = 1
    this.w_KEY1 = '"'
    this.w_KEY2 = '"'
    this.w_KEY3 = '"'
    this.w_KEY4 = '"'
    this.w_KEY5 = '"'
    this.w_LINKES = ""
    this.w_KL1 = .f.
    this.w_KL2 = .f.
    this.w_KL3 = .f.
    this.w_KL4 = .f.
    this.w_KL5 = .f.
    this.w_KLES = .f.
    this.w_CONTAINDICI = 0
    * --- INSERIMENTO CAMPI TABELLA
    Select Elab 
 Go Top
    do while not eof()
      this.w_INIETXDC = this.w_INIETXDC + '"'+Alltrim(NOME)+'"'+CHR(13)+CHR(10)+ '"'+Alltrim(TIPO)+'"'+CHR(13)+CHR(10)+" "+alltrim(str(LINT))+CHR(13)+CHR(10)+" "+alltrim(str(LDEC))+CHR(13)+CHR(10)
      this.w_INIETXDC = this.w_INIETXDC + '"'+Alltrim(DESCRI)+'"'+CHR(13)+CHR(10)+ '"'+Alltrim(CONTROLLO)+'"'+CHR(13)+CHR(10)+'"'+Alltrim(VALDEFA)+'"'+CHR(13)+CHR(10) + IIF(INDICE <>1,".F.",".T.")+CHR(13)+CHR(10)+"*---*"+CHR(13)+CHR(10)
      if INDICE > 0
        * --- Costruzione stringa chiave primaria
        do case
          case INDICE=1
            if !this.w_KL1
              this.w_CONTAINDICI = this.w_CONTAINDICI + 1
            endif
            this.w_KEY1 = this.w_KEY1+IIF(this.w_KL1,","+Alltrim(NOME),Alltrim(NOME))
            if TABELLA="DAELCLFD" And NOME<>"CPROWNUM"
              this.w_LINKES = " 1"+CHR(13)+CHR(10)+'"'+"DAELCLFO"+'"'+CHR(13)+CHR(10)+" 2"+CHR(13)+CHR(10)+'"'+Alltrim(NOME)+'"'+CHR(13)+CHR(10)+'"'+Alltrim(NOME)+'"'+CHR(13)+CHR(10)+"*---*"+CHR(13)+CHR(10)
              this.w_KLES = .t.
            endif
            if TABELLA="PAELCLFO" And NOME="PE__ANNO"
              this.w_LINKES = " 1"+CHR(13)+CHR(10)+'"'+"PAELCLFT"+'"'+CHR(13)+CHR(10)+" 2"+CHR(13)+CHR(10)+'"'+"PE__ANNO,PETIPCOM"+'"'+CHR(13)+CHR(10)+'"'+"PE__ANNO,PETIPCOM"+'"'+CHR(13)+CHR(10)+"*---*"+CHR(13)+CHR(10)
              this.w_KLES = .t.
            endif
            if TABELLA="PAELCLFM" And NOME="PE__ANNO"
              this.w_LINKES = " 1"+CHR(13)+CHR(10)+'"'+"PAELCLFT"+'"'+CHR(13)+CHR(10)+" 2"+CHR(13)+CHR(10)+'"'+"PE__ANNO,PETIPCOM"+'"'+CHR(13)+CHR(10)+'"'+"PE__ANNO,PETIPCOM"+'"'+CHR(13)+CHR(10)+"*---*"+CHR(13)+CHR(10)
              this.w_KLES = .t.
            endif
            this.w_KL1 = .t.
          case INDICE=2
            if !this.w_KL2
              this.w_CONTAINDICI = this.w_CONTAINDICI + 1
            endif
            this.w_KEY2 = this.w_KEY2+IIF(this.w_KL2,","+Alltrim(NOME),Alltrim(NOME))
            this.w_KL2 = .t.
          case INDICE=3
            if !this.w_KL3
              this.w_CONTAINDICI = this.w_CONTAINDICI + 1
            endif
            this.w_KEY3 = this.w_KEY3+IIF(this.w_KL3,","+Alltrim(NOME),Alltrim(NOME))
            this.w_KL3 = .t.
          case INDICE=4
            if !this.w_KL4
              this.w_CONTAINDICI = this.w_CONTAINDICI + 1
            endif
            this.w_KEY4 = this.w_KEY4+IIF(this.w_KL4,","+Alltrim(NOME),Alltrim(NOME))
            this.w_KL4 = .t.
          case INDICE=5
            if !this.w_KL5
              this.w_CONTAINDICI = this.w_CONTAINDICI + 1
            endif
            this.w_KEY5 = this.w_KEY5+IIF(this.w_KL5,","+Alltrim(NOME),Alltrim(NOME))
            this.w_KL5 = .t.
        endcase
      endif
      if Not Eof("Elab")
        Skip
      endif
    enddo
    * --- Parte finale, resoconto indici, descrizione tabella  e nome programma
    this.w_INIETXDC = this.w_INIETXDC + " "+alltrim(str(this.w_CONTAINDICI)) + CHR(13)+CHR(10)
    * --- Chiave primaria
    this.w_KEY1 = this.w_KEY1+'"' +CHR(13)+CHR(10)+".T."+CHR(13)+CHR(10)+"*---*"+CHR(13)+CHR(10)
    this.w_INIETXDC = this.w_INIETXDC + this.w_KEY1
    * --- Indici 
    this.w_KEY2 = this.w_KEY2+'"' +CHR(13)+CHR(10)+".F."+CHR(13)+CHR(10)+"*---*"+CHR(13)+CHR(10)
    if this.w_KL2
      this.w_INIETXDC = this.w_INIETXDC + this.w_KEY2
    endif
    this.w_KEY3 = this.w_KEY3+'"' +CHR(13)+CHR(10)+".F."+CHR(13)+CHR(10)+"*---*"+CHR(13)+CHR(10)
    if this.w_KL3
      this.w_INIETXDC = this.w_INIETXDC + this.w_KEY3
    endif
    this.w_KEY4 = this.w_KEY4+'"' +CHR(13)+CHR(10)+".F."+CHR(13)+CHR(10)+"*---*"+CHR(13)+CHR(10)
    if this.w_KL4
      this.w_INIETXDC = this.w_INIETXDC + this.w_KEY4
    endif
    this.w_KEY5 = this.w_KEY5+'"' +CHR(13)+CHR(10)+".F."+CHR(13)+CHR(10)+"*---*"+CHR(13)+CHR(10)
    if this.w_KL5
      this.w_INIETXDC = this.w_INIETXDC + this.w_KEY5
    endif
    if this.w_KLES
      this.w_INIETXDC = this.w_INIETXDC + this.w_LINKES
    else
      this.w_INIETXDC = this.w_INIETXDC + " 0" +CHR(13)+CHR(10)
    endif
    this.w_INIETXDC = this.w_INIETXDC + '"'+Alltrim(ARRTABLE (this.w_NUMTAB,2))+'"'+CHR(13)+CHR(10)+ '"'+TTOC(datetime(),1) +'"'+CHR(13)+CHR(10)+'""'+CHR(13)+CHR(10)+".F."+CHR(13)+CHR(10)+".F."+CHR(13)+CHR(10)+".F."+CHR(13)+CHR(10)+".F."+CHR(13)+CHR(10)
    this.w_INIETXDC = this.w_INIETXDC + '"'+Alltrim(ARRTABLE (this.w_NUMTAB,3))+'"'+CHR(13)+CHR(10)+'""'+CHR(13)+CHR(10)+"*---*"+CHR(13)+CHR(10)
    this.w_ORIXDC = this.w_ORIXDC + this.w_INIETXDC
  endproc


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
