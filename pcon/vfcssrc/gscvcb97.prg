* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcb97                                                        *
*              Valorizzazione descrizione prestazioni nei raggruppamenti       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-10-14                                                      *
* Last revis.: 2011-11-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcb97",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcb97 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_NCONN = 0
  w_CODPRE = space(20)
  w_DESPRE = space(40)
  w_CODRAGG = space(10)
  w_ROWNUM = 0
  * --- WorkFile variables
  PRE_ITER_idx=0
  KEY_ARTI_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Valorizzazione descrizione prestazioni nei raggruppamenti
    this.w_NCONN = i_TableProp[this.PRE_ITER_idx,3] 
    if ISALT()
      * --- Try
      local bErr_035FD450
      bErr_035FD450=bTrsErr
      this.Try_035FD450()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_035FD450
      * --- End
    else
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("Procedura di conversione eseguita con successo")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    * --- Resoconto - sempre positivo
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Elaborazione terminata con successo")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = ah_Msgformat("Elaborazione terminata con successo")
  endproc
  proc Try_035FD450()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Select from PRE_ITER
    i_nConn=i_TableProp[this.PRE_ITER_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRE_ITER_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select PRE_ITER.ITCODPRE, PRE_ITER.ITDESPRE, ITCODICE, CPROWNUM  from "+i_cTable+" PRE_ITER ";
           ,"_Curs_PRE_ITER")
    else
      select PRE_ITER.ITCODPRE, PRE_ITER.ITDESPRE, ITCODICE, CPROWNUM from (i_cTable);
        into cursor _Curs_PRE_ITER
    endif
    if used('_Curs_PRE_ITER')
      select _Curs_PRE_ITER
      locate for 1=1
      do while not(eof())
      if EMPTY(NVL(_Curs_PRE_ITER.ITDESPRE, ""))
        this.w_CODPRE = _Curs_PRE_ITER.ITCODPRE
        this.w_CODRAGG = _Curs_PRE_ITER.ITCODICE
        this.w_ROWNUM = _Curs_PRE_ITER.CPROWNUM
        * --- Legge la descrizione della prestazione
        * --- Read from KEY_ARTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CADESART"+;
            " from "+i_cTable+" KEY_ARTI where ";
                +"CACODICE = "+cp_ToStrODBC(this.w_CODPRE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CADESART;
            from (i_cTable) where;
                CACODICE = this.w_CODPRE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DESPRE = NVL(cp_ToDate(_read_.CADESART),cp_NullValue(_read_.CADESART))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Scrive la descrizione della prestazione
        * --- Write into PRE_ITER
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.PRE_ITER_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PRE_ITER_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.PRE_ITER_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ITDESPRE ="+cp_NullLink(cp_ToStrODBC(this.w_DESPRE),'PRE_ITER','ITDESPRE');
              +i_ccchkf ;
          +" where ";
              +"ITCODICE = "+cp_ToStrODBC(this.w_CODRAGG);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWNUM);
                 )
        else
          update (i_cTable) set;
              ITDESPRE = this.w_DESPRE;
              &i_ccchkf. ;
           where;
              ITCODICE = this.w_CODRAGG;
              and CPROWNUM = this.w_ROWNUM;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
        select _Curs_PRE_ITER
        continue
      enddo
      use
    endif
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Valorizzata descrizione prestazioni nei raggruppamenti")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='PRE_ITER'
    this.cWorkTables[2]='KEY_ARTI'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_PRE_ITER')
      use in _Curs_PRE_ITER
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
