* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscv_b98                                                        *
*              Aggiornamento cambio di apertura                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_50]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-12-02                                                      *
* Last revis.: 2006-12-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscv_b98",oParentObject,m.w_NHF)
return(i_retval)

define class tgscv_b98 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  * --- WorkFile variables
  PAR_TITE_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questa procedura di conversione modifica il valore campo PTCAOAPE
    * --- Try
    local bErr_0372FCF0
    bErr_0372FCF0=bTrsErr
    this.Try_0372FCF0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE - impossibile aggiornare campi PTCAOAPE,PTSERRIF,PTORDRIF,PTNUMRIF,PTNUMPRO su tabelle PAR_TITE" )
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_0372FCF0
    * --- End
  endproc
  proc Try_0372FCF0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    ah_Msg("Aggiornamento campi riferimenti scadenze raggruppate",.T.)
    * --- Valorizzo a vuoti i campi PTSERRIF, PTORDRIF e PTNUMRIF
    * --- Write into PAR_TITE
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PAR_TITE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_TITE_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PTSERRIF ="+cp_NullLink(cp_ToStrODBC(SPACE(10)),'PAR_TITE','PTSERRIF');
      +",PTORDRIF ="+cp_NullLink(cp_ToStrODBC(0),'PAR_TITE','PTORDRIF');
      +",PTNUMRIF ="+cp_NullLink(cp_ToStrODBC(0),'PAR_TITE','PTNUMRIF');
          +i_ccchkf ;
      +" where ";
          +"PTORDRIF <> "+cp_ToStrODBC(0);
             )
    else
      update (i_cTable) set;
          PTSERRIF = SPACE(10);
          ,PTORDRIF = 0;
          ,PTNUMRIF = 0;
          &i_ccchkf. ;
       where;
          PTORDRIF <> 0;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    ah_Msg("Aggiornamento cambio apertura",.T.)
    * --- Eseguo sql statment perch� tale operazione non � possibile effettuarla con una write
     
 i_Rows = cp_TrsSQL(i_TableProp[this.PAR_TITE_idx,3], "UPDATE "+cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])+" SET PTCAOAPE=PTCAOVAL "+; 
 +" where PTCAOAPE=0 " )
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Aggiornamento campi PTCAOAPE,PTSERRIF,PTORDRIF,PTNUMRIF,PTNUMPRO su tabella PAR_TITE eseguito con successo")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = this.oParentObject.w_PMSG + this.w_TMPC 
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='PAR_TITE'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
