* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvab79                                                        *
*              Aggiorna numero partita (/A)                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_80]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-02-27                                                      *
* Last revis.: 2003-02-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvab79",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvab79 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_MESS = space(100)
  w_ROWS = 0
  w_CODESE = space(4)
  w_NUMDOC = 0
  w_ALFDOC = space(10)
  w_PTNUMPAR = space(31)
  * --- WorkFile variables
  PAR_TITE_idx=0
  PNT_MAST_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna il numero partita per le partite di creazione che hanno
    *     numero partita che finisce con ACCONTO.
    *     Problema legato alla cancellazione di contabilizzazioni di distinte che
    *     chiudevano partite di fatture con alfa terminante per A
    * --- FIle di LOG
    * --- Try
    local bErr_0361DFA0
    bErr_0361DFA0=bTrsErr
    this.Try_0361DFA0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE %1 - impossibile aggiornare le partite", this.oParentObject.w_PMSG )
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_0361DFA0
    * --- End
  endproc
  proc Try_0361DFA0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    this.w_ROWS = 0
    * --- Recupero le partite che terminano con Acconto come numero partita e che
    *     sono di creazione. Devono anche essere legate a prima nota.
    *     
    *     Per ognuna di queste ricalcolo il numero partita recuperando le informazioni
    *     dalla testata prima nota.
    *     Teoricamente poteva essere fatto con una WRITE FROM QUERY
    *     visto l'esiguo numero di partite e la complessit� dell'espressione per determinare
    *     PTNUMPAR si � deciso di utilizzare una soluzione algoritmica
    * --- Select from PAR_TITE
    i_nConn=i_TableProp[this.PAR_TITE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select PTSERIAL,PTROWORD,CPROWNUM  from "+i_cTable+" PAR_TITE ";
          +" where PTFLCRSA='C' And PTNUMPAR Like '%Acconto%' And PTROWORD>0 ";
           ,"_Curs_PAR_TITE")
    else
      select PTSERIAL,PTROWORD,CPROWNUM from (i_cTable);
       where PTFLCRSA="C" And PTNUMPAR Like "%Acconto%" And PTROWORD>0 ;
        into cursor _Curs_PAR_TITE
    endif
    if used('_Curs_PAR_TITE')
      select _Curs_PAR_TITE
      locate for 1=1
      do while not(eof())
      * --- Read from PNT_MAST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PNT_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "PNNUMDOC,PNALFDOC,PNCOMPET"+;
          " from "+i_cTable+" PNT_MAST where ";
              +"PNSERIAL = "+cp_ToStrODBC(_Curs_PAR_TITE.PTSERIAL);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          PNNUMDOC,PNALFDOC,PNCOMPET;
          from (i_cTable) where;
              PNSERIAL = _Curs_PAR_TITE.PTSERIAL;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_NUMDOC = NVL(cp_ToDate(_read_.PNNUMDOC),cp_NullValue(_read_.PNNUMDOC))
        this.w_ALFDOC = NVL(cp_ToDate(_read_.PNALFDOC),cp_NullValue(_read_.PNALFDOC))
        this.w_CODESE = NVL(cp_ToDate(_read_.PNCOMPET),cp_NullValue(_read_.PNCOMPET))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_PTNUMPAR = CANUMPAR("N", this.w_CODESE, this.w_NUMDOC, this.w_ALFDOC)
      this.w_ROWS = this.w_ROWS + 1
      * --- Write into PAR_TITE
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.PAR_TITE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_TITE_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"PTNUMPAR ="+cp_NullLink(cp_ToStrODBC(this.w_PTNUMPAR),'PAR_TITE','PTNUMPAR');
            +i_ccchkf ;
        +" where ";
            +"PTSERIAL = "+cp_ToStrODBC(_Curs_PAR_TITE.PTSERIAL);
            +" and PTROWORD = "+cp_ToStrODBC(_Curs_PAR_TITE.PTROWORD);
            +" and CPROWNUM = "+cp_ToStrODBC(_Curs_PAR_TITE.CPROWNUM);
               )
      else
        update (i_cTable) set;
            PTNUMPAR = this.w_PTNUMPAR;
            &i_ccchkf. ;
         where;
            PTSERIAL = _Curs_PAR_TITE.PTSERIAL;
            and PTROWORD = _Curs_PAR_TITE.PTROWORD;
            and CPROWNUM = _Curs_PAR_TITE.CPROWNUM;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
        select _Curs_PAR_TITE
        continue
      enddo
      use
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = ah_Msgformat("Conversione eseguita correttamente su %1 partite", Alltrim(Str( this.w_Rows )) )
    this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.oParentObject.w_PMSG)
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='PAR_TITE'
    this.cWorkTables[2]='PNT_MAST'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_PAR_TITE')
      use in _Curs_PAR_TITE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
