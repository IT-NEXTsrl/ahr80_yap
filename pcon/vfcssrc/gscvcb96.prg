* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcb96                                                        *
*              Elenco documenti da ricalcolare                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_6]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-03-23                                                      *
* Last revis.: 2011-10-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcb96",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcb96 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_MESS = space(100)
  w_DATINI = ctod("  /  /  ")
  * --- WorkFile variables
  CONTROPA_idx=0
  RIPATMP1_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- FIle di LOG
    * --- Try
    local bErr_037A4D60
    bErr_037A4D60=bTrsErr
    this.Try_037A4D60()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = this.oParentObject.w_PMSG
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_037A4D60
    * --- End
  endproc
  proc Try_037A4D60()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.w_TMPC = "Elenco documenti da verificare"
    this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    this.w_TMPC = "----------------------------------------------"
    this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    this.w_DATINI = cp_chartodate("16-09-2011")
    * --- Select from GSCVCB96
    do vq_exec with 'GSCVCB96',this,'_Curs_GSCVCB96','',.f.,.t.
    if used('_Curs_GSCVCB96')
      select _Curs_GSCVCB96
      locate for 1=1
      do while not(eof())
      this.w_TMPC = "Seriale " + Alltrim(_Curs_GSCVCB96.MVSERIAL) +" "+ Alltrim(_Curs_GSCVCB96.MVTIPDOC) + " reg. " + DTOC(_Curs_GSCVCB96.MVDATREG) + " "+ Alltrim(str(_Curs_GSCVCB96.MVNUMREG)) + " intestatario " + Alltrim(_Curs_GSCVCB96.MVCODCON) + " del " + DTOC(_Curs_GSCVCB96.MVDATDOC) + " numero " + Alltrim(str(_Curs_GSCVCB96.MVNUMDOC)) + IIF(Not Empty(Nvl(_Curs_GSCVCB96.MVALFDOC," ")),"\"+Alltrim(Nvl(_Curs_GSCVCB96.MVALFDOC," ")),"") +" "+ Alltrim(_Curs_GSCVCB96.MVRIFCON) 
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        select _Curs_GSCVCB96
        continue
      enddo
      use
    endif
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella DB2 e ORACLE
    pName="CONTROPA"
    pDatabaseType=i_ServerConn[1,6]
    * --- Devo ricalcolare la connessione perch� creo la tabella da query e il painter non 
    *     la ricalcola da sola. i_nConn viene poi passato al GSCV_BDT
    i_nConn=i_TableProp[this.CONTROPA_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.CONTROPA_idx,2])
    * --- La query esegue una SUBSTR dei primi 20 caratteri sul campo COARTDES
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('GSCVBB57',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    * --- Elimino la tabella
    if Not GSCV_BDT( this, "CONTROPA" , i_nConn )
      * --- Raise
      i_Error="Error"
      return
    endif
    * --- Ricostruisco il database per la tabella
    GSCV_BRT(this, "CONTROPA" , i_nConn , .T.)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into CONTROPA
    i_nConn=i_TableProp[this.CONTROPA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTROPA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.CONTROPA_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    ah_Msg("Eliminata tabella temporanea tmp",.T.)
  endproc


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='CONTROPA'
    this.cWorkTables[2]='*RIPATMP1'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_GSCVCB96')
      use in _Curs_GSCVCB96
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
