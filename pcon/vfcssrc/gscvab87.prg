* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvab87                                                        *
*              Aggiornamento saldi iniziali\finali fuori linea                 *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_108]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-03-17                                                      *
* Last revis.: 2006-12-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvab87",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvab87 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TMPC = space(10)
  w_TEMP = space(10)
  * --- WorkFile variables
  SALDICON_idx=0
  PNTSMAST_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Procedura che aggiorna Saldo Iniziale \Finale  nella tabella SALDICON
    * --- FIle di LOG
    * --- Try
    local bErr_04A99300
    bErr_04A99300=bTrsErr
    this.Try_04A99300()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE GENERICO - Impossibile aggiornare tabella %1", "SALDICON")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_04A99300
    * --- End
  endproc
  proc Try_04A99300()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Write into SALDICON
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SALDICON_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="SLTIPCON,SLCODICE,SLCODESE"
      do vq_exec with 'GSCVAB87',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICON_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="SALDICON.SLTIPCON = _t2.SLTIPCON";
              +" and "+"SALDICON.SLCODICE = _t2.SLCODICE";
              +" and "+"SALDICON.SLCODESE = _t2.SLCODESE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SLDARINF = _t2.SLDARINF";
          +",SLAVEINF = _t2.SLAVEINF";
          +",SLDARFIF = _t2.SLDARFIF";
          +",SLAVEFIF = _t2.SLAVEFIF";
          +i_ccchkf;
          +" from "+i_cTable+" SALDICON, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="SALDICON.SLTIPCON = _t2.SLTIPCON";
              +" and "+"SALDICON.SLCODICE = _t2.SLCODICE";
              +" and "+"SALDICON.SLCODESE = _t2.SLCODESE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDICON, "+i_cQueryTable+" _t2 set ";
          +"SALDICON.SLDARINF = _t2.SLDARINF";
          +",SALDICON.SLAVEINF = _t2.SLAVEINF";
          +",SALDICON.SLDARFIF = _t2.SLDARFIF";
          +",SALDICON.SLAVEFIF = _t2.SLAVEFIF";
          +Iif(Empty(i_ccchkf),"",",SALDICON.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="SALDICON.SLTIPCON = t2.SLTIPCON";
              +" and "+"SALDICON.SLCODICE = t2.SLCODICE";
              +" and "+"SALDICON.SLCODESE = t2.SLCODESE";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDICON set (";
          +"SLDARINF,";
          +"SLAVEINF,";
          +"SLDARFIF,";
          +"SLAVEFIF";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.SLDARINF,";
          +"t2.SLAVEINF,";
          +"t2.SLDARFIF,";
          +"t2.SLAVEFIF";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="SALDICON.SLTIPCON = _t2.SLTIPCON";
              +" and "+"SALDICON.SLCODICE = _t2.SLCODICE";
              +" and "+"SALDICON.SLCODESE = _t2.SLCODESE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDICON set ";
          +"SLDARINF = _t2.SLDARINF";
          +",SLAVEINF = _t2.SLAVEINF";
          +",SLDARFIF = _t2.SLDARFIF";
          +",SLAVEFIF = _t2.SLAVEFIF";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".SLTIPCON = "+i_cQueryTable+".SLTIPCON";
              +" and "+i_cTable+".SLCODICE = "+i_cQueryTable+".SLCODICE";
              +" and "+i_cTable+".SLCODESE = "+i_cQueryTable+".SLCODESE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SLDARINF = (select SLDARINF from "+i_cQueryTable+" where "+i_cWhere+")";
          +",SLAVEINF = (select SLAVEINF from "+i_cQueryTable+" where "+i_cWhere+")";
          +",SLDARFIF = (select SLDARFIF from "+i_cQueryTable+" where "+i_cWhere+")";
          +",SLAVEFIF = (select SLAVEFIF from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Aggiornamento campo saldi iniziali\finali fuori linea tabella SALDICON avvenuto correttamente")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      this.w_TMPC = ah_Msgformat("Ricostruire i saldi contabili per gli esercizi storicizzati")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = ah_Msgformat("Conversione eseguita correttamente")
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='SALDICON'
    this.cWorkTables[2]='PNTSMAST'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
