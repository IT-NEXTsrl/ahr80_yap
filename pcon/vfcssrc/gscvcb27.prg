* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcb27                                                        *
*              Inserimento stati SOStitutiva Ver. 1.8                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-01-14                                                      *
* Last revis.: 2009-04-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcb27",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcb27 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TMPC = space(10)
  w_TEMP = space(10)
  w_MESS = space(250)
  * --- WorkFile variables
  RAP_PRES_idx=0
  DIPENDEN_idx=0
  STATISOS_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inserisce nella tabella STATISOS gli stati ammessi per i documenti inviati a SOStitutiva allineati alla Ver. 1.8
    * --- Try
    local bErr_03015418
    bErr_03015418=bTrsErr
    this.Try_03015418()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = this.oParentObject.w_PMSG
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_03015418
    * --- End
  endproc
  proc Try_03015418()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    Dimension STATISOST (13,2)
    * --- Inserisco gli stati ammessi nell'array
    STORE "Unknown" TO STATISOST(1,1) 
 STORE "Errore di sistema" TO STATISOST(1,2) 
 
 STORE "DOCSTD" TO STATISOST(2,1) 
 STORE "Archiviato" TO STATISOST(2,2) 
 
 STORE "DOCRDY" TO STATISOST(3,1) 
 STORE "Pronto per la firma" TO STATISOST(3,2) 
 
 STORE "DOCSIGN" TO STATISOST(4,1) 
 STORE "Firmato e conservato" TO STATISOST(4,2) 
 
 STORE "DOCREP" TO STATISOST(5,1) 
 STORE "Sostituito" TO STATISOST(5,2) 
 
 STORE "DOCKO" TO STATISOST(6,1) 
 STORE "Non archiviato" TO STATISOST(6,2) 
 
 STORE "DOCREPSIGN" TO STATISOST(7,1) 
 STORE "Sostitutivo" TO STATISOST(7,2) 
 
 STORE "DOCADDTOEVAL" TO STATISOST(8,1) 
 STORE "Integrativo da valutare" TO STATISOST(8,2) 
 
 STORE "DOCWRNEXP" TO STATISOST(9,1) 
 STORE "Scaduto" TO STATISOST(9,2) 
 
 STORE "NOT_SOS" TO STATISOST(10,1) 
 STORE "Servizio SOS non attivo" TO STATISOST(10,2) 
 
 STORE "DA_INVIARE" TO STATISOST(11,1) 
 STORE "Da inviare" TO STATISOST(11,2) 
 
 STORE "INVIATO" TO STATISOST(12,1) 
 STORE "Inviato" TO STATISOST(12,2) 
 
 
 STORE "ALL" TO STATISOST(13,1) 
 STORE "Tutti" TO STATISOST(13,2)
    * --- begin transaction
    cp_BeginTrs()
    * --- Inserisco gli stati nella tabella STATISOS
    For i=1 to alen("STATISOST",1)
    * --- Write into STATISOS
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.STATISOS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STATISOS_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.STATISOS_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"STATODES ="+cp_NullLink(cp_ToStrODBC(STATISOST(i,2)),'STATISOS','STATODES');
          +i_ccchkf ;
      +" where ";
          +"STATOVAL = "+cp_ToStrODBC(STATISOST(i,1));
             )
    else
      update (i_cTable) set;
          STATODES = STATISOST(i,2);
          &i_ccchkf. ;
       where;
          STATOVAL = STATISOST(i,1);

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    if i_Rows=0
      * --- Insert into STATISOS
      i_nConn=i_TableProp[this.STATISOS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.STATISOS_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.STATISOS_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"STATOVAL"+",STATODES"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(STATISOST(i,1)),'STATISOS','STATOVAL');
        +","+cp_NullLink(cp_ToStrODBC(STATISOST(i,2)),'STATISOS','STATODES');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'STATOVAL',STATISOST(i,1),'STATODES',STATISOST(i,2))
        insert into (i_cTable) (STATOVAL,STATODES &i_ccchkf. );
           values (;
             STATISOST(i,1);
             ,STATISOST(i,2);
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
    EndFor
    * --- Esecuzione ok
    * --- commit
    cp_EndTrs(.t.)
    this.oParentObject.w_PESEOK = .T.
    this.w_TMPC = ah_Msgformat("Inserimento stati SOS Ver. 1.8 avvenuto correttamente.")
    this.oParentObject.w_PMSG = ah_Msgformat("Conversione tabella STATISOS (elenco stati SOStitutiva ver. 1.8) eseguita correttamente")
    this.w_MESS = this.w_TMPC
    this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_MESS)
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='RAP_PRES'
    this.cWorkTables[2]='DIPENDEN'
    this.cWorkTables[3]='STATISOS'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
