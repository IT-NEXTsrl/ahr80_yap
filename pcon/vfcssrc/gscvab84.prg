* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvab84                                                        *
*              Nuova gestione file allegati                                    *
*                                                                              *
*      Author: Zucchetti SpA                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_221]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-03-17                                                      *
* Last revis.: 2007-09-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvab84",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvab84 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TMPC = space(10)
  w_TEMP = space(10)
  w_APPO = space(1)
  w_AZIENDA = space(5)
  w_NOMEFILE = space(254)
  w_Descri = space(254)
  w_GFSERIAL = space(10)
  w_Note = space(50)
  w_DatReg = ctod("  /  /  ")
  w_CodPad = space(1)
  w_Chiave = space(20)
  w_CODART = space(20)
  w_SERDOC = space(10)
  w_SERPNT = space(10)
  w_CODCAT = space(5)
  w_CODMAR = space(5)
  w_CODFAM = space(5)
  w_CODGRU = space(5)
  w_PRINC = space(1)
  w_GF_PATH = space(254)
  w_ESTENS = space(10)
  w_TIPDEFEX = space(5)
  w_CLADEFEX = space(5)
  w_GENERA = .f.
  w_APPO = space(50)
  w_CODICE = space(41)
  w_TESTPRI = space(10)
  w_APPOCHI = space(40)
  w_ARCHIVIO = space(2)
  w_CRITPERI = space(1)
  w_TIPDEF = space(5)
  w_CLADEF = space(5)
  w_LIPATIMG = space(254)
  w_VOLUME = space(10)
  w_FL_TIP = space(1)
  w_FL_CLA = space(1)
  w_GESTIONE = space(50)
  w_TIPDEFEX = space(5)
  w_CLADEFEX = space(5)
  w_TEST = .f.
  w_ImgPat = space(10)
  w_curdir = space(50)
  w_Loop = 0
  w_LoopFile = 0
  w_oERRORLOG = .NULL.
  * --- WorkFile variables
  LIB_IMMA_idx=0
  GESTFILE_idx=0
  EXT_ENS_idx=0
  ART_ICOL_idx=0
  CLA_ALLE_idx=0
  TIP_ALLE_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Procedura per recuperare i file allegati creati con la gestione GESTFILE
    *     e quindi il caricamento degli stessi nella nuova gestione di Allegati
    * --- FIle di LOG
    this.w_AZIENDA = ALLTRIM(I_CODAZI)
    this.oParentObject.w_PESEOK = .T.
    if Not Ah_YesNo("Prima di proseguire assicurarsi di avere precedentemente eseguito la procedura di conversione 4.0-E6349. %0Si desidera continuare?")
      this.w_TMPC = Ah_MsgFormat("Procedura interrotta dall'utente")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      this.oParentObject.w_PESEOK = .F.
    endif
    if this.oParentObject.w_PESEOK
      * --- Prima di inserire gli indici, verifico che siano presente la tipologia e classe allegato 'ALTRO'
      * --- Read from CLA_ALLE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CLA_ALLE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CLA_ALLE_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "TACLADES"+;
          " from "+i_cTable+" CLA_ALLE where ";
              +"TACODICE = "+cp_ToStrODBC("ALTRO");
              +" and TACODCLA = "+cp_ToStrODBC("ALTRO");
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          TACLADES;
          from (i_cTable) where;
              TACODICE = "ALTRO";
              and TACODCLA = "ALTRO";
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        w_DSCR = NVL(cp_ToDate(_read_.TACLADES),cp_NullValue(_read_.TACLADES))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if i_rows <= 0
        * --- Try
        local bErr_04A94770
        bErr_04A94770=bTrsErr
        this.Try_04A94770()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          this.oParentObject.w_PESEOK = .F.
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_04A94770
        * --- End
      endif
    endif
    if this.oParentObject.w_PESEOK
      * --- Try
      local bErr_04A93F90
      bErr_04A93F90=bTrsErr
      this.Try_04A93F90()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        * --- Gestisce log errori
        this.oParentObject.w_PMSG = Message()
        this.oParentObject.w_PESEOK = .F.
        if this.w_NHF>=0
          this.w_TMPC = ah_Msgformat("ERRORE GENERICO - impossibile aggiornare tabella GESTFILE (gestione file allegati)")
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
      bTrsErr=bTrsErr or bErr_04A93F90
      * --- End
    endif
  endproc
  proc Try_04A94770()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Nel caso in cui non sia presente, provvedo ad inserirla
    * --- Insert into TIP_ALLE
    i_nConn=i_TableProp[this.TIP_ALLE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TIP_ALLE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TIP_ALLE_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"TACODICE"+",TADESCRI"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC("ALTRO"),'TIP_ALLE','TACODICE');
      +","+cp_NullLink(cp_ToStrODBC("ALTRO"),'TIP_ALLE','TADESCRI');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'TACODICE',"ALTRO",'TADESCRI',"ALTRO")
      insert into (i_cTable) (TACODICE,TADESCRI &i_ccchkf. );
         values (;
           "ALTRO";
           ,"ALTRO";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Insert into CLA_ALLE
    i_nConn=i_TableProp[this.CLA_ALLE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CLA_ALLE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CLA_ALLE_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"TACODICE"+",TACODCLA"+",TACLADES"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC("ALTRO"),'CLA_ALLE','TACODICE');
      +","+cp_NullLink(cp_ToStrODBC("ALTRO"),'CLA_ALLE','TACODCLA');
      +","+cp_NullLink(cp_ToStrODBC("ALTRO"),'CLA_ALLE','TACLADES');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'TACODICE',"ALTRO",'TACODCLA',"ALTRO",'TACLADES',"ALTRO")
      insert into (i_cTable) (TACODICE,TACODCLA,TACLADES &i_ccchkf. );
         values (;
           "ALTRO";
           ,"ALTRO";
           ,"ALTRO";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_04A93F90()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Cancellazione record da GESTFILE caricati da precedente esecuzione di questa procedura
    * --- Delete from GESTFILE
    i_nConn=i_TableProp[this.GESTFILE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.GESTFILE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      local i_cQueryTable,i_cWhere
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_cWhere=i_cTable+".GFSERIAL = "+i_cQueryTable+".GFSERIAL";
    
      do vq_exec with 'GSCVAB84',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
            +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    if this.w_NHF>=0
      this.w_TMPC = ah_msgformat("Inserimento allegati nella tabella GESTFILE avvenuto correttamente")
      this.oParentObject.w_pMSG = this.w_TMPC
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    this.w_oERRORLOG.PrintLog(this.oParentObject,"Errori riscontrati",.t., "Stampo situazione cartelle incongruenti?")     
    if used("__TMP__")
      select __TMP__
      use
    endif
    if used("CONTROLLO")
      select CONTROLLO
      use
    endif
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = ah_Msgformat("Conversione eseguita correttamente")
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stanzia l'oggetto per la gestione delle messaggistiche di errore
    this.w_oERRORLOG=createobject("AH_ErrorLog")
    this.w_TEST = .T.
    * --- Select from LIB_IMMA
    i_nConn=i_TableProp[this.LIB_IMMA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.LIB_IMMA_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" LIB_IMMA ";
          +" where LICODICE = 'GESTFILE'";
           ,"_Curs_LIB_IMMA")
    else
      select * from (i_cTable);
       where LICODICE = "GESTFILE";
        into cursor _Curs_LIB_IMMA
    endif
    if used('_Curs_LIB_IMMA')
      select _Curs_LIB_IMMA
      locate for 1=1
      do while not(eof())
      this.w_ARCHIVIO = Nvl(_Curs_LIB_IMMA.LITIPARC,"  ")
      this.w_CRITPERI = Nvl(_Curs_LIB_IMMA.LIDATRIF," ")
      this.w_TIPDEF = Nvl(_Curs_LIB_IMMA.LITIPALL,Space(5))
      this.w_CLADEF = Nvl(_Curs_LIB_IMMA.LICLAALL,Space(5))
      this.w_LIPATIMG = Nvl(_Curs_LIB_IMMA.LIPATIMG,"")
      this.w_VOLUME = Nvl(_Curs_LIB_IMMA.LIVOLUME,Space(10) )
      this.w_FL_TIP = Nvl(_Curs_LIB_IMMA.LIFL_TIP, " ")
      this.w_FL_CLA = Nvl(_Curs_LIB_IMMA.LIFL_CLA, " ")
      this.w_LIPATIMG = alltrim(this.w_LIPATIMG) + iif(right(alltrim(this.w_LIPATIMG),1) <>"\" , "\" , "" )
      if (.not.directory(alltrim(this.w_LIPATIMG)))
        if this.w_TEST
          this.w_oERRORLOG.AddMsgLog("Questa stampa indica le cartelle di memorizzazione immagini definite nelle librerie immagini per GESTFILE che non esistono")     
          this.w_TEST = .F.
        endif
        this.w_oERRORLOG.AddMsgLogNoTranslate(Alltrim(this.w_LIPATIMG))     
      else
        * --- All'interno della cartella definita nelle Librerie Immagini per ogni categoria 
        *     esistono le cartelle dei periodi che contengono i file.
        *     Devo quindi eseguire una scansione di queste cartelle
        this.w_curdir = sys(5) + sys(2003)
        * --- Posizionamento nella cartella contenente i file da visualizzare
        cd (this.w_LIPATIMG)
        * --- Si inseriscono, in un array, tutte le cartelle presenti nella cartelle indicata nelle librerie immagini
        w_NumDir = ADIR(w_ARRAYDIR,"","D")
        if w_NumDir >0
          cd (this.w_curdir)
          this.w_Loop = 1
          * --- Eseguo un ciclo nelle cartelle dei periodi per prendere i file
          do while this.w_Loop <= w_NumDir
            * --- Prendo in considerazione solo le cartelle che sono identificate dal nome dell'azienda.
            *     Effettuo questa operazione per evitare di caricare immagini collegate 
            *     ad aziende diverse.
            if w_ARRAYDIR(this.w_Loop, 1) <> "." And w_ARRAYDIR(this.w_Loop, 1) <> ".." And LEFT(ALLTRIM(w_ARRAYDIR(this.w_Loop,1)),LEN(this.w_AZIENDA))=this.w_AZIENDA
              ah_Msg(Alltrim(w_ARRAYDIR(this.w_Loop,1)),.T.)
              * --- Costruisco la Path delle cartelle dei periodi
              this.w_ImgPat = Alltrim(this.w_LIPATIMG)+Alltrim(w_ARRAYDIR(this.w_Loop, 1))
              * --- Costruisco un oggetto che contiene i file delle cartelle di memorizzazione allegati
              *     Visto che la ADIR crea un Array (limitato a 65000 elementi) in alcuni casi potrebbe creare errori
              *     Diciamo con una cartella contenente 13000 files
               
 fso = CreateObject("Scripting.FileSystemObject") 
 folder = fso.GetFolder(this.w_ImgPat)
              cd (this.w_ImgPat)
              For Each myFile in folder.Files
              ah_Msg(Alltrim(MyFile.Name),.T.)
              this.Pag3()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              EndFor
              fso = null
            endif
            this.w_Loop = this.w_Loop + 1
          enddo
        endif
        cd (this.w_curdir)
      endif
        select _Curs_LIB_IMMA
        continue
      enddo
      use
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_GF_PATH = ""
    this.w_PRINC = " "
    this.w_CODART = ""
    this.w_SERDOC = ""
    this.w_SERPNT = ""
    this.w_CODCAT = ""
    this.w_CODMAR = ""
    this.w_CODFAM = ""
    this.w_CODGRU = ""
    this.w_Chiave = ""
    this.w_CodPad = ""
    this.w_NOMEFILE = ""
    this.w_Descri = ""
    this.w_ESTENS = ""
    this.w_TIPDEFEX = ""
    this.w_CLADEFEX = ""
    this.w_GENERA = .F.
    this.w_CODICE = ""
    this.w_Note = ah_Msgformat("Inserito tramite procedura di conversione importando i file allegati prima del rilascio della nuova gestione allegati")
    * --- Prendo il nome del file
    this.w_NOMEFILE = Alltrim(MyFile.Name)
    * --- Calcolo l'estensione
    this.w_ESTENS = Substr(this.w_NOMEFILE, Rat(".",this.w_NOMEFILE)+1)
    this.w_CodPad = Upper(Left(Alltrim(this.w_NOMEFILE),1))
    this.w_NOMEFILE = this.w_CODPAD+Alltrim(SUBSTR(this.w_NOMEFILE,2))
    * --- Se la prima lettera del file � A-P-D
    *     inserisco questo come codice padre, altrimenti significa che � un file non corretto.
    if (this.w_CodPad="A" And this.w_Archivio="AR" ) OR (this.w_CodPad="P" And this.w_Archivio="PN") OR (this.w_CodPad="D" And this.w_Archivio="DO") OR (this.w_CodPad="O" And this.w_Archivio="CO") OR (this.w_CodPad="F" And this.w_Archivio="FA") OR (this.w_CodPad="M" And this.w_Archivio="MA") OR (this.w_CodPad="G" And this.w_Archivio="GM")
      if Not Empty(this.w_ESTENS)
        * --- Devo controllare se il database � SqlServer oppure Oracle.
        *     Se Sql effettuo una semplice Read sulla tabella Ext_ens, mnetre se il database
        *     � oracle devo lanciare una query sulla tabella Ext_ens perch� il codice dell'estensione
        *     potrebbe essere scritto minuscolo
        if upper(CP_DBTYPE)="ORACLE" Or upper(CP_DBTYPE)="DB2"
          cd (this.w_curdir)
          VQ_EXEC("..\PCON\EXE\QUERY\GSCVEBDP.VQR",this,"CONTROLLO")
          if RECCOUNT("CONTROLLO")>0
            SELECT CONTROLLO
            Go Top
            this.w_TIPDEF = CONTROLLO.EXTIPALL
            this.w_CLADEF = CONTROLLO.EXCLAALL
          else
            this.w_TIPDEF = Nvl(_Curs_LIB_IMMA.LITIPALL,"ALTRO")
            this.w_CLADEF = Nvl(_Curs_LIB_IMMA.LICLAALL,"ALTRO")
          endif
        else
          * --- Read from EXT_ENS
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.EXT_ENS_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.EXT_ENS_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "EXTIPALL,EXCLAALL"+;
              " from "+i_cTable+" EXT_ENS where ";
                  +"EXCODICE = "+cp_ToStrODBC(this.w_ESTENS);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              EXTIPALL,EXCLAALL;
              from (i_cTable) where;
                  EXCODICE = this.w_ESTENS;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_TIPDEFEX = NVL(cp_ToDate(_read_.EXTIPALL),cp_NullValue(_read_.EXTIPALL))
            this.w_CLADEFEX = NVL(cp_ToDate(_read_.EXCLAALL),cp_NullValue(_read_.EXCLAALL))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if i_Rows<>0
            if Not Empty(this.w_TIPDEFEX) And Not Empty(this.w_CLADEFEX)
              this.w_TIPDEF = this.w_TIPDEFEX
              this.w_CLADEF = this.w_CLADEFEX
            endif
          else
            this.w_TIPDEF = Nvl(_Curs_LIB_IMMA.LITIPALL,"ALTRO")
            this.w_CLADEF = Nvl(_Curs_LIB_IMMA.LICLAALL,"ALTRO")
          endif
        endif
      endif
      * --- Inserisco come data la data di creazione del file allegato
      this.w_DatReg = TTOD(myfile.DateLastModified)
      this.w_GF_PATH = Alltrim(this.w_ImgPat) +"\"+ Alltrim(this.w_NOMEFILE)
      * --- Estrapolo la  descrizione
      this.w_Descri = SubStr(this.w_NOMEFILE,At(".",this.w_NOMEFILE)+1)
      if At( "." , this.w_Descri ) <> 0
        * --- Se c'� il punto il file � comprensivo di descrizione e quindi la riporto nella gestione file
        *     e non � file principale
        this.w_Descri = Left( this.w_Descri, rat(".",this.w_Descri)-1)
        this.w_PRINC = " "
      else
        * --- Se non c'� il punto significa che il file non � comprensivo di descrizione
        *     e quindi � file principale
        this.w_Descri = ""
        this.w_PRINC = "S"
      endif
      this.w_TESTPRI = Space(10)
      * --- Dal nome file Estrapolo la chiave del padre
      this.w_Chiave = substr(this.w_NOMEFILE,2, At(".",this.w_NOMEFILE) -2)
      * --- Sostituisco a ritroso i caratteri che in fase di salvataggio sono stati sostituiti
      this.w_APPOCHI = Upper(this.w_Chiave)
      this.w_APPOCHI = Strtran(this.w_APPOCHI,"^^","^")
      this.w_APPOCHI = Strtran(this.w_APPOCHI,"^1","\")
      this.w_APPOCHI = Strtran(this.w_APPOCHI,"^2","/")
      this.w_APPOCHI = Strtran(this.w_APPOCHI,"^3",":")
      this.w_APPOCHI = Strtran(this.w_APPOCHI,"^4","*")
      this.w_APPOCHI = Strtran(this.w_APPOCHI,"^5","?")
      this.w_APPOCHI = Strtran(this.w_APPOCHI,"^6",'"')
      this.w_APPOCHI = Strtran(this.w_APPOCHI,"^7","<")
      this.w_APPOCHI = Strtran(this.w_APPOCHI,"^8",">")
      this.w_APPOCHI = Strtran(this.w_APPOCHI,"^9","|")
      this.w_APPOCHI = Strtran(this.w_APPOCHI,"^A","&")
      this.w_APPOCHI = Strtran(this.w_APPOCHI,"^B",".")
      this.w_Chiave = this.w_APPOCHI
      do case
        case this.w_CodPad="A" 
          if upper(CP_DBTYPE)="ORACLE" Or upper(CP_DBTYPE)="DB2"
            this.w_APPO = Upper(this.w_Chiave)
            * --- Effettuo questa operazione per ovviare al problema che siano presenti codici
            *     non in maiuscolo.
            cd (this.w_curdir)
            VQ_EXEC("..\PCON\EXE\QUERY\GSCVABDP.VQR",this,"CONTROLLO")
            if RECCOUNT("CONTROLLO")>0
              SELECT CONTROLLO
              Go Top
              this.w_CODART = Controllo.Arcodart
            else
              this.w_CODART = this.w_APPO
            endif
          else
            this.w_APPO = this.w_Chiave
            * --- Read from ART_ICOL
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.ART_ICOL_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "*"+;
                " from "+i_cTable+" ART_ICOL where ";
                    +"ARCODART = "+cp_ToStrODBC(this.w_APPO);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                *;
                from (i_cTable) where;
                    ARCODART = this.w_APPO;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if i_Rows<>0
              * --- La lettura � andata a buon fine e quindi considero l'immagine collegata ad
              *     un articolo.
              this.w_CODART = this.w_Chiave
            endif
          endif
          if Empty(this.w_CODART) And this.w_NHF >= 0
            * --- L'articolo non esiste
            this.w_TMPC = ah_Msgformat("Immagine non inserita: %1 perch� associata ad un articolo inesistente", Alltrim(this.w_NOMEFILE))
            this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
          endif
          this.w_GENERA = Not Empty(this.w_CODART)
          if this.w_PRINC = "S"
            * --- Visto che il metodo per identificare il principale non � preciso devo controllare 
            *     che non ne esista gi� uno per la chiave (articolo, documento, primanota)
            * --- Read from GESTFILE
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.GESTFILE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.GESTFILE_idx,2])
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "GFSERIAL"+;
                " from "+i_cTable+" GESTFILE where ";
                    +"GFCODPAD = "+cp_ToStrODBC(this.w_CodPad);
                    +" and GFCODART = "+cp_ToStrODBC(this.w_CODART);
                    +" and GFFLPRIN = "+cp_ToStrODBC("S");
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                GFSERIAL;
                from (i_cTable) where;
                    GFCODPAD = this.w_CodPad;
                    and GFCODART = this.w_CODART;
                    and GFFLPRIN = "S";
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_TESTPRI = NVL(cp_ToDate(_read_.GFSERIAL),cp_NullValue(_read_.GFSERIAL))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
        case this.w_CodPad="P" 
          this.w_SERPNT = this.w_Chiave
          this.w_GENERA = .T.
          if this.w_PRINC = "S"
            * --- Visto che il metodo per identificare il principale non � preciso devo controllare 
            *     che non ne esista gi� uno per la chiave (articolo, documento, primanota)
            * --- Read from GESTFILE
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.GESTFILE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.GESTFILE_idx,2])
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "GFSERIAL"+;
                " from "+i_cTable+" GESTFILE where ";
                    +"GFCODPAD = "+cp_ToStrODBC(this.w_CodPad);
                    +" and GFSERPNT = "+cp_ToStrODBC(this.w_SERPNT);
                    +" and GFFLPRIN = "+cp_ToStrODBC("S");
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                GFSERIAL;
                from (i_cTable) where;
                    GFCODPAD = this.w_CodPad;
                    and GFSERPNT = this.w_SERPNT;
                    and GFFLPRIN = "S";
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_TESTPRI = NVL(cp_ToDate(_read_.GFSERIAL),cp_NullValue(_read_.GFSERIAL))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
        case this.w_CodPad="D" 
          this.w_SERDOC = this.w_Chiave
          this.w_GENERA = .T.
          if this.w_PRINC = "S"
            * --- Visto che il metodo per identificare il principale non � preciso devo controllare 
            *     che non ne esista gi� uno per la chiave (articolo, documento, primanota)
            * --- Read from GESTFILE
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.GESTFILE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.GESTFILE_idx,2])
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "GFSERIAL"+;
                " from "+i_cTable+" GESTFILE where ";
                    +"GFCODPAD = "+cp_ToStrODBC(this.w_CodPad);
                    +" and GFSERDOC = "+cp_ToStrODBC(this.w_SERDOC);
                    +" and GFFLPRIN = "+cp_ToStrODBC("S");
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                GFSERIAL;
                from (i_cTable) where;
                    GFCODPAD = this.w_CodPad;
                    and GFSERDOC = this.w_SERDOC;
                    and GFFLPRIN = "S";
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_TESTPRI = NVL(cp_ToDate(_read_.GFSERIAL),cp_NullValue(_read_.GFSERIAL))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
        case this.w_CodPad="O" 
          this.w_CODCAT = Upper(this.w_Chiave)
          this.w_GENERA = .T.
          if upper(CP_DBTYPE)="ORACLE" Or upper(CP_DBTYPE)="DB2"
            * --- Effettuo questa operazione per ovviare al problema che siano presenti codici
            *     non in maiuscolo.
            cd (this.w_curdir)
            VQ_EXEC("..\PCON\EXE\QUERY\GSCVOBDP.VQR",this,"CONTROLLO")
            if RECCOUNT("CONTROLLO")>0
              SELECT CONTROLLO
              Go Top
              this.w_CODCAT = Controllo.Omcodice
            endif
          endif
          if this.w_PRINC = "S"
            * --- Visto che il metodo per identificare il principale non � preciso devo controllare 
            *     che non ne esista gi� uno per la chiave (articolo, documento, primanota)
            * --- Read from GESTFILE
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.GESTFILE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.GESTFILE_idx,2])
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "GFSERIAL"+;
                " from "+i_cTable+" GESTFILE where ";
                    +"GFCODPAD = "+cp_ToStrODBC(this.w_CodPad);
                    +" and GFCODCAT = "+cp_ToStrODBC(this.w_CODCAT);
                    +" and GFFLPRIN = "+cp_ToStrODBC("S");
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                GFSERIAL;
                from (i_cTable) where;
                    GFCODPAD = this.w_CodPad;
                    and GFCODCAT = this.w_CODCAT;
                    and GFFLPRIN = "S";
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_TESTPRI = NVL(cp_ToDate(_read_.GFSERIAL),cp_NullValue(_read_.GFSERIAL))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
        case this.w_CodPad="F" 
          this.w_CODFAM = Upper(this.w_Chiave)
          this.w_GENERA = .T.
          if upper(CP_DBTYPE)="ORACLE" Or upper(CP_DBTYPE)="DB2"
            * --- Effettuo questa operazione per ovviare al problema che siano presenti codici
            *     non in maiuscolo.
            cd (this.w_curdir)
            VQ_EXEC("..\PCON\EXE\QUERY\GSCVFBDP.VQR",this,"CONTROLLO")
            if RECCOUNT("CONTROLLO")>0
              SELECT CONTROLLO
              Go Top
              this.w_CODFAM = Controllo.Facodice
            endif
          endif
          if this.w_PRINC = "S"
            * --- Visto che il metodo per identificare il principale non � preciso devo controllare 
            *     che non ne esista gi� uno per la chiave (articolo, documento, primanota)
            * --- Read from GESTFILE
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.GESTFILE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.GESTFILE_idx,2])
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "GFSERIAL"+;
                " from "+i_cTable+" GESTFILE where ";
                    +"GFCODPAD = "+cp_ToStrODBC(this.w_CodPad);
                    +" and GFCODFAM = "+cp_ToStrODBC(this.w_CODFAM);
                    +" and GFFLPRIN = "+cp_ToStrODBC("S");
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                GFSERIAL;
                from (i_cTable) where;
                    GFCODPAD = this.w_CodPad;
                    and GFCODFAM = this.w_CODFAM;
                    and GFFLPRIN = "S";
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_TESTPRI = NVL(cp_ToDate(_read_.GFSERIAL),cp_NullValue(_read_.GFSERIAL))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
        case this.w_CodPad="M" 
          this.w_CODMAR = Upper(this.w_Chiave)
          this.w_GENERA = .T.
          if upper(CP_DBTYPE)="ORACLE" Or upper(CP_DBTYPE)="DB2"
            * --- Effettuo questa operazione per ovviare al problema che siano presenti codici
            *     non in maiuscolo.
            cd (this.w_curdir)
            VQ_EXEC("..\PCON\EXE\QUERY\GSCVMBDP.VQR",this,"CONTROLLO")
            if RECCOUNT("CONTROLLO")>0
              SELECT CONTROLLO
              Go Top
              this.w_CODMAR = Controllo.Macodice
            endif
          endif
          if this.w_PRINC = "S"
            * --- Visto che il metodo per identificare il principale non � preciso devo controllare 
            *     che non ne esista gi� uno per la chiave (articolo, documento, primanota)
            * --- Read from GESTFILE
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.GESTFILE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.GESTFILE_idx,2])
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "GFSERIAL"+;
                " from "+i_cTable+" GESTFILE where ";
                    +"GFCODPAD = "+cp_ToStrODBC(this.w_CodPad);
                    +" and GFCODMAR = "+cp_ToStrODBC(this.w_CODMAR);
                    +" and GFFLPRIN = "+cp_ToStrODBC("S");
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                GFSERIAL;
                from (i_cTable) where;
                    GFCODPAD = this.w_CodPad;
                    and GFCODMAR = this.w_CODMAR;
                    and GFFLPRIN = "S";
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_TESTPRI = NVL(cp_ToDate(_read_.GFSERIAL),cp_NullValue(_read_.GFSERIAL))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
        case this.w_CodPad="G" 
          this.w_CODGRU = Upper(this.w_Chiave)
          this.w_GENERA = .T.
          if upper(CP_DBTYPE)="ORACLE" Or upper(CP_DBTYPE)="DB2"
            * --- Effettuo questa operazione per ovviare al problema che siano presenti codici
            *     non in maiuscolo.
            cd (this.w_curdir)
            VQ_EXEC("..\PCON\EXE\QUERY\GSCVGBDP.VQR",this,"CONTROLLO")
            if RECCOUNT("CONTROLLO")>0
              SELECT CONTROLLO
              Go Top
              this.w_CODGRU = Controllo.Gmcodice
            endif
          endif
          if this.w_PRINC = "S"
            * --- Visto che il metodo per identificare il principale non � preciso devo controllare 
            *     che non ne esista gi� uno per la chiave (articolo, documento, primanota)
            * --- Read from GESTFILE
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.GESTFILE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.GESTFILE_idx,2])
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "GFSERIAL"+;
                " from "+i_cTable+" GESTFILE where ";
                    +"GFCODPAD = "+cp_ToStrODBC(this.w_CodPad);
                    +" and GFCODGRU = "+cp_ToStrODBC(this.w_CODGRU);
                    +" and GFFLPRIN = "+cp_ToStrODBC("S");
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                GFSERIAL;
                from (i_cTable) where;
                    GFCODPAD = this.w_CodPad;
                    and GFCODGRU = this.w_CODGRU;
                    and GFFLPRIN = "S";
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_TESTPRI = NVL(cp_ToDate(_read_.GFSERIAL),cp_NullValue(_read_.GFSERIAL))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
      endcase
      if Not Empty(this.w_TESTPRI)
        this.w_PRINC = " "
      endif
      if Not Empty(this.w_CodPad) And Not Empty(this.w_ImgPat) And this.w_Genera
        this.w_GFSERIAL = Space(10)
        i_Conn=i_TableProp[this.GESTFILE_IDX, 3]
        cp_NextTableProg(this, i_Conn, "GESTSER", "I_codazi,w_GFSERIAL")
        * --- Provo ad inserire l'immagine, se non esiste pi� l'articolo/documento/primanota vado avanti
        *     Prima di inserire taglio la descrizione a 40.
        this.w_Descri = Left(this.w_DESCRI,40)
        * --- Try
        local bErr_04A9AAA0
        bErr_04A9AAA0=bTrsErr
        this.Try_04A9AAA0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          if this.w_NHF>=0
            do case
              case this.w_CodPad="A"
                this.w_TMPC = ah_Msgformat("Immagine non inserita: %1 perch� associata ad un articolo inesistente", Alltrim(this.w_NOMEFILE))
              case this.w_CodPad="P"
                this.w_TMPC = ah_Msgformat("Immagine non inserita: %1 perch� associata ad una registrazione di primanota inesistente", Alltrim(this.w_NOMEFILE))
              case this.w_CodPad="D"
                this.w_TMPC = ah_Msgformat("Immagine non inserita: %1 perch� associata ad un documento inesistente", Alltrim(this.w_NOMEFILE) )
              case this.w_CodPad="O"
                this.w_TMPC = ah_Msgformat("Immagine non inserita: %1 perch� associata ad una categoria omogenea inesistente", Alltrim(this.w_NOMEFILE) )
              case this.w_CodPad="F"
                this.w_TMPC = ah_Msgformat("Immagine non inserita: %1 perch� associata ad una famiglia articolo inesistente", Alltrim(this.w_NOMEFILE))
              case this.w_CodPad="M"
                this.w_TMPC = ah_Msgformat("Immagine non inserita: %1 perch� associata ad un marchio inesistente", Alltrim(this.w_NOMEFILE))
              case this.w_CodPad="G"
                this.w_TMPC = ah_Msgformat("Immagine non inserita: %1 perch� associata ad un gruppo merceologico inesistente", Alltrim(this.w_NOMEFILE))
            endcase
            this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
          endif
        endif
        bTrsErr=bTrsErr or bErr_04A9AAA0
        * --- End
      endif
    endif
  endproc
  proc Try_04A9AAA0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into GESTFILE
    i_nConn=i_TableProp[this.GESTFILE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.GESTFILE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.GESTFILE_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"GFSERIAL"+",GFTIPALL"+",GFCLAALL"+",GFDESCRI"+",GF__NOTE"+",GFDATREG"+",GFDATINI"+",GFMODALL"+",GF__PATH"+",GFCODPAD"+",GFSERDOC"+",GFSERPNT"+",GFCODART"+",GF__FILE"+",GFFLPRIN"+",GFPUBWEB"+",GFCODCAT"+",GFCODMAR"+",GFCODFAM"+",GFCODGRU"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_GFSERIAL),'GESTFILE','GFSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TIPDEF),'GESTFILE','GFTIPALL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CLADEF),'GESTFILE','GFCLAALL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DESCRI),'GESTFILE','GFDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NOTE),'GESTFILE','GF__NOTE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DATREG),'GESTFILE','GFDATREG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DATREG),'GESTFILE','GFDATINI');
      +","+cp_NullLink(cp_ToStrODBC("F"),'GESTFILE','GFMODALL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_GF_PATH),'GESTFILE','GF__PATH');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CodPad),'GESTFILE','GFCODPAD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SERDOC),'GESTFILE','GFSERDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SERPNT),'GESTFILE','GFSERPNT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODART),'GESTFILE','GFCODART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NOMEFILE),'GESTFILE','GF__FILE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PRINC),'GESTFILE','GFFLPRIN');
      +","+cp_NullLink(cp_ToStrODBC("S"),'GESTFILE','GFPUBWEB');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODCAT),'GESTFILE','GFCODCAT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODMAR),'GESTFILE','GFCODMAR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODFAM),'GESTFILE','GFCODFAM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODGRU),'GESTFILE','GFCODGRU');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'GFSERIAL',this.w_GFSERIAL,'GFTIPALL',this.w_TIPDEF,'GFCLAALL',this.w_CLADEF,'GFDESCRI',this.w_DESCRI,'GF__NOTE',this.w_NOTE,'GFDATREG',this.w_DATREG,'GFDATINI',this.w_DATREG,'GFMODALL',"F",'GF__PATH',this.w_GF_PATH,'GFCODPAD',this.w_CodPad,'GFSERDOC',this.w_SERDOC,'GFSERPNT',this.w_SERPNT)
      insert into (i_cTable) (GFSERIAL,GFTIPALL,GFCLAALL,GFDESCRI,GF__NOTE,GFDATREG,GFDATINI,GFMODALL,GF__PATH,GFCODPAD,GFSERDOC,GFSERPNT,GFCODART,GF__FILE,GFFLPRIN,GFPUBWEB,GFCODCAT,GFCODMAR,GFCODFAM,GFCODGRU &i_ccchkf. );
         values (;
           this.w_GFSERIAL;
           ,this.w_TIPDEF;
           ,this.w_CLADEF;
           ,this.w_DESCRI;
           ,this.w_NOTE;
           ,this.w_DATREG;
           ,this.w_DATREG;
           ,"F";
           ,this.w_GF_PATH;
           ,this.w_CodPad;
           ,this.w_SERDOC;
           ,this.w_SERPNT;
           ,this.w_CODART;
           ,this.w_NOMEFILE;
           ,this.w_PRINC;
           ,"S";
           ,this.w_CODCAT;
           ,this.w_CODMAR;
           ,this.w_CODFAM;
           ,this.w_CODGRU;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,6)]
    this.cWorkTables[1]='LIB_IMMA'
    this.cWorkTables[2]='GESTFILE'
    this.cWorkTables[3]='EXT_ENS'
    this.cWorkTables[4]='ART_ICOL'
    this.cWorkTables[5]='CLA_ALLE'
    this.cWorkTables[6]='TIP_ALLE'
    return(this.OpenAllTables(6))

  proc CloseCursors()
    if used('_Curs_LIB_IMMA')
      use in _Curs_LIB_IMMA
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
