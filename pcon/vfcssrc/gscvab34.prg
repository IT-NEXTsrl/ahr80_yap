* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvab34                                                        *
*              Aggiorna note saldaconto                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_23]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-08-31                                                      *
* Last revis.: 2004-08-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvab34",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvab34 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_DESCRI = space(35)
  * --- WorkFile variables
  SALMDACO_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- AGGIORNA NOTE SALDACONTO
    * --- FIle di LOG
    * --- Try
    local bErr_035D1980
    bErr_035D1980=bTrsErr
    this.Try_035D1980()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE - impossibile aggiornare SCDESSUP su tabella SALMDACO" )
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_035D1980
    * --- End
  endproc
  proc Try_035D1980()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Aggiornamento  SCDESSUP
    * --- Select from SALMDACO
    i_nConn=i_TableProp[this.SALMDACO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALMDACO_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" SALMDACO ";
           ,"_Curs_SALMDACO")
    else
      select * from (i_cTable);
        into cursor _Curs_SALMDACO
    endif
    if used('_Curs_SALMDACO')
      select _Curs_SALMDACO
      locate for 1=1
      do while not(eof())
      this.w_DESCRI = _Curs_SALMDACO.SCDESCRI
      * --- Write into SALMDACO
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.SALMDACO_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SALMDACO_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALMDACO_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"SCDESSUP ="+cp_NullLink(cp_ToStrODBC(this.w_DESCRI),'SALMDACO','SCDESSUP');
            +i_ccchkf ;
        +" where ";
            +"SCSERIAL = "+cp_ToStrODBC(_Curs_SALMDACO.SCSERIAL);
               )
      else
        update (i_cTable) set;
            SCDESSUP = this.w_DESCRI;
            &i_ccchkf. ;
         where;
            SCSERIAL = _Curs_SALMDACO.SCSERIAL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
        select _Curs_SALMDACO
        continue
      enddo
      use
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Aggiornamento campo SCDESSUP su tabella SALMDACO eseguito con successo")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = this.oParentObject.w_PMSG + this.w_TMPC 
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='SALMDACO'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_SALMDACO')
      use in _Curs_SALMDACO
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
