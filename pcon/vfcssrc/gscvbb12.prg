* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvbb12                                                        *
*              Aggiorna calcola sconti su omaggi                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_23]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-03-31                                                      *
* Last revis.: 2006-12-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvbb12",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvbb12 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_MESS = space(100)
  w_DATA = ctod("  /  /  ")
  w_NUMAGG = 0
  w_RECNUM = 0
  w_RECNUM1 = 0
  * --- WorkFile variables
  DOC_MAST_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Procedura per valorizzare il campo MVFLSCOM (Calcola Sconti su Omaggio) 
    *     nella tabella DOC_MAST
    * --- FIle di LOG
    this.w_DATA = CTOD("  -  -    ")
    * --- Try
    local bErr_038956D8
    bErr_038956D8=bTrsErr
    this.Try_038956D8()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE GENERICO - impossibile aggiornare i documenti")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_038956D8
    * --- End
    if used("CESP")
       
 select CESP 
 use
    endif
  endproc
  proc Try_038956D8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if g_FLSCOM = "S"
      * --- Solo nei documenti creati con flag attivo Calcola Sconti su Omaggi 
      *     valorizzo il campo MVFLSCOM a 'S'. Negli altri documenti assume il valore di default 'N'
      * --- Write into DOC_MAST
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DOC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="MVSERIAL"
        do vq_exec with 'GSCVBB12',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="DOC_MAST.MVSERIAL = _t2.MVSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MVFLSCOM ="+cp_NullLink(cp_ToStrODBC("S"),'DOC_MAST','MVFLSCOM');
            +i_ccchkf;
            +" from "+i_cTable+" DOC_MAST, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="DOC_MAST.MVSERIAL = _t2.MVSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_MAST, "+i_cQueryTable+" _t2 set ";
        +"DOC_MAST.MVFLSCOM ="+cp_NullLink(cp_ToStrODBC("S"),'DOC_MAST','MVFLSCOM');
            +Iif(Empty(i_ccchkf),"",",DOC_MAST.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="DOC_MAST.MVSERIAL = t2.MVSERIAL";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_MAST set (";
            +"MVFLSCOM";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +cp_NullLink(cp_ToStrODBC("S"),'DOC_MAST','MVFLSCOM')+"";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="DOC_MAST.MVSERIAL = _t2.MVSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_MAST set ";
        +"MVFLSCOM ="+cp_NullLink(cp_ToStrODBC("S"),'DOC_MAST','MVFLSCOM');
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MVFLSCOM ="+cp_NullLink(cp_ToStrODBC("S"),'DOC_MAST','MVFLSCOM');
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      this.w_RECNUM1 = i_Rows
    endif
    * --- Write into DOC_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="MVSERIAL"
      do vq_exec with '..\PCON\EXE\QUERY\GSCVBB12b',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="DOC_MAST.MVSERIAL = _t2.MVSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVCAUCON ="+cp_NullLink(cp_ToStrODBC(""),'DOC_MAST','MVCAUCON');
          +i_ccchkf;
          +" from "+i_cTable+" DOC_MAST, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="DOC_MAST.MVSERIAL = _t2.MVSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_MAST, "+i_cQueryTable+" _t2 set ";
      +"DOC_MAST.MVCAUCON ="+cp_NullLink(cp_ToStrODBC(""),'DOC_MAST','MVCAUCON');
          +Iif(Empty(i_ccchkf),"",",DOC_MAST.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="DOC_MAST.MVSERIAL = t2.MVSERIAL";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_MAST set (";
          +"MVCAUCON";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +cp_NullLink(cp_ToStrODBC(""),'DOC_MAST','MVCAUCON')+"";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="DOC_MAST.MVSERIAL = _t2.MVSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_MAST set ";
      +"MVCAUCON ="+cp_NullLink(cp_ToStrODBC(""),'DOC_MAST','MVCAUCON');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVCAUCON ="+cp_NullLink(cp_ToStrODBC(""),'DOC_MAST','MVCAUCON');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    this.w_RECNUM = i_Rows
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("- Aggiornamento tabella DOC_MAST (MVFLSCOM) eseguito con successo su %1 testate documenti%0Aggiornamento tabella DOC_MAST (MVCAUCON) eseguito con successo su %2 testate documenti%0", alltrim(Str( this.w_RECNUM1 )), alltrim(Str( this.w_RECNUM )) )
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='DOC_MAST'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
