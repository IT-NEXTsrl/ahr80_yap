* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcc15                                                        *
*              Aggiorna campo riferimenti P.N. incassi corrispettivi           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-03-18                                                      *
* Last revis.: 2012-06-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcc15",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcc15 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  * --- WorkFile variables
  CONTI_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- FIle di LOG
    * --- Try
    local bErr_033492C8
    bErr_033492C8=bTrsErr
    this.Try_033492C8()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = this.oParentObject.w_PMSG
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_033492C8
    * --- End
    * --- Chiude cursore
    if USED("__TMP__")
      SELECT __TMP__
      USE
    endif
  endproc
  proc Try_033492C8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    vq_exec("..\PCON\EXE\QUERY\GSCVCC15", this, "__TMP__")
    * --- begin transaction
    cp_BeginTrs()
    * --- Write into CONTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.CONTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"ANEVEECC ="+cp_NullLink(cp_ToStrODBC("0"),'CONTI','ANEVEECC');
          +i_ccchkf ;
      +" where ";
          +"ANEVEECC <> "+cp_ToStrODBC("1");
          +" and ANEVEECC <> "+cp_ToStrODBC("0");
          +" and ANTIPCON = "+cp_ToStrODBC("F");
             )
    else
      update (i_cTable) set;
          ANEVEECC = "0";
          &i_ccchkf. ;
       where;
          ANEVEECC <> "1";
          and ANEVEECC <> "0";
          and ANTIPCON = "F";

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    if RECCOUNT("__TMP__")>0
      if ah_YesNo("Si vuole stampare l'elenco dei fornitori modificati ?")
        SELECT __TMP__
        CP_CHPRN("..\PCON\EXE\QUERY\GSCVCC15.FRX", " ", this)
      endif
    endif
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.w_TMPC = AH_MsgFormat("Aggiornamento del campo ANEVEECC eseguito correttamente")
    this.oParentObject.w_PMSG = this.w_TMPC
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='CONTI'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
