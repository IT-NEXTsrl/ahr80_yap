* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcb15                                                        *
*              Valorizzazione nuovi campi traduzione da vecchie tabelle traduzione*
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-08-27                                                      *
* Last revis.: 2008-09-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_cTable,w_cField,w_cK,w_cTable1,w_cField1,w_cK1,w_LUCODISO
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcb15",oParentObject,m.w_cTable,m.w_cField,m.w_cK,m.w_cTable1,m.w_cField1,m.w_cK1,m.w_LUCODISO)
return(i_retval)

define class tgscvcb15 as StdBatch
  * --- Local variables
  w_cTable = space(30)
  w_cField = space(30)
  w_cK = space(30)
  w_cTable1 = space(30)
  w_cField1 = space(30)
  w_cK1 = space(30)
  w_LUCODISO = space(3)
  w_xLang = space(3)
  w_xFieldLang = space(15)
  w_IsFieldTranslate = .f.
  * --- WorkFile variables
  LINGUE_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Nome Tabella primaria
    * --- Campo da Restituire
    * --- Chiave chiave tabella primaria
    * --- Nome Tabella traduzione
    * --- Campo da Restituire della tabella traduzione
    * --- chiave su tabella traduzione
    * --- Codice ISO Lingua
    this.w_xFieldLang = iif(LEFT(this.w_cField1,2)<>"LG", LEFT(this.w_cField1,2)+"CODLIN", "LGCODLIN")
    * --- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    this.w_IsFieldTranslate = i_dcx.IsFieldNameTranslate(this.w_cTable,this.w_cField)
    if this.w_IsFieldTranslate
      * --- Read from LINGUE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.LINGUE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.LINGUE_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "LUCODICE"+;
          " from "+i_cTable+" LINGUE where ";
              +"LUCODISO = "+cp_ToStrODBC(this.w_LUCODISO);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          LUCODICE;
          from (i_cTable) where;
              LUCODISO = this.w_LUCODISO;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_xLang = NVL(cp_ToDate(_read_.LUCODICE),cp_NullValue(_read_.LUCODICE))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      i_NT=cp_OpenTable(this.w_cTable)
      if i_NT<>0
        i_nConn = i_TableProp[i_NT,3]
        i_cPhTable = cp_SetAzi(i_TableProp[i_NT,2])
        if i_cPhTable <> this.w_cTable
          this.w_cTable1 = alltrim(i_CODAZI)+this.w_cTable1
        endif
        i_cSQL="update "+i_cPhTable+" set "+alltrim(this.w_cField)+"_"+alltrim(this.w_LUCODISO)+"=(select "+alltrim(this.w_cField1)+" from "+this.w_cTable1+" A where A."+alltrim(this.w_cK1)+"="+alltrim(i_cPhTable)+"."+alltrim(this.w_cK)+" and "+this.w_xFieldLang+"='"+this.w_xLang+"')"
        i_cSQL=i_cSQL+" where "+cp_SetSQLFunctions("[NOTEMPTYSTR("+alltrim(this.w_cField)+"_"+alltrim(this.w_LUCODISO)+")]", cp_GetDatabaseType(IIF(i_nConn=0,i_serverconn[1,2],i_nConn)))+"=0"
        i_cSQL=i_cSQL+" and exists(select 1 from "+this.w_cTable1+" A where A."+alltrim(this.w_cK1)+"="+alltrim(i_cPhTable)+"."+alltrim(this.w_cK)+" and "+this.w_xFieldLang+"='"+this.w_xLang+"')"
        if i_nConn<>0
          i_rows=cp_sqlexec(i_nConn, i_cSQL)
        endif
      endif
    endif
    i_retcode = 'stop'
    i_retval = i_Rows
    return
  endproc


  proc Init(oParentObject,w_cTable,w_cField,w_cK,w_cTable1,w_cField1,w_cK1,w_LUCODISO)
    this.w_cTable=w_cTable
    this.w_cField=w_cField
    this.w_cK=w_cK
    this.w_cTable1=w_cTable1
    this.w_cField1=w_cField1
    this.w_cK1=w_cK1
    this.w_LUCODISO=w_LUCODISO
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='LINGUE'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_cTable,w_cField,w_cK,w_cTable1,w_cField1,w_cK1,w_LUCODISO"
endproc
