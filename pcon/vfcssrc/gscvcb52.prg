* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcb52                                                        *
*              Valorizzazione tipologie uffici/sezioni pratica                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_11]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-05-27                                                      *
* Last revis.: 2009-06-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcb52",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcb52 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TMPC = space(10)
  w_TEMP = space(10)
  w_UFCODICE = space(10)
  w_UFDESCRI = space(60)
  w_UF_TIPOL = space(1)
  * --- WorkFile variables
  PRA_UFFI_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Valorizzazione tipologie uffici/sezioni pratica
    * --- Try
    local bErr_0360D640
    bErr_0360D640=bTrsErr
    this.Try_0360D640()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("Errore generico - impossibile eseguire aggiornamento richiesto")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_0360D640
    * --- End
  endproc
  proc Try_0360D640()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Select from PRA_UFFI
    i_nConn=i_TableProp[this.PRA_UFFI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRA_UFFI_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select UFCODICE,UFDESCRI,UF_TIPOL  from "+i_cTable+" PRA_UFFI ";
           ,"_Curs_PRA_UFFI")
    else
      select UFCODICE,UFDESCRI,UF_TIPOL from (i_cTable);
        into cursor _Curs_PRA_UFFI
    endif
    if used('_Curs_PRA_UFFI')
      select _Curs_PRA_UFFI
      locate for 1=1
      do while not(eof())
      this.w_UFCODICE = UFCODICE
      this.w_UFDESCRI = ALLTRIM(UPPER(UFDESCRI))
      this.w_UF_TIPOL = NVL(UF_TIPOL,"")
      if EMPTY(this.w_UF_TIPOL)
        do case
          case "PENALE"$this.w_UFDESCRI OR "ASSISE"$this.w_UFDESCRI
            * --- Write into PRA_UFFI
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.PRA_UFFI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PRA_UFFI_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.PRA_UFFI_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"UF_TIPOL ="+cp_NullLink(cp_ToStrODBC("P"),'PRA_UFFI','UF_TIPOL');
                  +i_ccchkf ;
              +" where ";
                  +"UFCODICE = "+cp_ToStrODBC(this.w_UFCODICE);
                     )
            else
              update (i_cTable) set;
                  UF_TIPOL = "P";
                  &i_ccchkf. ;
               where;
                  UFCODICE = this.w_UFCODICE;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          case "AGRARIA"$this.w_UFDESCRI OR "CIVILE"$this.w_UFDESCRI OR "FALLIMENTARE"$this.w_UFDESCRI OR "LAVORO"$this.w_UFDESCRI
            * --- Write into PRA_UFFI
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.PRA_UFFI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PRA_UFFI_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.PRA_UFFI_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"UF_TIPOL ="+cp_NullLink(cp_ToStrODBC("C"),'PRA_UFFI','UF_TIPOL');
                  +i_ccchkf ;
              +" where ";
                  +"UFCODICE = "+cp_ToStrODBC(this.w_UFCODICE);
                     )
            else
              update (i_cTable) set;
                  UF_TIPOL = "C";
                  &i_ccchkf. ;
               where;
                  UFCODICE = this.w_UFCODICE;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
        endcase
      endif
        select _Curs_PRA_UFFI
        continue
      enddo
      use
    endif
    this.w_TMPC = ah_Msgformat("Aggiornamento campo UFTIPOL eseguita con successo")
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = this.w_TMPC
    * --- commit
    cp_EndTrs(.t.)
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='PRA_UFFI'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_PRA_UFFI')
      use in _Curs_PRA_UFFI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
