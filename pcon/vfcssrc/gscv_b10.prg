* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscv_b10                                                        *
*              Costi standard (Euro-kit)                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_32]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-06-04                                                      *
* Last revis.: 2001-06-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscv_b10",oParentObject,m.w_NHF)
return(i_retval)

define class tgscv_b10 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_CODEUR = space(3)
  w_CODLIR = space(3)
  w_OBSO = ctod("  /  /  ")
  w_TmpN = 0
  w_TASSOEUR = 0
  w_TASSOLIT = 0
  w_DECEUR = 0
  w_TMPC = space(10)
  w_TEMP = space(10)
  w_SIMLIR = space(3)
  w_SIMEUR = space(3)
  w_oMess = .NULL.
  w_oPart = .NULL.
  * --- WorkFile variables
  AZIENDA_idx=0
  VALUTE_idx=0
  PAR_RIOR_idx=0
  PAR_RIMA_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- FIle di LOG
    * --- Visibile dal padre
    * --- Variabili locali
    * --- Data Obsolescenza
    this.w_OBSO = cp_CharToDate("01-01-2002")
    * --- Leggo il codice valuta Euro e Lire
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZVALEUR,AZVALLIR"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZVALEUR,AZVALLIR;
        from (i_cTable) where;
            AZCODAZI = i_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CODEUR = NVL(cp_ToDate(_read_.AZVALEUR),cp_NullValue(_read_.AZVALEUR))
      this.w_CODLIR = NVL(cp_ToDate(_read_.AZVALLIR),cp_NullValue(_read_.AZVALLIR))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Controllo 1 - Devono essere caricati i dati azienda
    if Empty ( this.w_CODLIR ) Or Empty ( this.w_CODEUR )
      this.oParentObject.w_PMSG = ah_Msgformat("ERRORE 001 - codice Euro o valuta nazionale non definiti in dati azienda")
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE 001 - codice Euro o valuta nazionale non definiti in dati azienda")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
      i_retcode = 'stop'
      return
    endif
    * --- Controllo 2 - L'esercizio corrente deve essere EURO
    if g_PERVAL <> g_CODEUR
      this.oParentObject.w_PMSG = ah_Msgformat("ERRORE 002 - la valuta di conto dell'esercizio corrente deve essere Euro")
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE 002 - la valuta di conto dell'esercizio corrente deve essere Euro")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
      i_retcode = 'stop'
      return
    endif
    * --- Leggo i tassi fissi di Euro (1) e Lire (1936,27)
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VACAOVAL,VADECUNI,VASIMVAL"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(this.w_CODEUR);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VACAOVAL,VADECUNI,VASIMVAL;
        from (i_cTable) where;
            VACODVAL = this.w_CODEUR;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_TASSOEUR = NVL(cp_ToDate(_read_.VACAOVAL),cp_NullValue(_read_.VACAOVAL))
      this.w_DECEUR = NVL(cp_ToDate(_read_.VADECUNI),cp_NullValue(_read_.VADECUNI))
      this.w_SIMEUR = NVL(cp_ToDate(_read_.VASIMVAL),cp_NullValue(_read_.VASIMVAL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VACAOVAL,VASIMVAL"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(this.w_CODLIR);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VACAOVAL,VASIMVAL;
        from (i_cTable) where;
            VACODVAL = this.w_CODLIR;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_TASSOLIT = NVL(cp_ToDate(_read_.VACAOVAL),cp_NullValue(_read_.VACAOVAL))
      this.w_SIMLIR = NVL(cp_ToDate(_read_.VASIMVAL),cp_NullValue(_read_.VASIMVAL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Oggetto per messaggi incrementali
    this.w_oMess=createobject("Ah_Message")
    * --- Try
    local bErr_035D0F00
    bErr_035D0F00=bTrsErr
    this.Try_035D0F00()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      this.w_TMPC = ah_Msgformat("ERRORE GENERICO - operazione sospesa")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    bTrsErr=bTrsErr or bErr_035D0F00
    * --- End
  endproc
  proc Try_035D0F00()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Select from PAR_RIOR
    i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" PAR_RIOR ";
           ,"_Curs_PAR_RIOR")
    else
      select * from (i_cTable);
        into cursor _Curs_PAR_RIOR
    endif
    if used('_Curs_PAR_RIOR')
      select _Curs_PAR_RIOR
      locate for 1=1
      do while not(eof())
      this.w_TmpN = Nvl ( _Curs_PAR_RIOR.PRCOSSTA , 0 )
      if this.w_TMPN <>0
        this.w_oPart = this.w_oMess.AddMsgPartNL("Articolo: %1%0%2 - Da %3 a %4")
        this.w_oPart.AddParam(_Curs_PAR_RIOR.PRCODART)     
        this.w_oPart.AddParam(space(32))     
        this.w_oPart.AddParam(this.w_CODLIR + TRAN(this.w_TMPN,"99,999,999,999"))     
        this.w_TmpN = cp_round( this.w_TMPN / g_CAOEUR, this.w_DECEUR)
        * --- Write into PAR_RIOR
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_RIOR_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PRCOSSTA ="+cp_NullLink(cp_ToStrODBC(this.w_TmpN),'PAR_RIOR','PRCOSSTA');
              +i_ccchkf ;
          +" where ";
              +"PRCODART = "+cp_ToStrODBC(_Curs_PAR_RIOR.PRCODART);
                 )
        else
          update (i_cTable) set;
              PRCOSSTA = this.w_TmpN;
              &i_ccchkf. ;
           where;
              PRCODART = _Curs_PAR_RIOR.PRCODART;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        this.w_oPart.AddParam(this.w_SIMEUR + TRAN(this.w_TMPN,"99,999,999.99"))     
        this.w_TMPC = this.w_oMess.ComposeMessage()
        if this.w_NHF>=0
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
        select _Curs_PAR_RIOR
        continue
      enddo
      use
    endif
    * --- Select from PAR_RIMA
    i_nConn=i_TableProp[this.PAR_RIMA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIMA_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" PAR_RIMA ";
           ,"_Curs_PAR_RIMA")
    else
      select * from (i_cTable);
        into cursor _Curs_PAR_RIMA
    endif
    if used('_Curs_PAR_RIMA')
      select _Curs_PAR_RIMA
      locate for 1=1
      do while not(eof())
      this.w_TmpN = Nvl ( _Curs_PAR_RIMA.PRCOSSTA , 0 )
      if this.w_TMPN <>0
        this.w_oPart = this.w_oMess.AddMsgPartNL("Articolo: %1 - mag. %2%0%3 - Da %4 a %5")
        this.w_oPart.AddParam(_Curs_PAR_RIMA.PRCODART)     
        this.w_oPart.AddParam(_Curs_PAR_RIMA.PRCODMAG)     
        this.w_oPart.AddParam(space(32))     
        this.w_oPart.AddParam(this.w_CODLIR + TRAN(this.w_TMPN,"99,999,999,999"))     
        this.w_TmpN = cp_round( this.w_TMPN / g_CAOEUR, this.w_DECEUR)
        * --- Write into PAR_RIMA
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.PAR_RIMA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIMA_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_RIMA_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PRCOSSTA ="+cp_NullLink(cp_ToStrODBC(this.w_TmpN),'PAR_RIMA','PRCOSSTA');
              +i_ccchkf ;
          +" where ";
              +"PRCODART = "+cp_ToStrODBC(_Curs_PAR_RIMA.PRCODART);
              +" and PRCODMAG = "+cp_ToStrODBC(_Curs_PAR_RIMA.PRCODMAG);
                 )
        else
          update (i_cTable) set;
              PRCOSSTA = this.w_TmpN;
              &i_ccchkf. ;
           where;
              PRCODART = _Curs_PAR_RIMA.PRCODART;
              and PRCODMAG = _Curs_PAR_RIMA.PRCODMAG;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        this.w_oPart.AddParam(this.w_SIMEUR + TRAN(this.w_TMPN,"99,999,999.99"))     
        this.w_TMPC = this.w_oMess.ComposeMessage()
        if this.w_NHF>=0
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
        select _Curs_PAR_RIMA
        continue
      enddo
      use
    endif
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = ah_Msgformat("Elaborazione terminata con successo")
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='VALUTE'
    this.cWorkTables[3]='PAR_RIOR'
    this.cWorkTables[4]='PAR_RIMA'
    return(this.OpenAllTables(4))

  proc CloseCursors()
    if used('_Curs_PAR_RIOR')
      use in _Curs_PAR_RIOR
    endif
    if used('_Curs_PAR_RIMA')
      use in _Curs_PAR_RIMA
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
