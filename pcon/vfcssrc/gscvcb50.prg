* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcb50                                                        *
*              Aggiornamento campo LPTELFAX tabella PRO_LOGP                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_11]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-05-18                                                      *
* Last revis.: 2009-06-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcb50",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcb50 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TMPC = space(10)
  w_TEMP = space(10)
  w_CPROWNUM = 0
  w_MMSERIAL = space(10)
  w_MMCODMAG = space(5)
  * --- WorkFile variables
  MVM_DETT_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna saldi lotti per retifiche inventariali
    * --- Try
    local bErr_0376E1A0
    bErr_0376E1A0=bTrsErr
    this.Try_0376E1A0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      if this.w_NHF>=0
        this.w_TMPC = AH_MsgFormat("ERRORE GENERICO - %1", this.oParentObject.w_PMSG)
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_0376E1A0
    * --- End
  endproc
  proc Try_0376E1A0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Select from ..\PCON\EXE\QUERY\GSCVCB50
    do vq_exec with '..\PCON\EXE\QUERY\GSCVCB50',this,'_Curs__d__d__PCON_EXE_QUERY_GSCVCB50','',.f.,.t.
    if used('_Curs__d__d__PCON_EXE_QUERY_GSCVCB50')
      select _Curs__d__d__PCON_EXE_QUERY_GSCVCB50
      locate for 1=1
      do while not(eof())
      this.w_MMSERIAL = MMSERIAL
      this.w_CPROWNUM = CPROWNUM
      this.w_MMCODMAG = MMCODMAG
      * --- Aggiorna campo MMLOTMAG nei movimenti di magazzino
      * --- Write into MVM_DETT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.MVM_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MVM_DETT_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.MVM_DETT_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MMLOTMAG ="+cp_NullLink(cp_ToStrODBC(this.w_MMCODMAG),'MVM_DETT','MMLOTMAG');
            +i_ccchkf ;
        +" where ";
            +"MMSERIAL = "+cp_ToStrODBC(this.w_MMSERIAL);
            +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
               )
      else
        update (i_cTable) set;
            MMLOTMAG = this.w_MMCODMAG;
            &i_ccchkf. ;
         where;
            MMSERIAL = this.w_MMSERIAL;
            and CPROWNUM = this.w_CPROWNUM;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Aggiorna saldi
      if g_MADV="S" AND g_PERLOT="S"
        * --- Aggiorna saldi lotti per i movimenti di magazzino
        GSMD_BRL (this, this.w_MMSERIAL , "M" , "+" , this.w_CPROWNUM,.T.)
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
        select _Curs__d__d__PCON_EXE_QUERY_GSCVCB50
        continue
      enddo
      use
    endif
    if bTrsErr
      this.w_TMPC = AH_MsgFormat("Impossibile aggiornare il campo MMLOTMAG dei movimenti di magazzino")
      * --- Raise
      i_Error=this.w_TMPC
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    if this.w_NHF>=0
      this.w_TMPC = AH_MsgFormat("Aggiornamento terminato con successo")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = AH_MsgFormat("Conversione eseguita correttamente")
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='MVM_DETT'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs__d__d__PCON_EXE_QUERY_GSCVCB50')
      use in _Curs__d__d__PCON_EXE_QUERY_GSCVCB50
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
