* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscv_b09                                                        *
*              Conv. progressivi G.magazzino (Euro-kit)                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_68]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-06-04                                                      *
* Last revis.: 2001-06-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscv_b09",oParentObject,m.w_NHF)
return(i_retval)

define class tgscv_b09 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_CODEUR = space(3)
  w_CODLIR = space(3)
  w_OBSO = ctod("  /  /  ")
  w_TmpN = 0
  w_TASSOEUR = 0
  w_TASSOLIT = 0
  w_TMPC = space(10)
  w_CODAZI = space(5)
  w_DECEUR = 0
  w_DARE = 0
  w_AVERE = 0
  w_PUNPAD = .NULL.
  w_PUNNON = .NULL.
  w_TEMP = space(10)
  w_SIMLIR = space(3)
  w_SIMEUR = space(3)
  w_ESEINI = space(4)
  w_ESEFIN = space(4)
  w_OK = .f.
  w_ESEFOUND = .f.
  w_TESTO = space(10)
  * --- WorkFile variables
  AZIENDA_idx=0
  VALUTE_idx=0
  GIORMAGA_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- FIle di LOG
    * --- Visibile dal padre
    * --- Variabili locali
    * --- Lancia maschera
    this.w_OK = .F.
    this.w_ESEINI = space(4)
    this.w_ESEFIN = space(4)
    this.w_TESTO = ah_Msgformat("Progressivi giornale magazzino")
    do GSCV_K07 with this
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if not this.w_OK
      i_retcode = 'stop'
      return
    endif
    * --- Data Obsolescenza
    this.w_OBSO = cp_CharToDate("01-01-2002")
    * --- Leggo il codice valuta Euro e Lire
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZVALEUR,AZVALLIR"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZVALEUR,AZVALLIR;
        from (i_cTable) where;
            AZCODAZI = i_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CODEUR = NVL(cp_ToDate(_read_.AZVALEUR),cp_NullValue(_read_.AZVALEUR))
      this.w_CODLIR = NVL(cp_ToDate(_read_.AZVALLIR),cp_NullValue(_read_.AZVALLIR))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Controllo 1 - Devono essere caricati i dati azienda
    if Empty ( this.w_CODLIR ) Or Empty ( this.w_CODEUR )
      this.oParentObject.w_PMSG = ah_Msgformat("ERRORE 001 - codice Euro o valuta nazionale non definiti in dati azienda")
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE 001 - codice Euro o valuta nazionale non definiti in dati azienda")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
      i_retcode = 'stop'
      return
    endif
    * --- Controllo 2 - L'esercizio corrente deve essere EURO
    if g_PERVAL <> g_CODEUR
      this.oParentObject.w_PMSG = ah_Msgformat("ERRORE 002 - la valuta di conto dell'esercizio corrente deve essere Euro")
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE 002 - la valuta di conto dell'esercizio corrente deve essere Euro")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
      i_retcode = 'stop'
      return
    endif
    * --- Leggo i tassi fissi di Euro (1) e Lire (1936,27)
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VACAOVAL,VADECTOT,VASIMVAL"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(this.w_CODEUR);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VACAOVAL,VADECTOT,VASIMVAL;
        from (i_cTable) where;
            VACODVAL = this.w_CODEUR;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_TASSOEUR = NVL(cp_ToDate(_read_.VACAOVAL),cp_NullValue(_read_.VACAOVAL))
      this.w_DECEUR = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
      this.w_SIMEUR = NVL(cp_ToDate(_read_.VASIMVAL),cp_NullValue(_read_.VASIMVAL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VACAOVAL,VASIMVAL"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(this.w_CODLIR);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VACAOVAL,VASIMVAL;
        from (i_cTable) where;
            VACODVAL = this.w_CODLIR;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_TASSOLIT = NVL(cp_ToDate(_read_.VACAOVAL),cp_NullValue(_read_.VACAOVAL))
      this.w_SIMLIR = NVL(cp_ToDate(_read_.VASIMVAL),cp_NullValue(_read_.VASIMVAL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Try
    local bErr_03752420
    bErr_03752420=bTrsErr
    this.Try_03752420()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      this.w_TMPC = ah_Msgformat("ERRORE GENERICO - operazione sospesa")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    bTrsErr=bTrsErr or bErr_03752420
    * --- End
  endproc
  proc Try_03752420()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.w_CODAZI = i_CODAZI
    this.w_ESEFOUND = .F.
    * --- Converte progressivi numerazione
    * --- Select from GIORMAGA
    i_nConn=i_TableProp[this.GIORMAGA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.GIORMAGA_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" GIORMAGA ";
          +" where GMCODAZI="+cp_ToStrODBC(this.w_CODAZI)+" and GMCODESE="+cp_ToStrODBC(this.w_ESEINI)+"";
           ,"_Curs_GIORMAGA")
    else
      select * from (i_cTable);
       where GMCODAZI=this.w_CODAZI and GMCODESE=this.w_ESEINI;
        into cursor _Curs_GIORMAGA
    endif
    if used('_Curs_GIORMAGA')
      select _Curs_GIORMAGA
      locate for 1=1
      do while not(eof())
      this.w_ESEFOUND = .T.
      * --- Try
      local bErr_035F4320
      bErr_035F4320=bTrsErr
      this.Try_035F4320()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_035F4320
      * --- End
      * --- Write into GIORMAGA
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.GIORMAGA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.GIORMAGA_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.GIORMAGA_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"GMDATGIO ="+cp_NullLink(cp_ToStrODBC(_Curs_GIORMAGA.GMDATGIO),'GIORMAGA','GMDATGIO');
        +",GMRIGGIO ="+cp_NullLink(cp_ToStrODBC(_Curs_GIORMAGA.GMRIGGIO),'GIORMAGA','GMRIGGIO');
            +i_ccchkf ;
        +" where ";
            +"GMCODAZI = "+cp_ToStrODBC(_Curs_GIORMAGA.GMCODAZI);
            +" and GMCODESE = "+cp_ToStrODBC(this.w_ESEFIN);
            +" and GMCODMAG = "+cp_ToStrODBC(_Curs_GIORMAGA.GMCODMAG);
               )
      else
        update (i_cTable) set;
            GMDATGIO = _Curs_GIORMAGA.GMDATGIO;
            ,GMRIGGIO = _Curs_GIORMAGA.GMRIGGIO;
            &i_ccchkf. ;
         where;
            GMCODAZI = _Curs_GIORMAGA.GMCODAZI;
            and GMCODESE = this.w_ESEFIN;
            and GMCODMAG = _Curs_GIORMAGA.GMCODMAG;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
        select _Curs_GIORMAGA
        continue
      enddo
      use
    endif
    if this.w_ESEFOUND
      this.oParentObject.w_PESEOK = .T.
      this.oParentObject.w_PMSG = ah_Msgformat("Elaborazione terminata con successo")
      this.w_TMPC = ah_Msgformat("Tabella ESERCIZI - progressivi giornale magazzino riportati da esercizio %1 a %2",this.w_ESEINI,this.w_ESEFIN)
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    else
      this.oParentObject.w_PESEOK = .F.
      this.oParentObject.w_PMSG = ah_Msgformat("Progressivi giornale magazzino esercizio %1 non definiti", this.w_ESEFIN )
      this.w_TMPC = ah_Msgformat("ERRORE 003 - progressivi giornale magazzino esercizio %1 non definiti", this.w_ESEFIN)
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    return
  proc Try_035F4320()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into GIORMAGA
    i_nConn=i_TableProp[this.GIORMAGA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.GIORMAGA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.GIORMAGA_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"GMCODAZI"+",GMCODESE"+",GMCODMAG"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(_Curs_GIORMAGA.GMCODAZI),'GIORMAGA','GMCODAZI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ESEFIN),'GIORMAGA','GMCODESE');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_GIORMAGA.GMCODMAG),'GIORMAGA','GMCODMAG');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'GMCODAZI',_Curs_GIORMAGA.GMCODAZI,'GMCODESE',this.w_ESEFIN,'GMCODMAG',_Curs_GIORMAGA.GMCODMAG)
      insert into (i_cTable) (GMCODAZI,GMCODESE,GMCODMAG &i_ccchkf. );
         values (;
           _Curs_GIORMAGA.GMCODAZI;
           ,this.w_ESEFIN;
           ,_Curs_GIORMAGA.GMCODMAG;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='VALUTE'
    this.cWorkTables[3]='GIORMAGA'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_GIORMAGA')
      use in _Curs_GIORMAGA
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
