* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcb07                                                        *
*              Aggiornamento procedure di conversione                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-09-17                                                      *
* Last revis.: 2008-07-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcb07",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcb07 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TMPC = space(10)
  w_TEMP = space(10)
  w_CMODULO = space(10)
  w_NMODULO = 0
  w_PROCMOD_C = 0
  w_PROCMOD_A = 0
  * --- WorkFile variables
  AHEECONV_idx=0
  CONVERSI_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Procedura di conversione 
    * --- Try
    local bErr_04737FB0
    bErr_04737FB0=bTrsErr
    this.Try_04737FB0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = AH_MsgFormat("ERRORE GENERICO - %1", this.oParentObject.w_PMSG)
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_04737FB0
    * --- End
  endproc
  proc Try_04737FB0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Valorizza per le procedure di conversione caricate sul database
    *     il campo modulo trasformandolo da numero al corrispondente codice
    * --- begin transaction
    cp_BeginTrs()
    this.w_PROCMOD_C = 0
    * --- Select from AHEECONV
    i_nConn=i_TableProp[this.AHEECONV_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AHEECONV_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select COMODULO  from "+i_cTable+" AHEECONV ";
          +" where COMODULO<>0 And COCODMOD=''";
          +" group by COMODULO";
           ,"_Curs_AHEECONV")
    else
      select COMODULO from (i_cTable);
       where COMODULO<>0 And COCODMOD="";
       group by COMODULO;
        into cursor _Curs_AHEECONV
    endif
    if used('_Curs_AHEECONV')
      select _Curs_AHEECONV
      locate for 1=1
      do while not(eof())
      * --- In base all'applicativo determino quale modulo attribuire
      *     La procedura gestisce tutti quelli presenti al momento
      this.w_NMODULO = _Curs_AHEECONV.COMODULO
      this.w_CMODULO = ""
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if Not Empty( this.w_CMODULO )
        * --- Write into AHEECONV
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.AHEECONV_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.AHEECONV_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.AHEECONV_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"COCODMOD ="+cp_NullLink(cp_ToStrODBC(this.w_CMODULO),'AHEECONV','COCODMOD');
          +",COMODULO ="+cp_NullLink(cp_ToStrODBC(0),'AHEECONV','COMODULO');
              +i_ccchkf ;
          +" where ";
              +"COMODULO = "+cp_ToStrODBC(this.w_NMODULO);
                 )
        else
          update (i_cTable) set;
              COCODMOD = this.w_CMODULO;
              ,COMODULO = 0;
              &i_ccchkf. ;
           where;
              COMODULO = this.w_NMODULO;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        this.w_PROCMOD_A = this.w_PROCMOD_A + i_Rows
      endif
        select _Curs_AHEECONV
        continue
      enddo
      use
    endif
    * --- Select from CONVERSI
    i_nConn=i_TableProp[this.CONVERSI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONVERSI_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select COMODULO  from "+i_cTable+" CONVERSI ";
          +" where COMODULO<>0 And COCODMOD=''";
          +" group by COMODULO";
           ,"_Curs_CONVERSI")
    else
      select COMODULO from (i_cTable);
       where COMODULO<>0 And COCODMOD="";
       group by COMODULO;
        into cursor _Curs_CONVERSI
    endif
    if used('_Curs_CONVERSI')
      select _Curs_CONVERSI
      locate for 1=1
      do while not(eof())
      * --- In base all'applicativo determino quale modulo attribuire
      *     La procedura gestisce tutti quelli presenti al momento
      this.w_NMODULO = _Curs_CONVERSI.COMODULO
      this.w_CMODULO = ""
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if Not Empty( this.w_CMODULO )
        * --- Write into CONVERSI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.CONVERSI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONVERSI_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CONVERSI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"COCODMOD ="+cp_NullLink(cp_ToStrODBC(this.w_CMODULO),'CONVERSI','COCODMOD');
          +",COMODULO ="+cp_NullLink(cp_ToStrODBC(0),'CONVERSI','COMODULO');
              +i_ccchkf ;
          +" where ";
              +"COMODULO = "+cp_ToStrODBC(this.w_NMODULO);
                 )
        else
          update (i_cTable) set;
              COCODMOD = this.w_CMODULO;
              ,COMODULO = 0;
              &i_ccchkf. ;
           where;
              COMODULO = this.w_NMODULO;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        this.w_PROCMOD_C = this.w_PROCMOD_C + i_Rows
      endif
        select _Curs_CONVERSI
        continue
      enddo
      use
    endif
    * --- commit
    cp_EndTrs(.t.)
    if this.w_NHF>=0
      this.w_TMPC = AH_MsgFormat("Aggiornamento modulo procedure di conversione eseguito con successo su %1 INF conversione e su %2 tabelle di conversione.E' necessario rientrare nella procedura.", alltrim( str( this.w_PROCMOD_A ) ) , alltrim( str( this.w_PROCMOD_C ) ) )
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = AH_MsgFormat("Conversione eseguita correttamente")
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if g_APPLICATION = "ad hoc ENTERPRISE"
      do case
        case this.w_NMODULO=8
          this.w_CMODULO = "CESP"
        case this.w_NMODULO=7
          this.w_CMODULO = "RITE"
        case this.w_NMODULO=24
          this.w_CMODULO = "EPOS"
        case this.w_NMODULO=10
          this.w_CMODULO = "BILC"
        case this.w_NMODULO=9
          this.w_CMODULO = "DIBA"
        case this.w_NMODULO=11
          this.w_CMODULO = "IMPO"
        case this.w_NMODULO=20
          this.w_CMODULO = "STAT"
        case this.w_NMODULO=3
          this.w_CMODULO = "COAN"
        case this.w_NMODULO=4
          this.w_CMODULO = "MAGA"
        case this.w_NMODULO=12
          this.w_CMODULO = "COMM"
        case this.w_NMODULO=34
          this.w_CMODULO = "INFO"
        case this.w_NMODULO=36
          this.w_CMODULO = "JBSH"
        case this.w_NMODULO=32
          this.w_CMODULO = "REBA"
        case this.w_NMODULO=30
          this.w_CMODULO = "DOCM"
        case this.w_NMODULO=38
          this.w_CMODULO = "IRDR"
      endcase
    else
      do case
        case this.w_NMODULO=11
          this.w_CMODULO = "STAT"
        case this.w_NMODULO=23
          this.w_CMODULO = "IMPO"
        case this.w_NMODULO=15
          this.w_CMODULO = "DISB"
        case this.w_NMODULO=19
          this.w_CMODULO = "SOLL"
        case this.w_NMODULO=17 Or this.w_NMODULO=25
          this.w_CMODULO = "COLA"
        case this.w_NMODULO=22
          this.w_CMODULO = "LEMC"
        case this.w_NMODULO=8
          this.w_CMODULO = "OFFE"
        case this.w_NMODULO=21
          this.w_CMODULO = "GPOS"
        case this.w_NMODULO=10
          this.w_CMODULO = "COMM"
        case this.w_NMODULO=12
          this.w_CMODULO = "CESP"
        case this.w_NMODULO=20
          this.w_CMODULO = "BANC"
        case this.w_NMODULO=26
          this.w_CMODULO = "MADV"
        case this.w_NMODULO=13
          this.w_CMODULO = "RITE"
        case this.w_NMODULO=31
          this.w_CMODULO = "IRDR"
        case this.w_NMODULO=33
          this.w_CMODULO = "VEFA"
        case this.w_NMODULO=35
          this.w_CMODULO = "DOCM"
        case this.w_NMODULO=1
          this.w_CMODULO = "COGE"
        case this.w_NMODULO=32
          this.w_CMODULO = "JBSH"
      endcase
    endif
  endproc


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='AHEECONV'
    this.cWorkTables[2]='CONVERSI'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_AHEECONV')
      use in _Curs_AHEECONV
    endif
    if used('_Curs_CONVERSI')
      use in _Curs_CONVERSI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
