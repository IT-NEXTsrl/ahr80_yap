* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvbb37                                                        *
*              Aggiorna rischio su causali doc                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_124]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-03-17                                                      *
* Last revis.: 2003-03-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvbb37",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvbb37 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TMPC = space(10)
  w_TEMP = space(10)
  w_MESS = space(10)
  * --- WorkFile variables
  TIP_DOCU_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna i campi date sui movimenti cespiti in base ai check impostati sulla causale
    *     In precedenza le date di primo utilizzo fiscale e civile e la data di dismissione erano sempre 
    *     valorizzate sul movimento anche se non visibili.
    *     La ricostruzione saldi successivamente riportava tali date sul cespite.
    * --- FIle di LOG
    * --- Try
    local bErr_0361DAC0
    bErr_0361DAC0=bTrsErr
    this.Try_0361DAC0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE GENERICO - impossibile aggiornare causali documenti")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_0361DAC0
    * --- End
  endproc
  proc Try_0361DAC0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Attivo rischio su causali che hanno attivo il flag Esposizione finanziaria 
    *     reso obsoleto
    * --- Write into TIP_DOCU
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.TIP_DOCU_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"TDFLRISC ="+cp_NullLink(cp_ToStrODBC("S"),'TIP_DOCU','TDFLRISC');
          +i_ccchkf ;
      +" where ";
          +"TDFLESPF = "+cp_ToStrODBC("S");
             )
    else
      update (i_cTable) set;
          TDFLRISC = "S";
          &i_ccchkf. ;
       where;
          TDFLESPF = "S";

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Causali documenti aggiornate: %1", Alltrim(str(i_rows)) )
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    * --- Considerando che la scrittura precedente ha messo S anche sulel causali che hanno
    *     Esposizione finanziaria, posso modificare tutte le note di credito e mettere a 
    *     decrementa la combo del rischio/esposizione finanziaria
    * --- Write into TIP_DOCU
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.TIP_DOCU_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"TDFLRISC ="+cp_NullLink(cp_ToStrODBC("D"),'TIP_DOCU','TDFLRISC');
          +i_ccchkf ;
      +" where ";
          +"TDCATDOC = "+cp_ToStrODBC("NC");
          +" and TDFLRISC = "+cp_ToStrODBC("S");
             )
    else
      update (i_cTable) set;
          TDFLRISC = "D";
          &i_ccchkf. ;
       where;
          TDCATDOC = "NC";
          and TDFLRISC = "S";

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Causali documenti di tipo nota di credito aggiornate: %1", Alltrim(str(i_rows)) )
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = ah_Msgformat("Conversione eseguita correttamente")
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='TIP_DOCU'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
