* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscv_bf4                                                        *
*              Aggiornamento valute                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][4]                                                       *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-09-17                                                      *
* Last revis.: 2009-03-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscv_bf4",oParentObject,m.w_NHF)
return(i_retval)

define class tgscv_bf4 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TMPC = space(10)
  w_TEMP = space(10)
  w_OPER = space(1)
  w_DATA = ctod("  /  /  ")
  * --- WorkFile variables
  VALUTE_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Procedura di conversione 
    if ah_yesno("Attenzione, la procedura utilizzer� la valuta di conto impostata in dati azienda, verificare che sia corretta%0Si desidera continuare?")
      * --- Try
      local bErr_03013CD8
      bErr_03013CD8=bTrsErr
      this.Try_03013CD8()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        * --- Gestisce log errori
        this.oParentObject.w_PMSG = Message()
        this.oParentObject.w_PESEOK = .F.
        if this.w_NHF>=0
          this.w_TMPC = AH_MsgFormat("ERRORE GENERICO - %1", this.oParentObject.w_PMSG)
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
      bTrsErr=bTrsErr or bErr_03013CD8
      * --- End
    else
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = AH_MsgFormat("Sospesa in attesa di verifica della valuta di conto")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
  endproc
  proc Try_03013CD8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Valorizza i campi operatare cambi e moneta unica nell'anagrafica valute.
    *     La procedura di conversione imposta l'operatore di moltiplicatore a
    *     tutte le valute con tasso fisso diverso da zero.
    *     A questa regola c'� l'eccezione dell'Euro per il quale imposto il divisore.
    *     Per le valute con tasso fisso, nessuna esclusa, imposto l'euro come moneta 
    *     unica.
    *     Per operare la distinzione utilizzo g_CODEUR
    * --- begin transaction
    cp_BeginTrs()
    * --- Select from VALUTE
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" VALUTE ";
           ,"_Curs_VALUTE")
    else
      select * from (i_cTable);
        into cursor _Curs_VALUTE
    endif
    if used('_Curs_VALUTE')
      select _Curs_VALUTE
      locate for 1=1
      do while not(eof())
      if Nvl( _Curs_VALUTE.VACAOVAL , 0 )<>0
        if _Curs_VALUTE.VACODVAL=g_CODEUR AND ( ISLITA() OR ISLESP() )
          this.w_OPER = "/"
        else
          this.w_OPER = "*"
        endif
        if ISLRON()
          if  _Curs_VALUTE.VACODVAL=g_CODEUR
            * --- Write into VALUTE
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.VALUTE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.VALUTE_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"VAOPECAM ="+cp_NullLink(cp_ToStrODBC(this.w_OPER),'VALUTE','VAOPECAM');
              +",VAUNIVAL ="+cp_NullLink(cp_ToStrODBC(g_CODEUR),'VALUTE','VAUNIVAL');
              +",VADATEUR ="+cp_NullLink(cp_ToStrODBC(i_INIDAT),'VALUTE','VADATEUR');
                  +i_ccchkf ;
              +" where ";
                  +"VACODVAL = "+cp_ToStrODBC(_Curs_VALUTE.VACODVAL);
                     )
            else
              update (i_cTable) set;
                  VAOPECAM = this.w_OPER;
                  ,VAUNIVAL = g_CODEUR;
                  ,VADATEUR = i_INIDAT;
                  &i_ccchkf. ;
               where;
                  VACODVAL = _Curs_VALUTE.VACODVAL;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          else
            * --- Azzero il campo del cambio fisso, della data e della valuta di riferimento
            this.w_DATA = Cp_chartodate("  /  /    ")
            * --- Write into VALUTE
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.VALUTE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.VALUTE_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"VAOPECAM ="+cp_NullLink(cp_ToStrODBC(this.w_OPER),'VALUTE','VAOPECAM');
              +",VACAOVAL ="+cp_NullLink(cp_ToStrODBC(0),'VALUTE','VACAOVAL');
              +",VADATEUR ="+cp_NullLink(cp_ToStrODBC(this.w_DATA),'VALUTE','VADATEUR');
              +",VAUNIVAL ="+cp_NullLink(cp_ToStrODBC(SPACE(3)),'VALUTE','VAUNIVAL');
                  +i_ccchkf ;
              +" where ";
                  +"VACODVAL = "+cp_ToStrODBC(_Curs_VALUTE.VACODVAL);
                     )
            else
              update (i_cTable) set;
                  VAOPECAM = this.w_OPER;
                  ,VACAOVAL = 0;
                  ,VADATEUR = this.w_DATA;
                  ,VAUNIVAL = SPACE(3);
                  &i_ccchkf. ;
               where;
                  VACODVAL = _Curs_VALUTE.VACODVAL;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
        else
          if (ISLITA() OR ISLESP())
            if  _Curs_VALUTE.VACODVAL<>g_PERVAL
              this.w_DATA = Cp_chartodate("01/01/1999")
            else
              if type("g_DATMIN") <>"U"
                this.w_DATA = g_DATMIN
              else
                this.w_DATA = i_INIDAT
              endif
            endif
          else
            this.w_DATA = g_DATEUR
          endif
          * --- Write into VALUTE
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.VALUTE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.VALUTE_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"VAOPECAM ="+cp_NullLink(cp_ToStrODBC(this.w_OPER),'VALUTE','VAOPECAM');
            +",VAUNIVAL ="+cp_NullLink(cp_ToStrODBC(g_CODEUR),'VALUTE','VAUNIVAL');
            +",VADATEUR ="+cp_NullLink(cp_ToStrODBC(this.w_DATA),'VALUTE','VADATEUR');
                +i_ccchkf ;
            +" where ";
                +"VACODVAL = "+cp_ToStrODBC(_Curs_VALUTE.VACODVAL);
                   )
          else
            update (i_cTable) set;
                VAOPECAM = this.w_OPER;
                ,VAUNIVAL = g_CODEUR;
                ,VADATEUR = this.w_DATA;
                &i_ccchkf. ;
             where;
                VACODVAL = _Curs_VALUTE.VACODVAL;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      else
        if _Curs_VALUTE.VACODVAL=g_CODEUR
          do case
            case  ISLITA() OR ISLESP() 
              this.w_OPER = "/"
              * --- imposto il tasso fisso verso se stesso
              * --- Write into VALUTE
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.VALUTE_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.VALUTE_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"VAOPECAM ="+cp_NullLink(cp_ToStrODBC(this.w_OPER),'VALUTE','VAOPECAM');
                +",VAUNIVAL ="+cp_NullLink(cp_ToStrODBC(g_CODEUR),'VALUTE','VAUNIVAL');
                +",VADATEUR ="+cp_NullLink(cp_ToStrODBC(i_INIDAT),'VALUTE','VADATEUR');
                +",VACAOVAL ="+cp_NullLink(cp_ToStrODBC(1),'VALUTE','VACAOVAL');
                    +i_ccchkf ;
                +" where ";
                    +"VACODVAL = "+cp_ToStrODBC(_Curs_VALUTE.VACODVAL);
                       )
              else
                update (i_cTable) set;
                    VAOPECAM = this.w_OPER;
                    ,VAUNIVAL = g_CODEUR;
                    ,VADATEUR = i_INIDAT;
                    ,VACAOVAL = 1;
                    &i_ccchkf. ;
                 where;
                    VACODVAL = _Curs_VALUTE.VACODVAL;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            case ISLRON() 
              this.w_OPER = "*"
              * --- imposto il tasso fisso verso se stesso
              * --- Write into VALUTE
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.VALUTE_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.VALUTE_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"VAOPECAM ="+cp_NullLink(cp_ToStrODBC(this.w_OPER),'VALUTE','VAOPECAM');
                +",VAUNIVAL ="+cp_NullLink(cp_ToStrODBC(g_CODEUR),'VALUTE','VAUNIVAL');
                +",VADATEUR ="+cp_NullLink(cp_ToStrODBC(i_INIDAT),'VALUTE','VADATEUR');
                +",VACAOVAL ="+cp_NullLink(cp_ToStrODBC(1),'VALUTE','VACAOVAL');
                    +i_ccchkf ;
                +" where ";
                    +"VACODVAL = "+cp_ToStrODBC(_Curs_VALUTE.VACODVAL);
                       )
              else
                update (i_cTable) set;
                    VAOPECAM = this.w_OPER;
                    ,VAUNIVAL = g_CODEUR;
                    ,VADATEUR = i_INIDAT;
                    ,VACAOVAL = 1;
                    &i_ccchkf. ;
                 where;
                    VACODVAL = _Curs_VALUTE.VACODVAL;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
          endcase
        endif
      endif
        select _Curs_VALUTE
        continue
      enddo
      use
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Al termine eseguo il ricalcolo dell'array delle valute..
    =CAVALUTE(.T.)
    if this.w_NHF>=0
      this.w_TMPC = AH_MsgFormat("Aggiornamento tabella valute, valorizzazione operatore cambi e moneta unica.")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = AH_MsgFormat("Conversione eseguita correttamente")
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='VALUTE'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_VALUTE')
      use in _Curs_VALUTE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
