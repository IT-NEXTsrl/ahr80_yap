* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcb05                                                        *
*              Aggiorna campo DDTELFAX                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_6]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-03-23                                                      *
* Last revis.: 2008-05-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcb05",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcb05 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_MESS = space(100)
  * --- WorkFile variables
  DES_DIVE_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questa procedura di conversione esegue:
    *      Aggiornamento campo DDTELFAX con il numero presente nel campo DDNUMFAX
    * --- Try
    local bErr_038B7A58
    bErr_038B7A58=bTrsErr
    this.Try_038B7A58()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = this.oParentObject.w_PMSG
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_038B7A58
    * --- End
  endproc
  proc Try_038B7A58()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Aggiornamento campo DDTELFAX
    * --- Write into DES_DIVE
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DES_DIVE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DES_DIVE_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="DDTIPCON,DDCODICE,DDCODDES"
      do vq_exec with 'GSCVCB05',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DES_DIVE_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="DES_DIVE.DDTIPCON = _t2.DDTIPCON";
              +" and "+"DES_DIVE.DDCODICE = _t2.DDCODICE";
              +" and "+"DES_DIVE.DDCODDES = _t2.DDCODDES";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"DDTELFAX = _t2.DDNUMFAX";
          +i_ccchkf;
          +" from "+i_cTable+" DES_DIVE, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="DES_DIVE.DDTIPCON = _t2.DDTIPCON";
              +" and "+"DES_DIVE.DDCODICE = _t2.DDCODICE";
              +" and "+"DES_DIVE.DDCODDES = _t2.DDCODDES";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DES_DIVE, "+i_cQueryTable+" _t2 set ";
          +"DES_DIVE.DDTELFAX = _t2.DDNUMFAX";
          +Iif(Empty(i_ccchkf),"",",DES_DIVE.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="DES_DIVE.DDTIPCON = t2.DDTIPCON";
              +" and "+"DES_DIVE.DDCODICE = t2.DDCODICE";
              +" and "+"DES_DIVE.DDCODDES = t2.DDCODDES";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DES_DIVE set (";
          +"DDTELFAX";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.DDNUMFAX";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="DES_DIVE.DDTIPCON = _t2.DDTIPCON";
              +" and "+"DES_DIVE.DDCODICE = _t2.DDCODICE";
              +" and "+"DES_DIVE.DDCODDES = _t2.DDCODDES";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DES_DIVE set ";
          +"DDTELFAX = _t2.DDNUMFAX";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".DDTIPCON = "+i_cQueryTable+".DDTIPCON";
              +" and "+i_cTable+".DDCODICE = "+i_cQueryTable+".DDCODICE";
              +" and "+i_cTable+".DDCODDES = "+i_cQueryTable+".DDCODDES";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"DDTELFAX = (select DDNUMFAX from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    ah_Msg("Aggiornamento campo DDTELFAX",.T.)
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.w_TMPC = ah_Msgformat("Aggiornamento del campo DDTELFAX eseguito correttamente")
    this.oParentObject.w_PMSG = this.w_TMPC
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='DES_DIVE'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
