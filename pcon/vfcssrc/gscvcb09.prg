* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcb09                                                        *
*              Modifica campi DDNUMFAX e DDTELFAX                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_11]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-05-18                                                      *
* Last revis.: 2009-02-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcb09",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcb09 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TMPC = space(10)
  w_TEMP = space(10)
  w_CNAME = space(30)
  w_CODAZI = 0
  w_PHNAME = space(50)
  w_IDXTABLE = 0
  w_CONN = 0
  * --- WorkFile variables
  RIPATMP1_idx=0
  DES_DIVE_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Riduco la dimensione dei campi DDNUMFAX e DDTELFAX presenti nella tabella DES_DIVE
    * --- FIle di LOG
    * --- Try
    local bErr_03752420
    bErr_03752420=bTrsErr
    this.Try_03752420()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = AH_MsgFormat("ERRORE GENERICO - %1", this.oParentObject.w_PMSG)
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_03752420
    * --- End
  endproc
  proc Try_03752420()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- ATTENZIONE - procedura di conversioen senza transazione in quanto
    *     se ricostruisco il database per la tabella sotto transazione SQL Server e oracle
    *     annullano la INSERT INTO... al fallire di una ALTER TABLE con DROP
    *     CONTRAINTS.
    *     DB2 non gestisce le transazioni sui metadati
    this.w_CNAME = "DES_DIVE"
    * --- Rimuove l'integritÓ referenziale...
    *     Prima rimuovo dall'azienda corrente poi dall'azienda XXX
    this.w_CODAZI = i_CODAZI
    GSCV_BIN(this, this.w_CODAZI , this.w_CNAME , this.w_NHF )
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_CODAZI = "xxx"
    GSCV_BIN(this, this.w_CODAZI , this.w_CNAME , this.w_NHF )
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Solo per l'azienda corrente passo i dati
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if bTrsErr
      this.w_TMPC = AH_MsgFormat("Impossibile modificare la struttura dei campi DDNUMFAX e DDTELFAX")
      * --- Raise
      i_Error=this.w_TMPC
      return
    else
      if this.w_NHF>=0
        this.w_TMPC = AH_MsgFormat("Aggiornamento struttura campi DDNUMFAX e DDTELFAX completata con successo")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    if this.w_NHF>=0
      this.w_TMPC = AH_MsgFormat("Aggiornamento terminato con successo")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = AH_MsgFormat("Conversione eseguita correttamente")
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella DB2
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('..\PCON\EXE\QUERY\GSCVCB09',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    * --- Elimino la tabella
    this.w_IDXTABLE = cp_OpenTable( Alltrim(this.w_CNAME) ,.T.)
    this.w_CONN = i_TableProp[ this.w_IDXTABLE , 3 ] 
    if Not GSCV_BDT( this, this.w_CNAME , this.w_Conn )
      * --- Raise
      i_Error="Error"
      return
    endif
    * --- Ricostruisco il database per la tabella
    GSCV_BRT(this, this.w_CNAME , this.w_Conn , .F.)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into DES_DIVE
    i_nConn=i_TableProp[this.DES_DIVE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DES_DIVE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.DES_DIVE_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_Msg("Ripristinati valori di origine",.T.)
    * --- Ricostruico database e integritÓ referenziali
    GSCV_BRT(this, this.w_CNAME , this.w_Conn , .T., "all")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    ah_Msg("Eliminata tabella temporanea tmp",.T.)
  endproc


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='*RIPATMP1'
    this.cWorkTables[2]='DES_DIVE'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
