* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcb51                                                        *
*              Agg. ATFLNOTI e CAFLNOTI - OFF_ATTI e CAUMATTI                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_11]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-05-25                                                      *
* Last revis.: 2011-01-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcb51",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcb51 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TMPC = space(10)
  w_TEMP = space(10)
  w_SERIAL = space(20)
  w_TROVATA = .f.
  w_CodRel = space(15)
  * --- WorkFile variables
  OFF_ATTI_idx=0
  CAUMATTI_idx=0
  CONVERSI_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- OFF_ATTI e CAUMATTI: aggiornamento campo ATFLNOTI e CAFLNOTI, se vuoto deve diventare 'N'
    this.w_CodRel = "7.0-M5631"
    * --- Try
    local bErr_0379C4B0
    bErr_0379C4B0=bTrsErr
    this.Try_0379C4B0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("Errore generico - impossibile eseguire aggiornamento richiesto")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_0379C4B0
    * --- End
  endproc
  proc Try_0379C4B0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Legge se eseguita la conversione  7.0-M5631
    * --- Read from CONVERSI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CONVERSI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONVERSI_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "*"+;
        " from "+i_cTable+" CONVERSI where ";
            +"COCODREL = "+cp_ToStrODBC(this.w_CodRel);
            +" and COESEGUI = "+cp_ToStrODBC("S");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        *;
        from (i_cTable) where;
            COCODREL = this.w_CodRel;
            and COESEGUI = "S";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_TROVATA = i_Rows>0
    * --- begin transaction
    cp_BeginTrs()
    if NOT this.w_TROVATA
      * --- Select from QUERY\GSCVCB51
      do vq_exec with 'QUERY\GSCVCB51',this,'_Curs_QUERY_GSCVCB51','',.f.,.t.
      if used('_Curs_QUERY_GSCVCB51')
        select _Curs_QUERY_GSCVCB51
        locate for 1=1
        do while not(eof())
        this.w_SERIAL = CACODICE
        * --- Write into CAUMATTI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.CAUMATTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAUMATTI_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CAUMATTI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CAFLNOTI ="+cp_NullLink(cp_ToStrODBC("N"),'CAUMATTI','CAFLNOTI');
              +i_ccchkf ;
          +" where ";
              +"CACODICE = "+cp_ToStrODBC(this.w_SERIAL);
                 )
        else
          update (i_cTable) set;
              CAFLNOTI = "N";
              &i_ccchkf. ;
           where;
              CACODICE = this.w_SERIAL;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
          select _Curs_QUERY_GSCVCB51
          continue
        enddo
        use
      endif
      this.w_TMPC = ah_Msgformat("Aggiornamento campo CAFLNOTI eseguita con successo")
      * --- Select from QUERY\GSCVCB51_2
      do vq_exec with 'QUERY\GSCVCB51_2',this,'_Curs_QUERY_GSCVCB51_2','',.f.,.t.
      if used('_Curs_QUERY_GSCVCB51_2')
        select _Curs_QUERY_GSCVCB51_2
        locate for 1=1
        do while not(eof())
        this.w_SERIAL = ATSERIAL
        * --- Write into OFF_ATTI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.OFF_ATTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_ATTI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ATFLNOTI ="+cp_NullLink(cp_ToStrODBC("N"),'OFF_ATTI','ATFLNOTI');
              +i_ccchkf ;
          +" where ";
              +"ATSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                 )
        else
          update (i_cTable) set;
              ATFLNOTI = "N";
              &i_ccchkf. ;
           where;
              ATSERIAL = this.w_SERIAL;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
          select _Curs_QUERY_GSCVCB51_2
          continue
        enddo
        use
      endif
      this.w_TMPC = ah_Msgformat("Aggiornamento campo ATFLNOTI eseguita con successo")
    endif
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = this.w_TMPC
    * --- commit
    cp_EndTrs(.t.)
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='OFF_ATTI'
    this.cWorkTables[2]='CAUMATTI'
    this.cWorkTables[3]='CONVERSI'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_QUERY_GSCVCB51')
      use in _Curs_QUERY_GSCVCB51
    endif
    if used('_Curs_QUERY_GSCVCB51_2')
      use in _Curs_QUERY_GSCVCB51_2
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
