* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscv_ba5                                                        *
*              Aggiorna campo tipo provvigione in fadif                        *
*                                                                              *
*      Author: ZUCCHETTI TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_150]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-05-06                                                      *
* Last revis.: 2002-05-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscv_ba5",oParentObject,m.w_NHF)
return(i_retval)

define class tgscv_ba5 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_MESS = space(100)
  w_MVSERIAL = space(10)
  w_CPROWNUM = 0
  w_MVTIPPRO = space(2)
  w_OLDSERIAL = space(10)
  w_OLDROWNUM = 0
  w_MVPERPRO = 0
  w_MVIMPPRO = 0
  * --- WorkFile variables
  DOC_DETT_idx=0
  CONTI_idx=0
  ART_ICOL_idx=0
  TAB_PROV_idx=0
  PAR_PROV_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questa procedura Aggiorna il campo tipo Provvigione nelle fatture differite
    * --- FIle di LOG
    * --- Try
    local bErr_04A9FDE0
    bErr_04A9FDE0=bTrsErr
    this.Try_04A9FDE0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      wait wind message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE GENERICO - impossibile aggiornare i documenti")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_04A9FDE0
    * --- End
  endproc
  proc Try_04A9FDE0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Aggiorna preventivamente MVTIPPRO  E PPCALPRO nel caso in cui i campi siano NULL
     
 i_Rows = cp_TrsSQL(i_TableProp[this.DOC_DETT_idx,3], "UPDATE "+cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])+" SET MVTIPPRO='DC'  where MVTIPPRO IS  NULL" )
     
 i_Rows = cp_TrsSQL(i_TableProp[this.PAR_PROV_idx,3], "UPDATE "+ (i_TableProp[this.PAR_PROV_idx,2])+; 
 " SET PPCALPRO='DI' where PPCALPRO IS  NULL" )
    * --- Aggiorna le fatture nn raggruppate con il tipo provv. del documento d'origine
    * --- Write into DOC_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="MVSERIAL,CPROWNUM"
      do vq_exec with 'GSCV_B95',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
              +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MVTIPPRO = _t2.MVTIPPRO";
          +i_ccchkf;
          +" from "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
              +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 set ";
          +"DOC_DETT.MVTIPPRO = _t2.MVTIPPRO";
          +Iif(Empty(i_ccchkf),"",",DOC_DETT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="DOC_DETT.MVSERIAL = t2.MVSERIAL";
              +" and "+"DOC_DETT.CPROWNUM = t2.CPROWNUM";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set (";
          +"MVTIPPRO";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.MVTIPPRO";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
              +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set ";
          +"MVTIPPRO = _t2.MVTIPPRO";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MVTIPPRO = (select MVTIPPRO from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    this.w_OLDSERIAL = "XZHXJXKZJZ"
    this.w_OLDROWNUM = 0
    * --- Query che estrae le fatture raggruppate per articolo: se risale a un tipo provv. unico per tutte le righe raggruppate  mette quello,
    *      se c'� difformit� tra le righe dei docum. d'origine inserisce forzata se la percentuale o l'importo sono valorizzati, 
    *     lascia da calcolare se entrambi i campi sono vuoti
    * --- Select from gscv1b95
    do vq_exec with 'gscv1b95',this,'_Curs_gscv1b95','',.f.,.t.
    if used('_Curs_gscv1b95')
      select _Curs_gscv1b95
      locate for 1=1
      do while not(eof())
      if this.w_OLDSERIAL<>_Curs_GSCV1B95.MVSERIAL OR this.w_OLDROWNUM<>_Curs_GSCV1B95.CPROWNUM
        this.w_MVSERIAL = _Curs_GSCV1B95.MVSERIAL
        this.w_CPROWNUM = _Curs_GSCV1B95.CPROWNUM
        this.w_MVTIPPRO = NVL(_Curs_GSCV1B95.MVTIPPRO,SPACE(2))
        * --- Write into DOC_DETT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.DOC_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MVTIPPRO ="+cp_NullLink(cp_ToStrODBC(this.w_MVTIPPRO),'DOC_DETT','MVTIPPRO');
              +i_ccchkf ;
          +" where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
                 )
        else
          update (i_cTable) set;
              MVTIPPRO = this.w_MVTIPPRO;
              &i_ccchkf. ;
           where;
              MVSERIAL = this.w_MVSERIAL;
              and CPROWNUM = this.w_CPROWNUM;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      else
        this.w_MVPERPRO = NVL(MVPERPRO,0)
        this.w_MVIMPPRO = NVL(MVIMPPRO,0)
        if this.w_MVPERPRO<>0 OR this.w_MVIMPPRO<>0
          * --- Write into DOC_DETT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.DOC_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MVTIPPRO ="+cp_NullLink(cp_ToStrODBC("FO"),'DOC_DETT','MVTIPPRO');
                +i_ccchkf ;
            +" where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
                +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
                   )
          else
            update (i_cTable) set;
                MVTIPPRO = "FO";
                &i_ccchkf. ;
             where;
                MVSERIAL = this.w_MVSERIAL;
                and CPROWNUM = this.w_CPROWNUM;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      endif
      this.w_OLDSERIAL = _Curs_GSCV1B95.MVSERIAL
      this.w_OLDROWNUM = _Curs_GSCV1B95.CPROWNUM
      * --- --
        select _Curs_gscv1b95
        continue
      enddo
      use
    endif
    if bTrsErr
      * --- Raise
      i_Error=MESSAGE()
      return
    else
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("Elaborazione terminata con successo")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='DOC_DETT'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='ART_ICOL'
    this.cWorkTables[4]='TAB_PROV'
    this.cWorkTables[5]='PAR_PROV'
    return(this.OpenAllTables(5))

  proc CloseCursors()
    if used('_Curs_gscv1b95')
      use in _Curs_gscv1b95
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
