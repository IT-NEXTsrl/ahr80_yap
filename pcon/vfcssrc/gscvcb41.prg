* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcb41                                                        *
*              Aggiornamento campo nosogget in nominativi                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-01-14                                                      *
* Last revis.: 2009-07-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcb41",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcb41 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TMPC = space(10)
  w_TEMP = space(10)
  * --- WorkFile variables
  RAP_PRES_idx=0
  DIPENDEN_idx=0
  OFF_NOMI_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiornamento campo nosogget in nominativi
    *     verifica se il valore PF � corretto confrontando con persona fisica in clienti
    if ! isAlt()
      * --- Try
      local bErr_03816E28
      bErr_03816E28=bTrsErr
      this.Try_03816E28()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        * --- Gestisce log errori
        this.oParentObject.w_PMSG = Message()
        this.oParentObject.w_PESEOK = .F.
        if this.w_NHF>=0
          this.w_TMPC = this.oParentObject.w_PMSG
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
      bTrsErr=bTrsErr or bErr_03816E28
      * --- End
    else
      * --- se alter ego non eseguo la conversione
      * --- Esecuzione ok
      this.oParentObject.w_PESEOK = .T.
    endif
  endproc
  proc Try_03816E28()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    if IsAhr()
      * --- Write into OFF_NOMI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="NOCODICE"
        do vq_exec with 'GSCVCB41',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_NOMI_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="OFF_NOMI.NOCODICE = _t2.NOCODICE";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"NOSOGGET = _t2.NOSOGGET";
            +i_ccchkf;
            +" from "+i_cTable+" OFF_NOMI, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="OFF_NOMI.NOCODICE = _t2.NOCODICE";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OFF_NOMI, "+i_cQueryTable+" _t2 set ";
            +"OFF_NOMI.NOSOGGET = _t2.NOSOGGET";
            +Iif(Empty(i_ccchkf),"",",OFF_NOMI.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="OFF_NOMI.NOCODICE = t2.NOCODICE";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OFF_NOMI set (";
            +"NOSOGGET";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.NOSOGGET";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="OFF_NOMI.NOCODICE = _t2.NOCODICE";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OFF_NOMI set ";
            +"NOSOGGET = _t2.NOSOGGET";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".NOCODICE = "+i_cQueryTable+".NOCODICE";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"NOSOGGET = (select NOSOGGET from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    else
      * --- Write into OFF_NOMI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="NOCODICE"
        do vq_exec with 'GSCVCC41',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_NOMI_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="OFF_NOMI.NOCODICE = _t2.NOCODICE";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"NOSOGGET = _t2.NOSOGGET";
            +i_ccchkf;
            +" from "+i_cTable+" OFF_NOMI, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="OFF_NOMI.NOCODICE = _t2.NOCODICE";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OFF_NOMI, "+i_cQueryTable+" _t2 set ";
            +"OFF_NOMI.NOSOGGET = _t2.NOSOGGET";
            +Iif(Empty(i_ccchkf),"",",OFF_NOMI.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="OFF_NOMI.NOCODICE = t2.NOCODICE";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OFF_NOMI set (";
            +"NOSOGGET";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.NOSOGGET";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="OFF_NOMI.NOCODICE = _t2.NOCODICE";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OFF_NOMI set ";
            +"NOSOGGET = _t2.NOSOGGET";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".NOCODICE = "+i_cQueryTable+".NOCODICE";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"NOSOGGET = (select NOSOGGET from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- Esecuzione ok
    * --- commit
    cp_EndTrs(.t.)
    this.oParentObject.w_PESEOK = .T.
    this.w_TMPC = ah_Msgformat("Aggiornamento campo persona fisica in anagrafica nominativi avvenuto correttamente")
    this.oParentObject.w_PMSG = ah_Msgformat("Conversione eseguita correttamente")
    if this.w_NHF>=0
      this.w_TMPC = this.oParentObject.w_PMSG
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='RAP_PRES'
    this.cWorkTables[2]='DIPENDEN'
    this.cWorkTables[3]='OFF_NOMI'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
