* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvbb54                                                        *
*              Valorizza origini fatture differite                             *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-03-17                                                      *
* Last revis.: 2006-12-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvbb54",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvbb54 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TMPC = space(10)
  w_TEMP = space(10)
  w_TIPDOC = space(5)
  w_TIPDOCCOL = space(5)
  * --- WorkFile variables
  DOC_MAST_idx=0
  TIP_DOCU_idx=0
  DOC_COLL_idx=0
  TMPTIPDOC_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Popola origini delle causali fatture differite con le causali che hanno il check
    *     Genera FD
    * --- FIle di LOG
    * --- Try
    local bErr_03729B70
    bErr_03729B70=bTrsErr
    this.Try_03729B70()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = Ah_MsgFormat("ERRORE GENERICO - Impossibile aggiornare tabella %1", "DOC_COLL")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_03729B70
    * --- End
  endproc
  proc Try_03729B70()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Leggo le causali Genera FD, utilizzo una tabella temporanea perch� non posso
    *     fare due select annidate sulla stessa tabella
    * --- Create temporary table TMPTIPDOC
    i_nIdx=cp_AddTableDef('TMPTIPDOC') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.TIP_DOCU_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"TDTIPDOC "," from "+i_cTable;
          +" where TFFLGEFA='S'";
          )
    this.TMPTIPDOC_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Leggo le causali Fatture differite
    * --- Select from TIP_DOCU
    i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select TDTIPDOC  from "+i_cTable+" TIP_DOCU ";
          +" where TFFLGEFA='B'";
          +" order by TDTIPDOC";
           ,"_Curs_TIP_DOCU")
    else
      select TDTIPDOC from (i_cTable);
       where TFFLGEFA="B";
       order by TDTIPDOC;
        into cursor _Curs_TIP_DOCU
    endif
    if used('_Curs_TIP_DOCU')
      select _Curs_TIP_DOCU
      locate for 1=1
      do while not(eof())
      this.w_TIPDOC = _Curs_TIP_DOCU.TDTIPDOC
      * --- Leggo le causali Genera FD
      * --- Select from TMPTIPDOC
      i_nConn=i_TableProp[this.TMPTIPDOC_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPTIPDOC_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select TDTIPDOC  from "+i_cTable+" TMPTIPDOC ";
             ,"_Curs_TMPTIPDOC")
      else
        select TDTIPDOC from (i_cTable);
          into cursor _Curs_TMPTIPDOC
      endif
      if used('_Curs_TMPTIPDOC')
        select _Curs_TMPTIPDOC
        locate for 1=1
        do while not(eof())
        this.w_TIPDOCCOL = _Curs_TMPTIPDOC.TDTIPDOC
        * --- Se la causale � gi� presente non la inserisco
        * --- Read from DOC_COLL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DOC_COLL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_COLL_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "*"+;
            " from "+i_cTable+" DOC_COLL where ";
                +"DCCODICE = "+cp_ToStrODBC(this.w_TIPDOC);
                +" and DCCOLLEG = "+cp_ToStrODBC(this.w_TIPDOCCOL);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            *;
            from (i_cTable) where;
                DCCODICE = this.w_TIPDOC;
                and DCCOLLEG = this.w_TIPDOCCOL;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if i_Rows = 0
          * --- Insert into DOC_COLL
          i_nConn=i_TableProp[this.DOC_COLL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_COLL_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DOC_COLL_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"DCCODICE"+",DCCOLLEG"+",DCFLINTE"+",DCFLEVAS"+",DCFLACCO"+",DCFLGROR"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_TIPDOC),'DOC_COLL','DCCODICE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_TIPDOCCOL),'DOC_COLL','DCCOLLEG');
            +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_COLL','DCFLINTE');
            +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_COLL','DCFLEVAS');
            +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_COLL','DCFLACCO');
            +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_COLL','DCFLGROR');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'DCCODICE',this.w_TIPDOC,'DCCOLLEG',this.w_TIPDOCCOL,'DCFLINTE'," ",'DCFLEVAS'," ",'DCFLACCO'," ",'DCFLGROR'," ")
            insert into (i_cTable) (DCCODICE,DCCOLLEG,DCFLINTE,DCFLEVAS,DCFLACCO,DCFLGROR &i_ccchkf. );
               values (;
                 this.w_TIPDOC;
                 ,this.w_TIPDOCCOL;
                 ," ";
                 ," ";
                 ," ";
                 ," ";
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        endif
          select _Curs_TMPTIPDOC
          continue
        enddo
        use
      endif
        select _Curs_TIP_DOCU
        continue
      enddo
      use
    endif
    * --- Droppo la tabella di appoggio
    * --- Drop temporary table TMPTIPDOC
    i_nIdx=cp_GetTableDefIdx('TMPTIPDOC')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPTIPDOC')
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    if this.w_NHF>=0
      this.w_TMPC = Ah_MsgFormat("Aggiornamento causali avvenuto correttamente")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = Ah_MsgFormat("Conversione eseguita correttamente")
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='DOC_MAST'
    this.cWorkTables[2]='TIP_DOCU'
    this.cWorkTables[3]='DOC_COLL'
    this.cWorkTables[4]='*TMPTIPDOC'
    return(this.OpenAllTables(4))

  proc CloseCursors()
    if used('_Curs_TIP_DOCU')
      use in _Curs_TIP_DOCU
    endif
    if used('_Curs_TMPTIPDOC')
      use in _Curs_TMPTIPDOC
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
