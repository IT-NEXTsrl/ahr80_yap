* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcb21                                                        *
*              Valorizzazione tabelle COS_VITA, COE_RACC e INT_LEGA            *
*                                                                              *
*      Author: Zucchetti TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_77]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-11-26                                                      *
* Last revis.: 2010-12-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcb21",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcb21 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_COS_VITA = space(10)
  w_COE_RACC = space(10)
  w_INT_LEGA = space(10)
  w_CV__ANNO = 0
  w_CV__BASE = 0
  w_CVINDI01 = 0
  w_CVINDI02 = 0
  w_CVINDI03 = 0
  w_CVINDI04 = 0
  w_CVINDI05 = 0
  w_CVINDI06 = 0
  w_CVINDI07 = 0
  w_CVINDI08 = 0
  w_CVINDI09 = 0
  w_CVINDI10 = 0
  w_CVINDI11 = 0
  w_CVINDI12 = 0
  w_CRPRIBAS = 0
  w_CPROWNUM = 0
  w_CRSECBAS = 0
  w_CRCOERAC = 0
  w_INDATINI = ctod("  /  /  ")
  w_INTASLEG = 0
  w___BASE = 0
  w_INDI01 = 0
  w_INDI02 = 0
  w_INDI03 = 0
  w_INDI04 = 0
  w_INDI05 = 0
  w_INDI06 = 0
  w_INDI07 = 0
  w_INDI08 = 0
  w_INDI09 = 0
  w_INDI10 = 0
  w_INDI11 = 0
  w_INDI12 = 0
  w_SAG_MORA = space(10)
  w_IMDATINI = ctod("  /  /  ")
  w_IMDATFIN = ctod("  /  /  ")
  w_IM__ANNO = space(4)
  w_IMPERIOD = space(1)
  w_IMSAGRIF = 0
  w_IMMAGNDP = 0
  w_IMMAGDEP = 0
  * --- WorkFile variables
  COS_VITA_idx=0
  COE_RACC_idx=0
  INT_LEGA_idx=0
  INT_MORA_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Valorizzazione tabelle COS_VITA, COE_RACC e INT_LEGA
    * --- COS_VITA
    * --- COE_RACC
    * --- INT_LEGA
    if IsAlt()
      * --- I DBF delle tabelle devono essere contenuti nella cartella Files_x_Import
      * --- Se esistono i DBF da cui attingere, si estraggono i dati in un cursore e si inseriscono nelle tabelle COS_VITA, COE_RACC e INT_LEGA
      this.w_COS_VITA = "Files_x_Import\COS_VITA.DBF"
      if cp_FileExist(this.w_COS_VITA)
        SELECT * FROM (this.w_COS_VITA) INTO CURSOR TMP_COSVITA
        if USED("TMP_COSVITA")
          SELECT TMP_COSVITA
          SCAN
          * --- Try
          local bErr_035FBEC0
          bErr_035FBEC0=bTrsErr
          this.Try_035FBEC0()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- Try
            local bErr_037419B0
            bErr_037419B0=bTrsErr
            this.Try_037419B0()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
            endif
            bTrsErr=bTrsErr or bErr_037419B0
            * --- End
          endif
          bTrsErr=bTrsErr or bErr_035FBEC0
          * --- End
          ENDSCAN
        endif
        USE IN SELECT ("TMP_COSVITA")
        USE IN SELECT ("COS_VITA")
      endif
      this.w_COE_RACC = "Files_x_Import\COE_RACC.DBF"
      if cp_FileExist(this.w_COE_RACC)
        SELECT * FROM (this.w_COE_RACC) INTO CURSOR TMP_COERACC
        if USED("TMP_COERACC")
          SELECT TMP_COERACC
          SCAN
          this.w_CRPRIBAS = CRPRIBAS
          this.w_CPROWNUM = CPROWNUM
          this.w_CRSECBAS = CRSECBAS
          this.w_CRCOERAC = CRCOERAC
          * --- Try
          local bErr_03791790
          bErr_03791790=bTrsErr
          this.Try_03791790()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
          endif
          bTrsErr=bTrsErr or bErr_03791790
          * --- End
          ENDSCAN
        endif
        USE IN SELECT ("TMP_COERACC")
        USE IN SELECT ("COE_RACC")
      endif
      this.w_INT_LEGA = "Files_x_Import\INT_LEGA.DBF"
      if cp_FileExist(this.w_INT_LEGA)
        SELECT * FROM (this.w_INT_LEGA) INTO CURSOR TMP_INTLEGA
        if USED("TMP_INTLEGA")
          SELECT TMP_INTLEGA
          SCAN
          this.w_INDATINI = INDATINI
          this.w_INTASLEG = INTASLEG
          * --- Try
          local bErr_036000F0
          bErr_036000F0=bTrsErr
          this.Try_036000F0()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
          endif
          bTrsErr=bTrsErr or bErr_036000F0
          * --- End
          ENDSCAN
        endif
        USE IN SELECT ("TMP_INTLEGA")
        USE IN SELECT ("INT_LEGA")
      endif
      this.w_SAG_MORA = "Files_x_Import\SAG_MORA.DBF"
      if cp_FileExist(this.w_SAG_MORA)
        SELECT * FROM (this.w_SAG_MORA) INTO CURSOR TMP_SAGMORA
        if USED("TMP_SAGMORA")
          SELECT TMP_SAGMORA
          SCAN
          this.w_IM__ANNO = IM__ANNO
          this.w_IMPERIOD = IMPERIOD
          this.w_IMDATFIN = IMDATFIN
          this.w_IMDATINI = IMDATINI
          this.w_IMDATFIN = IMDATFIN
          this.w_IMSAGRIF = Nvl(IMSAGRIF,0)
          this.w_IMMAGNDP = Nvl(IMMAGNDP,0)
          this.w_IMMAGDEP = Nvl(IMMAGDEP,0)
          * --- Try
          local bErr_035D2910
          bErr_035D2910=bTrsErr
          this.Try_035D2910()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- Try
            local bErr_035FECE0
            bErr_035FECE0=bTrsErr
            this.Try_035FECE0()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
            endif
            bTrsErr=bTrsErr or bErr_035FECE0
            * --- End
          endif
          bTrsErr=bTrsErr or bErr_035D2910
          * --- End
          ENDSCAN
        endif
        USE IN SELECT ("TMP_SAGMORA")
        USE IN SELECT ("SAG_MORA")
      endif
    else
      this.w_TMPC = ah_Msgformat("Procedura di conversione eseguita con successo")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    * --- Resoconto - sempre positivo (da gestire eventuali errori)
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Elaborazione terminata con successo")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = ah_Msgformat("Elaborazione terminata con successo")
  endproc
  proc Try_035FBEC0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.w_CV__ANNO = CV__ANNO
    this.w_CV__BASE = CV__BASE
    this.w_CVINDI01 = CVINDI01
    this.w_CVINDI02 = CVINDI02
    this.w_CVINDI03 = CVINDI03
    this.w_CVINDI04 = CVINDI04
    this.w_CVINDI05 = CVINDI05
    this.w_CVINDI06 = CVINDI06
    this.w_CVINDI07 = CVINDI07
    this.w_CVINDI08 = CVINDI08
    this.w_CVINDI09 = CVINDI09
    this.w_CVINDI10 = CVINDI10
    this.w_CVINDI11 = CVINDI11
    this.w_CVINDI12 = CVINDI12
    * --- Insert into COS_VITA
    i_nConn=i_TableProp[this.COS_VITA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.COS_VITA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.COS_VITA_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CV__ANNO"+",CV__BASE"+",CVINDI01"+",CVINDI02"+",CVINDI03"+",CVINDI04"+",CVINDI05"+",CVINDI06"+",CVINDI07"+",CVINDI08"+",CVINDI09"+",CVINDI10"+",CVINDI11"+",CVINDI12"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CV__ANNO),'COS_VITA','CV__ANNO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CV__BASE),'COS_VITA','CV__BASE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CVINDI01),'COS_VITA','CVINDI01');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CVINDI02),'COS_VITA','CVINDI02');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CVINDI03),'COS_VITA','CVINDI03');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CVINDI04),'COS_VITA','CVINDI04');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CVINDI05),'COS_VITA','CVINDI05');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CVINDI06),'COS_VITA','CVINDI06');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CVINDI07),'COS_VITA','CVINDI07');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CVINDI08),'COS_VITA','CVINDI08');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CVINDI09),'COS_VITA','CVINDI09');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CVINDI10),'COS_VITA','CVINDI10');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CVINDI11),'COS_VITA','CVINDI11');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CVINDI12),'COS_VITA','CVINDI12');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CV__ANNO',this.w_CV__ANNO,'CV__BASE',this.w_CV__BASE,'CVINDI01',this.w_CVINDI01,'CVINDI02',this.w_CVINDI02,'CVINDI03',this.w_CVINDI03,'CVINDI04',this.w_CVINDI04,'CVINDI05',this.w_CVINDI05,'CVINDI06',this.w_CVINDI06,'CVINDI07',this.w_CVINDI07,'CVINDI08',this.w_CVINDI08,'CVINDI09',this.w_CVINDI09,'CVINDI10',this.w_CVINDI10)
      insert into (i_cTable) (CV__ANNO,CV__BASE,CVINDI01,CVINDI02,CVINDI03,CVINDI04,CVINDI05,CVINDI06,CVINDI07,CVINDI08,CVINDI09,CVINDI10,CVINDI11,CVINDI12 &i_ccchkf. );
         values (;
           this.w_CV__ANNO;
           ,this.w_CV__BASE;
           ,this.w_CVINDI01;
           ,this.w_CVINDI02;
           ,this.w_CVINDI03;
           ,this.w_CVINDI04;
           ,this.w_CVINDI05;
           ,this.w_CVINDI06;
           ,this.w_CVINDI07;
           ,this.w_CVINDI08;
           ,this.w_CVINDI09;
           ,this.w_CVINDI10;
           ,this.w_CVINDI11;
           ,this.w_CVINDI12;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    this.w_TMPC = ah_Msgformat("Inserito costo vita  per anno %1", ALLTRIM(STR(this.w_CV__ANNO)))
    this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    return
  proc Try_037419B0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.w___BASE = 0
    this.w_INDI01 = 0
    this.w_INDI02 = 0
    this.w_INDI03 = 0
    this.w_INDI04 = 0
    this.w_INDI05 = 0
    this.w_INDI06 = 0
    this.w_INDI07 = 0
    this.w_INDI08 = 0
    this.w_INDI09 = 0
    this.w_INDI10 = 0
    this.w_INDI11 = 0
    this.w_INDI12 = 0
    * --- Read from COS_VITA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.COS_VITA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.COS_VITA_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CV__BASE,CVINDI01,CVINDI02,CVINDI03,CVINDI04,CVINDI05,CVINDI06,CVINDI07,CVINDI08,CVINDI09,CVINDI10,CVINDI11,CVINDI12"+;
        " from "+i_cTable+" COS_VITA where ";
            +"CV__ANNO = "+cp_ToStrODBC(this.w_CV__ANNO);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CV__BASE,CVINDI01,CVINDI02,CVINDI03,CVINDI04,CVINDI05,CVINDI06,CVINDI07,CVINDI08,CVINDI09,CVINDI10,CVINDI11,CVINDI12;
        from (i_cTable) where;
            CV__ANNO = this.w_CV__ANNO;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w___BASE = NVL(cp_ToDate(_read_.CV__BASE),cp_NullValue(_read_.CV__BASE))
      this.w_INDI01 = NVL(cp_ToDate(_read_.CVINDI01),cp_NullValue(_read_.CVINDI01))
      this.w_INDI02 = NVL(cp_ToDate(_read_.CVINDI02),cp_NullValue(_read_.CVINDI02))
      this.w_INDI03 = NVL(cp_ToDate(_read_.CVINDI03),cp_NullValue(_read_.CVINDI03))
      this.w_INDI04 = NVL(cp_ToDate(_read_.CVINDI04),cp_NullValue(_read_.CVINDI04))
      this.w_INDI05 = NVL(cp_ToDate(_read_.CVINDI05),cp_NullValue(_read_.CVINDI05))
      this.w_INDI06 = NVL(cp_ToDate(_read_.CVINDI06),cp_NullValue(_read_.CVINDI06))
      this.w_INDI07 = NVL(cp_ToDate(_read_.CVINDI07),cp_NullValue(_read_.CVINDI07))
      this.w_INDI08 = NVL(cp_ToDate(_read_.CVINDI08),cp_NullValue(_read_.CVINDI08))
      this.w_INDI09 = NVL(cp_ToDate(_read_.CVINDI09),cp_NullValue(_read_.CVINDI09))
      this.w_INDI10 = NVL(cp_ToDate(_read_.CVINDI10),cp_NullValue(_read_.CVINDI10))
      this.w_INDI11 = NVL(cp_ToDate(_read_.CVINDI11),cp_NullValue(_read_.CVINDI11))
      this.w_INDI12 = NVL(cp_ToDate(_read_.CVINDI12),cp_NullValue(_read_.CVINDI12))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if NOT(this.w_CV__BASE=this.w___BASE AND this.w_CVINDI01=this.w_INDI01 AND this.w_CVINDI02=this.w_INDI02 AND this.w_CVINDI03=this.w_INDI03 AND this.w_CVINDI04=this.w_INDI04 AND this.w_CVINDI05=this.w_INDI05 AND this.w_CVINDI06=this.w_INDI06 AND this.w_CVINDI07=this.w_INDI07 AND this.w_CVINDI08=this.w_INDI08 AND this.w_CVINDI09=this.w_INDI09 AND this.w_CVINDI10=this.w_INDI10 AND this.w_CVINDI11=this.w_INDI11 AND this.w_CVINDI12=this.w_INDI12)
      * --- Write into COS_VITA
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.COS_VITA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.COS_VITA_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.COS_VITA_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"CV__BASE ="+cp_NullLink(cp_ToStrODBC(this.w_CV__BASE),'COS_VITA','CV__BASE');
        +",CVINDI01 ="+cp_NullLink(cp_ToStrODBC(this.w_CVINDI01),'COS_VITA','CVINDI01');
        +",CVINDI02 ="+cp_NullLink(cp_ToStrODBC(this.w_CVINDI02),'COS_VITA','CVINDI02');
        +",CVINDI03 ="+cp_NullLink(cp_ToStrODBC(this.w_CVINDI03),'COS_VITA','CVINDI03');
        +",CVINDI04 ="+cp_NullLink(cp_ToStrODBC(this.w_CVINDI04),'COS_VITA','CVINDI04');
        +",CVINDI05 ="+cp_NullLink(cp_ToStrODBC(this.w_CVINDI05),'COS_VITA','CVINDI05');
        +",CVINDI06 ="+cp_NullLink(cp_ToStrODBC(this.w_CVINDI06),'COS_VITA','CVINDI06');
        +",CVINDI07 ="+cp_NullLink(cp_ToStrODBC(this.w_CVINDI07),'COS_VITA','CVINDI07');
        +",CVINDI08 ="+cp_NullLink(cp_ToStrODBC(this.w_CVINDI08),'COS_VITA','CVINDI08');
        +",CVINDI09 ="+cp_NullLink(cp_ToStrODBC(this.w_CVINDI09),'COS_VITA','CVINDI09');
        +",CVINDI10 ="+cp_NullLink(cp_ToStrODBC(this.w_CVINDI10),'COS_VITA','CVINDI10');
        +",CVINDI11 ="+cp_NullLink(cp_ToStrODBC(this.w_CVINDI11),'COS_VITA','CVINDI11');
        +",CVINDI12 ="+cp_NullLink(cp_ToStrODBC(this.w_CVINDI12),'COS_VITA','CVINDI12');
            +i_ccchkf ;
        +" where ";
            +"CV__ANNO = "+cp_ToStrODBC(this.w_CV__ANNO);
               )
      else
        update (i_cTable) set;
            CV__BASE = this.w_CV__BASE;
            ,CVINDI01 = this.w_CVINDI01;
            ,CVINDI02 = this.w_CVINDI02;
            ,CVINDI03 = this.w_CVINDI03;
            ,CVINDI04 = this.w_CVINDI04;
            ,CVINDI05 = this.w_CVINDI05;
            ,CVINDI06 = this.w_CVINDI06;
            ,CVINDI07 = this.w_CVINDI07;
            ,CVINDI08 = this.w_CVINDI08;
            ,CVINDI09 = this.w_CVINDI09;
            ,CVINDI10 = this.w_CVINDI10;
            ,CVINDI11 = this.w_CVINDI11;
            ,CVINDI12 = this.w_CVINDI12;
            &i_ccchkf. ;
         where;
            CV__ANNO = this.w_CV__ANNO;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      this.w_TMPC = ah_Msgformat("Aggiornato costo vita  per anno %1", ALLTRIM(STR(this.w_CV__ANNO)))
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    return
  proc Try_03791790()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into COE_RACC
    i_nConn=i_TableProp[this.COE_RACC_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.COE_RACC_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.COE_RACC_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CRPRIBAS"+",CPROWNUM"+",CRSECBAS"+",CRCOERAC"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CRPRIBAS),'COE_RACC','CRPRIBAS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'COE_RACC','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CRSECBAS),'COE_RACC','CRSECBAS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CRCOERAC),'COE_RACC','CRCOERAC');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CRPRIBAS',this.w_CRPRIBAS,'CPROWNUM',this.w_CPROWNUM,'CRSECBAS',this.w_CRSECBAS,'CRCOERAC',this.w_CRCOERAC)
      insert into (i_cTable) (CRPRIBAS,CPROWNUM,CRSECBAS,CRCOERAC &i_ccchkf. );
         values (;
           this.w_CRPRIBAS;
           ,this.w_CPROWNUM;
           ,this.w_CRSECBAS;
           ,this.w_CRCOERAC;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    this.w_TMPC = ah_Msgformat("Inserito coefficiente di raccordo prima base %1 seconda base %2", ALLTRIM(STR(this.w_CRPRIBAS)), ALLTRIM(STR(this.w_CRSECBAS)) )
    this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    return
  proc Try_036000F0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into INT_LEGA
    i_nConn=i_TableProp[this.INT_LEGA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INT_LEGA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.INT_LEGA_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"INDATINI"+",INTASLEG"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_INDATINI),'INT_LEGA','INDATINI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_INTASLEG),'INT_LEGA','INTASLEG');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'INDATINI',this.w_INDATINI,'INTASLEG',this.w_INTASLEG)
      insert into (i_cTable) (INDATINI,INTASLEG &i_ccchkf. );
         values (;
           this.w_INDATINI;
           ,this.w_INTASLEG;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    this.w_TMPC = ah_Msgformat("Inserito interesse legale data %1", DTOC(this.w_INDATINI) )
    this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    return
  proc Try_035D2910()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into INT_MORA
    i_nConn=i_TableProp[this.INT_MORA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INT_MORA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.INT_MORA_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"IM__ANNO"+",IMPERIOD"+",IMDATINI"+",IMDATFIN"+",IMSAGRIF"+",IMMAGNDP"+",IMMAGDEP"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_IM__ANNO),'INT_MORA','IM__ANNO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_IMPERIOD),'INT_MORA','IMPERIOD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_IMDATINI),'INT_MORA','IMDATINI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_IMDATFIN),'INT_MORA','IMDATFIN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_IMSAGRIF),'INT_MORA','IMSAGRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_IMMAGNDP),'INT_MORA','IMMAGNDP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_IMMAGDEP),'INT_MORA','IMMAGDEP');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'IM__ANNO',this.w_IM__ANNO,'IMPERIOD',this.w_IMPERIOD,'IMDATINI',this.w_IMDATINI,'IMDATFIN',this.w_IMDATFIN,'IMSAGRIF',this.w_IMSAGRIF,'IMMAGNDP',this.w_IMMAGNDP,'IMMAGDEP',this.w_IMMAGDEP)
      insert into (i_cTable) (IM__ANNO,IMPERIOD,IMDATINI,IMDATFIN,IMSAGRIF,IMMAGNDP,IMMAGDEP &i_ccchkf. );
         values (;
           this.w_IM__ANNO;
           ,this.w_IMPERIOD;
           ,this.w_IMDATINI;
           ,this.w_IMDATFIN;
           ,this.w_IMSAGRIF;
           ,this.w_IMMAGNDP;
           ,this.w_IMMAGDEP;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    this.w_TMPC = ah_Msgformat("Inserito saggio interesse di mora  per anno %1 %2 semestre", ALLTRIM(this.w_IM__ANNO),ALLTRIM(iif(this.w_IMPERIOD="P","primo","secondo")))
    this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    return
  proc Try_035FECE0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into INT_MORA
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.INT_MORA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INT_MORA_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.INT_MORA_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"IMDATINI ="+cp_NullLink(cp_ToStrODBC(this.w_IMDATINI),'INT_MORA','IMDATINI');
      +",IMDATFIN ="+cp_NullLink(cp_ToStrODBC(this.w_IMDATFIN),'INT_MORA','IMDATFIN');
      +",IMSAGRIF ="+cp_NullLink(cp_ToStrODBC(this.w_IMSAGRIF),'INT_MORA','IMSAGRIF');
      +",IMMAGNDP ="+cp_NullLink(cp_ToStrODBC(this.w_IMMAGNDP),'INT_MORA','IMMAGNDP');
      +",IMMAGDEP ="+cp_NullLink(cp_ToStrODBC(this.w_IMMAGDEP),'INT_MORA','IMMAGDEP');
          +i_ccchkf ;
      +" where ";
          +"IM__ANNO = "+cp_ToStrODBC(this.w_IM__ANNO);
          +" and IMPERIOD = "+cp_ToStrODBC(this.w_IMPERIOD);
             )
    else
      update (i_cTable) set;
          IMDATINI = this.w_IMDATINI;
          ,IMDATFIN = this.w_IMDATFIN;
          ,IMSAGRIF = this.w_IMSAGRIF;
          ,IMMAGNDP = this.w_IMMAGNDP;
          ,IMMAGDEP = this.w_IMMAGDEP;
          &i_ccchkf. ;
       where;
          IM__ANNO = this.w_IM__ANNO;
          and IMPERIOD = this.w_IMPERIOD;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    this.w_TMPC = ah_Msgformat("Aggiornato saggio interesse di mora per anno %1 %2 semestre", ALLTRIM(this.w_IM__ANNO),ALLTRIM(iif(this.w_IMPERIOD="P","primo","secondo")))
    this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='COS_VITA'
    this.cWorkTables[2]='COE_RACC'
    this.cWorkTables[3]='INT_LEGA'
    this.cWorkTables[4]='INT_MORA'
    return(this.OpenAllTables(4))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
