* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvab06                                                        *
*              Agg. tabelle lotti/caricamento rapido                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_62]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-12-02                                                      *
* Last revis.: 2006-12-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvab06",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvab06 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  * --- WorkFile variables
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questa procedura di conversione ricostruisce gli archivi
    *     legati a magazzino e produzione e caricamento rapido:
    *     GESPENNA - Tabella appoggio Caricamento rapido
    *     SPL_LOTT - Aggiornamento differito lotti
    *     AGG_LOTT - Aggiornamento differito lotti
    * --- Try
    local bErr_036196B0
    bErr_036196B0=bTrsErr
    this.Try_036196B0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        if g_MADV="S"
          this.w_TMPC = ah_Msgformat("ERRORE - impossibile aggiornare tabelle GESPENNA, AGG_LOTT, SPL_LOTT, AGG_MATR")
        else
          this.w_TMPC = ah_Msgformat("ERRORE - impossibile aggiornare tabelle GESPENNA")
        endif
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_036196B0
    * --- End
  endproc
  proc Try_036196B0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    if Not GSCV_BDT( this, "GESPENNA" , i_ServerConn[1,2] )
      * --- Raise
      i_Error="Error"
      return
    endif
    if g_MADV="S"
      if Not GSCV_BDT( this, "AGG_MATR" , i_ServerConn[1,2] )
        * --- Raise
        i_Error="Error"
        return
      endif
      if Not GSCV_BDT( this, "SPL_LOTT" , i_ServerConn[1,2] )
        * --- Raise
        i_Error="Error"
        return
      endif
      if Not GSCV_BDT( this, "AGG_LOTT" , i_ServerConn[1,2] )
        * --- Raise
        i_Error="Error"
        return
      endif
    endif
    * --- Ricostruisco il database per la tabella
    GSCV_BRT(this, "GESPENNA", i_ServerConn[1,2] , .F.)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if g_MADV="S"
      GSCV_BRT(this, "AGG_MATR", i_ServerConn[1,2] , .F.)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      GSCV_BRT(this, "SPL_LOTT", i_ServerConn[1,2] , .F.)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      GSCV_BRT(this, "AGG_LOTT", i_ServerConn[1,2] , .F.)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    if this.w_NHF>=0
      if g_MADV="S"
        this.w_TMPC = ah_Msgformat("Aggiornamento tabelle GESPENNA, AGG_LOTT, SPL_LOTT, AGG_MATR  eseguito con successo%0")
      else
        this.w_TMPC = ah_Msgformat("Aggiornamento tabelle GESPENNA eseguito con successo%0")
      endif
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = this.oParentObject.w_PMSG + this.w_TMPC 
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
