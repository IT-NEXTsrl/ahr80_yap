* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcb79                                                        *
*              Copia dati da Parametri contratti a Parametri Agenda            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-05-26                                                      *
* Last revis.: 2010-06-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcb79",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcb79 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_CODAZI = space(5)
  w_PCTIPDOC = space(5)
  w_PCTIPATT = space(20)
  w_PCGRUPAR = space(5)
  w_PCNOFLATT = space(1)
  w_PACODPER = space(3)
  w_PCNOFLDC = space(1)
  w_PACODDOC = space(3)
  w_PADESSUP = space(1)
  w_PCESCRIN = space(1)
  w_PCPERCON = space(3)
  w_PCRINCON = space(1)
  w_PCNUMGIO = 0
  * --- WorkFile variables
  OFF_ATTI_idx=0
  PAR_COAT_idx=0
  PAR_AGEN_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Copia dati da Parametri contratti a Parametri Agenda
    * --- FIle di LOG
    if g_AGEN="S" and not IsAlt()
      * --- Try
      local bErr_0476EF50
      bErr_0476EF50=bTrsErr
      this.Try_0476EF50()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- Gestisce log errori
        this.oParentObject.w_PMSG = "Errore nella copia dati da Parametri contratti a Parametri agenda:" + Message()
        this.oParentObject.w_PESEOK = .F.
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        if this.w_NHF>=0
          this.w_TMPC = this.oParentObject.w_PMSG
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
      bTrsErr=bTrsErr or bErr_0476EF50
      * --- End
    else
      * --- Esecuzione ok
      this.oParentObject.w_PESEOK = .T.
      this.w_TMPC = AH_MsgFormat("Copia dati da parametri contratti a parametri agenda eseguita correttamente")
      this.oParentObject.w_PMSG = this.w_TMPC
    endif
  endproc
  proc Try_0476EF50()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- copia dati da Parametri contratti a Parametri agenda
    this.w_CODAZI = i_codazi
    * --- Read from PAR_COAT
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_COAT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_COAT_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PCTIPDOC,PCTIPATT,PCGRUPAR,PCNOFLAT,PACODPER,PCNOFLDC,PACODDOC,PADESSUP,PCESCRIN,PCPERCON,PCRINCON,PCNUMGIO"+;
        " from "+i_cTable+" PAR_COAT where ";
            +"PCCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PCTIPDOC,PCTIPATT,PCGRUPAR,PCNOFLAT,PACODPER,PCNOFLDC,PACODDOC,PADESSUP,PCESCRIN,PCPERCON,PCRINCON,PCNUMGIO;
        from (i_cTable) where;
            PCCODAZI = this.w_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PCTIPDOC = NVL(cp_ToDate(_read_.PCTIPDOC),cp_NullValue(_read_.PCTIPDOC))
      this.w_PCTIPATT = NVL(cp_ToDate(_read_.PCTIPATT),cp_NullValue(_read_.PCTIPATT))
      this.w_PCGRUPAR = NVL(cp_ToDate(_read_.PCGRUPAR),cp_NullValue(_read_.PCGRUPAR))
      w_PCNOFLAT = NVL(cp_ToDate(_read_.PCNOFLAT),cp_NullValue(_read_.PCNOFLAT))
      this.w_PACODPER = NVL(cp_ToDate(_read_.PACODPER),cp_NullValue(_read_.PACODPER))
      this.w_PCNOFLDC = NVL(cp_ToDate(_read_.PCNOFLDC),cp_NullValue(_read_.PCNOFLDC))
      this.w_PACODDOC = NVL(cp_ToDate(_read_.PACODDOC),cp_NullValue(_read_.PACODDOC))
      this.w_PADESSUP = NVL(cp_ToDate(_read_.PADESSUP),cp_NullValue(_read_.PADESSUP))
      this.w_PCESCRIN = NVL(cp_ToDate(_read_.PCESCRIN),cp_NullValue(_read_.PCESCRIN))
      this.w_PCPERCON = NVL(cp_ToDate(_read_.PCPERCON),cp_NullValue(_read_.PCPERCON))
      this.w_PCRINCON = NVL(cp_ToDate(_read_.PCRINCON),cp_NullValue(_read_.PCRINCON))
      this.w_PCNUMGIO = NVL(cp_ToDate(_read_.PCNUMGIO),cp_NullValue(_read_.PCNUMGIO))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Write into PAR_AGEN
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PAR_AGEN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_AGEN_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_AGEN_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PCTIPDOC ="+cp_NullLink(cp_ToStrODBC(this.w_PCTIPDOC),'PAR_AGEN','PCTIPDOC');
      +",PCTIPATT ="+cp_NullLink(cp_ToStrODBC(this.w_PCTIPATT),'PAR_AGEN','PCTIPATT');
      +",PCGRUPAR ="+cp_NullLink(cp_ToStrODBC(this.w_PCGRUPAR),'PAR_AGEN','PCGRUPAR');
      +",PCNOFLAT ="+cp_NullLink(cp_ToStrODBC(w_PCNOFLAT),'PAR_AGEN','PCNOFLAT');
      +",PACODPER ="+cp_NullLink(cp_ToStrODBC(this.w_PACODPER),'PAR_AGEN','PACODPER');
      +",PCNOFLDC ="+cp_NullLink(cp_ToStrODBC(this.w_PCNOFLDC),'PAR_AGEN','PCNOFLDC');
      +",PACODDOC ="+cp_NullLink(cp_ToStrODBC(this.w_PACODDOC),'PAR_AGEN','PACODDOC');
      +",PADESSUP ="+cp_NullLink(cp_ToStrODBC(this.w_PADESSUP),'PAR_AGEN','PADESSUP');
      +",PCESCRIN ="+cp_NullLink(cp_ToStrODBC(this.w_PCESCRIN),'PAR_AGEN','PCESCRIN');
      +",PCPERCON ="+cp_NullLink(cp_ToStrODBC(this.w_PCPERCON),'PAR_AGEN','PCPERCON');
      +",PCRINCON ="+cp_NullLink(cp_ToStrODBC(this.w_PCRINCON),'PAR_AGEN','PCRINCON');
      +",PCNUMGIO ="+cp_NullLink(cp_ToStrODBC(this.w_PCNUMGIO),'PAR_AGEN','PCNUMGIO');
          +i_ccchkf ;
      +" where ";
          +"PACODAZI = "+cp_ToStrODBC(this.w_CODAZI);
             )
    else
      update (i_cTable) set;
          PCTIPDOC = this.w_PCTIPDOC;
          ,PCTIPATT = this.w_PCTIPATT;
          ,PCGRUPAR = this.w_PCGRUPAR;
          ,PCNOFLAT = w_PCNOFLAT;
          ,PACODPER = this.w_PACODPER;
          ,PCNOFLDC = this.w_PCNOFLDC;
          ,PACODDOC = this.w_PACODDOC;
          ,PADESSUP = this.w_PADESSUP;
          ,PCESCRIN = this.w_PCESCRIN;
          ,PCPERCON = this.w_PCPERCON;
          ,PCRINCON = this.w_PCRINCON;
          ,PCNUMGIO = this.w_PCNUMGIO;
          &i_ccchkf. ;
       where;
          PACODAZI = this.w_CODAZI;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    this.oParentObject.w_PESEOK = .T.
    this.w_TMPC = AH_MsgFormat("Copia dati da parametri contratti a parametri agenda eseguita correttamente")
    this.oParentObject.w_PMSG = this.w_TMPC
    * --- commit
    cp_EndTrs(.t.)
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='OFF_ATTI'
    this.cWorkTables[2]='PAR_COAT'
    this.cWorkTables[3]='PAR_AGEN'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
