* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvbb05                                                        *
*              Aggiornamento numero documento                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_100]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-11-08                                                      *
* Last revis.: 2007-05-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvbb05",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvbb05 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_MESS = space(10)
  w_LOOP = 0
  w_RicTable = .f.
  w_TabellaDb2 = space(10)
  w_TableError = space(10)
  w_kConn = 0
  w_NRES = 0
  w_TABLECODE = space(254)
  w_CHIAVE = space(254)
  w_POSIZALFA = 0
  w_ALFAAPPO = space(254)
  w_DOCUM = space(254)
  w_ALFA = space(254)
  w_NEWALFA = space(254)
  w_NEWCODE = space(254)
  w_CODAZI = space(5)
  * --- WorkFile variables
  AGG_ANAL_idx=0
  ASSESTAM_idx=0
  AZIENDA_idx=0
  CAU_CONT_idx=0
  CDC_MANU_idx=0
  CONTI_idx=0
  COR_RISP_idx=0
  DET_DIFF_idx=0
  DET_PIAS_idx=0
  DOC_MAST_idx=0
  DOCSMAST_idx=0
  ELEIMAST_idx=0
  FABBIDET_idx=0
  FAT_DIFF_idx=0
  INC_CORR_idx=0
  MOP_MAST_idx=0
  MVM_MAST_idx=0
  MVMSMAST_idx=0
  PAC_MAST_idx=0
  PAR_TITE_idx=0
  PAR_VDET_idx=0
  PARSTITE_idx=0
  PNT_MAST_idx=0
  PNTSMAST_idx=0
  RILEVAZI_idx=0
  RIPATMP1_idx=0
  SAL_DACO_idx=0
  SALMDACO_idx=0
  SCA_VARI_idx=0
  SCASVARI_idx=0
  TIP_DOCU_idx=0
  ZORDWEBM_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiornamento campi Numero Documento della procedura
    *     Modificati da Numerico 6 a numerico 15
    *     Per Sql problema con valore di Default
    *     DB2 non permette la modifica quindi bisogna eseguire il solito travaso di dati
    *     Oraclo permette la modifica
    * --- Try
    local bErr_0376BC20
    bErr_0376BC20=bTrsErr
    this.Try_0376BC20()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Gestisce log errori
      if i_ErrMsg<>"BACKUP"
        this.oParentObject.w_PMSG = ah_Msgformat("ERRORE GENERICO - impossibile eseguire aggiornamento richiesto, tabella: %1 %2", this.w_TableError , Message() )
      else
        this.oParentObject.w_PMSG = ah_Msgformat("Procedura annullata dall'utente")
      endif
      if this.w_NHF>=0
        this.w_TMPC = this.oParentObject.w_PMSG
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
      this.oParentObject.w_PESEOK = .F.
    endif
    bTrsErr=bTrsErr or bErr_0376BC20
    * --- End
  endproc
  proc Try_0376BC20()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if Upper(CP_DBTYPE)="DB2" And Not ah_YesNo("Prima di avviare la procedura di conversione accertarsi di avere eseguito un Backup corretto del database. Si desidera proseguire?")
      * --- Raise
      i_Error="Backup"
      return
    endif
    DECLARE ARRCAMPI(41,3)
    * --- ARRCAMPI( w_LOOP, 1) = Campo
    *     ARRCAMPI( w_LOOP, 2) = Tabella
    *     ARRCAMPI( w_LOOP, 3) = 'S' ricostruisce tabella 'N' non ricostruisce tabella
     
 ARRCAMPI(1,1) = "CCNUMDOC" 
 ARRCAMPI(1,2) = "CCM_MAST" 
 ARRCAMPI(1,3) = "S" 
 ARRCAMPI(2,1) = "MCNUMDOC" 
 ARRCAMPI(2,2) = "MOV_CESP" 
 ARRCAMPI(2,3) = "S" 
 ARRCAMPI(3,1) = "MVNUMDOC" 
 ARRCAMPI(3,2) = "DOC_MAST" 
 ARRCAMPI(3,3) = "N" 
 ARRCAMPI(4,1) = "MVNUMEST" 
 ARRCAMPI(4,2) = "DOC_MAST" 
 ARRCAMPI(4,3) = "N" 
 ARRCAMPI(5,1) = "MVNUMREG" 
 ARRCAMPI(5,2) = "DOC_MAST" 
 ARRCAMPI(5,3) = "S" 
 ARRCAMPI(6,1) = "PNNUMDOC" 
 ARRCAMPI(6,2) = "PNT_MAST" 
 ARRCAMPI(6,3) = "N" 
 ARRCAMPI(7,1) = "PNNUMPRO" 
 ARRCAMPI(7,2) = "PNT_MAST" 
 ARRCAMPI(7,3) = "S" 
 ARRCAMPI(8,1) = "MMNUMDOC" 
 ARRCAMPI(8,2) = "MVM_MAST" 
 ARRCAMPI(8,3) = "S" 
 ARRCAMPI(9,1) = "MDNUMDOC" 
 ARRCAMPI(9,2) = "COR_RISP" 
 ARRCAMPI(9,3) = "S" 
 ARRCAMPI(10,1) = "SCNUMDOC" 
 ARRCAMPI(10,2) = "SALMDACO" 
 ARRCAMPI(10,3) = "S" 
 ARRCAMPI(11,1) = "PTNUMDOC" 
 ARRCAMPI(11,2) = "PAR_TITE" 
 ARRCAMPI(11,3) = "S" 
 ARRCAMPI(12,1) = "SANUMDOC" 
 ARRCAMPI(12,2) = "SAL_DACO" 
 ARRCAMPI(12,3) = "S" 
 ARRCAMPI(13,1) = "SCNUMDOC" 
 ARRCAMPI(13,2) = "SCA_VARI" 
 ARRCAMPI(13,3) = "S" 
 ARRCAMPI(14,1) = "CMNUMDOC" 
 ARRCAMPI(14,2) = "CDC_MANU" 
 ARRCAMPI(14,3) = "S" 
 ARRCAMPI(15,1) = "MPNUMDOC" 
 ARRCAMPI(15,2) = "MOP_MAST" 
 ARRCAMPI(15,3) = "S" 
 ARRCAMPI(16,1) = "PSNUMDOC" 
 ARRCAMPI(16,2) = "FAT_DIFF" 
 ARRCAMPI(16,3) = "N" 
 ARRCAMPI(17,1) = "PSNUMINI" 
 ARRCAMPI(17,2) = "FAT_DIFF" 
 ARRCAMPI(17,3) = "N" 
 ARRCAMPI(18,1) = "PSNUMFIN" 
 ARRCAMPI(18,2) = "FAT_DIFF" 
 ARRCAMPI(18,3) = "S" 
 ARRCAMPI(19,1) = "DPNUMDOC" 
 ARRCAMPI(19,2) = "DET_PIAS" 
 ARRCAMPI(19,3) = "S" 
 ARRCAMPI(20,1) = "INNUMDOC" 
 ARRCAMPI(20,2) = "INC_CORR" 
 ARRCAMPI(20,3) = "S" 
 ARRCAMPI(21,1) = "ASNUMDOC" 
 ARRCAMPI(21,2) = "ASSESTAM" 
 ARRCAMPI(21,3) = "S" 
 ARRCAMPI(22,1) = "PINUMDOC" 
 ARRCAMPI(22,2) = "ELEIMAST" 
 ARRCAMPI(22,3) = "S" 
 ARRCAMPI(23,1) = "SCNUMDOC" 
 ARRCAMPI(23,2) = "SCASVARI" 
 ARRCAMPI(23,3) = "S" 
 ARRCAMPI(24,1) = "PTNUMDOC" 
 ARRCAMPI(24,2) = "PARSTITE" 
 ARRCAMPI(24,3) = "S" 
 ARRCAMPI(25,1) = "PLNUMDOC" 
 ARRCAMPI(25,2) = "PAC_MAST" 
 ARRCAMPI(25,3) = "S" 
 ARRCAMPI(26,1) = "DRNUMDOC" 
 ARRCAMPI(26,2) = "RILEVAZI" 
 ARRCAMPI(26,3) = "S" 
 ARRCAMPI(27,1) = "PNNUMDOC" 
 ARRCAMPI(27,2) = "PNTSMAST" 
 ARRCAMPI(27,3) = "N" 
 ARRCAMPI(28,1) = "PNNUMPRO" 
 ARRCAMPI(28,2) = "PNTSMAST" 
 ARRCAMPI(28,3) = "S" 
 ARRCAMPI(29,1) = "PTNUMDOC" 
 ARRCAMPI(29,2) = "PAR_TVEN" 
 ARRCAMPI(29,3) = "S" 
 ARRCAMPI(30,1) = "OFNUMDOC" 
 ARRCAMPI(30,2) = "OFF_ERTE" 
 ARRCAMPI(30,3) = "S" 
 ARRCAMPI(31,1) = "MRNUMDOC" 
 ARRCAMPI(31,2) = "MOV_RITE" 
 ARRCAMPI(31,3) = "S" 
 ARRCAMPI(32,1) = "CONUMDOC" 
 ARRCAMPI(32,2) = "CON_TENZ" 
 ARRCAMPI(32,3) = "S" 
 ARRCAMPI(33,1) = "MVNUMDOC" 
 ARRCAMPI(33,2) = "DOCSMAST" 
 ARRCAMPI(33,3) = "N" 
 ARRCAMPI(34,1) = "MVNUMEST" 
 ARRCAMPI(34,2) = "DOCSMAST" 
 ARRCAMPI(34,3) = "N" 
 ARRCAMPI(35,1) = "MVNUMREG" 
 ARRCAMPI(35,2) = "DOCSMAST" 
 ARRCAMPI(35,3) = "S" 
 ARRCAMPI(36,1) = "MMNUMDOC" 
 ARRCAMPI(36,2) = "MVMSMAST" 
 ARRCAMPI(36,3) = "S" 
 ARRCAMPI(37,1) = "ORNUMDOC" 
 ARRCAMPI(37,2) = "ZORDWEBM" 
 ARRCAMPI(37,3) = "S" 
 ARRCAMPI(38,1) = "MVNUMDOC" 
 ARRCAMPI(38,2) = "AGG_ANAL" 
 ARRCAMPI(38,3) = "S" 
 ARRCAMPI(39,1) = "DPNUMDOC" 
 ARRCAMPI(39,2) = "DET_DIFF" 
 ARRCAMPI(39,3) = "S" 
 ARRCAMPI(40,1) = "FBNUMIMP" 
 ARRCAMPI(40,2) = "FABBIDET" 
 ARRCAMPI(40,3) = "S" 
 ARRCAMPI(41,1) = "PAALFDOC" 
 ARRCAMPI(41,2) = "PAR_VDET" 
 ARRCAMPI(41,3) = "S"
    * --- Ricostruzione Tabella (lancia GSCV_BRT)
    *     Devo ricostruire quando ho modificato solo tutti i campi della stessa 
    *     Vedi DOC_MAST, PNT_MAST o FAT_DIFF
    this.w_RicTable = .T.
    this.w_LOOP = 1
    do while this.w_LOOP <= Alen(Arrcampi, 1)
      * --- Controllo presenza tabella
      this.w_kConn = i_TableProp[this.AZIENDA_IDX, 3]
      this.w_NRES = SqlExec(this.w_kConn, "select * from "+Alltrim(i_CODAZI) + Alltrim(ARRCAMPI( this.w_LOOP, 2) ) )
      if this.w_NRES<>-1
        * --- Se w_NRES <> 1 significa che la tabella non esiste (probabile assenza modulo)
        this.w_MESS = ""
        ah_Msg("Tabella: %1",.T.,.F.,.F., Alltrim(ArrCampi(this.w_LOOP,2)) )
        this.w_TableError = Alltrim(ArrCampi( this.w_LOOP , 2) )
        do case
          case upper(CP_DBTYPE)="SQLSERVER"
            if this.w_LOOP <> 37 And this.w_LOOP <> 41
              this.w_RicTable = .T.
              * --- Le tabelle che hanno pi� campi da modificare devono essere ricostruite solo 
              *     quando non hanno pi� campi da modificare
              if Arrcampi( this.w_LOOP,3 ) = "N"
                this.w_RicTable = .F.
              endif
              this.w_MESS = GSCV_BMN( this, ARRCAMPI( this.w_LOOP , 2 ), ARRCAMPI( this.w_LOOP , 1 ), this.w_RicTable )
            endif
          case upper(CP_DBTYPE)="DB2"
            this.w_TabellaDb2 = Alltrim(ArrCampi( this.w_LOOP , 2))
            do case
              case this.w_LOOP = 1
                this.Pag5()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              case this.w_LOOP = 2
                this.Pag6()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              case this.w_LOOP = 3
                this.Pag7()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              case this.w_LOOP = 6
                this.Pag8()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              case this.w_LOOP = 8
                this.Pag9()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              case this.w_LOOP = 9
                this.Pag10()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              case this.w_LOOP = 10
                this.Pag11()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              case this.w_LOOP = 11
                this.Pag12()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              case this.w_LOOP = 12
                this.Pag13()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              case this.w_LOOP = 13
                this.Pag14()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
            endcase
            do case
              case this.w_LOOP = 14
                this.Pag15()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              case this.w_LOOP = 15
                this.Pag16()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              case this.w_LOOP = 16
                this.Pag17()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              case this.w_LOOP = 19
                this.Pag18()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              case this.w_LOOP = 20
                this.Pag19()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              case this.w_LOOP = 21
                this.Pag20()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              case this.w_LOOP = 22
                this.Pag21()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              case this.w_LOOP = 23
                this.Pag22()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              case this.w_LOOP = 24
                this.Pag23()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              case this.w_LOOP = 25
                this.Pag24()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
            endcase
            do case
              case this.w_LOOP = 26
                this.Pag25()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              case this.w_LOOP = 27
                this.Pag26()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              case this.w_LOOP = 29
                this.Pag27()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              case this.w_LOOP = 30
                this.Pag28()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              case this.w_LOOP = 31
                this.Pag29()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              case this.w_LOOP = 32
                this.Pag30()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              case this.w_LOOP = 33
                this.Pag31()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              case this.w_LOOP = 36
                this.Pag32()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              case this.w_LOOP = 37
                this.Pag33()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              case this.w_LOOP = 38
                this.Pag34()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              case this.w_LOOP = 39
                this.Pag35()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              case this.w_LOOP = 40
                this.Pag36()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              case this.w_LOOP = 41
                this.Pag37()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
            endcase
            GSCV_BRI(this, this.w_TabellaDb2 , this.w_kConn , .T. )
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
        endcase
        this.w_TMPC = this.w_TMPC + this.w_MESS
        if Not Empty(this.w_MESS) And this.w_NHF>0 
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_MESS)
        endif
      endif
      this.w_LOOP = this.w_LOOP + 1
    enddo
    * --- Alla fine ricostruisco le casuali che puntano ad archivi ricostruiti sopra..
    if Upper(CP_DBTYPE)="DB2"
      this.w_TabellaDb2 = "TIP_DOCU"
      this.w_TableError = this.w_TabellaDb2
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      GSCV_BRI(this, this.w_TabellaDb2 , this.w_kConn , .T. )
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_TabellaDb2 = "CAU_CONT"
      this.w_TableError = this.w_TabellaDb2
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      GSCV_BRI(this, this.w_TabellaDb2 , this.w_kConn , .T.)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- Per la modifica agli ALFA documento, devono essere modificati tutti i progressivi
    *     che contengono questo campo (vedi progressivo numero documento)
     
 DECLARE ARRPROG(4) 
 ARRPROG(1)="prog\PRDOC" 
 ARRPROG(2)="prog\PRPRO" 
 ARRPROG(3)="prog\PRINCASS" 
 ARRPROG(4)="prog\PROFFERT"
    this.w_LOOP = 1
    this.w_CODAZI = i_CODAZI
     
 k_Conn=i_TableProp[this.AZIENDA_IDX, 3] 
 SqlExec(k_Conn, "Select * from CPWARN", "__CPWARN__")
    do while this.w_LOOP <= Alen(Arrprog, 1)
      this.w_TABLECODE = ""
      this.w_NEWCODE = ""
       
 Select("__CPWARN__") 
 Go Top 
 Scan
      if Left(TABLECODE, Len(Alltrim(ARRPROG( this.w_LOOP )))) = Alltrim(ARRPROG( this.w_LOOP )) And WARNCODE = i_CODAZI
        this.w_CHIAVE = TABLECODE
        this.w_TABLECODE = Alltrim(TABLECODE)
        this.w_POSIZALFA = Rat("'",Alltrim(Left(this.w_TABLECODE, Len(Alltrim(this.w_TABLECODE))-1)))
        this.w_ALFAAPPO = Left(this.w_TABLECODE, this.w_POSIZALFA)
        * --- w_DOCUM contiene la chiave del progressivo senza alfa
        this.w_DOCUM = LEFT(this.w_ALFAAPPO,RAT("'",ALLTRIM(this.w_ALFAAPPO))-1)
        * --- Controllo se ultimo carattere � \ altrimenti potrebbe voler dire che ho gi� un 
        *     progressivo con alfa di 10 caratteri
        if Right(Alltrim(this.w_DOCUM),1) = "\"
          ah_Msg("Aggiornamento progressivo: %1",.t.,.f.,.t.,Alltrim(this.w_CHIAVE))
          * --- w_ALFA contiene solo l'alfa di 2 caratteri
          this.w_ALFA = Substr(Alltrim(this.w_TABLECODE),this.w_POSIZALFA)
          this.w_NEWALFA = Left(this.w_ALFA,3)+Space(8)+"'"
          this.w_NEWCODE = Alltrim(this.w_DOCUM)+Alltrim(this.w_NEWALFA)
           
 SqlExec(k_Conn, "Update CPWARN set tablecode = "+cp_ToStrODBC(this.w_NEWCODE) +" where tablecode = "+cp_ToStrODBC(this.w_CHIAVE)+" and warncode = "+cp_ToStrODBC(this.w_CODAZI))
        endif
      endif
      EndScan
      this.w_LOOP = this.w_LOOP + 1
    enddo
    if Used("__CPWARN__")
       
 Select("__CPWARN__") 
 Use
    endif
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = Empty(this.w_TMPC)
    if this.oParentObject.w_PESEOK And this.w_NHF>0
      this.w_MESS = Ah_Msgformat("Aggiornamento campi numero e alfa documento avvenuto correttamente")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_MESS)
    endif
    this.oParentObject.w_PMSG = this.w_MESS
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella DB2
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.TIP_DOCU_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"* "," from "+i_cTable;
          )
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into TIP_DOCU
    i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.TIP_DOCU_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_Msg("Ripristinati valori di origine",.T.)
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    ah_Msg("Eliminata tabella temporanea tmp",.T.)
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella DB2
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.CAU_CONT_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.CAU_CONT_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"* "," from "+i_cTable;
          )
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into CAU_CONT
    i_nConn=i_TableProp[this.CAU_CONT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAU_CONT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.CAU_CONT_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_Msg("Ripristinati valori di origine",.T.)
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    ah_Msg("Eliminata tabella temporanea tmp",.T.)
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elimino la tabella
    if Not GSCV_BDT( this, this.w_TabellaDb2 , this.w_kConn )
      * --- Raise
      i_Error="Error"
      return
    endif
    * --- Ricostruisco il database per la tabella
    GSCV_BRT(this, this.w_TabellaDb2 , this.w_kConn , .F.)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Ricostruisce CCM_MAST
    GSCVBB05_B(this,this.w_kConn)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  procedure Pag6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Ricostruisce MOV_CESP
    GSCVBB05_C(this,this.w_kConn)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  procedure Pag7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella DB2
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.DOC_MAST_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"* "," from "+i_cTable;
          )
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into DOC_MAST
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.DOC_MAST_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_Msg("Ripristinati valori di origine",.T.)
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    ah_Msg("Eliminata tabella temporanea tmp",.T.)
  endproc


  procedure Pag8
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella DB2
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.PNT_MAST_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"* "," from "+i_cTable;
          )
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into PNT_MAST
    i_nConn=i_TableProp[this.PNT_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.PNT_MAST_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_Msg("Ripristinati valori di origine",.T.)
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    ah_Msg("Eliminata tabella temporanea tmp",.T.)
  endproc


  procedure Pag9
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella DB2
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.MVM_MAST_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.MVM_MAST_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"* "," from "+i_cTable;
          )
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into MVM_MAST
    i_nConn=i_TableProp[this.MVM_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MVM_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.MVM_MAST_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_Msg("Ripristinati valori di origine",.T.)
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    ah_Msg("Eliminata tabella temporanea tmp",.T.)
  endproc


  procedure Pag10
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella DB2
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.COR_RISP_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.COR_RISP_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"* "," from "+i_cTable;
          )
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into COR_RISP
    i_nConn=i_TableProp[this.COR_RISP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.COR_RISP_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.COR_RISP_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_Msg("Ripristinati valori di origine",.T.)
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    ah_Msg("Eliminata tabella temporanea tmp",.T.)
  endproc


  procedure Pag11
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella DB2
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.SALMDACO_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.SALMDACO_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"* "," from "+i_cTable;
          )
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into SALMDACO
    i_nConn=i_TableProp[this.SALMDACO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALMDACO_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.SALMDACO_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_Msg("Ripristinati valori di origine",.T.)
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    ah_Msg("Eliminata tabella temporanea tmp",.T.)
  endproc


  procedure Pag12
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella DB2
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.PAR_TITE_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"* "," from "+i_cTable;
          )
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into PAR_TITE
    i_nConn=i_TableProp[this.PAR_TITE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.PAR_TITE_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_Msg("Ripristinati valori di origine",.T.)
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    ah_Msg("Eliminata tabella temporanea tmp",.T.)
  endproc


  procedure Pag13
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella DB2
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.SAL_DACO_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.SAL_DACO_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"* "," from "+i_cTable;
          )
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into SAL_DACO
    i_nConn=i_TableProp[this.SAL_DACO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SAL_DACO_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.SAL_DACO_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_Msg("Ripristinati valori di origine",.T.)
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    ah_Msg("Eliminata tabella temporanea tmp",.T.)
  endproc


  procedure Pag14
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella DB2
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.SCA_VARI_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.SCA_VARI_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"* "," from "+i_cTable;
          )
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into SCA_VARI
    i_nConn=i_TableProp[this.SCA_VARI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SCA_VARI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.SCA_VARI_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_Msg("Ripristinati valori di origine",.T.)
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    ah_Msg("Eliminata tabella temporanea tmp",.T.)
  endproc


  procedure Pag15
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella DB2
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.CDC_MANU_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.CDC_MANU_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"* "," from "+i_cTable;
          )
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into CDC_MANU
    i_nConn=i_TableProp[this.CDC_MANU_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CDC_MANU_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.CDC_MANU_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_Msg("Ripristinati valori di origine",.T.)
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    ah_Msg("Eliminata tabella temporanea tmp",.T.)
  endproc


  procedure Pag16
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella DB2
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.MOP_MAST_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.MOP_MAST_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"* "," from "+i_cTable;
          )
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into MOP_MAST
    i_nConn=i_TableProp[this.MOP_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOP_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.MOP_MAST_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_Msg("Ripristinati valori di origine",.T.)
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    ah_Msg("Eliminata tabella temporanea tmp",.T.)
  endproc


  procedure Pag17
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella DB2
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.FAT_DIFF_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.FAT_DIFF_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"* "," from "+i_cTable;
          )
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into FAT_DIFF
    i_nConn=i_TableProp[this.FAT_DIFF_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.FAT_DIFF_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.FAT_DIFF_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_Msg("Ripristinati valori di origine",.T.)
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    ah_Msg("Eliminata tabella temporanea tmp",.T.)
  endproc


  procedure Pag18
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella DB2
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.DET_PIAS_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.DET_PIAS_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"* "," from "+i_cTable;
          )
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into DET_PIAS
    i_nConn=i_TableProp[this.DET_PIAS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DET_PIAS_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.DET_PIAS_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_Msg("Ripristinati valori di origine",.T.)
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    ah_Msg("Eliminata tabella temporanea tmp",.T.)
  endproc


  procedure Pag19
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella DB2
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.INC_CORR_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.INC_CORR_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"* "," from "+i_cTable;
          )
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into INC_CORR
    i_nConn=i_TableProp[this.INC_CORR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INC_CORR_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.INC_CORR_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_Msg("Ripristinati valori di origine",.T.)
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    ah_Msg("Eliminata tabella temporanea tmp",.T.)
  endproc


  procedure Pag20
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella DB2
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.ASSESTAM_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.ASSESTAM_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"* "," from "+i_cTable;
          )
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into ASSESTAM
    i_nConn=i_TableProp[this.ASSESTAM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ASSESTAM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.ASSESTAM_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_Msg("Ripristinati valori di origine",.T.)
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    ah_Msg("Eliminata tabella temporanea tmp",.T.)
  endproc


  procedure Pag21
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella DB2
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.ELEIMAST_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.ELEIMAST_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"* "," from "+i_cTable;
          )
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into ELEIMAST
    i_nConn=i_TableProp[this.ELEIMAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ELEIMAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.ELEIMAST_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_Msg("Ripristinati valori di origine",.T.)
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    ah_Msg("Eliminata tabella temporanea tmp",.T.)
  endproc


  procedure Pag22
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella DB2
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.SCASVARI_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.SCASVARI_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"* "," from "+i_cTable;
          )
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into SCASVARI
    i_nConn=i_TableProp[this.SCASVARI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SCASVARI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.SCASVARI_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_Msg("Ripristinati valori di origine",.T.)
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    ah_Msg("Eliminata tabella temporanea tmp",.T.)
  endproc


  procedure Pag23
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella DB2
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.PARSTITE_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.PARSTITE_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"* "," from "+i_cTable;
          )
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into PARSTITE
    i_nConn=i_TableProp[this.PARSTITE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PARSTITE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.PARSTITE_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_Msg("Ripristinati valori di origine",.T.)
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    ah_Msg("Eliminata tabella temporanea tmp",.T.)
  endproc


  procedure Pag24
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella DB2
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.PAC_MAST_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.PAC_MAST_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"* "," from "+i_cTable;
          )
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into PAC_MAST
    i_nConn=i_TableProp[this.PAC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAC_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.PAC_MAST_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_Msg("Ripristinati valori di origine",.T.)
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    ah_Msg("Eliminata tabella temporanea tmp",.T.)
  endproc


  procedure Pag25
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella DB2
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.RILEVAZI_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.RILEVAZI_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"* "," from "+i_cTable;
          )
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into RILEVAZI
    i_nConn=i_TableProp[this.RILEVAZI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.RILEVAZI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.RILEVAZI_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_Msg("Ripristinati valori di origine",.T.)
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    ah_Msg("Eliminata tabella temporanea tmp",.T.)
  endproc


  procedure Pag26
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella DB2
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.PNTSMAST_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.PNTSMAST_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"* "," from "+i_cTable;
          )
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into PNTSMAST
    i_nConn=i_TableProp[this.PNTSMAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PNTSMAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.PNTSMAST_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_Msg("Ripristinati valori di origine",.T.)
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    ah_Msg("Eliminata tabella temporanea tmp",.T.)
  endproc


  procedure Pag27
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Ricostruisce PAR_TVEN
    GSCVBB05_P(this,this.w_kConn)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  procedure Pag28
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Ricostruisce OFF_ERTE
    GSCVBB05_O(this,this.w_kConn)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  procedure Pag29
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Ricostruisce MOV_RITE
    GSCVBB05_R(this,this.w_kConn)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  procedure Pag30
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Ricostruisce CON_TENZ
    GSCVBB05_S(this,this.w_kConn)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  procedure Pag31
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella DB2
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.DOCSMAST_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.DOCSMAST_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"* "," from "+i_cTable;
          )
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into DOCSMAST
    i_nConn=i_TableProp[this.DOCSMAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOCSMAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.DOCSMAST_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_Msg("Ripristinati valori di origine",.T.)
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    ah_Msg("Eliminata tabella temporanea tmp",.T.)
  endproc


  procedure Pag32
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella DB2
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.MVMSMAST_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.MVMSMAST_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"* "," from "+i_cTable;
          )
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into MVMSMAST
    i_nConn=i_TableProp[this.MVMSMAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MVMSMAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.MVMSMAST_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_Msg("Ripristinati valori di origine",.T.)
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    ah_Msg("Eliminata tabella temporanea tmp",.T.)
  endproc


  procedure Pag33
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella DB2
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.ZORDWEBM_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.ZORDWEBM_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"* "," from "+i_cTable;
          )
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into ZORDWEBM
    i_nConn=i_TableProp[this.ZORDWEBM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ZORDWEBM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.ZORDWEBM_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_Msg("Ripristinati valori di origine",.T.)
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    ah_Msg("Eliminata tabella temporanea tmp",.T.)
  endproc


  procedure Pag34
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella DB2
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.AGG_ANAL_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.AGG_ANAL_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"* "," from "+i_cTable;
          )
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into AGG_ANAL
    i_nConn=i_TableProp[this.AGG_ANAL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AGG_ANAL_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.AGG_ANAL_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_Msg("Ripristinati valori di origine",.T.)
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    ah_Msg("Eliminata tabella temporanea tmp",.T.)
  endproc


  procedure Pag35
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella DB2
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.DET_DIFF_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.DET_DIFF_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"* "," from "+i_cTable;
          )
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into DET_DIFF
    i_nConn=i_TableProp[this.DET_DIFF_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DET_DIFF_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.DET_DIFF_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_Msg("Ripristinati valori di origine",.T.)
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    ah_Msg("Eliminata tabella temporanea tmp",.T.)
  endproc


  procedure Pag36
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella DB2
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.FABBIDET_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.FABBIDET_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"* "," from "+i_cTable;
          )
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into FABBIDET
    i_nConn=i_TableProp[this.FABBIDET_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.FABBIDET_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.FABBIDET_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_Msg("Ripristinati valori di origine",.T.)
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    ah_Msg("Eliminata tabella temporanea tmp",.T.)
  endproc


  procedure Pag37
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella DB2
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.PAR_VDET_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_VDET_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"* "," from "+i_cTable;
          )
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into PAR_VDET
    i_nConn=i_TableProp[this.PAR_VDET_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_VDET_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.PAR_VDET_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_Msg("Ripristinati valori di origine",.T.)
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    ah_Msg("Eliminata tabella temporanea tmp",.T.)
  endproc


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,32)]
    this.cWorkTables[1]='AGG_ANAL'
    this.cWorkTables[2]='ASSESTAM'
    this.cWorkTables[3]='AZIENDA'
    this.cWorkTables[4]='CAU_CONT'
    this.cWorkTables[5]='CDC_MANU'
    this.cWorkTables[6]='CONTI'
    this.cWorkTables[7]='COR_RISP'
    this.cWorkTables[8]='DET_DIFF'
    this.cWorkTables[9]='DET_PIAS'
    this.cWorkTables[10]='DOC_MAST'
    this.cWorkTables[11]='DOCSMAST'
    this.cWorkTables[12]='ELEIMAST'
    this.cWorkTables[13]='FABBIDET'
    this.cWorkTables[14]='FAT_DIFF'
    this.cWorkTables[15]='INC_CORR'
    this.cWorkTables[16]='MOP_MAST'
    this.cWorkTables[17]='MVM_MAST'
    this.cWorkTables[18]='MVMSMAST'
    this.cWorkTables[19]='PAC_MAST'
    this.cWorkTables[20]='PAR_TITE'
    this.cWorkTables[21]='PAR_VDET'
    this.cWorkTables[22]='PARSTITE'
    this.cWorkTables[23]='PNT_MAST'
    this.cWorkTables[24]='PNTSMAST'
    this.cWorkTables[25]='RILEVAZI'
    this.cWorkTables[26]='*RIPATMP1'
    this.cWorkTables[27]='SAL_DACO'
    this.cWorkTables[28]='SALMDACO'
    this.cWorkTables[29]='SCA_VARI'
    this.cWorkTables[30]='SCASVARI'
    this.cWorkTables[31]='TIP_DOCU'
    this.cWorkTables[32]='ZORDWEBM'
    return(this.OpenAllTables(32))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
