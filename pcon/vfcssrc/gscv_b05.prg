* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscv_b05                                                        *
*              Conv.provvigioni (Euro-kit)                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_47]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-06-04                                                      *
* Last revis.: 2001-06-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscv_b05",oParentObject,m.w_NHF)
return(i_retval)

define class tgscv_b05 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_CODEUR = space(3)
  w_DECEUR = 0
  w_CODLIR = space(3)
  w_OBSO = ctod("  /  /  ")
  w_TmpN = 0
  w_TASSOEUR = 0
  w_TASSOLIT = 0
  w_PPMAXESC = 0
  w_PPMAXNES = 0
  w_PPMINESC = 0
  w_PPIMPIN2 = 0
  w_PPIMPPL2 = 0
  w_PPMINNES = 0
  w_PPIMPIN1 = 0
  w_PPIMPPL1 = 0
  w_PPIMPPL2 = 0
  w_OBSO = ctod("  /  /  ")
  w_TMPC = space(10)
  w_TEMP = space(10)
  w_SIMLIR = space(3)
  w_SIMEUR = space(3)
  w_CODAZI = space(5)
  w_oMess = .NULL.
  w_oPart = .NULL.
  * --- WorkFile variables
  AZIENDA_idx=0
  PAR_PROV_idx=0
  VALUTE_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- FIle di LOG
    * --- Visibile dal padre
    * --- Variabili locali
    * --- Data Obsolescenza
    this.w_OBSO = cp_CharToDate("01-01-2002")
    this.w_CODAZI = i_CODAZI
    * --- Leggo il codice valuta Euro e Lire
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZVALEUR,AZVALLIR"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZVALEUR,AZVALLIR;
        from (i_cTable) where;
            AZCODAZI = i_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CODEUR = NVL(cp_ToDate(_read_.AZVALEUR),cp_NullValue(_read_.AZVALEUR))
      this.w_CODLIR = NVL(cp_ToDate(_read_.AZVALLIR),cp_NullValue(_read_.AZVALLIR))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Controllo 1 - Devono essere caricati i dati azienda
    if Empty ( this.w_CODLIR ) Or Empty ( this.w_CODEUR )
      this.oParentObject.w_PMSG = ah_Msgformat("ERRORE 001 - codice Euro o valuta nazionale non definiti in dati azienda")
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE 001 - codice Euro o valuta nazionale non definiti in dati azienda")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
      i_retcode = 'stop'
      return
    endif
    * --- Controllo 2 - L'esercizio corrente deve essere EURO
    if g_PERVAL <> g_CODEUR
      this.oParentObject.w_PMSG = ah_Msgformat("ERRORE 002 - la valuta di conto dell'esercizio corrente deve essere Euro")
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE 002 - la valuta di conto dell'esercizio corrente deve essere Euro")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
      i_retcode = 'stop'
      return
      this.w_TMPC = this.w_TMPC+TRAN(this.w_PPMAXESC,"999,999,999.99")+CHR(13)+CHR(10)
    endif
    * --- Leggo i tassi fissi di Euro (1) e Lire (1936,27)
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VACAOVAL,VADECTOT,VASIMVAL"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(this.w_CODEUR);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VACAOVAL,VADECTOT,VASIMVAL;
        from (i_cTable) where;
            VACODVAL = this.w_CODEUR;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_TASSOEUR = NVL(cp_ToDate(_read_.VACAOVAL),cp_NullValue(_read_.VACAOVAL))
      this.w_DECEUR = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
      this.w_SIMEUR = NVL(cp_ToDate(_read_.VASIMVAL),cp_NullValue(_read_.VASIMVAL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VACAOVAL,VASIMVAL"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(this.w_CODLIR);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VACAOVAL,VASIMVAL;
        from (i_cTable) where;
            VACODVAL = this.w_CODLIR;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_TASSOLIT = NVL(cp_ToDate(_read_.VACAOVAL),cp_NullValue(_read_.VACAOVAL))
      this.w_SIMLIR = NVL(cp_ToDate(_read_.VASIMVAL),cp_NullValue(_read_.VASIMVAL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Oggetto per messaggi incrementali
    this.w_oMess=createobject("Ah_Message")
    * --- Try
    local bErr_036196B0
    bErr_036196B0=bTrsErr
    this.Try_036196B0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      this.w_TMPC = ah_Msgformat("ERRORE GENERICO - operazione sospesa")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    bTrsErr=bTrsErr or bErr_036196B0
    * --- End
  endproc
  proc Try_036196B0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Traduco in Euro gli importi per i parametri provvigioni
    * --- Select from PAR_PROV
    i_nConn=i_TableProp[this.PAR_PROV_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROV_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" PAR_PROV ";
          +" where PPCODAZI="+cp_ToStrODBC(this.w_CODAZI)+"";
           ,"_Curs_PAR_PROV")
    else
      select * from (i_cTable);
       where PPCODAZI=this.w_CODAZI;
        into cursor _Curs_PAR_PROV
    endif
    if used('_Curs_PAR_PROV')
      select _Curs_PAR_PROV
      locate for 1=1
      do while not(eof())
      this.w_PPMAXESC = Nvl ( _Curs_PAR_PROV.PPMAXESC , 0 )
      this.w_oPart = this.w_oMess.AddMsgPartNL("ENASARCO (max impon.monomandatari): da %1 %2 a %3 %4")
      this.w_oPart.AddParam(this.w_CODLIR)     
      this.w_oPart.AddParam(TRAN(this.w_PPMAXESC,"99,999,999,999"))     
      this.w_oPart.AddParam(this.w_SIMEUR)     
      if this.w_PPMAXESC<>0
        this.w_PPMAXESC = cp_round( this.w_PPMAXESC / g_CAOEUR, this.w_DECEUR)
      endif
      this.w_oPart.AddParam(TRAN(this.w_PPMAXESC,"99,999,999,999"))     
      * --- -----------------------------------------------------------------
      this.w_PPMAXNES = Nvl ( _Curs_PAR_PROV.PPMAXNES , 0 )
      this.w_oPart = this.w_oMess.AddMsgPartNL("ENASARCO (max impon.plurimandatari): da %1 %2 a %3 %4")
      this.w_oPart.AddParam(this.w_CODLIR)     
      this.w_oPart.AddParam(TRAN(this.w_PPMAXNES,"99,999,999,999"))     
      this.w_oPart.AddParam(this.w_SIMEUR)     
      if this.w_PPMAXNES<>0
        this.w_PPMAXNES = cp_round( this.w_PPMAXNES / g_CAOEUR, this.w_DECEUR)
      endif
      this.w_oPart.AddParam(TRAN(this.w_PPMAXNES,"99,999,999,999"))     
      * --- -----------------------------------------------------------------
      this.w_PPMINESC = Nvl ( _Curs_PAR_PROV.PPMINESC , 0 )
      this.w_oPart = this.w_oMess.AddMsgPartNL("ENASARCO (min importo monomand.): da %1 %2 a %3 %4")
      this.w_oPart.AddParam(this.w_CODLIR)     
      this.w_oPart.AddParam(TRAN(this.w_PPMINESC,"99,999,999,999"))     
      this.w_oPart.AddParam(this.w_SIMEUR)     
      if this.w_PPMINESC<>0
        this.w_PPMINESC = cp_round( this.w_PPMINESC / g_CAOEUR, this.w_DECEUR)
      endif
      this.w_oPart.AddParam(TRAN(this.w_PPMINESC,"99,999,999,999"))     
      * --- -----------------------------------------------------------------
      this.w_PPMINNES = Nvl ( _Curs_PAR_PROV.PPMINNES , 0 )
      this.w_oPart = this.w_oMess.AddMsgPartNL("ENASARCO (min importo plurimand.): da %1 %2 a %3 %4")
      this.w_oPart.AddParam(this.w_CODLIR)     
      this.w_oPart.AddParam(TRAN(this.w_PPMINNES,"99,999,999,999"))     
      this.w_oPart.AddParam(this.w_SIMEUR)     
      if this.w_PPMINNES<>0
        this.w_PPMINNES = cp_round( this.w_PPMINNES / g_CAOEUR, this.w_DECEUR)
      endif
      this.w_oPart.AddParam(TRAN(this.w_PPMINNES,"99,999,999,999"))     
      * --- -----------------------------------------------------------------
      this.w_PPIMPIN1 = Nvl ( _Curs_PAR_PROV.PPIMPIN1 , 0 )
      this.w_oPart = this.w_oMess.AddMsgPartNL("FIRR (primo scaglione monomand.): da %1 %2 a %3 %4")
      this.w_oPart.AddParam(this.w_CODLIR)     
      this.w_oPart.AddParam(TRAN(this.w_PPIMPIN1,"99,999,999,999"))     
      this.w_oPart.AddParam(this.w_SIMEUR)     
      if this.w_PPIMPIN1<>0
        this.w_PPIMPIN1 = cp_round( this.w_PPIMPIN1 / g_CAOEUR, this.w_DECEUR)
      endif
      this.w_oPart.AddParam(TRAN(this.w_PPIMPIN1,"99,999,999,999"))     
      * --- -----------------------------------------------------------------
      this.w_PPIMPPL1 = Nvl ( _Curs_PAR_PROV.PPIMPPL1 , 0 )
      this.w_oPart = this.w_oMess.AddMsgPartNL("FIRR (primo scaglione plurimand.): da %1 %2 a %3 %4")
      this.w_oPart.AddParam(this.w_CODLIR)     
      this.w_oPart.AddParam(TRAN(this.w_PPIMPPL1,"99,999,999,999"))     
      this.w_oPart.AddParam(this.w_SIMEUR)     
      if this.w_PPIMPPL1<>0
        this.w_PPIMPPL1 = cp_round( this.w_PPIMPPL1 / g_CAOEUR, this.w_DECEUR)
      endif
      this.w_oPart.AddParam(TRAN(this.w_PPIMPPL1,"99,999,999,999"))     
      * --- -----------------------------------------------------------------
      this.w_PPIMPIN2 = Nvl ( _Curs_PAR_PROV.PPIMPIN2 , 0 )
      this.w_oPart = this.w_oMess.AddMsgPartNL("FIRR (secondo scaglione monomand.): da %1 %2 a %3 %4")
      this.w_oPart.AddParam(this.w_CODLIR)     
      this.w_oPart.AddParam(TRAN(this.w_PPIMPIN2,"99,999,999,999"))     
      this.w_oPart.AddParam(this.w_SIMEUR)     
      if this.w_PPIMPIN2<>0
        this.w_PPIMPIN2 = cp_round( this.w_PPIMPIN2 / g_CAOEUR, this.w_DECEUR)
      endif
      this.w_oPart.AddParam(TRAN(this.w_PPIMPIN2,"99,999,999,999"))     
      * --- -----------------------------------------------------------------
      this.w_PPIMPPL2 = Nvl ( _Curs_PAR_PROV.PPIMPPL2 , 0 )
      this.w_oPart = this.w_oMess.AddMsgPartNL("FIRR (secondo scaglione plurimand.): da %1 %2 a %3 %4")
      this.w_oPart.AddParam(this.w_CODLIR)     
      this.w_oPart.AddParam(TRAN(this.w_PPIMPPL2,"99,999,999,999"))     
      this.w_oPart.AddParam(this.w_SIMEUR)     
      if this.w_PPIMPPL2<>0
        this.w_PPIMPPL2 = cp_round( this.w_PPIMPPL2 / g_CAOEUR, this.w_DECEUR)
      endif
      this.w_oPart.AddParam(TRAN(this.w_PPIMPPL2,"99,999,999,999"))     
      this.w_TMPC = this.w_oMess.ComposeMessage()
      * --- Write into PAR_PROV
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.PAR_PROV_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROV_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_PROV_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"PPMAXESC ="+cp_NullLink(cp_ToStrODBC(this.w_PPMAXESC),'PAR_PROV','PPMAXESC');
        +",PPMINESC ="+cp_NullLink(cp_ToStrODBC(this.w_PPMINESC),'PAR_PROV','PPMINESC');
        +",PPMAXNES ="+cp_NullLink(cp_ToStrODBC(this.w_PPMAXNES),'PAR_PROV','PPMAXNES');
        +",PPMINNES ="+cp_NullLink(cp_ToStrODBC(this.w_PPMINNES),'PAR_PROV','PPMINNES');
        +",PPIMPIN1 ="+cp_NullLink(cp_ToStrODBC(this.w_PPIMPIN1),'PAR_PROV','PPIMPIN1');
        +",PPIMPIN2 ="+cp_NullLink(cp_ToStrODBC(this.w_PPIMPIN2),'PAR_PROV','PPIMPIN2');
        +",PPIMPPL1 ="+cp_NullLink(cp_ToStrODBC(this.w_PPIMPPL1),'PAR_PROV','PPIMPPL1');
        +",PPIMPPL2 ="+cp_NullLink(cp_ToStrODBC(this.w_PPIMPPL2),'PAR_PROV','PPIMPPL2');
            +i_ccchkf ;
        +" where ";
            +"PPCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
               )
      else
        update (i_cTable) set;
            PPMAXESC = this.w_PPMAXESC;
            ,PPMINESC = this.w_PPMINESC;
            ,PPMAXNES = this.w_PPMAXNES;
            ,PPMINNES = this.w_PPMINNES;
            ,PPIMPIN1 = this.w_PPIMPIN1;
            ,PPIMPIN2 = this.w_PPIMPIN2;
            ,PPIMPPL1 = this.w_PPIMPPL1;
            ,PPIMPPL2 = this.w_PPIMPPL2;
            &i_ccchkf. ;
         where;
            PPCODAZI = this.w_CODAZI;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      if this.w_NHF>=0
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
        select _Curs_PAR_PROV
        continue
      enddo
      use
    endif
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = ah_Msgformat("Elaborazione terminata con successo")
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='PAR_PROV'
    this.cWorkTables[3]='VALUTE'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_PAR_PROV')
      use in _Curs_PAR_PROV
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
