* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscv_bb5                                                        *
*              Modifica chiave primaria pianocon                               *
*                                                                              *
*      Author: Zucchetti Tam S.p.A                                             *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_167]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-07-12                                                      *
* Last revis.: 2001-07-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscv_bb5",oParentObject,m.w_NHF)
return(i_retval)

define class tgscv_bb5 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  * --- WorkFile variables
  PIANOCON_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- FIle di LOG
    * --- Try
    local bErr_03750830
    bErr_03750830=bTrsErr
    this.Try_03750830()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE - impossibile assegnare la proprietÓ NOT NULL")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_03750830
    * --- End
  endproc
  proc Try_03750830()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    i_nConn=i_TableProp[this.PIANOCON_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.PIANOCON_idx,2])
    if i_nConn<>0
      * --- Elimino la tabella
      if Not GSCV_BDT( this, "PIANOCON" , i_nConn )
        * --- Raise
        i_Error="Error"
        return
      endif
      * --- Ricostruisco il database per la tabella
      GSCV_BRT(this, "PIANOCON" , i_nConn , .T.)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if bTrsErr
        * --- Raise
        i_Error="Impossibile assegnare la proprietÓ not null"
        return
      else
        if this.w_NHF>=0
          this.w_TMPC = ah_Msgformat("Adeguamento chiave primaria tabella PIANOCON eseguito correttamente%0ATTENZIONE: occorre andare in MANUTENZIONE DATABASE, selezionare la tabella%0PIANOCON e premere <OK>")
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
    endif
    this.oParentObject.w_PESEOK = .T.
    this.w_TMPC = ah_Msgformat("Adeguamento chiave primaria tabella PIANOCON eseguito correttamente%0ATTENZIONE: occorre andare in MANUTENZIONE DATABASE, selezionare la tabella%0PIANOCON e premere <OK>")
    this.oParentObject.w_PMSG = this.w_TMPC
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='PIANOCON'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
