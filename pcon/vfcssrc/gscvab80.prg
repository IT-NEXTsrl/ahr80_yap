* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvab80                                                        *
*              3.0 - aggiornamento anflgcpz/agflgcpz su conti/agenti           *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_96]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-03-17                                                      *
* Last revis.: 2006-12-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvab80",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvab80 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TMPC = space(10)
  w_TEMP = space(10)
  * --- WorkFile variables
  CONTI_idx=0
  AGENTI_idx=0
  MAGAZZIN_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Procedura che aggiorna i campi ANFLGCPZ/AGFLGCPZ su CONTI/AGENTI.
    *     Il flag viene posto a 'S' per tutte le anagrafiche con P.IVA e/o Codice Fiscale non vuoti.
    * --- FIle di LOG
    * --- Try
    local bErr_04A9F9C0
    bErr_04A9F9C0=bTrsErr
    this.Try_04A9F9C0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE GENERICO - Impossibile aggiornare tabella %1", "CONTI/AGENTI")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_04A9F9C0
    * --- End
  endproc
  proc Try_04A9F9C0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Aggiornamento ANFLGCPZ su tabella conti (la query seleziona i clienti/fornitori con P.IVA e/o Codice Fiscale NON vuoti)
    * --- Write into CONTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.CONTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="ANTIPCON,ANCODICE"
      do vq_exec with 'gscvab02a',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTI_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="CONTI.ANTIPCON = _t2.ANTIPCON";
              +" and "+"CONTI.ANCODICE = _t2.ANCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"ANFLGCPZ ="+cp_NullLink(cp_ToStrODBC("S"),'CONTI','ANFLGCPZ');
          +i_ccchkf;
          +" from "+i_cTable+" CONTI, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="CONTI.ANTIPCON = _t2.ANTIPCON";
              +" and "+"CONTI.ANCODICE = _t2.ANCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CONTI, "+i_cQueryTable+" _t2 set ";
      +"CONTI.ANFLGCPZ ="+cp_NullLink(cp_ToStrODBC("S"),'CONTI','ANFLGCPZ');
          +Iif(Empty(i_ccchkf),"",",CONTI.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="CONTI.ANTIPCON = t2.ANTIPCON";
              +" and "+"CONTI.ANCODICE = t2.ANCODICE";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CONTI set (";
          +"ANFLGCPZ";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +cp_NullLink(cp_ToStrODBC("S"),'CONTI','ANFLGCPZ')+"";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="CONTI.ANTIPCON = _t2.ANTIPCON";
              +" and "+"CONTI.ANCODICE = _t2.ANCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CONTI set ";
      +"ANFLGCPZ ="+cp_NullLink(cp_ToStrODBC("S"),'CONTI','ANFLGCPZ');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".ANTIPCON = "+i_cQueryTable+".ANTIPCON";
              +" and "+i_cTable+".ANCODICE = "+i_cQueryTable+".ANCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"ANFLGCPZ ="+cp_NullLink(cp_ToStrODBC("S"),'CONTI','ANFLGCPZ');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Aggiornamento AGFLGCPZ su tabella agenti (la query seleziona gli agenti con Codice Fiscale NON vuoto)
    * --- Write into AGENTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.AGENTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AGENTI_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="AGCODAGE"
      do vq_exec with 'gscvab02b',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.AGENTI_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="AGENTI.AGCODAGE = _t2.AGCODAGE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"AGFLGCPZ ="+cp_NullLink(cp_ToStrODBC("S"),'AGENTI','AGFLGCPZ');
          +i_ccchkf;
          +" from "+i_cTable+" AGENTI, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="AGENTI.AGCODAGE = _t2.AGCODAGE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" AGENTI, "+i_cQueryTable+" _t2 set ";
      +"AGENTI.AGFLGCPZ ="+cp_NullLink(cp_ToStrODBC("S"),'AGENTI','AGFLGCPZ');
          +Iif(Empty(i_ccchkf),"",",AGENTI.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="AGENTI.AGCODAGE = t2.AGCODAGE";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" AGENTI set (";
          +"AGFLGCPZ";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +cp_NullLink(cp_ToStrODBC("S"),'AGENTI','AGFLGCPZ')+"";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="AGENTI.AGCODAGE = _t2.AGCODAGE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" AGENTI set ";
      +"AGFLGCPZ ="+cp_NullLink(cp_ToStrODBC("S"),'AGENTI','AGFLGCPZ');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".AGCODAGE = "+i_cQueryTable+".AGCODAGE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"AGFLGCPZ ="+cp_NullLink(cp_ToStrODBC("S"),'AGENTI','AGFLGCPZ');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Aggiornamento MGMAGWEB su tabella magazzini (la query seleziona i magazzino con flag MGMAGWEB vuoto)
    * --- Write into MAGAZZIN
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="MGCODMAG"
      do vq_exec with 'gscvab02c',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.MAGAZZIN_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="MAGAZZIN.MGCODMAG = _t2.MGCODMAG";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MGMAGWEB ="+cp_NullLink(cp_ToStrODBC("N"),'MAGAZZIN','MGMAGWEB');
          +i_ccchkf;
          +" from "+i_cTable+" MAGAZZIN, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="MAGAZZIN.MGCODMAG = _t2.MGCODMAG";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MAGAZZIN, "+i_cQueryTable+" _t2 set ";
      +"MAGAZZIN.MGMAGWEB ="+cp_NullLink(cp_ToStrODBC("N"),'MAGAZZIN','MGMAGWEB');
          +Iif(Empty(i_ccchkf),"",",MAGAZZIN.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="MAGAZZIN.MGCODMAG = t2.MGCODMAG";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MAGAZZIN set (";
          +"MGMAGWEB";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +cp_NullLink(cp_ToStrODBC("N"),'MAGAZZIN','MGMAGWEB')+"";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="MAGAZZIN.MGCODMAG = _t2.MGCODMAG";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MAGAZZIN set ";
      +"MGMAGWEB ="+cp_NullLink(cp_ToStrODBC("N"),'MAGAZZIN','MGMAGWEB');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".MGCODMAG = "+i_cQueryTable+".MGCODMAG";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MGMAGWEB ="+cp_NullLink(cp_ToStrODBC("N"),'MAGAZZIN','MGMAGWEB');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Aggiornamento campo ANFLGCPZ tabella CONTI avvenuto correttamente")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      this.w_TMPC = ah_Msgformat("Aggiornamento campo AGFLGCPZ tabella AGENTI avvenuto correttamente")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      this.w_TMPC = ah_Msgformat("Aggiornamento campo MGMAGWEB tabella MAGAZZINI avvenuto correttamente")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = ah_Msgformat("Conversione eseguita correttamente")
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='AGENTI'
    this.cWorkTables[3]='MAGAZZIN'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
