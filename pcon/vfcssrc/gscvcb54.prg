* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcb54                                                        *
*              Valorizzazione Tipo ufficio competente                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-04-03                                                      *
* Last revis.: 2009-06-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcb54",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcb54 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_NCONN = 0
  w_CODICE = space(10)
  w_DESCRI = space(60)
  w_TIPUFF = space(1)
  * --- WorkFile variables
  PRA_ENTI_idx=0
  UFF_GIUD_idx=0
  OFF_ATTI_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Valorizzazione Tipo ufficio competente
    this.w_NCONN = i_TableProp[this.OFF_ATTI_idx,3] 
    if IsAlt()
      * --- Try
      local bErr_0374FB70
      bErr_0374FB70=bTrsErr
      this.Try_0374FB70()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_0374FB70
      * --- End
      * --- Try
      local bErr_03791970
      bErr_03791970=bTrsErr
      this.Try_03791970()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_03791970
      * --- End
      * --- Try
      local bErr_0361BDB0
      bErr_0361BDB0=bTrsErr
      this.Try_0361BDB0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_0361BDB0
      * --- End
    else
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("Procedura di conversione eseguita con successo")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    * --- Resoconto - sempre positivo
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Elaborazione terminata con successo")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = ah_Msgformat("Elaborazione terminata con successo")
  endproc
  proc Try_0374FB70()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Enti/Uffici giudiziari
    * --- Select from PRA_ENTI
    i_nConn=i_TableProp[this.PRA_ENTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRA_ENTI_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select PRA_ENTI.EPCODICE, PRA_ENTI.EPDESCRI, PRA_ENTI.EPTIPUFF  from "+i_cTable+" PRA_ENTI ";
           ,"_Curs_PRA_ENTI")
    else
      select PRA_ENTI.EPCODICE, PRA_ENTI.EPDESCRI, PRA_ENTI.EPTIPUFF from (i_cTable);
        into cursor _Curs_PRA_ENTI
    endif
    if used('_Curs_PRA_ENTI')
      select _Curs_PRA_ENTI
      locate for 1=1
      do while not(eof())
      if EMPTY(NVL(_Curs_PRA_ENTI.EPTIPUFF,""))
        this.w_TIPUFF = " "
        this.w_CODICE = _Curs_PRA_ENTI.EPCODICE
        this.w_DESCRI = ALLTRIM(_Curs_PRA_ENTI.EPCODICE)
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if NOT EMPTY(this.w_TIPUFF)
          * --- Aggiorna il Tipo
          * --- Write into PRA_ENTI
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.PRA_ENTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PRA_ENTI_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.PRA_ENTI_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"EPTIPUFF ="+cp_NullLink(cp_ToStrODBC(this.w_TIPUFF),'PRA_ENTI','EPTIPUFF');
                +i_ccchkf ;
            +" where ";
                +"EPCODICE = "+cp_ToStrODBC(this.w_CODICE);
                   )
          else
            update (i_cTable) set;
                EPTIPUFF = this.w_TIPUFF;
                &i_ccchkf. ;
             where;
                EPCODICE = this.w_CODICE;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      endif
        select _Curs_PRA_ENTI
        continue
      enddo
      use
    endif
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Valorizzato campo Tipo ufficio competente in Enti/Uffici giudiziari")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    return
  proc Try_03791970()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Codici uffici giudiziari
    * --- Select from UFF_GIUD
    i_nConn=i_TableProp[this.UFF_GIUD_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.UFF_GIUD_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select UFF_GIUD.UFCODICE, UFF_GIUD.UF__NOME, UFF_GIUD.UFTIPUFF  from "+i_cTable+" UFF_GIUD ";
           ,"_Curs_UFF_GIUD")
    else
      select UFF_GIUD.UFCODICE, UFF_GIUD.UF__NOME, UFF_GIUD.UFTIPUFF from (i_cTable);
        into cursor _Curs_UFF_GIUD
    endif
    if used('_Curs_UFF_GIUD')
      select _Curs_UFF_GIUD
      locate for 1=1
      do while not(eof())
      if NOT EMPTY(NVL(_Curs_UFF_GIUD.UF__NOME,"")) AND EMPTY(NVL(_Curs_UFF_GIUD.UFTIPUFF,""))
        this.w_TIPUFF = " "
        this.w_CODICE = _Curs_UFF_GIUD.UFCODICE
        this.w_DESCRI = UPPER(ALLTRIM(_Curs_UFF_GIUD.UF__NOME))
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if NOT EMPTY(this.w_TIPUFF)
          * --- Aggiorna il Tipo
          * --- Write into UFF_GIUD
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.UFF_GIUD_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.UFF_GIUD_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.UFF_GIUD_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"UFTIPUFF ="+cp_NullLink(cp_ToStrODBC(this.w_TIPUFF),'UFF_GIUD','UFTIPUFF');
                +i_ccchkf ;
            +" where ";
                +"UFCODICE = "+cp_ToStrODBC(this.w_CODICE);
                   )
          else
            update (i_cTable) set;
                UFTIPUFF = this.w_TIPUFF;
                &i_ccchkf. ;
             where;
                UFCODICE = this.w_CODICE;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      endif
        select _Curs_UFF_GIUD
        continue
      enddo
      use
    endif
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Valorizzato campo Tipo ufficio competente in Codici uffici giudiziari")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    return
  proc Try_0361BDB0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Gestione ente per versamento tributi
    * --- Select from PRA_ENTI
    i_nConn=i_TableProp[this.PRA_ENTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRA_ENTI_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select PRA_ENTI.EPCODICE, PRA_ENTI.EPDESCRI, EPCODENT  from "+i_cTable+" PRA_ENTI ";
           ,"_Curs_PRA_ENTI")
    else
      select PRA_ENTI.EPCODICE, PRA_ENTI.EPDESCRI, EPCODENT from (i_cTable);
        into cursor _Curs_PRA_ENTI
    endif
    if used('_Curs_PRA_ENTI')
      select _Curs_PRA_ENTI
      locate for 1=1
      do while not(eof())
      if EMPTY(NVL(_Curs_PRA_ENTI.EPCODENT,""))
        this.w_TIPUFF = " "
        this.w_CODICE = _Curs_PRA_ENTI.EPCODICE
        this.w_DESCRI = UPPER(ALLTRIM(_Curs_PRA_ENTI.EPCODICE))
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if NOT EMPTY(this.w_TIPUFF)
          * --- Aggiorna l'ente per versamento tributi solo se ne ha trovato il valore
          * --- Write into PRA_ENTI
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.PRA_ENTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PRA_ENTI_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.PRA_ENTI_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"EPCODENT ="+cp_NullLink(cp_ToStrODBC(this.w_TIPUFF),'PRA_ENTI','EPCODENT');
                +i_ccchkf ;
            +" where ";
                +"EPCODICE = "+cp_ToStrODBC(this.w_CODICE);
                   )
          else
            update (i_cTable) set;
                EPCODENT = this.w_TIPUFF;
                &i_ccchkf. ;
             where;
                EPCODICE = this.w_CODICE;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      endif
        select _Curs_PRA_ENTI
        continue
      enddo
      use
    endif
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Valorizzato campo Tipo ufficio competente in Enti/Uffici giudiziari")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case this.w_DESCRI == "CORTE SUPREMA DI CASSAZIONE"
        this.w_TIPUFF = "A"
      case this.w_DESCRI == "CORTE D'APPELLO" OR this.w_DESCRI == "CORTE D' APPELLO" OR this.w_DESCRI == "CORTE DI APPELLO" 
        this.w_TIPUFF = "B"
      case this.w_DESCRI == "CORTE DI ASSISE" OR this.w_DESCRI == "CORTE D'ASSISE"
        this.w_TIPUFF = "C"
      case this.w_DESCRI == "CORTE DI ASSISE DI APPELLO" OR this.w_DESCRI == "CORTE D'ASSISE DI APPELLO"
        this.w_TIPUFF = "D"
      case this.w_DESCRI == "DIREZIONE NAZIONALE ANTIMAFIA"
        this.w_TIPUFF = "E"
      case this.w_DESCRI == "GIUDICE DI PACE"
        this.w_TIPUFF = "F"
      case this.w_DESCRI == "PROCURA GENERALE DELLA REPUBBLICA PRESSO LA CORTE SUPREMA DI CASSAZIONE" OR this.w_DESCRI == "PROCURA GENERALE DELLA REPUBBLICA PRESSO LA CASSAZIONE" 
        this.w_TIPUFF = "G"
      case this.w_DESCRI == "PROCURA GENERALE DELLA REPUBBLICA PRESSO LA CORTE D'APPELLO" OR this.w_DESCRI == "PROCURA GENERALE DELLA REPUBBLICA PRESSO LA CORTE DI APPELLO"
        this.w_TIPUFF = "H"
      case this.w_DESCRI == "PROCURA DELLA REPUBBLICA PRESSO IL TRIBUNALE" OR this.w_DESCRI == "PROCURA DELLA REPUBBLICA PRESSO IL TRIBUNALE ORDINARIO" OR this.w_DESCRI == "PROCURA DELLA REPUBBLICA PRESSO LA PRETURA CIRCONDARIALE" OR this.w_DESCRI == "PROCURA DELLA REPUBBLICA PRESSO IL TRIBUNALE ORDINARIO - GIUDICE UNICO"
        this.w_TIPUFF = "I"
      case this.w_DESCRI == "PROCURA DELLA REPUBBLICA PRESSO IL TRIBUNALE PER I MINORENNI" OR this.w_DESCRI == "PROCURA DELLA REPUBBLICA PRESSO IL TRIBUNALE DEI MINORI"
        this.w_TIPUFF = "L"
      case this.w_DESCRI == "SEZIONE DISTACCATA DI TRIBUNALE" OR this.w_DESCRI == "SEZIONE DISTACCATA PRETURA CIRCONDARIALE"
        this.w_TIPUFF = "M"
      case this.w_DESCRI == "TRIBUNALE" OR this.w_DESCRI == "TRIBUNALE ORDINARIO" OR this.w_DESCRI == "PRETURA CIRCONDARIALE" OR this.w_DESCRI == "TRIBUNALE ORDINARIO - GIUDICE UNICO"
        this.w_TIPUFF = "N"
      case this.w_DESCRI == "TRIBUNALE REGIONALE DELLE ACQUE PUBBLICHE"
        this.w_TIPUFF = "O"
      case this.w_DESCRI == "TRIBUNALE SUPERIORE DELLE ACQUE PUBBLICHE"
        this.w_TIPUFF = "P"
      case this.w_DESCRI == "TRIBUNALE DI SORVEGLIANZA"
        this.w_TIPUFF = "Q"
      case this.w_DESCRI == "TRIBUNALE PER I MINORENNI"
        this.w_TIPUFF = "R"
      case this.w_DESCRI == "UFFICIO DI SORVEGLIANZA"
        this.w_TIPUFF = "S"
    endcase
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case this.w_DESCRI == "Amm02" OR this.w_DESCRI=="Amm03" OR this.w_DESCRI=="Civ01" OR this.w_DESCRI=="Civ04" OR this.w_DESCRI=="Civ05" OR this.w_DESCRI=="Civ06" OR this.w_DESCRI=="Civ07" OR this.w_DESCRI=="Civ09" OR this.w_DESCRI=="Civ15" OR this.w_DESCRI=="Pen04" OR this.w_DESCRI=="Pen14"
        this.w_TIPUFF = "A"
      case this.w_DESCRI=="Civ03" OR this.w_DESCRI=="Pen01" OR this.w_DESCRI== "Civ11" OR this.w_DESCRI=="Ctri02" OR this.w_DESCRI=="Pen05"
        this.w_TIPUFF = "B"
      case this.w_DESCRI == "Pen03"
        this.w_TIPUFF = "C"
      case this.w_DESCRI == "Pen02"
        this.w_TIPUFF = "D"
      case this.w_DESCRI == "Pen25"
        this.w_TIPUFF = "E"
      case this.w_DESCRI == "Civ13" OR this.w_DESCRI=="Pen12"
        this.w_TIPUFF = "F"
      case this.w_DESCRI == "Civ22" OR this.w_DESCRI == "Pen23" 
        this.w_TIPUFF = "G"
      case this.w_DESCRI == "Civ21" OR this.w_DESCRI == "Pen22"
        this.w_TIPUFF = "H"
      case this.w_DESCRI == "Civ20" OR this.w_DESCRI == "Pen21"
        this.w_TIPUFF = "I"
      case this.w_DESCRI == "Civ23" OR this.w_DESCRI == "Pen24"
        this.w_TIPUFF = "L"
      case this.w_DESCRI == "Civ12" OR this.w_DESCRI == "Pen08" OR this.w_DESCRI=="Pen09"
        this.w_TIPUFF = "M"
      case this.w_DESCRI == "Civ17" OR this.w_DESCRI == "Amm01" OR this.w_DESCRI == "Amm04" OR this.w_DESCRI == "Civ02" OR this.w_DESCRI=="Civ08" OR this.w_DESCRI=="Civ10" OR this.w_DESCRI=="Civ14" OR this.w_DESCRI=="Ctri01" OR this.w_DESCRI=="Pen10" OR this.w_DESCRI=="Pen11" OR this.w_DESCRI=="Pen15" OR this.w_DESCRI=="Pen16" OR this.w_DESCRI=="Pen17" OR this.w_DESCRI=="Pen19"
        this.w_TIPUFF = "N"
      case this.w_DESCRI == "Civ18"
        this.w_TIPUFF = "O"
      case this.w_DESCRI == "Civ19"
        this.w_TIPUFF = "P"
      case this.w_DESCRI == "Pen20" OR this.w_DESCRI=="Pen07"
        this.w_TIPUFF = "Q"
      case this.w_DESCRI == "Civ16" OR this.w_DESCRI=="Pen18"
        this.w_TIPUFF = "R"
      case this.w_DESCRI == "Pen06" OR this.w_DESCRI=="Pen13"
        this.w_TIPUFF = "S"
    endcase
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case this.w_DESCRI == "AMM01"
        this.w_TIPUFF = "9R9"
      case this.w_DESCRI == "AMM03"
        this.w_TIPUFF = "9S9"
      case this.w_DESCRI == "AMM04"
        this.w_TIPUFF = "9T9"
      case this.w_DESCRI == "CIV03" OR this.w_DESCRI == "CIV11" OR this.w_DESCRI == "PEN01" OR this.w_DESCRI == "PEN05"
        this.w_TIPUFF = "Hxx"
      case this.w_DESCRI == "CIV09"
        this.w_TIPUFF = "SAE"
      case this.w_DESCRI == "CIV07" OR this.w_DESCRI == "PEN04"
        this.w_TIPUFF = "4AE"
      case this.w_DESCRI == "CIV10"
        this.w_TIPUFF = "Vxx"
      case this.w_DESCRI == "CIV13" OR this.w_DESCRI == "PEN12"
        this.w_TIPUFF = "9C3"
      case this.w_DESCRI == "PEN13" OR this.w_DESCRI == "PEN06"
        this.w_TIPUFF = "Sxx"
      case this.w_DESCRI == "CIV12" OR this.w_DESCRI == "PEN08" OR this.w_DESCRI == "PEN09" OR this.w_DESCRI == "CIV17" OR this.w_DESCRI == "PEN15" OR this.w_DESCRI == "PEN19"
        this.w_TIPUFF = "9BX"
      case this.w_DESCRI == "PEN07" OR this.w_DESCRI == "PEN20"
        this.w_TIPUFF = "Nxx"
      case this.w_DESCRI == "CIV16" OR this.w_DESCRI == "PEN18"
        this.w_TIPUFF = "Pxx"
      case this.w_DESCRI == "CIV17" OR this.w_DESCRI == "PEN17"
        this.w_TIPUFF = "1xx"
      case this.w_DESCRI == "CIV19"
        this.w_TIPUFF = "8AE"
    endcase
  endproc


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='PRA_ENTI'
    this.cWorkTables[2]='UFF_GIUD'
    this.cWorkTables[3]='OFF_ATTI'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_PRA_ENTI')
      use in _Curs_PRA_ENTI
    endif
    if used('_Curs_UFF_GIUD')
      use in _Curs_UFF_GIUD
    endif
    if used('_Curs_PRA_ENTI')
      use in _Curs_PRA_ENTI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
