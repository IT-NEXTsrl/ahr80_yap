* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvbb06                                                        *
*              Aggiorna status                                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_172]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-02-23                                                      *
* Last revis.: 2001-02-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvbb06",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvbb06 as StdBatch
  * --- Local variables
  w_DATAGG = ctod("  /  /  ")
  w_CODCON = space(15)
  w_TIPCON = space(1)
  w_CODVAL = space(3)
  w_DATINC = ctod("  /  /  ")
  w_IMPINC = 0
  w_MODPAG = space(10)
  w_DATSCA = ctod("  /  /  ")
  w_DESCRI = space(40)
  w_STATUS = space(1)
  w_NUMREG = 0
  w_TIPO = space(2)
  w_DATREG = ctod("  /  /  ")
  w_COMPET = space(4)
  w_OSERCONT = space(10)
  w_PTSERIAL = space(10)
  w_PTROWORD = 0
  w_CPROWNUM = 0
  w_ROWINC = 0
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_MESS = space(100)
  * --- WorkFile variables
  INC_AVVE_idx=0
  TMP_GESPIAN_idx=0
  PAR_TITE_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Esegue valorizzazione campi IASERRIF,IAORDRIF,IANUMRIF
    * --- FIle di LOG
    * --- Legge Incassi Avvenuti per Contenzioso
    *     Creo un temporaneo con i contenziosi da valutare con il totale partite associate
    *     al contenzioso
    if g_SOLL="S"
      ah_Msg("Lettura incassi...",.T.)
      * --- Create temporary table TMP_GESPIAN
      i_nIdx=cp_AddTableDef('TMP_GESPIAN') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('GSSO_QAS_3',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.TMP_GESPIAN_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      * --- Cursore Incassi da passare alla Stampa Finale
       
 CREATE CURSOR INCAVVE (COSERIAL C(10), COCODCON C(15),ANDESCRI C(40),CONUMREG N(6),COCOMPET C(4),; 
 COSTATUS C(2), CO__TIPO C(1), CODATREG D(10), COCODVAL C(3), IADATINC D(10),IAIMPINC N(18,4),IATIPPAG C(10) )
      * --- Lancio la query per recuperare gli incassi successivi ai vari
      *     contenziosi..
      * --- Try
      local bErr_03618930
      bErr_03618930=bTrsErr
      this.Try_03618930()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- Gestisce log errori
        this.oParentObject.w_PMSG = Message()
        this.oParentObject.w_PESEOK = .F.
        if this.w_NHF>=0
          this.w_TMPC = ah_Msgformat("ERRORE GENERICO - impossibile aggiornare incassi insoluti")
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
      endif
      bTrsErr=bTrsErr or bErr_03618930
      * --- End
      * --- Eseguo Stampa  Incassi
      if RECCOUNT("INCAVVE") >0
        SELECT * FROM INCAVVE INTO CURSOR __TMP__ ORDER BY 1, 3
        SELECT __TMP__
        GO TOP
        ah_Msg("Stampa in corso",.T.)
        CP_CHPRN("..\PCON\EXE\QUERY\GSCVBB06.FRX"," ",This)
        if used("__TMP__")
          SELECT __TMP__
          use
        endif
        this.w_TMPC = ah_Msgformat("Attenzione - procedura di conversione non terminata con successo, esistono incassi avvenuti incongruenti!")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        this.oParentObject.w_PESEOK = .F.
      else
        * --- Esecuzione ok
        this.oParentObject.w_PESEOK = .T.
      endif
      if used("INCAVVE")
         
 SELECT INCAVVE
        use
      endif
      * --- Drop temporary table TMP_GESPIAN
      i_nIdx=cp_GetTableDefIdx('TMP_GESPIAN')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('TMP_GESPIAN')
      endif
    endif
  endproc
  proc Try_03618930()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Azzero riferimenti per le partite legate a conti SBF di creazione
    * --- Select from GSCVBB06
    do vq_exec with 'GSCVBB06',this,'_Curs_GSCVBB06','',.f.,.t.
    if used('_Curs_GSCVBB06')
      select _Curs_GSCVBB06
      locate for 1=1
      do while not(eof())
      this.w_DATINC = CP_TODATE(_Curs_GSCVBB06.PNDATREG)
      this.w_MODPAG = _Curs_GSCVBB06.PTMODPAG
      this.w_IMPINC = IIF(NVL(_Curs_GSCVBB06.PT_SEGNO," ")="D",-NVL(_Curs_GSCVBB06.PTTOTIMP,0),NVL(_Curs_GSCVBB06.PTTOTIMP,0))
      this.w_DESCRI = _Curs_GSCVBB06.ANDESCRI
      this.w_CODVAL = _Curs_GSCVBB06.COCODVAL
      this.w_NUMREG = _Curs_GSCVBB06.CONUMREG
      this.w_COMPET = _Curs_GSCVBB06.COCOMPET
      this.w_TIPO = _Curs_GSCVBB06.CO__TIPO
      this.w_STATUS = _Curs_GSCVBB06.COSTATUS
      this.w_DATREG = _Curs_GSCVBB06.CODATREG
      this.w_CODCON = _Curs_GSCVBB06.COCODCON
      this.w_OSERCONT = _Curs_GSCVBB06.COSERIAL
      this.w_PTSERIAL = _Curs_GSCVBB06.PTSERIAL
      this.w_PTROWORD = _Curs_GSCVBB06.PTROWORD
      this.w_CPROWNUM = _Curs_GSCVBB06.CPROWNUM
      * --- Read from INC_AVVE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.INC_AVVE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.INC_AVVE_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CPROWNUM"+;
          " from "+i_cTable+" INC_AVVE where ";
              +"IADATINC = "+cp_ToStrODBC(this.w_DATINC);
              +" and IAIMPINC = "+cp_ToStrODBC(this.w_IMPINC);
              +" and IATIPPAG = "+cp_ToStrODBC(this.w_MODPAG);
              +" and IANUMRIF = "+cp_ToStrODBC(0);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CPROWNUM;
          from (i_cTable) where;
              IADATINC = this.w_DATINC;
              and IAIMPINC = this.w_IMPINC;
              and IATIPPAG = this.w_MODPAG;
              and IANUMRIF = 0;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_ROWINC = NVL(cp_ToDate(_read_.CPROWNUM),cp_NullValue(_read_.CPROWNUM))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_ROWINC<>0
        * --- Write into INC_AVVE
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.INC_AVVE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.INC_AVVE_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.INC_AVVE_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"IASERRIF ="+cp_NullLink(cp_ToStrODBC(this.w_PTSERIAL),'INC_AVVE','IASERRIF');
          +",IAORDRIF ="+cp_NullLink(cp_ToStrODBC(this.w_PTROWORD),'INC_AVVE','IAORDRIF');
          +",IANUMRIF ="+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'INC_AVVE','IANUMRIF');
              +i_ccchkf ;
          +" where ";
              +"IASERIAL = "+cp_ToStrODBC(this.w_OSERCONT);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWINC);
              +" and IAORDRIF = "+cp_ToStrODBC(0);
                 )
        else
          update (i_cTable) set;
              IASERRIF = this.w_PTSERIAL;
              ,IAORDRIF = this.w_PTROWORD;
              ,IANUMRIF = this.w_CPROWNUM;
              &i_ccchkf. ;
           where;
              IASERIAL = this.w_OSERCONT;
              and CPROWNUM = this.w_ROWINC;
              and IAORDRIF = 0;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      else
         
 SELECT INCAVVE 
 APPEND BLANK 
 REPLACE INCAVVE.COSERIAL WITH this.w_OSERCONT 
 REPLACE INCAVVE.ANDESCRI WITH this.w_DESCRI 
 REPLACE INCAVVE.COCODCON WITH this.w_CODCON 
 REPLACE INCAVVE.CONUMREG WITH this.w_NUMREG 
 REPLACE INCAVVE.COCOMPET WITH this.w_COMPET 
 REPLACE INCAVVE.CO__TIPO WITH this.w_TIPO 
 REPLACE INCAVVE.COSTATUS WITH this.w_STATUS 
 REPLACE INCAVVE.CODATREG WITH this.w_DATREG 
 REPLACE INCAVVE.COCODVAL WITH this.w_CODVAL 
 REPLACE INCAVVE.IADATINC WITH this.w_DATINC 
 REPLACE INCAVVE.IAIMPINC WITH this.w_IMPINC 
 REPLACE INCAVVE.IATIPPAG WITH this.w_MODPAG
      endif
        select _Curs_GSCVBB06
        continue
      enddo
      use
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='INC_AVVE'
    this.cWorkTables[2]='*TMP_GESPIAN'
    this.cWorkTables[3]='PAR_TITE'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_GSCVBB06')
      use in _Curs_GSCVBB06
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
