* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvab04                                                        *
*              Aggiorna campo an__BBAN                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_56]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-10-08                                                      *
* Last revis.: 2003-10-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvab04",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvab04 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  * --- WorkFile variables
  CONTI_idx=0
  COC_MAST_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- AGGIORNA CAMPO AN__BBAN
    * --- Rendo scrivibile il cursore
    * --- Try
    local bErr_04A9F8D0
    bErr_04A9F8D0=bTrsErr
    this.Try_04A9F8D0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0 
        this.w_TMPC = this.oParentObject.w_PMSG
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_04A9F8D0
    * --- End
    * --- Recupero dal database i Conti / Conti Banca ancora da valorizzate.
    vq_exec("GSCVAB06", this, "MESSERR")
    if RECCOUNT("MESSERR")>0
      if ah_YesNo("Stampo riepilogo aggiornamento?")
        Select * from MESSERR INTO CURSOR __TMP__ 
        CP_CHPRN("..\PCON\EXE\QUERY\GSCVAB04.FRX", " ", this)
      endif
    endif
  endproc
  proc Try_04A9F8D0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Write into CONTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.CONTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="ANTIPCON,ANCODICE"
      do vq_exec with 'GSCVAB04',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTI_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="CONTI.ANTIPCON = _t2.ANTIPCON";
              +" and "+"CONTI.ANCODICE = _t2.ANCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"AN__BBAN = _t2.AN__BBAN";
          +i_ccchkf;
          +" from "+i_cTable+" CONTI, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="CONTI.ANTIPCON = _t2.ANTIPCON";
              +" and "+"CONTI.ANCODICE = _t2.ANCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CONTI, "+i_cQueryTable+" _t2 set ";
          +"CONTI.AN__BBAN = _t2.AN__BBAN";
          +Iif(Empty(i_ccchkf),"",",CONTI.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="CONTI.ANTIPCON = t2.ANTIPCON";
              +" and "+"CONTI.ANCODICE = t2.ANCODICE";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CONTI set (";
          +"AN__BBAN";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.AN__BBAN";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="CONTI.ANTIPCON = _t2.ANTIPCON";
              +" and "+"CONTI.ANCODICE = _t2.ANCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CONTI set ";
          +"AN__BBAN = _t2.AN__BBAN";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".ANTIPCON = "+i_cQueryTable+".ANTIPCON";
              +" and "+i_cTable+".ANCODICE = "+i_cQueryTable+".ANCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"AN__BBAN = (select AN__BBAN from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Write into COC_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.COC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.COC_MAST_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="BACODBAN"
      do vq_exec with 'gscvab05',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.COC_MAST_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="COC_MAST.BACODBAN = _t2.BACODBAN";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"BA__BBAN = _t2.BA__BBAN";
          +i_ccchkf;
          +" from "+i_cTable+" COC_MAST, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="COC_MAST.BACODBAN = _t2.BACODBAN";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" COC_MAST, "+i_cQueryTable+" _t2 set ";
          +"COC_MAST.BA__BBAN = _t2.BA__BBAN";
          +Iif(Empty(i_ccchkf),"",",COC_MAST.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="COC_MAST.BACODBAN = t2.BACODBAN";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" COC_MAST set (";
          +"BA__BBAN";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.BA__BBAN";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="COC_MAST.BACODBAN = _t2.BACODBAN";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" COC_MAST set ";
          +"BA__BBAN = _t2.BA__BBAN";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".BACODBAN = "+i_cQueryTable+".BACODBAN";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"BA__BBAN = (select BA__BBAN from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Aggiornamento campo AN__BBAN eseguito correttamente%0Aggiornamento campo BA__BBAN eseguito correttamente%0")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = ah_Msgformat("Aggiornamento campo AN__BBAN eseguito correttamente%0Aggiornamento campo BA__BBAN eseguito correttamente%0")
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='COC_MAST'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
