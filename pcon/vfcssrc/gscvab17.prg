* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvab17                                                        *
*              Aggiorna campo tdsincfl nelle causali documenti                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_80]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-02-27                                                      *
* Last revis.: 2003-02-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvab17",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvab17 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_MESS = space(100)
  * --- WorkFile variables
  DOC_DETT_idx=0
  TIP_DOCU_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questa procedura Aggiorna il campo Segno Cash Flow delle Causali Documenti:
    *     0 = Non gestita
    *     1 = Positivo (Fatture Vendite, N. di Credito Acquisti, Ordini da Clienti, DDT Clienti)
    *     -1 = Negativo (Fatture Acquisti, N. di Credito Vendite, Ordini a Fornitori, DDT Fornitori)
    * --- FIle di LOG
    * --- Try
    local bErr_036090E0
    bErr_036090E0=bTrsErr
    this.Try_036090E0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE %1 - impossibile aggiornare causali documenti", this.oParentObject.w_PMSG )
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_036090E0
    * --- End
  endproc
  proc Try_036090E0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Write into TIP_DOCU
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.TIP_DOCU_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"TDSINCFL ="+cp_NullLink(cp_ToStrODBC(1),'TIP_DOCU','TDSINCFL');
          +i_ccchkf ;
      +" where ";
          +"TDCATDOC = "+cp_ToStrODBC("FA");
          +" and TDFLVEAC = "+cp_ToStrODBC("V");
             )
    else
      update (i_cTable) set;
          TDSINCFL = 1;
          &i_ccchkf. ;
       where;
          TDCATDOC = "FA";
          and TDFLVEAC = "V";

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Write into TIP_DOCU
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.TIP_DOCU_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"TDSINCFL ="+cp_NullLink(cp_ToStrODBC(-1),'TIP_DOCU','TDSINCFL');
          +i_ccchkf ;
      +" where ";
          +"TDCATDOC = "+cp_ToStrODBC("FA");
          +" and TDFLVEAC = "+cp_ToStrODBC("A");
             )
    else
      update (i_cTable) set;
          TDSINCFL = -1;
          &i_ccchkf. ;
       where;
          TDCATDOC = "FA";
          and TDFLVEAC = "A";

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Write into TIP_DOCU
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.TIP_DOCU_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"TDSINCFL ="+cp_NullLink(cp_ToStrODBC(-1),'TIP_DOCU','TDSINCFL');
          +i_ccchkf ;
      +" where ";
          +"TDCATDOC = "+cp_ToStrODBC("NC");
          +" and TDFLVEAC = "+cp_ToStrODBC("V");
             )
    else
      update (i_cTable) set;
          TDSINCFL = -1;
          &i_ccchkf. ;
       where;
          TDCATDOC = "NC";
          and TDFLVEAC = "V";

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Write into TIP_DOCU
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.TIP_DOCU_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"TDSINCFL ="+cp_NullLink(cp_ToStrODBC(1),'TIP_DOCU','TDSINCFL');
          +i_ccchkf ;
      +" where ";
          +"TDCATDOC = "+cp_ToStrODBC("NC");
          +" and TDFLVEAC = "+cp_ToStrODBC("A");
             )
    else
      update (i_cTable) set;
          TDSINCFL = 1;
          &i_ccchkf. ;
       where;
          TDCATDOC = "NC";
          and TDFLVEAC = "A";

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Write into TIP_DOCU
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.TIP_DOCU_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"TDSINCFL ="+cp_NullLink(cp_ToStrODBC(1),'TIP_DOCU','TDSINCFL');
          +i_ccchkf ;
      +" where ";
          +"TDCATDOC = "+cp_ToStrODBC("OR");
          +" and TDFLVEAC = "+cp_ToStrODBC("V");
             )
    else
      update (i_cTable) set;
          TDSINCFL = 1;
          &i_ccchkf. ;
       where;
          TDCATDOC = "OR";
          and TDFLVEAC = "V";

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Write into TIP_DOCU
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.TIP_DOCU_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"TDSINCFL ="+cp_NullLink(cp_ToStrODBC(-1),'TIP_DOCU','TDSINCFL');
          +i_ccchkf ;
      +" where ";
          +"TDCATDOC = "+cp_ToStrODBC("OR");
          +" and TDFLVEAC = "+cp_ToStrODBC("A");
             )
    else
      update (i_cTable) set;
          TDSINCFL = -1;
          &i_ccchkf. ;
       where;
          TDCATDOC = "OR";
          and TDFLVEAC = "A";

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    if bTrsErr
      * --- Raise
      i_Error=i_TrsMsg
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = ah_Msgformat("Conversione eseguita correttamente su %1 righe", Alltrim(Str( i_Rows )) )
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='DOC_DETT'
    this.cWorkTables[2]='TIP_DOCU'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
