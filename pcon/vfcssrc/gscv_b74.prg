* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscv_b74                                                        *
*              Aggiornamento tabelle di sistema per DB2                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_457]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-07-18                                                      *
* Last revis.: 2002-07-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscv_b74",oParentObject,m.w_NHF)
return(i_retval)

define class tgscv_b74 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_PROGNAME = space(50)
  w_GRPCODE = 0
  w_SEC1 = 0
  w_SEC2 = 0
  w_SEC3 = 0
  w_SEC4 = 0
  w_SERVER = space(10)
  w_DESCRI = space(30)
  w_ODBC = space(200)
  w_WHENCON = 0
  w_Database = space(30)
  w_POSTIT = space(1)
  * --- WorkFile variables
  AZIENDA_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- FIle di LOG
    * --- Test sul tipo Database
    if upper(CP_DBTYPE) <> "DB2" 
      ah_ErrorMsg("Conversione prevista solo per database IBM DB2",,"")
      this.oParentObject.w_PESEOK = .T.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("Elaborazione terminata con successo")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    else
      * --- Try
      local bErr_036184B0
      bErr_036184B0=bTrsErr
      this.Try_036184B0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        * --- Gestisce log errori
        this.oParentObject.w_PMSG = Message()
        this.oParentObject.w_PESEOK = .F.
        if this.w_NHF>=0
          this.w_TMPC = this.oParentObject.w_PMSG
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
      bTrsErr=bTrsErr or bErr_036184B0
      * --- End
    endif
  endproc
  proc Try_036184B0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if bTrsErr
      * --- Raise
      i_Error=MESSAGE()
      return
    else
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("Elaborazione terminata con successo")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = ah_Msgformat("Elaborazione terminata con successo")
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Tabella CPPRGSEC
    this.w_GRPCODE = 0
    this.w_SEC1 = 0
    this.w_SEC2 = 0
    this.w_SEC3 = 0
    this.w_SEC4 = 0
    pName="CPPRGSEC"
    pDatabaseType=i_ServerConn[1,6]
    * --- Crea tabella temporanea RIPATMP1
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    * --- Aggiunge la definizione nella lista delle tabelle
     i_nIdx=cp_AddTableDef("RIPATMP1") 
    * --- Recupera il nome assegnato
     i_cTempTable=i_TableProp[i_nIdx,2]
    * --- Recupera la connessione
    i_nConn=1
    i_cTable="CPPRGSEC"
    cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable)
    * --- Segna come usato 1 volta
    i_TableProp[i_nIdx,4]=1
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    * --- Cancellazione tabella CPPRGSEC
    cp_SQL(i_nConn,"drop table cpprgsec")
    ah_Msg("Ricostruita tabella di origine",.T.)
    * --- Ricostruzione tabella CPPRGSEC
    db=pDatabaseType
    cp_SQL(i_nConn,"create table cpprgsec (progname Char(50) not null, grpcode "+db_FieldType(db,"N",6,0)+" not null, sec1 "+db_FieldType(db,"L",1,0)+",sec2 "+db_FieldType(db,"L",1,0)+",sec3 "+db_FieldType(db,"L",1,0)+",sec4 "+db_FieldType(db,"L",1,0)+")")
    cp_SQL(i_nConn,"alter table cpprgsec "+db_CreatePrimaryKey("cpprgsec","cpprgsec","progname,grpcode",pDatabaseType))
    cp_SQL(i_nConn,"create index cpprgsec1 on cpprgsec(progname)")
    cp_SQL(i_nConn,"create index cpprgsec2 on cpprgsec(grpcode)")
    ah_Msg("Ricostruita tabella di origine",.T.)
    * --- Select from gscv_b74
    do vq_exec with 'gscv_b74',this,'_Curs_gscv_b74','',.f.,.t.
    if used('_Curs_gscv_b74')
      select _Curs_gscv_b74
      locate for 1=1
      do while not(eof())
      this.w_PROGNAME =  NVL(_Curs_gscv_b74.PROGNAME,SPACE(10))
      this.w_GRPCODE =  nvl(_Curs_gscv_b74.GRPCODE,0)
      this.w_SEC1 = nvl(_Curs_gscv_b74.SEC1,0)
      this.w_SEC2 = nvl(_Curs_gscv_b74.SEC2,0)
      this.w_SEC3 = nvl(_Curs_gscv_b74.SEC3,0)
      this.w_SEC4 = nvl(_Curs_gscv_b74.SEC4,0)
      cp_SQL(i_nConn,"INSERT INTO "+i_cTable+"  (PROGNAME,GRPCODE,SEC1,SEC2,SEC3,SEC4) VALUES (" + cp_ToStrODBC(this.w_PROGNAME) + ","+ ALLTRIM(STR(this.w_GRPCODE))+","+ALLTRIM(STR(this.w_SEC1))+","+ALLTRIM(STR(this.w_SEC2))+","+ALLTRIM(STR(this.w_SEC3))+","+ALLTRIM(STR(this.w_SEC4))+")")
        select _Curs_gscv_b74
        continue
      enddo
      use
    endif
    ah_Msg("Ripristinati valori di origine",.T.)
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    ah_Msg("Eliminata tabella temporanea tmp",.T.)
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Tabella CPTSRVR
    this.w_WHENCON = 0
    pName="CPTSRVR"
    pDatabaseType=i_ServerConn[1,6]
    * --- Crea tabella temporanea RIPATMP1
    * --- Aggiunge la definizione nella lista delle tabelle
     i_nIdx=cp_AddTableDef("RIPATMP1") 
    * --- Recupera il nome assegnato
     i_cTempTable=i_TableProp[i_nIdx,2]
    * --- Recupera la connessione
    i_nConn=1
    i_cTable="CPTSRVR"
    cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable)
    * --- Segna come usato 1 volta
    i_TableProp[i_nIdx,4]=1
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    * --- Cancellazione tabella CPTSRVR
    cp_SQL(i_nConn,"drop table CPTSRVR")
    ah_Msg("Ricostruita tabella di origine",.T.)
    * --- Ricostruzione tabella CPTSRVR
    db=pDatabaseType
    cp_SQL(i_nConn,"create table cptsrvr (ServerName Char(10) not null, ServerDesc Char(30), ODBCDataSource Char(200), WhenConn "+db_FieldType(db,"N",6,0)+", PostIt Char(1), DatabaseType Char(30))")
    cp_SQL(i_nConn,"alter table cptsrvr "+db_CreatePrimaryKey("cptsrvr","cptsrvr","ServerName",pDatabaseType))
    ah_Msg("Ricostruita tabella di origine",.T.)
    * --- Select from gscv_b74
    do vq_exec with 'gscv_b74',this,'_Curs_gscv_b74','',.f.,.t.
    if used('_Curs_gscv_b74')
      select _Curs_gscv_b74
      locate for 1=1
      do while not(eof())
      this.w_SERVER =  nvl(_Curs_gscv_b74.ServerName,space(10))
      this.w_DESCRI =  nvl(_Curs_gscv_b74.ServerDesc,space(30))
      this.w_ODBC = nvl(_Curs_gscv_b74.ODBCDataSource,space(200))
      this.w_WHENCON = nvl(_Curs_gscv_b74.WhenConn,0)
      this.w_Database = nvl(_Curs_gscv_b74.DataBaseType,space(30))
      this.w_POSTIT = nvl(_Curs_gscv_b74.Postit," ")
      cp_SQL(i_nConn,"INSERT INTO "+i_cTable+ "(ServerName,ServerDesc,ODBCDataSource,WhenConn,Postit,DatabaseType) VALUES (" + cp_ToStrodbc(this.w_SERVER) + ","+;
       cp_ToStrodbc(this.w_DESCRI)+","+cp_ToStrodbc(alltrim(this.w_ODBC))+","+alltrim(str(this.w_WhenCon))+","+cp_ToStrodbc(alltrim(this.w_Postit))+","+cp_ToStrodbc(alltrim(this.w_Database))+")")
        select _Curs_gscv_b74
        continue
      enddo
      use
    endif
    ah_Msg("Ripristinati valori di origine",.T.)
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    ah_Msg("Eliminata tabella temporanea tmp",.T.)
  endproc


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='AZIENDA'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_gscv_b74')
      use in _Curs_gscv_b74
    endif
    if used('_Curs_gscv_b74')
      use in _Curs_gscv_b74
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
