* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvbb62                                                        *
*              Aggiornamento riferimenti contabili distinte                    *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-03-17                                                      *
* Last revis.: 2007-01-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvbb62",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvbb62 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TMPC = space(10)
  w_TEMP = space(10)
  * --- WorkFile variables
  PAR_TITE_idx=0
  DIS_TINT_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna campi DIRIFCON,PTRIFIND
    *     nel caso di cancellazione di contabilizzazione distinte con errore 6520
    * --- FIle di LOG
    * --- Try
    local bErr_03895408
    bErr_03895408=bTrsErr
    this.Try_03895408()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = Ah_MsgFormat("ERRORE GENERICO - Impossibile aggiornare tabella %1", "DIS_TINT")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_03895408
    * --- End
  endproc
  proc Try_03895408()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Aggiorno MVFLEVAS
    * --- Write into DIS_TINT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DIS_TINT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DIS_TINT_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="DINUMDIS"
      do vq_exec with 'GSCVBB62',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DIS_TINT_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="DIS_TINT.DINUMDIS = _t2.DINUMDIS";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"DIRIFCON ="+cp_NullLink(cp_ToStrODBC(SPACE(10)),'DIS_TINT','DIRIFCON');
          +i_ccchkf;
          +" from "+i_cTable+" DIS_TINT, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="DIS_TINT.DINUMDIS = _t2.DINUMDIS";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DIS_TINT, "+i_cQueryTable+" _t2 set ";
      +"DIS_TINT.DIRIFCON ="+cp_NullLink(cp_ToStrODBC(SPACE(10)),'DIS_TINT','DIRIFCON');
          +Iif(Empty(i_ccchkf),"",",DIS_TINT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="DIS_TINT.DINUMDIS = t2.DINUMDIS";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DIS_TINT set (";
          +"DIRIFCON";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +cp_NullLink(cp_ToStrODBC(SPACE(10)),'DIS_TINT','DIRIFCON')+"";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="DIS_TINT.DINUMDIS = _t2.DINUMDIS";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DIS_TINT set ";
      +"DIRIFCON ="+cp_NullLink(cp_ToStrODBC(SPACE(10)),'DIS_TINT','DIRIFCON');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".DINUMDIS = "+i_cQueryTable+".DINUMDIS";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"DIRIFCON ="+cp_NullLink(cp_ToStrODBC(SPACE(10)),'DIS_TINT','DIRIFCON');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Write into PAR_TITE
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PAR_TITE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="PTSERIAL,PTROWORD,CPROWNUM"
      do vq_exec with 'GSCVBB62',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_TITE_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="PAR_TITE.PTSERIAL = _t2.PTSERIAL";
              +" and "+"PAR_TITE.PTROWORD = _t2.PTROWORD";
              +" and "+"PAR_TITE.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PTRIFIND ="+cp_NullLink(cp_ToStrODBC(SPACE(10)),'PAR_TITE','PTRIFIND');
      +",PTFLIMPE ="+cp_NullLink(cp_ToStrODBC("  "),'PAR_TITE','PTFLIMPE');
          +i_ccchkf;
          +" from "+i_cTable+" PAR_TITE, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="PAR_TITE.PTSERIAL = _t2.PTSERIAL";
              +" and "+"PAR_TITE.PTROWORD = _t2.PTROWORD";
              +" and "+"PAR_TITE.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" PAR_TITE, "+i_cQueryTable+" _t2 set ";
      +"PAR_TITE.PTRIFIND ="+cp_NullLink(cp_ToStrODBC(SPACE(10)),'PAR_TITE','PTRIFIND');
      +",PAR_TITE.PTFLIMPE ="+cp_NullLink(cp_ToStrODBC("  "),'PAR_TITE','PTFLIMPE');
          +Iif(Empty(i_ccchkf),"",",PAR_TITE.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="PAR_TITE.PTSERIAL = t2.PTSERIAL";
              +" and "+"PAR_TITE.PTROWORD = t2.PTROWORD";
              +" and "+"PAR_TITE.CPROWNUM = t2.CPROWNUM";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" PAR_TITE set (";
          +"PTRIFIND,";
          +"PTFLIMPE";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +cp_NullLink(cp_ToStrODBC(SPACE(10)),'PAR_TITE','PTRIFIND')+",";
          +cp_NullLink(cp_ToStrODBC("  "),'PAR_TITE','PTFLIMPE')+"";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="PAR_TITE.PTSERIAL = _t2.PTSERIAL";
              +" and "+"PAR_TITE.PTROWORD = _t2.PTROWORD";
              +" and "+"PAR_TITE.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" PAR_TITE set ";
      +"PTRIFIND ="+cp_NullLink(cp_ToStrODBC(SPACE(10)),'PAR_TITE','PTRIFIND');
      +",PTFLIMPE ="+cp_NullLink(cp_ToStrODBC("  "),'PAR_TITE','PTFLIMPE');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".PTSERIAL = "+i_cQueryTable+".PTSERIAL";
              +" and "+i_cTable+".PTROWORD = "+i_cQueryTable+".PTROWORD";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PTRIFIND ="+cp_NullLink(cp_ToStrODBC(SPACE(10)),'PAR_TITE','PTRIFIND');
      +",PTFLIMPE ="+cp_NullLink(cp_ToStrODBC("  "),'PAR_TITE','PTFLIMPE');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    if this.w_NHF>=0
      this.w_TMPC = Ah_MsgFormat("Aggiornamento DIRIFCON,PTRIFIND avvenuto correttamente")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = Ah_MsgFormat("Conversione eseguita correttamente")
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='PAR_TITE'
    this.cWorkTables[2]='DIS_TINT'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
