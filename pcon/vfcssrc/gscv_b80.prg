* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscv_b80                                                        *
*              Campo costo unitario tabella risorse                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_354]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-10-18                                                      *
* Last revis.: 2001-10-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscv_b80",oParentObject,m.w_NHF)
return(i_retval)

define class tgscv_b80 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_CODICE = space(15)
  w_PREZZO = 0
  * --- WorkFile variables
  RIPATMP1_idx=0
  TAB_RISO_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- FIle di LOG
    * --- Try
    local bErr_035F6390
    bErr_035F6390=bTrsErr
    this.Try_035F6390()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0 
        this.w_TMPC = this.oParentObject.w_PMSG
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_035F6390
    * --- End
  endproc
  proc Try_035F6390()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    i_nConn=i_TableProp[this.TAB_RISO_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.TAB_RISO_idx,2])
    if i_nConn<>0
      do case
        case upper(CP_DBTYPE)="DB2"
          * --- Caso DB2
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        case upper(CP_DBTYPE)="ORACLE"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        otherwise
          ah_ErrorMsg("Conversione prevista per database IBM DB2 e Oracle",,"")
          * --- commit
          cp_EndTrs(.t.)
      endcase
      if bTrsErr
        * --- Raise
        i_Error=MESSAGE()
        return
      else
        if this.w_NHF>=0 AND upper(CP_DBTYPE)="DB2"
          this.w_TMPC = ah_Msgformat("Adeguamento campo RIPREZZO tabella TAB_RISO eseguito correttamente%0ATTENZIONE: occorre effettuare la conversione su ogni azienda")
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.w_TMPC = ah_Msgformat("Adeguamento campo RIPREZZO tabella TAB_RISO eseguito correttamente%0ATTENZIONE: occorre effettuare la conversione su ogni azienda")
    this.oParentObject.w_PMSG = this.w_TMPC
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella DB2
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.TAB_RISO_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.TAB_RISO_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
          )
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    * --- Elimino la tabella
    if Not GSCV_BDT( this, "TAB_RISO" , i_nConn )
      * --- Raise
      i_Error="Error"
      return
    endif
    * --- Ricostruisco il database per la tabella
    GSCV_BRT(this, "TAB_RISO" , i_nConn , .T.)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into TAB_RISO
    i_nConn=i_TableProp[this.TAB_RISO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TAB_RISO_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.TAB_RISO_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    ah_Msg("Eliminata tabella temporanea tmp",.T.)
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella Oracle
    pName="TAB_RISO"
    pDatabaseType=i_ServerConn[1,6]
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.TAB_RISO_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.TAB_RISO_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"RICODICE,RIPREZZO "," from "+i_cTable;
          )
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    i_nConn=i_TableProp[this.TAB_RISO_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.TAB_RISO_idx,2])
    w_iRows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set "; 
 +"RIPREZZO =NULL")
    w_iRows = SQLExec(i_nConn,"ALTER TABLE "+i_cTable+; 
 " MODIFY(RIPREZZO NUMERIC(18,5))")
    * --- Select from RIPATMP1
    i_nConn=i_TableProp[this.RIPATMP1_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" RIPATMP1 ";
           ,"_Curs_RIPATMP1")
    else
      select * from (i_cTable);
        into cursor _Curs_RIPATMP1
    endif
    if used('_Curs_RIPATMP1')
      select _Curs_RIPATMP1
      locate for 1=1
      do while not(eof())
      this.w_CODICE = _Curs_RIPATMP1.RICODICE
      this.w_PREZZO = _Curs_RIPATMP1.RIPREZZO
      * --- Write into TAB_RISO
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TAB_RISO_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TAB_RISO_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TAB_RISO_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"RIPREZZO ="+cp_NullLink(cp_ToStrODBC(this.w_PREZZO),'TAB_RISO','RIPREZZO');
            +i_ccchkf ;
        +" where ";
            +"RICODICE = "+cp_ToStrODBC(this.w_CODICE);
               )
      else
        update (i_cTable) set;
            RIPREZZO = this.w_PREZZO;
            &i_ccchkf. ;
         where;
            RICODICE = this.w_CODICE;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
        select _Curs_RIPATMP1
        continue
      enddo
      use
    endif
  endproc


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='*RIPATMP1'
    this.cWorkTables[2]='TAB_RISO'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_RIPATMP1')
      use in _Curs_RIPATMP1
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
