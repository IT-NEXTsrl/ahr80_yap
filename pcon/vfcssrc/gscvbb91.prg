* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvbb91                                                        *
*              Aggiorna campo chiave della tabella DIC_INTE e campo MVRIFDIC della DOC_MAST*
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-01-16                                                      *
* Last revis.: 2009-10-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvbb91",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvbb91 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_CODAZI = space(5)
  w_AUTONUM = space(10)
  w_ANNO = space(4)
  w_ANNO1 = space(4)
  w_NUMPRO = space(3)
  w_TIPCON = space(1)
  w_TIPIVA = space(1)
  w_SERDOC = space(3)
  w_CODE = space(50)
  w_SERDIC = space(3)
  w_NEWCODE = space(50)
  w_ALFDIC = space(2)
  w_ALFDOC = space(2)
  w_PROGRESS = 0
  * --- WorkFile variables
  DIC_INTE_idx=0
  DOC_MAST_idx=0
  DOCSMAST_idx=0
  DES_DIVE_idx=0
  cpwarn_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna campo chiave della tabella DIC_INTE e campo MVRIFDIC della DOC_MAST
    *     Aggiorna gli autonumber e aggoirna i nuovi campi diserdic e diserdoc con i valori dei vecchi dialfic e dialfdoc
    this.w_CODAZI = I_CODAZI
    * --- Try
    local bErr_038171B8
    bErr_038171B8=bTrsErr
    this.Try_038171B8()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("Errore generico - impossibile eseguire aggiornamento richiesto")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_038171B8
    * --- End
  endproc
  proc Try_038171B8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    if g_APPLICATION="ad hoc ENTERPRISE"
      * --- Aggiorno il campo DISERIAL
      * --- Write into DIC_INTE
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DIC_INTE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DIC_INTE_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="DISERIAL"
        do vq_exec with 'GSCVAB03',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.DIC_INTE_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="DIC_INTE.DISERIAL = _t2.DISERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"DISERIAL = _t2.SERIALE";
            +i_ccchkf;
            +" from "+i_cTable+" DIC_INTE, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="DIC_INTE.DISERIAL = _t2.DISERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DIC_INTE, "+i_cQueryTable+" _t2 set ";
            +"DIC_INTE.DISERIAL = _t2.SERIALE";
            +Iif(Empty(i_ccchkf),"",",DIC_INTE.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="DIC_INTE.DISERIAL = t2.DISERIAL";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DIC_INTE set (";
            +"DISERIAL";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.SERIALE";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="DIC_INTE.DISERIAL = _t2.DISERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DIC_INTE set ";
            +"DISERIAL = _t2.SERIALE";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".DISERIAL = "+i_cQueryTable+".DISERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"DISERIAL = (select SERIALE from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Aggiorno il campo MVRIFDIC sui documenti
      * --- Write into DOC_MAST
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DOC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="MVSERIAL"
        do vq_exec with 'GSCVAB03_1',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="DOC_MAST.MVSERIAL = _t2.MVSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MVRIFDIC = _t2.MVRIFDIC";
            +i_ccchkf;
            +" from "+i_cTable+" DOC_MAST, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="DOC_MAST.MVSERIAL = _t2.MVSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_MAST, "+i_cQueryTable+" _t2 set ";
            +"DOC_MAST.MVRIFDIC = _t2.MVRIFDIC";
            +Iif(Empty(i_ccchkf),"",",DOC_MAST.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="DOC_MAST.MVSERIAL = t2.MVSERIAL";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_MAST set (";
            +"MVRIFDIC";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.MVRIFDIC";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="DOC_MAST.MVSERIAL = _t2.MVSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_MAST set ";
            +"MVRIFDIC = _t2.MVRIFDIC";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MVRIFDIC = (select MVRIFDIC from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Aggiorno il campo MVRIFDIC sui documenti Storico
      * --- Write into DOCSMAST
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DOCSMAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOCSMAST_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="MVSERIAL"
        do vq_exec with 'GSCVAB03_2',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.DOCSMAST_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="DOCSMAST.MVSERIAL = _t2.MVSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MVRIFDIC = _t2.MVRIFDIC";
            +i_ccchkf;
            +" from "+i_cTable+" DOCSMAST, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="DOCSMAST.MVSERIAL = _t2.MVSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOCSMAST, "+i_cQueryTable+" _t2 set ";
            +"DOCSMAST.MVRIFDIC = _t2.MVRIFDIC";
            +Iif(Empty(i_ccchkf),"",",DOCSMAST.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="DOCSMAST.MVSERIAL = t2.MVSERIAL";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOCSMAST set (";
            +"MVRIFDIC";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.MVRIFDIC";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="DOCSMAST.MVSERIAL = _t2.MVSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOCSMAST set ";
            +"MVRIFDIC = _t2.MVRIFDIC";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MVRIFDIC = (select MVRIFDIC from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Aggiorno il campo DDRFDICH sulle sedi clienti/fornitori
      * --- Write into DES_DIVE
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DES_DIVE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DES_DIVE_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="DDTIPCON,DDCODICE,DDCODDES"
        do vq_exec with 'GSCVAB03_3',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.DES_DIVE_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="DES_DIVE.DDTIPCON = _t2.DDTIPCON";
                +" and "+"DES_DIVE.DDCODICE = _t2.DDCODICE";
                +" and "+"DES_DIVE.DDCODDES = _t2.DDCODDES";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"DDRFDICH = _t2.DDRFDICH";
            +i_ccchkf;
            +" from "+i_cTable+" DES_DIVE, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="DES_DIVE.DDTIPCON = _t2.DDTIPCON";
                +" and "+"DES_DIVE.DDCODICE = _t2.DDCODICE";
                +" and "+"DES_DIVE.DDCODDES = _t2.DDCODDES";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DES_DIVE, "+i_cQueryTable+" _t2 set ";
            +"DES_DIVE.DDRFDICH = _t2.DDRFDICH";
            +Iif(Empty(i_ccchkf),"",",DES_DIVE.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="DES_DIVE.DDTIPCON = t2.DDTIPCON";
                +" and "+"DES_DIVE.DDCODICE = t2.DDCODICE";
                +" and "+"DES_DIVE.DDCODDES = t2.DDCODDES";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DES_DIVE set (";
            +"DDRFDICH";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.DDRFDICH";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="DES_DIVE.DDTIPCON = _t2.DDTIPCON";
                +" and "+"DES_DIVE.DDCODICE = _t2.DDCODICE";
                +" and "+"DES_DIVE.DDCODDES = _t2.DDCODDES";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DES_DIVE set ";
            +"DDRFDICH = _t2.DDRFDICH";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".DDTIPCON = "+i_cQueryTable+".DDTIPCON";
                +" and "+i_cTable+".DDCODICE = "+i_cQueryTable+".DDCODICE";
                +" and "+i_cTable+".DDCODDES = "+i_cQueryTable+".DDCODDES";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"DDRFDICH = (select DDRFDICH from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    else
      * --- Select from DIC_INTE
      i_nConn=i_TableProp[this.DIC_INTE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DIC_INTE_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" DIC_INTE ";
             ,"_Curs_DIC_INTE")
      else
        select * from (i_cTable);
          into cursor _Curs_DIC_INTE
      endif
      if used('_Curs_DIC_INTE')
        select _Curs_DIC_INTE
        locate for 1=1
        do while not(eof())
        this.w_ALFDIC = _Curs_DIC_INTE.DIALFDIC
        this.w_ALFDOC = _Curs_DIC_INTE.DIALFDOC
        * --- Write into DIC_INTE
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.DIC_INTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DIC_INTE_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.DIC_INTE_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"DISERDIC ="+cp_NullLink(cp_ToStrODBC(this.w_ALFDIC),'DIC_INTE','DISERDIC');
          +",DISERDOC ="+cp_NullLink(cp_ToStrODBC(this.w_ALFDOC),'DIC_INTE','DISERDOC');
              +i_ccchkf ;
          +" where ";
              +"DISERIAL = "+cp_ToStrODBC(_Curs_DIC_INTE.DISERIAL);
                 )
        else
          update (i_cTable) set;
              DISERDIC = this.w_ALFDIC;
              ,DISERDOC = this.w_ALFDOC;
              &i_ccchkf. ;
           where;
              DISERIAL = _Curs_DIC_INTE.DISERIAL;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
          select _Curs_DIC_INTE
          continue
        enddo
        use
      endif
    endif
    * --- Recupero tutti i progressivi dell'azienda PRDID e PRDIN.
    *     Per AHE aggiornereno PRDIN, per AHR PRDID.
    * --- Select from cpwarn
    i_nConn=i_TableProp[this.cpwarn_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.cpwarn_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" cpwarn ";
          +" where warncode="+cp_ToStrODBC(this.w_CODAZI)+" AND tablecode like '%PRDI%'";
           ,"_Curs_cpwarn")
    else
      select * from (i_cTable);
       where warncode=this.w_CODAZI AND tablecode like "%PRDI%";
        into cursor _Curs_cpwarn
    endif
    if used('_Curs_cpwarn')
      select _Curs_cpwarn
      locate for 1=1
      do while not(eof())
      this.w_CODE = ALLTRIM(_Curs_cpwarn.tablecode)
      this.w_AUTONUM = SUBSTR(this.w_CODE, 6,AT("\",this.w_CODE,2)-6)
      this.w_PROGRESS = _Curs_cpwarn.autonum
      if this.w_AUTONUM="PRDID" AND g_APPLICATION="ADHOC REVOLUTION"
        * --- Il progressivo in AHR era formato da:
        *     
        *     DI__ANNO
        *     DITIPCON
        *     DINUMPRO dove dinumpro era la somma di DITIPIVA e DIALFDOC
        *     
        *     Ora diventa
        *     
        *     DIAUTON2 composto da DI__ANNO e DISERDOC (Nuovo DIALFDOC di 3 caratteri piuttosto che 2)
        *     DITIPCON
        *     DITIPIVA
        * --- Calcolato tra il terzo e il quarto \
        this.w_ANNO = SUBSTR(this.w_CODE, AT("\",this.w_CODE,3)+1,AT("\",this.w_CODE,4)-AT("\",this.w_CODE,3)-1)
        * --- Calcolato 6 caratteri dopo il quinto \ (a fine stringa)
        *     La norma prevede 5 caratteri 3 + 2 apici.
        *     Su alcuni archivi ne abbiamo trovati 4+2 e quindi gestiamo anche questo caso(prendendone 6)
        this.w_NUMPRO = SUBSTR(this.w_CODE, AT("\",this.w_CODE,5)+1,6)
        * --- Prendo l'apice, il primo carattere e richiudo l'apice
        this.w_TIPIVA = left(this.w_NUMPRO,2)+"'"
        * --- prendo dal terzo carattere all'ultimo apice escluso.
        *     Se la stringa ottenuta � lunga 3, non devo fare nulla(archivio non corretto in quanto alfa di 2)
        *     se � lunga 2 aggiungo uno spazio in coda
        this.w_SERDOC = SUBSTR(this.w_NUMPRO, 3,LEN(this.w_NUMPRO)-1-3)
        if LEN(this.w_SERDOC)=2
          this.w_SERDOC = this.w_SERDOC+" "
        endif
        * --- Calcolato tra il quarto e il quinto \
        this.w_TIPCON = SUBSTR(this.w_CODE, AT("\",this.w_CODE,4)+1,AT("\",this.w_CODE,5)-AT("\",this.w_CODE,4)-1)
        this.w_NEWCODE = "prog\PRDID\'"+TRIM(this.w_CODAZI)+"'\"+LEFT(this.w_ANNO,5)+this.w_SERDOC+"'"++"\"+this.w_TIPCON+"\"+this.w_TIPIVA
        * --- Delete from cpwarn
        i_nConn=i_TableProp[this.cpwarn_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.cpwarn_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"tablecode = "+cp_ToStrODBC(this.w_NEWCODE);
                +" and warncode = "+cp_ToStrODBC(this.w_CODAZI);
                 )
        else
          delete from (i_cTable) where;
                tablecode = this.w_NEWCODE;
                and warncode = this.w_CODAZI;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        * --- Insert into cpwarn
        i_nConn=i_TableProp[this.cpwarn_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.cpwarn_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.cpwarn_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"tablecode"+",autonum"+",warncode"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_NEWCODE),'cpwarn','tablecode');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PROGRESS),'cpwarn','autonum');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CODAZI),'cpwarn','warncode');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'tablecode',this.w_NEWCODE,'autonum',this.w_PROGRESS,'warncode',this.w_CODAZI)
          insert into (i_cTable) (tablecode,autonum,warncode &i_ccchkf. );
             values (;
               this.w_NEWCODE;
               ,this.w_PROGRESS;
               ,this.w_CODAZI;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      endif
      if this.w_AUTONUM="PRDIN" 
        * --- AHE era
        *     
        *     DIAUTONU1 composto da DI__ANNO e DISERDIC
        *     DITIPCON
        *     
        *     Ora diventa
        *     
        *     DI__ANNO
        *     DITIPCON
        *     DISERDIC
        *     
        *     Per AHR � sufficiente aggiungere uno spazio in coda al serdic
        if g_APPLICATION="ad hoc ENTERPRISE"
          * --- Calcolato tra il terzo e il quarto \
          this.w_ANNO = SUBSTR(this.w_CODE, AT("\",this.w_CODE,3)+1,AT("\",this.w_CODE,4)-AT("\",this.w_CODE,3)-1)
          * --- Calcolato tra il quarto e il quinto \
          this.w_TIPCON = SUBSTR(this.w_CODE, AT("\",this.w_CODE,4)+1,AT("\",this.w_CODE,5)-AT("\",this.w_CODE,4)-1)
          this.w_SERDIC = "'"+SUBSTR(this.w_ANNO, 6,LEN(this.w_NUMPRO)-1-1)
          this.w_NEWCODE = "prog\PRDIN\'"+TRIM(this.w_CODAZI)+"'\"+LEFT(this.w_ANNO,6)+"\"+this.w_TIPCON+"\"+this.w_SERDIC
        else
          this.w_ANNO1 = SUBSTR(this.w_CODE, AT("\",this.w_CODE,3)+1,6)
          this.w_TIPCON = SUBSTR(this.w_CODE, AT("\",this.w_CODE,4)+1,AT("\",this.w_CODE,5)-AT("\",this.w_CODE,4)-1)
          if AT("\",this.w_CODE,5)>0
            this.w_SERDIC = SUBSTR(this.w_CODE, AT("\",this.w_CODE,5)+1)
            this.w_SERDIC = SUBSTR(this.w_SERDIC,1,3)+" "+"'"
          else
            this.w_SERDIC = "'"+"   "+"'"
          endif
          this.w_NEWCODE = "prog\PRDIN\'"+TRIM(this.w_CODAZI)+"'\"+LEFT(this.w_ANNO1,6)+"\"+this.w_TIPCON+"\"+this.w_SERDIC
        endif
        * --- Delete from cpwarn
        i_nConn=i_TableProp[this.cpwarn_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.cpwarn_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"warncode = "+cp_ToStrODBC(this.w_CODAZI);
                +" and tablecode = "+cp_ToStrODBC(this.w_NEWCODE);
                 )
        else
          delete from (i_cTable) where;
                warncode = this.w_CODAZI;
                and tablecode = this.w_NEWCODE;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        * --- Insert into cpwarn
        i_nConn=i_TableProp[this.cpwarn_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.cpwarn_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.cpwarn_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"tablecode"+",autonum"+",warncode"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_NEWCODE),'cpwarn','tablecode');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PROGRESS),'cpwarn','autonum');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CODAZI),'cpwarn','warncode');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'tablecode',this.w_NEWCODE,'autonum',this.w_PROGRESS,'warncode',this.w_CODAZI)
          insert into (i_cTable) (tablecode,autonum,warncode &i_ccchkf. );
             values (;
               this.w_NEWCODE;
               ,this.w_PROGRESS;
               ,this.w_CODAZI;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      endif
        select _Curs_cpwarn
        continue
      enddo
      use
    endif
    this.w_TMPC = ah_Msgformat("Aggiornamento campi DISERIAL e MVRIFDIC eseguita con successo")
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = this.w_TMPC
    * --- commit
    cp_EndTrs(.t.)
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='DIC_INTE'
    this.cWorkTables[2]='DOC_MAST'
    this.cWorkTables[3]='DOCSMAST'
    this.cWorkTables[4]='DES_DIVE'
    this.cWorkTables[5]='cpwarn'
    return(this.OpenAllTables(5))

  proc CloseCursors()
    if used('_Curs_DIC_INTE')
      use in _Curs_DIC_INTE
    endif
    if used('_Curs_cpwarn')
      use in _Curs_cpwarn
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
