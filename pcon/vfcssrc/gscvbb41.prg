* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvbb41                                                        *
*              Output utente a 30 char                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_11]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-05-18                                                      *
* Last revis.: 2006-05-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvbb41",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvbb41 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TMPC = space(10)
  w_TEMP = space(10)
  w_NOMPRG = space(30)
  w_OLDPRG = space(8)
  * --- WorkFile variables
  OUT_PUTS_idx=0
  RIPATMP1_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Il campo OUNOMPRG passa da 8 caratteri a 30 per dare la possibilit� di memorizzare nomi di programmi lunghi pi� di 8 caratteri
    * --- FIle di LOG
    * --- Try
    local bErr_035D0D80
    bErr_035D0D80=bTrsErr
    this.Try_035D0D80()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = AH_MsgFormat("ERRORE GENERICO - %1", this.oParentObject.w_PMSG)
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_035D0D80
    * --- End
  endproc
  proc Try_035D0D80()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    i_nConn=i_TableProp[this.OUT_PUTS_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.OUT_PUTS_idx,2])
    * --- begin transaction
    cp_BeginTrs()
    if i_nConn<>0
      do case
        case upper(CP_DBTYPE)="SQLSERVER"
          bTrsErr=bTrsErr or SQLExec(i_nConn,"alter table "+i_cTable+" drop constraint pk_"+i_cTable)=-1 
 bTrsErr=bTrsErr or SQLExec(i_nConn,"alter table "+i_cTable+" alter column OUNOMPRG char(30) NOT NULL ")=-1 
 bTrsErr=bTrsErr or SQLExec(i_nConn,"alter table "+i_cTable+" add constraint pk_"+i_cTable+; 
 " primary key (OUNOMPRG,OUROWNUM)")=-1
        case upper(CP_DBTYPE)="ORACLE"
          * --- Non occorre specificare NOT NULL nella ridefinizione del campo (� gia NOT NULL)
          bTrsErr=bTrsErr or SQLExec(i_nConn,"alter table "+i_cTable+" drop constraint pk_"+i_cTable)=-1 
 bTrsErr=bTrsErr or SQLExec(i_nConn,"alter table "+i_cTable+" modify OUNOMPRG char(30) ")=-1 
 bTrsErr=bTrsErr or SQLExec(i_nConn,"alter table "+i_cTable+" add constraint pk_"+i_cTable+; 
 " primary key (OUNOMPRG,OUROWNUM)")=-1
        case upper(CP_DBTYPE)="DB2"
          * --- Caso DB2
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
      endcase
    endif
    if bTrsErr
      this.w_TMPC = AH_MsgFormat("Impossibile modificare la struttura dell'Output utente")
      * --- Raise
      i_Error=this.w_TMPC
      return
    else
      if this.w_NHF>=0
        this.w_TMPC = AH_MsgFormat("Aggiornamento struttura Output utente completato con successo")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    if this.w_NHF>=0
      this.w_TMPC = AH_MsgFormat("Aggiornamento terminato con successo")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = AH_MsgFormat("Conversione eseguita correttamente")
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella DB2
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.OUT_PUTS_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.OUT_PUTS_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
          )
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    * --- Select from RIPATMP1
    i_nConn=i_TableProp[this.RIPATMP1_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" RIPATMP1 ";
           ,"_Curs_RIPATMP1")
    else
      select * from (i_cTable);
        into cursor _Curs_RIPATMP1
    endif
    if used('_Curs_RIPATMP1')
      select _Curs_RIPATMP1
      locate for 1=1
      do while not(eof())
      this.w_OLDPRG = _Curs_RIPATMP1.OUNOMPRG
      this.w_NOMPRG = LEFT(this.w_OLDPRG+ SPACE(30),30)
      * --- Write into RIPATMP1
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.RIPATMP1_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.RIPATMP1_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"OUNOMPRG ="+cp_NullLink(cp_ToStrODBC(this.w_NOMPRG),'RIPATMP1','OUNOMPRG');
            +i_ccchkf ;
        +" where ";
            +"OUNOMPRG = "+cp_ToStrODBC(this.w_OLDPRG);
               )
      else
        update (i_cTable) set;
            OUNOMPRG = this.w_NOMPRG;
            &i_ccchkf. ;
         where;
            OUNOMPRG = this.w_OLDPRG;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
        select _Curs_RIPATMP1
        continue
      enddo
      use
    endif
    * --- Elimino la tabella
    if Not GSCV_BDT( this, "OUT_PUTS" , i_nConn )
      * --- Raise
      i_Error="Error"
      return
    endif
    * --- Ricostruisco il database per la tabella
    GSCV_BRT(this, "OUT_PUTS" , i_nConn , .T.)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Try
    local bErr_035FCB50
    bErr_035FCB50=bTrsErr
    this.Try_035FCB50()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_035FCB50
    * --- End
    ah_Msg("Ripristinati valori di origine",.T.)
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    ah_Msg("Eliminata tabella temporanea tmp",.T.)
  endproc
  proc Try_035FCB50()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into OUT_PUTS
    i_nConn=i_TableProp[this.OUT_PUTS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OUT_PUTS_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.OUT_PUTS_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='OUT_PUTS'
    this.cWorkTables[2]='*RIPATMP1'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_RIPATMP1')
      use in _Curs_RIPATMP1
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
