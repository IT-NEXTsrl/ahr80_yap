* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcc21                                                        *
*              Genera dati cespiti ammortamento civile                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-09-12                                                      *
* Last revis.: 2012-09-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcc21",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcc21 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_Cecodice = space(20)
  w_Ceespric = space(4)
  w_Cecodese = space(4)
  w_Iniese = ctod("  /  /  ")
  w_Iniese2 = ctod("  /  /  ")
  w_Cecoeciv = 0
  w_Ceflamci = 0
  w_Ceperciv = 0
  w_Cequociv = 0
  w_Codazi = space(5)
  w_Pctipamm = space(1)
  w_Aggiorna = .f.
  w_Tmpc = space(10)
  w_Temp = space(10)
  * --- WorkFile variables
  CES_PITI_idx=0
  ESERCIZI_idx=0
  PAR_CESP_idx=0
  AMC_CESP_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questa procedura valorizza la tabella AMC_CESP 
    * --- FIle di LOG
    this.w_Codazi = i_Codazi
    this.w_Aggiorna = .f.
    * --- Read from PAR_CESP
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_CESP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_CESP_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PCTIPAMM"+;
        " from "+i_cTable+" PAR_CESP where ";
            +"PCCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PCTIPAMM;
        from (i_cTable) where;
            PCCODAZI = this.w_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PCTIPAMM = NVL(cp_ToDate(_read_.PCTIPAMM),cp_NullValue(_read_.PCTIPAMM))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Try
    local bErr_04A91620
    bErr_04A91620=bTrsErr
    this.Try_04A91620()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE %1 - impossibile aggiornare la tabella cespiti/ammortamenti", this.oParentObject.w_PMSG)
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_04A91620
    * --- End
  endproc
  proc Try_04A91620()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Verifico la tipologia di ammortamento specificata nei "Parametri Cespiti"
    *     Nel caso fosse stato impostato "Solo fiscale" la procedura di conversione
    *     non deve effettuare nessuna operazione
    if this.w_Pctipamm $ "EC"
      * --- Select from Gscvcc21
      do vq_exec with 'Gscvcc21',this,'_Curs_Gscvcc21','',.f.,.t.
      if used('_Curs_Gscvcc21')
        select _Curs_Gscvcc21
        locate for 1=1
        do while not(eof())
        this.w_Cecodice = _Curs_Gscvcc21.Cecodice
        this.w_Ceespric = _Curs_Gscvcc21.Ceespric
        * --- Read from ESERCIZI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ESERCIZI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ESINIESE"+;
            " from "+i_cTable+" ESERCIZI where ";
                +"ESCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
                +" and ESCODESE = "+cp_ToStrODBC(this.w_CEESPRIC);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ESINIESE;
            from (i_cTable) where;
                ESCODAZI = this.w_CODAZI;
                and ESCODESE = this.w_CEESPRIC;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_INIESE = NVL(cp_ToDate(_read_.ESINIESE),cp_NullValue(_read_.ESINIESE))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_Cecodese = _Curs_Gscvcc21.Cecodese
        * --- Read from ESERCIZI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ESERCIZI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ESINIESE"+;
            " from "+i_cTable+" ESERCIZI where ";
                +"ESCODAZI = "+cp_ToStrODBC(i_CODAZI);
                +" and ESCODESE = "+cp_ToStrODBC(this.w_CECODESE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ESINIESE;
            from (i_cTable) where;
                ESCODAZI = i_CODAZI;
                and ESCODESE = this.w_CECODESE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_INIESE2 = NVL(cp_ToDate(_read_.ESINIESE),cp_NullValue(_read_.ESINIESE))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_Cecoeciv = NVL(_Curs_Gscvcc21.Cecoeciv,0)
        this.w_Ceflamci = NVL(_Curs_Gscvcc21.Ceflamci,"I")
        this.w_Ceperciv = NVL(_Curs_Gscvcc21.Ceperciv,0)
        this.w_Cequociv = NVL(_Curs_Gscvcc21.Cequociv,0)
        * --- Insert into AMC_CESP
        i_nConn=i_TableProp[this.AMC_CESP_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.AMC_CESP_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"Gscvcc21a",this.AMC_CESP_idx)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
          select _Curs_Gscvcc21
          continue
        enddo
        use
      endif
      this.w_Aggiorna = .t.
    endif
    * --- commit
    cp_EndTrs(.t.)
    if bTrsErr
      * --- Raise
      i_Error=i_TrsMsg
      return
    endif
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    if this.w_Aggiorna
      this.oParentObject.w_PMSG = ah_Msgformat("Conversione eseguita correttamente")
      * --- Lancia il report di controllo
      vx_exec("..\PCON\EXE\QUERY\GSCVCC21C.VQR, ..\PCON\EXE\QUERY\GSCVCC21.FRX",this)
    else
      this.oParentObject.w_PMSG = ah_Msgformat("Nei parametri cespiti � stato impostato come tipo ammortamento solo fiscale%0Non � stato effettuato nessun aggiornamento")
    endif
    if this.w_NHF>=0 
      this.w_TMPC = this.oParentObject.w_PMSG
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='CES_PITI'
    this.cWorkTables[2]='ESERCIZI'
    this.cWorkTables[3]='PAR_CESP'
    this.cWorkTables[4]='AMC_CESP'
    return(this.OpenAllTables(4))

  proc CloseCursors()
    if used('_Curs_Gscvcc21')
      use in _Curs_Gscvcc21
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
