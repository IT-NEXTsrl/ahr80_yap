* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvab72                                                        *
*              Controllo codici ENASARCO                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_20]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-03-30                                                      *
* Last revis.: 2005-03-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvab72",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvab72 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_CODENA = space(15)
  w_CONTA = 0
  w_LEN = 0
  w_TESTDEL = .f.
  * --- WorkFile variables
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- CONTROLLO CODICI ENASARCO
    * --- FIle di LOG
    this.w_TESTDEL = .F.
    VQ_EXEC("..\PCON\EXE\QUERY\GSCVAB72.VQR",this,"AGENTI")
     
 SELECT AGENTI 
 GO TOP 
 SCAN
    this.w_CODENA = ALLTRIM(AGCODENA)
    this.w_LEN = LEN(this.w_CODENA)
    this.w_CONTA = 1
    if this.w_LEN>8
      this.w_TESTDEL = .F.
    else
      this.w_TESTDEL = IS_NUMBER(this.w_CODENA, "")
    endif
    if this.w_TESTDEL
       
 SELECT AGENTI 
 DELETE
    endif
    endscan
    select * from AGENTI where not deleted() into cursor __tmp__
    if RECCOUNT("__TMP__")>0
      CP_CHPRN("..\PCON\EXE\QUERY\GSCVAB72.FRX", " ", this)
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE %1 - esistono codici Enasarco incongruenti ", this.oParentObject.w_PMSG )
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    else
      * --- Esecuzione ok
      this.oParentObject.w_PESEOK = .T.
      this.oParentObject.w_PMSG = ah_Msgformat("Conversione eseguita correttamente")
      if this.w_NHF>=0 
        this.w_TMPC = this.oParentObject.w_PMSG
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    if used("AGENTI")
      select AGENTI
      use
    endif
    if used("__TMP__")
      select __TMP__
      use
    endif
  endproc


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
