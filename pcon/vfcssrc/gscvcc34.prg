* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcc34                                                        *
*              Valorizzazione campo MDLOTMAG in righe importate di CORDRISP    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_100]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-11-08                                                      *
* Last revis.: 2013-11-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcc34",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcc34 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_MESS = space(10)
  w_TableError = space(10)
  * --- WorkFile variables
  CORDRISP_idx=0
  runtime_filters = 2

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Valorizzazione campo MDLOTMAG in righe importate di vendita negozio
    this.w_TableError = "CORDRISP"
    * --- Try
    local bErr_04A90ED0
    bErr_04A90ED0=bTrsErr
    this.Try_04A90ED0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = ah_Msgformat("ERRORE GENERICO - impossibile eseguire aggiornamento richiesto, tabella: %1 %2", this.w_TableError , Message() )
      if this.w_NHF>=0
        this.w_TMPC = this.oParentObject.w_PMSG
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
      this.oParentObject.w_PESEOK = .F.
    endif
    bTrsErr=bTrsErr or bErr_04A90ED0
    * --- End
  endproc
  proc Try_04A90ED0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into CORDRISP
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.CORDRISP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CORDRISP_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="MDSERIAL,CPROWNUM"
      do vq_exec with 'GSCVCC34',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.CORDRISP_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="CORDRISP.MDSERIAL = _t2.MDSERIAL";
              +" and "+"CORDRISP.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MDLOTMAG = _t2.MDLOTMAG";
          +i_ccchkf;
          +" from "+i_cTable+" CORDRISP, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="CORDRISP.MDSERIAL = _t2.MDSERIAL";
              +" and "+"CORDRISP.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CORDRISP, "+i_cQueryTable+" _t2 set ";
          +"CORDRISP.MDLOTMAG = _t2.MDLOTMAG";
          +Iif(Empty(i_ccchkf),"",",CORDRISP.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="CORDRISP.MDSERIAL = t2.MDSERIAL";
              +" and "+"CORDRISP.CPROWNUM = t2.CPROWNUM";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CORDRISP set (";
          +"MDLOTMAG";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.MDLOTMAG";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="CORDRISP.MDSERIAL = _t2.MDSERIAL";
              +" and "+"CORDRISP.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CORDRISP set ";
          +"MDLOTMAG = _t2.MDLOTMAG";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".MDSERIAL = "+i_cQueryTable+".MDSERIAL";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MDLOTMAG = (select MDLOTMAG from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = Empty(this.w_TMPC)
    if this.oParentObject.w_PESEOK And this.w_NHF>0
      this.w_MESS = Ah_Msgformat("Aggiornamento campo MDLOTMAG avvenuto correttamente")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_MESS)
    endif
    this.oParentObject.w_PMSG = this.w_MESS
    return

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='CORDRISP'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
