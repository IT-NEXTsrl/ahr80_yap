* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvab62                                                        *
*              4.0 - modifica lunghezza su DB Oracle log storico               *
*                                                                              *
*      Author: Zucchetti TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_73]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-05-07                                                      *
* Last revis.: 2002-05-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvab62",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvab62 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_ROWS = 0
  * --- WorkFile variables
  LOG_STOR_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- FIle di LOG
    *     4.0 - MODIFICA LUNGHEZZA SU DB ORACLE LOG STORICO
    * --- Try
    local bErr_03600A20
    bErr_03600A20=bTrsErr
    this.Try_03600A20()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_msgformat("ERRORE GENERICO - impossibile eseguire aggiornamento richiesto")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_03600A20
    * --- End
  endproc
  proc Try_03600A20()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.w_ROWS = 0
    if upper(CP_DBTYPE)="ORACLE"
      i_nConn=i_TableProp[this.LOG_STOR_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.LOG_STOR_idx,2])
      this.w_ROWS = SQLExec(i_nConn,"ALTER TABLE "+i_cTable+" MODIFY(LG__NOTE VARCHAR2(4000))")
      this.w_ROWS = SQLExec(i_nConn,"ALTER TABLE xxxLOG_STOR MODIFY(LG__NOTE VARCHAR2(4000))")
    endif
    if this.w_ROWS<0
      * --- Raise
      i_Error=Message()
      return
    else
      * --- Esecuzione ok
      this.oParentObject.w_PESEOK = .T.
      this.w_TMPC = ah_Msgformat("Aggiornamento dimensione campo note log storico eseguito con successo%0")
      this.oParentObject.w_PMSG = this.w_TMPC
    endif
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='LOG_STOR'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
