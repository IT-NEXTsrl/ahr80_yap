* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvbb97                                                        *
*              Ricostruzione e aggiornamento campi saldi lotti ubicazioni      *
*                                                                              *
*      Author: Nicola Gorlandi                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: TRCF][111][VRS_33]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-03-31                                                      *
* Last revis.: 2008-07-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvbb97",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvbb97 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TMPC = space(10)
  w_TEMP = space(10)
  * --- WorkFile variables
  DOC_DETT_idx=0
  DOC_MAST_idx=0
  MVM_DETT_idx=0
  SALDILOT_idx=0
  CORDRISP_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Valorizza i campi MVLOTMAT dei documenti  e MMLOTMAT dei movimenti di magazzino  conformemente a quanto avviene nelle maschere dei documenti/movimenti di magazzino
    *       (ex:    iif( Empty( w_MVCODLOT ) , SPACE(5) , w_MVCODMAT ))
    *     Copia i saldi fuori linea contenuti nella tabella SALLOTUBI
    *     Lancia aggiornamento saldi lotti
    * --- FIle di LOG
    * --- Try
    local bErr_04A83270
    bErr_04A83270=bTrsErr
    this.Try_04A83270()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("La procedura di conversione � terminata con errore : %1", this.oParentObject.w_PMSG)
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_04A83270
    * --- End
  endproc
  proc Try_04A83270()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    ah_Msg("Inizio aggiornamento documenti",.T.)
    * --- Aggiorna campo MVLOTMAT dei documenti
    * --- Write into DOC_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="MVSERIAL,CPROWNUM"
      do vq_exec with 'gscvbb97d',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
              +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MVLOTMAG = _t2.MVCODMAG";
          +",MVLOTMAT  = _t2.MVCODMAT ";
          +i_ccchkf;
          +" from "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
              +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 set ";
          +"DOC_DETT.MVLOTMAG = _t2.MVCODMAG";
          +",DOC_DETT.MVLOTMAT  = _t2.MVCODMAT ";
          +Iif(Empty(i_ccchkf),"",",DOC_DETT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="DOC_DETT.MVSERIAL = t2.MVSERIAL";
              +" and "+"DOC_DETT.CPROWNUM = t2.CPROWNUM";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set (";
          +"MVLOTMAG,";
          +"MVLOTMAT ";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.MVCODMAG,";
          +"t2.MVCODMAT ";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
              +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set ";
          +"MVLOTMAG = _t2.MVCODMAG";
          +",MVLOTMAT  = _t2.MVCODMAT ";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MVLOTMAG = (select MVCODMAG from "+i_cQueryTable+" where "+i_cWhere+")";
          +",MVLOTMAT  = (select MVCODMAT  from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Aggiorna campo MMLOTMAT dei movimenti di magazzino
    ah_Msg("Inizio aggiornamento movimenti di magazzino",.T.)
    * --- Write into MVM_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.MVM_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MVM_DETT_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="MMSERIAL,CPROWNUM"
      do vq_exec with 'gscvbb97m',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.MVM_DETT_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="MVM_DETT.MMSERIAL = _t2.MMSERIAL";
              +" and "+"MVM_DETT.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MMLOTMAG = _t2.MMCODMAG";
          +",MMLOTMAT  = _t2.MMCODMAT ";
          +i_ccchkf;
          +" from "+i_cTable+" MVM_DETT, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="MVM_DETT.MMSERIAL = _t2.MMSERIAL";
              +" and "+"MVM_DETT.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MVM_DETT, "+i_cQueryTable+" _t2 set ";
          +"MVM_DETT.MMLOTMAG = _t2.MMCODMAG";
          +",MVM_DETT.MMLOTMAT  = _t2.MMCODMAT ";
          +Iif(Empty(i_ccchkf),"",",MVM_DETT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="MVM_DETT.MMSERIAL = t2.MMSERIAL";
              +" and "+"MVM_DETT.CPROWNUM = t2.CPROWNUM";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MVM_DETT set (";
          +"MMLOTMAG,";
          +"MMLOTMAT ";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.MMCODMAG,";
          +"t2.MMCODMAT ";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="MVM_DETT.MMSERIAL = _t2.MMSERIAL";
              +" and "+"MVM_DETT.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MVM_DETT set ";
          +"MMLOTMAG = _t2.MMCODMAG";
          +",MMLOTMAT  = _t2.MMCODMAT ";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".MMSERIAL = "+i_cQueryTable+".MMSERIAL";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MMLOTMAG = (select MMCODMAG from "+i_cQueryTable+" where "+i_cWhere+")";
          +",MMLOTMAT  = (select MMCODMAT  from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    if g_GPOS="S"
      ah_Msg("Inizio aggiornamento vendite negozio",.T.)
      * --- Write into CORDRISP
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.CORDRISP_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CORDRISP_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="MDSERIAL,CPROWNUM"
        do vq_exec with 'gscvbb97p',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CORDRISP_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="CORDRISP.MDSERIAL = _t2.MDSERIAL";
                +" and "+"CORDRISP.CPROWNUM = _t2.CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MDLOTMAG = _t2.MDLOTMAG";
            +i_ccchkf;
            +" from "+i_cTable+" CORDRISP, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="CORDRISP.MDSERIAL = _t2.MDSERIAL";
                +" and "+"CORDRISP.CPROWNUM = _t2.CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CORDRISP, "+i_cQueryTable+" _t2 set ";
            +"CORDRISP.MDLOTMAG = _t2.MDLOTMAG";
            +Iif(Empty(i_ccchkf),"",",CORDRISP.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="CORDRISP.MDSERIAL = t2.MDSERIAL";
                +" and "+"CORDRISP.CPROWNUM = t2.CPROWNUM";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CORDRISP set (";
            +"MDLOTMAG";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.MDLOTMAG";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="CORDRISP.MDSERIAL = _t2.MDSERIAL";
                +" and "+"CORDRISP.CPROWNUM = _t2.CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CORDRISP set ";
            +"MDLOTMAG = _t2.MDLOTMAG";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".MDSERIAL = "+i_cQueryTable+".MDSERIAL";
                +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MDLOTMAG = (select MDLOTMAG from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    if g_PERLOT="S" OR g_PERUBI="S"
      * --- Calcola saldi e popola tabella l anagrafica dei saldi lotti 
      ah_Msg("Ricostruzione saldi lotti/ubicazioni",.T.)
      GSMD_BRL (this, , , , , .T. , this)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    this.oParentObject.w_PESEOK = .T.
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Elaborazione terminata con successo")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='DOC_DETT'
    this.cWorkTables[2]='DOC_MAST'
    this.cWorkTables[3]='MVM_DETT'
    this.cWorkTables[4]='SALDILOT'
    this.cWorkTables[5]='CORDRISP'
    return(this.OpenAllTables(5))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
