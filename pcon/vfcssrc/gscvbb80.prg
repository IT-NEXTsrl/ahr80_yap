* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvbb80                                                        *
*              Aggiorna lunghezza massima elementi                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_6]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-03-23                                                      *
* Last revis.: 2007-08-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvbb80",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvbb80 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_MESS = space(100)
  w_CPROWNUM = 0
  w_OLDCOD = space(21)
  * --- WorkFile variables
  VAELEMEN_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questa procedura di conversione esegue:
    *      Aggiornamento lunghezza massima per elemeti di tipo Valore\Espressione di tipo 
    *      descrittivo per i quali in passato non era prevista.
    * --- Try
    local bErr_04A9EA00
    bErr_04A9EA00=bTrsErr
    this.Try_04A9EA00()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = this.oParentObject.w_PMSG
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_04A9EA00
    * --- End
  endproc
  proc Try_04A9EA00()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Aggiornamento campo lunghezza MASSIMA neglielementi
    * --- Write into VAELEMEN
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.VAELEMEN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VAELEMEN_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="ELCODSTR,ELCODICE"
      do vq_exec with 'gscvbb63_1',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.VAELEMEN_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="VAELEMEN.ELCODSTR = _t2.ELCODSTR";
              +" and "+"VAELEMEN.ELCODICE = _t2.ELCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ELLUNMAX = _t2.LUNMAX";
          +i_ccchkf;
          +" from "+i_cTable+" VAELEMEN, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="VAELEMEN.ELCODSTR = _t2.ELCODSTR";
              +" and "+"VAELEMEN.ELCODICE = _t2.ELCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" VAELEMEN, "+i_cQueryTable+" _t2 set ";
          +"VAELEMEN.ELLUNMAX = _t2.LUNMAX";
          +Iif(Empty(i_ccchkf),"",",VAELEMEN.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="VAELEMEN.ELCODSTR = t2.ELCODSTR";
              +" and "+"VAELEMEN.ELCODICE = t2.ELCODICE";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" VAELEMEN set (";
          +"ELLUNMAX";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.LUNMAX";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="VAELEMEN.ELCODSTR = _t2.ELCODSTR";
              +" and "+"VAELEMEN.ELCODICE = _t2.ELCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" VAELEMEN set ";
          +"ELLUNMAX = _t2.LUNMAX";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".ELCODSTR = "+i_cQueryTable+".ELCODSTR";
              +" and "+i_cTable+".ELCODICE = "+i_cQueryTable+".ELCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ELLUNMAX = (select LUNMAX from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    ah_Msg("Aggiornamento lunghezza massima elementi",.T.)
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.w_TMPC = ah_Msgformat("Adeguamento tabella VAELEMEN eseguito correttamente")
    this.oParentObject.w_PMSG = this.w_TMPC
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questa Procedura aggiorna il cprownum della GESTIONE TRASCODIFICHE
    this.w_OLDCOD = Repl("@",21)
    * --- Try
    local bErr_04A9A200
    bErr_04A9A200=bTrsErr
    this.Try_04A9A200()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE %1 -impossibile aggiornare la gestione trascodifiche", this.oParentObject.w_PMSG )
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_04A9A200
    * --- End
  endproc
  proc Try_04A9A200()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    ah_Msg("Aggiornamento CPROWNUM",.T.)
    * --- Select from GSCVBB63
    do vq_exec with 'GSCVBB63',this,'_Curs_GSCVBB63','',.f.,.t.
    if used('_Curs_GSCVBB63')
      select _Curs_GSCVBB63
      locate for 1=1
      do while not(eof())
      if _Curs_GSCVBB63.TRCODICE <> this.w_OLDCOD
        this.w_CPROWNUM = 0
      endif
      this.w_CPROWNUM = this.w_CPROWNUM + 1
      * --- Write into TRS_DETT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TRS_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TRS_DETT_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TRS_DETT_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"CPROWNUM ="+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'TRS_DETT','CPROWNUM');
            +i_ccchkf ;
        +" where ";
            +"TRCODICE = "+cp_ToStrODBC(_Curs_GSCVBB63.TRCODICE);
            +" and TRCODINT = "+cp_ToStrODBC(_Curs_GSCVBB63.TRCODINT);
               )
      else
        update (i_cTable) set;
            CPROWNUM = this.w_CPROWNUM;
            &i_ccchkf. ;
         where;
            TRCODICE = _Curs_GSCVBB63.TRCODICE;
            and TRCODINT = _Curs_GSCVBB63.TRCODINT;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      this.w_OLDCOD = _Curs_GSCVBB63.TRCODICE
        select _Curs_GSCVBB63
        continue
      enddo
      use
    endif
    if bTrsErr
      * --- Raise
      i_Error=i_TrsMsg
      return
    endif
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = ah_Msgformat("Conversione eseguita correttamente")
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Conversione eseguita correttamente")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='VAELEMEN'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_GSCVBB63')
      use in _Curs_GSCVBB63
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
