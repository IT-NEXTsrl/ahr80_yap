* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcb82                                                        *
*              F23 - Anno e numero documento alfanumerici                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-07-08                                                      *
* Last revis.: 2010-10-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcb82",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcb82 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  * --- WorkFile variables
  F23_PAGA_idx=0
  AZIENDA_idx=0
  SEDIAZIE_idx=0
  NOM_CONT_idx=0
  OFF_NOMI_idx=0
  UTE_NTI_idx=0
  MAGAZZIN_idx=0
  AGENTI_idx=0
  TITOLARI_idx=0
  CONTI_idx=0
  DES_DIVE_idx=0
  DIPENDEN_idx=0
  CAN_TIER_idx=0
  BAN_CONTI_idx=0
  SED_STOR_idx=0
  CONTATTI_idx=0
  DAT_RAPP_idx=0
  OFF_ATTI_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- F23 - Anno e numero documento alfanumerici
    if IsAlt()
      * --- Solo per AeTop
      * --- Try
      local bErr_04A919B0
      bErr_04A919B0=bTrsErr
      this.Try_04A919B0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        * --- Gestisce log errori
        this.oParentObject.w_PMSG = Message()
        this.oParentObject.w_PESEOK = .F.
        if this.w_NHF>=0
          this.w_TMPC = ah_Msgformat("Errore generico - impossibile eseguire aggiornamento richiesto")
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
      bTrsErr=bTrsErr or bErr_04A919B0
      * --- End
    else
      this.w_TMPC = ah_Msgformat("Procedura di conversione eseguita con successo")
      this.oParentObject.w_PMSG = this.w_TMPC
      * --- Esecuzione ok
      this.oParentObject.w_PESEOK = .T.
    endif
  endproc
  proc Try_04A919B0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Aggiorno i campi PAADOC_C e PANDOC_C
    * --- Write into F23_PAGA
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.F23_PAGA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.F23_PAGA_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="PASERIAL"
      do vq_exec with 'gscvcb82',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.F23_PAGA_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="F23_PAGA.PASERIAL = _t2.PASERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PAADOC_C = _t2.ANNDOC";
          +",PANDOC_C = _t2.NUMDOC";
          +i_ccchkf;
          +" from "+i_cTable+" F23_PAGA, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="F23_PAGA.PASERIAL = _t2.PASERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" F23_PAGA, "+i_cQueryTable+" _t2 set ";
          +"F23_PAGA.PAADOC_C = _t2.ANNDOC";
          +",F23_PAGA.PANDOC_C = _t2.NUMDOC";
          +Iif(Empty(i_ccchkf),"",",F23_PAGA.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="F23_PAGA.PASERIAL = t2.PASERIAL";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" F23_PAGA set (";
          +"PAADOC_C,";
          +"PANDOC_C";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.ANNDOC,";
          +"t2.NUMDOC";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="F23_PAGA.PASERIAL = _t2.PASERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" F23_PAGA set ";
          +"PAADOC_C = _t2.ANNDOC";
          +",PANDOC_C = _t2.NUMDOC";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".PASERIAL = "+i_cQueryTable+".PASERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PAADOC_C = (select ANNDOC from "+i_cQueryTable+" where "+i_cWhere+")";
          +",PANDOC_C = (select NUMDOC from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    this.w_TMPC = ah_Msgformat("Aggiornamento campi PAADOC_C e PANDOC_C eseguito con successo")
    this.oParentObject.w_PMSG = this.w_TMPC
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if g_AGEN="S" AND NOT ISALT()
      * --- Impianti
      * --- Select from ..\AGEN\EXE\QUERY\TELIMPIAN.VQR
      do vq_exec with '..\AGEN\EXE\QUERY\TELIMPIAN.VQR',this,'_Curs__d__d__AGEN_EXE_QUERY_TELIMPIAN_d_VQR','',.f.,.t.
      if used('_Curs__d__d__AGEN_EXE_QUERY_TELIMPIAN_d_VQR')
        select _Curs__d__d__AGEN_EXE_QUERY_TELIMPIAN_d_VQR
        locate for 1=1
        do while not(eof())
        this.w_TABELLA = "IMP_MAST"
        this.w_DESKEY1 = "IMCODICE"
        this.w_KEY1 = IMCODICE
        this.w_OLDTEL = IMTELEFO
        this.w_TEL = ConvNum(IMTELEFO, "+", 1)
        if NOT(ALLTRIM(this.w_OLDTEL)==ALLTRIM(this.w_TEL))
          this.w_ERROR = this.w_ERROR+1
          this.w_oERRORMESS.AddMsgLogPartNoTrans(space(4), "Tabella Impianti Cod.: %1 Telef.: %2 Telef. normalizzato: %3", IMCODICE, this.w_OLDTEL, this.w_TEL)     
          INSERT INTO Temp (TABE, DESKEY1, KEY1, CAMPOTEL, TEL) VALUES (this.w_TABELLA, this.w_DESKEY1, this.w_KEY1, "IMTELEFO", this.w_TEL)
        endif
          select _Curs__d__d__AGEN_EXE_QUERY_TELIMPIAN_d_VQR
          continue
        enddo
        use
      endif
    endif
    if ISAHR() AND g_GPOS="S"
      * --- Clienti negozio
      * --- Select from ..\PCON\EXE\QUERY\TELCLIENTI.VQR
      do vq_exec with '..\PCON\EXE\QUERY\TELCLIENTI.VQR',this,'_Curs__d__d__PCON_EXE_QUERY_TELCLIENTI_d_VQR','',.f.,.t.
      if used('_Curs__d__d__PCON_EXE_QUERY_TELCLIENTI_d_VQR')
        select _Curs__d__d__PCON_EXE_QUERY_TELCLIENTI_d_VQR
        locate for 1=1
        do while not(eof())
        this.w_TABELLA = "CLI_VEND"
        this.w_DESKEY1 = "CLCODCLI"
        this.w_KEY1 = CLCODCLI
        this.w_OLDTEL = CLTELEFO
        this.w_TEL = ConvNum(CLTELEFO, "+", 1)
        this.w_OLDFAX = CLTELFAX
        this.w_FAX = ConvNum(CLTELFAX, "+", 1)
        this.w_OLDCEL = CLNUMCEL
        this.w_CEL = ConvNum(CLNUMCEL, "+", 1)
        if NOT(ALLTRIM(this.w_OLDTEL)==ALLTRIM(this.w_TEL)) OR NOT(ALLTRIM(this.w_OLDFAX)==ALLTRIM(this.w_FAX)) OR NOT(ALLTRIM(this.w_OLDCEL)==ALLTRIM(this.w_CEL))
          this.w_ERROR = this.w_ERROR+1
          if NOT(ALLTRIM(this.w_OLDTEL)==ALLTRIM(this.w_TEL)) 
            this.w_oERRORMESS.AddMsgLogPartNoTrans(space(4), "Tabella Clienti negozio Cod.: %1 Telef.: %2 Telef. normalizzato: %3", CLCODCLI, this.w_OLDTEL, this.w_TEL)     
          endif
          if NOT(ALLTRIM(this.w_OLDFAX)==ALLTRIM(this.w_FAX))
            this.w_oERRORMESS.AddMsgLogPartNoTrans(space(4), "Tabella Clienti negozio Cod.: %1 Fax: %2 Fax normalizzato: %3", CLCODCLI, this.w_OLDFAX, this.w_FAX)     
          endif
          if NOT(ALLTRIM(this.w_OLDCEL)==ALLTRIM(this.w_CEL))
            this.w_oERRORMESS.AddMsgLogPartNoTrans(space(4), "Tabella Clienti negozio Cod.: %1 Cel.: %2 Cel. normalizzato: %3", CLCODCLI, this.w_OLDCEL, this.w_CEL)     
          endif
          INSERT INTO Temp (TABE, DESKEY1, KEY1, CAMPOTEL, TEL, CAMPOFAX, FAX,CAMPOTEL2, TEL2) VALUES (this.w_TABELLA, this.w_DESKEY1, this.w_KEY1, "CLTELEFO", this.w_TEL, "CLTELFAX", this.w_FAX,"CLNUMCEL", this.w_CEL)
        endif
          select _Curs__d__d__PCON_EXE_QUERY_TELCLIENTI_d_VQR
          continue
        enddo
        use
      endif
    endif
    if NOT ISAHE()
      * --- Conti banche
      * --- Select from QUERY\TELCONTIBAN.VQR
      do vq_exec with 'QUERY\TELCONTIBAN.VQR',this,'_Curs_QUERY_TELCONTIBAN_d_VQR','',.f.,.t.
      if used('_Curs_QUERY_TELCONTIBAN_d_VQR')
        select _Curs_QUERY_TELCONTIBAN_d_VQR
        locate for 1=1
        do while not(eof())
        this.w_TABELLA = "COC_MAST"
        this.w_DESKEY1 = "BACODBAN"
        this.w_KEY1 = BACODBAN
        this.w_OLDTEL = BANUMTEL
        this.w_TEL = ConvNum(BANUMTEL, "+", 1)
        this.w_OLDFAX = BANUMFAX
        this.w_FAX = ConvNum(BANUMFAX, "+", 1)
        if NOT(ALLTRIM(this.w_OLDTEL)==ALLTRIM(this.w_TEL)) OR NOT(ALLTRIM(this.w_OLDFAX)==ALLTRIM(this.w_FAX))
          this.w_ERROR = this.w_ERROR+1
          if NOT(ALLTRIM(this.w_OLDTEL)==ALLTRIM(this.w_TEL)) 
            this.w_oERRORMESS.AddMsgLogPartNoTrans(space(4), "Tabella Conti banche Cod.: %1 Telef.: %2 Telef. normalizzato: %3", BACODBAN, this.w_OLDTEL, this.w_TEL)     
          endif
          if NOT(ALLTRIM(this.w_OLDFAX)==ALLTRIM(this.w_FAX))
            this.w_oERRORMESS.AddMsgLogPartNoTrans(space(4), "Tabella Conti banche Cod.: %1 Fax: %2 Fax normalizzato: %3", BACODBAN, this.w_OLDFAX, this.w_FAX)     
          endif
          INSERT INTO Temp (TABE, DESKEY1, KEY1, CAMPOTEL, TEL, CAMPOFAX, FAX) VALUES (this.w_TABELLA, this.w_DESKEY1, this.w_KEY1, "BANUMTEL", this.w_TEL, "BANUMFAX", this.w_FAX)
        endif
          select _Curs_QUERY_TELCONTIBAN_d_VQR
          continue
        enddo
        use
      endif
      * --- Dati azienda
      * --- Select from AZIENDA
      i_nConn=i_TableProp[this.AZIENDA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select AZCODAZI,AZTELEFO,AZTELFAX,AZAITELE,AZTELSOB  from "+i_cTable+" AZIENDA ";
            +" where AZCODAZI=w_COD_AZI";
             ,"_Curs_AZIENDA")
      else
        select AZCODAZI,AZTELEFO,AZTELFAX,AZAITELE,AZTELSOB from (i_cTable);
         where AZCODAZI=w_COD_AZI;
          into cursor _Curs_AZIENDA
      endif
      if used('_Curs_AZIENDA')
        select _Curs_AZIENDA
        locate for 1=1
        do while not(eof())
        this.w_TABELLA = "AZIENDA"
        this.w_DESKEY1 = "AZCODAZI"
        this.w_KEY1 = AZCODAZI
        this.w_OLDTEL = AZTELEFO
        this.w_TEL = ConvNum(AZTELEFO, "+", 1)
        this.w_OLDTEL2 = AZAITELE
        this.w_TEL2 = ConvNum(AZAITELE, "+", 1)
        this.w_OLDTEL3 = AZTELSOB
        this.w_TEL3 = ConvNum(AZTELSOB, "+", 1)
        this.w_OLDFAX = AZTELFAX
        this.w_FAX = ConvNum(AZTELFAX , "+", 1)
        if NOT(ALLTRIM(this.w_OLDTEL)==ALLTRIM(this.w_TEL)) OR NOT(ALLTRIM(this.w_OLDFAX)==ALLTRIM(this.w_FAX)) OR NOT(ALLTRIM(this.w_OLDTEL2)==ALLTRIM(this.w_TEL2)) OR NOT(ALLTRIM(this.w_OLDTEL3)==ALLTRIM(this.w_TEL3))
          this.w_ERROR = this.w_ERROR+1
          if NOT(ALLTRIM(this.w_OLDTEL)==ALLTRIM(this.w_TEL))
            this.w_oERRORMESS.AddMsgLogPartNoTrans(space(4), "Tabella Dati azienda Telef.: %1 Telef. normalizzato: %2", this.w_OLDTEL, this.w_TEL)     
          endif
          if NOT(ALLTRIM(this.w_OLDTEL2)==ALLTRIM(this.w_TEL2))
            this.w_oERRORMESS.AddMsgLogPartNoTrans(space(4), "Tabella Dati azienda Telef.sogg.deleg: %1 Telef.sogg.deleg normalizzato: %2", this.w_OLDTEL2, this.w_TEL2)     
          endif
          if NOT(ALLTRIM(this.w_OLDTEL3)==ALLTRIM(this.w_TEL3))
            this.w_oERRORMESS.AddMsgLogPartNoTrans(space(4), "Tabella Dati azienda Telef.sogg.obblig: %1 Telef.sogg.obblig normalizzato: %2", this.w_OLDTEL3, this.w_TEL3)     
          endif
          if NOT(ALLTRIM(this.w_OLDFAX)==ALLTRIM(this.w_FAX))
            this.w_oERRORMESS.AddMsgLogPartNoTrans(space(4), "Tabella Dati azienda Fax: %1 Fax normalizzato: %1", this.w_OLDFAX, this.w_FAX)     
          endif
          INSERT INTO Temp (TABE, DESKEY1, KEY1, CAMPOTEL, TEL, CAMPOFAX, FAX) VALUES (this.w_TABELLA, this.w_DESKEY1, this.w_KEY1, "AZTELEFO", this.w_TEL, "AZTELFAX", this.w_FAX)
          INSERT INTO Temp (TABE, DESKEY1, KEY1, CAMPOTEL, TEL, CAMPOFAX, FAX) VALUES (this.w_TABELLA, this.w_DESKEY1, this.w_KEY1, "AZAITELE", this.w_TEL2, "AZTELSOB", this.w_TEL3)
        endif
          select _Curs_AZIENDA
          continue
        enddo
        use
      endif
    endif
    if ISALT()
      * --- Uffici competenti relativi al comune
      * --- Select from QUERY\TELUFFCOMU.VQR
      do vq_exec with 'QUERY\TELUFFCOMU.VQR',this,'_Curs_QUERY_TELUFFCOMU_d_VQR','',.f.,.t.
      if used('_Curs_QUERY_TELUFFCOMU_d_VQR')
        select _Curs_QUERY_TELUFFCOMU_d_VQR
        locate for 1=1
        do while not(eof())
        this.w_TABELLA = "UFF_COMP"
        this.w_DESKEY1 = "UPSERIAL"
        this.w_KEY1 = UPSERIAL
        this.w_DESKEY2 = "CPROWNUM"
        this.w_KEY2 = CPROWNUM
        this.w_OLDTEL = UPTELUFF
        this.w_TEL = ConvNum(UPTELUFF, "+", 1)
        this.w_OLDFAX = UPFAXUFF
        this.w_FAX = ConvNum(UPFAXUFF, "+", 1)
        if NOT(ALLTRIM(this.w_OLDTEL)==ALLTRIM(this.w_TEL)) OR NOT(ALLTRIM(this.w_OLDFAX)==ALLTRIM(this.w_FAX))
          this.w_ERROR = this.w_ERROR+1
          if NOT(ALLTRIM(this.w_OLDTEL)==ALLTRIM(this.w_TEL)) 
            this.w_oERRORMESS.AddMsgLogPartNoTrans(space(4), "Tabella Uffici competenti comune  Comune: %1 Telef.: %2 Telef. normalizzato: %3", alltrim(UPDESCOM), this.w_OLDTEL, this.w_TEL)     
          endif
          if NOT(ALLTRIM(this.w_OLDFAX)==ALLTRIM(this.w_FAX))
            this.w_oERRORMESS.AddMsgLogPartNoTrans(space(4), "Tabella Uffici competenti comune  Comune: %1 Fax: %2 Fax normalizzato: %3", alltrim(UPDESCOM), this.w_OLDFAX, this.w_FAX)     
          endif
          INSERT INTO Temp (TABE, DESKEY1, KEY1, DESKEY2, KEY2, CAMPOTEL, TEL, CAMPOFAX, FAX) VALUES (this.w_TABELLA, this.w_DESKEY1, this.w_KEY1, this.w_DESKEY2, ALLTRIM(STR(this.w_KEY2)), "UPTELUFF", this.w_TEL, "UPFAXUFF", this.w_FAX)
        endif
          select _Curs_QUERY_TELUFFCOMU_d_VQR
          continue
        enddo
        use
      endif
    endif
    if ISAHE()
      * --- Dati azienda
      * --- Select from AZIENDA
      i_nConn=i_TableProp[this.AZIENDA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select AZCODAZI,AZTELEFO,AZTELFAX,AZAITELE  from "+i_cTable+" AZIENDA ";
            +" where AZCODAZI=w_COD_AZI";
             ,"_Curs_AZIENDA")
      else
        select AZCODAZI,AZTELEFO,AZTELFAX,AZAITELE from (i_cTable);
         where AZCODAZI=w_COD_AZI;
          into cursor _Curs_AZIENDA
      endif
      if used('_Curs_AZIENDA')
        select _Curs_AZIENDA
        locate for 1=1
        do while not(eof())
        this.w_TABELLA = "AZIENDA"
        this.w_DESKEY1 = "AZCODAZI"
        this.w_KEY1 = AZCODAZI
        this.w_OLDTEL = AZTELEFO
        this.w_TEL = ConvNum(AZTELEFO, "+", 1)
        this.w_OLDTEL2 = AZAITELE
        this.w_TEL2 = ConvNum(AZAITELE, "+", 1)
        this.w_OLDFAX = AZTELFAX
        this.w_FAX = ConvNum(AZTELFAX , "+", 1)
        if NOT(ALLTRIM(this.w_OLDTEL)==ALLTRIM(this.w_TEL)) OR NOT(ALLTRIM(this.w_OLDFAX)==ALLTRIM(this.w_FAX)) OR NOT(ALLTRIM(this.w_OLDTEL2)==ALLTRIM(this.w_TEL2))
          this.w_ERROR = this.w_ERROR+1
          if NOT(ALLTRIM(this.w_OLDTEL)==ALLTRIM(this.w_TEL))
            this.w_oERRORMESS.AddMsgLogPartNoTrans(space(4), "Tabella Dati azienda Telef.: %1 Telef. normalizzato: %2", this.w_OLDTEL, this.w_TEL)     
          endif
          if NOT(ALLTRIM(this.w_OLDTEL2)==ALLTRIM(this.w_TEL2))
            this.w_oERRORMESS.AddMsgLogPartNoTrans(space(4), "Tabella Dati azienda Telef.sogg.deleg: %1 Telef.sogg.deleg normalizzato: %2", this.w_OLDTEL2, this.w_TEL2)     
          endif
          if NOT(ALLTRIM(this.w_OLDFAX)==ALLTRIM(this.w_FAX))
            this.w_oERRORMESS.AddMsgLogPartNoTrans(space(4), "Tabella Dati azienda Fax: %1 Fax normalizzato: %1", this.w_OLDFAX, this.w_FAX)     
          endif
          INSERT INTO Temp (TABE, DESKEY1, KEY1, CAMPOTEL, TEL, CAMPOFAX, FAX) VALUES (this.w_TABELLA, this.w_DESKEY1, this.w_KEY1, "AZTELEFO", this.w_TEL, "AZTELFAX", this.w_FAX)
          INSERT INTO Temp (TABE, DESKEY1, KEY1, CAMPOTEL, TEL) VALUES (this.w_TABELLA, this.w_DESKEY1, this.w_KEY1, "AZAITELE", this.w_TEL2)
        endif
          select _Curs_AZIENDA
          continue
        enddo
        use
      endif
      * --- Riferimenti banca
      * --- Select from QUERY\TELRIFBAN.VQR
      do vq_exec with 'QUERY\TELRIFBAN.VQR',this,'_Curs_QUERY_TELRIFBAN_d_VQR','',.f.,.t.
      if used('_Curs_QUERY_TELRIFBAN_d_VQR')
        select _Curs_QUERY_TELRIFBAN_d_VQR
        locate for 1=1
        do while not(eof())
        this.w_TABELLA = "RIFEBANC"
        this.w_DESKEY1 = "DDCODICE"
        this.w_KEY1 = DDCODICE
        this.w_DESKEY2 = "DDCODRIF"
        this.w_KEY2 = DDCODRIF
        this.w_OLDTEL = DDTELEFO
        this.w_TEL = ConvNum(DDTELEFO, "+", 1)
        if NOT(ALLTRIM(this.w_OLDTEL)==ALLTRIM(this.w_TEL))
          this.w_ERROR = this.w_ERROR+1
          this.w_oERRORMESS.AddMsgLogPartNoTrans(space(4), "Tabella Riferimenti banca Cod.: %1 Telef.: %2 Telef. normalizzato: %3", DDCODICE, this.w_OLDTEL, this.w_TEL)     
          INSERT INTO Temp (TABE, DESKEY1, KEY1, DESKEY2, KEY2, CAMPOTEL, TEL) VALUES (this.w_TABELLA, this.w_DESKEY1, this.w_KEY1, this.w_DESKEY2, this.w_KEY2, "DDTELEFO", this.w_TEL)
        endif
          select _Curs_QUERY_TELRIFBAN_d_VQR
          continue
        enddo
        use
      endif
      * --- Vettori
      * --- Select from QUERY\TELVETTORI.VQR
      do vq_exec with 'QUERY\TELVETTORI.VQR',this,'_Curs_QUERY_TELVETTORI_d_VQR','',.f.,.t.
      if used('_Curs_QUERY_TELVETTORI_d_VQR')
        select _Curs_QUERY_TELVETTORI_d_VQR
        locate for 1=1
        do while not(eof())
        this.w_TABELLA = "VETTORI"
        this.w_DESKEY1 = "VTCODVET"
        this.w_KEY1 = VTCODVET
        this.w_OLDTEL = VTTELEFO
        this.w_TEL = ConvNum(VTTELEFO, "+", 1)
        this.w_OLDFAX = VTTELFAX
        this.w_FAX = ConvNum(VTTELFAX, "+", 1)
        this.w_OLDCEL = VTNUMCEL
        this.w_CEL = ConvNum(VTNUMCEL, "+", 1)
        if NOT(ALLTRIM(this.w_OLDTEL)==ALLTRIM(this.w_TEL)) OR NOT(ALLTRIM(this.w_OLDFAX)==ALLTRIM(this.w_FAX)) OR NOT(ALLTRIM(this.w_OLDCEL)==ALLTRIM(this.w_CEL))
          this.w_ERROR = this.w_ERROR+1
          if NOT(ALLTRIM(this.w_OLDTEL)==ALLTRIM(this.w_TEL))
            this.w_oERRORMESS.AddMsgLogPartNoTrans(space(4), "Tabella Vettori Cod.: %1 Telef.: %2 Telef. normalizzato: %3", VTCODVET, this.w_OLDTEL, this.w_TEL)     
          endif
          if NOT(ALLTRIM(this.w_OLDFAX)==ALLTRIM(this.w_FAX))
            this.w_oERRORMESS.AddMsgLogPartNoTrans(space(4), "Tabella Conti banche Cod.: %1 Fax: %2 Fax normalizzato: %3", VTCODVET, this.w_OLDFAX, this.w_FAX)     
          endif
          if NOT(ALLTRIM(this.w_OLDCEL)==ALLTRIM(this.w_CEL))
            this.w_oERRORMESS.AddMsgLogPartNoTrans(space(4), "Tabella Conti banche Cod.: %1 Cel: %2 Cel. normalizzato: %3", VTNUMCEL, this.w_OLDCEL, this.w_CEL)     
          endif
          INSERT INTO Temp (TABE, DESKEY1, KEY1, CAMPOTEL, TEL, CAMPOFAX, FAX,CAMPOTEL2, TEL2) VALUES (this.w_TABELLA, this.w_DESKEY1, this.w_KEY1, "VTTELEFO", this.w_TEL, "VTTELFAX", this.w_FAX, "VTNUMCEL", this.w_CEL)
        endif
          select _Curs_QUERY_TELVETTORI_d_VQR
          continue
        enddo
        use
      endif
      * --- Anagrafe
      * --- Select from QUERY\TELANAGRAF.VQR
      do vq_exec with 'QUERY\TELANAGRAF.VQR',this,'_Curs_QUERY_TELANAGRAF_d_VQR','',.f.,.t.
      if used('_Curs_QUERY_TELANAGRAF_d_VQR')
        select _Curs_QUERY_TELANAGRAF_d_VQR
        locate for 1=1
        do while not(eof())
        this.w_TABELLA = "ANAGRAFE"
        this.w_DESKEY1 = "ANCODICE"
        this.w_KEY1 = ANCODICE
        this.w_OLDTEL = ANTELEFO
        this.w_TEL = ConvNum(ANTELEFO, "+", 1)
        this.w_OLDFAX = ANTELFAX
        this.w_FAX = ConvNum(ANTELFAX, "+", 1)
        this.w_OLDCEL = ANNUMCEL
        this.w_CEL = ConvNum(ANNUMCEL, "+", 1)
        if NOT(ALLTRIM(this.w_OLDTEL)==ALLTRIM(this.w_TEL)) OR NOT(ALLTRIM(this.w_OLDFAX)==ALLTRIM(this.w_FAX)) OR NOT(ALLTRIM(this.w_OLDCEL)==ALLTRIM(this.w_CEL))
          this.w_ERROR = this.w_ERROR+1
          if NOT(ALLTRIM(this.w_OLDTEL)==ALLTRIM(this.w_TEL))
            this.w_oERRORMESS.AddMsgLogPartNoTrans(space(4), "Tabella Anagrafe Cod.: %1 Telef.: %2 Telef. normalizzato: %3", ANCODICE, this.w_OLDTEL, this.w_TEL)     
          endif
          if NOT(ALLTRIM(this.w_OLDFAX)==ALLTRIM(this.w_FAX))
            this.w_oERRORMESS.AddMsgLogPartNoTrans(space(4), "Tabella Anagrafe Cod.: %1 Fax: %2 Fax normalizzato: %3", ANCODICE, this.w_OLDFAX, this.w_FAX)     
          endif
          if NOT(ALLTRIM(this.w_OLDCEL)==ALLTRIM(this.w_CEL))
            this.w_oERRORMESS.AddMsgLogPartNoTrans(space(4), "Tabella Anagrafe Cod.: %1 Cel.: %2  Cel.normalizzato: %3", ANCODICE, this.w_OLDCEL, this.w_CEL)     
          endif
          INSERT INTO Temp (TABE, DESKEY1, KEY1, CAMPOTEL, TEL, CAMPOFAX, FAX,CAMPOTEL2, TEL2) VALUES (this.w_TABELLA, this.w_DESKEY1, this.w_KEY1, "ANTELEFO", this.w_TEL, "ANTELFAX", this.w_FAX,"ANNUMCEL", this.w_CEL)
        endif
          select _Curs_QUERY_TELANAGRAF_d_VQR
          continue
        enddo
        use
      endif
    endif
    * --- Sedi azienda
    * --- Select from SEDIAZIE
    i_nConn=i_TableProp[this.SEDIAZIE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SEDIAZIE_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select SECODAZI,SECODDES,SETELEFO  from "+i_cTable+" SEDIAZIE ";
          +" where SECODAZI=w_COD_AZI";
           ,"_Curs_SEDIAZIE")
    else
      select SECODAZI,SECODDES,SETELEFO from (i_cTable);
       where SECODAZI=w_COD_AZI;
        into cursor _Curs_SEDIAZIE
    endif
    if used('_Curs_SEDIAZIE')
      select _Curs_SEDIAZIE
      locate for 1=1
      do while not(eof())
      this.w_TABELLA = "SEDIAZIE"
      this.w_DESKEY1 = "SECODAZI"
      this.w_KEY1 = SECODAZI
      this.w_DESKEY2 = "SECODDES"
      this.w_KEY2 = SECODDES
      this.w_OLDTEL = SETELEFO
      this.w_TEL = ConvNum(SETELEFO, "+", 1)
      if NOT(ALLTRIM(this.w_OLDTEL)==ALLTRIM(this.w_TEL))
        this.w_ERROR = this.w_ERROR+1
        this.w_oERRORMESS.AddMsgLogPartNoTrans(space(4), "Tabella Sedi azienda Cod.: %1 Telef.: %2 Telef. normalizzato: %3", SECODDES, this.w_OLDTEL, this.w_TEL)     
        INSERT INTO Temp (TABE, DESKEY1, KEY1, DESKEY2, KEY2, CAMPOTEL, TEL) VALUES (this.w_TABELLA, this.w_DESKEY1, this.w_KEY1, this.w_DESKEY2, this.w_KEY2, "SETELEFO", this.w_TEL)
      endif
        select _Curs_SEDIAZIE
        continue
      enddo
      use
    endif
    * --- Contatti
    * --- Select from NOM_CONT
    i_nConn=i_TableProp[this.NOM_CONT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.NOM_CONT_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select NCCODICE,NCCODCON,NCTELEFO,NCTELFAX,NCTELEF2,NCNUMCEL  from "+i_cTable+" NOM_CONT ";
           ,"_Curs_NOM_CONT")
    else
      select NCCODICE,NCCODCON,NCTELEFO,NCTELFAX,NCTELEF2,NCNUMCEL from (i_cTable);
        into cursor _Curs_NOM_CONT
    endif
    if used('_Curs_NOM_CONT')
      select _Curs_NOM_CONT
      locate for 1=1
      do while not(eof())
      this.w_TABELLA = "NOM_CONT"
      this.w_DESKEY1 = "NCCODICE"
      this.w_KEY1 = NCCODICE
      this.w_DESKEY2 = "NCCODCON"
      this.w_KEY2 = NCCODCON
      this.w_OLDTEL = NCTELEFO
      this.w_TEL = ConvNum(NCTELEFO, "+", 1)
      this.w_OLDTEL2 = NCTELEF2
      this.w_TEL2 = ConvNum(NCTELEF2, "+", 1)
      this.w_OLDFAX = NCTELFAX 
      this.w_FAX = ConvNum(NCTELFAX , "+", 1)
      this.w_OLDCEL = NCNUMCEL
      this.w_CEL = ConvNum(NCNUMCEL , "+", 1)
      if NOT(ALLTRIM(this.w_OLDTEL)==ALLTRIM(this.w_TEL)) OR NOT(ALLTRIM(this.w_OLDTEL2)==ALLTRIM(this.w_TEL2))
        this.w_ERROR = this.w_ERROR+1
        if NOT(ALLTRIM(this.w_OLDTEL)==ALLTRIM(this.w_TEL))
          this.w_oERRORMESS.AddMsgLogPartNoTrans(space(4), "Tabella Contatti Nomin.: %1 Telef.: %2 Telef. normalizzato: %3", NCCODICE, this.w_OLDTEL, this.w_TEL)     
        endif
        if NOT(ALLTRIM(this.w_OLDTEL2)==ALLTRIM(this.w_TEL2))
          this.w_oERRORMESS.AddMsgLogPartNoTrans(space(4), "Tabella Contatti Nomin.: %1 Telef.2: %2 Telef.2 normalizzato: %3", NCCODICE, this.w_OLDTEL2, this.w_TEL2)     
        endif
        INSERT INTO Temp (TABE, DESKEY1, KEY1, DESKEY2, KEY2, CAMPOTEL, TEL, CAMPOTEL2, TEL2, CAMPOFAX, FAX) VALUES (this.w_TABELLA, this.w_DESKEY1, this.w_KEY1, this.w_DESKEY2, this.w_KEY2, "NCTELEFO", this.w_TEL, "NCTELEF2", this.w_TEL2, "NCTELFAX", this.w_FAX)
      endif
      if NOT(ALLTRIM(this.w_OLDCEL)==ALLTRIM(this.w_CEL)) OR NOT(ALLTRIM(this.w_OLDFAX)==ALLTRIM(this.w_FAX)) 
        this.w_ERROR = this.w_ERROR+1
        if NOT(ALLTRIM(this.w_OLDFAX)==ALLTRIM(this.w_FAX)) 
          this.w_oERRORMESS.AddMsgLogPartNoTrans(space(4), "Tabella Contatti Nomin.: %1 Fax: %2 Fax normalizzato: %3", NCCODICE, this.w_OLDFAX, this.w_FAX)     
        endif
        if NOT(ALLTRIM(this.w_OLDCEL)==ALLTRIM(this.w_CEL))
          this.w_oERRORMESS.AddMsgLogPartNoTrans(space(4), "Tabella Contatti Nomin.: %1 Cel.: %2 Cel normalizzato: %3", NCCODICE, this.w_OLDCEL, this.w_CEL)     
        endif
        INSERT INTO Temp (TABE, DESKEY1, KEY1, DESKEY2, KEY2,CAMPOTEL, TEL,CAMPOFAX,FAX) VALUES (this.w_TABELLA, this.w_DESKEY1, this.w_KEY1, this.w_DESKEY2, this.w_KEY2,"NCTELFAX", this.w_FAX,"NCNUMCEL", this.w_CEL)
      endif
        select _Curs_NOM_CONT
        continue
      enddo
      use
    endif
    * --- Nominativi
    * --- Select from OFF_NOMI
    i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select NOCODICE,NOTELEFO,NOTELFAX,NONUMCEL  from "+i_cTable+" OFF_NOMI ";
           ,"_Curs_OFF_NOMI")
    else
      select NOCODICE,NOTELEFO,NOTELFAX,NONUMCEL from (i_cTable);
        into cursor _Curs_OFF_NOMI
    endif
    if used('_Curs_OFF_NOMI')
      select _Curs_OFF_NOMI
      locate for 1=1
      do while not(eof())
      this.w_TABELLA = "OFF_NOMI"
      this.w_DESKEY1 = "NOCODICE"
      this.w_KEY1 = NOCODICE
      this.w_OLDTEL = NOTELEFO
      this.w_TEL = ConvNum(NOTELEFO, "+", 1)
      this.w_OLDFAX = NOTELFAX
      this.w_FAX = ConvNum(NOTELFAX, "+", 1)
      this.w_OLDCEL = NONUMCEL
      this.w_CEL = ConvNum(NONUMCEL, "+", 1)
      if NOT(ALLTRIM(this.w_OLDTEL)==ALLTRIM(this.w_TEL)) OR NOT(ALLTRIM(this.w_OLDFAX)==ALLTRIM(this.w_FAX)) OR NOT(ALLTRIM(this.w_OLDCEL)==ALLTRIM(this.w_CEL))
        this.w_ERROR = this.w_ERROR+1
        if NOT(ALLTRIM(this.w_OLDTEL)==ALLTRIM(this.w_TEL)) 
          this.w_oERRORMESS.AddMsgLogPartNoTrans(space(4), "Tabella Nominativi Cod.: %1 Telef.: %2 Telef. normalizzato: %3", NOCODICE, this.w_OLDTEL, this.w_TEL)     
        endif
        if NOT(ALLTRIM(this.w_OLDFAX)==ALLTRIM(this.w_FAX))
          this.w_oERRORMESS.AddMsgLogPartNoTrans(space(4), "Tabella Nominativi Cod.: %1 Fax: %2 Fax normalizzato: %3", NOCODICE, this.w_OLDFAX, this.w_FAX)     
        endif
        if NOT(ALLTRIM(this.w_OLDCEL)==ALLTRIM(this.w_CEL))
          this.w_oERRORMESS.AddMsgLogPartNoTrans(space(4), "Tabella Nominativi Cod.: %1 Cel.: %2 Cel. normalizzato: %3", NOCODICE, this.w_OLDCEL, this.w_CEL)     
        endif
        INSERT INTO Temp (TABE, DESKEY1, KEY1, CAMPOTEL, TEL, CAMPOFAX, FAX,CAMPOTEL2, TEL2) VALUES (this.w_TABELLA, this.w_DESKEY1, this.w_KEY1, "NOTELEFO", this.w_TEL, "NOTELFAX", this.w_FAX,"NONUMCEL", this.w_CEL)
      endif
        select _Curs_OFF_NOMI
        continue
      enddo
      use
    endif
    * --- Servizi FAX / Telefono / Mail / PostaLite / IZCP
    * --- Select from UTE_NTI
    i_nConn=i_TableProp[this.UTE_NTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.UTE_NTI_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select UTCODICE,UTTELEFO,UTTELEF2,UTNUMCEL  from "+i_cTable+" UTE_NTI ";
           ,"_Curs_UTE_NTI")
    else
      select UTCODICE,UTTELEFO,UTTELEF2,UTNUMCEL from (i_cTable);
        into cursor _Curs_UTE_NTI
    endif
    if used('_Curs_UTE_NTI')
      select _Curs_UTE_NTI
      locate for 1=1
      do while not(eof())
      this.w_TABELLA = "UTE_NTI"
      this.w_DESKEY1 = "UTCODICE"
      this.w_KEY1 = UTCODICE
      this.w_OLDTEL = UTTELEFO
      this.w_TEL = ConvNum(UTTELEFO, "+", 1)
      this.w_OLDFAX = UTTELEF2
      this.w_FAX = ConvNum(UTTELEF2, "+", 1)
      this.w_OLDCEL = UTNUMCEL
      this.w_CEL = ConvNum(UTNUMCEL, "+", 1)
      if NOT(ALLTRIM(this.w_OLDTEL)==ALLTRIM(this.w_TEL)) OR NOT(ALLTRIM(this.w_OLDFAX)==ALLTRIM(this.w_FAX)) OR NOT(ALLTRIM(this.w_OLDCEL)==ALLTRIM(this.w_CEL))
        this.w_ERROR = this.w_ERROR+1
        if NOT(ALLTRIM(this.w_OLDTEL)==ALLTRIM(this.w_TEL))
          this.w_oERRORMESS.AddMsgLogPartNoTrans(space(4), "Tabella Servizi fax Cod.: %1 Telef.: %2 Telef. normalizzato: %3", UTCODICE, this.w_OLDTEL, this.w_TEL)     
        endif
        if NOT(ALLTRIM(this.w_OLDFAX)==ALLTRIM(this.w_FAX))
          this.w_oERRORMESS.AddMsgLogPartNoTrans(space(4), "Tabella Servizi fax Cod.: %1 Telef2: %2 Telef2 normalizzato: %3", UTCODICE, this.w_OLDFAX, this.w_FAX)     
        endif
        if NOT(ALLTRIM(this.w_OLDCEL)==ALLTRIM(this.w_CEL))
          this.w_oERRORMESS.AddMsgLogPartNoTrans(space(4), "Tabella Servizi fax Cod.: %1 Cel.: %2 Cel. normalizzato: %3", UTCODICE, this.w_OLDCEL, this.w_CEL)     
        endif
        INSERT INTO Temp (TABE, DESKEY1, KEY1, CAMPOTEL, TEL, CAMPOFAX, FAX,CAMPOTEL2, TEL2) VALUES (this.w_TABELLA, this.w_DESKEY1, ALLTRIM(STR(this.w_KEY1)), "UTTELEFO", this.w_TEL, "UTTELEF2", this.w_FAX, "UTNUMCEL", this.w_CEL)
      endif
        select _Curs_UTE_NTI
        continue
      enddo
      use
    endif
    * --- Magazzini
    * --- Select from MAGAZZIN
    i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select MGCODMAG,MGTELEFO  from "+i_cTable+" MAGAZZIN ";
           ,"_Curs_MAGAZZIN")
    else
      select MGCODMAG,MGTELEFO from (i_cTable);
        into cursor _Curs_MAGAZZIN
    endif
    if used('_Curs_MAGAZZIN')
      select _Curs_MAGAZZIN
      locate for 1=1
      do while not(eof())
      this.w_TABELLA = "MAGAZZIN"
      this.w_DESKEY1 = "MGCODMAG"
      this.w_KEY1 = MGCODMAG
      this.w_OLDTEL = MGTELEFO
      this.w_TEL = ConvNum(MGTELEFO, "+", 1)
      if NOT(ALLTRIM(this.w_OLDTEL)==ALLTRIM(this.w_TEL))
        this.w_ERROR = this.w_ERROR+1
        this.w_oERRORMESS.AddMsgLogPartNoTrans(space(4), "Tabella Magazzini Cod.: %1 Telef.: %2 Telef. normalizzato: %3", MGCODMAG, this.w_OLDTEL, this.w_TEL)     
        INSERT INTO Temp (TABE, DESKEY1, KEY1, CAMPOTEL, TEL) VALUES (this.w_TABELLA, this.w_DESKEY1, this.w_KEY1, "MGTELEFO", this.w_TEL)
      endif
        select _Curs_MAGAZZIN
        continue
      enddo
      use
    endif
    * --- Agenti
    * --- Select from AGENTI
    i_nConn=i_TableProp[this.AGENTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AGENTI_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select AGCODAGE,AGTELEFO,AGTELFAX  from "+i_cTable+" AGENTI ";
           ,"_Curs_AGENTI")
    else
      select AGCODAGE,AGTELEFO,AGTELFAX from (i_cTable);
        into cursor _Curs_AGENTI
    endif
    if used('_Curs_AGENTI')
      select _Curs_AGENTI
      locate for 1=1
      do while not(eof())
      this.w_TABELLA = "AGENTI"
      this.w_DESKEY1 = "AGCODAGE"
      this.w_KEY1 = AGCODAGE
      this.w_OLDTEL = AGTELEFO
      this.w_TEL = ConvNum(AGTELEFO, "+", 1)
      this.w_OLDFAX = AGTELFAX
      this.w_FAX = ConvNum(AGTELFAX, "+", 1)
      if NOT(ALLTRIM(this.w_OLDTEL)==ALLTRIM(this.w_TEL)) OR NOT(ALLTRIM(this.w_OLDFAX)==ALLTRIM(this.w_FAX))
        this.w_ERROR = this.w_ERROR+1
        if NOT(ALLTRIM(this.w_OLDTEL)==ALLTRIM(this.w_TEL))
          this.w_oERRORMESS.AddMsgLogPartNoTrans(space(4), "Tabella Agenti Cod.: %1 Telef.: %2 Telef. normalizzato: %3", AGCODAGE, this.w_OLDTEL, this.w_TEL)     
        endif
        if NOT(ALLTRIM(this.w_OLDFAX)==ALLTRIM(this.w_FAX))
          this.w_oERRORMESS.AddMsgLogPartNoTrans(space(4), "Tabella Agenti Cod.: %1 Fax: %2 Fax normalizzato: %3", AGCODAGE, this.w_OLDFAX, this.w_FAX)     
        endif
        INSERT INTO Temp (TABE, DESKEY1, KEY1, CAMPOTEL, TEL, CAMPOFAX, FAX) VALUES (this.w_TABELLA, this.w_DESKEY1, this.w_KEY1, "AGTELEFO", this.w_TEL, "AGTELFAX", this.w_FAX)
      endif
        select _Curs_AGENTI
        continue
      enddo
      use
    endif
    * --- Titolari
    * --- Select from TITOLARI
    i_nConn=i_TableProp[this.TITOLARI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TITOLARI_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select TTCODAZI,CPROWNUM,TTTELEFO,TTCOGTIT,TTNOMTIT  from "+i_cTable+" TITOLARI ";
          +" where TTCODAZI=w_COD_AZI";
           ,"_Curs_TITOLARI")
    else
      select TTCODAZI,CPROWNUM,TTTELEFO,TTCOGTIT,TTNOMTIT from (i_cTable);
       where TTCODAZI=w_COD_AZI;
        into cursor _Curs_TITOLARI
    endif
    if used('_Curs_TITOLARI')
      select _Curs_TITOLARI
      locate for 1=1
      do while not(eof())
      this.w_TABELLA = "TITOLARI"
      this.w_DESKEY1 = "TTCODAZI"
      this.w_KEY1 = TTCODAZI
      this.w_DESKEY2 = "CPROWNUM"
      this.w_KEY2 = CPROWNUM
      this.w_OLDTEL = TTTELEFO
      this.w_TEL = ConvNum(TTTELEFO, "+", 1)
      if NOT(ALLTRIM(this.w_OLDTEL)==ALLTRIM(this.w_TEL))
        this.w_ERROR = this.w_ERROR+1
        this.w_oERRORMESS.AddMsgLogPartNoTrans(space(4), "Tabella Titolari   Titol.: %1 Telef.: %2 Telef. normalizzato: %3", alltrim(TTCOGTIT)+space(1)+alltrim(TTNOMTIT), this.w_OLDTEL, this.w_TEL)     
        INSERT INTO Temp (TABE, DESKEY1, KEY1, DESKEY2, KEY2, CAMPOTEL, TEL) VALUES (this.w_TABELLA, this.w_DESKEY1, this.w_KEY1, this.w_DESKEY2, ALLTRIM(STR(this.w_KEY2)), "TTTELEFO", this.w_TEL)
      endif
        select _Curs_TITOLARI
        continue
      enddo
      use
    endif
    * --- Clienti/Fornitori
    * --- Select from CONTI
    i_nConn=i_TableProp[this.CONTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select ANTIPCON,ANCODICE,ANTELEFO,ANTELFAX,ANNUMCEL  from "+i_cTable+" CONTI ";
          +" where ANTIPCON='C' OR ANTIPCON='F'";
           ,"_Curs_CONTI")
    else
      select ANTIPCON,ANCODICE,ANTELEFO,ANTELFAX,ANNUMCEL from (i_cTable);
       where ANTIPCON="C" OR ANTIPCON="F";
        into cursor _Curs_CONTI
    endif
    if used('_Curs_CONTI')
      select _Curs_CONTI
      locate for 1=1
      do while not(eof())
      this.w_TABELLA = "CONTI"
      this.w_DESKEY1 = "ANTIPCON"
      this.w_KEY1 = ANTIPCON
      this.w_DESKEY2 = "ANCODICE"
      this.w_KEY2 = ANCODICE
      this.w_OLDTEL = ANTELEFO
      this.w_TEL = ConvNum(ANTELEFO, "+", 1)
      this.w_OLDFAX = ANTELFAX
      this.w_FAX = ConvNum(ANTELFAX, "+", 1)
      this.w_OLDCEL = ANNUMCEL
      this.w_CEL = ConvNum(ANNUMCEL, "+", 1)
      if NOT(ALLTRIM(this.w_OLDTEL)==ALLTRIM(this.w_TEL)) OR NOT(ALLTRIM(this.w_OLDFAX)==ALLTRIM(this.w_FAX)) OR NOT(ALLTRIM(this.w_OLDCEL)==ALLTRIM(this.w_CEL))
        this.w_ERROR = this.w_ERROR+1
        if NOT(ALLTRIM(this.w_OLDTEL)==ALLTRIM(this.w_TEL)) 
          this.w_oERRORMESS.AddMsgLogPartNoTrans(space(4), "Tabella %1  %2: %3 Telef.: %4 Telef. normalizzato: %5", IIF(ANTIPCON="C", "Clienti","Fornitori"), IIF(ANTIPCON="C", "Cli.","For."), ANCODICE, this.w_OLDTEL, this.w_TEL)     
        endif
        if NOT(ALLTRIM(this.w_OLDFAX)==ALLTRIM(this.w_FAX))
          this.w_oERRORMESS.AddMsgLogPartNoTrans(space(4), "Tabella %1  %2: %3 Fax: %4 Fax normalizzato: %5", IIF(ANTIPCON="C", "Clienti","Fornitori"), IIF(ANTIPCON="C", "Cli.","For."), ANCODICE, this.w_OLDFAX, this.w_FAX)     
        endif
        if NOT(ALLTRIM(this.w_OLDCEL)==ALLTRIM(this.w_CEL))
          this.w_oERRORMESS.AddMsgLogPartNoTrans(space(4), "Tabella %1  %2: %3 Cel.: %4 Cel. normalizzato: %5", IIF(ANTIPCON="C", "Clienti","Fornitori"), IIF(ANTIPCON="C", "Cli.","For."), ANCODICE, this.w_OLDCEL, this.w_CEL)     
        endif
        INSERT INTO Temp (TABE, DESKEY1, KEY1, DESKEY2, KEY2, CAMPOTEL, TEL, CAMPOFAX, FAX,CAMPOTEL2,TEL2) VALUES (this.w_TABELLA, this.w_DESKEY1, this.w_KEY1, this.w_DESKEY2, this.w_KEY2, "ANTELEFO", this.w_TEL, "ANTELFAX", this.w_FAX,"ANNUMCEL", this.w_CEL)
      endif
        select _Curs_CONTI
        continue
      enddo
      use
    endif
    * --- Destinazioni diverse
    * --- Select from DES_DIVE
    i_nConn=i_TableProp[this.DES_DIVE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DES_DIVE_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" DES_DIVE ";
           ,"_Curs_DES_DIVE")
    else
      select * from (i_cTable);
        into cursor _Curs_DES_DIVE
    endif
    if used('_Curs_DES_DIVE')
      select _Curs_DES_DIVE
      locate for 1=1
      do while not(eof())
      this.w_TABELLA = "DES_DIVE"
      this.w_DESKEY1 = "DDTIPCON"
      this.w_KEY1 = DDTIPCON
      this.w_DESKEY2 = "DDCODICE"
      this.w_KEY2 = DDCODICE
      this.w_DESKEY3 = "DDCODDES"
      this.w_KEY3 = DDCODDES
      this.w_OLDTEL = DDTELEFO
      this.w_TEL = ConvNum(DDTELEFO, "+", 1)
      this.w_OLDFAX = IIF(NOT ISAHE(), DDNUMFAX, DDTELFAX)
      this.w_FAX = ConvNum(IIF(NOT ISAHE(), DDNUMFAX, DDTELFAX), "+", 1)
      if NOT(ALLTRIM(this.w_OLDTEL)==ALLTRIM(this.w_TEL)) OR NOT(ALLTRIM(this.w_OLDFAX)==ALLTRIM(this.w_FAX))
        this.w_ERROR = this.w_ERROR+1
        if NOT(ALLTRIM(this.w_OLDTEL)==ALLTRIM(this.w_TEL)) 
          this.w_oERRORMESS.AddMsgLogPartNoTrans(space(4), "Tabella Destinaz. div.  %1: %2 Cod.: %3 Telef.: %4 Telef. normalizzato: %5", IIF(DDTIPCON="C", "Cli.","For."), DDCODICE, alltrim(DDCODDES), this.w_OLDTEL, this.w_TEL)     
        endif
        if NOT(ALLTRIM(this.w_OLDFAX)==ALLTRIM(this.w_FAX))
          this.w_oERRORMESS.AddMsgLogPartNoTrans(space(4), "Tabella Destinaz. div.   %1: %2 Cod.: %3 Fax: %4 Fax normalizzato: %5", IIF(DDTIPCON="C", "Cli.","For."), DDCODICE, alltrim(DDCODDES), this.w_OLDFAX, this.w_FAX)     
        endif
        INSERT INTO Temp (TABE, DESKEY1, KEY1, DESKEY2, KEY2, DESKEY3, KEY3, CAMPOTEL, TEL, CAMPOFAX, FAX) VALUES (this.w_TABELLA, this.w_DESKEY1, this.w_KEY1, this.w_DESKEY2, this.w_KEY2, this.w_DESKEY3, this.w_KEY3, "DDTELEFO", this.w_TEL, IIF(NOT ISAHE(), "DDNUMFAX", "DDTELFAX"), this.w_FAX)
      endif
        select _Curs_DES_DIVE
        continue
      enddo
      use
    endif
    * --- Persone
    * --- Select from DIPENDEN
    i_nConn=i_TableProp[this.DIPENDEN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DIPENDEN_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select DPCODICE,DPTELEF1,DPTELEF2,DPNUMCEL  from "+i_cTable+" DIPENDEN ";
          +" where DPTIPRIS='P'";
           ,"_Curs_DIPENDEN")
    else
      select DPCODICE,DPTELEF1,DPTELEF2,DPNUMCEL from (i_cTable);
       where DPTIPRIS="P";
        into cursor _Curs_DIPENDEN
    endif
    if used('_Curs_DIPENDEN')
      select _Curs_DIPENDEN
      locate for 1=1
      do while not(eof())
      this.w_TABELLA = "DIPENDEN"
      this.w_DESKEY1 = "DPCODICE"
      this.w_KEY1 = DPCODICE
      this.w_OLDTEL = DPTELEF1
      this.w_TEL = ConvNum(DPTELEF1, "+", 1)
      this.w_OLDFAX = DPTELEF2
      this.w_FAX = ConvNum(DPTELEF2, "+", 1)
      this.w_OLDCEL = DPNUMCEL
      this.w_CEL = ConvNum(DPNUMCEL, "+", 1)
      if NOT(ALLTRIM(this.w_OLDTEL)==ALLTRIM(this.w_TEL)) OR NOT(ALLTRIM(this.w_OLDFAX)==ALLTRIM(this.w_FAX)) OR NOT(ALLTRIM(this.w_OLDCEL)==ALLTRIM(this.w_CEL))
        this.w_ERROR = this.w_ERROR+1
        if NOT(ALLTRIM(this.w_OLDTEL)==ALLTRIM(this.w_TEL)) 
          this.w_oERRORMESS.AddMsgLogPartNoTrans(space(4), "Tabella Persone Cod.: %1 Telef.1: %2 Telef.1 normalizzato: %3", DPCODICE, this.w_OLDTEL, this.w_TEL)     
        endif
        if NOT(ALLTRIM(this.w_OLDFAX)==ALLTRIM(this.w_FAX))
          this.w_oERRORMESS.AddMsgLogPartNoTrans(space(4), "Tabella Persone Cod.: %1 Telef.2: %2 Telef.2 normalizzato: %3", DPCODICE, this.w_OLDFAX, this.w_FAX)     
        endif
        if NOT(ALLTRIM(this.w_OLDCEL)==ALLTRIM(this.w_CEL))
          this.w_oERRORMESS.AddMsgLogPartNoTrans(space(4), "Tabella Persone Cod.: %1 Cel.: %2 Cel. normalizzato: %3", DPCODICE, this.w_OLDCEL, this.w_CEL)     
        endif
        INSERT INTO Temp (TABE, DESKEY1, KEY1, CAMPOTEL, TEL, CAMPOFAX, FAX) VALUES (this.w_TABELLA, this.w_DESKEY1, this.w_KEY1, "DPTELEF1", this.w_TEL, "DPTELEF2", this.w_FAX,"DPNUMCEL", this.w_CEL)
      endif
        select _Curs_DIPENDEN
        continue
      enddo
      use
    endif
    if NOT ISALT()
      * --- Commesse
      * --- Select from CAN_TIER
      i_nConn=i_TableProp[this.CAN_TIER_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select CNCODCAN,CNTELEFO  from "+i_cTable+" CAN_TIER ";
             ,"_Curs_CAN_TIER")
      else
        select CNCODCAN,CNTELEFO from (i_cTable);
          into cursor _Curs_CAN_TIER
      endif
      if used('_Curs_CAN_TIER')
        select _Curs_CAN_TIER
        locate for 1=1
        do while not(eof())
        this.w_TABELLA = "CAN_TIER"
        this.w_DESKEY1 = "CNCODCAN"
        this.w_KEY1 = CNCODCAN
        this.w_OLDTEL = CNTELEFO
        this.w_TEL = ConvNum(CNTELEFO, "+", 1)
        if NOT(ALLTRIM(this.w_OLDTEL)==ALLTRIM(this.w_TEL))
          this.w_ERROR = this.w_ERROR+1
          this.w_oERRORMESS.AddMsgLogPartNoTrans(space(4), "Tabella Commesse Cod.: %1 Telef.: %2 Telef. normalizzato: %3", CNCODCAN, this.w_OLDTEL, this.w_TEL)     
          INSERT INTO Temp (TABE, DESKEY1, KEY1, CAMPOTEL, TEL) VALUES (this.w_TABELLA, this.w_DESKEY1, this.w_KEY1, "CNTELEFO", this.w_TEL)
        endif
          select _Curs_CAN_TIER
          continue
        enddo
        use
      endif
    endif
    * --- Conti correnti
    * --- Select from BAN_CONTI
    i_nConn=i_TableProp[this.BAN_CONTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.BAN_CONTI_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR,CCNUMTEL  from "+i_cTable+" BAN_CONTI ";
           ,"_Curs_BAN_CONTI")
    else
      select CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR,CCNUMTEL from (i_cTable);
        into cursor _Curs_BAN_CONTI
    endif
    if used('_Curs_BAN_CONTI')
      select _Curs_BAN_CONTI
      locate for 1=1
      do while not(eof())
      this.w_TABELLA = "BAN_CONTI"
      this.w_DESKEY1 = "CCTIPCON"
      this.w_KEY1 = CCTIPCON
      this.w_DESKEY2 = "CCCODCON"
      this.w_KEY2 = CCCODCON
      this.w_DESKEY3 = "CCCODBAN"
      this.w_KEY3 = CCCODBAN
      this.w_DESKEY4 = "CCCONCOR"
      this.w_KEY4 = CCCONCOR
      this.w_OLDTEL = CCNUMTEL
      this.w_TEL = ConvNum(CCNUMTEL, "+", 1)
      if NOT(ALLTRIM(this.w_OLDTEL)==ALLTRIM(this.w_TEL)) 
        this.w_ERROR = this.w_ERROR+1
        if NOT(ALLTRIM(this.w_OLDTEL)==ALLTRIM(this.w_TEL)) 
          this.w_oERRORMESS.AddMsgLogPartNoTrans(space(4), "Tabella Conti correnti  %1: %2 Banca: %3 Telef.: %4 Telef. normalizzato: %5", IIF(CCTIPCON="C", "Cli.","For."), CCCODCON, alltrim(CCCODBAN), this.w_OLDTEL, this.w_TEL)     
        endif
        INSERT INTO Temp (TABE, DESKEY1, KEY1, DESKEY2, KEY2, DESKEY3, KEY3, DESKEY4, KEY4, CAMPOTEL, TEL) VALUES (this.w_TABELLA, this.w_DESKEY1, this.w_KEY1, this.w_DESKEY2, this.w_KEY2, this.w_DESKEY3, this.w_KEY3, this.w_DESKEY4, this.w_KEY4, "CCNUMTEL", this.w_TEL)
      endif
        select _Curs_BAN_CONTI
        continue
      enddo
      use
    endif
    * --- Sedi storicizzate
    * --- Select from SED_STOR
    i_nConn=i_TableProp[this.SED_STOR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SED_STOR_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select SSTIPCON,SSCODICE,SSDATINI,SSDATFIN,SSTELEFO,SSTELFAX,SSNUMCEL  from "+i_cTable+" SED_STOR ";
           ,"_Curs_SED_STOR")
    else
      select SSTIPCON,SSCODICE,SSDATINI,SSDATFIN,SSTELEFO,SSTELFAX,SSNUMCEL from (i_cTable);
        into cursor _Curs_SED_STOR
    endif
    if used('_Curs_SED_STOR')
      select _Curs_SED_STOR
      locate for 1=1
      do while not(eof())
      this.w_TABELLA = "SED_STOR"
      this.w_DESKEY1 = "SSTIPCON"
      this.w_KEY1 = SSTIPCON
      this.w_DESKEY2 = "SSCODICE"
      this.w_KEY2 = SSCODICE
      this.w_DESKEY3 = "SSDATINI"
      this.w_KEY3 = SSDATINI
      this.w_DESKEY4 = "SSDATFIN"
      this.w_KEY4 = SSDATFIN
      this.w_OLDTEL = SSTELEFO
      this.w_TEL = ConvNum(SSTELEFO, "+", 1)
      this.w_OLDFAX = SSTELFAX
      this.w_FAX = ConvNum(SSTELFAX, "+", 1)
      this.w_OLDCEL = SSNUMCEL
      this.w_CEL = ConvNum(SSNUMCEL, "+", 1)
      if NOT(ALLTRIM(this.w_OLDTEL)==ALLTRIM(this.w_TEL)) OR NOT(ALLTRIM(this.w_OLDFAX)==ALLTRIM(this.w_FAX)) OR NOT(ALLTRIM(this.w_OLDCEL)==ALLTRIM(this.w_CEL))
        this.w_ERROR = this.w_ERROR+1
        if NOT(ALLTRIM(this.w_OLDTEL)==ALLTRIM(this.w_TEL)) 
          this.w_oERRORMESS.AddMsgLogPartNoTrans(space(4), "Tabella Dati storicizzati  %1: %2 Periodo: %3 Telef.: %4 Telef. normalizzato: %5", IIF(SSTIPCON="C", "Cli.","For."), SSCODICE, dtoc(SSDATINI)+" - "+dtoc(SSDATFIN), this.w_OLDTEL, this.w_TEL)     
        endif
        if NOT(ALLTRIM(this.w_OLDFAX)==ALLTRIM(this.w_FAX))
          this.w_oERRORMESS.AddMsgLogPartNoTrans(space(4), "Tabella Dati storicizzati  %1: %2 Periodo: %3 Fax: %4 Fax normalizzato: %5", IIF(SSTIPCON="C", "Cli.","For."), SSCODICE, dtoc(SSDATINI)+" - "+dtoc(SSDATFIN), this.w_OLDFAX, this.w_FAX)     
        endif
        if NOT(ALLTRIM(this.w_OLDCEL)==ALLTRIM(this.w_CEL))
          this.w_oERRORMESS.AddMsgLogPartNoTrans(space(4), "Tabella Dati storicizzati  %1: %2 Periodo: %3 Cel.: %4 Cel. normalizzato: %5", IIF(SSTIPCON="C", "Cli.","For."), SSCODICE, dtoc(SSDATINI)+" - "+dtoc(SSDATFIN), this.w_OLDCEL, this.w_CEL)     
        endif
        INSERT INTO Temp (TABE, DESKEY1, KEY1, DESKEY2, KEY2, DESKEY3, KEY3, DESKEY4, KEY4, CAMPOTEL, TEL, CAMPOFAX, FAX,CAMPOTEL2, TEL2) VALUES (this.w_TABELLA, this.w_DESKEY1, this.w_KEY1, this.w_DESKEY2, this.w_KEY2, this.w_DESKEY3, dtos(this.w_KEY3), this.w_DESKEY4, dtos(this.w_KEY4), "SSTELEFO", this.w_TEL, "SSTELFAX", this.w_FAX,"SSNUMCEL", this.w_CEL)
      endif
        select _Curs_SED_STOR
        continue
      enddo
      use
    endif
    * --- Contatti
    * --- Select from CONTATTI
    i_nConn=i_TableProp[this.CONTATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTATTI_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select COTIPCON,COTIPCON,COCODCON,CPROWNUM,CONUMTEL,CORIFPER,CONUMCEL  from "+i_cTable+" CONTATTI ";
           ,"_Curs_CONTATTI")
    else
      select COTIPCON,COTIPCON,COCODCON,CPROWNUM,CONUMTEL,CORIFPER,CONUMCEL from (i_cTable);
        into cursor _Curs_CONTATTI
    endif
    if used('_Curs_CONTATTI')
      select _Curs_CONTATTI
      locate for 1=1
      do while not(eof())
      this.w_TABELLA = "CONTATTI"
      this.w_DESKEY1 = "COTIPCON"
      this.w_KEY1 = COTIPCON
      this.w_DESKEY2 = "COCODCON"
      this.w_KEY2 = COCODCON
      this.w_DESKEY3 = "CPROWNUM"
      this.w_KEY3 = CPROWNUM
      this.w_OLDTEL = CONUMTEL
      this.w_TEL = ConvNum(CONUMTEL, "+", 1)
      this.w_OLDCEL = CONUMCEL
      this.w_CEL = ConvNum(CONUMCEL, "+", 1)
      if NOT(ALLTRIM(this.w_OLDTEL)==ALLTRIM(this.w_TEL)) OR NOT(ALLTRIM(this.w_OLDCEL)==ALLTRIM(this.w_CEL)) 
        this.w_ERROR = this.w_ERROR+1
        if NOT(ALLTRIM(this.w_OLDTEL)==ALLTRIM(this.w_TEL)) 
          this.w_oERRORMESS.AddMsgLogPartNoTrans(space(4), "Tabella Contatti  %1: %2 Rif.persona: %3 Telef.: %4 Telef. normalizzato: %5", IIF(COTIPCON="C", "Cli.","For."), COCODCON, alltrim(CORIFPER), this.w_OLDTEL, this.w_TEL)     
        endif
        if NOT(ALLTRIM(this.w_OLDCEL)==ALLTRIM(this.w_CEL)) 
          this.w_oERRORMESS.AddMsgLogPartNoTrans(space(4), "Tabella Contatti  %1: %2 Rif.persona: %3 Cel.: %4 Cel. normalizzato: %5", IIF(COTIPCON="C", "Cli.","For."), COCODCON, alltrim(CORIFPER), this.w_OLDCEL, this.w_CEL)     
        endif
        INSERT INTO Temp (TABE, DESKEY1, KEY1, DESKEY2, KEY2, DESKEY3, KEY3, CAMPOTEL, TEL,CAMPOTEL2,TEL2) VALUES (this.w_TABELLA, this.w_DESKEY1, this.w_KEY1, this.w_DESKEY2, this.w_KEY2, this.w_DESKEY3, ALLTRIM(STR(this.w_KEY3)), "CONUMTEL", this.w_TEL,"CONUMCEL", this.w_CEL)
      endif
        select _Curs_CONTATTI
        continue
      enddo
      use
    endif
    * --- Dati rappresentante firmatario
    * --- Select from DAT_RAPP
    i_nConn=i_TableProp[this.DAT_RAPP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DAT_RAPP_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select RFCODAZI,RFTELEFO,RFCOGNOM,RF__NOME  from "+i_cTable+" DAT_RAPP ";
          +" where RFCODAZI=w_COD_AZI";
           ,"_Curs_DAT_RAPP")
    else
      select RFCODAZI,RFTELEFO,RFCOGNOM,RF__NOME from (i_cTable);
       where RFCODAZI=w_COD_AZI;
        into cursor _Curs_DAT_RAPP
    endif
    if used('_Curs_DAT_RAPP')
      select _Curs_DAT_RAPP
      locate for 1=1
      do while not(eof())
      this.w_TABELLA = "DAT_RAPP"
      this.w_DESKEY1 = "RFCODAZI"
      this.w_KEY1 = RFCODAZI
      this.w_OLDTEL = RFTELEFO
      this.w_TEL = ConvNum(RFTELEFO, "+", 1)
      if NOT(ALLTRIM(this.w_OLDTEL)==ALLTRIM(this.w_TEL))
        this.w_ERROR = this.w_ERROR+1
        this.w_oERRORMESS.AddMsgLogPartNoTrans(space(4), "Tabella Dati rappresentante firmatario  Rappr.: %1 Telef.: %2 Telef. normalizzato: %3", alltrim(RFCOGNOM)+space(1)+alltrim(RF__NOME), this.w_OLDTEL, this.w_TEL)     
        INSERT INTO Temp (TABE, DESKEY1, KEY1, CAMPOTEL, TEL) VALUES (this.w_TABELLA, this.w_DESKEY1, this.w_KEY1, "RFTELEFO", this.w_TEL)
      endif
        select _Curs_DAT_RAPP
        continue
      enddo
      use
    endif
    * --- Attivit�
    * --- Select from OFF_ATTI
    i_nConn=i_TableProp[this.OFF_ATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select ATSERIAL,ATTELEFO,AT___FAX,ATOGGETT,ATDATINI,ATCELLUL  from "+i_cTable+" OFF_ATTI ";
           ,"_Curs_OFF_ATTI")
    else
      select ATSERIAL,ATTELEFO,AT___FAX,ATOGGETT,ATDATINI,ATCELLUL from (i_cTable);
        into cursor _Curs_OFF_ATTI
    endif
    if used('_Curs_OFF_ATTI')
      select _Curs_OFF_ATTI
      locate for 1=1
      do while not(eof())
      this.w_TABELLA = "OFF_ATTI"
      this.w_DESKEY1 = "ATSERIAL"
      this.w_KEY1 = ATSERIAL
      this.w_OLDTEL = ATTELEFO
      this.w_TEL = ConvNum(ATTELEFO, "+", 1)
      this.w_OLDFAX = AT___FAX
      this.w_FAX = ConvNum(AT___FAX, "+", 1)
      this.w_OLDCEL = ATCELLUL
      this.w_CEL = ConvNum(ATCELLUL, "+", 1)
      if NOT(ALLTRIM(this.w_OLDTEL)==ALLTRIM(this.w_TEL)) OR NOT(ALLTRIM(this.w_OLDFAX)==ALLTRIM(this.w_FAX)) OR NOT(ALLTRIM(this.w_OLDCEL)==ALLTRIM(this.w_CEL))
        this.w_ERROR = this.w_ERROR+1
        if NOT(ALLTRIM(this.w_OLDTEL)==ALLTRIM(this.w_TEL)) 
          this.w_oERRORMESS.AddMsgLogPartNoTrans(space(4), "Tabella Attivit�  Oggetto: %1 Data iniz.: %2 Telef.: %3 Telef. normalizzato: %4", alltrim(ATOGGETT), TTOC(ATDATINI), this.w_OLDTEL, this.w_TEL)     
        endif
        if NOT(ALLTRIM(this.w_OLDFAX)==ALLTRIM(this.w_FAX))
          * --- Non inseriamo il Fax nella stampa di log poich� il Fax non appare nell'anagrafica delle Attivit�
        endif
        if NOT(ALLTRIM(this.w_OLDCEL)==ALLTRIM(this.w_CEL))
          this.w_oERRORMESS.AddMsgLogPartNoTrans(space(4), "Tabella Attivit�  Oggetto: %1 Data iniz.: %2 Cel.: %3 Cel. normalizzato: %4", alltrim(ATOGGETT), TTOC(ATDATINI), this.w_OLDCEL, this.w_CEL)     
        endif
        INSERT INTO Temp (TABE, DESKEY1, KEY1, CAMPOTEL, TEL, CAMPOFAX, FAX,CAMPOTEL2, TEL2) VALUES (this.w_TABELLA, this.w_DESKEY1, this.w_KEY1, "ATTELEFO", this.w_TEL, "AT___FAX", this.w_FAX, "ATCELLUL", this.w_CEL)
      endif
        select _Curs_OFF_ATTI
        continue
      enddo
      use
    endif
  endproc


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,18)]
    this.cWorkTables[1]='F23_PAGA'
    this.cWorkTables[2]='AZIENDA'
    this.cWorkTables[3]='SEDIAZIE'
    this.cWorkTables[4]='NOM_CONT'
    this.cWorkTables[5]='OFF_NOMI'
    this.cWorkTables[6]='UTE_NTI'
    this.cWorkTables[7]='MAGAZZIN'
    this.cWorkTables[8]='AGENTI'
    this.cWorkTables[9]='TITOLARI'
    this.cWorkTables[10]='CONTI'
    this.cWorkTables[11]='DES_DIVE'
    this.cWorkTables[12]='DIPENDEN'
    this.cWorkTables[13]='CAN_TIER'
    this.cWorkTables[14]='BAN_CONTI'
    this.cWorkTables[15]='SED_STOR'
    this.cWorkTables[16]='CONTATTI'
    this.cWorkTables[17]='DAT_RAPP'
    this.cWorkTables[18]='OFF_ATTI'
    return(this.OpenAllTables(18))

  proc CloseCursors()
    if used('_Curs__d__d__AGEN_EXE_QUERY_TELIMPIAN_d_VQR')
      use in _Curs__d__d__AGEN_EXE_QUERY_TELIMPIAN_d_VQR
    endif
    if used('_Curs__d__d__PCON_EXE_QUERY_TELCLIENTI_d_VQR')
      use in _Curs__d__d__PCON_EXE_QUERY_TELCLIENTI_d_VQR
    endif
    if used('_Curs_QUERY_TELCONTIBAN_d_VQR')
      use in _Curs_QUERY_TELCONTIBAN_d_VQR
    endif
    if used('_Curs_AZIENDA')
      use in _Curs_AZIENDA
    endif
    if used('_Curs_QUERY_TELUFFCOMU_d_VQR')
      use in _Curs_QUERY_TELUFFCOMU_d_VQR
    endif
    if used('_Curs_AZIENDA')
      use in _Curs_AZIENDA
    endif
    if used('_Curs_QUERY_TELRIFBAN_d_VQR')
      use in _Curs_QUERY_TELRIFBAN_d_VQR
    endif
    if used('_Curs_QUERY_TELVETTORI_d_VQR')
      use in _Curs_QUERY_TELVETTORI_d_VQR
    endif
    if used('_Curs_QUERY_TELANAGRAF_d_VQR')
      use in _Curs_QUERY_TELANAGRAF_d_VQR
    endif
    if used('_Curs_SEDIAZIE')
      use in _Curs_SEDIAZIE
    endif
    if used('_Curs_NOM_CONT')
      use in _Curs_NOM_CONT
    endif
    if used('_Curs_OFF_NOMI')
      use in _Curs_OFF_NOMI
    endif
    if used('_Curs_UTE_NTI')
      use in _Curs_UTE_NTI
    endif
    if used('_Curs_MAGAZZIN')
      use in _Curs_MAGAZZIN
    endif
    if used('_Curs_AGENTI')
      use in _Curs_AGENTI
    endif
    if used('_Curs_TITOLARI')
      use in _Curs_TITOLARI
    endif
    if used('_Curs_CONTI')
      use in _Curs_CONTI
    endif
    if used('_Curs_DES_DIVE')
      use in _Curs_DES_DIVE
    endif
    if used('_Curs_DIPENDEN')
      use in _Curs_DIPENDEN
    endif
    if used('_Curs_CAN_TIER')
      use in _Curs_CAN_TIER
    endif
    if used('_Curs_BAN_CONTI')
      use in _Curs_BAN_CONTI
    endif
    if used('_Curs_SED_STOR')
      use in _Curs_SED_STOR
    endif
    if used('_Curs_CONTATTI')
      use in _Curs_CONTATTI
    endif
    if used('_Curs_DAT_RAPP')
      use in _Curs_DAT_RAPP
    endif
    if used('_Curs_OFF_ATTI')
      use in _Curs_OFF_ATTI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
