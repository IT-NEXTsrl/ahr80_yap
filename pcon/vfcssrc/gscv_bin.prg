* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscv_bin                                                        *
*              Rimuovo integrit� della tabella                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-04-28                                                      *
* Last revis.: 2009-02-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pCODAZI,pTABLE,pNHF,pPK
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscv_bin",oParentObject,m.pCODAZI,m.pTABLE,m.pNHF,m.pPK)
return(i_retval)

define class tgscv_bin as StdBatch
  * --- Local variables
  pCODAZI = space(5)
  pTABLE = space(30)
  pNHF = 0
  pPK = space(1)
  w_IDXTABLE = 0
  w_PHNAME = space(50)
  w_CONN = 0
  w_TMPC = space(10)
  w_TEMP = space(10)
  w_PK = space(1)
  * --- WorkFile variables
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Rimuove integrit� referenziale riceve:
    *     - Codice Azienda
    *     - nome tabella su cui rimuovere intergirt� referenziali
    *     - riferimento file di log procedura di conversione
    *     - parametro per eliminare o meno anche le integrita referenziali delle tabelle collegate
    * --- Di default elimino tutte le integrit� referenziali
    this.w_PK = IIf( vartype(this.pPK)="C" , this.pPK="S" , .t. ) 
    this.w_IDXTABLE = cp_OpenTable( Alltrim(this.pTABLE) ,.T.)
    * --- Se indice a 0 � una tabella temporanea...
    if this.w_IDXTABLE<>0
      this.w_PHNAME = i_dcx.GetPhTable( i_dcx.GetTableIdx( this.pTABLE ) , this.pCODAZI )
      this.w_CONN = i_TableProp[ this.w_IDXTABLE , 3 ] 
      if this.w_CONN<>0
         
 cp_RemoveReferences( this.pTABLE , this.w_PHNAME , this.w_CONN ,i_dcx, cp_GetDatabaseType( this.w_CONN) ,this.pCODAZI ) 
        this.RemoveIndexes(this.pTABLE , this.w_PHNAME , this.w_CONN ,i_dcx, cp_GetDatabaseType( this.w_CONN) ,this.pCODAZI )
        if Vartype(this.pNHF)="N"
          this.w_TMPC = AH_MSGFORMAT( "Integrit� cancellate per tabella %1 " , this.w_PHNAME) 
          this.w_TEMP = SUPPSRVP("WRITELOG",this.pNHF, this.w_TMPC)
        endif
      endif
    endif
  endproc


  proc Init(oParentObject,pCODAZI,pTABLE,pNHF,pPK)
    this.pCODAZI=pCODAZI
    this.pTABLE=pTABLE
    this.pNHF=pNHF
    this.pPK=pPK
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- gscv_bin
  proc RemoveIndexes(cName,cPhName,nConn,oDCX,cDatabaseType,cAzi)
    local i,idxname,r,k
    private res
    if nConn=0
      * --- apre il file
      cp_SafeUseExcl(cPhName)
    endif
    * --- rimuove le chiavi
    * --- zucchetti aulla: modifica per errore riduzione indici su oracle
    for i=1 to max(oDCX.GetIdxCount(cName),10)
      if nConn<>0
        idxname = cPhName+alltrim(str(i))
        if i=1 
          * --- controlla se la chiave primaria e' stata modificata
          IF (this.w_PK=.T.  AND upper(CP_DBTYPE)<>'DB2')
            k=db_DropPrimaryKey(cName,cPhName,cDatabaseType)
            r=sqlexec(nConn,"alter table "+cPhName+" "+k)
            if r=-1
              * --- non e' riuscito a cancellare la chiave primaria, ci sono delle referenze
              * --- droppa tutte le foreign key, che tanto poi dovranno essere ricostruite
              cp_RemoveReferences(cName,cPhName,nConn,oDcx,cDatabaseType,cAzi)
              r=cp_SQL(nConn,"alter table "+cPhName+" "+k,.F.,.F.,.T.)
            endif
            
            * --- cancellazione indice se su oracle 10
            if cDatabaseType='DB2/390' or cDatabaseType='Oracle'
              r=cp_SQL(nConn,"drop index pk_"+cPhName,.F.,.F.,.T.)
            endif
          ENDIF
        else
          k=db_DropIndex(cName,cPhName,idxname,cDatabaseType)
          r=sqlexec(nConn,"drop index "+k)
          if i>oDCX.GetIdxCount(cName) and r=-1
             exit
          endif
          
        endif
      else
        idxname = 'IDX'+alltrim(str(i))
        on error res=.f.
        delete tag &idxname
        if i=1
          delete tag idxdel
          delete tag idx1bis
        endif
        on error
      endif
    next
    if nConn=0
      * --- chiude il file
      use
    endif
    return
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pCODAZI,pTABLE,pNHF,pPK"
endproc
