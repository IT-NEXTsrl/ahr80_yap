* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscv_bgm                                                        *
*              Controllo allegati cliente fornitore                            *
*                                                                              *
*      Author: Zucchetti - AT                                                  *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-06-20                                                      *
* Last revis.: 2007-09-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscv_bgm",oParentObject,m.w_NHF)
return(i_retval)

define class tgscv_bgm as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_PATH = space(100)
  w_ELABPATH = space(100)
  w_CONTAVMN = 0
  w_TOTVMN = 0
  w_NOMEVMN = space(15)
  w_OKCUSTOM = 0
  w_APPMENU = space(10)
  w_LEFTMENU = space(10)
  w_RIGHTMENU = space(10)
  w_PRINCMENU = space(10)
  w_AGGANCIO = space(40)
  * --- WorkFile variables
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- CONTROLLO ALLEGATI CLIENTE FORNITORE 
    * --- FIle di LOG
    this.w_PATH = SYS(5)+SYS(2003)
    * --- ELABPATH � passato alla routine  GSCV_BGN che elabora il VMN
    this.w_ELABPATH = this.w_PATH+"\"
    * --- Se non esiste il CUSTOM.VMN viene creato ex-novo
    if !FILE(this.w_ELABPATH+"CUSTOM.VMN") And !FILE(this.w_ELABPATH+"CUSTOM\CUSTOM.VMN")
      * --- Creazione file CUSTOM.VMN in exe 
      this.w_APPMENU = FILETOSTR(this.w_ELABPATH+"\DEFAULT.VMN")
      this.w_PRINCMENU = SUBSTR(this.w_APPMENU, 1, AT(CHR(13)+CHR(10), this.w_APPMENU, 27)+1)
      this.w_LEFTMENU = SUBSTR(this.w_PRINCMENU, 1, AT(CHR(13)+CHR(10), this.w_PRINCMENU, 6)+1) +" 1"
      this.w_RIGHTMENU = SUBSTR(this.w_PRINCMENU, AT(CHR(13)+CHR(10), this.w_APPMENU, 7)) +"*"+ CHR(13)+CHR(10)
      this.w_OKCUSTOM = STRTOFILE(this.w_LEFTMENU+this.w_RIGHTMENU, this.w_ELABPATH+"CUSTOM.VMN")
    endif
    * --- Ricerca menu CUSTOM*.VMN prima in EXE e quindi  in EXE\CUSTOM
    this.w_TOTVMN = ADIR(ELEMENU, this.w_ELABPATH+"CUSTOM*.VMN")
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_ELABPATH = this.w_PATH+"\CUSTOM\"
    this.w_TOTVMN = ADIR(ELEMENU, this.w_ELABPATH+"CUSTOM*.VMN")
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if this.w_NHF>=0
      this.w_TMPC = AH_MsgFormat("- Aggiornamento menu eseguito con successo.%0Per rendere operative le modifiche a men�, uscire e rientrare nella procedura.")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = this.w_TMPC
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Cicla sui .VMN da elaborare
    * --- Se viene passato l'aggancio la procedura aggancia la voce con il path specificato
    *     altrimenti viene legata la chiave alla voce di menu passata come chiave
    *     g_MENUSEP=CHR(172)
    *     Parametri 
    *     Men�
    *     Programma Chiave di ricerca
    *     Programma da inserire
    *     Descrizione Voce
    *     SubMenu
    *     Path di aggancio (significativo solo se non specificato il Programma chiave di ricerca)
    this.w_CONTAVMN = 1
    do while this.w_CONTAVMN <= this.w_TOTVMN
      this.w_NOMEVMN = ELEMENU(this.w_CONTAVMN,1)
      ah_msg( "Aggiornamento men� %1 in corso",.t.,.f.,.f.,ALLTRIM(this.w_ELABPATH+this.w_NOMEVMN))
      COPY FILE (this.w_ELABPATH+this.w_NOMEVMN) TO this.w_ELABPATH + this.w_NOMEVMN +".bak"
      * --- Elabora nenu (inserimento voce di menu)
      if g_APPLICATION = "ADHOC REVOLUTION"
        this.w_AGGANCIO = "CONTABILIT�"+g_MENUSEP+"CONTABILIT� IVA"+g_MENUSEP+"STAMPA PLAFOND SOSPENSIONE IVA###D"
        GSCV_BGN(this,this.w_ELABPATH+this.w_NOMEVMN,"","GSCG_KEZ","&Gestione dati comunicazione","Elen&chi clienti/fornitori","",this.w_AGGANCIO)
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      else
        this.w_AGGANCIO = "GENERALE"+g_MENUSEP+"OPERAZIONI ANNUALI"+g_MENUSEP+"STAMPA PLAFOND ANNUALE###D"
        GSCV_BGN(this,this.w_ELABPATH+this.w_NOMEVMN,"","GSCG_KEZ","&Gestione dati comunicazione","Elen&chi clienti/fornitori","",this.w_AGGANCIO)
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      GSCV_BGN(this,this.w_ELABPATH+this.w_NOMEVMN,"GSCG_KEZ","GSCG_KEE","&Estrazione dati","","")
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      GSCV_BGN(this,this.w_ELABPATH+this.w_NOMEVMN,"GSCG_KEE","GSCG_AGE","&Generazione file","","")
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      GSCV_BGN(this,this.w_ELABPATH+this.w_NOMEVMN,"GSCG_AGE","GSCG_MEL","&Dati comunicazione","","")
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      GSCV_BGN(this,this.w_ELABPATH+this.w_NOMEVMN,"GSCG_MEL","GSCG_APE","&Parametri","","")
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_CONTAVMN = this.w_CONTAVMN+1
    enddo
  endproc


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
