* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcb45                                                        *
*              Aggiornamento attivit� offerte                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-03-18                                                      *
* Last revis.: 2011-01-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcb45",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcb45 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_READAZI = space(5)
  w_POTIPATT = space(20)
  w_ATDATFIN = ctot("")
  w_TROVATA = .f.
  w_CodRel = space(15)
  w_NORMINI = 0
  w_NORMFIN = 0
  w_URGEINI = 0
  w_URGEFIN = 0
  w_SCADINI = 0
  w_SCADFIN = 0
  w_PRIINI = 0
  w_PRIFIN = 0
  w_AGGPRI = 0
  w_OKPRI = .f.
  * --- WorkFile variables
  OFF_ATTI_idx=0
  PAR_OFFE_idx=0
  OFF_PART_idx=0
  CONVERSI_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiornamento tabella OFF_ATTI
    * --- FIle di LOG
    this.w_CodRel = "7.0-M5930"
    this.w_OKPRI = .F.
    this.w_READAZI = i_CODAZI
    * --- Legge se eseguita la conversione  7.0-M5930
    * --- Read from CONVERSI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CONVERSI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONVERSI_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "*"+;
        " from "+i_cTable+" CONVERSI where ";
            +"COCODREL = "+cp_ToStrODBC(this.w_CodRel);
            +" and COESEGUI = "+cp_ToStrODBC("S");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        *;
        from (i_cTable) where;
            COCODREL = this.w_CodRel;
            and COESEGUI = "S";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_TROVATA = i_Rows>0
    if g_AGEN="S" and g_OFFE="S" AND NOT this.w_TROVATA
      vq_exec ("..\pcon\exe\query\GSCVCB45_1", this, "__tmp__")
      * --- Read from PAR_OFFE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PAR_OFFE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_OFFE_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "POTIPATT"+;
          " from "+i_cTable+" PAR_OFFE where ";
              +"POCODAZI = "+cp_ToStrODBC(this.w_READAZI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          POTIPATT;
          from (i_cTable) where;
              POCODAZI = this.w_READAZI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_POTIPATT = NVL(cp_ToDate(_read_.POTIPATT),cp_NullValue(_read_.POTIPATT))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if Reccount("__TMP__") <>0
        if Ah_yesno("Si desidera stampare elenco attivit� con operatori da associare alle persone?")
          CP_CHPRN("..\PCON\EXE\QUERY\GSCVCB45_1.FRX"," ",this.oParentObject)
        endif
      else
        do gscv_k45 with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      if Not Empty(this.w_POTIPATT) and Reccount("__TMP__") =0 AND this.w_OKPRI
        * --- Try
        local bErr_03816768
        bErr_03816768=bTrsErr
        this.Try_03816768()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- Gestisce log errori
          this.oParentObject.w_PMSG = "Errore nell'aggiornamento tabella OFF_ATTI:" + Message()
          this.oParentObject.w_PESEOK = .F.
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          if this.w_NHF>=0
            this.w_TMPC = this.oParentObject.w_PMSG
            this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
          endif
        endif
        bTrsErr=bTrsErr or bErr_03816768
        * --- End
      else
        do case
          case Empty(this.w_POTIPATT)
            this.oParentObject.w_PMSG = "Attenzione, tipo attivit� non presente nei parametri offerte"
          case Reccount("__TMP__") <>0
            this.oParentObject.w_PMSG = "Attenzione, esistono attivit� con operatori non associati a persone"
          otherwise
            this.oParentObject.w_PMSG = "Operazione abbandonata dall'utente"
        endcase
        this.oParentObject.w_PESEOK = .F.
        if this.w_NHF>=0
          this.w_TMPC = this.oParentObject.w_PMSG
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
    else
      * --- Esecuzione ok
      this.oParentObject.w_PESEOK = .T.
      this.w_TMPC = AH_MsgFormat("aggiornamento tabella OFF_ATTI eseguita correttamente")
      this.oParentObject.w_PMSG = this.w_TMPC
    endif
    USE IN SELECT("__TMP__")
  endproc
  proc Try_03816768()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- aggiorno status
    * --- Select from gscvcb45
    do vq_exec with 'gscvcb45',this,'_Curs_gscvcb45','',.f.,.t.
    if used('_Curs_gscvcb45')
      select _Curs_gscvcb45
      locate for 1=1
      do while not(eof())
      this.w_ATDATFIN = _Curs_gscvcb45.ATDATINI+INT((Nvl(_Curs_gscvcb45.CADURORE,0)+INT((Nvl(_Curs_gscvcb45.CADURMIN,0))/60)) / 24)
      * --- Write into OFF_ATTI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.OFF_ATTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_ATTI_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"ATSTATUS ="+cp_NullLink(cp_ToStrODBC(_Curs_gscvcb45.ATSTATUS),'OFF_ATTI','ATSTATUS');
        +",ATCAUATT ="+cp_NullLink(cp_ToStrODBC(this.w_POTIPATT),'OFF_ATTI','ATCAUATT');
        +",ATDATFIN ="+cp_NullLink(cp_ToStrODBC(this.w_ATDATFIN),'OFF_ATTI','ATDATFIN');
            +i_ccchkf ;
        +" where ";
            +"ATSERIAL = "+cp_ToStrODBC(_Curs_gscvcb45.ATSERIAL);
               )
      else
        update (i_cTable) set;
            ATSTATUS = _Curs_gscvcb45.ATSTATUS;
            ,ATCAUATT = this.w_POTIPATT;
            ,ATDATFIN = this.w_ATDATFIN;
            &i_ccchkf. ;
         where;
            ATSERIAL = _Curs_gscvcb45.ATSERIAL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Insert into OFF_PART
      i_nConn=i_TableProp[this.OFF_PART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.OFF_PART_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.OFF_PART_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"PASERIAL"+",CPROWNUM"+",CPROWORD"+",PATIPRIS"+",PACODRIS"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(_Curs_gscvcb45.ATSERIAL),'OFF_PART','PASERIAL');
        +","+cp_NullLink(cp_ToStrODBC(1),'OFF_PART','CPROWNUM');
        +","+cp_NullLink(cp_ToStrODBC(10),'OFF_PART','CPROWORD');
        +","+cp_NullLink(cp_ToStrODBC("P"),'OFF_PART','PATIPRIS');
        +","+cp_NullLink(cp_ToStrODBC(_Curs_gscvcb45.DPCODICE),'OFF_PART','PACODRIS');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'PASERIAL',_Curs_gscvcb45.ATSERIAL,'CPROWNUM',1,'CPROWORD',10,'PATIPRIS',"P",'PACODRIS',_Curs_gscvcb45.DPCODICE)
        insert into (i_cTable) (PASERIAL,CPROWNUM,CPROWORD,PATIPRIS,PACODRIS &i_ccchkf. );
           values (;
             _Curs_gscvcb45.ATSERIAL;
             ,1;
             ,10;
             ,"P";
             ,_Curs_gscvcb45.DPCODICE;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
        select _Curs_gscvcb45
        continue
      enddo
      use
    endif
    this.w_PRIINI = this.w_NORMINI
    this.w_PRIFIN = this.w_NORMFIN
    this.w_AGGPRI = 1
    * --- Write into OFF_ATTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.OFF_ATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="ATSERIAL"
      do vq_exec with 'gscvpb45',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_ATTI_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="OFF_ATTI.ATSERIAL = _t2.ATSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ATNUMPRI  = _t2.ATNUMPRI";
          +i_ccchkf;
          +" from "+i_cTable+" OFF_ATTI, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="OFF_ATTI.ATSERIAL = _t2.ATSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OFF_ATTI, "+i_cQueryTable+" _t2 set ";
          +"OFF_ATTI.ATNUMPRI  = _t2.ATNUMPRI";
          +Iif(Empty(i_ccchkf),"",",OFF_ATTI.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="OFF_ATTI.ATSERIAL = t2.ATSERIAL";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OFF_ATTI set (";
          +"ATNUMPRI ";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.ATNUMPRI";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="OFF_ATTI.ATSERIAL = _t2.ATSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OFF_ATTI set ";
          +"ATNUMPRI  = _t2.ATNUMPRI";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".ATSERIAL = "+i_cQueryTable+".ATSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ATNUMPRI  = (select ATNUMPRI from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    this.w_PRIINI = this.w_URGEINI
    this.w_PRIFIN = this.w_URGEFIN
    this.w_AGGPRI = 3
    * --- Write into OFF_ATTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.OFF_ATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="ATSERIAL"
      do vq_exec with 'gscvpb45',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_ATTI_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="OFF_ATTI.ATSERIAL = _t2.ATSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ATNUMPRI  = _t2.ATNUMPRI";
          +i_ccchkf;
          +" from "+i_cTable+" OFF_ATTI, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="OFF_ATTI.ATSERIAL = _t2.ATSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OFF_ATTI, "+i_cQueryTable+" _t2 set ";
          +"OFF_ATTI.ATNUMPRI  = _t2.ATNUMPRI";
          +Iif(Empty(i_ccchkf),"",",OFF_ATTI.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="OFF_ATTI.ATSERIAL = t2.ATSERIAL";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OFF_ATTI set (";
          +"ATNUMPRI ";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.ATNUMPRI";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="OFF_ATTI.ATSERIAL = _t2.ATSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OFF_ATTI set ";
          +"ATNUMPRI  = _t2.ATNUMPRI";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".ATSERIAL = "+i_cQueryTable+".ATSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ATNUMPRI  = (select ATNUMPRI from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    this.w_PRIINI = this.w_SCADINI
    this.w_PRIFIN = this.w_SCADFIN
    this.w_AGGPRI = 4
    * --- Write into OFF_ATTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.OFF_ATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="ATSERIAL"
      do vq_exec with 'gscvpb45',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_ATTI_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="OFF_ATTI.ATSERIAL = _t2.ATSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ATNUMPRI  = _t2.ATNUMPRI";
          +i_ccchkf;
          +" from "+i_cTable+" OFF_ATTI, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="OFF_ATTI.ATSERIAL = _t2.ATSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OFF_ATTI, "+i_cQueryTable+" _t2 set ";
          +"OFF_ATTI.ATNUMPRI  = _t2.ATNUMPRI";
          +Iif(Empty(i_ccchkf),"",",OFF_ATTI.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="OFF_ATTI.ATSERIAL = t2.ATSERIAL";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OFF_ATTI set (";
          +"ATNUMPRI ";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.ATNUMPRI";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="OFF_ATTI.ATSERIAL = _t2.ATSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OFF_ATTI set ";
          +"ATNUMPRI  = _t2.ATNUMPRI";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".ATSERIAL = "+i_cQueryTable+".ATSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ATNUMPRI  = (select ATNUMPRI from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- metto normali le rimanenti
    * --- Write into OFF_ATTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.OFF_ATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="ATSERIAL"
      do vq_exec with 'gscvnb45',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_ATTI_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="OFF_ATTI.ATSERIAL = _t2.ATSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ATNUMPRI  = _t2.ATNUMPRI";
          +i_ccchkf;
          +" from "+i_cTable+" OFF_ATTI, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="OFF_ATTI.ATSERIAL = _t2.ATSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OFF_ATTI, "+i_cQueryTable+" _t2 set ";
          +"OFF_ATTI.ATNUMPRI  = _t2.ATNUMPRI";
          +Iif(Empty(i_ccchkf),"",",OFF_ATTI.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="OFF_ATTI.ATSERIAL = t2.ATSERIAL";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OFF_ATTI set (";
          +"ATNUMPRI ";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.ATNUMPRI";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="OFF_ATTI.ATSERIAL = _t2.ATSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OFF_ATTI set ";
          +"ATNUMPRI  = _t2.ATNUMPRI";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".ATSERIAL = "+i_cQueryTable+".ATSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ATNUMPRI  = (select ATNUMPRI from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.w_TMPC = AH_MsgFormat("aggiornamento tabella OFF_ATTI eseguita correttamente")
    this.oParentObject.w_PMSG = this.w_TMPC
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='OFF_ATTI'
    this.cWorkTables[2]='PAR_OFFE'
    this.cWorkTables[3]='OFF_PART'
    this.cWorkTables[4]='CONVERSI'
    return(this.OpenAllTables(4))

  proc CloseCursors()
    if used('_Curs_gscvcb45')
      use in _Curs_gscvcb45
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
