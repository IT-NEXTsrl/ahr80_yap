* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcc18                                                        *
*              Imposta maiuscolo in nota di iscrizione                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-03-18                                                      *
* Last revis.: 2012-12-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcc18",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcc18 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  * --- WorkFile variables
  ISC_MAST_idx=0
  ISC_DETT_idx=0
  ISC_AVVO_idx=0
  CONF_INT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- FIle di LOG
    if IsAlt()
      * --- Solo per AeTop
      * --- Try
      local bErr_04C860A8
      bErr_04C860A8=bTrsErr
      this.Try_04C860A8()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        * --- Gestisce log errori
        this.oParentObject.w_PMSG = Message()
        this.oParentObject.w_PESEOK = .F.
        if this.w_NHF>=0
          this.w_TMPC = this.oParentObject.w_PMSG
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
      bTrsErr=bTrsErr or bErr_04C860A8
      * --- End
    else
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("Procedura di conversione eseguita con successo")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
  endproc
  proc Try_04C860A8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Write into ISC_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.ISC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ISC_MAST_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="NISERIAL"
      do vq_exec with '..\PCON\EXE\QUERY\GSCVCC18',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ISC_MAST_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="ISC_MAST.NISERIAL = _t2.NISERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"NIPRADES = _t2.NIPRADES";
          +",NIDESCRI = _t2.NIDESCRI";
          +",NI_RUOLO = _t2.NI_RUOLO";
          +",NI__FORO = _t2.NI__FORO";
          +",NIESTVER = _t2.NIESTVER";
          +",NIAVVCOG = _t2.NIAVVCOG";
          +",NIAVVNOM = _t2.NIAVVNOM";
          +",NIAVVIND = _t2.NIAVVIND";
          +",NIAVVCAP = _t2.NIAVVCAP";
          +",NIAVVLOC = _t2.NIAVVLOC";
          +",NIAVVPRO = _t2.NIAVVPRO";
          +",NIAVVFIS = _t2.NIAVVFIS";
          +",NIRUOOGG = _t2.NIRUOOGG";
          +",NISFRPRO = _t2.NISFRPRO";
          +",NICFAPA1 = _t2.NICFAPA1";
          +",NICFAPA2 = _t2.NICFAPA2";
          +",NICFANUM = _t2.NICFANUM";
          +",NICFAREM = _t2.NICFAREM";
          +",NICFASER = _t2.NICFASER";
          +",NICFACIT = _t2.NICFACIT";
          +",NICFAPRO = _t2.NICFAPRO";
          +",NICFALUT = _t2.NICFALUT";
          +",NINRGRA1 = _t2.NINRGRA1";
          +",NISENAPP = _t2.NISENAPP";
          +",NIUFFEMI = _t2.NIUFFEMI";
          +",NIGIUDIC = _t2.NIGIUDIC";
          +",NIEPROPE = _t2.NIEPROPE";
          +",NIMENUMN = _t2.NIMENUMN";
          +",NIOPNUMR = _t2.NIOPNUMR";
          +",NIOPNUMD = _t2.NIOPNUMD";
          +",NIUFCACA = _t2.NIUFCACA";
          +",NINRGCAC = _t2.NINRGCAC";
          +",NIRGGRA1 = _t2.NIRGGRA1";
          +",NINUMSEC = _t2.NINUMSEC";
          +",NIDEPCBP = _t2.NIDEPCBP";
          +",NICTUCOD = _t2.NICTUCOD";
          +i_ccchkf;
          +" from "+i_cTable+" ISC_MAST, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="ISC_MAST.NISERIAL = _t2.NISERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ISC_MAST, "+i_cQueryTable+" _t2 set ";
          +"ISC_MAST.NIPRADES = _t2.NIPRADES";
          +",ISC_MAST.NIDESCRI = _t2.NIDESCRI";
          +",ISC_MAST.NI_RUOLO = _t2.NI_RUOLO";
          +",ISC_MAST.NI__FORO = _t2.NI__FORO";
          +",ISC_MAST.NIESTVER = _t2.NIESTVER";
          +",ISC_MAST.NIAVVCOG = _t2.NIAVVCOG";
          +",ISC_MAST.NIAVVNOM = _t2.NIAVVNOM";
          +",ISC_MAST.NIAVVIND = _t2.NIAVVIND";
          +",ISC_MAST.NIAVVCAP = _t2.NIAVVCAP";
          +",ISC_MAST.NIAVVLOC = _t2.NIAVVLOC";
          +",ISC_MAST.NIAVVPRO = _t2.NIAVVPRO";
          +",ISC_MAST.NIAVVFIS = _t2.NIAVVFIS";
          +",ISC_MAST.NIRUOOGG = _t2.NIRUOOGG";
          +",ISC_MAST.NISFRPRO = _t2.NISFRPRO";
          +",ISC_MAST.NICFAPA1 = _t2.NICFAPA1";
          +",ISC_MAST.NICFAPA2 = _t2.NICFAPA2";
          +",ISC_MAST.NICFANUM = _t2.NICFANUM";
          +",ISC_MAST.NICFAREM = _t2.NICFAREM";
          +",ISC_MAST.NICFASER = _t2.NICFASER";
          +",ISC_MAST.NICFACIT = _t2.NICFACIT";
          +",ISC_MAST.NICFAPRO = _t2.NICFAPRO";
          +",ISC_MAST.NICFALUT = _t2.NICFALUT";
          +",ISC_MAST.NINRGRA1 = _t2.NINRGRA1";
          +",ISC_MAST.NISENAPP = _t2.NISENAPP";
          +",ISC_MAST.NIUFFEMI = _t2.NIUFFEMI";
          +",ISC_MAST.NIGIUDIC = _t2.NIGIUDIC";
          +",ISC_MAST.NIEPROPE = _t2.NIEPROPE";
          +",ISC_MAST.NIMENUMN = _t2.NIMENUMN";
          +",ISC_MAST.NIOPNUMR = _t2.NIOPNUMR";
          +",ISC_MAST.NIOPNUMD = _t2.NIOPNUMD";
          +",ISC_MAST.NIUFCACA = _t2.NIUFCACA";
          +",ISC_MAST.NINRGCAC = _t2.NINRGCAC";
          +",ISC_MAST.NIRGGRA1 = _t2.NIRGGRA1";
          +",ISC_MAST.NINUMSEC = _t2.NINUMSEC";
          +",ISC_MAST.NIDEPCBP = _t2.NIDEPCBP";
          +",ISC_MAST.NICTUCOD = _t2.NICTUCOD";
          +Iif(Empty(i_ccchkf),"",",ISC_MAST.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="ISC_MAST.NISERIAL = t2.NISERIAL";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ISC_MAST set (";
          +"NIPRADES,";
          +"NIDESCRI,";
          +"NI_RUOLO,";
          +"NI__FORO,";
          +"NIESTVER,";
          +"NIAVVCOG,";
          +"NIAVVNOM,";
          +"NIAVVIND,";
          +"NIAVVCAP,";
          +"NIAVVLOC,";
          +"NIAVVPRO,";
          +"NIAVVFIS,";
          +"NIRUOOGG,";
          +"NISFRPRO,";
          +"NICFAPA1,";
          +"NICFAPA2,";
          +"NICFANUM,";
          +"NICFAREM,";
          +"NICFASER,";
          +"NICFACIT,";
          +"NICFAPRO,";
          +"NICFALUT,";
          +"NINRGRA1,";
          +"NISENAPP,";
          +"NIUFFEMI,";
          +"NIGIUDIC,";
          +"NIEPROPE,";
          +"NIMENUMN,";
          +"NIOPNUMR,";
          +"NIOPNUMD,";
          +"NIUFCACA,";
          +"NINRGCAC,";
          +"NIRGGRA1,";
          +"NINUMSEC,";
          +"NIDEPCBP,";
          +"NICTUCOD";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.NIPRADES,";
          +"t2.NIDESCRI,";
          +"t2.NI_RUOLO,";
          +"t2.NI__FORO,";
          +"t2.NIESTVER,";
          +"t2.NIAVVCOG,";
          +"t2.NIAVVNOM,";
          +"t2.NIAVVIND,";
          +"t2.NIAVVCAP,";
          +"t2.NIAVVLOC,";
          +"t2.NIAVVPRO,";
          +"t2.NIAVVFIS,";
          +"t2.NIRUOOGG,";
          +"t2.NISFRPRO,";
          +"t2.NICFAPA1,";
          +"t2.NICFAPA2,";
          +"t2.NICFANUM,";
          +"t2.NICFAREM,";
          +"t2.NICFASER,";
          +"t2.NICFACIT,";
          +"t2.NICFAPRO,";
          +"t2.NICFALUT,";
          +"t2.NINRGRA1,";
          +"t2.NISENAPP,";
          +"t2.NIUFFEMI,";
          +"t2.NIGIUDIC,";
          +"t2.NIEPROPE,";
          +"t2.NIMENUMN,";
          +"t2.NIOPNUMR,";
          +"t2.NIOPNUMD,";
          +"t2.NIUFCACA,";
          +"t2.NINRGCAC,";
          +"t2.NIRGGRA1,";
          +"t2.NINUMSEC,";
          +"t2.NIDEPCBP,";
          +"t2.NICTUCOD";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="ISC_MAST.NISERIAL = _t2.NISERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ISC_MAST set ";
          +"NIPRADES = _t2.NIPRADES";
          +",NIDESCRI = _t2.NIDESCRI";
          +",NI_RUOLO = _t2.NI_RUOLO";
          +",NI__FORO = _t2.NI__FORO";
          +",NIESTVER = _t2.NIESTVER";
          +",NIAVVCOG = _t2.NIAVVCOG";
          +",NIAVVNOM = _t2.NIAVVNOM";
          +",NIAVVIND = _t2.NIAVVIND";
          +",NIAVVCAP = _t2.NIAVVCAP";
          +",NIAVVLOC = _t2.NIAVVLOC";
          +",NIAVVPRO = _t2.NIAVVPRO";
          +",NIAVVFIS = _t2.NIAVVFIS";
          +",NIRUOOGG = _t2.NIRUOOGG";
          +",NISFRPRO = _t2.NISFRPRO";
          +",NICFAPA1 = _t2.NICFAPA1";
          +",NICFAPA2 = _t2.NICFAPA2";
          +",NICFANUM = _t2.NICFANUM";
          +",NICFAREM = _t2.NICFAREM";
          +",NICFASER = _t2.NICFASER";
          +",NICFACIT = _t2.NICFACIT";
          +",NICFAPRO = _t2.NICFAPRO";
          +",NICFALUT = _t2.NICFALUT";
          +",NINRGRA1 = _t2.NINRGRA1";
          +",NISENAPP = _t2.NISENAPP";
          +",NIUFFEMI = _t2.NIUFFEMI";
          +",NIGIUDIC = _t2.NIGIUDIC";
          +",NIEPROPE = _t2.NIEPROPE";
          +",NIMENUMN = _t2.NIMENUMN";
          +",NIOPNUMR = _t2.NIOPNUMR";
          +",NIOPNUMD = _t2.NIOPNUMD";
          +",NIUFCACA = _t2.NIUFCACA";
          +",NINRGCAC = _t2.NINRGCAC";
          +",NIRGGRA1 = _t2.NIRGGRA1";
          +",NINUMSEC = _t2.NINUMSEC";
          +",NIDEPCBP = _t2.NIDEPCBP";
          +",NICTUCOD = _t2.NICTUCOD";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".NISERIAL = "+i_cQueryTable+".NISERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"NIPRADES = (select NIPRADES from "+i_cQueryTable+" where "+i_cWhere+")";
          +",NIDESCRI = (select NIDESCRI from "+i_cQueryTable+" where "+i_cWhere+")";
          +",NI_RUOLO = (select NI_RUOLO from "+i_cQueryTable+" where "+i_cWhere+")";
          +",NI__FORO = (select NI__FORO from "+i_cQueryTable+" where "+i_cWhere+")";
          +",NIESTVER = (select NIESTVER from "+i_cQueryTable+" where "+i_cWhere+")";
          +",NIAVVCOG = (select NIAVVCOG from "+i_cQueryTable+" where "+i_cWhere+")";
          +",NIAVVNOM = (select NIAVVNOM from "+i_cQueryTable+" where "+i_cWhere+")";
          +",NIAVVIND = (select NIAVVIND from "+i_cQueryTable+" where "+i_cWhere+")";
          +",NIAVVCAP = (select NIAVVCAP from "+i_cQueryTable+" where "+i_cWhere+")";
          +",NIAVVLOC = (select NIAVVLOC from "+i_cQueryTable+" where "+i_cWhere+")";
          +",NIAVVPRO = (select NIAVVPRO from "+i_cQueryTable+" where "+i_cWhere+")";
          +",NIAVVFIS = (select NIAVVFIS from "+i_cQueryTable+" where "+i_cWhere+")";
          +",NIRUOOGG = (select NIRUOOGG from "+i_cQueryTable+" where "+i_cWhere+")";
          +",NISFRPRO = (select NISFRPRO from "+i_cQueryTable+" where "+i_cWhere+")";
          +",NICFAPA1 = (select NICFAPA1 from "+i_cQueryTable+" where "+i_cWhere+")";
          +",NICFAPA2 = (select NICFAPA2 from "+i_cQueryTable+" where "+i_cWhere+")";
          +",NICFANUM = (select NICFANUM from "+i_cQueryTable+" where "+i_cWhere+")";
          +",NICFAREM = (select NICFAREM from "+i_cQueryTable+" where "+i_cWhere+")";
          +",NICFASER = (select NICFASER from "+i_cQueryTable+" where "+i_cWhere+")";
          +",NICFACIT = (select NICFACIT from "+i_cQueryTable+" where "+i_cWhere+")";
          +",NICFAPRO = (select NICFAPRO from "+i_cQueryTable+" where "+i_cWhere+")";
          +",NICFALUT = (select NICFALUT from "+i_cQueryTable+" where "+i_cWhere+")";
          +",NINRGRA1 = (select NINRGRA1 from "+i_cQueryTable+" where "+i_cWhere+")";
          +",NISENAPP = (select NISENAPP from "+i_cQueryTable+" where "+i_cWhere+")";
          +",NIUFFEMI = (select NIUFFEMI from "+i_cQueryTable+" where "+i_cWhere+")";
          +",NIGIUDIC = (select NIGIUDIC from "+i_cQueryTable+" where "+i_cWhere+")";
          +",NIEPROPE = (select NIEPROPE from "+i_cQueryTable+" where "+i_cWhere+")";
          +",NIMENUMN = (select NIMENUMN from "+i_cQueryTable+" where "+i_cWhere+")";
          +",NIOPNUMR = (select NIOPNUMR from "+i_cQueryTable+" where "+i_cWhere+")";
          +",NIOPNUMD = (select NIOPNUMD from "+i_cQueryTable+" where "+i_cWhere+")";
          +",NIUFCACA = (select NIUFCACA from "+i_cQueryTable+" where "+i_cWhere+")";
          +",NINRGCAC = (select NINRGCAC from "+i_cQueryTable+" where "+i_cWhere+")";
          +",NIRGGRA1 = (select NIRGGRA1 from "+i_cQueryTable+" where "+i_cWhere+")";
          +",NINUMSEC = (select NINUMSEC from "+i_cQueryTable+" where "+i_cWhere+")";
          +",NIDEPCBP = (select NIDEPCBP from "+i_cQueryTable+" where "+i_cWhere+")";
          +",NICTUCOD = (select NICTUCOD from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Write into ISC_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.ISC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ISC_DETT_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="NISERIAL,CPROWNUM"
      do vq_exec with '..\PCON\EXE\QUERY\GSCVCC18_2',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ISC_DETT_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="ISC_DETT.NISERIAL = _t2.NISERIAL";
              +" and "+"ISC_DETT.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"NISOGDEN = _t2.NISOGDEN";
          +",NISOGNOM = _t2.NISOGNOM";
          +",NISOGCOG = _t2.NISOGCOG";
          +",NISOGLOC = _t2.NISOGLOC";
          +",NISOGPRO = _t2.NISOGPRO";
          +",NISOGIND = _t2.NISOGIND";
          +",NISOGCAP = _t2.NISOGCAP";
          +",NISOGCDF = _t2.NISOGCDF";
          +",NISOGLNA = _t2.NISOGLNA";
          +",NISOGNAZ = _t2.NISOGNAZ";
          +",NISOGEMA = _t2.NISOGEMA";
          +",NISOGDE2 = _t2.NISOGDE2";
          +i_ccchkf;
          +" from "+i_cTable+" ISC_DETT, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="ISC_DETT.NISERIAL = _t2.NISERIAL";
              +" and "+"ISC_DETT.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ISC_DETT, "+i_cQueryTable+" _t2 set ";
          +"ISC_DETT.NISOGDEN = _t2.NISOGDEN";
          +",ISC_DETT.NISOGNOM = _t2.NISOGNOM";
          +",ISC_DETT.NISOGCOG = _t2.NISOGCOG";
          +",ISC_DETT.NISOGLOC = _t2.NISOGLOC";
          +",ISC_DETT.NISOGPRO = _t2.NISOGPRO";
          +",ISC_DETT.NISOGIND = _t2.NISOGIND";
          +",ISC_DETT.NISOGCAP = _t2.NISOGCAP";
          +",ISC_DETT.NISOGCDF = _t2.NISOGCDF";
          +",ISC_DETT.NISOGLNA = _t2.NISOGLNA";
          +",ISC_DETT.NISOGNAZ = _t2.NISOGNAZ";
          +",ISC_DETT.NISOGEMA = _t2.NISOGEMA";
          +",ISC_DETT.NISOGDE2 = _t2.NISOGDE2";
          +Iif(Empty(i_ccchkf),"",",ISC_DETT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="ISC_DETT.NISERIAL = t2.NISERIAL";
              +" and "+"ISC_DETT.CPROWNUM = t2.CPROWNUM";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ISC_DETT set (";
          +"NISOGDEN,";
          +"NISOGNOM,";
          +"NISOGCOG,";
          +"NISOGLOC,";
          +"NISOGPRO,";
          +"NISOGIND,";
          +"NISOGCAP,";
          +"NISOGCDF,";
          +"NISOGLNA,";
          +"NISOGNAZ,";
          +"NISOGEMA,";
          +"NISOGDE2";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.NISOGDEN,";
          +"t2.NISOGNOM,";
          +"t2.NISOGCOG,";
          +"t2.NISOGLOC,";
          +"t2.NISOGPRO,";
          +"t2.NISOGIND,";
          +"t2.NISOGCAP,";
          +"t2.NISOGCDF,";
          +"t2.NISOGLNA,";
          +"t2.NISOGNAZ,";
          +"t2.NISOGEMA,";
          +"t2.NISOGDE2";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="ISC_DETT.NISERIAL = _t2.NISERIAL";
              +" and "+"ISC_DETT.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ISC_DETT set ";
          +"NISOGDEN = _t2.NISOGDEN";
          +",NISOGNOM = _t2.NISOGNOM";
          +",NISOGCOG = _t2.NISOGCOG";
          +",NISOGLOC = _t2.NISOGLOC";
          +",NISOGPRO = _t2.NISOGPRO";
          +",NISOGIND = _t2.NISOGIND";
          +",NISOGCAP = _t2.NISOGCAP";
          +",NISOGCDF = _t2.NISOGCDF";
          +",NISOGLNA = _t2.NISOGLNA";
          +",NISOGNAZ = _t2.NISOGNAZ";
          +",NISOGEMA = _t2.NISOGEMA";
          +",NISOGDE2 = _t2.NISOGDE2";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".NISERIAL = "+i_cQueryTable+".NISERIAL";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"NISOGDEN = (select NISOGDEN from "+i_cQueryTable+" where "+i_cWhere+")";
          +",NISOGNOM = (select NISOGNOM from "+i_cQueryTable+" where "+i_cWhere+")";
          +",NISOGCOG = (select NISOGCOG from "+i_cQueryTable+" where "+i_cWhere+")";
          +",NISOGLOC = (select NISOGLOC from "+i_cQueryTable+" where "+i_cWhere+")";
          +",NISOGPRO = (select NISOGPRO from "+i_cQueryTable+" where "+i_cWhere+")";
          +",NISOGIND = (select NISOGIND from "+i_cQueryTable+" where "+i_cWhere+")";
          +",NISOGCAP = (select NISOGCAP from "+i_cQueryTable+" where "+i_cWhere+")";
          +",NISOGCDF = (select NISOGCDF from "+i_cQueryTable+" where "+i_cWhere+")";
          +",NISOGLNA = (select NISOGLNA from "+i_cQueryTable+" where "+i_cWhere+")";
          +",NISOGNAZ = (select NISOGNAZ from "+i_cQueryTable+" where "+i_cWhere+")";
          +",NISOGEMA = (select NISOGEMA from "+i_cQueryTable+" where "+i_cWhere+")";
          +",NISOGDE2 = (select NISOGDE2 from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Write into ISC_AVVO
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.ISC_AVVO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ISC_AVVO_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="AASERIAL,AARIFPAD,CPROWNUM"
      do vq_exec with '..\PCON\EXE\QUERY\GSCVCC18_3',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ISC_AVVO_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="ISC_AVVO.AASERIAL = _t2.AASERIAL";
              +" and "+"ISC_AVVO.AARIFPAD = _t2.AARIFPAD";
              +" and "+"ISC_AVVO.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"AACOGNOM = _t2.AACOGNOM";
          +",AA__NOME = _t2.AA__NOME";
          +",AAINDIRIZ = _t2.AAINDIRIZ";
          +",AALOCALI = _t2.AALOCALI";
          +",AAPROVIN = _t2.AAPROVIN";
          +",AA___CAP = _t2.AA___CAP";
          +",AACODFIS = _t2.AACODFIS";
          +i_ccchkf;
          +" from "+i_cTable+" ISC_AVVO, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="ISC_AVVO.AASERIAL = _t2.AASERIAL";
              +" and "+"ISC_AVVO.AARIFPAD = _t2.AARIFPAD";
              +" and "+"ISC_AVVO.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ISC_AVVO, "+i_cQueryTable+" _t2 set ";
          +"ISC_AVVO.AACOGNOM = _t2.AACOGNOM";
          +",ISC_AVVO.AA__NOME = _t2.AA__NOME";
          +",ISC_AVVO.AAINDIRIZ = _t2.AAINDIRIZ";
          +",ISC_AVVO.AALOCALI = _t2.AALOCALI";
          +",ISC_AVVO.AAPROVIN = _t2.AAPROVIN";
          +",ISC_AVVO.AA___CAP = _t2.AA___CAP";
          +",ISC_AVVO.AACODFIS = _t2.AACODFIS";
          +Iif(Empty(i_ccchkf),"",",ISC_AVVO.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="ISC_AVVO.AASERIAL = t2.AASERIAL";
              +" and "+"ISC_AVVO.AARIFPAD = t2.AARIFPAD";
              +" and "+"ISC_AVVO.CPROWNUM = t2.CPROWNUM";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ISC_AVVO set (";
          +"AACOGNOM,";
          +"AA__NOME,";
          +"AAINDIRIZ,";
          +"AALOCALI,";
          +"AAPROVIN,";
          +"AA___CAP,";
          +"AACODFIS";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.AACOGNOM,";
          +"t2.AA__NOME,";
          +"t2.AAINDIRIZ,";
          +"t2.AALOCALI,";
          +"t2.AAPROVIN,";
          +"t2.AA___CAP,";
          +"t2.AACODFIS";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="ISC_AVVO.AASERIAL = _t2.AASERIAL";
              +" and "+"ISC_AVVO.AARIFPAD = _t2.AARIFPAD";
              +" and "+"ISC_AVVO.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ISC_AVVO set ";
          +"AACOGNOM = _t2.AACOGNOM";
          +",AA__NOME = _t2.AA__NOME";
          +",AAINDIRIZ = _t2.AAINDIRIZ";
          +",AALOCALI = _t2.AALOCALI";
          +",AAPROVIN = _t2.AAPROVIN";
          +",AA___CAP = _t2.AA___CAP";
          +",AACODFIS = _t2.AACODFIS";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".AASERIAL = "+i_cQueryTable+".AASERIAL";
              +" and "+i_cTable+".AARIFPAD = "+i_cQueryTable+".AARIFPAD";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"AACOGNOM = (select AACOGNOM from "+i_cQueryTable+" where "+i_cWhere+")";
          +",AA__NOME = (select AA__NOME from "+i_cQueryTable+" where "+i_cWhere+")";
          +",AAINDIRIZ = (select AAINDIRIZ from "+i_cQueryTable+" where "+i_cWhere+")";
          +",AALOCALI = (select AALOCALI from "+i_cQueryTable+" where "+i_cWhere+")";
          +",AAPROVIN = (select AAPROVIN from "+i_cQueryTable+" where "+i_cWhere+")";
          +",AA___CAP = (select AA___CAP from "+i_cQueryTable+" where "+i_cWhere+")";
          +",AACODFIS = (select AACODFIS from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Write into CONF_INT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.CONF_INT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONF_INT_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.CONF_INT_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"CITOOLMN ="+cp_NullLink(cp_ToStrODBC(" "),'CONF_INT','CITOOLMN');
          +i_ccchkf ;
      +" where ";
          +"CITOOLMN = "+cp_ToStrODBC("S");
             )
    else
      update (i_cTable) set;
          CITOOLMN = " ";
          &i_ccchkf. ;
       where;
          CITOOLMN = "S";

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    this.oParentObject.w_PESEOK = .T.
    this.w_TMPC = AH_MsgFormat("Aggiornamento nota di iscrizione a ruolo eseguito correttamente")
    this.oParentObject.w_PMSG = this.w_TMPC
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='ISC_MAST'
    this.cWorkTables[2]='ISC_DETT'
    this.cWorkTables[3]='ISC_AVVO'
    this.cWorkTables[4]='CONF_INT'
    return(this.OpenAllTables(4))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
