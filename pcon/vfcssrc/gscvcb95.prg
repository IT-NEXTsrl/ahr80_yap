* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcb95                                                        *
*              Aggiornamento gruppo predefinito nelle risorse                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-05-26                                                      *
* Last revis.: 2011-05-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcb95",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcb95 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  * --- WorkFile variables
  DIPENDEN_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiornamento gruppo predefinito nelle risorse
    * --- Try
    local bErr_04A283F8
    bErr_04A283F8=bTrsErr
    this.Try_04A283F8()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = "Errore nell'aggiornamento gruppo predefinito nelle risorse:" + Message()
      this.oParentObject.w_PESEOK = .F.
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      if this.w_NHF>=0
        this.w_TMPC = this.oParentObject.w_PMSG
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_04A283F8
    * --- End
  endproc
  proc Try_04A283F8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Write into DIPENDEN
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DIPENDEN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DIPENDEN_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="DPCODICE"
      do vq_exec with 'gscvcb95',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DIPENDEN_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="DIPENDEN.DPCODICE = _t2.DPCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"DPGRUPRE ="+cp_NullLink(cp_ToStrODBC("     "),'DIPENDEN','DPGRUPRE');
          +i_ccchkf;
          +" from "+i_cTable+" DIPENDEN, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="DIPENDEN.DPCODICE = _t2.DPCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DIPENDEN, "+i_cQueryTable+" _t2 set ";
      +"DIPENDEN.DPGRUPRE ="+cp_NullLink(cp_ToStrODBC("     "),'DIPENDEN','DPGRUPRE');
          +Iif(Empty(i_ccchkf),"",",DIPENDEN.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="DIPENDEN.DPCODICE = t2.DPCODICE";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DIPENDEN set (";
          +"DPGRUPRE";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +cp_NullLink(cp_ToStrODBC("     "),'DIPENDEN','DPGRUPRE')+"";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="DIPENDEN.DPCODICE = _t2.DPCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DIPENDEN set ";
      +"DPGRUPRE ="+cp_NullLink(cp_ToStrODBC("     "),'DIPENDEN','DPGRUPRE');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".DPCODICE = "+i_cQueryTable+".DPCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"DPGRUPRE ="+cp_NullLink(cp_ToStrODBC("     "),'DIPENDEN','DPGRUPRE');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    this.oParentObject.w_PESEOK = .T.
    this.w_TMPC = AH_MsgFormat("Aggiornamento gruppo predefinito nelle risorse")
    this.oParentObject.w_PMSG = this.w_TMPC
    * --- commit
    cp_EndTrs(.t.)
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='DIPENDEN'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
