* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvab41                                                        *
*              Crea categorie cespiti mancanti                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_38]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-11-11                                                      *
* Last revis.: 2006-11-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvab41",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvab41 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_MESS = space(100)
  w_CCDTOBSO = ctod("  /  /  ")
  w_NCAT = 0
  w_NCAU = 0
  w_NUBI = 0
  w_NFAM = 0
  w_CONN = 0
  * --- WorkFile variables
  CAT_CESP_idx=0
  CES_PITI_idx=0
  CAU_CESP_idx=0
  MOV_CESP_idx=0
  UBI_CESP_idx=0
  FAM_CESP_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questa procedura individua e riinserisce le categorie cespiti cancellate erroneamente 
    * --- FIle di LOG
    this.w_CCDTOBSO = CTOD("01-01-1950")
    * --- Try
    local bErr_04A9E400
    bErr_04A9E400=bTrsErr
    this.Try_04A9E400()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE %1 - impossibile inserire le categorie / causali cespiti mancanti", this.oParentObject.w_PMSG )
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_04A9E400
    * --- End
  endproc
  proc Try_04A9E400()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Insert into CAT_CESP
    i_nConn=i_TableProp[this.CAT_CESP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAT_CESP_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSCVAB41",this.CAT_CESP_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    this.w_NCAT = i_ROWS
    * --- Insert into CAU_CESP
    i_nConn=i_TableProp[this.CAU_CESP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAU_CESP_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSCVAB41_1",this.CAU_CESP_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    this.w_NCAU = i_ROWS
    * --- Insert into FAM_CESP
    i_nConn=i_TableProp[this.FAM_CESP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.FAM_CESP_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSCVAB41_2",this.FAM_CESP_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    this.w_NFAM = i_ROWS
    * --- Insert into UBI_CESP
    i_nConn=i_TableProp[this.UBI_CESP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.UBI_CESP_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSCVAB41_3",this.UBI_CESP_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    this.w_NUBI = i_ROWS
    pName="CES_PITI"
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = ah_Msgformat("Conversione eseguita correttamente, inseriti %1 categorie %2 causali %3 famiglie %4 ubicazioni", Alltrim(Str( this.w_NCAT )), Alltrim(Str( this.w_NCAU )), Alltrim(Str( this.w_NFAM )), Alltrim(Str( this.w_NUBI )) )
    * --- commit
    cp_EndTrs(.t.)
    this.w_CONN = i_TableProp[this.CES_PITI_idx,3]
    GSCV_BRT(this, "CES_PITI" , i_nConn , .T.)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_CONN = i_TableProp[this.MOV_CESP_idx,3]
    GSCV_BRT(this, "MOV_CESP" , i_nConn , .T.)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Lancia il report di controllo
    vx_exec("..\PCON\EXE\QUERY\GSCVAB41B.VQR, ..\PCON\EXE\QUERY\GSCVAB41B.FRX",this)
    if this.w_NHF>=0 
      this.w_TMPC = this.oParentObject.w_PMSG
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,6)]
    this.cWorkTables[1]='CAT_CESP'
    this.cWorkTables[2]='CES_PITI'
    this.cWorkTables[3]='CAU_CESP'
    this.cWorkTables[4]='MOV_CESP'
    this.cWorkTables[5]='UBI_CESP'
    this.cWorkTables[6]='FAM_CESP'
    return(this.OpenAllTables(6))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
