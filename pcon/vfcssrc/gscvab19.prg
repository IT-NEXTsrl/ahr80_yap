* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvab19                                                        *
*              Agg. flag causali di magazzino                                  *
*                                                                              *
*      Author: Zucchetti Tam Spa                                               *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_17]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-08-29                                                      *
* Last revis.: 2001-08-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvab19",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvab19 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_FLELGM = space(1)
  w_CMCODICE = space(5)
  w_CMAGGVAL = space(1)
  * --- WorkFile variables
  CAM_AGAZ_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- FIle di LOG
    this.w_CMAGGVAL = " "
    * --- Try
    local bErr_0360DAF0
    bErr_0360DAF0=bTrsErr
    this.Try_0360DAF0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE - impossibile aggiornare CMAGGVAL su tabella CAM_AGAZ")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_0360DAF0
    * --- End
    * --- Chiude cursore
    if USED("CAUSALI")
      SELECT CAUSALI
      USE
    endif
  endproc
  proc Try_0360DAF0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Aggiornamento  CMAGGVAL
    vq_exec("..\pcon\exe\query\gscvab19.vqr",this,"CAUSALI")
    if USED("CAUSALI")
      if Reccount ("CAUSALI")>0
        SELECT CAUSALI
        GO TOP
        SCAN
        this.w_CMCODICE = CAUSALI.CMCODICE
        * --- Write into CAM_AGAZ
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CAM_AGAZ_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CMAGGVAL ="+cp_NullLink(cp_ToStrODBC(this.w_CMAGGVAL),'CAM_AGAZ','CMAGGVAL');
              +i_ccchkf ;
          +" where ";
              +"CMCODICE = "+cp_ToStrODBC(this.w_CMCODICE);
                 )
        else
          update (i_cTable) set;
              CMAGGVAL = this.w_CMAGGVAL;
              &i_ccchkf. ;
           where;
              CMCODICE = this.w_CMCODICE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        SELECT CAUSALI
        ENDSCAN
        * --- commit
        cp_EndTrs(.t.)
        * --- Esecuzione ok
        if this.w_NHF>=0
          this.w_TMPC = ah_Msgformat("Aggiornamento campo CMAGGVAL su tabella CAM_AGAZ eseguito con successo")
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
        this.oParentObject.w_PESEOK = .T.
        this.oParentObject.w_PMSG = this.oParentObject.w_PMSG + this.w_TMPC 
      else
        * --- Gestisce log errori
        if this.w_NHF>=0
          this.w_TMPC = ah_Msgformat("ATTENZIONE: non esistono dati da convertire")
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
        * --- Esecuzione ok
        this.oParentObject.w_PESEOK = .T.
        this.oParentObject.w_PMSG = this.w_TMPC
      endif
    endif
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='CAM_AGAZ'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
