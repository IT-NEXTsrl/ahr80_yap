* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcb77                                                        *
*              Pulizia valori licenza                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-01-14                                                      *
* Last revis.: 2010-04-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcb77",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcb77 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TMPC = space(10)
  w_TEMP = space(10)
  * --- WorkFile variables
  ACTKEY_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Eliminazione valore di default (actkey)
    * --- Try
    local bErr_03015418
    bErr_03015418=bTrsErr
    this.Try_03015418()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = this.oParentObject.w_PMSG
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_03015418
    * --- End
  endproc
  proc Try_03015418()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Azzero vecchi addon
    * --- Try
    local bErr_030168B8
    bErr_030168B8=bTrsErr
    this.Try_030168B8()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_030168B8
    * --- End
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.w_TMPC = ah_Msgformat("Pulizia valori licenza avvenuta correttamente")
    this.oParentObject.w_PMSG = ah_Msgformat("Conversione eseguita correttamente")
    if this.w_NHF>=0
      this.w_TMPC = this.oParentObject.w_PMSG
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    return
  proc Try_030168B8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    w_nConn=i_TableProp[this.ACTKEY_idx,3] 
 w_dbtype=LOWER(SQLXGetDatabaseType(w_nConn))
    do case
      case w_dbtype="oracle"
        * --- Oracle
        cp_SqlExec(w_nConn, "ALTER TABLE ACTKEY MODIFY ( MOSCALIC DEFAULT NULL )")
      case w_dbtype="sqlserver"
        * --- Sql Server
        cp_SqlExec(w_nConn, "select 'ALTER TABLE ACTKEY DROP CONSTRAINT '+so.namefrom as fldcomm sysobjects so, syscolumns sc where sc.name = 'MOSCALIC' and sc.cdefault = so.id and so.name like '%ACTKEY%'", "curscommand")
        if used("curscommand")
          if not empty(curscommand.fldcomm)
            cp_SqlExec(w_nConn, curscommand.fldcomm)
          endif
          USE IN curscommand
        endif
      case w_dbtype="db2"
        * --- DB2
        cp_SqlExec(w_nConn, "ALTER TABLE ACTKEY ALTER COLUMN MOSCALIC DROP DEFAULT")
    endcase
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='ACTKEY'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
