* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvbb82                                                        *
*              AGGIORNAMENTO MVKEYSAL, MVCODMAG DOCUMENTI GENERATI DA OFFERTA  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-02-27                                                      *
* Last revis.: 2008-07-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvbb82",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvbb82 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_MESS = space(100)
  * --- WorkFile variables
  DOC_DETT_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questa procedura Aggiorna:
    *     Campo MVKEYSAL e MVCODMAG in DOC_DETT (Documenti detail) perch�
    *     deve essere valorizzato per gli articoli
    *     MVCODMAG viene messo a NULL per i servizi
    * --- FIle di LOG
    * --- Try
    local bErr_038B7A58
    bErr_038B7A58=bTrsErr
    this.Try_038B7A58()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = "ERRORE GENERICO - Impossibile aggiornare campo MVKEYSAL o MVCODMAG"
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_038B7A58
    * --- End
  endproc
  proc Try_038B7A58()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Esecuzione ok
    if g_OFFE="S"
      * --- Write into DOC_DETT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DOC_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="MVSERIAL,CPROWNUM,MVNUMRIF"
        do vq_exec with 'gscvbb82',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
                +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MVKEYSAL = _t2.MVCODART";
            +i_ccchkf;
            +" from "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
                +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 set ";
            +"DOC_DETT.MVKEYSAL = _t2.MVCODART";
            +Iif(Empty(i_ccchkf),"",",DOC_DETT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="DOC_DETT.MVSERIAL = t2.MVSERIAL";
                +" and "+"DOC_DETT.CPROWNUM = t2.CPROWNUM";
                +" and "+"DOC_DETT.MVNUMRIF = t2.MVNUMRIF";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set (";
            +"MVKEYSAL";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.MVCODART";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
                +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set ";
            +"MVKEYSAL = _t2.MVCODART";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
                +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
                +" and "+i_cTable+".MVNUMRIF = "+i_cQueryTable+".MVNUMRIF";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MVKEYSAL = (select MVCODART from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Write into DOC_DETT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DOC_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="MVSERIAL,CPROWNUM,MVNUMRIF"
        do vq_exec with 'gscvbb82_1',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
                +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MVCODMAG ="+cp_NullLink(cp_ToStrODBC(.NULL.),'DOC_DETT','MVCODMAG');
            +i_ccchkf;
            +" from "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
                +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 set ";
        +"DOC_DETT.MVCODMAG ="+cp_NullLink(cp_ToStrODBC(.NULL.),'DOC_DETT','MVCODMAG');
            +Iif(Empty(i_ccchkf),"",",DOC_DETT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="DOC_DETT.MVSERIAL = t2.MVSERIAL";
                +" and "+"DOC_DETT.CPROWNUM = t2.CPROWNUM";
                +" and "+"DOC_DETT.MVNUMRIF = t2.MVNUMRIF";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set (";
            +"MVCODMAG";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +cp_NullLink(cp_ToStrODBC(.NULL.),'DOC_DETT','MVCODMAG')+"";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
                +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set ";
        +"MVCODMAG ="+cp_NullLink(cp_ToStrODBC(.NULL.),'DOC_DETT','MVCODMAG');
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
                +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
                +" and "+i_cTable+".MVNUMRIF = "+i_cQueryTable+".MVNUMRIF";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MVCODMAG ="+cp_NullLink(cp_ToStrODBC(.NULL.),'DOC_DETT','MVCODMAG');
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      this.oParentObject.w_PESEOK = .T.
      this.oParentObject.w_PMSG = this.oParentObject.w_PMSG + chr(13)+"Aggiornamento campi MVKEYSAL e MVCODMAG effettuato con successo!"
    else
      this.oParentObject.w_PESEOK = .T.
      this.oParentObject.w_PMSG = this.oParentObject.w_PMSG + chr(13)+"Modulo offerte non attivo, conversione non necessaria"
    endif
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='DOC_DETT'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
