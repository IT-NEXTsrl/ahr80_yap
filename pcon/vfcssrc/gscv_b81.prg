* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscv_b81                                                        *
*              Aggiornamento chiave statisti                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_245]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-07-12                                                      *
* Last revis.: 2001-07-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscv_b81",oParentObject,m.w_NHF)
return(i_retval)

define class tgscv_b81 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  * --- WorkFile variables
  STATISTI_idx=0
  STATELAB_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- FIle di LOG
    * --- Try
    local bErr_03722370
    bErr_03722370=bTrsErr
    this.Try_03722370()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = this.oParentObject.w_PMSG
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_03722370
    * --- End
  endproc
  proc Try_03722370()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    i_nConn=i_TableProp[this.STATISTI_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.STATISTI_idx,2])
    if i_nConn<>0
      do case
        case upper(CP_DBTYPE)="SQLSERVER"
          * --- Caso SQLServer
          i_nConn=i_TableProp[this.STATISTI_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.STATISTI_idx,2])
          w_iRows = SQLExec(i_nConn,"ALTER TABLE "+i_cTable+; 
 " ALTER COLUMN STSERRIF char(10) NOT NULL")
          w_iRows = SQLExec(i_nConn,"ALTER TABLE "+i_cTable+; 
 " ALTER COLUMN STROWNUM smallint NOT NULL")
          w_iRows = SQLExec(i_nConn,"ALTER TABLE "+i_cTable+; 
 " ALTER COLUMN STNUMRIF smallint NOT NULL")
          w_iRows = SQLExec(i_nConn,"ALTER TABLE "+i_cTable+; 
 " ALTER COLUMN STROWNUM int NOT NULL")
          w_iRows = SQLExec(i_nConn,"ALTER TABLE "+i_cTable+; 
 " ALTER COLUMN STNUMRIF int NOT NULL")
          w_iRows = SQLExec(i_nConn,"ALTER TABLE xxxSTATISTI"+; 
 " ALTER COLUMN STSERRIF char(10) NOT NULL")
          w_iRows = SQLExec(i_nConn,"ALTER TABLE xxxSTATISTI"+; 
 " ALTER COLUMN STROWNUM smallint NOT NULL")
          w_iRows = SQLExec(i_nConn,"ALTER TABLE xxxSTATISTI"+; 
 " ALTER COLUMN STNUMRIF smallint NOT NULL")
          w_iRows = SQLExec(i_nConn,"ALTER TABLE xxxSTATISTI"+; 
 " ALTER COLUMN STROWNUM int NOT NULL")
          w_iRows = SQLExec(i_nConn,"ALTER TABLE xxxSTATISTI"+; 
 " ALTER COLUMN STNUMRIF int NOT NULL")
        case upper(CP_DBTYPE)="ORACLE"
          * --- Caso ORACLE
          i_nConn=i_TableProp[this.STATISTI_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.STATISTI_idx,2])
          w_iRows = SQLExec(i_nConn,"ALTER TABLE "+i_cTable+; 
 " MODIFY(STSERRIF NOT NULL)")
          w_iRows = SQLExec(i_nConn,"ALTER TABLE "+i_cTable+; 
 " MODIFY(STROWNUM NOT NULL)")
          w_iRows = SQLExec(i_nConn,"ALTER TABLE "+i_cTable+; 
 " MODIFY(STNUMRIF NOT NULL)")
          w_iRows = SQLExec(i_nConn,"ALTER TABLE xxxSTATISTI"+; 
 " MODIFY(STSERRIF NOT NULL)")
          w_iRows = SQLExec(i_nConn,"ALTER TABLE xxxSTATISTI"+; 
 " MODIFY(STROWNUM NOT NULL)")
          w_iRows = SQLExec(i_nConn,"ALTER TABLE xxxSTATISTI"+; 
 " MODIFY(STNUMRIF NOT NULL)")
        case upper(CP_DBTYPE)="DB2"
          * --- Caso DB2
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
      endcase
      if bTrsErr
        * --- Raise
        i_Error=MESSAGE()
        return
      else
        if this.w_NHF>=0
          if upper(CP_DBTYPE)="DB2"
            this.w_TMPC = ah_Msgformat("Elaborazione terminata con successo")
          else
            this.w_TMPC = ah_Msgformat("Adeguamento chiave primaria tabella STATISTI eseguito correttamente%0ATTENZIONE: occorre andare in MANUTENZIONE DATABASE, selezionare la tabella%0STATISTI e premere <OK>")
          endif
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    if upper(CP_DBTYPE)="DB2"
      this.oParentObject.w_PMSG = ah_Msgformat("Elaborazione terminata con successo")
    else
      this.w_TMPC = ah_Msgformat("Adeguamento chiave primaria tabella STATISTI eseguito correttamente%0ATTENZIONE: occorre andare in MANUTENZIONE DATABASE, selezionare la tabella%0STATISTI e premere <OK>")
      this.oParentObject.w_PMSG = this.w_TMPC
    endif
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella DB2
    * --- Create temporary table STATELAB
    i_nIdx=cp_AddTableDef('STATELAB') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.STATISTI_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.STATISTI_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
          )
    this.STATELAB_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    * --- Elimino la tabella
    if Not GSCV_BDT( this, "STATISTI" , i_nConn )
      * --- Raise
      i_Error="Error"
      return
    endif
    * --- Ricostruisco il database per la tabella
    GSCV_BRT(this, "STATISTI" , i_nConn , .T.)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Select from STATELAB
    i_nConn=i_TableProp[this.STATELAB_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STATELAB_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" STATELAB ";
           ,"_Curs_STATELAB")
    else
      select * from (i_cTable);
        into cursor _Curs_STATELAB
    endif
    if used('_Curs_STATELAB')
      select _Curs_STATELAB
      locate for 1=1
      do while not(eof())
      * --- Try
      local bErr_035FD750
      bErr_035FD750=bTrsErr
      this.Try_035FD750()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_035FD750
      * --- End
        select _Curs_STATELAB
        continue
      enddo
      use
    endif
    ah_Msg("Ripristinati valori di origine",.T.)
    * --- Elimina tabella TMP
    * --- Drop temporary table STATELAB
    i_nIdx=cp_GetTableDefIdx('STATELAB')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('STATELAB')
    endif
    ah_Msg("Eliminata tabella temporanea tmp",.T.)
  endproc
  proc Try_035FD750()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into STATISTI
    i_nConn=i_TableProp[this.STATISTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STATISTI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.STATISTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"STSERIAL"+",STSERRIF"+",STROWNUM"+",STNUMRIF"+",STDATREG"+",STDATDOC"+",STCATCOM"+",STFLOMAG"+",STCODCAU"+",STCODMAG"+",STFLCASC"+",STFLORDI"+",STFLIMPE"+",STFLRISE"+",STTIPCON"+",STCODCON"+",STCODAGE"+",STCODZON"+",STGRUMER"+",STCODFAM"+",STCATOMO"+",STCATCON"+",STCODMAR"+",STCODART"+",STCAUDOC"+",STQTAMOV"+",STUNIMIS"+",STQTAUM1"+",STUMPRIN"+",STIMPLOR"+",STIMPNET"+",STIMPACC"+",STCODVAL"+",STCAOVAL"+",STCODESE"+",STVALNAZ"+",STCODDES"+",STVOCCEN"+",STCODCEN"+",STCODATT"+",STCODCOM"+",STCODAG2"+",STCODPAG"+",STCODORN"+",STCODVET"+",STPROVIN"+",STCONSUP"+",STCODFOR"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(_Curs_STATELAB.STSERIAL),'STATISTI','STSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_STATELAB.STSERRIF),'STATISTI','STSERRIF');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_STATELAB.STROWNUM),'STATISTI','STROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_STATELAB.STNUMRIF),'STATISTI','STNUMRIF');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_STATELAB.STDATREG),'STATISTI','STDATREG');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_STATELAB.STDATDOC),'STATISTI','STDATDOC');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_STATELAB.STCATCOM),'STATISTI','STCATCOM');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_STATELAB.STFLOMAG),'STATISTI','STFLOMAG');
      +","+cp_NullLink(cp_ToStrODBC(w_STCODCAU),'STATISTI','STCODCAU');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_STATELAB.STCODMAG),'STATISTI','STCODMAG');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_STATELAB.STFLCASC),'STATISTI','STFLCASC');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_STATELAB.STFLORDI),'STATISTI','STFLORDI');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_STATELAB.STFLIMPE),'STATISTI','STFLIMPE');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_STATELAB.STFLRISE),'STATISTI','STFLRISE');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_STATELAB.STTIPCON),'STATISTI','STTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_STATELAB.STCODCON),'STATISTI','STCODCON');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_STATELAB.STCODAGE),'STATISTI','STCODAGE');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_STATELAB.STCODZON),'STATISTI','STCODZON');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_STATELAB.STGRUMER),'STATISTI','STGRUMER');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_STATELAB.STCODFAM),'STATISTI','STCODFAM');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_STATELAB.STCATOMO),'STATISTI','STCATOMO');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_STATELAB.STCATCON),'STATISTI','STCATCON');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_STATELAB.STCODMAR),'STATISTI','STCODMAR');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_STATELAB.STCODART),'STATISTI','STCODART');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_STATELAB.STCAUDOC),'STATISTI','STCAUDOC');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_STATELAB.STQTAMOV),'STATISTI','STQTAMOV');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_STATELAB.STUNIMIS),'STATISTI','STUNIMIS');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_STATELAB.STQTAUM1),'STATISTI','STQTAUM1');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_STATELAB.STUMPRIN),'STATISTI','STUMPRIN');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_STATELAB.STIMPLOR),'STATISTI','STIMPLOR');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_STATELAB.STIMPNET),'STATISTI','STIMPNET');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_STATELAB.STIMPACC),'STATISTI','STIMPACC');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_STATELAB.STCODVAL),'STATISTI','STCODVAL');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_STATELAB.STCAOVAL),'STATISTI','STCAOVAL');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_STATELAB.STCODESE),'STATISTI','STCODESE');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_STATELAB.STVALNAZ),'STATISTI','STVALNAZ');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_STATELAB.STCODDES),'STATISTI','STCODDES');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_STATELAB.STVOCCEN),'STATISTI','STVOCCEN');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_STATELAB.STCODCEN),'STATISTI','STCODCEN');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_STATELAB.STCODATT),'STATISTI','STCODATT');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_STATELAB.STCODCOM),'STATISTI','STCODCOM');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_STATELAB.STCODAG2),'STATISTI','STCODAG2');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_STATELAB.STCODPAG),'STATISTI','STCODPAG');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_STATELAB.STCODORN),'STATISTI','STCODORN');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_STATELAB.STCODVET),'STATISTI','STCODVET');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_STATELAB.STPROVIN),'STATISTI','STPROVIN');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_STATELAB.STCONSUP),'STATISTI','STCONSUP');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_STATELAB.STCODFOR),'STATISTI','STCODFOR');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'STSERIAL',_Curs_STATELAB.STSERIAL,'STSERRIF',_Curs_STATELAB.STSERRIF,'STROWNUM',_Curs_STATELAB.STROWNUM,'STNUMRIF',_Curs_STATELAB.STNUMRIF,'STDATREG',_Curs_STATELAB.STDATREG,'STDATDOC',_Curs_STATELAB.STDATDOC,'STCATCOM',_Curs_STATELAB.STCATCOM,'STFLOMAG',_Curs_STATELAB.STFLOMAG,'STCODCAU',w_STCODCAU,'STCODMAG',_Curs_STATELAB.STCODMAG,'STFLCASC',_Curs_STATELAB.STFLCASC,'STFLORDI',_Curs_STATELAB.STFLORDI)
      insert into (i_cTable) (STSERIAL,STSERRIF,STROWNUM,STNUMRIF,STDATREG,STDATDOC,STCATCOM,STFLOMAG,STCODCAU,STCODMAG,STFLCASC,STFLORDI,STFLIMPE,STFLRISE,STTIPCON,STCODCON,STCODAGE,STCODZON,STGRUMER,STCODFAM,STCATOMO,STCATCON,STCODMAR,STCODART,STCAUDOC,STQTAMOV,STUNIMIS,STQTAUM1,STUMPRIN,STIMPLOR,STIMPNET,STIMPACC,STCODVAL,STCAOVAL,STCODESE,STVALNAZ,STCODDES,STVOCCEN,STCODCEN,STCODATT,STCODCOM,STCODAG2,STCODPAG,STCODORN,STCODVET,STPROVIN,STCONSUP,STCODFOR &i_ccchkf. );
         values (;
           _Curs_STATELAB.STSERIAL;
           ,_Curs_STATELAB.STSERRIF;
           ,_Curs_STATELAB.STROWNUM;
           ,_Curs_STATELAB.STNUMRIF;
           ,_Curs_STATELAB.STDATREG;
           ,_Curs_STATELAB.STDATDOC;
           ,_Curs_STATELAB.STCATCOM;
           ,_Curs_STATELAB.STFLOMAG;
           ,w_STCODCAU;
           ,_Curs_STATELAB.STCODMAG;
           ,_Curs_STATELAB.STFLCASC;
           ,_Curs_STATELAB.STFLORDI;
           ,_Curs_STATELAB.STFLIMPE;
           ,_Curs_STATELAB.STFLRISE;
           ,_Curs_STATELAB.STTIPCON;
           ,_Curs_STATELAB.STCODCON;
           ,_Curs_STATELAB.STCODAGE;
           ,_Curs_STATELAB.STCODZON;
           ,_Curs_STATELAB.STGRUMER;
           ,_Curs_STATELAB.STCODFAM;
           ,_Curs_STATELAB.STCATOMO;
           ,_Curs_STATELAB.STCATCON;
           ,_Curs_STATELAB.STCODMAR;
           ,_Curs_STATELAB.STCODART;
           ,_Curs_STATELAB.STCAUDOC;
           ,_Curs_STATELAB.STQTAMOV;
           ,_Curs_STATELAB.STUNIMIS;
           ,_Curs_STATELAB.STQTAUM1;
           ,_Curs_STATELAB.STUMPRIN;
           ,_Curs_STATELAB.STIMPLOR;
           ,_Curs_STATELAB.STIMPNET;
           ,_Curs_STATELAB.STIMPACC;
           ,_Curs_STATELAB.STCODVAL;
           ,_Curs_STATELAB.STCAOVAL;
           ,_Curs_STATELAB.STCODESE;
           ,_Curs_STATELAB.STVALNAZ;
           ,_Curs_STATELAB.STCODDES;
           ,_Curs_STATELAB.STVOCCEN;
           ,_Curs_STATELAB.STCODCEN;
           ,_Curs_STATELAB.STCODATT;
           ,_Curs_STATELAB.STCODCOM;
           ,_Curs_STATELAB.STCODAG2;
           ,_Curs_STATELAB.STCODPAG;
           ,_Curs_STATELAB.STCODORN;
           ,_Curs_STATELAB.STCODVET;
           ,_Curs_STATELAB.STPROVIN;
           ,_Curs_STATELAB.STCONSUP;
           ,_Curs_STATELAB.STCODFOR;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Insert Record in Archivio Statistico Fallito'
      return
    endif
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='STATISTI'
    this.cWorkTables[2]='*STATELAB'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_STATELAB')
      use in _Curs_STATELAB
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
