* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscv_bgn                                                        *
*              Controllo allegati - iniezione menu                             *
*                                                                              *
*      Author: Zucchetti - AT                                                  *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-06-20                                                      *
* Last revis.: 2007-09-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPATHVMN,pKEYPRG,pNEWPRG,pNEWDES,pSUBMEN,pMODULO,pAGGANCIO
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscv_bgn",oParentObject,m.pPATHVMN,m.pKEYPRG,m.pNEWPRG,m.pNEWDES,m.pSUBMEN,m.pMODULO,m.pAGGANCIO)
return(i_retval)

define class tgscv_bgn as StdBatch
  * --- Local variables
  pPATHVMN = space(100)
  pKEYPRG = space(10)
  pNEWPRG = space(10)
  pNEWDES = space(30)
  pSUBMEN = space(30)
  pMODULO = space(20)
  pAGGANCIO = space(40)
  w_ORIMENU = space(10)
  w_NEWMENU = space(10)
  w_LEFTMENU = space(10)
  w_RESTOLEFT = space(10)
  w_TOTVOCISTR = space(5)
  w_RIGHTMENU = space(10)
  w_INIETVMN = space(10)
  w_INDEX = 0
  w_APPLEFT = space(10)
  w_APPRIGHT = space(10)
  w_RESTO = space(10)
  w_PROGSTR = space(10)
  w_PROGVMN = space(10)
  w_TERMINA = .f.
  w_NUM = 0
  w_PADRE = space(10)
  w_PADRESTR = space(10)
  w_APPVMN = space(10)
  w_LIVELLOSTR = space(10)
  w_INIET0 = space(4)
  w_INIET1 = space(4)
  w_INIET2 = space(4)
  w_INIET3 = space(4)
  w_INIET4 = space(4)
  w_INIETMOD = space(10)
  w_AGGANCIO = space(40)
  w_BLANK = space(3)
  w_PADRESTR = space(10)
  * --- WorkFile variables
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- La routine gestisce l'iniezione  della voce di menu 
    *     Controllo allegati Cli.\For. dal custom.vmn e tutti  men� custom per utente e gruppi
    *     Routine chiamante GSCV_BGM
    *     ---
    *     Se presente Programma Chiave la voce viene aggiunta sotto senza path di aggancio
    *     altrimenti la voce viene agganciata con il path passato come parametro (g_MENUSEP=CHR(172))
    *     --
    * --- Tipo operazione 
    *     Men� di origine con path
    *     Programma chiave per inserimento (non deve essere valorizzato l'aggancio)
    *     Programma da aggiungere
    *     Descrizione Voce di Menu
    *     Prima devo inserire il submenu
    *     Voce condizionata es:"(g_MAGA=*S*)"
    *     Path di aggancio
    * --- Men� origine
    * --- Nuovo menu
    * --- Parte iniziale - sinistra
    * --- Numero che indica il totale delle voci
    * --- Parte finale - destra
    * --- Parte da iniettare
    * --- Gestione aggiornamrnto progressivo voci
    * --- Variabili di appoggio nel caso di iniezione SUBMENU
    * --- Carico menu in una stringa per poterlo scomporre
    this.w_ORIMENU = FILETOSTR(this.pPATHVMN)
    if AT(this.pNEWPRG,this.w_ORIMENU) >0
      * --- Voce gi� presente
      i_retcode = 'stop'
      return
    endif
    * --- Occorre aggiornare la stringa della data
    this.w_ORIMENU = SUBSTR(this.w_ORIMENU, 1, AT(CHR(13)+CHR(10), this.w_ORIMENU, 24)+2) + STRTRAN(ALLTRIM(TTOC(datetime(),3)),"T"," ") +'"'+ SUBSTR(this.w_ORIMENU, AT(CHR(13)+CHR(10),this.w_ORIMENU,25))
    do case
      case !Empty(this.pKEYPRG)
        * --- E' presente il programma chiave di ricerca, in questo caso non viene effettuato l'aggancio 
        *     ma l'aggiunta della voce di menu sotto quella indicata (come per le build pre 53)
        * --- Ricerca programma (indica la posizione del primo carattere del nome programma chiave di ricerca)
        *     Memorizzo il livello da riportare sulla voce aggiunta oppure se si aggiunge un nuovo submenu 
        *     questo deve esere incrementato
        this.w_INDEX = AT(this.pKEYPRG,this.w_ORIMENU)
        this.w_LIVELLOSTR = SUBSTR(this.w_ORIMENU, AT(this.pKEYPRG,this.w_ORIMENU)-5, 2)
        * --- Incremento del progressivo iniziale (indica il TOTALE)
        * --- Aggiornamento numero presente all'inizio che indica il totale delle voci
        this.w_TOTVOCISTR = SUBSTR(this.w_ORIMENU, AT(CHR(13)+CHR(10), this.w_ORIMENU, 6)+2,(AT(CHR(13)+CHR(10), this.w_ORIMENU, 7)-AT(CHR(13)+CHR(10), this.w_ORIMENU, 6)-2) )
        * --- Incremento il totale delle voci 
        if Not Empty(this.pSUBMEN)
          this.w_TOTVOCISTR = " "+alltrim(str(VAL(alltrim(this.w_TOTVOCISTR))+2))
        else
          this.w_TOTVOCISTR = " "+alltrim(str(VAL(alltrim(this.w_TOTVOCISTR))+1))
        endif
        this.w_RESTOLEFT = SUBSTR(this.w_ORIMENU, AT(CHR(13)+CHR(10), this.w_ORIMENU, 7),this.w_INDEX - AT(CHR(13)+CHR(10),this.w_ORIMENU,7) +1)
        * --- Stringhe di appoggio per definire correttamente le parti che compongono il menu
        this.w_APPLEFT = SUBSTR(this.w_ORIMENU, 1, AT(CHR(13)+CHR(10), this.w_ORIMENU, 6)+1) + this.w_TOTVOCISTR + this.w_RESTOLEFT
        * --- Parte destra di appoggio
        this.w_APPRIGHT = SUBSTR(this.w_ORIMENU, this.w_INDEX +1)
        * --- CREAZIONE PARTE SINISTRA DEL MENU
        *     Creata la parte sinistra del men�, siamo pronti per INIETTARE LA NUOVA VOCE
        *     Il resto contiene la parte dalla voce di menu chiave di ricerca al punto in cui occorre inserire la descrizione
        this.w_RESTO = SUBSTR(this.w_APPRIGHT, 1, AT(CHR(13)+CHR(10), this.w_APPRIGHT, 15)+1)
        this.w_LEFTMENU = this.w_APPLEFT + this.w_RESTO
        * --- Si definisce la nuova parte da INIETTARE
        * --- -------------------------------------------------------
        * --- Occorre inserire un eventuale voce di submenu
        ah_msg( "Inserimento nuova voce di men� in corso..")
        if Not Empty(this.pSUBMEN)
          * --- Iniezione submenu
          this.w_INIETVMN = '"'+Alltrim(this.pSUBMEN)+'"'+CHR(13)+CHR(10)+ " "+alltrim(str(VAL(alltrim(this.w_LIVELLOSTR)))) +CHR(13)+CHR(10)+'"'+ SUBSTR(this.w_RESTO,LEN(alltrim(this.pKEYPRG)))
          * --- Costruisco i vari pezzi del submenu
          this.w_INIET0 = SUBSTR(this.w_INIETVMN, 1, AT(CHR(13) + CHR(10),this.w_INIETVMN,3) + 1)
          this.w_INIET1 = SUBSTR(this.w_INIETVMN, AT(CHR(13) + CHR(10),this.w_INIETVMN,3) + 2, AT(CHR(13) + CHR(10),this.w_INIETVMN, 4) - AT(CHR(13) + CHR(10),this.w_INIETVMN, 3)-2)
          this.w_INIET2 = SUBSTR(this.w_INIETVMN, AT(CHR(13) + CHR(10),this.w_INIETVMN, 4) +2, AT(CHR(13) + CHR(10),this.w_INIETVMN, 8) - AT(CHR(13) + CHR(10),this.w_INIETVMN, 4))
          this.w_INIET3 = SUBSTR(this.w_INIETVMN, AT(CHR(13) + CHR(10),this.w_INIETVMN, 8) + 2, AT(CHR(13) + CHR(10),this.w_INIETVMN, 9) - AT(CHR(13) + CHR(10),this.w_INIETVMN, 8)-2)
          this.w_INIET4 = CHR(13)+CHR(10) +'"'+"DIRCL.BMP" +'"'+ SUBSTR(this.w_INIETVMN, AT(CHR(13) + CHR(10),this.w_INIETVMN, 10))
          * --- Aggiorno il submenu con i nuovi progressivi
          this.w_INIETVMN = + this.w_INIET0 + " "+ alltrim(str(VAL(alltrim(this.w_INIET1))+1)) + CHR(13)+CHR(10) + this.w_INIET2 + " "+ alltrim(str(VAL(alltrim(this.w_INIET3))+1)) +this.w_INIET4
          * --- Il submenu � condizionato
          if !Empty(this.pMODULO)
            this.w_INIET0 = SUBSTR(this.w_INIETVMN, 1, AT(CHR(13) + CHR(10),this.w_INIETVMN,11) + 1)
            this.w_INIET1 = SUBSTR(this.w_INIETVMN, AT(CHR(13) + CHR(10),this.w_INIETVMN, 12) )
            this.w_INIETVMN = this.w_INIET0 +'"'+alltrim(this.pMODULO) + '"'+ this.w_INIET1
          endif
          * --- Lo aggiungo al menu di sinistra
          this.w_LEFTMENU = this.w_LEFTMENU+this.w_INIETVMN
          * --- Iniezione voce  (livello incrementato di uno)
          this.w_INIETVMN = '"'+Alltrim(this.pNEWDES)+'"'+CHR(13)+CHR(10)+ " "+alltrim(str(VAL(alltrim(this.w_LIVELLOSTR))+1)) +CHR(13)+CHR(10)+'"'+alltrim(this.pNEWPRG)+ SUBSTR(this.w_RESTO,LEN(alltrim(this.pKEYPRG)))
          * --- Occorre incrementare il puntatore al padre
          this.w_INIET0 = SUBSTR(this.w_INIETVMN, 1, AT(CHR(13) + CHR(10),this.w_INIETVMN,4) + 1)
          this.w_INIET1 = SUBSTR(this.w_INIETVMN, AT(CHR(13) + CHR(10),this.w_INIETVMN, 5))
          this.w_INIETVMN = this.w_INIET0 + " "+ alltrim(str(VAL(alltrim(this.w_INIET3))+1)) +this.w_INIET1
        else
          * --- Non � presente il submenu come parametro
          this.w_INIETVMN = '"'+Alltrim(this.pNEWDES)+'"'+CHR(13)+CHR(10)+ " "+alltrim(str(VAL(alltrim(this.w_LIVELLOSTR)))) +CHR(13)+CHR(10)+'"'+alltrim(this.pNEWPRG)+ SUBSTR(this.w_RESTO,LEN(alltrim(this.pKEYPRG)))
          * --- La voce � condizionata
          if !Empty(this.pMODULO)
            this.w_INIET0 = SUBSTR(this.w_INIETVMN, 1, AT(CHR(13) + CHR(10),this.w_INIETVMN,11) + 1)
            this.w_INIET1 = SUBSTR(this.w_INIETVMN, AT(CHR(13) + CHR(10),this.w_INIETVMN, 12) )
            this.w_INIETVMN = this.w_INIET0 +'"'+alltrim(this.pMODULO) + '"'+ this.w_INIET1
          endif
        endif
        * --- --------------------------------------------------------
        * --- Creo la parte destra del menu (� temporanea contiene tutti i progressivi DA AGGIORNARE)
        *     quella nuova sar� costruita a partire da questa sul ciclo while successivo
        this.w_APPRIGHT = this.w_INIETVMN + SUBSTR(this.w_APPRIGHT, AT(CHR(13)+CHR(10), this.w_APPRIGHT, 15)+2 )
        * --- Incremento indice - numero (SULLA PARTE DESTRA) e creazione parte destra definitiva
        ah_msg( "Ricostruzione men�   %1 in corso",.t.,.f.,.f.,ALLTRIM(this.pPATHVMN))
        this.w_TERMINA = .f.
        this.w_NUM = 0
        * --- Men� di destra definitivo (blocco di partenza)
        this.w_RIGHTMENU = SUBSTR(this.w_APPRIGHT, 1, AT(CHR(13)+CHR(10), this.w_APPRIGHT, 8)+1)
        * --- Lettura Riferimento al Men� Padre 
        this.w_PADRE = SUBSTR(this.w_APPRIGHT, AT(CHR(13)+CHR(10), this.w_APPRIGHT, 4)+2, (AT(CHR(13)+CHR(10), this.w_APPRIGHT, 5)-AT(CHR(13)+CHR(10), this.w_APPRIGHT, 4)-2) )
        do while !this.w_TERMINA
          * --- Lettura progressivo DA AGGIORNARE
          this.w_PROGSTR = SUBSTR(this.w_APPRIGHT, AT(CHR(13)+CHR(10), this.w_APPRIGHT, 8+this.w_NUM)+2, (AT(CHR(13)+CHR(10), this.w_APPRIGHT, 9+this.w_NUM)-AT(CHR(13)+CHR(10), this.w_APPRIGHT, 8+this.w_NUM)-2) )
          * --- Incremento del progressivo 
          if Not Empty(this.pSUBMEN)
            this.w_PROGSTR = " "+alltrim(STR(VAL(alltrim(this.w_PROGSTR))+2))
          else
            this.w_PROGSTR = " "+alltrim(STR(VAL(alltrim(this.w_PROGSTR))+1))
          endif
          * --- Sottostringa (dal carattere dopo il  progressivo fino al termine)
          this.w_PROGVMN = SUBSTR(this.w_APPRIGHT, AT(CHR(13)+CHR(10), this.w_APPRIGHT, 9+this.w_NUM))
          this.w_TERMINA = AT(CHR(13)+CHR(10), this.w_PROGVMN, 17) = 0
          if !this.w_TERMINA
            this.w_NUM = this.w_NUM+ 17
            * --- ------------PRIMA DI PASSARE AL PROSSIMO PROGRESSIVO OCCORRE CONTROLLARE IL PROSSIMO PUNTATORE AL MENU PADRE CHE LO PRECEDE-----------
            * --- Voce di menu che NON ha lo stesso padre della voce di menu inserita
            *     avra sicuramente un padre con il proprio progressivo incrementato, devo quindi 
            *     incrementare anche il riferimento al padre (al primo giro coincideranno sicuramente)
            this.w_PADRESTR = SUBSTR(this.w_APPRIGHT, AT(CHR(13)+CHR(10), this.w_APPRIGHT, 4+this.w_NUM)+2, (AT(CHR(13)+CHR(10), this.w_APPRIGHT, 5+this.w_NUM)-AT(CHR(13)+CHR(10), this.w_APPRIGHT, 4+this.w_NUM)-2) )
            if val(this.w_PADRE) < val(this.w_PADRESTR)
              * --- Il menu di destra � definito come
              *     menu di destra creato + nuovo progressivo
              this.w_RIGHTMENU = this.w_RIGHTMENU + this.w_PROGSTR
              * --- ADESSO SIAMO POSIZIONATI DOPO IL PROGRESSIVO
              * --- APPVMN=Comprende la parte dal carattere successivo del progressivo appena aggiornato
              *     fino al carattere precedente al riferimento menu padre (in questo ramo sicuramente da aggiornare)
              this.w_APPVMN = SUBSTR(this.w_PROGVMN, 1, AT(CHR(13)+CHR(10), this.w_PROGVMN, 13)+1)
              * --- Sottostringa (dal carattere dopo il  riferimento al padre fino al termine)
              this.w_PROGVMN = SUBSTR(this.w_APPRIGHT, AT(CHR(13)+CHR(10), this.w_APPRIGHT, 5+this.w_NUM))
              * --- Incremento del progressivo (questo � il riferimento al padre)
              if Not Empty(this.pSUBMEN)
                this.w_PROGSTR = " "+alltrim(STR(VAL(alltrim(this.w_PADRESTR))+2))
              else
                this.w_PROGSTR = " "+alltrim(STR(VAL(alltrim(this.w_PADRESTR))+1))
              endif
              * --- Aggiorno il menu di destra con il nuovo riferimento al padre
              this.w_RIGHTMENU = this.w_RIGHTMENU + this.w_APPVMN + this.w_PROGSTR + SUBSTR(this.w_PROGVMN, 1, AT(CHR(13)+CHR(10), this.w_PROGVMN, 4)+1)
              * --- ADESSO SIAMO POSIZIONATI PRIMA DEL PROSSIMO PROGRESSIVO 
            else
              * --- Il menu di destra � definito come
              *     menu di destra creato + nuovo progressivo + sottostringa fino al carattere prima del nuovo progressivo da aggiornare
              this.w_RIGHTMENU = this.w_RIGHTMENU + this.w_PROGSTR + SUBSTR(this.w_PROGVMN, 1, AT(CHR(13)+CHR(10), this.w_PROGVMN, 17)+1)
              * --- ADESSO SIAMO POSIZIONATI PRIMA DEL PROSSIMO PROGRESSIVO 
            endif
          else
            * --- L'ultimo pezzettino � sicuramente il solo w_PROGVMN
            this.w_RIGHTMENU = this.w_RIGHTMENU +this.w_PROGSTR + this.w_PROGVMN
          endif
        enddo
        * --- Composizione menu finale
        this.w_NEWMENU = this.w_LEFTMENU+this.w_RIGHTMENU
        this.w_NUM = STRTOFILE(this.w_NEWMENU,this.pPATHVMN)
      case !Empty(this.pAGGANCIO)
        * --- E' presente l'aggancio 
        * --- Voce di aggancio
        this.w_AGGANCIO = "@"+alltrim(this.pAGGANCIO) + "���T"
        * --- Totale voci
        this.w_TOTVOCISTR = SUBSTR(this.w_ORIMENU, AT(CHR(13)+CHR(10), this.w_ORIMENU, 6)+2,(AT(CHR(13)+CHR(10), this.w_ORIMENU, 7)-AT(CHR(13)+CHR(10), this.w_ORIMENU, 6)-2) )
        * --- Incremento del progressivo iniziale (indica il TOTALE)
        if Not Empty(this.pSUBMEN)
          this.w_TOTVOCISTR = " "+alltrim(str(VAL(alltrim(this.w_TOTVOCISTR))+2))
        else
          this.w_TOTVOCISTR = " "+alltrim(str(VAL(alltrim(this.w_TOTVOCISTR))+1))
        endif
        * --- Elimino l'ultima *
        this.w_ORIMENU = SUBSTR(this.w_ORIMENU,1, LEN(this.w_ORIMENU)-3)
        * --- Aggiorno il totale voci
        this.w_ORIMENU = SUBSTR(this.w_ORIMENU, 1, AT(CHR(13)+CHR(10), this.w_ORIMENU, 6)+1) + this.w_TOTVOCISTR + SUBSTR(this.w_ORIMENU, AT(CHR(13)+CHR(10),this.w_ORIMENU,7))
        * --- Si definisce la nuova parte da INIETTARE
        * --- -------------------------------------------------------
        * --- Occorre inserire un eventuale voce di submenu
        ah_msg( "Ricostruzione men� %1 con nuova voce in corso",.t.,.f.,.f.,ALLTRIM(this.pPATHVMN))
        this.w_INIET0 = CHR(13) + CHR(10)
        this.w_BLANK = '""'
        if Not Empty(this.pSUBMEN)
          * --- Iniezione submenu (sempre livello 1, padre Menu principale)
          this.w_INIET1 = this.w_INIET0+" 2" + this.w_INIET0+ this.w_BLANK+this.w_INIET0+ " 2" +this.w_INIET0+" 0"+this.w_INIET0+ " 1"+ this.w_INIET0+'"'+"_24B0RWQL1"+'"'+ this.w_INIET0+".t."+this.w_INIET0 
          this.w_INIET2 = this.w_INIET0+'"'+"DIRCL.BMP" +'"'+this.w_INIET0+this.w_BLANK+this.w_INIET0+this.w_BLANK+this.w_INIET0
          this.w_INIET3 = this.w_INIET0+this.w_BLANK+this.w_INIET0+this.w_BLANK+this.w_INIET0+this.w_BLANK+this.w_INIET0
          this.w_INIETVMN = '"'+Alltrim(this.pSUBMEN)+'"'+ this.w_INIET1 + " "+alltrim(str(VAL(alltrim(this.w_TOTVOCISTR))-2)) +this.w_INIET2 +'"'+Alltrim(this.w_AGGANCIO)+'"'+this.w_INIET3 +"*"+this.w_INIET0
          * --- Il submenu � condizionato
          if !Empty(this.pMODULO)
            this.w_INIET0 = SUBSTR(this.w_INIETVMN, 1, AT(CHR(13) + CHR(10),this.w_INIETVMN,11) + 1)
            this.w_INIET1 = SUBSTR(this.w_INIETVMN, AT(CHR(13) + CHR(10),this.w_INIETVMN, 12) )
            this.w_INIETVMN = this.w_INIET0 +'"'+alltrim(this.pMODULO) + '"'+ this.w_INIET1
          endif
          * --- Lo aggiungo al menu
          this.w_NEWMENU = this.w_ORIMENU+this.w_INIETVMN
          * --- Iniezione voce  (sempre livello 2, padre Progressivo Submenu aggiunto)
          this.w_INIET1 = this.w_INIET0+" 3" + this.w_INIET0+'"'+alltrim(this.pNEWPRG) +'"'+this.w_INIET0+ " 1" +this.w_INIET0+" "+alltrim(str(VAL(alltrim(this.w_TOTVOCISTR))-2)) +this.w_INIET0+ " 0"+ this.w_INIET0+'"'+"_24B0RWQL2"+'"'+ this.w_INIET0+".t."+this.w_INIET0 
          this.w_INIET2 = this.w_INIET0+'"'+"DIR_DESC.BMP" +'"'+this.w_INIET0+this.w_BLANK+this.w_INIET0+this.w_BLANK+this.w_INIET0+this.w_BLANK+this.w_INIET0+this.w_BLANK+this.w_INIET0+this.w_BLANK+this.w_INIET0+this.w_BLANK+this.w_INIET0+"*"+this.w_INIET0+"*"+this.w_INIET0
          this.w_INIETVMN = '"'+Alltrim(this.pNEWDES)+'"'+this.w_INIET1+ " "+alltrim(str(VAL(alltrim(this.w_TOTVOCISTR))-1)) +this.w_INIET2 
          * --- Lo aggiungo al menu
          this.w_NEWMENU = this.w_NEWMENU+this.w_INIETVMN
        else
          * --- Non � presente il submenu come parametro (sempre livello 1, padre Menu principale)
          this.w_INIET1 = this.w_INIET0+" 2" + this.w_INIET0 +'"'+alltrim(this.pNEWPRG)+'"'+this.w_INIET0+ " 2" +this.w_INIET0+" 0"+this.w_INIET0+ " 1"+ this.w_INIET0+'"'+"_24B0RWQLO"+'"'+ this.w_INIET0+".t."+this.w_INIET0 
          this.w_INIET2 = this.w_INIET0+'"'+"DIR_DESC.BMP" +'"'+this.w_INIET0+this.w_BLANK+this.w_INIET0+this.w_BLANK+this.w_INIET0
          this.w_INIET3 = this.w_INIET0+this.w_BLANK+this.w_INIET0+this.w_BLANK+this.w_INIET0+this.w_BLANK+this.w_INIET0
          this.w_INIETVMN = '"'+Alltrim(this.pNEWDES)+'"'+ this.w_INIET1 + " "+alltrim(str(VAL(alltrim(this.w_TOTVOCISTR))-1)) +this.w_INIET2 +'"'+Alltrim(this.w_AGGANCIO)+'"'+this.w_INIET3+"*"+this.w_INIET0+"*"+this.w_INIET0
          * --- Il submenu � condizionato
          if !Empty(this.pMODULO)
            this.w_INIET0 = SUBSTR(this.w_INIETVMN, 1, AT(CHR(13) + CHR(10),this.w_INIETVMN,11) + 1)
            this.w_INIET1 = SUBSTR(this.w_INIETVMN, AT(CHR(13) + CHR(10),this.w_INIETVMN, 12) )
            this.w_INIETVMN = this.w_INIET0 +'"'+alltrim(this.pMODULO) + '"'+ this.w_INIET1
          endif
          * --- Lo aggiungo al menu
          this.w_NEWMENU = this.w_ORIMENU+this.w_INIETVMN
        endif
        * --- Riscrittura finale su file
        this.w_NUM = STRTOFILE(this.w_NEWMENU,this.pPATHVMN)
    endcase
  endproc


  proc Init(oParentObject,pPATHVMN,pKEYPRG,pNEWPRG,pNEWDES,pSUBMEN,pMODULO,pAGGANCIO)
    this.pPATHVMN=pPATHVMN
    this.pKEYPRG=pKEYPRG
    this.pNEWPRG=pNEWPRG
    this.pNEWDES=pNEWDES
    this.pSUBMEN=pSUBMEN
    this.pMODULO=pMODULO
    this.pAGGANCIO=pAGGANCIO
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPATHVMN,pKEYPRG,pNEWPRG,pNEWDES,pSUBMEN,pMODULO,pAGGANCIO"
endproc
