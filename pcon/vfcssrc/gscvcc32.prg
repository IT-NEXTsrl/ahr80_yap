* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcc32                                                        *
*              Tipologie/classi allegati                                       *
*                                                                              *
*      Author: Zucchetti SpA                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_221]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-03-17                                                      *
* Last revis.: 2013-05-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcc32",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcc32 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TMPC = space(10)
  w_TEMP = space(10)
  w_UPDOK = .f.
  w_UPDERR = .f.
  w_IDTIPFIL = space(10)
  w_oMess = .NULL.
  w_oPart = .NULL.
  w_TIPDEFEX = space(5)
  w_CLADEFEX = space(5)
  * --- WorkFile variables
  PROMINDI_idx=0
  PROMCLAS_idx=0
  EXT_ENS_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Procedura di conversione per eliminare il punto nella tipologia file degli indici documentali
    * --- FIle di LOG
    * --- Oggetto per messaggi incrementali
    this.oParentObject.w_PESEOK = TRUE
    this.w_oMess=createobject("Ah_Message")
    * --- Select from PROMINDI
    i_nConn=i_TableProp[this.PROMINDI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select DISTINCT IDTIPFIL  from "+i_cTable+" PROMINDI ";
          +" where IDTIPFIL LIKE '.%'";
           ,"_Curs_PROMINDI")
    else
      select DISTINCT IDTIPFIL from (i_cTable);
       where IDTIPFIL LIKE ".%";
        into cursor _Curs_PROMINDI
    endif
    if used('_Curs_PROMINDI')
      select _Curs_PROMINDI
      locate for 1=1
      do while not(eof())
      this.w_IDTIPFIL = _Curs_PROMINDI.IDTIPFIL
      this.w_UPDOK = TRUE
      * --- Try
      local bErr_03819A98
      bErr_03819A98=bTrsErr
      this.Try_03819A98()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_TMPC = message()
        this.w_oPart = this.w_oMess.AddMsgPartNL("Errore aggiornando il tipo file <%1>")
        this.w_oPart.AddParam(alltrim(this.w_IDTIPFIL))     
        this.w_oPart = this.w_oMess.AddMsgPartNL("messaggio di errore: %1",2)
        this.w_oPart.AddParam(this.w_TMPC)     
        this.w_UPDERR = TRUE
        this.oParentObject.w_PESEOK = FALSE
      endif
      bTrsErr=bTrsErr or bErr_03819A98
      * --- End
        select _Curs_PROMINDI
        continue
      enddo
      use
    endif
    if this.w_UPDOK
      if this.w_UPDERR
        this.w_TMPC = ah_msgformat("ATTENZIONE: si sono verificati errori durante l'aggiornamento, leggere il log per visualizzare il dettaglio")
      else
        this.w_TMPC = ah_msgformat("Conversione eseguita correttamente, leggere il log per visualizzare il dettaglio")
      endif
      this.oParentObject.w_PMSG = this.w_TMPC
      this.w_TMPC = this.w_oMess.ComposeMessage()
    else
      this.w_TMPC = ah_msgformat("Conversione eseguita correttamente, nessun problema rilevato")
      this.oParentObject.w_PMSG = this.w_TMPC
    endif
    if this.w_NHF>=0
      * --- Gestisce log errori
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
  endproc
  proc Try_03819A98()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into PROMINDI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PROMINDI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PROMINDI_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"IDTIPFIL ="+cp_NullLink(cp_ToStrODBC(substr(this.w_IDTIPFIL,2)),'PROMINDI','IDTIPFIL');
          +i_ccchkf ;
      +" where ";
          +"IDTIPFIL = "+cp_ToStrODBC(this.w_IDTIPFIL);
             )
    else
      update (i_cTable) set;
          IDTIPFIL = substr(this.w_IDTIPFIL,2);
          &i_ccchkf. ;
       where;
          IDTIPFIL = this.w_IDTIPFIL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    this.w_oPart = this.w_oMess.AddMsgPartNL("Aggiornato il tipo file <%1>")
    this.w_oPart.AddParam(alltrim(this.w_IDTIPFIL))     
    this.w_oPart = this.w_oMess.AddMsgPartNL("Numero record aggiornati: %1",2)
    this.w_oPart.AddParam(alltrim(str(i_rows)))     
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Per modificare la tipologia e classe eseguo una scansione delle estensioni presenti
    *     In questo modo riesco 
    * --- Select from EXT_ENS
    i_nConn=i_TableProp[this.EXT_ENS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.EXT_ENS_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" EXT_ENS ";
           ,"_Curs_EXT_ENS")
    else
      select * from (i_cTable);
        into cursor _Curs_EXT_ENS
    endif
    if used('_Curs_EXT_ENS')
      select _Curs_EXT_ENS
      locate for 1=1
      do while not(eof())
      this.w_ESTENS = Alltrim(_Curs_EXT_ENS.EXCODICE)
      ah_Msg(this.w_ESTENS,.T.)
      this.w_TIPDEFEX = _Curs_EXT_ENS.EXTIPALL
      this.w_CLADEFEX = _Curs_EXT_ENS.EXCLAALL
      * --- Write into PROMINDI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.PROMINDI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="IDSERIAL"
        do vq_exec with 'GSCVBB18',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.PROMINDI_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="PROMINDI.IDSERIAL = _t2.IDSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"IDTIPALL = _t2.GFTIPALL";
            +",IDCLAALL = _t2.GFCLAALL";
            +",IDOGGETT = _t2.GFDESCRI";
            +i_ccchkf;
            +" from "+i_cTable+" PROMINDI, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="PROMINDI.IDSERIAL = _t2.IDSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" PROMINDI, "+i_cQueryTable+" _t2 set ";
            +"PROMINDI.IDTIPALL = _t2.GFTIPALL";
            +",PROMINDI.IDCLAALL = _t2.GFCLAALL";
            +",PROMINDI.IDOGGETT = _t2.GFDESCRI";
            +Iif(Empty(i_ccchkf),"",",PROMINDI.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="PROMINDI.IDSERIAL = t2.IDSERIAL";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" PROMINDI set (";
            +"IDTIPALL,";
            +"IDCLAALL,";
            +"IDOGGETT";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.GFTIPALL,";
            +"t2.GFCLAALL,";
            +"t2.GFDESCRI";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="PROMINDI.IDSERIAL = _t2.IDSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" PROMINDI set ";
            +"IDTIPALL = _t2.GFTIPALL";
            +",IDCLAALL = _t2.GFCLAALL";
            +",IDOGGETT = _t2.GFDESCRI";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".IDSERIAL = "+i_cQueryTable+".IDSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"IDTIPALL = (select GFTIPALL from "+i_cQueryTable+" where "+i_cWhere+")";
            +",IDCLAALL = (select GFCLAALL from "+i_cQueryTable+" where "+i_cWhere+")";
            +",IDOGGETT = (select GFDESCRI from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
        select _Curs_EXT_ENS
        continue
      enddo
      use
    endif
  endproc


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='PROMINDI'
    this.cWorkTables[2]='PROMCLAS'
    this.cWorkTables[3]='EXT_ENS'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_PROMINDI')
      use in _Curs_PROMINDI
    endif
    if used('_Curs_EXT_ENS')
      use in _Curs_EXT_ENS
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
