* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcb12                                                        *
*              AGGIORNAMENTO CHIAVE TABELLA RAP_PRES                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-01-14                                                      *
* Last revis.: 2008-09-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcb12",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcb12 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TMPC = space(10)
  w_TEMP = space(10)
  w_RECODICE = space(15)
  w_REDESCRI = space(40)
  w_CNAME = space(30)
  w_CODAZI = 0
  w_PHNAME = space(50)
  w_IDXTABLE = 0
  w_CONN = 0
  w_PK = .f.
  w_LoopIFK = 0
  w_LTABLE = space(200)
  w_LCPHTABLE = space(200)
  w_OK = 0
  w_IDXTABLEIFK = 0
  * --- WorkFile variables
  RAP_PRES_idx=0
  TMP_DETT_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Copia i dati della tabella RAP_PRES per eliminarla
    *     e ricostruirla con struttura corretta, procedura di convesrione associata 5.0-M1538
    * --- Try
    local bErr_037A2840
    bErr_037A2840=bTrsErr
    this.Try_037A2840()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.w_TMPC = ah_Msgformat("La procedura ha creato il dbf %1 con i dati originari della tabella: contattare l'assistenza",nametable)
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
      release nametable
    endif
    bTrsErr=bTrsErr or bErr_037A2840
    * --- End
  endproc
  proc Try_037A2840()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    public nametable
    nametable = tempadhoc()+"\RAP_PRES.DBF"
    local w_nConn, w_cTable 
 w_nConn=i_TableProp[this.RAP_PRES_idx,3] 
 w_cTable=cp_SetAzi(i_TableProp[this.RAP_PRES_idx,2])
    pName="RAP_PRES"
    pDatabaseType=i_ServerConn[1,6]
    * --- Ricostruisco  la tabella prima di copiarne il contenuto  (si evita il rischio che la tabella copiata abbia dei campi in meno della tabella ricostruita)
    * --- Create temporary table TMP_DETT
    i_nIdx=cp_AddTableDef('TMP_DETT') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('gscvcb12',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMP_DETT_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    vq_exec("gscvcb12.vqr",this,"CursErrore") 
 select CursErrore 
 COPY TO (FORCEEXT(nametable,"dbf"))
    if USED("CursErrore")
      select CursErrore 
 use
    endif
    ah_msg("Creata tabelle temporanee di appoggio dettaglio")
    this.w_CNAME = "RAP_PRES"
    this.w_CODAZI = i_CODAZI
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_CODAZI = "xxx"
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if Not GSCV_BDT( this, "RAP_PRES" , w_nConn )
      * --- Raise
      i_Error="Error"
      return
    endif
    * --- Ricostruisco il database per la tabella
    GSCV_BRT(this, "RAP_PRES" , w_nConn , .T.)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into RAP_PRES
    i_nConn=i_TableProp[this.RAP_PRES_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.RAP_PRES_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.TMP_DETT_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.RAP_PRES_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Drop temporary table TMP_DETT
    i_nIdx=cp_GetTableDefIdx('TMP_DETT')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMP_DETT')
    endif
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.w_TMPC = ah_Msgformat("Tabella prestazioni giornaliere creata correttamente")
    this.oParentObject.w_PMSG = ah_Msgformat("Conversione eseguita correttamente")
    release nametable
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Rimuove integrit� referenziale
    this.w_IDXTABLE = cp_OpenTable( Alltrim(this.w_CNAME) ,.T.)
    * --- Se indice a 0 � una tabella temporanea...
    if this.w_IDXTABLE<>0
      this.w_PHNAME = i_dcx.GetPhTable( i_dcx.GetTableIdx( this.w_CNAME ) , this.w_CODAZI )
      this.w_CONN = i_TableProp[ this.w_IDXTABLE , 3 ] 
      if this.w_CONN<>0
         
 cp_RemoveReferences( this.w_CNAME , this.w_PHNAME , this.w_CONN ,i_dcx, cp_GetDatabaseType( this.w_CONN) ,this.w_CODAZI ) 
        this.RemoveIndexes(this.w_CNAME , this.w_PHNAME , this.w_CONN ,i_dcx, cp_GetDatabaseType( this.w_CONN) ,this.w_CODAZI )
        this.w_TMPC = AH_MSGFORMAT( "Integrit� cancellate per tabella %1 " , this.w_PHNAME) 
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    * --- Rimuovo integrit� referenziali delle tabelle che mi puntano
    this.w_LoopIFK = 1
    do while this.w_LoopIFK<=i_Dcx.GetFKCount( this.w_CNAME )
      this.w_LTABLE = I_DCX.GetFKTable( this.w_CNAME , this.w_LoopIFK )
      this.w_IDXTABLEIFK = cp_OpenTable( Alltrim(this.w_LTABLE) ,.T.)
      this.w_CONN = i_TableProp[ this.w_IDXTABLEIFK , 3 ] 
      this.w_LCPHTABLE = I_DCX.GetPhTable(I_DCX.GetTableIdx( this.w_LTABLE ), "xxx" )
      cp_RemoveReferences( this.w_LTABLE , this.w_LCPHTABLE , this.w_CONN ,i_dcx, cp_GetDatabaseType( this.w_CONN) , this.w_CODAZI ) 
      this.w_TMPC = AH_MSGFORMAT( "Integrit� cancellate per tabella %1 " , this.w_LCPHTABLE)
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      this.w_LoopIFK = this.w_LoopIFK + 1 
    enddo
  endproc


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='RAP_PRES'
    this.cWorkTables[2]='*TMP_DETT'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- gscvcb12
  proc RemoveIndexes(cName,cPhName,nConn,oDCX,cDatabaseType,cAzi)
    local i,idxname,r,k
    private res
    if nConn=0
      * --- apre il file
      cp_SafeUseExcl(cPhName)
    endif
    * --- rimuove le chiavi
    * --- zucchetti aulla: modifica per errore riduzione indici su oracle
    for i=1 to max(oDCX.GetIdxCount(cName),10)
      if nConn<>0
        idxname = cPhName+alltrim(str(i))
        if i=1 
          * --- controlla se la chiave primaria e' stata modificata
          IF (this.w_PK=.T.  AND upper(CP_DBTYPE)<>'DB2')
            k=db_DropPrimaryKey(cName,cPhName,cDatabaseType)
            r=sqlexec(nConn,"alter table "+cPhName+" "+k)
            if r=-1
              * --- non e' riuscito a cancellare la chiave primaria, ci sono delle referenze
              * --- droppa tutte le foreign key, che tanto poi dovranno essere ricostruite
              cp_RemoveReferences(cName,cPhName,nConn,oDcx,cDatabaseType,cAzi)
              r=cp_SQL(nConn,"alter table "+cPhName+" "+k)
            endif
            
            * --- cancellazione indice se su oracle 10
            if cDatabaseType='DB2/390' or cDatabaseType='Oracle'
              r=sqlexec(nConn,"drop index pk_"+cPhName)
            endif
          ENDIF
        else
          k=db_DropIndex(cName,cPhName,idxname,cDatabaseType)
          r=sqlexec(nConn,"drop index "+k)
          if i>oDCX.GetIdxCount(cName) and r=-1
             exit
          endif
          
        endif
      else
        idxname = 'IDX'+alltrim(str(i))
        on error res=.f.
        delete tag &idxname
        if i=1
          delete tag idxdel
          delete tag idx1bis
        endif
        on error
      endif
    next
    if nConn=0
      * --- chiude il file
      use
    endif
    return
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
