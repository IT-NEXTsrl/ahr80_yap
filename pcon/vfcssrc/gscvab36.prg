* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvab36                                                        *
*              Aggiornamento codice agente                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_44]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-12-02                                                      *
* Last revis.: 2002-12-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvab36",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvab36 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_TIPCON = space(1)
  w_CODCON = space(15)
  w_DATSCA = ctod("  /  /  ")
  w_NUMPAR = space(14)
  w_CODVAL = space(5)
  w_PTSERIAL = space(10)
  w_PTROWORD = 0
  w_CPROWNUM = 0
  w_CODAGE = space(5)
  w_SERIAL = space(10)
  w_CAUINS = space(5)
  w_CODAZI = space(5)
  w_CODREL = space(15)
  * --- WorkFile variables
  PNT_DETT_idx=0
  PAR_TITE_idx=0
  CONTROPA_idx=0
  CAU_CONT_idx=0
  CONVERSI_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questa procedura di conversione modifica il valore 
    *     campo PTCODAGE,PNCODAGE
    *     prelevando il valore da assegnare dal relativo documento collegato
    *     o in assenza dall'anagrafica Clienti\Fornitori relativa
    * --- Try
    local bErr_03600A20
    bErr_03600A20=bTrsErr
    this.Try_03600A20()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE - impossibile aggiornare PNCODAGE,PTCODAGE su tabelle PNT_DETT,PAR_TITE" )
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_03600A20
    * --- End
  endproc
  proc Try_03600A20()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Read from CONVERSI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CONVERSI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONVERSI_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "COCODREL"+;
        " from "+i_cTable+" CONVERSI where ";
            +"CONOMPRO = "+cp_ToStrODBC("GSCVAB36");
            +" and COESEGUI = "+cp_ToStrODBC("S");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        COCODREL;
        from (i_cTable) where;
            CONOMPRO = "GSCVAB36";
            and COESEGUI = "S";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CODREL = NVL(cp_ToDate(_read_.COCODREL),cp_NullValue(_read_.COCODREL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if EMPTY(this.w_CODREL)
      * --- Valorizzo Flag Insoluti
      this.w_CODAZI = i_CODAZI
      * --- Read from CONTROPA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTROPA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTROPA_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "COCAUINS"+;
          " from "+i_cTable+" CONTROPA where ";
              +"COCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          COCAUINS;
          from (i_cTable) where;
              COCODAZI = this.w_CODAZI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CAUINS = NVL(cp_ToDate(_read_.COCAUINS),cp_NullValue(_read_.COCAUINS))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if Not Empty(this.w_CAUINS)
        * --- Write into CAU_CONT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.CAU_CONT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAU_CONT_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CAU_CONT_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CCFLINSO ="+cp_NullLink(cp_ToStrODBC("S"),'CAU_CONT','CCFLINSO');
              +i_ccchkf ;
          +" where ";
              +"CCCODICE = "+cp_ToStrODBC(this.w_CAUINS);
                 )
        else
          update (i_cTable) set;
              CCFLINSO = "S";
              &i_ccchkf. ;
           where;
              CCCODICE = this.w_CAUINS;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Aggiornamento  Codice Agente
        * --- Select from gscv_b94
        do vq_exec with 'gscv_b94',this,'_Curs_gscv_b94','',.f.,.t.
        if used('_Curs_gscv_b94')
          select _Curs_gscv_b94
          locate for 1=1
          do while not(eof())
          this.w_TIPCON = NVL(_Curs_gscv_b94.PTTIPCON," ")
          this.w_CODCON = NVL(_Curs_gscv_b94.PTCODCON,SPACE(15))
          this.w_DATSCA = CP_TODATE(_Curs_gscv_b94.PTDATSCA)
          this.w_NUMPAR = NVL(_Curs_gscv_b94.PTNUMPAR,SPACE(14))
          this.w_CODVAL = NVL(_Curs_gscv_b94.PTCODVAL,SPACE(5))
          this.w_PTSERIAL = _Curs_gscv_b94.PTSERIAL
          this.w_PTROWORD = _Curs_gscv_b94.PTROWORD
          this.w_CPROWNUM = _Curs_gscv_b94.CPROWNUM
          this.w_CODAGE = NVL(_Curs_gscv_b94.MVCODAGE,SPACE(5))
          * --- Aggiorno Campo in Primanota
          if this.w_PTROWORD>0
            * --- Write into PNT_DETT
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.PNT_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.PNT_DETT_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"PNCODAGE ="+cp_NullLink(cp_ToStrODBC(this.w_CODAGE),'PNT_DETT','PNCODAGE');
                  +i_ccchkf ;
              +" where ";
                  +"PNSERIAL = "+cp_ToStrODBC(this.w_PTSERIAL);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.w_PTROWORD);
                     )
            else
              update (i_cTable) set;
                  PNCODAGE = this.w_CODAGE;
                  &i_ccchkf. ;
               where;
                  PNSERIAL = this.w_PTSERIAL;
                  and CPROWNUM = this.w_PTROWORD;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error='Errore scrittura codice agente in primanota'
              return
            endif
          endif
          * --- Inserisco Codice Agente Nelle Partite di Origine.
          * --- Write into PAR_TITE
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.PAR_TITE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_TITE_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"PTCODAGE ="+cp_NullLink(cp_ToStrODBC(this.w_CODAGE),'PAR_TITE','PTCODAGE');
                +i_ccchkf ;
            +" where ";
                +"PTNUMPAR = "+cp_ToStrODBC(this.w_NUMPAR);
                +" and PTDATSCA = "+cp_ToStrODBC(this.w_DATSCA);
                +" and PTTIPCON = "+cp_ToStrODBC(this.w_TIPCON);
                +" and PTCODCON = "+cp_ToStrODBC(this.w_CODCON);
                +" and PTCODVAL = "+cp_ToStrODBC(this.w_CODVAL);
                   )
          else
            update (i_cTable) set;
                PTCODAGE = this.w_CODAGE;
                &i_ccchkf. ;
             where;
                PTNUMPAR = this.w_NUMPAR;
                and PTDATSCA = this.w_DATSCA;
                and PTTIPCON = this.w_TIPCON;
                and PTCODCON = this.w_CODCON;
                and PTCODVAL = this.w_CODVAL;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error='Errore scrittura riferimenti'
            return
          endif
            select _Curs_gscv_b94
            continue
          enddo
          use
        endif
        * --- commit
        cp_EndTrs(.t.)
        if this.w_NHF>=0
          this.w_TMPC = ah_Msgformat("Aggiornamento campo agente su tabelle PAR_TITE e PNT_DETT eseguito con successo")
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
        this.oParentObject.w_PESEOK = .T.
      else
        if this.w_NHF>=0
          this.w_TMPC = ah_Msgformat("Attenzione - inserire causale insoluti nei parametri distinte")
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
        this.oParentObject.w_PESEOK = .F.
      endif
    else
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("Aggiornamento campo agente su tabelle PAR_TITE e PNT_DETT gi� eseguito nella Rel. 2.2")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
      this.oParentObject.w_PESEOK = .T.
    endif
    * --- Esecuzione ok
    this.oParentObject.w_PMSG = this.oParentObject.w_PMSG + this.w_TMPC 
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='PNT_DETT'
    this.cWorkTables[2]='PAR_TITE'
    this.cWorkTables[3]='CONTROPA'
    this.cWorkTables[4]='CAU_CONT'
    this.cWorkTables[5]='CONVERSI'
    return(this.OpenAllTables(5))

  proc CloseCursors()
    if used('_Curs_gscv_b94')
      use in _Curs_gscv_b94
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
