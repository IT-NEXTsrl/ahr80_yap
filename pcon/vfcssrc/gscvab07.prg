* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvab07                                                        *
*              Aggiornamento documenti e causali                               *
*                                                                              *
*      Author: Zucchetti Tam Spa                                               *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_37]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-10-17                                                      *
* Last revis.: 2001-08-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvab07",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvab07 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_MESS = space(200)
  w_TDTIPDOC = space(5)
  w_TDCATDOC = space(2)
  w_TDFLVEAC = space(1)
  w_CMFLCASC = space(1)
  w_MVSERIAL = space(10)
  w_CPROWNUM = 0
  w_MVNUMRIF = 0
  w_MVNUMDOC = 0
  w_MVALFDOC = space(10)
  w_MVDATDOC = space(2)
  w_MV_SEGNO = space(1)
  * --- WorkFile variables
  TIP_DOCU_idx=0
  DOC_DETT_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- FIle di LOG
    * --- Variabili per l'aggiornamento delle causali documenti
    * --- Variabili per l'aggiornamento dei documenti
    * --- Try
    local bErr_0360CD10
    bErr_0360CD10=bTrsErr
    this.Try_0360CD10()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = this.oParentObject.w_PMSG +" "+ Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE GENERICO - impossibile aggiornare causali documenti")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_0360CD10
    * --- End
    * --- Try
    local bErr_035FC1F0
    bErr_035FC1F0=bTrsErr
    this.Try_035FC1F0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = this.oParentObject.w_PMSG +" "+ Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE GENERICO - impossibile aggiornare documenti")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_035FC1F0
    * --- End
  endproc
  proc Try_0360CD10()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Aggiornamento  TDVOCECR e TD_SEGNO
    VQ_EXEC("..\PCON\EXE\QUERY\GSCV_B07.VQR", this, "CAUSALI")
    if USED("CAUSALI")
      if Reccount ("CAUSALI")>0
        SELECT CAUSALI
        GO TOP
        SCAN
        this.w_TDTIPDOC = NVL(TDTIPDOC, "     ")
        this.w_TDCATDOC = NVL(TDCATDOC, "  ")
        this.w_TDFLVEAC = NVL(TDFLVEAC, " ")
        this.w_CMFLCASC = NVL(CMFLCASC, " ")
        ah_Msg("Aggiornamento causale documento %1",.T.,.F.,.F., this.w_TDTIPDOC)
        do case
          case this.w_TDCATDOC<>"NC" AND this.w_TDFLVEAC="V"
            if this.w_TDCATDOC="DT" AND this.w_CMFLCASC="+"
              * --- Voce di Ricavo in Dare
              * --- Write into TIP_DOCU
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.TIP_DOCU_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"TDVOCECR ="+cp_NullLink(cp_ToStrODBC("R"),'TIP_DOCU','TDVOCECR');
                +",TD_SEGNO ="+cp_NullLink(cp_ToStrODBC("D"),'TIP_DOCU','TD_SEGNO');
                    +i_ccchkf ;
                +" where ";
                    +"TDTIPDOC = "+cp_ToStrODBC(this.w_TDTIPDOC);
                       )
              else
                update (i_cTable) set;
                    TDVOCECR = "R";
                    ,TD_SEGNO = "D";
                    &i_ccchkf. ;
                 where;
                    TDTIPDOC = this.w_TDTIPDOC;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            else
              * --- Voce di Ricavo in Avere
              * --- Write into TIP_DOCU
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.TIP_DOCU_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"TDVOCECR ="+cp_NullLink(cp_ToStrODBC("R"),'TIP_DOCU','TDVOCECR');
                +",TD_SEGNO ="+cp_NullLink(cp_ToStrODBC("A"),'TIP_DOCU','TD_SEGNO');
                    +i_ccchkf ;
                +" where ";
                    +"TDTIPDOC = "+cp_ToStrODBC(this.w_TDTIPDOC);
                       )
              else
                update (i_cTable) set;
                    TDVOCECR = "R";
                    ,TD_SEGNO = "A";
                    &i_ccchkf. ;
                 where;
                    TDTIPDOC = this.w_TDTIPDOC;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
          case this.w_TDCATDOC="NC" AND this.w_TDFLVEAC="V"
            * --- Voce di Ricavo in Dare
            * --- Write into TIP_DOCU
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.TIP_DOCU_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"TDVOCECR ="+cp_NullLink(cp_ToStrODBC("R"),'TIP_DOCU','TDVOCECR');
              +",TD_SEGNO ="+cp_NullLink(cp_ToStrODBC("D"),'TIP_DOCU','TD_SEGNO');
                  +i_ccchkf ;
              +" where ";
                  +"TDTIPDOC = "+cp_ToStrODBC(this.w_TDTIPDOC);
                     )
            else
              update (i_cTable) set;
                  TDVOCECR = "R";
                  ,TD_SEGNO = "D";
                  &i_ccchkf. ;
               where;
                  TDTIPDOC = this.w_TDTIPDOC;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          case this.w_TDCATDOC<>"NC" AND this.w_TDFLVEAC="A"
            if this.w_TDCATDOC="DT" AND this.w_CMFLCASC="-"
              * --- Voce di Costo in Avere
              * --- Write into TIP_DOCU
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.TIP_DOCU_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"TDVOCECR ="+cp_NullLink(cp_ToStrODBC("C"),'TIP_DOCU','TDVOCECR');
                +",TD_SEGNO ="+cp_NullLink(cp_ToStrODBC("A"),'TIP_DOCU','TD_SEGNO');
                    +i_ccchkf ;
                +" where ";
                    +"TDTIPDOC = "+cp_ToStrODBC(this.w_TDTIPDOC);
                       )
              else
                update (i_cTable) set;
                    TDVOCECR = "C";
                    ,TD_SEGNO = "A";
                    &i_ccchkf. ;
                 where;
                    TDTIPDOC = this.w_TDTIPDOC;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            else
              * --- Voce di Costo in Dare
              * --- Write into TIP_DOCU
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.TIP_DOCU_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"TDVOCECR ="+cp_NullLink(cp_ToStrODBC("C"),'TIP_DOCU','TDVOCECR');
                +",TD_SEGNO ="+cp_NullLink(cp_ToStrODBC("D"),'TIP_DOCU','TD_SEGNO');
                    +i_ccchkf ;
                +" where ";
                    +"TDTIPDOC = "+cp_ToStrODBC(this.w_TDTIPDOC);
                       )
              else
                update (i_cTable) set;
                    TDVOCECR = "C";
                    ,TD_SEGNO = "D";
                    &i_ccchkf. ;
                 where;
                    TDTIPDOC = this.w_TDTIPDOC;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
          case this.w_TDCATDOC="NC" AND this.w_TDFLVEAC="A"
            * --- Voce di Costo in Avere
            * --- Write into TIP_DOCU
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.TIP_DOCU_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"TDVOCECR ="+cp_NullLink(cp_ToStrODBC("C"),'TIP_DOCU','TDVOCECR');
              +",TD_SEGNO ="+cp_NullLink(cp_ToStrODBC("A"),'TIP_DOCU','TD_SEGNO');
                  +i_ccchkf ;
              +" where ";
                  +"TDTIPDOC = "+cp_ToStrODBC(this.w_TDTIPDOC);
                     )
            else
              update (i_cTable) set;
                  TDVOCECR = "C";
                  ,TD_SEGNO = "A";
                  &i_ccchkf. ;
               where;
                  TDTIPDOC = this.w_TDTIPDOC;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
        endcase
        SELECT CAUSALI
        ENDSCAN
        * --- Esecuzione ok
        this.oParentObject.w_PESEOK = .T.
        this.w_TMPC = Ah_Msgformat("Aggiornamento campo TDVOCECR e TD_SEGNO su tabella TIP_DOCU eseguito con successo")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        this.oParentObject.w_PMSG = this.oParentObject.w_PMSG +" "+ this.w_TMPC 
      else
        * --- Gestisce log errori
        if this.w_NHF>=0
          this.w_TMPC = ah_Msgformat("ERRORE - impossibile aggiornare TDVOCECR e TD_SEGNO su tabella TIP_DOCU" )
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
        * --- Esecuzione ok
        this.oParentObject.w_PESEOK = .T.
        this.oParentObject.w_PMSG = this.oParentObject.w_PMSG +" "+ this.w_TMPC 
      endif
      * --- Chiude cursore
      SELECT CAUSALI
      USE
    endif
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_035FC1F0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Aggiornamento  MV_SEGNO
    * --- begin transaction
    cp_BeginTrs()
    VQ_EXEC("..\PCON\EXE\QUERY\GSCV1B07.VQR", this, "ELABOR")
    if USED("ELABOR")
      if Reccount ("ELABOR")>0
        SELECT ELABOR
        GO TOP
        SCAN
        this.w_MVSERIAL = NVL(MVSERIAL, "          ")
        this.w_CPROWNUM = NVL(CPROWNUM, 0)
        this.w_MVNUMRIF = NVL(MVNUMRIF, 0)
        this.w_MVNUMDOC = NVL(MVNUMDOC, 0)
        this.w_MVALFDOC = NVL(MVALFDOC, Space(10))
        this.w_MVDATDOC = CP_TODATE(MVDATDOC)
        ah_Msg("Aggiornamento documento %1 del %2",.T.,.F.,.F., ALLTR(STR(this.w_MVNUMDOC,15))+IIF(Not Empty(this.w_MVALFDOC)," "+Alltrim(this.w_MVALFDOC),""), DTOC(this.w_MVDATDOC) )
        if (ELABOR.MVCLADOC="NC" AND ELABOR.MVFLVEAC="V") OR (ELABOR.MVCLADOC<>"NC" AND ELABOR.MVFLVEAC="A")
          this.w_MV_SEGNO = "D"
        else
          this.w_MV_SEGNO = "A"
        endif
        * --- Write into DOC_DETT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.DOC_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MV_SEGNO ="+cp_NullLink(cp_ToStrODBC(this.w_MV_SEGNO),'DOC_DETT','MV_SEGNO');
              +i_ccchkf ;
          +" where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
                 )
        else
          update (i_cTable) set;
              MV_SEGNO = this.w_MV_SEGNO;
              &i_ccchkf. ;
           where;
              MVSERIAL = this.w_MVSERIAL;
              and CPROWNUM = this.w_CPROWNUM;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        SELECT ELABOR
        ENDSCAN
        * --- Esecuzione ok
        this.oParentObject.w_PESEOK = .T.
        this.w_TMPC = ah_Msgformat("Aggiornamento campo MV_SEGNO su tabella DOC_DETT eseguito con successo")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        this.oParentObject.w_PMSG = this.oParentObject.w_PMSG +" "+ this.w_TMPC 
      else
        * --- Gestisce log errori
        if this.w_NHF>=0
          this.w_TMPC = ah_Msgformat("ATTENZIONE: non esistono documenti da aggiornare")
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
        * --- Esecuzione ok
        this.oParentObject.w_PESEOK = .T.
        this.oParentObject.w_PMSG = this.oParentObject.w_PMSG +" "+ this.w_TMPC 
      endif
      * --- Chiude cursore
      SELECT ELABOR
      USE
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='TIP_DOCU'
    this.cWorkTables[2]='DOC_DETT'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
