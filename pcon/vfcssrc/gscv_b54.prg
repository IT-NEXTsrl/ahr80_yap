* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscv_b54                                                        *
*              Aggiornamento flag lotti\ubicazioni                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_12]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-11-19                                                      *
* Last revis.: 2001-11-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscv_b54",oParentObject,m.w_NHF)
return(i_retval)

define class tgscv_b54 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_FLLOTT = space(1)
  w_CODART = space(20)
  w_FLUBIC = space(1)
  w_CODMAG = space(5)
  * --- WorkFile variables
  ART_ICOL_idx=0
  MAGAZZIN_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- FIle di LOG
    * --- Try
    local bErr_035EED00
    bErr_035EED00=bTrsErr
    this.Try_035EED00()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE - impossibile aggiornare ARFLLOTT su tabella ART_ICOL, MGFLUBIC su tabella MAGAZZIN")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_035EED00
    * --- End
  endproc
  proc Try_035EED00()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Aggiornamento  COMBO LOTTI
    * --- Select from ART_ICOL
    i_nConn=i_TableProp[this.ART_ICOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" ART_ICOL ";
           ,"_Curs_ART_ICOL")
    else
      select * from (i_cTable);
        into cursor _Curs_ART_ICOL
    endif
    if used('_Curs_ART_ICOL')
      select _Curs_ART_ICOL
      locate for 1=1
      do while not(eof())
      this.w_FLLOTT = NVL(_Curs_ART_ICOL.ARFLLOTT,"N")
      this.w_CODART = _Curs_ART_ICOL.ARCODART
      if EMPTY(this.w_FLLOTT)
        this.w_FLLOTT = "N"
      endif
      if this.w_FLLOTT="N"
        * --- Write into ART_ICOL
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ART_ICOL_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ARFLLOTT ="+cp_NullLink(cp_ToStrODBC(this.w_FLLOTT),'ART_ICOL','ARFLLOTT');
              +i_ccchkf ;
          +" where ";
              +"ARCODART = "+cp_ToStrODBC(this.w_CODART);
                 )
        else
          update (i_cTable) set;
              ARFLLOTT = this.w_FLLOTT;
              &i_ccchkf. ;
           where;
              ARCODART = this.w_CODART;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
        select _Curs_ART_ICOL
        continue
      enddo
      use
    endif
    * --- Aggiornamento  Flag Ubicazioni
    * --- Select from MAGAZZIN
    i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" MAGAZZIN ";
           ,"_Curs_MAGAZZIN")
    else
      select * from (i_cTable);
        into cursor _Curs_MAGAZZIN
    endif
    if used('_Curs_MAGAZZIN')
      select _Curs_MAGAZZIN
      locate for 1=1
      do while not(eof())
      this.w_FLUBIC = IIF(NVL(_Curs_MAGAZZIN.MGFLUBIC," ") <>"S"," ","S")
      this.w_CODMAG = _Curs_MAGAZZIN.MGCODMAG
      * --- Aggiorno Flag
      * --- Write into MAGAZZIN
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.MAGAZZIN_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MGFLUBIC ="+cp_NullLink(cp_ToStrODBC(this.w_FLUBIC),'MAGAZZIN','MGFLUBIC');
            +i_ccchkf ;
        +" where ";
            +"MGCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
               )
      else
        update (i_cTable) set;
            MGFLUBIC = this.w_FLUBIC;
            &i_ccchkf. ;
         where;
            MGCODMAG = this.w_CODMAG;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
        select _Curs_MAGAZZIN
        continue
      enddo
      use
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Aggiornamento campo ARFLLOTT su tabella ART_ICOL, MGFLUBIC su tabella MAGAZZIN eseguito con successo")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = this.oParentObject.w_PMSG + this.w_TMPC 
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='MAGAZZIN'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_ART_ICOL')
      use in _Curs_ART_ICOL
    endif
    if used('_Curs_MAGAZZIN')
      use in _Curs_MAGAZZIN
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
