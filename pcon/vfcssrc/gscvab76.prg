* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvab76                                                        *
*              Controllo fatturazione                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_98]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-02-27                                                      *
* Last revis.: 2003-02-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvab76",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvab76 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_MESS = space(100)
  w_DATA = ctod("  /  /  ")
  w_oMess = .NULL.
  * --- WorkFile variables
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questa procedura Aggiorna il campo QTA Evasa nei Documenti evasi nelle fatture differite  generate in Automatico
    * --- FIle di LOG
    * --- Oggetto per messaggi incrementali
    this.w_oMess=createobject("Ah_Message")
    vq_exec("QUERY\GSVE_QCF", this, "__TMP__")
    if RECCOUNT("__TMP__")>0
      if ah_YesNo("Stampo riepilogo documenti incongruenti?")
        CP_CHPRN("QUERY\GSVE_QCF.FRX", " ", this)
      endif
      this.w_oMess.AddMsgPartNL("Esistono documenti incongruenti")     
      this.w_oMess.AddMsgPartNL("Esistono fatture differite generate a fronte di DDT evasi parzialmente")     
      this.w_oMess.AddMsgPartNL("Il valore fiscale delle righe della fattura differita � stato calcolato sulla quantit� originaria del DDT e non solo per la parte che era realmente da evadere")     
      this.w_oMess.AddMsgPartNL("Per risolvere il problema � possibile:")     
      this.w_oMess.AddMsgPartNL("1) Eliminare il piano di fatturazione nel quale sono contenute le fatture sotto elencate e quindi rigenerarlo")     
      this.w_oMess.AddMsgPartNL("2) Se punto 1) non possibile, allora inserire una nota di credito che non movimenta l'esistenza di magazzino ma solo il venduto per stornare il valore della fattura differita")     
      this.w_oMess.AddMsgPartNL("Di seguito riportiamo le righe delle fatture differite che devono essere importate in note di credito modificando quindi la quantit� importata con quella indicata nel campo 'Differenza'")     
      this.oParentObject.w_PMSG = this.w_oMess.ComposeMessage()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("Attenzione%0%1",this.oParentObject.w_PMSG)
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    else
      * --- Esecuzione ok
      this.oParentObject.w_PMSG = ah_Msgformat("Conversione eseguita correttamente")
    endif
    this.oParentObject.w_PESEOK = .T.
    if Used("__TMP__")
       
 Select __TMP__ 
 Use
    endif
  endproc


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
