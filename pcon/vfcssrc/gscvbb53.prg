* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvbb53                                                        *
*              Inserimento caratteri speciali                                  *
*                                                                              *
*      Author: Zucchetti SpA                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-03-17                                                      *
* Last revis.: 2006-11-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvbb53",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvbb53 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TMPC = space(10)
  w_TEMP = space(10)
  w_CODICE = space(10)
  w_DESCRI = space(35)
  w_CODORI = space(5)
  w_NORMAL = space(10)
  w_RIGA = 0
  w_MESS = space(100)
  * --- WorkFile variables
  CAR_SPEC_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Procedura per valorizzare gestione caratteri speciali 
    *     relativi a generazioni file
    *     XML
    *     EDIFACT
    * --- FIle di LOG
    this.oParentObject.w_PESEOK = .T.
    * --- Try
    local bErr_0378C960
    bErr_0378C960=bTrsErr
    this.Try_0378C960()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = Ah_MsgFormat("ERRORE GENERICO - Impossibile rinominare file allegati ")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_0378C960
    * --- End
  endproc
  proc Try_0378C960()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Read from CAR_SPEC
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CAR_SPEC_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAR_SPEC_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CACODICE"+;
        " from "+i_cTable+" CAR_SPEC where ";
            +"CACODICE = "+cp_ToStrODBC("XML");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CACODICE;
        from (i_cTable) where;
            CACODICE = "XML";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CODICE = NVL(cp_ToDate(_read_.CACODICE),cp_NullValue(_read_.CACODICE))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if i_ROWS=0
      this.w_CODICE = "XML"
      this.w_DESCRI = "Normalizzazione XML"
      this.w_RIGA = 0
      this.w_CODORI = "&"
      this.w_NORMAL = "&amp;"
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_CODORI = "<"
      this.w_NORMAL = "&lt;"
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_CODORI = ">"
      this.w_NORMAL = "&gt;"
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_CODORI = '"'
      this.w_NORMAL = "&quot;"
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_CODORI = "'"
      this.w_NORMAL = "&apos;"
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_CODORI = "�"
      this.w_NORMAL = "&#224;"
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_CODORI = "�"
      this.w_NORMAL = "&#232;"
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_CODORI = "�"
      this.w_NORMAL = "&#233;"
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_CODORI = "�"
      this.w_NORMAL = "&#236;"
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_CODORI = "�"
      this.w_NORMAL = "&#242;"
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_CODORI = "�"
      this.w_NORMAL = "&#249;"
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_CODORI = "�"
      this.w_NORMAL = "&#176;"
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_CODICE = "EDIFACT"
      this.w_DESCRI = "Normalizzazione EDIFACT"
      this.w_RIGA = 0
      this.w_CODORI = "?"
      this.w_NORMAL = "??"
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_CODORI = "'"
      this.w_NORMAL = "?'"
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_CODORI = "+"
      this.w_NORMAL = " ?+"
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_CODORI = ":"
      this.w_NORMAL = " ?:"
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_MESS = "Inserimento caratteri speciali"
    else
      this.w_MESS = "Conversione gi� eseguita correttamente su altre aziende"
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    if this.w_NHF>=0
      this.w_TMPC = Ah_MsgFormat(this.w_mess)
      this.oParentObject.w_pMSG = this.w_TMPC
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    this.oParentObject.w_PMSG = Ah_MsgFormat("Conversione eseguita correttamente")
    this.oParentObject.w_PESEOK = .T.
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_RIGA = this.w_RIGA+10
    * --- Insert into CAR_SPEC
    i_nConn=i_TableProp[this.CAR_SPEC_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAR_SPEC_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CAR_SPEC_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CACODICE"+",CACODORI"+",CANORMAL"+",CPROWORD"+",CADESCRI"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CODICE),'CAR_SPEC','CACODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODORI),'CAR_SPEC','CACODORI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NORMAL),'CAR_SPEC','CANORMAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_RIGA),'CAR_SPEC','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DESCRI),'CAR_SPEC','CADESCRI');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CACODICE',this.w_CODICE,'CACODORI',this.w_CODORI,'CANORMAL',this.w_NORMAL,'CPROWORD',this.w_RIGA,'CADESCRI',this.w_DESCRI)
      insert into (i_cTable) (CACODICE,CACODORI,CANORMAL,CPROWORD,CADESCRI &i_ccchkf. );
         values (;
           this.w_CODICE;
           ,this.w_CODORI;
           ,this.w_NORMAL;
           ,this.w_RIGA;
           ,this.w_DESCRI;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
  endproc


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='CAR_SPEC'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
