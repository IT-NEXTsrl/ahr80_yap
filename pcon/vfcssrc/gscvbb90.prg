* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvbb90                                                        *
*              Ridimensiona chiave nominativi                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_345]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-10-18                                                      *
* Last revis.: 2012-11-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvbb90",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvbb90 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_NEWKEY = space(15)
  w_LETTO = space(15)
  w_NUMERO = 0
  w_TROVATO = .f.
  w_ERRORE = .f.
  w_CNAME = space(30)
  w_CAR = 0
  w_POS = 0
  w_CODAZI = 0
  w_CONTA = 0
  w_KEYELAB = space(15)
  w_iRows = 0
  w_PK = .f.
  w_MSGERROR = space(100)
  w_MASCHERA = space(20)
  w_OPER = space(0)
  w_ARCHNAME = space(40)
  w_FLDNAME = space(10)
  w_PHNAME = space(80)
  w_NCONN = 0
  w_NULL = space(20)
  * --- WorkFile variables
  ALL_ATTR_idx=0
  ALL_EGAT_idx=0
  NOM_ATTR_idx=0
  NOM_CONT_idx=0
  OFF_ATTI_idx=0
  OFF_ATTR_idx=0
  OFF_DETT_idx=0
  OFF_ERTE_idx=0
  OFF_NOMI_idx=0
  OFF_SEZI_idx=0
  PAR_OFFE_idx=0
  PAR_TITE_idx=0
  RIPATMP1_idx=0
  RIPATMP2_idx=0
  RIPATMP3_idx=0
  RIPATMP4_idx=0
  RIPATMP5_idx=0
  RIPATMP6_idx=0
  RIPATMP7_idx=0
  RIPATMP8_idx=0
  RIS_ESTE_idx=0
  SAL_DACO_idx=0
  SCA_VARI_idx=0
  TEMPNOM_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Modifica la dimensione della chiave dei NOMINATIVI da 20 caratteri a 15 caratteri
    *     Modificando la lunghezza della chiave dei nominativi si deve anche modificare tutte le tavelle che contengono un link ai nominativi : 
    *     OFF_NOMI,NOM_ATTR,NOM_CONT,OFF_ERTE,OFF_ATTI,RIS_ESTE,,ALL_EGAT,OFF_ATTI
    * --- FIle di LOG
    this.w_MSGERROR = " "
    this.w_ERRORE = .F.
    * --- Ricostruzione preventiva delle tabelle (se le tabelle nel DB non fossero aggiornate ed avessoro dei campi in meno allora in DB2 e ORACLE la procedura fallirebbe
    this.w_OPER = "all"
    this.w_TMPC = ah_msgformat("Ricostruzione struttura campi")
    this.Pag6()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Se tutto � andato a buon fine nella tabella TMP1NOM (OLDKEY , NEWKEY)  ci sono le nuove chiavi rimappate
    * --- Try
    local bErr_04A2AE88
    bErr_04A2AE88=bTrsErr
    this.Try_04A2AE88()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = this.w_MSGERROR + Message()
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = "Elaborazione terminata con errore : " + this.oParentObject.w_PMSG
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_04A2AE88
    * --- End
    * --- Drop temporary table TMP1NOM
    i_nIdx=cp_GetTableDefIdx('TMP1NOM')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMP1NOM')
    endif
  endproc
  proc Try_04A2AE88()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.oParentObject.w_PESEOK = .T.
    if upper(CP_DBTYPE)<>"DB2"
      this.w_TMPC = ah_msgformat("Rimozione integrit� referenziale")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      this.Pag5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- begin transaction
    cp_BeginTrs()
    * --- Rimozione vincoli PRIMARY KEY
    * --- Aggiorna tabella PAR_OFFE
    * --- Select from PAR_OFFE
    i_nConn=i_TableProp[this.PAR_OFFE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_OFFE_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select POCODAZI , POMASOFF  from "+i_cTable+" PAR_OFFE ";
           ,"_Curs_PAR_OFFE")
    else
      select POCODAZI , POMASOFF from (i_cTable);
        into cursor _Curs_PAR_OFFE
    endif
    if used('_Curs_PAR_OFFE')
      select _Curs_PAR_OFFE
      locate for 1=1
      do while not(eof())
      this.w_MASCHERA = LEFT (NVL (POMASOFF,"") ,15)
      this.w_CODAZI = POCODAZI
      * --- Write into PAR_OFFE
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.PAR_OFFE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_OFFE_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_OFFE_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"POMASOFF ="+cp_NullLink(cp_ToStrODBC(this.w_MASCHERA),'PAR_OFFE','POMASOFF');
            +i_ccchkf ;
        +" where ";
            +"POCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
               )
      else
        update (i_cTable) set;
            POMASOFF = this.w_MASCHERA;
            &i_ccchkf. ;
         where;
            POCODAZI = this.w_CODAZI;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
        select _Curs_PAR_OFFE
        continue
      enddo
      use
    endif
    * --- Effettua l' aggiornamento delle tabelle inserendovi le nuove chiavi dei nominativi
    if upper(CP_DBTYPE)="SQLSERVER"
      * --- Archivi con i nominativi in chiave
      * --- Write into OFF_NOMI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="NOCODICE"
        do vq_exec with 'GSCVBF86',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_NOMI_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="OFF_NOMI.NOCODICE = _t2.NOCODICE";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"NOCODICE = _t2.NEWKEY";
            +i_ccchkf;
            +" from "+i_cTable+" OFF_NOMI, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="OFF_NOMI.NOCODICE = _t2.NOCODICE";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OFF_NOMI, "+i_cQueryTable+" _t2 set ";
            +"OFF_NOMI.NOCODICE = _t2.NEWKEY";
            +Iif(Empty(i_ccchkf),"",",OFF_NOMI.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="OFF_NOMI.NOCODICE = t2.NOCODICE";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OFF_NOMI set (";
            +"NOCODICE";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.NEWKEY";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="OFF_NOMI.NOCODICE = _t2.NOCODICE";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OFF_NOMI set ";
            +"NOCODICE = _t2.NEWKEY";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".NOCODICE = "+i_cQueryTable+".NOCODICE";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"NOCODICE = (select NEWKEY from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Write into NOM_ATTR
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.NOM_ATTR_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.NOM_ATTR_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="ANCODICE"
        do vq_exec with 'GSCVBG86',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.NOM_ATTR_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="NOM_ATTR.ANCODICE = _t2.ANCODICE";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"ANCODICE = _t2.NEWKEY";
            +i_ccchkf;
            +" from "+i_cTable+" NOM_ATTR, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="NOM_ATTR.ANCODICE = _t2.ANCODICE";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" NOM_ATTR, "+i_cQueryTable+" _t2 set ";
            +"NOM_ATTR.ANCODICE = _t2.NEWKEY";
            +Iif(Empty(i_ccchkf),"",",NOM_ATTR.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="NOM_ATTR.ANCODICE = t2.ANCODICE";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" NOM_ATTR set (";
            +"ANCODICE";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.NEWKEY";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="NOM_ATTR.ANCODICE = _t2.ANCODICE";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" NOM_ATTR set ";
            +"ANCODICE = _t2.NEWKEY";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".ANCODICE = "+i_cQueryTable+".ANCODICE";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"ANCODICE = (select NEWKEY from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Write into NOM_CONT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.NOM_CONT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.NOM_CONT_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="NCCODICE"
        do vq_exec with 'GSCVBl86',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.NOM_CONT_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="NOM_CONT.NCCODICE = _t2.NCCODICE";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"NCCODICE = _t2.NEWKEY";
            +i_ccchkf;
            +" from "+i_cTable+" NOM_CONT, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="NOM_CONT.NCCODICE = _t2.NCCODICE";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" NOM_CONT, "+i_cQueryTable+" _t2 set ";
            +"NOM_CONT.NCCODICE = _t2.NEWKEY";
            +Iif(Empty(i_ccchkf),"",",NOM_CONT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="NOM_CONT.NCCODICE = t2.NCCODICE";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" NOM_CONT set (";
            +"NCCODICE";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.NEWKEY";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="NOM_CONT.NCCODICE = _t2.NCCODICE";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" NOM_CONT set ";
            +"NCCODICE = _t2.NEWKEY";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".NCCODICE = "+i_cQueryTable+".NCCODICE";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"NCCODICE = (select NEWKEY from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Write into OFF_ERTE
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.OFF_ERTE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.OFF_ERTE_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="OFCODNOM"
        do vq_exec with 'GSCVBH86',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_ERTE_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="OFF_ERTE.OFCODNOM = _t2.OFCODNOM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"OFCODNOM = _t2.NEWKEY";
            +i_ccchkf;
            +" from "+i_cTable+" OFF_ERTE, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="OFF_ERTE.OFCODNOM = _t2.OFCODNOM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OFF_ERTE, "+i_cQueryTable+" _t2 set ";
            +"OFF_ERTE.OFCODNOM = _t2.NEWKEY";
            +Iif(Empty(i_ccchkf),"",",OFF_ERTE.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="OFF_ERTE.OFCODNOM = t2.OFCODNOM";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OFF_ERTE set (";
            +"OFCODNOM";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.NEWKEY";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="OFF_ERTE.OFCODNOM = _t2.OFCODNOM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OFF_ERTE set ";
            +"OFCODNOM = _t2.NEWKEY";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".OFCODNOM = "+i_cQueryTable+".OFCODNOM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"OFCODNOM = (select NEWKEY from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Write into OFF_ATTI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.OFF_ATTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="ATCODNOM"
        do vq_exec with 'GSCVBM86',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_ATTI_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="OFF_ATTI.ATCODNOM = _t2.ATCODNOM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"ATCODNOM = _t2.NEWKEY";
            +i_ccchkf;
            +" from "+i_cTable+" OFF_ATTI, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="OFF_ATTI.ATCODNOM = _t2.ATCODNOM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OFF_ATTI, "+i_cQueryTable+" _t2 set ";
            +"OFF_ATTI.ATCODNOM = _t2.NEWKEY";
            +Iif(Empty(i_ccchkf),"",",OFF_ATTI.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="OFF_ATTI.ATCODNOM = t2.ATCODNOM";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OFF_ATTI set (";
            +"ATCODNOM";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.NEWKEY";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="OFF_ATTI.ATCODNOM = _t2.ATCODNOM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OFF_ATTI set ";
            +"ATCODNOM = _t2.NEWKEY";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".ATCODNOM = "+i_cQueryTable+".ATCODNOM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"ATCODNOM = (select NEWKEY from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Write into RIS_ESTE
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.RIS_ESTE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.RIS_ESTE_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="SECODSES"
        do vq_exec with 'GSCVBN86',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.RIS_ESTE_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="RIS_ESTE.SECODSES = _t2.SECODSES";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SECODSES = _t2.NEWKEY";
            +i_ccchkf;
            +" from "+i_cTable+" RIS_ESTE, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="RIS_ESTE.SECODSES = _t2.SECODSES";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" RIS_ESTE, "+i_cQueryTable+" _t2 set ";
            +"RIS_ESTE.SECODSES = _t2.NEWKEY";
            +Iif(Empty(i_ccchkf),"",",RIS_ESTE.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="RIS_ESTE.SECODSES = t2.SECODSES";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" RIS_ESTE set (";
            +"SECODSES";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.NEWKEY";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="RIS_ESTE.SECODSES = _t2.SECODSES";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" RIS_ESTE set ";
            +"SECODSES = _t2.NEWKEY";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".SECODSES = "+i_cQueryTable+".SECODSES";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SECODSES = (select NEWKEY from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Write into ALL_EGAT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.ALL_EGAT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ALL_EGAT_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="ALRIFNOM"
        do vq_exec with 'GSCVBO86',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ALL_EGAT_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="ALL_EGAT.ALRIFNOM = _t2.ALRIFNOM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"ALRIFNOM = _t2.NEWKEY";
            +i_ccchkf;
            +" from "+i_cTable+" ALL_EGAT, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="ALL_EGAT.ALRIFNOM = _t2.ALRIFNOM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ALL_EGAT, "+i_cQueryTable+" _t2 set ";
            +"ALL_EGAT.ALRIFNOM = _t2.NEWKEY";
            +Iif(Empty(i_ccchkf),"",",ALL_EGAT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="ALL_EGAT.ALRIFNOM = t2.ALRIFNOM";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ALL_EGAT set (";
            +"ALRIFNOM";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.NEWKEY";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="ALL_EGAT.ALRIFNOM = _t2.ALRIFNOM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ALL_EGAT set ";
            +"ALRIFNOM = _t2.NEWKEY";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".ALRIFNOM = "+i_cQueryTable+".ALRIFNOM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"ALRIFNOM = (select NEWKEY from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Aggiorna  Tabelle di tesoreria (solo in AHE)
      *     QUESTI ARCHIVI SONO DA RICOSTRUIRE ANCHE PER ORACLE
      *     SE ARRIVO DA AD HOC PA (ANDREBBERO QUINDI GESTITI A PAG2)
      *     (PER ORA NON ABBIAMO INSTALLAZIONI DI PA CON ORACLE)
      if UPPER (g_APPLICATION) ="AD HOC ENTERPRISE"
        * --- Write into SAL_DACO
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.SAL_DACO_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SAL_DACO_idx,2])
        if i_nConn<>0
          local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
          declare i_aIndex[1]
          i_cQueryTable=cp_getTempTableName(i_nConn)
          i_aIndex(1)="SACODBEN"
          do vq_exec with 'GSCVBP86',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.SAL_DACO_idx,i_nConn)	
          i_cDB=cp_GetDatabaseType(i_nConn)
          do case
          case i_cDB="SQLServer"
            i_cWhere="SAL_DACO.SACODBEN = _t2.SACODBEN";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SACODBEN = _t2.NEWKEY";
              +i_ccchkf;
              +" from "+i_cTable+" SAL_DACO, "+i_cQueryTable+" _t2 where "+i_cWhere)
          case i_cDB="MySQL"
            i_cWhere="SAL_DACO.SACODBEN = _t2.SACODBEN";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SAL_DACO, "+i_cQueryTable+" _t2 set ";
              +"SAL_DACO.SACODBEN = _t2.NEWKEY";
              +Iif(Empty(i_ccchkf),"",",SAL_DACO.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
              +" where "+i_cWhere)
          case i_cDB="Oracle"
            i_cWhere="SAL_DACO.SACODBEN = t2.SACODBEN";
            
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SAL_DACO set (";
              +"SACODBEN";
              +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
              +"t2.NEWKEY";
              +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
              +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
          case i_cDB="PostgreSQL"
            i_cWhere="SAL_DACO.SACODBEN = _t2.SACODBEN";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SAL_DACO set ";
              +"SACODBEN = _t2.NEWKEY";
              +i_ccchkf;
              +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
          otherwise
            i_cWhere=i_cTable+".SACODBEN = "+i_cQueryTable+".SACODBEN";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SACODBEN = (select NEWKEY from "+i_cQueryTable+" where "+i_cWhere+")";
              +i_ccchkf;
              +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          endcase
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Se installazione std ENTERPRISE le 2 write sotto non 
        *     dovrebbero avere effetto in quanto
        *     il campo � stato creato solo in versioni Beta (PA)
        * --- Write into SCA_VARI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.SCA_VARI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SCA_VARI_idx,2])
        if i_nConn<>0
          local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
          declare i_aIndex[1]
          i_cQueryTable=cp_getTempTableName(i_nConn)
          i_aIndex(1)="SCCODBEN"
          do vq_exec with 'GSCVBQ86',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.SCA_VARI_idx,i_nConn)	
          i_cDB=cp_GetDatabaseType(i_nConn)
          do case
          case i_cDB="SQLServer"
            i_cWhere="SCA_VARI.SCCODBEN = _t2.SCCODBEN";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SCCODBEN = _t2.NEWKEY";
              +i_ccchkf;
              +" from "+i_cTable+" SCA_VARI, "+i_cQueryTable+" _t2 where "+i_cWhere)
          case i_cDB="MySQL"
            i_cWhere="SCA_VARI.SCCODBEN = _t2.SCCODBEN";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SCA_VARI, "+i_cQueryTable+" _t2 set ";
              +"SCA_VARI.SCCODBEN = _t2.NEWKEY";
              +Iif(Empty(i_ccchkf),"",",SCA_VARI.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
              +" where "+i_cWhere)
          case i_cDB="Oracle"
            i_cWhere="SCA_VARI.SCCODBEN = t2.SCCODBEN";
            
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SCA_VARI set (";
              +"SCCODBEN";
              +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
              +"t2.NEWKEY";
              +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
              +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
          case i_cDB="PostgreSQL"
            i_cWhere="SCA_VARI.SCCODBEN = _t2.SCCODBEN";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SCA_VARI set ";
              +"SCCODBEN = _t2.NEWKEY";
              +i_ccchkf;
              +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
          otherwise
            i_cWhere=i_cTable+".SCCODBEN = "+i_cQueryTable+".SCCODBEN";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SCCODBEN = (select NEWKEY from "+i_cQueryTable+" where "+i_cWhere+")";
              +i_ccchkf;
              +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          endcase
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Write into PAR_TITE
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.PAR_TITE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
        if i_nConn<>0
          local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
          declare i_aIndex[1]
          i_cQueryTable=cp_getTempTableName(i_nConn)
          i_aIndex(1)="PTCODBEN"
          do vq_exec with 'GSCVBR86',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_TITE_idx,i_nConn)	
          i_cDB=cp_GetDatabaseType(i_nConn)
          do case
          case i_cDB="SQLServer"
            i_cWhere="PAR_TITE.PTCODBEN = _t2.PTCODBEN";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"PTCODBEN = _t2.NEWKEY";
              +i_ccchkf;
              +" from "+i_cTable+" PAR_TITE, "+i_cQueryTable+" _t2 where "+i_cWhere)
          case i_cDB="MySQL"
            i_cWhere="PAR_TITE.PTCODBEN = _t2.PTCODBEN";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" PAR_TITE, "+i_cQueryTable+" _t2 set ";
              +"PAR_TITE.PTCODBEN = _t2.NEWKEY";
              +Iif(Empty(i_ccchkf),"",",PAR_TITE.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
              +" where "+i_cWhere)
          case i_cDB="Oracle"
            i_cWhere="PAR_TITE.PTCODBEN = t2.PTCODBEN";
            
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" PAR_TITE set (";
              +"PTCODBEN";
              +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
              +"t2.NEWKEY";
              +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
              +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
          case i_cDB="PostgreSQL"
            i_cWhere="PAR_TITE.PTCODBEN = _t2.PTCODBEN";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" PAR_TITE set ";
              +"PTCODBEN = _t2.NEWKEY";
              +i_ccchkf;
              +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
          otherwise
            i_cWhere=i_cTable+".PTCODBEN = "+i_cQueryTable+".PTCODBEN";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"PTCODBEN = (select NEWKEY from "+i_cQueryTable+" where "+i_cWhere+")";
              +i_ccchkf;
              +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          endcase
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
    else
      * --- Caso DB2/Oracle
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- Ridimensiona i campi delle tabelle sul Database
    this.w_TMPC = ah_msgformat("Imposta dimensione campo")
    this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    * --- Modifica Dimensione campo
    if upper(CP_DBTYPE)="SQLSERVER"
      this.w_ARCHNAME = "OFF_NOMI"
      this.w_FLDNAME = "NOCODICE"
      this.Pag7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_ARCHNAME = "NOM_ATTR"
      this.w_FLDNAME = "ANCODICE"
      this.Pag7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_ARCHNAME = "NOM_CONT"
      this.w_FLDNAME = "NCCODICE"
      this.Pag7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_ARCHNAME = "RIS_ESTE"
      this.w_FLDNAME = "SECODSES"
      this.Pag7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_ARCHNAME = "ALL_EGAT"
      this.w_FLDNAME = "ALRIFNOM"
      this.Pag7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_ARCHNAME = "OFF_ATTI"
      this.w_FLDNAME = "ATCODNOM"
      this.Pag7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_ARCHNAME = "OFF_ERTE"
      this.w_FLDNAME = "OFCODNOM"
      this.Pag7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_ARCHNAME = "PAR_OFFE"
      this.w_FLDNAME = "POMASOFF"
      this.Pag7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if UPPER (g_APPLICATION) ="AD HOC ENTERPRISE"
        this.w_ARCHNAME = "SAL_DACO"
        this.w_FLDNAME = "SACODBEN"
        this.Pag7()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_ARCHNAME = "PAR_TITE"
        this.w_FLDNAME = "PTCODBEN"
        this.Pag7()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_ARCHNAME = "SCA_VARI"
        this.w_FLDNAME = "SCCODBEN"
        this.Pag7()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    * --- Riscostruzione chiavi
    this.w_TMPC = ah_msgformat("Ricostruzione chiavi primarie")
    this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    if bTrsErr
      * --- Raise
      i_Error=MESSAGE()
      return
    else
      this.w_TMPC = ah_Msgformat("Adeguamento chiave primaria tabella OFF_NOMI eseguito correttamente.")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    * --- commit
    cp_EndTrs(.t.)
    if upper(CP_DBTYPE)="SQLSERVER"
      this.w_TMPC = ah_msgformat("Ricostruzione struttura tabelle (ricostruzione completa)")
      this.w_OPER = "all"
      this.Pag6()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella DB2
    pName="OFF_NOMI"
    pDatabaseType=i_ServerConn[1,6]
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.OFF_NOMI_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
          )
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    this.w_TMPC = ah_msgformat("Copiata tabella OFF_NOMI nella tabella temporanea %1" , i_cTempTable)
    this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    * --- Create temporary table RIPATMP2
    i_nIdx=cp_AddTableDef('RIPATMP2') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.NOM_ATTR_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.NOM_ATTR_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
          )
    this.RIPATMP2_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    this.w_TMPC = ah_msgformat("Copiata tabella NOM_ATTR nella tabella temporanea %1" , i_cTempTable)
    this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    * --- Create temporary table RIPATMP3
    i_nIdx=cp_AddTableDef('RIPATMP3') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.NOM_CONT_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.NOM_CONT_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
          )
    this.RIPATMP3_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    this.w_TMPC = ah_msgformat("Copiata tabella NOM_CONT nella tabella temporanea %1" , i_cTempTable)
    this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    * --- Create temporary table RIPATMP4
    i_nIdx=cp_AddTableDef('RIPATMP4') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.OFF_ERTE_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.OFF_ERTE_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
          )
    this.RIPATMP4_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    this.w_TMPC = ah_msgformat("Copiata tabella OFF_ERTE nella tabella temporanea %1" , i_cTempTable)
    this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    * --- Create temporary table RIPATMP5
    i_nIdx=cp_AddTableDef('RIPATMP5') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.OFF_ATTI_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
          )
    this.RIPATMP5_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    this.w_TMPC = ah_msgformat("Copiata tabella OFF_ATTI nella tabella temporanea %1" , i_cTempTable)
    this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    * --- Create temporary table RIPATMP6
    i_nIdx=cp_AddTableDef('RIPATMP6') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.RIS_ESTE_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.RIS_ESTE_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
          )
    this.RIPATMP6_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    this.w_TMPC = ah_msgformat("Copiata tabella RIS_ESTE nella tabella temporanea %1" , i_cTempTable)
    this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    * --- Create temporary table RIPATMP7
    i_nIdx=cp_AddTableDef('RIPATMP7') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.ALL_EGAT_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.ALL_EGAT_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
          )
    this.RIPATMP7_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    this.w_TMPC = ah_msgformat("Copiata tabella ALL_EGAT nella tabella temporanea %1" , i_cTempTable)
    this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    * --- Create temporary table RIPATMP8
    i_nIdx=cp_AddTableDef('RIPATMP8') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.PAR_OFFE_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_OFFE_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
          )
    this.RIPATMP8_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    this.w_TMPC = ah_msgformat("Copiata tabella PAR_OFFE nella tabella temporanea %1" , i_cTempTable)
    this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    this.w_OPER = "table"
    this.w_TMPC = ah_msgformat("Ricostruzione struttura campi")
    * --- Modifico le chiavi dei nominativi
    * --- Write into RIPATMP1
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.RIPATMP1_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="NOCODICE"
      do vq_exec with 'GSCVBF86',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.RIPATMP1_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="RIPATMP1.NOCODICE = _t2.NOCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"NOCODICE = _t2.NEWKEY";
          +i_ccchkf;
          +" from "+i_cTable+" RIPATMP1, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="RIPATMP1.NOCODICE = _t2.NOCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" RIPATMP1, "+i_cQueryTable+" _t2 set ";
          +"RIPATMP1.NOCODICE = _t2.NEWKEY";
          +Iif(Empty(i_ccchkf),"",",RIPATMP1.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="RIPATMP1.NOCODICE = t2.NOCODICE";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" RIPATMP1 set (";
          +"NOCODICE";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.NEWKEY";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="RIPATMP1.NOCODICE = _t2.NOCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" RIPATMP1 set ";
          +"NOCODICE = _t2.NEWKEY";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".NOCODICE = "+i_cQueryTable+".NOCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"NOCODICE = (select NEWKEY from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Write into RIPATMP2
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.RIPATMP2_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.RIPATMP2_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="ANCODICE"
      do vq_exec with 'GSCVBG86',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.RIPATMP2_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="RIPATMP2.ANCODICE = _t2.ANCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ANCODICE = _t2.NEWKEY";
          +i_ccchkf;
          +" from "+i_cTable+" RIPATMP2, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="RIPATMP2.ANCODICE = _t2.ANCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" RIPATMP2, "+i_cQueryTable+" _t2 set ";
          +"RIPATMP2.ANCODICE = _t2.NEWKEY";
          +Iif(Empty(i_ccchkf),"",",RIPATMP2.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="RIPATMP2.ANCODICE = t2.ANCODICE";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" RIPATMP2 set (";
          +"ANCODICE";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.NEWKEY";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="RIPATMP2.ANCODICE = _t2.ANCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" RIPATMP2 set ";
          +"ANCODICE = _t2.NEWKEY";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".ANCODICE = "+i_cQueryTable+".ANCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ANCODICE = (select NEWKEY from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Write into RIPATMP3
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.RIPATMP3_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.RIPATMP3_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="NCCODICE"
      do vq_exec with 'GSCVBl86',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.RIPATMP3_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="RIPATMP3.NCCODICE = _t2.NCCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"NCCODICE = _t2.NEWKEY";
          +i_ccchkf;
          +" from "+i_cTable+" RIPATMP3, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="RIPATMP3.NCCODICE = _t2.NCCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" RIPATMP3, "+i_cQueryTable+" _t2 set ";
          +"RIPATMP3.NCCODICE = _t2.NEWKEY";
          +Iif(Empty(i_ccchkf),"",",RIPATMP3.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="RIPATMP3.NCCODICE = t2.NCCODICE";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" RIPATMP3 set (";
          +"NCCODICE";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.NEWKEY";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="RIPATMP3.NCCODICE = _t2.NCCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" RIPATMP3 set ";
          +"NCCODICE = _t2.NEWKEY";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".NCCODICE = "+i_cQueryTable+".NCCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"NCCODICE = (select NEWKEY from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Write into RIPATMP4
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.RIPATMP4_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.RIPATMP4_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="OFCODNOM"
      do vq_exec with 'GSCVBH86',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.RIPATMP4_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="RIPATMP4.OFCODNOM = _t2.OFCODNOM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"OFCODNOM = _t2.NEWKEY";
          +i_ccchkf;
          +" from "+i_cTable+" RIPATMP4, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="RIPATMP4.OFCODNOM = _t2.OFCODNOM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" RIPATMP4, "+i_cQueryTable+" _t2 set ";
          +"RIPATMP4.OFCODNOM = _t2.NEWKEY";
          +Iif(Empty(i_ccchkf),"",",RIPATMP4.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="RIPATMP4.OFCODNOM = t2.OFCODNOM";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" RIPATMP4 set (";
          +"OFCODNOM";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.NEWKEY";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="RIPATMP4.OFCODNOM = _t2.OFCODNOM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" RIPATMP4 set ";
          +"OFCODNOM = _t2.NEWKEY";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".OFCODNOM = "+i_cQueryTable+".OFCODNOM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"OFCODNOM = (select NEWKEY from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Write into RIPATMP5
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.RIPATMP5_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.RIPATMP5_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="ATCODNOM"
      do vq_exec with 'GSCVBM86',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.RIPATMP5_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="RIPATMP5.ATCODNOM = _t2.ATCODNOM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ATCODNOM = _t2.NEWKEY";
          +i_ccchkf;
          +" from "+i_cTable+" RIPATMP5, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="RIPATMP5.ATCODNOM = _t2.ATCODNOM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" RIPATMP5, "+i_cQueryTable+" _t2 set ";
          +"RIPATMP5.ATCODNOM = _t2.NEWKEY";
          +Iif(Empty(i_ccchkf),"",",RIPATMP5.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="RIPATMP5.ATCODNOM = t2.ATCODNOM";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" RIPATMP5 set (";
          +"ATCODNOM";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.NEWKEY";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="RIPATMP5.ATCODNOM = _t2.ATCODNOM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" RIPATMP5 set ";
          +"ATCODNOM = _t2.NEWKEY";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".ATCODNOM = "+i_cQueryTable+".ATCODNOM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ATCODNOM = (select NEWKEY from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Write into RIPATMP6
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.RIPATMP6_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.RIPATMP6_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="SECODSES"
      do vq_exec with 'GSCVBN86',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.RIPATMP6_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="RIPATMP6.SECODSES = _t2.SECODSES";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SECODSES = _t2.NEWKEY";
          +i_ccchkf;
          +" from "+i_cTable+" RIPATMP6, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="RIPATMP6.SECODSES = _t2.SECODSES";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" RIPATMP6, "+i_cQueryTable+" _t2 set ";
          +"RIPATMP6.SECODSES = _t2.NEWKEY";
          +Iif(Empty(i_ccchkf),"",",RIPATMP6.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="RIPATMP6.SECODSES = t2.SECODSES";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" RIPATMP6 set (";
          +"SECODSES";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.NEWKEY";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="RIPATMP6.SECODSES = _t2.SECODSES";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" RIPATMP6 set ";
          +"SECODSES = _t2.NEWKEY";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".SECODSES = "+i_cQueryTable+".SECODSES";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SECODSES = (select NEWKEY from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Write into RIPATMP7
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.RIPATMP7_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.RIPATMP7_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="ALRIFNOM"
      do vq_exec with 'GSCVBO86',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.RIPATMP7_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="RIPATMP7.ALRIFNOM = _t2.ALRIFNOM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ALRIFNOM = _t2.NEWKEY";
          +i_ccchkf;
          +" from "+i_cTable+" RIPATMP7, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="RIPATMP7.ALRIFNOM = _t2.ALRIFNOM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" RIPATMP7, "+i_cQueryTable+" _t2 set ";
          +"RIPATMP7.ALRIFNOM = _t2.NEWKEY";
          +Iif(Empty(i_ccchkf),"",",RIPATMP7.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="RIPATMP7.ALRIFNOM = t2.ALRIFNOM";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" RIPATMP7 set (";
          +"ALRIFNOM";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.NEWKEY";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="RIPATMP7.ALRIFNOM = _t2.ALRIFNOM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" RIPATMP7 set ";
          +"ALRIFNOM = _t2.NEWKEY";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".ALRIFNOM = "+i_cQueryTable+".ALRIFNOM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ALRIFNOM = (select NEWKEY from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Elimino la tabella
    if Not gscv2bdt( this, "OFF_NOMI" , i_nConn )
      * --- Raise
      i_Error="Error in eliminazione OFF_NOMI"
      return
    endif
    i_nConn=i_TableProp[this.OFF_NOMI_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2])
    GSCV_BRT(this, "OFF_NOMI" , i_nConn , .T. , this.w_OPER,"S" )
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into OFF_NOMI
    i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSCVBBO1",this.OFF_NOMI_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    if Not gscv2bdt( this, "NOM_ATTR" , i_nConn )
      * --- Raise
      i_Error="Error in eliminazione NOM_ATTR"
      return
    endif
    i_nConn=i_TableProp[this.NOM_ATTR_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.NOM_ATTR_idx,2])
    GSCV_BRT(this, "NOM_ATTR" , i_nConn , .T. , this.w_OPER,"S")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into NOM_ATTR
    i_nConn=i_TableProp[this.NOM_ATTR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.NOM_ATTR_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSCVBBO2",this.NOM_ATTR_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    if Not gscv2bdt( this, "NOM_CONT" , i_nConn )
      * --- Raise
      i_Error="Error in eliminazione NOM_CONT"
      return
    endif
    i_nConn=i_TableProp[this.NOM_CONT_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.NOM_CONT_idx,2])
    GSCV_BRT(this, "NOM_CONT" , i_nConn , .T. , this.w_OPER,"S")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into NOM_CONT
    i_nConn=i_TableProp[this.NOM_CONT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.NOM_CONT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSCVBBO3",this.NOM_CONT_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    if Not gscv2bdt( this, "OFF_ERTE" , i_nConn )
      * --- Raise
      i_Error="Error in eliminazione OFF_ERTE"
      return
    endif
    i_nConn=i_TableProp[this.OFF_ERTE_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.OFF_ERTE_idx,2])
    GSCV_BRT(this, "OFF_ERTE" , i_nConn , .T. , this.w_OPER,"S")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into OFF_ERTE
    i_nConn=i_TableProp[this.OFF_ERTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFF_ERTE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSCVBBO4",this.OFF_ERTE_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    if Not gscv2bdt( this, "OFF_ATTI" , i_nConn )
      * --- Raise
      i_Error="Error in eliminazione OFF_ATTI"
      return
    endif
    i_nConn=i_TableProp[this.OFF_ATTI_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2])
    GSCV_BRT(this, "OFF_ATTI" , i_nConn , .T., this.w_OPER,"S")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into OFF_ATTI
    i_nConn=i_TableProp[this.OFF_ATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSCVBBO5",this.OFF_ATTI_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    if Not gscv2bdt( this, "RIS_ESTE" , i_nConn )
      * --- Raise
      i_Error="Error in eliminazione RIS_ESTE"
      return
    endif
    i_nConn=i_TableProp[this.RIS_ESTE_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.RIS_ESTE_idx,2])
    GSCV_BRT(this, "RIS_ESTE" , i_nConn , .T., this.w_OPER,"S")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into RIS_ESTE
    i_nConn=i_TableProp[this.RIS_ESTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.RIS_ESTE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSCVBBO6",this.RIS_ESTE_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    if Not gscv2bdt( this, "ALL_EGAT" , i_nConn )
      * --- Raise
      i_Error="Error in eliminazione ALL_EGAT"
      return
    endif
    i_nConn=i_TableProp[this.ALL_EGAT_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.ALL_EGAT_idx,2])
    GSCV_BRT(this, "ALL_EGAT" , i_nConn , .T., this.w_OPER,"S")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into ALL_EGAT
    i_nConn=i_TableProp[this.ALL_EGAT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ALL_EGAT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSCVBBO7",this.ALL_EGAT_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    if Not gscv2bdt( this, "PAR_OFFE" , i_nConn )
      * --- Raise
      i_Error="Error in eliminazione PAR_OFFE"
      return
    endif
    i_nConn=i_TableProp[this.PAR_OFFE_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.PAR_OFFE_idx,2])
    GSCV_BRT(this, "PAR_OFFE" , i_nConn , .T. , this.w_OPER,"S")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into PAR_OFFE
    i_nConn=i_TableProp[this.PAR_OFFE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_OFFE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSCVBBO8",this.PAR_OFFE_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_Msg("Ripristinati valori di origine",.T.)
    * --- Ripristina tutti i vincoli di iontegrita
    this.w_OPER = "all"
    this.w_TMPC = ah_msgformat("Ricostruzione struttura tabelle (ricostruzione completa)")
    this.Pag6()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    * --- Drop temporary table RIPATMP2
    i_nIdx=cp_GetTableDefIdx('RIPATMP2')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP2')
    endif
    * --- Drop temporary table RIPATMP3
    i_nIdx=cp_GetTableDefIdx('RIPATMP3')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP3')
    endif
    * --- Drop temporary table RIPATMP4
    i_nIdx=cp_GetTableDefIdx('RIPATMP4')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP4')
    endif
    * --- Drop temporary table RIPATMP5
    i_nIdx=cp_GetTableDefIdx('RIPATMP5')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP5')
    endif
    * --- Drop temporary table RIPATMP6
    i_nIdx=cp_GetTableDefIdx('RIPATMP6')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP6')
    endif
    * --- Drop temporary table RIPATMP7
    i_nIdx=cp_GetTableDefIdx('RIPATMP7')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP7')
    endif
    * --- Drop temporary table RIPATMP8
    i_nIdx=cp_GetTableDefIdx('RIPATMP8')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP8')
    endif
    ah_Msg("Eliminata tabella temporanea tmp",.T.)
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Valorizza la tabella TEMPNOM (OLDKEY , NEWKEY). La tabella conterr� le nuove chiavi ottenute eliminando 5 caratteri dalle vecchie chiavi.
    *     La trasformazione si baser� su:
    *     Se gli ultimi 5 char sono spazi trimmo a destra left(campo,15)
    *     Se i primi 5 char sono 0 right(campo,15)
    *     Altrimenti Left(campo,15)
    *     
    *     Al termine occorre eseguire su questo temporaneo lato server una query per verificare l'univocit� del nuovo campo.
    *     
    *     Se alcune chiavi sono doppie parte un procedimento di modifica del carattere pi� a destra cercando, se non si trova una modifica che permetta di ottenere una chiave univoca allora prova a modificare il carattere successivo
    * --- Legge il campo chiave dei NOMINATIVI e lo salva in una tabella temporanea  (le chiavi dei nominativi dovranno essere trasformate da 20 caratteri a 15 caratteri perci� potrebbe essere necessario modificare la chiave 
    this.w_TMPC = ah_msgformat("Calcolo nuove chiavi dei nominativi:")
    this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    * --- Create temporary table TEMPNOM
    i_nIdx=cp_AddTableDef('TEMPNOM') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('GSCVBZ86',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TEMPNOM_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Calcola la nuova chiave
    *     - se gli ultimi 5 caratteri sono spazi allora si tengono i primi 15 caratteri che verranno usati come nuova chiave
    * --- Write into TEMPNOM
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TEMPNOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TEMPNOM_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="OLDKEY"
      do vq_exec with 'GSCVBB86',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TEMPNOM_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="TEMPNOM.OLDKEY = _t2.OLDKEY";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"NEWKEY = _t2.NEWKEY";
          +i_ccchkf;
          +" from "+i_cTable+" TEMPNOM, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="TEMPNOM.OLDKEY = _t2.OLDKEY";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TEMPNOM, "+i_cQueryTable+" _t2 set ";
          +"TEMPNOM.NEWKEY = _t2.NEWKEY";
          +Iif(Empty(i_ccchkf),"",",TEMPNOM.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="TEMPNOM.OLDKEY = t2.OLDKEY";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TEMPNOM set (";
          +"NEWKEY";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.NEWKEY";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="TEMPNOM.OLDKEY = _t2.OLDKEY";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TEMPNOM set ";
          +"NEWKEY = _t2.NEWKEY";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".OLDKEY = "+i_cQueryTable+".OLDKEY";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"NEWKEY = (select NEWKEY from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Per i record per cui non � stato calcolata una nuova chiave al passo precedente
    *     - se i primi 5 caratteri sono '00000' allora si tengono gli ultimi 15 caratteri che verranno usati come nuova chiave
    * --- Write into TEMPNOM
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TEMPNOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TEMPNOM_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="OLDKEY"
      do vq_exec with 'GSCVBA86',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TEMPNOM_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="TEMPNOM.OLDKEY = _t2.OLDKEY";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"NEWKEY = _t2.NEWKEY";
          +i_ccchkf;
          +" from "+i_cTable+" TEMPNOM, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="TEMPNOM.OLDKEY = _t2.OLDKEY";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TEMPNOM, "+i_cQueryTable+" _t2 set ";
          +"TEMPNOM.NEWKEY = _t2.NEWKEY";
          +Iif(Empty(i_ccchkf),"",",TEMPNOM.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="TEMPNOM.OLDKEY = t2.OLDKEY";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TEMPNOM set (";
          +"NEWKEY";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.NEWKEY";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="TEMPNOM.OLDKEY = _t2.OLDKEY";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TEMPNOM set ";
          +"NEWKEY = _t2.NEWKEY";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".OLDKEY = "+i_cQueryTable+".OLDKEY";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"NEWKEY = (select NEWKEY from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Per i record per cui non � stato calcolata una nuova chiave ai passi precedente
    *     - si tengono ii primi 15 caratteri che verranno usati come nuova chiave
    * --- Write into TEMPNOM
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TEMPNOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TEMPNOM_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="OLDKEY"
      do vq_exec with 'GSCVBC86',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TEMPNOM_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="TEMPNOM.OLDKEY = _t2.OLDKEY";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"NEWKEY = _t2.NEWKEY";
          +i_ccchkf;
          +" from "+i_cTable+" TEMPNOM, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="TEMPNOM.OLDKEY = _t2.OLDKEY";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TEMPNOM, "+i_cQueryTable+" _t2 set ";
          +"TEMPNOM.NEWKEY = _t2.NEWKEY";
          +Iif(Empty(i_ccchkf),"",",TEMPNOM.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="TEMPNOM.OLDKEY = t2.OLDKEY";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TEMPNOM set (";
          +"NEWKEY";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.NEWKEY";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="TEMPNOM.OLDKEY = _t2.OLDKEY";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TEMPNOM set ";
          +"NEWKEY = _t2.NEWKEY";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".OLDKEY = "+i_cQueryTable+".OLDKEY";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"NEWKEY = (select NEWKEY from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Alcune delle chiavi ottenute ai passaggi precedenti potrebbero essere doppie percio se esistono chiavi doppie allora bisogna rinominarle
    * --- IN SEGUITO AL CALCOLO DELLE NUOVE CHIAVI SI POTREBBERO ESSERE OTTENUTI DEI DUPLICATI PERCIO' LA PROCEDURA TENTA DI MODIFICARE LE CHIAVI DOPPIE
    this.w_TMPC = ah_msgformat("Ricalcolo chiavi doppie :")
    this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    * --- Select from GSCVBE86
    do vq_exec with 'GSCVBE86',this,'_Curs_GSCVBE86','',.f.,.t.
    if used('_Curs_GSCVBE86')
      select _Curs_GSCVBE86
      locate for 1=1
      do while not(eof())
      this.w_TROVATO = .f.
      * --- CICLO FOR ESTERNO - cicla sui 15 caratteri della nuova chiave. Il carattere alla posizione POS � il carattere della chiave su cui si tenteranno le modifiche per costruire una nuova chiave univoca
      this.w_POS = 1
      do while this.w_POS<=15 AND NOT (this.w_TROVATO)
        * --- 1� CICLO - Cerca di sostituire con un numero il carattere puntato dalla variabile POS per costruire la nuova chiave
        this.w_CAR = 0
        do while this.w_CAR<=9 AND NOT (this.w_TROVATO)
          this.w_NEWKEY = left (NEWKEY,15 - this.w_POS) +ALLTRIM (str (this.w_CAR))+ left (NEWKEY, this.w_POS-1) 
          * --- Read from TEMPNOM
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.TEMPNOM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TEMPNOM_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "OLDKEY"+;
              " from "+i_cTable+" TEMPNOM where ";
                  +"NEWKEY = "+cp_ToStrODBC(this.w_NEWKEY);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              OLDKEY;
              from (i_cTable) where;
                  NEWKEY = this.w_NEWKEY;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_LETTO = NVL(cp_ToDate(_read_.OLDKEY),cp_NullValue(_read_.OLDKEY))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if i_Rows=0
            * --- Write into TEMPNOM
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.TEMPNOM_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TEMPNOM_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.TEMPNOM_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"NEWKEY ="+cp_NullLink(cp_ToStrODBC(this.w_NEWKEY),'TEMPNOM','NEWKEY');
                  +i_ccchkf ;
              +" where ";
                  +"OLDKEY = "+cp_ToStrODBC(_Curs_GSCVBE86.OLDKEY);
                     )
            else
              update (i_cTable) set;
                  NEWKEY = this.w_NEWKEY;
                  &i_ccchkf. ;
               where;
                  OLDKEY = _Curs_GSCVBE86.OLDKEY;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            this.w_TROVATO = .T.
          endif
          this.w_CAR = this.w_CAR + 1
        enddo
        * --- 2� CICLO - Cerca di sostituire con una lettera il carattere puntato dalla variabile POS per costruire la nuova chiave
        this.w_CAR = 65
        do while this.w_CAR<=90 AND NOT (this.w_TROVATO)
          this.w_NEWKEY = left (NEWKEY,15 - this.w_POS) +ALLTRIM (CHR(this.w_CAR))+ left (NEWKEY, this.w_POS-1) 
          * --- Read from TEMPNOM
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.TEMPNOM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TEMPNOM_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "OLDKEY"+;
              " from "+i_cTable+" TEMPNOM where ";
                  +"NEWKEY = "+cp_ToStrODBC(this.w_NEWKEY);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              OLDKEY;
              from (i_cTable) where;
                  NEWKEY = this.w_NEWKEY;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_LETTO = NVL(cp_ToDate(_read_.OLDKEY),cp_NullValue(_read_.OLDKEY))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if i_Rows=0
            * --- Write into TEMPNOM
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.TEMPNOM_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TEMPNOM_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.TEMPNOM_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"NEWKEY ="+cp_NullLink(cp_ToStrODBC(this.w_NEWKEY),'TEMPNOM','NEWKEY');
                  +i_ccchkf ;
              +" where ";
                  +"OLDKEY = "+cp_ToStrODBC(_Curs_GSCVBE86.OLDKEY);
                     )
            else
              update (i_cTable) set;
                  NEWKEY = this.w_NEWKEY;
                  &i_ccchkf. ;
               where;
                  OLDKEY = _Curs_GSCVBE86.OLDKEY;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            this.w_TROVATO = .T.
          endif
          this.w_CAR = this.w_CAR + 1
        enddo
        this.w_POS = this.w_POS + 1
      enddo
      * --- Se non si � trovata una nuova chiave per il record corrente allora si avvisa l utente indicandogli le chiavi dei nominativi per cui non si � riusciti a trovare una nuova chiave
      if NOT (this.w_TROVATO)
        this.w_ERRORE = .T.
        this.w_TMPC = ah_msgformat("ERRORE : Il nominativo %1 � stato trasformato ottenendo %3 chiavi identiche  - La Procedura non � riuscita  rinominare la  chiave in modo da preservarne l' univocita", OLDKEY, this.w_NEWKEY, NUMERO)
        this.oParentObject.w_PMSG = this.w_TMPC
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      else
        this.w_TMPC = ah_msgformat(" Il nominativo %1 � stato trasformato in %2 ottenendo una nuova chiave univoca", OLDKEY, this.w_NEWKEY)
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
      if this.w_ERRORE
        this.w_TMPC = ah_msgformat("Impossibile ricalcolare alcune chiavi , la procedura non verr�  eseguita")
        this.oParentObject.w_PMSG = this.w_TMPC
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        i_retcode = 'stop'
        return
      endif
      this.w_KEYELAB = NEWKEY
        select _Curs_GSCVBE86
        continue
      enddo
      use
    endif
    if this.w_ERRORE=.F.
      this.w_TMPC = ah_msgformat("Riassunto trasformazioni effettuate sulle chiavi dei nominativi : ")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      * --- Select from TEMPNOM
      i_nConn=i_TableProp[this.TEMPNOM_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TEMPNOM_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select *  from "+i_cTable+" TEMPNOM ";
            +" where NEWKEY<>OLDKEY";
             ,"_Curs_TEMPNOM")
      else
        select * from (i_cTable);
         where NEWKEY<>OLDKEY;
          into cursor _Curs_TEMPNOM
      endif
      if used('_Curs_TEMPNOM')
        select _Curs_TEMPNOM
        locate for 1=1
        do while not(eof())
        this.w_TMPC = ah_msgformat("Il nominativo %1 � stato trasformato nel nominativo %2 ", OLDKEY, NEWKEY)
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
          select _Curs_TEMPNOM
          continue
        enddo
        use
      endif
    endif
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Rimuove integrit� referenziale
    GSCV_BIN(this, this.w_CODAZI, this.w_CNAME , this.w_NHF, IIF( this.w_PK , "S" ,"N"))
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if lower(i_TableProp[ cp_TablePropScan( Alltrim( this.w_CNAME ) ) , 2 ])<>lower(i_TableProp[ cp_TablePropScan( Alltrim( this.w_CNAME ) ) , 1 ])
      * --- Se tabelle multicomnay elimino anche per la XXX
      GSCV_BIN(this, "xxx", this.w_CNAME , this.w_NHF, IIF( this.w_PK , "S" ,"N") )
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  endproc


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elimina i vincoli di integrita delle tabelle su cui si dovranno modificare i nominativi 
    * --- In Oracle non riesce a cancellare la tabella OFF_ERTE e ALL_EGAT se altre tabelle vi fanno riferimento
    this.w_CODAZI = i_CODAZI
    * --- Se .f. non distruggo la chiave primaria, la chiave primaria va distrutta
    *     se il campo in modifica appartiene alla chiave...
    this.w_PK = .F.
    if UPPER (g_APPLICATION) ="AD HOC ENTERPRISE"
      this.w_CNAME = "SCA_VARI"
      this.Pag4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    this.w_CNAME = "RIS_ESTE"
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- In Oracle non riesce a cancellare la tabella OFF_ERTE  e  ALL_EGAT
    *     per cui rimuovo anche la chiave
    this.w_PK = upper(CP_DBTYPE)="ORACLE"
    this.w_CNAME = "ALL_EGAT"
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_CNAME = "OFF_ERTE"
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Tabelle che hanno in chiave codice nominativo (devo rimuovere anche la chiave)
    this.w_PK = .T.
    this.w_CNAME = "NOM_ATTR"
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_CNAME = "NOM_CONT"
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_CNAME = "OFF_NOMI"
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_CNAME = "OFF_ATTI"
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Per oracle, per poter distruggere le tabelle e ricrearle occorre eliminare
    *     tutte le intergrit� attorno...
    if upper(CP_DBTYPE)="ORACLE"
      this.w_CNAME = "PAR_OFFE"
      this.Pag4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if UPPER (g_APPLICATION) ="AD HOC ENTERPRISE"
        this.w_CNAME = "SAL_DACO"
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_CNAME = "PAR_TITE"
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
  endproc


  procedure Pag6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Ricostruisco struttura database per le tabella modificate
    this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
    GSCV_BRT(this, "OFF_NOMI" , i_nConn , .T. , this.w_OPER,"S" )
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    i_nConn=i_TableProp[this.NOM_ATTR_idx,3]
    GSCV_BRT(this, "NOM_ATTR" , i_nConn , .T. , this.w_OPER,"S")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    i_nConn=i_TableProp[this.NOM_CONT_idx,3]
    GSCV_BRT(this, "NOM_CONT" , i_nConn , .T. , this.w_OPER,"S")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    i_nConn=i_TableProp[this.OFF_ERTE_idx,3]
    GSCV_BRT(this, "OFF_ERTE" , i_nConn , .T. , this.w_OPER,"S")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    i_nConn=i_TableProp[this.OFF_ATTI_idx,3]
    GSCV_BRT(this, "OFF_ATTI" , i_nConn , .T., this.w_OPER,"S")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    i_nConn=i_TableProp[this.RIS_ESTE_idx,3]
    GSCV_BRT(this, "RIS_ESTE" , i_nConn , .T., this.w_OPER,"S")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    i_nConn=i_TableProp[this.ALL_EGAT_idx,3]
    GSCV_BRT(this, "ALL_EGAT" , i_nConn , .T., this.w_OPER,"S")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    i_nConn=i_TableProp[this.PAR_OFFE_idx,3]
    GSCV_BRT(this, "PAR_OFFE" , i_nConn , .T. , this.w_OPER,"S")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if UPPER (g_APPLICATION) ="AD HOC ENTERPRISE"
      i_nConn=i_TableProp[this.SCA_VARI_idx,3]
      GSCV_BRT(this, "SCA_VARI" , i_nConn , .T. , this.w_OPER,"S")
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      i_nConn=i_TableProp[this.SAL_DACO_idx,3]
      GSCV_BRT(this, "SAL_DACO" , i_nConn , .T., this.w_OPER,"S")
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      i_nConn=i_TableProp[this.PAR_TITE_idx,3]
      GSCV_BRT(this, "PAR_TITE" , i_nConn , .T. , this.w_OPER,"S")
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  endproc


  procedure Pag7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Modifica lunghezza campo....
    *     Riceve archivio e campo da aggiornare
    this.w_PHNAME = cp_SetAzi(i_TableProp[ cp_TablePropScan( Alltrim( this.w_ARCHNAME ) ) , 2 ])
    this.w_NCONN = i_TableProp[ cp_TablePropScan( Alltrim( this.w_ARCHNAME ) ) , 3 ]
    if not empty( this.w_PHNAME ) 
      * --- Se il campo fa parte della chiave dell'archivio aggiungo NOT NULL
      if this.w_FLDNAME$cp_KeyToSQL ( I_DCX.GetIdxDef( this.w_ARCHNAME ,1 ) )
        this.w_NULL = " NOT NULL"
      else
        this.w_NULL = ""
      endif
      this.w_iRows = cp_sqlexec( this.w_NCONN ,"alter table "+this.w_PHNAME+" alter column "+this.w_FLDNAME+" char(15) "+this.w_NULL)
      if this.w_iRows=-1
        this.w_MSGERROR = "Allarga campo in " + this.w_ARCHNAME
        * --- Raise
        i_Error=MESSAGE()
        return
      else
        * --- Se archivio aziendale ricostruisco anche l'azienda XXX
        if Lower(i_TableProp[ cp_TablePropScan( Alltrim( this.w_ARCHNAME ) ) , 2 ])<>lower(i_TableProp[ cp_TablePropScan( Alltrim( this.w_ARCHNAME ) ) , 1 ])
          this.w_iRows = cp_sqlexec( this.w_NCONN ,"alter table "+i_TableProp[ cp_TablePropScan( Alltrim( this.w_ARCHNAME ) ) , 2 ]+" alter column "+this.w_FLDNAME+" char(15) "+this.w_NULL)
        endif
      endif
    endif
  endproc


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,24)]
    this.cWorkTables[1]='ALL_ATTR'
    this.cWorkTables[2]='ALL_EGAT'
    this.cWorkTables[3]='NOM_ATTR'
    this.cWorkTables[4]='NOM_CONT'
    this.cWorkTables[5]='OFF_ATTI'
    this.cWorkTables[6]='OFF_ATTR'
    this.cWorkTables[7]='OFF_DETT'
    this.cWorkTables[8]='OFF_ERTE'
    this.cWorkTables[9]='OFF_NOMI'
    this.cWorkTables[10]='OFF_SEZI'
    this.cWorkTables[11]='PAR_OFFE'
    this.cWorkTables[12]='PAR_TITE'
    this.cWorkTables[13]='*RIPATMP1'
    this.cWorkTables[14]='*RIPATMP2'
    this.cWorkTables[15]='*RIPATMP3'
    this.cWorkTables[16]='*RIPATMP4'
    this.cWorkTables[17]='*RIPATMP5'
    this.cWorkTables[18]='*RIPATMP6'
    this.cWorkTables[19]='*RIPATMP7'
    this.cWorkTables[20]='*RIPATMP8'
    this.cWorkTables[21]='RIS_ESTE'
    this.cWorkTables[22]='SAL_DACO'
    this.cWorkTables[23]='SCA_VARI'
    this.cWorkTables[24]='*TEMPNOM'
    return(this.OpenAllTables(24))

  proc CloseCursors()
    if used('_Curs_PAR_OFFE')
      use in _Curs_PAR_OFFE
    endif
    if used('_Curs_GSCVBE86')
      use in _Curs_GSCVBE86
    endif
    if used('_Curs_TEMPNOM')
      use in _Curs_TEMPNOM
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
