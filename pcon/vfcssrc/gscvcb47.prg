* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcb47                                                        *
*              Inserisce data inizio giudici                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-03-19                                                      *
* Last revis.: 2009-05-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcb47",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcb47 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_PRATICA = space(15)
  w_LASTROWNUM = 0
  w_DATINI = ctod("  /  /  ")
  w_TIPCAT = space(1)
  * --- WorkFile variables
  RIS_ESTE_idx=0
  CAT_SOGG_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Valorizza intervallo validità giudici in fascicolo pratiche
    *     Aggiorna i campi SEDATINI e SEDATFIN in RIS_ESTE
    * --- Try
    local bErr_0361DF70
    bErr_0361DF70=bTrsErr
    this.Try_0361DF70()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("Errore generico - impossibile eseguire aggiornamento richiesto")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_0361DF70
    * --- End
  endproc
  proc Try_0361DF70()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- La procedura si preoccupa di valorizzare data inizio e fine
    *     validiità dei giudici.
    * --- Select from RIS_ESTE
    i_nConn=i_TableProp[this.RIS_ESTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.RIS_ESTE_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" RIS_ESTE ";
          +" order by SECODPRA, CPROWORD";
           ,"_Curs_RIS_ESTE")
    else
      select * from (i_cTable);
       order by SECODPRA, CPROWORD;
        into cursor _Curs_RIS_ESTE
    endif
    if used('_Curs_RIS_ESTE')
      select _Curs_RIS_ESTE
      locate for 1=1
      do while not(eof())
      * --- Ciclo non ottimizzato (l'ottimo sarebbe stata una query
      *     per non introdure una nuova query (cmq i record recuperati
      *     non sono coś rilevanti).
      * --- Read from CAT_SOGG
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CAT_SOGG_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAT_SOGG_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CSTIPCAT"+;
          " from "+i_cTable+" CAT_SOGG where ";
              +"CSCODCAT = "+cp_ToStrODBC(_Curs_RIS_ESTE.SECODCSE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CSTIPCAT;
          from (i_cTable) where;
              CSCODCAT = _Curs_RIS_ESTE.SECODCSE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_TIPCAT = NVL(cp_ToDate(_read_.CSTIPCAT),cp_NullValue(_read_.CSTIPCAT))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_TIPCAT="G"
        if this.w_PRATICA<>_Curs_RIS_ESTE.SECODPRA
          if this.w_LASTROWNUM<>0
            * --- Write into RIS_ESTE
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.RIS_ESTE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.RIS_ESTE_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.RIS_ESTE_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SEDATINI ="+cp_NullLink(cp_ToStrODBC(this.w_DATINI),'RIS_ESTE','SEDATINI');
              +",SEDATFIN ="+cp_NullLink(cp_ToStrODBC(i_findat),'RIS_ESTE','SEDATFIN');
                  +i_ccchkf ;
              +" where ";
                  +"SECODPRA = "+cp_ToStrODBC(this.w_PRATICA);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.w_LASTROWNUM);
                     )
            else
              update (i_cTable) set;
                  SEDATINI = this.w_DATINI;
                  ,SEDATFIN = i_findat;
                  &i_ccchkf. ;
               where;
                  SECODPRA = this.w_PRATICA;
                  and CPROWNUM = this.w_LASTROWNUM;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
          * --- ricomincio...
          this.w_PRATICA = _Curs_RIS_ESTE.SECODPRA
          this.w_DATINI = i_INIDAT
        endif
        this.w_LASTROWNUM = _Curs_RIS_ESTE.CPROWNUM
        * --- Write into RIS_ESTE
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.RIS_ESTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.RIS_ESTE_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.RIS_ESTE_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SEDATINI ="+cp_NullLink(cp_ToStrODBC(this.w_DATINI),'RIS_ESTE','SEDATINI');
          +",SEDATFIN ="+cp_NullLink(cp_ToStrODBC(this.w_DATINI),'RIS_ESTE','SEDATFIN');
              +i_ccchkf ;
          +" where ";
              +"SECODPRA = "+cp_ToStrODBC(this.w_PRATICA);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_LASTROWNUM);
                 )
        else
          update (i_cTable) set;
              SEDATINI = this.w_DATINI;
              ,SEDATFIN = this.w_DATINI;
              &i_ccchkf. ;
           where;
              SECODPRA = this.w_PRATICA;
              and CPROWNUM = this.w_LASTROWNUM;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        this.w_DATINI = this.w_DATINI +1 
      endif
        select _Curs_RIS_ESTE
        continue
      enddo
      use
    endif
    * --- Aggiorno l'ultimo...
    * --- Write into RIS_ESTE
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.RIS_ESTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.RIS_ESTE_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.RIS_ESTE_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"SEDATINI ="+cp_NullLink(cp_ToStrODBC(this.w_DATINI),'RIS_ESTE','SEDATINI');
      +",SEDATFIN ="+cp_NullLink(cp_ToStrODBC(i_findat),'RIS_ESTE','SEDATFIN');
          +i_ccchkf ;
      +" where ";
          +"SECODPRA = "+cp_ToStrODBC(this.w_PRATICA);
          +" and CPROWNUM = "+cp_ToStrODBC(this.w_LASTROWNUM);
             )
    else
      update (i_cTable) set;
          SEDATINI = this.w_DATINI;
          ,SEDATFIN = i_findat;
          &i_ccchkf. ;
       where;
          SECODPRA = this.w_PRATICA;
          and CPROWNUM = this.w_LASTROWNUM;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    this.w_TMPC = ah_Msgformat("Aggiornamento tabella RIS_ESTE eseguito con successo")
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = this.w_TMPC
    * --- commit
    cp_EndTrs(.t.)
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='RIS_ESTE'
    this.cWorkTables[2]='CAT_SOGG'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_RIS_ESTE')
      use in _Curs_RIS_ESTE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
