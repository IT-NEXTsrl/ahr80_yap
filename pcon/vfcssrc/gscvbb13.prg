* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvbb13                                                        *
*              Valorizza mvimpcom ricavi commessa                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_107]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-02-27                                                      *
* Last revis.: 2003-02-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvbb13",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvbb13 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_SAMEVAL = space(1)
  w_NROWS = 0
  w_IMPCOM = 0
  * --- WorkFile variables
  DOC_DETT_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna il valore MVIMPCOM per i ricavi di commessa evasi. Quindi
    *     per le righe di documenti con flag evaso MVIMPCOM=0 MVIMPNAZ<>0 e con commessa.
    *     Inoltre non devono movimentare l'impeganto di commessa.
    * --- FIle di LOG
    * --- Try
    local bErr_04A93F90
    bErr_04A93F90=bTrsErr
    this.Try_04A93F90()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE %1 - impossibile aggiornare documenti", this.oParentObject.w_PMSG )
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_04A93F90
    * --- End
    * --- Drop temporary table TMP_FATTDIF
    i_nIdx=cp_GetTableDefIdx('TMP_FATTDIF')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMP_FATTDIF')
    endif
  endproc
  proc Try_04A93F90()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- In un'unica istruzione aggiorno gli importi di commesse con valuta identica alla valuta di mvimpnaz
    this.w_SAMEVAL = "S"
    * --- Write into DOC_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="MVSERIAL,CPROWNUM,MVNUMRIF"
      do vq_exec with 'GSCVBB13',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
              +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
              +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MVIMPCOM = _t2.MVIMPNAZ";
          +i_ccchkf;
          +" from "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
              +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
              +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 set ";
          +"DOC_DETT.MVIMPCOM = _t2.MVIMPNAZ";
          +Iif(Empty(i_ccchkf),"",",DOC_DETT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="DOC_DETT.MVSERIAL = t2.MVSERIAL";
              +" and "+"DOC_DETT.CPROWNUM = t2.CPROWNUM";
              +" and "+"DOC_DETT.MVNUMRIF = t2.MVNUMRIF";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set (";
          +"MVIMPCOM";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.MVIMPNAZ";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
              +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
              +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set ";
          +"MVIMPCOM = _t2.MVIMPNAZ";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
              +" and "+i_cTable+".MVNUMRIF = "+i_cQueryTable+".MVNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MVIMPCOM = (select MVIMPNAZ from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    this.w_NROWS = i_ROWS
    this.w_SAMEVAL = "N"
    * --- Per gli importi legati a commesse in valuta differente riga per riga svolgo il cambio..
    * --- Select from GSCVBB13
    do vq_exec with 'GSCVBB13',this,'_Curs_GSCVBB13','',.f.,.t.
    if used('_Curs_GSCVBB13')
      select _Curs_GSCVBB13
      locate for 1=1
      do while not(eof())
      this.w_IMPCOM = CAIMPCOM( IIF(Empty(_Curs_GSCVBB13.MVCODCOM),"S", " "), _Curs_GSCVBB13.MVVALNAZ, _Curs_GSCVBB13.COCODVAL, _Curs_GSCVBB13.MVIMPNAZ, _Curs_GSCVBB13.CAONAZ, IIF(EMPTY(_Curs_GSCVBB13.MVDATDOC), _Curs_GSCVBB13.MVDATREG, _Curs_GSCVBB13.MVDATDOC), _Curs_GSCVBB13.DECCOM )
      * --- Write into DOC_DETT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DOC_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MVIMPCOM ="+cp_NullLink(cp_ToStrODBC(this.w_IMPCOM),'DOC_DETT','MVIMPCOM');
            +i_ccchkf ;
        +" where ";
            +"MVSERIAL = "+cp_ToStrODBC(_Curs_GSCVBB13.MVSERIAL);
            +" and CPROWNUM = "+cp_ToStrODBC(_Curs_GSCVBB13.CPROWNUM);
            +" and MVNUMRIF = "+cp_ToStrODBC(_Curs_GSCVBB13.MVNUMRIF);
               )
      else
        update (i_cTable) set;
            MVIMPCOM = this.w_IMPCOM;
            &i_ccchkf. ;
         where;
            MVSERIAL = _Curs_GSCVBB13.MVSERIAL;
            and CPROWNUM = _Curs_GSCVBB13.CPROWNUM;
            and MVNUMRIF = _Curs_GSCVBB13.MVNUMRIF;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      this.w_NROWS = this.w_NROWS + 1
        select _Curs_GSCVBB13
        continue
      enddo
      use
    endif
    * --- commit
    cp_EndTrs(.t.)
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = ah_Msgformat("Conversione eseguita correttamente su %1 righe", Alltrim(Str( this.w_NROWS )) )
    this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.oParentObject.w_PMSG)
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='DOC_DETT'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_GSCVBB13')
      use in _Curs_GSCVBB13
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
