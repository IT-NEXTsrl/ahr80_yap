* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvab02                                                        *
*              Aggiorna e_mail e web (offe)                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_40]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-10-01                                                      *
* Last revis.: 2003-10-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvab02",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvab02 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_NHF0 = 0
  * --- WorkFile variables
  NOM_CONT_idx=0
  OFF_NOMI_idx=0
  RIPATMP1_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiornamento campi relativi all'indirizzo e-mail e web nelle seguenti gestioni:
    *     NOMINATIVI (OFF_NOMI)
    *     CONTATTI (NOM_CONT)
    * --- FIle di LOG
    if upper(CP_DBTYPE)="DB2"
      * --- Try
      local bErr_03735B30
      bErr_03735B30=bTrsErr
      this.Try_03735B30()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        * --- Gestisce log errori
        this.oParentObject.w_PMSG = Message()
        this.oParentObject.w_PESEOK = .F.
        if this.w_NHF>=0
          this.w_TMPC = this.oParentObject.w_PMSG
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
      bTrsErr=bTrsErr or bErr_03735B30
      * --- End
      * --- Try
      local bErr_03601050
      bErr_03601050=bTrsErr
      this.Try_03601050()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        * --- Gestisce log errori
        this.oParentObject.w_PMSG = Message()
        this.oParentObject.w_PESEOK = .F.
        if this.w_NHF>=0
          this.w_TMPC = this.oParentObject.w_PMSG
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
      bTrsErr=bTrsErr or bErr_03601050
      * --- End
    else
      ah_ErrorMsg("Conversione prevista solo per database IBM DB2",,"")
      this.oParentObject.w_PESEOK = .T.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("Elaborazione terminata con successo")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
  endproc
  proc Try_03735B30()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    i_nConn=i_TableProp[this.OFF_NOMI_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2])
    if i_nConn<>0
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if bTrsErr
        * --- Raise
        i_Error=MESSAGE()
        return
      else
        if this.w_NHF>=0
          this.w_TMPC = ah_Msgformat("Adeguamento campi e-Mail e Web tabella OFF_NOMI eseguito correttamente%0ATTENZIONE: occorre andare in MANUTENZIONE DATABASE, selezionare la tabella%0OFF_NOMI e premere <OK>")
        endif
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.w_TMPC = ah_Msgformat("Adeguamento campi e-Mail e Web tabella OFF_NOMI eseguito correttamente%0ATTENZIONE: occorre andare in MANUTENZIONE DATABASE, selezionare la tabella%0OFF_NOMI e premere <OK>")
    this.oParentObject.w_PMSG = this.w_TMPC
    return
  proc Try_03601050()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    i_nConn=i_TableProp[this.NOM_CONT_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.NOM_CONT_idx,2])
    if i_nConn<>0
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if bTrsErr
        * --- Raise
        i_Error=MESSAGE()
        return
      else
        if this.w_NHF>=0
          this.w_TMPC = ah_Msgformat("Adeguamento campi e-Mail e Web tabella NOM_CONT eseguito correttamente%0ATTENZIONE: occorre andare in MANUTENZIONE DATABASE, selezionare la tabella%0NOM_CONT e premere <OK>")
        endif
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.w_TMPC = ah_Msgformat("Adeguamento campi e-Mail e Web tabella NOM_CONT eseguito correttamente%0ATTENZIONE: occorre andare in MANUTENZIONE DATABASE, selezionare la tabella%0NOM_CONT e premere <OK>")
    this.oParentObject.w_PMSG = this.w_TMPC
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella DB2
    pName="OFF_NOMI"
    pDatabaseType=i_ServerConn[1,6]
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.OFF_NOMI_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
          )
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    * --- Elimino la tabella
    if Not GSCV_BDT( this, "OFF_NOMI" , i_nConn )
      * --- Raise
      i_Error="Errore"
      return
    endif
    * --- Ricostruisco il database per la tabella
    GSCV_BRT(this, "OFF_NOMI" , i_nConn , .T.)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into OFF_NOMI
    i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.OFF_NOMI_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_Msg("Ripristinati valori di origine",.T.)
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    ah_Msg("Eliminata tabella temporanea tmp",.T.)
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella DB2
    pName="NOM_CONT"
    pDatabaseType=i_ServerConn[1,6]
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.NOM_CONT_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.NOM_CONT_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
          )
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    * --- Elimino la tabella
    if Not GSCV_BDT( this, "NOM_CONT" , i_nConn )
      * --- Raise
      i_Error="Errore"
      return
    endif
    * --- Ricostruisco il database per la tabella
    GSCV_BRT(this, "NOM_CONT" , i_nConn , .T.)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into NOM_CONT
    i_nConn=i_TableProp[this.NOM_CONT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.NOM_CONT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.NOM_CONT_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_Msg("Ripristinati valori di origine",.T.)
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    ah_Msg("Eliminata tabella temporanea tmp",.T.)
  endproc


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='NOM_CONT'
    this.cWorkTables[2]='OFF_NOMI'
    this.cWorkTables[3]='*RIPATMP1'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
