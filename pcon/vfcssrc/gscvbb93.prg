* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvbb93                                                        *
*              Aumenta la lunghezza delle ragioni sociali                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-05-21                                                      *
* Last revis.: 2009-02-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvbb93",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvbb93 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_MESS = space(100)
  w_GOON = .f.
  w_GOON1 = .f.
  w_GOON2 = .f.
  * --- WorkFile variables
  CONTI_idx=0
  RIPATMP1_idx=0
  AZIENDA_idx=0
  RIPATMP2_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Procedura di conversione per DB2 (gli altri database riescono ad allargare i campi carattere senza bisogno di procedure di conversione)
    *     I campi allargati sono la reagione sociale nelle tabelle CONTI,AZIENDA,  CLI_VEND (� in gpos) 
    this.w_GOON = .T.
    this.w_GOON1 = .T.
    this.w_GOON2 = .T.
    this.oParentObject.w_PESEOK = .T.
    * --- Se il daabase gestisce i varchar non serve la procedura di conversione
     
 i_nConn=i_TableProp[this.CONTI_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
    A=SQLEXEC(i_nConn,"Select ANDESCRI from "+i_cTable + " where 1=0","CURS")
    afields(DATI,"CURS")
    if Used("CURS")
      Select ("CURS") 
 use
    endif
    if DATI(1,3)=60
      this.w_GOON = .F.
    endif
     
 i_nConn=i_TableProp[this.AZIENDA_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
    A=SQLEXEC(i_nConn,"Select  AZCOFAZI , AZPIVAZI from "+i_cTable + " where 1=0","CURS")
    afields(DATI1,"CURS")
    if Used("CURS")
      Select ("CURS") 
 use
    endif
    if DATI1(1,3)=20 OR DATI1(2,3)=20
      this.w_GOON1 = .F.
    endif
    if g_GPOS="S"
      GSCVBB94 (this,"CONTROLLO")
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      this.w_GOON2 = .F.
    endif
    if this.w_GOON OR this.w_GOON1 OR this.w_GOON2
      * --- Try
      local bErr_03791EB0
      bErr_03791EB0=bTrsErr
      this.Try_03791EB0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- Gestisce log errori
        this.oParentObject.w_PMSG = Message()
        this.oParentObject.w_PESEOK = .F.
        if this.w_NHF>=0
          this.w_TMPC = this.oParentObject.w_PMSG
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
      bTrsErr=bTrsErr or bErr_03791EB0
      * --- End
    else
      this.oParentObject.w_PMSG = "La procedura di conversione non � stata eseguita poich� non pi� necessaria"
      this.w_TMPC = this.oParentObject.w_PMSG
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
  endproc
  proc Try_03791EB0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Esecuzione ok
    if upper(CP_DBTYPE)="DB2"
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    this.oParentObject.w_PMSG = "Elaborazione terminata con successo"
    if this.w_NHF>=0
      this.w_TMPC = this.oParentObject.w_PMSG
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if this.w_GOON
      * --- Converte tabella DB2
      i_nConn=i_TableProp[this.CONTI_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
      pName="CONTI"
      pDatabaseType=i_ServerConn[1,6]
      * --- Ricostruisco  la tabella prima di copiarne il contenuto  (si evita il rischio che la tabella copiata abbia dei campi in meno della tabella ricostruita)
      GSCV_BRT(this, "CONTI" , i_nConn , .T.)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Create temporary table RIPATMP1
      i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      i_nConn=i_TableProp[this.CONTI_idx,3] && recupera la connessione
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
      cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
            )
      this.RIPATMP1_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      wait window "Creata tabella temporanea di appoggio" NOWAIT
      * --- Elimino la tabella
      if Not GSCV_BDT( this, "CONTI" , i_nConn )
        * --- Raise
        i_Error="Error"
        return
      endif
      * --- Ricostruisco il database per la tabella
      GSCV_BRT(this, "CONTI" , i_nConn , .T.)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Insert into CONTI
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
        i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.CONTI_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Elimina tabella TMP
      * --- Drop temporary table RIPATMP1
      i_nIdx=cp_GetTableDefIdx('RIPATMP1')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('RIPATMP1')
      endif
      wait window "Eliminata tabella temporanea TMP" NOWAIT
    endif
    * --- Converte tabella DB2
    if this.w_GOON1
      pName="AZIENDA"
      pDatabaseType=i_ServerConn[1,6]
      * --- Ricostruisco  la tabella prima di copiarne il contenuto  (si evita il rischio che la tabella copiata abbia dei campi in meno della tabella ricostruita)
      GSCV_BRT(this, "AZIENDA" , i_nConn , .T.)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Create temporary table RIPATMP2
      i_nIdx=cp_AddTableDef('RIPATMP2') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      i_nConn=i_TableProp[this.AZIENDA_idx,3] && recupera la connessione
      i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
      cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
            )
      this.RIPATMP2_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      wait window "Creata tabella temporanea di appoggio" NOWAIT
      * --- Elimino la tabella
      if Not GSCV_BDT( this, "AZIENDA" , i_nConn )
        * --- Raise
        i_Error="Error"
        return
      endif
      * --- Ricostruisco il database per la tabella
      GSCV_BRT(this, "AZIENDA" , i_nConn , .T.)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Insert into AZIENDA
      i_nConn=i_TableProp[this.AZIENDA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP2_idx,2])
        i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.AZIENDA_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Elimina tabella TMP
      * --- Drop temporary table RIPATMP2
      i_nIdx=cp_GetTableDefIdx('RIPATMP2')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('RIPATMP2')
      endif
      wait window "Eliminata tabella temporanea TMP" NOWAIT
    endif
    * --- Allarga campi CLI_VEND
    if g_GPOS="S" AND this.w_GOON2
      GSCVBB94 (this,"AGGIORNA")
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  endproc


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='*RIPATMP1'
    this.cWorkTables[3]='AZIENDA'
    this.cWorkTables[4]='*RIPATMP2'
    return(this.OpenAllTables(4))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
