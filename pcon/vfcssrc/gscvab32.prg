* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvab32                                                        *
*              Aggiornamento mrparame                                          *
*                                                                              *
*      Author: Nicola Gorlandi                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_352]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-10-18                                                      *
* Last revis.: 2001-10-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvab32",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvab32 as StdBatch
  * --- Local variables
  w_CONN = 0
  w_pName = space(200)
  w_CONN = 0
  w_CONN = 0
  w_CONN = 0
  w_CONN = 0
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_oMess = .NULL.
  w_oPart = .NULL.
  * --- WorkFile variables
  COLLCENT_idx=0
  MOVICOST_idx=0
  RIPACENT_idx=0
  STORCOST_idx=0
  RIPATMP1_idx=0
  RIPACOMM_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Procedura di conversione per DB2 per aggiornare il tipo del campo
    *     MRPARAME in 
    * --- FIle di LOG
    * --- Oggetto per messaggi incrementali
    this.w_oMess=createobject("Ah_Message")
    * --- Try
    local bErr_035F34E0
    bErr_035F34E0=bTrsErr
    this.Try_035F34E0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = this.oParentObject.w_PMSG
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_035F34E0
    * --- End
  endproc
  proc Try_035F34E0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if bTrsErr
      * --- Raise
      i_Error=MESSAGE()
      return
    else
      if this.w_NHF>=0
        this.w_oMess.AddMsgPartNL("Adeguamento campi MRPARAME tabella COLLCENT")     
        this.w_oMess.AddMsgPartNL("Adeguamento campi MRPARAME tabella MOVICOST")     
        this.w_oMess.AddMsgPartNL("Adeguamento campi MRPARAME tabella RIPACENT")     
        this.w_oMess.AddMsgPartNL("Adeguamento campi MRPARAME tabella STORCOST")     
        this.w_oMess.AddMsgPartNL("Adeguamento campi MCPARAME tabella RIPACOMM%0Eseguito correttamente")     
        this.w_oMess.AddMsgPartNL("Occorre ripetere la procedura su ogni azienda")     
        this.w_TMPC = this.w_oMess.ComposeMessage()
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = this.w_TMPC
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_CONN = i_TableProp[this.COLLCENT_idx,3]
    this.w_pName = "COLLCENT"
    if  this.w_CONN <>0
      * --- Create temporary table RIPATMP1
      i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      i_nConn=i_TableProp[this.COLLCENT_idx,3] && recupera la connessione
      i_cTable=cp_SetAzi(i_TableProp[this.COLLCENT_idx,2])
      cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
            )
      this.RIPATMP1_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      ah_Msg("Creata tabella temporanea di appoggio %1",.T.,.F.,.F., this.w_pName )
      * --- Converte tabella DB2
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Insert into COLLCENT
      i_nConn=i_TableProp[this.COLLCENT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.COLLCENT_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
        i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.COLLCENT_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      ah_Msg("Ripristinati valori di origine",.T.)
      * --- Elimina tabella TMP
      * --- Drop temporary table RIPATMP1
      i_nIdx=cp_GetTableDefIdx('RIPATMP1')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('RIPATMP1')
      endif
      ah_Msg("Eliminata tabella temporanea tmp",.T.)
    endif
    this.w_CONN = i_TableProp[this.MOVICOST_idx,3]
    this.w_pName = "MOVICOST"
    if  this.w_CONN <>0
      * --- Create temporary table RIPATMP1
      i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      i_nConn=i_TableProp[this.MOVICOST_idx,3] && recupera la connessione
      i_cTable=cp_SetAzi(i_TableProp[this.MOVICOST_idx,2])
      cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
            )
      this.RIPATMP1_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      ah_Msg("Creata tabella temporanea di appoggio %1",.T.,.F.,.F., this.w_pName )
      * --- Converte tabella DB2
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Insert into MOVICOST
      i_nConn=i_TableProp[this.MOVICOST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MOVICOST_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
        i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.MOVICOST_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      ah_Msg("Ripristinati valori di origine",.T.)
      * --- Elimina tabella TMP
      * --- Drop temporary table RIPATMP1
      i_nIdx=cp_GetTableDefIdx('RIPATMP1')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('RIPATMP1')
      endif
      ah_Msg("Eliminata tabella temporanea tmp",.T.)
    endif
    this.w_CONN = i_TableProp[this.RIPACENT_idx,3]
    this.w_pName = "RIPACENT"
    if  this.w_CONN <>0
      * --- Create temporary table RIPATMP1
      i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      i_nConn=i_TableProp[this.RIPACENT_idx,3] && recupera la connessione
      i_cTable=cp_SetAzi(i_TableProp[this.RIPACENT_idx,2])
      cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
            )
      this.RIPATMP1_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      ah_Msg("Creata tabella temporanea di appoggio %1",.T.,.F.,.F., this.w_pName )
      * --- Converte tabella DB2
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Insert into RIPACENT
      i_nConn=i_TableProp[this.RIPACENT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.RIPACENT_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
        i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.RIPACENT_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      ah_Msg("Ripristinati valori di origine",.T.)
      * --- Elimina tabella TMP
      * --- Drop temporary table RIPATMP1
      i_nIdx=cp_GetTableDefIdx('RIPATMP1')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('RIPATMP1')
      endif
      ah_Msg("Eliminata tabella temporanea tmp",.T.)
    endif
    this.w_CONN = i_TableProp[this.STORCOST_idx,3]
    this.w_pName = "STORCOST"
    if  this.w_CONN <>0
      * --- Create temporary table RIPATMP1
      i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      i_nConn=i_TableProp[this.STORCOST_idx,3] && recupera la connessione
      i_cTable=cp_SetAzi(i_TableProp[this.STORCOST_idx,2])
      cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
            )
      this.RIPATMP1_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      ah_Msg("Creata tabella temporanea di appoggio %1",.T.,.F.,.F., this.w_pName )
      * --- Converte tabella DB2
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Insert into STORCOST
      i_nConn=i_TableProp[this.STORCOST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.STORCOST_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
        i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.STORCOST_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      ah_Msg("Ripristinati valori di origine",.T.)
      * --- Elimina tabella TMP
      * --- Drop temporary table RIPATMP1
      i_nIdx=cp_GetTableDefIdx('RIPATMP1')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('RIPATMP1')
      endif
      ah_Msg("Eliminata tabella temporanea tmp",.T.)
    endif
    this.w_CONN = i_TableProp[this.STORCOST_idx,3]
    this.w_pName = "RIPACOMM"
    if  this.w_CONN <>0
      * --- Create temporary table RIPATMP1
      i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      i_nConn=i_TableProp[this.RIPACOMM_idx,3] && recupera la connessione
      i_cTable=cp_SetAzi(i_TableProp[this.RIPACOMM_idx,2])
      cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
            )
      this.RIPATMP1_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      ah_Msg("Creata tabella temporanea di appoggio %1",.T.,.F.,.F., this.w_pName )
      * --- Converte tabella DB2
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Insert into RIPACOMM
      i_nConn=i_TableProp[this.RIPACOMM_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.RIPACOMM_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
        i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.RIPACOMM_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      ah_Msg("Ripristinati valori di origine",.T.)
      * --- Elimina tabella TMP
      * --- Drop temporary table RIPATMP1
      i_nIdx=cp_GetTableDefIdx('RIPATMP1')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('RIPATMP1')
      endif
      ah_Msg("Eliminata tabella temporanea tmp",.T.)
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elimino la tabella
    if Not GSCV_BDT( this, this.w_pName , this.w_CONN )
      * --- Raise
      i_Error="Error"
      return
    endif
    * --- Ricostruisco il database per la tabella
    GSCV_BRT(this, this.w_pName , this.w_CONN , .T.)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,6)]
    this.cWorkTables[1]='COLLCENT'
    this.cWorkTables[2]='MOVICOST'
    this.cWorkTables[3]='RIPACENT'
    this.cWorkTables[4]='STORCOST'
    this.cWorkTables[5]='*RIPATMP1'
    this.cWorkTables[6]='RIPACOMM'
    return(this.OpenAllTables(6))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
