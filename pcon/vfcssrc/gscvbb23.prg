* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvbb23                                                        *
*              Conversione per la localizzazione                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_30]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-02-07                                                      *
* Last revis.: 2006-02-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvbb23",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvbb23 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_MESS = space(100)
  w_CODAZI = space(5)
  w_CODREL = space(10)
  w_ESEGUI = space(1)
  w_CODPRO = space(2)
  w_DESPRO = space(30)
  * --- WorkFile variables
  TIP_DIST_idx=0
  ANAG_PRO_idx=0
  CON_CONS_idx=0
  AZIENDA_idx=0
  AHEECONV_idx=0
  CONVERSI_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questa procedura di conversione per la localizzazione spagnola
    * --- FIle di LOG
    this.w_ESEGUI = "N"
    this.w_CODREL = "4.0-M3553"
    this.w_CODAZI = cp_GetGlobalVar("i_CODAZI")
    * --- Select from AZIENDA
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select AZCODAZI  from "+i_cTable+" AZIENDA ";
          +" where AZCODAZI<> "+cp_ToStrODBC(this.w_CODAZI)+"";
           ,"_Curs_AZIENDA")
    else
      select AZCODAZI from (i_cTable);
       where AZCODAZI<> this.w_CODAZI;
        into cursor _Curs_AZIENDA
    endif
    if used('_Curs_AZIENDA')
      select _Curs_AZIENDA
      locate for 1=1
      do while not(eof())
      cp_SetGlobalVar("I_CODAZI",_Curs_AZIENDA.AZCODAZI)
      * --- Read from CONVERSI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONVERSI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONVERSI_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "COESEGUI"+;
          " from "+i_cTable+" CONVERSI where ";
              +"COCODREL = "+cp_ToStrODBC(this.w_CODREL);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          COESEGUI;
          from (i_cTable) where;
              COCODREL = this.w_CODREL;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_ESEGUI = NVL(cp_ToDate(_read_.COESEGUI),cp_NullValue(_read_.COESEGUI))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_ESEGUI="S"
        Exit
      endif
        select _Curs_AZIENDA
        continue
      enddo
      use
    endif
    cp_SetGlobalVar("I_CODAZI",this.w_CODAZI)
    * --- Try
    local bErr_035F5BE0
    bErr_035F5BE0=bTrsErr
    this.Try_035F5BE0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = AH_MsgFormat("ERRORE - Impossibile inserire record nelle tabelle %1","TIP_DIST/CON_CONS/ANAG_PRO")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_035F5BE0
    * --- End
  endproc
  proc Try_035F5BE0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    if this.w_ESEGUI<>"S"
      * --- Inserisco i codici italiani
      * --- Insert into TIP_DIST
      i_nConn=i_TableProp[this.TIP_DIST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TIP_DIST_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TIP_DIST_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"DICODISO"+",DITIPDIS"+",DICAUDIS"+",DIDESCRI"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC("ITA"),'TIP_DIST','DICODISO');
        +","+cp_NullLink(cp_ToStrODBC("BO"),'TIP_DIST','DITIPDIS');
        +","+cp_NullLink(cp_ToStrODBC("27000"),'TIP_DIST','DICAUDIS');
        +","+cp_NullLink(cp_ToStrODBC("Emolumenti generici"),'TIP_DIST','DIDESCRI');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'DICODISO',"ITA",'DITIPDIS',"BO",'DICAUDIS',"27000",'DIDESCRI',"Emolumenti generici")
        insert into (i_cTable) (DICODISO,DITIPDIS,DICAUDIS,DIDESCRI &i_ccchkf. );
           values (;
             "ITA";
             ,"BO";
             ,"27000";
             ,"Emolumenti generici";
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Insert into TIP_DIST
      i_nConn=i_TableProp[this.TIP_DIST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TIP_DIST_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TIP_DIST_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"DICODISO"+",DITIPDIS"+",DICAUDIS"+",DIDESCRI"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC("ITA"),'TIP_DIST','DICODISO');
        +","+cp_NullLink(cp_ToStrODBC("BO"),'TIP_DIST','DITIPDIS');
        +","+cp_NullLink(cp_ToStrODBC("27010"),'TIP_DIST','DICAUDIS');
        +","+cp_NullLink(cp_ToStrODBC("Emolumenti - pensioni"),'TIP_DIST','DIDESCRI');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'DICODISO',"ITA",'DITIPDIS',"BO",'DICAUDIS',"27010",'DIDESCRI',"Emolumenti - pensioni")
        insert into (i_cTable) (DICODISO,DITIPDIS,DICAUDIS,DIDESCRI &i_ccchkf. );
           values (;
             "ITA";
             ,"BO";
             ,"27010";
             ,"Emolumenti - pensioni";
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Insert into TIP_DIST
      i_nConn=i_TableProp[this.TIP_DIST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TIP_DIST_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TIP_DIST_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"DICODISO"+",DITIPDIS"+",DICAUDIS"+",DIDESCRI"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC("ITA"),'TIP_DIST','DICODISO');
        +","+cp_NullLink(cp_ToStrODBC("BO"),'TIP_DIST','DITIPDIS');
        +","+cp_NullLink(cp_ToStrODBC("27020"),'TIP_DIST','DICAUDIS');
        +","+cp_NullLink(cp_ToStrODBC("Emolumenti - stipendi"),'TIP_DIST','DIDESCRI');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'DICODISO',"ITA",'DITIPDIS',"BO",'DICAUDIS',"27020",'DIDESCRI',"Emolumenti - stipendi")
        insert into (i_cTable) (DICODISO,DITIPDIS,DICAUDIS,DIDESCRI &i_ccchkf. );
           values (;
             "ITA";
             ,"BO";
             ,"27020";
             ,"Emolumenti - stipendi";
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Insert into TIP_DIST
      i_nConn=i_TableProp[this.TIP_DIST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TIP_DIST_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TIP_DIST_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"DICODISO"+",DITIPDIS"+",DICAUDIS"+",DIDESCRI"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC("ITA"),'TIP_DIST','DICODISO');
        +","+cp_NullLink(cp_ToStrODBC("BO"),'TIP_DIST','DITIPDIS');
        +","+cp_NullLink(cp_ToStrODBC("34000"),'TIP_DIST','DICAUDIS');
        +","+cp_NullLink(cp_ToStrODBC("Giroconto"),'TIP_DIST','DIDESCRI');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'DICODISO',"ITA",'DITIPDIS',"BO",'DICAUDIS',"34000",'DIDESCRI',"Giroconto")
        insert into (i_cTable) (DICODISO,DITIPDIS,DICAUDIS,DIDESCRI &i_ccchkf. );
           values (;
             "ITA";
             ,"BO";
             ,"34000";
             ,"Giroconto";
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Insert into TIP_DIST
      i_nConn=i_TableProp[this.TIP_DIST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TIP_DIST_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TIP_DIST_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"DICODISO"+",DITIPDIS"+",DICAUDIS"+",DIDESCRI"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC("ITA"),'TIP_DIST','DICODISO');
        +","+cp_NullLink(cp_ToStrODBC("BO"),'TIP_DIST','DITIPDIS');
        +","+cp_NullLink(cp_ToStrODBC("48000"),'TIP_DIST','DICAUDIS');
        +","+cp_NullLink(cp_ToStrODBC("Bonifici generici"),'TIP_DIST','DIDESCRI');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'DICODISO',"ITA",'DITIPDIS',"BO",'DICAUDIS',"48000",'DIDESCRI',"Bonifici generici")
        insert into (i_cTable) (DICODISO,DITIPDIS,DICAUDIS,DIDESCRI &i_ccchkf. );
           values (;
             "ITA";
             ,"BO";
             ,"48000";
             ,"Bonifici generici";
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Insert into TIP_DIST
      i_nConn=i_TableProp[this.TIP_DIST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TIP_DIST_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TIP_DIST_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"DICODISO"+",DITIPDIS"+",DICAUDIS"+",DIDESCRI"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC("ITA"),'TIP_DIST','DICODISO');
        +","+cp_NullLink(cp_ToStrODBC("BO"),'TIP_DIST','DITIPDIS');
        +","+cp_NullLink(cp_ToStrODBC("48015"),'TIP_DIST','DICAUDIS');
        +","+cp_NullLink(cp_ToStrODBC("Bonifici previdenza complementare"),'TIP_DIST','DIDESCRI');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'DICODISO',"ITA",'DITIPDIS',"BO",'DICAUDIS',"48015",'DIDESCRI',"Bonifici previdenza complementare")
        insert into (i_cTable) (DICODISO,DITIPDIS,DICAUDIS,DIDESCRI &i_ccchkf. );
           values (;
             "ITA";
             ,"BO";
             ,"48015";
             ,"Bonifici previdenza complementare";
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Insert into TIP_DIST
      i_nConn=i_TableProp[this.TIP_DIST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TIP_DIST_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TIP_DIST_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"DICODISO"+",DITIPDIS"+",DICAUDIS"+",DIDESCRI"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC("ITA"),'TIP_DIST','DICODISO');
        +","+cp_NullLink(cp_ToStrODBC("BO"),'TIP_DIST','DITIPDIS');
        +","+cp_NullLink(cp_ToStrODBC("79000"),'TIP_DIST','DICAUDIS');
        +","+cp_NullLink(cp_ToStrODBC("Girofondi"),'TIP_DIST','DIDESCRI');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'DICODISO',"ITA",'DITIPDIS',"BO",'DICAUDIS',"79000",'DIDESCRI',"Girofondi")
        insert into (i_cTable) (DICODISO,DITIPDIS,DICAUDIS,DIDESCRI &i_ccchkf. );
           values (;
             "ITA";
             ,"BO";
             ,"79000";
             ,"Girofondi";
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Inserisco i codici spagnoli per il bonifico
      * --- Insert into TIP_DIST
      i_nConn=i_TableProp[this.TIP_DIST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TIP_DIST_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TIP_DIST_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"DICODISO"+",DITIPDIS"+",DICAUDIS"+",DIDESCRI"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC("ESP"),'TIP_DIST','DICODISO');
        +","+cp_NullLink(cp_ToStrODBC("BO"),'TIP_DIST','DITIPDIS');
        +","+cp_NullLink(cp_ToStrODBC("10056"),'TIP_DIST','DICAUDIS');
        +","+cp_NullLink(cp_ToStrODBC("N�minas y Transferencias (Nomina)"),'TIP_DIST','DIDESCRI');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'DICODISO',"ESP",'DITIPDIS',"BO",'DICAUDIS',"10056",'DIDESCRI',"N�minas y Transferencias (Nomina)")
        insert into (i_cTable) (DICODISO,DITIPDIS,DICAUDIS,DIDESCRI &i_ccchkf. );
           values (;
             "ESP";
             ,"BO";
             ,"10056";
             ,"N�minas y Transferencias (Nomina)";
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Insert into TIP_DIST
      i_nConn=i_TableProp[this.TIP_DIST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TIP_DIST_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TIP_DIST_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"DICODISO"+",DITIPDIS"+",DICAUDIS"+",DIDESCRI"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC("ESP"),'TIP_DIST','DICODISO');
        +","+cp_NullLink(cp_ToStrODBC("BO"),'TIP_DIST','DITIPDIS');
        +","+cp_NullLink(cp_ToStrODBC("10057"),'TIP_DIST','DICAUDIS');
        +","+cp_NullLink(cp_ToStrODBC("Cheques bancarios (Nomina)"),'TIP_DIST','DIDESCRI');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'DICODISO',"ESP",'DITIPDIS',"BO",'DICAUDIS',"10057",'DIDESCRI',"Cheques bancarios (Nomina)")
        insert into (i_cTable) (DICODISO,DITIPDIS,DICAUDIS,DIDESCRI &i_ccchkf. );
           values (;
             "ESP";
             ,"BO";
             ,"10057";
             ,"Cheques bancarios (Nomina)";
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Insert into TIP_DIST
      i_nConn=i_TableProp[this.TIP_DIST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TIP_DIST_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TIP_DIST_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"DICODISO"+",DITIPDIS"+",DICAUDIS"+",DIDESCRI"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC("ESP"),'TIP_DIST','DICODISO');
        +","+cp_NullLink(cp_ToStrODBC("BO"),'TIP_DIST','DITIPDIS');
        +","+cp_NullLink(cp_ToStrODBC("10058"),'TIP_DIST','DICAUDIS');
        +","+cp_NullLink(cp_ToStrODBC("Pagar�s (Nomina)"),'TIP_DIST','DIDESCRI');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'DICODISO',"ESP",'DITIPDIS',"BO",'DICAUDIS',"10058",'DIDESCRI',"Pagar�s (Nomina)")
        insert into (i_cTable) (DICODISO,DITIPDIS,DICAUDIS,DIDESCRI &i_ccchkf. );
           values (;
             "ESP";
             ,"BO";
             ,"10058";
             ,"Pagar�s (Nomina)";
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Insert into TIP_DIST
      i_nConn=i_TableProp[this.TIP_DIST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TIP_DIST_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TIP_DIST_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"DICODISO"+",DITIPDIS"+",DICAUDIS"+",DIDESCRI"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC("ESP"),'TIP_DIST','DICODISO');
        +","+cp_NullLink(cp_ToStrODBC("BO"),'TIP_DIST','DITIPDIS');
        +","+cp_NullLink(cp_ToStrODBC("10059"),'TIP_DIST','DICAUDIS');
        +","+cp_NullLink(cp_ToStrODBC("Pagos_Certificados (Nomina)"),'TIP_DIST','DIDESCRI');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'DICODISO',"ESP",'DITIPDIS',"BO",'DICAUDIS',"10059",'DIDESCRI',"Pagos_Certificados (Nomina)")
        insert into (i_cTable) (DICODISO,DITIPDIS,DICAUDIS,DIDESCRI &i_ccchkf. );
           values (;
             "ESP";
             ,"BO";
             ,"10059";
             ,"Pagos_Certificados (Nomina)";
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- --
      * --- Insert into TIP_DIST
      i_nConn=i_TableProp[this.TIP_DIST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TIP_DIST_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TIP_DIST_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"DICODISO"+",DITIPDIS"+",DICAUDIS"+",DIDESCRI"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC("ESP"),'TIP_DIST','DICODISO');
        +","+cp_NullLink(cp_ToStrODBC("BO"),'TIP_DIST','DITIPDIS');
        +","+cp_NullLink(cp_ToStrODBC("80056"),'TIP_DIST','DICAUDIS');
        +","+cp_NullLink(cp_ToStrODBC("N�minas y Transferencias (Pensi�n)"),'TIP_DIST','DIDESCRI');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'DICODISO',"ESP",'DITIPDIS',"BO",'DICAUDIS',"80056",'DIDESCRI',"N�minas y Transferencias (Pensi�n)")
        insert into (i_cTable) (DICODISO,DITIPDIS,DICAUDIS,DIDESCRI &i_ccchkf. );
           values (;
             "ESP";
             ,"BO";
             ,"80056";
             ,"N�minas y Transferencias (Pensi�n)";
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Insert into TIP_DIST
      i_nConn=i_TableProp[this.TIP_DIST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TIP_DIST_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TIP_DIST_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"DICODISO"+",DITIPDIS"+",DICAUDIS"+",DIDESCRI"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC("ESP"),'TIP_DIST','DICODISO');
        +","+cp_NullLink(cp_ToStrODBC("BO"),'TIP_DIST','DITIPDIS');
        +","+cp_NullLink(cp_ToStrODBC("80057"),'TIP_DIST','DICAUDIS');
        +","+cp_NullLink(cp_ToStrODBC("Cheques bancarios (Pensi�n)"),'TIP_DIST','DIDESCRI');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'DICODISO',"ESP",'DITIPDIS',"BO",'DICAUDIS',"80057",'DIDESCRI',"Cheques bancarios (Pensi�n)")
        insert into (i_cTable) (DICODISO,DITIPDIS,DICAUDIS,DIDESCRI &i_ccchkf. );
           values (;
             "ESP";
             ,"BO";
             ,"80057";
             ,"Cheques bancarios (Pensi�n)";
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Insert into TIP_DIST
      i_nConn=i_TableProp[this.TIP_DIST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TIP_DIST_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TIP_DIST_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"DICODISO"+",DITIPDIS"+",DICAUDIS"+",DIDESCRI"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC("ESP"),'TIP_DIST','DICODISO');
        +","+cp_NullLink(cp_ToStrODBC("BO"),'TIP_DIST','DITIPDIS');
        +","+cp_NullLink(cp_ToStrODBC("80058"),'TIP_DIST','DICAUDIS');
        +","+cp_NullLink(cp_ToStrODBC("Pagar�s (Pensi�n)"),'TIP_DIST','DIDESCRI');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'DICODISO',"ESP",'DITIPDIS',"BO",'DICAUDIS',"80058",'DIDESCRI',"Pagar�s (Pensi�n)")
        insert into (i_cTable) (DICODISO,DITIPDIS,DICAUDIS,DIDESCRI &i_ccchkf. );
           values (;
             "ESP";
             ,"BO";
             ,"80058";
             ,"Pagar�s (Pensi�n)";
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Insert into TIP_DIST
      i_nConn=i_TableProp[this.TIP_DIST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TIP_DIST_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TIP_DIST_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"DICODISO"+",DITIPDIS"+",DICAUDIS"+",DIDESCRI"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC("ESP"),'TIP_DIST','DICODISO');
        +","+cp_NullLink(cp_ToStrODBC("BO"),'TIP_DIST','DITIPDIS');
        +","+cp_NullLink(cp_ToStrODBC("80059"),'TIP_DIST','DICAUDIS');
        +","+cp_NullLink(cp_ToStrODBC("Pagos_Certificados (Pensi�n)" ),'TIP_DIST','DIDESCRI');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'DICODISO',"ESP",'DITIPDIS',"BO",'DICAUDIS',"80059",'DIDESCRI',"Pagos_Certificados (Pensi�n)" )
        insert into (i_cTable) (DICODISO,DITIPDIS,DICAUDIS,DIDESCRI &i_ccchkf. );
           values (;
             "ESP";
             ,"BO";
             ,"80059";
             ,"Pagos_Certificados (Pensi�n)" ;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- --
      * --- Insert into TIP_DIST
      i_nConn=i_TableProp[this.TIP_DIST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TIP_DIST_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TIP_DIST_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"DICODISO"+",DITIPDIS"+",DICAUDIS"+",DIDESCRI"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC("ESP"),'TIP_DIST','DICODISO');
        +","+cp_NullLink(cp_ToStrODBC("BO"),'TIP_DIST','DITIPDIS');
        +","+cp_NullLink(cp_ToStrODBC("90056"),'TIP_DIST','DICAUDIS');
        +","+cp_NullLink(cp_ToStrODBC("N�minas y Transferencias (Otros Conceptos)"),'TIP_DIST','DIDESCRI');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'DICODISO',"ESP",'DITIPDIS',"BO",'DICAUDIS',"90056",'DIDESCRI',"N�minas y Transferencias (Otros Conceptos)")
        insert into (i_cTable) (DICODISO,DITIPDIS,DICAUDIS,DIDESCRI &i_ccchkf. );
           values (;
             "ESP";
             ,"BO";
             ,"90056";
             ,"N�minas y Transferencias (Otros Conceptos)";
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Insert into TIP_DIST
      i_nConn=i_TableProp[this.TIP_DIST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TIP_DIST_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TIP_DIST_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"DICODISO"+",DITIPDIS"+",DICAUDIS"+",DIDESCRI"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC("ESP"),'TIP_DIST','DICODISO');
        +","+cp_NullLink(cp_ToStrODBC("BO"),'TIP_DIST','DITIPDIS');
        +","+cp_NullLink(cp_ToStrODBC("90057"),'TIP_DIST','DICAUDIS');
        +","+cp_NullLink(cp_ToStrODBC("Cheques bancarios (Otros Conceptos)"),'TIP_DIST','DIDESCRI');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'DICODISO',"ESP",'DITIPDIS',"BO",'DICAUDIS',"90057",'DIDESCRI',"Cheques bancarios (Otros Conceptos)")
        insert into (i_cTable) (DICODISO,DITIPDIS,DICAUDIS,DIDESCRI &i_ccchkf. );
           values (;
             "ESP";
             ,"BO";
             ,"90057";
             ,"Cheques bancarios (Otros Conceptos)";
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Insert into TIP_DIST
      i_nConn=i_TableProp[this.TIP_DIST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TIP_DIST_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TIP_DIST_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"DICODISO"+",DITIPDIS"+",DICAUDIS"+",DIDESCRI"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC("ESP"),'TIP_DIST','DICODISO');
        +","+cp_NullLink(cp_ToStrODBC("BO"),'TIP_DIST','DITIPDIS');
        +","+cp_NullLink(cp_ToStrODBC("90058"),'TIP_DIST','DICAUDIS');
        +","+cp_NullLink(cp_ToStrODBC("Pagar�s (Otros Conceptos)"),'TIP_DIST','DIDESCRI');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'DICODISO',"ESP",'DITIPDIS',"BO",'DICAUDIS',"90058",'DIDESCRI',"Pagar�s (Otros Conceptos)")
        insert into (i_cTable) (DICODISO,DITIPDIS,DICAUDIS,DIDESCRI &i_ccchkf. );
           values (;
             "ESP";
             ,"BO";
             ,"90058";
             ,"Pagar�s (Otros Conceptos)";
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Insert into TIP_DIST
      i_nConn=i_TableProp[this.TIP_DIST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TIP_DIST_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TIP_DIST_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"DICODISO"+",DITIPDIS"+",DICAUDIS"+",DIDESCRI"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC("ESP"),'TIP_DIST','DICODISO');
        +","+cp_NullLink(cp_ToStrODBC("BO"),'TIP_DIST','DITIPDIS');
        +","+cp_NullLink(cp_ToStrODBC("90059"),'TIP_DIST','DICAUDIS');
        +","+cp_NullLink(cp_ToStrODBC("Pagos_Certificados (Otros Conceptos)"),'TIP_DIST','DIDESCRI');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'DICODISO',"ESP",'DITIPDIS',"BO",'DICAUDIS',"90059",'DIDESCRI',"Pagos_Certificados (Otros Conceptos)")
        insert into (i_cTable) (DICODISO,DITIPDIS,DICAUDIS,DIDESCRI &i_ccchkf. );
           values (;
             "ESP";
             ,"BO";
             ,"90059";
             ,"Pagos_Certificados (Otros Conceptos)";
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Inserisco i codici spagnoli per le Riba
      * --- Insert into TIP_DIST
      i_nConn=i_TableProp[this.TIP_DIST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TIP_DIST_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TIP_DIST_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"DICODISO"+",DITIPDIS"+",DICAUDIS"+",DIDESCRI"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC("ESP"),'TIP_DIST','DICODISO');
        +","+cp_NullLink(cp_ToStrODBC("RB"),'TIP_DIST','DITIPDIS');
        +","+cp_NullLink(cp_ToStrODBC("00001"),'TIP_DIST','DICAUDIS');
        +","+cp_NullLink(cp_ToStrODBC("Letra de cambio"),'TIP_DIST','DIDESCRI');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'DICODISO',"ESP",'DITIPDIS',"RB",'DICAUDIS',"00001",'DIDESCRI',"Letra de cambio")
        insert into (i_cTable) (DICODISO,DITIPDIS,DICAUDIS,DIDESCRI &i_ccchkf. );
           values (;
             "ESP";
             ,"RB";
             ,"00001";
             ,"Letra de cambio";
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Insert into TIP_DIST
      i_nConn=i_TableProp[this.TIP_DIST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TIP_DIST_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TIP_DIST_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"DICODISO"+",DITIPDIS"+",DICAUDIS"+",DIDESCRI"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC("ESP"),'TIP_DIST','DICODISO');
        +","+cp_NullLink(cp_ToStrODBC("RB"),'TIP_DIST','DITIPDIS');
        +","+cp_NullLink(cp_ToStrODBC("00002"),'TIP_DIST','DICAUDIS');
        +","+cp_NullLink(cp_ToStrODBC("Recibo"),'TIP_DIST','DIDESCRI');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'DICODISO',"ESP",'DITIPDIS',"RB",'DICAUDIS',"00002",'DIDESCRI',"Recibo")
        insert into (i_cTable) (DICODISO,DITIPDIS,DICAUDIS,DIDESCRI &i_ccchkf. );
           values (;
             "ESP";
             ,"RB";
             ,"00002";
             ,"Recibo";
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Insert into TIP_DIST
      i_nConn=i_TableProp[this.TIP_DIST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TIP_DIST_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TIP_DIST_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"DICODISO"+",DITIPDIS"+",DICAUDIS"+",DIDESCRI"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC("ESP"),'TIP_DIST','DICODISO');
        +","+cp_NullLink(cp_ToStrODBC("RB"),'TIP_DIST','DITIPDIS');
        +","+cp_NullLink(cp_ToStrODBC("00003"),'TIP_DIST','DICAUDIS');
        +","+cp_NullLink(cp_ToStrODBC("Pagar�"),'TIP_DIST','DIDESCRI');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'DICODISO',"ESP",'DITIPDIS',"RB",'DICAUDIS',"00003",'DIDESCRI',"Pagar�")
        insert into (i_cTable) (DICODISO,DITIPDIS,DICAUDIS,DIDESCRI &i_ccchkf. );
           values (;
             "ESP";
             ,"RB";
             ,"00003";
             ,"Pagar�";
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Insert into TIP_DIST
      i_nConn=i_TableProp[this.TIP_DIST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TIP_DIST_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TIP_DIST_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"DICODISO"+",DITIPDIS"+",DICAUDIS"+",DIDESCRI"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC("ESP"),'TIP_DIST','DICODISO');
        +","+cp_NullLink(cp_ToStrODBC("RB"),'TIP_DIST','DITIPDIS');
        +","+cp_NullLink(cp_ToStrODBC("00004"),'TIP_DIST','DICAUDIS');
        +","+cp_NullLink(cp_ToStrODBC("Anticipos de efectos"),'TIP_DIST','DIDESCRI');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'DICODISO',"ESP",'DITIPDIS',"RB",'DICAUDIS',"00004",'DIDESCRI',"Anticipos de efectos")
        insert into (i_cTable) (DICODISO,DITIPDIS,DICAUDIS,DIDESCRI &i_ccchkf. );
           values (;
             "ESP";
             ,"RB";
             ,"00004";
             ,"Anticipos de efectos";
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- ------------------------
      * --- Condizioni di Consegna
      * --- Italia
      * --- Insert into CON_CONS
      i_nConn=i_TableProp[this.CON_CONS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CON_CONS_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CON_CONS_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"COCODICE"+",CCCODISO"+",CODESCRI"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC("E"),'CON_CONS','COCODICE');
        +","+cp_NullLink(cp_ToStrODBC("ITA"),'CON_CONS','CCCODISO');
        +","+cp_NullLink(cp_ToStrODBC("Franco fabbrica"),'CON_CONS','CODESCRI');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'COCODICE',"E",'CCCODISO',"ITA",'CODESCRI',"Franco fabbrica")
        insert into (i_cTable) (COCODICE,CCCODISO,CODESCRI &i_ccchkf. );
           values (;
             "E";
             ,"ITA";
             ,"Franco fabbrica";
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Insert into CON_CONS
      i_nConn=i_TableProp[this.CON_CONS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CON_CONS_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CON_CONS_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"COCODICE"+",CCCODISO"+",CODESCRI"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC("F"),'CON_CONS','COCODICE');
        +","+cp_NullLink(cp_ToStrODBC("ITA"),'CON_CONS','CCCODISO');
        +","+cp_NullLink(cp_ToStrODBC("Franco vettore"),'CON_CONS','CODESCRI');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'COCODICE',"F",'CCCODISO',"ITA",'CODESCRI',"Franco vettore")
        insert into (i_cTable) (COCODICE,CCCODISO,CODESCRI &i_ccchkf. );
           values (;
             "F";
             ,"ITA";
             ,"Franco vettore";
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Insert into CON_CONS
      i_nConn=i_TableProp[this.CON_CONS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CON_CONS_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CON_CONS_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"COCODICE"+",CCCODISO"+",CODESCRI"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC("C"),'CON_CONS','COCODICE');
        +","+cp_NullLink(cp_ToStrODBC("ITA"),'CON_CONS','CCCODISO');
        +","+cp_NullLink(cp_ToStrODBC("Costo e nolo"),'CON_CONS','CODESCRI');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'COCODICE',"C",'CCCODISO',"ITA",'CODESCRI',"Costo e nolo")
        insert into (i_cTable) (COCODICE,CCCODISO,CODESCRI &i_ccchkf. );
           values (;
             "C";
             ,"ITA";
             ,"Costo e nolo";
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Insert into CON_CONS
      i_nConn=i_TableProp[this.CON_CONS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CON_CONS_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CON_CONS_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"COCODICE"+",CCCODISO"+",CODESCRI"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC("D"),'CON_CONS','COCODICE');
        +","+cp_NullLink(cp_ToStrODBC("ITA"),'CON_CONS','CCCODISO');
        +","+cp_NullLink(cp_ToStrODBC("Reso"),'CON_CONS','CODESCRI');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'COCODICE',"D",'CCCODISO',"ITA",'CODESCRI',"Reso")
        insert into (i_cTable) (COCODICE,CCCODISO,CODESCRI &i_ccchkf. );
           values (;
             "D";
             ,"ITA";
             ,"Reso";
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Insert into CON_CONS
      i_nConn=i_TableProp[this.CON_CONS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CON_CONS_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CON_CONS_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"COCODICE"+",CCCODISO"+",CODESCRI"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC("N"),'CON_CONS','COCODICE');
        +","+cp_NullLink(cp_ToStrODBC("ITA"),'CON_CONS','CCCODISO');
        +","+cp_NullLink(cp_ToStrODBC("Nessuna"),'CON_CONS','CODESCRI');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'COCODICE',"N",'CCCODISO',"ITA",'CODESCRI',"Nessuna")
        insert into (i_cTable) (COCODICE,CCCODISO,CODESCRI &i_ccchkf. );
           values (;
             "N";
             ,"ITA";
             ,"Nessuna";
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Spagna
      * --- Insert into CON_CONS
      i_nConn=i_TableProp[this.CON_CONS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CON_CONS_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CON_CONS_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"COCODICE"+",CCCODISO"+",CODESCRI"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC("1"),'CON_CONS','COCODICE');
        +","+cp_NullLink(cp_ToStrODBC("ESP"),'CON_CONS','CCCODISO');
        +","+cp_NullLink(cp_ToStrODBC("En la fabrica"),'CON_CONS','CODESCRI');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'COCODICE',"1",'CCCODISO',"ESP",'CODESCRI',"En la fabrica")
        insert into (i_cTable) (COCODICE,CCCODISO,CODESCRI &i_ccchkf. );
           values (;
             "1";
             ,"ESP";
             ,"En la fabrica";
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Insert into CON_CONS
      i_nConn=i_TableProp[this.CON_CONS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CON_CONS_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CON_CONS_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"COCODICE"+",CCCODISO"+",CODESCRI"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC("2"),'CON_CONS','COCODICE');
        +","+cp_NullLink(cp_ToStrODBC("ESP"),'CON_CONS','CCCODISO');
        +","+cp_NullLink(cp_ToStrODBC("Franco transportista"),'CON_CONS','CODESCRI');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'COCODICE',"2",'CCCODISO',"ESP",'CODESCRI',"Franco transportista")
        insert into (i_cTable) (COCODICE,CCCODISO,CODESCRI &i_ccchkf. );
           values (;
             "2";
             ,"ESP";
             ,"Franco transportista";
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Insert into CON_CONS
      i_nConn=i_TableProp[this.CON_CONS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CON_CONS_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CON_CONS_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"COCODICE"+",CCCODISO"+",CODESCRI"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC("3"),'CON_CONS','COCODICE');
        +","+cp_NullLink(cp_ToStrODBC("ESP"),'CON_CONS','CCCODISO');
        +","+cp_NullLink(cp_ToStrODBC("Franco al contado del buque"),'CON_CONS','CODESCRI');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'COCODICE',"3",'CCCODISO',"ESP",'CODESCRI',"Franco al contado del buque")
        insert into (i_cTable) (COCODICE,CCCODISO,CODESCRI &i_ccchkf. );
           values (;
             "3";
             ,"ESP";
             ,"Franco al contado del buque";
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Insert into CON_CONS
      i_nConn=i_TableProp[this.CON_CONS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CON_CONS_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CON_CONS_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"COCODICE"+",CCCODISO"+",CODESCRI"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC("4"),'CON_CONS','COCODICE');
        +","+cp_NullLink(cp_ToStrODBC("ESP"),'CON_CONS','CCCODISO');
        +","+cp_NullLink(cp_ToStrODBC("Franco a bordo"),'CON_CONS','CODESCRI');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'COCODICE',"4",'CCCODISO',"ESP",'CODESCRI',"Franco a bordo")
        insert into (i_cTable) (COCODICE,CCCODISO,CODESCRI &i_ccchkf. );
           values (;
             "4";
             ,"ESP";
             ,"Franco a bordo";
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Insert into CON_CONS
      i_nConn=i_TableProp[this.CON_CONS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CON_CONS_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CON_CONS_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"COCODICE"+",CCCODISO"+",CODESCRI"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC("5"),'CON_CONS','COCODICE');
        +","+cp_NullLink(cp_ToStrODBC("ESP"),'CON_CONS','CCCODISO');
        +","+cp_NullLink(cp_ToStrODBC("Coste y flete (C&F)"),'CON_CONS','CODESCRI');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'COCODICE',"5",'CCCODISO',"ESP",'CODESCRI',"Coste y flete (C&F)")
        insert into (i_cTable) (COCODICE,CCCODISO,CODESCRI &i_ccchkf. );
           values (;
             "5";
             ,"ESP";
             ,"Coste y flete (C&F)";
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Insert into CON_CONS
      i_nConn=i_TableProp[this.CON_CONS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CON_CONS_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CON_CONS_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"COCODICE"+",CODESCRI"+",CCCODISO"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC("6"),'CON_CONS','COCODICE');
        +","+cp_NullLink(cp_ToStrODBC("Coste, seguro y flete"),'CON_CONS','CODESCRI');
        +","+cp_NullLink(cp_ToStrODBC("ESP"),'CON_CONS','CCCODISO');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'COCODICE',"6",'CODESCRI',"Coste, seguro y flete",'CCCODISO',"ESP")
        insert into (i_cTable) (COCODICE,CODESCRI,CCCODISO &i_ccchkf. );
           values (;
             "6";
             ,"Coste, seguro y flete";
             ,"ESP";
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Insert into CON_CONS
      i_nConn=i_TableProp[this.CON_CONS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CON_CONS_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CON_CONS_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"COCODICE"+",CODESCRI"+",CCCODISO"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC("7"),'CON_CONS','COCODICE');
        +","+cp_NullLink(cp_ToStrODBC("Porte pagado hasta"),'CON_CONS','CODESCRI');
        +","+cp_NullLink(cp_ToStrODBC("ESP"),'CON_CONS','CCCODISO');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'COCODICE',"7",'CODESCRI',"Porte pagado hasta",'CCCODISO',"ESP")
        insert into (i_cTable) (COCODICE,CODESCRI,CCCODISO &i_ccchkf. );
           values (;
             "7";
             ,"Porte pagado hasta";
             ,"ESP";
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Insert into CON_CONS
      i_nConn=i_TableProp[this.CON_CONS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CON_CONS_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CON_CONS_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"COCODICE"+",CODESCRI"+",CCCODISO"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC("8"),'CON_CONS','COCODICE');
        +","+cp_NullLink(cp_ToStrODBC("Porte pagado, incluido seguro, hasta"),'CON_CONS','CODESCRI');
        +","+cp_NullLink(cp_ToStrODBC("ESP"),'CON_CONS','CCCODISO');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'COCODICE',"8",'CODESCRI',"Porte pagado, incluido seguro, hasta",'CCCODISO',"ESP")
        insert into (i_cTable) (COCODICE,CODESCRI,CCCODISO &i_ccchkf. );
           values (;
             "8";
             ,"Porte pagado, incluido seguro, hasta";
             ,"ESP";
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Insert into CON_CONS
      i_nConn=i_TableProp[this.CON_CONS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CON_CONS_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CON_CONS_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"COCODICE"+",CODESCRI"+",CCCODISO"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC("9"),'CON_CONS','COCODICE');
        +","+cp_NullLink(cp_ToStrODBC("Franco frontera"),'CON_CONS','CODESCRI');
        +","+cp_NullLink(cp_ToStrODBC("ESP"),'CON_CONS','CCCODISO');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'COCODICE',"9",'CODESCRI',"Franco frontera",'CCCODISO',"ESP")
        insert into (i_cTable) (COCODICE,CODESCRI,CCCODISO &i_ccchkf. );
           values (;
             "9";
             ,"Franco frontera";
             ,"ESP";
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Insert into CON_CONS
      i_nConn=i_TableProp[this.CON_CONS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CON_CONS_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CON_CONS_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"COCODICE"+",CODESCRI"+",CCCODISO"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC("W"),'CON_CONS','COCODICE');
        +","+cp_NullLink(cp_ToStrODBC("Franco EX SHIP"),'CON_CONS','CODESCRI');
        +","+cp_NullLink(cp_ToStrODBC("ESP"),'CON_CONS','CCCODISO');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'COCODICE',"W",'CODESCRI',"Franco EX SHIP",'CCCODISO',"ESP")
        insert into (i_cTable) (COCODICE,CODESCRI,CCCODISO &i_ccchkf. );
           values (;
             "W";
             ,"Franco EX SHIP";
             ,"ESP";
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Insert into CON_CONS
      i_nConn=i_TableProp[this.CON_CONS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CON_CONS_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CON_CONS_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"COCODICE"+",CODESCRI"+",CCCODISO"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC("Y"),'CON_CONS','COCODICE');
        +","+cp_NullLink(cp_ToStrODBC("Franco muelle"),'CON_CONS','CODESCRI');
        +","+cp_NullLink(cp_ToStrODBC("ESP"),'CON_CONS','CCCODISO');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'COCODICE',"Y",'CODESCRI',"Franco muelle",'CCCODISO',"ESP")
        insert into (i_cTable) (COCODICE,CODESCRI,CCCODISO &i_ccchkf. );
           values (;
             "Y";
             ,"Franco muelle";
             ,"ESP";
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Insert into CON_CONS
      i_nConn=i_TableProp[this.CON_CONS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CON_CONS_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CON_CONS_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"COCODICE"+",CODESCRI"+",CCCODISO"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC("Z"),'CON_CONS','COCODICE');
        +","+cp_NullLink(cp_ToStrODBC("Franco sin despachar en aduana"),'CON_CONS','CODESCRI');
        +","+cp_NullLink(cp_ToStrODBC("ESP"),'CON_CONS','CCCODISO');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'COCODICE',"Z",'CODESCRI',"Franco sin despachar en aduana",'CCCODISO',"ESP")
        insert into (i_cTable) (COCODICE,CODESCRI,CCCODISO &i_ccchkf. );
           values (;
             "Z";
             ,"Franco sin despachar en aduana";
             ,"ESP";
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Insert into CON_CONS
      i_nConn=i_TableProp[this.CON_CONS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CON_CONS_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CON_CONS_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"COCODICE"+",CODESCRI"+",CCCODISO"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC("K"),'CON_CONS','COCODICE');
        +","+cp_NullLink(cp_ToStrODBC("Franco despacho en aduana"),'CON_CONS','CODESCRI');
        +","+cp_NullLink(cp_ToStrODBC("ESP"),'CON_CONS','CCCODISO');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'COCODICE',"K",'CODESCRI',"Franco despacho en aduana",'CCCODISO',"ESP")
        insert into (i_cTable) (COCODICE,CODESCRI,CCCODISO &i_ccchkf. );
           values (;
             "K";
             ,"Franco despacho en aduana";
             ,"ESP";
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Insert into CON_CONS
      i_nConn=i_TableProp[this.CON_CONS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CON_CONS_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CON_CONS_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"COCODICE"+",CODESCRI"+",CCCODISO"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC("X"),'CON_CONS','COCODICE');
        +","+cp_NullLink(cp_ToStrODBC("Condiciones de entrega distinta de las anteriores"),'CON_CONS','CODESCRI');
        +","+cp_NullLink(cp_ToStrODBC("ESP"),'CON_CONS','CCCODISO');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'COCODICE',"X",'CODESCRI',"Condiciones de entrega distinta de las anteriores",'CCCODISO',"ESP")
        insert into (i_cTable) (COCODICE,CODESCRI,CCCODISO &i_ccchkf. );
           values (;
             "X";
             ,"Condiciones de entrega distinta de las anteriores";
             ,"ESP";
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
    * --- Devo controllare che la tabella ANAG_PRO esista nel database che si intende
    *     convertire.
    l_Temp = SQLEXEC(i_Serverconn(1,2), "select * from ANAG_PRO")
    if l_Temp>=0
      * --- Salvo il vecchio contenuto della tabella ANAG_PRO
      l_ret=cp_SQL(i_ServerConn[1,2],"select * from ANAG_PRO","AppoCur")
      if l_ret < 0
        * --- Errore Esecuzione
        this.oParentObject.w_PESEOK = .F.
        this.oParentObject.w_PMSG = this.oParentObject.w_PMSG + Message()
        this.oParentObject.w_PMSG = this.oParentObject.w_PMSG + chr(13)+Ah_Msgformat("Errore durante il salvataggio della tabella %1","ANAG_PRO")
        if this.w_NHF>=0
          this.w_TMPC = Ah_Msgformat("ERRORE GENERICO - Impossibile creare il cursore %1","AppoCur")
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
        use in select("AppoCur")
        i_retcode = 'stop'
        return
      endif
       
 Select AppoCur 
 go top 
 scan
      this.w_CODPRO = AppoCur.PRCODPRO
      this.w_DESPRO = AppoCur.PRDESPRO
      * --- Insert into ANAG_PRO
      i_nConn=i_TableProp[this.ANAG_PRO_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ANAG_PRO_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ANAG_PRO_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"PRCODPRO"+",PRDESPRO"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_CODPRO),'ANAG_PRO','PRCODPRO');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DESPRO),'ANAG_PRO','PRDESPRO');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'PRCODPRO',this.w_CODPRO,'PRDESPRO',this.w_DESPRO)
        insert into (i_cTable) (PRCODPRO,PRDESPRO &i_ccchkf. );
           values (;
             this.w_CODPRO;
             ,this.w_DESPRO;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      endscan
      if used("Appocur")
        select Appocur
        use
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    if bTrsErr
      * --- Raise
      i_Error=i_TrsMsg
      return
    endif
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = Ah_Msgformat("Inserimento dati nelle tabelle %1 eseguito correttamente","TIP_DIST/CON_CONS/ANAG_PRO")
    if this.w_NHF>=0 
      this.w_TMPC = this.oParentObject.w_PMSG
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,6)]
    this.cWorkTables[1]='TIP_DIST'
    this.cWorkTables[2]='ANAG_PRO'
    this.cWorkTables[3]='CON_CONS'
    this.cWorkTables[4]='AZIENDA'
    this.cWorkTables[5]='AHEECONV'
    this.cWorkTables[6]='CONVERSI'
    return(this.OpenAllTables(6))

  proc CloseCursors()
    if used('_Curs_AZIENDA')
      use in _Curs_AZIENDA
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
