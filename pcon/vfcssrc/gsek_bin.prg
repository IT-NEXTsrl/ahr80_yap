* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsek_bin                                                        *
*              Elabora inventario                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_279]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-06-04                                                      *
* Last revis.: 2001-06-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_Evento
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsek_bin",oParentObject,m.w_Evento)
return(i_retval)

define class tgsek_bin as StdBatch
  * --- Local variables
  w_Evento = space(10)
  w_PAG2 = space(20)
  w_MAGRAG = space(5)
  w_INNUMINV = space(6)
  w_INDATINV = ctod("  /  /  ")
  w_INCODESE = space(4)
  w_INNUMPRE = space(6)
  w_INESEPRE = space(4)
  w_INCODMAG = space(5)
  w_INCATOMO = space(5)
  w_INGRUMER = space(5)
  w_INCODFAM = space(5)
  w_INDESINV = space(40)
  w_INTIPINV = space(1)
  w_INSTAINV = space(1)
  w_INELABOR = space(1)
  w_INMAGCOL = space(1)
  w_INFLGLCO = space(1)
  w_INFLGLSC = space(1)
  w_INFLGFCO = space(1)
  w_PRDATINV = ctod("  /  /  ")
  w_ESINIESE = ctod("  /  /  ")
  w_FLMESS = space(1)
  w_VALESE = space(3)
  w_VALPRE = space(3)
  w_DECESE = 0
  w_CAOESE = 0
  w_CAOPRE = 0
  w_PrzUlt = 0
  w_RecElab = 0
  w_CosMpa = 0
  w_PqtMpp = 0
  w_CosMpp = 0
  w_QtaScagl = 0
  w_CqtMpp = 0
  w_CpRowNum = 0
  w_CosUlt = 0
  w_Mess = space(10)
  w_NEWREC = space(10)
  w_Appo = space(20)
  w_TmpN = 0
  w_Appo1 = 0
  w_QtaEsiUno = 0
  w_UltimoArt = space(40)
  w_IsDatMov = ctod("  /  /  ")
  w_CosSta = 0
  w_IsDatFin = ctod("  /  /  ")
  w_CosLco = 0
  w_IsQtaEsi = ctod("  /  /  ")
  w_CosLsc = 0
  w_IsValUni = 0
  w_CosFco = 0
  w_DataPrzUlt = ctod("  /  /  ")
  w_DataCosUlt = ctod("  /  /  ")
  w_TipValor = space(1)
  w_ValQta = space(1)
  w_TipMagaz = space(1)
  w_ValTip = space(1)
  w_Segnalato = .f.
  w_DatReg = space(10)
  w_Trec = 0
  w_PrePreUlt = 0
  w_QtaVenUno = 0
  w_PreQtaEsr = 0
  w_QtaAcqUno = 0
  w_PreQtaVer = 0
  w_PreQtaAcr = 0
  w_QtaEsiWrite = 0
  w_QtaEsr = 0
  w_QtaVenWrite = 0
  w_QtaVer = 0
  w_QtaAcqWrite = 0
  w_Messaggio = space(10)
  w_FlaVal = space(10)
  w_Codice = space(40)
  w_CriVal = space(10)
  w_CodArt = space(20)
  w_PreQtaEsi = 0
  w_PreQtaVen = 0
  w_QtaEsi = 0
  w_PreQtaAcq = 0
  w_QtaVen = 0
  w_PrePrzMpa = 0
  w_QtaAcq = 0
  w_PreCosMpa = 0
  w_PrzMpa = 0
  w_PreCosUlt = 0
  w_PrzMpp = 0
  w_ElenMag = space(10)
  w_QtaAcr = 0
  w_Maga = space(5)
  w_RaggrMagaColl = space(5)
  w_SLCODART = space(20)
  * --- WorkFile variables
  INVEDETT_idx=0
  LIFOSCAT_idx=0
  LIFOCONT_idx=0
  FIFOCONT_idx=0
  INVENTAR_idx=0
  VALUTE_idx=0
  MAGAZZIN_idx=0
  ESERCIZI_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elaborazione Inventario per EURO-KIT (da GSEK_AIN)
    * --- L'elaborazione dell'inventario, in caso di update, viene effettuata solo se � stato modificato un campo
    * --- Valori previsti: INSERTED (dopo l'inserimento), UPDATED (dopo la variazione)
    * --- che non sia lo stato o la descrizione: questo viene fatto testando la w_UPDINV, calcolata in GSMA_BVS
    this.w_INELABOR = this.oParentObject.w_INELABOR
    this.w_PAG2 = "DICHIARAZIONI"
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_PAG2 = "CURSORI"
    * --- D'ora in avanti la pag2 sar� chiamata solo per chiudere i cursori
    * --- Richiesta conferma
    if NOT ah_YesNo("ATTENZIONE%0L'elaborazione dell'inventario pu� richiedere%0molto tempo in relazione alla dimensione degli archivi%0%0Confermi l'elaborazione?")
      i_retcode = 'stop'
      return
    endif
    * --- Verifica campi obbligatori e compilazione elenco magazzini
    this.w_MAGRAG = SPACE(5)
    * --- Read from MAGAZZIN
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "MGMAGRAG"+;
        " from "+i_cTable+" MAGAZZIN where ";
            +"MGCODMAG = "+cp_ToStrODBC(this.w_INCODMAG);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        MGMAGRAG;
        from (i_cTable) where;
            MGCODMAG = this.w_INCODMAG;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_MAGRAG = NVL(cp_ToDate(_read_.MGMAGRAG),cp_NullValue(_read_.MGMAGRAG))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_MAGRAG = IIF (EMPTY(NVL(this.w_MAGRAG,"")),this.w_INCODMAG,this.w_MAGRAG)
    this.Pag8()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Eliminazione elaborazione precedente
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Creazione cursore articoli compresi in inventario
    create cursor AppMov ;
    (CACODART C(20), MMCODMAG C(5), CMCRIVAL C(2), CMFLAVAL C(1), CMFLCASC C(1), ;
    VALORE N(18,5), MMDATREG D(8), MGMAGRAG C(5))
    ah_Msg("Lettura movimenti di magazzino 1...",.T.)
    vq_exec("..\PCON\EXE\QUERY\GSEKIBIN.VQR",this,"__tmp__")
    if reccount("__tmp__")>0
      this.w_NEWREC = "#########"
      select __tmp__
      go top
      scan for not empty(nvl(MMCODART," ")) and not empty(nvl(MMCODMAG,""))
      if this.w_NEWREC <>MMCODART+MMCODMAG+NVL(CMCRIVAL,"  ")+NVL(CMFLAVAL," ")+NVL(CMFLCASC," ")
        INSERT INTO AppMov ;
        (CACODART, MMCODMAG, VALORE, CMCRIVAL, CMFLAVAL, CMFLCASC, MMDATREG, MGMAGRAG) VALUES ;
        (__TMP__.MMCODART, __TMP__.MMCODMAG, NVL(__Tmp__.VALUNI,0 ), ;
        NVL(__TMP__.CMCRIVAL,"  "), NVL(__TMP__.CMFLAVAL," "), NVL(__TMP__.CMFLCASC," "), __TMP__.MMDATREG,;
        NVL(__TMP__.MGMAGRAG,SPACE(5)))
        this.w_NEWREC = MMCODART+MMCODMAG+NVL(CMCRIVAL,"  ")+NVL(CMFLAVAL," ")+NVL(CMFLCASC," ")
      endif
      select __tmp__
      endscan
      select __tmp__
      USE
    else
      if EMPTY(this.w_INNUMPRE)
        ah_ErrorMsg("Per la selezione effettuata non esistono dati da elaborare",,"")
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        i_retcode = 'stop'
        return
      endif
    endif
    ah_Msg("Lettura movimenti di magazzino 2...",.T.)
    vq_exec("..\PCON\EXE\QUERY\GSEK_BIN.VQR",this,"__tmp__")
    if reccount("__tmp__")=0
      ah_ErrorMsg("Per la selezione effettuata non esistono dati da elaborare",,"")
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      i_retcode = 'stop'
      return
    endif
    * --- Raggruppamento dati ed eliminazione delle informazioni non necessarie
    ah_Msg("Raggruppamento movimenti per articolo...",.T.)
    * --- 1) La select seguente crea piu' records di totali per ogni articolo distinti per Tipo Movimento e Criterio di Valorizzazione.
    * --- 2) Rispetto alla visual query raggruppa record con le stesse caratteristiche che erano rimasti separati perche' provenienti
    * ---      da archivi differenti (TipoRiga: A=Articoli, M=Mov.Magazzino, V=Vendite).
    * --- 3) Per le righe di tipo articolo (TipoRiga=A) la data di registrazione non deve essere considerata.
    select cacodart, mmcodmag, "A" as Ordine, ;
    max(cp_CharToDate("  -  -  ")) as mmdatreg, ;
    sum(iif(cauprival $ "AC", MagPriCar-MagPriSca, MagPriCar*0)) as Acquisti, ;
    sum(iif(cauprival $ "VS", MagPriSca-MagPriCar, MagPriSca*0)) as Vendite, ;
    sum(iif(cauprival $ "AC", Valore, Valore*0)) as ValAcqu, ;
    sum(iif(cauprival $ "VS", Valore, Valore*0)) as ValVend, ;
    CauPriCri as CauPriCri, ;
    CauPriVal as CauPriVal, ;
    CauPriEsi as CauPriEsi, ;
    sum(9999999999999.9999*0) as Valore, mgmagrag as mgmagrag ;
    from __tmp__ ;
    group by cacodart, mmcodmag, Ordine, CauPriCri, CauPriVal, CauPriEsi, mgmagrag ;
    union select cacodart, mmcodmag, "Z" as Ordine, ;
    mmdatreg as mmdatreg, ;
    0 as Acquisti, ;
    0 as Vendite, ;
    0 as ValAcqu, ;
    0 as ValVend, ;
    CMCRIVAL as CauPriCri , ;
    CMFLAVAL as CauPriVal , ;
    CMFLCASC as CauPriEsi, ;
    VALORE as Valore, mgmagrag as mgmagrag ;
    from AppMov ;
    group by cacodart, mmcodmag, Ordine, CauPriCri, CauPriVal, CauPriEsi,mgmagrag ;
    order by 1,2,3 ;
    into cursor __tmp__
    select AppMov
    use
    if this.w_INFLGLSC = "S" .or. this.w_INFLGLCO = "S" .or. this.w_INFLGFCO = "S"
      ah_Msg("Elabora movimenti per calcoli LIFO/FIFO...",.T.)
      * --- Ordinati per Codice Articolo, Data registrazione, Seq. Elaborazione, Serial
      vq_exec("..\PCON\EXE\QUERY\GSEKABIN.VQR",this,"FifoLifo")
    endif
    ah_Msg("Lettura parametri articoli/magazzini...",.T.)
    vq_exec("query\GSMAUBIN",this,"DatiArti")
    * --- Memorizzazione anagrafica inventario con scrittura del flag elaborato
    ah_Msg("Aggiornamento anagrafica inventario...",.T.)
    * --- Write into INVENTAR
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.INVENTAR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INVENTAR_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.INVENTAR_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"INELABOR ="+cp_NullLink(cp_ToStrODBC("S"),'INVENTAR','INELABOR');
          +i_ccchkf ;
      +" where ";
          +"INNUMINV = "+cp_ToStrODBC(this.w_INNUMINV);
          +" and INCODESE = "+cp_ToStrODBC(this.w_INCODESE);
             )
    else
      update (i_cTable) set;
          INELABOR = "S";
          &i_ccchkf. ;
       where;
          INNUMINV = this.w_INNUMINV;
          and INCODESE = this.w_INCODESE;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Elaborazione, valorizzazioni ed aggiornamento archivi storici inventario
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Chiusura cursori
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    ah_ErrorMsg("Elaborazione inventario terminata",,"")
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case this.w_PAG2="CURSORI"
        * --- Chiusura cursori
        if used("__tmp__")
          select __tmp__
          use
        endif
        if used("FifoLifo")
          select FifoLifo
          use
        endif
        if used("DatiArti")
          select DatiArti
          use
        endif
        if used("valori")
          select valori
          use
        endif
        if used("magazzini")
          select magazzini
          use
        endif
        if used("ScLiCo")
          select ScLiCo
          use
        endif
        if used("ScLiSc")
          select ScLiSc
          use
        endif
        if used("ScFiCo")
          select ScFiCo
          use
        endif
        if used("ScPrec")
          select ScPrec
          use
        endif
        if used("AppMov")
          select AppMov
          use
        endif
      case this.w_PAG2="DICHIARAZIONI"
        * --- Variabili locali inizializzate con il valore dei campi dell'anagrafica
        * --- Vengono inizializzate in modo diverso dal solito perche' devono essere letti i valori dei campi
        * --- dell'anagrafica al momento della chiamata del batch. Questo e' necessario perche' nelle righe
        * --- successive viene richiamato l'evento di salvataggio dell'anagrafica che, dopo aver memorizzato
        * --- i records, ripulisce il contenuto dei campi.
        this.w_INNUMINV = this.oParentObject.w_INNUMINV
        this.w_INDATINV = this.oParentObject.w_INDATINV
        this.w_INCODESE = this.oParentObject.w_INCODESE
        this.w_INNUMPRE = this.oParentObject.w_INNUMPRE
        this.w_INESEPRE = this.oParentObject.w_INESEPRE
        this.w_INCODMAG = this.oParentObject.w_INCODMAG
        this.w_INCATOMO = this.oParentObject.w_INCATOMO
        this.w_INGRUMER = this.oParentObject.w_INGRUMER
        this.w_INCODFAM = this.oParentObject.w_INCODFAM
        this.w_INDESINV = this.oParentObject.w_INDESINV
        this.w_INTIPINV = this.oParentObject.w_INTIPINV
        this.w_INSTAINV = this.oParentObject.w_INSTAINV
        this.w_INMAGCOL = this.oParentObject.w_INMAGCOL
        this.w_INFLGLCO = this.oParentObject.w_INFLGLCO
        this.w_INFLGLSC = this.oParentObject.w_INFLGLSC
        this.w_INFLGFCO = this.oParentObject.w_INFLGFCO
        this.w_PRDATINV = this.oParentObject.w_PRDATINV
        this.w_ESINIESE = this.oParentObject.w_ESINIESE
        this.w_FLMESS = this.oParentObject.w_FLMESS
        this.w_VALESE = this.oParentObject.w_VALESE
        this.w_VALPRE = this.oParentObject.w_VALPRE
        this.w_DECESE = g_PERPUL
        this.w_CAOESE = g_CAOVAL
        * --- Legge i Riferimenti alle Valute Inventari
        * --- Read from VALUTE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VALUTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "VACAOVAL,VADECUNI"+;
            " from "+i_cTable+" VALUTE where ";
                +"VACODVAL = "+cp_ToStrODBC(this.w_VALESE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            VACAOVAL,VADECUNI;
            from (i_cTable) where;
                VACODVAL = this.w_VALESE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CAOESE = NVL(cp_ToDate(_read_.VACAOVAL),cp_NullValue(_read_.VACAOVAL))
          this.w_DECESE = NVL(cp_ToDate(_read_.VADECUNI),cp_NullValue(_read_.VADECUNI))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_CAOESE = IIF(EMPTY(this.w_CAOESE), g_CAOVAL, this.w_CAOESE)
        this.w_CAOPRE = this.w_CAOESE
        if NOT EMPTY(this.w_INNUMPRE)
          * --- Read from VALUTE
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.VALUTE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "VACAOVAL"+;
              " from "+i_cTable+" VALUTE where ";
                  +"VACODVAL = "+cp_ToStrODBC(this.w_VALPRE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              VACAOVAL;
              from (i_cTable) where;
                  VACODVAL = this.w_VALPRE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CAOPRE = NVL(cp_ToDate(_read_.VACAOVAL),cp_NullValue(_read_.VACAOVAL))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_CAOPRE = IIF(EMPTY(this.w_CAOPRE), this.w_CAOESE, this.w_CAOPRE)
        endif
        * --- Variabili locali
        this.w_Messaggio = ""
        this.w_FlaVal = ""
        this.w_Codice = 0
        this.w_CriVal = ""
        this.w_CodArt = ""
        this.w_PreQtaEsi = 0
        this.w_PreQtaVen = 0
        this.w_QtaEsi = 0
        this.w_PreQtaAcq = 0
        this.w_QtaVen = 0
        this.w_PrePrzMpa = 0
        this.w_QtaAcq = 0
        this.w_PreCosMpa = 0
        this.w_PrzMpa = 0
        this.w_PreCosUlt = 0
        this.w_PrzMpp = 0
        this.w_ElenMag = ""
        this.w_PrzUlt = 0
        this.w_RecElab = 0
        this.w_CosMpa = 0
        this.w_PqtMpp = 0
        this.w_CosMpp = 0
        this.w_QtaScagl = 0
        this.w_CqtMpp = 0
        this.w_CpRowNum = 0
        this.w_CosUlt = 0
        this.w_IsDatMov = cp_CharToDate("  -  -  ")
        this.w_CosSta = 0
        this.w_IsDatFin = cp_CharToDate("  -  -  ")
        this.w_CosLco = 0
        this.w_IsQtaEsi = 0
        this.w_CosLsc = 0
        this.w_IsValUni = 0
        this.w_CosFco = 0
        this.w_DataPrzUlt = cp_CharToDate("  -  -  ")
        this.w_UltimoArt = ""
        this.w_DataCosUlt = cp_CharToDate("  -  -  ")
        this.w_TipValor = ""
        this.w_ValQta = 0
        this.w_TipMagaz = ""
        this.w_ValTip = 0
        this.w_Segnalato = .F.
        this.w_DatReg = cp_CharToDate("  -  -  ")
        this.w_Trec = 0
        this.w_QtaEsiUno = 0
        this.w_PrePreUlt = 0
        this.w_QtaVenUno = 0
        this.w_PreQtaEsr = 0
        this.w_QtaAcqUno = 0
        this.w_PreQtaVer = 0
        this.w_PreQtaAcr = 0
        this.w_QtaEsiWrite = 0
        this.w_QtaEsr = 0
        this.w_QtaVenWrite = 0
        this.w_QtaVer = 0
        this.w_QtaAcqWrite = 0
        this.w_QtaAcr = 0
    endcase
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elaborazioni dati presenti in inventario
    * --- Aggiornamento archivi
    ah_Msg("Aggiornamento archivi storici inventario...",.T.)
    * --- Creazione cursore scaglioni Lifo Continuo
    if this.w_INFLGLCO = "S"
      create cursor ScLiCo (DatIni D(8,0) , QtaEsi N(12,3) , ValUni N(18,5))
      index on dtos(DatIni) tag tag1
    endif
    * --- Creazione cursore scaglioni Lifo Scatti
    if this.w_INFLGLSC = "S"
      create cursor ScLiSc (DatIni D(8,0) , DatFin D(8,0) , QtaEsi N(12,3) , ValUni N(18,5))
      index on dtos(DatIni)+dtos(DatFin) tag tag1
    endif
    * --- Creazione cursore scaglioni Fifo Continuo
    if this.w_INFLGFCO = "S"
      create cursor ScFiCo (DatIni D(8,0) , QtaEsi N(12,3) , ValUni N(18,5))
      index on dtos(DatIni) tag tag1
    endif
    * --- Try
    local bErr_03743CC0
    bErr_03743CC0=bTrsErr
    this.Try_03743CC0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Messaggio errore aggiornamento archivi
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      ah_ErrorMsg("Impossibile aggiornare gli archivi storici dell'inventario",,"")
      i_retcode = 'stop'
      return
    endif
    bTrsErr=bTrsErr or bErr_03743CC0
    * --- End
    * --- Azzeramento messaggio di elaborazione
    wait clear
  endproc
  proc Try_03743CC0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Imposta il codice dell'ultimo articolo elaborato
    this.w_UltimoArt = "<BOF>"
    * --- Imposta le data fino alle quali sono stati considerati i movimenti di prezzo e costo
    this.w_DataPrzUlt = cp_CharToDate("  -  -  ")
    this.w_DataCosUlt = cp_CharToDate("  -  -  ")
    * --- Aggiornamento valorizzazioni articoli
    this.w_RecElab = 0
    select __tmp__
    this.w_Trec = RECCOUNT()
    go top
    SCAN
    this.w_RecElab = this.w_RecElab + 1
    * --- Lettura dati record corrente
    this.w_Codice = __tmp__.cacodart
    * --- Cambio articolo
    if this.w_Codice <> this.w_UltimoArt
      * --- Messaggio a Video
      ah_Msg("Elabora articolo: %1%0Num. totale articoli %2 (Elaborato il %3%)",.T.,.F.,.F., ALLTRIM(this.w_Codice), ALLTRIM(STR(this.w_Trec)), alltrim(str((this.w_RecElab*100)/this.w_Trec,10,0)) )
      * --- Calcolo delle valorizzazioni che considerano i movimenti dall'inizio dell'esercizio
      this.Pag7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Scrittura valorizzazioni articolo
      this.Pag6()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Azzeramento totalizzatori articolo
      this.w_QtaEsr = 0
      this.w_QtaEsi = 0
      this.w_PrzUlt = 0
      this.w_CosLco = 0
      this.w_QtaEsiUno = 0
      this.w_QtaVer = 0
      this.w_QtaVen = 0
      this.w_CosMpa = 0
      this.w_CosLsc = 0
      this.w_QtaVenUno = 0
      this.w_QtaAcr = 0
      this.w_QtaAcq = 0
      this.w_CosMpp = 0
      this.w_CosFco = 0
      this.w_QtaAcqUno = 0
      this.w_PrzMpa = 0
      this.w_CosUlt = 0
      this.w_PqtMpp = 0
      this.w_PrzMpp = 0
      this.w_CosSta = 0
      this.w_CqtMpp = 0
      * --- Memorizza il codice dell'ultimo articolo elaborato
      this.w_UltimoArt = this.w_Codice
      this.w_CodArt = __tmp__.cacodart
      this.w_DataPrzUlt = cp_CharToDate("  -  -  ")
      this.w_DataCosUlt = cp_CharToDate("  -  -  ")
      this.w_Segnalato = .F.
    endif
    this.w_Maga = NVL(__tmp__.mmcodmag,space(5))
    * --- Va messo fuori dall'if, altrimenti non viene aggiornato quando cambia il magazzino, ma non cambia l'articolo
    this.w_RaggrMagaColl = NVL(__tmp__.mgmagrag, space(5))
    * --- Estraggo il raggruppamento fiscale del magazzino collegato (quello del maga principale � in w_MAGRAG)
    * --- Calcolo valorizzazioni
    this.Pag5()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Passaggio al record successivo
    select __tmp__
    ENDSCAN
    * --- Calcolo delle valorizzazioni che considerano i movimenti dall'inizio dell'esercizio
    this.Pag7()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Scrittura valorizzazioni ultimo articolo
    this.Pag6()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Eliminazione precedenti elaborazioni
    if this.w_INELABOR = "S"
      * --- Richiesta eliminazione
      if this.w_FLMESS="S" AND NOT ah_YesNo("Confermi azzeramento precedente elaborazione?")
        i_retcode = 'stop'
        return
      endif
      * --- Eliminazione elaborazione precedente
      ah_Msg("Eliminazione elaborazione precedente...",.T.)
      * --- Try
      local bErr_03783AE0
      bErr_03783AE0=bTrsErr
      this.Try_03783AE0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        ah_ErrorMsg("Impossibile eliminare gli archivi storici dell'inventario",,"")
        i_retcode = 'stop'
        return
      endif
      bTrsErr=bTrsErr or bErr_03783AE0
      * --- End
    endif
  endproc
  proc Try_03783AE0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- LIFO Continuo
    * --- Delete from LIFOCONT
    i_nConn=i_TableProp[this.LIFOCONT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.LIFOCONT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"ISNUMINV = "+cp_ToStrODBC(this.w_INNUMINV);
            +" and ISCODESE = "+cp_ToStrODBC(this.w_INCODESE);
             )
    else
      delete from (i_cTable) where;
            ISNUMINV = this.w_INNUMINV;
            and ISCODESE = this.w_INCODESE;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- LIFO a Scatti
    * --- Delete from LIFOSCAT
    i_nConn=i_TableProp[this.LIFOSCAT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.LIFOSCAT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"ISNUMINV = "+cp_ToStrODBC(this.w_INNUMINV);
            +" and ISCODESE = "+cp_ToStrODBC(this.w_INCODESE);
             )
    else
      delete from (i_cTable) where;
            ISNUMINV = this.w_INNUMINV;
            and ISCODESE = this.w_INCODESE;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- FIFO Continuo
    * --- Delete from FIFOCONT
    i_nConn=i_TableProp[this.FIFOCONT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.FIFOCONT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"ISNUMINV = "+cp_ToStrODBC(this.w_INNUMINV);
            +" and ISCODESE = "+cp_ToStrODBC(this.w_INCODESE);
             )
    else
      delete from (i_cTable) where;
            ISNUMINV = this.w_INNUMINV;
            and ISCODESE = this.w_INCODESE;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Dettaglio articoli
    * --- Delete from INVEDETT
    i_nConn=i_TableProp[this.INVEDETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INVEDETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"DINUMINV = "+cp_ToStrODBC(this.w_INNUMINV);
            +" and DICODESE = "+cp_ToStrODBC(this.w_INCODESE);
             )
    else
      delete from (i_cTable) where;
            DINUMINV = this.w_INNUMINV;
            and DICODESE = this.w_INCODESE;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcolo Valorizzazioni Periodo
    * --- La procedura richiama questa pagina piu' volte per ogni articolo e calcola tutte le valorizzazioni di periodo.
    select __tmp__
    * --- Inizializzazione dei parametri di selezione delle visual query richiamate in questa pagina
    this.w_DatReg = mmdatreg
    * --- Quantita'
    * --- Esistenza
    this.w_QtaEsi = this.w_QtaEsi + (Acquisti - Vendite)
    this.w_QtaEsr = this.w_QtaEsr + (Acquisti - Vendite)
    * --- Quantita' Venduta
    if CauPriVal$"V|S" AND Vendite<>0 and this.w_RaggrMagaColl<>this.w_MAGRAG
      this.w_QtaVen = this.w_QtaVen + Vendite
      this.w_QtaVer = this.w_QtaVer + Vendite
    endif
    * --- Quantita' Acquistata
    if CauPriVal$"A|C" AND Acquisti<>0 and this.w_RaggrMagaColl<>this.w_MAGRAG
      this.w_QtaAcq = this.w_QtaAcq + Acquisti
      this.w_QtaAcr = this.w_QtaAcr + Acquisti
    endif
    if this.w_Maga=this.w_INCODMAG
      * --- Salvo i dati parziali relativi al solo magazzino specificato
      * --- Esistenza
      this.w_QtaEsiUno = this.w_QtaEsiUno + (Acquisti - Vendite)
      * --- Quantita' Venduta
      if CauPriVal$"V|S" AND Vendite<>0 and this.w_RaggrMagaColl<>this.w_MAGRAG
        this.w_QtaVenUno = this.w_QtaVenUno + Vendite
      endif
      * --- Quantita' Acquistata
      if CauPriVal$"A|C" AND Acquisti<>0 and this.w_RaggrMagaColl<>this.w_MAGRAG
        this.w_QtaAcqUno = this.w_QtaAcqUno + Acquisti
      endif
    endif
    * --- VALORIZZAZIONI
    * --- La valorizzazione NON viene effettuata solo nel caso in cui
    * --- 1) esista una causale collegata (movimento di trasferimento);
    * --- 2) i magazzini appartengano allo stesso raggruppamento fiscale (basta testare questa che � pi� forte)
    if this.w_RaggrMagaColl<>this.w_MAGRAG
      * --- Prezzi
      * --- Prezzo Medio Ponderato Periodo
      * --- Sommatoria delle quantita' per i prezzi divisa per la sommatoria delle quantita'.
      * --- Considera i movimenti dall'inizio del periodo (se reso diminuisce il Valore)
      if CauPriVal$"V|S" AND NVL(Vendite,0)<>0
        this.w_PrzMpp = this.w_PrzMpp + (ValVend * IIF(Vendite<0, -1, 1))
        this.w_PqtMpp = this.w_PqtMpp + Vendite
      endif
      * --- Costi
      * --- Costo Medio Ponderato Periodo
      * --- Sommatoria delle quantita' per i costi divisa per la sommatoria delle quantita'.
      * --- Considera i movimenti dall'inizio del periodo (se reso diminuisce il Valore)
      if CauPriVal$"A|C" AND NVL(Acquisti,0)<>0
        this.w_CosMpp = this.w_CosMpp + (ValAcqu * IIF(Acquisti<0, -1, 1))
        this.w_CqtMpp = this.w_CqtMpp + Acquisti
      endif
      if nvl(Ordine," ")="Z"
        * --- Calcola Ultimo Prezzo/Ultimo Costo
        * --- Verifica se deve eseguire la valorizzazione a Ult.Prezzo o Ult.Costo o Costo Standard
        if this.w_DatReg>this.w_DataPrzUlt AND CauPriVal$"V|S" AND nvl(Valore, 0)<>0 and CauPriEsi="-"
          * --- Ultimo Prezzo
          this.w_PrzUlt = NVL(Valore, 0)
          this.w_DataPrzUlt = this.w_DatReg
        endif
        if this.w_DatReg>this.w_DataCosUlt AND CauPriVal$"A|C" AND CauPriEsi<>"-" AND nvl(Valore, 0)<>0 and CauPriEsi="+"
          * --- Ultimo Costo
          * --- Considera i movimenti dall'inizio del periodo
          * --- ATTENZIONE! Se e' zero prende quello dell'inventario precedente nella pagina delle valorizzazioni d'esercizio.
          this.w_CosUlt = NVL(Valore, 0)
          this.w_DataCosUlt = this.w_DatReg
        endif
      endif
    endif
  endproc


  procedure Pag6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Prima di Aggiornare Calcola il Costo Standard
    * --- Ricerca il Costo Standard nei Dati Articolo per Magazzino
    this.w_CosSta = 0
    if used("DatiArti")
      select DatiArti
      GO TOP
      LOCATE FOR PRCODART=this.w_CodArt AND NVL(PRCODMAG,"     ")=this.w_MAGRAG
      if FOUND()
        this.w_CosSta = NVL(PRCOSSTA,0)
      else
        * --- Se non esiste il Costo Standard riferito al Magazzino cerca quello Generico
        GO TOP
        LOCATE FOR PRCODART=this.w_CodArt
        if FOUND()
          this.w_CosSta = NVL(PRCOSSTA,0)
        endif
      endif
      select __tmp__
    endif
    * --- Scrittura archivi inventario
    if this.w_UltimoArt <> "<BOF>"
      * --- Arrotondamenti in base ai decimali della valuta di conto
      this.w_PrzMpa = cp_round( this.w_PrzMpa , this.w_DECESE )
      this.w_PrzMpp = cp_round( this.w_PrzMpp , this.w_DECESE )
      this.w_PrzUlt = cp_round( this.w_PrzUlt , this.w_DECESE )
      this.w_CosMpa = cp_round( this.w_CosMpa , this.w_DECESE )
      this.w_CosMpp = cp_round( this.w_CosMpp , this.w_DECESE )
      this.w_CosUlt = cp_round( this.w_CosUlt , this.w_DECESE )
      this.w_CosSta = cp_round( this.w_CosSta , this.w_DECESE )
      this.w_CosLco = cp_round( this.w_CosLco , this.w_DECESE )
      this.w_CosLsc = cp_round( this.w_CosLsc , this.w_DECESE )
      this.w_CosFco = cp_round( this.w_CosFco , this.w_DECESE )
      this.w_QTAESIWRITE = iif(this.w_INMAGCOL="S",this.w_QTAESI,this.w_QTAESIUNO)
      this.w_QTAACQWRITE = iif(this.w_INMAGCOL="S",this.w_QTAACQ,this.w_QTAACQUNO)
      this.w_QTAVENWRITE = iif(this.w_INMAGCOL="S",this.w_QTAVEN,this.w_QTAVENUNO)
      * --- Scrittura Dettaglio Articolo
      * --- Insert into INVEDETT
      i_nConn=i_TableProp[this.INVEDETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.INVEDETT_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.INVEDETT_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"DINUMINV"+",DICODICE"+",DIQTAESI"+",DIQTAVEN"+",DIQTAACQ"+",DIPRZMPA"+",DIPRZMPP"+",DICOSMPA"+",DICOSMPP"+",DICOSULT"+",DICOSSTA"+",DICOSLCO"+",DICOSLSC"+",DICOSFCO"+",DIPRZULT"+",DICODESE"+",DIQTAESR"+",DIQTAVER"+",DIQTAACR"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_INNUMINV),'INVEDETT','DINUMINV');
        +","+cp_NullLink(cp_ToStrODBC(this.w_UltimoArt),'INVEDETT','DICODICE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_QTAESIWRITE),'INVEDETT','DIQTAESI');
        +","+cp_NullLink(cp_ToStrODBC(this.w_QTAVENWRITE),'INVEDETT','DIQTAVEN');
        +","+cp_NullLink(cp_ToStrODBC(this.w_QTAACQWRITE),'INVEDETT','DIQTAACQ');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PRZMPA),'INVEDETT','DIPRZMPA');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PRZMPP),'INVEDETT','DIPRZMPP');
        +","+cp_NullLink(cp_ToStrODBC(this.w_COSMPA),'INVEDETT','DICOSMPA');
        +","+cp_NullLink(cp_ToStrODBC(this.w_COSMPP),'INVEDETT','DICOSMPP');
        +","+cp_NullLink(cp_ToStrODBC(this.w_COSULT),'INVEDETT','DICOSULT');
        +","+cp_NullLink(cp_ToStrODBC(this.w_COSSTA),'INVEDETT','DICOSSTA');
        +","+cp_NullLink(cp_ToStrODBC(this.w_COSLCO),'INVEDETT','DICOSLCO');
        +","+cp_NullLink(cp_ToStrODBC(this.w_COSLSC),'INVEDETT','DICOSLSC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_COSFCO),'INVEDETT','DICOSFCO');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PRZULT),'INVEDETT','DIPRZULT');
        +","+cp_NullLink(cp_ToStrODBC(this.w_INCODESE),'INVEDETT','DICODESE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_QTAESR),'INVEDETT','DIQTAESR');
        +","+cp_NullLink(cp_ToStrODBC(this.w_QTAVER),'INVEDETT','DIQTAVER');
        +","+cp_NullLink(cp_ToStrODBC(this.w_QTAACR),'INVEDETT','DIQTAACR');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'DINUMINV',this.w_INNUMINV,'DICODICE',this.w_UltimoArt,'DIQTAESI',this.w_QTAESIWRITE,'DIQTAVEN',this.w_QTAVENWRITE,'DIQTAACQ',this.w_QTAACQWRITE,'DIPRZMPA',this.w_PRZMPA,'DIPRZMPP',this.w_PRZMPP,'DICOSMPA',this.w_COSMPA,'DICOSMPP',this.w_COSMPP,'DICOSULT',this.w_COSULT,'DICOSSTA',this.w_COSSTA,'DICOSLCO',this.w_COSLCO)
        insert into (i_cTable) (DINUMINV,DICODICE,DIQTAESI,DIQTAVEN,DIQTAACQ,DIPRZMPA,DIPRZMPP,DICOSMPA,DICOSMPP,DICOSULT,DICOSSTA,DICOSLCO,DICOSLSC,DICOSFCO,DIPRZULT,DICODESE,DIQTAESR,DIQTAVER,DIQTAACR &i_ccchkf. );
           values (;
             this.w_INNUMINV;
             ,this.w_UltimoArt;
             ,this.w_QTAESIWRITE;
             ,this.w_QTAVENWRITE;
             ,this.w_QTAACQWRITE;
             ,this.w_PRZMPA;
             ,this.w_PRZMPP;
             ,this.w_COSMPA;
             ,this.w_COSMPP;
             ,this.w_COSULT;
             ,this.w_COSSTA;
             ,this.w_COSLCO;
             ,this.w_COSLSC;
             ,this.w_COSFCO;
             ,this.w_PRZULT;
             ,this.w_INCODESE;
             ,this.w_QTAESR;
             ,this.w_QTAVER;
             ,this.w_QTAACR;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error='Errore in Inserimento Dettaglio Inventari'
        return
      endif
      * --- Aggiornamento scaglioni Lifo Continuo
      if this.w_INFLGLCO = "S"
        this.w_CpRowNum = 0
        select ScLiCo
        go top
        do while (.not. eof())
          this.w_IsDatMov = DatIni
          this.w_IsQtaEsi = QtaEsi
          this.w_IsValUni = ValUni
          if this.w_IsQtaEsi<>0
            this.w_CpRowNum = this.w_CpRowNum + 1
            * --- Insert into LIFOCONT
            i_nConn=i_TableProp[this.LIFOCONT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.LIFOCONT_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LIFOCONT_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"ISNUMINV"+",ISCODICE"+",CPROWNUM"+",ISDATMOV"+",ISQTAESI"+",ISVALUNI"+",ISCODESE"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.w_INNUMINV),'LIFOCONT','ISNUMINV');
              +","+cp_NullLink(cp_ToStrODBC(this.w_UltimoArt),'LIFOCONT','ISCODICE');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CpRowNum),'LIFOCONT','CPROWNUM');
              +","+cp_NullLink(cp_ToStrODBC(this.w_IsDatMov),'LIFOCONT','ISDATMOV');
              +","+cp_NullLink(cp_ToStrODBC(this.w_IsQtaEsi),'LIFOCONT','ISQTAESI');
              +","+cp_NullLink(cp_ToStrODBC(this.w_IsValUni),'LIFOCONT','ISVALUNI');
              +","+cp_NullLink(cp_ToStrODBC(this.w_INCODESE),'LIFOCONT','ISCODESE');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'ISNUMINV',this.w_INNUMINV,'ISCODICE',this.w_UltimoArt,'CPROWNUM',this.w_CpRowNum,'ISDATMOV',this.w_IsDatMov,'ISQTAESI',this.w_IsQtaEsi,'ISVALUNI',this.w_IsValUni,'ISCODESE',this.w_INCODESE)
              insert into (i_cTable) (ISNUMINV,ISCODICE,CPROWNUM,ISDATMOV,ISQTAESI,ISVALUNI,ISCODESE &i_ccchkf. );
                 values (;
                   this.w_INNUMINV;
                   ,this.w_UltimoArt;
                   ,this.w_CpRowNum;
                   ,this.w_IsDatMov;
                   ,this.w_IsQtaEsi;
                   ,this.w_IsValUni;
                   ,this.w_INCODESE;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error='Errore in Inserimento LIFO Continuo'
              return
            endif
          endif
          select ScLiCo
          skip
        enddo
        select __tmp__
      endif
      * --- Aggiornamento scaglioni Lifo Scatti
      if this.w_INFLGLSC = "S"
        this.w_CpRowNum = 0
        select ScLiSc
        go top
        do while (.not. eof())
          this.w_IsDatMov = DatIni
          this.w_IsDatFin = DatFin
          this.w_IsQtaEsi = QtaEsi
          * --- Se ValUni non valorizzato perch� presenti solo carichi (quindi non c'� il costo medio del periodo)
          * --- Prendo il costo medio dell'esercizio
          this.w_IsValUni = iif(ValUni=0,this.w_CosMpa,ValUni)
          if this.w_IsQtaEsi<>0
            this.w_CpRowNum = this.w_CpRowNum + 1
            * --- Insert into LIFOSCAT
            i_nConn=i_TableProp[this.LIFOSCAT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.LIFOSCAT_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LIFOSCAT_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"ISNUMINV"+",ISCODICE"+",CPROWNUM"+",ISDATINI"+",ISQTAESI"+",ISVALUNI"+",ISDATFIN"+",ISCODESE"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.w_INNUMINV),'LIFOSCAT','ISNUMINV');
              +","+cp_NullLink(cp_ToStrODBC(this.w_UltimoArt),'LIFOSCAT','ISCODICE');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CpRowNum),'LIFOSCAT','CPROWNUM');
              +","+cp_NullLink(cp_ToStrODBC(this.w_IsDatMov),'LIFOSCAT','ISDATINI');
              +","+cp_NullLink(cp_ToStrODBC(this.w_IsQtaEsi),'LIFOSCAT','ISQTAESI');
              +","+cp_NullLink(cp_ToStrODBC(this.w_IsValUni),'LIFOSCAT','ISVALUNI');
              +","+cp_NullLink(cp_ToStrODBC(this.w_IsDatFin),'LIFOSCAT','ISDATFIN');
              +","+cp_NullLink(cp_ToStrODBC(this.w_INCODESE),'LIFOSCAT','ISCODESE');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'ISNUMINV',this.w_INNUMINV,'ISCODICE',this.w_UltimoArt,'CPROWNUM',this.w_CpRowNum,'ISDATINI',this.w_IsDatMov,'ISQTAESI',this.w_IsQtaEsi,'ISVALUNI',this.w_IsValUni,'ISDATFIN',this.w_IsDatFin,'ISCODESE',this.w_INCODESE)
              insert into (i_cTable) (ISNUMINV,ISCODICE,CPROWNUM,ISDATINI,ISQTAESI,ISVALUNI,ISDATFIN,ISCODESE &i_ccchkf. );
                 values (;
                   this.w_INNUMINV;
                   ,this.w_UltimoArt;
                   ,this.w_CpRowNum;
                   ,this.w_IsDatMov;
                   ,this.w_IsQtaEsi;
                   ,this.w_IsValUni;
                   ,this.w_IsDatFin;
                   ,this.w_INCODESE;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error='Errore in Inserimento LIFO Scatti'
              return
            endif
          endif
          select ScLiSc
          skip
        enddo
        select __tmp__
      endif
      * --- Aggiornamento scaglioni Fifo Continuo
      if this.w_INFLGFCO = "S"
        this.w_CpRowNum = 0
        select ScFiCo
        go top
        do while (.not. eof())
          this.w_IsDatMov = DatIni
          this.w_IsQtaEsi = QtaEsi
          this.w_IsValUni = ValUni
          if this.w_IsQtaEsi<>0
            this.w_CpRowNum = this.w_CpRowNum + 1
            * --- Insert into FIFOCONT
            i_nConn=i_TableProp[this.FIFOCONT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.FIFOCONT_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.FIFOCONT_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"ISNUMINV"+",ISCODICE"+",CPROWNUM"+",ISDATMOV"+",ISQTAESI"+",ISVALUNI"+",ISCODESE"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.w_INNUMINV),'FIFOCONT','ISNUMINV');
              +","+cp_NullLink(cp_ToStrODBC(this.w_UltimoArt),'FIFOCONT','ISCODICE');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CpRowNum),'FIFOCONT','CPROWNUM');
              +","+cp_NullLink(cp_ToStrODBC(this.w_IsDatMov),'FIFOCONT','ISDATMOV');
              +","+cp_NullLink(cp_ToStrODBC(this.w_IsQtaEsi),'FIFOCONT','ISQTAESI');
              +","+cp_NullLink(cp_ToStrODBC(this.w_IsValUni),'FIFOCONT','ISVALUNI');
              +","+cp_NullLink(cp_ToStrODBC(this.w_INCODESE),'FIFOCONT','ISCODESE');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'ISNUMINV',this.w_INNUMINV,'ISCODICE',this.w_UltimoArt,'CPROWNUM',this.w_CpRowNum,'ISDATMOV',this.w_IsDatMov,'ISQTAESI',this.w_IsQtaEsi,'ISVALUNI',this.w_IsValUni,'ISCODESE',this.w_INCODESE)
              insert into (i_cTable) (ISNUMINV,ISCODICE,CPROWNUM,ISDATMOV,ISQTAESI,ISVALUNI,ISCODESE &i_ccchkf. );
                 values (;
                   this.w_INNUMINV;
                   ,this.w_UltimoArt;
                   ,this.w_CpRowNum;
                   ,this.w_IsDatMov;
                   ,this.w_IsQtaEsi;
                   ,this.w_IsValUni;
                   ,this.w_INCODESE;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error='Errore in Inserimento FIFO Continuo'
              return
            endif
          endif
          select ScFiCo
          skip
        enddo
        select __tmp__
      endif
    endif
  endproc


  procedure Pag7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcolo Valorizzazioni Esercizio
    * --- La procedura richiama questa pagina una sola volte per ogni articolo e calcola tutte le valorizzazioni d'esercizio.
    * --- Calcolo delle valorizzazioni che considerano i movimenti dall'inizio dell'esercizio
    * --- Variabili relative all'inventario precedente
    this.w_PreQtaEsi = 0
    this.w_PreQtaVen = 0
    this.w_PreQtaAcq = 0
    this.w_PreQtaEsr = 0
    this.w_PreQtaVer = 0
    this.w_PreQtaAcr = 0
    this.w_PrePrzMpa = 0
    this.w_PreCosMpa = 0
    this.w_PreCosUlt = 0
    this.w_PrePreUlt = 0
    if this.w_UltimoArt <> "<BOF>"
      * --- Legge le valorizzazioni dell'inventario precedente
      if NOT EMPTY(this.w_INNUMPRE)
        vq_exec("query\GSMA6BIN",this,"valori")
        select valori
        go top
        if reccount() > 0
          this.w_PreQtaEsi = DIQTAESI
          this.w_PreQtaEsr = DIQTAESR
          if this.w_INCODESE=this.w_INESEPRE
            * --- Vengono estratte le qta totali di raggruppamento
            * --- per eseguire correttamente i calcoli a partire da un inventario precedente
            this.w_PreQtaVen = DIQTAVEN
            this.w_PreQtaAcq = DIQTAACQ
            this.w_PreQtaVer = DIQTAVER
            this.w_PreQtaAcr = DIQTAACR
          endif
          this.w_PrePrzMpa = DIPRZMPA
          this.w_PreCosMpa = DICOSMPA
          this.w_PreCosUlt = DICOSULT
          this.w_PrePreUlt = DIPRZULT
          if this.w_CAOPRE<>this.w_CAOESE
            * --- Se L'inventario precedente e' di una altra valuta converte i valori alla valuta del nuovo
            this.w_PrePrzMpa = cp_ROUND(VAL2MON(this.w_PrePrzMpa,this.w_CAOPRE,this.w_CAOESE, this.w_INDATINV), this.w_DECESE)
            this.w_PreCosMpa = cp_ROUND(VAL2MON(this.w_PreCosMpa,this.w_CAOPRE,this.w_CAOESE, this.w_INDATINV), this.w_DECESE)
            this.w_PreCosUlt = cp_ROUND(VAL2MON(this.w_PreCosUlt,this.w_CAOPRE,this.w_CAOESE, this.w_INDATINV), this.w_DECESE)
            this.w_PrePreUlt = cp_ROUND(VAL2MON(this.w_PrePreUlt,this.w_CAOPRE,this.w_CAOESE, this.w_INDATINV), this.w_DECESE)
          endif
        endif
        select __tmp__
      endif
      * --- Quantita'
      * --- Esistenza
      * --- L'esistenza e' data dalla somma della quantita' presente nel periodo precedente con la differenza fra
      * --- gli acquisti e le vendite del periodo (calcolata nella pagina delle valorizzazioni di periodo).
      this.w_QtaEsi = this.w_PreQtaEsi + this.w_QtaEsi
      this.w_QtaEsr = this.w_PreQtaEsr+this.w_QtaEsr
      if this.w_INMAGCOL="N"
        * --- OLD: w_Maga=w_INCODMAG
        this.w_QtaEsiUno = this.w_PreQtaEsi + this.w_QtaEsiUno
      endif
      * --- VALORIZZAZIONI
      * --- La valorizzazione NON viene effettuata solo nel caso in cui
      * --- 1) esista una causale collegata (movimento di trasferimento);
      * --- 2) i magazzini appartengano allo stesso raggruppamento fiscale (basta testare questa che � pi� forte)
      * --- (in quel caso non calcolo neanche gli scaglioni)
      * --- Prezzi
      * --- Prezzo Medio Ponderato Periodo
      * --- Adesso che sono stati letti tutti i prezzi e le quantit� dell'articolo � possibile calcolare la media del periodo
      this.w_PrzMpp = iif( empty( this.w_PqtMpp ) , 0 , this.w_PrzMpp / this.w_PqtMpp )
      * --- Prezzo Medio Ponderato Esercizio
      * --- Sommatoria delle quantita' per i prezzi divisa per la sommatoria delle quantita'.
      * --- Considera i movimenti dall'inizio dell'esercizio
      * --- Se non e' stato specificato l'inventario precedente corrisponde al valore di periodo.
      if this.w_QtaVer+this.w_PreQtaVer<>0
        this.w_PrzMpa = ((this.w_PrzMpp*this.w_QtaVer) + (this.w_PrePrzMpa*this.w_PreQtaVer)) / (this.w_QtaVer+this.w_PreQtaVer)
      else
        if this.w_PreQtaVer=0
          this.w_PrzMpa = this.w_PrePrzMpa
        else
          * --- Se le quantit� si annullano senza questo default avrei una divisione per zero!
          this.w_PrzMpa = 0
        endif
      endif
      * --- Costi
      * --- Costo Medio Ponderato Periodo
      * --- Adesso che sono stati letti tutti i prezzi e le quantit� dell'articolo � possibile calcolare la media del periodo
      this.w_CosMpp = iif( empty( this.w_CqtMpp ) , 0 , this.w_CosMpp / this.w_CqtMpp )
      * --- Costo Medio Ponderato Esercizio
      * --- Sommatoria delle quantita' per i costi divisa per la sommatoria delle quantita'.
      * --- Considera i movimenti dall'inizio dell'esercizio
      * --- Se non e' stato specificato l'inventario precedente o non � dello stesso Esercizio corrisponde al valore di periodo.
      if this.w_QtaAcr+this.w_PreQtaAcr<>0
        this.w_CosMpa = ((this.w_CosMpp*this.w_QtaAcr) + (this.w_PreCosMpa*this.w_PreQtaAcr)) / (this.w_QtaAcr+this.w_PreQtaAcr)
      else
        if this.w_PreQtaAcr=0
          this.w_CosMpa = this.w_PreCosMpa
        else
          * --- Se le quantit� si annullano senza questo default avrei una divisione per zero!
          this.w_CosMpa = 0
        endif
      endif
      * --- Ultimo Costo
      * --- Se e' zero l'ultimo costo del periodo prende quello dell'inventario precedente
      if empty( this.w_CosUlt ) .and. (.not. empty(this.w_PreCosUlt))
        this.w_CosUlt = this.w_PreCosUlt
      endif
      * --- Ultimo Prezzo
      * --- Se e' zero l'ultimo prezzo del periodo prende quello dell'inventario precedente
      if empty( this.w_PrzUlt ) .and. (.not. empty(this.w_PrePreUlt))
        this.w_PrzUlt = this.w_PrePreUlt
      endif
      * --- OLD:  IF NOT EMPTY(w_INNUMPRE) AND w_INESEPRE=w_INCODESE
      this.w_QtaAcq = this.w_QtaAcq + this.w_PreQtaAcq
      this.w_QtaVen = this.w_QtaVen + this.w_PreQtaVen
      this.w_QtaAcr = this.w_QtaAcr + this.w_PreQtaAcr
      this.w_QtaVer = this.w_QtaVer + this.w_PreQtaVer
      if this.w_INMAGCOL="N"
        * --- OLD: w_Maga=w_INCODMAG
        this.w_QtaAcqUno = this.w_QtaAcqUno + iif(this.w_INCODESE<>this.w_INESEPRE,0,this.w_PreQtaAcq)
        this.w_QtaVenUno = this.w_QtaVenUno + iif(this.w_INCODESE<>this.w_INESEPRE,0,this.w_PreQtaVen)
      endif
      * --- LIFO e FIFO
      * --- Se l'esistenza e' invariata mantiene la valorizzazione dell'inventario precedente.
      * --- Se l'esistenza e' diminuita toglie la quantita' dagli scaglioni memorizzati in precedenza.
      * --- Se l'esistenza e' aumentata aggiunge uno scaglione per il nuovo periodo.
      if this.w_INFLGLSC = "S" .or. this.w_INFLGLCO = "S" .or. this.w_INFLGFCO = "S"
        * --- Le valorizzazioni vengono calcolate soltanto se l'esistenza e' cambiata.
        * --- In caso contrario la procedura si limita a copiare gli scaglioni del periodo precedente se e'
        * --- stato specificato.
        * --- Azzero sempre i cursori degli scaglioni Prima di controllare l'esistenza
        * --- (altrimenti non vengono riportati quelli precedenti)
        if this.w_INFLGLCO = "S"
          select ScLiCo
          zap
        endif
        if this.w_INFLGLSC = "S"
          select ScLiSc
          zap
        endif
        if this.w_INFLGFCO = "S"
          select ScFiCo
          zap
        endif
        if (this.w_PreQtaEsr<>this.w_QtaEsr) .or. (.not. empty( this.w_INNUMPRE ))
          * --- Lettura movimenti del periodo
          * --- Crea un nuovo cursore perch� quello dell'elaborazione principale non contiene il dettaglio dei
          * --- movimenti ma i valori raggruppati.
          SELECT mmdatreg, mmserial, cmseqcal, tipo, mmcodmag, qtaprin, valtot, cauprival, mmcaucol, mmcodmat ;
          FROM FifoLifo ;
          WHERE mmcodart = this.w_CODART ;
          INTO CURSOR valori ORDER BY mmdatreg, cmseqcal, mmserial
          select valori
          * --- Lifo Continuo
          if this.w_INFLGLCO = "S"
            this.w_TipValor = "LIFOCONTINUO"
            * --- Selezione cursore scaglioni
            select ScLiCo
            * --- Lettura scaglioni periodo precedente
            this.Pag10()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- Ai Fini delle Valorizzazioni devono essere considerati i soli Movimenti di Acquisto e Vendita
            if (this.w_PreQtaEsr<>this.w_QtaEsr)
              select valori
              go top
              SCAN
              * --- Magazzino principale
              if mmcodmag $ this.w_ElenMag AND CauPriVal $ "AVCS" and !(nvl(mmcodmat,"") $ this.w_ElenMag)
                * --- Considero Acquisti, Vendite, Carico, Scarico
                * --- Nell'elaborazione controllo il campo Tipo='A' se Cauprival$'AC' o Tipo='V' se Cauprival$'VS'
                this.Pag9()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
              select valori
              ENDSCAN
            endif
            * --- Calcolo valore LIFO Continuo
            select sum(qtaesi) as qtaesi, sum(valuni*qtaesi) as valuni from ScLiCo into cursor SommaLF
            this.w_CosLco = iif( sommalf.qtaesi = 0 , 0 , sommalf.valuni / sommalf.qtaesi )
          endif
          * --- Lifo Scatti
          if this.w_INFLGLSC = "S"
            this.w_TipValor = "LIFOSCATTI"
            * --- Creazione cursore scaglioni
            select ScLiSc
            * --- Lettura scaglioni periodo precedente
            this.Pag10()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- Aggiornamento scaglioni
            do case
              case this.w_QtaEsr > this.w_PreQtaEsr
                * --- L'esistenza e' aumentata.
                * --- Aggiunge uno scaglione per la quantita' aumentata valorizzato a costo medio ponderato del periodo.
                select ScLiSc
                append blank
                replace DatIni with iif(empty(this.w_PRDATINV),this.w_ESINIESE,this.w_PRDATINV)
                replace DatFin with this.w_INDATINV
                replace QtaEsi with ( this.w_QtaEsi - this.w_PreQtaEsi )
                replace ValUni with cp_round( this.w_CosMpp , this.w_DECESE )
              case this.w_QtaEsr < this.w_PreQtaEsr
                * --- L'esistenza e' diminuita
                * --- Toglie dagli scaglione esistenti la quantita' diminuita
                this.w_TipMagaz = ""
                this.Pag9()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
            endcase
            * --- Calcolo valore LIFO Scatti
            select sum(qtaesi) as qtaesi, sum(valuni*qtaesi) as valuni from ScLiSc into cursor SommaLF
            this.w_CosLsc = iif( sommalf.qtaesi = 0 , 0 , sommalf.valuni / sommalf.qtaesi )
          endif
          * --- Fifo Continuo
          if this.w_INFLGFCO = "S"
            this.w_TipValor = "FIFOCONTINUO"
            * --- Creazione cursore scaglioni
            select ScFiCo
            * --- Lettura scaglioni periodo precedente
            this.Pag10()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- Ai Fini delle Valorizzazioni devono essere considerati i soli Movimenti di Acquisto e Vendita
            select valori
            go top
            if this.w_PreQtaEsr<>this.w_QtaEsr or reccount("valori")>0
              SCAN
              * --- Magazzino principale
              if mmcodmag $ this.w_ElenMag AND CauPriVal $ "AVCS" and !(nvl(mmcodmat,"") $ this.w_ElenMag)
                * --- Considero Acuisti, Vendite, Carico, Scarico
                * --- Nell'elaborazione controllo il campo Tipo='A' se Cauprival$'AC' o Tipo='V' se Cauprival$'VS'
                this.Pag9()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
              select valori
              ENDSCAN
            endif
            * --- Calcolo valore FIFO Continuo
            select sum(qtaesi) as qtaesi, sum(valuni*qtaesi) as valuni from ScFiCo into cursor SommaLF
            this.w_CosFco = iif( sommalf.qtaesi = 0 , 0 , sommalf.valuni / sommalf.qtaesi )
          endif
        endif
        select __tmp__
      endif
    endif
  endproc


  procedure Pag8
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Verifica campi obbligatori e compilazione elenco magazzini
    * --- Verifica campi obbligatori
    if empty(this.w_INDATINV) .or. empty(this.w_INCODESE) .or. empty(this.w_INDESINV) .or. empty(this.w_INCODMAG)
      ah_ErrorMsg("Specificare il valore dei campi obbligatori",,"")
      i_retcode = 'stop'
      return
    endif
    * --- Lettura elenco magazzini interessati
    this.w_ElenMag = ""
    vq_exec("query\GSMAXBIN",this,"magazzini")
    select magazzini
    go top
    SCAN
    if .not. empty(mgcodmag)
      this.w_ElenMag = this.w_ElenMag + "|" + mgcodmag
    endif
    ENDSCAN
    use
    if empty(this.w_ElenMag)
      ah_ErrorMsg("Il magazzino selezionato non � utilizzabile",,"")
      i_retcode = 'stop'
      return
    endif
  endproc


  procedure Pag9
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna cursori scaglioni
    * --- Seleziona il cursore
    do case
      case this.w_TipValor = "LIFOCONTINUO"
        select ScLiCo
      case this.w_TipValor = "FIFOCONTINUO"
        select ScFiCo
      case this.w_TipValor = "LIFOSCATTI"
        select ScLiSc
    endcase
    * --- Aggiorna gli scaglioni
    * --- Il campo valori.Tipo assume il valore "A" sia in caso di acquisto che in caso di carico
    * --- Il campo valori.Tipo assume il valore "V" sia in caso di vendita che in caso di scarico
    this.w_ValQta = valori.qtaprin
    this.w_ValTip = valori.tipo
    if valori.qtaprin<0
      * --- Se Reso Inverte il Tipo
      this.w_ValQta = ABS(this.w_ValQta)
      this.w_ValTip = IIF(this.w_ValTip="A", "V", "A")
    endif
    if this.w_ValTip="A" AND this.w_TipValor <> "LIFOSCATTI"
      * --- Acquisto o altro carico
      * --- Aggiunge uno scaglione con la data della registrazione.
      append blank
      replace DatIni with valori.mmdatreg
      replace QtaEsi with this.w_ValQta
      replace ValUni with IIF(this.w_ValQta<>0, valori.valtot/this.w_ValQta, 0)
    else
      * --- Vendita, altro scarico o LIFO a Scatti
      * --- Toglie la quantita' dagli scaglioni caricati con gli acquisti e gli altri carichi
      * --- Se la valorizzazione corrente e' LIFO a Scatti non considera i dati presenti sul temporaneo dei movimenti ma
      * --- rimuove dagli scaglioni la quantita' diminuita rispetto al periodo precedente.
      if this.w_TipValor = "LIFOSCATTI"
        this.w_QtaScagl = this.w_PreQtaEsr - this.w_QtaEsr
      else
        this.w_QtaScagl = this.w_ValQta
      endif
      do case
        case this.w_TipValor = "LIFOCONTINUO" .or. this.w_TipValor = "LIFOSCATTI"
          * --- LIFO Continuo ed a Scatti : Toglie la quantita' dagli scaglioni finali
          go bottom
          do while (.not. bof()) .and. this.w_QtaScagl > QtaEsi
            this.w_QtaScagl = this.w_QtaScagl - QtaEsi
            delete
            go bottom
          enddo
        case this.w_TipValor = "FIFOCONTINUO"
          * --- FIFO Continuo : Toglie la quantita' dagli scaglioni iniziali
          go top
          do while (.not. eof()) .and. this.w_QtaScagl > QtaEsi
            this.w_QtaScagl = this.w_QtaScagl - QtaEsi
            delete
            go top
          enddo
      endcase
      * --- Toglie la quantita' residua
      if bof() .or. eof()
        * --- Interrompe l'elaborazione se gli scaglioni sono incongruenti (vendite maggiori degli acquisti)
        if .not. this.w_Segnalato
          if this.w_FLMESS="S"
            if NOT ah_YesNo("L'articolo %1 presenta dati LIFO/FIFO incongruenti%0%0Si desidera proseguire?","", trim(this.w_Codart))
              * --- rollback
              bTrsErr=.t.
              cp_EndTrs(.t.)
              i_retcode = 'stop'
              return
            endif
          endif
          this.w_Segnalato = .T.
        endif
      else
        * --- Toglie la quantita' restante
        if this.w_QtaScagl > 0
          replace QtaEsi with QtaEsi - this.w_QtaScagl
        endif
      endif
    endif
    * --- Si riposiziona sul cursore dei movimenti
    select valori
  endproc


  procedure Pag10
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Copia scaglioni periodo precedente
    if (.not. empty( this.w_INNUMPRE ))
      * --- Creazione cursore
      do case
        case this.w_TipValor = "LIFOCONTINUO"
          vq_exec("query\GSMARBIN",this,"ScPrec")
        case this.w_TipValor = "FIFOCONTINUO"
          vq_exec("query\GSMATBIN",this,"ScPrec")
        case this.w_TipValor = "LIFOSCATTI"
          vq_exec("query\GSMASBIN",this,"ScPrec")
      endcase
      * --- Trasferimento records
      select ScPrec
      go top
      SCAN
      this.w_Appo1 = ScPrec.ValUni
      if this.w_CAOPRE<>this.w_CAOESE
        * --- Se L'inventario precedente e' di una altra valuta converte i valori alla valuta del nuovo
        this.w_Appo1 = cp_ROUND(VAL2MON(this.w_Appo1,this.w_CAOPRE,this.w_CAOESE, this.w_INDATINV), this.w_DECESE)
      endif
      * --- Selezione cursore da aggiornare
      do case
        case this.w_TipValor = "LIFOCONTINUO"
          select ScLiCo
        case this.w_TipValor = "FIFOCONTINUO"
          select ScFiCo
        case this.w_TipValor = "LIFOSCATTI"
          select ScLiSc
      endcase
      * --- Scrittura record
      append blank
      replace DatIni with ScPrec.DatIni
      replace QtaEsi with ScPrec.QtaEsi
      replace ValUni with this.w_Appo1
      if this.w_TipValor = "LIFOSCATTI"
        replace DatFin with ScPrec.DatFin
      endif
      * --- Passaggio al record successivo
      selec ScPrec
      ENDSCAN
    endif
    * --- Si riposiziona sul cursore dei movimenti
    select valori
  endproc


  proc Init(oParentObject,w_Evento)
    this.w_Evento=w_Evento
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,8)]
    this.cWorkTables[1]='INVEDETT'
    this.cWorkTables[2]='LIFOSCAT'
    this.cWorkTables[3]='LIFOCONT'
    this.cWorkTables[4]='FIFOCONT'
    this.cWorkTables[5]='INVENTAR'
    this.cWorkTables[6]='VALUTE'
    this.cWorkTables[7]='MAGAZZIN'
    this.cWorkTables[8]='ESERCIZI'
    return(this.OpenAllTables(8))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_Evento"
endproc
