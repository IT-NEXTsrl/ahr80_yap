* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcb29                                                        *
*              Modifica campi DPTELEF1,DPTELEF2,DPRESVIA,DPRESLOC tabella DIPENDEN*
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_11]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-05-18                                                      *
* Last revis.: 2012-04-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcb29",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcb29 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TMPC = space(10)
  w_TEMP = space(10)
  w_CNAME = space(30)
  w_CODAZI = 0
  w_PHNAME = space(50)
  w_IDXTABLE = 0
  w_CONN = 0
  w_nCount = 0
  w_FIELDLEN = 0
  w_LOOP = 0
  w_iRows = 0
  w_FIELD = space(200)
  w_LOOP = 0
  w_CTABLE = space(30)
  w_MSGERROR = space(100)
  * --- WorkFile variables
  RIPATMP1_idx=0
  DIPENDEN_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Riduco la dimensione dei campi DPTELEF1,DPTELEF2,DPRESVIA,DPRESLOC presenti nella tabella DIPENDEN
    * --- FIle di LOG
    this.w_MSGERROR = ""
    * --- Try
    local bErr_04A297A8
    bErr_04A297A8=bTrsErr
    this.Try_04A297A8()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = this.w_MSGERROR+Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = AH_MsgFormat("ERRORE GENERICO - %1", this.oParentObject.w_PMSG)
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_04A297A8
    * --- End
  endproc
  proc Try_04A297A8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    this.w_FIELD = " "
    this.w_CNAME = "DIPENDEN"
    this.w_FIELDLEN = 1
    this.w_LOOP = 1
    this.w_IDXTABLE = cp_OpenTable( Alltrim(this.w_CNAME) ,.T.)
    this.w_CONN = i_TableProp[ this.w_IDXTABLE , 3 ] 
    this.w_CTABLE = cp_SetAzi(i_TableProp[this.w_IDXTABLE,2])
    * --- Write into DIPENDEN
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DIPENDEN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DIPENDEN_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="DPCODICE"
      do vq_exec with 'GSCVCB29',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DIPENDEN_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="DIPENDEN.DPCODICE = _t2.DPCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"DPTELEF1 = _t2.DPTELEF1";
          +",DPTELEF2 = _t2.DPTELEF2";
          +",DPRESLOC = _t2.DPRESLOC";
          +",DPRESVIA = _t2.DPRESVIA";
          +i_ccchkf;
          +" from "+i_cTable+" DIPENDEN, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="DIPENDEN.DPCODICE = _t2.DPCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DIPENDEN, "+i_cQueryTable+" _t2 set ";
          +"DIPENDEN.DPTELEF1 = _t2.DPTELEF1";
          +",DIPENDEN.DPTELEF2 = _t2.DPTELEF2";
          +",DIPENDEN.DPRESLOC = _t2.DPRESLOC";
          +",DIPENDEN.DPRESVIA = _t2.DPRESVIA";
          +Iif(Empty(i_ccchkf),"",",DIPENDEN.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="DIPENDEN.DPCODICE = t2.DPCODICE";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DIPENDEN set (";
          +"DPTELEF1,";
          +"DPTELEF2,";
          +"DPRESLOC,";
          +"DPRESVIA";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.DPTELEF1,";
          +"t2.DPTELEF2,";
          +"t2.DPRESLOC,";
          +"t2.DPRESVIA";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="DIPENDEN.DPCODICE = _t2.DPCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DIPENDEN set ";
          +"DPTELEF1 = _t2.DPTELEF1";
          +",DPTELEF2 = _t2.DPTELEF2";
          +",DPRESLOC = _t2.DPRESLOC";
          +",DPRESVIA = _t2.DPRESVIA";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".DPCODICE = "+i_cQueryTable+".DPCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"DPTELEF1 = (select DPTELEF1 from "+i_cQueryTable+" where "+i_cWhere+")";
          +",DPTELEF2 = (select DPTELEF2 from "+i_cQueryTable+" where "+i_cWhere+")";
          +",DPRESLOC = (select DPRESLOC from "+i_cQueryTable+" where "+i_cWhere+")";
          +",DPRESVIA = (select DPRESVIA from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    do while this.w_LOOP<= 4
      this.w_FIELDLEN = ICASE(this.w_LOOP=1 OR this.w_LOOP=2,18,this.w_LOOP=3,35,this.w_LOOP=4,30)
      this.w_FIELD = ICASE(this.w_LOOP=1,"DPTELEF1", this.w_LOOP=2,"DPTELEF2",this.w_LOOP=3,"DPRESVIA",this.w_LOOP=4,"DPRESLOC")
      this.w_iRows = CP_SQLExec(this.w_CONN,"alter table "+this.w_CTABLE+" "+db_ModifyColumn(this.w_FIELD,db_FieldType(CP_DBTYPE,"C",this.w_FIELDLEN,0),CP_DBTYPE,this.w_CONN)+iif(upper(CP_DBTYPE)="SQLSERVER"," NULL",""))
      this.w_LOOP = this.w_LOOP+1
    enddo
    this.w_FIELD = "DPTELEF1,DPTELEF2,DPRESVIA,DPRESLOC"
    this.w_iRows = CP_SQLExec(this.w_CONN,"select "+this.w_FIELD+" from "+this.w_CTABLE+" where 1=0")
    this.w_nCount = AFIELDS(Arrfield)
    if this.w_iRows=-1
      * --- Raise
      i_Error="Errore"+MESSAGE()
      return
    endif
    if Arrfield(1,3)>18 OR Arrfield(2,3)>18 OR Arrfield(3,3)>35 OR Arrfield(4,3)>30
      * --- Rimuove l'integritÓ referenziale...
      *     Prima rimuovo dall'azienda corrente poi dall'azienda XXX
      this.w_CODAZI = i_CODAZI
      GSCV_BIN(this, this.w_CODAZI , this.w_CNAME , this.w_NHF )
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_CODAZI = "xxx"
      GSCV_BIN(this, this.w_CODAZI , this.w_CNAME , this.w_NHF )
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Solo per l'azienda corrente passo i dati
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if bTrsErr
      this.w_TMPC = AH_MsgFormat("Impossibile modificare la struttura dei campi %1 in tabella %2",this.w_FIELD,this.w_CNAME)
      * --- Raise
      i_Error=this.w_TMPC
      return
    else
      if this.w_NHF>=0
        this.w_TMPC = AH_MsgFormat("Aggiornamento struttura campi %1 in tabella %2 completata con successo",this.w_FIELD,this.w_CNAME)
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = AH_MsgFormat("Conversione eseguita correttamente")
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella DB2
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('GSCVCB29',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    * --- Elimino la tabella
    this.w_IDXTABLE = cp_OpenTable( Alltrim(this.w_CNAME) ,.T.)
    this.w_CONN = i_TableProp[ this.w_IDXTABLE , 3 ] 
    if Not GSCV_BDT( this, this.w_CNAME , this.w_Conn )
      * --- Raise
      i_Error="Error"
      return
    endif
    * --- Ricostruisco il database per la tabella
    GSCV_BRT(this, this.w_CNAME , this.w_Conn , .F.)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into DIPENDEN
    i_nConn=i_TableProp[this.DIPENDEN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DIPENDEN_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.DIPENDEN_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_Msg("Ripristinati valori di origine",.T.)
    * --- Ricostruisco il database per la tabella
    GSCV_BRT(this, this.w_CNAME , this.w_Conn , .T., "all")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    ah_Msg("Eliminata tabella temporanea tmp",.T.)
  endproc


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='*RIPATMP1'
    this.cWorkTables[2]='DIPENDEN'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
