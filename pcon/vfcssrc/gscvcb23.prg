* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcb23                                                        *
*              Ricostruzione integritÓ referenziali archivi                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_11]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-05-18                                                      *
* Last revis.: 2009-06-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcb23",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcb23 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TMPC = space(10)
  w_TEMP = space(10)
  w_CODREL = space(10)
  w_TABELLA = space(40)
  w_LoopIFK = 0
  w_TEMP = space(10)
  w_CONN = 0
  w_LTABLE = space(200)
  w_IDXTABLEIFK = 0
  * --- WorkFile variables
  CONVERSI_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Ricostruisce tabelle oggetto di procedura di covnersione: 
    *     GSCVCB09
    *     GSCVCB18
    *     GSCVCB90
    *     
    *     che ha causa di un errore su GSCV_BIN rimuovevano le intergirtÓ referenziali
    *     anche alle tabelle collegate delle tabelle oggetto di procedura di conversione
    * --- FIle di LOG
    * --- Read from CONVERSI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CONVERSI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONVERSI_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "COCODREL"+;
        " from "+i_cTable+" CONVERSI where ";
            +"CONOMPRO = "+cp_ToStrODBC("GSCVCB23");
            +" and COESEGUI = "+cp_ToStrODBC("S");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        COCODREL;
        from (i_cTable) where;
            CONOMPRO = "GSCVCB23";
            and COESEGUI = "S";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CODREL = NVL(cp_ToDate(_read_.COCODREL),cp_NullValue(_read_.COCODREL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if EMPTY(this.w_CODREL)
      * --- Try
      local bErr_035D25E0
      bErr_035D25E0=bTrsErr
      this.Try_035D25E0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- Gestisce log errori
        this.oParentObject.w_PMSG = Message()
        this.oParentObject.w_PESEOK = .F.
        if this.w_NHF>=0
          this.w_TMPC = AH_MsgFormat("ERRORE GENERICO - %1", this.oParentObject.w_PMSG)
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
      bTrsErr=bTrsErr or bErr_035D25E0
      * --- End
    else
      if this.w_NHF>=0
        this.w_TMPC = AH_MsgFormat("Aggiornamento terminato con successo")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
      this.oParentObject.w_PESEOK = .T.
    endif
  endproc
  proc Try_035D25E0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Per ogni archivio occorre determinare l'elenco degli archivi che
    *     da esso dipendono, e per ognuno occorre ricostruire il database...
    * --- Elenco archivi:
    *     ALL_ATTR
    *     ALL_EGAT
    *     BAN_CONTI
    *     DES_DIVE
    *     NOM_ATTR
    *     NOM_CONT
    *     OFF_ATTI
    *     OFF_ATTR
    *     OFF_DETT
    *     OFF_ERTE
    *     OFF_NOMI
    *     OFF_SEZI
    *     RIS_ESTE
    *     
    this.w_TABELLA = "ALL_ATTR"
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_TABELLA = "ALL_EGAT"
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_TABELLA = "BAN_CONTI"
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_TABELLA = "DES_DIVE"
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_TABELLA = "NOM_ATTR"
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_TABELLA = "NOM_CONT"
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_TABELLA = "OFF_ATTI"
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_TABELLA = "OFF_ATTR"
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_TABELLA = "OFF_DETT"
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_TABELLA = "OFF_ERTE"
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_TABELLA = "OFF_NOMI"
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_TABELLA = "OFF_SEZI"
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_TABELLA = "RIS_ESTE"
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if this.w_NHF>=0
      this.w_TMPC = AH_MsgFormat("Aggiornamento terminato con successo")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = AH_MsgFormat("Conversione eseguita correttamente")
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Ripristino integritÓ referenziali
    this.w_LoopIFK = 1
    do while this.w_LoopIFK<=i_Dcx.GetFKCount( this.w_TABELLA )
      this.w_LTABLE = I_DCX.GetFKTable( this.w_TABELLA , this.w_LoopIFK )
      this.w_IDXTABLEIFK = cp_OpenTable( Alltrim(this.w_LTABLE) ,.T.)
      this.w_CONN = i_TableProp[ this.w_IDXTABLEIFK , 3 ] 
      GSCV_BRT(this, this.w_LTABLE , this.w_Conn , .T.)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if this.w_NHF>=0
        this.w_TMPC = AH_MSGFORMAT( "IntegritÓ ripristinate per tabella %1 " , this.w_LTABLE)
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
      this.w_LoopIFK = this.w_LoopIFK + 1 
    enddo
  endproc


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='CONVERSI'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
