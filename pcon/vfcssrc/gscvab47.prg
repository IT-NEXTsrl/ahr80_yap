* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvab47                                                        *
*              Popola la tabella ammortamenti cespiti                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_38]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-11-11                                                      *
* Last revis.: 2004-11-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvab47",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvab47 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_MESS = space(100)
  w_CECODICE = space(20)
  w_CEESPRIU = space(4)
  w_CECODESE = space(4)
  w_INIESE = ctod("  /  /  ")
  w_INIESE2 = ctod("  /  /  ")
  w_CECOEFI1 = 0
  w_CECOEFI2 = 0
  w_CEQUOFIS = 0
  w_CEQUOFI1 = 0
  * --- WorkFile variables
  CES_PITI_idx=0
  AMM_CESP_idx=0
  ESERCIZI_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questa procedura valorizza la tabella AMM_CESP 
    * --- FIle di LOG
    * --- Try
    local bErr_04A9E6A0
    bErr_04A9E6A0=bTrsErr
    this.Try_04A9E6A0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE %1 - impossibile aggiornare la tabella cespiti/ammortamenti", this.oParentObject.w_PMSG)
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_04A9E6A0
    * --- End
  endproc
  proc Try_04A9E6A0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Select from CES_PITI
    i_nConn=i_TableProp[this.CES_PITI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CES_PITI_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" CES_PITI ";
          +" where CEESPRIU IS NOT NULL AND CECODESE IS NOT NULL AND CECOEFI1<>0 ";
           ,"_Curs_CES_PITI")
    else
      select * from (i_cTable);
       where CEESPRIU IS NOT NULL AND CECODESE IS NOT NULL AND CECOEFI1<>0 ;
        into cursor _Curs_CES_PITI
    endif
    if used('_Curs_CES_PITI')
      select _Curs_CES_PITI
      locate for 1=1
      do while not(eof())
      this.w_CECODICE = _Curs_CES_PITI.CECODICE
      this.w_CEESPRIU = _Curs_CES_PITI.CEESPRIU
      * --- Read from ESERCIZI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ESERCIZI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ESINIESE"+;
          " from "+i_cTable+" ESERCIZI where ";
              +"ESCODAZI = "+cp_ToStrODBC(i_CODAZI);
              +" and ESCODESE = "+cp_ToStrODBC(this.w_CEESPRIU);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ESINIESE;
          from (i_cTable) where;
              ESCODAZI = i_CODAZI;
              and ESCODESE = this.w_CEESPRIU;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_INIESE = NVL(cp_ToDate(_read_.ESINIESE),cp_NullValue(_read_.ESINIESE))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_CECODESE = _Curs_CES_PITI.CECODESE
      * --- Read from ESERCIZI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ESERCIZI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ESINIESE"+;
          " from "+i_cTable+" ESERCIZI where ";
              +"ESCODAZI = "+cp_ToStrODBC(i_CODAZI);
              +" and ESCODESE = "+cp_ToStrODBC(this.w_CECODESE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ESINIESE;
          from (i_cTable) where;
              ESCODAZI = i_CODAZI;
              and ESCODESE = this.w_CECODESE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_INIESE2 = NVL(cp_ToDate(_read_.ESINIESE),cp_NullValue(_read_.ESINIESE))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_CECOEFI1 = NVL(_Curs_CES_PITI.CECOEFI1,0)
      this.w_CECOEFI2 = NVL(_Curs_CES_PITI.CECOEFI2,0)
      this.w_CEQUOFIS = NVL(_Curs_CES_PITI.CEQUOFIS,0)
      this.w_CEQUOFI1 = NVL(_Curs_CES_PITI.CEQUOFI1,0)
      * --- Insert into AMM_CESP
      i_nConn=i_TableProp[this.AMM_CESP_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AMM_CESP_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSCVAB47",this.AMM_CESP_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
        select _Curs_CES_PITI
        continue
      enddo
      use
    endif
    * --- commit
    cp_EndTrs(.t.)
    if bTrsErr
      * --- Raise
      i_Error=i_TrsMsg
      return
    endif
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = ah_Msgformat("Conversione eseguita correttamente")
    * --- Lancia il report di controllo
    vx_exec("..\PCON\EXE\QUERY\GSCVAB47B.VQR, ..\PCON\EXE\QUERY\GSCVAB47B.FRX",this)
    if this.w_NHF>=0 
      this.w_TMPC = this.oParentObject.w_PMSG
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='CES_PITI'
    this.cWorkTables[2]='AMM_CESP'
    this.cWorkTables[3]='ESERCIZI'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_CES_PITI')
      use in _Curs_CES_PITI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
