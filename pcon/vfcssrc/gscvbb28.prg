* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvbb28                                                        *
*              Aggiorna chiave tabella creddive                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_6]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-03-23                                                      *
* Last revis.: 2006-03-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvbb28",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvbb28 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_MESS = space(100)
  * --- WorkFile variables
  CREDDIVA_idx=0
  RIPATMP1_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- FIle di LOG
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Try
    local bErr_03894E68
    bErr_03894E68=bTrsErr
    this.Try_03894E68()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = this.oParentObject.w_PMSG
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_03894E68
    * --- End
  endproc
  proc Try_03894E68()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    i_nConn=i_TableProp[this.CREDDIVA_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.CREDDIVA_idx,2])
    if i_nConn<>0
      do case
        case upper(CP_DBTYPE)="SQLSERVER"
          * --- Caso SQLServer
          w_iRows = SQLExec(i_nConn,"ALTER TABLE "+i_cTable+; 
 " ALTER COLUMN CPROWNUM INT NOT NULL")
          w_iRows = SQLExec(i_nConn,"ALTER TABLE "+i_cTable+; 
 " ALTER COLUMN CINUMPER INT")
          w_iRows = SQLExec(i_nConn,"ALTER TABLE xxxCREDDIVA"+; 
 " ALTER COLUMN CPROWNUM INT NOT NULL")
          w_iRows = SQLExec(i_nConn,"ALTER TABLE xxxCREDDIVA"+; 
 " ALTER COLUMN CINUMPER INT")
        case upper(CP_DBTYPE)="ORACLE"
          * --- Caso ORACLE
          w_iRows = SQLExec(i_nConn,"ALTER TABLE "+i_cTable+; 
 " MODIFY(CPROWNUM NOT NULL)")
          w_iRows = SQLExec(i_nConn,"ALTER TABLE xxxCREDDIVA"+; 
 " MODIFY(CPROWNUM NOT NULL)")
          w_iRows = SQLExec(i_nConn,"ALTER TABLE "+i_cTable+; 
 " MODIFY(CINUMPER)")
          w_iRows = SQLExec(i_nConn,"ALTER TABLE xxxCREDDIVA"+; 
 " MODIFY(CINUMPER)")
          w_DropIndex=SQLExec(i_nConn,"DROP INDEX pk_" + i_cTable)
          w_DropIndex=SQLExec(i_nConn,"DROP INDEX pk_xxxCREDDIVA")
        case upper(CP_DBTYPE)="DB2"
          * --- Caso DB2
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
      endcase
      if bTrsErr
        * --- Raise
        i_Error=MESSAGE()
        return
      else
        if this.w_NHF>=0
          if upper(CP_DBTYPE)="DB2"
            this.w_TMPC = ah_Msgformat("Elaborazione terminata con successo")
          else
            this.w_TMPC = ah_Msgformat("Adeguamento chiave primaria tabella CREDDIVA eseguito correttamente%0ATTENZIONE: occorre andare in MANUTENZIONE DATABASE, selezionare la tabella%0CREDDIVA e premere <OK>")
          endif
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    if upper(CP_DBTYPE)="DB2"
      this.oParentObject.w_PMSG = ah_Msgformat("Elaborazione terminata con successo")
    else
      this.w_TMPC = ah_Msgformat("Adeguamento chiave primaria tabella CREDDIVA eseguito correttamente%0ATTENZIONE: occorre andare in MANUTENZIONE DATABASE, selezionare la tabella%0CREDDIVA e premere <OK>")
      this.oParentObject.w_PMSG = this.w_TMPC
    endif
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella DB2
    pName="CREDDIVA"
    pDatabaseType=i_ServerConn[1,6]
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.CREDDIVA_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.CREDDIVA_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
          )
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    * --- Elimino la tabella
    if Not GSCV_BDT( this, "CREDDIVA" , i_nConn )
      * --- Raise
      i_Error="Error"
      return
    endif
    * --- Ricostruisco il database per la tabella
    GSCV_BRT(this, "CREDDIVA" , i_nConn , .T.)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into CREDDIVA
    i_nConn=i_TableProp[this.CREDDIVA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CREDDIVA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.CREDDIVA_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    ah_Msg("Eliminata tabella temporanea tmp",.T.)
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questa Procedura aggiorna il cprownum della GESTIONE CREDITO IVA
    * --- Try
    local bErr_04A93AE0
    bErr_04A93AE0=bTrsErr
    this.Try_04A93AE0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE %1 -impossibile aggiornare la gestione credito IVA", this.oParentObject.w_PMSG )
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_04A93AE0
    * --- End
  endproc
  proc Try_04A93AE0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    ah_Msg("Aggiornamento CPROWNUM",.T.)
    * --- Write into CREDDIVA
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.CREDDIVA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CREDDIVA_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="CI__ANNO,CINUMPER"
      do vq_exec with 'CREDITO_IVA',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.CREDDIVA_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="CREDDIVA.CI__ANNO = _t2.CI__ANNO";
              +" and "+"CREDDIVA.CINUMPER = _t2.CINUMPER";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CPROWNUM = _t2.CPROWNUM";
          +i_ccchkf;
          +" from "+i_cTable+" CREDDIVA, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="CREDDIVA.CI__ANNO = _t2.CI__ANNO";
              +" and "+"CREDDIVA.CINUMPER = _t2.CINUMPER";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CREDDIVA, "+i_cQueryTable+" _t2 set ";
          +"CREDDIVA.CPROWNUM = _t2.CPROWNUM";
          +Iif(Empty(i_ccchkf),"",",CREDDIVA.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="CREDDIVA.CI__ANNO = t2.CI__ANNO";
              +" and "+"CREDDIVA.CINUMPER = t2.CINUMPER";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CREDDIVA set (";
          +"CPROWNUM";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.CPROWNUM";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="CREDDIVA.CI__ANNO = _t2.CI__ANNO";
              +" and "+"CREDDIVA.CINUMPER = _t2.CINUMPER";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CREDDIVA set ";
          +"CPROWNUM = _t2.CPROWNUM";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".CI__ANNO = "+i_cQueryTable+".CI__ANNO";
              +" and "+i_cTable+".CINUMPER = "+i_cQueryTable+".CINUMPER";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CPROWNUM = (select CPROWNUM from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    if bTrsErr
      * --- Raise
      i_Error=i_TrsMsg
      return
    endif
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = ah_Msgformat("Conversione eseguita correttamente")
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Conversione eseguita correttamente")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='CREDDIVA'
    this.cWorkTables[2]='*RIPATMP1'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
