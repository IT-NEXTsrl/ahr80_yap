* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscv_b84                                                        *
*              Aggiornamento combo contratti                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_14]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-12-02                                                      *
* Last revis.: 2002-12-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscv_b84",oParentObject,m.w_NHF)
return(i_retval)

define class tgscv_b84 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_FLUCOA = space(1)
  w_CONUMERO = space(15)
  * --- WorkFile variables
  CON_TRAM_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questa procedura di conversione modifica il valore della combo COFLUCOA
    * --- Try
    local bErr_037BFB90
    bErr_037BFB90=bTrsErr
    this.Try_037BFB90()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE - impossibile aggiornare COFLUCOA su tabella CON_TRAM")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_037BFB90
    * --- End
  endproc
  proc Try_037BFB90()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Aggiornamento  COMBO Contratti
    * --- Select from CON_TRAM
    i_nConn=i_TableProp[this.CON_TRAM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CON_TRAM_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" CON_TRAM ";
           ,"_Curs_CON_TRAM")
    else
      select * from (i_cTable);
        into cursor _Curs_CON_TRAM
    endif
    if used('_Curs_CON_TRAM')
      select _Curs_CON_TRAM
      locate for 1=1
      do while not(eof())
      this.w_FLUCOA = NVL(_Curs_CON_TRAM.COFLUCOA,"N")
      this.w_CONUMERO = NVL(_Curs_CON_TRAM.CONUMERO, SPACE(15))
      if EMPTY(this.w_FLUCOA)
        this.w_FLUCOA = "N"
      endif
      if this.w_FLUCOA="N"
        * --- Write into CON_TRAM
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.CON_TRAM_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CON_TRAM_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CON_TRAM_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"COFLUCOA ="+cp_NullLink(cp_ToStrODBC(this.w_FLUCOA),'CON_TRAM','COFLUCOA');
              +i_ccchkf ;
          +" where ";
              +"CONUMERO = "+cp_ToStrODBC(this.w_CONUMERO);
                 )
        else
          update (i_cTable) set;
              COFLUCOA = this.w_FLUCOA;
              &i_ccchkf. ;
           where;
              CONUMERO = this.w_CONUMERO;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
        select _Curs_CON_TRAM
        continue
      enddo
      use
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Aggiornamento campo COFLUCOA su tabella CON_TRAM eseguito con successo")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = this.oParentObject.w_PMSG + this.w_TMPC 
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='CON_TRAM'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_CON_TRAM')
      use in _Curs_CON_TRAM
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
