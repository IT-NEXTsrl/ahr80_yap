* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvbb18                                                        *
*              Tipologie/classi allegati                                       *
*                                                                              *
*      Author: Zucchetti SpA                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_221]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-03-17                                                      *
* Last revis.: 2008-05-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvbb18",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvbb18 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TMPC = space(10)
  w_TEMP = space(10)
  w_APPO = space(1)
  w_AZIENDA = space(5)
  w_LIPATIMG = space(254)
  w_TEST = .f.
  w_ImgPat = space(10)
  w_curdir = space(50)
  w_Loop = 0
  w_LoopFile = 0
  w_oMess = .NULL.
  w_oPart = .NULL.
  w_Messaggio = space(30)
  w_NOMEFILE = space(254)
  w_ESTENS = space(10)
  w_oMess1 = .NULL.
  w_oPart1 = .NULL.
  w_TIPPRIN = space(5)
  w_CLAPRIN = space(5)
  w_CONFOPE = space(1)
  w_TIPDEFEX = space(5)
  w_CLADEFEX = space(5)
  * --- WorkFile variables
  LIB_IMMA_idx=0
  GESTFILE_idx=0
  EXT_ENS_idx=0
  ART_ICOL_idx=0
  PROMCLAS_idx=0
  PROMINDI_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Procedura di conversione per impostare negli allegati la Tipologia e la Classe
    *     adatti alla pubblicazione degli allegati su ICPZ
    * --- FIle di LOG
    this.w_AZIENDA = ALLTRIM(I_CODAZI)
    * --- Creo cursore che conterr� l'elenco delle estensioni dei files presenti nelle 
    *     cartelle di archiviazione degli allegati per dare un resoconto delle estensioni
    *     che bisogna ancora caricare nella tabella relativa
    CREATE CURSOR ESTENSIONI (CODICE C(20))
    CREATE CURSOR MESSERR (CODICE C(20))
    this.oParentObject.w_PESEOK = .T.
    * --- Select from PROMCLAS
    i_nConn=i_TableProp[this.PROMCLAS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PROMCLAS_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select CDTIPALL,CDCLAALL  from "+i_cTable+" PROMCLAS ";
           ,"_Curs_PROMCLAS")
    else
      select CDTIPALL,CDCLAALL from (i_cTable);
        into cursor _Curs_PROMCLAS
    endif
    if used('_Curs_PROMCLAS')
      select _Curs_PROMCLAS
      locate for 1=1
      do while not(eof())
      if Empty(Nvl(_Curs_PROMCLAS.CDTIPALL," ")) Or Empty(Nvl(_Curs_PROMCLAS.CDCLAALL," ")) 
        * --- Oggetto per messaggi incrementali
        this.w_Messaggio = iif(g_DOCM="S","classi documentali","classi librerie allegati")
        this.w_oMess=createobject("Ah_Message")
        this.w_oPart = this.w_oMess.AddMsgPartNL("Per il caricamento degli allegati bisogna definire obbligatoriamente nelle %1 la tipologia e classe predefinite")
        this.w_oPart.AddParam(alltrim(this.w_Messaggio))     
        this.w_oMess.AddMsgPart("Possono essere definite anche tipologia e classe specifiche per ogni estensione")     
        this.w_TMPC = this.w_oMess.ComposeMessage()
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        this.oParentObject.w_PESEOK = .F.
        EXIT
      endif
        select _Curs_PROMCLAS
        continue
      enddo
      use
    endif
    * --- Select from PROMINDI
    i_nConn=i_TableProp[this.PROMINDI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select IDNOMFIL  from "+i_cTable+" PROMINDI ";
           ,"_Curs_PROMINDI")
    else
      select IDNOMFIL from (i_cTable);
        into cursor _Curs_PROMINDI
    endif
    if used('_Curs_PROMINDI')
      select _Curs_PROMINDI
      locate for 1=1
      do while not(eof())
      this.w_NOMEFILE = _Curs_PROMINDI.IDNOMFIL
      ah_Msg("Controllo file: %1",.t.,.f.,.f.,Alltrim(this.w_NOMEFILE))
      this.w_ESTENS = Upper(justext(this.w_NOMEFILE))
      * --- Inserisco il codice estensione e S per indicare che l'estensione � presente.
      *     Successivamente metter� N per tutte le estensioni non ancora caricate nella tabella relativa
      Select ESTENSIONI 
 Locate For CODICE = Alltrim(this.w_ESTENS)
      if Not Found()
        * --- Controllo se ho gi� caricato l'estensione
        if upper(CP_DBTYPE)="ORACLE" Or upper(CP_DBTYPE)="DB2"
          VQ_EXEC("..\PCON\EXE\QUERY\GSCVEBDP.VQR",this,"CONTROLLO")
          if RECCOUNT("CONTROLLO")=0
            INSERT INTO MESSERR (CODICE) VALUES (this.w_ESTENS)
          endif
        else
          * --- Controllo se ho gi� caricato l'estensione
          * --- Read from EXT_ENS
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.EXT_ENS_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.EXT_ENS_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "*"+;
              " from "+i_cTable+" EXT_ENS where ";
                  +"EXCODICE = "+cp_ToStrODBC(this.w_ESTENS);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              *;
              from (i_cTable) where;
                  EXCODICE = this.w_ESTENS;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if i_Rows=0
            INSERT INTO MESSERR (CODICE) VALUES (this.w_ESTENS)
          endif
        endif
        INSERT INTO ESTENSIONI (CODICE) VALUES (this.w_ESTENS)
      endif
        select _Curs_PROMINDI
        continue
      enddo
      use
    endif
    if Used("MESSERR") And Reccount("MESSERR")>0 And this.oParentObject.w_PESEOK
      Select * from MESSERR Into Cursor __Tmp__ Order By Codice
      if Used("MESSERR")
         
 Select("MESSERR") 
 Use
      endif
      CP_CHPRN("..\PCON\EXE\QUERY\GSCVBB18.FRX", " ", this)
      * --- Oggetto per messaggi incrementali
      this.w_Messaggio = iif(g_DOCM="S","classi documentali","classi librerie allegati")
      this.w_oMess1=createobject("Ah_Message")
      this.w_oMess1.AddMsgPartNL("Sono ancora presenti file con estensione non caricata nella tabella relativa")     
      this.w_oPart1 = this.w_oMess1.AddMsgPartNL("Gli allegati con queste estensioni rimarranno archiviati con la tipologia e la classe generiche definite nelle %1.%0Si desidera continuare?")
      this.w_oPart1.AddParam(alltrim(this.w_Messaggio))     
      if this.w_oMess1.ah_YesNo()
        this.oParentObject.w_PESEOK = .T.
      else
        this.oParentObject.w_PESEOK = .F.
      endif
    endif
    if this.oParentObject.w_PESEOK
      this.w_TIPPRIN = Space(5)
      this.w_CLAPRIN = Space(5)
      this.w_CONFOPE = "N"
      do GSCV_K18 with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_Messaggio = iif(g_DOCM="S","indici documenti","indici allegati")
      if this.w_CONFOPE = "S"
        * --- Try
        local bErr_038171B8
        bErr_038171B8=bTrsErr
        this.Try_038171B8()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          * --- Gestisce log errori
          this.oParentObject.w_PMSG = Message()
          this.oParentObject.w_PESEOK = .F.
          if this.w_NHF>=0
            this.w_TMPC = ah_Msgformat("ERRORE GENERICO - impossibile aggiornare tabella %1",alltrim(this.w_Messaggio))
            this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
          endif
        endif
        bTrsErr=bTrsErr or bErr_038171B8
        * --- End
      else
        * --- Gestisce log errori
        this.oParentObject.w_PMSG = ""
        this.oParentObject.w_PESEOK = .F.
        if this.w_NHF>=0
          this.w_TMPC = ah_Msgformat("Aggiornamento tabella %1 abbandonata",alltrim(this.w_Messaggio))
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
    endif
  endproc
  proc Try_038171B8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Modifica allegati nella tabella indici documenti avvenuto correttamente")
      this.oParentObject.w_pMSG = this.w_TMPC
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    if used("__TMP__")
      select __TMP__
      use
    endif
    if used("CONTROLLO")
      select CONTROLLO
      use
    endif
    if Used("ESTENSIONI")
       
 Select("ESTENSIONI") 
 Use
    endif
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = ah_Msgformat("Conversione eseguita correttamente")
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Per modificare la tipologia e classe eseguo una scansione delle estensioni presenti
    *     In questo modo riesco 
    * --- Select from EXT_ENS
    i_nConn=i_TableProp[this.EXT_ENS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.EXT_ENS_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" EXT_ENS ";
           ,"_Curs_EXT_ENS")
    else
      select * from (i_cTable);
        into cursor _Curs_EXT_ENS
    endif
    if used('_Curs_EXT_ENS')
      select _Curs_EXT_ENS
      locate for 1=1
      do while not(eof())
      this.w_ESTENS = Alltrim(_Curs_EXT_ENS.EXCODICE)
      ah_Msg(this.w_ESTENS,.T.)
      this.w_TIPDEFEX = _Curs_EXT_ENS.EXTIPALL
      this.w_CLADEFEX = _Curs_EXT_ENS.EXCLAALL
      * --- Write into PROMINDI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.PROMINDI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="IDSERIAL"
        do vq_exec with 'GSCVBB18',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.PROMINDI_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="PROMINDI.IDSERIAL = _t2.IDSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"IDTIPALL = _t2.GFTIPALL";
            +",IDCLAALL = _t2.GFCLAALL";
            +",IDOGGETT = _t2.GFDESCRI";
            +i_ccchkf;
            +" from "+i_cTable+" PROMINDI, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="PROMINDI.IDSERIAL = _t2.IDSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" PROMINDI, "+i_cQueryTable+" _t2 set ";
            +"PROMINDI.IDTIPALL = _t2.GFTIPALL";
            +",PROMINDI.IDCLAALL = _t2.GFCLAALL";
            +",PROMINDI.IDOGGETT = _t2.GFDESCRI";
            +Iif(Empty(i_ccchkf),"",",PROMINDI.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="PROMINDI.IDSERIAL = t2.IDSERIAL";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" PROMINDI set (";
            +"IDTIPALL,";
            +"IDCLAALL,";
            +"IDOGGETT";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.GFTIPALL,";
            +"t2.GFCLAALL,";
            +"t2.GFDESCRI";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="PROMINDI.IDSERIAL = _t2.IDSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" PROMINDI set ";
            +"IDTIPALL = _t2.GFTIPALL";
            +",IDCLAALL = _t2.GFCLAALL";
            +",IDOGGETT = _t2.GFDESCRI";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".IDSERIAL = "+i_cQueryTable+".IDSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"IDTIPALL = (select GFTIPALL from "+i_cQueryTable+" where "+i_cWhere+")";
            +",IDCLAALL = (select GFCLAALL from "+i_cQueryTable+" where "+i_cWhere+")";
            +",IDOGGETT = (select GFDESCRI from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
        select _Curs_EXT_ENS
        continue
      enddo
      use
    endif
  endproc


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,6)]
    this.cWorkTables[1]='LIB_IMMA'
    this.cWorkTables[2]='GESTFILE'
    this.cWorkTables[3]='EXT_ENS'
    this.cWorkTables[4]='ART_ICOL'
    this.cWorkTables[5]='PROMCLAS'
    this.cWorkTables[6]='PROMINDI'
    return(this.OpenAllTables(6))

  proc CloseCursors()
    if used('_Curs_PROMCLAS')
      use in _Curs_PROMCLAS
    endif
    if used('_Curs_PROMINDI')
      use in _Curs_PROMINDI
    endif
    if used('_Curs_EXT_ENS')
      use in _Curs_EXT_ENS
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
