* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvbb59                                                        *
*              Aggiorna campi banche su partite da indiretta effetti           *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-03-17                                                      *
* Last revis.: 2007-01-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvbb59",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvbb59 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TMPC = space(10)
  w_TEMP = space(10)
  w_CODREL = space(10)
  * --- WorkFile variables
  PAR_TITE_idx=0
  CONVERSI_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Valorizza i campi PTBANAPP,PTBANNOS nella registrazione di primanota
    *      nel caso di variazione nell'indiretta effetti 
    * --- FIle di LOG
    * --- Read from CONVERSI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CONVERSI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONVERSI_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "COCODREL"+;
        " from "+i_cTable+" CONVERSI where ";
            +"CONOMPRO = "+cp_ToStrODBC("GSCVBB59");
            +" and COESEGUI = "+cp_ToStrODBC("S");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        COCODREL;
        from (i_cTable) where;
            CONOMPRO = "GSCVBB59";
            and COESEGUI = "S";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CODREL = NVL(cp_ToDate(_read_.COCODREL),cp_NullValue(_read_.COCODREL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if EMPTY(this.w_CODREL)
      * --- Try
      local bErr_04C02398
      bErr_04C02398=bTrsErr
      this.Try_04C02398()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        * --- Gestisce log errori
        this.oParentObject.w_PMSG = Message()
        this.oParentObject.w_PESEOK = .F.
        if this.w_NHF>=0
          this.w_TMPC = this.oParentObject.w_PMSG
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
      bTrsErr=bTrsErr or bErr_04C02398
      * --- End
    else
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("Conversione gi� eseguita nella Rel. 4.0")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
      this.oParentObject.w_PESEOK = .T.
    endif
  endproc
  proc Try_04C02398()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Write into PAR_TITE
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PAR_TITE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="PTSERIAL,PTROWORD,CPROWNUM"
      do vq_exec with 'GSCVBB59',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_TITE_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="PAR_TITE.PTSERIAL = _t2.PTSERIAL";
              +" and "+"PAR_TITE.PTROWORD = _t2.PTROWORD";
              +" and "+"PAR_TITE.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PTBANNOS = _t2.PTBANNOS";
          +",PTBANAPP = _t2.PTBANAPP";
      +",PTNUMCOR ="+cp_NullLink(cp_ToStrODBC(SPACE(25)),'PAR_TITE','PTNUMCOR');
          +i_ccchkf;
          +" from "+i_cTable+" PAR_TITE, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="PAR_TITE.PTSERIAL = _t2.PTSERIAL";
              +" and "+"PAR_TITE.PTROWORD = _t2.PTROWORD";
              +" and "+"PAR_TITE.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" PAR_TITE, "+i_cQueryTable+" _t2 set ";
          +"PAR_TITE.PTBANNOS = _t2.PTBANNOS";
          +",PAR_TITE.PTBANAPP = _t2.PTBANAPP";
      +",PAR_TITE.PTNUMCOR ="+cp_NullLink(cp_ToStrODBC(SPACE(25)),'PAR_TITE','PTNUMCOR');
          +Iif(Empty(i_ccchkf),"",",PAR_TITE.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="PAR_TITE.PTSERIAL = t2.PTSERIAL";
              +" and "+"PAR_TITE.PTROWORD = t2.PTROWORD";
              +" and "+"PAR_TITE.CPROWNUM = t2.CPROWNUM";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" PAR_TITE set (";
          +"PTBANNOS,";
          +"PTBANAPP,";
          +"PTNUMCOR";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.PTBANNOS,";
          +"t2.PTBANAPP,";
          +cp_NullLink(cp_ToStrODBC(SPACE(25)),'PAR_TITE','PTNUMCOR')+"";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="PAR_TITE.PTSERIAL = _t2.PTSERIAL";
              +" and "+"PAR_TITE.PTROWORD = _t2.PTROWORD";
              +" and "+"PAR_TITE.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" PAR_TITE set ";
          +"PTBANNOS = _t2.PTBANNOS";
          +",PTBANAPP = _t2.PTBANAPP";
      +",PTNUMCOR ="+cp_NullLink(cp_ToStrODBC(SPACE(25)),'PAR_TITE','PTNUMCOR');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".PTSERIAL = "+i_cQueryTable+".PTSERIAL";
              +" and "+i_cTable+".PTROWORD = "+i_cQueryTable+".PTROWORD";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PTBANNOS = (select PTBANNOS from "+i_cQueryTable+" where "+i_cWhere+")";
          +",PTBANAPP = (select PTBANAPP from "+i_cQueryTable+" where "+i_cWhere+")";
      +",PTNUMCOR ="+cp_NullLink(cp_ToStrODBC(SPACE(25)),'PAR_TITE','PTNUMCOR');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Aggiorno righe indiretta effetti senza data scadenza o pagamento
    * --- Write into PAR_TITE
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PAR_TITE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="PTSERIAL,PTROWORD,CPROWNUM"
      do vq_exec with 'GSCVBB59_1',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_TITE_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="PAR_TITE.PTSERIAL = _t2.PTSERIAL";
              +" and "+"PAR_TITE.PTROWORD = _t2.PTROWORD";
              +" and "+"PAR_TITE.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PTDATSCA = _t2.PTDATSCA";
          +",PTMODPAG = _t2.PTMODPAG";
          +i_ccchkf;
          +" from "+i_cTable+" PAR_TITE, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="PAR_TITE.PTSERIAL = _t2.PTSERIAL";
              +" and "+"PAR_TITE.PTROWORD = _t2.PTROWORD";
              +" and "+"PAR_TITE.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" PAR_TITE, "+i_cQueryTable+" _t2 set ";
          +"PAR_TITE.PTDATSCA = _t2.PTDATSCA";
          +",PAR_TITE.PTMODPAG = _t2.PTMODPAG";
          +Iif(Empty(i_ccchkf),"",",PAR_TITE.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="PAR_TITE.PTSERIAL = t2.PTSERIAL";
              +" and "+"PAR_TITE.PTROWORD = t2.PTROWORD";
              +" and "+"PAR_TITE.CPROWNUM = t2.CPROWNUM";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" PAR_TITE set (";
          +"PTDATSCA,";
          +"PTMODPAG";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.PTDATSCA,";
          +"t2.PTMODPAG";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="PAR_TITE.PTSERIAL = _t2.PTSERIAL";
              +" and "+"PAR_TITE.PTROWORD = _t2.PTROWORD";
              +" and "+"PAR_TITE.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" PAR_TITE set ";
          +"PTDATSCA = _t2.PTDATSCA";
          +",PTMODPAG = _t2.PTMODPAG";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".PTSERIAL = "+i_cQueryTable+".PTSERIAL";
              +" and "+i_cTable+".PTROWORD = "+i_cQueryTable+".PTROWORD";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PTDATSCA = (select PTDATSCA from "+i_cQueryTable+" where "+i_cWhere+")";
          +",PTMODPAG = (select PTMODPAG from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    if this.w_NHF>=0
      this.w_TMPC = "Aggiornamento campi  PTBANNOS,PTBANAPP tabella PAR_TITE avvenuto correttamente. "
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = "Conversione eseguita correttamente"
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='PAR_TITE'
    this.cWorkTables[2]='CONVERSI'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
