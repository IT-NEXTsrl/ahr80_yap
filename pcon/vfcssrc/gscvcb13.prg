* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcb13                                                        *
*              ASSEGNAMENTO GRUPPO PREDEFINITO TABELLA RAP_PRES                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-01-14                                                      *
* Last revis.: 2008-07-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcb13",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcb13 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TMPC = space(10)
  w_TEMP = space(10)
  w_DACODRES = space(5)
  w_DAGRURES = space(5)
  * --- WorkFile variables
  RAP_PRES_idx=0
  DIPENDEN_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Assegna il gruppo predefinito alle risorse della tabella RAP_PRES.
    *     Procedura di convesrione associata 5.0-M1538
    * --- Try
    local bErr_035D0900
    bErr_035D0900=bTrsErr
    this.Try_035D0900()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = this.oParentObject.w_PMSG
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_035D0900
    * --- End
  endproc
  proc Try_035D0900()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Select from RAP_PRES
    i_nConn=i_TableProp[this.RAP_PRES_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.RAP_PRES_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select DACODRES AS DACODRES, DAGRURES AS DAGRURES  from "+i_cTable+" RAP_PRES ";
          +" group by DACODRES, DAGRURES";
           ,"_Curs_RAP_PRES")
    else
      select DACODRES AS DACODRES, DAGRURES AS DAGRURES from (i_cTable);
       group by DACODRES, DAGRURES;
        into cursor _Curs_RAP_PRES
    endif
    if used('_Curs_RAP_PRES')
      select _Curs_RAP_PRES
      locate for 1=1
      do while not(eof())
      this.w_DACODRES = _Curs_RAP_PRES.DACODRES
      this.w_DAGRURES = _Curs_RAP_PRES.DAGRURES
      if EMPTY(NVL(this.w_DAGRURES, " "))
        * --- Read from DIPENDEN
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DIPENDEN_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DIPENDEN_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "DPGRUPRE"+;
            " from "+i_cTable+" DIPENDEN where ";
                +"DPCODICE = "+cp_ToStrODBC(this.w_DACODRES);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            DPGRUPRE;
            from (i_cTable) where;
                DPCODICE = this.w_DACODRES;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DAGRURES = NVL(cp_ToDate(_read_.DPGRUPRE),cp_NullValue(_read_.DPGRUPRE))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if !EMPTY(NVL(this.w_DAGRURES, " "))
          * --- Write into RAP_PRES
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.RAP_PRES_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.RAP_PRES_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.RAP_PRES_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"DAGRURES ="+cp_NullLink(cp_ToStrODBC(this.w_DAGRURES),'RAP_PRES','DAGRURES');
                +i_ccchkf ;
            +" where ";
                +"DACODRES = "+cp_ToStrODBC(this.w_DACODRES);
                   )
          else
            update (i_cTable) set;
                DAGRURES = this.w_DAGRURES;
                &i_ccchkf. ;
             where;
                DACODRES = this.w_DACODRES;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      endif
        select _Curs_RAP_PRES
        continue
      enddo
      use
    endif
    * --- Esecuzione ok
    * --- commit
    cp_EndTrs(.t.)
    this.oParentObject.w_PESEOK = .T.
    this.w_TMPC = ah_Msgformat("Assegnamento gruppo predefinito delle risorse avvenuto correttamente.")
    this.oParentObject.w_PMSG = ah_Msgformat("Conversione eseguita correttamente")
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='RAP_PRES'
    this.cWorkTables[2]='DIPENDEN'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_RAP_PRES')
      use in _Curs_RAP_PRES
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
