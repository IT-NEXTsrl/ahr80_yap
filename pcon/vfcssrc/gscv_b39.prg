* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscv_b39                                                        *
*              Aggiorna flag saldo iniziale/finale su P.N. dei conti transitori*
*                                                                              *
*      Author: ZUCCHETTI TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_33]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-05-06                                                      *
* Last revis.: 2002-05-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscv_b39",oParentObject,m.w_NHF)
return(i_retval)

define class tgscv_b39 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_MESS = space(100)
  w_TIPCON = space(1)
  w_CODCON = space(15)
  w_COMPET = space(4)
  w_FLSALI = space(1)
  w_FLSALF = space(1)
  w_IMPDAR = 0
  w_IMPAVE = 0
  w_SERIAL = space(10)
  w_ROWNUM = 0
  * --- WorkFile variables
  PNT_DETT_idx=0
  SALDICON_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questa procedura Aggiusta il Flag Saldo Iniziale/Finale dei Conti Transitori, Aggiorna anche i Saldi
    * --- FIle di LOG
    * --- Try
    local bErr_035EC120
    bErr_035EC120=bTrsErr
    this.Try_035EC120()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE GENERICO - impossibile aggiornare primanota e/o saldi contabili")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_035EC120
    * --- End
  endproc
  proc Try_035EC120()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Leggo le Registrazioni Contabili Incongruenti
    *     Registrazioni con Causale che Aggiorna Saldo Iniziale/Finale e non Provvisorie
    *     Righe Conti Transitori che hanno il Flag PNFLSALI/PNFLSALF = '+'
    vq_exec("..\pcon\exe\query\gscv0b39.vqr",this,"CONTI")
    if USED("Conti")
      if Reccount ("Conti")>0
        SELECT CONTI
        GO TOP
        SCAN FOR NVL(PNTIPCON," ")="G" AND NOT EMPTY(NVL(PNCODCON,"")) AND NOT EMPTY(NVL(PNCOMPET,""))
        this.w_TIPCON = PNTIPCON
        this.w_CODCON = PNCODCON
        this.w_COMPET = PNCOMPET
        this.w_FLSALI = NVL(PNFLSALI, " ")
        this.w_FLSALF = NVL(PNFLSALF, " ")
        this.w_IMPDAR = NVL(PNIMPDAR, 0)
        this.w_IMPAVE = NVL(PNIMPAVE, 0)
        this.w_SERIAL = NVL(PNSERIAL, SPACE(10))
        this.w_ROWNUM = NVL(CPROWNUM, 0)
        if NOT EMPTY(this.w_SERIAL) AND this.w_ROWNUM<>0
          * --- Aggiorna Flag Primanota
          * --- Write into PNT_DETT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.PNT_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.PNT_DETT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"PNFLSALI ="+cp_NullLink(cp_ToStrODBC(" "),'PNT_DETT','PNFLSALI');
            +",PNFLSALF ="+cp_NullLink(cp_ToStrODBC(" "),'PNT_DETT','PNFLSALF');
                +i_ccchkf ;
            +" where ";
                +"PNSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWNUM);
                   )
          else
            update (i_cTable) set;
                PNFLSALI = " ";
                ,PNFLSALF = " ";
                &i_ccchkf. ;
             where;
                PNSERIAL = this.w_SERIAL;
                and CPROWNUM = this.w_ROWNUM;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        * --- Azzera Saldo Iniziale/Finale
        * --- Write into SALDICON
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.SALDICON_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICON_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SLDARINI ="+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARINI');
          +",SLAVEINI ="+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEINI');
          +",SLAVEFIN ="+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEFIN');
          +",SLDARFIN ="+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARFIN');
              +i_ccchkf ;
          +" where ";
              +"SLTIPCON = "+cp_ToStrODBC(this.w_TIPCON);
              +" and SLCODICE = "+cp_ToStrODBC(this.w_CODCON);
              +" and SLCODESE = "+cp_ToStrODBC(this.w_COMPET);
                 )
        else
          update (i_cTable) set;
              SLDARINI = 0;
              ,SLAVEINI = 0;
              ,SLAVEFIN = 0;
              ,SLDARFIN = 0;
              &i_ccchkf. ;
           where;
              SLTIPCON = this.w_TIPCON;
              and SLCODICE = this.w_CODCON;
              and SLCODESE = this.w_COMPET;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        SELECT CONTI
        ENDSCAN
        * --- Esecuzione ok
        this.oParentObject.w_PESEOK = .T.
        this.oParentObject.w_PMSG = ah_Msgformat("Conversione eseguita correttamente")
      else
        * --- Gestisce log errori
        if this.w_NHF>=0
          this.w_TMPC = ah_Msgformat("ATTENZIONE: non esistono dati da convertire o non � stata aggiornata la sezione di bilancio sui mastri")
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
        * --- Esecuzione ok
        this.oParentObject.w_PESEOK = .T.
        this.oParentObject.w_PMSG = this.w_TMPC
      endif
      * --- Chiude cursore
      SELECT CONTI
      USE
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='PNT_DETT'
    this.cWorkTables[2]='SALDICON'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
