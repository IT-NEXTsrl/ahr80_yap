* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcb92                                                        *
*              Aggiorna ripartizione sconti globali                            *
*                                                                              *
*      Author: Zucchetti S.p.A.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-11-30                                                      *
* Last revis.: 2011-04-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcb92",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcb92 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_DATINI = ctod("  /  /  ")
  w_DATFIN = ctod("  /  /  ")
  w_MVSERIAL = space(10)
  w_OKELAB = .f.
  w_CODREL = space(10)
  w_CONTA = 0
  w_CODESE = space(4)
  * --- WorkFile variables
  CONVERSI_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna ripartizione sconti globali
    * --- Read from CONVERSI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CONVERSI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONVERSI_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "COCODREL"+;
        " from "+i_cTable+" CONVERSI where ";
            +"CONOMPRO = "+cp_ToStrODBC("GSCVCB92");
            +" and COESEGUI = "+cp_ToStrODBC("S");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        COCODREL;
        from (i_cTable) where;
            CONOMPRO = "GSCVCB92";
            and COESEGUI = "S";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CODREL = NVL(cp_ToDate(_read_.COCODREL),cp_NullValue(_read_.COCODREL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if EMPTY(this.w_CODREL)
      do gscv_k92 with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if this.w_OKELAB
        this.w_CONTA = 0
        * --- Try
        local bErr_0360CB90
        bErr_0360CB90=bTrsErr
        this.Try_0360CB90()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          * --- Gestisce log errori
          this.oParentObject.w_PMSG = Message()
          this.oParentObject.w_PESEOK = .F.
          if this.w_NHF>=0
            this.w_TMPC = ah_Msgformat("Errore generico - impossibile eseguire aggiornamento richiesto")
            this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
          endif
        endif
        bTrsErr=bTrsErr or bErr_0360CB90
        * --- End
      else
        this.oParentObject.w_PESEOK = .F.
        this.w_TMPC = ah_Msgformat("Errore generico - impossibile eseguire aggiornamento richiesto")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    else
      if this.w_NHF>=0
        this.w_TMPC = AH_MsgFormat("Aggiornamento gi� eseguito nella Rel 6.0")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
      this.oParentObject.w_PESEOK = .T.
    endif
  endproc
  proc Try_0360CB90()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Select from GSCVCB92
    do vq_exec with 'GSCVCB92',this,'_Curs_GSCVCB92','',.f.,.t.
    if used('_Curs_GSCVCB92')
      select _Curs_GSCVCB92
      locate for 1=1
      do while not(eof())
      this.w_MVSERIAL = _Curs_GSCVCB92.MVSERIAL
      GSAR_BRD(this,this.w_MVSERIAL)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_CONTA = this.w_CONTA + 1
        select _Curs_GSCVCB92
        continue
      enddo
      use
    endif
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = this.w_TMPC
    * --- commit
    cp_EndTrs(.t.)
    if this.w_CONTA=1
      this.w_TMPC = ah_Msgformat("Numero  %1 documenti aggiornato con successo",Alltrim(str(this.w_CONTA)))
    else
      this.w_TMPC = ah_Msgformat("Numero  %1 documenti aggiornati con successo",Alltrim(str(this.w_CONTA)))
    endif
    this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='CONVERSI'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_GSCVCB92')
      use in _Curs_GSCVCB92
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
