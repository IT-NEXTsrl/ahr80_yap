* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcc92                                                        *
*              Aggiornamento valori PUTIPDOC e PUDISMAG su PAR__MRP            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2017-10-31                                                      *
* Last revis.: 2017-11-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcc92",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcc92 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_NUMKEY = 0
  w_DISMAG = space(1)
  w_TIPDOC = space(0)
  w_CHECKSTR = space(1)
  w_CONTA1 = 0
  w_CONTA2 = 0
  w_CONTA3 = 0
  w_CONTA4 = 0
  w_AGGSALDI = space(9)
  w_CAUDOC = space(5)
  w_CAUMAG = space(5)
  w_CAUCOL = space(5)
  w_FLORDI = space(1)
  w_FLORDI1 = space(1)
  * --- WorkFile variables
  PAR__MRP_idx=0
  SEL__MRP_idx=0
  TIP_DOCU_idx=0
  CAM_AGAZ_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiornamento valori PUTIPDOC e PUDISMAG su PAR__MRP
    * --- FIle di LOG
    * --- Try
    local bErr_0513C630
    bErr_0513C630=bTrsErr
    this.Try_0513C630()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("Errore generico - impossibile eseguire aggiornamento richiesto")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_0513C630
    * --- End
  endproc
  proc Try_0513C630()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Select from PAR__MRP
    i_nConn=i_TableProp[this.PAR__MRP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR__MRP_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select PUNUMKEY,PUDISMAG,PUTIPDOC  from "+i_cTable+" PAR__MRP ";
           ,"_Curs_PAR__MRP")
    else
      select PUNUMKEY,PUDISMAG,PUTIPDOC from (i_cTable);
        into cursor _Curs_PAR__MRP
    endif
    if used('_Curs_PAR__MRP')
      select _Curs_PAR__MRP
      locate for 1=1
      do while not(eof())
      this.w_NUMKEY = _Curs_PAR__MRP.PUNUMKEY
      this.w_DISMAG = _Curs_PAR__MRP.PUDISMAG
      this.w_TIPDOC = _Curs_PAR__MRP.PUTIPDOC
      if ! NVL(this.w_DISMAG," ")$"N-S"
        * --- 1-Aggiorna PUDISMAG
        * --- Write into PAR__MRP
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.PAR__MRP_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR__MRP_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR__MRP_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PUDISMAG ="+cp_NullLink(cp_ToStrODBC("N"),'PAR__MRP','PUDISMAG');
              +i_ccchkf ;
          +" where ";
              +"PUNUMKEY = "+cp_ToStrODBC(this.w_NUMKEY);
                 )
        else
          update (i_cTable) set;
              PUDISMAG = "N";
              &i_ccchkf. ;
           where;
              PUNUMKEY = this.w_NUMKEY;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
      * --- 2-Aggiorna PUTIPDOC
      * --- Vecchia composizione campo:
      *     CAUSALE+DESCRIZIONE+CHECK ->5+35+1=41
      *     Nuova composizione campo:
      *     CAUSALE+DESCRIZIONE+TIPO+CHECK ->5+35+9+1=50
      this.w_CONTA1 = 1
      this.w_CONTA2 = 0
      this.w_CONTA3 = 41
      this.w_CONTA4 = 0
      this.w_CHECKSTR = SUBSTR(this.w_TIPDOC,this.w_CONTA3,this.w_CONTA1)
      this.w_TIPDOC = ""
      if this.w_CHECKSTR="1" or this.w_CHECKSTR="0"
        do while this.w_CHECKSTR="1" or this.w_CHECKSTR="0"
          this.w_CONTA4 = (43*this.w_CONTA2)+1
          this.w_CAUDOC = alltrim(substr(_Curs_PAR__MRP.PUTIPDOC,this.w_CONTA4,5))
          * --- Read from TIP_DOCU
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "TDCAUMAG"+;
              " from "+i_cTable+" TIP_DOCU where ";
                  +"TDTIPDOC = "+cp_ToStrODBC(this.w_CAUDOC);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              TDCAUMAG;
              from (i_cTable) where;
                  TDTIPDOC = this.w_CAUDOC;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CAUMAG = NVL(cp_ToDate(_read_.TDCAUMAG),cp_NullValue(_read_.TDCAUMAG))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Read from CAM_AGAZ
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CMFLORDI"+;
              " from "+i_cTable+" CAM_AGAZ where ";
                  +"CMCODICE = "+cp_ToStrODBC(this.w_CAUCOL);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CMFLORDI;
              from (i_cTable) where;
                  CMCODICE = this.w_CAUCOL;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_FLORDI1 = NVL(cp_ToDate(_read_.CMFLORDI),cp_NullValue(_read_.CMFLORDI))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Read from CAM_AGAZ
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CMCAUCOL,CMFLORDI"+;
              " from "+i_cTable+" CAM_AGAZ where ";
                  +"CMCODICE = "+cp_ToStrODBC(this.w_CAUMAG);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CMCAUCOL,CMFLORDI;
              from (i_cTable) where;
                  CMCODICE = this.w_CAUMAG;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CAUCOL = NVL(cp_ToDate(_read_.CMCAUCOL),cp_NullValue(_read_.CMCAUCOL))
            this.w_FLORDI = NVL(cp_ToDate(_read_.CMFLORDI),cp_NullValue(_read_.CMFLORDI))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_AGGSALDI = IIF(this.w_FLORDI="+" or this.w_FLORDI1="+",ah_Msgformat("Ordinato"),ah_Msgformat("Impegnato"))
          this.w_TIPDOC = alltrim(this.w_TIPDOC)+substr(_Curs_PAR__MRP.PUTIPDOC,this.w_CONTA4,40)+left(alltrim(this.w_AGGSALDI)+space(9),9)+substr(_Curs_PAR__MRP.PUTIPDOC,this.w_CONTA3,1)+chr(13)+chr(10)
          this.w_CONTA1 = this.w_CONTA1+1
          this.w_CONTA2 = this.w_CONTA2+1
          this.w_CONTA3 = (41*this.w_CONTA1)+(2*this.w_CONTA2)
          this.w_CHECKSTR = SUBSTR(_Curs_PAR__MRP.PUTIPDOC,this.w_CONTA3,1)
        enddo
      endif
      if not empty(this.w_TIPDOC)
        * --- Write into PAR__MRP
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.PAR__MRP_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR__MRP_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR__MRP_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PUTIPDOC ="+cp_NullLink(cp_ToStrODBC(this.w_TIPDOC),'PAR__MRP','PUTIPDOC');
              +i_ccchkf ;
          +" where ";
              +"PUNUMKEY = "+cp_ToStrODBC(this.w_NUMKEY);
                 )
        else
          update (i_cTable) set;
              PUTIPDOC = this.w_TIPDOC;
              &i_ccchkf. ;
           where;
              PUNUMKEY = this.w_NUMKEY;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
        select _Curs_PAR__MRP
        continue
      enddo
      use
    endif
    * --- Select from SEL__MRP
    i_nConn=i_TableProp[this.SEL__MRP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SEL__MRP_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select GMSERIAL,GMTIPDOC  from "+i_cTable+" SEL__MRP ";
           ,"_Curs_SEL__MRP")
    else
      select GMSERIAL,GMTIPDOC from (i_cTable);
        into cursor _Curs_SEL__MRP
    endif
    if used('_Curs_SEL__MRP')
      select _Curs_SEL__MRP
      locate for 1=1
      do while not(eof())
      this.w_NUMKEY = _Curs_SEL__MRP.GMSERIAL
      this.w_TIPDOC = _Curs_SEL__MRP.GMTIPDOC
      * --- 1-Aggiorna GMTIPDOC
      * --- Vecchia composizione campo:
      *     CAUSALE+DESCRIZIONE+CHECK ->5+35+1=41
      *     Nuova composizione campo:
      *     CAUSALE+DESCRIZIONE+TIPO+CHECK ->5+35+9+1=50
      this.w_CONTA1 = 1
      this.w_CONTA2 = 0
      this.w_CONTA3 = 41
      this.w_CONTA4 = 0
      this.w_CHECKSTR = SUBSTR(this.w_TIPDOC,this.w_CONTA3,this.w_CONTA1)
      this.w_TIPDOC = ""
      if this.w_CHECKSTR="1" or this.w_CHECKSTR="0"
        do while this.w_CHECKSTR="1" or this.w_CHECKSTR="0"
          this.w_CONTA4 = (43*this.w_CONTA2)+1
          this.w_CAUDOC = alltrim(substr(_Curs_SEL__MRP.GMTIPDOC,this.w_CONTA4,5))
          * --- Read from TIP_DOCU
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "TDCAUMAG"+;
              " from "+i_cTable+" TIP_DOCU where ";
                  +"TDTIPDOC = "+cp_ToStrODBC(this.w_CAUDOC);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              TDCAUMAG;
              from (i_cTable) where;
                  TDTIPDOC = this.w_CAUDOC;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CAUMAG = NVL(cp_ToDate(_read_.TDCAUMAG),cp_NullValue(_read_.TDCAUMAG))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Read from CAM_AGAZ
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CMFLORDI"+;
              " from "+i_cTable+" CAM_AGAZ where ";
                  +"CMCODICE = "+cp_ToStrODBC(this.w_CAUCOL);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CMFLORDI;
              from (i_cTable) where;
                  CMCODICE = this.w_CAUCOL;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_FLORDI1 = NVL(cp_ToDate(_read_.CMFLORDI),cp_NullValue(_read_.CMFLORDI))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Read from CAM_AGAZ
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CMCAUCOL,CMFLORDI"+;
              " from "+i_cTable+" CAM_AGAZ where ";
                  +"CMCODICE = "+cp_ToStrODBC(this.w_CAUMAG);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CMCAUCOL,CMFLORDI;
              from (i_cTable) where;
                  CMCODICE = this.w_CAUMAG;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CAUCOL = NVL(cp_ToDate(_read_.CMCAUCOL),cp_NullValue(_read_.CMCAUCOL))
            this.w_FLORDI = NVL(cp_ToDate(_read_.CMFLORDI),cp_NullValue(_read_.CMFLORDI))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_AGGSALDI = IIF(this.w_FLORDI="+" or this.w_FLORDI1="+",ah_Msgformat("Ordinato"),ah_Msgformat("Impegnato"))
          this.w_TIPDOC = alltrim(this.w_TIPDOC)+substr(_Curs_SEL__MRP.GMTIPDOC,this.w_CONTA4,40)+left(alltrim(this.w_AGGSALDI)+space(9),9)+substr(_Curs_SEL__MRP.GMTIPDOC,this.w_CONTA3,1)+chr(13)+chr(10)
          this.w_CONTA1 = this.w_CONTA1+1
          this.w_CONTA2 = this.w_CONTA2+1
          this.w_CONTA3 = (41*this.w_CONTA1)+(2*this.w_CONTA2)
          this.w_CHECKSTR = SUBSTR(_Curs_SEL__MRP.GMTIPDOC,this.w_CONTA3,1)
        enddo
      endif
      if not empty(this.w_TIPDOC)
        * --- Write into SEL__MRP
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.SEL__MRP_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SEL__MRP_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.SEL__MRP_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"GMTIPDOC ="+cp_NullLink(cp_ToStrODBC(this.w_TIPDOC),'SEL__MRP','GMTIPDOC');
              +i_ccchkf ;
          +" where ";
              +"GMSERIAL = "+cp_ToStrODBC(this.w_NUMKEY);
                 )
        else
          update (i_cTable) set;
              GMTIPDOC = this.w_TIPDOC;
              &i_ccchkf. ;
           where;
              GMSERIAL = this.w_NUMKEY;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
        select _Curs_SEL__MRP
        continue
      enddo
      use
    endif
    * --- commit
    cp_EndTrs(.t.)
    this.w_TMPC = ah_Msgformat("Aggiornamento eseguito con successo")
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = this.w_TMPC
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='PAR__MRP'
    this.cWorkTables[2]='SEL__MRP'
    this.cWorkTables[3]='TIP_DOCU'
    this.cWorkTables[4]='CAM_AGAZ'
    return(this.OpenAllTables(4))

  proc CloseCursors()
    if used('_Curs_PAR__MRP')
      use in _Curs_PAR__MRP
    endif
    if used('_Curs_SEL__MRP')
      use in _Curs_SEL__MRP
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
