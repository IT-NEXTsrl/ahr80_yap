* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscv_b85                                                        *
*              Aggiornamento valore fiscale                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_27]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-11-19                                                      *
* Last revis.: 2001-11-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscv_b85",oParentObject,m.w_NHF)
return(i_retval)

define class tgscv_b85 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_SERIAL = space(10)
  w_IMPNAZ = 0
  w_CPROWNUM = 0
  w_VALMAG = 0
  w_VALULT = 0
  w_PERIVA = 0
  w_PERIVE = 0
  w_INDIVE = 0
  w_INDIVA = 0
  w_CAONAZ = 0
  w_CODVAL = space(3)
  w_VALNAZ = space(3)
  w_CAOVAL = 0
  w_DATREG = ctod("  /  /  ")
  w_DATDOC = ctod("  /  /  ")
  w_DECTOU = 0
  w_QTAUM1 = 0
  w_TIPO = space(2)
  w_NUMRIF = 0
  * --- WorkFile variables
  DOC_DETT_idx=0
  MVM_DETT_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- FIle di LOG
    * --- Try
    local bErr_037A0830
    bErr_037A0830=bTrsErr
    this.Try_037A0830()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE - impossibile aggiornare valore fiscale")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_037A0830
    * --- End
  endproc
  proc Try_037A0830()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Aggiornamento  Valore Fiscale
    * --- Select from gscv_b85
    do vq_exec with 'gscv_b85',this,'_Curs_gscv_b85','',.f.,.t.
    if used('_Curs_gscv_b85')
      select _Curs_gscv_b85
      locate for 1=1
      do while not(eof())
      this.w_SERIAL = _Curs_GSCV_B85.MVSERIAL
      this.w_CPROWNUM = _Curs_GSCV_B85.CPROWNUM
      this.w_VALMAG = NVL(_Curs_GSCV_B85.MVVALMAG,0)
      this.w_PERIVE = NVL(_Curs_GSCV_B85.PERIVE,0)
      this.w_PERIVA = NVL(_Curs_GSCV_B85.PERIVA,0)
      this.w_INDIVA = NVL(_Curs_GSCV_B85.INDIVA,0)
      this.w_INDIVE = NVL(_Curs_GSCV_B85.INDIVE,0)
      this.w_CAONAZ = NVL(_Curs_GSCV_B85.CAONAZ,0)
      this.w_DATDOC = CP_TODATE(_Curs_GSCV_B85.MVDATDOC)
      this.w_DATREG = CP_TODATE(_Curs_GSCV_B85.MVDATREG)
      this.w_CODVAL = NVL(_Curs_GSCV_B85.MVCODVAL,SPACE(3))
      this.w_VALNAZ = NVL(_Curs_GSCV_B85.MVVALNAZ,SPACE(3))
      this.w_CAOVAL = NVL(_Curs_GSCV_B85.MVCAOVAL,0)
      this.w_DECTOU = NVL(_Curs_GSCV_B85.DECTOU,0)
      this.w_QTAUM1 = NVL(_Curs_GSCV_B85.MVQTAUM1,0)
      this.w_IMPNAZ = CALCNAZ(this.w_VALMAG, this.w_CAOVAL, this.w_CAONAZ, IIF(EMPTY(this.w_DATDOC),this.w_DATREG,this.w_DATDOC), this.w_VALNAZ,this.w_CODVAL,IIF(this.w_PERIVE<>0,this.w_PERIVE*this.w_INDIVE,this.w_PERIVA*this.w_INDIVA))
      this.w_VALULT = IIF(this.w_QTAUM1=0, 0, cp_ROUND(this.w_IMPNAZ/this.w_QTAUM1, this.w_DECTOU))
      this.w_TIPO = NVL(_Curs_GSCV_B85.TIPO," ")
      if this.w_TIPO="D"
        this.w_NUMRIF = -20
        * --- Write into DOC_DETT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.DOC_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MVIMPNAZ ="+cp_NullLink(cp_ToStrODBC(this.w_IMPNAZ),'DOC_DETT','MVIMPNAZ');
          +",MVVALULT ="+cp_NullLink(cp_ToStrODBC(this.w_VALULT),'DOC_DETT','MVVALULT');
              +i_ccchkf ;
          +" where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
              +" and MVNUMRIF = "+cp_ToStrODBC(this.w_NUMRIF);
                 )
        else
          update (i_cTable) set;
              MVIMPNAZ = this.w_IMPNAZ;
              ,MVVALULT = this.w_VALULT;
              &i_ccchkf. ;
           where;
              MVSERIAL = this.w_SERIAL;
              and CPROWNUM = this.w_CPROWNUM;
              and MVNUMRIF = this.w_NUMRIF;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      else
        this.w_NUMRIF = -10
        * --- Write into MVM_DETT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.MVM_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MVM_DETT_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.MVM_DETT_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MMIMPNAZ ="+cp_NullLink(cp_ToStrODBC(this.w_IMPNAZ),'MVM_DETT','MMIMPNAZ');
          +",MMVALULT ="+cp_NullLink(cp_ToStrODBC(this.w_VALULT),'MVM_DETT','MMVALULT');
              +i_ccchkf ;
          +" where ";
              +"MMSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
                 )
        else
          update (i_cTable) set;
              MMIMPNAZ = this.w_IMPNAZ;
              ,MMVALULT = this.w_VALULT;
              &i_ccchkf. ;
           where;
              MMSERIAL = this.w_SERIAL;
              and CPROWNUM = this.w_CPROWNUM;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
        select _Curs_gscv_b85
        continue
      enddo
      use
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Aggiornamento valore fiscale eseguito con successo")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = this.oParentObject.w_PMSG + this.w_TMPC 
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='DOC_DETT'
    this.cWorkTables[2]='MVM_DETT'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_gscv_b85')
      use in _Curs_gscv_b85
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
