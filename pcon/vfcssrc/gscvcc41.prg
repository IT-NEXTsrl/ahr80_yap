* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcc41                                                        *
*              Gestione chiave BUS_TIPI                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-07-02                                                      *
* Last revis.: 2014-10-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcc41",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcc41 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  * --- WorkFile variables
  BUS_TIPI_idx=0
  TMP_PRAT_idx=0
  ISC_MAST_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- FIle di LOG
    * --- Try
    local bErr_00E17138
    bErr_00E17138=bTrsErr
    this.Try_00E17138()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("Errore generico")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_00E17138
    * --- End
    * --- 
  endproc
  proc Try_00E17138()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if IsAlt()
      * --- Appoggia il contenuto della tabella in una tabella temporanea
      *     Utilizziamo la definition from query perch� in questo modo inseriamo il campo BTTIPREC opportunamente inizializzato
      * --- begin transaction
      cp_BeginTrs()
       
 i_nConn=i_TableProp[this.BUS_TIPI_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.BUS_TIPI_idx,2])
      * --- Create temporary table TMP_PRAT
      i_nIdx=cp_AddTableDef('TMP_PRAT') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('..\PCON\EXE\QUERY\GSCVCC41',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.TMP_PRAT_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      ah_Msg("Creata tabella temporanea di appoggio",.T.)
      * --- Elimino la tabella
      if Not GSCV_BDT( this, "BUS_TIPI" , i_nConn )
        * --- Raise
        i_Error="Error"
        return
      endif
      * --- Ricostruisco il database per la tabella
      GSCV_BRT(this, "BUS_TIPI" , i_nConn , .T.)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Ripristina il contenuto di TMP_PRAT in BUS_TIPI
      * --- Insert into BUS_TIPI
      i_nConn=i_TableProp[this.BUS_TIPI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.BUS_TIPI_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_cTempTable=cp_SetAzi(i_TableProp[this.TMP_PRAT_idx,2])
        i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"BTCODICE, BTDESCRI, BT_SCHEMA, BTTIPREC"," from "+i_cTempTable,this.BUS_TIPI_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Elimina tabella TMP
      * --- Drop temporary table TMP_PRAT
      i_nIdx=cp_GetTableDefIdx('TMP_PRAT')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('TMP_PRAT')
      endif
      * --- commit
      cp_EndTrs(.t.)
      * --- Procede con l'aggiornamento dei flag NISIECIC per quelle buste per le quali il flag in BUS_TIPI ha un valore diverso 
      *     Lo facciamo dopo aver chiuso la transazione per evitare che BUS_TIPI possa restare vuoto
      * --- Write into ISC_MAST
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.ISC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ISC_MAST_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="NISERIAL"
        do vq_exec with 'gscvcc41_2',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ISC_MAST_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="ISC_MAST.NISERIAL = _t2.NISERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"NISIECIC = _t2.NISIECIC";
            +i_ccchkf;
            +" from "+i_cTable+" ISC_MAST, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="ISC_MAST.NISERIAL = _t2.NISERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ISC_MAST, "+i_cQueryTable+" _t2 set ";
            +"ISC_MAST.NISIECIC = _t2.NISIECIC";
            +Iif(Empty(i_ccchkf),"",",ISC_MAST.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="ISC_MAST.NISERIAL = t2.NISERIAL";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ISC_MAST set (";
            +"NISIECIC";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.NISIECIC";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="ISC_MAST.NISERIAL = _t2.NISERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ISC_MAST set ";
            +"NISIECIC = _t2.NISIECIC";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".NISERIAL = "+i_cQueryTable+".NISERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"NISIECIC = (select NISIECIC from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Esecuzione ok
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("Aggiornamento BUS_TIPI eseguito correttamente")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
      this.oParentObject.w_PESEOK = .T.
      this.oParentObject.w_PMSG = ah_msgformat("Conversione eseguita correttamente")
    endif
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='BUS_TIPI'
    this.cWorkTables[2]='*TMP_PRAT'
    this.cWorkTables[3]='ISC_MAST'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
