* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcb58                                                        *
*              Creazione nuovo indice su tabella DES_DIVE                      *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-03-17                                                      *
* Last revis.: 2009-10-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcb58",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcb58 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TMPC = space(10)
  w_TEMP = space(10)
  w_TABLE = space(8)
  w_CONN = 0
  * --- WorkFile variables
  DOC_MAST_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Creazione nuovo indice in tabella DOC_MAST su tabella DES_DIVE (DDTIPCON,DDCODICE,DDCODDES) 
    * --- FIle di LOG
    * --- Try
    local bErr_04A83780
    bErr_04A83780=bTrsErr
    this.Try_04A83780()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = Ah_MsgFormat("ERRORE GENERICO - Impossibile creare l'indice in tabella  %1", "DOC_MAST")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_04A83780
    * --- End
  endproc
  proc Try_04A83780()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Converto la stringa vuota in null, questo per potere creare la chiave esterna nella tabella DOC_MAST
    * --- Write into DOC_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="MVSERIAL"
      do vq_exec with 'GSCVCB58',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="DOC_MAST.MVSERIAL = _t2.MVSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVCODDES ="+cp_NullLink(cp_ToStrODBC(NULL),'DOC_MAST','MVCODDES');
          +i_ccchkf;
          +" from "+i_cTable+" DOC_MAST, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="DOC_MAST.MVSERIAL = _t2.MVSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_MAST, "+i_cQueryTable+" _t2 set ";
      +"DOC_MAST.MVCODDES ="+cp_NullLink(cp_ToStrODBC(NULL),'DOC_MAST','MVCODDES');
          +Iif(Empty(i_ccchkf),"",",DOC_MAST.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="DOC_MAST.MVSERIAL = t2.MVSERIAL";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_MAST set (";
          +"MVCODDES";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +cp_NullLink(cp_ToStrODBC(NULL),'DOC_MAST','MVCODDES')+"";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="DOC_MAST.MVSERIAL = _t2.MVSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_MAST set ";
      +"MVCODDES ="+cp_NullLink(cp_ToStrODBC(NULL),'DOC_MAST','MVCODDES');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVCODDES ="+cp_NullLink(cp_ToStrODBC(NULL),'DOC_MAST','MVCODDES');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Verifico che non vi siano incogruenze relazionali, fra i codici sede presenti nei documenti e la relativa anagrafica 
    *     Se ci sono esco senza fare nulla
    vq_exec("..\Pcon\EXE\QUERY\GSCVCB58_2",this,"__TMP__")
    if reccount("__TMP__") > 0
      if Ah_YesNo("Riscontrate incongruenze relazionali su alcuni codici sede presenti nei documenti. %0Si vuole stampare l'elenco?")
        cp_chprn("..\pcon\exe\query\GSCVCB58_2.FRX")
      endif
      this.oParentObject.w_PESEOK = .F.
      this.oParentObject.w_PMSG = "Riscontrate incongruenze relazionali su alcuni codici sede presenti nei documenti ."
      this.w_TMPC = Ah_MsgFormat("Sono presenti codici sede nei documenti non inseriti nell'anagrafica Clienti/Fornitori.%0Provvedere a sistemare i codici nell'anagrafica Clienti/Fornitori, facendo attenzione alla tipologia sede, e successivamente rieseguire la procedura di conversione.")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      Use in Select("__TMP__")
      i_retcode = 'stop'
      return
    endif
    if upper(CP_DBTYPE)<>"DB2"
      this.w_TABLE = "DOC_MAST"
      * --- Elimino integritÓ referenziali su tabella DOC_MAST (Fuori dalla transazione)
      this.w_TMPC = ah_msgformat("Rimozione integritÓ referenziale")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      GSCV_BIN(this,i_CODAZI,this.w_TABLE)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- begin transaction
    cp_BeginTrs()
    * --- commit
    cp_EndTrs(.t.)
    this.w_TABLE = "DES_DIVE"
    * --- Acquisisco handle della connessione al database
    this.w_CONN = i_ServerConn[1,2]
    * --- Ricostruisco le integritÓ referenziali su tabella DOC_MAST
    if this.w_CONN # 0
      GSCV_BRI(this, this.w_TABLE , this.w_CONN )
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      * --- Raise
      i_Error="Connessione al database non trovata"
      return
    endif
    * --- Esecuzione ok
    if this.w_NHF>=0
      this.w_TMPC = Ah_MsgFormat("Creazione indice in tabella DOC_MAST avvenuta correttamente")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = Ah_MsgFormat("Conversione eseguita correttamente")
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='DOC_MAST'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
