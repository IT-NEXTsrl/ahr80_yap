* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvbb03                                                        *
*              Ricostruzione cpttbls                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_12]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-11-14                                                      *
* Last revis.: 2005-11-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvbb03",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvbb03 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_MESS = space(100)
  * --- WorkFile variables
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Ricostruzione tabella CPTTBLS
    *     Estende il campo FileName a C(30) e ricostruisce la tabella per eliminare 
    *     eventuali tabelle non pi� in analisi
    * --- FIle di LOG
    * --- Salvo il vecchio contenuto della tabella CPTTBLS
    l_ret=cp_SQL(i_ServerConn[1,2],"select * from cpttbls","AppoCur")
    if l_ret < 0
      * --- Errore Esecuzione
      this.oParentObject.w_PESEOK = .F.
      this.oParentObject.w_PMSG = ah_Msgformat("%1%0Errore durante il salvataggio della tabella CPTTBLS",Message() )
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE GENERICO - impossibile creare il cursore AppoCur")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
      use in select("AppoCur")
      i_retcode = 'stop'
      return
    endif
    * --- Verifico se deve essere eseguita la procedura di conversione
    select AppoCur
    AFIELDS(AppoArr)
    if AppoArr[ASCAN(AppoArr, "FILENAME",-1,-1,1,8),3]<30
      * --- Cancello la tabella CPTTBLS
      l_ret=cp_SQL(i_ServerConn[1,2],"drop table cpttbls")
      if l_ret < 0
        * --- Errore Esecuzione
        this.oParentObject.w_PESEOK = .F.
        this.oParentObject.w_PMSG = ah_Msgformat("%1%0Errore durante la cancellazione della tabella CPTTBLS",Message() )
        if this.w_NHF>=0
          this.w_TMPC = ah_Msgformat("ERRORE GENERICO -impossibile cancellare la tabella CPTTBLS")
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
        use in select("AppoCur")
        i_retcode = 'stop'
        return
      endif
      * --- Ricostruisco la tabella CPTTBLS
      This.UpdateCPTTBLS()
      * --- Rimetto a posto i campi DateMod e ServerName
       
 select AppoCur 
 locate for 1=1
      do while not(eof())
        * --- Ciclo sul cursore temporaneo ed effettuo gli update
        l_ret=cp_SQL(i_ServerConn[1,2],"update cpttbls set ServerName="+cp_ToStr(NVL(servername,""))+" , DateMod="+cp_ToStr(NVL(datemod,"")) +" where filename="+cp_ToStr(NVL(filename,"")))
        if l_ret < 0
          * --- Errore Esecuzione
          this.oParentObject.w_PESEOK = .F.
          this.oParentObject.w_PMSG = ah_Msgformat("%1%0Errore durante l'aggiornamento della tabella CPTTBLS",Message() )
          if this.w_NHF>=0
            this.w_TMPC = ah_Msgformat("ERRORE GENERICO - impossibile aggiornare la tabella CPTTBLS")
            this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
          endif
          i_retcode = 'stop'
          return
        endif
         
 select AppoCur 
 continue
      enddo
      * --- Esecuzione ok
      this.oParentObject.w_PESEOK = .T.
      this.oParentObject.w_PMSG = ah_Msgformat("Aggiornamento della tabella CPTTBLS effettuato correttamente")
    else
      * --- Esecuzione ok
      this.oParentObject.w_PESEOK = .T.
      this.oParentObject.w_PMSG = ah_Msgformat("Procedura di conversione non necessaria")
    endif
    use in select("AppoCur")
  endproc


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- gscvbb03
  *--- Questa funzione � stata copiata da CP_DBADM, metodo UpdateReg della
  *--- classe stdaManageDB
  Procedure UpdateCPTTBLS()
      local i,ph,i_phname
      bOk=.t.
      *--- Rileggo XDC
      cp_ReadXdc()
      *--- Creo la tabella
      cDatabaseType=cp_DbType
      db=cDatabaseType
      ah_Msg(MSG_CREATING_TABLE___D,.T.,.F.,.F.,"cpttbls")
      if i_ServerConn[1,2]<>0
        cp_SQL(i_ServerConn[1,2],"create table cpttbls (FileName "+db_FieldType(db,'C',30,0)+" not null, PhName "+db_FieldType(db,'C',50,0)+", FlagYear "+db_FieldType(db,'C',1,0)+", FlagUser "+db_FieldType(db,'C',1,0)+", FlagComp "+db_FieldType(db,'C',1,0)+", ServerName "+db_FieldType(db,'C',10,0)+", DateMod "+db_FieldType(db,'C',14,0)+", Replica "+db_FieldType(db,'C',10,0)+", Stato "+db_FieldType(db,'N',6,0)+", MasterTable "+db_FieldType(db,'C',10,0)+", ReplKey "+db_FieldType(db,'C',10,0)+db_InlinePrimaryKey("cpttbls","cpttbls","filename",cDatabaseType)+')'+cp_PostTable(cDatabaseType))
        if cDatabaseType='DB2/390'
          r=cp_SQL(i_ServerConn[1,2],'create unique index pk_cpttbls on cpttbls(filename)')
        endif
        db_CreatePrimaryKey(i_ServerConn[1,2],"cpttbls","cpttbls","filename",cDatabaseType)
      else
        create table cpttbls (FileName C(30), PhName C(50), FlagYear C(1), FlagUser C(1), FlagComp C(1), ServerName C(10), DateMod C(14), Replica C(10), Stato N(1), MasterTable C(10), ReplKey C(10))
        index on filename tag tag1 collate "MACHINE"
        use
      endif
      *--- Popola la tabella
      for i=1 to i_DCX.nfi
        if i_ServerConn[1,2]<>0
          =cp_SQL(i_ServerConn[1,2],"select * from cpttbls where filename="+cp_ToStr(i_DCX.Tbls[i,1]),"_tmp_")
        else
          select * from cpttbls where filename==i_DCX.Tbls[i,1] into cursor _tmp_
        endif
        if !i_DCX.IsTableAzi(i_DCX.Tbls[i,1])
          bOk = bOk and cp_VerifyLinkTables(i,i_DCX.Tbls[i,1],i_DCX)
        endIf
        if bOk
          if reccount()=0
            * --- inserisce il nuovo file nel registro
            i_phname=IIF(i_DCX.Tbls[i,19],i_DCX.Tbls[i,1]+'_proto',i_DCX.Tbls[i,2])
            IF !i_DCX.Tbls[i,19] && tabelle non temporanee
              if i_ServerConn[1,2]<>0
                =cp_SQL(i_ServerConn[1,2],"insert into cpttbls (filename,phname,flagyear,flaguser,flagcomp,datemod,servername) values ("+cp_ToStr(i_DCX.Tbls[i,1])+","+cp_ToStr(i_phname)+","+iif(i_DCX.Tbls[i,3],"'T'","'N'")+","+iif(i_DCX.Tbls[i,4],"'T'","'N'")+","+iif(i_DCX.Tbls[i,5],"'T'","'N'")+",'','')")
              else
                insert into cpttbls (filename,phname,flagyear,flaguser,flagcomp,datemod,servername) values (i_DCX.Tbls[i,1],i_phname,iif(i_DCX.Tbls[i,3],"T","N"),iif(i_DCX.Tbls[i,4],"T","N"),iif(i_DCX.Tbls[i,5],"T","N"),'','')
              ENDIF
            ENDIF
          else
            ph=i_DCX.Tbls[i,2]
            if ph<>trim(_tmp_.phname)
              if ah_Yesno("Il nome fisico della tabella � stato modificato rispetto alla fase di design%0Design: %1, database: %2%0Mantenere il nome corrente?",'',ph,trim(_tmp_.phname) )
                ph=_tmp_.phname
              endif
            endif
            if i_ServerConn[1,2]<>0
              =cp_SQL(i_ServerConn[1,2],"update cpttbls set phname="+cp_ToStr(ph)+",flagyear="+iif(i_DCX.Tbls[i,3],"'T'","'N'")+",flaguser="+iif(i_DCX.Tbls[i,4],"'T'","'N'")+",flagcomp="+iif(i_DCX.Tbls[i,5],"'T'","'N'")+" where filename="+cp_ToStr(i_DCX.Tbls[i,1]))
            else
              update cpttbls set phname=ph,flagyear=iif(i_DCX.Tbls[i,3],"T","N"),flaguser=iif(i_DCX.Tbls[i,4],"T","N"),flagcomp=iif(i_DCX.Tbls[i,5],"T","N") where filename==i_DCX.Tbls[i,1]
            endif
          endif
        endif
      next
  EndProc
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
