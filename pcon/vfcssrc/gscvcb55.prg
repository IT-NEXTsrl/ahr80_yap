* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcb55                                                        *
*              Inserimento attributo primario descrizione                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-01-16                                                      *
* Last revis.: 2011-01-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcb55",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcb55 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_CLASSEDOC = space(15)
  w_TABNAME = space(10)
  w_CAMPATPRI = space(15)
  w_CODATTDES = space(15)
  w_CODINDIC = space(20)
  w_CPROWNUM = 0
  w_CPROWORD = 0
  w_VALKEY = space(50)
  w_CAMPOCHIAVE = space(10)
  w_CAMPODESC = space(10)
  w_DESCRIZIONE = space(100)
  w_KEY_FIELDS = space(250)
  w_TROVATA = .f.
  w_CodRel = space(15)
  * --- WorkFile variables
  PRODINDI_idx=0
  CONVERSI_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inserimento all'interno degli indici dell'attributo primario descrizione, ove non presente
    this.w_CodRel = "7.0-M5877"
    * --- Try
    local bErr_03015418
    bErr_03015418=bTrsErr
    this.Try_03015418()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("Errore generico - impossibile eseguire l'inserimento richiesto")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_03015418
    * --- End
  endproc
  proc Try_03015418()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Legge se eseguita la conversione  7.0-M5877
    * --- Read from CONVERSI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CONVERSI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONVERSI_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "*"+;
        " from "+i_cTable+" CONVERSI where ";
            +"COCODREL = "+cp_ToStrODBC(this.w_CodRel);
            +" and COESEGUI = "+cp_ToStrODBC("S");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        *;
        from (i_cTable) where;
            COCODREL = this.w_CodRel;
            and COESEGUI = "S";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_TROVATA = i_Rows>0
    this.w_CLASSEDOC = "@@@@@@@@@@"
    * --- begin transaction
    cp_BeginTrs()
    if NOT this.w_TROVATA
      vq_Exec("query\GSCVCB55_1",this,"INDICI")
       
 Select INDICI 
 Go Top 
 Scan
      this.w_CODINDIC = INDICI.IDSERIAL
      this.w_VALKEY = ALLTRIM(NVL(INDICI.IDVALATT ," "))
      if ! empty(this.w_VALKEY)
        if this.w_CLASSEDOC<>INDICI.CDCODCLA
          this.w_CLASSEDOC = INDICI.CDCODCLA
          this.w_TABNAME = alltrim(nvl(INDICI.CDTABKEY," "))
          this.w_KEY_FIELDS = ALLTRIM(cp_KeyToSQL(I_DCX.GetIdxDef(this.w_TABNAME,1)))
        endif
        * --- Select from query\GSCVCB55_2
        do vq_exec with 'query\GSCVCB55_2',this,'_Curs_query_GSCVCB55_2','',.f.,.t.
        if used('_Curs_query_GSCVCB55_2')
          select _Curs_query_GSCVCB55_2
          locate for 1=1
          do while not(eof())
          this.w_CAMPATPRI = alltrim(nvl(_Curs_query_GSCVCB55_2.CDCAMCUR," "))
          if at(this.w_CAMPATPRI,this.w_KEY_FIELDS) > 0
            * --- Chiave
            this.w_CAMPOCHIAVE = alltrim(this.w_CAMPATPRI)
          else
            * --- Descrizione
            this.w_CODATTDES = alltrim(nvl(_Curs_query_GSCVCB55_2.CDCODATT," "))
            this.w_CAMPODESC = alltrim(this.w_CAMPATPRI)
            exit
          endif
            select _Curs_query_GSCVCB55_2
            continue
          enddo
          use
        endif
        * --- Inserisco l'attributo descrittivo primario per l'indice in oggetto
        * --- Calcolo il primo numero di riga disponibile
        * --- Select from QUERY\GSCVCB55_3
        do vq_exec with 'QUERY\GSCVCB55_3',this,'_Curs_QUERY_GSCVCB55_3','',.f.,.t.
        if used('_Curs_QUERY_GSCVCB55_3')
          select _Curs_QUERY_GSCVCB55_3
          locate for 1=1
          do while not(eof())
          this.w_CPROWNUM = _Curs_QUERY_GSCVCB55_3.CPROWNUM + 1
          this.w_CPROWORD = _Curs_QUERY_GSCVCB55_3.CPROWORD + 10
            select _Curs_QUERY_GSCVCB55_3
            continue
          enddo
          use
        endif
        * --- Leggo la desrizione associata all'attributo primario chiave
         i_nOldArea=select()
         
 i_nConn=i_TableProp[this.PRODINDI_idx,3] 
 i_cTable=alltrim(i_CODAZI)+alltrim(this.w_TABNAME)
        if i_nConn<>0
           
 cp_sqlexec(i_nConn,"select "+; 
 this.w_CAMPODESC +; 
 " from "+i_cTable+"  where "; 
 +this.w_CAMPOCHIAVE+" = "+cp_ToStrODBC(this.w_VALKEY); 
 ,"_read_")
        endif
        if used("_read_")
          L_campo ="_read_."+alltrim(this.w_campodesc)
          this.w_DESCRIZIONE = &L_campo
          USE IN SELECT("_read_")
        endif
        select (i_nOldArea)
        * --- Inserisco il nuovo attributo
        * --- Insert into PRODINDI
        i_nConn=i_TableProp[this.PRODINDI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PRODINDI_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PRODINDI_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"IDSERIAL"+",CPROWNUM"+",CPROWORD"+",IDCODATT"+",IDVALATT"+",IDTIPATT"+",IDCHKOBB"+",IDTABKEY"+",IDDESATT"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_CODINDIC),'PRODINDI','IDSERIAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'PRODINDI','CPROWNUM');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'PRODINDI','CPROWORD');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CODATTDES),'PRODINDI','IDCODATT');
          +","+cp_NullLink(cp_ToStrODBC(this.w_DESCRIZIONE),'PRODINDI','IDVALATT');
          +","+cp_NullLink(cp_ToStrODBC("C"),'PRODINDI','IDTIPATT');
          +","+cp_NullLink(cp_ToStrODBC("S"),'PRODINDI','IDCHKOBB');
          +","+cp_NullLink(cp_ToStrODBC(this.w_TABNAME),'PRODINDI','IDTABKEY');
          +","+cp_NullLink(cp_ToStrODBC("Descrizione attributo primario"),'PRODINDI','IDDESATT');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'IDSERIAL',this.w_CODINDIC,'CPROWNUM',this.w_CPROWNUM,'CPROWORD',this.w_CPROWORD,'IDCODATT',this.w_CODATTDES,'IDVALATT',this.w_DESCRIZIONE,'IDTIPATT',"C",'IDCHKOBB',"S",'IDTABKEY',this.w_TABNAME,'IDDESATT',"Descrizione attributo primario")
          insert into (i_cTable) (IDSERIAL,CPROWNUM,CPROWORD,IDCODATT,IDVALATT,IDTIPATT,IDCHKOBB,IDTABKEY,IDDESATT &i_ccchkf. );
             values (;
               this.w_CODINDIC;
               ,this.w_CPROWNUM;
               ,this.w_CPROWORD;
               ,this.w_CODATTDES;
               ,this.w_DESCRIZIONE;
               ,"C";
               ,"S";
               ,this.w_TABNAME;
               ,"Descrizione attributo primario";
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      endif
      EndScan
      this.w_TMPC = ah_Msgformat("Inserimento attributo primario 'Descrizione' eseguito con successo")
    endif
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = this.w_TMPC
    * --- commit
    cp_EndTrs(.t.)
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='PRODINDI'
    this.cWorkTables[2]='CONVERSI'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_query_GSCVCB55_2')
      use in _Curs_query_GSCVCB55_2
    endif
    if used('_Curs_QUERY_GSCVCB55_3')
      use in _Curs_QUERY_GSCVCB55_3
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
