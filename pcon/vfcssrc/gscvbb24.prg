* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvbb24                                                        *
*              Aggiornamento mvnumcol mvqtacol su documenti                    *
*                                                                              *
*      Author: Nicola Gorlandi                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_360]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-10-18                                                      *
* Last revis.: 2001-10-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvbb24",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvbb24 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_CSQL = space(200)
  w_IROWS = 0
  * --- WorkFile variables
  TMPMOVIMAST_idx=0
  DOC_DETT_idx=0
  DOC_MAST_idx=0
  PAC_DETT_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiornamento DOC_DETT campo MVNUMCOL per DB2 e ORACLE
    * --- Try
    local bErr_0361DFA0
    bErr_0361DFA0=bTrsErr
    this.Try_0361DFA0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0 
        this.w_TMPC = this.oParentObject.w_PMSG
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_0361DFA0
    * --- End
  endproc
  proc Try_0361DFA0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    i_nConn=i_TableProp[this.DOC_DETT_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
    * --- Carica dizionario dati (per sicurezza)
    =cp_ReadXdc()
    if upper(CP_DBTYPE)="ORACLE"
      this.w_CSQL = "alter table "+i_cTable+" "+db_ModifyColumn("MVNUMCOL",db_FieldType(cp_dbtype,"N",5,0),cp_dbtype,1)
      this.w_IROWS = SQLExec(i_nConn, this.w_CSQL )
      if this.w_iRows=-1
        * --- Raise
        i_Error="Error"
        return
      else
        * --- Se tutto Ok aggiorno anche MVQTACOL in DOC_MAST
        i_nConn=i_TableProp[this.DOC_MAST_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
        this.w_CSQL = "alter table "+i_cTable+" "+db_ModifyColumn("MVQTACOL",db_FieldType(cp_dbtype,"N",5,0),cp_dbtype,1)
        this.w_IROWS = SQLExec(i_nConn, this.w_CSQL )
        if this.w_iRows=-1
          * --- Raise
          i_Error="Error"
          return
        else
          * --- Se tutto Ok aggiorno anche CPROWORD in PAC_DETT
          i_nConn=i_TableProp[this.PAC_DETT_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.PAC_DETT_idx,2])
          this.w_CSQL = "alter table "+i_cTable+" "+db_ModifyColumn("CPROWORD",db_FieldType(cp_dbtype,"N",6,0),cp_dbtype,1)
          this.w_IROWS = SQLExec(i_nConn, this.w_CSQL )
          if this.w_iRows=-1
            * --- Raise
            i_Error="Error"
            return
          endif
        endif
      endif
    else
      * --- Create temporary table TMPMOVIMAST
      i_nIdx=cp_AddTableDef('TMPMOVIMAST') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      i_nConn=i_TableProp[this.DOC_DETT_idx,3] && recupera la connessione
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
      cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
            )
      this.TMPMOVIMAST_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      ah_Msg("Creata tabella temporanea di appoggio (DOC_DETT)",.T.)
      * --- Elimino la tabella
      if Not GSCV_BDT( this, "DOC_DETT" , i_nConn )
        * --- Raise
        i_Error="Error"
        return
      endif
      * --- Ricostruisco il database per la tabella
      GSCV_BRT(this, "DOC_DETT" , i_nConn , .f.)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Insert into DOC_DETT
      i_nConn=i_TableProp[this.DOC_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_cTempTable=cp_SetAzi(i_TableProp[this.TMPMOVIMAST_idx,2])
        i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.DOC_DETT_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      ah_Msg("Ripristinati valori di origine (DOC_DETT)",.T.)
      GSCV_BRI(this, "DOC_DETT" , i_nConn , .T.)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Elimina tabella TMP
      * --- Drop temporary table TMPMOVIMAST
      i_nIdx=cp_GetTableDefIdx('TMPMOVIMAST')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('TMPMOVIMAST')
      endif
      ah_Msg("Eliminata tabella temporanea tmp (DOC_DETT)",.T.)
      * --- Aggiorno MVQTACOL in DOC_MAST
      * --- Create temporary table TMPMOVIMAST
      i_nIdx=cp_AddTableDef('TMPMOVIMAST') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      i_nConn=i_TableProp[this.DOC_MAST_idx,3] && recupera la connessione
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
      cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
            )
      this.TMPMOVIMAST_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      ah_Msg("Creata tabella temporanea di appoggio (DOC_MAST)",.T.)
      * --- Elimino la tabella
      if Not GSCV_BDT( this, "DOC_MAST" , i_nConn )
        * --- Raise
        i_Error="Error"
        return
      endif
      * --- Ricostruisco il database per la tabella
      GSCV_BRT(this, "DOC_MAST" , i_nConn , .f.)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Insert into DOC_MAST
      i_nConn=i_TableProp[this.DOC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_cTempTable=cp_SetAzi(i_TableProp[this.TMPMOVIMAST_idx,2])
        i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.DOC_MAST_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      ah_Msg("Ripristinati valori di origine (DOC_MAST)",.T.)
      GSCV_BRI(this, "DOC_MAST" , i_nConn , .T.)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Elimina tabella TMP
      * --- Drop temporary table TMPMOVIMAST
      i_nIdx=cp_GetTableDefIdx('TMPMOVIMAST')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('TMPMOVIMAST')
      endif
      ah_Msg("Eliminata tabella temporanea tmp (DOC_MAST)",.T.)
      * --- Aggiorno CPROWNUM in PAC_DETT
      * --- Create temporary table TMPMOVIMAST
      i_nIdx=cp_AddTableDef('TMPMOVIMAST') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      i_nConn=i_TableProp[this.PAC_DETT_idx,3] && recupera la connessione
      i_cTable=cp_SetAzi(i_TableProp[this.PAC_DETT_idx,2])
      cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
            )
      this.TMPMOVIMAST_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      ah_Msg("Creata tabella temporanea di appoggio (PAC_DETT)",.T.)
      * --- Elimino la tabella
      if Not GSCV_BDT( this, "PAC_DETT" , i_nConn )
        * --- Raise
        i_Error="Error"
        return
      endif
      * --- Ricostruisco il database per la tabella
      GSCV_BRT(this, "PAC_DETT" , i_nConn , .f.)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Insert into PAC_DETT
      i_nConn=i_TableProp[this.PAC_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAC_DETT_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_cTempTable=cp_SetAzi(i_TableProp[this.TMPMOVIMAST_idx,2])
        i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.PAC_DETT_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      ah_Msg("Ripristinati valori di origine (PAC_DETT)",.T.)
      GSCV_BRI(this, "PAC_DETT" , i_nConn , .T.)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Elimina tabella TMP
      * --- Drop temporary table TMPMOVIMAST
      i_nIdx=cp_GetTableDefIdx('TMPMOVIMAST')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('TMPMOVIMAST')
      endif
      ah_Msg("Eliminata tabella temporanea tmp (PAC_DETT)",.T.)
    endif
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.w_TMPC = Ah_Msgformat("Adeguamento campo MVNUMCOL tabella DOC_DETT e MVQTACOL tabella DOC_MAST eseguito correttamente%0")
    this.oParentObject.w_PMSG = this.w_TMPC
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='*TMPMOVIMAST'
    this.cWorkTables[2]='DOC_DETT'
    this.cWorkTables[3]='DOC_MAST'
    this.cWorkTables[4]='PAC_DETT'
    return(this.OpenAllTables(4))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
