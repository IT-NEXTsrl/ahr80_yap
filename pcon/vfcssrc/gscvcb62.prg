* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcb62                                                        *
*              Aggiornamento attivit� offerte                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-03-18                                                      *
* Last revis.: 2011-01-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcb62",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcb62 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_TROVATA = .f.
  w_CodRel = space(15)
  * --- WorkFile variables
  OFF_ATTI_idx=0
  CONVERSI_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiornamento tabella OFF_ATTI
    * --- FIle di LOG
    this.w_CodRel = "7.0-M5423"
    if g_AGEN="S" 
      * --- Try
      local bErr_03817368
      bErr_03817368=bTrsErr
      this.Try_03817368()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- Gestisce log errori
        this.oParentObject.w_PMSG = "Errore nell'aggiornamento tabella OFF_ATTI:" + Message()
        this.oParentObject.w_PESEOK = .F.
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        if this.w_NHF>=0
          this.w_TMPC = this.oParentObject.w_PMSG
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
      bTrsErr=bTrsErr or bErr_03817368
      * --- End
    else
      * --- Esecuzione ok
      this.oParentObject.w_PESEOK = .T.
      this.w_TMPC = AH_MsgFormat("aggiornamento tabella OFF_ATTI eseguita correttamente")
      this.oParentObject.w_PMSG = this.w_TMPC
    endif
  endproc
  proc Try_03817368()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Legge se eseguita la conversione  7.0-M5423
    * --- Read from CONVERSI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CONVERSI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONVERSI_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "*"+;
        " from "+i_cTable+" CONVERSI where ";
            +"COCODREL = "+cp_ToStrODBC(this.w_CodRel);
            +" and COESEGUI = "+cp_ToStrODBC("S");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        *;
        from (i_cTable) where;
            COCODREL = this.w_CodRel;
            and COESEGUI = "S";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_TROVATA = i_Rows>0
    * --- begin transaction
    cp_BeginTrs()
    if NOT this.w_TROVATA
      * --- aggiorno status
      * --- Write into OFF_ATTI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.OFF_ATTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="ATSERIAL"
        do vq_exec with 'GSCVCB62',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_ATTI_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="OFF_ATTI.ATSERIAL = _t2.ATSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"ATFLATRI ="+cp_NullLink(cp_ToStrODBC("N"),'OFF_ATTI','ATFLATRI');
            +i_ccchkf;
            +" from "+i_cTable+" OFF_ATTI, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="OFF_ATTI.ATSERIAL = _t2.ATSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OFF_ATTI, "+i_cQueryTable+" _t2 set ";
        +"OFF_ATTI.ATFLATRI ="+cp_NullLink(cp_ToStrODBC("N"),'OFF_ATTI','ATFLATRI');
            +Iif(Empty(i_ccchkf),"",",OFF_ATTI.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="OFF_ATTI.ATSERIAL = t2.ATSERIAL";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OFF_ATTI set (";
            +"ATFLATRI";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +cp_NullLink(cp_ToStrODBC("N"),'OFF_ATTI','ATFLATRI')+"";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="OFF_ATTI.ATSERIAL = _t2.ATSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OFF_ATTI set ";
        +"ATFLATRI ="+cp_NullLink(cp_ToStrODBC("N"),'OFF_ATTI','ATFLATRI');
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".ATSERIAL = "+i_cQueryTable+".ATSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"ATFLATRI ="+cp_NullLink(cp_ToStrODBC("N"),'OFF_ATTI','ATFLATRI');
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    this.oParentObject.w_PESEOK = .T.
    this.w_TMPC = AH_MsgFormat("aggiornamento tabella OFF_ATTI eseguita correttamente")
    this.oParentObject.w_PMSG = this.w_TMPC
    * --- commit
    cp_EndTrs(.t.)
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='OFF_ATTI'
    this.cWorkTables[2]='CONVERSI'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
