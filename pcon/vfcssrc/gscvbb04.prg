* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvbb04                                                        *
*              Controllo doc. con sedi cancellate                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_84]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-02-27                                                      *
* Last revis.: 2003-02-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvbb04",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvbb04 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_MESS = space(100)
  * --- WorkFile variables
  STU_PIAC_idx=0
  STUMPIAC_idx=0
  DOC_MAST_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controlla la presenza di documenti con sedi cancellate dall'anagrafica conti
    * --- FIle di LOG
    * --- Stampo i documenti che hanno una destinazione diversa che � stata cancellata
    VQ_EXEC("..\PCON\EXE\QUERY\GSCVBB04.VQR", this, "__TMP__")
    SELECT __TMP__
    if reccount() > 0
      CP_CHPRN("..\PCON\EXE\QUERY\GSCVBB04.FRX")
      * --- Chiedo all'utente se desidera sbiancare il campo MVCODDES dei documenti
      *     evidenziati dalla stampa precedente
      if ah_YesNo("Sbiancare la destinazione diversa dei documenti rilevati dalla stampa precedente?")
        * --- Try
        local bErr_04A94770
        bErr_04A94770=bTrsErr
        this.Try_04A94770()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- Gestisce log errori
          this.oParentObject.w_PMSG = Message()
          this.oParentObject.w_PESEOK = .F.
          if this.w_NHF>=0
            this.w_TMPC = ah_Msgformat("ERRORE GENERICO - impossibile aggiornare i documenti")
            this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
          endif
        endif
        bTrsErr=bTrsErr or bErr_04A94770
        * --- End
      else
        * --- Esecuzione ok
        this.oParentObject.w_PESEOK = .T.
        this.oParentObject.w_PMSG = ah_Msgformat("Nessun documento � stato aggiornato")
      endif
    else
      ah_ErrorMsg("Non esistono dati da stampare",,"")
      * --- Esecuzione ok
      this.oParentObject.w_PESEOK = .T.
      this.oParentObject.w_PMSG = ah_Msgformat("Non esistono documenti con sedi di destinazione cancellate")
    endif
    USE IN SELECT("__TMP__")
  endproc
  proc Try_04A94770()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into DOC_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="MVSERIAL"
      do vq_exec with '..\PCON\EXE\QUERY\GSCVBB04',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="DOC_MAST.MVSERIAL = _t2.MVSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVCODDES ="+cp_NullLink(cp_ToStrODBC(.NULL.),'DOC_MAST','MVCODDES');
          +i_ccchkf;
          +" from "+i_cTable+" DOC_MAST, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="DOC_MAST.MVSERIAL = _t2.MVSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_MAST, "+i_cQueryTable+" _t2 set ";
      +"DOC_MAST.MVCODDES ="+cp_NullLink(cp_ToStrODBC(.NULL.),'DOC_MAST','MVCODDES');
          +Iif(Empty(i_ccchkf),"",",DOC_MAST.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="DOC_MAST.MVSERIAL = t2.MVSERIAL";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_MAST set (";
          +"MVCODDES";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +cp_NullLink(cp_ToStrODBC(.NULL.),'DOC_MAST','MVCODDES')+"";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="DOC_MAST.MVSERIAL = _t2.MVSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_MAST set ";
      +"MVCODDES ="+cp_NullLink(cp_ToStrODBC(.NULL.),'DOC_MAST','MVCODDES');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVCODDES ="+cp_NullLink(cp_ToStrODBC(.NULL.),'DOC_MAST','MVCODDES');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = Ah_Msgformat("I documenti sono stati aggiornati")
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='STU_PIAC'
    this.cWorkTables[2]='STUMPIAC'
    this.cWorkTables[3]='DOC_MAST'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
