* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvab63                                                        *
*              Eliminazione campi ptdessup\rsdessup obsoleti                   *
*                                                                              *
*      Author: Zucchetti TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_77]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-05-07                                                      *
* Last revis.: 2002-05-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvab63",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvab63 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_ROWS = 0
  w_OKPAR = .f.
  w_OKDOC = .f.
  w_PTDESSUP = space(0)
  * --- WorkFile variables
  LOG_STOR_idx=0
  PAR_TITE_idx=0
  DOC_RATE_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- FIle di LOG
    *     4.0 - Elimina campi obsoleti 
    *     PTDESSUP da PAR_TITE
    *     RSDESSUP da DOC_RATE
    this.w_OKPAR = .T.
    this.w_OKDOC = .T.
    if upper(CP_DBTYPE)<>"DB2"
      * --- Try
      local bErr_035EF1B0
      bErr_035EF1B0=bTrsErr
      this.Try_035EF1B0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_OKPAR = .F.
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_035EF1B0
      * --- End
      * --- Try
      local bErr_036008A0
      bErr_036008A0=bTrsErr
      this.Try_036008A0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_OKDOC = .F.
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_036008A0
      * --- End
    else
      this.w_OKPAR = .F.
      this.w_OKDOC = .F.
    endif
    * --- Try
    local bErr_0360E450
    bErr_0360E450=bTrsErr
    this.Try_0360E450()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_msgformat("ERRORE GENERICO - Impossibile eseguire aggiornamento richiesto")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_0360E450
    * --- End
  endproc
  proc Try_035EF1B0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Read from PAR_TITE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_TITE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PTDESSUP"+;
        " from "+i_cTable+" PAR_TITE where ";
            +"PTSERIAL = "+cp_ToStrODBC("0000000001");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PTDESSUP;
        from (i_cTable) where;
            PTSERIAL = "0000000001";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PTDESSUP = NVL(cp_ToDate(_read_.PTDESSUP),cp_NullValue(_read_.PTDESSUP))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    return
  proc Try_036008A0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Read from DOC_RATE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.DOC_RATE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_RATE_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "RSDESSUP"+;
        " from "+i_cTable+" DOC_RATE where ";
            +"RSSERIAL = "+cp_ToStrODBC("0000000001");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        RSDESSUP;
        from (i_cTable) where;
            RSSERIAL = "0000000001";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PTDESSUP = NVL(cp_ToDate(_read_.RSDESSUP),cp_NullValue(_read_.RSDESSUP))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    return
  proc Try_0360E450()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.w_ROWS = 0
    if this.w_OKPAR
      i_nConn=i_TableProp[this.PAR_TITE_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
      this.w_ROWS = SQLExec(i_nConn,"ALTER TABLE "+i_cTable+" DROP COLUMN PTDESSUP")
      if this.w_ROWS<0
        * --- Raise
        i_Error=Message()
        return
      else
        * --- Esecuzione ok
        this.oParentObject.w_PESEOK = .T.
        this.w_TMPC = ah_msgformat("Eliminazione campo PTDESSUP eseguita con successo%0")
        this.oParentObject.w_PMSG = this.w_TMPC
      endif
    else
      * --- Esecuzione ok
      this.oParentObject.w_PESEOK = .T.
      this.w_TMPC = ah_msgformat("Eliminazione campo PTDESSUP eseguita con successo%0")
      this.oParentObject.w_PMSG = this.w_TMPC
    endif
    if this.w_OKDOC
      i_nConn=i_TableProp[this.DOC_RATE_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.DOC_RATE_idx,2])
      this.w_ROWS = SQLExec(i_nConn,"ALTER TABLE "+i_cTable+" DROP COLUMN RSDESSUP")
      if this.w_ROWS<0
        * --- Raise
        i_Error=Message()
        return
      else
        * --- Esecuzione ok
        this.oParentObject.w_PESEOK = .T.
        this.w_TMPC = ah_msgformat("Eliminazione campo RSDESSUP eseguita con successo%0")
        this.oParentObject.w_PMSG = this.oParentObject.w_PMSG + this.w_TMPC
      endif
    else
      * --- Esecuzione ok
      this.oParentObject.w_PESEOK = .T.
      this.w_TMPC = ah_msgformat("Eliminazione campo RSDESSUP eseguita con successo%0")
      this.oParentObject.w_PMSG = this.oParentObject.w_PMSG + this.w_TMPC
    endif
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='LOG_STOR'
    this.cWorkTables[2]='PAR_TITE'
    this.cWorkTables[3]='DOC_RATE'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
