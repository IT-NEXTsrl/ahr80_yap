* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcc20                                                        *
*              Genera dati piano ammortamento civile                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-09-11                                                      *
* Last revis.: 2012-09-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcc20",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcc20 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_CCCODICE = space(15)
  w_CCDURCES = 0
  w_CCCOECIV = 0
  w_CCFLAMCI = space(1)
  w_CCPERCIV = 0
  w_CONTA = 0
  w_TOTPEORD = 0
  w_ECTIPAMM = space(1)
  w_ECPERORD = 0
  w_APPO = 0
  * --- WorkFile variables
  CAT_CESP_idx=0
  CAT_AMMC_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Genera dati piano ammortamento civile
    * --- FIle di LOG
    * --- Effettuo una lettura sulla tabella delle categorie, escludendo tutte le categorie
    *     che hanno il flag "Non soggetto ad ammortamento civile" attivo
    * --- Try
    local bErr_035EDEF0
    bErr_035EDEF0=bTrsErr
    this.Try_035EDEF0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = AH_MsgFormat("ERRORE - Impossibile generare la tabella esercizi/ammortamento civile")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_035EDEF0
    * --- End
  endproc
  proc Try_035EDEF0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Select from CAT_CESP
    i_nConn=i_TableProp[this.CAT_CESP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAT_CESP_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select CCCODICE,CCDURCES,CCCOECIV,CCFLAMCI,CCPERCIV  from "+i_cTable+" CAT_CESP ";
          +" where CCNSOAMM<>'S' AND CCCOECIV<>0";
          +" order by CCCODICE";
           ,"_Curs_CAT_CESP")
    else
      select CCCODICE,CCDURCES,CCCOECIV,CCFLAMCI,CCPERCIV from (i_cTable);
       where CCNSOAMM<>"S" AND CCCOECIV<>0;
       order by CCCODICE;
        into cursor _Curs_CAT_CESP
    endif
    if used('_Curs_CAT_CESP')
      select _Curs_CAT_CESP
      locate for 1=1
      do while not(eof())
      this.w_CCCODICE = _Curs_CAT_CESP.CCCODICE
      this.w_CCDURCES = _Curs_CAT_CESP.CCDURCES
      this.w_CCCOECIV = _Curs_CAT_CESP.CCCOECIV
      this.w_CCFLAMCI = _Curs_CAT_CESP.CCFLAMCI
      this.w_CCPERCIV = _Curs_CAT_CESP.CCPERCIV
      this.w_CONTA = 1
      this.w_TOTPEORD = 0
      * --- Devo inserire tanti record quanto � la durata del cespite per ogni categoria
      do while this.w_CONTA<=this.w_CCDURCES
        * --- Devo verificare se mi trovo nel primo esercizio, nell'ultimo oppure in un esercizio
        *     intermedio.
        do case
          case this.w_CONTA=1
            * --- Se sono nel primo esercizio il tipo ammortamento pu� assumere i valori (Ordinario - Ridotto 50% - Rateo utilizzo )
            do case
              case this.w_CCFLAMCI="R"
                * --- Rateo utilizzo
                this.w_ECTIPAMM = "R"
                this.w_ECPERORD = this.w_CCCOECIV
              case this.w_CCFLAMCI="5"
                * --- Ridotto 50 %
                this.w_ECTIPAMM = "5"
                this.w_ECPERORD = cp_ROUND(this.w_CCCOECIV/2, 2)
              case this.w_CCFLAMCI="I"
                * --- Intero
                this.w_ECTIPAMM = "I"
                this.w_ECPERORD = this.w_CCCOECIV
              case this.w_CCFLAMCI="D"
                * --- Diverso
                this.w_ECTIPAMM = "D"
                this.w_ECPERORD = this.w_CCPERCIV
            endcase
            this.w_TOTPEORD = this.w_ECPERORD
          case this.w_CONTA=this.w_CCDURCES
            * --- Devo verificare che la somma degli ammortamenti non superi il 100%
            if this.w_TOTPEORD>=100
              exit
            else
              * --- Se mi trovo nell'ultimo esercizio il tipo di ammortamento sar� valorizzato
              *     a 'Intero' ma il coefficiente sar� dato dalla differenza tra 100 ed 
              *     il totale dei coefficienti inseriti
              this.w_ECTIPAMM = "I"
              this.w_ECPERORD = 100-this.w_TOTPEORD
            endif
          case this.w_CONTA>1 AND this.w_CONTA<this.w_CCDURCES
            * --- Devo verificare che la somma degli ammortamenti non superi il 100%
            if this.w_TOTPEORD>=100
              exit
            else
              * --- Se mi trovo in un esercizio intermedio il tipo di ammortamento sar� valorizzato
              *     a 'Ordinario'
              this.w_ECTIPAMM = "I"
              this.w_ECPERORD = this.w_CCCOECIV
              this.w_APPO = this.w_TOTPEORD
              this.w_TOTPEORD = this.w_TOTPEORD+this.w_ECPERORD
              * --- Verifico che non superi il 100% con la quota dell'esercizio in corso
              if this.w_TOTPEORD>100
                this.w_ECPERORD = 100-this.w_APPO
              endif
            endif
        endcase
        * --- Insert into CAT_AMMC
        i_nConn=i_TableProp[this.CAT_AMMC_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAT_AMMC_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CAT_AMMC_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"ECCODCAT"+",ECPROESE"+",ECTIPAMM"+",ECPERORD"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_CCCODICE),'CAT_AMMC','ECCODCAT');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CONTA),'CAT_AMMC','ECPROESE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ECTIPAMM),'CAT_AMMC','ECTIPAMM');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ECPERORD),'CAT_AMMC','ECPERORD');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'ECCODCAT',this.w_CCCODICE,'ECPROESE',this.w_CONTA,'ECTIPAMM',this.w_ECTIPAMM,'ECPERORD',this.w_ECPERORD)
          insert into (i_cTable) (ECCODCAT,ECPROESE,ECTIPAMM,ECPERORD &i_ccchkf. );
             values (;
               this.w_CCCODICE;
               ,this.w_CONTA;
               ,this.w_ECTIPAMM;
               ,this.w_ECPERORD;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        this.w_CONTA = this.w_CONTA+1
      enddo
        select _Curs_CAT_CESP
        continue
      enddo
      use
    endif
    this.w_TMPC = AH_MsgFormat("Aggiornamento tabella esercizi/ammortamento civile eseguito con successo.%0")
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = this.w_TMPC
    * --- commit
    cp_EndTrs(.t.)
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='CAT_CESP'
    this.cWorkTables[2]='CAT_AMMC'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_CAT_CESP')
      use in _Curs_CAT_CESP
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
