* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcc30                                                        *
*              Manutenzione indici documenti                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2013-05-08                                                      *
* Last revis.: 2013-05-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcc30",oParentObject,m.w_NHF,m.pOper)
return(i_retval)

define class tgscvcc30 as StdBatch
  * --- Local variables
  w_NHF = 0
  pOper = space(10)
  w_IDTIPFIL = space(10)
  w_IDNOMFIL = space(200)
  w_IDORIGIN = space(200)
  w_IDORIFIL = space(200)
  w_IDWEBFIL = space(200)
  w_IDSERIAL = space(20)
  w_IDCLADOC = space(15)
  w_IDMODALL = space(1)
  w_IDWEBRET = space(1)
  w_IDDATRAG = ctod("  /  /  ")
  w_IDPATALL = space(200)
  w_PATSTD = space(200)
  w_TIPRAG = space(1)
  w_NEWFILE = space(15)
  w_TMPC = space(100)
  w_TmpPat = space(100)
  w_COPATHDO = space(200)
  w_FLORIFIL = space(1)
  w_FLORIGIN = space(1)
  w_FLNOMFIL = space(1)
  w_FLWEBFIL = space(1)
  w_TEMP = space(10)
  w_WRLOG = .f.
  w_NRECSEL = 0
  w_NRECUPD = 0
  w_oMess = .NULL.
  w_oPart = .NULL.
  w_RESFILE = space(100)
  w_LENEXT = 0
  * --- WorkFile variables
  PRODINDI_idx=0
  CONTROPA_idx=0
  PROMINDI_idx=0
  runtime_filters = 2

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Procedura di conversione per verificare che i campi IDORIGIN, IDORIFIL e IDWEBFIB
    *     sono compatibili con l'estensione file (manca il . prima dell'estensione)
    *     La procedurra, se necessario, rinomina anche il file collegato a IDWEBFIB
    * --- FIle di LOG
    * --- Oggetto per messaggi incrementali
    do case
      case empty(this.pOper)
        * --- Chiamato dalla gestione delle procedure di conversione
        this.oParentObject.w_PESEOK = .T.
        this.w_TMPC = ah_msgformat("Verifica modifica estensioni errate in indici documenti")
        this.oParentObject.w_pMSG = this.w_TMPC
        if this.w_NHF>=0
          this.oParentObject.w_pMSG = this.w_TMPC
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
        vq_exec("..\pcon\exe\query\gscvck30", this, "gscvcc30")
        this.w_NRECSEL = RecCount("gscvcc30")
        use in gscvcc30
        if this.w_NRECSEL>0
          do GSCVCK30 with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_TMPC = ah_msgformat("Conversione eseguita correttamente, leggere il log per visualizzare il dettaglio")
        endif
        this.w_oMess=createobject("Ah_Message")
        * --- Select from PROMINDI
        i_nConn=i_TableProp[this.PROMINDI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select IDSERIAL,IDORIGIN,IDORIFIL,IDNOMFIL  from "+i_cTable+" PROMINDI ";
              +" where IDMODALL='L' AND   UPPER(IDNOMFIL)<>UPPER(IDORIGIN)";
               ,"_Curs_PROMINDI")
        else
          select IDSERIAL,IDORIGIN,IDORIFIL,IDNOMFIL from (i_cTable);
           where IDMODALL="L" AND   UPPER(IDNOMFIL)<>UPPER(IDORIGIN);
            into cursor _Curs_PROMINDI
        endif
        if used('_Curs_PROMINDI')
          select _Curs_PROMINDI
          locate for 1=1
          do while not(eof())
          this.w_IDSERIAL = _Curs_PROMINDI.IDSERIAL
          this.w_IDORIGIN = _Curs_PROMINDI.IDORIGIN
          this.w_IDORIFIL = _Curs_PROMINDI.IDORIFIL
          if UPPER(ALLTRIM(this.w_IDORIGIN))=UPPER(JUSTFNAME(this.w_IDORIFIL))
            this.w_oPart = this.w_oMess.AddMsgPartNL("Aggiornamento indice %1")
            this.w_oPart.AddParam(this.w_IDSERIAL)     
            if FILE(this.w_IDORIFIL)
              * --- Write into PROMINDI
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.PROMINDI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.PROMINDI_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"IDNOMFIL ="+cp_NullLink(cp_ToStrODBC(this.w_IDORIGIN),'PROMINDI','IDNOMFIL');
                    +i_ccchkf ;
                +" where ";
                    +"IDSERIAL = "+cp_ToStrODBC(this.w_IDSERIAL);
                       )
              else
                update (i_cTable) set;
                    IDNOMFIL = this.w_IDORIGIN;
                    &i_ccchkf. ;
                 where;
                    IDSERIAL = this.w_IDSERIAL;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
              this.w_oMess.AddMsgPartNL("Aggiornato campo IDNOMFIL")     
              this.w_oPart = this.w_oMess.AddMsgPartNL("   vecchio valore= <%1>")
              this.w_oPart.AddParam(alltrim(_Curs_PROMINDI.IDNOMFIL))     
              this.w_oPart = this.w_oMess.AddMsgPartNL("   nuovo valore= <%1>")
              this.w_oPart.AddParam(alltrim(this.w_IDORIGIN))     
              this.w_WRLOG = True
            else
              this.w_oPart = this.w_oMess.AddMsgPartNL("   file <%1> non trovato")
              this.w_oPart.AddParam(this.w_IDORIFIL)     
            endif
          endif
            select _Curs_PROMINDI
            continue
          enddo
          use
        endif
        if this.w_WRLOG
          this.w_TMPC = this.w_oMess.ComposeMessage()
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        else
          if this.w_NRECSEL=0
            this.w_TMPC = ah_msgformat("Conversione eseguita correttamente, nessun problema rilevato")
            this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
          endif
        endif
        this.oParentObject.w_pMSG = ah_msgformat("%1%0%2",this.oParentObject.w_pMSG,this.w_TMPC)
      case this.pOper="RICALCOLA"
        * --- chiamato da maschera GSCVCK30
        Select (this.oParentObject.w_SZOOM.cCursor)
        scan
        this.w_IDTIPFIL = upper(alltrim(IDTIPFIL))
        if this.w_IDTIPFIL="P7M"
          this.w_IDORIFIL = this.Page_2(upper(alltrim(IDORIFIL)), this.w_IDTIPFIL)
          replace IDORIFIL1 with this.w_IDORIFIL
        else
          this.w_IDORIGIN = this.Page_2(upper(alltrim(IDORIGIN)), this.w_IDTIPFIL)
          this.w_IDORIFIL = this.Page_2(upper(alltrim(IDORIFIL)), this.w_IDTIPFIL)
          this.w_IDWEBFIL = this.Page_2(upper(alltrim(IDWEBFIL)), this.w_IDTIPFIL)
          replace IDORIGIN1 with this.w_IDORIGIN, IDORIFIL1 with this.w_IDORIFIL, IDWEBFIL1 with this.w_IDWEBFIL
        endif
        endscan
      case this.pOper="SALVA"
        * --- chiamato da maschera GSCVCK30
        if EMPTY(this.w_COPATHDO)
          * --- Read from CONTROPA
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CONTROPA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTROPA_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "COPATHDO"+;
              " from "+i_cTable+" CONTROPA where ";
                  +"COCODAZI = "+cp_ToStrODBC(i_CODAZI);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              COPATHDO;
              from (i_cTable) where;
                  COCODAZI = i_CODAZI;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_COPATHDO = NVL(cp_ToDate(_read_.COPATHDO),cp_NullValue(_read_.COPATHDO))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        Select (this.oParentObject.w_SZOOM.cCursor)
        SUM XCHK TO this.w_NRECSEL
        if this.w_NRECSEL=0
          ah_errormsg("Nessun record selezionato")
        else
          scan for xchk=1
          this.w_IDSERIAL = IDSERIAL
          this.w_IDCLADOC = IDCLADOC
          this.w_IDMODALL = IDMODALL
          this.w_IDTIPFIL = upper(alltrim(IDTIPFIL))
          this.w_IDDATRAG = IDDATRAG
          this.w_IDPATALL = NVL(IDPATALL,"")
          this.w_IDWEBRET = NVL(IDWEBRET," ")
          this.w_PATSTD = NVL(CDPATSTD,"")
          this.w_TIPRAG = NVL(CDTIPRAG,"")
          this.w_FLORIFIL = IIF(this.oParentObject.w_AORIFIL = "S" AND IDORIFIL<>IDORIFIL1, "=", " ")
          this.w_FLORIGIN = IIF(this.oParentObject.w_AORIGIN = "S" AND IDORIGIN<>IDORIGIN1, "=", " ")
          this.w_FLNOMFIL = IIF(this.w_IDTIPFIL="P7M", this.w_FLORIFIL, " ")
          * --- Oggetto per messaggi incrementali
          this.w_WRLOG = False
          this.w_oMess=createobject("Ah_Message")
          this.w_oPart = this.w_oMess.AddMsgPartNL("Aggiornamento indice %1")
          this.w_oPart.AddParam(this.w_IDSERIAL)     
          if this.w_FLORIGIN = "=" OR this.w_FLORIFIL = "="
            this.w_IDORIGIN = alltrim(IDORIGIN1)
            this.w_IDORIFIL = alltrim(IDORIFIL1)
            this.w_IDNOMFIL = IIF(this.w_FLNOMFIL= "=", this.w_IDORIFIL, IDNOMFIL)
            if this.w_FLNOMFIL = " " AND IDNOMFIL<>this.w_IDORIGIN and this.w_IDMODALL="L" AND ; 
 UPPER(ALLTRIM(this.w_IDORIGIN))=UPPER(JUSTFNAME(this.w_IDORIFIL)) AND FILE(this.w_IDORIFIL)
              this.w_IDNOMFIL = this.w_IDORIGIN
              this.w_FLNOMFIL = "="
            endif
            * --- Write into PROMINDI
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.PROMINDI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2])
              i_cOp1=cp_SetTrsOp(this.w_FLORIGIN,'IDORIGIN','this.w_IDORIGIN',this.w_IDORIGIN,'update',i_nConn)
              i_cOp2=cp_SetTrsOp(this.w_FLORIFIL,'IDORIFIL','this.w_IDORIFIL',this.w_IDORIFIL,'update',i_nConn)
              i_cOp3=cp_SetTrsOp(this.w_FLNOMFIL,'IDNOMFIL','this.w_IDNOMFIL',this.w_IDNOMFIL,'update',i_nConn)
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.PROMINDI_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"IDORIGIN ="+cp_NullLink(i_cOp1,'PROMINDI','IDORIGIN');
              +",IDORIFIL ="+cp_NullLink(i_cOp2,'PROMINDI','IDORIFIL');
              +",IDNOMFIL ="+cp_NullLink(i_cOp3,'PROMINDI','IDNOMFIL');
                  +i_ccchkf ;
              +" where ";
                  +"IDSERIAL = "+cp_ToStrODBC(this.w_IDSERIAL);
                     )
            else
              update (i_cTable) set;
                  IDORIGIN = &i_cOp1.;
                  ,IDORIFIL = &i_cOp2.;
                  ,IDNOMFIL = &i_cOp3.;
                  &i_ccchkf. ;
               where;
                  IDSERIAL = this.w_IDSERIAL;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            this.w_NRECUPD = this.w_NRECUPD+1
            if this.w_FLORIGIN = "="
              this.w_oMess.AddMsgPartNL("Aggiornato campo IDORIGIN")     
              this.w_oPart = this.w_oMess.AddMsgPartNL("   vecchio valore= <%1>")
              this.w_oPart.AddParam(alltrim(IDORIGIN))     
              this.w_oPart = this.w_oMess.AddMsgPartNL("   nuovo valore= <%1>")
              this.w_oPart.AddParam(this.w_IDORIGIN)     
            endif
            if this.w_FLORIFIL = "="
              this.w_oMess.AddMsgPartNL("Aggiornato campo IDORIFIL")     
              this.w_oPart = this.w_oMess.AddMsgPartNL("   vecchio valore= <%1>")
              this.w_oPart.AddParam(alltrim(IDORIFIL))     
              this.w_oPart = this.w_oMess.AddMsgPartNL("   nuovo valore= <%1>")
              this.w_oPart.AddParam(this.w_IDORIFIL)     
            endif
            if this.w_FLNOMFIL = "="
              this.w_oMess.AddMsgPartNL("Aggiornato campo IDNOMFIL")     
              this.w_oPart = this.w_oMess.AddMsgPartNL("   vecchio valore= <%1>")
              this.w_oPart.AddParam(alltrim(IDNOMFIL))     
              this.w_oPart = this.w_oMess.AddMsgPartNL("   nuovo valore= <%1>")
              this.w_oPart.AddParam(this.w_IDNOMFIL)     
            endif
            this.w_WRLOG = True
          endif
          if this.oParentObject.w_AWEBFIL = "S" and IDWEBFIL<>IDWEBFIL1 and not empty(IDWEBFIL1)
            this.w_IDORIGIN = alltrim(IDORIGIN)
            this.w_IDORIFIL = alltrim(IDORIFIL)
            this.w_IDWEBFIL = alltrim(IDWEBFIL)
            if this.w_IDWEBRET<>"S"
              this.w_TmpPat = ADDBS(alltrim(this.w_COPATHDO))+JUSTFNAME(alltrim(this.w_IDWEBFIL))
            else
              this.w_TmpPat = ""
            endif
            this.w_TmpPat = alltrim(this.w_TmpPat)
            this.w_IDWEBFIL = alltrim(IDWEBFIL1)
            this.w_NEWFILE = addbs(justpath(this.w_TmpPat)) + this.w_IDWEBFIL
            if not empty(this.w_TMPPAT)
              this.w_WRLOG = True
              if file(this.w_TMPPAT)
                w_ERRORE = .F.
                if this.w_TmpPat<>this.w_NEWFILE
                  * --- rinomino il ifle
                  w_ErrorHandler = on("ERROR")
                  on error w_ERRORE = .T.
                  * --- se non si mette il . alla fine forza l'estensione <dbf>
                  rename (this.w_TmpPat+".") to (this.w_NEWFILE)
                  on error &w_ErrorHandler
                endif
                if w_ERRORE or not cp_fileexist(this.w_NEWFILE)
                  this.w_oPart = this.w_oMess.AddMsgPartNL("Impossibile rinominare il file <%1>")
                  this.w_oPart.AddParam(this.w_TmpPat)     
                  this.w_oPart = this.w_oMess.AddMsgPartNL("in <%1>")
                  this.w_oPart.AddParam(this.w_NEWFILE)     
                else
                  * --- Write into PROMINDI
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.PROMINDI_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2])
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.PROMINDI_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"IDWEBFIL ="+cp_NullLink(cp_ToStrODBC(this.w_IDWEBFIL),'PROMINDI','IDWEBFIL');
                        +i_ccchkf ;
                    +" where ";
                        +"IDSERIAL = "+cp_ToStrODBC(this.w_IDSERIAL);
                           )
                  else
                    update (i_cTable) set;
                        IDWEBFIL = this.w_IDWEBFIL;
                        &i_ccchkf. ;
                     where;
                        IDSERIAL = this.w_IDSERIAL;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error=MSG_WRITE_ERROR
                    return
                  endif
                  if empty(this.w_FLORIGIN+this.w_FLORIFIL)
                    this.w_NRECUPD = this.w_NRECUPD+1
                  endif
                  this.w_oMess.AddMsgPartNL("Aggiornato campo IDWEBFIL")     
                  this.w_oPart = this.w_oMess.AddMsgPartNL("   vecchio valore= <%1>")
                  this.w_oPart.AddParam(alltrim(IDWEBFIL))     
                  this.w_oPart = this.w_oMess.AddMsgPartNL("   nuovo valore= <%1>")
                  this.w_oPart.AddParam(this.w_IDWEBFIL)     
                  if this.w_TmpPat<>this.w_NEWFILE
                    this.w_oPart = this.w_oMess.AddMsgPartNL("Rinominato il file <%1>")
                    this.w_oPart.AddParam(this.w_TmpPat)     
                    this.w_oPart = this.w_oMess.AddMsgPartNL("in <%1>")
                    this.w_oPart.AddParam(this.w_NEWFILE)     
                  endif
                endif
              else
                this.w_oPart = this.w_oMess.AddMsgPartNL("File <%1> non trovato")
                this.w_oPart.AddParam(this.w_TmpPat)     
              endif
            endif
          endif
          this.w_TMPC = this.w_oMess.ComposeMessage()
          if this.w_WRLOG
            this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
          endif
          endscan
          ah_errormsg("Aggionati %1 record di %2 selezionati",,, alltrim(str(this.w_NRECUPD)), alltrim(str(this.w_NRECSEL)))
          this.oParentObject.NotifyEvent("Interrroga")
          Select (this.oParentObject.w_SZOOM.cCursor)
          this.w_NRECSEL = RecCount(this.oParentObject.w_SZOOM.cCursor)
          go top
          if this.w_NRECSEL=0
            this.oParentObject.ecpQuit()
          endif
        endif
    endcase
  endproc


  procedure Page_2
    param cFILENAME,cEXT
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_LENEXT = len(m.cEXT)
    this.w_RESFILE = m.cFILENAME
    if not empty(m.cFILENAME) and not empty(m.cEXT) and RIGHT(this.w_RESFILE, this.w_LENEXT+1)<>"."+m.cEXT
      if RIGHT(this.w_RESFILE, 2*this.w_LENEXT) = m.cEXT+m.cEXT
        this.w_RESFILE = LEFT(this.w_RESFILE, LEN(this.w_RESFILE)-2*this.w_LENEXT) + "." + m.cEXT
      else
        this.w_RESFILE = LEFT(this.w_RESFILE, LEN(this.w_RESFILE)-this.w_LENEXT) + "." + m.cEXT
      endif
    endif
    return(this.w_RESFILE)
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,w_NHF,pOper)
    this.w_NHF=w_NHF
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='PRODINDI'
    this.cWorkTables[2]='CONTROPA'
    this.cWorkTables[3]='PROMINDI'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_PROMINDI')
      use in _Curs_PROMINDI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF,pOper"
endproc
