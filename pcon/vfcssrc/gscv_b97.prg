* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscv_b97                                                        *
*              Aggiornamento CIN EUR                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_8]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-04-14                                                      *
* Last revis.: 2003-04-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscv_b97",oParentObject,m.w_NHF)
return(i_retval)

define class tgscv_b97 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_CINCAB = space(1)
  * --- WorkFile variables
  BAN_CHE_idx=0
  COC_MAST_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- FIle di LOG
    * --- Try
    local bErr_037C4F00
    bErr_037C4F00=bTrsErr
    this.Try_037C4F00()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE - impossibile aggiornare BACINEUR su tabelle BAN_CHE/COC_MAST")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_037C4F00
    * --- End
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc
  proc Try_037C4F00()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Aggiornamento  BACINEUR
    * --- Select from BAN_CHE
    i_nConn=i_TableProp[this.BAN_CHE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.BAN_CHE_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" BAN_CHE ";
           ,"_Curs_BAN_CHE")
    else
      select * from (i_cTable);
        into cursor _Curs_BAN_CHE
    endif
    if used('_Curs_BAN_CHE')
      select _Curs_BAN_CHE
      locate for 1=1
      do while not(eof())
      this.w_CINCAB = _Curs_BAN_CHE.BACINCAB
      * --- Write into BAN_CHE
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.BAN_CHE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.BAN_CHE_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.BAN_CHE_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"BACINEUR ="+cp_NullLink(cp_ToStrODBC(this.w_CINCAB),'BAN_CHE','BACINEUR');
            +i_ccchkf ;
        +" where ";
            +"BACODBAN = "+cp_ToStrODBC(_Curs_BAN_CHE.BACODBAN);
               )
      else
        update (i_cTable) set;
            BACINEUR = this.w_CINCAB;
            &i_ccchkf. ;
         where;
            BACODBAN = _Curs_BAN_CHE.BACODBAN;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
        select _Curs_BAN_CHE
        continue
      enddo
      use
    endif
    * --- Select from COC_MAST
    i_nConn=i_TableProp[this.COC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.COC_MAST_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" COC_MAST ";
           ,"_Curs_COC_MAST")
    else
      select * from (i_cTable);
        into cursor _Curs_COC_MAST
    endif
    if used('_Curs_COC_MAST')
      select _Curs_COC_MAST
      locate for 1=1
      do while not(eof())
      this.w_CINCAB = _Curs_COC_MAST.BACINCAB
      * --- Write into COC_MAST
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.COC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.COC_MAST_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.COC_MAST_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"BACINEUR ="+cp_NullLink(cp_ToStrODBC(this.w_CINCAB),'COC_MAST','BACINEUR');
            +i_ccchkf ;
        +" where ";
            +"BACODBAN = "+cp_ToStrODBC(_Curs_COC_MAST.BACODBAN);
               )
      else
        update (i_cTable) set;
            BACINEUR = this.w_CINCAB;
            &i_ccchkf. ;
         where;
            BACODBAN = _Curs_COC_MAST.BACODBAN;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
        select _Curs_COC_MAST
        continue
      enddo
      use
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Aggiornamento campo BACINEUR su tabelle BAN_CHE/COC_MAST eseguito con successo")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = this.oParentObject.w_PMSG + this.w_TMPC 
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiornamento Tabella COC_MAST
    * --- Try
    local bErr_037C0FA0
    bErr_037C0FA0=bTrsErr
    this.Try_037C0FA0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE - impossibile aggiornare BACINEUR su tabelle COC_MAST")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_037C0FA0
    * --- End
  endproc
  proc Try_037C0FA0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Aggiornamento  BACINEUR
    * --- Select from COC_MAST
    i_nConn=i_TableProp[this.COC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.COC_MAST_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" COC_MAST ";
           ,"_Curs_COC_MAST")
    else
      select * from (i_cTable);
        into cursor _Curs_COC_MAST
    endif
    if used('_Curs_COC_MAST')
      select _Curs_COC_MAST
      locate for 1=1
      do while not(eof())
      this.w_CINCAB = _Curs_COC_MAST.BACINCAB
      * --- Write into COC_MAST
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.COC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.COC_MAST_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.COC_MAST_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"BACINEUR ="+cp_NullLink(cp_ToStrODBC(this.w_CINCAB),'COC_MAST','BACINEUR');
            +i_ccchkf ;
        +" where ";
            +"BACODBAN = "+cp_ToStrODBC(_Curs_COC_MAST.BACODBAN);
               )
      else
        update (i_cTable) set;
            BACINEUR = this.w_CINCAB;
            &i_ccchkf. ;
         where;
            BACODBAN = _Curs_COC_MAST.BACODBAN;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
        select _Curs_COC_MAST
        continue
      enddo
      use
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Aggiornamento campo BACINEUR su tabelle COC_MAST eseguito con successo")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = this.oParentObject.w_PMSG + this.w_TMPC 
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='BAN_CHE'
    this.cWorkTables[2]='COC_MAST'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_BAN_CHE')
      use in _Curs_BAN_CHE
    endif
    if used('_Curs_COC_MAST')
      use in _Curs_COC_MAST
    endif
    if used('_Curs_COC_MAST')
      use in _Curs_COC_MAST
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
