* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscv_bri                                                        *
*              Ricostruisce integrita referenziali                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_10]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-01-12                                                      *
* Last revis.: 2008-12-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_TABLE,w_CONN,bChiudiMsg
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscv_bri",oParentObject,m.w_TABLE,m.w_CONN,m.bChiudiMsg)
return(i_retval)

define class tgscv_bri as StdBatch
  * --- Local variables
  w_TABLE = space(200)
  w_CONN = 0
  bChiudiMsg = .f.
  w_LOOP = 0
  w_LTABLE = space(200)
  w_LCPHTABLE = space(200)
  w_OK = 0
  * --- WorkFile variables
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Dato il nome logico e la connessione di una tabella
    *     ricostruisce le integritÓ referenziali per gli archivi che la puntano.
    *     Es. ricostruiendo CONTI ricrea l'intergritÓ referenziale tra DOC_MAST e CONTI
    =Cp_ReadXdc()
     
 Local cMsg,xServers[1,3],nServers 
 cMsg=""
    this.w_LOOP = 1
    if Type("msgwnd.caption")="U"
       
 public msgwnd 
 msgwnd=createobject("mkdbmsg") 
 msgwnd.show()
    endif
     
 sqlexec(i_ServerConn[1,2],"select * from cpazi where codazi="+cp_ToStrODBC(i_codazi),"_azi_")
    do while this.w_LOOP<=i_Dcx.GetIFKCount( this.w_TABLE )
      this.w_LTABLE = I_DCX.GetIFKTable( this.w_TABLE , this.w_LOOP )
      this.w_LCPHTABLE = I_DCX.GetPhTable(I_DCX.GetTableIdx( this.w_LTABLE ), "xxx" )
      this.w_OK = db_CreateTableStruct( this.w_LTABLE , this.w_LCPHTABLE , "" , this.w_Conn , i_DCX , @cMsg,"index",@xServers,@nServers )
      this.w_LOOP = this.w_LOOP + 1 
    enddo
    msgwnd.End(ah_Msgformat("IntegritÓ referenziali verso %1 ricostruite", this.w_TABLE))
    if Empty ( msgwnd.edt_err.value ) Or this.bChiudiMsg
      * --- Se non ho errori chiudo la maschera
      msgwnd.btnok.click()
    endif
    if used("_azi_")
       
 select _azi_ 
 use
    endif
  endproc


  proc Init(oParentObject,w_TABLE,w_CONN,bChiudiMsg)
    this.w_TABLE=w_TABLE
    this.w_CONN=w_CONN
    this.bChiudiMsg=bChiudiMsg
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_TABLE,w_CONN,bChiudiMsg"
endproc
