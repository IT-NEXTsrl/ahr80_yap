* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcb67                                                        *
*              Modifica classe documentale GESTFILE_DO (attributo su intestatario) *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-01-14                                                      *
* Last revis.: 2011-08-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcb67",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcb67 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TMPC = space(10)
  w_TEMP = space(10)
  w_MESS = space(10)
  w_SERIALE = space(10)
  w_CODCLAS = space(15)
  w_CODATT = space(15)
  w_DESATT = space(40)
  w_CAMCUR = space(20)
  w_NOMTAB = space(20)
  w_INTESTATARIO = space(21)
  w_IDSERIAL = space(20)
  * --- WorkFile variables
  PRODINDI_idx=0
  PROMINDI_idx=0
  PRODCLAS_idx=0
  DOC_MAST_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Modifica classe documentale GESTFILE_DO per aggiunta nuovo attributo su intestatario, inserimento e valorizzazione dello stesso negli 
    *     indici inerenti alla classe documentale stessa.
    if g_CPIN#"S" AND g_DMIP#"S"
      this.w_TMPC = ah_Msgformat("Profilo Infinity non attivato, la procedura non sar� eseguita.")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      this.oParentObject.w_PMSG = ah_Msgformat("Conversione non eseguita perch� non � attivo il profilo Infinity")
      this.oParentObject.w_PESEOK = .T.
      i_retcode = 'stop'
      return
    endif
    * --- Try
    local bErr_03798430
    bErr_03798430=bTrsErr
    this.Try_03798430()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE GENERICO - impossibile aggiungere l'attributo su intestatario documento nella classe GESTFILE_DO")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_03798430
    * --- End
  endproc
  proc Try_03798430()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Inizializzo le variabili per l'inserimento del nuovo attributo
    this.w_CODCLAS = "GESTFILE_DO"
    this.w_CODATT = "INTESTATARIO"
    this.w_DESATT = "INTESTATARIO"
    this.w_CAMCUR = "MVTIPCON+MVCODCON"
    this.w_NOMTAB = "CONTI"
    * --- Inserisco il nuovo attributo nel dettaglio della classe documentale GESTFILE_DO
    * --- Try
    local bErr_037548E0
    bErr_037548E0=bTrsErr
    this.Try_037548E0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
      * --- Read from PRODCLAS
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PRODCLAS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PRODCLAS_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CDCODATT"+;
          " from "+i_cTable+" PRODCLAS where ";
              +"CDCODCLA = "+cp_ToStrODBC(this.w_CODCLAS);
              +" and CPROWNUM = "+cp_ToStrODBC(2);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CDCODATT;
          from (i_cTable) where;
              CDCODCLA = this.w_CODCLAS;
              and CPROWNUM = 2;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CODATT = NVL(cp_ToDate(_read_.CDCODATT),cp_NullValue(_read_.CDCODATT))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if not(i_rows = 1 and this.w_CODATT = "INTESTATARIO")
        * --- transaction error
        bTrsErr=.t.
        i_TrsMsg="Impossibile aggiornare la classe documentale GESTFILE_DO"
        i_Error=i_TrsMsg
        return
      endif
    endif
    bTrsErr=bTrsErr or bErr_037548E0
    * --- End
    * --- Ciclo sugli indici relativi alla classe documentale GESTFILE_DO per aggiungere e valorizzare il nuovo attributo 'Intestatario'
    * --- Select from GSCVCB67
    do vq_exec with 'GSCVCB67',this,'_Curs_GSCVCB67','',.f.,.t.
    if used('_Curs_GSCVCB67')
      select _Curs_GSCVCB67
      locate for 1=1
      do while not(eof())
      this.w_IDSERIAL = _Curs_GSCVCB67.IDSERIAL
       ah_Msg("Modifico indice: %1",.T.,.F.,.F., ALLTRIM(this.w_IDSERIAL))
      this.w_SERIALE = _Curs_GSCVCB67.IDVALATT 
      this.w_INTESTATARIO = _Curs_GSCVCB67.MVTIPCON+alltrim(_Curs_GSCVCB67.MVCODCON)
      * --- Aggiungo riga dettaglio inerente al nuovo attributo 'Intestatario'
      * --- Try
      local bErr_035FF160
      bErr_035FF160=bTrsErr
      this.Try_035FF160()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
        * --- Write into PRODINDI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.PRODINDI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PRODINDI_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.PRODINDI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"IDVALATT ="+cp_NullLink(cp_ToStrODBC(this.w_INTESTATARIO),'PRODINDI','IDVALATT');
          +",IDCODATT ="+cp_NullLink(cp_ToStrODBC(this.w_CODATT),'PRODINDI','IDCODATT');
          +",IDDESATT ="+cp_NullLink(cp_ToStrODBC(this.w_DESATT),'PRODINDI','IDDESATT');
          +",IDTABKEY ="+cp_NullLink(cp_ToStrODBC(this.w_NOMTAB),'PRODINDI','IDTABKEY');
          +",IDCHKOBB ="+cp_NullLink(cp_ToStrODBC("N"),'PRODINDI','IDCHKOBB');
          +",IDTIPATT ="+cp_NullLink(cp_ToStrODBC("C"),'PRODINDI','IDTIPATT');
              +i_ccchkf ;
          +" where ";
              +"IDSERIAL = "+cp_ToStrODBC(this.w_IDSERIAL);
              +" and CPROWNUM = "+cp_ToStrODBC(2);
                 )
        else
          update (i_cTable) set;
              IDVALATT = this.w_INTESTATARIO;
              ,IDCODATT = this.w_CODATT;
              ,IDDESATT = this.w_DESATT;
              ,IDTABKEY = this.w_NOMTAB;
              ,IDCHKOBB = "N";
              ,IDTIPATT = "C";
              &i_ccchkf. ;
           where;
              IDSERIAL = this.w_IDSERIAL;
              and CPROWNUM = 2;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
      bTrsErr=bTrsErr or bErr_035FF160
      * --- End
        select _Curs_GSCVCB67
        continue
      enddo
      use
    endif
    * --- Esecuzione ok
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Attributo su intestatario documento aggiunto alla classe GESTFILE_DO")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = ah_Msgformat("Conversione eseguita correttamente")
    return
  proc Try_037548E0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PRODCLAS
    i_nConn=i_TableProp[this.PRODCLAS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRODCLAS_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PRODCLAS_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CDCODCLA"+",CPROWNUM"+",CPROWORD"+",CDCODATT"+",CDDESATT"+",CDCHKOBB"+",CDTIPATT"+",CDVLPRED"+",CDCAMCUR"+",CDTABKEY"+",CDNATAWE"+",CDKEYAWE"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CODCLAS),'PRODCLAS','CDCODCLA');
      +","+cp_NullLink(cp_ToStrODBC(2),'PRODCLAS','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(20),'PRODCLAS','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODATT),'PRODCLAS','CDCODATT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DESATT),'PRODCLAS','CDDESATT');
      +","+cp_NullLink(cp_ToStrODBC("N"),'PRODCLAS','CDCHKOBB');
      +","+cp_NullLink(cp_ToStrODBC("C"),'PRODCLAS','CDTIPATT');
      +","+cp_NullLink(cp_ToStrODBC(" "),'PRODCLAS','CDVLPRED');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CAMCUR),'PRODCLAS','CDCAMCUR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NOMTAB),'PRODCLAS','CDTABKEY');
      +","+cp_NullLink(cp_ToStrODBC(" "),'PRODCLAS','CDNATAWE');
      +","+cp_NullLink(cp_ToStrODBC(" "),'PRODCLAS','CDKEYAWE');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CDCODCLA',this.w_CODCLAS,'CPROWNUM',2,'CPROWORD',20,'CDCODATT',this.w_CODATT,'CDDESATT',this.w_DESATT,'CDCHKOBB',"N",'CDTIPATT',"C",'CDVLPRED'," ",'CDCAMCUR',this.w_CAMCUR,'CDTABKEY',this.w_NOMTAB,'CDNATAWE'," ",'CDKEYAWE'," ")
      insert into (i_cTable) (CDCODCLA,CPROWNUM,CPROWORD,CDCODATT,CDDESATT,CDCHKOBB,CDTIPATT,CDVLPRED,CDCAMCUR,CDTABKEY,CDNATAWE,CDKEYAWE &i_ccchkf. );
         values (;
           this.w_CODCLAS;
           ,2;
           ,20;
           ,this.w_CODATT;
           ,this.w_DESATT;
           ,"N";
           ,"C";
           ," ";
           ,this.w_CAMCUR;
           ,this.w_NOMTAB;
           ," ";
           ," ";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_035FF160()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PRODINDI
    i_nConn=i_TableProp[this.PRODINDI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRODINDI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PRODINDI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"IDSERIAL"+",CPROWNUM"+",CPROWORD"+",IDVALATT"+",IDCODATT"+",IDTIPATT"+",IDCHKOBB"+",IDTABKEY"+",IDDESATT"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_IDSERIAL),'PRODINDI','IDSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(2),'PRODINDI','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(20),'PRODINDI','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_INTESTATARIO),'PRODINDI','IDVALATT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODATT),'PRODINDI','IDCODATT');
      +","+cp_NullLink(cp_ToStrODBC("C"),'PRODINDI','IDTIPATT');
      +","+cp_NullLink(cp_ToStrODBC("N"),'PRODINDI','IDCHKOBB');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NOMTAB),'PRODINDI','IDTABKEY');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DESATT),'PRODINDI','IDDESATT');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'IDSERIAL',this.w_IDSERIAL,'CPROWNUM',2,'CPROWORD',20,'IDVALATT',this.w_INTESTATARIO,'IDCODATT',this.w_CODATT,'IDTIPATT',"C",'IDCHKOBB',"N",'IDTABKEY',this.w_NOMTAB,'IDDESATT',this.w_DESATT)
      insert into (i_cTable) (IDSERIAL,CPROWNUM,CPROWORD,IDVALATT,IDCODATT,IDTIPATT,IDCHKOBB,IDTABKEY,IDDESATT &i_ccchkf. );
         values (;
           this.w_IDSERIAL;
           ,2;
           ,20;
           ,this.w_INTESTATARIO;
           ,this.w_CODATT;
           ,"C";
           ,"N";
           ,this.w_NOMTAB;
           ,this.w_DESATT;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='PRODINDI'
    this.cWorkTables[2]='PROMINDI'
    this.cWorkTables[3]='PRODCLAS'
    this.cWorkTables[4]='DOC_MAST'
    return(this.OpenAllTables(4))

  proc CloseCursors()
    if used('_Curs_GSCVCB67')
      use in _Curs_GSCVCB67
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
