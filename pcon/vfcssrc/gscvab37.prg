* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvab37                                                        *
*              Aggiorna campo cssegqta (criteri di elaborazione)               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_5]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-09-09                                                      *
* Last revis.: 2004-09-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvab37",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvab37 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_SEGNO = space(1)
  * --- WorkFile variables
  CAU_SELE_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- AGGIORNA CAMPO CSSEGQTA (Criteri di elaborazione)
    * --- FIle di LOG
    * --- Try
    local bErr_036090E0
    bErr_036090E0=bTrsErr
    this.Try_036090E0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE - Impossibile aggiornare CS_SEGNO su tabella CAU_SELE")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_036090E0
    * --- End
  endproc
  proc Try_036090E0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Aggiornamento  CS_SEGNO
    * --- Select from CAU_SELE
    i_nConn=i_TableProp[this.CAU_SELE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAU_SELE_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" CAU_SELE ";
           ,"_Curs_CAU_SELE")
    else
      select * from (i_cTable);
        into cursor _Curs_CAU_SELE
    endif
    if used('_Curs_CAU_SELE')
      select _Curs_CAU_SELE
      locate for 1=1
      do while not(eof())
      this.w_SEGNO = _Curs_CAU_SELE.CS_SEGNO
      * --- Write into CAU_SELE
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.CAU_SELE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAU_SELE_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.CAU_SELE_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"CSSEGQTA ="+cp_NullLink(cp_ToStrODBC(this.w_SEGNO),'CAU_SELE','CSSEGQTA');
            +i_ccchkf ;
        +" where ";
            +"CSCODICE = "+cp_ToStrODBC(_Curs_CAU_SELE.CSCODICE);
            +" and CPROWNUM = "+cp_ToStrODBC(_Curs_CAU_SELE.CPROWNUM);
               )
      else
        update (i_cTable) set;
            CSSEGQTA = this.w_SEGNO;
            &i_ccchkf. ;
         where;
            CSCODICE = _Curs_CAU_SELE.CSCODICE;
            and CPROWNUM = _Curs_CAU_SELE.CPROWNUM;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
        select _Curs_CAU_SELE
        continue
      enddo
      use
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Aggiornamento campo CS_SEGNO su tabella CAU_SELE eseguito con successo")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = this.oParentObject.w_PMSG + this.w_TMPC 
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='CAU_SELE'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_CAU_SELE')
      use in _Curs_CAU_SELE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
