* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvab94                                                        *
*              Aggiorna data cont. in mov.cesp                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_15]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-03-31                                                      *
* Last revis.: 2005-03-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvab94",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvab94 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_MESS = space(100)
  w_DATA = ctod("  /  /  ")
  w_NUMAGG = 0
  * --- WorkFile variables
  MOV_CESP_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questa procedura sbianca il campo di data contabilizzazione movimento cespite se 
    *     il seriale movimento non � presente in PTN_CESP e PNTSCESP
    * --- FIle di LOG
    this.w_DATA = cp_CharToDate("  -  -    ")
    if g_CESP="S"
      * --- Try
      local bErr_04A84950
      bErr_04A84950=bTrsErr
      this.Try_04A84950()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        * --- Gestisce log errori
        this.oParentObject.w_PMSG = Message()
        this.oParentObject.w_PESEOK = .F.
        if this.w_NHF>=0
          this.w_TMPC = ah_Msgformat("ERRORE GENERICO - impossibile aggiornare i movimenti cespiti")
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
      bTrsErr=bTrsErr or bErr_04A84950
      * --- End
      if used("CESP")
         
 select CESP 
 use
      endif
    else
      this.w_TMPC = ah_Msgformat("Cespiti non presenti")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
  endproc
  proc Try_04A84950()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    VQ_EXEC("GSCVAB94.VQR",this,"CESP")
    if RECCOUNT("CESP")>0
      * --- Write into MOV_CESP
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.MOV_CESP_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MOV_CESP_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="MCSERIAL"
        do vq_exec with 'GSCVAB94',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.MOV_CESP_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="MOV_CESP.MCSERIAL = _t2.MCSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MCDATCON ="+cp_NullLink(cp_ToStrODBC(this.w_DATA),'MOV_CESP','MCDATCON');
            +i_ccchkf;
            +" from "+i_cTable+" MOV_CESP, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="MOV_CESP.MCSERIAL = _t2.MCSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MOV_CESP, "+i_cQueryTable+" _t2 set ";
        +"MOV_CESP.MCDATCON ="+cp_NullLink(cp_ToStrODBC(this.w_DATA),'MOV_CESP','MCDATCON');
            +Iif(Empty(i_ccchkf),"",",MOV_CESP.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="MOV_CESP.MCSERIAL = t2.MCSERIAL";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MOV_CESP set (";
            +"MCDATCON";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +cp_NullLink(cp_ToStrODBC(this.w_DATA),'MOV_CESP','MCDATCON')+"";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="MOV_CESP.MCSERIAL = _t2.MCSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MOV_CESP set ";
        +"MCDATCON ="+cp_NullLink(cp_ToStrODBC(this.w_DATA),'MOV_CESP','MCDATCON');
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".MCSERIAL = "+i_cQueryTable+".MCSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MCDATCON ="+cp_NullLink(cp_ToStrODBC(this.w_DATA),'MOV_CESP','MCDATCON');
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      if bTrsErr
        * --- Raise
        i_Error=MESSAGE()
        return
      else
        Select CESP 
 scan
        this.w_NUMAGG = this.w_NUMAGG+1
        Endscan
      endif
    endif
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("- Aggiornamento tabella MOV_CESP eseguito con successo%0Variati %1 records%0", ALLTRIM(STR(this.w_NUMAGG)) )
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='MOV_CESP'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
