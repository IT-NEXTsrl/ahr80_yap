* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscv_b75                                                        *
*              Aggiornamento 1^UM in distinta base                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_467]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-07-31                                                      *
* Last revis.: 2008-04-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscv_b75",oParentObject,m.w_NHF)
return(i_retval)

define class tgscv_b75 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_COEUM1 = 0
  w_FLVARI = space(1)
  w_CODICE = space(20)
  w_CODART = space(20)
  w_UNIMIS = space(3)
  w_UNMIS1 = space(3)
  w_UNMIS2 = space(3)
  w_UNMIS3 = space(3)
  w_OPERAT = space(1)
  w_OPERA3 = space(1)
  w_MOLTIP = 0
  w_MOLTI3 = 0
  w_QTAMOV = 0
  w_DATINI = ctod("  /  /  ")
  w_DATFIN = ctod("  /  /  ")
  * --- WorkFile variables
  AZIENDA_idx=0
  DISTBASE_idx=0
  COM_VARI_idx=0
  ART_ICOL_idx=0
  KEY_ARTI_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- FIle di LOG
    * --- Test sul tipo Database
    * --- Try
    local bErr_035FA660
    bErr_035FA660=bTrsErr
    this.Try_035FA660()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = this.oParentObject.w_PMSG
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_035FA660
    * --- End
  endproc
  proc Try_035FA660()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Select from DISTBASE
    i_nConn=i_TableProp[this.DISTBASE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DISTBASE_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" DISTBASE ";
           ,"_Curs_DISTBASE")
    else
      select * from (i_cTable);
        into cursor _Curs_DISTBASE
    endif
    if used('_Curs_DISTBASE')
      select _Curs_DISTBASE
      locate for 1=1
      do while not(eof())
      this.w_CODICE = NVL(_Curs_DISTBASE.DBCODCOM,"")
      this.w_CODART = NVL(_Curs_DISTBASE.DBARTCOM,"")
      this.w_UNIMIS = NVL(_Curs_DISTBASE.DBUNIMIS, "   ")
      this.w_UNIMIS = LEFT(ALLTRIM(this.w_UNIMIS)+SPACE(3), 3)
      this.w_QTAMOV = NVL(_Curs_DISTBASE.DBQTAMOV, 0)
      this.w_DATINI = CP_TODATE(_Curs_DISTBASE.DBINIVAL)
      this.w_DATFIN = CP_TODATE(_Curs_DISTBASE.DBFINVAL)
      this.w_FLVARI = NVL(_Curs_DISTBASE.DBFLVARI, " ")
      this.w_DATINI = IIF(EMPTY(this.w_DATINI), i_INIDAT, this.w_DATINI)
      this.w_DATFIN = IIF(EMPTY(this.w_DATFIN), i_FINDAT, this.w_DATFIN)
      if this.w_FLVARI="S"
        * --- Write into DISTBASE
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.DISTBASE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DISTBASE_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.DISTBASE_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"DBINIVAL ="+cp_NullLink(cp_ToStrODBC(this.w_DATINI),'DISTBASE','DBINIVAL');
          +",DBFINVAL ="+cp_NullLink(cp_ToStrODBC(this.w_DATFIN),'DISTBASE','DBFINVAL');
              +i_ccchkf ;
          +" where ";
              +"DBCODICE = "+cp_ToStrODBC(_Curs_DISTBASE.DBCODICE);
              +" and CPROWNUM = "+cp_ToStrODBC(_Curs_DISTBASE.CPROWNUM);
                 )
        else
          update (i_cTable) set;
              DBINIVAL = this.w_DATINI;
              ,DBFINVAL = this.w_DATFIN;
              &i_ccchkf. ;
           where;
              DBCODICE = _Curs_DISTBASE.DBCODICE;
              and CPROWNUM = _Curs_DISTBASE.CPROWNUM;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      else
        if this.w_QTAMOV<>0 AND NOT EMPTY(this.w_UNIMIS) AND NOT EMPTY(this.w_CODART) AND NOT EMPTY(this.w_CODICE)
          this.w_UNMIS1 = "   "
          this.w_UNMIS2 = "   "
          this.w_UNMIS3 = "   "
          this.w_OPERAT = " "
          this.w_OPERA3 = " "
          this.w_MOLTIP = 0
          this.w_MOLTI3 = 0
          * --- Read from ART_ICOL
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ART_ICOL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ARUNMIS1,AROPERAT,ARMOLTIP,ARUNMIS2"+;
              " from "+i_cTable+" ART_ICOL where ";
                  +"ARCODART = "+cp_ToStrODBC(this.w_CODART);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ARUNMIS1,AROPERAT,ARMOLTIP,ARUNMIS2;
              from (i_cTable) where;
                  ARCODART = this.w_CODART;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_UNMIS1 = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
            this.w_OPERAT = NVL(cp_ToDate(_read_.AROPERAT),cp_NullValue(_read_.AROPERAT))
            this.w_MOLTIP = NVL(cp_ToDate(_read_.ARMOLTIP),cp_NullValue(_read_.ARMOLTIP))
            this.w_UNMIS2 = NVL(cp_ToDate(_read_.ARUNMIS2),cp_NullValue(_read_.ARUNMIS2))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Dimensiona per problemi di Passaggio Parametri alla CALMMLIS
          this.w_UNMIS1 = LEFT(ALLTRIM(this.w_UNMIS1)+SPACE(3), 3)
          this.w_UNMIS2 = LEFT(ALLTRIM(this.w_UNMIS2)+SPACE(3), 3)
          this.w_OPERAT = LEFT(ALLTRIM(this.w_OPERAT)+" ", 1)
          this.w_COEUM1 = this.w_QTAMOV
          if this.w_UNIMIS<>this.w_UNMIS1
            * --- Read from KEY_ARTI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2])
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "CAUNIMIS,CAOPERAT,CAMOLTIP"+;
                " from "+i_cTable+" KEY_ARTI where ";
                    +"CACODICE = "+cp_ToStrODBC(this.w_CODICE);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                CAUNIMIS,CAOPERAT,CAMOLTIP;
                from (i_cTable) where;
                    CACODICE = this.w_CODICE;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_UNMIS3 = NVL(cp_ToDate(_read_.CAUNIMIS),cp_NullValue(_read_.CAUNIMIS))
              this.w_OPERA3 = NVL(cp_ToDate(_read_.CAOPERAT),cp_NullValue(_read_.CAOPERAT))
              this.w_MOLTI3 = NVL(cp_ToDate(_read_.CAMOLTIP),cp_NullValue(_read_.CAMOLTIP))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.w_UNMIS3 = LEFT(ALLTRIM(this.w_UNMIS3)+SPACE(3), 3)
            this.w_OPERA3 = LEFT(ALLTRIM(this.w_OPERA3)+" ", 1)
            this.w_COEUM1 = CALQTA(this.w_QTAMOV,this.w_UNIMIS,this.w_UNMIS2,this.w_OPERAT, this.w_MOLTIP, " ", "N", " ", , this.w_UNMIS3, this.w_OPERA3, this.w_MOLTI3)
          endif
          * --- Write into DISTBASE
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.DISTBASE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DISTBASE_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.DISTBASE_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"DBCOEUM1 ="+cp_NullLink(cp_ToStrODBC(this.w_COEUM1),'DISTBASE','DBCOEUM1');
            +",DBINIVAL ="+cp_NullLink(cp_ToStrODBC(this.w_DATINI),'DISTBASE','DBINIVAL');
            +",DBFINVAL ="+cp_NullLink(cp_ToStrODBC(this.w_DATFIN),'DISTBASE','DBFINVAL');
                +i_ccchkf ;
            +" where ";
                +"DBCODICE = "+cp_ToStrODBC(_Curs_DISTBASE.DBCODICE);
                +" and CPROWNUM = "+cp_ToStrODBC(_Curs_DISTBASE.CPROWNUM);
                   )
          else
            update (i_cTable) set;
                DBCOEUM1 = this.w_COEUM1;
                ,DBINIVAL = this.w_DATINI;
                ,DBFINVAL = this.w_DATFIN;
                &i_ccchkf. ;
             where;
                DBCODICE = _Curs_DISTBASE.DBCODICE;
                and CPROWNUM = _Curs_DISTBASE.CPROWNUM;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      endif
        select _Curs_DISTBASE
        continue
      enddo
      use
    endif
    * --- Select from COM_VARI
    i_nConn=i_TableProp[this.COM_VARI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.COM_VARI_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" COM_VARI ";
           ,"_Curs_COM_VARI")
    else
      select * from (i_cTable);
        into cursor _Curs_COM_VARI
    endif
    if used('_Curs_COM_VARI')
      select _Curs_COM_VARI
      locate for 1=1
      do while not(eof())
      this.w_CODICE = NVL(_Curs_COM_VARI.CVCODCOM,"")
      this.w_CODART = NVL(_Curs_COM_VARI.CVARTCOM,"")
      this.w_UNIMIS = NVL(_Curs_COM_VARI.CVUNIMIS, "   ")
      this.w_UNIMIS = LEFT(ALLTRIM(this.w_UNIMIS)+SPACE(3), 3)
      this.w_QTAMOV = NVL(_Curs_COM_VARI.CVQTAMOV, 0)
      if this.w_QTAMOV<>0 AND NOT EMPTY(this.w_UNIMIS) AND NOT EMPTY(this.w_CODART) AND NOT EMPTY(this.w_CODICE)
        this.w_UNMIS1 = "   "
        this.w_UNMIS2 = "   "
        this.w_UNMIS3 = "   "
        this.w_OPERAT = " "
        this.w_OPERA3 = " "
        this.w_MOLTIP = 0
        this.w_MOLTI3 = 0
        * --- Read from ART_ICOL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ARUNMIS1,AROPERAT,ARMOLTIP,ARUNMIS2"+;
            " from "+i_cTable+" ART_ICOL where ";
                +"ARCODART = "+cp_ToStrODBC(this.w_CODART);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ARUNMIS1,AROPERAT,ARMOLTIP,ARUNMIS2;
            from (i_cTable) where;
                ARCODART = this.w_CODART;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_UNMIS1 = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
          this.w_OPERAT = NVL(cp_ToDate(_read_.AROPERAT),cp_NullValue(_read_.AROPERAT))
          this.w_MOLTIP = NVL(cp_ToDate(_read_.ARMOLTIP),cp_NullValue(_read_.ARMOLTIP))
          this.w_UNMIS2 = NVL(cp_ToDate(_read_.ARUNMIS2),cp_NullValue(_read_.ARUNMIS2))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Dimensiona per problemi di Passaggio Parametri alla CALMMLIS
        this.w_UNMIS1 = LEFT(ALLTRIM(this.w_UNMIS1)+SPACE(3), 3)
        this.w_UNMIS2 = LEFT(ALLTRIM(this.w_UNMIS2)+SPACE(3), 3)
        this.w_OPERAT = LEFT(ALLTRIM(this.w_OPERAT)+" ", 1)
        this.w_COEUM1 = this.w_QTAMOV
        if this.w_UNIMIS<>this.w_UNMIS1
          * --- Read from KEY_ARTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CAUNIMIS,CAOPERAT,CAMOLTIP"+;
              " from "+i_cTable+" KEY_ARTI where ";
                  +"CACODICE = "+cp_ToStrODBC(this.w_CODICE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CAUNIMIS,CAOPERAT,CAMOLTIP;
              from (i_cTable) where;
                  CACODICE = this.w_CODICE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_UNMIS3 = NVL(cp_ToDate(_read_.CAUNIMIS),cp_NullValue(_read_.CAUNIMIS))
            this.w_OPERA3 = NVL(cp_ToDate(_read_.CAOPERAT),cp_NullValue(_read_.CAOPERAT))
            this.w_MOLTI3 = NVL(cp_ToDate(_read_.CAMOLTIP),cp_NullValue(_read_.CAMOLTIP))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_UNMIS3 = LEFT(ALLTRIM(this.w_UNMIS3)+SPACE(3), 3)
          this.w_OPERA3 = LEFT(ALLTRIM(this.w_OPERA3)+" ", 1)
          this.w_COEUM1 = CALQTA(this.w_QTAMOV,this.w_UNIMIS,this.w_UNMIS2,this.w_OPERAT, this.w_MOLTIP, " ", "N", " ", , this.w_UNMIS3, this.w_OPERA3, this.w_MOLTI3)
        endif
        * --- Write into COM_VARI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.COM_VARI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.COM_VARI_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.COM_VARI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CVCOEUM1 ="+cp_NullLink(cp_ToStrODBC(this.w_COEUM1),'COM_VARI','CVCOEUM1');
              +i_ccchkf ;
          +" where ";
              +"CVCODICE = "+cp_ToStrODBC(_Curs_COM_VARI.CVCODICE);
              +" and CVNUMRIF = "+cp_ToStrODBC(_Curs_COM_VARI.CVNUMRIF);
              +" and CPROWNUM = "+cp_ToStrODBC(_Curs_COM_VARI.CPROWNUM);
                 )
        else
          update (i_cTable) set;
              CVCOEUM1 = this.w_COEUM1;
              &i_ccchkf. ;
           where;
              CVCODICE = _Curs_COM_VARI.CVCODICE;
              and CVNUMRIF = _Curs_COM_VARI.CVNUMRIF;
              and CPROWNUM = _Curs_COM_VARI.CPROWNUM;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
        select _Curs_COM_VARI
        continue
      enddo
      use
    endif
    if bTrsErr
      * --- Raise
      i_Error=MESSAGE()
      return
    else
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("Elaborazione terminata con successo")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = ah_Msgformat("Elaborazione terminata con successo")
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='DISTBASE'
    this.cWorkTables[3]='COM_VARI'
    this.cWorkTables[4]='ART_ICOL'
    this.cWorkTables[5]='KEY_ARTI'
    return(this.OpenAllTables(5))

  proc CloseCursors()
    if used('_Curs_DISTBASE')
      use in _Curs_DISTBASE
    endif
    if used('_Curs_COM_VARI')
      use in _Curs_COM_VARI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
