* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscv_ba2                                                        *
*              Aggiornamento mvqtaimp / espl.distinta                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_309]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-10-18                                                      *
* Last revis.: 2001-10-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscv_ba2",oParentObject,m.w_NHF)
return(i_retval)

define class tgscv_ba2 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_SERIAL = space(10)
  w_CPROWNUM = 0
  w_TOTIMP = 0
  w_TOTIM1 = 0
  w_QTAORI = 0
  w_QTAOR1 = 0
  w_QTAIMP = 0
  w_QTAIM1 = 0
  * --- WorkFile variables
  DOC_DETT_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiornamento campi MVQTAIMP e MVQTAUM1 nel caso
    *     il documento di origine abbia una qt� movimentata inferiore alla
    *     qt� evase. Inoltre storno la qt� evasa sui documenti generati dall'esplosione
    *     distinta base se cancellato il documento che li ha generati
    * --- Try
    local bErr_03894D18
    bErr_03894D18=bTrsErr
    this.Try_03894D18()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0 
        this.w_TMPC = this.oParentObject.w_PMSG
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_03894D18
    * --- End
  endproc
  proc Try_03894D18()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Valorizzo le qt� per documenti evasi da un unico documento
    * --- Write into DOC_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="MVSERIAL,CPROWNUM"
      do vq_exec with 'GSCV_BA2',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
              +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MVQTAIMP = _t2.QTA_ORI";
          +",MVQTAIM1 = _t2.QT1_ORI ";
          +i_ccchkf;
          +" from "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
              +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 set ";
          +"DOC_DETT.MVQTAIMP = _t2.QTA_ORI";
          +",DOC_DETT.MVQTAIM1 = _t2.QT1_ORI ";
          +Iif(Empty(i_ccchkf),"",",DOC_DETT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="DOC_DETT.MVSERIAL = t2.MVSERIAL";
              +" and "+"DOC_DETT.CPROWNUM = t2.CPROWNUM";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set (";
          +"MVQTAIMP,";
          +"MVQTAIM1";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.QTA_ORI,";
          +"t2.QT1_ORI ";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
              +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set ";
          +"MVQTAIMP = _t2.QTA_ORI";
          +",MVQTAIM1 = _t2.QT1_ORI ";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MVQTAIMP = (select QTA_ORI from "+i_cQueryTable+" where "+i_cWhere+")";
          +",MVQTAIM1 = (select QT1_ORI  from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Esamo uno per uno i documenti evasi da pi�  righe
    *     che hanno la qt� evasa superiore alla qt� movimentata.
    * --- Select from GSCV1BA2
    do vq_exec with 'GSCV1BA2',this,'_Curs_GSCV1BA2','',.f.,.t.
    if used('_Curs_GSCV1BA2')
      select _Curs_GSCV1BA2
      locate for 1=1
      do while not(eof())
      * --- Per ogni riga di origine esamino i documenti che la evadono, li ordino
      *     per MVSERIAL e valorizzo MVQTAIMP all'arrivare alla qt�
      *     movimentata
      this.w_SERIAL = _Curs_GSCV1BA2.SER_ORI 
      this.w_CPROWNUM = _Curs_GSCV1BA2.ROW_ORI 
      this.w_QTAORI = _Curs_GSCV1BA2.QTA_ORI 
      this.w_QTAOR1 = _Curs_GSCV1BA2.QT1_ORI 
      this.w_TOTIMP = 0
      this.w_TOTIM1 = 0
      ah_Msg("Esamino doc. %1 riga %2",.T.,.F.,.F., this.w_SERIAL, Alltrim(Str(this.w_CPROWNUM)))
      * --- Recupero tutte le righe che evadono ordinate per qt� ascendente..
      * --- Select from DOC_DETT
      i_nConn=i_TableProp[this.DOC_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select MVSERIAL,CPROWNUM,MVQTAMOV,MVQTAIMP,MVQTAIM1  from "+i_cTable+" DOC_DETT ";
            +" where MVSERRIF= "+cp_ToStrODBC(this.w_SERIAL)+" And MVROWRIF= "+cp_ToStrODBC(this.w_CPROWNUM)+"";
            +" order by MVQTAIMP ASC";
             ,"_Curs_DOC_DETT")
      else
        select MVSERIAL,CPROWNUM,MVQTAMOV,MVQTAIMP,MVQTAIM1 from (i_cTable);
         where MVSERRIF= this.w_SERIAL And MVROWRIF= this.w_CPROWNUM;
         order by MVQTAIMP ASC;
          into cursor _Curs_DOC_DETT
      endif
      if used('_Curs_DOC_DETT')
        select _Curs_DOC_DETT
        locate for 1=1
        do while not(eof())
        this.w_TOTIMP = this.w_TOTIMP + NVL( _Curs_DOC_DETT.MVQTAIMP , 0 )
        this.w_TOTIM1 = this.w_TOTIM1 + NVL( _Curs_DOC_DETT.MVQTAIM1 , 0 )
        * --- Se il totale impegnato supera la quantit� sul movimento di origine
        *     la procedura sul movimento corrente storna l'impegno (storna se
        *     MVQTAIMP rimane cmq maggiore di 0)
        if this.w_TOTIMP>this.w_QTAORI
          if ( this.w_QTAORI - ( this.w_TOTIMP - NVL( _Curs_DOC_DETT.MVQTAIMP , 0 ) ) )>=1
            this.w_QTAIMP = ( this.w_QTAORI - ( this.w_TOTIMP - NVL( _Curs_DOC_DETT.MVQTAIMP , 0 ) ) )
            this.w_QTAIM1 = ( this.w_QTAOR1 - ( this.w_TOTIM1 - NVL( _Curs_DOC_DETT.MVQTAIM1 , 0 ) ) )
            * --- Write into DOC_DETT
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.DOC_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"MVQTAIMP ="+cp_NullLink(cp_ToStrODBC(this.w_QTAIMP),'DOC_DETT','MVQTAIMP');
              +",MVQTAIM1 ="+cp_NullLink(cp_ToStrODBC(this.w_QTAIM1),'DOC_DETT','MVQTAIM1');
                  +i_ccchkf ;
              +" where ";
                  +"MVSERIAL = "+cp_ToStrODBC(_Curs_DOC_DETT.MVSERIAL);
                  +" and CPROWNUM = "+cp_ToStrODBC(_Curs_DOC_DETT.CPROWNUM);
                  +" and MVNUMRIF = "+cp_ToStrODBC(-20);
                     )
            else
              update (i_cTable) set;
                  MVQTAIMP = this.w_QTAIMP;
                  ,MVQTAIM1 = this.w_QTAIM1;
                  &i_ccchkf. ;
               where;
                  MVSERIAL = _Curs_DOC_DETT.MVSERIAL;
                  and CPROWNUM = _Curs_DOC_DETT.CPROWNUM;
                  and MVNUMRIF = -20;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          else
            * --- Riga documento con problema, memorizzo il serial ed il cprownum
            this.w_TMPC = ah_Msgformat("Attenzione documento con quantit� evasa non impegnabile sulle righe di evasione%0Seriale %1 riga %2%0",this.w_SERIAL, ALLTRIM( STR( this.w_CPROWNUM ) ) )
            this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
            Exit
          endif
        endif
          select _Curs_DOC_DETT
          continue
        enddo
        use
      endif
        select _Curs_GSCV1BA2
        continue
      enddo
      use
    endif
    * --- Sotttraggo alla quantita evasa dei documenti generati la parte non stornata
    * --- Write into DOC_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="MVSERIAL,CPROWNUM"
      do vq_exec with 'GSCV2BA2',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
              +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MVQTAEVA = DOC_DETT.MVQTAEVA-_t2.EVA";
          +",MVQTAEV1 = DOC_DETT.MVQTAEV1-_t2.EV1";
          +i_ccchkf;
          +" from "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
              +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 set ";
          +"DOC_DETT.MVQTAEVA = DOC_DETT.MVQTAEVA-_t2.EVA";
          +",DOC_DETT.MVQTAEV1 = DOC_DETT.MVQTAEV1-_t2.EV1";
          +Iif(Empty(i_ccchkf),"",",DOC_DETT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="DOC_DETT.MVSERIAL = t2.MVSERIAL";
              +" and "+"DOC_DETT.CPROWNUM = t2.CPROWNUM";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set (";
          +"MVQTAEVA,";
          +"MVQTAEV1";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"MVQTAEVA-t2.EVA,";
          +"MVQTAEV1-t2.EV1";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
              +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set ";
          +"MVQTAEVA = DOC_DETT.MVQTAEVA-_t2.EVA";
          +",MVQTAEV1 = DOC_DETT.MVQTAEV1-_t2.EV1";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MVQTAEVA = (select "+i_cTable+".MVQTAEVA-EVA from "+i_cQueryTable+" where "+i_cWhere+")";
          +",MVQTAEV1 = (select "+i_cTable+".MVQTAEV1-EV1 from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    if bTrsErr
      * --- Raise
      i_Error=MESSAGE()
      return
    else
      * --- commit
      cp_EndTrs(.t.)
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("Adeguamento campi MVQTAIMP, MVQTAIM1 e MVQTAEVA su tabella DOC_DETT eseguito correttamente%0")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = ah_Msgformat("Adeguamento campi MVQTAIMP, MVQTAIM1 e MVQTAEVA su tabella DOC_DETT eseguito correttamente%0")
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='DOC_DETT'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_GSCV1BA2')
      use in _Curs_GSCV1BA2
    endif
    if used('_Curs_DOC_DETT')
      use in _Curs_DOC_DETT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
