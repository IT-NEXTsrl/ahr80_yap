* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcc40                                                        *
*              Valorizzazione campo PRTIPDOC in tabella PRE_STAZ               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-01-21                                                      *
* Last revis.: 2014-01-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcc40",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcc40 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_NCONN = 0
  w_PRSERIAL = space(10)
  w_PAGENPRE = space(1)
  w_PACAUPRE = space(5)
  w_CODAZI = space(5)
  * --- WorkFile variables
  PRE_STAZ_idx=0
  PAR_ALTE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Valorizzazione campo PRTIPDOC in tabella PRE_STAZ
    this.w_NCONN = i_TableProp[this.PRE_STAZ_idx,3] 
    if ISALT()
      this.w_CODAZI = i_CODAZI
      * --- Read from PAR_ALTE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PAR_ALTE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_ALTE_idx,2],.t.,this.PAR_ALTE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "PAGENPRE,PACAUPRE"+;
          " from "+i_cTable+" PAR_ALTE where ";
              +"PACODAZI = "+cp_ToStrODBC(this.w_CODAZI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          PAGENPRE,PACAUPRE;
          from (i_cTable) where;
              PACODAZI = this.w_CODAZI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PAGENPRE = NVL(cp_ToDate(_read_.PAGENPRE),cp_NullValue(_read_.PAGENPRE))
        this.w_PACAUPRE = NVL(cp_ToDate(_read_.PACAUPRE),cp_NullValue(_read_.PACAUPRE))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Se attiva la Parcellazione semplificata
      if this.w_PAGENPRE="S"
        * --- Se valorizzato il campo Causale Prestazione nei Parametri generali
        if NOT EMPTY(NVL(this.w_PACAUPRE, ""))
          * --- Try
          local bErr_03619530
          bErr_03619530=bTrsErr
          this.Try_03619530()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_03619530
          * --- End
          if this.w_NHF>=0
            this.w_TMPC = ah_Msgformat("Elaborazione terminata con successo")
            this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
          endif
          * --- Esecuzione ok
          this.oParentObject.w_PESEOK = .T.
          this.oParentObject.w_PMSG = ah_Msgformat("Elaborazione terminata con successo")
        else
          if this.w_NHF>=0
            this.w_TMPC = ah_Msgformat("Procedura non eseguita. Valorizzare la Causale prestazione nei Parametri generali.")
            this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
          endif
          * --- Esecuzione negativa
          this.oParentObject.w_PESEOK = .F.
          this.oParentObject.w_PMSG = ah_Msgformat("Procedura non eseguita")
        endif
      else
        if this.w_NHF>=0
          this.w_TMPC = ah_Msgformat("Procedura di conversione eseguita con successo")
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
        * --- Esecuzione ok
        this.oParentObject.w_PESEOK = .T.
        this.oParentObject.w_PMSG = ah_Msgformat("Elaborazione terminata con successo")
      endif
    else
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("Procedura di conversione eseguita con successo")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
      * --- Esecuzione ok
      this.oParentObject.w_PESEOK = .T.
      this.oParentObject.w_PMSG = ah_Msgformat("Elaborazione terminata con successo")
    endif
  endproc
  proc Try_03619530()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Select from PRE_STAZ
    i_nConn=i_TableProp[this.PRE_STAZ_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRE_STAZ_idx,2],.t.,this.PRE_STAZ_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" PRE_STAZ ";
           ,"_Curs_PRE_STAZ")
    else
      select * from (i_cTable);
        into cursor _Curs_PRE_STAZ
    endif
    if used('_Curs_PRE_STAZ')
      select _Curs_PRE_STAZ
      locate for 1=1
      do while not(eof())
      if EMPTY(NVL(_Curs_PRE_STAZ.PRCODNOM, ""))
        this.w_PRSERIAL = _Curs_PRE_STAZ.PRSERIAL
        * --- Aggiorna campo PRTIPDOC
        * --- Write into PRE_STAZ
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.PRE_STAZ_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PRE_STAZ_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.PRE_STAZ_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PRTIPDOC ="+cp_NullLink(cp_ToStrODBC(this.w_PACAUPRE),'PRE_STAZ','PRTIPDOC');
              +i_ccchkf ;
          +" where ";
              +"PRSERIAL = "+cp_ToStrODBC(this.w_PRSERIAL);
                 )
        else
          update (i_cTable) set;
              PRTIPDOC = this.w_PACAUPRE;
              &i_ccchkf. ;
           where;
              PRSERIAL = this.w_PRSERIAL;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
        select _Curs_PRE_STAZ
        continue
      enddo
      use
    endif
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Valorizzato campo PRTIPDOC in tabella PRE_STAZ")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='PRE_STAZ'
    this.cWorkTables[2]='PAR_ALTE'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_PRE_STAZ')
      use in _Curs_PRE_STAZ
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
