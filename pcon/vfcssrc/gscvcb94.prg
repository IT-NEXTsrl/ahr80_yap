* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcb94                                                        *
*              Valorizzazione flag di stampa data prestazioni                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-04-18                                                      *
* Last revis.: 2011-04-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcb94",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcb94 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_NCONN = 0
  w_PRCODAZI = space(5)
  w_PRCAUPRE = space(5)
  w_PRRIFDAT = space(1)
  * --- WorkFile variables
  PAR_PARC_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Valorizzazione flag di stampa data prestazioni in tabella PAR_PARC (Parametri parcellazione)
    this.w_NCONN = i_TableProp[this.PAR_PARC_idx,3] 
    if IsAlt()
      * --- Try
      local bErr_037759A0
      bErr_037759A0=bTrsErr
      this.Try_037759A0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_037759A0
      * --- End
    else
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("Procedura di conversione eseguita con successo")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    * --- Resoconto - sempre positivo
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Elaborazione terminata con successo")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = ah_Msgformat("Elaborazione terminata con successo")
  endproc
  proc Try_037759A0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Select from PAR_PARC
    i_nConn=i_TableProp[this.PAR_PARC_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_PARC_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select PAR_PARC.PRCODAZI,PAR_PARC.PRCAUPRE,PAR_PARC.PRRIFDAT  from "+i_cTable+" PAR_PARC ";
           ,"_Curs_PAR_PARC")
    else
      select PAR_PARC.PRCODAZI,PAR_PARC.PRCAUPRE,PAR_PARC.PRRIFDAT from (i_cTable);
        into cursor _Curs_PAR_PARC
    endif
    if used('_Curs_PAR_PARC')
      select _Curs_PAR_PARC
      locate for 1=1
      do while not(eof())
      this.w_PRCODAZI = _Curs_PAR_PARC.PRCODAZI
      this.w_PRCAUPRE = _Curs_PAR_PARC.PRCAUPRE
      this.w_PRRIFDAT = _Curs_PAR_PARC.PRRIFDAT
      if this.w_PRRIFDAT="S"
        * --- Write into PAR_PARC
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.PAR_PARC_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_PARC_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_PARC_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PRDATDIR ="+cp_NullLink(cp_ToStrODBC("S"),'PAR_PARC','PRDATDIR');
          +",PRDATONO ="+cp_NullLink(cp_ToStrODBC("S"),'PAR_PARC','PRDATONO');
          +",PRDATTEM ="+cp_NullLink(cp_ToStrODBC("S"),'PAR_PARC','PRDATTEM');
          +",PRDATSPA ="+cp_NullLink(cp_ToStrODBC("S"),'PAR_PARC','PRDATSPA');
              +i_ccchkf ;
          +" where ";
              +"PRCODAZI = "+cp_ToStrODBC(this.w_PRCODAZI);
              +" and PRCAUPRE = "+cp_ToStrODBC(this.w_PRCAUPRE);
                 )
        else
          update (i_cTable) set;
              PRDATDIR = "S";
              ,PRDATONO = "S";
              ,PRDATTEM = "S";
              ,PRDATSPA = "S";
              &i_ccchkf. ;
           where;
              PRCODAZI = this.w_PRCODAZI;
              and PRCAUPRE = this.w_PRCAUPRE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
        select _Curs_PAR_PARC
        continue
      enddo
      use
    endif
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='PAR_PARC'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_PAR_PARC')
      use in _Curs_PAR_PARC
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
