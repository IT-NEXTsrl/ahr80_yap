* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcb40                                                        *
*              Valorizzazione campo Calcolo diritti per valore indet.          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-04-03                                                      *
* Last revis.: 2009-04-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcb40",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcb40 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_NCONN = 0
  w_CODPRAT = space(15)
  w_CNFLDIND = space(1)
  * --- WorkFile variables
  OFF_ATTI_idx=0
  CAN_TIER_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Valorizzazione campo Calcolo diritti per valore indet. nella tabella delle Pratiche
    this.w_NCONN = i_TableProp[this.OFF_ATTI_idx,3] 
    if IsAlt()
      * --- Try
      local bErr_0361DEE0
      bErr_0361DEE0=bTrsErr
      this.Try_0361DEE0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
      endif
      bTrsErr=bTrsErr or bErr_0361DEE0
      * --- End
    else
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("Procedura di conversione eseguita con successo")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    * --- Resoconto - sempre positivo
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Elaborazione terminata con successo")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = ah_Msgformat("Elaborazione terminata con successo")
  endproc
  proc Try_0361DEE0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Vengono selezionate le pratiche con valore indeterminabile oppure di particolare importanza e indeterminabile
    * --- Select from CAN_TIER
    i_nConn=i_TableProp[this.CAN_TIER_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select CNCODCAN,CNFLVALO,CNFLDIND  from "+i_cTable+" CAN_TIER ";
          +" where CNFLVALO='I' OR CNFLVALO='P'";
           ,"_Curs_CAN_TIER")
    else
      select CNCODCAN,CNFLVALO,CNFLDIND from (i_cTable);
       where CNFLVALO="I" OR CNFLVALO="P";
        into cursor _Curs_CAN_TIER
    endif
    if used('_Curs_CAN_TIER')
      select _Curs_CAN_TIER
      locate for 1=1
      do while not(eof())
      * --- Se il campo 'Calcolo dei diritti per valore indeterminabile'  � vuoto
      if EMPTY(NVL(_Curs_CAN_TIER.CNFLDIND,""))
        this.w_CODPRAT = _Curs_CAN_TIER.CNCODCAN
        * --- Se il valore della pratica � indeterminabile
        if _Curs_CAN_TIER.CNFLVALO="I"
          this.w_CNFLDIND = "I"
        else
          * --- Se il valore della pratica � di particolare importanza e indeterminabile
          this.w_CNFLDIND = "P"
        endif
        * --- Valorizzazione campo Calcolo dei diritti per valore indeterminabile
        * --- Write into CAN_TIER
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.CAN_TIER_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CAN_TIER_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CNFLDIND ="+cp_NullLink(cp_ToStrODBC(this.w_CNFLDIND),'CAN_TIER','CNFLDIND');
              +i_ccchkf ;
          +" where ";
              +"CNCODCAN = "+cp_ToStrODBC(this.w_CODPRAT);
                 )
        else
          update (i_cTable) set;
              CNFLDIND = this.w_CNFLDIND;
              &i_ccchkf. ;
           where;
              CNCODCAN = this.w_CODPRAT;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
        select _Curs_CAN_TIER
        continue
      enddo
      use
    endif
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Valorizzato campo Calcolo dei diritti per valore indeterminabile")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='OFF_ATTI'
    this.cWorkTables[2]='CAN_TIER'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_CAN_TIER')
      use in _Curs_CAN_TIER
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
