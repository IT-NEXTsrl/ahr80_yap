* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscv2bdt                                                        *
*              Elimina tabella                                                 *
*                                                                              *
*      Author: Nicola Gorlandi                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_8]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-12-23                                                      *
* Last revis.: 2008-09-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTableName,pConn
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscv2bdt",oParentObject,m.pTableName,m.pConn)
return(i_retval)

define class tgscv2bdt as StdBatch
  * --- Local variables
  pTableName = space(200)
  pConn = 0
  w_RESULT = .f.
  w_PHNAME = space(200)
  * --- WorkFile variables
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Dato il nome logico della tabella e l'handle della connessione  la elimina dal database.
    *     Da utilizzare per svolgere procedure di conversione su BD2
    *     per archivi costruiti pre rel .2.2
    *     Restituisce .t. se tutto Ok, altrimenti .f.
    if type("bTrsErr")="U"
       public bTrsErr
    endif
    if not(bTrsErr)
      this.w_RESULT = .T.
      * --- Se la connessione non � passata allora utilizzo la connessione principale
      this.pConn = IIF(Parameters()=1, i_ServerConn[1,2] , this.pConn )
      * --- Carica dizionario dati (per sicurezza)
      =cp_ReadXdc()
      this.w_PHNAME = i_dcx.GetPhTable(i_dcx.GetTableIdx( this.pTableName ), i_CODAZI )
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if this.w_RESULT
        * --- Se tabella con attivo il check company, distruggo anche la tabella legata all'azienda xxx
        if I_DCX.IsTableAzi( this.pTableName )
          this.w_PHNAME = i_dcx.GetPhTable(i_dcx.GetTableIdx( this.pTableName ), "xxx" )
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      endif
    else
      this.w_RESULT = .f.
    endif
    i_retcode = 'stop'
    i_retval = this.w_RESULT
    return
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Eseguo direttamente la Drop avvalendomi del fatto che DB2 elimina eventuali
    *     Link presenti sulla tabella.
    if cp_SQL( this.pConn ,"drop table "+ this.w_PHNAME ) < 0
      this.w_RESULT = .F.
    else
      * --- Costringo il sistema a verificare se sul database esiste la tabella..
      i_existenttbls = ""
    endif
    ah_Msg("Cancellata tabella di origine",.T.)
  endproc


  proc Init(oParentObject,pTableName,pConn)
    this.pTableName=pTableName
    this.pConn=pConn
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTableName,pConn"
endproc
