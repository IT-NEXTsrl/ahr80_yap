* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvab61                                                        *
*              Valorizza spese incasso                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_4]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-02-11                                                      *
* Last revis.: 2005-02-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvab61",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvab61 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_MESS = space(100)
  w_CODICE = space(5)
  w_PASPEINC = 0
  w_PASPEIN2 = 0
  * --- WorkFile variables
  PAG_AMEN_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Valorizza le spese di incasso ciclo vendite e ciclo acquisti
    *     PASPEINA = PASPEINC e PASPEIA2 = PASPEIN2
    * --- Try
    local bErr_035F6690
    bErr_035F6690=bTrsErr
    this.Try_035F6690()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE GENERICO - impossibile aggiornare i campi PASPEINA e PASPEIA2 tabella PAG_AMEN")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_035F6690
    * --- End
  endproc
  proc Try_035F6690()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    vq_exec("..\pcon\exe\query\gscvab61.vqr",this,"TmpCurs")
    if USED("TmpCurs")
      if Reccount( "TmpCurs" ) > 0
        Select TmpCurs
        Go Top
        Scan
        this.w_CODICE = NVL( TmpCurs.PACODICE, "" )
        this.w_PASPEINC = NVL( TmpCurs.PASPEINC, 0 )
        this.w_PASPEIN2 = NVL( TmpCurs.PASPEIN2, 0 )
        * --- Effettuo l'aggiornamento nella tabella PAG_AMEN
        * --- Write into PAG_AMEN
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.PAG_AMEN_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAG_AMEN_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.PAG_AMEN_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PASPEINA ="+cp_NullLink(cp_ToStrODBC(this.w_PASPEINC),'PAG_AMEN','PASPEINA');
          +",PASPEIA2 ="+cp_NullLink(cp_ToStrODBC(this.w_PASPEIN2),'PAG_AMEN','PASPEIA2');
              +i_ccchkf ;
          +" where ";
              +"PACODICE = "+cp_ToStrODBC(this.w_CODICE);
                 )
        else
          update (i_cTable) set;
              PASPEINA = this.w_PASPEINC;
              ,PASPEIA2 = this.w_PASPEIN2;
              &i_ccchkf. ;
           where;
              PACODICE = this.w_CODICE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        Endscan
        * --- Esecuzione ok
        if this.w_NHF>=0
          this.w_TMPC = ah_Msgformat("Aggiornamento campi PASPEINA e PASPEIA2 eseguito correttamente")
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
        this.oParentObject.w_PESEOK = .T.
        this.oParentObject.w_PMSG = ah_Msgformat("Valorizzazione eseguita correttamente")
      else
        * --- Gestisce log errori
        if this.w_NHF>=0
          this.w_TMPC = ah_Msgformat("ATTENZIONE: non esistono dati da processare")
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
        * --- Esecuzione ok
        this.oParentObject.w_PESEOK = .T.
        this.oParentObject.w_PMSG = this.w_TMPC
      endif
      * --- Chiude cursore
      select TmpCurs
      use
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='PAG_AMEN'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
