* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvab24                                                        *
*              Aggiorna chiave conti correnti                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-05-21                                                      *
* Last revis.: 2004-05-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvab24",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvab24 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_MESS = space(100)
  w_TEST = space(10)
  * --- WorkFile variables
  CCC_DETT_idx=0
  RIPATMP1_idx=0
  CONVERSI_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- FIle di LOG
    * --- Se procedura gia lanciata sulla 3.0 non faccio nulla...
    * --- Read from CONVERSI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CONVERSI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONVERSI_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "COCODREL"+;
        " from "+i_cTable+" CONVERSI where ";
            +"COCODREL = "+cp_ToStrODBC("3.0-E4525");
            +" and COESEGUI = "+cp_ToStrODBC("S");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        COCODREL;
        from (i_cTable) where;
            COCODREL = "3.0-E4525";
            and COESEGUI = "S";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_TEST = NVL(cp_ToDate(_read_.COCODREL),cp_NullValue(_read_.COCODREL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if Empty( this.w_TEST )
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Try
      local bErr_04A98370
      bErr_04A98370=bTrsErr
      this.Try_04A98370()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        * --- Gestisce log errori
        this.oParentObject.w_PMSG = Message()
        this.oParentObject.w_PESEOK = .F.
        if this.w_NHF>=0
          this.w_TMPC = this.oParentObject.w_PMSG
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
      bTrsErr=bTrsErr or bErr_04A98370
      * --- End
    else
      this.oParentObject.w_PESEOK = .T.
      this.oParentObject.w_PMSG = "Elaborazione gia eseguita"
      if this.w_NHF>=0
        this.w_TMPC = this.oParentObject.w_PMSG
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
  endproc
  proc Try_04A98370()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    i_nConn=i_TableProp[this.CCC_DETT_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.CCC_DETT_idx,2])
    if i_nConn<>0
      do case
        case upper(CP_DBTYPE)="SQLSERVER"
          * --- Caso SQLServer
          w_iRows = SQLExec(i_nConn,"ALTER TABLE "+i_cTable+; 
 " ALTER COLUMN CPROWNUM INT NOT NULL")
          w_iRows = SQLExec(i_nConn,"ALTER TABLE "+i_cTable+; 
 " ALTER COLUMN CANUMCOR CHAR(15)")
          w_iRows = SQLExec(i_nConn,"ALTER TABLE xxxCCC_DETT"+; 
 " ALTER COLUMN CPROWNUM INT NOT NULL")
          w_iRows = SQLExec(i_nConn,"ALTER TABLE xxxCCC_DETT"+; 
 " ALTER COLUMN CANUMCOR CHAR(15)")
        case upper(CP_DBTYPE)="ORACLE"
          * --- Caso ORACLE
          w_iRows = SQLExec(i_nConn,"ALTER TABLE "+i_cTable+; 
 " MODIFY(CPROWNUM NOT NULL)")
          w_iRows = SQLExec(i_nConn,"ALTER TABLE xxxCCC_DETT"+; 
 " MODIFY(CPROWNUM NOT NULL)")
          w_iRows = SQLExec(i_nConn,"ALTER TABLE "+i_cTable+; 
 " MODIFY(CANUMCOR)")
          w_iRows = SQLExec(i_nConn,"ALTER TABLE xxxCCC_DETT"+; 
 " MODIFY(CANUMCOR)")
          w_DropIndex=SQLExec(i_nConn,"DROP INDEX pk_" + i_cTable)
          w_DropIndex=SQLExec(i_nConn,"DROP INDEX pk_xxxCCC_DETT")
        case upper(CP_DBTYPE)="DB2"
          * --- Caso DB2
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
      endcase
      if bTrsErr
        * --- Raise
        i_Error=MESSAGE()
        return
      else
        if this.w_NHF>=0
          if upper(CP_DBTYPE)="DB2"
            this.w_TMPC = "Elaborazione terminata con successo"
          else
            this.w_TMPC = "Adeguamento chiave primaria tabella CCC_DETT eseguito correttamente."+chr(13)+chr(10)
            this.w_TMPC = this.w_TMPC + "ATTENZIONE! Occorre andare in MANUTENZIONE DATABASE, selezionare la tabella " + chr(13)+chr(10)
            this.w_TMPC = this.w_TMPC + "CCC_DETT e premere <OK>"
          endif
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    if upper(CP_DBTYPE)="DB2"
      this.oParentObject.w_PMSG = "Elaborazione terminata con successo"
    else
      this.w_TMPC = "Adeguamento chiave primaria tabella CCC_DETT eseguito correttamente."+chr(13)+chr(10)
      this.w_TMPC = this.w_TMPC + "ATTENZIONE! Occorre andare in MANUTENZIONE DATABASE, selezionare la tabella " + chr(13)+chr(10)
      this.w_TMPC = this.w_TMPC + "CCC_DETT e premere <OK>"
      this.oParentObject.w_PMSG = this.w_TMPC
    endif
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella DB2
    pName="CCC_DETT"
    pDatabaseType=i_ServerConn[1,6]
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.CCC_DETT_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.CCC_DETT_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
          )
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    wait window "Creata tabella temporanea di appoggio" NOWAIT
    * --- Elimino la tabella
    if Not GSCV_BDT( this, "CCC_DETT" , i_nConn )
      * --- Raise
      i_Error="Error"
      return
    endif
    * --- Ricostruisco il database per la tabella
    GSCV_BRT(this, "CCC_DETT" , i_nConn , .T.)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into CCC_DETT
    i_nConn=i_TableProp[this.CCC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CCC_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.CCC_DETT_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    wait window "Eliminata tabella temporanea TMP" NOWAIT
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questa Procedura aggiorna il cprownum delle causali di Conto Corrente
    * --- Try
    local bErr_04A9AC50
    bErr_04A9AC50=bTrsErr
    this.Try_04A9AC50()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = "ERRORE "+this.oParentObject.w_PMSG+" - Impossibile aggiornare le Causali Conti Correnti"
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_04A9AC50
    * --- End
  endproc
  proc Try_04A9AC50()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    WAIT WIND "Aggiornamento Fatture di Acconto" Nowait
    * --- Write into CCC_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.CCC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CCC_DETT_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="CACODICE,CANUMCOR"
      do vq_exec with 'CONTI_CORRENTI',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.CCC_DETT_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="CCC_DETT.CACODICE = _t2.CACODICE";
              +" and "+"CCC_DETT.CANUMCOR = _t2.CANUMCOR";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CPROWNUM = _t2.CPROWNUM";
          +i_ccchkf;
          +" from "+i_cTable+" CCC_DETT, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="CCC_DETT.CACODICE = _t2.CACODICE";
              +" and "+"CCC_DETT.CANUMCOR = _t2.CANUMCOR";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CCC_DETT, "+i_cQueryTable+" _t2 set ";
          +"CCC_DETT.CPROWNUM = _t2.CPROWNUM";
          +Iif(Empty(i_ccchkf),"",",CCC_DETT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="CCC_DETT.CACODICE = t2.CACODICE";
              +" and "+"CCC_DETT.CANUMCOR = t2.CANUMCOR";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CCC_DETT set (";
          +"CPROWNUM";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.CPROWNUM";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="CCC_DETT.CACODICE = _t2.CACODICE";
              +" and "+"CCC_DETT.CANUMCOR = _t2.CANUMCOR";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CCC_DETT set ";
          +"CPROWNUM = _t2.CPROWNUM";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".CACODICE = "+i_cQueryTable+".CACODICE";
              +" and "+i_cTable+".CANUMCOR = "+i_cQueryTable+".CANUMCOR";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CPROWNUM = (select CPROWNUM from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    if bTrsErr
      * --- Raise
      i_Error=i_TrsMsg
      return
    endif
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = "Conversione eseguita correttamente."
    if this.w_NHF>=0
      this.w_TMPC = "Conversione eseguita correttamente"
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='CCC_DETT'
    this.cWorkTables[2]='*RIPATMP1'
    this.cWorkTables[3]='CONVERSI'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
