* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscv_b07                                                        *
*              Conv. progressivi esercizi (Euro-kit)                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_64]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-06-04                                                      *
* Last revis.: 2001-06-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscv_b07",oParentObject,m.w_NHF)
return(i_retval)

define class tgscv_b07 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_CODEUR = space(3)
  w_CODLIR = space(3)
  w_OBSO = ctod("  /  /  ")
  w_TmpN = 0
  w_TASSOEUR = 0
  w_TASSOLIT = 0
  w_TMPC = space(10)
  w_CODAZI = space(5)
  w_DECEUR = 0
  w_DARE = 0
  w_AVERE = 0
  w_PUNPAD = .NULL.
  w_PUNNON = .NULL.
  w_TEMP = space(10)
  w_SIMLIR = space(3)
  w_SIMEUR = space(3)
  w_ESEINI = space(4)
  w_ESEFIN = space(4)
  w_OK = .f.
  w_TESTO = space(10)
  w_oMess = .NULL.
  w_oPart = .NULL.
  * --- WorkFile variables
  AZIENDA_idx=0
  VALUTE_idx=0
  ESERCIZI_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- FIle di LOG
    * --- Visibile dal padre
    * --- Variabili locali
    * --- Lancia maschera
    this.w_OK = .F.
    this.w_ESEINI = space(4)
    this.w_ESEFIN = space(4)
    this.w_TESTO = ah_Msgformat("Progressivi esercizi")
    do GSCV_K07 with this
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if not this.w_OK
      i_retcode = 'stop'
      return
    endif
    * --- Data Obsolescenza
    this.w_OBSO = cp_CharToDate("01-01-2002")
    * --- Leggo il codice valuta Euro e Lire
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZVALEUR,AZVALLIR"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZVALEUR,AZVALLIR;
        from (i_cTable) where;
            AZCODAZI = i_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CODEUR = NVL(cp_ToDate(_read_.AZVALEUR),cp_NullValue(_read_.AZVALEUR))
      this.w_CODLIR = NVL(cp_ToDate(_read_.AZVALLIR),cp_NullValue(_read_.AZVALLIR))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Controllo 1 - Devono essere caricati i dati azienda
    if Empty ( this.w_CODLIR ) Or Empty ( this.w_CODEUR )
      this.oParentObject.w_PMSG = ah_Msgformat("ERRORE 001 - codice Euro o valuta nazionale non definiti in dati azienda")
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE 001 - codice Euro o valuta nazionale non definiti in dati azienda")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
      i_retcode = 'stop'
      return
    endif
    * --- Controllo 2 - L'esercizio corrente deve essere EURO
    if g_PERVAL <> g_CODEUR
      this.oParentObject.w_PMSG = ah_Msgformat("ERRORE 002 - la valuta di conto dell'esercizio corrente deve essere Euro")
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE 002 - la valuta di conto dell'esercizio corrente deve essere Euro")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
      i_retcode = 'stop'
      return
    endif
    * --- Leggo i tassi fissi di Euro (1) e Lire (1936,27)
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VACAOVAL,VADECTOT,VASIMVAL"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(this.w_CODEUR);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VACAOVAL,VADECTOT,VASIMVAL;
        from (i_cTable) where;
            VACODVAL = this.w_CODEUR;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_TASSOEUR = NVL(cp_ToDate(_read_.VACAOVAL),cp_NullValue(_read_.VACAOVAL))
      this.w_DECEUR = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
      this.w_SIMEUR = NVL(cp_ToDate(_read_.VASIMVAL),cp_NullValue(_read_.VASIMVAL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VACAOVAL,VASIMVAL"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(this.w_CODLIR);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VACAOVAL,VASIMVAL;
        from (i_cTable) where;
            VACODVAL = this.w_CODLIR;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_TASSOLIT = NVL(cp_ToDate(_read_.VACAOVAL),cp_NullValue(_read_.VACAOVAL))
      this.w_SIMLIR = NVL(cp_ToDate(_read_.VASIMVAL),cp_NullValue(_read_.VASIMVAL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Oggetto per messaggi incrementali
    this.w_oMess=createobject("Ah_Message")
    * --- Try
    local bErr_0378C420
    bErr_0378C420=bTrsErr
    this.Try_0378C420()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      this.w_TMPC = ah_Msgformat("ERRORE GENERICO - operazione sospesa")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    bTrsErr=bTrsErr or bErr_0378C420
    * --- End
  endproc
  proc Try_0378C420()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.w_CODAZI = i_CODAZI
    * --- Select from ESERCIZI
    i_nConn=i_TableProp[this.ESERCIZI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" ESERCIZI ";
          +" where ESCODAZI="+cp_ToStrODBC(this.w_CODAZI)+" and ESCODESE="+cp_ToStrODBC(this.w_ESEINI)+"";
           ,"_Curs_ESERCIZI")
    else
      select * from (i_cTable);
       where ESCODAZI=this.w_CODAZI and ESCODESE=this.w_ESEINI;
        into cursor _Curs_ESERCIZI
    endif
    if used('_Curs_ESERCIZI')
      select _Curs_ESERCIZI
      locate for 1=1
      do while not(eof())
      * --- Tot dare ...
      this.w_TmpN = Nvl ( _Curs_ESERCIZI.ESDARLIG , 0 )
      this.w_oPart = this.w_oMess.AddMsgPartNL("Tabella ESERCIZI - tot.dare L.G.: da %1 %2 a %3 %4")
      this.w_oPart.AddParam(this.w_CODLIR)     
      this.w_oPart.AddParam(TRAN(this.w_TMPN,"99,999,999,999"))     
      this.w_oPart.AddParam(this.w_SIMEUR)     
      * --- Converte
      this.w_DARE = cp_round( this.w_TMPN / g_CAOEUR, this.w_DECEUR)
      this.w_oPart.AddParam(TRAN(this.w_DARE,"99,999,999,999"))     
      * --- Tot avere ...
      this.w_TmpN = Nvl ( _Curs_ESERCIZI.ESAVELIG , 0 )
      this.w_oPart = this.w_oMess.AddMsgPartNL("Tabella ESERCIZI - tot.avere L.G.: da %1 %2 a %3 %4")
      this.w_oPart.AddParam(this.w_CODLIR)     
      this.w_oPart.AddParam(TRAN(this.w_TMPN,"99,999,999,999"))     
      this.w_oPart.AddParam(this.w_SIMEUR)     
      * --- Converte
      this.w_AVERE = cp_round( this.w_TMPN / g_CAOEUR, this.w_DECEUR)
      this.w_oPart.AddParam(TRAN(this.w_AVERE,"99,999,999,999"))     
      this.w_TMPC = this.w_oMess.ComposeMessage()
      if this.w_NHF>=0
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
      * --- Aggiorna
      * --- Write into ESERCIZI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.ESERCIZI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ESERCIZI_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"ESSTAINT ="+cp_NullLink(cp_ToStrODBC(_Curs_ESERCIZI.ESSTAINT),'ESERCIZI','ESSTAINT');
        +",ESDARLIG ="+cp_NullLink(cp_ToStrODBC(this.w_DARE),'ESERCIZI','ESDARLIG');
        +",ESAVELIG ="+cp_NullLink(cp_ToStrODBC(this.w_AVERE),'ESERCIZI','ESAVELIG');
        +",ESPROLIG ="+cp_NullLink(cp_ToStrODBC(_Curs_ESERCIZI.ESPROLIG),'ESERCIZI','ESPROLIG');
        +",ESFLSTAM ="+cp_NullLink(cp_ToStrODBC(_Curs_ESERCIZI.ESFLSTAM),'ESERCIZI','ESFLSTAM');
            +i_ccchkf ;
        +" where ";
            +"ESCODAZI = "+cp_ToStrODBC(_Curs_ESERCIZI.ESCODAZI);
            +" and ESCODESE = "+cp_ToStrODBC(this.w_ESEFIN);
               )
      else
        update (i_cTable) set;
            ESSTAINT = _Curs_ESERCIZI.ESSTAINT;
            ,ESDARLIG = this.w_DARE;
            ,ESAVELIG = this.w_AVERE;
            ,ESPROLIG = _Curs_ESERCIZI.ESPROLIG;
            ,ESFLSTAM = _Curs_ESERCIZI.ESFLSTAM;
            &i_ccchkf. ;
         where;
            ESCODAZI = _Curs_ESERCIZI.ESCODAZI;
            and ESCODESE = this.w_ESEFIN;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
        select _Curs_ESERCIZI
        continue
      enddo
      use
    endif
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = ah_Msgformat("Elaborazione terminata con successo")
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='VALUTE'
    this.cWorkTables[3]='ESERCIZI'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_ESERCIZI')
      use in _Curs_ESERCIZI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
