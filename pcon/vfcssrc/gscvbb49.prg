* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvbb49                                                        *
*              Aggiornamento flag ripartizione in causali documento            *
*                                                                              *
*      Author: Leopoldo Abbateggio                                             *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_86]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-08-03                                                      *
* Last revis.: 2006-08-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvbb49",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvbb49 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_MESS = space(100)
  * --- WorkFile variables
  TIP_DOCU_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Valorizza i flag di ripartizione nelle causali documento
    * --- FIle di LOG
    * --- Try
    local bErr_035EE220
    bErr_035EE220=bTrsErr
    this.Try_035EE220()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = Ah_Msgformat("Aggiornamento flag di ripartizione spese nelle causali documento fallito:%0%1" , this.oParentObject.w_PMSG)
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_035EE220
    * --- End
  endproc
  proc Try_035EE220()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into TIP_DOCU
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.TIP_DOCU_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"TDRIPINC ="+cp_NullLink(cp_ToStrODBC("S"),'TIP_DOCU','TDRIPINC');
      +",TDRIPIMB ="+cp_NullLink(cp_ToStrODBC("S"),'TIP_DOCU','TDRIPIMB');
      +",TDRIPTRA ="+cp_NullLink(cp_ToStrODBC("S"),'TIP_DOCU','TDRIPTRA');
          +i_ccchkf ;
      +" where ";
          +"TDFLVEAC = "+cp_ToStrODBC("A");
             )
    else
      update (i_cTable) set;
          TDRIPINC = "S";
          ,TDRIPIMB = "S";
          ,TDRIPTRA = "S";
          &i_ccchkf. ;
       where;
          TDFLVEAC = "A";

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Write into TIP_DOCU
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.TIP_DOCU_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"TDRIPINC ="+cp_NullLink(cp_ToStrODBC(" "),'TIP_DOCU','TDRIPINC');
      +",TDRIPIMB ="+cp_NullLink(cp_ToStrODBC(" "),'TIP_DOCU','TDRIPIMB');
      +",TDRIPTRA ="+cp_NullLink(cp_ToStrODBC(" "),'TIP_DOCU','TDRIPTRA');
          +i_ccchkf ;
      +" where ";
          +"TDFLVEAC = "+cp_ToStrODBC("V");
             )
    else
      update (i_cTable) set;
          TDRIPINC = " ";
          ,TDRIPIMB = " ";
          ,TDRIPTRA = " ";
          &i_ccchkf. ;
       where;
          TDFLVEAC = "V";

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = ah_Msgformat("Aggiornamento flag di ripartizione spese nelle causali documento eseguito con successo")
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='TIP_DOCU'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
