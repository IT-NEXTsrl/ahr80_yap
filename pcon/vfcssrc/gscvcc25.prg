* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcc25                                                        *
*              Ripristina dei collegamenti Eventi-Attivit� in base all'ID richiesta*
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-10-22                                                      *
* Last revis.: 2012-10-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcc25",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcc25 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TMPC = space(10)
  w_TEMP = space(10)
  w_EV__ANNO = space(4)
  w_EVSERIAL = space(10)
  * --- WorkFile variables
  OFF_ATTI_idx=0
  ANEVENTI_idx=0
  ASS_ATEV_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Ripristina dei collegamenti Eventi-Attivit� in base all'ID richiesta
    * --- FIle di LOG
    * --- Try
    local bErr_04A27FD8
    bErr_04A27FD8=bTrsErr
    this.Try_04A27FD8()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_msgformat("ERRORE GENERICO - Impossibile ripristinare i collegamenti")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_04A27FD8
    * --- End
  endproc
  proc Try_04A27FD8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Write into OFF_ATTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.OFF_ATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="ATSERIAL"
      do vq_exec with 'GSCVCC25',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_ATTI_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="OFF_ATTI.ATSERIAL = _t2.ATSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ATSEREVE = _t2.SERIALE";
          +",ATEVANNO = _t2.ANNO";
          +i_ccchkf;
          +" from "+i_cTable+" OFF_ATTI, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="OFF_ATTI.ATSERIAL = _t2.ATSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OFF_ATTI, "+i_cQueryTable+" _t2 set ";
          +"OFF_ATTI.ATSEREVE = _t2.SERIALE";
          +",OFF_ATTI.ATEVANNO = _t2.ANNO";
          +Iif(Empty(i_ccchkf),"",",OFF_ATTI.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="OFF_ATTI.ATSERIAL = t2.ATSERIAL";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OFF_ATTI set (";
          +"ATSEREVE,";
          +"ATEVANNO";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.SERIALE,";
          +"t2.ANNO";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="OFF_ATTI.ATSERIAL = _t2.ATSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OFF_ATTI set ";
          +"ATSEREVE = _t2.SERIALE";
          +",ATEVANNO = _t2.ANNO";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".ATSERIAL = "+i_cQueryTable+".ATSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ATSEREVE = (select SERIALE from "+i_cQueryTable+" where "+i_cWhere+")";
          +",ATEVANNO = (select ANNO from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Write into ASS_ATEV
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.ASS_ATEV_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ASS_ATEV_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="AECODATT"
      do vq_exec with 'GSCVCC25',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ASS_ATEV_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="ASS_ATEV.AECODATT = _t2.AECODATT";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"AECODEVE = _t2.SERIALE";
          +",AE__ANNO = _t2.ANNO";
          +i_ccchkf;
          +" from "+i_cTable+" ASS_ATEV, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="ASS_ATEV.AECODATT = _t2.AECODATT";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ASS_ATEV, "+i_cQueryTable+" _t2 set ";
          +"ASS_ATEV.AECODEVE = _t2.SERIALE";
          +",ASS_ATEV.AE__ANNO = _t2.ANNO";
          +Iif(Empty(i_ccchkf),"",",ASS_ATEV.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="ASS_ATEV.AECODATT = t2.AECODATT";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ASS_ATEV set (";
          +"AECODEVE,";
          +"AE__ANNO";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.SERIALE,";
          +"t2.ANNO";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="ASS_ATEV.AECODATT = _t2.AECODATT";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ASS_ATEV set ";
          +"AECODEVE = _t2.SERIALE";
          +",AE__ANNO = _t2.ANNO";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".AECODATT = "+i_cQueryTable+".AECODATT";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"AECODEVE = (select SERIALE from "+i_cQueryTable+" where "+i_cWhere+")";
          +",AE__ANNO = (select ANNO from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Select from GSCVPCC25
    do vq_exec with 'GSCVPCC25',this,'_Curs_GSCVPCC25','',.f.,.t.
    if used('_Curs_GSCVPCC25')
      select _Curs_GSCVPCC25
      locate for 1=1
      do while not(eof())
      this.w_EV__ANNO = _Curs_GSCVPCC25.EV__ANNO
      this.w_EVSERIAL = _Curs_GSCVPCC25.EVSERIAL
      i_Conn=i_TableProp[this.ANEVENTI_IDX, 3]
      cp_NextTableProg(this,i_nConn,"SEREVE","i_CODAZI,w_EV__ANNO,w_EVSERIAL", .T.)
        select _Curs_GSCVPCC25
        continue
      enddo
      use
    endif
    * --- Write into ANEVENTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.ANEVENTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ANEVENTI_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="EVSERIAL,EV__ANNO"
      do vq_exec with '..\PCON\EXE\QUERY\GSCVMCC25',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ANEVENTI_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="ANEVENTI.EVSERIAL = _t2.EVSERIAL";
              +" and "+"ANEVENTI.EV__ANNO = _t2.EV__ANNO";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"EVFLIDPA ="+cp_NullLink(cp_ToStrODBC(" "),'ANEVENTI','EVFLIDPA');
          +i_ccchkf;
          +" from "+i_cTable+" ANEVENTI, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="ANEVENTI.EVSERIAL = _t2.EVSERIAL";
              +" and "+"ANEVENTI.EV__ANNO = _t2.EV__ANNO";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ANEVENTI, "+i_cQueryTable+" _t2 set ";
      +"ANEVENTI.EVFLIDPA ="+cp_NullLink(cp_ToStrODBC(" "),'ANEVENTI','EVFLIDPA');
          +Iif(Empty(i_ccchkf),"",",ANEVENTI.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="ANEVENTI.EVSERIAL = t2.EVSERIAL";
              +" and "+"ANEVENTI.EV__ANNO = t2.EV__ANNO";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ANEVENTI set (";
          +"EVFLIDPA";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +cp_NullLink(cp_ToStrODBC(" "),'ANEVENTI','EVFLIDPA')+"";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="ANEVENTI.EVSERIAL = _t2.EVSERIAL";
              +" and "+"ANEVENTI.EV__ANNO = _t2.EV__ANNO";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ANEVENTI set ";
      +"EVFLIDPA ="+cp_NullLink(cp_ToStrODBC(" "),'ANEVENTI','EVFLIDPA');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".EVSERIAL = "+i_cQueryTable+".EVSERIAL";
              +" and "+i_cTable+".EV__ANNO = "+i_cQueryTable+".EV__ANNO";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"EVFLIDPA ="+cp_NullLink(cp_ToStrODBC(" "),'ANEVENTI','EVFLIDPA');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    if this.w_NHF>=0
      this.w_TMPC = ah_msgformat("Il ripristino dei collegamenti Eventi-Attivit� in base all'ID richiesta � avvenuto correttamente.")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = ah_msgformat("Conversione eseguita correttamente")
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='OFF_ATTI'
    this.cWorkTables[2]='ANEVENTI'
    this.cWorkTables[3]='ASS_ATEV'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_GSCVPCC25')
      use in _Curs_GSCVPCC25
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
