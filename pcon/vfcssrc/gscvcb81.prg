* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcb81                                                        *
*              Modifica ciclo di lavorazione legato all'ordine di lavorazione  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-06-25                                                      *
* Last revis.: 2012-11-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcb81",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcb81 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_inConn = 0
  w_FraseSQL = space(200)
  w_PHNAME = space(200)
  * --- WorkFile variables
  MAT_PROD_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Modifica ciclo di lavorazione legato all'ordine di lavorazione
    * --- Try
    local bErr_0360ABE0
    bErr_0360ABE0=bTrsErr
    this.Try_0360ABE0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("Errore generico - impossibile eseguire aggiornamento richiesto")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_0360ABE0
    * --- End
  endproc
  proc Try_0360ABE0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- --------------------------------------------------------------------------------------
    if this.w_NHF>=0
      this.w_TMPC = ah_msgformat("Eliminazione constraint tabella MAT_PROD e  AVA_MATR")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    * --- Elimino i constraint
    ah_msg("Aggiornamento in corso..." )
    GSCV_BDC(this,"xxx", "MAT_PROD")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    GSCV_BDC(this,i_CODAZI, "MAT_PROD")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    GSCV_BDC(this,"xxx", "AVA_MATR")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    GSCV_BDC(this,i_CODAZI, "AVA_MATR")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- --------------------------------------------------------------------------------------
    this.w_iNConn = i_ServerConn[1,2]
    if UPPER(CP_DBTYPE)="SQLSERVER"
      if this.w_NHF>=0
        this.w_TMPC = ah_msgformat("Aggiornamento campi chiave su tabella MAT_PROD e AVA_MATR")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
      ah_msg("Aggiornamento in corso..." )
      * --- Devo mettere not null inuovi campi costituenti la chiave
      this.w_FraseSQL = "ALTER TABLE xxxMAT_PROD ALTER COLUMN MPNUMRIF INT NOT NULL"
      w_iRows = CP_SQLEXEC(this.w_iNConn, rtrim(this.w_FraseSQL))
      this.w_FraseSQL = "ALTER TABLE " + alltrim(i_codazi) + "MAT_PROD ALTER COLUMN MPNUMRIF INT NOT NULL"
      w_iRows = CP_SQLEXEC(this.w_iNConn, rtrim(this.w_FraseSQL))
      this.w_FraseSQL = "ALTER TABLE xxxMAT_PROD ALTER COLUMN MPROWDOC INT NOT NULL"
      w_iRows = CP_SQLEXEC(this.w_iNConn, rtrim(this.w_FraseSQL))
      this.w_FraseSQL = "ALTER TABLE " + alltrim(i_codazi) + "MAT_PROD ALTER COLUMN MPROWDOC INT NOT NULL"
      w_iRows = CP_SQLEXEC(this.w_iNConn, rtrim(this.w_FraseSQL))
      * --- Devo mettere not null inuovi campi costituenti la chiave
      this.w_FraseSQL = "ALTER TABLE xxxAVA_MATR ALTER COLUMN MTNUMRIF INT NOT NULL"
      w_iRows = CP_SQLEXEC(this.w_iNConn, rtrim(this.w_FraseSQL))
      this.w_FraseSQL = "ALTER TABLE " + alltrim(i_codazi) + "AVA_MATR ALTER COLUMN MTNUMRIF INT NOT NULL"
      w_iRows = CP_SQLEXEC(this.w_iNConn, rtrim(this.w_FraseSQL))
      this.w_FraseSQL = "ALTER TABLE xxxAVA_MATR ALTER COLUMN MTROWDOC INT NOT NULL"
      w_iRows = CP_SQLEXEC(this.w_iNConn, rtrim(this.w_FraseSQL))
      this.w_FraseSQL = "ALTER TABLE " + alltrim(i_codazi) + "AVA_MATR ALTER COLUMN MTROWDOC INT NOT NULL"
      w_iRows = CP_SQLEXEC(this.w_iNConn, rtrim(this.w_FraseSQL))
    endif
    * --- commit
    cp_EndTrs(.t.)
    this.w_inConn =  i_ServerConn[1,2]
    GSCV_BRT(this,"MAT_PROD" , this.w_inConn , .T. , "all", "N")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    GSCV_BRT(this,"AVA_MATR" , this.w_inConn , .T. , "all", "N")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_TMPC = ah_Msgformat("Aggiornamento eseguito con successo")
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = this.w_TMPC
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='MAT_PROD'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
