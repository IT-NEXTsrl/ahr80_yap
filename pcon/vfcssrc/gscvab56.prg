* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvab56                                                        *
*              Aggiorna campo ctnrrigt (catmlist)                              *
*                                                                              *
*      Author: Zucchetti S.p.A.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_38]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-06-18                                                      *
* Last revis.: 2001-06-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvab56",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvab56 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_CODICE = space(15)
  w_RIGATEST = 0
  w_MESS = space(100)
  * --- WorkFile variables
  CATMLIST_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questa procedura valorizza il campo 'ARPUBWEB' della tabella ART_ICOL, dove � nullo o vuoto a 'N'
    * --- FIle di LOG
    * --- Try
    local bErr_035FC9A0
    bErr_035FC9A0=bTrsErr
    this.Try_035FC9A0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE GENERICO - Impossibile aggiornare il campo CTNRRIGT tabella CATMLIST")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_035FC9A0
    * --- End
  endproc
  proc Try_035FC9A0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Vado a leggere nella tabella CATMLIST le Categorie Listini, che hanno il check 'Riga di testata' (CTFLRIGT) attivato.
    *     Per queste imposto il campo CTNRRIGT a uno.
    *     
    vq_exec("..\pcon\exe\query\gscvab56.vqr",this,"CatLis")
    if USED("CatLis")
      if Reccount ("CatLis")>0
        Select CatLis
        Go Top
        Scan
        this.w_CODICE = ALLTRIM(NVL(CatLis.CTCODCAT,""))
        this.w_RIGATEST = 1
        * --- Effettuo l'aggiornamento nella tabella CATMLIST
        * --- Write into CATMLIST
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.CATMLIST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CATMLIST_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CATMLIST_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CTNRRIGT ="+cp_NullLink(cp_ToStrODBC(this.w_RIGATEST),'CATMLIST','CTNRRIGT');
              +i_ccchkf ;
          +" where ";
              +"CTCODCAT = "+cp_ToStrODBC(this.w_CODICE);
                 )
        else
          update (i_cTable) set;
              CTNRRIGT = this.w_RIGATEST;
              &i_ccchkf. ;
           where;
              CTCODCAT = this.w_CODICE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        Endscan
        * --- Esecuzione ok
        if this.w_NHF>=0
          this.w_TMPC = ah_Msgformat("Aggiornamento campo CTNRRIGT eseguito correttamente")
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
        this.oParentObject.w_PESEOK = .T.
        this.oParentObject.w_PMSG = ah_msgformat("Conversione eseguita correttamente")
      else
        * --- Gestisce log errori
        if this.w_NHF>=0
          this.w_TMPC = ah_Msgformat("ATTENZIONE: non esistono dati da convertire")
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
        * --- Esecuzione ok
        this.oParentObject.w_PESEOK = .T.
        this.oParentObject.w_PMSG = this.w_TMPC
      endif
      * --- Chiude cursore
      select CatLis
      use
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='CATMLIST'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
