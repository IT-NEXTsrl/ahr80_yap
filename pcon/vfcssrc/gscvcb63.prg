* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcb63                                                        *
*              Inserimento partecipante in attvit� sprovviste                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-01-16                                                      *
* Last revis.: 2009-11-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcb63",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcb63 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_CODPART = space(5)
  w_GRUPART = space(5)
  * --- WorkFile variables
  PRODINDI_idx=0
  OFF_PART_idx=0
  TMPVEND1_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inserimento partecipante di default in attivit� senza record nella tabella OFF_PART
    do GSCV_K63 with this
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if Not Empty(this.w_CODPART)
      * --- Try
      local bErr_04A84110
      bErr_04A84110=bTrsErr
      this.Try_04A84110()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        * --- Gestisce log errori
        this.oParentObject.w_PMSG = Message()
        this.oParentObject.w_PESEOK = .F.
        if this.w_NHF>=0
          this.w_TMPC = ah_Msgformat("Errore generico - impossibile eseguire l'inserimento richiesto")
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
      bTrsErr=bTrsErr or bErr_04A84110
      * --- End
      * --- Drop temporary table TMPVEND1
      i_nIdx=cp_GetTableDefIdx('TMPVEND1')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('TMPVEND1')
      endif
    else
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("Nessun partecipante selezionato - impossibile eseguire l'inserimento richiesto")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
  endproc
  proc Try_04A84110()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    this.w_TMPC = ah_Msgformat("Inserimento partecipanti eseguito con successo")
    * --- Esecuzione ok
    * --- Create temporary table TMPVEND1
    i_nIdx=cp_AddTableDef('TMPVEND1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('gscvcb63',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPVEND1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Insert into OFF_PART
    i_nConn=i_TableProp[this.OFF_PART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFF_PART_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"gscvcb63",this.OFF_PART_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = this.w_TMPC
    * --- commit
    cp_EndTrs(.t.)
     
 VQ_EXEC("..\PCON\EXE\QUERY\GSCVCB63_1.VQR",this,"__tmp__") 
 CP_CHPRN("..\PCON\EXE\QUERY\GSCVCB63_1.FRX", " ", this)
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='PRODINDI'
    this.cWorkTables[2]='OFF_PART'
    this.cWorkTables[3]='*TMPVEND1'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
