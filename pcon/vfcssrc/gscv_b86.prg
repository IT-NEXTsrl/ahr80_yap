* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscv_b86                                                        *
*              Aggiornamento progressivo spedizione                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_30]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-12-12                                                      *
* Last revis.: 2002-12-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscv_b86",oParentObject,m.w_NHF)
return(i_retval)

define class tgscv_b86 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_SERIAL = space(10)
  w_NUMTRA = 0
  * --- WorkFile variables
  DOC_DETT_idx=0
  MVM_DETT_idx=0
  PNT_MAST_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questa procedura di conversione valorizza il campo PNNUMTR2 con il valore di PNNUMTRA solo se la registrazione contabile non movimenta l'IVA.
    * --- Try
    local bErr_035FB710
    bErr_035FB710=bTrsErr
    this.Try_035FB710()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE - impossibile aggiornare progressivo spedizione")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_035FB710
    * --- End
  endproc
  proc Try_035FB710()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Aggiornamento  Progressivo Spedizione
    * --- Select from PNT_MAST
    i_nConn=i_TableProp[this.PNT_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select PNSERIAL,PNNUMTRA  from "+i_cTable+" PNT_MAST ";
          +" where PNNUMTRA<>0 AND PNTIPREG='N'";
           ,"_Curs_PNT_MAST")
    else
      select PNSERIAL,PNNUMTRA from (i_cTable);
       where PNNUMTRA<>0 AND PNTIPREG="N";
        into cursor _Curs_PNT_MAST
    endif
    if used('_Curs_PNT_MAST')
      select _Curs_PNT_MAST
      locate for 1=1
      do while not(eof())
      this.w_SERIAL = _Curs_PNT_MAST.PNSERIAL
      this.w_NUMTRA = _Curs_PNT_MAST.PNNUMTRA
      * --- Write into PNT_MAST
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.PNT_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.PNT_MAST_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"PNNUMTR2 ="+cp_NullLink(cp_ToStrODBC(this.w_NUMTRA),'PNT_MAST','PNNUMTR2');
            +i_ccchkf ;
        +" where ";
            +"PNSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
               )
      else
        update (i_cTable) set;
            PNNUMTR2 = this.w_NUMTRA;
            &i_ccchkf. ;
         where;
            PNSERIAL = this.w_SERIAL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
        select _Curs_PNT_MAST
        continue
      enddo
      use
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Aggiornamento progressivo spedizione eseguito con successo")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = this.oParentObject.w_PMSG + this.w_TMPC 
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='DOC_DETT'
    this.cWorkTables[2]='MVM_DETT'
    this.cWorkTables[3]='PNT_MAST'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_PNT_MAST')
      use in _Curs_PNT_MAST
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
