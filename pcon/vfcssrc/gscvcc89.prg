* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcc89                                                        *
*              Aggiornamento tipo articolo PK I.revolution                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2017-05-02                                                      *
* Last revis.: 2017-05-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcc89",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcc89 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_RECNO = 0
  w_ERROR_STATE = .f.
  * --- WorkFile variables
  ART_ICOL_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiornamento tipo articolo PK I.revolution
    *     Il campo ARTIPPKR deve essere valorizzato uguale al campo ARTIPART in caricamento, ma non deve mai essere modificato
    *     Nei servizi ARTIPPKR deve sempre essere uguale a ARTIPART (dato che il campo � editabile solo in caricamento di un nuovo record)
    *     ARTIPPKR potrebbe essere errato per un errore nel caricamento rapido articoli (servizi)
    * --- FIle di LOG
    * --- Try
    local bErr_04F94AA0
    bErr_04F94AA0=bTrsErr
    this.Try_04F94AA0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_pESEOK = .F.
      this.oParentObject.w_pMSG = Message()
    endif
    bTrsErr=bTrsErr or bErr_04F94AA0
    * --- End
    if this.w_NHF>=0
      this.w_TMPC = this.oParentObject.w_pMSG
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
  endproc
  proc Try_04F94AA0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Select from ART_ICOL
    i_nConn=i_TableProp[this.ART_ICOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select count(*) AS RECNO  from "+i_cTable+" ART_ICOL ";
          +" where ARTIPART in ('FO','FM','DE') AND ARTIPART<>ARTIPPKR";
           ,"_Curs_ART_ICOL")
    else
      select count(*) AS RECNO from (i_cTable);
       where ARTIPART in ("FO","FM","DE") AND ARTIPART<>ARTIPPKR;
        into cursor _Curs_ART_ICOL
    endif
    if used('_Curs_ART_ICOL')
      select _Curs_ART_ICOL
      locate for 1=1
      do while not(eof())
      this.w_RECNO = _Curs_ART_ICOL.RECNO
        select _Curs_ART_ICOL
        continue
      enddo
      use
    endif
    if this.w_RECNO>0
      * --- Write into ART_ICOL
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
        i_cTempTable=cp_GetTempTableName(i_nConn)
        i_aIndex[1]='ARCODART'
        cp_CreateTempTable(i_nConn,i_cTempTable,"ARCODART,ARTIPART "," from "+i_cQueryTable+" where ARTIPART in ('FO','FM','DE') AND ARTIPART<>ARTIPPKR",.f.,@i_aIndex)
        i_cQueryTable=i_cTempTable
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ART_ICOL_idx,i_nConn)
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="ART_ICOL.ARCODART = _t2.ARCODART";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"ARTIPPKR = ART_ICOL.ARTIPART";
            +i_ccchkf;
            +" from "+i_cTable+" ART_ICOL, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="ART_ICOL.ARCODART = _t2.ARCODART";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ART_ICOL, "+i_cQueryTable+" _t2 set ";
            +"ART_ICOL.ARTIPPKR = ART_ICOL.ARTIPART";
            +Iif(Empty(i_ccchkf),"",",ART_ICOL.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="PostgreSQL"
          i_cWhere="ART_ICOL.ARCODART = _t2.ARCODART";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ART_ICOL set ";
            +"ARTIPPKR = ART_ICOL.ARTIPART";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".ARCODART = "+i_cQueryTable+".ARCODART";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"ARTIPPKR = (select ARTIPART from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      this.oParentObject.w_pMSG = Ah_Msgformat("Aggiornamento campo ARTIPPKR eseguito con successo")
      if g_REVI="S"
        do GSRV_BAI with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    else
      this.oParentObject.w_pMSG = Ah_Msgformat("Tutti i valori del campo ARTIPPKR erano gi� corretti.")
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    this.oParentObject.w_pESEOK = .T.
    * --- Esegue sincronizzazione Articoli/servizi (fuori transazione!)
    if this.w_RECNO>0 and g_REVICRM="S" and Alltrim(i_codazi) $ assoclst()
      * --- Lancio la sincronizzazione articoli con I.revolution
      this.oParentObject.w_pMSG = this.oParentObject.w_pMSG + Ah_Msgformat("%0Sincronizzazione articoli/servizi eseguita alle ore %1.", time())
      this.w_ERROR_STATE = GSRV_BEA(This, g_PUBPROCNAME, "RECBOCLACON;RECBOPARAMDOC;RECBOARTICOLI;RECBOARTDES;RECBOARTALTE;RECBOCODRIC", i_codazi)
      if this.w_ERROR_STATE
        this.oParentObject.w_pMSG = this.oParentObject.w_pMSG + Ah_Msgformat("%0Sincronizzazione articoli/servizi terminata con anomalie, verificare il log di elaborazione.")
      else
        this.oParentObject.w_pMSG = this.oParentObject.w_pMSG + Ah_Msgformat("%0Sincronizzazione articoli/servizi terminata con successo.")
      endif
    endif
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='ART_ICOL'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_ART_ICOL')
      use in _Curs_ART_ICOL
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
