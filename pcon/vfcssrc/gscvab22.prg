* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvab22                                                        *
*              Aggiorna status e residuo nei solleciti                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_65]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-12-02                                                      *
* Last revis.: 2006-12-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvab22",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvab22 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  * --- WorkFile variables
  CON_TENZ_idx=0
  INC_AVVE_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questa procedura di conversione aggiorna Status e Residuo Insoluti con residuo <0
    *     dovuto alla presenza di spese
    *     
    * --- Try
    local bErr_04A90FF0
    bErr_04A90FF0=bTrsErr
    this.Try_04A90FF0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE - impossibile aggiornare aggiornamento status e residuo solleciti" )
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_04A90FF0
    * --- End
  endproc
  proc Try_04A90FF0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if g_SOLL="S"
      * --- begin transaction
      cp_BeginTrs()
      * --- Write into CON_TENZ
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.CON_TENZ_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CON_TENZ_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="COSERIAL"
        do vq_exec with '..\pcon\exe\query\GSCVAB22',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CON_TENZ_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="CON_TENZ.COSERIAL = _t2.COSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"COSTATUS ="+cp_NullLink(cp_ToStrODBC("CI"),'CON_TENZ','COSTATUS');
            +i_ccchkf;
            +" from "+i_cTable+" CON_TENZ, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="CON_TENZ.COSERIAL = _t2.COSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CON_TENZ, "+i_cQueryTable+" _t2 set ";
        +"CON_TENZ.COSTATUS ="+cp_NullLink(cp_ToStrODBC("CI"),'CON_TENZ','COSTATUS');
            +Iif(Empty(i_ccchkf),"",",CON_TENZ.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="CON_TENZ.COSERIAL = t2.COSERIAL";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CON_TENZ set (";
            +"COSTATUS";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +cp_NullLink(cp_ToStrODBC("CI"),'CON_TENZ','COSTATUS')+"";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="CON_TENZ.COSERIAL = _t2.COSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CON_TENZ set ";
        +"COSTATUS ="+cp_NullLink(cp_ToStrODBC("CI"),'CON_TENZ','COSTATUS');
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".COSERIAL = "+i_cQueryTable+".COSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"COSTATUS ="+cp_NullLink(cp_ToStrODBC("CI"),'CON_TENZ','COSTATUS');
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Write into INC_AVVE
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.INC_AVVE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.INC_AVVE_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.INC_AVVE_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"IARESINC ="+cp_NullLink(cp_ToStrODBC(0),'INC_AVVE','IARESINC');
            +i_ccchkf ;
        +" where ";
            +"IARESINC < "+cp_ToStrODBC(0);
               )
      else
        update (i_cTable) set;
            IARESINC = 0;
            &i_ccchkf. ;
         where;
            IARESINC < 0;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- commit
      cp_EndTrs(.t.)
      * --- Esecuzione ok
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("Aggiornamento status e residuo solleciti eseguito con successo%0")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
      this.oParentObject.w_PESEOK = .T.
      this.oParentObject.w_PMSG = this.oParentObject.w_PMSG + this.w_TMPC 
    endif
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='CON_TENZ'
    this.cWorkTables[2]='INC_AVVE'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
