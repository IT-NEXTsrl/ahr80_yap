* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvbb39                                                        *
*              Modifica tabella progressivi cpwarn (lettere d'intento)         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-05-12                                                      *
* Last revis.: 2008-06-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvbb39",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvbb39 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_MESS = space(10)
  w_LOOP = 0
  w_TABLECODE = space(254)
  w_CHIAVE = space(254)
  w_NEWCODE = space(254)
  w_POSIZALFA = 0
  w_ALFAAPPO = space(254)
  w_DOCUM = space(254)
  w_CODAZI = space(5)
  * --- WorkFile variables
  AZIENDA_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Try
    local bErr_0378A770
    bErr_0378A770=bTrsErr
    this.Try_0378A770()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = this.oParentObject.w_PMSG
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_0378A770
    * --- End
  endproc
  proc Try_0378A770()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
     
 DECLARE ARRPROG(2) 
 ARRPROG(1)="prog\PRDIN" 
 ARRPROG(2)="prog\PRDID"
    this.w_LOOP = 1
    this.w_CODAZI = i_CODAZI
     
 k_Conn=i_TableProp[this.AZIENDA_IDX, 3] 
 SqlExec(k_Conn, "Select * from CPWARN", "__CPWARN__")
    * --- begin transaction
    cp_BeginTrs()
    do while this.w_LOOP <= Alen(Arrprog, 1)
      this.w_TABLECODE = ""
      this.w_NEWCODE = ""
       
 Select("__CPWARN__") 
 Go Top 
 Scan
      if Left(TABLECODE, Len(Alltrim(ARRPROG( this.w_LOOP )))) = Alltrim(ARRPROG( this.w_LOOP )) And WARNCODE = i_CODAZI
        this.w_CHIAVE = TABLECODE
        this.w_TABLECODE = Alltrim(TABLECODE)
        if ARRPROG( this.w_LOOP )="prog\PRDID"
          this.w_POSIZALFA = Rat("'",Alltrim(Left(this.w_TABLECODE, Len(Alltrim(this.w_TABLECODE)))))-1
          this.w_ALFAAPPO = Left(this.w_TABLECODE, this.w_POSIZALFA)
          this.w_NEWCODE = Alltrim(this.w_ALFAAPPO)+"'"+SPACE(2)+"'"
        else
          this.w_NEWCODE = this.w_TABLECODE+"\"+"'"+SPACE(2)+"'"
        endif
        ah_Msg("Aggiornamento progressivo: %1",.t.,.f.,.t.,Alltrim(this.w_CHIAVE))
         
 SqlExec(k_Conn, "Update CPWARN set tablecode = "+cp_ToStrODBC(this.w_NEWCODE) +" where tablecode = "+cp_ToStrODBC(this.w_CHIAVE)+" and warncode = "+cp_ToStrODBC(this.w_CODAZI))
      endif
      EndScan
      this.w_LOOP = this.w_LOOP + 1
    enddo
    if Used("__CPWARN__")
       
 Select("__CPWARN__") 
 Use
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    if upper(CP_DBTYPE)="DB2"
      this.oParentObject.w_PMSG = ah_Msgformat("Elaborazione terminata con successo")
    else
      this.w_TMPC = ah_Msgformat("Modifica tabella progressivi CPWARN eseguito correttamente")
      this.oParentObject.w_PMSG = this.w_TMPC
    endif
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='AZIENDA'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
