* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvbb96                                                        *
*              Impostazione filtri su import documenti                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-05-21                                                      *
* Last revis.: 2008-07-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvbb96",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvbb96 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_MESS = space(100)
  w_FLNOFLIM = space(1)
  * --- WorkFile variables
  FILDTREG_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Procedura di coversione per l'impostazione filtri su import documenti.
    *     Nel caso in cui la data iniziale sia 0 occorre mettere il check ad intervallo [0-365] per mantenere il vecchio comportamento
    * --- variabile da testare
    * --- Se il daabase gestisce i varchar non serve la procedura di conversione
    * --- Try
    local bErr_037E7C50
    bErr_037E7C50=bTrsErr
    this.Try_037E7C50()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = this.oParentObject.w_PMSG
        this.w_TMPC = ah_Msgformat("ERRORE GENERICO - impossibile aggiornare filtri importazione")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_037E7C50
    * --- End
  endproc
  proc Try_037E7C50()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Read from FILDTREG
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.FILDTREG_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.FILDTREG_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "FLNOFLIM"+;
        " from "+i_cTable+" FILDTREG where ";
            +"FLCODAZI = "+cp_ToStrODBC(i_codazi);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        FLNOFLIM;
        from (i_cTable) where;
            FLCODAZI = i_codazi;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_FLNOFLIM = NVL(cp_ToDate(_read_.FLNOFLIM),cp_NullValue(_read_.FLNOFLIM))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- begin transaction
    cp_BeginTrs()
    if nvl(this.w_FLNOFLIM,"N")="N" or empty(this.w_FLNOFLIM)
      * --- Ripristino il vecchio comportamento intervallo [0,365]
      g_DTINIM = 365
      g_DTFIIM = 0
      * --- Write into FILDTREG
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.FILDTREG_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.FILDTREG_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.FILDTREG_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"FLNOFLIM ="+cp_NullLink(cp_ToStrODBC("S"),'FILDTREG','FLNOFLIM');
        +",FLDATIMP ="+cp_NullLink(cp_ToStrODBC(g_DTINIM),'FILDTREG','FLDATIMP');
        +",FLDATIMS ="+cp_NullLink(cp_ToStrODBC(g_DTFIIM),'FILDTREG','FLDATIMS');
            +i_ccchkf ;
        +" where ";
            +"FLCODAZI = "+cp_ToStrODBC(i_codazi);
               )
      else
        update (i_cTable) set;
            FLNOFLIM = "S";
            ,FLDATIMP = g_DTINIM;
            ,FLDATIMS = g_DTFIIM;
            &i_ccchkf. ;
         where;
            FLCODAZI = i_codazi;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    else
      this.w_TMPC = ah_Msgformat("ATTENZIONE: non esistono dati da aggiornare")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = this.w_TMPC
    * --- commit
    cp_EndTrs(.t.)
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='FILDTREG'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
