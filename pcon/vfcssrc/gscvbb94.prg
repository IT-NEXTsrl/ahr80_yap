* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvbb94                                                        *
*              Aumenta la lunghezza delle ragioni sociali                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-05-21                                                      *
* Last revis.: 2008-02-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,PAR1
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvbb94",oParentObject,m.PAR1)
return(i_retval)

define class tgscvbb94 as StdBatch
  * --- Local variables
  PAR1 = space(0)
  * --- WorkFile variables
  CLI_VEND_idx=0
  RIPATMP3_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.oParentObject.w_GOON2 = .F.
    * --- Procedura di conversione per DB2 (gli altri database riescono ad allargare i campi carattere senza bisogno di procedure di conversione)
    *     I campi allargati sono la reagione sociale nelle tabelle  CLI_VEND (� in gpos) 
    if this.PAR1="CONTROLLO"
       
 i_nConn=i_TableProp[this.CLI_VEND_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.CLI_VEND_idx,2])
      A=SQLEXEC(i_nConn,"Select  CLDESCRI from "+i_cTable + " where 1=0","CURS")
      afields(DATI2,"CURS")
      if Used("CURS")
        Select ("CURS") 
 use
      endif
      if DATI2(1,3)=60 
        this.oParentObject.w_GOON2 = .F.
      endif
    else
      * --- Converte tabella DB2
      i_nConn=i_TableProp[this.CLI_VEND_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.CLI_VEND_idx,2])
      pName="CLI_VEND"
      pDatabaseType=i_ServerConn[1,6]
      * --- Ricostruisco  la tabella prima di copiarne il contenuto  (si evita il rischio che la tabella copiata abbia dei campi in meno della tabella ricostruita)
      GSCV_BRT(this, "CLI_VEND" , i_nConn , .T.)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Create temporary table RIPATMP3
      i_nIdx=cp_AddTableDef('RIPATMP3') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      i_nConn=i_TableProp[this.CLI_VEND_idx,3] && recupera la connessione
      i_cTable=cp_SetAzi(i_TableProp[this.CLI_VEND_idx,2])
      cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
            )
      this.RIPATMP3_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      wait window "Creata tabella temporanea di appoggio" NOWAIT
      * --- Elimino la tabella
      if Not GSCV_BDT( this, "CLI_VEND" , i_nConn )
        * --- Raise
        i_Error="Error"
        return
      endif
      * --- Ricostruisco il database per la tabella
      GSCV_BRT(this, "CLI_VEND" , i_nConn , .T.)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Insert into CLI_VEND
      i_nConn=i_TableProp[this.CLI_VEND_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CLI_VEND_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP3_idx,2])
        i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.CLI_VEND_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Elimina tabella TMP
      * --- Drop temporary table RIPATMP3
      i_nIdx=cp_GetTableDefIdx('RIPATMP3')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('RIPATMP3')
      endif
      wait window "Eliminata tabella temporanea TMP" NOWAIT
    endif
  endproc


  proc Init(oParentObject,PAR1)
    this.PAR1=PAR1
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='CLI_VEND'
    this.cWorkTables[2]='*RIPATMP3'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="PAR1"
endproc
