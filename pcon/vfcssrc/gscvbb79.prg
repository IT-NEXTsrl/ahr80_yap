* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvbb79                                                        *
*              5.0 - inizializza campo sede in processi documentali            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-10-12                                                      *
* Last revis.: 2007-07-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvbb79",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvbb79 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  * --- WorkFile variables
  PRODDOCU_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- MODULO DOCUMENT MANAGEMENT
    *     5.0 - INIZIALIZZAZIONE CAMPO SEDE IN PROCESSI DOCUMENTALI
    * --- FIle di LOG
    * --- Try
    local bErr_035FBEC0
    bErr_035FBEC0=bTrsErr
    this.Try_035FBEC0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = AH_MSGFORMAT ("ERRORE - Impossibile eseguire aggiornamento richiesto")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_035FBEC0
    * --- End
  endproc
  proc Try_035FBEC0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Write into PRODDOCU
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PRODDOCU_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRODDOCU_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PRODDOCU_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PDTIPRIF ="+cp_NullLink(cp_ToStrODBC("NO"),'PRODDOCU','PDTIPRIF');
          +i_ccchkf ;
      +" where ";
          +"PDTIPRIF is null ";
             )
    else
      update (i_cTable) set;
          PDTIPRIF = "NO";
          &i_ccchkf. ;
       where;
          PDTIPRIF is null;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Write into PRODDOCU
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PRODDOCU_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRODDOCU_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PRODDOCU_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PDTIPRIF ="+cp_NullLink(cp_ToStrODBC("NO"),'PRODDOCU','PDTIPRIF');
          +i_ccchkf ;
      +" where ";
          +"PDTIPRIF = "+cp_ToStrODBC("");
             )
    else
      update (i_cTable) set;
          PDTIPRIF = "NO";
          &i_ccchkf. ;
       where;
          PDTIPRIF = "";

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Write into PRODDOCU
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PRODDOCU_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRODDOCU_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PRODDOCU_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PDTIPRIF ="+cp_NullLink(cp_ToStrODBC("NO"),'PRODDOCU','PDTIPRIF');
          +i_ccchkf ;
      +" where ";
          +"PDTIPRIF = "+cp_ToStrODBC(" ");
             )
    else
      update (i_cTable) set;
          PDTIPRIF = "NO";
          &i_ccchkf. ;
       where;
          PDTIPRIF = " ";

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    if this.w_NHF>=0
      this.w_TMPC = AH_MSGFORMAT ("- Inizializzazione campo sede PDTIPRIF su tabella processi documentali PRODDOCU eseguita con successo")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = this.w_TMPC
    * --- commit
    cp_EndTrs(.t.)
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='PRODDOCU'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
