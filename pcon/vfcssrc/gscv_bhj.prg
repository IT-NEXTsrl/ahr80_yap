* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscv_bhj                                                        *
*              Valorizzazione campo anno movimenti di commessa e aggiornamento progressivi*
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-06-10                                                      *
* Last revis.: 2008-06-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscv_bhj",oParentObject,m.w_NHF)
return(i_retval)

define class tgscv_bhj as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_MESS = space(10)
  w_TABLECODE = space(254)
  w_CHIAVE = space(254)
  w_NEWCODE = space(254)
  w_POSIZALFA = 0
  w_ALFAAPPO = space(254)
  w_DOCUM = space(254)
  w_CODAZI = space(5)
  w_VALPROG = space(10)
  * --- WorkFile variables
  MAT_MAST_idx=0
  AZIENDA_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Try
    local bErr_030191F8
    bErr_030191F8=bTrsErr
    this.Try_030191F8()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = this.oParentObject.w_PMSG
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_030191F8
    * --- End
  endproc
  proc Try_030191F8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.w_CODAZI = i_CODAZI
    this.w_VALPROG = "prog\NUMAT"
     
 k_Conn=i_TableProp[this.AZIENDA_IDX, 3] 
 SqlExec(k_Conn, "Select * from CPWARN", "__CPWARN__")
    * --- begin transaction
    cp_BeginTrs()
    this.w_TABLECODE = ""
    this.w_NEWCODE = ""
     
 Select("__CPWARN__") 
 Go Top 
 Scan
    if Left(TABLECODE, Len(Alltrim(this.w_VALPROG))) = Alltrim(this.w_VALPROG) And WARNCODE = i_CODAZI
      this.w_CHIAVE = alltrim(TABLECODE)
      if OCCURS("\",this.w_CHIAVE) = 3
        this.w_NEWCODE = LEFT(this.w_CHIAVE,RAT("\",this.w_CHIAVE))+"'"+g_CODESE+"'"+SUBSTR(this.w_CHIAVE,RAT("\",this.w_CHIAVE),LEN(this.w_CHIAVE)+1-RAT("\",this.w_CHIAVE))
        ah_Msg("Aggiornamento progressivo: %1",.t.,.f.,.t.,Alltrim(this.w_CHIAVE))
         
 SqlExec(k_Conn, "Update CPWARN set tablecode = "+cp_ToStrODBC(this.w_NEWCODE) +" where tablecode = "+cp_ToStrODBC(this.w_CHIAVE)+" and warncode = "+cp_ToStrODBC(this.w_CODAZI))
      endif
    endif
    EndScan
    if Used("__CPWARN__")
       
 Select("__CPWARN__") 
 Use
    endif
    * --- Aggiornamento campo anno movimenti di commessa
    ah_Msg("Aggiornamento campo anno movimenti di commessa %1",.t.,.f.,.t.,Alltrim(g_CODESE))
    * --- Write into MAT_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.MAT_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MAT_MAST_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.MAT_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MA__ANNO ="+cp_NullLink(cp_ToStrODBC(g_CODESE),'MAT_MAST','MA__ANNO');
          +i_ccchkf ;
      +" where ";
          +"1 = "+cp_ToStrODBC(1);
             )
    else
      update (i_cTable) set;
          MA__ANNO = g_CODESE;
          &i_ccchkf. ;
       where;
          1 = 1;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.w_TMPC = ah_Msgformat("Modifica tabella progressivi CPWARN eseguito correttamente")
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='MAT_MAST'
    this.cWorkTables[2]='AZIENDA'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
