* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscv_b30                                                        *
*              2001.1.0 - aggiornamento chiave moviripa                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_225]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-07-12                                                      *
* Last revis.: 2001-07-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscv_b30",oParentObject,m.w_NHF)
return(i_retval)

define class tgscv_b30 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  * --- WorkFile variables
  MOVIRIPA_idx=0
  RIPATMP1_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- FIle di LOG
    * --- Try
    local bErr_0372E670
    bErr_0372E670=bTrsErr
    this.Try_0372E670()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = this.oParentObject.w_PMSG
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_0372E670
    * --- End
  endproc
  proc Try_0372E670()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    i_nConn=i_TableProp[this.MOVIRIPA_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.MOVIRIPA_idx,2])
    if i_nConn<>0
      do case
        case upper(CP_DBTYPE)="SQLSERVER"
          * --- Caso SQLServer
          i_nConn=i_TableProp[this.MOVIRIPA_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.MOVIRIPA_idx,2])
          w_iRows = SQLExec(i_nConn,"DROP INDEX "+ALLTRIM(i_cTable)+; 
 "."+ALLTRIM(i_cTable)+"4")
          w_iRows = SQLExec(i_nConn,"DROP INDEX "+ALLTRIM(i_cTable)+; 
 "."+ALLTRIM(i_cTable)+"5")
          w_iRows = SQLExec(i_nConn,"DROP INDEX xxxMOVIRIPA.xxxMOVIRIPA4")
          w_iRows = SQLExec(i_nConn,"DROP INDEX xxxMOVIRIPA.xxxMOVIRIPA5")
          w_iRows = SQLExec(i_nConn,"ALTER TABLE "+i_cTable+; 
 " ALTER COLUMN MRNUMRIG int NULL")
          w_iRows = SQLExec(i_nConn,"ALTER TABLE "+i_cTable+; 
 " ALTER COLUMN MRCODICE char(15) NOT NULL")
          w_iRows = SQLExec(i_nConn,"ALTER TABLE "+i_cTable+; 
 " ALTER COLUMN MRSERRIF char(10) NOT NULL")
          w_iRows = SQLExec(i_nConn,"ALTER TABLE "+i_cTable+; 
 " ALTER COLUMN MRORDRIF smallint NOT NULL")
          w_iRows = SQLExec(i_nConn,"ALTER TABLE "+i_cTable+; 
 " ALTER COLUMN MRNUMRIF smallint NOT NULL")
          w_iRows = SQLExec(i_nConn,"ALTER TABLE xxxMOVIRIPA"+; 
 " ALTER COLUMN MRNUMRIG int NULL")
          w_iRows = SQLExec(i_nConn,"ALTER TABLE xxxMOVIRIPA"+; 
 " ALTER COLUMN MRCODICE char(15) NOT NULL")
          w_iRows = SQLExec(i_nConn,"ALTER TABLE xxxMOVIRIPA"+; 
 " ALTER COLUMN MRSERRIF char(10) NOT NULL")
          w_iRows = SQLExec(i_nConn,"ALTER TABLE xxxMOVIRIPA"+; 
 " ALTER COLUMN MRORDRIF smallint NOT NULL")
          w_iRows = SQLExec(i_nConn,"ALTER TABLE xxxMOVIRIPA"+; 
 " ALTER COLUMN MRNUMRIF smallint NOT NULL")
        case upper(CP_DBTYPE)="ORACLE"
          * --- Caso ORACLE
          w_iRows = SQLExec(i_nConn,"DROP INDEX "+ALLTRIM(i_cTable)+"4")
          w_iRows = SQLExec(i_nConn,"DROP INDEX "+ALLTRIM(i_cTable)+"5")
          w_iRows = SQLExec(i_nConn,"DROP INDEX xxxMOVIRIPA4")
          w_iRows = SQLExec(i_nConn,"DROP INDEX xxxMOVIRIPA5")
          i_nConn=i_TableProp[this.MOVIRIPA_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.MOVIRIPA_idx,2])
          w_iRows = SQLExec(i_nConn,"ALTER TABLE "+i_cTable+; 
 " MODIFY(MRNUMRIG NULL)")
          w_iRows = SQLExec(i_nConn,"ALTER TABLE "+i_cTable+; 
 " MODIFY(MRCODICE NOT NULL)")
          w_iRows = SQLExec(i_nConn,"ALTER TABLE "+i_cTable+; 
 " MODIFY(MRSERRIF NOT NULL)")
          w_iRows = SQLExec(i_nConn,"ALTER TABLE "+i_cTable+; 
 " MODIFY(MRORDRIF NOT NULL)")
          w_iRows = SQLExec(i_nConn,"ALTER TABLE "+i_cTable+; 
 " MODIFY(MRNUMRIF NOT NULL)")
          w_iRows = SQLExec(i_nConn,"ALTER TABLE xxxMOVIRIPA"+; 
 " MODIFY(MRNUMRIG NULL)")
          w_iRows = SQLExec(i_nConn,"ALTER TABLE xxxMOVIRIPA"+; 
 " MODIFY(MRCODICE NOT NULL)")
          w_iRows = SQLExec(i_nConn,"ALTER TABLE xxxMOVIRIPA"+; 
 " MODIFY(MRSERRIF NOT NULL)")
          w_iRows = SQLExec(i_nConn,"ALTER TABLE xxxMOVIRIPA"+; 
 " MODIFY(MRORDRIF NOT NULL)")
          w_iRows = SQLExec(i_nConn,"ALTER TABLE xxxMOVIRIPA"+; 
 " MODIFY(MRNUMRIF NOT NULL)")
        case upper(CP_DBTYPE)="DB2"
          * --- Caso DB2
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
      endcase
      if bTrsErr
        * --- Raise
        i_Error=MESSAGE()
        return
      else
        if this.w_NHF>=0
          if upper(CP_DBTYPE)="DB2"
            this.w_TMPC = ah_Msgformat("Elaborazione terminata con successo")
          else
            this.w_TMPC = ah_Msgformat("Adeguamento chiave primaria tabella MOVIRIPA eseguito correttamente%0ATTENZIONE: occorre andare in MANUTENZIONE DATABASE, selezionare la tabella%0MOVIRIPA e premere <OK>")
          endif
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    if upper(CP_DBTYPE)="DB2"
      this.oParentObject.w_PMSG = ah_Msgformat("Elaborazione terminata con successo")
    else
      this.w_TMPC = ah_Msgformat("Adeguamento chiave primaria tabella MOVIRIPA eseguito correttamente%0ATTENZIONE: occorre andare in MANUTENZIONE DATABASE, selezionare la tabella%0MOVIRIPA e premere <OK>")
      this.oParentObject.w_PMSG = this.w_TMPC
    endif
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella DB2
    pName="MOVIRIPA"
    pDatabaseType=i_ServerConn[1,6]
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.MOVIRIPA_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.MOVIRIPA_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
          )
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    * --- Elimino la tabella
    if Not GSCV_BDT( this, "MOVIRIPA" , i_nConn )
      * --- Raise
      i_Error="Error"
      return
    endif
    * --- Ricostruisco il database per la tabella
    GSCV_BRT(this, "MOVIRIPA" , i_nConn , .T.)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into MOVIRIPA
    i_nConn=i_TableProp[this.MOVIRIPA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOVIRIPA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.MOVIRIPA_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Errore nel trasferimento dati'
      return
    endif
    ah_Msg("Ripristinati valori di origine",.T.)
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    ah_Msg("Eliminata tabella temporanea tmp",.T.)
  endproc


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='MOVIRIPA'
    this.cWorkTables[2]='*RIPATMP1'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
