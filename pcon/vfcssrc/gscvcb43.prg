* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcb43                                                        *
*              D2b e B77 devono diventare a qt� e valore                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-04-24                                                      *
* Last revis.: 2009-05-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcb43",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcb43 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_NCONN = 0
  * --- WorkFile variables
  ART_ICOL_idx=0
  KEY_ARTI_idx=0
  OFFDATTI_idx=0
  DOC_DETT_idx=0
  RAP_PRES_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Modifica le voci del tariffario facendole diventare a qt� e valore
    this.w_NCONN = i_TableProp[this.ART_ICOL_idx,3] 
    if IsAlt()
      * --- Try
      local bErr_03619830
      bErr_03619830=bTrsErr
      this.Try_03619830()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
      endif
      bTrsErr=bTrsErr or bErr_03619830
      * --- End
      * --- Try
      local bErr_0361D970
      bErr_0361D970=bTrsErr
      this.Try_0361D970()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
      endif
      bTrsErr=bTrsErr or bErr_0361D970
      * --- End
      * --- Try
      local bErr_0375A070
      bErr_0375A070=bTrsErr
      this.Try_0375A070()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
      endif
      bTrsErr=bTrsErr or bErr_0375A070
      * --- End
      * --- Try
      local bErr_03766220
      bErr_03766220=bTrsErr
      this.Try_03766220()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
      endif
      bTrsErr=bTrsErr or bErr_03766220
      * --- End
      * --- Try
      local bErr_035FD450
      bErr_035FD450=bTrsErr
      this.Try_035FD450()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
      endif
      bTrsErr=bTrsErr or bErr_035FD450
      * --- End
      * --- Try
      local bErr_0379FA80
      bErr_0379FA80=bTrsErr
      this.Try_0379FA80()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
      endif
      bTrsErr=bTrsErr or bErr_0379FA80
      * --- End
      * --- Try
      local bErr_0360B090
      bErr_0360B090=bTrsErr
      this.Try_0360B090()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
      endif
      bTrsErr=bTrsErr or bErr_0360B090
      * --- End
      * --- Try
      local bErr_0361DD90
      bErr_0361DD90=bTrsErr
      this.Try_0361DD90()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
      endif
      bTrsErr=bTrsErr or bErr_0361DD90
      * --- End
      * --- Try
      local bErr_0361DEE0
      bErr_0361DEE0=bTrsErr
      this.Try_0361DEE0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
      endif
      bTrsErr=bTrsErr or bErr_0361DEE0
      * --- End
      * --- Try
      local bErr_0360CB90
      bErr_0360CB90=bTrsErr
      this.Try_0360CB90()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
      endif
      bTrsErr=bTrsErr or bErr_0360CB90
      * --- End
      * --- Try
      local bErr_03628C90
      bErr_03628C90=bTrsErr
      this.Try_03628C90()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
      endif
      bTrsErr=bTrsErr or bErr_03628C90
      * --- End
      * --- Try
      local bErr_035FA000
      bErr_035FA000=bTrsErr
      this.Try_035FA000()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
      endif
      bTrsErr=bTrsErr or bErr_035FA000
      * --- End
      * --- Try
      local bErr_035FFDC0
      bErr_035FFDC0=bTrsErr
      this.Try_035FFDC0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
      endif
      bTrsErr=bTrsErr or bErr_035FFDC0
      * --- End
      * --- Try
      local bErr_037563B0
      bErr_037563B0=bTrsErr
      this.Try_037563B0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
      endif
      bTrsErr=bTrsErr or bErr_037563B0
      * --- End
      * --- Try
      local bErr_0376F0A0
      bErr_0376F0A0=bTrsErr
      this.Try_0376F0A0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
      endif
      bTrsErr=bTrsErr or bErr_0376F0A0
      * --- End
      * --- Try
      local bErr_03750830
      bErr_03750830=bTrsErr
      this.Try_03750830()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
      endif
      bTrsErr=bTrsErr or bErr_03750830
      * --- End
      * --- Try
      local bErr_037906E0
      bErr_037906E0=bTrsErr
      this.Try_037906E0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
      endif
      bTrsErr=bTrsErr or bErr_037906E0
      * --- End
      * --- Try
      local bErr_035D3960
      bErr_035D3960=bTrsErr
      this.Try_035D3960()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
      endif
      bTrsErr=bTrsErr or bErr_035D3960
      * --- End
    else
      * --- Procedura di conversione eseguita con successo
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("Procedura di conversione eseguita con successo")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    * --- Resoconto - sempre positivo
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Elaborazione terminata con successo")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = ah_Msgformat("Elaborazione terminata con successo")
  endproc
  proc Try_03619830()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Modifica voce del tariffario
    * --- Write into ART_ICOL
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.ART_ICOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.ART_ICOL_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"ARTIPART ="+cp_NullLink(cp_ToStrODBC("FM"),'ART_ICOL','ARTIPART');
      +",ARUNMIS1 ="+cp_NullLink(cp_ToStrODBC("Ogn"),'ART_ICOL','ARUNMIS1');
          +i_ccchkf ;
      +" where ";
          +"ARCODART = "+cp_ToStrODBC("D2b");
             )
    else
      update (i_cTable) set;
          ARTIPART = "FM";
          ,ARUNMIS1 = "Ogn";
          &i_ccchkf. ;
       where;
          ARCODART = "D2b";

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Modificata voce del tariffario D2b")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    return
  proc Try_0361D970()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Modifica codice di ricerca voce del tariffario
    * --- Write into KEY_ARTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.KEY_ARTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"CATIPCON ="+cp_NullLink(cp_ToStrODBC("M"),'KEY_ARTI','CATIPCON');
      +",CA__TIPO ="+cp_NullLink(cp_ToStrODBC("M"),'KEY_ARTI','CA__TIPO');
          +i_ccchkf ;
      +" where ";
          +"CACODICE = "+cp_ToStrODBC("D2b");
             )
    else
      update (i_cTable) set;
          CATIPCON = "M";
          ,CA__TIPO = "M";
          &i_ccchkf. ;
       where;
          CACODICE = "D2b";

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Modificato codice di ricerca voce del tariffario D2b")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    return
  proc Try_0375A070()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Modifica dettaglio attivit�
    * --- Write into OFFDATTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.OFFDATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFFDATTI_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.OFFDATTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"DAUNIMIS ="+cp_NullLink(cp_ToStrODBC("Ogn"),'OFFDATTI','DAUNIMIS');
          +i_ccchkf ;
      +" where ";
          +"DACODATT = "+cp_ToStrODBC("D2b");
             )
    else
      update (i_cTable) set;
          DAUNIMIS = "Ogn";
          &i_ccchkf. ;
       where;
          DACODATT = "D2b";

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Modificati dettagli attivit� delle prestazioni D2b")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    return
  proc Try_03766220()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Modifica dettaglio documenti
    * --- Write into DOC_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVTIPRIG ="+cp_NullLink(cp_ToStrODBC("M"),'DOC_DETT','MVTIPRIG');
      +",MVUNIMIS ="+cp_NullLink(cp_ToStrODBC("Ogn"),'DOC_DETT','MVUNIMIS');
      +",MVFLTRAS ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLTRAS');
          +i_ccchkf ;
      +" where ";
          +"MVCODICE = "+cp_ToStrODBC("D2b");
             )
    else
      update (i_cTable) set;
          MVTIPRIG = "M";
          ,MVUNIMIS = "Ogn";
          ,MVFLTRAS = " ";
          &i_ccchkf. ;
       where;
          MVCODICE = "D2b";

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Modificati dettagli documenti delle prestazioni D2b")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    return
  proc Try_035FD450()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Modifica inserimento provvisorio delle prestazioni
    * --- Write into RAP_PRES
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.RAP_PRES_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.RAP_PRES_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.RAP_PRES_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PRUNIMIS ="+cp_NullLink(cp_ToStrODBC("Ogn"),'RAP_PRES','PRUNIMIS');
          +i_ccchkf ;
      +" where ";
          +"DACODATT = "+cp_ToStrODBC("D2b");
             )
    else
      update (i_cTable) set;
          PRUNIMIS = "Ogn";
          &i_ccchkf. ;
       where;
          DACODATT = "D2b";

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Modificati dettagli inserimento provvisorio delle prestazioni D2b")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    return
  proc Try_0379FA80()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Modifica codice di ricerca voce del tariffario
    * --- Write into KEY_ARTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.KEY_ARTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"CATIPCON ="+cp_NullLink(cp_ToStrODBC("M"),'KEY_ARTI','CATIPCON');
      +",CA__TIPO ="+cp_NullLink(cp_ToStrODBC("M"),'KEY_ARTI','CA__TIPO');
          +i_ccchkf ;
      +" where ";
          +"CACODICE = "+cp_ToStrODBC("D2bbis");
             )
    else
      update (i_cTable) set;
          CATIPCON = "M";
          ,CA__TIPO = "M";
          &i_ccchkf. ;
       where;
          CACODICE = "D2bbis";

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Modificato codice di ricerca voce del tariffario D2bbis")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    return
  proc Try_0360B090()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Modifica dettaglio attivit�
    * --- Write into OFFDATTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.OFFDATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFFDATTI_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.OFFDATTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"DAUNIMIS ="+cp_NullLink(cp_ToStrODBC("Ogn"),'OFFDATTI','DAUNIMIS');
          +i_ccchkf ;
      +" where ";
          +"DACODATT = "+cp_ToStrODBC("D2bbis");
             )
    else
      update (i_cTable) set;
          DAUNIMIS = "Ogn";
          &i_ccchkf. ;
       where;
          DACODATT = "D2bbis";

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Modificati dettagli attivit� delle prestazioni D2bbis")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    return
  proc Try_0361DD90()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Modifica dettaglio documenti
    * --- Write into DOC_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVTIPRIG ="+cp_NullLink(cp_ToStrODBC("M"),'DOC_DETT','MVTIPRIG');
      +",MVUNIMIS ="+cp_NullLink(cp_ToStrODBC("Ogn"),'DOC_DETT','MVUNIMIS');
      +",MVFLTRAS ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLTRAS');
          +i_ccchkf ;
      +" where ";
          +"MVCODICE = "+cp_ToStrODBC("D2bbis");
             )
    else
      update (i_cTable) set;
          MVTIPRIG = "M";
          ,MVUNIMIS = "Ogn";
          ,MVFLTRAS = " ";
          &i_ccchkf. ;
       where;
          MVCODICE = "D2bbis";

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Modificati dettagli documenti delle prestazioni D2bbis")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    return
  proc Try_0361DEE0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Modifica inserimento provvisorio delle prestazioni
    * --- Write into RAP_PRES
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.RAP_PRES_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.RAP_PRES_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.RAP_PRES_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PRUNIMIS ="+cp_NullLink(cp_ToStrODBC("Ogn"),'RAP_PRES','PRUNIMIS');
          +i_ccchkf ;
      +" where ";
          +"DACODATT = "+cp_ToStrODBC("D2bbis");
             )
    else
      update (i_cTable) set;
          PRUNIMIS = "Ogn";
          &i_ccchkf. ;
       where;
          DACODATT = "D2bbis";

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Modificati dettagli inserimento provvisorio delle prestazioni D2bbis")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    return
  proc Try_0360CB90()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Modifica codice di ricerca voce del tariffario
    * --- Write into KEY_ARTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.KEY_ARTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"CATIPCON ="+cp_NullLink(cp_ToStrODBC("M"),'KEY_ARTI','CATIPCON');
      +",CA__TIPO ="+cp_NullLink(cp_ToStrODBC("M"),'KEY_ARTI','CA__TIPO');
          +i_ccchkf ;
      +" where ";
          +"CACODICE = "+cp_ToStrODBC("D2bter");
             )
    else
      update (i_cTable) set;
          CATIPCON = "M";
          ,CA__TIPO = "M";
          &i_ccchkf. ;
       where;
          CACODICE = "D2bter";

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Modificato codice di ricerca voce del tariffario D2bter")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    return
  proc Try_03628C90()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Modifica dettaglio attivit�
    * --- Write into OFFDATTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.OFFDATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFFDATTI_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.OFFDATTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"DAUNIMIS ="+cp_NullLink(cp_ToStrODBC("Ogn"),'OFFDATTI','DAUNIMIS');
          +i_ccchkf ;
      +" where ";
          +"DACODATT = "+cp_ToStrODBC("D2bter");
             )
    else
      update (i_cTable) set;
          DAUNIMIS = "Ogn";
          &i_ccchkf. ;
       where;
          DACODATT = "D2bter";

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Modificati dettagli attivit� delle prestazioni D2bter")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    return
  proc Try_035FA000()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Modifica dettaglio documenti
    * --- Write into DOC_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVTIPRIG ="+cp_NullLink(cp_ToStrODBC("M"),'DOC_DETT','MVTIPRIG');
      +",MVUNIMIS ="+cp_NullLink(cp_ToStrODBC("Ogn"),'DOC_DETT','MVUNIMIS');
      +",MVFLTRAS ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLTRAS');
          +i_ccchkf ;
      +" where ";
          +"MVCODICE = "+cp_ToStrODBC("D2bter");
             )
    else
      update (i_cTable) set;
          MVTIPRIG = "M";
          ,MVUNIMIS = "Ogn";
          ,MVFLTRAS = " ";
          &i_ccchkf. ;
       where;
          MVCODICE = "D2bter";

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Modificati dettagli documenti delle prestazioni D2bter")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    return
  proc Try_035FFDC0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Modifica inserimento provvisorio delle prestazioni
    * --- Write into RAP_PRES
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.RAP_PRES_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.RAP_PRES_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.RAP_PRES_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PRUNIMIS ="+cp_NullLink(cp_ToStrODBC("Ogn"),'RAP_PRES','PRUNIMIS');
          +i_ccchkf ;
      +" where ";
          +"DACODATT = "+cp_ToStrODBC("D2bter");
             )
    else
      update (i_cTable) set;
          PRUNIMIS = "Ogn";
          &i_ccchkf. ;
       where;
          DACODATT = "D2bter";

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Modificati dettagli inserimento provvisorio delle prestazioni D2bter")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    return
  proc Try_037563B0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Modifica voce del tariffario
    * --- Write into ART_ICOL
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.ART_ICOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.ART_ICOL_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"ARTIPART ="+cp_NullLink(cp_ToStrODBC("FM"),'ART_ICOL','ARTIPART');
      +",ARUNMIS1 ="+cp_NullLink(cp_ToStrODBC("Ora"),'ART_ICOL','ARUNMIS1');
          +i_ccchkf ;
      +" where ";
          +"ARCODART = "+cp_ToStrODBC("B77");
             )
    else
      update (i_cTable) set;
          ARTIPART = "FM";
          ,ARUNMIS1 = "Ora";
          &i_ccchkf. ;
       where;
          ARCODART = "B77";

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Modificata voce del tariffario B77")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    return
  proc Try_0376F0A0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Modifica codice di ricerca voce del tariffario
    * --- Write into KEY_ARTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.KEY_ARTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"CATIPCON ="+cp_NullLink(cp_ToStrODBC("M"),'KEY_ARTI','CATIPCON');
      +",CA__TIPO ="+cp_NullLink(cp_ToStrODBC("M"),'KEY_ARTI','CA__TIPO');
          +i_ccchkf ;
      +" where ";
          +"CACODICE = "+cp_ToStrODBC("B77");
             )
    else
      update (i_cTable) set;
          CATIPCON = "M";
          ,CA__TIPO = "M";
          &i_ccchkf. ;
       where;
          CACODICE = "B77";

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Modificato codice di ricerca voce del tariffario B77")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    return
  proc Try_03750830()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Modifica dettaglio attivit�
    * --- Write into OFFDATTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.OFFDATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFFDATTI_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.OFFDATTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"DAUNIMIS ="+cp_NullLink(cp_ToStrODBC("Ora"),'OFFDATTI','DAUNIMIS');
          +i_ccchkf ;
      +" where ";
          +"DACODATT = "+cp_ToStrODBC("B77");
             )
    else
      update (i_cTable) set;
          DAUNIMIS = "Ora";
          &i_ccchkf. ;
       where;
          DACODATT = "B77";

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Modificati dettagli attivit� delle prestazioni B77")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    return
  proc Try_037906E0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Modifica dettaglio documenti
    * --- Write into DOC_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVTIPRIG ="+cp_NullLink(cp_ToStrODBC("M"),'DOC_DETT','MVTIPRIG');
      +",MVUNIMIS ="+cp_NullLink(cp_ToStrODBC("Ora"),'DOC_DETT','MVUNIMIS');
      +",MVFLTRAS ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLTRAS');
          +i_ccchkf ;
      +" where ";
          +"MVCODICE = "+cp_ToStrODBC("B77");
             )
    else
      update (i_cTable) set;
          MVTIPRIG = "M";
          ,MVUNIMIS = "Ora";
          ,MVFLTRAS = " ";
          &i_ccchkf. ;
       where;
          MVCODICE = "B77";

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Modificati dettagli documenti delle prestazioni B77")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    return
  proc Try_035D3960()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Modifica inserimento provvisorio delle prestazioni
    * --- Write into RAP_PRES
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.RAP_PRES_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.RAP_PRES_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.RAP_PRES_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PRUNIMIS ="+cp_NullLink(cp_ToStrODBC("Ora"),'RAP_PRES','PRUNIMIS');
          +i_ccchkf ;
      +" where ";
          +"DACODATT = "+cp_ToStrODBC("B77");
             )
    else
      update (i_cTable) set;
          PRUNIMIS = "Ora";
          &i_ccchkf. ;
       where;
          DACODATT = "B77";

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Modificati dettagli inserimento provvisorio delle prestazioni B77")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='KEY_ARTI'
    this.cWorkTables[3]='OFFDATTI'
    this.cWorkTables[4]='DOC_DETT'
    this.cWorkTables[5]='RAP_PRES'
    return(this.OpenAllTables(5))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
