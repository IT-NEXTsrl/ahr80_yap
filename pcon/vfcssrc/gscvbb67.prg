* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvbb67                                                        *
*              Valorizzazione sepersre                                         *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-03-17                                                      *
* Last revis.: 2007-06-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvbb67",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvbb67 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TMPC = space(10)
  w_TEMP = space(10)
  w_TIPDOC = space(5)
  w_TIPDOCCOL = space(5)
  w_PROGRE = space(5)
  w_SECODDES = space(5)
  w_SEPERSON = space(40)
  w_SEPERSRE = space(5)
  w_NOOK = .f.
  w_CODAZI = space(5)
  w_DPFLINEX = space(1)
  * --- WorkFile variables
  SEDIAZIE_idx=0
  DIPENDEN_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Valoriizzazione campo SEPERSRE
    * --- FIle di LOG
    * --- Try
    local bErr_035ECAE0
    bErr_035ECAE0=bTrsErr
    this.Try_035ECAE0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = Ah_MsgFormat("ERRORE GENERICO - Impossibile aggiornare tabella %1", "SEDIAZIE")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_035ECAE0
    * --- End
  endproc
  proc Try_035ECAE0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Aggiorno campo SEPERSRE 
    this.w_PROGRE = "001"
    this.w_CODAZI = i_CODAZI
    * --- Fisso la tipologia (Risorsa umana) a 'I' cio� interna (reparto)
    this.w_DPFLINEX = "I"
    * --- Select from SEDIAZIE
    i_nConn=i_TableProp[this.SEDIAZIE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SEDIAZIE_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select SECODDES,SEPERSON,SEPERSRE  from "+i_cTable+" SEDIAZIE ";
          +" where SECODAZI="+cp_ToStrODBC(this.w_CODAZI)+"";
           ,"_Curs_SEDIAZIE")
    else
      select SECODDES,SEPERSON,SEPERSRE from (i_cTable);
       where SECODAZI=this.w_CODAZI;
        into cursor _Curs_SEDIAZIE
    endif
    if used('_Curs_SEDIAZIE')
      select _Curs_SEDIAZIE
      locate for 1=1
      do while not(eof())
      this.w_SECODDES = _Curs_SEDIAZIE.SECODDES
      this.w_SEPERSON = _Curs_SEDIAZIE.SEPERSON
      this.w_SEPERSRE = _Curs_SEDIAZIE.SEPERSRE
      * --- Sposto il filtro in un comando condizionale, per evitare la creazione di una visual query, che 
      *     data la dimensione della tabella SEDIAZIE sarebbe sprecata.
      if NOT EMPTY(NVL(this.w_SEPERSON," ")) AND EMPTY(NVL(this.w_SEPERSRE," "))
        this.w_NOOK = .T.
        do while this.w_NOOK
          * --- accept error
          bTrsErr=.f.
          this.w_PROGRE = right("000"+alltrim(str(val(this.w_PROGRE) + 1,5,0)),3)
          * --- Try
          local bErr_03783AE0
          bErr_03783AE0=bTrsErr
          this.Try_03783AE0()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_03783AE0
          * --- End
        enddo
        * --- Write into SEDIAZIE
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.SEDIAZIE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SEDIAZIE_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.SEDIAZIE_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SEPERSRE ="+cp_NullLink(cp_ToStrODBC(this.w_PROGRE),'SEDIAZIE','SEPERSRE');
              +i_ccchkf ;
          +" where ";
              +"SECODAZI = "+cp_ToStrODBC(i_CODAZI);
              +" and SECODDES = "+cp_ToStrODBC(this.w_SECODDES);
                 )
        else
          update (i_cTable) set;
              SEPERSRE = this.w_PROGRE;
              &i_ccchkf. ;
           where;
              SECODAZI = i_CODAZI;
              and SECODDES = this.w_SECODDES;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
        select _Curs_SEDIAZIE
        continue
      enddo
      use
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    if this.w_NHF>=0
      this.w_TMPC = Ah_MsgFormat("Aggiornamento SEPERSRE avvenuto correttamente")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = Ah_MsgFormat("Conversione eseguita correttamente")
    return
  proc Try_03783AE0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into DIPENDEN
    i_nConn=i_TableProp[this.DIPENDEN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DIPENDEN_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DIPENDEN_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"DPCODICE"+",DPCOGNOM"+",DPFLINEX"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PROGRE),'DIPENDEN','DPCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SEPERSON),'DIPENDEN','DPCOGNOM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DPFLINEX),'DIPENDEN','DPFLINEX');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'DPCODICE',this.w_PROGRE,'DPCOGNOM',this.w_SEPERSON,'DPFLINEX',this.w_DPFLINEX)
      insert into (i_cTable) (DPCODICE,DPCOGNOM,DPFLINEX &i_ccchkf. );
         values (;
           this.w_PROGRE;
           ,this.w_SEPERSON;
           ,this.w_DPFLINEX;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    this.w_NOOK = .F.
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='SEDIAZIE'
    this.cWorkTables[2]='DIPENDEN'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_SEDIAZIE')
      use in _Curs_SEDIAZIE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
