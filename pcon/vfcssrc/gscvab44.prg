* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvab44                                                        *
*              Aggiorna chiave lotti                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_11]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-11-19                                                      *
* Last revis.: 2004-11-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvab44",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvab44 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_MESS = space(100)
  w_CONN = 0
  w_TABLE = space(50)
  * --- WorkFile variables
  LOTTIART_idx=0
  RIPATMP1_idx=0
  ART_ICOL_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questa procedura modifica la chiave della tabella LOTTIART aggiungendo
    *     LOCODART come ulteriore chiave primaria
    *     Setta anche a 'S' il ARFLESUL dell' anagrafica articoli per tutti i servizi
    * --- Metto a 'S' il flag ARFLESUL dei servizi
    * --- Try
    local bErr_03600750
    bErr_03600750=bTrsErr
    this.Try_03600750()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0 
        this.w_TMPC = this.oParentObject.w_PMSG
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_03600750
    * --- End
    * --- Aggiorno la chiave dei lotti
    * --- Try
    local bErr_0360EED0
    bErr_0360EED0=bTrsErr
    this.Try_0360EED0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = this.oParentObject.w_PMSG
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_0360EED0
    * --- End
  endproc
  proc Try_03600750()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Write into ART_ICOL
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.ART_ICOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
      i_cTempTable=cp_GetTempTableName(i_nConn)
      i_aIndex[1]='ARCODART'
      cp_CreateTempTable(i_nConn,i_cTempTable,"ARCODART "," from "+i_cQueryTable+" where ( ARTIPART in ( 'FM', 'FO', 'DE' ) )",.f.,@i_aIndex)
      i_cQueryTable=i_cTempTable
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ART_ICOL_idx,i_nConn)
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="ART_ICOL.ARCODART = _t2.ARCODART";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"ARFLESUL ="+cp_NullLink(cp_ToStrODBC("S"),'ART_ICOL','ARFLESUL');
          +i_ccchkf;
          +" from "+i_cTable+" ART_ICOL, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="ART_ICOL.ARCODART = _t2.ARCODART";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ART_ICOL, "+i_cQueryTable+" _t2 set ";
      +"ART_ICOL.ARFLESUL ="+cp_NullLink(cp_ToStrODBC("S"),'ART_ICOL','ARFLESUL');
          +Iif(Empty(i_ccchkf),"",",ART_ICOL.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="PostgreSQL"
        i_cWhere="ART_ICOL.ARCODART = _t2.ARCODART";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ART_ICOL set ";
      +"ARFLESUL ="+cp_NullLink(cp_ToStrODBC("S"),'ART_ICOL','ARFLESUL');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".ARCODART = "+i_cQueryTable+".ARCODART";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"ARFLESUL ="+cp_NullLink(cp_ToStrODBC("S"),'ART_ICOL','ARFLESUL');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    if bTrsErr
      * --- Raise
      i_Error=MESSAGE()
      return
    else
      * --- commit
      cp_EndTrs(.t.)
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("Aggiornamento campo ARFLESUL eseguito correttamente%0")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = ah_Msgformat("Aggiornamento campo ARFLESUL eseguito correttamente%0")
    return
  proc Try_0360EED0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    i_nConn=i_TableProp[this.LOTTIART_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.LOTTIART_idx,2])
    if i_nConn<>0
      do case
        case upper(CP_DBTYPE)="SQLSERVER"
          * --- Caso SQLServer
          w_iRows = SQLExec(i_nConn,"ALTER TABLE "+i_cTable+; 
 " ALTER COLUMN LOCODART CHAR(20) NOT NULL")
          w_iRows = SQLExec(i_nConn,"ALTER TABLE xxxLOTTIART"+; 
 " ALTER COLUMN LOCODART CHAR(20) NOT NULL")
        case upper(CP_DBTYPE)="ORACLE"
          * --- Caso ORACLE
          w_iRows = SQLExec(i_nConn,"ALTER TABLE "+i_cTable+; 
 " MODIFY(LOCODART NOT NULL)")
          w_iRows = SQLExec(i_nConn,"ALTER TABLE xxxLOTTIART"+; 
 " MODIFY(LOCODART NOT NULL)")
        case upper(CP_DBTYPE)="DB2"
          * --- Caso DB2
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
      endcase
      if bTrsErr
        * --- Raise
        i_Error=MESSAGE()
        return
      else
        if this.w_NHF>=0
          if upper(CP_DBTYPE)="DB2"
            this.w_TMPC = ah_Msgformat("Elaborazione terminata con successo")
          else
            this.w_TABLE = "LOTTIART"
            this.w_CONN = i_TableProp[this.LOTTIART_idx,3]
            * --- Ricostruisco il database per la tabella
            GSCV_BRT(this, "LOTTIART" , this.w_CONN , .T.)
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_TMPC = ah_Msgformat("Adeguamento chiave primaria tabella LOTTIART eseguito correttamente%0")
          endif
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    if upper(CP_DBTYPE)="DB2"
      this.oParentObject.w_PMSG = ah_Msgformat("Elaborazione terminata con successo")
    else
      this.w_TABLE = "LOTTIART"
      this.w_CONN = i_TableProp[this.LOTTIART_idx,3]
      GSCV_BRT(this, "LOTTIART" , this.w_CONN , .T.)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.oParentObject.w_PMSG = ah_Msgformat("Adeguamento chiave primaria tabella LOTTIART eseguito correttamente%0")
    endif
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella DB2
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.LOTTIART_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.LOTTIART_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
          )
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    * --- Elimino la tabella
    if Not GSCV_BDT( this, "LOTTIART" , i_nConn )
      * --- Raise
      i_Error="Error"
      return
    endif
    * --- Ricostruisco il database per la tabella
    GSCV_BRT(this, "LOTTIART" , i_nConn , .T.)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Try
    local bErr_0361EB10
    bErr_0361EB10=bTrsErr
    this.Try_0361EB10()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_0361EB10
    * --- End
    ah_Msg("Ripristinati valori di origine",.T.)
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    ah_Msg("Eliminata tabella temporanea tmp",.T.)
  endproc
  proc Try_0361EB10()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into LOTTIART
    i_nConn=i_TableProp[this.LOTTIART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.LOTTIART_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.LOTTIART_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='LOTTIART'
    this.cWorkTables[2]='*RIPATMP1'
    this.cWorkTables[3]='ART_ICOL'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
