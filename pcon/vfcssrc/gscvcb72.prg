* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcb72                                                        *
*              Aggiorna chiave primaria ELE_CONT                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-02-23                                                      *
* Last revis.: 2011-01-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcb72",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcb72 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_TROVATA = .f.
  w_CodRel = space(15)
  w_nCONN = 0
  w_cTABLE = space(10)
  w_nCONN2 = 0
  w_cTABLE2 = space(10)
  w_nCONN3 = 0
  w_cTABLE3 = space(10)
  w_NOMPRG = space(30)
  w_OLDPRG = space(8)
  * --- WorkFile variables
  ELE_CONT_idx=0
  DOC_GENE_idx=0
  DET_GEN_idx=0
  RIPATMP1_idx=0
  RIPATMP2_idx=0
  RIPATMP3_idx=0
  CON_TRAS_idx=0
  CONVERSI_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- FIle di LOG
    this.w_CodRel = "7.0-M3438"
    * --- Try
    local bErr_04676D30
    bErr_04676D30=bTrsErr
    this.Try_04676D30()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = this.oParentObject.w_PMSG
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_04676D30
    * --- End
  endproc
  proc Try_04676D30()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Legge se eseguita la conversione  7.0-M3438
    * --- Read from CONVERSI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CONVERSI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONVERSI_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "*"+;
        " from "+i_cTable+" CONVERSI where ";
            +"COCODREL = "+cp_ToStrODBC(this.w_CodRel);
            +" and COESEGUI = "+cp_ToStrODBC("S");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        *;
        from (i_cTable) where;
            COCODREL = this.w_CodRel;
            and COESEGUI = "S";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_TROVATA = i_Rows>0
    * --- begin transaction
    cp_BeginTrs()
    if NOT this.w_TROVATA
      this.w_nCONN = i_TableProp[this.ELE_CONT_idx,3]
      this.w_cTABLE = cp_SetAzi(i_TableProp[this.ELE_CONT_idx,2])
      this.w_nCONN2 = i_TableProp[this.DOC_GENE_idx,3]
      this.w_cTABLE2 = cp_SetAzi(i_TableProp[this.DOC_GENE_idx,2])
      this.w_nCONN3 = i_TableProp[this.DET_GEN_idx,3]
      this.w_cTABLE3 = cp_SetAzi(i_TableProp[this.DET_GEN_idx,2])
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Write into CON_TRAS
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.CON_TRAS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CON_TRAS_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.CON_TRAS_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"CORINCON ="+cp_NullLink(cp_ToStrODBC("M"),'CON_TRAS','CORINCON');
            +i_ccchkf ;
        +" where ";
            +"CORINCON = "+cp_ToStrODBC(" ");
               )
      else
        update (i_cTable) set;
            CORINCON = "M";
            &i_ccchkf. ;
         where;
            CORINCON = " ";

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    this.oParentObject.w_PMSG = ah_Msgformat("Elaborazione terminata con successo")
    this.oParentObject.w_PESEOK = .T.
    if this.w_NHF>=0
      this.w_TMPC = this.oParentObject.w_PMSG
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella DB2
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.ELE_CONT_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.ELE_CONT_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
          )
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Create temporary table RIPATMP2
    i_nIdx=cp_AddTableDef('RIPATMP2') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.DOC_GENE_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_GENE_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
          )
    this.RIPATMP2_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Create temporary table RIPATMP3
    i_nIdx=cp_AddTableDef('RIPATMP3') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.DET_GEN_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.DET_GEN_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
          )
    this.RIPATMP3_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    ah_Msg("Create tabelle temporanee di appoggio",.T.)
    * --- Elimino la tabella DOC_GENE
    if Not GSCV_BDT( this, "DOC_GENE" , i_nConn )
      * --- Raise
      i_Error="Error"
      return
    endif
    * --- Elimino la tabella DET_GEN
    if Not GSCV_BDT( this, "DET_GEN" , i_nConn )
      * --- Raise
      i_Error="Error"
      return
    endif
    * --- Elimino la tabella ELE_CONT
    if Not GSCV_BDT( this, "ELE_CONT" , i_nConn )
      * --- Raise
      i_Error="Error"
      return
    endif
    * --- Ricostruisco il database per la tabella ELE_CONT
    GSCV_BRT(this, "ELE_CONT" , i_nConn , .T.)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Ricostruisco il database per la tabella DOC_GENE
    GSCV_BRT(this, "DOC_GENE" , i_nConn , .T.)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Ricostruisco il database per la tabella DET_GEN
    GSCV_BRT(this, "DET_GEN" , i_nConn , .T.)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Try
    local bErr_04746860
    bErr_04746860=bTrsErr
    this.Try_04746860()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_04746860
    * --- End
    ah_Msg("Ripristinati valori di origine",.T.)
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    * --- Drop temporary table RIPATMP2
    i_nIdx=cp_GetTableDefIdx('RIPATMP2')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP2')
    endif
    * --- Drop temporary table RIPATMP3
    i_nIdx=cp_GetTableDefIdx('RIPATMP3')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP3')
    endif
    ah_Msg("Eliminata tabella temporanea tmp",.T.)
  endproc
  proc Try_04746860()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into ELE_CONT
    i_nConn=i_TableProp[this.ELE_CONT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ELE_CONT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.ELE_CONT_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Insert into DOC_GENE
    i_nConn=i_TableProp[this.DOC_GENE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_GENE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP2_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.DOC_GENE_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Insert into DET_GEN
    i_nConn=i_TableProp[this.DET_GEN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DET_GEN_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP3_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.DET_GEN_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,8)]
    this.cWorkTables[1]='ELE_CONT'
    this.cWorkTables[2]='DOC_GENE'
    this.cWorkTables[3]='DET_GEN'
    this.cWorkTables[4]='*RIPATMP1'
    this.cWorkTables[5]='*RIPATMP2'
    this.cWorkTables[6]='*RIPATMP3'
    this.cWorkTables[7]='CON_TRAS'
    this.cWorkTables[8]='CONVERSI'
    return(this.OpenAllTables(8))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
