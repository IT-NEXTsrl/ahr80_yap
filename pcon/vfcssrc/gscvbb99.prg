* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvbb99                                                        *
*              UNIFICAZIONE TABELLA FESTIVITA                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-01-14                                                      *
* Last revis.: 2009-02-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvbb99",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvbb99 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TMPC = space(10)
  w_TEMP = space(10)
  w_RECODICE = space(15)
  w_REDESCRI = space(40)
  * --- WorkFile variables
  FES_MAST_idx=0
  TMP_DETT_idx=0
  FES_DETT_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Copia i dati delle tabelle FES_MAST e FES_DETT per eliminarle
    *     e ricostruirle con struttura corretta, procedura di convesrione associata 5.0-M1538
    * --- Try
    local bErr_03750830
    bErr_03750830=bTrsErr
    this.Try_03750830()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = this.oParentObject.w_PMSG
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_03750830
    * --- End
  endproc
  proc Try_03750830()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    i_nConn=i_TableProp[this.FES_DETT_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.FES_DETT_idx,2])
    pName="FES_DETT"
    pDatabaseType=i_ServerConn[1,6]
    * --- Ricostruisco  la tabella prima di copiarne il contenuto  (si evita il rischio che la tabella copiata abbia dei campi in meno della tabella ricostruita)
    GSCV_BRT(this, "FES_DETT" , i_nConn , .F.)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Create temporary table TMP_DETT
    i_nIdx=cp_AddTableDef('TMP_DETT') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.FES_DETT_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.FES_DETT_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"FECODICE, FEDATFES, FEDESFES "," from "+i_cTable;
          )
    this.TMP_DETT_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    ah_msg("Creata tabelle temporanee di appoggio dettaglio")
    if Not GSCV_BDT( this, "FES_DETT" , i_nConn )
      * --- Raise
      i_Error="Error"
      return
    endif
    * --- Ricostruisco il database per la tabella
    GSCV_BRT(this, "FES_DETT" , i_nConn , .T.)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into FES_DETT
    i_nConn=i_TableProp[this.FES_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.FES_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.TMP_DETT_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.FES_DETT_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Drop temporary table TMP_DETT
    i_nIdx=cp_GetTableDefIdx('TMP_DETT')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMP_DETT')
    endif
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.w_TMPC = ah_Msgformat("Tabella festivitÓ creata correttamente")
    this.oParentObject.w_PMSG = ah_Msgformat("Conversione eseguita correttamente")
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='FES_MAST'
    this.cWorkTables[2]='*TMP_DETT'
    this.cWorkTables[3]='FES_DETT'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
