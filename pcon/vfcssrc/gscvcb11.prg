* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcb11                                                        *
*              Aggiornamento campi chiave temporaneo articoli                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-07-16                                                      *
* Last revis.: 2009-02-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcb11",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcb11 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TMPC = space(10)
  w_TEMP = space(10)
  w_PMSG = space(200)
  w_PHNAME = space(200)
  w_FRASESQL = space(200)
  w_Table = space(10)
  w_AZI = space(5)
  * --- WorkFile variables
  ART_TEMP_idx=0
  CONVERSI_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- FIle di LOG
    * --- ----------------------------------------------------------------------------------------------
    * --- Try
    local bErr_0374E2E0
    bErr_0374E2E0=bTrsErr
    this.Try_0374E2E0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Gestisce log errori
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF >=0
        this.w_PMSG = MESSAGE()
        this.w_TMPC = AH_MsgFormat("ERRORE GENERICO - %1",this.w_PMSG)
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_0374E2E0
    * --- End
  endproc
  proc Try_0374E2E0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    i_nConn=i_TableProp[this.ART_TEMP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
    * --- Elimino la tabella
    GSCV_BDT(this, "ART_TEMP" , i_nConn )
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    wait clear
    * --- Ricostruisco la tabella
    i_nConn=i_TableProp[this.CONVERSI_idx,3]
    GSCV_BRT(this, "ART_TEMP" , i_nConn , .t., "all" , .F.)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    ah_Msg("Ricostruita tabella di origine",.T.)
    this.w_TMPC = AH_MsgFormat("Elaborazione terminata con successo")
    this.oParentObject.w_PESEOK = .T.
    if this.w_NHF>=0
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='ART_TEMP'
    this.cWorkTables[2]='CONVERSI'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
