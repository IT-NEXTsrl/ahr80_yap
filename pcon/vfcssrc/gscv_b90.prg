* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscv_b90                                                        *
*              Aggiorna tabella actkey                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_364]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-02-11                                                      *
* Last revis.: 2003-02-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscv_b90",oParentObject,m.w_NHF)
return(i_retval)

define class tgscv_b90 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  * --- WorkFile variables
  ACTKEY_idx=0
  RIPATMP1_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- FIle di LOG
    * --- Try
    local bErr_0360A0A0
    bErr_0360A0A0=bTrsErr
    this.Try_0360A0A0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0 
        this.w_TMPC = this.oParentObject.w_PMSG
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_0360A0A0
    * --- End
  endproc
  proc Try_0360A0A0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    i_nConn=i_TableProp[this.ACTKEY_idx,3] 
 i_cTable=i_TableProp[this.ACTKEY_idx,2]
    if i_nConn<>0
      do case
        case upper(CP_DBTYPE)="DB2"
          * --- Caso DB2
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        case upper(CP_DBTYPE)="ORACLE"
          ah_ErrorMsg("Conversione prevista solo per database IBM DB2",,"")
          * --- commit
          cp_EndTrs(.t.)
        otherwise
          ah_ErrorMsg("Conversione prevista solo per database IBM DB2",,"")
          * --- commit
          cp_EndTrs(.t.)
      endcase
      if bTrsErr
        * --- Raise
        i_Error=MESSAGE()
        return
      else
        if this.w_NHF>=0 AND upper(CP_DBTYPE)="DB2"
          this.w_TMPC = ah_Msgformat("Adeguamento campo MATRICOLA tabella ACTKEY eseguito correttamente%0")
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.w_TMPC = ah_Msgformat("Adeguamento campo MATRICOLA tabella ACTKEY eseguito correttamente%0")
    this.oParentObject.w_PMSG = this.w_TMPC
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella DB2
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.ACTKEY_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.ACTKEY_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
          )
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    * --- Elimino la tabella
    if Not GSCV_BDT( this, "ACTKEY" , i_nConn )
      * --- Raise
      i_Error="Error"
      return
    endif
    * --- Ricostruisco il database per la tabella
    GSCV_BRT(this, "ACTKEY" , i_nConn , .T.)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Select from RIPATMP1
    i_nConn=i_TableProp[this.RIPATMP1_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" RIPATMP1 ";
           ,"_Curs_RIPATMP1")
    else
      select * from (i_cTable);
        into cursor _Curs_RIPATMP1
    endif
    if used('_Curs_RIPATMP1')
      select _Curs_RIPATMP1
      locate for 1=1
      do while not(eof())
      * --- Try
      local bErr_035FBD40
      bErr_035FBD40=bTrsErr
      this.Try_035FBD40()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_035FBD40
      * --- End
        select _Curs_RIPATMP1
        continue
      enddo
      use
    endif
    ah_Msg("Ripristinati valori di origine",.T.)
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    ah_Msg("Eliminata tabella temporanea tmp",.T.)
  endproc
  proc Try_035FBD40()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into ACTKEY
    i_nConn=i_TableProp[this.ACTKEY_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ACTKEY_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ACTKEY_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"MOCODICE"+",MORAGCLI"+",MOINDCLI"+",MOPIVCLI"+",MORAGINS"+",MOPIVINS"+",MONUMREL"+",MOMAXUTE"+",MOLISMOD"+",MOMATPRO"+",MOTIPDBF"+",MONOMDBF"+",MOACTKE1"+",MOACTKE2"+",MOACTKE3"+",MOACTKE4"+",MOADD001"+",MOADD002"+",MOADD003"+",MOADD004"+",MOADD005"+",MOADD006"+",MOADD007"+",MOADD008"+",MOADD009"+",MOADD010"+",MOADD011"+",MOADD012"+",MOADD013"+",MOADD014"+",MOADD015"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MOCODICE),'ACTKEY','MOCODICE');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MORAGCLI),'ACTKEY','MORAGCLI');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MOINDCLI),'ACTKEY','MOINDCLI');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MOPIVCLI),'ACTKEY','MOPIVCLI');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MORAGINS),'ACTKEY','MORAGINS');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MOPIVINS),'ACTKEY','MOPIVINS');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MONUMREL),'ACTKEY','MONUMREL');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MOMAXUTE),'ACTKEY','MOMAXUTE');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MOLISMOD),'ACTKEY','MOLISMOD');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MOMATPRO),'ACTKEY','MOMATPRO');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MOTIPDBF),'ACTKEY','MOTIPDBF');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MONOMDBF),'ACTKEY','MONOMDBF');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MOACTKE1),'ACTKEY','MOACTKE1');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MOACTKE2),'ACTKEY','MOACTKE2');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MOACTKE3),'ACTKEY','MOACTKE3');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MOACTKE4),'ACTKEY','MOACTKE4');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MOADD001),'ACTKEY','MOADD001');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MOADD002),'ACTKEY','MOADD002');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MOADD003),'ACTKEY','MOADD003');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MOADD004),'ACTKEY','MOADD004');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MOADD005),'ACTKEY','MOADD005');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MOADD006),'ACTKEY','MOADD006');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MOADD007),'ACTKEY','MOADD007');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MOADD008),'ACTKEY','MOADD008');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MOADD009),'ACTKEY','MOADD009');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MOADD010),'ACTKEY','MOADD010');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MOADD011),'ACTKEY','MOADD011');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MOADD012),'ACTKEY','MOADD012');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MOADD013),'ACTKEY','MOADD013');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MOADD014),'ACTKEY','MOADD014');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MOADD015),'ACTKEY','MOADD015');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'MOCODICE',_Curs_RIPATMP1.MOCODICE,'MORAGCLI',_Curs_RIPATMP1.MORAGCLI,'MOINDCLI',_Curs_RIPATMP1.MOINDCLI,'MOPIVCLI',_Curs_RIPATMP1.MOPIVCLI,'MORAGINS',_Curs_RIPATMP1.MORAGINS,'MOPIVINS',_Curs_RIPATMP1.MOPIVINS,'MONUMREL',_Curs_RIPATMP1.MONUMREL,'MOMAXUTE',_Curs_RIPATMP1.MOMAXUTE,'MOLISMOD',_Curs_RIPATMP1.MOLISMOD,'MOMATPRO',_Curs_RIPATMP1.MOMATPRO,'MOTIPDBF',_Curs_RIPATMP1.MOTIPDBF,'MONOMDBF',_Curs_RIPATMP1.MONOMDBF)
      insert into (i_cTable) (MOCODICE,MORAGCLI,MOINDCLI,MOPIVCLI,MORAGINS,MOPIVINS,MONUMREL,MOMAXUTE,MOLISMOD,MOMATPRO,MOTIPDBF,MONOMDBF,MOACTKE1,MOACTKE2,MOACTKE3,MOACTKE4,MOADD001,MOADD002,MOADD003,MOADD004,MOADD005,MOADD006,MOADD007,MOADD008,MOADD009,MOADD010,MOADD011,MOADD012,MOADD013,MOADD014,MOADD015 &i_ccchkf. );
         values (;
           _Curs_RIPATMP1.MOCODICE;
           ,_Curs_RIPATMP1.MORAGCLI;
           ,_Curs_RIPATMP1.MOINDCLI;
           ,_Curs_RIPATMP1.MOPIVCLI;
           ,_Curs_RIPATMP1.MORAGINS;
           ,_Curs_RIPATMP1.MOPIVINS;
           ,_Curs_RIPATMP1.MONUMREL;
           ,_Curs_RIPATMP1.MOMAXUTE;
           ,_Curs_RIPATMP1.MOLISMOD;
           ,_Curs_RIPATMP1.MOMATPRO;
           ,_Curs_RIPATMP1.MOTIPDBF;
           ,_Curs_RIPATMP1.MONOMDBF;
           ,_Curs_RIPATMP1.MOACTKE1;
           ,_Curs_RIPATMP1.MOACTKE2;
           ,_Curs_RIPATMP1.MOACTKE3;
           ,_Curs_RIPATMP1.MOACTKE4;
           ,_Curs_RIPATMP1.MOADD001;
           ,_Curs_RIPATMP1.MOADD002;
           ,_Curs_RIPATMP1.MOADD003;
           ,_Curs_RIPATMP1.MOADD004;
           ,_Curs_RIPATMP1.MOADD005;
           ,_Curs_RIPATMP1.MOADD006;
           ,_Curs_RIPATMP1.MOADD007;
           ,_Curs_RIPATMP1.MOADD008;
           ,_Curs_RIPATMP1.MOADD009;
           ,_Curs_RIPATMP1.MOADD010;
           ,_Curs_RIPATMP1.MOADD011;
           ,_Curs_RIPATMP1.MOADD012;
           ,_Curs_RIPATMP1.MOADD013;
           ,_Curs_RIPATMP1.MOADD014;
           ,_Curs_RIPATMP1.MOADD015;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='ACTKEY'
    this.cWorkTables[2]='*RIPATMP1'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_RIPATMP1')
      use in _Curs_RIPATMP1
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
