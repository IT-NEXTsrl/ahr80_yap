* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvab50                                                        *
*              Aggiorna conto corrente                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_13]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-12-21                                                      *
* Last revis.: 2004-12-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvab50",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvab50 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_MESS = space(100)
  * --- WorkFile variables
  BAN_CONTI_idx=0
  PAR_TITE_idx=0
  DOC_RATE_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questa procedura di conversione inserisce un record nel figlio integrato dell'anagrafica clienti/fornitori (GSAR_MCC)
    * --- FIle di LOG
    * --- Try
    local bErr_04A9B010
    bErr_04A9B010=bTrsErr
    this.Try_04A9B010()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE %1 - impossibile aggiornare i conti correnti", this.oParentObject.w_PMSG )
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_04A9B010
    * --- End
  endproc
  proc Try_04A9B010()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Insert into BAN_CONTI
    i_nConn=i_TableProp[this.BAN_CONTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.BAN_CONTI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSCVAB50",this.BAN_CONTI_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Write into PAR_TITE
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PAR_TITE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="PTSERIAL,PTROWORD,CPROWNUM"
      do vq_exec with 'GSCVAB50_1',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_TITE_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="PAR_TITE.PTSERIAL = _t2.PTSERIAL";
              +" and "+"PAR_TITE.PTROWORD = _t2.PTROWORD";
              +" and "+"PAR_TITE.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PTNUMCOR = _t2.ANNUMCOR";
          +i_ccchkf;
          +" from "+i_cTable+" PAR_TITE, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="PAR_TITE.PTSERIAL = _t2.PTSERIAL";
              +" and "+"PAR_TITE.PTROWORD = _t2.PTROWORD";
              +" and "+"PAR_TITE.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" PAR_TITE, "+i_cQueryTable+" _t2 set ";
          +"PAR_TITE.PTNUMCOR = _t2.ANNUMCOR";
          +Iif(Empty(i_ccchkf),"",",PAR_TITE.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="PAR_TITE.PTSERIAL = t2.PTSERIAL";
              +" and "+"PAR_TITE.PTROWORD = t2.PTROWORD";
              +" and "+"PAR_TITE.CPROWNUM = t2.CPROWNUM";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" PAR_TITE set (";
          +"PTNUMCOR";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.ANNUMCOR";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="PAR_TITE.PTSERIAL = _t2.PTSERIAL";
              +" and "+"PAR_TITE.PTROWORD = _t2.PTROWORD";
              +" and "+"PAR_TITE.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" PAR_TITE set ";
          +"PTNUMCOR = _t2.ANNUMCOR";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".PTSERIAL = "+i_cQueryTable+".PTSERIAL";
              +" and "+i_cTable+".PTROWORD = "+i_cQueryTable+".PTROWORD";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PTNUMCOR = (select ANNUMCOR from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Write into DOC_RATE
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_RATE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_RATE_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="RSSERIAL,RSNUMRAT"
      do vq_exec with 'GSCVAB50_2',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_RATE_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="DOC_RATE.RSSERIAL = _t2.RSSERIAL";
              +" and "+"DOC_RATE.RSNUMRAT = _t2.RSNUMRAT";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"RSBANAPP = _t2.RSBANAPP";
          +",RSCONCOR = _t2.RSCONCOR";
          +i_ccchkf;
          +" from "+i_cTable+" DOC_RATE, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="DOC_RATE.RSSERIAL = _t2.RSSERIAL";
              +" and "+"DOC_RATE.RSNUMRAT = _t2.RSNUMRAT";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_RATE, "+i_cQueryTable+" _t2 set ";
          +"DOC_RATE.RSBANAPP = _t2.RSBANAPP";
          +",DOC_RATE.RSCONCOR = _t2.RSCONCOR";
          +Iif(Empty(i_ccchkf),"",",DOC_RATE.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="DOC_RATE.RSSERIAL = t2.RSSERIAL";
              +" and "+"DOC_RATE.RSNUMRAT = t2.RSNUMRAT";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_RATE set (";
          +"RSBANAPP,";
          +"RSCONCOR";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.RSBANAPP,";
          +"t2.RSCONCOR";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="DOC_RATE.RSSERIAL = _t2.RSSERIAL";
              +" and "+"DOC_RATE.RSNUMRAT = _t2.RSNUMRAT";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_RATE set ";
          +"RSBANAPP = _t2.RSBANAPP";
          +",RSCONCOR = _t2.RSCONCOR";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".RSSERIAL = "+i_cQueryTable+".RSSERIAL";
              +" and "+i_cTable+".RSNUMRAT = "+i_cQueryTable+".RSNUMRAT";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"RSBANAPP = (select RSBANAPP from "+i_cQueryTable+" where "+i_cWhere+")";
          +",RSCONCOR = (select RSCONCOR from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    if bTrsErr
      * --- Raise
      i_Error=i_TrsMsg
      return
    endif
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = ah_Msgformat("Conversione eseguita correttamente")
    if this.w_NHF>=0 
      this.w_TMPC = this.oParentObject.w_PMSG
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='BAN_CONTI'
    this.cWorkTables[2]='PAR_TITE'
    this.cWorkTables[3]='DOC_RATE'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
