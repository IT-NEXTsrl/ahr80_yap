* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvab96                                                        *
*              Aggiorna codice agente partite di acconto                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_20]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-03-31                                                      *
* Last revis.: 2005-03-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvab96",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvab96 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_MESS = space(100)
  w_NUMAGG = 0
  * --- WorkFile variables
  PAR_TITE_idx=0
  RIPATMP1_idx=0
  PNT_DETT_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna PTCODAGE nelle partite di Acconto prendendo il codice agente nell'anagrafica
    *     Cliente\Fornitore se valorizzato
    * --- FIle di LOG
    this.w_NUMAGG = 0
    * --- Try
    local bErr_0375A5E0
    bErr_0375A5E0=bTrsErr
    this.Try_0375A5E0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE GENERICO - impossibile aggiornare partite")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_0375A5E0
    * --- End
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
  endproc
  proc Try_0375A5E0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('gscvab96',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Write into PAR_TITE
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PAR_TITE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_TITE_idx,i_nConn)
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="PAR_TITE.PTSERIAL = _t2.PTSERIAL";
              +" and "+"PAR_TITE.PTROWORD = _t2.PTROWORD";
              +" and "+"PAR_TITE.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PTCODAGE = _t2.CODAGE";
          +i_ccchkf;
          +" from "+i_cTable+" PAR_TITE, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="PAR_TITE.PTSERIAL = _t2.PTSERIAL";
              +" and "+"PAR_TITE.PTROWORD = _t2.PTROWORD";
              +" and "+"PAR_TITE.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" PAR_TITE, "+i_cQueryTable+" _t2 set ";
          +"PAR_TITE.PTCODAGE = _t2.CODAGE";
          +Iif(Empty(i_ccchkf),"",",PAR_TITE.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="PostgreSQL"
        i_cWhere="PAR_TITE.PTSERIAL = _t2.PTSERIAL";
              +" and "+"PAR_TITE.PTROWORD = _t2.PTROWORD";
              +" and "+"PAR_TITE.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" PAR_TITE set ";
          +"PTCODAGE = _t2.CODAGE";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".PTSERIAL = "+i_cQueryTable+".PTSERIAL";
              +" and "+i_cTable+".PTROWORD = "+i_cQueryTable+".PTROWORD";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PTCODAGE = (select CODAGE from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error='Errore Scrittura PAR_TITE'
      return
    endif
    this.w_NUMAGG = i_ROWS
    * --- Write into PNT_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PNT_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_cTempTable=cp_GetTempTableName(i_nConn)
      i_aIndex[1]='PNSERIAL,CPROWNUM'
      cp_CreateTempTable(i_nConn,i_cTempTable,"PTSERIAL AS PNSERIAL,PTROWORD AS CPROWNUM,CODAGE "," from "+i_cQueryTable+" where 1=1",.f.,@i_aIndex)
      i_cQueryTable=i_cTempTable
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.PNT_DETT_idx,i_nConn)
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="PNT_DETT.PNSERIAL = _t2.PNSERIAL";
              +" and "+"PNT_DETT.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PNCODAGE = _t2.CODAGE";
          +i_ccchkf;
          +" from "+i_cTable+" PNT_DETT, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="PNT_DETT.PNSERIAL = _t2.PNSERIAL";
              +" and "+"PNT_DETT.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" PNT_DETT, "+i_cQueryTable+" _t2 set ";
          +"PNT_DETT.PNCODAGE = _t2.CODAGE";
          +Iif(Empty(i_ccchkf),"",",PNT_DETT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="PostgreSQL"
        i_cWhere="PNT_DETT.PNSERIAL = _t2.PNSERIAL";
              +" and "+"PNT_DETT.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" PNT_DETT set ";
          +"PNCODAGE = _t2.CODAGE";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".PNSERIAL = "+i_cQueryTable+".PNSERIAL";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PNCODAGE = (select CODAGE from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error='Errore Scrittura PNT_DETT'
      return
    endif
    this.w_NUMAGG = this.w_NUMAGG+i_ROWS
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("- Aggiornamento tabella PAR_TITE eseguito con successo%0Variati %1 records%0", ALLTRIM(STR(this.w_NUMAGG)) )
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='PAR_TITE'
    this.cWorkTables[2]='*RIPATMP1'
    this.cWorkTables[3]='PNT_DETT'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
