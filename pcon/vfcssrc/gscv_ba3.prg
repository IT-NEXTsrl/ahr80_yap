* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscv_ba3                                                        *
*              Aggiornamento cotipeff, codesspe solleciti                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_311]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-10-18                                                      *
* Last revis.: 2001-10-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscv_ba3",oParentObject,m.w_NHF)
return(i_retval)

define class tgscv_ba3 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  * --- WorkFile variables
  DOC_DETT_idx=0
  CON_TENZ_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiornamento campi COTIPEFF, CODESSPE solleciti per i vecchi solleciti
    *     questi nuovi campi vanno valorizzati :
    *     COTIPEFF ='E' Tipologia effetti solo effetti
    *     CODESSPE ='C' Destinazione Spese Bancarie = Assegna Spese al Costo
    * --- Try
    local bErr_035EC2A0
    bErr_035EC2A0=bTrsErr
    this.Try_035EC2A0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0 
        this.w_TMPC = this.oParentObject.w_PMSG
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_035EC2A0
    * --- End
  endproc
  proc Try_035EC2A0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Write into CON_TENZ
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.CON_TENZ_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CON_TENZ_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.CON_TENZ_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"COTIPEFF ="+cp_NullLink(cp_ToStrODBC("E"),'CON_TENZ','COTIPEFF');
          +i_ccchkf ;
      +" where ";
          +"COTIPEFF is null ";
             )
    else
      update (i_cTable) set;
          COTIPEFF = "E";
          &i_ccchkf. ;
       where;
          COTIPEFF is null;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Write into CON_TENZ
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.CON_TENZ_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CON_TENZ_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.CON_TENZ_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"CODESSPE ="+cp_NullLink(cp_ToStrODBC("C"),'CON_TENZ','CODESSPE');
          +i_ccchkf ;
      +" where ";
          +"CODESSPE is null ";
             )
    else
      update (i_cTable) set;
          CODESSPE = "C";
          &i_ccchkf. ;
       where;
          CODESSPE is null;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    if bTrsErr
      * --- Raise
      i_Error=MESSAGE()
      return
    else
      * --- commit
      cp_EndTrs(.t.)
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("Adeguamento campi COTIPEFF, CODESSPE CON_TENZ eseguito correttamente")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = ah_Msgformat("Adeguamento campi COTIPEFF, CODESSPE CON_TENZ eseguito correttamente")
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='DOC_DETT'
    this.cWorkTables[2]='CON_TENZ'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
