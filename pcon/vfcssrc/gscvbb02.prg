* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvbb02                                                        *
*              Aggiornamento campi cespiti                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_7]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-11-08                                                      *
* Last revis.: 2005-11-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvbb02",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvbb02 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  * --- WorkFile variables
  CES_PITI_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiornamento campi Cespiti derivanti dalla Rel 3.0 (Valori di default)
    * --- Try
    local bErr_04A9C720
    bErr_04A9C720=bTrsErr
    this.Try_04A9C720()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE GENERICO - impossibile eseguire aggiornamento richiesto")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_04A9C720
    * --- End
  endproc
  proc Try_04A9C720()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Write into CES_PITI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.CES_PITI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CES_PITI_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="CECODICE"
      do vq_exec with 'gscvbb02',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.CES_PITI_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="CES_PITI.CECODICE = _t2.CECODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CECOECIV = _t2.CECOECIV";
          +",CEAMMIMM = _t2.CEAMMIMM";
          +",CEAMMIMC = _t2.CEAMMIMC";
          +",CENSOAMF  = _t2.CENSOAMF ";
          +",CENSOAMM = _t2.CENSOAMM";
          +i_ccchkf;
          +" from "+i_cTable+" CES_PITI, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="CES_PITI.CECODICE = _t2.CECODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CES_PITI, "+i_cQueryTable+" _t2 set ";
          +"CES_PITI.CECOECIV = _t2.CECOECIV";
          +",CES_PITI.CEAMMIMM = _t2.CEAMMIMM";
          +",CES_PITI.CEAMMIMC = _t2.CEAMMIMC";
          +",CES_PITI.CENSOAMF  = _t2.CENSOAMF ";
          +",CES_PITI.CENSOAMM = _t2.CENSOAMM";
          +Iif(Empty(i_ccchkf),"",",CES_PITI.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="CES_PITI.CECODICE = t2.CECODICE";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CES_PITI set (";
          +"CECOECIV,";
          +"CEAMMIMM,";
          +"CEAMMIMC,";
          +"CENSOAMF ,";
          +"CENSOAMM";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.CECOECIV,";
          +"t2.CEAMMIMM,";
          +"t2.CEAMMIMC,";
          +"t2.CENSOAMF ,";
          +"t2.CENSOAMM";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="CES_PITI.CECODICE = _t2.CECODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CES_PITI set ";
          +"CECOECIV = _t2.CECOECIV";
          +",CEAMMIMM = _t2.CEAMMIMM";
          +",CEAMMIMC = _t2.CEAMMIMC";
          +",CENSOAMF  = _t2.CENSOAMF ";
          +",CENSOAMM = _t2.CENSOAMM";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".CECODICE = "+i_cQueryTable+".CECODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CECOECIV = (select CECOECIV from "+i_cQueryTable+" where "+i_cWhere+")";
          +",CEAMMIMM = (select CEAMMIMM from "+i_cQueryTable+" where "+i_cWhere+")";
          +",CEAMMIMC = (select CEAMMIMC from "+i_cQueryTable+" where "+i_cWhere+")";
          +",CENSOAMF  = (select CENSOAMF  from "+i_cQueryTable+" where "+i_cWhere+")";
          +",CENSOAMM = (select CENSOAMM from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    this.w_TMPC = ah_Msgformat("Aggiornamento cespiti eseguito con successo")
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = this.w_TMPC
    * --- commit
    cp_EndTrs(.t.)
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='CES_PITI'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
