* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcb25                                                        *
*              Aggiornamento numero documento                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_100]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-11-08                                                      *
* Last revis.: 2009-01-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcb25",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcb25 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_MESS = space(10)
  w_RicTable = .f.
  w_TabellaDb2 = space(10)
  w_TableError = space(10)
  w_kConn = 0
  w_NRES = 0
  * --- WorkFile variables
  LDOCGENE_idx=0
  RIPATMP1_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiornamento campi Numero Documento della procedura
    *     Modificati da Numerico 6 a numerico 15
    *     Per Sql problema con valore di Default
    *     DB2 non permette la modifica quindi bisogna eseguire il solito travaso di dati
    *     Oraclo permette la modifica
    this.w_TableError = "LDOCGENE"
    this.w_kConn = i_TableProp[this.LDOCGENE_IDX, 3]
    * --- Try
    local bErr_0468E870
    bErr_0468E870=bTrsErr
    this.Try_0468E870()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = ah_Msgformat("ERRORE GENERICO - impossibile eseguire aggiornamento richiesto, tabella: %1 %2", this.w_TableError , Message() )
      if this.w_NHF>=0
        this.w_TMPC = this.oParentObject.w_PMSG
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
      this.oParentObject.w_PESEOK = .F.
    endif
    bTrsErr=bTrsErr or bErr_0468E870
    * --- End
  endproc
  proc Try_0468E870()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    do case
      case upper(CP_DBTYPE)="SQLSERVER"
        this.w_MESS = GSCV_BMN( this, "LDOCGENE", "LDNUMDOC", .t.)
        this.w_TMPC = this.w_TMPC + this.w_MESS
        if Not Empty(this.w_MESS) And this.w_NHF>0 
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_MESS)
        endif
      case upper(CP_DBTYPE)="DB2"
        this.w_TabellaDb2 = "LDOCGENE"
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        GSCV_BRI(this, this.w_TabellaDb2 , this.w_kConn , .T. )
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
    endcase
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = Empty(this.w_TMPC)
    if this.oParentObject.w_PESEOK And this.w_NHF>0
      this.w_MESS = Ah_Msgformat("Aggiornamento campi numero e alfa documento avvenuto correttamente")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_MESS)
    endif
    this.oParentObject.w_PMSG = this.w_MESS
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella DB2
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.LDOCGENE_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.LDOCGENE_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"* "," from "+i_cTable;
          )
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    * --- Elimino la tabella
    if Not GSCV_BDT( this, this.w_TabellaDb2 , this.w_kConn )
      * --- Raise
      i_Error="Error"
      return
    endif
    * --- Ricostruisco il database per la tabella
    GSCV_BRT(this, this.w_TabellaDb2 , this.w_kConn , .F.)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into LDOCGENE
    i_nConn=i_TableProp[this.LDOCGENE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.LDOCGENE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.LDOCGENE_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_Msg("Ripristinati valori di origine",.T.)
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    ah_Msg("Eliminata tabella temporanea tmp",.T.)
  endproc


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='LDOCGENE'
    this.cWorkTables[2]='*RIPATMP1'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
