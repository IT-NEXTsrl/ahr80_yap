* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvbb85                                                        *
*              Elimina copie allegati                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-01-14                                                      *
* Last revis.: 2009-04-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvbb85",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvbb85 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TMPC = space(10)
  w_TEMP = space(10)
  w_MESS = space(10)
  w_NMAXCOPIE = 0
  w_SERIALE = space(10)
  * --- WorkFile variables
  PRODINDI_idx=0
  PROMINDI_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elimina le eventuali copie relative alla gestione allegati, dovute all'esecuzione multipla della procedura
    *     di conversione associata a 5.0-E7329
    * --- Try
    local bErr_03015418
    bErr_03015418=bTrsErr
    this.Try_03015418()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE GENERICO - impossibile cancellare le copie degli allegati")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_03015418
    * --- End
  endproc
  proc Try_03015418()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.w_NMAXCOPIE = 2
    * --- Ciclo sul numero di copie. Presuppongo che esista almeno un allegato duplicato,
    *     nel caso in cui questo non corrispondesse al vero entro nel ciclo ed esco subito.
    do while this.w_NMAXCOPIE >= 2
      * --- Select from GSCVBB85
      do vq_exec with 'GSCVBB85',this,'_Curs_GSCVBB85','',.f.,.t.
      if used('_Curs_GSCVBB85')
        select _Curs_GSCVBB85
        locate for 1=1
        do while not(eof())
        this.w_SERIALE = _Curs_GSCVBB85.IDSERIAL
        * --- Cancello il dettaglio
        * --- Delete from PRODINDI
        i_nConn=i_TableProp[this.PRODINDI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PRODINDI_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"IDSERIAL = "+cp_ToStrODBC(this.w_SERIALE);
                 )
        else
          delete from (i_cTable) where;
                IDSERIAL = this.w_SERIALE;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        * --- Cancello la testata
        * --- Delete from PROMINDI
        i_nConn=i_TableProp[this.PROMINDI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"IDSERIAL = "+cp_ToStrODBC(this.w_SERIALE);
                 )
        else
          delete from (i_cTable) where;
                IDSERIAL = this.w_SERIALE;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        this.w_NMAXCOPIE = MAX(this.w_NMAXCOPIE,_Curs_GSCVBB85.TOTREC)
          select _Curs_GSCVBB85
          continue
        enddo
        use
      endif
      this.w_NMAXCOPIE = this.w_NMAXCOPIE - 1
    enddo
    * --- Esecuzione ok
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Eliminazione copia/e allegati")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = ah_Msgformat("Conversione eseguita correttamente")
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='PRODINDI'
    this.cWorkTables[2]='PROMINDI'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_GSCVBB85')
      use in _Curs_GSCVBB85
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
