* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcc33                                                        *
*              Allungamento numero registrazione in tabella PREMIDOC           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_100]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-11-08                                                      *
* Last revis.: 2013-11-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcc33",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcc33 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_MESS = space(10)
  w_TableError = space(10)
  * --- WorkFile variables
  runtime_filters = 2

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiornamento campo numero registrazione MVNUMREG in tabella PREMIDOC
    *     Modificati da numerico 6 a numerico 15
    *     Per Sql problema con valore di Default
    *     DB2 non permette la modifica quindi bisogna eseguire il solito travaso di dati
    *     Oracle permette la modifica
    this.w_TableError = "PREMIDOC"
    * --- Try
    local bErr_036199E0
    bErr_036199E0=bTrsErr
    this.Try_036199E0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = ah_Msgformat("ERRORE GENERICO - impossibile eseguire aggiornamento richiesto, tabella: %1 %2", this.w_TableError , Message() )
      if this.w_NHF>=0
        this.w_TMPC = this.oParentObject.w_PMSG
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
      this.oParentObject.w_PESEOK = .F.
    endif
    bTrsErr=bTrsErr or bErr_036199E0
    * --- End
  endproc
  proc Try_036199E0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if upper(CP_DBTYPE)="SQLSERVER"
      this.w_MESS = GSCV_BMN( this, "PREMIDOC", "MVNUMREG", .t.)
      this.w_TMPC = this.w_TMPC + this.w_MESS
      if Not Empty(this.w_MESS) And this.w_NHF>0 
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_MESS)
      endif
    endif
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = Empty(this.w_TMPC)
    if this.oParentObject.w_PESEOK And this.w_NHF>0
      this.w_MESS = Ah_Msgformat("Aggiornamento numero registrazione avvenuto correttamente")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_MESS)
    endif
    this.oParentObject.w_PMSG = this.w_MESS
    return

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
