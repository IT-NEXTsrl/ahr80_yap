* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvab43                                                        *
*              Valorizza righe descrittive da varia note                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_29]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-11-11                                                      *
* Last revis.: 2004-11-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvab43",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvab43 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_MESS = space(100)
  * --- WorkFile variables
  CAT_CESP_idx=0
  CES_PITI_idx=0
  DOC_DETT_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questa procedura valorizza alcuni campi nelle righe descrittive create con il bottone varia note
    * --- FIle di LOG
    * --- Try
    local bErr_04A9F9C0
    bErr_04A9F9C0=bTrsErr
    this.Try_04A9F9C0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE %1 - impossibile aggiornare i documenti", this.oParentObject.w_PMSG )
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_04A9F9C0
    * --- End
  endproc
  proc Try_04A9F9C0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Write into DOC_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="MVSERIAL,CPROWNUM,MVNUMRIF"
      do vq_exec with 'GSCVAB43',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
              +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
              +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MVFLRAGG = _t2.MVFLRAGG ";
          +",MVCODCLA = _t2.MVCODCLA";
          +",MVDATEVA = _t2.MVDATEVA";
          +",MVCAUMAG = _t2.MVCAUMAG";
      +",MVCODMAG ="+cp_NullLink(cp_ToStrODBC(SPACE(5)),'DOC_DETT','MVCODMAG');
      +",MVCODMAT ="+cp_NullLink(cp_ToStrODBC(SPACE(5)),'DOC_DETT','MVCODMAT');
          +",MVCAUCOL = _t2.MVCAUCOL";
          +",MVFLCASC = _t2.MVFLCASC";
          +",MVFLORDI = _t2.MVFLORDI";
          +",MVFLIMPE = _t2.MVFLIMPE";
          +",MVFLRISE = _t2.MVFLRISE";
          +",MVF2CASC = _t2.MVF2CASC";
          +",MVF2ORDI = _t2.MVF2ORDI";
          +",MVF2IMPE = _t2.MVF2IMPE";
          +",MVF2RISE = _t2.MVF2RISE";
      +",MVCODCOM ="+cp_NullLink(cp_ToStrODBC(SPACE(15)),'DOC_DETT','MVCODCOM');
      +",MVINICOM ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'DOC_DETT','MVINICOM');
      +",MVFINCOM ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'DOC_DETT','MVFINCOM');
      +",MVKEYSAL ="+cp_NullLink(cp_ToStrODBC(SPACE(20)),'DOC_DETT','MVKEYSAL');
      +",MVPREZZO ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVPREZZO');
      +",MVQTAMOV ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAMOV');
      +",MVQTAUM1 ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAUM1');
      +",MVQTAIMP ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAIMP');
      +",MVQTAIM1 ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAIM1');
      +",MVVALRIG ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVVALRIG');
      +",MVVALMAG ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVVALMAG');
      +",MVIMPNAZ ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVIMPNAZ');
      +",MVVALULT ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVVALULT');
      +",MVFLULPV ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLULPV');
      +",MVIMPCOM ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVIMPCOM');
          +i_ccchkf;
          +" from "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
              +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
              +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 set ";
          +"DOC_DETT.MVFLRAGG = _t2.MVFLRAGG ";
          +",DOC_DETT.MVCODCLA = _t2.MVCODCLA";
          +",DOC_DETT.MVDATEVA = _t2.MVDATEVA";
          +",DOC_DETT.MVCAUMAG = _t2.MVCAUMAG";
      +",DOC_DETT.MVCODMAG ="+cp_NullLink(cp_ToStrODBC(SPACE(5)),'DOC_DETT','MVCODMAG');
      +",DOC_DETT.MVCODMAT ="+cp_NullLink(cp_ToStrODBC(SPACE(5)),'DOC_DETT','MVCODMAT');
          +",DOC_DETT.MVCAUCOL = _t2.MVCAUCOL";
          +",DOC_DETT.MVFLCASC = _t2.MVFLCASC";
          +",DOC_DETT.MVFLORDI = _t2.MVFLORDI";
          +",DOC_DETT.MVFLIMPE = _t2.MVFLIMPE";
          +",DOC_DETT.MVFLRISE = _t2.MVFLRISE";
          +",DOC_DETT.MVF2CASC = _t2.MVF2CASC";
          +",DOC_DETT.MVF2ORDI = _t2.MVF2ORDI";
          +",DOC_DETT.MVF2IMPE = _t2.MVF2IMPE";
          +",DOC_DETT.MVF2RISE = _t2.MVF2RISE";
      +",DOC_DETT.MVCODCOM ="+cp_NullLink(cp_ToStrODBC(SPACE(15)),'DOC_DETT','MVCODCOM');
      +",DOC_DETT.MVINICOM ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'DOC_DETT','MVINICOM');
      +",DOC_DETT.MVFINCOM ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'DOC_DETT','MVFINCOM');
      +",DOC_DETT.MVKEYSAL ="+cp_NullLink(cp_ToStrODBC(SPACE(20)),'DOC_DETT','MVKEYSAL');
      +",DOC_DETT.MVPREZZO ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVPREZZO');
      +",DOC_DETT.MVQTAMOV ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAMOV');
      +",DOC_DETT.MVQTAUM1 ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAUM1');
      +",DOC_DETT.MVQTAIMP ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAIMP');
      +",DOC_DETT.MVQTAIM1 ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAIM1');
      +",DOC_DETT.MVVALRIG ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVVALRIG');
      +",DOC_DETT.MVVALMAG ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVVALMAG');
      +",DOC_DETT.MVIMPNAZ ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVIMPNAZ');
      +",DOC_DETT.MVVALULT ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVVALULT');
      +",DOC_DETT.MVFLULPV ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLULPV');
      +",DOC_DETT.MVIMPCOM ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVIMPCOM');
          +Iif(Empty(i_ccchkf),"",",DOC_DETT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="DOC_DETT.MVSERIAL = t2.MVSERIAL";
              +" and "+"DOC_DETT.CPROWNUM = t2.CPROWNUM";
              +" and "+"DOC_DETT.MVNUMRIF = t2.MVNUMRIF";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set (";
          +"MVFLRAGG,";
          +"MVCODCLA,";
          +"MVDATEVA,";
          +"MVCAUMAG,";
          +"MVCODMAG,";
          +"MVCODMAT,";
          +"MVCAUCOL,";
          +"MVFLCASC,";
          +"MVFLORDI,";
          +"MVFLIMPE,";
          +"MVFLRISE,";
          +"MVF2CASC,";
          +"MVF2ORDI,";
          +"MVF2IMPE,";
          +"MVF2RISE,";
          +"MVCODCOM,";
          +"MVINICOM,";
          +"MVFINCOM,";
          +"MVKEYSAL,";
          +"MVPREZZO,";
          +"MVQTAMOV,";
          +"MVQTAUM1,";
          +"MVQTAIMP,";
          +"MVQTAIM1,";
          +"MVVALRIG,";
          +"MVVALMAG,";
          +"MVIMPNAZ,";
          +"MVVALULT,";
          +"MVFLULPV,";
          +"MVIMPCOM";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.MVFLRAGG ,";
          +"t2.MVCODCLA,";
          +"t2.MVDATEVA,";
          +"t2.MVCAUMAG,";
          +cp_NullLink(cp_ToStrODBC(SPACE(5)),'DOC_DETT','MVCODMAG')+",";
          +cp_NullLink(cp_ToStrODBC(SPACE(5)),'DOC_DETT','MVCODMAT')+",";
          +"t2.MVCAUCOL,";
          +"t2.MVFLCASC,";
          +"t2.MVFLORDI,";
          +"t2.MVFLIMPE,";
          +"t2.MVFLRISE,";
          +"t2.MVF2CASC,";
          +"t2.MVF2ORDI,";
          +"t2.MVF2IMPE,";
          +"t2.MVF2RISE,";
          +cp_NullLink(cp_ToStrODBC(SPACE(15)),'DOC_DETT','MVCODCOM')+",";
          +cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'DOC_DETT','MVINICOM')+",";
          +cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'DOC_DETT','MVFINCOM')+",";
          +cp_NullLink(cp_ToStrODBC(SPACE(20)),'DOC_DETT','MVKEYSAL')+",";
          +cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVPREZZO')+",";
          +cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAMOV')+",";
          +cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAUM1')+",";
          +cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAIMP')+",";
          +cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAIM1')+",";
          +cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVVALRIG')+",";
          +cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVVALMAG')+",";
          +cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVIMPNAZ')+",";
          +cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVVALULT')+",";
          +cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLULPV')+",";
          +cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVIMPCOM')+"";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
              +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
              +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set ";
          +"MVFLRAGG = _t2.MVFLRAGG ";
          +",MVCODCLA = _t2.MVCODCLA";
          +",MVDATEVA = _t2.MVDATEVA";
          +",MVCAUMAG = _t2.MVCAUMAG";
      +",MVCODMAG ="+cp_NullLink(cp_ToStrODBC(SPACE(5)),'DOC_DETT','MVCODMAG');
      +",MVCODMAT ="+cp_NullLink(cp_ToStrODBC(SPACE(5)),'DOC_DETT','MVCODMAT');
          +",MVCAUCOL = _t2.MVCAUCOL";
          +",MVFLCASC = _t2.MVFLCASC";
          +",MVFLORDI = _t2.MVFLORDI";
          +",MVFLIMPE = _t2.MVFLIMPE";
          +",MVFLRISE = _t2.MVFLRISE";
          +",MVF2CASC = _t2.MVF2CASC";
          +",MVF2ORDI = _t2.MVF2ORDI";
          +",MVF2IMPE = _t2.MVF2IMPE";
          +",MVF2RISE = _t2.MVF2RISE";
      +",MVCODCOM ="+cp_NullLink(cp_ToStrODBC(SPACE(15)),'DOC_DETT','MVCODCOM');
      +",MVINICOM ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'DOC_DETT','MVINICOM');
      +",MVFINCOM ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'DOC_DETT','MVFINCOM');
      +",MVKEYSAL ="+cp_NullLink(cp_ToStrODBC(SPACE(20)),'DOC_DETT','MVKEYSAL');
      +",MVPREZZO ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVPREZZO');
      +",MVQTAMOV ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAMOV');
      +",MVQTAUM1 ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAUM1');
      +",MVQTAIMP ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAIMP');
      +",MVQTAIM1 ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAIM1');
      +",MVVALRIG ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVVALRIG');
      +",MVVALMAG ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVVALMAG');
      +",MVIMPNAZ ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVIMPNAZ');
      +",MVVALULT ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVVALULT');
      +",MVFLULPV ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLULPV');
      +",MVIMPCOM ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVIMPCOM');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
              +" and "+i_cTable+".MVNUMRIF = "+i_cQueryTable+".MVNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MVFLRAGG = (select MVFLRAGG  from "+i_cQueryTable+" where "+i_cWhere+")";
          +",MVCODCLA = (select MVCODCLA from "+i_cQueryTable+" where "+i_cWhere+")";
          +",MVDATEVA = (select MVDATEVA from "+i_cQueryTable+" where "+i_cWhere+")";
          +",MVCAUMAG = (select MVCAUMAG from "+i_cQueryTable+" where "+i_cWhere+")";
      +",MVCODMAG ="+cp_NullLink(cp_ToStrODBC(SPACE(5)),'DOC_DETT','MVCODMAG');
      +",MVCODMAT ="+cp_NullLink(cp_ToStrODBC(SPACE(5)),'DOC_DETT','MVCODMAT');
          +",MVCAUCOL = (select MVCAUCOL from "+i_cQueryTable+" where "+i_cWhere+")";
          +",MVFLCASC = (select MVFLCASC from "+i_cQueryTable+" where "+i_cWhere+")";
          +",MVFLORDI = (select MVFLORDI from "+i_cQueryTable+" where "+i_cWhere+")";
          +",MVFLIMPE = (select MVFLIMPE from "+i_cQueryTable+" where "+i_cWhere+")";
          +",MVFLRISE = (select MVFLRISE from "+i_cQueryTable+" where "+i_cWhere+")";
          +",MVF2CASC = (select MVF2CASC from "+i_cQueryTable+" where "+i_cWhere+")";
          +",MVF2ORDI = (select MVF2ORDI from "+i_cQueryTable+" where "+i_cWhere+")";
          +",MVF2IMPE = (select MVF2IMPE from "+i_cQueryTable+" where "+i_cWhere+")";
          +",MVF2RISE = (select MVF2RISE from "+i_cQueryTable+" where "+i_cWhere+")";
      +",MVCODCOM ="+cp_NullLink(cp_ToStrODBC(SPACE(15)),'DOC_DETT','MVCODCOM');
      +",MVINICOM ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'DOC_DETT','MVINICOM');
      +",MVFINCOM ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'DOC_DETT','MVFINCOM');
      +",MVKEYSAL ="+cp_NullLink(cp_ToStrODBC(SPACE(20)),'DOC_DETT','MVKEYSAL');
      +",MVPREZZO ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVPREZZO');
      +",MVQTAMOV ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAMOV');
      +",MVQTAUM1 ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAUM1');
      +",MVQTAIMP ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAIMP');
      +",MVQTAIM1 ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAIM1');
      +",MVVALRIG ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVVALRIG');
      +",MVVALMAG ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVVALMAG');
      +",MVIMPNAZ ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVIMPNAZ');
      +",MVVALULT ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVVALULT');
      +",MVFLULPV ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLULPV');
      +",MVIMPCOM ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVIMPCOM');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    if bTrsErr
      * --- Raise
      i_Error=i_TrsMsg
      return
    endif
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = ah_Msgformat("Conversione eseguita correttamente")
    if this.w_NHF>=0 
      this.w_TMPC = this.oParentObject.w_PMSG
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='CAT_CESP'
    this.cWorkTables[2]='CES_PITI'
    this.cWorkTables[3]='DOC_DETT'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
