* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvbb55                                                        *
*              Aggiorna documenti con mvqtasal negativo                        *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-03-17                                                      *
* Last revis.: 2003-03-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvbb55",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvbb55 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TMPC = space(10)
  w_TEMP = space(10)
  * --- WorkFile variables
  DOC_DETT_idx=0
  TMPVEND1_idx=0
  SALDIART_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Procedura per azzerrare MVQTASAL se negativo con conseguente
    *     storno dei saldi di magazzino
    * --- FIle di LOG
    * --- Try
    local bErr_04C02608
    bErr_04C02608=bTrsErr
    this.Try_04C02608()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = "ERRORE GENERICO - Impossibile completare conversione "
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_04C02608
    * --- End
  endproc
  proc Try_04C02608()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Creo il temporaneo con le righe aventi MVQTASAL negativo ed evase
    *     senza importazione
    * --- Create temporary table TMPVEND1
    i_nIdx=cp_AddTableDef('TMPVEND1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('..\PCON\EXE\QUERY\GSCVBB55',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPVEND1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Metto a zero MVQTASAL per queste righe..
    * --- Write into DOC_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
              +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
              +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVQTASAL ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTASAL');
          +i_ccchkf;
          +" from "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
              +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
              +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 set ";
      +"DOC_DETT.MVQTASAL ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTASAL');
          +Iif(Empty(i_ccchkf),"",",DOC_DETT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="PostgreSQL"
        i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
              +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
              +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set ";
      +"MVQTASAL ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTASAL');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
              +" and "+i_cTable+".MVNUMRIF = "+i_cQueryTable+".MVNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVQTASAL ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTASAL');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Storno i saldi di magazzino...
    * --- Write into SALDIART
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SALDIART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="SLCODICE,SLCODMAG"
      do vq_exec with 'gscvbb55_b',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="SALDIART.SLCODICE = _t2.SLCODICE";
              +" and "+"SALDIART.SLCODMAG = _t2.SLCODMAG";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SLQTOPER = SALDIART.SLQTOPER+_t2.ORDI";
          +",SLQTIPER = SALDIART.SLQTIPER+_t2.IMPE";
          +",SLQTRPER = SALDIART.SLQTRPER+_t2.RISE";
          +i_ccchkf;
          +" from "+i_cTable+" SALDIART, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="SALDIART.SLCODICE = _t2.SLCODICE";
              +" and "+"SALDIART.SLCODMAG = _t2.SLCODMAG";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDIART, "+i_cQueryTable+" _t2 set ";
          +"SALDIART.SLQTOPER = SALDIART.SLQTOPER+_t2.ORDI";
          +",SALDIART.SLQTIPER = SALDIART.SLQTIPER+_t2.IMPE";
          +",SALDIART.SLQTRPER = SALDIART.SLQTRPER+_t2.RISE";
          +Iif(Empty(i_ccchkf),"",",SALDIART.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="SALDIART.SLCODICE = t2.SLCODICE";
              +" and "+"SALDIART.SLCODMAG = t2.SLCODMAG";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDIART set (";
          +"SLQTOPER,";
          +"SLQTIPER,";
          +"SLQTRPER";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"SLQTOPER+t2.ORDI,";
          +"SLQTIPER+t2.IMPE,";
          +"SLQTRPER+t2.RISE";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="SALDIART.SLCODICE = _t2.SLCODICE";
              +" and "+"SALDIART.SLCODMAG = _t2.SLCODMAG";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDIART set ";
          +"SLQTOPER = SALDIART.SLQTOPER+_t2.ORDI";
          +",SLQTIPER = SALDIART.SLQTIPER+_t2.IMPE";
          +",SLQTRPER = SALDIART.SLQTRPER+_t2.RISE";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".SLCODICE = "+i_cQueryTable+".SLCODICE";
              +" and "+i_cTable+".SLCODMAG = "+i_cQueryTable+".SLCODMAG";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SLQTOPER = (select "+i_cTable+".SLQTOPER+ORDI from "+i_cQueryTable+" where "+i_cWhere+")";
          +",SLQTIPER = (select "+i_cTable+".SLQTIPER+IMPE from "+i_cQueryTable+" where "+i_cWhere+")";
          +",SLQTRPER = (select "+i_cTable+".SLQTRPER+RISE from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    if this.w_NHF>=0
      this.w_TMPC = "Azzeramento MVQTASAL e aggiornamento saldi eseguito correttamente "
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = "Conversione eseguita correttamente"
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='DOC_DETT'
    this.cWorkTables[2]='*TMPVEND1'
    this.cWorkTables[3]='SALDIART'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
