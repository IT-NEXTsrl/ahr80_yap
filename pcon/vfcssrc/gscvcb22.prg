* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcb22                                                        *
*              Predisposizione tabella dei ruoli soggetti interni pratica      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-12-22                                                      *
* Last revis.: 2009-02-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcb22",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcb22 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_NCONN = 0
  * --- WorkFile variables
  RUO_SOGI_idx=0
  RIS_INTE_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inserimento di 4 records nella tabella Ruoli soggetti interni della pratica
    this.w_NCONN = i_TableProp[this.RIS_INTE_idx,3] 
    if IsAlt()
      * --- Try
      local bErr_03782400
      bErr_03782400=bTrsErr
      this.Try_03782400()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
      endif
      bTrsErr=bTrsErr or bErr_03782400
      * --- End
      * --- Try
      local bErr_0360C710
      bErr_0360C710=bTrsErr
      this.Try_0360C710()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
      endif
      bTrsErr=bTrsErr or bErr_0360C710
      * --- End
      * --- Try
      local bErr_03786B70
      bErr_03786B70=bTrsErr
      this.Try_03786B70()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
      endif
      bTrsErr=bTrsErr or bErr_03786B70
      * --- End
      * --- Try
      local bErr_035ED8C0
      bErr_035ED8C0=bTrsErr
      this.Try_035ED8C0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
      endif
      bTrsErr=bTrsErr or bErr_035ED8C0
      * --- End
      * --- Try
      local bErr_03784200
      bErr_03784200=bTrsErr
      this.Try_03784200()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
      endif
      bTrsErr=bTrsErr or bErr_03784200
      * --- End
    else
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("Procedura di conversione eseguita con successo")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    * --- Resoconto - sempre positivo
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Elaborazione terminata con successo")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = ah_Msgformat("Elaborazione terminata con successo")
  endproc
  proc Try_03782400()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Inserimento ruolo Titolare
    * --- Insert into RUO_SOGI
    i_nConn=i_TableProp[this.RUO_SOGI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.RUO_SOGI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.RUO_SOGI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"RSCODRUO"+",RSDESRUO"+",RSTIPRUO"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC("T"),'RUO_SOGI','RSCODRUO');
      +","+cp_NullLink(cp_ToStrODBC("Titolare"),'RUO_SOGI','RSDESRUO');
      +","+cp_NullLink(cp_ToStrODBC("T"),'RUO_SOGI','RSTIPRUO');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'RSCODRUO',"T",'RSDESRUO',"Titolare",'RSTIPRUO',"T")
      insert into (i_cTable) (RSCODRUO,RSDESRUO,RSTIPRUO &i_ccchkf. );
         values (;
           "T";
           ,"Titolare";
           ,"T";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Inserito ruolo Titolare")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    return
  proc Try_0360C710()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Inserimento ruolo Responsabile
    * --- Insert into RUO_SOGI
    i_nConn=i_TableProp[this.RUO_SOGI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.RUO_SOGI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.RUO_SOGI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"RSCODRUO"+",RSDESRUO"+",RSTIPRUO"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC("R"),'RUO_SOGI','RSCODRUO');
      +","+cp_NullLink(cp_ToStrODBC("Responsabile"),'RUO_SOGI','RSDESRUO');
      +","+cp_NullLink(cp_ToStrODBC("R"),'RUO_SOGI','RSTIPRUO');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'RSCODRUO',"R",'RSDESRUO',"Responsabile",'RSTIPRUO',"R")
      insert into (i_cTable) (RSCODRUO,RSDESRUO,RSTIPRUO &i_ccchkf. );
         values (;
           "R";
           ,"Responsabile";
           ,"R";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Inserito ruolo Responsabile")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    return
  proc Try_03786B70()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Inserimento ruolo Collaboratore
    * --- Insert into RUO_SOGI
    i_nConn=i_TableProp[this.RUO_SOGI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.RUO_SOGI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.RUO_SOGI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"RSCODRUO"+",RSDESRUO"+",RSTIPRUO"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC("C"),'RUO_SOGI','RSCODRUO');
      +","+cp_NullLink(cp_ToStrODBC("Collaboratore"),'RUO_SOGI','RSDESRUO');
      +","+cp_NullLink(cp_ToStrODBC("C"),'RUO_SOGI','RSTIPRUO');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'RSCODRUO',"C",'RSDESRUO',"Collaboratore",'RSTIPRUO',"C")
      insert into (i_cTable) (RSCODRUO,RSDESRUO,RSTIPRUO &i_ccchkf. );
         values (;
           "C";
           ,"Collaboratore";
           ,"C";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Inserito ruolo Collaboratore")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    return
  proc Try_035ED8C0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Inserimento ruolo Altro
    * --- Insert into RUO_SOGI
    i_nConn=i_TableProp[this.RUO_SOGI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.RUO_SOGI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.RUO_SOGI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"RSCODRUO"+",RSDESRUO"+",RSTIPRUO"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC("A"),'RUO_SOGI','RSCODRUO');
      +","+cp_NullLink(cp_ToStrODBC("Altro"),'RUO_SOGI','RSDESRUO');
      +","+cp_NullLink(cp_ToStrODBC("A"),'RUO_SOGI','RSTIPRUO');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'RSCODRUO',"A",'RSDESRUO',"Altro",'RSTIPRUO',"A")
      insert into (i_cTable) (RSCODRUO,RSDESRUO,RSTIPRUO &i_ccchkf. );
         values (;
           "A";
           ,"Altro";
           ,"A";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Inserito ruolo Altro")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    return
  proc Try_03784200()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Ricostruisce integritÓ referenziali verso tabella RIS_INTE (Risorse interne pratica)
    GSCV_BRT(this, "RIS_INTE" , this.w_NCONN )
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Ricostruite integritÓ referenziali verso tabella Risorse interne pratica")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='RUO_SOGI'
    this.cWorkTables[2]='RIS_INTE'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
