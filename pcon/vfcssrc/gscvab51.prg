* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvab51                                                        *
*              Aggiornamento campo mvimpeva                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_366]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-10-18                                                      *
* Last revis.: 2007-05-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvab51",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvab51 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_MVSERIAL = space(10)
  w_CPROWNUM = 0
  w_MVIMPEVA = 0
  * --- WorkFile variables
  RIPATMP1_idx=0
  DOC_DETT_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- FIle di LOG
    * --- Try
    local bErr_03609F20
    bErr_03609F20=bTrsErr
    this.Try_03609F20()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Recupero il Nome fisico della tabella temporanea 
      *     per indicarla nel Log in caso di errore
      this.w_TMPC = cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = ah_Msgformat("%1%0Tabella temporanea contenente i dati: %2" , i_ErrMsg , Alltrim(this.w_TMPC) )
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = this.oParentObject.w_PMSG
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_03609F20
    * --- End
  endproc
  proc Try_03609F20()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
     
 i_nConn=i_TableProp[this.DOC_DETT_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
    if i_nConn<>0
      do case
        case upper(CP_DBTYPE)="SQLSERVER"
          * --- Caso Sql
          this.w_TMPC = ah_Msgformat("Procedura di conversione solo per DB2 e Oracle")
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        case upper(CP_DBTYPE)="ORACLE"
          * --- Caso ORACLE
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        case upper(CP_DBTYPE)="DB2"
          * --- Caso DB2
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
      endcase
      if bTrsErr
        * --- Raise
        i_Error=MESSAGE()
        return
      else
        if this.w_NHF>=0
          this.w_TMPC = ah_Msgformat("Adeguamento campo MVIMPEVA tabella DOC_DETT eseguito correttamente%0Occorre ripetere la procedura su ogni azienda")
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.w_TMPC = ah_Msgformat("Adeguamento campo MVIMPEVA tabella DOC_DETT eseguito correttamente%0Occorre ripetere la procedura su ogni azienda")
    this.oParentObject.w_PMSG = this.w_TMPC
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella DB2
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.DOC_DETT_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
          )
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    * --- Elimino la tabella
    if Not GSCV_BDT( this, "DOC_DETT" , i_nConn )
      * --- Raise
      i_Error="Error"+Message()
      return
    endif
    * --- Ricostruisco il database per la tabella
    GSCV_BRT(this, "DOC_DETT", i_nConn )
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into DOC_DETT
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.DOC_DETT_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_Msg("Ripristinati valori di origine",.T.)
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    ah_Msg("Eliminata tabella temporanea tmp",.T.)
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella ORACLE
    pName="DOC_DETT"
    pDatabaseType=i_ServerConn[1,6]
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('GSCVAB51',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    * --- Inserisco NULL nel campo MVIMPEVA poich� Oracle 
    *     non permette la modifica se il campo � valorizzato anche a 0 
    * --- Write into DOC_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVIMPEVA ="+cp_NullLink(cp_ToStrODBC(NULL),'DOC_DETT','MVIMPEVA');
          +i_ccchkf ;
             )
    else
      update (i_cTable) set;
          MVIMPEVA = NULL;
          &i_ccchkf. ;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    ah_Msg("Ricostruzione tabella...",.T.)
    * --- Ricostruisco tabella DOC_DETT
    GSCV_BRT(this, pName, i_nConn )
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Riscrivo MVIMPEVA originale nel campo
    * --- Select from RIPATMP1
    i_nConn=i_TableProp[this.RIPATMP1_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" RIPATMP1 ";
           ,"_Curs_RIPATMP1")
    else
      select * from (i_cTable);
        into cursor _Curs_RIPATMP1
    endif
    if used('_Curs_RIPATMP1')
      select _Curs_RIPATMP1
      locate for 1=1
      do while not(eof())
      this.w_MVSERIAL = _Curs_RIPATMP1.MVSERIAL
      this.w_CPROWNUM = _Curs_RIPATMP1.CPROWNUM
      this.w_MVIMPEVA = _Curs_RIPATMP1.MVIMPEVA
      * --- Write into DOC_DETT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DOC_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MVIMPEVA ="+cp_NullLink(cp_ToStrODBC(this.w_MVIMPEVA),'DOC_DETT','MVIMPEVA');
            +i_ccchkf ;
        +" where ";
            +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
            +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
               )
      else
        update (i_cTable) set;
            MVIMPEVA = this.w_MVIMPEVA;
            &i_ccchkf. ;
         where;
            MVSERIAL = this.w_MVSERIAL;
            and CPROWNUM = this.w_CPROWNUM;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
        select _Curs_RIPATMP1
        continue
      enddo
      use
    endif
    * --- Scrivo 0 in quelli che avevo messo a Null che in precedenza erano 0
    * --- Write into DOC_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVIMPEVA ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVIMPEVA');
          +i_ccchkf ;
      +" where ";
          +"MVIMPEVA is null ";
             )
    else
      update (i_cTable) set;
          MVIMPEVA = 0;
          &i_ccchkf. ;
       where;
          MVIMPEVA is null;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    ah_Msg("Ripristinati valori di origine",.T.)
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    ah_Msg("Eliminata tabella temporanea tmp",.T.)
  endproc


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='*RIPATMP1'
    this.cWorkTables[2]='DOC_DETT'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_RIPATMP1')
      use in _Curs_RIPATMP1
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
