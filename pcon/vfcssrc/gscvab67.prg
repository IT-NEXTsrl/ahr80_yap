* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvab67                                                        *
*              Aggiorna ritenute                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_36]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-03-04                                                      *
* Last revis.: 2005-03-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvab67",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvab67 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TMPC = space(10)
  w_TEMP = space(10)
  w_APPO = 0
  w_NUMRIGA = 0
  w_ANTIPCLF = space(1)
  w_PERCIMP = 0
  w_AGRITIRP = 0
  w_AGPERIMP = 0
  w_ANCOINPS = 0
  w_DRPERRIT = 0
  w_DRNONSO1 = 0
  w_DRCODCAU = space(1)
  w_DRFODPRO = 0
  w_DRCODTRI = space(5)
  w_IMPORIGA = 0
  w_IMPOESCL = 0
  w_IMPONONS = 0
  w_INPSNS = 0
  w_IMPOINPS = 0
  w_IMPONIBI = 0
  w_RITENUTA = 0
  w_OSERRIF = space(10)
  w_SERRIF = space(10)
  w_TRFLINPS = space(1)
  w_TRPERRIT = 0
  w_ANCODTR2 = space(5)
  w_CODTRI = space(5)
  w_PNTIPCON = space(1)
  w_ANRIINPS = 0
  w_PNCODCON = space(15)
  w_ANPEINPS = 0
  w_APPO = space(10)
  w_APPO1 = 0
  w_MESS = space(100)
  w_DATAREG = ctod("  /  /  ")
  w_ANRITENU = space(1)
  w_CODAZI = space(5)
  w_ROWNUM = 0
  * --- WorkFile variables
  DATIRITE_idx=0
  MOV_RITE_idx=0
  TAB_RITE_idx=0
  ESERCIZI_idx=0
  CONTI_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Procedura di conversione ritenute 
    * --- FIle di LOG
    * --- Visibile dal padre
    this.w_CODAZI = i_CODAZI
    * --- Verifico congruenza tabella ritenute
    * --- Delete from TAB_RITE
    i_nConn=i_TableProp[this.TAB_RITE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TAB_RITE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      local i_cQueryTable,i_cWhere
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_cWhere=i_cTable+".TRCODAZI = "+i_cQueryTable+".TRCODAZI";
            +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
    
      do vq_exec with 'gsripqgr_conv',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
            +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    CREATE CURSOR RITENUTE ( DRSERIAL C(10), DRSOMESC N(18,4), DRFODPRO N(18,4), DRCODTRI C(5),; 
 DRCODCAU C(1), DRIMPONI N(18,4), DRRITENU N(18,4), DRPERRIT N(6,2), DRNONSOG N(18,4),; 
 DRNONSO1 N(18,4), CPROWNUM N(4) )
    * --- Select from ESERCIZI
    i_nConn=i_TableProp[this.ESERCIZI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select ESCODAZI,MIN(ESINIESE) AS INIESE  from "+i_cTable+" ESERCIZI ";
          +" where ESCODAZI="+cp_ToStrODBC(this.w_CODAZI)+"";
          +" group by ESCODAZI";
           ,"_Curs_ESERCIZI")
    else
      select ESCODAZI,MIN(ESINIESE) AS INIESE from (i_cTable);
       where ESCODAZI=this.w_CODAZI;
       group by ESCODAZI;
        into cursor _Curs_ESERCIZI
    endif
    if used('_Curs_ESERCIZI')
      select _Curs_ESERCIZI
      locate for 1=1
      do while not(eof())
      this.w_DATAREG = CP_TODATE(INIESE)
        select _Curs_ESERCIZI
        continue
      enddo
      use
    endif
    do GSCV_KAB with this
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Carica Documenti da Generare
    ah_Msg("Ricerca documenti di primanota...",.T.)
    vq_exec("..\PCON\EXE\QUERY\GSRI6QGR_CONV.VQR",this,"Dettrite")
    if USED("DETTRITE") AND RECCOUNT("DETTRITE")>0
      * --- La procedura elabora tutte le registrazioni facenti capo ad un percipiente che non hanno causali contabili
      *     con attivo il flag rileva ritenute. La stampa consente all'utente di verificare, tramite la causale, che le registrazioni siano corrette
      SELECT PNCAURIG, MIN(CCDESCRI) AS CCDESCRI FROM DETTRITE GROUP BY PNCAURIG INTO CURSOR __TMP__
      CP_CHPRN("..\PCON\EXE\QUERY\GSRI_QGR_CONV.FRX","",this)
      if !ah_YesNo("� necessario verificare le causali contabili associate alle registrazioni soggette a ritenuta%0Se la verifica � stata effettuata � possibile proseguire%0Proseguo con l'elaborazione?")
        this.oParentObject.w_PMSG = ah_Msgformat("ERRORE 001 - esecuzione bloccata da operatore")
        this.oParentObject.w_PESEOK = .F.
        if this.w_NHF>=0
          this.w_TMPC = ah_Msgformat("ERRORE 001 - esecuzione bloccata da operatore")
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        i_retcode = 'stop'
        return
      endif
      * --- Creo un cursore con il contenuto della Tabella Ritenute
      vq_exec("..\PCON\EXE\QUERY\GSRI2QGR_CONV.VQR",this,"TABRITE")
      * --- Inizio Aggiornamento vero e proprio
      ah_Msg("Inizio conversione...",.T.)
      * --- Testa il Cambio di Documento
      this.w_OSERRIF = "xxxyyykkka"
      SELECT DETTRITE
      GO TOP
      this.w_OSERRIF = PNSERIAL
      do while !EOF("DETTRITE")
        this.w_SERRIF = PNSERIAL
        * --- Legge i conti associati alla tabella ritenute
        this.w_PNCODCON = PNCODCON
        this.w_PNTIPCON = PNTIPCON
        * --- Codice e percentuali Previdenziali
        this.w_ANRITENU = NVL(ANRITENU, "N")
        this.w_ANRIINPS = NVL(ANRIINPS, 0)
        this.w_ANPEINPS = NVL(ANPEINPS, 0)
        this.w_ANCODTR2 = NVL(ANCODTR2, " ")
        * --- % Carico Percipiente
        this.w_ANCOINPS = NVL(ANCOINPS, 0)
        * --- Se agente leggo i dati
        this.w_ANTIPCLF = ANTIPCLF
        this.w_AGRITIRP = AGRITIRP
        this.w_AGPERIMP = AGPERIMP
        * --- Calcolo dati ritenuta
        this.w_IMPORIGA = NVL(PNIMPDAR,0)
        this.w_IMPOESCL = 0
        this.w_IMPONONS = IIF(this.w_IMPORIGA<0,-1,1) * NVL(PNIMPIND,0)
        this.w_INPSNS = this.w_IMPONONS
        this.w_DRNONSO1 = this.w_IMPONONS + this.w_DRNONSO1
        this.w_IMPONIBI = this.w_IMPORIGA - this.w_IMPOESCL - this.w_IMPONONS
        this.w_IMPOINPS = this.w_IMPONIBI
        if NVL(TRFLINPS, " ") = "S" 
          this.w_PERCIMP = this.w_ANPEINPS
          this.w_DRPERRIT = this.w_ANCOINPS
          this.w_DRCODTRI = this.w_ANCODTR2
        else
          if this.w_ANTIPCLF = "A"
            this.w_PERCIMP = this.w_AGPERIMP
            this.w_DRPERRIT = this.w_AGRITIRP
          else
            this.w_PERCIMP = NVL(TRPERIMP,0)
            this.w_DRPERRIT = NVL(TRPERRIT,0)
          endif
          this.w_DRCODTRI = TRCODTRI
        endif
        this.w_APPO = this.w_IMPONIBI
        this.w_IMPONIBI = cp_ROUND(this.w_IMPONIBI * NVL(this.w_PERCIMP,0) /100,2)
        this.w_IMPONONS = this.w_IMPONONS + (this.w_APPO - this.w_IMPONIBI)
        this.w_DRNONSO1 = this.w_IMPONONS + this.w_DRNONSO1
        this.w_RITENUTA = cp_ROUND(this.w_IMPONIBI * this.w_DRPERRIT/100, 2)
        if this.w_SERRIF <> this.w_OSERRIF
          this.w_NUMRIGA = 10
          this.w_OSERRIF = this.w_SERRIF
        else
          this.w_NUMRIGA = this.w_NUMRIGA + 10
        endif
        SELECT RITENUTE
        APPEND BLANK
        REPLACE DRSERIAL WITH this.w_SERRIF
        REPLACE CPROWNUM WITH this.w_NUMRIGA
        REPLACE DRSOMESC WITH this.w_IMPOESCL
        REPLACE DRFODPRO WITH 0
        REPLACE DRCODTRI WITH this.w_DRCODTRI
        REPLACE DRCODCAU WITH "A"
        REPLACE DRIMPONI WITH this.w_IMPONIBI
        REPLACE DRRITENU WITH this.w_RITENUTA
        REPLACE DRPERRIT WITH this.w_DRPERRIT
        REPLACE DRNONSOG WITH this.w_IMPONONS
        REPLACE DRNONSO1 WITH 0
        SELECT DETTRITE
        SKIP
      enddo
      SELECT DRSERIAL, MIN(CPROWNUM) AS CPROWNUM, SUM(DRSOMESC) AS DRSOMESC, MIN(DRFODPRO) AS DRFODPRO, DRCODTRI, ; 
 DRCODCAU, SUM(DRIMPONI) AS DRIMPONI, SUM(DRRITENU) AS DRRITENU, MIN(DRPERRIT) AS DRPERRIT, ; 
 SUM(DRNONSOG) AS DRNONSOG, MIN(DRNONSO1) AS DRNONSO1; 
 GROUP BY DRSERIAL, DRCODTRI, DRCODCAU FROM RITENUTE ; 
 INTO CURSOR GENERA ORDER BY DRSERIAL, DRCODTRI
      * --- Try
      local bErr_04A9F930
      bErr_04A9F930=bTrsErr
      this.Try_04A9F930()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        * --- Gestisce log errori
        this.oParentObject.w_PMSG = Message()
        this.oParentObject.w_PESEOK = .F.
        if this.w_NHF>=0
          this.w_TMPC = ah_Msgformat("ERRORE GENERICO - %1" , Message())
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
      bTrsErr=bTrsErr or bErr_04A9F930
      * --- End
    else
      this.w_MESS = ah_Msgformat("Non esistono dati da elaborare. Esecuzione terminata")
      this.oParentObject.w_PMSG = this.w_MESS
      this.oParentObject.w_PESEOK = .T.
      if this.w_NHF>=0
        this.w_TMPC = this.w_MESS
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    * --- Valorizzo la combo ' Causale Prestazione ' sui movimenti Ritenute
    * --- Write into MOV_RITE
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.MOV_RITE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOV_RITE_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="MRSERIAL"
      do vq_exec with 'GSCVAB67',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.MOV_RITE_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="MOV_RITE.MRSERIAL = _t2.MRSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MRCAURIT = _t2.TRCAUPRE2";
          +i_ccchkf;
          +" from "+i_cTable+" MOV_RITE, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="MOV_RITE.MRSERIAL = _t2.MRSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MOV_RITE, "+i_cQueryTable+" _t2 set ";
          +"MOV_RITE.MRCAURIT = _t2.TRCAUPRE2";
          +Iif(Empty(i_ccchkf),"",",MOV_RITE.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="MOV_RITE.MRSERIAL = t2.MRSERIAL";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MOV_RITE set (";
          +"MRCAURIT";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.TRCAUPRE2";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="MOV_RITE.MRSERIAL = _t2.MRSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MOV_RITE set ";
          +"MRCAURIT = _t2.TRCAUPRE2";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".MRSERIAL = "+i_cQueryTable+".MRSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MRCAURIT = (select TRCAUPRE2 from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error='ERRORE QUERY GSCVAB67'
      return
    endif
    * --- Elimino le righe relative a tributi di tipo Previdenziale
    * --- Select from TAB_RITE
    i_nConn=i_TableProp[this.TAB_RITE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TAB_RITE_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" TAB_RITE ";
           ,"_Curs_TAB_RITE")
    else
      select * from (i_cTable);
        into cursor _Curs_TAB_RITE
    endif
    if used('_Curs_TAB_RITE')
      select _Curs_TAB_RITE
      locate for 1=1
      do while not(eof())
      this.w_ROWNUM = _Curs_TAB_RITE.CPROWNUM
      * --- Write into TAB_RITE
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TAB_RITE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TAB_RITE_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TAB_RITE_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"TRFLIRPE ="+cp_NullLink(cp_ToStrODBC(SPACE(1)),'TAB_RITE','TRFLIRPE');
        +",TRFLINPS ="+cp_NullLink(cp_ToStrODBC(SPACE(1)),'TAB_RITE','TRFLINPS');
            +i_ccchkf ;
        +" where ";
            +"TRCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
            +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWNUM);
               )
      else
        update (i_cTable) set;
            TRFLIRPE = SPACE(1);
            ,TRFLINPS = SPACE(1);
            &i_ccchkf. ;
         where;
            TRCODAZI = this.w_CODAZI;
            and CPROWNUM = this.w_ROWNUM;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error='ERRORE WRITE TAB_RITE'
        return
      endif
        select _Curs_TAB_RITE
        continue
      enddo
      use
    endif
    * --- Delete from TAB_RITE
    i_nConn=i_TableProp[this.TAB_RITE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TAB_RITE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      local i_cQueryTable,i_cWhere
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_cWhere=i_cTable+".TRCODAZI = "+i_cQueryTable+".TRCODAZI";
            +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
    
      do vq_exec with 'GSCVAB67_1',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
            +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- valorizzare codice Irpef (e relativa Causale prestazione) nell'anagrafica 
    *     fornitori in base ai dati ritenute dell'ultima registrazione contabile di quel fornitore
    * --- Write into CONTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.CONTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="ANTIPCON,ANCODICE"
      do vq_exec with 'GSCVAB67_C',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTI_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="CONTI.ANTIPCON = _t2.ANTIPCON";
              +" and "+"CONTI.ANCODICE = _t2.ANCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ANCODIRP = _t2.DRCODTRI";
          +",ANCAURIT = _t2.DRCODCAU";
          +i_ccchkf;
          +" from "+i_cTable+" CONTI, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="CONTI.ANTIPCON = _t2.ANTIPCON";
              +" and "+"CONTI.ANCODICE = _t2.ANCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CONTI, "+i_cQueryTable+" _t2 set ";
          +"CONTI.ANCODIRP = _t2.DRCODTRI";
          +",CONTI.ANCAURIT = _t2.DRCODCAU";
          +Iif(Empty(i_ccchkf),"",",CONTI.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="CONTI.ANTIPCON = t2.ANTIPCON";
              +" and "+"CONTI.ANCODICE = t2.ANCODICE";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CONTI set (";
          +"ANCODIRP,";
          +"ANCAURIT";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.DRCODTRI,";
          +"t2.DRCODCAU";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="CONTI.ANTIPCON = _t2.ANTIPCON";
              +" and "+"CONTI.ANCODICE = _t2.ANCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CONTI set ";
          +"ANCODIRP = _t2.DRCODTRI";
          +",ANCAURIT = _t2.DRCODCAU";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".ANTIPCON = "+i_cQueryTable+".ANTIPCON";
              +" and "+i_cTable+".ANCODICE = "+i_cQueryTable+".ANCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ANCODIRP = (select DRCODTRI from "+i_cQueryTable+" where "+i_cWhere+")";
          +",ANCAURIT = (select DRCODCAU from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error='ERRORE QUERY GSCVAB67_C'
      return
    endif
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc
  proc Try_04A9F930()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    SELECT GENERA
    GO TOP 
 SCAN
    this.w_SERRIF = DRSERIAL
    this.w_NUMRIGA = CPROWNUM
    this.w_IMPOESCL = DRSOMESC
    this.w_DRCODTRI = DRCODTRI
    this.w_IMPONIBI = DRIMPONI
    this.w_RITENUTA = DRRITENU
    this.w_DRPERRIT = DRPERRIT
    this.w_IMPONONS = DRNONSOG
    this.w_DRCODCAU = "A"
    this.w_DRFODPRO = 0
    this.w_DRNONSO1 = 0
    ah_Msg("Aggiorno registrazione di primanota n� %1",.T.,.F.,.F., this.w_SERRIF)
    * --- Insert into DATIRITE
    i_nConn=i_TableProp[this.DATIRITE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DATIRITE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DATIRITE_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"DRSERIAL"+",CPROWNUM"+",DRSOMESC"+",DRFODPRO"+",DRCODTRI"+",DRCODCAU"+",DRIMPONI"+",DRRITENU"+",DRPERRIT"+",DRNONSOG"+",DRNONSO1"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_SERRIF),'DATIRITE','DRSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIGA),'DATIRITE','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_IMPOESCL),'DATIRITE','DRSOMESC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DRFODPRO),'DATIRITE','DRFODPRO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DRCODTRI),'DATIRITE','DRCODTRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DRCODCAU),'DATIRITE','DRCODCAU');
      +","+cp_NullLink(cp_ToStrODBC(this.w_IMPONIBI),'DATIRITE','DRIMPONI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_RITENUTA),'DATIRITE','DRRITENU');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DRPERRIT),'DATIRITE','DRPERRIT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_IMPONONS),'DATIRITE','DRNONSOG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DRNONSO1),'DATIRITE','DRNONSO1');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'DRSERIAL',this.w_SERRIF,'CPROWNUM',this.w_NUMRIGA,'DRSOMESC',this.w_IMPOESCL,'DRFODPRO',this.w_DRFODPRO,'DRCODTRI',this.w_DRCODTRI,'DRCODCAU',this.w_DRCODCAU,'DRIMPONI',this.w_IMPONIBI,'DRRITENU',this.w_RITENUTA,'DRPERRIT',this.w_DRPERRIT,'DRNONSOG',this.w_IMPONONS,'DRNONSO1',this.w_DRNONSO1)
      insert into (i_cTable) (DRSERIAL,CPROWNUM,DRSOMESC,DRFODPRO,DRCODTRI,DRCODCAU,DRIMPONI,DRRITENU,DRPERRIT,DRNONSOG,DRNONSO1 &i_ccchkf. );
         values (;
           this.w_SERRIF;
           ,this.w_NUMRIGA;
           ,this.w_IMPOESCL;
           ,this.w_DRFODPRO;
           ,this.w_DRCODTRI;
           ,this.w_DRCODCAU;
           ,this.w_IMPONIBI;
           ,this.w_RITENUTA;
           ,this.w_DRPERRIT;
           ,this.w_IMPONONS;
           ,this.w_DRNONSO1;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Errore nella scrittura'
      return
    endif
    ENDSCAN
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = ah_msgformat("Conversione eseguita correttamente")
    this.w_TMPC = ah_msgformat("Conversione eseguita correttamente")
    if this.w_NHF>=0
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if USED("RITENUTE")
      SELECT RITENUTE
      USE
    endif
    if USED("TABRITE")
      SELECT TABRITE
      USE
    endif
    if USED("DETTRITE")
      SELECT DETTRITE
      USE
    endif
    if USED("GENERA")
      SELECT GENERA 
 USE
    endif
  endproc


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='DATIRITE'
    this.cWorkTables[2]='MOV_RITE'
    this.cWorkTables[3]='TAB_RITE'
    this.cWorkTables[4]='ESERCIZI'
    this.cWorkTables[5]='CONTI'
    return(this.OpenAllTables(5))

  proc CloseCursors()
    if used('_Curs_ESERCIZI')
      use in _Curs_ESERCIZI
    endif
    if used('_Curs_TAB_RITE')
      use in _Curs_TAB_RITE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
