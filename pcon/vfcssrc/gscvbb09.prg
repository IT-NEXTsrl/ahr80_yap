* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvbb09                                                        *
*              Ripartizione fatture spese                                      *
*                                                                              *
*      Author: Nicola Gorlandi                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_121]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-03-17                                                      *
* Last revis.: 2006-12-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvbb09",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvbb09 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TMPC = space(10)
  w_TEMP = space(10)
  w_DATREG = ctod("  /  /  ")
  w_FLVEAC = space(1)
  w_NUMFAT = 0
  w_VALNAZ = space(3)
  w_FLCOCO = space(1)
  w_TIPATT = space(1)
  w_CODATT = space(1)
  w_CODCOM = space(1)
  w_CODCOS = space(5)
  w_IMPAGG = 0
  w_TIPDOC = space(5)
  w_RIGA = 0
  w_CODART = space(20)
  w_PREZZO = 0
  w_CATDOC = space(2)
  w_BWRITE = .f.
  w_oMess = .NULL.
  w_VALCOM = space(3)
  w_COMDECTOT = 0
  w_IMPCOM = 0
  w_DOCCAOVAL = space(3)
  w_DOCDECTOT = 0
  * --- WorkFile variables
  ADDE_SPE_idx=0
  DOC_DETT_idx=0
  DOC_MAST_idx=0
  VALUTE_idx=0
  CAN_TIER_idx=0
  MA_COSTI_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elimina dalla tabella di relazione fatture spese le righe che hanno uguale riga di partenza e riga di arrivo.
    *     Inoltre rimuove MV_FLAGG dalle righe di DOC_DETT puntate dalla
    *     chiave di sinistra delle relazioni identificate, mentre annulla la 
    *     riprtizione per quelle di destra (di fatto simula un annulla ripartizione)
    *     Questa situazione si ha nel caso in cui si incappi nell'errore E5763.
    *     
    *     ATTENZIONE la procedura di conversione � facoltativa in quanto
    *     funziona correttamente solo se l'errore si � incontrato su Fatture. Questo perch� 
    *     la procedura non � pi� in grado di capire se la spesa ripartita apparteneva
    *     ad una nota di credito o ad una fattura, quindi per scelta considera tutte le ripartizioni
    *     errate come ripartizioni di una FATTURA DI NOTE SPESE
    * --- FIle di LOG
    * --- Try
    local bErr_035ECAE0
    bErr_035ECAE0=bTrsErr
    this.Try_035ECAE0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE GENERICO - Impossibile aggiornare tabella %1", "ADDE_SPE")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_035ECAE0
    * --- End
  endproc
  proc Try_035ECAE0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    this.w_NUMFAT = 0
    * --- Tutte le righe errate come Fatture...
    this.w_CATDOC = "FA"
    * --- Recupero l'elenco delle relazioni che hanno seriale di parte destra 
    *     e sinistra uguali (la rigap u� mutare..)
    * --- Select from ADDE_SPE
    i_nConn=i_TableProp[this.ADDE_SPE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ADDE_SPE_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" ADDE_SPE ";
          +" where RLSERFAT=RLSERDOC";
          +" order by RLSERFAT, RLNUMFAT, RLROWFAT";
           ,"_Curs_ADDE_SPE")
    else
      select * from (i_cTable);
       where RLSERFAT=RLSERDOC;
       order by RLSERFAT, RLNUMFAT, RLROWFAT;
        into cursor _Curs_ADDE_SPE
    endif
    if used('_Curs_ADDE_SPE')
      select _Curs_ADDE_SPE
      locate for 1=1
      do while not(eof())
      if this.w_NUMFAT=0
        * --- Oggetto per messaggi incrementali
        this.w_oMess=createobject("Ah_Message")
        this.w_oMess.AddMsgPartNL("Sono presenti spese erroneamente ripartite, si desidera ripristinarle?")     
        this.w_oMess.AddMsgPartNL("(Nel caso si abbia ripartito spese presenti in note di credito non � possibile lanciare questo aggiornamento )")     
        this.w_oMess.AddMsgPart("Rispondendo no sar� prodotta una stampa con le righe da verificare, senza nessuna scrittura sul database")     
        this.w_BWRITE = this.w_oMess.ah_YesNo()
        if Used("__Tmp__")
           
 Select ("__Tmp__") 
 Use
        endif
         
 Create Cursor __Tmp__ ( Seriale C(10), DatReg D, NumReg N(6), TipDoc C(5), FlveAc C(1), RowOrd N(5,0), CodArt C(20), Prezzo N(18,5), ValRip N(18,5))
      endif
      this.w_NUMFAT = this.w_NUMFAT + 1
      * --- Leggo dati di testata del documento rivalorizzato
      * --- Read from DOC_MAST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.DOC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MVDATREG,MVFLVEAC,MVVALNAZ,MVTIPDOC,MVNUMREG"+;
          " from "+i_cTable+" DOC_MAST where ";
              +"MVSERIAL = "+cp_ToStrODBC(_Curs_ADDE_SPE.RLSERDOC);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MVDATREG,MVFLVEAC,MVVALNAZ,MVTIPDOC,MVNUMREG;
          from (i_cTable) where;
              MVSERIAL = _Curs_ADDE_SPE.RLSERDOC;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_DATREG = NVL(cp_ToDate(_read_.MVDATREG),cp_NullValue(_read_.MVDATREG))
        this.w_FLVEAC = NVL(cp_ToDate(_read_.MVFLVEAC),cp_NullValue(_read_.MVFLVEAC))
        this.w_VALNAZ = NVL(cp_ToDate(_read_.MVVALNAZ),cp_NullValue(_read_.MVVALNAZ))
        this.w_TIPDOC = NVL(cp_ToDate(_read_.MVTIPDOC),cp_NullValue(_read_.MVTIPDOC))
        this.w_DATREG = NVL(cp_ToDate(_read_.MVDATREG),cp_NullValue(_read_.MVDATREG))
        w_NUMREG = NVL(cp_ToDate(_read_.MVNUMREG),cp_NullValue(_read_.MVNUMREG))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Leggo la riga rivalorizzata..
      * --- Read from DOC_DETT
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.DOC_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MVFLCOCO,MVTIPATT,MVCODATT,MVCODCOM,MVCODCOS,CPROWORD,MVCODICE,MVPREZZO"+;
          " from "+i_cTable+" DOC_DETT where ";
              +"MVSERIAL = "+cp_ToStrODBC(_Curs_ADDE_SPE.RLSERDOC);
              +" and CPROWNUM = "+cp_ToStrODBC(_Curs_ADDE_SPE.RLNUMDOC);
              +" and MVNUMRIF = "+cp_ToStrODBC(_Curs_ADDE_SPE.RLROWDOC);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MVFLCOCO,MVTIPATT,MVCODATT,MVCODCOM,MVCODCOS,CPROWORD,MVCODICE,MVPREZZO;
          from (i_cTable) where;
              MVSERIAL = _Curs_ADDE_SPE.RLSERDOC;
              and CPROWNUM = _Curs_ADDE_SPE.RLNUMDOC;
              and MVNUMRIF = _Curs_ADDE_SPE.RLROWDOC;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_FLCOCO = NVL(cp_ToDate(_read_.MVFLCOCO),cp_NullValue(_read_.MVFLCOCO))
        this.w_TIPATT = NVL(cp_ToDate(_read_.MVTIPATT),cp_NullValue(_read_.MVTIPATT))
        this.w_CODATT = NVL(cp_ToDate(_read_.MVCODATT),cp_NullValue(_read_.MVCODATT))
        this.w_CODCOM = NVL(cp_ToDate(_read_.MVCODCOM),cp_NullValue(_read_.MVCODCOM))
        this.w_CODCOS = NVL(cp_ToDate(_read_.MVCODCOS),cp_NullValue(_read_.MVCODCOS))
        this.w_RIGA = NVL(cp_ToDate(_read_.CPROWORD),cp_NullValue(_read_.CPROWORD))
        this.w_CODART = NVL(cp_ToDate(_read_.MVCODICE),cp_NullValue(_read_.MVCODICE))
        this.w_PREZZO = NVL(cp_ToDate(_read_.MVPREZZO),cp_NullValue(_read_.MVPREZZO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
       
 Insert into __Tmp__ ; 
 ( Seriale , DatReg , NumReg , TipDoc , FlveAc , RowOrd , CodArt , Prezzo , ValRip ) ; 
 Values ; 
 ( _Curs_ADDE_SPE.RLSERFAT, this.w_DATREG , w_NUMREG , this.w_TIPDOC , this.w_FLVEAC , this.w_RIGA , this.w_CODART , this.w_PREZZO , _Curs_ADDE_SPE.RLIMPNAZ )
      if this.w_BWRITE
        * --- Cambio di segno il flag aggiorna consuntivo di commessa
        this.w_FLCOCO = IIF (Nvl ( this.w_FLCOCO ,"" ) ="+" ,"-", IIF (Nvl ( this.w_FLCOCO ,"" ) ="-" ,"+", IIF (Nvl ( this.w_FLCOCO ,"" ) ="=" ,"=", " " ) ) )
        this.w_IMPAGG = _Curs_ADDE_SPE.RLIMPNAZ 
        * --- Calcoli per aggiornamento produzione su commessa
        if g_COMM="S" And Not Empty( this.w_CODATT ) And Not Empty( this.w_CODCOS ) And Not Empty( this.w_FLCOCO )
          * --- Read from CAN_TIER
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CAN_TIER_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CNCODVAL"+;
              " from "+i_cTable+" CAN_TIER where ";
                  +"CNCODCAN = "+cp_ToStrODBC(this.w_CODCOM);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CNCODVAL;
              from (i_cTable) where;
                  CNCODCAN = this.w_CODCOM;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_VALCOM = NVL(cp_ToDate(_read_.CNCODVAL),cp_NullValue(_read_.CNCODVAL))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Read from VALUTE
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.VALUTE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "VADECTOT"+;
              " from "+i_cTable+" VALUTE where ";
                  +"VACODVAL = "+cp_ToStrODBC(this.w_VALCOM);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              VADECTOT;
              from (i_cTable) where;
                  VACODVAL = this.w_VALCOM;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_COMDECTOT = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Read from VALUTE
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.VALUTE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "VACAOVAL,VADECTOT"+;
              " from "+i_cTable+" VALUTE where ";
                  +"VACODVAL = "+cp_ToStrODBC(this.w_VALNAZ);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              VACAOVAL,VADECTOT;
              from (i_cTable) where;
                  VACODVAL = this.w_VALNAZ;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_DOCCAOVAL = NVL(cp_ToDate(_read_.VACAOVAL),cp_NullValue(_read_.VACAOVAL))
            this.w_DOCDECTOT = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if this.w_VALCOM<>this.w_VALNAZ
            this.w_IMPCOM = VAL2CAM(this.w_IMPAGG , this.w_VALNAZ , this.w_VALCOM , this.w_DOCCAOVAL , i_DATSYS , this.w_COMDECTOT)
          else
            this.w_IMPCOM = this.w_IMPAGG
          endif
          * --- Aggiorna i saldi di commessa
          this.w_IMPCOM = IIF( this.w_CATDOC="FA" , 1 , -1) * this.w_IMPCOM
          * --- Write into MA_COSTI
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.MA_COSTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MA_COSTI_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.MA_COSTI_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CSCONSUN =CSCONSUN- "+cp_ToStrODBC(this.w_IMPCOM);
                +i_ccchkf ;
            +" where ";
                +"CSCODCOM = "+cp_ToStrODBC(this.w_CODCOM);
                +" and CSTIPSTR = "+cp_ToStrODBC(this.w_TIPATT);
                +" and CSCODMAT = "+cp_ToStrODBC(this.w_CODATT);
                +" and CSCODCOS = "+cp_ToStrODBC(this.w_CODCOS);
                   )
          else
            update (i_cTable) set;
                CSCONSUN = CSCONSUN - this.w_IMPCOM;
                &i_ccchkf. ;
             where;
                CSCODCOM = this.w_CODCOM;
                and CSTIPSTR = this.w_TIPATT;
                and CSCODMAT = this.w_CODATT;
                and CSCODCOS = this.w_CODCOS;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        else
          this.w_IMPCOM = 0
        endif
        this.w_IMPAGG = IIF( this.w_CATDOC="FA" , 1 , -1) * this.w_IMPAGG
        * --- Gestione ultimo prezzo / Ultimo Costo
        GSAR_BAC(this, _Curs_ADDE_SPE.RLSERDOC , _Curs_ADDE_SPE.RLNUMDOC , _Curs_ADDE_SPE.RLROWDOC ,-this.w_IMPAGG ,.T. )
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- detraggo da MVIMPNAZ l'importo impostato
        * --- Write into DOC_DETT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.DOC_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
          i_cOp2=cp_SetTrsOp(this.w_FLCOCO,'MVIMPCOM','this.w_IMPCOM',this.w_IMPCOM,'update',i_nConn)
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MVIMPNAZ =MVIMPNAZ- "+cp_ToStrODBC(this.w_IMPAGG);
          +",MVIMPCOM ="+cp_NullLink(i_cOp2,'DOC_DETT','MVIMPCOM');
              +i_ccchkf ;
          +" where ";
              +"MVSERIAL = "+cp_ToStrODBC(_Curs_ADDE_SPE.RLSERDOC);
              +" and CPROWNUM = "+cp_ToStrODBC(_Curs_ADDE_SPE.RLNUMDOC);
              +" and MVNUMRIF = "+cp_ToStrODBC(_Curs_ADDE_SPE.RLROWDOC);
                 )
        else
          update (i_cTable) set;
              MVIMPNAZ = MVIMPNAZ - this.w_IMPAGG;
              ,MVIMPCOM = &i_cOp2.;
              &i_ccchkf. ;
           where;
              MVSERIAL = _Curs_ADDE_SPE.RLSERDOC;
              and CPROWNUM = _Curs_ADDE_SPE.RLNUMDOC;
              and MVNUMRIF = _Curs_ADDE_SPE.RLROWDOC;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Elimino l'entrata errata...
        * --- Delete from ADDE_SPE
        i_nConn=i_TableProp[this.ADDE_SPE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ADDE_SPE_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"RLSERFAT = "+cp_ToStrODBC(_Curs_ADDE_SPE.RLSERFAT);
                +" and RLNUMFAT = "+cp_ToStrODBC(_Curs_ADDE_SPE.RLNUMFAT);
                +" and RLROWFAT = "+cp_ToStrODBC(_Curs_ADDE_SPE.RLROWFAT);
                +" and RLSERDOC = "+cp_ToStrODBC(_Curs_ADDE_SPE.RLSERDOC);
                +" and RLNUMDOC = "+cp_ToStrODBC(_Curs_ADDE_SPE.RLNUMDOC);
                +" and RLROWDOC = "+cp_ToStrODBC(_Curs_ADDE_SPE.RLROWDOC);
                 )
        else
          delete from (i_cTable) where;
                RLSERFAT = _Curs_ADDE_SPE.RLSERFAT;
                and RLNUMFAT = _Curs_ADDE_SPE.RLNUMFAT;
                and RLROWFAT = _Curs_ADDE_SPE.RLROWFAT;
                and RLSERDOC = _Curs_ADDE_SPE.RLSERDOC;
                and RLNUMDOC = _Curs_ADDE_SPE.RLNUMDOC;
                and RLROWDOC = _Curs_ADDE_SPE.RLROWDOC;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        * --- Rimuovo cmq il flag di abbina fatture spese, la riga puntata (se esiste) � interna al
        *     documento rivalorizzato.
        * --- Write into DOC_DETT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.DOC_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MV_FLAGG ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MV_FLAGG');
              +i_ccchkf ;
          +" where ";
              +"MVSERIAL = "+cp_ToStrODBC(_Curs_ADDE_SPE.RLSERFAT);
              +" and CPROWNUM = "+cp_ToStrODBC(_Curs_ADDE_SPE.RLNUMFAT);
              +" and MVNUMRIF = "+cp_ToStrODBC(_Curs_ADDE_SPE.RLROWFAT);
                 )
        else
          update (i_cTable) set;
              MV_FLAGG = " ";
              &i_ccchkf. ;
           where;
              MVSERIAL = _Curs_ADDE_SPE.RLSERFAT;
              and CPROWNUM = _Curs_ADDE_SPE.RLNUMFAT;
              and MVNUMRIF = _Curs_ADDE_SPE.RLROWFAT;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
        select _Curs_ADDE_SPE
        continue
      enddo
      use
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    if Used("__Tmp__") And this.w_NUMFAT>0
      * --- Lancio l'elenco delle fatture spesa con problemi...
       
 Cp_Chprn("Query\GSCVBB09.FRX") 
 Select("__Tmp__") 
 Use
    endif
    if this.w_NHF>=0
      if this.w_BWRITE
        this.w_TMPC = ah_Msgformat("Eliminazione righe errate ADDE_SPE avvenuta correttamente su %1", Alltrim(Str( this.w_NUMFAT )) )
      else
        this.w_TMPC = ah_Msgformat("Identificate %1 righe errate su ADDE_SPE", Alltrim(Str( this.w_NUMFAT )) )
      endif
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = ah_Msgformat("Conversione eseguita correttamente")
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,6)]
    this.cWorkTables[1]='ADDE_SPE'
    this.cWorkTables[2]='DOC_DETT'
    this.cWorkTables[3]='DOC_MAST'
    this.cWorkTables[4]='VALUTE'
    this.cWorkTables[5]='CAN_TIER'
    this.cWorkTables[6]='MA_COSTI'
    return(this.OpenAllTables(6))

  proc CloseCursors()
    if used('_Curs_ADDE_SPE')
      use in _Curs_ADDE_SPE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
