* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcc45                                                        *
*              Inserimento tariffa a tempo concordata                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-10-06                                                      *
* Last revis.: 2014-11-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcc45",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcc45 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_NCONN = 0
  w_CODPRA = space(15)
  w_IMPORTO = 0
  * --- WorkFile variables
  CAN_TIER_idx=0
  TAR_CONC_idx=0
  CONF_INT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inserimento tariffa a tempo concordata
    this.w_NCONN = i_TableProp[this.CAN_TIER_idx,3] 
    * --- Try
    local bErr_00D83230
    bErr_00D83230=bTrsErr
    this.Try_00D83230()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_00D83230
    * --- End
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Elaborazione terminata con successo")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = ah_Msgformat("Elaborazione terminata con successo")
  endproc
  proc Try_00D83230()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Seleziona le pratiche con Tariffa concordata e con relativo importo non nullo
    * --- Select from CAN_TIER
    i_nConn=i_TableProp[this.CAN_TIER_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2],.t.,this.CAN_TIER_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select CNCODCAN, CNTARCON  from "+i_cTable+" CAN_TIER ";
          +" where CNTARTEM='C' AND CNTARCON<>0";
           ,"_Curs_CAN_TIER")
    else
      select CNCODCAN, CNTARCON from (i_cTable);
       where CNTARTEM="C" AND CNTARCON<>0;
        into cursor _Curs_CAN_TIER
    endif
    if used('_Curs_CAN_TIER')
      select _Curs_CAN_TIER
      locate for 1=1
      do while not(eof())
      this.w_CODPRA = _Curs_CAN_TIER.CNCODCAN
      this.w_IMPORTO = _Curs_CAN_TIER.CNTARCON
      * --- Inserisce riga
      * --- Insert into TAR_CONC
      i_nConn=i_TableProp[this.TAR_CONC_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TAR_CONC_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TAR_CONC_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"TCCODPRA"+",TCIMPORT"+",CPROWNUM"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_CODPRA),'TAR_CONC','TCCODPRA');
        +","+cp_NullLink(cp_ToStrODBC(this.w_IMPORTO),'TAR_CONC','TCIMPORT');
        +","+cp_NullLink(cp_ToStrODBC(1),'TAR_CONC','CPROWNUM');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'TCCODPRA',this.w_CODPRA,'TCIMPORT',this.w_IMPORTO,'CPROWNUM',1)
        insert into (i_cTable) (TCCODPRA,TCIMPORT,CPROWNUM &i_ccchkf. );
           values (;
             this.w_CODPRA;
             ,this.w_IMPORTO;
             ,1;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Azzera l'importo della tariffa concordata sulla pratica
      * --- Write into CAN_TIER
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.CAN_TIER_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.CAN_TIER_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"CNTARCON ="+cp_NullLink(cp_ToStrODBC(0),'CAN_TIER','CNTARCON');
            +i_ccchkf ;
        +" where ";
            +"CNCODCAN = "+cp_ToStrODBC(this.w_CODPRA);
               )
      else
        update (i_cTable) set;
            CNTARCON = 0;
            &i_ccchkf. ;
         where;
            CNCODCAN = this.w_CODPRA;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
        select _Curs_CAN_TIER
        continue
      enddo
      use
    endif
    if ISALT()
      * --- Write into CONF_INT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.CONF_INT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONF_INT_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.CONF_INT_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"CISCRCOL ="+cp_NullLink(cp_ToStrODBC(16711679),'CONF_INT','CISCRCOL');
            +i_ccchkf ;
        +" where ";
            +"CISCRCOL = "+cp_ToStrODBC(16777215);
               )
      else
        update (i_cTable) set;
            CISCRCOL = 16711679;
            &i_ccchkf. ;
         where;
            CISCRCOL = 16777215;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='CAN_TIER'
    this.cWorkTables[2]='TAR_CONC'
    this.cWorkTables[3]='CONF_INT'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_CAN_TIER')
      use in _Curs_CAN_TIER
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
