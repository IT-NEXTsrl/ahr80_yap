* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvbb70                                                        *
*              Vendite negozio - aggiorna campo mdscoven                       *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-03-17                                                      *
* Last revis.: 2007-07-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvbb70",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvbb70 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TMPC = space(10)
  w_TEMP = space(10)
  w_TEST = space(1)
  * --- WorkFile variables
  CORDRISP_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Azzera Codice Contratto Eliminato Dalla relativa gestione
    * --- FIle di LOG
    * --- Try
    local bErr_04A9F690
    bErr_04A9F690=bTrsErr
    this.Try_04A9F690()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = "ERRORE GENERICO - Impossibile aggiornare tabella  CORDRISP "
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_04A9F690
    * --- End
  endproc
  proc Try_04A9F690()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    this.w_TEST = "N"
    * --- Nel primo giro modifico gli importi di MDSCOVEN non arrotondati, nel secondo
    *     aggiusto anche quelli che provocano una differenza tra la somma sulle righe 
    *     ed il totale nel piede di sconti e promozioni, entrambi questi importi infatti sono ventiati e
    *     in GSPS_BPZ e GSPS1BPZ confkluiscono in MDSCOVEN.
    * --- Write into CORDRISP
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.CORDRISP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CORDRISP_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="MDSERIAL,CPROWNUM"
      do vq_exec with 'GSCVBB70B',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.CORDRISP_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="CORDRISP.MDSERIAL = _t2.MDSERIAL";
              +" and "+"CORDRISP.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MDSCOVEN = _t2.MDSCOVEN";
          +i_ccchkf;
          +" from "+i_cTable+" CORDRISP, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="CORDRISP.MDSERIAL = _t2.MDSERIAL";
              +" and "+"CORDRISP.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CORDRISP, "+i_cQueryTable+" _t2 set ";
          +"CORDRISP.MDSCOVEN = _t2.MDSCOVEN";
          +Iif(Empty(i_ccchkf),"",",CORDRISP.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="CORDRISP.MDSERIAL = t2.MDSERIAL";
              +" and "+"CORDRISP.CPROWNUM = t2.CPROWNUM";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CORDRISP set (";
          +"MDSCOVEN";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.MDSCOVEN";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="CORDRISP.MDSERIAL = _t2.MDSERIAL";
              +" and "+"CORDRISP.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CORDRISP set ";
          +"MDSCOVEN = _t2.MDSCOVEN";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".MDSERIAL = "+i_cQueryTable+".MDSERIAL";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MDSCOVEN = (select MDSCOVEN from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Secondo giro aggiusto l'importo per le righe con CPROWNUM minimo...
    this.w_TEST = "S"
    * --- Write into CORDRISP
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.CORDRISP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CORDRISP_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="MDSERIAL,CPROWNUM"
      do vq_exec with 'GSCVBB70B',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.CORDRISP_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="CORDRISP.MDSERIAL = _t2.MDSERIAL";
              +" and "+"CORDRISP.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MDSCOVEN = _t2.MDSCOVEN";
          +i_ccchkf;
          +" from "+i_cTable+" CORDRISP, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="CORDRISP.MDSERIAL = _t2.MDSERIAL";
              +" and "+"CORDRISP.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CORDRISP, "+i_cQueryTable+" _t2 set ";
          +"CORDRISP.MDSCOVEN = _t2.MDSCOVEN";
          +Iif(Empty(i_ccchkf),"",",CORDRISP.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="CORDRISP.MDSERIAL = t2.MDSERIAL";
              +" and "+"CORDRISP.CPROWNUM = t2.CPROWNUM";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CORDRISP set (";
          +"MDSCOVEN";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.MDSCOVEN";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="CORDRISP.MDSERIAL = _t2.MDSERIAL";
              +" and "+"CORDRISP.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CORDRISP set ";
          +"MDSCOVEN = _t2.MDSCOVEN";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".MDSERIAL = "+i_cQueryTable+".MDSERIAL";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MDSCOVEN = (select MDSCOVEN from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    if this.w_NHF>=0
      this.w_TMPC = "Aggiornamento campo  MDSCOVEN dettaglio vendite negozio avvenuto correttamente. "
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = "Conversione eseguita correttamente"
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='CORDRISP'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
