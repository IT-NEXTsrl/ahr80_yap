* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcb04                                                        *
*              Inserisce associazioni tabelle runtime                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-03-19                                                      *
* Last revis.: 2008-07-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcb04",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcb04 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_TABASS = space(30)
  w_NLOOP = 0
  * --- WorkFile variables
  TAB_RUNT_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inserisce associazioni tabelle runtime
    Dimension TAB(23,2)
     
 TAB[1,1]="DOC_MAST" 
 TAB[1,2]="DOCSMAST" 
 TAB[2,1]="DOC_DETT" 
 TAB[2,2]="DOCSDETT" 
 TAB[3,1]="DOC_RATE" 
 TAB[3,2]="DOCSRATE" 
 TAB[4,1]="PNT_MAST" 
 TAB[4,2]="PNTSMAST" 
 TAB[5,1]="PNT_DETT" 
 TAB[5,2]="PNTSDETT" 
 TAB[6,1]="PNT_IVA" 
 TAB[6,2]="PNTSIVA" 
 TAB[7,1]="MOVICOST" 
 TAB[7,2]="MOVISCOST" 
 TAB[8,1]="PAR_TITE" 
 TAB[8,2]="PARSTITE" 
 TAB[9,1]="LOTTIART" 
 TAB[9,2]="LOTSIART" 
 TAB[10,1]="SCA_VARI" 
 TAB[10,2]="SCASVARI"
    if g_APPLICATION="ADHOC REVOLUTION"
      this.w_NLOOP = 13
       
 TAB[11,1]="MATRICOL" 
 TAB[11,2]="STO_MATR" 
 TAB[12,1]="CCM_DETT" 
 TAB[12,2]="CCMSDETT" 
 TAB[13,1]="PNT_CESP" 
 TAB[13,2]="PNTSCESP" 
 
    else
      this.w_NLOOP = 23
       
 TAB[11,1]="MOVILOTT" 
 TAB[11,2]="MOVSLOTT" 
 TAB[12,1]="SIT_FIDI" 
 TAB[12,2]="SITSFIDI" 
 TAB[13,1]="MOP_DETT" 
 TAB[13,2]="MOPSDETT" 
 TAB[14,1]="MOP_DEAN" 
 TAB[14,2]="MOPSDEAN" 
 TAB[15,1]="ODL_MAST" 
 TAB[15,2]="ODLSMAST" 
 TAB[16,1]="ODL_DETT" 
 TAB[16,2]="ODLSDETT" 
 TAB[17,1]="ODL_CICL" 
 TAB[17,2]="ODLSCICL" 
 TAB[18,1]="ODL_RISO" 
 TAB[18,2]="ODLSRISO" 
 TAB[19,1]="SCCI_ASF" 
 TAB[19,2]="SCCISASF" 
 TAB[20,1]="AVA_PROD" 
 TAB[20,2]="AVASPROD" 
 TAB[21,1]="DIC_PROD" 
 TAB[21,2]="DICSPROD" 
 TAB[22,1]="DIC_REPA" 
 TAB[22,2]="DICSREPA" 
 TAB[23,1]="DIC_VALO" 
 TAB[23,2]="DICSVALO"
    endif
    if not IsAlt()
      * --- Try
      local bErr_047495F0
      bErr_047495F0=bTrsErr
      this.Try_047495F0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        * --- Gestisce log errori
        this.oParentObject.w_PMSG = Message()
        this.oParentObject.w_PESEOK = .F.
        if this.w_NHF>=0
          this.w_TMPC = ah_Msgformat("Errore generico - impossibile eseguire aggiornamento richiesto")
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
      bTrsErr=bTrsErr or bErr_047495F0
      * --- End
    else
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("Elaborazione terminata con successo")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
      * --- Esecuzione ok
      this.oParentObject.w_PESEOK = .T.
      this.oParentObject.w_PMSG = ah_Msgformat("Elaborazione terminata con successo")
    endif
  endproc
  proc Try_047495F0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    For i =1 to this.w_Nloop
    * --- Read from TAB_RUNT
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.TAB_RUNT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TAB_RUNT_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "RTTABASS"+;
        " from "+i_cTable+" TAB_RUNT where ";
            +"RTCODTAB = "+cp_ToStrODBC(TAB[I,1]);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        RTTABASS;
        from (i_cTable) where;
            RTCODTAB = TAB[I,1];
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_TABASS = NVL(cp_ToDate(_read_.RTTABASS),cp_NullValue(_read_.RTTABASS))
      use
      if i_Rows=0
        * --- Insert into TAB_RUNT
        i_nConn=i_TableProp[this.TAB_RUNT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TAB_RUNT_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TAB_RUNT_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"RTCODTAB"+",RTTABASS"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(TAB[I,1]),'TAB_RUNT','RTCODTAB');
          +","+cp_NullLink(cp_ToStrODBC(TAB[I,2]),'TAB_RUNT','RTTABASS');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'RTCODTAB',TAB[I,1],'RTTABASS',TAB[I,2])
          insert into (i_cTable) (RTCODTAB,RTTABASS &i_ccchkf. );
             values (;
               TAB[I,1];
               ,TAB[I,2];
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error='Errore inserimento misure'
          return
        endif
      endif
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    endfor
    this.w_TMPC = ah_Msgformat("Aggiornamento tabella TAB_RUNT eseguito con successo")
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = this.w_TMPC
    * --- commit
    cp_EndTrs(.t.)
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='TAB_RUNT'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
