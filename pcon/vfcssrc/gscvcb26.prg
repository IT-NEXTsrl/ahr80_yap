* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcb26                                                        *
*              Cancellazione campi AZIENDA                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_239]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-07-12                                                      *
* Last revis.: 2011-01-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcb26",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcb26 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_MRCODCOM = space(15)
  w_FRASE = space(200)
  w_CODAZI = space(5)
  w_CODREL = space(10)
  w_ESEGUITO = space(1)
  w_BCKLOG = space(200)
  w_NONESISTE = space(1)
  w_TROVATA = .f.
  w_CodRel = space(15)
  w_LOOP = 0
  w_ROWS = 0
  w_CAMPO = space(10)
  w_CONSTRAINT = space(200)
  w_STRINGA = space(250)
  w_CAMPI = space(0)
  w_EXIST = space(1)
  w_CFLD = space(250)
  w_ARCHIVIO = space(50)
  w_LOOP = 0
  w_NUM_FIELDS = 0
  * --- WorkFile variables
  AZIENDA_idx=0
  AZBACKUP_idx=0
  CONVERSI_idx=0
  TMPMOVIMAST_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- FIle di LOG
    this.w_CodRel = "7.0-M5502"
    * --- Valorizzo i campi della nuova tabella AZBACKUP
    * --- Cancello i campi dalla tabella AZIENDA
    this.w_NONESISTE = "N"
    this.w_ESEGUITO = "N"
    this.w_CODAZI = i_CODAZI
    * --- verifico preventivamente se la procedura deve essere eseguita
    * --- Try
    local bErr_0474BFF0
    bErr_0474BFF0=bTrsErr
    this.Try_0474BFF0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
      this.w_NONESISTE = "S"
    endif
    bTrsErr=bTrsErr or bErr_0474BFF0
    * --- End
    * --- Try
    local bErr_0468EA50
    bErr_0468EA50=bTrsErr
    this.Try_0468EA50()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_0468EA50
    * --- End
    if this.w_NONESISTE<>"S"
      * --- Try
      local bErr_04768620
      bErr_04768620=bTrsErr
      this.Try_04768620()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        * --- Gestisce log errori
        this.oParentObject.w_PMSG = Message()
        this.oParentObject.w_PESEOK = .F.
        if this.w_NHF>=0
          this.w_TMPC = this.oParentObject.w_PMSG
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
      bTrsErr=bTrsErr or bErr_04768620
      * --- End
    else
      * --- Esecuzione ok
      this.oParentObject.w_PESEOK = .T.
    endif
  endproc
  proc Try_0474BFF0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZBCKLOG"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZBCKLOG;
        from (i_cTable) where;
            AZCODAZI = this.w_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_BCKLOG = NVL(cp_ToDate(_read_.AZBCKLOG),cp_NullValue(_read_.AZBCKLOG))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    return
  proc Try_0468EA50()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Read from AZBACKUP
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZBACKUP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZBACKUP_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZBCKLOG"+;
        " from "+i_cTable+" AZBACKUP where ";
            +"AZCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZBCKLOG;
        from (i_cTable) where;
            AZCODAZI = this.w_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_BCKLOG = NVL(cp_ToDate(_read_.AZBCKLOG),cp_NullValue(_read_.AZBCKLOG))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if i_ROWS>0
      this.w_ESEGUITO = "S"
    endif
    return
  proc Try_04768620()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Legge se eseguita la conversione  7.0-M5502
    * --- Read from CONVERSI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CONVERSI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONVERSI_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "*"+;
        " from "+i_cTable+" CONVERSI where ";
            +"COCODREL = "+cp_ToStrODBC(this.w_CodRel);
            +" and COESEGUI = "+cp_ToStrODBC("S");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        *;
        from (i_cTable) where;
            COCODREL = this.w_CodRel;
            and COESEGUI = "S";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_TROVATA = i_Rows>0
    * --- begin transaction
    cp_BeginTrs()
    if NOT this.w_TROVATA
      if this.w_ESEGUITO<>"S"
        if IsAhe()
          * --- Insert into AZBACKUP
          i_nConn=i_TableProp[this.AZBACKUP_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.AZBACKUP_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_cTempTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
            i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"AZCODAZI,AZBCKLOG,AZORABCK,AZMINBCK,AZLUNBCK,AZMARBCK,AZMERBCK,AZGIOBCK,AZVENBCK,AZSABBCK,AZDOMBCK"," from "+i_cTempTable,this.AZBACKUP_idx)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        else
          * --- In AHR  i campi AZBCKDAY  e AZBCKNUM sono stati aggiunti successivamente agli altri relativi al backup (f.p. della 5.0)
          *     Pertanto occorre testare la loro presenza per differenziare l'inserimento
           
 i_nConn=i_TableProp[this.AZIENDA_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
          A=SQLEXEC(i_nConn,"Select  * from "+i_cTable + " where 1=0","CURS")
          afields(DATI2,"CURS")
          b= ASCAN(DATI2,"AZBCKDAY",1,1)
          if Used("CURS")
            Select ("CURS") 
 use
          endif
          * --- Delete from AZBACKUP
          i_nConn=i_TableProp[this.AZBACKUP_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.AZBACKUP_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"1 = "+cp_ToStrODBC(1);
                   )
          else
            delete from (i_cTable) where;
                  1 = 1;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
          if b >0
            * --- Insert into AZBACKUP
            i_nConn=i_TableProp[this.AZBACKUP_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.AZBACKUP_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_cTempTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
              i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"AZCODAZI,AZBCKLOG,AZORABCK,AZMINBCK,AZLUNBCK,AZMARBCK,AZMERBCK,AZGIOBCK,AZVENBCK,AZSABBCK,AZDOMBCK,AZBCKDAY,AZBCKNUM"," from "+i_cTempTable,this.AZBACKUP_idx)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          else
            * --- Insert into AZBACKUP
            i_nConn=i_TableProp[this.AZBACKUP_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.AZBACKUP_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_cTempTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
              i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"AZCODAZI,AZBCKLOG,AZORABCK,AZMINBCK,AZLUNBCK,AZMARBCK,AZMERBCK,AZGIOBCK,AZVENBCK,AZSABBCK,AZDOMBCK"," from "+i_cTempTable,this.AZBACKUP_idx)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          endif
        endif
      endif
      if upper(CP_DBTYPE)<>"DB2"
        if isAhe()
           
 Dimension ARFIELD(16) 
 ARFIELD[1]="AZBCKLOG" 
 ARFIELD[2]="AZORABCK" 
 ARFIELD[3]="AZMINBCK" 
 ARFIELD[4]="AZLUNBCK" 
 ARFIELD[5]="AZMARBCK" 
 ARFIELD[6]="AZMERBCK" 
 ARFIELD[7]="AZGIOBCK" 
 ARFIELD[8]="AZVENBCK" 
 ARFIELD[9]="AZSABBCK" 
 ARFIELD[10]="AZDOMBCK" 
 ARFIELD[11]="AZCATCLI" 
 ARFIELD[12]="AZCATFOR" 
 ARFIELD[13]="AZSUPCLI" 
 ARFIELD[14]="AZSUPFOR" 
 ARFIELD[15]="AZCONLIN" 
 ARFIELD[16]="AZCONVAL"
        else
           
 Dimension ARFIELD(12) 
 ARFIELD[1]="AZBCKLOG" 
 ARFIELD[2]="AZORABCK" 
 ARFIELD[3]="AZMINBCK" 
 ARFIELD[4]="AZLUNBCK" 
 ARFIELD[5]="AZMARBCK" 
 ARFIELD[6]="AZMERBCK" 
 ARFIELD[7]="AZGIOBCK" 
 ARFIELD[8]="AZVENBCK" 
 ARFIELD[9]="AZSABBCK" 
 ARFIELD[10]="AZDOMBCK" 
 ARFIELD[11]="AZBCKDAY" 
 ARFIELD[12]="AZBCKNUM"
        endif
      endif
      i_nConn=i_TableProp[this.AZIENDA_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
      if i_nConn<>0
        if upper(CP_DBTYPE)<>"DB2"
          this.w_LOOP = 0
           
 Dimension ArrTest[1,2] 
 ArrTest[1,1]="AZCODAZI" 
 ArrTest[1,2]=this.w_CODAZI
          do while this.w_LOOP< IIF(IsAhe(),16,12)
            this.w_LOOP = this.w_LOOP + 1
            this.w_EXIST = "N"
            this.w_CAMPO = ARFIELD[this.w_LOOP]
            CURSOR=READTABLE("AZIENDA",this.w_CAMPO,@ARRTEST,,,)
            if Used((CURSOR))
              this.w_EXIST = "S"
               
 Select (CURSOR) 
 use
            endif
            this.w_ROWS = 0
            * --- Elimino il constraint dovuto al valore di default sul campo da cancellare
            if this.w_EXIST="S"
              GSCV_BDC(this,.f., "AZIENDA", ARFIELD[this.w_LOOP])
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              this.w_ROWS = cp_SQLExec(i_nConn,"ALTER TABLE "+i_cTable+" DROP COLUMN " + ARFIELD[this.w_LOOP] )
              this.Pag3()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
          enddo
        else
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    if this.w_NHF>=0
      this.w_TMPC = this.oParentObject.w_PMSG
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella DB2
    * --- Carica dizionario dati (per sicurezza)
    =cp_ReadXdc()
    pName="AZIENDA"
    pDatabaseType=i_ServerConn[1,6]
    * --- Costruisco una stringa contenente l'elenco dei campi in analisi...
    this.w_ARCHIVIO = "AZIENDA"
    this.w_CFLD = ""
    this.w_LOOP = 1
    this.w_NUM_FIELDS = i_dcx.GetFieldsCount(this.w_ARCHIVIO)
    do while this.w_LOOP<=this.w_NUM_FIELDS
      this.w_CFLD = this.w_cFld+iif(empty(this.w_cFld),"",",")+i_dcx.GetFieldName(this.w_ARCHIVIO, this.w_LOOP )
      this.w_LOOP = this.w_LOOP + 1
    enddo
    * --- in coda NON aggiungo CPCCCHK
    * --- Occorre creare un temporaneo fittizio per poter instanziare in modo
    *     corretto le strutture dati Painter
    * --- Create temporary table TMPMOVIMAST
    i_nIdx=cp_AddTableDef('TMPMOVIMAST') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.AZIENDA_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"AZCODAZI "," from "+i_cTable;
          +" where 1=0";
          )
    this.TMPMOVIMAST_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Drop temporary table TMPMOVIMAST
    i_nIdx=cp_GetTableDefIdx('TMPMOVIMAST')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPMOVIMAST')
    endif
    * --- Copiato codice generato da una SELECT INTO, perch� non � possibile
    *     passare a tale istruzioni l'elenco dei campi tramite una variabile (w_CFLD)
     
 i_nIdx=cp_AddTableDef("TMPMOVIMAST") && aggiunge la definizione nella lista delle tabelle 
 i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato 
 i_nConn=i_TableProp[this.AZIENDA_idx,3] && recupera la connessione 
 i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2]) 
 cp_CreateTempTable(i_nConn,i_cTempTable, this.w_CFLD+" " ," from "+i_cTable )
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    * --- Elimino la tabella
    if Not GSCV_BDT( this, "AZIENDA" , i_nConn )
      * --- Raise
      i_Error="Error"
      return
    endif
    * --- Ricostruisco il database per la tabella
    GSCV_BRT(this, "AZIENDA" , i_nConn , .T.)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into AZIENDA
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.TMPMOVIMAST_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.AZIENDA_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_Msg("Ripristinati valori di origine",.T.)
    * --- Elimina tabella TMP
    * --- Drop temporary table TMPMOVIMAST
    i_nIdx=cp_GetTableDefIdx('TMPMOVIMAST')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPMOVIMAST')
    endif
    ah_Msg("Eliminata tabella temporanea",.T.)
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if this.w_ROWS<0
      * --- Raise
      i_Error=Message()
      return
    else
      * --- Esecuzione ok
      this.oParentObject.w_PESEOK = .T.
      this.w_TMPC = ah_msgformat("Eliminazione campo %1 eseguita con successo%0",arfield[this.w_loop])
      this.oParentObject.w_PMSG = this.oParentObject.w_PMSG + this.w_TMPC
    endif
  endproc


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='AZBACKUP'
    this.cWorkTables[3]='CONVERSI'
    this.cWorkTables[4]='*TMPMOVIMAST'
    return(this.OpenAllTables(4))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
