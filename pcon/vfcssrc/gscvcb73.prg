* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcb73                                                        *
*              Aggiornamento cproword nella tabella contatti                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-10-18                                                      *
* Last revis.: 2010-03-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcb73",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcb73 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_NCCODICE = space(15)
  * --- WorkFile variables
  NOM_CONT_idx=0
  PNT_DETT_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiornamento cproword nella tabella contatti
    * --- Try
    local bErr_04A27FD8
    bErr_04A27FD8=bTrsErr
    this.Try_04A27FD8()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = this.oParentObject.w_PMSG
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_04A27FD8
    * --- End
  endproc
  proc Try_04A27FD8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Select from NOM_CONT
    i_nConn=i_TableProp[this.NOM_CONT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.NOM_CONT_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select NCCODICE  from "+i_cTable+" NOM_CONT ";
           ,"_Curs_NOM_CONT")
    else
      select NCCODICE from (i_cTable);
        into cursor _Curs_NOM_CONT
    endif
    if used('_Curs_NOM_CONT')
      select _Curs_NOM_CONT
      locate for 1=1
      do while not(eof())
      this.w_NCCODICE = _Curs_NOM_CONT.NCCODICE
      * --- Write into NOM_CONT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.NOM_CONT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.NOM_CONT_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="NCCODICE,NCCODCON"
        do vq_exec with 'gscvcb73',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.NOM_CONT_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="NOM_CONT.NCCODICE = _t2.NCCODICE";
                +" and "+"NOM_CONT.NCCODCON = _t2.NCCODCON";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CPROWORD = _t2.CPROWORD";
            +i_ccchkf;
            +" from "+i_cTable+" NOM_CONT, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="NOM_CONT.NCCODICE = _t2.NCCODICE";
                +" and "+"NOM_CONT.NCCODCON = _t2.NCCODCON";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" NOM_CONT, "+i_cQueryTable+" _t2 set ";
            +"NOM_CONT.CPROWORD = _t2.CPROWORD";
            +Iif(Empty(i_ccchkf),"",",NOM_CONT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="NOM_CONT.NCCODICE = t2.NCCODICE";
                +" and "+"NOM_CONT.NCCODCON = t2.NCCODCON";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" NOM_CONT set (";
            +"CPROWORD";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.CPROWORD";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="NOM_CONT.NCCODICE = _t2.NCCODICE";
                +" and "+"NOM_CONT.NCCODCON = _t2.NCCODCON";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" NOM_CONT set ";
            +"CPROWORD = _t2.CPROWORD";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".NCCODICE = "+i_cQueryTable+".NCCODICE";
                +" and "+i_cTable+".NCCODCON = "+i_cQueryTable+".NCCODCON";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CPROWORD = (select CPROWORD from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
        select _Curs_NOM_CONT
        continue
      enddo
      use
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.w_TMPC = ah_Msgformat("Adeguamento campo contatore di riga in NOM_CONT eseguito correttamente")
    if this.w_NHF>=0
      this.w_TMPC = this.oParentObject.w_PMSG
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella DB2
    pName="PNT_DETT"
    pDatabaseType=i_ServerConn[1,6]
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.PNT_DETT_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
          )
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    * --- Elimino la tabella
    if Not GSCV_BDT( this, "PNT_DETT" , i_nConn )
      * --- Raise
      i_Error="Error"
      return
    endif
    * --- Ricostruisco il database per la tabella
    GSCV_BRT(this, "PNT_DETT" , i_nConn , .T.)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into PNT_DETT
    i_nConn=i_TableProp[this.PNT_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.PNT_DETT_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_Msg("Ripristinati valori di origine",.T.)
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    ah_Msg("Eliminata tabella temporanea tmp",.T.)
  endproc


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='NOM_CONT'
    this.cWorkTables[2]='PNT_DETT'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_NOM_CONT')
      use in _Curs_NOM_CONT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
