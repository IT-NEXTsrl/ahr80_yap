* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscv_bmn                                                        *
*              Modifica numerici                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_47]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-11-16                                                      *
* Last revis.: 2015-01-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,Archivio,Campo,RicTable
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscv_bmn",oParentObject,m.Archivio,m.Campo,m.RicTable)
return(i_retval)

define class tgscv_bmn as StdBatch
  * --- Local variables
  Archivio = space(15)
  Campo = space(10)
  RicTable = .f.
  w_LOOP = 0
  Result = space(10)
  ErrMess = space(10)
  Pos = 0
  k_err = .f.
  NameConst = space(10)
  Scale = 0
  NomeConstraint = space(10)
  k_Codazi = space(10)
  Tabella = space(10)
  tmpAzi = space(10)
  Lunghezza = 0
  decimali = 0
  precisione = 0
  Trovato = .f.
  NumChar = 0
  FldName = space(10)
  w_MESS = space(10)
  w_Dipendenza = space(50)
  w_ValDef = space(50)
  * --- WorkFile variables
  AZIENDA_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch per modifica campi numerici Esclusivamente in Sql Server che hanno un valore di default
    *     
    *     ATTENZIONE: ESCLUSIVAMENTE PER SQL SERVER
    k_Conn=i_TableProp[this.AZIENDA_IDX, 3]
    this.w_LOOP = 1
    do while this.w_LOOP <=2
      * --- valore di default per il numero di decimali se non trovo niente sul dizionario
      this.precisione = 4
      this.Lunghezza = 0
      this.decimali = 0
      this.NumChar = 3
      this.Tabella = ""
      this.k_Codazi = IIF(this.w_LOOP = 1, Upper(i_CODAZI),"XXX")
      this.Tabella = Alltrim(this.k_Codazi)+Upper( Alltrim(this.Archivio))
      * --- .f. se in presenza di un errore
      this.k_err = .T.
      * --- leggo dal dizionario la lunghezza e i decimali
      this.Trovato = .f.
      this.decimali = this.Precisione
      i_oldexact=set("EXACT")
      set exact on
      for i=1 to i_DCX.GetFieldsCount(this.Archivio)
      this.FldName = i_DCX.GetFieldName(this.Archivio,i)
      if this.fldname=this.campo
        this.decimali = i_Dcx.GetFieldDec(this.Archivio,i)
        this.Lunghezza = i_Dcx.GetFieldLen(this.Archivio,i)
        this.Trovato = .t.
        Exit
      endif
      Next
      set exact &i_oldexact
      * --- * se trovo il campo nel dizionario applico la sua lunghezza 
      *     * altrimenti non la vario (standard Precisione)
      if this.Trovato
        this.Scale = this.Decimali
      else
        this.Scale = this.precisione
      endif
      * --- se trovo il campo nel dizionario vado a leggere la sua lunghezza
      if !this.Trovato
        * --- non trovo il campo nel dizionario. 
        *     Ok perch� � dovuto a tabella presente nel database ma disattivato modulo. 
        *     In questo caso non viene caricato nel dizionario.
        *     Unico problema se venisse riattivato il modulo in seguito.
        *     La tabella non sarebbe aggiornata. Dovr� essere rieseguita la procedura
        i_retcode = 'stop'
        return
      else
        * --- mi dice se devo costruire o meno il constraints con il valore di default
        Valoredef = "N"
        * --- provo immediatamente a cambiare il tipo in modo diretto
        *     ALTER TABLE XXXSALDICON ALTER COLUMN SLAVEPER DECIMAL(18,2) 
        this.Result = SqlExec(k_Conn, "Alter table "+this.tabella+" ALTER COLUMN "+this.CAMPO+" DECIMAL("+alltrim(str(this.lunghezza))+","+Str(this.Scale)+")  " )
        * --- .f. se in presenza di un errore
        this.k_err = .t.
        if this.Result = -1
          this.ErrMess = Alltrim(Message())
          * --- Cancella tutti i CONSTRRAINTS
          *     Definita nell'area manuale
          this.k_err = this.DropContraints(this.Tabella,this.Campo,this.Scale,this.ErrMess,this.Lunghezza)
        endif
        if Valoredef = "N"
          * ---  devo aggiungere il valore di default Nome: DF_<archivio>_AHEDECIMALI_sys(2105) numero univoco di Fox
          *      questo perch� ogni contraints deve avere nome diverso
          this.Result = SqlExec(k_Conn, "Alter table "+this.tabella+" ADD CONSTRAINT DF_"+this.tabella+"_AHRDECIMALI_"+sys(2015)+" DEFAULT 0 FOR "+this.CAMPO )
          if this.Result = -1
            * --- Column already has a DEFAULT bound to it
            *     c'� gi� valore di default a 0 e comunque � riuscito a modificare il campo a Numerico 15
            *     quindi OK
            *     Store Procedure per avere le propriet� della tabella
            *     Nel caso c'� gi� valore di default a 0 � OK
            this.Result = SqlExec(k_Conn, "EXECUTE sp_MShelpcolumns N'[" + ALLTRIM(this.Tabella)+"]' , NULL, 1","__PropCampi__")
            if USED("__PropCampi__")
              this.w_Dipendenza = ""
              this.w_ValDef = ""
               
 Select("__PropCampi__") 
 Locate For Upper(Col_Name) = Upper(this.CAMPO)
              if Found()
                this.w_Dipendenza = Alltrim(__PropCampi__.col_dridefname)
                this.w_ValDef = Alltrim(__PropCampi__.text)
                * --- ValDef contiene il valore di default ma poich� SqlServer ritorna dei caratteri particolari
                *     non riesco a valutarli. Mi basta comunque sapere se � presente un contraint che indica il valore di default
                if Not Empty(this.w_Dipendenza)
                  * --- Il valore di default � gi� impostato a 0 e il campo � gi� stato modificato nel numerico desiderato
                  *     Quindi Ok
                  this.k_err = .T.
                else
                  this.k_err = .f.
                  this.ErrMess=ah_MsgFormat("Impossibile impostare valore di default")
                endif
              else
                this.ErrMess=ah_MsgFormat("Campo %1 non presente",this.campo)
              endif
               
 Select("__PropCampi__") 
 Use
            endif
          else
            this.k_err = this.Result<>-1
          endif
          if this.k_err 
            ah_Msg("Impostato valore default 0 in %1 campo: %2",.T.,.F.,.F., this.Archivio, this.campo)
          else
            ah_Msg("Errore impostando valore default 0 in %1 campo: %2: %3",.T.,.F.,.F.,this.Archivio, this.campo, message() )
          endif
        endif
      endif
      this.w_LOOP = this.w_LOOP + 1
    enddo
    * --- Solo se richiesto, aggiorno la tabella.
    if this.RicTable
      GSCV_BRT(this,Alltrim(this.archivio), k_Conn , .T.)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- Se ho incontrato un errore ritorno il messaggio
    if this.k_err
      i_retcode = 'stop'
      return
    else
      i_retcode = 'stop'
      i_retval = Message()
      return
    endif
  endproc


  proc Init(oParentObject,Archivio,Campo,RicTable)
    this.Archivio=Archivio
    this.Campo=Campo
    this.RicTable=RicTable
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='AZIENDA'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- gscv_bmn
  * la procedura cancella tutti i CONSTRAINTS validi sul campo
  * se incontra un Constraints di tipo DEFAULT lo rimette (al ricostruzione database non lo fa )
  * restituisce .f. se incontra un errore .t. se tutto OK
  
  Func DropContraints(Tabella,Campo,Scale,ErrMess,Lunghezza)
  Local Pos,NomeCon,DefCon,Result,DisAllowNull, lIndexError
  * provo a cancellare il constrants dei valori di default
  * a seconda se SQL Server 2000 o SQL Server 7 ho 2 messaggi di risposta differenti
  * in SQL Server 2000: The object 'DF__TAMSWSALD__SLDAR__5CACADF9' is dependent on column 'sldarfin'.
  * quindi leggo la stringa tra i primi 2 apici mentre in SQL Server 7:
  * ALTER TABLE ALTER COLUMN AZDENMAG failed because DEFAULT CONSTRAINT DF__AZIENDA__AZDENMA__17036CC0 accesses this column.
  * cerco DEFAULT CONSTRAINT e prendo fino a accesses.
  * comincio da SQL Server 2000 (se trovo l'apice)
  * cerco il primo apice
  DisAllowNull=.f.
  lIndexError = .f.
  NomeCon=this.NomeContraints(ErrMess)
  * cancello tutti i Constraints
  Do While !Empty(Nomecon)
  
   Result=SqlExec(k_Conn, 'Alter table '+Tabella+' DROP CONSTRAINT '+NomeCon)
   If Result=-1
    * esco con errore, non riesco a cancellare il constraints
    NomeCon=''
    Return .f.
    
   Else
   
  	  if Upper(Left(Nomecon,2))='DF' 
  	   Valoredef='S'
  	   DefCon=NomeCon
  	  Endif
  	  
  	  If Upper(Left(Nomecon,2))='PK' 
  	    * se cancello la chiave primaria lo segno nel Log degli Errori
  	     *LogErrori(Tabella,campo,Alltrim('Eliminata la chiave primaria, ricostruire il Database'))      
  	    * se cancello la chiave primaria devo ricordarmi di togliere il flag Allow Null 
  		DisAllowNull=.t.
  	  Endif
  
  	  * provo a modificare il tipo del campo  
  	  if DisAllowNull
  	      * se chiave primaria devo diabilitare l'Allow Null
    	      Result=SqlExec(k_Conn, 'Alter table '+Tabella+' ALTER COLUMN '+CAMPO+' DECIMAL('+alltrim(str(lunghezza))+','+Str(Scale)+')  '+' not null' )
    	  else
       	  Result=SqlExec(k_Conn, 'Alter table '+Tabella+' ALTER COLUMN '+CAMPO+' DECIMAL('+alltrim(str(lunghezza))+','+Str(Scale)+')  ' )
    	  endif
    	  
  	  If RESULT=-1 
  	    ErrMess=Message()
  	    NomeCon=this.NomeContraints(ErrMess)
        lIndexError = UPPER(LEFT(NomeCon, LEN(ALLTRIM(Tabella))) )= UPPER(ALLTRIM(Tabella))
  	  Else
  	    NomeCon=''
  	  ENDIF
  	  * --- Incontrato errore per presenza indice sul campo
  	  IF lIndexError
  	     Result=SqlExec(k_Conn, "EXECUTE sp_MShelpindex N'[" + ALLTRIM(Tabella) + "]' , NULL, 1","__IndexKey__")
  	     IF USED("__IndexKey__")
  	        SELECT("__IndexKey__")
  	        LOCATE FOR UPPER(Name) = UPPER(NomeCon)
  	        IF FOUND()
  	           Result=SqlExec(k_Conn, "drop index "+ALLTRIM(Tabella)+"."+ALLTRIM(NomeCon) )
  	           IF Result= -1
  	              ErrMess=Message()
  	           ELSE
  	           	  if DisAllowNull
  	              * a questo punto provo a rifare la modifica del tipo di dato
    	      			 Result=SqlExec(k_Conn, 'Alter table '+Tabella+' ALTER COLUMN '+CAMPO+' DECIMAL('+alltrim(str(lunghezza))+','+Str(Scale)+')  '+' not null' )
    	  			  else
       	             Result=SqlExec(k_Conn, 'Alter table '+Tabella+' ALTER COLUMN '+CAMPO+' DECIMAL('+alltrim(str(lunghezza))+','+Str(Scale)+')  ' )
              	  endif
  	              NomeCon=''
  	           Endif
  	        ELSE
                 ErrMess=ah_MsgFormat("Impossibile eliminare gli indici della tabella")
  	        Endif
            SELECT("__IndexKey__")
            Use
  	     Endif   
  	     
  	  Endif
    
   Endif
  Enddo
  
  If  Not Empty(ErrMess)  And At("Arithmetic overflow error converting float to data type numeric",ErrMess)<>0
    * se ho un errore all'esecuzione della SQLEXEC	(non riesce a convertire) 
    * se ho errore di Overflow passo a due decimali
   *LogErrori(Tabella,campo,Alltrim('Scalato a precisione 2 '+message())) 
   scale=2 
   Result=SqlExec(k_Conn, 'Alter table '+Tabella+' ALTER COLUMN '+CAMPO+' DECIMAL('+alltrim(str(lunghezza))+','+Str(Scale)+')  ' )
   Return  (result<>-1)      
  Endif
  
  
  * rimetto il valore di Default
  if ValoreDef='S'
  	Result=SqlExec(k_Conn, 'Alter table '+Tabella+' ADD CONSTRAINT '+DefCon+' DEFAULT 0 FOR '+CAMPO )
  	* non riesco a ricostruire il  CONSTRAINT per il valore di Default
  	ValoreDef=IIF(result=-1,'N','S')
  	Return  (result<>-1)      
  EndIf
  
  Return .t.
  
  EndFunc
  
  
  * -- Funzione che ritorna il vero nome del constraint
  Func NomeContraints(Messaggio)
  Local Pos,Mess2
  Mess2=Messaggio
  Pos=At("'" , Mess2  )
  if pos<>0
     * leggo il nome del constrains che da il problema
     Mess2=Right(Mess2,Len(Mess2)-Pos)
     Pos=At("'" , Mess2  )
     * cerco l'altro apice e in ErrMess dovrei trovarmi il nome del contraint da droppare
     return Alltrim(Left(Mess2,Pos-1))
  else
     * caso SQL Server 7.0
     * non trovo l'apice cerco CONSTRAINT
     * non riesco a gestire l'errore
     Pos=At("CONSTRAINT" , Mess2  )
     if pos<>0
       * l'ho trovato cerco accesses
       Mess2=Right(Mess2,Len(Mess2)-Pos-10)
       Pos=At("accesses" , Mess2 )
       * cerco accesses e in ErrMess dovrei trovarmi il nome del contraint da droppare
       Return Alltrim(Left(Mess2,Pos-1))
     else
       Pos=At("INDEX" , Mess2  )
       if pos<>0
         * l'ho trovato cerco accesses
         Mess2=Right(Mess2,Len(Mess2)-Pos-5)
         Pos=At("accesses" , Mess2 )
         * cerco accesses e in ErrMess dovrei trovarmi il nome del contraint da droppare
         Return Alltrim(Left(Mess2,Pos-1))
       Else
         *non riesco a capire dal messaggio di errore il constraints
         Return ''
       Endif
     endif
  endif
  endfunc
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="Archivio,Campo,RicTable"
endproc
