* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscv_bb1                                                        *
*              Pulitura campo agente                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_49]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-09-29                                                      *
* Last revis.: 2003-09-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscv_bb1",oParentObject,m.w_NHF)
return(i_retval)

define class tgscv_bb1 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_NHF0 = 0
  * --- WorkFile variables
  DOC_MAST_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Procedura di conversione per pulitura Codice Agente nei documenti del ciclo passivo
    * --- FIle di LOG
    * --- Write into DOC_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVCODAGE ="+cp_NullLink(cp_ToStrODBC(""),'DOC_MAST','MVCODAGE');
      +",MVCODAG2 ="+cp_NullLink(cp_ToStrODBC(""),'DOC_MAST','MVCODAG2');
          +i_ccchkf ;
      +" where ";
          +"MVFLVEAC = "+cp_ToStrODBC("A");
          +" and MVTIPCON = "+cp_ToStrODBC("F");
          +" and MVCODAGE is not null ";
             )
    else
      update (i_cTable) set;
          MVCODAGE = "";
          ,MVCODAG2 = "";
          &i_ccchkf. ;
       where;
          MVFLVEAC = "A";
          and MVTIPCON = "F";
          and MVCODAGE is not null;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Write into DOC_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVCODAG2 ="+cp_NullLink(cp_ToStrODBC(""),'DOC_MAST','MVCODAG2');
          +i_ccchkf ;
      +" where ";
          +"MVFLVEAC = "+cp_ToStrODBC("A");
          +" and MVTIPCON = "+cp_ToStrODBC("F");
          +" and MVCODAG2 is not null ";
             )
    else
      update (i_cTable) set;
          MVCODAG2 = "";
          &i_ccchkf. ;
       where;
          MVFLVEAC = "A";
          and MVTIPCON = "F";
          and MVCODAG2 is not null;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.w_TMPC = ah_Msgformat("Pulitura campi agente e capoarea eseguita correttamente%0")
    this.oParentObject.w_PMSG = this.w_TMPC
  endproc


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='DOC_MAST'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
