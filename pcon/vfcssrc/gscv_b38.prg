* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscv_b38                                                        *
*              3.0 - aggiorna sezione di bilancio su mastri                    *
*                                                                              *
*      Author: ZUCCHETTI TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_15]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-05-06                                                      *
* Last revis.: 2002-05-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscv_b38",oParentObject,m.w_NHF)
return(i_retval)

define class tgscv_b38 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_MESS = space(100)
  w_MASTRI = space(15)
  w_CONSUP = space(15)
  w_SEZBIL = space(1)
  w_KO = 0
  * --- WorkFile variables
  MASTRI_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questa procedura aggiorna la sezione di bilancio sui mastri di livello inferiore al massimo.
    * --- FIle di LOG
    this.w_KO = 0
    * --- Try
    local bErr_0375A070
    bErr_0375A070=bTrsErr
    this.Try_0375A070()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE GENERICO - impossibile aggiornare sezione di bilancio su MASTRI")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_0375A070
    * --- End
  endproc
  proc Try_0375A070()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Aggiorno flag tipo conto sui mastri di livello > di 1 perch� deve essere 'G'
    * --- Write into MASTRI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.MASTRI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MASTRI_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.MASTRI_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MCTIPMAS ="+cp_NullLink(cp_ToStrODBC("G"),'MASTRI','MCTIPMAS');
          +i_ccchkf ;
      +" where ";
          +"MCNUMLIV > "+cp_ToStrODBC(1);
          +" and MCTIPMAS <> "+cp_ToStrODBC("G");
             )
    else
      update (i_cTable) set;
          MCTIPMAS = "G";
          &i_ccchkf. ;
       where;
          MCNUMLIV > 1;
          and MCTIPMAS <> "G";

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Vado a leggere sui mastri tutti quelli di livello non massimo per calcolare
    *     la sezione di bilancio.
    vq_exec("..\pcon\exe\query\gscv0b38.vqr",this,"Mastri")
    if USED("Mastri")
      if Reccount ("Mastri")>0
        Select Mastri
        Go Top
        Scan
        this.w_MASTRI = Mastri.Mccodice
        this.w_CONSUP = Mastri.Mcconsup
        * --- Calcolo la sezione di bilancio
        this.w_SEZBIL = CALCSEZ(this.w_CONSUP)
        * --- Effettuo la scrittura nella tabella mastri della sezione di bilancio
        * --- Write into MASTRI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.MASTRI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MASTRI_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.MASTRI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MCSEZBIL ="+cp_NullLink(cp_ToStrODBC(this.w_SEZBIL),'MASTRI','MCSEZBIL');
              +i_ccchkf ;
          +" where ";
              +"MCCODICE = "+cp_ToStrODBC(this.w_MASTRI);
                 )
        else
          update (i_cTable) set;
              MCSEZBIL = this.w_SEZBIL;
              &i_ccchkf. ;
           where;
              MCCODICE = this.w_MASTRI;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        Endscan
        * --- Controlla se esistono mastri senza sezione di bilancio
        * --- Select from MASTRI
        i_nConn=i_TableProp[this.MASTRI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MASTRI_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" MASTRI ";
              +" where (MCSEZBIL=' ' or MCSEZBIL is NULL)";
               ,"_Curs_MASTRI")
        else
          select * from (i_cTable);
           where (MCSEZBIL=" " or MCSEZBIL is NULL);
            into cursor _Curs_MASTRI
        endif
        if used('_Curs_MASTRI')
          select _Curs_MASTRI
          locate for 1=1
          do while not(eof())
          * --- Gestisce log errori
          this.w_KO = this.w_KO+1
          if this.w_KO=1
            if this.w_NHF>=0
              this.w_TMPC = ah_Msgformat("Elenco MASTRI per i quali non � stata aggiornata la sezione di bilancio")
              this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
            endif
          endif
          if this.w_NHF>=0
            this.w_TMPC = "- "+_Curs_MASTRI.MCCODICE+" "+_Curs_MASTRI.MCDESCRI
            this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
          endif
            select _Curs_MASTRI
            continue
          enddo
          use
        endif
        * --- Esecuzione ok
        if this.w_KO=0
          this.w_TMPC = ah_Msgformat("Conversione eseguita correttamente")
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
        this.oParentObject.w_PESEOK = .T.
        this.oParentObject.w_PMSG = ah_Msgformat("Conversione eseguita correttamente")
      else
        * --- Gestisce log errori
        if this.w_NHF>=0
          this.w_TMPC = ah_Msgformat("ATTENZIONE: non esistono dati da convertire")
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
        * --- Esecuzione ok
        this.oParentObject.w_PESEOK = .T.
        this.oParentObject.w_PMSG = this.w_TMPC
      endif
      * --- Chiude cursore
      select Mastri
      use
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='MASTRI'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_MASTRI')
      use in _Curs_MASTRI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
