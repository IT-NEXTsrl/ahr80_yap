* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscv_b53                                                        *
*              Aggiornamento criteri statistici                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_5]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-11-19                                                      *
* Last revis.: 2001-11-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscv_b53",oParentObject,m.w_NHF)
return(i_retval)

define class tgscv_b53 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_FLGRAF = space(100)
  * --- WorkFile variables
  CRIMELAB_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- FIle di LOG
    * --- Try
    local bErr_037A0A70
    bErr_037A0A70=bTrsErr
    this.Try_037A0A70()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE - impossibile aggiornare CENUMCAM su tabella CRIDELAB")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_037A0A70
    * --- End
  endproc
  proc Try_037A0A70()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Aggiornamento  CENUMCAM
    * --- Select from CRIMELAB
    i_nConn=i_TableProp[this.CRIMELAB_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CRIMELAB_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" CRIMELAB ";
           ,"_Curs_CRIMELAB")
    else
      select * from (i_cTable);
        into cursor _Curs_CRIMELAB
    endif
    if used('_Curs_CRIMELAB')
      select _Curs_CRIMELAB
      locate for 1=1
      do while not(eof())
      this.w_FLGRAF = NVL(_Curs_CRIMELAB.CENOMGRP," ")
      if NOT EMPTY(this.w_FLGRAF)
        * --- Write into CRIMELAB
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.CRIMELAB_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CRIMELAB_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CRIMELAB_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CEFLGRAF ="+cp_NullLink(cp_ToStrODBC("S"),'CRIMELAB','CEFLGRAF');
              +i_ccchkf ;
          +" where ";
              +"CECODICE = "+cp_ToStrODBC(_Curs_CRIMELAB.CECODICE);
                 )
        else
          update (i_cTable) set;
              CEFLGRAF = "S";
              &i_ccchkf. ;
           where;
              CECODICE = _Curs_CRIMELAB.CECODICE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
        select _Curs_CRIMELAB
        continue
      enddo
      use
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Aggiornamento campo CEFLGRAF su tabella CRIMELAB eseguito con successo")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = this.oParentObject.w_PMSG + this.w_TMPC 
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='CRIMELAB'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_CRIMELAB')
      use in _Curs_CRIMELAB
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
