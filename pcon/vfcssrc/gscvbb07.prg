* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvbb07                                                        *
*              Aggiorna riferimenti partite SBF                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_172]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-02-23                                                      *
* Last revis.: 2001-02-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvbb07",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvbb07 as StdBatch
  * --- Local variables
  w_DATAGG = ctod("  /  /  ")
  w_CODCON = space(15)
  w_TIPCON = space(1)
  w_CODVAL = space(3)
  w_DATINC = ctod("  /  /  ")
  w_IMPINC = 0
  w_MODPAG = space(10)
  w_DATSCA = ctod("  /  /  ")
  w_DESCRI = space(40)
  w_STATUS = space(1)
  w_NUMREG = 0
  w_TIPO = space(2)
  w_DATREG = ctod("  /  /  ")
  w_COMPET = space(4)
  w_OSERCONT = space(10)
  w_PTSERIAL = space(10)
  w_PTROWORD = 0
  w_CPROWNUM = 0
  w_ROWINC = 0
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_MESS = space(100)
  * --- WorkFile variables
  PAR_TITE_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Esegue valorizzazione campi IASERRIF,IAORDRIF,IANUMRIF
    * --- FIle di LOG
    * --- Azzera riferimenti Partite collegate conti SBF
    * --- Try
    local bErr_0376D2A0
    bErr_0376D2A0=bTrsErr
    this.Try_0376D2A0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE GENERICO - impossibile aggiornare struttura piano conti trasferimento studio")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
    endif
    bTrsErr=bTrsErr or bErr_0376D2A0
    * --- End
  endproc
  proc Try_0376D2A0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Azzero riferimenti per le partite legate a conti SBF di creazione
    * --- Write into PAR_TITE
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PAR_TITE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_TITE_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PTSERRIF ="+cp_NullLink(cp_ToStrODBC(Space(10)),'PAR_TITE','PTSERRIF');
      +",PTORDRIF ="+cp_NullLink(cp_ToStrODBC(0),'PAR_TITE','PTORDRIF');
      +",PTNUMRIF ="+cp_NullLink(cp_ToStrODBC(0),'PAR_TITE','PTNUMRIF');
          +i_ccchkf ;
      +" where ";
          +"PTTIPCON = "+cp_ToStrODBC("G");
          +" and PTFLCRSA = "+cp_ToStrODBC("C");
          +" and PTORDRIF <> "+cp_ToStrODBC(0);
             )
    else
      update (i_cTable) set;
          PTSERRIF = Space(10);
          ,PTORDRIF = 0;
          ,PTNUMRIF = 0;
          &i_ccchkf. ;
       where;
          PTTIPCON = "G";
          and PTFLCRSA = "C";
          and PTORDRIF <> 0;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    * --- commit
    cp_EndTrs(.t.)
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='PAR_TITE'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
