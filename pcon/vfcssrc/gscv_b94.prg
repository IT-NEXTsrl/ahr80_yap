* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscv_b94                                                        *
*              Aggiornamento codice agente                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_33]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-12-02                                                      *
* Last revis.: 2002-12-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscv_b94",oParentObject,m.w_NHF)
return(i_retval)

define class tgscv_b94 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_TIPCON = space(1)
  w_CODCON = space(15)
  w_DATSCA = ctod("  /  /  ")
  w_NUMPAR = space(14)
  w_CODVAL = space(5)
  w_PTSERIAL = space(10)
  w_PTROWORD = 0
  w_CPROWNUM = 0
  w_CODAGE = space(5)
  w_SERIAL = space(10)
  w_ROWORD = 0
  w_ROWNUM = 0
  w_SERRIF = space(10)
  * --- WorkFile variables
  PNT_DETT_idx=0
  PAR_TITE_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questa procedura di conversione modifica il valore della combo COFLUCOA
    * --- Try
    local bErr_03608990
    bErr_03608990=bTrsErr
    this.Try_03608990()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE - impossibile aggiornare PNCODAGE, PTCODAGE su tabelle PNT_DETT, PAR_TITE") 
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_03608990
    * --- End
  endproc
  proc Try_03608990()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Aggiornamento  Codice Agente
    * --- Select from gscv_b94
    do vq_exec with 'gscv_b94',this,'_Curs_gscv_b94','',.f.,.t.
    if used('_Curs_gscv_b94')
      select _Curs_gscv_b94
      locate for 1=1
      do while not(eof())
      this.w_TIPCON = NVL(_Curs_gscv_b94.PTTIPCON," ")
      this.w_CODCON = NVL(_Curs_gscv_b94.PTCODCON,SPACE(15))
      this.w_DATSCA = CP_TODATE(_Curs_gscv_b94.PTDATSCA)
      this.w_NUMPAR = NVL(_Curs_gscv_b94.PTNUMPAR,SPACE(14))
      this.w_CODVAL = NVL(_Curs_gscv_b94.PTCODVAL,SPACE(5))
      this.w_PTSERIAL = _Curs_gscv_b94.PTSERIAL
      this.w_PTROWORD = _Curs_gscv_b94.PTROWORD
      this.w_CPROWNUM = _Curs_gscv_b94.CPROWNUM
      this.w_CODAGE = NVL(_Curs_gscv_b94.MVCODAGE,SPACE(5))
      * --- Aggiorno Campo in Primanota
      if this.w_PTROWORD>0
        * --- Write into PNT_DETT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.PNT_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.PNT_DETT_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PNCODAGE ="+cp_NullLink(cp_ToStrODBC(this.w_CODAGE),'PNT_DETT','PNCODAGE');
              +i_ccchkf ;
          +" where ";
              +"PNSERIAL = "+cp_ToStrODBC(this.w_PTSERIAL);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_PTROWORD);
                 )
        else
          update (i_cTable) set;
              PNCODAGE = this.w_CODAGE;
              &i_ccchkf. ;
           where;
              PNSERIAL = this.w_PTSERIAL;
              and CPROWNUM = this.w_PTROWORD;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error='Errore scrittura codice agente in primanota'
          return
        endif
      endif
      * --- Inserisco Codice Agente Nelle Partite di Origine.
      * --- Write into PAR_TITE
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.PAR_TITE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_TITE_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"PTCODAGE ="+cp_NullLink(cp_ToStrODBC(this.w_CODAGE),'PAR_TITE','PTCODAGE');
            +i_ccchkf ;
        +" where ";
            +"PTNUMPAR = "+cp_ToStrODBC(this.w_NUMPAR);
            +" and PTDATSCA = "+cp_ToStrODBC(this.w_DATSCA);
            +" and PTTIPCON = "+cp_ToStrODBC(this.w_TIPCON);
            +" and PTCODCON = "+cp_ToStrODBC(this.w_CODCON);
            +" and PTCODVAL = "+cp_ToStrODBC(this.w_CODVAL);
               )
      else
        update (i_cTable) set;
            PTCODAGE = this.w_CODAGE;
            &i_ccchkf. ;
         where;
            PTNUMPAR = this.w_NUMPAR;
            and PTDATSCA = this.w_DATSCA;
            and PTTIPCON = this.w_TIPCON;
            and PTCODCON = this.w_CODCON;
            and PTCODVAL = this.w_CODVAL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error='Errore scrittura riferimenti'
        return
      endif
        select _Curs_gscv_b94
        continue
      enddo
      use
    endif
    * --- Select from gscvrb94
    do vq_exec with 'gscvrb94',this,'_Curs_gscvrb94','',.f.,.t.
    if used('_Curs_gscvrb94')
      select _Curs_gscvrb94
      locate for 1=1
      do while not(eof())
      this.w_PTSERIAL = _Curs_gscvrb94.PTSERIAL
      this.w_PTROWORD = _Curs_gscvrb94.PTROWORD
      this.w_CPROWNUM = _Curs_gscvrb94.CPROWNUM
      this.w_TIPCON = NVL(_Curs_gscvrb94.PTTIPCON," ")
      this.w_CODCON = NVL(_Curs_gscvrb94.PTCODCON,SPACE(15))
      this.w_DATSCA = CP_TODATE(_Curs_gscvrb94.PTDATSCA)
      this.w_NUMPAR = NVL(_Curs_gscvrb94.PTNUMPAR,SPACE(14))
      this.w_CODVAL = NVL(_Curs_gscvrb94.PTCODVAL,SPACE(5))
      this.w_PTSERIAL = _Curs_gscvrb94.PTSERIAL
      this.w_PTROWORD = _Curs_gscvrb94.PTROWORD
      this.w_CPROWNUM = _Curs_gscvrb94.CPROWNUM
      * --- Select from PAR_TITE
      i_nConn=i_TableProp[this.PAR_TITE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" PAR_TITE ";
            +" where PTCODCON= "+cp_ToStrODBC(this.w_CODCON)+" AND PTTIPCON= "+cp_ToStrODBC(this.w_TIPCON)+" AND PTCODVAL= "+cp_ToStrODBC(this.w_CODVAL)+" AND PTNUMPAR= "+cp_ToStrODBC(this.w_NUMPAR)+" AND PTDATSCA= "+cp_ToStrODBC(this.w_DATSCA)+"   AND NOT( PTSERIAL= "+cp_ToStrODBC(this.w_PTSERIAL)+" AND PTROWORD= "+cp_ToStrODBC(this.w_PTROWORD)+" AND CPROWNUM= "+cp_ToStrODBC(this.w_CPROWNUM)+")";
             ,"_Curs_PAR_TITE")
      else
        select * from (i_cTable);
         where PTCODCON= this.w_CODCON AND PTTIPCON= this.w_TIPCON AND PTCODVAL= this.w_CODVAL AND PTNUMPAR= this.w_NUMPAR AND PTDATSCA= this.w_DATSCA   AND NOT( PTSERIAL= this.w_PTSERIAL AND PTROWORD= this.w_PTROWORD AND CPROWNUM= this.w_CPROWNUM);
          into cursor _Curs_PAR_TITE
      endif
      if used('_Curs_PAR_TITE')
        select _Curs_PAR_TITE
        locate for 1=1
        do while not(eof())
        * --- Inserisco riferimenti della partita di origine nelle partite collegate.
        this.w_SERIAL = _Curs_PAR_TITE.PTSERIAL
        this.w_ROWORD = _Curs_PAR_TITE.PTROWORD
        this.w_ROWNUM = _Curs_PAR_TITE.CPROWNUM
        this.w_SERRIF = NVL(_Curs_PAR_TITE.PTSERRIF,SPACE(10))
        if EMPTY(this.w_SERRIF)
          * --- Write into PAR_TITE
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.PAR_TITE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_TITE_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"PTSERRIF ="+cp_NullLink(cp_ToStrODBC(this.w_PTSERIAL),'PAR_TITE','PTSERRIF');
            +",PTORDRIF ="+cp_NullLink(cp_ToStrODBC(this.w_PTROWORD),'PAR_TITE','PTORDRIF');
            +",PTNUMRIF ="+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'PAR_TITE','PTNUMRIF');
                +i_ccchkf ;
            +" where ";
                +"PTSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                +" and PTROWORD = "+cp_ToStrODBC(this.w_ROWORD);
                +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWNUM);
                   )
          else
            update (i_cTable) set;
                PTSERRIF = this.w_PTSERIAL;
                ,PTORDRIF = this.w_PTROWORD;
                ,PTNUMRIF = this.w_CPROWNUM;
                &i_ccchkf. ;
             where;
                PTSERIAL = this.w_SERIAL;
                and PTROWORD = this.w_ROWORD;
                and CPROWNUM = this.w_ROWNUM;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error='Errore scrittura riferimenti'
            return
          endif
        endif
          select _Curs_PAR_TITE
          continue
        enddo
        use
      endif
        select _Curs_gscvrb94
        continue
      enddo
      use
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Aggiornamento campo agente su tabelle PAR_TITE e PNT_DETT eseguito con successo")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = this.oParentObject.w_PMSG + this.w_TMPC 
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='PNT_DETT'
    this.cWorkTables[2]='PAR_TITE'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_gscv_b94')
      use in _Curs_gscv_b94
    endif
    if used('_Curs_gscvrb94')
      use in _Curs_gscvrb94
    endif
    if used('_Curs_PAR_TITE')
      use in _Curs_PAR_TITE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
