* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcb84                                                        *
*              Polis Web: pratiche con stesso RG in fori diversi               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-09-27                                                      *
* Last revis.: 2011-04-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcb84",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcb84 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_NUMRIG = 0
  w_NUMRID = 0
  w_PWSERIAL = space(10)
  * --- WorkFile variables
  PWMEVENT_idx=0
  PWDEVENT_idx=0
  PWE_MAST_idx=0
  PWE_DETT_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiornament nuove tabelle PWE_MAST/PWE_DETT
    if IsAlt()
      * --- Solo per AeTop
      * --- Try
      local bErr_038B4548
      bErr_038B4548=bTrsErr
      this.Try_038B4548()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- Gestisce log errori
        this.oParentObject.w_PMSG = Message()
        this.oParentObject.w_PESEOK = .F.
        if this.w_NHF>=0
          this.w_TMPC = ah_Msgformat("Errore generico - impossibile eseguire aggiornamento richiesto")
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
      bTrsErr=bTrsErr or bErr_038B4548
      * --- End
      if this.w_NUMRID>0
        * --- Try
        local bErr_04C025A8
        bErr_04C025A8=bTrsErr
        this.Try_04C025A8()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          * --- Gestisce log errori
          this.oParentObject.w_PMSG = Message()
          this.oParentObject.w_PESEOK = .F.
          if this.w_NHF>=0
            this.w_TMPC = ah_Msgformat("Errore generico - impossibile eseguire aggiornamento richiesto")
            this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
          endif
        endif
        bTrsErr=bTrsErr or bErr_04C025A8
        * --- End
      else
        * --- Esecuzione ok
        this.oParentObject.w_PESEOK = .T.
        this.w_TMPC = ah_Msgformat("Aggiornamento  PWE_MAST/PWE_DETT eseguito correttamente%0")
        this.oParentObject.w_PMSG = this.w_TMPC
      endif
    endif
  endproc
  proc Try_038B4548()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PWE_MAST
    i_nConn=i_TableProp[this.PWE_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PWE_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"gscvcb84",this.PWE_MAST_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    this.w_NUMRIG = i_ROWS
    if this.w_NUMRIG>0
      * --- Insert into PWE_DETT
      i_nConn=i_TableProp[this.PWE_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PWE_DETT_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"gscvcb84_1",this.PWE_DETT_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      this.w_NUMRID = i_ROWS
    endif
    return
  proc Try_04C025A8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Aggiorno progressivo
    this.w_PWSERIAL = Right("0000000000"+Alltrim(str(this.w_NUMRIG)),10)
    i_Conn=i_TableProp[this.PWE_MAST_idx, 3]
    cp_NextTableProg(this, i_Conn, "SEPWB", "i_CODAZI,w_PWSERIAL", .T. )
    * --- Elimino record
    * --- Delete from PWDEVENT
    i_nConn=i_TableProp[this.PWDEVENT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PWDEVENT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"1 = "+cp_ToStrODBC(1);
             )
    else
      delete from (i_cTable) where;
            1 = 1;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from PWMEVENT
    i_nConn=i_TableProp[this.PWMEVENT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PWMEVENT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"1 = "+cp_ToStrODBC(1);
             )
    else
      delete from (i_cTable) where;
            1 = 1;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.w_TMPC = ah_Msgformat("Aggiornamento  PWE_MAST/PWE_DETT eseguito correttamente%0")
    this.oParentObject.w_PMSG = this.w_TMPC
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='PWMEVENT'
    this.cWorkTables[2]='PWDEVENT'
    this.cWorkTables[3]='PWE_MAST'
    this.cWorkTables[4]='PWE_DETT'
    return(this.OpenAllTables(4))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
