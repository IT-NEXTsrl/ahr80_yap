* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvbb31                                                        *
*              Aggiorna date movimenti cespiti                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_124]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-03-17                                                      *
* Last revis.: 2003-03-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvbb31",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvbb31 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TMPC = space(10)
  w_TEMP = space(10)
  w_MESS = space(10)
  * --- WorkFile variables
  MOV_CESP_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna i campi date sui movimenti cespiti in base ai check impostati sulla causale
    *     In precedenza le date di primo utilizzo fiscale e civile e la data di dismissione erano sempre 
    *     valorizzate sul movimento anche se non visibili.
    *     La ricostruzione saldi successivamente riportava tali date sul cespite.
    * --- FIle di LOG
    * --- Try
    local bErr_03894E68
    bErr_03894E68=bTrsErr
    this.Try_03894E68()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE GENERICO - impossibile aggiornare movimenti cespiti")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_03894E68
    * --- End
  endproc
  proc Try_03894E68()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.w_MESS = Ah_MsgFormat("Controllare le corrette impostazioni delle causali cespiti prima di proseguire con l'elaborazione%0Si desidera eseguire l'aggiornamento?")
    if Not Ah_YesNo("Questa procedura elimina le date primo utilizzo fiscale o civile e la data dismissione sui movimenti cespiti in base ai flag impostati sulla causale relativa%0%1","",this.w_MESS)
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("Procedura bloccata dall'utente")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        this.oParentObject.w_PESEOK = .F.
      endif
      i_retcode = 'stop'
      return
    endif
    * --- Write into MOV_CESP
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.MOV_CESP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOV_CESP_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="MCSERIAL"
      do vq_exec with 'gscvbb31',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.MOV_CESP_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="MOV_CESP.MCSERIAL = _t2.MCSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MCDTPRIU = _t2.DTPRIU";
          +",MCDATDIS = _t2.DATDIS";
          +",MCDTPRIC = _t2.DTPRIC";
          +i_ccchkf;
          +" from "+i_cTable+" MOV_CESP, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="MOV_CESP.MCSERIAL = _t2.MCSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MOV_CESP, "+i_cQueryTable+" _t2 set ";
          +"MOV_CESP.MCDTPRIU = _t2.DTPRIU";
          +",MOV_CESP.MCDATDIS = _t2.DATDIS";
          +",MOV_CESP.MCDTPRIC = _t2.DTPRIC";
          +Iif(Empty(i_ccchkf),"",",MOV_CESP.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="MOV_CESP.MCSERIAL = t2.MCSERIAL";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MOV_CESP set (";
          +"MCDTPRIU,";
          +"MCDATDIS,";
          +"MCDTPRIC";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.DTPRIU,";
          +"t2.DATDIS,";
          +"t2.DTPRIC";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="MOV_CESP.MCSERIAL = _t2.MCSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MOV_CESP set ";
          +"MCDTPRIU = _t2.DTPRIU";
          +",MCDATDIS = _t2.DATDIS";
          +",MCDTPRIC = _t2.DTPRIC";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".MCSERIAL = "+i_cQueryTable+".MCSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MCDTPRIU = (select DTPRIU from "+i_cQueryTable+" where "+i_cWhere+")";
          +",MCDATDIS = (select DATDIS from "+i_cQueryTable+" where "+i_cWhere+")";
          +",MCDTPRIC = (select DTPRIC from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Esecuzione ok
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Aggiornamento date su %1 movimenti", Alltrim(str(i_rows)) )
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = ah_Msgformat("Conversione eseguita correttamente")
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='MOV_CESP'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
