* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcb31                                                        *
*              Modifica unit� di misura tabella ELE_CONT                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_239]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-07-12                                                      *
* Last revis.: 2009-02-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcb31",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcb31 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_MRCODCOM = space(15)
  w_FRASE = space(200)
  w_ROWS = 0
  * --- WorkFile variables
  ELE_CONT_idx=0
  RIPATMP1_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- FIle di LOG
    * --- Cancello i campi dalla tabella ELE_CONT
    * --- Try
    local bErr_04A9E400
    bErr_04A9E400=bTrsErr
    this.Try_04A9E400()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = this.oParentObject.w_PMSG
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_04A9E400
    * --- End
  endproc
  proc Try_04A9E400()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
     
 i_nConn=i_TableProp[this.ELE_CONT_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.ELE_CONT_idx,2])
    if i_nConn<>0
      if upper(CP_DBTYPE)<>"DB2"
        * --- Create temporary table RIPATMP1
        i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('gscvcb31',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.RIPATMP1_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        this.w_ROWS = SQLExec(i_nConn,"ALTER TABLE "+i_cTable+" DROP COLUMN ELUNIMIS " )
        if this.w_ROWS<0
          * --- Raise
          i_Error=Message()
          return
        else
          * --- Esecuzione ok
          GSCV_BRT(this, "ELE_CONT" , i_nConn , .F.)
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.oParentObject.w_PESEOK = .T.
          this.w_TMPC = ah_msgformat("Eliminazione campo %1 eseguita con successo%0",arfield[w_loop])
          this.oParentObject.w_PMSG = this.oParentObject.w_PMSG + this.w_TMPC
        endif
      else
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    if upper(CP_DBTYPE)<>"DB2"
      * --- Write into ELE_CONT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.ELE_CONT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ELE_CONT_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="ELCONTRA,ELCODMOD,ELCODIMP,ELCODCOM"
        do vq_exec with 'GSCVCB31_1',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ELE_CONT_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="ELE_CONT.ELCONTRA = _t2.ELCONTRA";
                +" and "+"ELE_CONT.ELCODMOD = _t2.ELCODMOD";
                +" and "+"ELE_CONT.ELCODIMP = _t2.ELCODIMP";
                +" and "+"ELE_CONT.ELCODCOM = _t2.ELCODCOM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"ELUNIMIS = _t2.ELUNIMIS";
            +i_ccchkf;
            +" from "+i_cTable+" ELE_CONT, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="ELE_CONT.ELCONTRA = _t2.ELCONTRA";
                +" and "+"ELE_CONT.ELCODMOD = _t2.ELCODMOD";
                +" and "+"ELE_CONT.ELCODIMP = _t2.ELCODIMP";
                +" and "+"ELE_CONT.ELCODCOM = _t2.ELCODCOM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ELE_CONT, "+i_cQueryTable+" _t2 set ";
            +"ELE_CONT.ELUNIMIS = _t2.ELUNIMIS";
            +Iif(Empty(i_ccchkf),"",",ELE_CONT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="ELE_CONT.ELCONTRA = t2.ELCONTRA";
                +" and "+"ELE_CONT.ELCODMOD = t2.ELCODMOD";
                +" and "+"ELE_CONT.ELCODIMP = t2.ELCODIMP";
                +" and "+"ELE_CONT.ELCODCOM = t2.ELCODCOM";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ELE_CONT set (";
            +"ELUNIMIS";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.ELUNIMIS";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="ELE_CONT.ELCONTRA = _t2.ELCONTRA";
                +" and "+"ELE_CONT.ELCODMOD = _t2.ELCODMOD";
                +" and "+"ELE_CONT.ELCODIMP = _t2.ELCODIMP";
                +" and "+"ELE_CONT.ELCODCOM = _t2.ELCODCOM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ELE_CONT set ";
            +"ELUNIMIS = _t2.ELUNIMIS";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".ELCONTRA = "+i_cQueryTable+".ELCONTRA";
                +" and "+i_cTable+".ELCODMOD = "+i_cQueryTable+".ELCODMOD";
                +" and "+i_cTable+".ELCODIMP = "+i_cQueryTable+".ELCODIMP";
                +" and "+i_cTable+".ELCODCOM = "+i_cQueryTable+".ELCODCOM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"ELUNIMIS = (select ELUNIMIS from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Drop temporary table RIPATMP1
      i_nIdx=cp_GetTableDefIdx('RIPATMP1')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('RIPATMP1')
      endif
    endif
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    if this.w_NHF>=0
      this.w_TMPC = this.oParentObject.w_PMSG
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella DB2
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.ELE_CONT_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.ELE_CONT_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
          )
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    * --- Elimino la tabella
    if Not GSCV_BDT( this, "ELE_CONT" , i_nConn )
      * --- Raise
      i_Error="Error"
      return
    endif
    * --- Ricostruisco il database per la tabella
    GSCV_BRT(this, "ELE_CONT" , i_nConn , .T.)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into ELE_CONT
    i_nConn=i_TableProp[this.ELE_CONT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ELE_CONT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.ELE_CONT_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_Msg("Ripristinati valori di origine",.T.)
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    ah_Msg("Eliminata tabella temporanea",.T.)
  endproc


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='ELE_CONT'
    this.cWorkTables[2]='*RIPATMP1'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
