* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscv_b70                                                        *
*              Conversione importi F24                                         *
*                                                                              *
*      Author: Zucchetti TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_235]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-01-07                                                      *
* Last revis.: 2002-01-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscv_b70",oParentObject,m.w_NHF)
return(i_retval)

define class tgscv_b70 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  * --- WorkFile variables
  TMPIMPF24_idx=0
  MOD_PAG_idx=0
  MODEPAG_idx=0
  TMPERF24_idx=0
  TMPCOF24_idx=0
  TMPEVF24_idx=0
  MODCPAG_idx=0
  MODVPAG_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- FIle di LOG
    if upper(CP_DBTYPE)<>"DB2"
      * --- Esecuzione ok
      this.w_TMPC = ah_Msgformat("Su Database differenti da IBM DB2 il tracciato record non deve essere modificato")
      this.oParentObject.w_PESEOK = .T.
      this.oParentObject.w_PMSG = this.w_TMPC
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    else
      * --- La Procedura di conversione deve essere eseguita solo per DB2
      * --- Try
      local bErr_037A4E80
      bErr_037A4E80=bTrsErr
      this.Try_037A4E80()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        * --- Gestisce log errori
        this.oParentObject.w_PMSG = Message()
        this.oParentObject.w_PESEOK = .F.
        if this.w_NHF>=0
          this.w_TMPC = ah_Msgformat("ERRORE GENERICO - impossibile eseguire aggiornamento richiesto")
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
      bTrsErr=bTrsErr or bErr_037A4E80
      * --- End
    endif
  endproc
  proc Try_037A4E80()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Try
    local bErr_0376E920
    bErr_0376E920=bTrsErr
    this.Try_0376E920()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Raise
      i_Error="Impossibile aggiornare gli importi del modello F24"
      return
    endif
    bTrsErr=bTrsErr or bErr_0376E920
    * --- End
    * --- begin transaction
    cp_BeginTrs()
     
 i_nConn=i_TableProp[this.MOD_PAG_idx,3]
    * --- Elimino la tabella
    if Not GSCV_BDT( this, "MODEPAG" , i_nConn )
      * --- Raise
      i_Error="Error"
      return
    endif
    * --- Ricostruisco il database per la tabella
    GSCV_BRT(this, "MODEPAG", i_nConn )
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Elimino la tabella
    if Not GSCV_BDT( this, "MODCPAG" , i_nConn )
      * --- Raise
      i_Error="Error"
      return
    endif
    * --- Ricostruisco il database per la tabella
    GSCV_BRT(this, "MODCPAG", i_nConn )
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Elimino la tabella
    if Not GSCV_BDT( this, "MODVPAG" , i_nConn )
      * --- Raise
      i_Error="Error"
      return
    endif
    * --- Ricostruisco il database per la tabella
    GSCV_BRT(this, "MODVPAG", i_nConn )
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Elimino la tabella
    if Not GSCV_BDT( this, "MOD_PAG" , i_nConn )
      * --- Raise
      i_Error="Error"
      return
    endif
    * --- Ricostruisco il database per la tabella
    GSCV_BRT(this, "MOD_PAG", i_nConn )
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Ripristino gli Importi
    * --- Try
    local bErr_035FE830
    bErr_035FE830=bTrsErr
    this.Try_035FE830()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_035FE830
    * --- End
    * --- Drop temporary table TMPIMPF24
    i_nIdx=cp_GetTableDefIdx('TMPIMPF24')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPIMPF24')
    endif
    * --- Drop temporary table TMPERF24
    i_nIdx=cp_GetTableDefIdx('TMPERF24')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPERF24')
    endif
    * --- Drop temporary table TMPCOF24
    i_nIdx=cp_GetTableDefIdx('TMPCOF24')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPCOF24')
    endif
    * --- Drop temporary table TMPEVF24
    i_nIdx=cp_GetTableDefIdx('TMPEVF24')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPEVF24')
    endif
    if bTrsErr
      * --- Raise
      i_Error=MESSAGE()
      return
    else
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("- Aggiornamento importi F24 tabelle MOD_PAG, MODEPAG eseguito con successo")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = this.w_TMPC
    return
  proc Try_0376E920()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Pulisco le tebella 
    * --- Create temporary table TMPIMPF24
    i_nIdx=cp_AddTableDef('TMPIMPF24') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('..\pcon\exe\query\gscv_b70',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPIMPF24_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Create temporary table TMPERF24
    i_nIdx=cp_AddTableDef('TMPERF24') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('..\pcon\exe\query\gscveb70',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPERF24_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Create temporary table TMPCOF24
    i_nIdx=cp_AddTableDef('TMPCOF24') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('..\pcon\exe\query\gscvcb70',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPCOF24_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Create temporary table TMPEVF24
    i_nIdx=cp_AddTableDef('TMPEVF24') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('..\pcon\exe\query\gscvvb70',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPEVF24_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Delete from MODEPAG
    i_nConn=i_TableProp[this.MODEPAG_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MODEPAG_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"1 = "+cp_ToStrODBC(1);
             )
    else
      delete from (i_cTable) where;
            1 = 1;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from MODCPAG
    i_nConn=i_TableProp[this.MODCPAG_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MODCPAG_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"1 = "+cp_ToStrODBC(1);
             )
    else
      delete from (i_cTable) where;
            1 = 1;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from MODVPAG
    i_nConn=i_TableProp[this.MODVPAG_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MODVPAG_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"1 = "+cp_ToStrODBC(1);
             )
    else
      delete from (i_cTable) where;
            1 = 1;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from MOD_PAG
    i_nConn=i_TableProp[this.MOD_PAG_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOD_PAG_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"1 = "+cp_ToStrODBC(1);
             )
    else
      delete from (i_cTable) where;
            1 = 1;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_035FE830()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into MOD_PAG
    i_nConn=i_TableProp[this.MOD_PAG_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOD_PAG_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.TMPIMPF24_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.MOD_PAG_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Insert into MODEPAG
    i_nConn=i_TableProp[this.MODEPAG_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MODEPAG_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.TMPERF24_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.MODEPAG_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Insert into MODCPAG
    i_nConn=i_TableProp[this.MODCPAG_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MODCPAG_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.TMPCOF24_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.MODCPAG_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Insert into MODVPAG
    i_nConn=i_TableProp[this.MODVPAG_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MODVPAG_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.TMPEVF24_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.MODVPAG_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,8)]
    this.cWorkTables[1]='*TMPIMPF24'
    this.cWorkTables[2]='MOD_PAG'
    this.cWorkTables[3]='MODEPAG'
    this.cWorkTables[4]='*TMPERF24'
    this.cWorkTables[5]='*TMPCOF24'
    this.cWorkTables[6]='*TMPEVF24'
    this.cWorkTables[7]='MODCPAG'
    this.cWorkTables[8]='MODVPAG'
    return(this.OpenAllTables(8))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
