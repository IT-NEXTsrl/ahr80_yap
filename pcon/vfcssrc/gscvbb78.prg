* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvbb78                                                        *
*              CONTROLLO ALLEGATI CLIENTE FORNITORE - 2                        *
*                                                                              *
*      Author: Zucchetti - AT                                                  *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-06-26                                                      *
* Last revis.: 2010-12-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvbb78",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvbb78 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_CODREL = space(10)
  w_APPLEFT = space(10)
  w_NUM = 0
  w_ORIMENU = space(10)
  w_NEWMENU = space(10)
  w_RESTOLEFT = space(10)
  w_TOTVOCISTR = space(5)
  w_INDEX = 0
  w_INDEXSUB = 0
  w_PATH_TEMP = space(254)
  pPATHVMN = space(100)
  pKEYPRG = space(10)
  pNEWPRG = space(10)
  pSUBMEN = space(30)
  pAGGANCIO = space(40)
  w_PATH = space(100)
  w_ELABPATH = space(100)
  w_TOTVMN = 0
  w_CONTAVMN = 0
  w_NOMEVMN = space(15)
  w_ERRORE = .f.
  w_MESS = space(10)
  w_STAR = space(1)
  * --- WorkFile variables
  AHEECONV_idx=0
  CONVERSI_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- CONTROLLO ALLEGATI CLIENTE FORNITORE
    * --- FIle di LOG
    * --- pXXX Per l'aggiornamento dei custom.vmn
    * --- Read from CONVERSI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CONVERSI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONVERSI_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "*"+;
        " from "+i_cTable+" CONVERSI where ";
            +"COESEGUI = "+cp_ToStrODBC("S");
            +" and COCODREL = "+cp_ToStrODBC("5.0-M4141");
            +" and CONOMPRO = "+cp_ToStrODBC("GSCVBB76");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        *;
        from (i_cTable) where;
            COESEGUI = "S";
            and COCODREL = "5.0-M4141";
            and CONOMPRO = "GSCVBB76";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Se procedura di conversione Iniezione non eseguita allora nessuna operazione
    if i_Rows=0
      if this.w_NHF>=0
        this.w_TMPC = AH_MsgFormat("Nessuna operazione eseguita. Procedura terminata correttamente.")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
      this.oParentObject.w_PESEOK = .T.
      this.oParentObject.w_PMSG = this.w_TMPC
      i_retcode = 'stop'
      return
    endif
    * --- Copio i custom.vmn eliminati o modificati in TEMP
    this.w_PATH_TEMP = ADDBS(tempadhoc())
    * --- Se non modificati dall'utente, da tutti i CUSTOM*.VMN viene eliminato il submenu
    *     e la relativa voce di menu Controllo allegati
    this.pKEYPRG = "GSCG_SKA"
    this.pSUBMEN = "Elen&chi Clienti/Fornitori"
    this.pAGGANCIO = "CONTABILIT�"+g_MENUSEP+"CONTABILIT� IVA"+g_MENUSEP+"STAMPA REGISTRI IVA###D"
    this.w_PATH = SYS(5)+SYS(2003)
    * --- Ricerca menu CUSTOM*.VMN prima in EXE e quindi  in EXE\CUSTOM
    this.w_ELABPATH = this.w_PATH+"\"
    this.w_TOTVMN = ADIR(ELEMENU, this.w_ELABPATH+"CUSTOM*.VMN")
    this.w_CONTAVMN = 1
    do while this.w_CONTAVMN <= this.w_TOTVMN
      this.w_NOMEVMN = ELEMENU(this.w_CONTAVMN,1)
      this.pPATHVMN = this.w_ELABPATH+this.w_NOMEVMN
      this.w_ERRORE = .F.
      ah_msg( "Eliminazione voce men� da %1 in corso",.t.,.f.,.f.,ALLTRIM(this.w_ELABPATH+this.w_NOMEVMN))
      * --- Elabora nenu (Eliminazione voce di menu)
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_CONTAVMN = this.w_CONTAVMN+1
    enddo
    this.w_ELABPATH = this.w_PATH+"\CUSTOM\"
    this.w_TOTVMN = ADIR(ELEMENU, this.w_ELABPATH+"CUSTOM*.VMN")
    this.w_CONTAVMN = 1
    do while this.w_CONTAVMN <= this.w_TOTVMN
      this.w_NOMEVMN = ELEMENU(this.w_CONTAVMN,1)
      this.w_ERRORE = .F.
      this.pPATHVMN = this.w_ELABPATH+this.w_NOMEVMN
      ah_msg( "Eliminazione voce men� da %1 in corso",.t.,.f.,.f.,ALLTRIM(this.w_ELABPATH+this.w_NOMEVMN))
      * --- Elabora nenu (Eliminazione voce di menu)
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_CONTAVMN = this.w_CONTAVMN+1
    enddo
    * --- Aggiorno il flag su DB
    * --- Try
    local bErr_0361D5E0
    bErr_0361D5E0=bTrsErr
    this.Try_0361D5E0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = AH_MsgFormat("ERRORE GENERICO - Impossibile eseguire aggiornamento richiesto")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_0361D5E0
    * --- End
  endproc
  proc Try_0361D5E0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Try
    local bErr_037687A0
    bErr_037687A0=bTrsErr
    this.Try_037687A0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Raise
      i_Error=message()
      return
    endif
    bTrsErr=bTrsErr or bErr_037687A0
    * --- End
    * --- Aggiorna LOG OPERAZIONI
    if this.w_NHF>=0
      this.w_TMPC = AH_MsgFormat("Eseguito aggiornamento flag ripetibile da attivo a disattivo relativo alla conversione GSCVBB76 (codice release 5.0-m4141) ")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = this.w_TMPC
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_037687A0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Imposta non ripetibile la conversione della FP precedente relativa al controllo allegati
    * --- Write into AHEECONV
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.AHEECONV_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AHEECONV_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.AHEECONV_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"CORIPETI ="+cp_NullLink(cp_ToStrODBC("N"),'AHEECONV','CORIPETI');
          +i_ccchkf ;
      +" where ";
          +"COCODREL = "+cp_ToStrODBC("5.0-M4141");
          +" and CONOMPRO = "+cp_ToStrODBC("GSCVBB76");
             )
    else
      update (i_cTable) set;
          CORIPETI = "N";
          &i_ccchkf. ;
       where;
          COCODREL = "5.0-M4141";
          and CONOMPRO = "GSCVBB76";

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Write into CONVERSI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.CONVERSI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONVERSI_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.CONVERSI_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"CORIPETI ="+cp_NullLink(cp_ToStrODBC("N"),'CONVERSI','CORIPETI');
          +i_ccchkf ;
      +" where ";
          +"COCODREL = "+cp_ToStrODBC("5.0-M4141");
          +" and CONOMPRO = "+cp_ToStrODBC("GSCVBB76");
             )
    else
      update (i_cTable) set;
          CORIPETI = "N";
          &i_ccchkf. ;
       where;
          COCODREL = "5.0-M4141";
          and CONOMPRO = "GSCVBB76";

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Eliminazione submenu e relativa voce di menu
    *     Se il menu non � stato modificato, viene cancellato il file
    * --- Carico menu in una stringa per poterlo scomporre
    this.w_ORIMENU = FILETOSTR(this.pPATHVMN)
    if AT(this.pKEYPRG,this.w_ORIMENU) =0
      * --- Programma non presente
      this.w_MESS = "Verificare men� %1, programma  %2 non trovato, impossibile eliminare la voce inserita"
      if this.w_NHF>=0
        this.w_TMPC = ah_msgformat(this.w_MESS,this.pPATHVMN,this.pKEYPRG)
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
      this.w_ERRORE = .T.
    endif
    if !this.w_ERRORE And AT(this.pSUBMEN,this.w_ORIMENU) =0
      * --- Menu non presente
      this.w_MESS = "Verificare men� %1, submenu  %2 non trovato, impossibile eliminare la voce inserita"
      if this.w_NHF>=0
        this.w_TMPC = ah_msgformat(this.w_MESS,this.pPATHVMN,this.pSUBMEN)
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
      this.w_ERRORE = .T.
    endif
    if !this.w_ERRORE And AT(this.pAGGANCIO,this.w_ORIMENU) =0
      * --- Aggancio non presente
      this.w_MESS = "Verificare men� %1, path di aggancio  %2 non trovato, impossibile eliminare la voce inserita"
      if this.w_NHF>=0
        this.w_TMPC = ah_msgformat(this.w_MESS,this.pPATHVMN,this.pAGGANCIO)
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
      this.w_ERRORE = .T.
    endif
    * --- memorizzo il vecchio file nella Temp
    this.w_NUM = STRTOFILE(this.w_ORIMENU,this.w_PATH_TEMP+alltrim(this.w_NOMEVMN)+".bak")
    if !this.w_ERRORE
      this.w_ORIMENU = SUBSTR(this.w_ORIMENU, 1, AT(CHR(13)+CHR(10), this.w_ORIMENU, 24)+2) + STRTRAN(ALLTRIM(TTOC(datetime(),3)),"T"," ") +'"'+ SUBSTR(this.w_ORIMENU, AT(CHR(13)+CHR(10),this.w_ORIMENU,25))
      this.w_INDEX = AT(this.pKEYPRG,this.w_ORIMENU)
      this.w_INDEXSUB = AT(this.pSUBMEN,this.w_ORIMENU)
      * --- Verifico dopo 15 chr(13) + chr(10) ho un asterisco
      this.w_APPLEFT = SUBSTR(this.w_ORIMENU, this.w_INDEX)
      this.w_STAR = SUBSTR(this.w_APPLEFT, AT(CHR(13)+CHR(10), this.w_APPLEFT, 15)+2,(AT(CHR(13)+CHR(10), this.w_ORIMENU, 16)-AT(CHR(13)+CHR(10), this.w_ORIMENU, 15)-2) )
      if alltrim(this.w_STAR)="*"
        * --- L'ultima voce � proprio controllo allegati.Controllo se sono presenti solo le mie voci
        this.w_TOTVOCISTR = SUBSTR(this.w_ORIMENU, AT(CHR(13)+CHR(10), this.w_ORIMENU, 6)+2,(AT(CHR(13)+CHR(10), this.w_ORIMENU, 7)-AT(CHR(13)+CHR(10), this.w_ORIMENU, 6)-2) )
        if Not Empty(this.pSUBMEN)
          this.w_TOTVOCISTR = " "+alltrim(str(VAL(alltrim(this.w_TOTVOCISTR))-2))
        else
          this.w_TOTVOCISTR = " "+alltrim(str(VAL(alltrim(this.w_TOTVOCISTR))-1))
        endif
        if Val(this.w_TOTVOCISTR) = 1
          * --- Posso eliminare il .VMN
          this.w_MESS = "Eliminato men� %1."
          if this.w_NHF>=0
            this.w_TMPC = ah_msgformat(this.w_MESS,this.pPATHVMN)
            this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
          endif
          DELETE FILE (this.pPATHVMN)
        else
          * --- Occorre ricomporre il menu dalla posizione dopo il progressivo sino al menu da eliminare
          this.w_APPLEFT = SUBSTR(this.w_ORIMENU, 1,this.w_INDEXSUB-2)
          this.w_RESTOLEFT = SUBSTR(this.w_APPLEFT, AT(CHR(13)+CHR(10), this.w_APPLEFT, 7))
          * --- Ricomposizione del menu
          this.w_NEWMENU = SUBSTR(this.w_ORIMENU, 1, AT(CHR(13)+CHR(10), this.w_ORIMENU, 6)+1) + this.w_TOTVOCISTR + this.w_RESTOLEFT + "*" +CHR(13)+CHR(10)
          this.w_NUM = STRTOFILE(this.w_NEWMENU,this.pPATHVMN)
        endif
      else
        * --- E' stata aggiunta una voce di men� dopo il controllo allegati, non � possibile
        *     disinstallare la voce iniettata
        this.w_MESS = "Verificare men�  %1, sono state aggiunte nuove voci o submenu, impossibile eliminare la voce inserita"
        if this.w_NHF>=0
          this.w_TMPC = ah_msgformat(this.w_MESS,this.pPATHVMN)
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
        this.w_ERRORE = .T.
      endif
    endif
  endproc


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='AHEECONV'
    this.cWorkTables[2]='CONVERSI'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
