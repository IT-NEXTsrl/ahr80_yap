* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvab29                                                        *
*              Sbianca codice costo se attivita vuota                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_76]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-02-27                                                      *
* Last revis.: 2006-12-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvab29",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvab29 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_MESS = space(100)
  w_TMPN = 0
  * --- WorkFile variables
  DOC_DETT_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questa procedura Aggiorna:
    *     il campo MVCODCOS su DOC_DETT se il campo MVCODATT � vuoto
    * --- FIle di LOG
    * --- Try
    local bErr_035ED740
    bErr_035ED740=bTrsErr
    this.Try_035ED740()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE GENERICO - impossibile aggiornare documenti")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_035ED740
    * --- End
  endproc
  proc Try_035ED740()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Nono scrivo una WRITE FROM QUERY utilizzo piuttosto due WRITE una per valore Null
    *     ed una per valore Space(15) 
    * --- Write into DOC_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVCODCOS ="+cp_NullLink(cp_ToStrODBC(""),'DOC_DETT','MVCODCOS');
          +i_ccchkf ;
      +" where ";
          +"MVCODATT is null ";
          +" and MVCODCOS is not null ";
             )
    else
      update (i_cTable) set;
          MVCODCOS = "";
          &i_ccchkf. ;
       where;
          MVCODATT is null;
          and MVCODCOS is not null;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    this.w_TMPN = i_ROWS
    * --- Write into DOC_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVCODCOS ="+cp_NullLink(cp_ToStrODBC(""),'DOC_DETT','MVCODCOS');
          +i_ccchkf ;
      +" where ";
          +"MVCODATT = "+cp_ToStrODBC(Space(15));
          +" and MVCODCOS is not null ";
             )
    else
      update (i_cTable) set;
          MVCODCOS = "";
          &i_ccchkf. ;
       where;
          MVCODATT = Space(15);
          and MVCODCOS is not null;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = ah_Msgformat("Aggiornamento MVCODCOS eseguito correttamente su %1 righe", +Alltrim(Str( i_Rows + this.w_TmpN )) )
    if this.w_NHF>=0
      this.w_TMPC = this.oParentObject.w_PMSG
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='DOC_DETT'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
