* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcc46                                                        *
*              Aggiornamento chiave tabella risorse RIS_ORSE                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-10-21                                                      *
* Last revis.: 2015-01-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcc46",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcc46 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_COMMDEFA = space(15)
  w_DATINI = ctod("  /  /  ")
  w_DATFIN = ctod("  /  /  ")
  w_FraseSQL = space(200)
  w_iRows = 0
  * --- WorkFile variables
  RIS_ORSE_idx=0
  TIP_RISO_idx=0
  TIP_DIBA_idx=0
  DISMBASE_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiornamento chiave tabella risorse RIS_ORSE, inizializzazione TIP_RISO e TIP_DIBA
    * --- FIle di LOG
    * --- Try
    local bErr_0388AEB8
    bErr_0388AEB8=bTrsErr
    this.Try_0388AEB8()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("Errore generico - impossibile eseguire aggiornamento richiesto")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_0388AEB8
    * --- End
  endproc
  proc Try_0388AEB8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    GSCV_BDC(this,"xxx", "RIS_ORSE")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    GSCV_BDC(this,i_CODAZI, "RIS_ORSE")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into TIP_RISO
    i_nConn=i_TableProp[this.TIP_RISO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TIP_RISO_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TIP_RISO_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"TRCODICE"+",TRDESCRI"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC("RE"),'TIP_RISO','TRCODICE');
      +","+cp_NullLink(cp_ToStrODBC("Reparti"),'TIP_RISO','TRDESCRI');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'TRCODICE',"RE",'TRDESCRI',"Reparti")
      insert into (i_cTable) (TRCODICE,TRDESCRI &i_ccchkf. );
         values (;
           "RE";
           ,"Reparti";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Insert into TIP_RISO
    i_nConn=i_TableProp[this.TIP_RISO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TIP_RISO_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TIP_RISO_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"TRCODICE"+",TRDESCRI"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC("AR"),'TIP_RISO','TRCODICE');
      +","+cp_NullLink(cp_ToStrODBC("Aree"),'TIP_RISO','TRDESCRI');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'TRCODICE',"AR",'TRDESCRI',"Aree")
      insert into (i_cTable) (TRCODICE,TRDESCRI &i_ccchkf. );
         values (;
           "AR";
           ,"Aree";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Insert into TIP_RISO
    i_nConn=i_TableProp[this.TIP_RISO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TIP_RISO_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TIP_RISO_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"TRCODICE"+",TRDESCRI"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC("AT"),'TIP_RISO','TRCODICE');
      +","+cp_NullLink(cp_ToStrODBC("Attrezzature"),'TIP_RISO','TRDESCRI');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'TRCODICE',"AT",'TRDESCRI',"Attrezzature")
      insert into (i_cTable) (TRCODICE,TRDESCRI &i_ccchkf. );
         values (;
           "AT";
           ,"Attrezzature";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Insert into TIP_RISO
    i_nConn=i_TableProp[this.TIP_RISO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TIP_RISO_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TIP_RISO_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"TRCODICE"+",TRDESCRI"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC("CL"),'TIP_RISO','TRCODICE');
      +","+cp_NullLink(cp_ToStrODBC("Centri di lavoro"),'TIP_RISO','TRDESCRI');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'TRCODICE',"CL",'TRDESCRI',"Centri di lavoro")
      insert into (i_cTable) (TRCODICE,TRDESCRI &i_ccchkf. );
         values (;
           "CL";
           ,"Centri di lavoro";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Insert into TIP_RISO
    i_nConn=i_TableProp[this.TIP_RISO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TIP_RISO_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TIP_RISO_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"TRCODICE"+",TRDESCRI"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC("MA"),'TIP_RISO','TRCODICE');
      +","+cp_NullLink(cp_ToStrODBC("Macchine"),'TIP_RISO','TRDESCRI');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'TRCODICE',"MA",'TRDESCRI',"Macchine")
      insert into (i_cTable) (TRCODICE,TRDESCRI &i_ccchkf. );
         values (;
           "MA";
           ,"Macchine";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Insert into TIP_RISO
    i_nConn=i_TableProp[this.TIP_RISO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TIP_RISO_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TIP_RISO_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"TRCODICE"+",TRDESCRI"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC("SQ"),'TIP_RISO','TRCODICE');
      +","+cp_NullLink(cp_ToStrODBC("Squadre"),'TIP_RISO','TRDESCRI');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'TRCODICE',"SQ",'TRDESCRI',"Squadre")
      insert into (i_cTable) (TRCODICE,TRDESCRI &i_ccchkf. );
         values (;
           "SQ";
           ,"Squadre";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Insert into TIP_RISO
    i_nConn=i_TableProp[this.TIP_RISO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TIP_RISO_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TIP_RISO_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"TRCODICE"+",TRDESCRI"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC("RG"),'TIP_RISO','TRCODICE');
      +","+cp_NullLink(cp_ToStrODBC("Risorsa generica"),'TIP_RISO','TRDESCRI');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'TRCODICE',"RG",'TRDESCRI',"Risorsa generica")
      insert into (i_cTable) (TRCODICE,TRDESCRI &i_ccchkf. );
         values (;
           "RG";
           ,"Risorsa generica";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Insert into TIP_RISO
    i_nConn=i_TableProp[this.TIP_RISO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TIP_RISO_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TIP_RISO_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"TRCODICE"+",TRDESCRI"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC("RD"),'TIP_RISO','TRCODICE');
      +","+cp_NullLink(cp_ToStrODBC("Risorsa descrittiva"),'TIP_RISO','TRDESCRI');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'TRCODICE',"RD",'TRDESCRI',"Risorsa descrittiva")
      insert into (i_cTable) (TRCODICE,TRDESCRI &i_ccchkf. );
         values (;
           "RD";
           ,"Risorsa descrittiva";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Insert into TIP_RISO
    i_nConn=i_TableProp[this.TIP_RISO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TIP_RISO_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TIP_RISO_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"TRCODICE"+",TRDESCRI"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC("UP"),'TIP_RISO','TRCODICE');
      +","+cp_NullLink(cp_ToStrODBC("Unit� produttiva"),'TIP_RISO','TRDESCRI');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'TRCODICE',"UP",'TRDESCRI',"Unit� produttiva")
      insert into (i_cTable) (TRCODICE,TRDESCRI &i_ccchkf. );
         values (;
           "UP";
           ,"Unit� produttiva";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_msg("Aggiornamento tipo risorsa in corso...." )
    * --- Aggiorno il tipo delle vecchie risorse, tutti reparti
    * --- Write into RIS_ORSE
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.RIS_ORSE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.RIS_ORSE_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.RIS_ORSE_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"RL__TIPO ="+cp_NullLink(cp_ToStrODBC("RE"),'RIS_ORSE','RL__TIPO');
          +i_ccchkf ;
          +" where 1=1")
    else
      update (i_cTable) set;
          RL__TIPO = "RE";
          &i_ccchkf. ;
          where 1=1

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    ah_msg("Aggiornamento in corso..." )
    i_nConn=i_TableProp[this.RIS_ORSE_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.RIS_ORSE_idx,2])
    if i_nConn<>0
      do case
        case UPPER(CP_DBTYPE)="SQLSERVER"
          this.w_FraseSQL = "ALTER TABLE xxxRIS_ORSE ALTER COLUMN RL__TIPO CHAR(2) NOT NULL"
          this.w_iRows = SQLExec(i_nConn, rtrim(this.w_FraseSQL))
          this.w_FraseSQL = "ALTER TABLE " + i_cTable + " ALTER COLUMN RL__TIPO CHAR(2) NOT NULL"
          this.w_iRows = SQLExec(i_nConn, rtrim(this.w_FraseSQL))
          this.w_FraseSQL = "ALTER TABLE xxxRIS_ORSE ALTER COLUMN RLUMTDEF CHAR(3) NULL"
          this.w_iRows = SQLExec(i_nConn, rtrim(this.w_FraseSQL))
          this.w_FraseSQL = "ALTER TABLE " + i_cTable + " ALTER COLUMN RLUMTDEF CHAR(3) NULL"
          this.w_iRows = SQLExec(i_nConn, rtrim(this.w_FraseSQL))
        case UPPER(CP_DBTYPE)="ORACLE"
          this.w_FraseSQL = "ALTER TABLE xxxRIS_ORSE MODIFY(RL__TIPO NOT NULL)"
          this.w_iRows = SQLExec(i_nConn, rtrim(this.w_FraseSQL))
          this.w_FraseSQL = "ALTER TABLE " + i_cTable + " MODIFY(RL__TIPO NOT NULL)"
          this.w_iRows = SQLExec(i_nConn, rtrim(this.w_FraseSQL))
          this.w_FraseSQL = "ALTER TABLE xxxRIS_ORSE MODIFY(RLUMTDEF CHAR(3))"
          this.w_iRows = SQLExec(i_nConn, rtrim(this.w_FraseSQL))
          this.w_FraseSQL = "ALTER TABLE " + i_cTable + " MODIFY(RLUMTDEF CHAR(3))"
          this.w_iRows = SQLExec(i_nConn, rtrim(this.w_FraseSQL))
      endcase
      if bTrsErr
        if this.w_NHF>=0
          this.w_TMPC = Message()
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
        * --- Raise
        i_Error="Impossibile assegnare la propriet� NOT NULL alla colonna RL__TIPO della tabella RIS_ORSE"
        return
      else
        if this.w_NHF>=0
          this.w_TMPC = AH_MsgFormat("Adeguamento chiave primaria tabella RIS_ORSE eseguito correttamente.")
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
    endif
    * --- Inizializzazione tabella TIP_DIBA
    * --- Insert into TIP_DIBA
    i_nConn=i_TableProp[this.TIP_DIBA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TIP_DIBA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TIP_DIBA_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"TDCODICE"+",TDDESCRI"+",TDDFAULT"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC("0001"),'TIP_DIBA','TDCODICE');
      +","+cp_NullLink(cp_ToStrODBC("Gestionale"),'TIP_DIBA','TDDESCRI');
      +","+cp_NullLink(cp_ToStrODBC("S"),'TIP_DIBA','TDDFAULT');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'TDCODICE',"0001",'TDDESCRI',"Gestionale",'TDDFAULT',"S")
      insert into (i_cTable) (TDCODICE,TDDESCRI,TDDFAULT &i_ccchkf. );
         values (;
           "0001";
           ,"Gestionale";
           ,"S";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Insert into TIP_DIBA
    i_nConn=i_TableProp[this.TIP_DIBA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TIP_DIBA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TIP_DIBA_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"TDCODICE"+",TDDESCRI"+",TDDFAULT"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC("0002"),'TIP_DIBA','TDCODICE');
      +","+cp_NullLink(cp_ToStrODBC("Tecnica"),'TIP_DIBA','TDDESCRI');
      +","+cp_NullLink(cp_ToStrODBC("N"),'TIP_DIBA','TDDFAULT');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'TDCODICE',"0002",'TDDESCRI',"Tecnica",'TDDFAULT',"N")
      insert into (i_cTable) (TDCODICE,TDDESCRI,TDDFAULT &i_ccchkf. );
         values (;
           "0002";
           ,"Tecnica";
           ,"N";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Insert into TIP_DIBA
    i_nConn=i_TableProp[this.TIP_DIBA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TIP_DIBA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TIP_DIBA_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"TDCODICE"+",TDDESCRI"+",TDDFAULT"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC("0003"),'TIP_DIBA','TDCODICE');
      +","+cp_NullLink(cp_ToStrODBC("Pianificazione"),'TIP_DIBA','TDDESCRI');
      +","+cp_NullLink(cp_ToStrODBC("N"),'TIP_DIBA','TDDFAULT');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'TDCODICE',"0003",'TDDESCRI',"Pianificazione",'TDDFAULT',"N")
      insert into (i_cTable) (TDCODICE,TDDESCRI,TDDFAULT &i_ccchkf. );
         values (;
           "0003";
           ,"Pianificazione";
           ,"N";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Insert into TIP_DIBA
    i_nConn=i_TableProp[this.TIP_DIBA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TIP_DIBA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TIP_DIBA_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"TDCODICE"+",TDDESCRI"+",TDDFAULT"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC("0004"),'TIP_DIBA','TDCODICE');
      +","+cp_NullLink(cp_ToStrODBC("Costificazione"),'TIP_DIBA','TDDESCRI');
      +","+cp_NullLink(cp_ToStrODBC("N"),'TIP_DIBA','TDDFAULT');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'TDCODICE',"0004",'TDDESCRI',"Costificazione",'TDDFAULT',"N")
      insert into (i_cTable) (TDCODICE,TDDESCRI,TDDFAULT &i_ccchkf. );
         values (;
           "0004";
           ,"Costificazione";
           ,"N";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Inizializzazione DBTIPDIS su distinta base
    * --- Write into DISMBASE
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DISMBASE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DISMBASE_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DISMBASE_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"DBTIPDIS ="+cp_NullLink(cp_ToStrODBC("0001"),'DISMBASE','DBTIPDIS');
      +",DBPRIORI ="+cp_NullLink(cp_ToStrODBC("0000"),'DISMBASE','DBPRIORI');
          +i_ccchkf ;
             )
    else
      update (i_cTable) set;
          DBTIPDIS = "0001";
          ,DBPRIORI = "0000";
          &i_ccchkf. ;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    i_nConn=i_TableProp[this.RIS_ORSE_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.RIS_ORSE_idx,2])
    GSCV_BRT(this,"RIS_ORSE" , i_nConn , .T. , "all", "N")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_TMPC = ah_Msgformat("Aggiornamento eseguito con successo")
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = this.w_TMPC
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='RIS_ORSE'
    this.cWorkTables[2]='TIP_RISO'
    this.cWorkTables[3]='TIP_DIBA'
    this.cWorkTables[4]='DISMBASE'
    return(this.OpenAllTables(4))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
