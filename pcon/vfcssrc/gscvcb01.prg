* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcb01                                                        *
*              UNIFICAZIONE TABELLA CALENDARI AZIENDALI                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-01-14                                                      *
* Last revis.: 2008-11-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcb01",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcb01 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TMPC = space(10)
  w_TEMP = space(10)
  w_TCCODICE = space(5)
  * --- WorkFile variables
  CAL_AZIE_idx=0
  TMP_DETT_idx=0
  TAB_CALE_idx=0
  PAR_PROD_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Copia i dati della tabella CAL_AZIE per eliminarla
    *     e ricostruirla con struttura corretta, procedura di convesrione associata 5.0-M1538
    * --- Try
    local bErr_0379BF10
    bErr_0379BF10=bTrsErr
    this.Try_0379BF10()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.w_TMPC = ah_Msgformat("La procedura ha creato il dbf %1 con i dati originari della tabella: contattare l'assistenza",nametable)
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = this.oParentObject.w_PMSG
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_0379BF10
    * --- End
  endproc
  proc Try_0379BF10()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.w_TCCODICE = SPACE(5)
    * --- Read from TAB_CALE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.TAB_CALE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TAB_CALE_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "TCCODICE"+;
        " from "+i_cTable+" TAB_CALE where ";
            +"TCFLDEFA = "+cp_ToStrODBC("S");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        TCCODICE;
        from (i_cTable) where;
            TCFLDEFA = "S";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_TCCODICE = NVL(cp_ToDate(_read_.TCCODICE),cp_NullValue(_read_.TCCODICE))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if EMPTY(NVL(this.w_TCCODICE, " "))
      * --- Inserisco il calendario aziendale predefinito Zucchetti
      * --- Insert into TAB_CALE
      i_nConn=i_TableProp[this.TAB_CALE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TAB_CALE_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TAB_CALE_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"TCCODICE"+",TCDESCRI"+",TCFLDEFA"+",TCDA1STD"+",TCAL1STD"+",TCDA2STD"+",TCAL2STD"+",TC__DAY1"+",TC__DAY2"+",TC__DAY3"+",TC__DAY4"+",TC__DAY5"+",TC__DAY6"+",TC__DAY7"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC("ZZCAP"),'TAB_CALE','TCCODICE');
        +","+cp_NullLink(cp_ToStrODBC(ah_msgformat("Calendario aziendale predefinito")),'TAB_CALE','TCDESCRI');
        +","+cp_NullLink(cp_ToStrODBC("S"),'TAB_CALE','TCFLDEFA');
        +","+cp_NullLink(cp_ToStrODBC("09:00"),'TAB_CALE','TCDA1STD');
        +","+cp_NullLink(cp_ToStrODBC("13:00"),'TAB_CALE','TCAL1STD');
        +","+cp_NullLink(cp_ToStrODBC(":"),'TAB_CALE','TCDA2STD');
        +","+cp_NullLink(cp_ToStrODBC(":"),'TAB_CALE','TCAL2STD');
        +","+cp_NullLink(cp_ToStrODBC("N"),'TAB_CALE','TC__DAY1');
        +","+cp_NullLink(cp_ToStrODBC("S"),'TAB_CALE','TC__DAY2');
        +","+cp_NullLink(cp_ToStrODBC("S"),'TAB_CALE','TC__DAY3');
        +","+cp_NullLink(cp_ToStrODBC("S"),'TAB_CALE','TC__DAY4');
        +","+cp_NullLink(cp_ToStrODBC("S"),'TAB_CALE','TC__DAY5');
        +","+cp_NullLink(cp_ToStrODBC("S"),'TAB_CALE','TC__DAY6');
        +","+cp_NullLink(cp_ToStrODBC("N"),'TAB_CALE','TC__DAY7');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'TCCODICE',"ZZCAP",'TCDESCRI',ah_msgformat("Calendario aziendale predefinito"),'TCFLDEFA',"S",'TCDA1STD',"09:00",'TCAL1STD',"13:00",'TCDA2STD',":",'TCAL2STD',":",'TC__DAY1',"N",'TC__DAY2',"S",'TC__DAY3',"S",'TC__DAY4',"S",'TC__DAY5',"S")
        insert into (i_cTable) (TCCODICE,TCDESCRI,TCFLDEFA,TCDA1STD,TCAL1STD,TCDA2STD,TCAL2STD,TC__DAY1,TC__DAY2,TC__DAY3,TC__DAY4,TC__DAY5,TC__DAY6,TC__DAY7 &i_ccchkf. );
           values (;
             "ZZCAP";
             ,ah_msgformat("Calendario aziendale predefinito");
             ,"S";
             ,"09:00";
             ,"13:00";
             ,":";
             ,":";
             ,"N";
             ,"S";
             ,"S";
             ,"S";
             ,"S";
             ,"S";
             ,"N";
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Write into PAR_PROD
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.PAR_PROD_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROD_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_PROD_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"PPCALSTA ="+cp_NullLink(cp_ToStrODBC("ZZCAP"),'PAR_PROD','PPCALSTA');
            +i_ccchkf ;
        +" where ";
            +"PPCODICE = "+cp_ToStrODBC("PP");
               )
      else
        update (i_cTable) set;
            PPCALSTA = "ZZCAP";
            &i_ccchkf. ;
         where;
            PPCODICE = "PP";

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    local w_nConn, w_cTable 
 w_nConn=i_TableProp[this.CAL_AZIE_idx,3] 
 w_cTable=cp_SetAzi(i_TableProp[this.CAL_AZIE_idx,2])
    pName="CAL_AZIE"
    pDatabaseType=i_ServerConn[1,6]
    * --- Ricostruisco  la tabella prima di copiarne il contenuto  (si evita il rischio che la tabella copiata abbia dei campi in meno della tabella ricostruita)
    * --- Create temporary table TMP_DETT
    i_nIdx=cp_AddTableDef('TMP_DETT') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('gscvcb01',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMP_DETT_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    vq_exec("gscvcb01.vqr",this,"CursErrore") 
 local nametable 
 nametable = tempadhoc()+"\CAL_AZIE.DBF" 
 select CursErrore 
 COPY TO (FORCEEXT(nametable,"dbf"))
    if USED("CursErrore")
      select CursErrore 
 use
    endif
    ah_msg("Creata tabelle temporanea di appoggio")
    if Not GSCV_BDT( this, "CAL_AZIE" , w_nConn)
      * --- Raise
      i_Error="Error"
      return
    endif
    * --- Ricostruisco il database per la tabella
    GSCV_BRT(this, "CAL_AZIE" , w_nConn , .T.)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into CAL_AZIE
    i_nConn=i_TableProp[this.CAL_AZIE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAL_AZIE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.TMP_DETT_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"CACODCAL, CAGIORNO, CADESGIO, CANUMORE, CAPROFIL, CAGIOLAV, CAORAIN1, CAORAFI1, CAORAIN2, CAORAFI2"," from "+i_cTempTable,this.CAL_AZIE_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Drop temporary table TMP_DETT
    i_nIdx=cp_GetTableDefIdx('TMP_DETT')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMP_DETT')
    endif
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.w_TMPC = ah_Msgformat("Tabella calendari aziendali creata correttamente")
    this.oParentObject.w_PMSG = ah_Msgformat("Conversione eseguita correttamente")
    if IsAlt()
      * --- Try
      local bErr_035FB290
      bErr_035FB290=bTrsErr
      this.Try_035FB290()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        accept error
      endif
      bTrsErr=bTrsErr or bErr_035FB290
      * --- End
    endif
    return
  proc Try_035FB290()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Drop tabelle
    w_nConn=i_ServerConn[i_TableProp[cp_GetTableDefIdx("cprecsec"),5],2]
    Local w_rowsusr, w_rowssec 
 w_rowsusr = sqlexec(w_nConn, "select * from cprecsecusr", "_curs_cprecsecusr") 
 w_rowssec = sqlexec(w_nConn, "select * from cprecsec", "_curs_cprecsec")
    if used("_curs_cprecsecusr")
      select _curs_cprecsecusr 
 use
    endif
    if used("_curs_cprecsec")
      select _curs_cprecsec 
 use
    endif
    if w_rowsusr <> -1 and w_rowssec<1
      sqlexec(w_nConn, "DROP TABLE cprecsecusr")
      sqlexec(w_nConn, "DROP TABLE cprecsed")
      sqlexec(w_nConn, "DROP TABLE cprecsecd")
      sqlexec(w_nConn, "DROP TABLE cprecsec")
      GSCV_BRT(this, "cprecsed" , w_nConn , .T.)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      GSCV_BRT(this, "cprecsecd" , w_nConn , .T.)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      GSCV_BRT(this, "cprecsec" , w_nConn , .T.)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='CAL_AZIE'
    this.cWorkTables[2]='*TMP_DETT'
    this.cWorkTables[3]='TAB_CALE'
    this.cWorkTables[4]='PAR_PROD'
    return(this.OpenAllTables(4))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
