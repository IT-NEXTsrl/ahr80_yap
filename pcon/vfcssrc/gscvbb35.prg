* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvbb35                                                        *
*              Aggiornamento report                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRS_161][VRS_27]                                               *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-05-02                                                      *
* Last revis.: 2006-05-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvbb35",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvbb35 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TMPC = space(10)
  w_TEMP = space(10)
  w_TDTIPDOC = space(5)
  w_TDPRGSTA = 0
  w_TDPROALT = 0
  w_TDPROSTA = 0
  w_LGCODLIN = space(3)
  w_LGDESCOD = space(35)
  w_LGPRGSTA = 0
  w_CPROWNUM = 0
  w_CPROWORD = 0
  w_INSMAIN = .f.
  w_OLDTIPDOC = space(5)
  w_TDCATDOC = space(2)
  w_PRGSTA = space(30)
  w_ONUMBAT = space(254)
  w_OUROWNUM = 0
  * --- WorkFile variables
  REPO_DOC_idx=0
  OUT_PUTS_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Conversione causali report; trasferisce i report delle causali nella nuova tabella
    *     REPO_DOC
    * --- FIle di LOG
    * --- --
    * --- Aggiornamento Report documenti
    * --- Try
    local bErr_0376AD20
    bErr_0376AD20=bTrsErr
    this.Try_0376AD20()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("Errore: impossibile aggiornare la tabella Output Utente (OUT_PUTS) %1", "(GSVE_MDV-GSAC_MDV)")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_0376AD20
    * --- End
    * --- Aggiornamento GSVEAMDV - Report Alternativi Documenti
    * --- Try
    local bErr_0360CD10
    bErr_0360CD10=bTrsErr
    this.Try_0360CD10()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("Errore: impossibile aggiornare la tabella Output Utente (OUT_PUTS) %1", "(GSVEAMDV)")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_0360CD10
    * --- End
    * --- Aggiornamento GSDS_SDP - Report produzione
    * --- Try
    local bErr_035D3180
    bErr_035D3180=bTrsErr
    this.Try_035D3180()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("Errore: impossibile aggiornare la tabella Output Utente (OUT_PUTS) %1", "(GSDS_SDP)")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_035D3180
    * --- End
    * --- Aggiornamento report causali
    * --- Try
    local bErr_03618DB0
    bErr_03618DB0=bTrsErr
    this.Try_03618DB0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("Errore: impossibile aggiornare la tabella Report Causali Doc. (REPO_DOC)")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_03618DB0
    * --- End
    if this.oParentObject.w_PESEOK
      this.oParentObject.w_PMSG = ah_Msgformat("Conversione eseguita correttamente")
    endif
  endproc
  proc Try_0376AD20()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- GSVE_MDV
    this.w_PRGSTA = LEFT("GSVE_MDV"+SPACE(30),30)
    * --- Write into OUT_PUTS
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.OUT_PUTS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OUT_PUTS_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.OUT_PUTS_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"OUNOMBAT ="+cp_NullLink(cp_ToStrODBC("GSVE1BSD(bText, cStmpOrder, nCarMemo)"),'OUT_PUTS','OUNOMBAT');
          +i_ccchkf ;
      +" where ";
          +"OUNOMPRG = "+cp_ToStrODBC(this.w_PRGSTA);
             )
    else
      update (i_cTable) set;
          OUNOMBAT = "GSVE1BSD(bText, cStmpOrder, nCarMemo)";
          &i_ccchkf. ;
       where;
          OUNOMPRG = this.w_PRGSTA;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- GSAC_MDV
    this.w_PRGSTA = LEFT("GSAC_MDV"+SPACE(30),30)
    * --- Write into OUT_PUTS
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.OUT_PUTS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OUT_PUTS_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.OUT_PUTS_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"OUNOMBAT ="+cp_NullLink(cp_ToStrODBC("GSVE1BSD(bText, cStmpOrder, nCarMemo)"),'OUT_PUTS','OUNOMBAT');
          +i_ccchkf ;
      +" where ";
          +"OUNOMPRG = "+cp_ToStrODBC(this.w_PRGSTA);
             )
    else
      update (i_cTable) set;
          OUNOMBAT = "GSVE1BSD(bText, cStmpOrder, nCarMemo)";
          &i_ccchkf. ;
       where;
          OUNOMPRG = this.w_PRGSTA;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Esecuzione ok
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Aggiornamento output utente effettuato correttamente %1", "(GSVE_MDV-GSAC_MDV)")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    this.oParentObject.w_PESEOK = .T.
    return
  proc Try_0360CD10()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.w_PRGSTA = LEFT("GSVEAMDV"+SPACE(30),30)
    * --- Write into OUT_PUTS
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.OUT_PUTS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OUT_PUTS_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.OUT_PUTS_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"OUNOMBAT ="+cp_NullLink(cp_ToStrODBC("GSVE2BSD"),'OUT_PUTS','OUNOMBAT');
          +i_ccchkf ;
      +" where ";
          +"OUNOMPRG = "+cp_ToStrODBC(this.w_PRGSTA);
             )
    else
      update (i_cTable) set;
          OUNOMBAT = "GSVE2BSD";
          &i_ccchkf. ;
       where;
          OUNOMPRG = this.w_PRGSTA;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Esecuzione ok
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Aggiornamento output utente effettuato correttamente %1", "(GSVEAMDV)")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    this.oParentObject.w_PESEOK = .T.
    return
  proc Try_035D3180()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.w_PRGSTA = LEFT("GSDS_SDP"+SPACE(30),30)
    * --- Select from OUT_PUTS
    i_nConn=i_TableProp[this.OUT_PUTS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OUT_PUTS_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select OUROWNUM  from "+i_cTable+" OUT_PUTS ";
          +" where OUNOMPRG="+cp_ToStrODBC(this.w_PRGSTA)+"";
           ,"_Curs_OUT_PUTS")
    else
      select OUROWNUM from (i_cTable);
       where OUNOMPRG=this.w_PRGSTA;
        into cursor _Curs_OUT_PUTS
    endif
    if used('_Curs_OUT_PUTS')
      select _Curs_OUT_PUTS
      locate for 1=1
      do while not(eof())
      this.w_OUROWNUM = OUROWNUM
      this.w_ONUMBAT = "GSDS1BG1("+ALLTRIM(STR(this.w_OUROWNUM))+")"
      * --- Write into OUT_PUTS
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.OUT_PUTS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.OUT_PUTS_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.OUT_PUTS_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"OUNOMBAT ="+cp_NullLink(cp_ToStrODBC(this.w_ONUMBAT),'OUT_PUTS','OUNOMBAT');
        +",OUNOMQUE ="+cp_NullLink(cp_ToStrODBC("..\DISB\EXE\QUERY\GSDS_SDP.VQR"),'OUT_PUTS','OUNOMQUE');
            +i_ccchkf ;
        +" where ";
            +"OUNOMPRG = "+cp_ToStrODBC(this.w_PRGSTA);
            +" and OUROWNUM = "+cp_ToStrODBC(this.w_OUROWNUM);
               )
      else
        update (i_cTable) set;
            OUNOMBAT = this.w_ONUMBAT;
            ,OUNOMQUE = "..\DISB\EXE\QUERY\GSDS_SDP.VQR";
            &i_ccchkf. ;
         where;
            OUNOMPRG = this.w_PRGSTA;
            and OUROWNUM = this.w_OUROWNUM;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
        select _Curs_OUT_PUTS
        continue
      enddo
      use
    endif
    * --- Esecuzione ok
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Aggiornamento output utente effettuato correttamente %1", "(GSDS_SDP)")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    this.oParentObject.w_PESEOK = .T.
    return
  proc Try_03618DB0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    do vq_exec with "..\PCON\EXE\QUERY\GSCVBB35_1",this,"GSCVBB35_1","",.f.,.t.
    if used("GSCVBB35_1")
      LOCATE FOR 1=1
      do while NOT(EOF())
        this.w_TDTIPDOC = TDTIPDOC
        this.w_TDCATDOC = TDCATDOC
        this.w_TDPRGSTA = NVL(TDPRGSTA,0)
        this.w_TDPROALT = NVL(TDPROALT,0)
        this.w_TDPROSTA = NVL(TDPROSTA,0)
        this.w_LGCODLIN = NVL(LGCODLIN,"")
        this.w_LGDESCOD = LGDESCOD
        this.w_LGPRGSTA = NVL(LGPRGSTA,0)
        this.w_INSMAIN = .F.
        this.w_CPROWNUM = 0
        this.w_CPROWORD = 0
        * --- Select from REPO_DOC
        i_nConn=i_TableProp[this.REPO_DOC_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.REPO_DOC_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select MAX(CPROWNUM) AS CPROWNUM, MAX(CPROWORD) AS CPROWORD  from "+i_cTable+" REPO_DOC ";
              +" where LGCODICE="+cp_ToStrODBC(this.w_TDTIPDOC)+"";
              +" group by LGCODICE";
               ,"_Curs_REPO_DOC")
        else
          select MAX(CPROWNUM) AS CPROWNUM, MAX(CPROWORD) AS CPROWORD from (i_cTable);
           where LGCODICE=this.w_TDTIPDOC;
           group by LGCODICE;
            into cursor _Curs_REPO_DOC
        endif
        if used('_Curs_REPO_DOC')
          select _Curs_REPO_DOC
          locate for 1=1
          do while not(eof())
          this.w_CPROWNUM = CPROWNUM
          this.w_CPROWORD = CPROWORD
            select _Curs_REPO_DOC
            continue
          enddo
          use
        endif
        if this.w_OLDTIPDOC <> this.w_TDTIPDOC And this.w_CPROWNUM<>0 And Ah_YesNo("Esistono gi� dei report caricati per la causale %1, cancellarli?","i", this.w_TDTIPDOC)
          this.w_CPROWNUM = 0
          this.w_CPROWORD = 0
          * --- Cancello i report precedenti
          * --- Delete from REPO_DOC
          i_nConn=i_TableProp[this.REPO_DOC_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.REPO_DOC_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"LGCODICE = "+cp_ToStrODBC(this.w_TDTIPDOC);
                   )
          else
            delete from (i_cTable) where;
                  LGCODICE = this.w_TDTIPDOC;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
        endif
        if Empty(AllTrim(this.w_LGCODLIN))
          if this.w_TDPRGSTA<>0
            this.w_CPROWNUM = this.w_CPROWNUM + 1
            this.w_CPROWORD = this.w_CPROWORD + 10
            * --- Inserisco Report principale
            * --- Insert into REPO_DOC
            i_nConn=i_TableProp[this.REPO_DOC_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.REPO_DOC_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.REPO_DOC_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"LGCODICE"+",CPROWNUM"+",LGTIPREP"+",LGPRGSTA"+",CPROWORD"+",LG_SPLIT"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.w_TDTIPDOC),'REPO_DOC','LGCODICE');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'REPO_DOC','CPROWNUM');
              +","+cp_NullLink(cp_ToStrODBC("M"),'REPO_DOC','LGTIPREP');
              +","+cp_NullLink(cp_ToStrODBC(this.w_TDPRGSTA),'REPO_DOC','LGPRGSTA');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'REPO_DOC','CPROWORD');
              +","+cp_NullLink(cp_ToStrODBC("MVSERIAL"),'REPO_DOC','LG_SPLIT');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'LGCODICE',this.w_TDTIPDOC,'CPROWNUM',this.w_CPROWNUM,'LGTIPREP',"M",'LGPRGSTA',this.w_TDPRGSTA,'CPROWORD',this.w_CPROWORD,'LG_SPLIT',"MVSERIAL")
              insert into (i_cTable) (LGCODICE,CPROWNUM,LGTIPREP,LGPRGSTA,CPROWORD,LG_SPLIT &i_ccchkf. );
                 values (;
                   this.w_TDTIPDOC;
                   ,this.w_CPROWNUM;
                   ,"M";
                   ,this.w_TDPRGSTA;
                   ,this.w_CPROWORD;
                   ,"MVSERIAL";
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
            this.w_INSMAIN = .T.
            * --- Se sono ordini devo modificare il batch dell'output utente
            if this.w_TDCATDOC = "OR"
              this.w_PRGSTA = LEFT("GSVE_MDV"+SPACE(30),30)
              * --- Write into OUT_PUTS
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.OUT_PUTS_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.OUT_PUTS_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.OUT_PUTS_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"OUNOMBAT ="+cp_NullLink(cp_ToStrODBC("GSOR1BSD(bText, cStmpOrder, nCarMemo)"),'OUT_PUTS','OUNOMBAT');
                    +i_ccchkf ;
                +" where ";
                    +"OUNOMPRG = "+cp_ToStrODBC(this.w_PRGSTA);
                    +" and OUROWNUM = "+cp_ToStrODBC(this.w_TDPRGSTA);
                       )
              else
                update (i_cTable) set;
                    OUNOMBAT = "GSOR1BSD(bText, cStmpOrder, nCarMemo)";
                    &i_ccchkf. ;
                 where;
                    OUNOMPRG = this.w_PRGSTA;
                    and OUROWNUM = this.w_TDPRGSTA;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
          endif
          if this.w_TDPROALT<>0
            this.w_CPROWNUM = this.w_CPROWNUM + 1
            this.w_CPROWORD = this.w_CPROWORD + 10
            * --- Inserisco Report secondario
            * --- Insert into REPO_DOC
            i_nConn=i_TableProp[this.REPO_DOC_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.REPO_DOC_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.REPO_DOC_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"LGCODICE"+",CPROWNUM"+",LGTIPREP"+",LGPRGSTA"+",CPROWORD"+",LG__LINK"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.w_TDTIPDOC),'REPO_DOC','LGCODICE');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'REPO_DOC','CPROWNUM');
              +","+cp_NullLink(cp_ToStrODBC("S"),'REPO_DOC','LGTIPREP');
              +","+cp_NullLink(cp_ToStrODBC(this.w_TDPROALT),'REPO_DOC','LGPRGSTA');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'REPO_DOC','CPROWORD');
              +","+cp_NullLink(cp_ToStrODBC("MVSERIAL"),'REPO_DOC','LG__LINK');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'LGCODICE',this.w_TDTIPDOC,'CPROWNUM',this.w_CPROWNUM,'LGTIPREP',"S",'LGPRGSTA',this.w_TDPROALT,'CPROWORD',this.w_CPROWORD,'LG__LINK',"MVSERIAL")
              insert into (i_cTable) (LGCODICE,CPROWNUM,LGTIPREP,LGPRGSTA,CPROWORD,LG__LINK &i_ccchkf. );
                 values (;
                   this.w_TDTIPDOC;
                   ,this.w_CPROWNUM;
                   ,"S";
                   ,this.w_TDPROALT;
                   ,this.w_CPROWORD;
                   ,"MVSERIAL";
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          endif
          if this.w_TDPROSTA<>0
            this.w_CPROWNUM = this.w_CPROWNUM + 1
            this.w_CPROWORD = this.w_CPROWORD + 10
            * --- Inserisco Report produzione
            * --- Se non ho principale, diventa principale quello di produzione
            if this.w_INSMAIN
              * --- Insert into REPO_DOC
              i_nConn=i_TableProp[this.REPO_DOC_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.REPO_DOC_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_ccchkf=''
              i_ccchkv=''
              this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.REPO_DOC_idx,i_nConn)
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                            " ("+"LGCODICE"+",CPROWNUM"+",LGTIPREP"+",LGPRGSTA"+",CPROWORD"+",LG__LINK"+i_ccchkf+") values ("+;
                cp_NullLink(cp_ToStrODBC(this.w_TDTIPDOC),'REPO_DOC','LGCODICE');
                +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'REPO_DOC','CPROWNUM');
                +","+cp_NullLink(cp_ToStrODBC("P"),'REPO_DOC','LGTIPREP');
                +","+cp_NullLink(cp_ToStrODBC(this.w_TDPROSTA),'REPO_DOC','LGPRGSTA');
                +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'REPO_DOC','CPROWORD');
                +","+cp_NullLink(cp_ToStrODBC("MVSERIAL"),'REPO_DOC','LG__LINK');
                     +i_ccchkv+")")
              else
                cp_CheckDeletedKey(i_cTable,0,'LGCODICE',this.w_TDTIPDOC,'CPROWNUM',this.w_CPROWNUM,'LGTIPREP',"P",'LGPRGSTA',this.w_TDPROSTA,'CPROWORD',this.w_CPROWORD,'LG__LINK',"MVSERIAL")
                insert into (i_cTable) (LGCODICE,CPROWNUM,LGTIPREP,LGPRGSTA,CPROWORD,LG__LINK &i_ccchkf. );
                   values (;
                     this.w_TDTIPDOC;
                     ,this.w_CPROWNUM;
                     ,"P";
                     ,this.w_TDPROSTA;
                     ,this.w_CPROWORD;
                     ,"MVSERIAL";
                     &i_ccchkv. )
                i_Rows=iif(bTrsErr,0,1)
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if i_Rows<0 or bTrsErr
                * --- Error: insert not accepted
                i_Error=MSG_INSERT_ERROR
                return
              endif
            else
              * --- Insert into REPO_DOC
              i_nConn=i_TableProp[this.REPO_DOC_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.REPO_DOC_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_ccchkf=''
              i_ccchkv=''
              this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.REPO_DOC_idx,i_nConn)
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                            " ("+"LGCODICE"+",CPROWNUM"+",LGTIPREP"+",LGPRGSTA"+",CPROWORD"+",LG__LINK"+i_ccchkf+") values ("+;
                cp_NullLink(cp_ToStrODBC(this.w_TDTIPDOC),'REPO_DOC','LGCODICE');
                +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'REPO_DOC','CPROWNUM');
                +","+cp_NullLink(cp_ToStrODBC("R"),'REPO_DOC','LGTIPREP');
                +","+cp_NullLink(cp_ToStrODBC(this.w_TDPROSTA),'REPO_DOC','LGPRGSTA');
                +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'REPO_DOC','CPROWORD');
                +","+cp_NullLink(cp_ToStrODBC("MVSERIAL"),'REPO_DOC','LG__LINK');
                     +i_ccchkv+")")
              else
                cp_CheckDeletedKey(i_cTable,0,'LGCODICE',this.w_TDTIPDOC,'CPROWNUM',this.w_CPROWNUM,'LGTIPREP',"R",'LGPRGSTA',this.w_TDPROSTA,'CPROWORD',this.w_CPROWORD,'LG__LINK',"MVSERIAL")
                insert into (i_cTable) (LGCODICE,CPROWNUM,LGTIPREP,LGPRGSTA,CPROWORD,LG__LINK &i_ccchkf. );
                   values (;
                     this.w_TDTIPDOC;
                     ,this.w_CPROWNUM;
                     ,"R";
                     ,this.w_TDPROSTA;
                     ,this.w_CPROWORD;
                     ,"MVSERIAL";
                     &i_ccchkv. )
                i_Rows=iif(bTrsErr,0,1)
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if i_Rows<0 or bTrsErr
                * --- Error: insert not accepted
                i_Error=MSG_INSERT_ERROR
                return
              endif
            endif
          endif
        else
          * --- Se ho una lingua devo aggiungere il report in lingua
          if this.w_LGPRGSTA<>0
            this.w_CPROWNUM = this.w_CPROWNUM + 1
            this.w_CPROWORD = this.w_CPROWORD + 10
            * --- Inserisco Report principale in lingua
            * --- Insert into REPO_DOC
            i_nConn=i_TableProp[this.REPO_DOC_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.REPO_DOC_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.REPO_DOC_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"LGCODICE"+",CPROWNUM"+",LGTIPREP"+",LGPRGSTA"+",LGCODLIN"+",CPROWORD"+",LG_SPLIT"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.w_TDTIPDOC),'REPO_DOC','LGCODICE');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'REPO_DOC','CPROWNUM');
              +","+cp_NullLink(cp_ToStrODBC("M"),'REPO_DOC','LGTIPREP');
              +","+cp_NullLink(cp_ToStrODBC(this.w_LGPRGSTA),'REPO_DOC','LGPRGSTA');
              +","+cp_NullLink(cp_ToStrODBC(this.w_LGCODLIN),'REPO_DOC','LGCODLIN');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'REPO_DOC','CPROWORD');
              +","+cp_NullLink(cp_ToStrODBC("MVSERIAL"),'REPO_DOC','LG_SPLIT');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'LGCODICE',this.w_TDTIPDOC,'CPROWNUM',this.w_CPROWNUM,'LGTIPREP',"M",'LGPRGSTA',this.w_LGPRGSTA,'LGCODLIN',this.w_LGCODLIN,'CPROWORD',this.w_CPROWORD,'LG_SPLIT',"MVSERIAL")
              insert into (i_cTable) (LGCODICE,CPROWNUM,LGTIPREP,LGPRGSTA,LGCODLIN,CPROWORD,LG_SPLIT &i_ccchkf. );
                 values (;
                   this.w_TDTIPDOC;
                   ,this.w_CPROWNUM;
                   ,"M";
                   ,this.w_LGPRGSTA;
                   ,this.w_LGCODLIN;
                   ,this.w_CPROWORD;
                   ,"MVSERIAL";
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          endif
        endif
        this.w_OLDTIPDOC = this.w_TDTIPDOC
         
 SELECT("GSCVBB35_1") 
 CONTINUE
      enddo
      USE IN SELECT("GSCVBB35_1")
    endif
    * --- Esecuzione ok
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Aggiornamento tabella Report Causali Doc. (REPO_DOC) effettuato correttamente")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    this.oParentObject.w_PESEOK = .T.
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='REPO_DOC'
    this.cWorkTables[2]='OUT_PUTS'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_OUT_PUTS')
      use in _Curs_OUT_PUTS
    endif
    if used('_Curs_REPO_DOC')
      use in _Curs_REPO_DOC
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
