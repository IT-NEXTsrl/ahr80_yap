* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcb60                                                        *
*              Sostituzione nome funzionalitą nominativi in tabella POSTIT     *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-03-17                                                      *
* Last revis.: 2009-10-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcb60",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcb60 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TMPC = space(10)
  w_TEMP = space(10)
  w_CONN = 0
  * --- WorkFile variables
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Sosituzione all'interno della tabella POSTIT della funzionalitą nominativi GSOF_ANM con la nuova GSAR_ANO, introdotta a partire dalla Rel. 6.0
    * --- FIle di LOG
    * --- Try
    local bErr_037362B0
    bErr_037362B0=bTrsErr
    this.Try_037362B0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = Ah_MsgFormat("ERRORE GENERICO - Impossibile eseguire la sostituzione in tabella  %1", "POSTIT")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_037362B0
    * --- End
  endproc
  proc Try_037362B0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Acquisisco handle della connessione al database
    this.w_CONN = i_ServerConn[1,2]
    if this.w_CONN # 0
      * --- Sostituisco la stringa "gsof_anm" presente nella colonna 'postit' dell'omonima tabella con "gsar_ano"
      cp_sqlexec(this.w_Conn,"update postit set postit = replace(cast(postit as varchar(4000)),"+cp_ToStrODBC('"gsof_anm"')+","+cp_ToStrODBC('"gsar_ano"')+") where postit like '%gsof_anm%'","_Read_")
      use in Select("_Read_")
    else
      * --- Raise
      i_Error="Connessione al database non trovata"
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = Ah_MsgFormat("Conversione eseguita correttamente")
    if this.w_NHF>=0
      this.w_TMPC = Ah_MsgFormat("Sostituzione della funzionalitą nominativi in tabella POSTIT avvenuta correttamente")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
