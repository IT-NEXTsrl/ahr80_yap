* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvbb52                                                        *
*              Nuova gestione file allegati                                    *
*                                                                              *
*      Author: Zucchetti SpA                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-03-17                                                      *
* Last revis.: 2003-03-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvbb52",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvbb52 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TMPC = space(10)
  w_TEMP = space(10)
  w_APPO = space(1)
  w_AZIENDA = space(5)
  w_ARCHIVIO = space(2)
  w_CRITPERI = space(1)
  w_TIPDEF = space(5)
  w_CLADEF = space(5)
  w_LIPATIMG = space(254)
  w_VOLUME = space(10)
  w_FL_TIP = space(1)
  w_FL_CLA = space(1)
  w_GESTIONE = space(50)
  w_TIPDEFEX = space(5)
  w_CLADEFEX = space(5)
  w_TEST = .f.
  w_ImgPat = space(10)
  w_curdir = space(50)
  w_Loop = 0
  w_LoopFile = 0
  w_FileName = space(254)
  w_TrueFile = space(254)
  w_DescExt = space(254)
  w_NumFile = 0
  w_NewFile = 0
  w_OldFile = 0
  w_CODART = space(20)
  w_TotFile = 0
  w_Periodo = ctod("  /  /  ")
  * --- WorkFile variables
  LIB_IMMA_idx=0
  GESTFILE_idx=0
  EXT_ENS_idx=0
  ART_ICOL_idx=0
  PNT_MAST_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Procedura per recuperare i file allegati creati con la gestione GESTFILE
    *     e quindi il caricamento degli stessi nella nuova gestione di Allegati
    * --- FIle di LOG
    this.w_AZIENDA = ALLTRIM(I_CODAZI)
    this.oParentObject.w_PESEOK = .T.
    * --- Try
    local bErr_03608C60
    bErr_03608C60=bTrsErr
    this.Try_03608C60()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = Ah_MsgFormat("ERRORE GENERICO - Impossibile rinominare file allegati ")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_03608C60
    * --- End
  endproc
  proc Try_03608C60()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if Ah_YesNo("Questa procedura � utile solo nel caso siano stati allegati file ad articoli con il punto (.) presente nel codice. Continuare?")
      * --- begin transaction
      cp_BeginTrs()
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- commit
      cp_EndTrs(.t.)
    endif
    * --- Esecuzione ok
    if this.w_NHF>=0
      this.w_TMPC = Ah_MsgFormat("Modificati n. %1 file allegati",Alltrim(Str(this.w_TotFile,10,0)))
      this.oParentObject.w_pMSG = this.w_TMPC
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = Ah_MsgFormat("Conversione eseguita correttamente")
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_FileName = ""
    this.w_TrueFile = ""
    this.w_DescExt = ""
    this.w_NewFile = ""
    this.w_OldFile = ""
    this.w_TotFile = 0
    * --- All'interno della cartella definita nelle Librerie Immagini per ogni categoria 
    *     esistono le cartelle dei periodi che contengono i file.
    *     Devo quindi eseguire una scansione di queste cartelle
    this.w_curdir = sys(5) + sys(2003)
    * --- Select from LIB_IMMA
    i_nConn=i_TableProp[this.LIB_IMMA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.LIB_IMMA_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" LIB_IMMA ";
          +" where LICODICE = 'GESTFILE' AND LITIPARC='AR'";
           ,"_Curs_LIB_IMMA")
    else
      select * from (i_cTable);
       where LICODICE = "GESTFILE" AND LITIPARC="AR";
        into cursor _Curs_LIB_IMMA
    endif
    if used('_Curs_LIB_IMMA')
      select _Curs_LIB_IMMA
      locate for 1=1
      do while not(eof())
      * --- Seleziono solo gli articoli
      this.w_ARCHIVIO = Nvl(_Curs_LIB_IMMA.LITIPARC,"  ")
      this.w_CRITPERI = Nvl(_Curs_LIB_IMMA.LIDATRIF," ")
      this.w_TIPDEF = Nvl(_Curs_LIB_IMMA.LITIPALL,Space(5))
      this.w_CLADEF = Nvl(_Curs_LIB_IMMA.LICLAALL,Space(5))
      this.w_LIPATIMG = Nvl(_Curs_LIB_IMMA.LIPATIMG,"")
      this.w_VOLUME = Nvl(_Curs_LIB_IMMA.LIVOLUME,Space(10) )
      this.w_FL_TIP = Nvl(_Curs_LIB_IMMA.LIFL_TIP, " ")
      this.w_FL_CLA = Nvl(_Curs_LIB_IMMA.LIFL_CLA, " ")
      this.w_LIPATIMG = alltrim(this.w_LIPATIMG) + iif(right(alltrim(this.w_LIPATIMG),1) <>"\" , "\" , "" )
      * --- Select from GSCVBB52
      do vq_exec with 'GSCVBB52',this,'_Curs_GSCVBB52','',.f.,.t.
      if used('_Curs_GSCVBB52')
        select _Curs_GSCVBB52
        locate for 1=1
        do while not(eof())
        this.w_CODART = Alltrim(_Curs_GSCVBB52.CODART)
        * --- Se nel codice articolo c'� il punto eseguo l'operazione
        this.w_Periodo = _Curs_GSCVBB52.UTDC
        * --- Gestione Periodo
        * --- Calcolo nome directory memorizzazione immagini
        this.w_ImgPat = this.w_LIPATIMG+alltrim(i_codazi)
        do case
          case this.w_CritPeri = "M"
            * --- Raggruppamento per mesi
            this.w_ImgPat = this.w_ImgPat + right("00"+alltrim(str(month(this.w_Periodo),2,0)), 2) + str(year(this.w_Periodo),4,0)
          case this.w_CritPeri = "T"
            * --- Raggruppamento per trimestri
            do case
              case month(this.w_Periodo) > 0 .and. month(this.w_Periodo) < 4
                this.w_ImgPat = this.w_ImgPat + "T1" + str(year(this.w_Periodo),4,0)
              case month(this.w_Periodo) > 3 .and. month(this.w_Periodo) < 7
                this.w_ImgPat = this.w_ImgPat + "T2" + str(year(this.w_Periodo),4,0)
              case month(this.w_Periodo) > 6 .and. month(this.w_Periodo) < 10
                this.w_ImgPat = this.w_ImgPat + "T3" + str(year(this.w_Periodo),4,0)
              case month(this.w_Periodo) > 9
                this.w_ImgPat = this.w_ImgPat + "T4" + str(year(this.w_Periodo),4,0)
            endcase
          case this.w_CritPeri = "Q"
            * --- Raggruppamento per quadrimestri
            do case
              case month(this.w_Periodo) > 0 .and. month(this.w_Periodo) < 5
                this.w_ImgPat = this.w_ImgPat + "Q1" + str(year(this.w_Periodo),4,0)
              case month(this.w_Periodo) > 4 .and. month(this.w_Periodo) < 9
                this.w_ImgPat = this.w_ImgPat + "Q2" + str(year(this.w_Periodo),4,0)
              case month(this.w_Periodo) > 8
                this.w_ImgPat = this.w_ImgPat + "Q3" + str(year(this.w_Periodo),4,0)
            endcase
          case this.w_CritPeri = "S"
            * --- Raggruppamento per semestri
            this.w_ImgPat = this.w_ImgPat + iif( month(this.w_Periodo) >6 , "S2", "S1") + str(year(this.w_Periodo),4,0)
          case this.w_CritPeri = "A"
            * --- Raggruppamento per anni
            this.w_ImgPat = this.w_ImgPat + str(year(this.w_Periodo),4,0)
        endcase
        this.w_ImgPat = this.w_ImgPat+"\"
        this.w_FileName = Alltrim(this.w_ImgPat)+"A"+this.w_CODART
        if Directory( this.w_ImgPat )
          * --- Posizionamento nella cartella contenente i file da visualizzare
          this.w_NumFile = 0
          * --- Cerco tutti i file legati all'articolo
          this.w_NumFile = Adir(aFiletomod, Alltrim(this.w_FileName)+"*")
          if this.w_NumFile<>0
            this.w_Loop = 1
            * --- Ciclo sui file e modifico il nome
            do while this.w_Loop <= Alen(aFiletomod,1)
              this.w_TrueFile = aFiletomod(this.w_Loop,1)
              * --- Descrizione e estensione
              *     + 2 per considerare anche la A iniziale
              this.w_DescExt = substr(this.w_TrueFile,LEN(Alltrim(this.w_CODART))+2)
              * --- Nome vecchio file
              this.w_OldFile = Alltrim(this.w_TrueFile)
              * --- Nuovo file: sostituisco il punto (.) con ^B
              this.w_NewFile = "A"+Alltrim(StrTran(this.w_CODART,".","^B"))
              this.w_NewFile = Alltrim(this.w_NewFile)+Alltrim(this.w_DescExt)
              cd (this.w_ImgPat)
              Rename (this.w_OldFile) To (this.w_NewFile)
              cd (this.w_curdir)
              Ah_Msg("File :%1",.T.,.F.,.F., this.w_NewFile)
              this.w_TotFile = this.w_TotFile + 1
              this.w_Loop = this.w_Loop + 1
            enddo
          endif
        endif
          select _Curs_GSCVBB52
          continue
        enddo
        use
      endif
        select _Curs_LIB_IMMA
        continue
      enddo
      use
    endif
  endproc


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='LIB_IMMA'
    this.cWorkTables[2]='GESTFILE'
    this.cWorkTables[3]='EXT_ENS'
    this.cWorkTables[4]='ART_ICOL'
    this.cWorkTables[5]='PNT_MAST'
    return(this.OpenAllTables(5))

  proc CloseCursors()
    if used('_Curs_LIB_IMMA')
      use in _Curs_LIB_IMMA
    endif
    if used('_Curs_GSCVBB52')
      use in _Curs_GSCVBB52
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
