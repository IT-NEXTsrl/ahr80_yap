* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcb86                                                        *
*              Aggiornamento tabella materiali dichiarazioni di produzione     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-01-20                                                      *
* Last revis.: 2011-02-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcb86",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcb86 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_TABLE = space(8)
  w_inConn = 0
  w_FraseSQL = space(200)
  w_PHNAME = space(200)
  * --- WorkFile variables
  MAT_PROD_idx=0
  AVA_LOT_idx=0
  AVA_MATR_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Modifica ciclo di lavorazione legato all'ordine di lavorazione
    * --- Try
    local bErr_036290E0
    bErr_036290E0=bTrsErr
    this.Try_036290E0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("Errore generico - impossibile eseguire aggiornamento richiesto")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_036290E0
    * --- End
  endproc
  proc Try_036290E0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if upper(CP_DBTYPE)<>"DB2"
      * --- Elimino integritÓ referenziali su tabelle (Fuori dalla transazione)
      this.w_TABLE = "MAT_PROD"
      if this.w_NHF>=0
        this.w_TMPC = ah_msgformat("Rimozione integritÓ referenziale tabella " + this.w_TABLE)
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
      GSCV_BIN(this,i_CODAZI,this.w_TABLE)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- --------------------------------------------------------------------------------------
      this.w_TABLE = "AVA_LOT"
      if this.w_NHF>=0
        this.w_TMPC = ah_msgformat("Rimozione integritÓ referenziale tabella " + this.w_TABLE)
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
      GSCV_BIN(this,i_CODAZI,this.w_TABLE)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- --------------------------------------------------------------------------------------
      this.w_TABLE = "AVA_MATR"
      if this.w_NHF>=0
        this.w_TMPC = ah_msgformat("Rimozione integritÓ referenziale tabella " + this.w_TABLE)
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
      GSCV_BIN(this,i_CODAZI,this.w_TABLE)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- begin transaction
    cp_BeginTrs()
    * --- --------------------------------------------------------------------------------------
    this.w_iNConn = i_ServerConn[1,2]
    ah_msg("Aggiornamento in corso..." )
    if this.w_NHF>=0
      this.w_TMPC = ah_msgformat("Aggiornamento valori campo MPROWDOC su tabella MAT_PROD")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    * --- Select from gscvcb86
    do vq_exec with 'gscvcb86',this,'_Curs_gscvcb86','',.f.,.t.
    if used('_Curs_gscvcb86')
      select _Curs_gscvcb86
      locate for 1=1
      do while not(eof())
      * --- Write into MAT_PROD
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.MAT_PROD_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MAT_PROD_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.MAT_PROD_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MPROWDOC ="+cp_NullLink(cp_ToStrODBC(_Curs_gscvcb86.mprowdoc),'MAT_PROD','MPROWDOC');
            +i_ccchkf ;
        +" where ";
            +"MPSERIAL = "+cp_ToStrODBC(_Curs_gscvcb86.mpserial);
            +" and MPNUMRIF = "+cp_ToStrODBC(_Curs_gscvcb86.mpnumrif);
               )
      else
        update (i_cTable) set;
            MPROWDOC = _Curs_gscvcb86.mprowdoc;
            &i_ccchkf. ;
         where;
            MPSERIAL = _Curs_gscvcb86.mpserial;
            and MPNUMRIF = _Curs_gscvcb86.mpnumrif;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Write into AVA_LOT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.AVA_LOT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AVA_LOT_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.AVA_LOT_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"CLROWDOC ="+cp_NullLink(cp_ToStrODBC(_Curs_gscvcb86.mprowdoc),'AVA_LOT','CLROWDOC');
            +i_ccchkf ;
        +" where ";
            +"CLDICHIA = "+cp_ToStrODBC(_Curs_gscvcb86.mpserial);
            +" and CLNUMRIF = "+cp_ToStrODBC(_Curs_gscvcb86.mpnumrif);
               )
      else
        update (i_cTable) set;
            CLROWDOC = _Curs_gscvcb86.mprowdoc;
            &i_ccchkf. ;
         where;
            CLDICHIA = _Curs_gscvcb86.mpserial;
            and CLNUMRIF = _Curs_gscvcb86.mpnumrif;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Write into AVA_MATR
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.AVA_MATR_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AVA_MATR_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.AVA_MATR_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MTROWDOC ="+cp_NullLink(cp_ToStrODBC(_Curs_gscvcb86.mprowdoc),'AVA_MATR','MTROWDOC');
            +i_ccchkf ;
        +" where ";
            +"MTSERIAL = "+cp_ToStrODBC(_Curs_gscvcb86.mpserial);
            +" and MTNUMRIF = "+cp_ToStrODBC(_Curs_gscvcb86.mpnumrif);
               )
      else
        update (i_cTable) set;
            MTROWDOC = _Curs_gscvcb86.mprowdoc;
            &i_ccchkf. ;
         where;
            MTSERIAL = _Curs_gscvcb86.mpserial;
            and MTNUMRIF = _Curs_gscvcb86.mpnumrif;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
        select _Curs_gscvcb86
        continue
      enddo
      use
    endif
    * --- commit
    cp_EndTrs(.t.)
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Aggiornamento eseguito con successo")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    this.w_inConn =  i_ServerConn[1,2]
    this.w_TABLE = "MAT_PROD"
    GSCV_BRT(this,this.w_TABLE , this.w_inConn , .T. , "all", "N")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_TABLE = "AVA_LOT"
    GSCV_BRT(this,this.w_TABLE , this.w_inConn , .T. , "all", "N")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_TABLE = "AVA_MATR"
    GSCV_BRT(this,this.w_TABLE , this.w_inConn , .T. , "all", "N")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = this.w_TMPC
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='MAT_PROD'
    this.cWorkTables[2]='AVA_LOT'
    this.cWorkTables[3]='AVA_MATR'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_gscvcb86')
      use in _Curs_gscvcb86
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
