* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscv_b95                                                        *
*              Aggiornamento campo dd___CAP                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_352]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-10-18                                                      *
* Last revis.: 2001-10-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscv_b95",oParentObject,m.w_NHF)
return(i_retval)

define class tgscv_b95 as StdBatch
  * --- Local variables
  w_CODABI = space(5)
  w_CODCAB = space(5)
  w_DESFIL = space(0)
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_CODCAP = space(8)
  * --- WorkFile variables
  RIPATMP1_idx=0
  DES_DIVE_idx=0
  BAN_CHE_idx=0
  MOD_COAN_idx=0
  COD_CAB_idx=0
  CAN_TIER_idx=0
  SE_INAIL_idx=0
  SEDIAZIE_idx=0
  MAGAZZIN_idx=0
  AGENTI_idx=0
  VETTORI_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiornamento del campo C.A.P. nelle rispettive tabelle:
    *     DES_DIVE (Destinazioni Diverse)
    *     BAN_CHE (Banche)
    *     MOD_COAN (Com. Annuale Dati IVA)
    *     COD_CAB (Codici CAB)
    *     CAN_TIER (Commesse)
    *     SE_INAIL (Sedi Inail)
    *     SEDIAZIE (Sedi Azienda)
    *     MAGAZZIN
    *     AGENTI
    *     VETTORI
    * --- FIle di LOG
    * --- Try
    local bErr_037C0A30
    bErr_037C0A30=bTrsErr
    this.Try_037C0A30()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = this.oParentObject.w_PMSG
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_037C0A30
    * --- End
    * --- Try
    local bErr_03628690
    bErr_03628690=bTrsErr
    this.Try_03628690()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = this.oParentObject.w_PMSG
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_03628690
    * --- End
    * --- Try
    local bErr_035FA7E0
    bErr_035FA7E0=bTrsErr
    this.Try_035FA7E0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = this.oParentObject.w_PMSG
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_035FA7E0
    * --- End
    * --- Try
    local bErr_037B9A10
    bErr_037B9A10=bTrsErr
    this.Try_037B9A10()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = this.oParentObject.w_PMSG
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_037B9A10
    * --- End
    * --- Try
    local bErr_037C6580
    bErr_037C6580=bTrsErr
    this.Try_037C6580()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = this.oParentObject.w_PMSG
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_037C6580
    * --- End
    * --- Try
    local bErr_035F3180
    bErr_035F3180=bTrsErr
    this.Try_035F3180()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = this.oParentObject.w_PMSG
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_035F3180
    * --- End
    * --- Try
    local bErr_03736A30
    bErr_03736A30=bTrsErr
    this.Try_03736A30()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = this.oParentObject.w_PMSG
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_03736A30
    * --- End
    * --- Try
    local bErr_037371B0
    bErr_037371B0=bTrsErr
    this.Try_037371B0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      ah_Msg(MESSAGE(),.T.)
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE - impossibile aggiornare CAP su tabelle MAGAZZIN/AGENTI/VETTORI") 
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_037371B0
    * --- End
  endproc
  proc Try_037C0A30()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    i_nConn=i_TableProp[this.DES_DIVE_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.DES_DIVE_idx,2])
    if i_nConn<>0
      do case
        case upper(CP_DBTYPE)="SQLSERVER"
          * --- Caso SQLServer
          w_iRows = SQLExec(i_nConn,"ALTER TABLE "+i_cTable+; 
 " ALTER COLUMN DD___CAP char(8)")
          w_iRows = SQLExec(i_nConn,"ALTER TABLE xxxDES_DIVE"+; 
 " ALTER COLUMN DD___CAP char(8)")
        case upper(CP_DBTYPE)="ORACLE"
          * --- Caso ORACLE
          w_iRows = SQLExec(i_nConn,"ALTER TABLE "+i_cTable+; 
 " MODIFY(DD___CAP)")
          w_iRows = SQLExec(i_nConn,"ALTER TABLE xxxDES_DIVE"+; 
 " MODIFY(DD___CAP)")
        case upper(CP_DBTYPE)="DB2"
          * --- Caso DB2
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
      endcase
      if bTrsErr
        * --- Raise
        i_Error=MESSAGE()
        return
      else
        if this.w_NHF>=0
          this.w_TMPC = ah_Msgformat("Adeguamento chiave primaria tabella DES_DIVE eseguito correttamente%0ATTENZIONE: occorre andare in MANUTENZIONE DATABASE, selezionare la tabella%0DES_DIVE e premere <OK>")
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.w_TMPC = ah_Msgformat("Adeguamento chiave primaria tabella DES_DIVE eseguito correttamente%0ATTENZIONE: occorre andare in MANUTENZIONE DATABASE, selezionare la tabella%0DES_DIVE e premere <OK>")
    this.oParentObject.w_PMSG = this.w_TMPC
    return
  proc Try_03628690()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    i_nConn=i_TableProp[this.BAN_CHE_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.BAN_CHE_idx,2])
    if i_nConn<>0
      do case
        case upper(CP_DBTYPE)="SQLSERVER"
          * --- Caso SQLServer
          w_iRows = SQLExec(i_nConn,"ALTER TABLE "+i_cTable+; 
 " ALTER COLUMN BA___CAP char(8)")
          w_iRows = SQLExec(i_nConn,"ALTER TABLE xxxBAN_CHE"+; 
 " ALTER COLUMN BA___CAP char(8)")
        case upper(CP_DBTYPE)="ORACLE"
          * --- Caso ORACLE
          w_iRows = SQLExec(i_nConn,"ALTER TABLE "+i_cTable+; 
 " MODIFY(BA___CAP)")
          w_iRows = SQLExec(i_nConn,"ALTER TABLE xxxBAN_CHE"+; 
 " MODIFY(BA___CAP)")
        case upper(CP_DBTYPE)="DB2"
          * --- Caso DB2
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
      endcase
      if bTrsErr
        * --- Raise
        i_Error=MESSAGE()
        return
      else
        if this.w_NHF>=0
          this.w_TMPC = ah_Msgformat("Adeguamento chiave primaria tabella BAN_CHE eseguito correttamente%0ATTENZIONE: occorre andare in MANUTENZIONE DATABASE, selezionare la tabella%0BAN_CHE e premere <OK>")
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.w_TMPC = ah_Msgformat("Adeguamento chiave primaria tabella BAN_CHE eseguito correttamente%0ATTENZIONE: occorre andare in MANUTENZIONE DATABASE, selezionare la tabella%0BAN_CHE e premere <OK>")
    this.oParentObject.w_PMSG = this.w_TMPC
    return
  proc Try_035FA7E0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    i_nConn=i_TableProp[this.MOD_COAN_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.MOD_COAN_idx,2])
    if i_nConn<>0
      do case
        case upper(CP_DBTYPE)="SQLSERVER"
          * --- Caso SQLServer
          w_iRows = SQLExec(i_nConn,"ALTER TABLE "+i_cTable+; 
 " ALTER COLUMN AI___CAP char(8)")
          w_iRows = SQLExec(i_nConn,"ALTER TABLE xxxMOD_COAN"+; 
 " ALTER COLUMN AI___CAP char(8)")
          w_iRows = SQLExec(i_nConn,"ALTER TABLE "+i_cTable+; 
 " ALTER COLUMN AISE_CAP char(8)")
          w_iRows = SQLExec(i_nConn,"ALTER TABLE xxxMOD_COAN"+; 
 " ALTER COLUMN AISE_CAP char(8)")
          w_iRows = SQLExec(i_nConn,"ALTER TABLE "+i_cTable+; 
 " ALTER COLUMN AISERCAP char(8)")
          w_iRows = SQLExec(i_nConn,"ALTER TABLE xxxMOD_COAN"+; 
 " ALTER COLUMN AISERCAP char(8)")
        case upper(CP_DBTYPE)="ORACLE"
          * --- Caso ORACLE
          w_iRows = SQLExec(i_nConn,"ALTER TABLE "+i_cTable+; 
 " MODIFY(AI___CAP)")
          w_iRows = SQLExec(i_nConn,"ALTER TABLE xxxMOD_COAN"+; 
 " MODIFY(AI___CAP)")
          w_iRows = SQLExec(i_nConn,"ALTER TABLE "+i_cTable+; 
 " MODIFY(AISE_CAP)")
          w_iRows = SQLExec(i_nConn,"ALTER TABLE xxxMOD_COAN"+; 
 " MODIFY(AISE_CAP)")
          w_iRows = SQLExec(i_nConn,"ALTER TABLE "+i_cTable+; 
 " MODIFY(AISERCAP)")
          w_iRows = SQLExec(i_nConn,"ALTER TABLE xxxMOD_COAN"+; 
 " MODIFY(AISERCAP)")
        case upper(CP_DBTYPE)="DB2"
          * --- Caso DB2
          this.Pag4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
      endcase
      if bTrsErr
        * --- Raise
        i_Error=MESSAGE()
        return
      else
        if this.w_NHF>=0
          this.w_TMPC = ah_Msgformat("Adeguamento chiave primaria tabella MOD_COAN eseguito correttamente%0ATTENZIONE: occorre andare in MANUTENZIONE DATABASE, selezionare la tabella%MOD_COAN e premere <OK>")
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.w_TMPC = ah_Msgformat("Adeguamento chiave primaria tabella MOD_COAN eseguito correttamente%0ATTENZIONE: occorre andare in MANUTENZIONE DATABASE, selezionare la tabella%MOD_COAN e premere <OK>")
    this.oParentObject.w_PMSG = this.w_TMPC
    return
  proc Try_037B9A10()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    i_nConn=i_TableProp[this.COD_CAB_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.COD_CAB_idx,2])
    if i_nConn<>0
      do case
        case upper(CP_DBTYPE)="SQLSERVER"
          * --- Caso SQLServer
          w_iRows = SQLExec(i_nConn,"ALTER TABLE "+i_cTable+; 
 " ALTER COLUMN FI___CAP char(8)")
          w_iRows = SQLExec(i_nConn,"ALTER TABLE xxxCOD_CAB"+; 
 " ALTER COLUMN FI___CAP char(8)")
        case upper(CP_DBTYPE)="ORACLE"
          * --- Caso ORACLE
          w_iRows = SQLExec(i_nConn,"ALTER TABLE "+i_cTable+; 
 " MODIFY(FI___CAP)")
          w_iRows = SQLExec(i_nConn,"ALTER TABLE xxxCOD_CAB"+; 
 " MODIFY(FI___CAP)")
        case upper(CP_DBTYPE)="DB2"
          * --- Caso DB2
          this.Pag5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
      endcase
      if bTrsErr
        * --- Raise
        i_Error=MESSAGE()
        return
      else
        if this.w_NHF>=0
          this.w_TMPC = ah_Msgformat("Adeguamento chiave primaria tabella COD_CAB eseguito correttamente%0ATTENZIONE: occorre andare in MANUTENZIONE DATABASE, selezionare la tabella%COD_CAB e premere <OK>")
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.w_TMPC = ah_Msgformat("Adeguamento chiave primaria tabella COD_CAB eseguito correttamente%0ATTENZIONE: occorre andare in MANUTENZIONE DATABASE, selezionare la tabella%COD_CAB e premere <OK>")
    this.oParentObject.w_PMSG = this.w_TMPC
    return
  proc Try_037C6580()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    i_nConn=i_TableProp[this.CAN_TIER_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2])
    if i_nConn<>0
      do case
        case upper(CP_DBTYPE)="SQLSERVER"
          * --- Caso SQLServer
          w_iRows = SQLExec(i_nConn,"ALTER TABLE "+i_cTable+; 
 " ALTER COLUMN CN___CAP char(8)")
          w_iRows = SQLExec(i_nConn,"ALTER TABLE xxxCAN_TIER"+; 
 " ALTER COLUMN CN___CAP char(8)")
        case upper(CP_DBTYPE)="ORACLE"
          * --- Caso ORACLE
          w_iRows = SQLExec(i_nConn,"ALTER TABLE "+i_cTable+; 
 " MODIFY(CN___CAP)")
          w_iRows = SQLExec(i_nConn,"ALTER TABLE xxxCAN_TIER"+; 
 " MODIFY(CN___CAP)")
        case upper(CP_DBTYPE)="DB2"
          * --- Caso DB2
          this.Pag6()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
      endcase
      if bTrsErr
        * --- Raise
        i_Error=MESSAGE()
        return
      else
        if this.w_NHF>=0
          this.w_TMPC = ah_Msgformat("Adeguamento chiave primaria tabella CAN_TIER eseguito correttamente%0ATTENZIONE: occorre andare in MANUTENZIONE DATABASE, selezionare la tabella%CAN_TIER e premere <OK>")
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.w_TMPC = ah_Msgformat("Adeguamento chiave primaria tabella CAN_TIER eseguito correttamente%0ATTENZIONE: occorre andare in MANUTENZIONE DATABASE, selezionare la tabella%CAN_TIER e premere <OK>")
    this.oParentObject.w_PMSG = this.w_TMPC
    return
  proc Try_035F3180()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    i_nConn=i_TableProp[this.SE_INAIL_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.SE_INAIL_idx,2])
    if i_nConn<>0
      do case
        case upper(CP_DBTYPE)="SQLSERVER"
          * --- Caso SQLServer
          w_iRows = SQLExec(i_nConn,"ALTER TABLE "+i_cTable+; 
 " ALTER COLUMN SI___CAP char(8)")
          w_iRows = SQLExec(i_nConn,"ALTER TABLE xxxSE_INAIL"+; 
 " ALTER COLUMN SI___CAP char(8)")
        case upper(CP_DBTYPE)="ORACLE"
          * --- Caso ORACLE
          w_iRows = SQLExec(i_nConn,"ALTER TABLE "+i_cTable+; 
 " MODIFY(SI___CAP)")
          w_iRows = SQLExec(i_nConn,"ALTER TABLE xxxSE_INAIL"+; 
 " MODIFY(SI___CAP)")
        case upper(CP_DBTYPE)="DB2"
          * --- Caso DB2
          this.Pag7()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
      endcase
      if bTrsErr
        * --- Raise
        i_Error=MESSAGE()
        return
      else
        if this.w_NHF>=0
          this.w_TMPC = ah_Msgformat("Adeguamento chiave primaria tabella SE_INAIL eseguito correttamente%0ATTENZIONE: occorre andare in MANUTENZIONE DATABASE, selezionare la tabella%SE_INAIL e premere <OK>")
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.w_TMPC = ah_Msgformat("Adeguamento chiave primaria tabella SE_INAIL eseguito correttamente%0ATTENZIONE: occorre andare in MANUTENZIONE DATABASE, selezionare la tabella%SE_INAIL e premere <OK>")
    this.oParentObject.w_PMSG = this.w_TMPC
    return
  proc Try_03736A30()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    i_nConn=i_TableProp[this.SEDIAZIE_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.SEDIAZIE_idx,2])
    if i_nConn<>0
      do case
        case upper(CP_DBTYPE)="SQLSERVER"
          * --- Caso SQLServer
          w_iRows = SQLExec(i_nConn,"ALTER TABLE "+i_cTable+; 
 " ALTER COLUMN SE___CAP char(8)")
          w_iRows = SQLExec(i_nConn,"ALTER TABLE xxxSEDIAZIE"+; 
 " ALTER COLUMN SE___CAP char(8)")
        case upper(CP_DBTYPE)="ORACLE"
          * --- Caso ORACLE
          w_iRows = SQLExec(i_nConn,"ALTER TABLE "+i_cTable+; 
 " MODIFY(SE___CAP)")
          w_iRows = SQLExec(i_nConn,"ALTER TABLE xxxSEDIAZIE"+; 
 " MODIFY(SE___CAP)")
        case upper(CP_DBTYPE)="DB2"
          * --- Caso DB2
          this.Pag8()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
      endcase
      if bTrsErr
        * --- Raise
        i_Error=MESSAGE()
        return
      else
        if this.w_NHF>=0
          this.w_TMPC = ah_Msgformat("Adeguamento chiave primaria tabella SEDIAZIE eseguito correttamente%0ATTENZIONE: occorre andare in MANUTENZIONE DATABASE, selezionare la tabella%SEDIAZIE e premere <OK>")
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.w_TMPC = ah_Msgformat("Adeguamento chiave primaria tabella SEDIAZIE eseguito correttamente%0ATTENZIONE: occorre andare in MANUTENZIONE DATABASE, selezionare la tabella%SEDIAZIE e premere <OK>")
    this.oParentObject.w_PMSG = this.w_TMPC
    return
  proc Try_037371B0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Aggiornamento  MGMAGCAP
    * --- Select from MAGAZZIN
    i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" MAGAZZIN ";
           ,"_Curs_MAGAZZIN")
    else
      select * from (i_cTable);
        into cursor _Curs_MAGAZZIN
    endif
    if used('_Curs_MAGAZZIN')
      select _Curs_MAGAZZIN
      locate for 1=1
      do while not(eof())
      this.w_CODCAP = _Curs_MAGAZZIN.MGCAPMAG
      * --- Write into MAGAZZIN
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.MAGAZZIN_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MGMAGCAP ="+cp_NullLink(cp_ToStrODBC(this.w_CODCAP),'MAGAZZIN','MGMAGCAP');
            +i_ccchkf ;
        +" where ";
            +"MGCODMAG = "+cp_ToStrODBC(_Curs_MAGAZZIN.MGCODMAG);
               )
      else
        update (i_cTable) set;
            MGMAGCAP = this.w_CODCAP;
            &i_ccchkf. ;
         where;
            MGCODMAG = _Curs_MAGAZZIN.MGCODMAG;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
        select _Curs_MAGAZZIN
        continue
      enddo
      use
    endif
    * --- Select from AGENTI
    i_nConn=i_TableProp[this.AGENTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AGENTI_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" AGENTI ";
           ,"_Curs_AGENTI")
    else
      select * from (i_cTable);
        into cursor _Curs_AGENTI
    endif
    if used('_Curs_AGENTI')
      select _Curs_AGENTI
      locate for 1=1
      do while not(eof())
      this.w_CODCAP = _Curs_AGENTI.AGCAPAGE
      * --- Write into AGENTI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.AGENTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AGENTI_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.AGENTI_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"AGAGECAP ="+cp_NullLink(cp_ToStrODBC(this.w_CODCAP),'AGENTI','AGAGECAP');
            +i_ccchkf ;
        +" where ";
            +"AGCODAGE = "+cp_ToStrODBC(_Curs_AGENTI.AGCODAGE);
               )
      else
        update (i_cTable) set;
            AGAGECAP = this.w_CODCAP;
            &i_ccchkf. ;
         where;
            AGCODAGE = _Curs_AGENTI.AGCODAGE;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
        select _Curs_AGENTI
        continue
      enddo
      use
    endif
    * --- Select from VETTORI
    i_nConn=i_TableProp[this.VETTORI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VETTORI_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" VETTORI ";
           ,"_Curs_VETTORI")
    else
      select * from (i_cTable);
        into cursor _Curs_VETTORI
    endif
    if used('_Curs_VETTORI')
      select _Curs_VETTORI
      locate for 1=1
      do while not(eof())
      this.w_CODCAP = _Curs_VETTORI.VTCAPVET
      * --- Write into VETTORI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.VETTORI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VETTORI_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.VETTORI_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"VTVETCAP ="+cp_NullLink(cp_ToStrODBC(this.w_CODCAP),'VETTORI','VTVETCAP');
            +i_ccchkf ;
        +" where ";
            +"VTCODVET = "+cp_ToStrODBC(_Curs_VETTORI.VTCODVET);
               )
      else
        update (i_cTable) set;
            VTVETCAP = this.w_CODCAP;
            &i_ccchkf. ;
         where;
            VTCODVET = _Curs_VETTORI.VTCODVET;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
        select _Curs_VETTORI
        continue
      enddo
      use
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Aggiornamento campo CAP su tabelle MAGAZZIN/AGENTI/VETTORI eseguito con successo")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = this.oParentObject.w_PMSG + this.w_TMPC 
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella DB2
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.DES_DIVE_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.DES_DIVE_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
          )
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    * --- Elimino la tabella
    if Not GSCV_BDT( this, "DES_DIVE" , i_nConn )
      * --- Raise
      i_Error="Error"
      return
    endif
    * --- Ricostruisco il database per la tabella
    GSCV_BRT(this, "DES_DIVE" , i_nConn , .T.)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into DES_DIVE
    i_nConn=i_TableProp[this.DES_DIVE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DES_DIVE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.DES_DIVE_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_Msg("Ripristinati valori di origine",.T.)
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    ah_Msg("Eliminata tabella temporanea tmp",.T.)
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella DB2
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.BAN_CHE_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.BAN_CHE_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
          )
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    * --- Elimino la tabella
    if Not GSCV_BDT( this, "BAN_CHE" , i_nConn )
      * --- Raise
      i_Error="Error"
      return
    endif
    * --- Ricostruisco il database per la tabella
    GSCV_BRT(this, "BAN_CHE" , i_nConn , .T.)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into BAN_CHE
    i_nConn=i_TableProp[this.BAN_CHE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.BAN_CHE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.BAN_CHE_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_Msg("Ripristinati valori di origine",.T.)
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    ah_Msg("Eliminata tabella temporanea tmp",.T.)
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella DB2
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.MOD_COAN_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.MOD_COAN_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
          )
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    * --- Elimino la tabella
    if Not GSCV_BDT( this, "MOD_COAN" , i_nConn )
      * --- Raise
      i_Error="Error"
      return
    endif
    * --- Ricostruisco il database per la tabella
    GSCV_BRT(this, "MOD_COAN" , i_nConn , .T.)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into MOD_COAN
    i_nConn=i_TableProp[this.MOD_COAN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOD_COAN_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.MOD_COAN_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_Msg("Ripristinati valori di origine",.T.)
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    ah_Msg("Eliminata tabella temporanea tmp",.T.)
  endproc


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella DB2
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.COD_CAB_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.COD_CAB_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
          )
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    * --- Elimino la tabella
    if Not GSCV_BDT( this, "COD_CAB" , i_nConn )
      * --- Raise
      i_Error="Error"
      return
    endif
    * --- Ricostruisco il database per la tabella
    GSCV_BRT(this, "COD_CAB" , i_nConn , .T.)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into COD_CAB
    i_nConn=i_TableProp[this.COD_CAB_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.COD_CAB_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.COD_CAB_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_Msg("Ripristinati valori di origine",.T.)
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    ah_Msg("Eliminata tabella temporanea tmp",.T.)
  endproc


  procedure Pag6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella DB2
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.CAN_TIER_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
          )
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    * --- Elimino la tabella
    if Not GSCV_BDT( this, "CAN_TIER" , i_nConn )
      * --- Raise
      i_Error="Error"
      return
    endif
    * --- Ricostruisco il database per la tabella
    GSCV_BRT(this, "CAN_TIER" , i_nConn , .T.)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into CAN_TIER
    i_nConn=i_TableProp[this.CAN_TIER_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.CAN_TIER_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_Msg("Ripristinati valori di origine",.T.)
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    ah_Msg("Eliminata tabella temporanea tmp",.T.)
  endproc


  procedure Pag7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella DB2
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.SE_INAIL_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.SE_INAIL_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
          )
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    * --- Elimino la tabella
    if Not GSCV_BDT( this, "SE_INAIL" , i_nConn )
      * --- Raise
      i_Error="Error"
      return
    endif
    * --- Ricostruisco il database per la tabella
    GSCV_BRT(this, "SE_INAIL" , i_nConn , .T.)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into SE_INAIL
    i_nConn=i_TableProp[this.SE_INAIL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SE_INAIL_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.SE_INAIL_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_Msg("Ripristinati valori di origine",.T.)
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    ah_Msg("Eliminata tabella temporanea tmp",.T.)
  endproc


  procedure Pag8
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella DB2
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.SEDIAZIE_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.SEDIAZIE_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
          )
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    * --- Elimino la tabella
    if Not GSCV_BDT( this, "SEDIAZIE" , i_nConn )
      * --- Raise
      i_Error="Error"
      return
    endif
    * --- Ricostruisco il database per la tabella
    GSCV_BRT(this, "SEDIAZIE" , i_nConn , .T.)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into SEDIAZIE
    i_nConn=i_TableProp[this.SEDIAZIE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SEDIAZIE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.SEDIAZIE_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_Msg("Ripristinati valori di origine",.T.)
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    ah_Msg("Eliminata tabella temporanea tmp",.T.)
  endproc


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,11)]
    this.cWorkTables[1]='*RIPATMP1'
    this.cWorkTables[2]='DES_DIVE'
    this.cWorkTables[3]='BAN_CHE'
    this.cWorkTables[4]='MOD_COAN'
    this.cWorkTables[5]='COD_CAB'
    this.cWorkTables[6]='CAN_TIER'
    this.cWorkTables[7]='SE_INAIL'
    this.cWorkTables[8]='SEDIAZIE'
    this.cWorkTables[9]='MAGAZZIN'
    this.cWorkTables[10]='AGENTI'
    this.cWorkTables[11]='VETTORI'
    return(this.OpenAllTables(11))

  proc CloseCursors()
    if used('_Curs_MAGAZZIN')
      use in _Curs_MAGAZZIN
    endif
    if used('_Curs_AGENTI')
      use in _Curs_AGENTI
    endif
    if used('_Curs_VETTORI')
      use in _Curs_VETTORI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
