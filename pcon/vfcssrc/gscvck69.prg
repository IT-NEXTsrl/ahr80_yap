* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvck69                                                        *
*              Selezione dei documenti da aggiornare                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_115]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-14                                                      *
* Last revis.: 2018-02-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscvck69",oParentObject))

* --- Class definition
define class tgscvck69 as StdForm
  Top    = 34
  Left   = 50

  * --- Standard Properties
  Width  = 537
  Height = 281
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2018-02-21"
  HelpContextID=48575849
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=19

  * --- Constant Properties
  _IDX = 0
  TIP_DOCU_IDX = 0
  CONTI_IDX = 0
  cPrg = "gscvck69"
  cComment = "Selezione dei documenti da aggiornare"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CICLO = space(1)
  w_CATDO1 = space(2)
  o_CATDO1 = space(2)
  w_CATEGO = space(2)
  w_TIPO = space(1)
  o_TIPO = space(1)
  w_BDATE = ctod('  /  /  ')
  o_BDATE = ctod('  /  /  ')
  w_EDATE = ctod('  /  /  ')
  w_numero = 0
  w_serie1 = space(10)
  w_numero1 = 0
  w_serie2 = space(10)
  w_FLPROV = space(1)
  w_CLIENTE = space(15)
  w_DESCRI = space(35)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_CATDOC = space(2)
  w_FLVEAC = space(1)
  w_TIPODOC = space(5)
  w_DESCRI1 = space(40)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscvck69Pag1","gscvck69",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCATDO1_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='TIP_DOCU'
    this.cWorkTables[2]='CONTI'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CICLO=space(1)
      .w_CATDO1=space(2)
      .w_CATEGO=space(2)
      .w_TIPO=space(1)
      .w_BDATE=ctod("  /  /  ")
      .w_EDATE=ctod("  /  /  ")
      .w_numero=0
      .w_serie1=space(10)
      .w_numero1=0
      .w_serie2=space(10)
      .w_FLPROV=space(1)
      .w_CLIENTE=space(15)
      .w_DESCRI=space(35)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_CATDOC=space(2)
      .w_FLVEAC=space(1)
      .w_TIPODOC=space(5)
      .w_DESCRI1=space(40)
      .w_CICLO=oParentObject.w_CICLO
      .w_CATEGO=oParentObject.w_CATEGO
      .w_TIPO=oParentObject.w_TIPO
      .w_BDATE=oParentObject.w_BDATE
      .w_EDATE=oParentObject.w_EDATE
      .w_numero=oParentObject.w_numero
      .w_serie1=oParentObject.w_serie1
      .w_numero1=oParentObject.w_numero1
      .w_serie2=oParentObject.w_serie2
      .w_FLPROV=oParentObject.w_FLPROV
      .w_CLIENTE=oParentObject.w_CLIENTE
      .w_TIPODOC=oParentObject.w_TIPODOC
        .w_CICLO = 'V'
        .w_CATDO1 = 'XX'
        .w_CATEGO = IIF(.w_CATDO1='XX', '  ', .w_CATDO1)
        .w_TIPO = 'C'
        .w_BDATE = G_INIESE
        .w_EDATE = G_FINESE
        .w_numero = 1
        .w_serie1 = ''
        .w_numero1 = 999999999999999
        .w_serie2 = ''
        .w_FLPROV = 'N'
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_CLIENTE))
          .link_1_12('Full')
        endif
          .DoRTCalc(13,13,.f.)
        .w_OBTEST = IIF(EMPTY(.w_BDATE),i_INIDAT,.w_BDATE)
        .DoRTCalc(15,18,.f.)
        if not(empty(.w_TIPODOC))
          .link_1_31('Full')
        endif
    endwith
    this.DoRTCalc(19,19,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_34.enabled = this.oPgFrm.Page1.oPag.oBtn_1_34.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_CICLO=.w_CICLO
      .oParentObject.w_CATEGO=.w_CATEGO
      .oParentObject.w_TIPO=.w_TIPO
      .oParentObject.w_BDATE=.w_BDATE
      .oParentObject.w_EDATE=.w_EDATE
      .oParentObject.w_numero=.w_numero
      .oParentObject.w_serie1=.w_serie1
      .oParentObject.w_numero1=.w_numero1
      .oParentObject.w_serie2=.w_serie2
      .oParentObject.w_FLPROV=.w_FLPROV
      .oParentObject.w_CLIENTE=.w_CLIENTE
      .oParentObject.w_TIPODOC=.w_TIPODOC
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
            .w_CATEGO = IIF(.w_CATDO1='XX', '  ', .w_CATDO1)
            .w_TIPO = 'C'
        .DoRTCalc(5,11,.t.)
        if .o_TIPO<>.w_TIPO
            .w_CLIENTE = SPACE(15)
          .link_1_12('Full')
        endif
        .DoRTCalc(13,13,.t.)
        if .o_BDATE<>.w_BDATE
            .w_OBTEST = IIF(EMPTY(.w_BDATE),i_INIDAT,.w_BDATE)
        endif
        .DoRTCalc(15,17,.t.)
        if .o_CATDO1<>.w_CATDO1
            .w_TIPODOC = SPACE(5)
          .link_1_31('Full')
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(19,19,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCLIENTE_1_12.enabled = this.oPgFrm.Page1.oPag.oCLIENTE_1_12.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCATDO1_1_2.visible=!this.oPgFrm.Page1.oPag.oCATDO1_1_2.mHide()
    this.oPgFrm.Page1.oPag.oCLIENTE_1_12.visible=!this.oPgFrm.Page1.oPag.oCLIENTE_1_12.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_22.visible=!this.oPgFrm.Page1.oPag.oStr_1_22.mHide()
    this.oPgFrm.Page1.oPag.oDESCRI_1_23.visible=!this.oPgFrm.Page1.oPag.oDESCRI_1_23.mHide()
    this.oPgFrm.Page1.oPag.oTIPODOC_1_31.visible=!this.oPgFrm.Page1.oPag.oTIPODOC_1_31.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CLIENTE
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CLIENTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CLIENTE)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_tipo);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_tipo;
                     ,'ANCODICE',trim(this.w_CLIENTE))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CLIENTE)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CLIENTE)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_tipo);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CLIENTE)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_tipo);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CLIENTE) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCLIENTE_1_12'),i_cWhere,'GSAR_BZC',"Elenco clienti/fornitori",'GSCG1MPN.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_tipo<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice intestatario inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_tipo);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CLIENTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CLIENTE);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_tipo);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_tipo;
                       ,'ANCODICE',this.w_CLIENTE)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CLIENTE = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCRI1 = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CLIENTE = space(15)
      endif
      this.w_DESCRI1 = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice intestatario inesistente oppure obsoleto")
        endif
        this.w_CLIENTE = space(15)
        this.w_DESCRI1 = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CLIENTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TIPODOC
  func Link_1_31(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPODOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVE_ATD',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_TIPODOC)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_TIPODOC))
          select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TIPODOC)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TIPODOC) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oTIPODOC_1_31'),i_cWhere,'GSVE_ATD',"Causali documenti",'GSCVCK69.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPODOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_TIPODOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_TIPODOC)
            select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPODOC = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESCRI = NVL(_Link_.TDDESDOC,space(35))
      this.w_CATDOC = NVL(_Link_.TDCATDOC,space(2))
      this.w_FLVEAC = NVL(_Link_.TDFLVEAC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_TIPODOC = space(5)
      endif
      this.w_DESCRI = space(35)
      this.w_CATDOC = space(2)
      this.w_FLVEAC = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_TIPODOC) OR ((g_ACQU<>'S' OR .w_FLVEAC='V') AND .w_CATDOC$'FA-NC')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_TIPODOC = space(5)
        this.w_DESCRI = space(35)
        this.w_CATDOC = space(2)
        this.w_FLVEAC = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPODOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCATDO1_1_2.RadioValue()==this.w_CATDO1)
      this.oPgFrm.Page1.oPag.oCATDO1_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oBDATE_1_5.value==this.w_BDATE)
      this.oPgFrm.Page1.oPag.oBDATE_1_5.value=this.w_BDATE
    endif
    if not(this.oPgFrm.Page1.oPag.oEDATE_1_6.value==this.w_EDATE)
      this.oPgFrm.Page1.oPag.oEDATE_1_6.value=this.w_EDATE
    endif
    if not(this.oPgFrm.Page1.oPag.onumero_1_7.value==this.w_numero)
      this.oPgFrm.Page1.oPag.onumero_1_7.value=this.w_numero
    endif
    if not(this.oPgFrm.Page1.oPag.oserie1_1_8.value==this.w_serie1)
      this.oPgFrm.Page1.oPag.oserie1_1_8.value=this.w_serie1
    endif
    if not(this.oPgFrm.Page1.oPag.onumero1_1_9.value==this.w_numero1)
      this.oPgFrm.Page1.oPag.onumero1_1_9.value=this.w_numero1
    endif
    if not(this.oPgFrm.Page1.oPag.oserie2_1_10.value==this.w_serie2)
      this.oPgFrm.Page1.oPag.oserie2_1_10.value=this.w_serie2
    endif
    if not(this.oPgFrm.Page1.oPag.oFLPROV_1_11.RadioValue()==this.w_FLPROV)
      this.oPgFrm.Page1.oPag.oFLPROV_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCLIENTE_1_12.value==this.w_CLIENTE)
      this.oPgFrm.Page1.oPag.oCLIENTE_1_12.value=this.w_CLIENTE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_23.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_23.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPODOC_1_31.value==this.w_TIPODOC)
      this.oPgFrm.Page1.oPag.oTIPODOC_1_31.value=this.w_TIPODOC
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI1_1_32.value==this.w_DESCRI1)
      this.oPgFrm.Page1.oPag.oDESCRI1_1_32.value=this.w_DESCRI1
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_BDATE)) or not((empty(.w_edate)) OR (.w_edate>=.w_bdate)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oBDATE_1_5.SetFocus()
            i_bnoObbl = !empty(.w_BDATE)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data finale � minore della data iniziale")
          case   ((empty(.w_EDATE)) or not((.w_EDATE>=.w_BDATE) OR  ( empty(.w_bdate))))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oEDATE_1_6.SetFocus()
            i_bnoObbl = !empty(.w_EDATE)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data finale � minore della data iniziale")
          case   not((empty(.w_NUMERO1)) OR  (.w_NUMERO<=.w_NUMERO1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.onumero_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il numero documento iniziale � pi� grande del codice finale")
          case   not((empty(.w_SERIE2)) OR  (.w_SERIE1<=.w_SERIE2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oserie1_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La serie iniziale � pi� grande della serie finale")
          case   not((.w_NUMERO1>=.w_NUMERO) or (empty(.w_NUMERO1)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.onumero1_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il numero documento iniziale � pi� grande del codice finale")
          case   not((.w_serie2>=.w_serie1) or (empty(.w_serie1)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oserie2_1_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La serie iniziale � maggire della serie finale")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(.w_CICLO='A')  and (!empty(.w_CICLO))  and not(empty(.w_CLIENTE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCLIENTE_1_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice intestatario inesistente oppure obsoleto")
          case   not(EMPTY(.w_TIPODOC) OR ((g_ACQU<>'S' OR .w_FLVEAC='V') AND .w_CATDOC$'FA-NC'))  and not(.w_CATDO1='XX')  and not(empty(.w_TIPODOC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTIPODOC_1_31.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CATDO1 = this.w_CATDO1
    this.o_TIPO = this.w_TIPO
    this.o_BDATE = this.w_BDATE
    return

enddefine

* --- Define pages as container
define class tgscvck69Pag1 as StdContainer
  Width  = 533
  height = 281
  stdWidth  = 533
  stdheight = 281
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oCATDO1_1_2 as StdCombo with uid="QDJUHAEQBR",rtseq=2,rtrep=.f.,left=106,top=9,width=162,height=21;
    , ToolTipText = "Categoria documento di selezione";
    , HelpContextID = 55857190;
    , cFormVar="w_CATDO1",RowSource=""+"Fatture e Note di credito,"+"Fatture,"+"Note di credito", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCATDO1_1_2.RadioValue()
    return(iif(this.value =1,'XX',;
    iif(this.value =2,'FA',;
    iif(this.value =3,'NC',;
    space(2)))))
  endfunc
  func oCATDO1_1_2.GetRadio()
    this.Parent.oContained.w_CATDO1 = this.RadioValue()
    return .t.
  endfunc

  func oCATDO1_1_2.SetRadio()
    this.Parent.oContained.w_CATDO1=trim(this.Parent.oContained.w_CATDO1)
    this.value = ;
      iif(this.Parent.oContained.w_CATDO1=='XX',1,;
      iif(this.Parent.oContained.w_CATDO1=='FA',2,;
      iif(this.Parent.oContained.w_CATDO1=='NC',3,;
      0)))
  endfunc

  func oCATDO1_1_2.mHide()
    with this.Parent.oContained
      return (.w_CICLO='A')
    endwith
  endfunc

  add object oBDATE_1_5 as StdField with uid="JWHNCRKDRD",rtseq=5,rtrep=.f.,;
    cFormVar = "w_BDATE", cQueryName = "BDATE",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data finale � minore della data iniziale",;
    ToolTipText = "Data documento di inizio stampa",;
    HelpContextID = 238869738,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=75, Top=110

  func oBDATE_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((empty(.w_edate)) OR (.w_edate>=.w_bdate))
    endwith
    return bRes
  endfunc

  add object oEDATE_1_6 as StdField with uid="OWEPFQDEHL",rtseq=6,rtrep=.f.,;
    cFormVar = "w_EDATE", cQueryName = "EDATE",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data finale � minore della data iniziale",;
    ToolTipText = "Data documento di fine stampa",;
    HelpContextID = 238869690,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=75, Top=136

  func oEDATE_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_EDATE>=.w_BDATE) OR  ( empty(.w_bdate)))
    endwith
    return bRes
  endfunc

  add object onumero_1_7 as StdField with uid="BXBQYPGVTU",rtseq=7,rtrep=.f.,;
    cFormVar = "w_numero", cQueryName = "numero",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il numero documento iniziale � pi� grande del codice finale",;
    ToolTipText = "Numero del documento iniziale selezionato",;
    HelpContextID = 61282006,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=297, Top=110, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  func onumero_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((empty(.w_NUMERO1)) OR  (.w_NUMERO<=.w_NUMERO1))
    endwith
    return bRes
  endfunc

  add object oserie1_1_8 as StdField with uid="HVPVSEHHGU",rtseq=8,rtrep=.f.,;
    cFormVar = "w_serie1", cQueryName = "serie1",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "La serie iniziale � pi� grande della serie finale",;
    ToolTipText = "Alfa del documento iniziale selezionato",;
    HelpContextID = 81483558,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=426, Top=110, cSayPict='"!!!!!!!!!!"', cGetPict='"!!!!!!!!!!"', InputMask=replicate('X',10)

  func oserie1_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((empty(.w_SERIE2)) OR  (.w_SERIE1<=.w_SERIE2))
    endwith
    return bRes
  endfunc

  add object onumero1_1_9 as StdField with uid="PROBAWFVAK",rtseq=9,rtrep=.f.,;
    cFormVar = "w_numero1", cQueryName = "numero1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il numero documento iniziale � pi� grande del codice finale",;
    ToolTipText = "Numero del documento finale selezionato",;
    HelpContextID = 61282006,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=297, Top=136, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  func onumero1_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_NUMERO1>=.w_NUMERO) or (empty(.w_NUMERO1)))
    endwith
    return bRes
  endfunc

  add object oserie2_1_10 as StdField with uid="LHPNDMBVHJ",rtseq=10,rtrep=.f.,;
    cFormVar = "w_serie2", cQueryName = "serie2",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "La serie iniziale � maggire della serie finale",;
    ToolTipText = "Alfa del documento finale selezionato",;
    HelpContextID = 98260774,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=426, Top=136, cSayPict='"!!!!!!!!!!"', cGetPict='"!!!!!!!!!!"', InputMask=replicate('X',10)

  func oserie2_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_serie2>=.w_serie1) or (empty(.w_serie1)))
    endwith
    return bRes
  endfunc


  add object oFLPROV_1_11 as StdCombo with uid="QMVLQXRXPN",value=3,rtseq=11,rtrep=.f.,left=406,top=168,width=103,height=21;
    , ToolTipText = "Permette di selezionare tutti i documenti, solo quelli provvisori o confermati";
    , HelpContextID = 140647254;
    , cFormVar="w_FLPROV",RowSource=""+"Confermati,"+"Provvisori,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLPROV_1_11.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'',;
    space(1)))))
  endfunc
  func oFLPROV_1_11.GetRadio()
    this.Parent.oContained.w_FLPROV = this.RadioValue()
    return .t.
  endfunc

  func oFLPROV_1_11.SetRadio()
    this.Parent.oContained.w_FLPROV=trim(this.Parent.oContained.w_FLPROV)
    this.value = ;
      iif(this.Parent.oContained.w_FLPROV=='N',1,;
      iif(this.Parent.oContained.w_FLPROV=='S',2,;
      iif(this.Parent.oContained.w_FLPROV=='',3,;
      0)))
  endfunc

  add object oCLIENTE_1_12 as StdField with uid="DNOSNHKUKW",rtseq=12,rtrep=.f.,;
    cFormVar = "w_CLIENTE", cQueryName = "CLIENTE",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice intestatario inesistente oppure obsoleto",;
    ToolTipText = "Codice intestatario di selezione",;
    HelpContextID = 105163558,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=75, Top=200, cSayPict="p_CLF", cGetPict="p_CLF", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_tipo", oKey_2_1="ANCODICE", oKey_2_2="this.w_CLIENTE"

  func oCLIENTE_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_CICLO))
    endwith
   endif
  endfunc

  func oCLIENTE_1_12.mHide()
    with this.Parent.oContained
      return (.w_CICLO='A')
    endwith
  endfunc

  func oCLIENTE_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oCLIENTE_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCLIENTE_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_tipo)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_tipo)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCLIENTE_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Elenco clienti/fornitori",'GSCG1MPN.CONTI_VZM',this.parent.oContained
  endproc
  proc oCLIENTE_1_12.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_tipo
     i_obj.w_ANCODICE=this.parent.oContained.w_CLIENTE
     i_obj.ecpSave()
  endproc


  add object oBtn_1_13 as StdButton with uid="ZNREPGHWCX",left=426, top=232, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , HelpContextID = 48555290;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_13.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_14 as StdButton with uid="XYGIHPUFHG",left=479, top=232, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 5709062;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_14.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESCRI_1_23 as StdField with uid="RLYFINUUXX",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 193152054,;
   bGlobalFont=.t.,;
    Height=21, Width=272, Left=178, Top=42, InputMask=replicate('X',35)

  func oDESCRI_1_23.mHide()
    with this.Parent.oContained
      return (.w_CATDO1='XX')
    endwith
  endfunc

  add object oTIPODOC_1_31 as StdField with uid="HRNQOHNFNU",rtseq=18,rtrep=.f.,;
    cFormVar = "w_TIPODOC", cQueryName = "TIPODOC",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Tipo documento selezionato",;
    HelpContextID = 11475254,;
   bGlobalFont=.t.,;
    Height=21, Width=64, Left=106, Top=42, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSVE_ATD", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_TIPODOC"

  func oTIPODOC_1_31.mHide()
    with this.Parent.oContained
      return (.w_CATDO1='XX')
    endwith
  endfunc

  func oTIPODOC_1_31.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_31('Part',this)
    endwith
    return bRes
  endfunc

  proc oTIPODOC_1_31.ecpDrop(oSource)
    this.Parent.oContained.link_1_31('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTIPODOC_1_31.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oTIPODOC_1_31'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVE_ATD',"Causali documenti",'GSCVCK69.TIP_DOCU_VZM',this.parent.oContained
  endproc
  proc oTIPODOC_1_31.mZoomOnZoom
    local i_obj
    i_obj=GSVE_ATD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TDTIPDOC=this.parent.oContained.w_TIPODOC
     i_obj.ecpSave()
  endproc

  add object oDESCRI1_1_32 as StdField with uid="JOIXSKKUCG",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DESCRI1", cQueryName = "DESCRI1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 193152054,;
   bGlobalFont=.t.,;
    Height=21, Width=272, Left=213, Top=200, InputMask=replicate('X',40)


  add object oBtn_1_34 as StdButton with uid="WXXTVFWWXV",left=373, top=232, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per stampare l'elenco dei documenti che verranno modificati per le selezioni effettuate";
    , HelpContextID = 93213734;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_34.Click()
      vx_exec("GSCVCC69.VQR,QUERY\GSCVCC69.FRX",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_15 as StdString with uid="NXSDBFWLJV",Visible=.t., Left=12, Top=112,;
    Alignment=1, Width=61, Height=15,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="LJKHLIFCHP",Visible=.t., Left=26, Top=138,;
    Alignment=1, Width=47, Height=15,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="NCIHTWZTBT",Visible=.t., Left=212, Top=110,;
    Alignment=1, Width=83, Height=15,;
    Caption="Da numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="MQFLLNLPCM",Visible=.t., Left=226, Top=136,;
    Alignment=1, Width=69, Height=15,;
    Caption="A numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="OOAUBIWOYB",Visible=.t., Left=11, Top=9,;
    Alignment=1, Width=92, Height=15,;
    Caption="Categoria:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="HDIGEMQYKL",Visible=.t., Left=13, Top=80,;
    Alignment=0, Width=385, Height=15,;
    Caption="Selezione documenti"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="XSZYPLRAJY",Visible=.t., Left=6, Top=44,;
    Alignment=1, Width=97, Height=15,;
    Caption="Tipo documento:"  ;
  , bGlobalFont=.t.

  func oStr_1_22.mHide()
    with this.Parent.oContained
      return (.w_CATDO1='XX')
    endwith
  endfunc

  add object oStr_1_26 as StdString with uid="LVVBINNLET",Visible=.t., Left=414, Top=110,;
    Alignment=2, Width=12, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="PKVCFMTCZS",Visible=.t., Left=414, Top=136,;
    Alignment=2, Width=12, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="DMPRCDDUMT",Visible=.t., Left=335, Top=168,;
    Alignment=1, Width=67, Height=18,;
    Caption="Stato doc:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="JCXYFOOVAI",Visible=.t., Left=6, Top=200,;
    Alignment=1, Width=67, Height=18,;
    Caption="Cliente:"  ;
  , bGlobalFont=.t.

  add object oBox_1_19 as StdBox with uid="YDFMXVPMAP",left=4, top=96, width=519,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscvck69','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
