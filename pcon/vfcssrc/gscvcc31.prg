* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcc31                                                        *
*              inserimento tabella UTEMAILS e ACC_MAIL                         *
*                                                                              *
*      Author: Zucchetti s.p.a.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][4]                                                       *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2013-05-09                                                      *
* Last revis.: 2015-03-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcc31",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcc31 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TMPC = space(10)
  w_TEMP = space(10)
  w_NUMREC = 0
  w_CPROWORD = 0
  w_CODICE = 0
  w_MAIL = space(254)
  w_AM_INVIO = space(1)
  w_FLGDEF = space(1)
  w_AMSERVER = space(254)
  w_AM_PORTA = 0
  w_AMAUTSRV = space(1)
  w_UMFLSYNC = space(1)
  w_UMSYNCTO = 0
  w_CompanyList = space(10)
  w_COCODREL = space(10)
  w_AMSERIAL = space(5)
  w_AMCCNEML = space(254)
  w_AM_FIRMA = space(0)
  w_AMCC_EML = space(254)
  w_AMUTENTE = 0
  w_AMSHARED = space(1)
  w_AM_LOGIN = space(254)
  w_AMPASSWD = space(254)
  w_LPLOGIN = space(254)
  w_LPPSW = space(254)
  * --- WorkFile variables
  UTE_NTI_idx=0
  UTEMAILS_idx=0
  AUT_LPE_idx=0
  AZIENDA_idx=0
  ACC_MAIL_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- inserimento tabella UTEMAILS e ACC_MAIL
    * --- FIle di LOG
    * --- Questa procedura di conversione deve essere eseguita una sola volta per tutte le aziende
    this.w_CompanyList = ""
    * --- Select from AZIENDA
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select AZCODAZI  from "+i_cTable+" AZIENDA ";
           ,"_Curs_AZIENDA")
    else
      select AZCODAZI from (i_cTable);
        into cursor _Curs_AZIENDA
    endif
    if used('_Curs_AZIENDA')
      select _Curs_AZIENDA
      locate for 1=1
      do while not(eof())
      this.w_CompanyList = this.w_CompanyList + ALLTRIM(_Curs_AZIENDA.AZCODAZI)+ ","
        select _Curs_AZIENDA
        continue
      enddo
      use
    endif
    this.w_CompanyList = Left(this.w_CompanyList, Len(this.w_CompanyList)-1)
    * --- Select from gscvcc31
    do vq_exec with 'gscvcc31',this,'_Curs_gscvcc31','',.f.,.t.
    if used('_Curs_gscvcc31')
      select _Curs_gscvcc31
      locate for 1=1
      do while not(eof())
      this.w_COCODREL = _Curs_gscvcc31.COCODREL
        select _Curs_gscvcc31
        continue
      enddo
      use
    endif
    if empty(this.w_COCODREL)
      * --- Try
      local bErr_01A413C8
      bErr_01A413C8=bTrsErr
      this.Try_01A413C8()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_01A413C8
      * --- End
      * --- Try
      local bErr_01A42628
      bErr_01A42628=bTrsErr
      this.Try_01A42628()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        * --- Gestisce log errori
        this.oParentObject.w_PMSG = Message()
        this.oParentObject.w_PESEOK = .F.
        if this.w_NHF>=0
          this.w_TMPC = AH_MsgFormat("ERRORE GENERICO - %1", this.oParentObject.w_PMSG)
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
      bTrsErr=bTrsErr or bErr_01A42628
      * --- End
    else
      this.oParentObject.w_PESEOK = .T.
      this.oParentObject.w_PMSG = AH_MsgFormat("Conversione eseguita correttamente")
      if this.w_NHF>=0
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.oParentObject.w_PMSG)
      endif
    endif
  endproc
  proc Try_01A413C8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    i_nConn=i_TableProp[this.UTE_NTI_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.UTE_NTI_idx,2])
    if i_nConn<>0 And upper(CP_DBTYPE)="SQLSERVER"
      SqlColumns(1,i_cTable,"NATIVE","Stru_Utenti")
      Select Stru_Utenti
      Locate for Upper(Column_Name)="UTCODICE"
      if Found() And Upper(Type_Name)="SMALLINT"
        w_iRows = SQLExec(i_nConn,"ALTER TABLE "+i_cTable+" DROP CONSTRAINT PK_UTE_NTI")
        w_iRows = SQLExec(i_nConn,"ALTER TABLE "+i_cTable+" ALTER COLUMN UTCODICE int NOT NULL")
        w_iRows = SQLExec(i_nConn,"ALTER TABLE "+i_cTable+" ADD CONSTRAINT pk_UTE_NTI primary key(UTCODICE)")
        Gscv_Brt(this, "UTEMAILS" , i_nConn , .T.)
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    Use in Select ("Stru_Utenti")
    return
  proc Try_01A42628()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Select from UTE_NTI
    i_nConn=i_TableProp[this.UTE_NTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.UTE_NTI_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" UTE_NTI ";
           ,"_Curs_UTE_NTI")
    else
      select * from (i_cTable);
        into cursor _Curs_UTE_NTI
    endif
    if used('_Curs_UTE_NTI')
      select _Curs_UTE_NTI
      locate for 1=1
      do while not(eof())
      * --- Controllo se l'indirizzo mail dell'utente � gi� stato riportato dall'esecuzione della procedura da un'altra azienda
      this.w_CODICE = _Curs_UTE_NTI.UTCODICE
      this.w_AM_INVIO = NVL(_Curs_UTE_NTI.UT_INVIO,"")
      this.w_AMSHARED = "N"
      if Not EMPTY(this.w_AM_INVIO)
        this.w_MAIL = ALLTRIM(NVL(_Curs_UTE_NTI.UTMITTEN,""))
        this.w_AMSERVER = ALLTRIM(NVL(_Curs_UTE_NTI.UTSERVER," "))
        this.w_AM_PORTA = _Curs_UTE_NTI.UT_PORTA
        this.w_AMAUTSRV = NVL(_Curs_UTE_NTI.UTAUTSRV,"N")
        this.w_UMFLSYNC = NVL(_Curs_UTE_NTI.UTFLSYNC,"N")
        this.w_UMSYNCTO = NVL(_Curs_UTE_NTI.UTSYNCTO,"N")
        this.w_AMSERIAL = space(5)
        if this.w_AM_INVIO $ "MO"
          * --- MAPI o OUTLOOK
          this.w_AMUTENTE = this.w_CODICE
          this.w_MAIL = ah_msgformat("Account %1 - (Utente %2)", IIF(this.w_AM_INVIO="M", "MAPI", "Outlook"), alltrim(str(this.w_AMUTENTE,5,0)))
          this.w_AMAUTSRV = "N"
          this.w_AM_LOGIN = ""
          this.w_AMPASSWD = ""
          * --- Setto AMSHARED='S' cos� da leggere i dati mail dall'anagrafica Mail utente (CC, CCN e firma)
          this.w_AMSHARED = "S"
          this.w_AM_FIRMA = ""
          this.w_AMCC_EML = ""
          this.w_AMCCNEML = ""
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          this.w_AMUTENTE = 0
          if Empty(this.w_MAIL)
            * --- Errore: nessun indirizzo mail indicato
            this.w_NUMREC = -1
          else
            * --- Leggo login e pwd
            * --- Read from AUT_LPE
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.AUT_LPE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.AUT_LPE_idx,2])
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "LPLOGIN,LPPSW"+;
                " from "+i_cTable+" AUT_LPE where ";
                    +"LPCODUTE = "+cp_ToStrODBC(this.w_CODICE);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                LPLOGIN,LPPSW;
                from (i_cTable) where;
                    LPCODUTE = this.w_CODICE;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_LPLOGIN = NVL(cp_ToDate(_read_.LPLOGIN),cp_NullValue(_read_.LPLOGIN))
              this.w_LPPSW = NVL(cp_ToDate(_read_.LPPSW),cp_NullValue(_read_.LPPSW))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.w_AMAUTSRV = iif(empty(nvl(this.w_LPPSW, "")), "N", "S")
            * --- Read from ACC_MAIL
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.ACC_MAIL_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ACC_MAIL_idx,2])
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "AMSERIAL,AMSERVER,AM_PORTA,AMAUTSRV,AMCC_EML,AMCCNEML,AM_FIRMA,AM_LOGIN,AMPASSWD"+;
                " from "+i_cTable+" ACC_MAIL where ";
                    +"AM_EMAIL = "+cp_ToStrODBC(this.w_MAIL);
                    +" and AM_INVIO = "+cp_ToStrODBC(this.w_AM_INVIO);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                AMSERIAL,AMSERVER,AM_PORTA,AMAUTSRV,AMCC_EML,AMCCNEML,AM_FIRMA,AM_LOGIN,AMPASSWD;
                from (i_cTable) where;
                    AM_EMAIL = this.w_MAIL;
                    and AM_INVIO = this.w_AM_INVIO;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_AMSERIAL = NVL(cp_ToDate(_read_.AMSERIAL),cp_NullValue(_read_.AMSERIAL))
              this.w_AMSERVER = NVL(cp_ToDate(_read_.AMSERVER),cp_NullValue(_read_.AMSERVER))
              this.w_AM_PORTA = NVL(cp_ToDate(_read_.AM_PORTA),cp_NullValue(_read_.AM_PORTA))
              this.w_AMAUTSRV = NVL(cp_ToDate(_read_.AMAUTSRV),cp_NullValue(_read_.AMAUTSRV))
              this.w_AMCC_EML = NVL(cp_ToDate(_read_.AMCC_EML),cp_NullValue(_read_.AMCC_EML))
              this.w_AMCCNEML = NVL(cp_ToDate(_read_.AMCCNEML),cp_NullValue(_read_.AMCCNEML))
              this.w_AM_FIRMA = NVL(cp_ToDate(_read_.AM_FIRMA),cp_NullValue(_read_.AM_FIRMA))
              this.w_AM_LOGIN = NVL(cp_ToDate(_read_.AM_LOGIN),cp_NullValue(_read_.AM_LOGIN))
              this.w_AMPASSWD = NVL(cp_ToDate(_read_.AMPASSWD),cp_NullValue(_read_.AMPASSWD))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if empty(this.w_AMSERIAL)
              * --- Nuovo account mail
              this.w_AMSERVER = ALLTRIM(NVL(_Curs_UTE_NTI.UTSERVER," "))
              this.w_AM_PORTA = _Curs_UTE_NTI.UT_PORTA
              this.w_AMAUTSRV = NVL(_Curs_UTE_NTI.UTAUTSRV,"S")
              this.w_AM_FIRMA = _Curs_UTE_NTI.UT_FIRMA
              this.w_AMCC_EML = _Curs_UTE_NTI.UTCC_EML
              this.w_AMCCNEML = _Curs_UTE_NTI.UTCCNEML
              this.w_AM_LOGIN = this.w_LPLOGIN
              this.w_AMPASSWD = this.w_LPPSW
              this.Pag2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            else
              * --- Account mail comune a pi� utenti
              * --- Verifico le credenziali di accesso
              if alltrim(this.w_AM_LOGIN)<>alltrim(this.w_LPLOGIN) OR alltrim(this.w_AMPASSWD)<>alltrim(this.w_LPPSW)
                if this.w_AM_LOGIN<>"impostare login e password"
                  if this.w_NHF>=0
                    this.w_TMPC = AH_MsgFormat("Credenziali di accesso dell'account mail %1 non coerenti tra tutti gli utenti: impostare login e password manualmente", this.w_MAIL)
                    this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
                  endif
                  this.w_AM_LOGIN = "impostare login e password"
                  this.w_AMPASSWD = " "
                endif
              endif
              * --- Setto il flag account condiviso
              * --- Write into ACC_MAIL
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.ACC_MAIL_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ACC_MAIL_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.ACC_MAIL_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"AMCC_EML ="+cp_NullLink(cp_ToStrODBC(" "),'ACC_MAIL','AMCC_EML');
                +",AMCCNEML ="+cp_NullLink(cp_ToStrODBC(" "),'ACC_MAIL','AMCCNEML');
                +",AM_FIRMA ="+cp_NullLink(cp_ToStrODBC(" "),'ACC_MAIL','AM_FIRMA');
                +",AMSHARED ="+cp_NullLink(cp_ToStrODBC("S"),'ACC_MAIL','AMSHARED');
                +",AM_LOGIN ="+cp_NullLink(cp_ToStrODBC(this.w_AM_LOGIN),'ACC_MAIL','AM_LOGIN');
                +",AMPASSWD ="+cp_NullLink(cp_ToStrODBC(this.w_AMPASSWD),'ACC_MAIL','AMPASSWD');
                    +i_ccchkf ;
                +" where ";
                    +"AMSERIAL = "+cp_ToStrODBC(this.w_AMSERIAL);
                       )
              else
                update (i_cTable) set;
                    AMCC_EML = " ";
                    ,AMCCNEML = " ";
                    ,AM_FIRMA = " ";
                    ,AMSHARED = "S";
                    ,AM_LOGIN = this.w_AM_LOGIN;
                    ,AMPASSWD = this.w_AMPASSWD;
                    &i_ccchkf. ;
                 where;
                    AMSERIAL = this.w_AMSERIAL;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
          endif
        endif
        if NOT empty(this.w_AMSERIAL)
          this.w_NUMREC = 0
          this.w_CPROWORD = 0
          * --- Select from UTEMAILS
          i_nConn=i_TableProp[this.UTEMAILS_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.UTEMAILS_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select count(*) as NUMROW, max(CPROWORD) as CPROWORD  from "+i_cTable+" UTEMAILS ";
                +" where UMCODUTE="+cp_ToStrODBC(this.w_CODICE)+"";
                 ,"_Curs_UTEMAILS")
          else
            select count(*) as NUMROW, max(CPROWORD) as CPROWORD from (i_cTable);
             where UMCODUTE=this.w_CODICE;
              into cursor _Curs_UTEMAILS
          endif
          if used('_Curs_UTEMAILS')
            select _Curs_UTEMAILS
            locate for 1=1
            do while not(eof())
            this.w_NUMREC = NUMROW
            this.w_CPROWORD = nvl(_Curs_UTEMAILS.CPROWORD,0)
              select _Curs_UTEMAILS
              continue
            enddo
            use
          endif
          this.w_CPROWORD = this.w_CPROWORD+10
          if this.w_NUMREC = 0
            this.w_FLGDEF = "S"
            * --- Insert into UTEMAILS
            i_nConn=i_TableProp[this.UTEMAILS_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.UTEMAILS_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.UTEMAILS_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"UMCODUTE"+",CPROWORD"+",UMCODAZI"+",UMSERIAL"+",UMFLGDEF"+",UMFLSYNC"+",UMSYNCTO"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(_Curs_UTE_NTI.UTCODICE),'UTEMAILS','UMCODUTE');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'UTEMAILS','CPROWORD');
              +","+cp_NullLink(cp_ToStrODBC("     "),'UTEMAILS','UMCODAZI');
              +","+cp_NullLink(cp_ToStrODBC(this.w_AMSERIAL),'UTEMAILS','UMSERIAL');
              +","+cp_NullLink(cp_ToStrODBC(this.w_FLGDEF),'UTEMAILS','UMFLGDEF');
              +","+cp_NullLink(cp_ToStrODBC(this.w_UMFLSYNC),'UTEMAILS','UMFLSYNC');
              +","+cp_NullLink(cp_ToStrODBC(this.w_UMSYNCTO),'UTEMAILS','UMSYNCTO');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'UMCODUTE',_Curs_UTE_NTI.UTCODICE,'CPROWORD',this.w_CPROWORD,'UMCODAZI',"     ",'UMSERIAL',this.w_AMSERIAL,'UMFLGDEF',this.w_FLGDEF,'UMFLSYNC',this.w_UMFLSYNC,'UMSYNCTO',this.w_UMSYNCTO)
              insert into (i_cTable) (UMCODUTE,CPROWORD,UMCODAZI,UMSERIAL,UMFLGDEF,UMFLSYNC,UMSYNCTO &i_ccchkf. );
                 values (;
                   _Curs_UTE_NTI.UTCODICE;
                   ,this.w_CPROWORD;
                   ,"     ";
                   ,this.w_AMSERIAL;
                   ,this.w_FLGDEF;
                   ,this.w_UMFLSYNC;
                   ,this.w_UMSYNCTO;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          endif
        endif
      else
        this.w_NUMREC = -1
      endif
      if bTrsErr
        * --- Raise
        i_Error=AH_MsgFormat("Impossibile aggiornare i record nella tabella %1", "UTEMAILS")
        return
      else
        if this.w_NHF>=0
          do case
            case this.w_NUMREC = -1
              this.w_TMPC = AH_MsgFormat("Tabella %1 non aggiornata per utente %2: nessuna mail configurata per tale utente nella tabella %3.", "UTEMAILS",this.w_CODICE, "UTE_NTI")
            case this.w_NUMREC = 0
              this.w_TMPC = AH_MsgFormat("Tabella %1 aggiornata per utente %2, inserito %3 come account %4", "UTEMAILS",this.w_CODICE, this.w_MAIL, IIF(this.w_FLGDEF="S","predefinito.","non predefinito."))
            otherwise
              this.w_TMPC = AH_MsgFormat("Tabella %1 non aggiornata per utente %2: %3 era gi� stato inserito in precedenza.", "UTEMAILS",this.w_CODICE, this.w_MAIL)
          endcase
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
        select _Curs_UTE_NTI
        continue
      enddo
      use
    endif
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = AH_MsgFormat("Conversione eseguita correttamente")
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Creo account
    this.w_AMSERIAL = space(5)
    this.w_AMSERIAL = cp_getprog("ACC_MAIL", "SEACM", this.w_AMSERIAL)
    * --- Try
    local bErr_02CD7C60
    bErr_02CD7C60=bTrsErr
    this.Try_02CD7C60()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
      if this.w_NHF>=0
        this.w_TMPC = AH_MsgFormat("Errore creazione account mail %1%0%2%0", this.w_MAIL,message())
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
      this.w_AMSERIAL = space(5)
      this.w_NUMREC = -1
    endif
    bTrsErr=bTrsErr or bErr_02CD7C60
    * --- End
  endproc
  proc Try_02CD7C60()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into ACC_MAIL
    i_nConn=i_TableProp[this.ACC_MAIL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ACC_MAIL_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ACC_MAIL_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"AMSERIAL"+",AM_EMAIL"+",AM_INVIO"+",AMSERVER"+",AM_PORTA"+",AMAUTSRV"+",AMCC_EML"+",AMCCNEML"+",AM_FIRMA"+",AMUTENTE"+",AMSHARED"+",AM_LOGIN"+",AMPASSWD"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_AMSERIAL),'ACC_MAIL','AMSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MAIL),'ACC_MAIL','AM_EMAIL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_AM_INVIO),'ACC_MAIL','AM_INVIO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_AMSERVER),'ACC_MAIL','AMSERVER');
      +","+cp_NullLink(cp_ToStrODBC(this.w_AM_PORTA),'ACC_MAIL','AM_PORTA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_AMAUTSRV),'ACC_MAIL','AMAUTSRV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_AMCC_EML),'ACC_MAIL','AMCC_EML');
      +","+cp_NullLink(cp_ToStrODBC(this.w_AMCCNEML),'ACC_MAIL','AMCCNEML');
      +","+cp_NullLink(cp_ToStrODBC(this.w_AM_FIRMA),'ACC_MAIL','AM_FIRMA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_AMUTENTE),'ACC_MAIL','AMUTENTE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_AMSHARED),'ACC_MAIL','AMSHARED');
      +","+cp_NullLink(cp_ToStrODBC(this.w_AM_LOGIN),'ACC_MAIL','AM_LOGIN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_AMPASSWD),'ACC_MAIL','AMPASSWD');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'AMSERIAL',this.w_AMSERIAL,'AM_EMAIL',this.w_MAIL,'AM_INVIO',this.w_AM_INVIO,'AMSERVER',this.w_AMSERVER,'AM_PORTA',this.w_AM_PORTA,'AMAUTSRV',this.w_AMAUTSRV,'AMCC_EML',this.w_AMCC_EML,'AMCCNEML',this.w_AMCCNEML,'AM_FIRMA',this.w_AM_FIRMA,'AMUTENTE',this.w_AMUTENTE,'AMSHARED',this.w_AMSHARED,'AM_LOGIN',this.w_AM_LOGIN)
      insert into (i_cTable) (AMSERIAL,AM_EMAIL,AM_INVIO,AMSERVER,AM_PORTA,AMAUTSRV,AMCC_EML,AMCCNEML,AM_FIRMA,AMUTENTE,AMSHARED,AM_LOGIN,AMPASSWD &i_ccchkf. );
         values (;
           this.w_AMSERIAL;
           ,this.w_MAIL;
           ,this.w_AM_INVIO;
           ,this.w_AMSERVER;
           ,this.w_AM_PORTA;
           ,this.w_AMAUTSRV;
           ,this.w_AMCC_EML;
           ,this.w_AMCCNEML;
           ,this.w_AM_FIRMA;
           ,this.w_AMUTENTE;
           ,this.w_AMSHARED;
           ,this.w_AM_LOGIN;
           ,this.w_AMPASSWD;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    if this.w_NHF>=0
      this.w_TMPC = AH_MsgFormat("Creato account mail %1 nella tabella ACC_MAIL", this.w_MAIL)
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='UTE_NTI'
    this.cWorkTables[2]='UTEMAILS'
    this.cWorkTables[3]='AUT_LPE'
    this.cWorkTables[4]='AZIENDA'
    this.cWorkTables[5]='ACC_MAIL'
    return(this.OpenAllTables(5))

  proc CloseCursors()
    if used('_Curs_AZIENDA')
      use in _Curs_AZIENDA
    endif
    if used('_Curs_gscvcc31')
      use in _Curs_gscvcc31
    endif
    if used('_Curs_UTE_NTI')
      use in _Curs_UTE_NTI
    endif
    if used('_Curs_UTEMAILS')
      use in _Curs_UTEMAILS
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
