* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvbb65                                                        *
*              Aggiornamento chiave tabella ritenute                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-05-24                                                      *
* Last revis.: 2008-07-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvbb65",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvbb65 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TMPC = space(10)
  w_TEMP = space(10)
  w_NUMREC = 0
  w_RESERIAL = space(10)
  w_RECODICE = space(20)
  Result = space(10)
  w_TEST = .f.
  w_ESEGUI = space(1)
  * --- WorkFile variables
  TAB_RITE_idx=0
  RIPATMP1_idx=0
  TRI_BUTI_idx=0
  CONVERSI_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- FIle di LOG
    this.w_TEST = .F.
    * --- Try
    local bErr_03789D20
    bErr_03789D20=bTrsErr
    this.Try_03789D20()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF >=0
        this.w_TMPC = AH_MsgFormat("ERRORE GENERICO - %1",this.oParentObject.w_PMSG)
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_03789D20
    * --- End
  endproc
  proc Try_03789D20()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Read from CONVERSI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CONVERSI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONVERSI_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "COESEGUI"+;
        " from "+i_cTable+" CONVERSI where ";
            +"COCODREL = "+cp_ToStrODBC("4.0-M3928");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        COESEGUI;
        from (i_cTable) where;
            COCODREL = "4.0-M3928";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_ESEGUI = NVL(cp_ToDate(_read_.COESEGUI),cp_NullValue(_read_.COESEGUI))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if this.w_ESEGUI<>"S"
      * --- begin transaction
      cp_BeginTrs()
      i_nConn=i_TableProp[this.TAB_RITE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TAB_RITE_idx,2])
      if i_nConn <>0
        do case
          case upper(CP_DBTYPE)="SQLSERVER"
            this.Result = SqlExec(i_nConn, "EXECUTE sp_MShelpcolumns N'[" + "TAB_RITE"+"]' , NULL, 1","__PropCampi__")
            Select __PropCampi__ 
 Locate for col_name="TRTIPRIT" and Not col_null
            if !Found()
              a=SQLExec(i_nConn,"alter table "+i_cTable+" alter column TRTIPRIT char(1) NOT NULL ")=-1 
 bTrsErr=bTrsErr or SQLExec(i_nConn,"alter table "+i_cTable+" add constraint pk_"+i_cTable+; 
 " primary key (TRCODAZI,TRTIPRIT,CPROWNUM)")=-1
            else
              this.w_TEST = .T.
            endif
          case upper(CP_DBTYPE)="ORACLE"
            A=SQLExec(i_nConn,"alter table "+i_cTable+" modify(TRTIPRIT NOT NULL)")=-1 
 A=SQLExec(i_nConn,"DROP INDEX pk_" + i_cTable) 
 a=SQLExec(i_nConn,"alter table "+i_cTable+" drop constraint pk_"+i_cTable) 
 bTrsErr=SQLExec(i_nConn,"alter table "+i_cTable+" add constraint pk_"+i_cTable+; 
 " primary key (TRCODAZI,TRTIPRIT,CPROWNUM)")=-1
          case upper(CP_DBTYPE)="DB2"
            * --- Caso DB2
            this.Pag2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
             
 i_nConn=i_TableProp[this.TRI_BUTI_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.TRI_BUTI_idx,2])
            L_A=SQLEXEC(i_nConn,"Select TRDESTRI from "+i_cTable,"CURS")
            afields(DATI,"CURS")
            if DATI(1,3)=35
              this.Pag3()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
        endcase
        if Used("__PropCampi__")
           
 Select("__PropCampi__") 
 Use
        endif
        if bTrsErr
          this.oParentObject.w_PMSG = Message()
          if type("i_trsmsg")="C"
            * --- Raise
            i_Error=AH_MsgFormat("Errore nella creazione chiave %1", message())
            return
          else
            * --- Raise
            i_Error=AH_MsgFormat("Errore nella creazione chiave %1", i_trsmsg)
            return
          endif
        else
          if this.w_TEST
            if this.w_NHF >=0
              this.w_TMPC = ah_Msgformat("Conversione gi� eseguita correttamente su altre aziende")
              this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
            endif
            this.oParentObject.w_PESEOK = .T.
            this.oParentObject.w_PMSG = ah_Msgformat("Conversione gi� eseguita correttamente su altre aziende")
          else
            if this.w_NHF >=0
              this.w_TMPC = "Modificata la chiave della tabella TAB_RITE -   completato con successo."
              this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
            endif
            this.oParentObject.w_PESEOK = .T.
            this.oParentObject.w_PMSG = ah_Msgformat("Conversione eseguita correttamente")
          endif
        endif
      endif
      * --- commit
      cp_EndTrs(.t.)
    else
      this.oParentObject.w_PESEOK = .T.
      this.w_TMPC = Ah_MsgFormat("Aggiornamento gi� eseguito nella rel 4.0")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella DB2
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.TAB_RITE_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.TAB_RITE_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
          )
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    wait window "Creata tabella temporanea di appoggio" NOWAIT
    if Not GSCV_BDT( this, "TAB_RITE" , i_nConn)
      * --- Raise
      i_Error="Error"
      return
    endif
    * --- Ricostruisco il database per la tabella
    GSCV_BRT(this, "TAB_RITE" , i_nConn , .t.)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into TAB_RITE
    i_nConn=i_TableProp[this.TAB_RITE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TAB_RITE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.TAB_RITE_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    wait window "Ripristinati valori di origine" NOWAIT
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    wait window "Eliminata tabella temporanea TMP" NOWAIT
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella DB2
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.TRI_BUTI_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.TRI_BUTI_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
          )
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    wait window "Creata tabella temporanea di appoggio" NOWAIT
    if Not GSCV_BDT( this, "TRI_BUTI" , i_nConn)
      * --- Raise
      i_Error="Error"
      return
    endif
    * --- Ricostruisco il database per la tabella
    GSCV_BRT(this, "TRI_BUTI" , i_nConn , .t.)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into TRI_BUTI
    i_nConn=i_TableProp[this.TRI_BUTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TRI_BUTI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.TRI_BUTI_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    wait window "Ripristinati valori di origine" NOWAIT
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    wait window "Eliminata tabella temporanea TMP" NOWAIT
  endproc


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='TAB_RITE'
    this.cWorkTables[2]='*RIPATMP1'
    this.cWorkTables[3]='TRI_BUTI'
    this.cWorkTables[4]='CONVERSI'
    return(this.OpenAllTables(4))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
