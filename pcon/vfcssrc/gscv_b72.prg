* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscv_b72                                                        *
*              Aggiornamento dati IVA annuali e plafond                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_282]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-10-18                                                      *
* Last revis.: 2001-10-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscv_b72",oParentObject,m.w_NHF)
return(i_retval)

define class tgscv_b72 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_CODAZI = space(5)
  w_PLNODO = space(1)
  w_CATAZI = space(5)
  w___ANNO = space(4)
  w_MINIVA = 0
  w_MINIVE = 0
  w_PLAMOB = space(1)
  w_VALPLA = space(3)
  w_PLAINI = 0
  w_ACANNO = space(4)
  w_PEAIVA = 0
  w_MACIVA = 0
  w_MACIVE = 0
  w_DATVER = ctod("  /  /  ")
  w_CODBAN = space(15)
  w_DESBAN = space(50)
  w_VALIVA = space(3)
  w_ACCIVA = 0
  w_PERIOD = 0
  w_TOTESP = 0
  w_PLAUTI = 0
  w_PLADIS = 0
  * --- WorkFile variables
  AZIENDA_idx=0
  DAT_IVAN_idx=0
  PLA_IVAN_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- FIle di LOG
    * --- Codice Azienda
    * --- Stampa Plafond non considera il ciclo documentale
    * --- Attivit� Principale
    * --- Dati IVA Annuali (relativi all'anno della data di sistema)
    * --- vengono valorizzati anche i campi IAVALIVA, IAVALPLA, IAPEAIVA, IAMACIVA e IAMACIVE
    * --- Dati IVA Annuali relativi all'acconto (si riferiscono all'anno precedente a quello della data di sistema)
    * --- vengono valorizzati anche i campi IAVALIVA, IAVALPLA, IAMINIVA, IAMINIVE e IAPLAMOB
    * --- Dati Plafond
    this.w_CODAZI = i_CODAZI
    this.w_PLNODO = " "
    * --- Dati IVA Annuali (relativi all'anno della data di sistema)
    this.w___ANNO = STR(YEAR(i_DATSYS),4,0)
    this.w_MINIVA = 0
    this.w_MINIVE = 0
    this.w_PLAMOB = SPACE(1)
    this.w_VALPLA = g_PERVAL
    this.w_PLAINI = 0
    * --- Dati IVA Annuali relativi all'acconto (si riferiscono all'anno precedente a quello della data di sistema)
    this.w_ACANNO = STR((YEAR(i_DATSYS)-1),4,0)
    this.w_PEAIVA = 0
    this.w_MACIVA = 0
    this.w_MACIVE = 0
    this.w_DATVER = cp_CharToDate("  -  -  ")
    this.w_CODBAN = SPACE(15)
    this.w_DESBAN = SPACE(50)
    this.w_VALIVA = g_PERVAL
    this.w_ACCIVA = 0
    * --- Dati Plafond
    this.w_PERIOD = 0
    this.w_TOTESP = 0
    this.w_PLAUTI = 0
    this.w_PLADIS = 0
    * --- Try
    local bErr_037B94D0
    bErr_037B94D0=bTrsErr
    this.Try_037B94D0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = this.oParentObject.w_PMSG
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_037B94D0
    * --- End
    if USED("PLAFOND")
      SELECT PLAFOND
      USE
    endif
  endproc
  proc Try_037B94D0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Lettura Dati IVA Annuali e Attivit� Principale
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZMINIVA,AZMINIVE,AZPLAMOB,AZVALPLA,AZPLAINI,AZPEAIVA,AZMACIVA,AZMACIVE,AZDATVER,AZCODBAN,AZDESBAN,AZVALIVA,AZACCIVA,AZCATAZI"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZMINIVA,AZMINIVE,AZPLAMOB,AZVALPLA,AZPLAINI,AZPEAIVA,AZMACIVA,AZMACIVE,AZDATVER,AZCODBAN,AZDESBAN,AZVALIVA,AZACCIVA,AZCATAZI;
        from (i_cTable) where;
            AZCODAZI = this.w_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_MINIVA = NVL(cp_ToDate(_read_.AZMINIVA),cp_NullValue(_read_.AZMINIVA))
      this.w_MINIVE = NVL(cp_ToDate(_read_.AZMINIVE),cp_NullValue(_read_.AZMINIVE))
      this.w_PLAMOB = NVL(cp_ToDate(_read_.AZPLAMOB),cp_NullValue(_read_.AZPLAMOB))
      this.w_VALPLA = NVL(cp_ToDate(_read_.AZVALPLA),cp_NullValue(_read_.AZVALPLA))
      this.w_PLAINI = NVL(cp_ToDate(_read_.AZPLAINI),cp_NullValue(_read_.AZPLAINI))
      this.w_PEAIVA = NVL(cp_ToDate(_read_.AZPEAIVA),cp_NullValue(_read_.AZPEAIVA))
      this.w_MACIVA = NVL(cp_ToDate(_read_.AZMACIVA),cp_NullValue(_read_.AZMACIVA))
      this.w_MACIVE = NVL(cp_ToDate(_read_.AZMACIVE),cp_NullValue(_read_.AZMACIVE))
      this.w_DATVER = NVL(cp_ToDate(_read_.AZDATVER),cp_NullValue(_read_.AZDATVER))
      this.w_CODBAN = NVL(cp_ToDate(_read_.AZCODBAN),cp_NullValue(_read_.AZCODBAN))
      this.w_DESBAN = NVL(cp_ToDate(_read_.AZDESBAN),cp_NullValue(_read_.AZDESBAN))
      this.w_VALIVA = NVL(cp_ToDate(_read_.AZVALIVA),cp_NullValue(_read_.AZVALIVA))
      this.w_ACCIVA = NVL(cp_ToDate(_read_.AZACCIVA),cp_NullValue(_read_.AZACCIVA))
      this.w_CATAZI = NVL(cp_ToDate(_read_.AZCATAZI),cp_NullValue(_read_.AZCATAZI))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Valorizzazione a spazio del campo Stampa Plafond non considera il ciclo documentale
    * --- Write into AZIENDA
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.AZIENDA_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"AZPLNODO ="+cp_NullLink(cp_ToStrODBC(this.w_PLNODO),'AZIENDA','AZPLNODO');
          +i_ccchkf ;
      +" where ";
          +"AZCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
             )
    else
      update (i_cTable) set;
          AZPLNODO = this.w_PLNODO;
          &i_ccchkf. ;
       where;
          AZCODAZI = this.w_CODAZI;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Se l'anno dell'acconto � maggiore del 2001 la valuta deve essere EURO
    if VAL(this.w_ACANNO)>2001
      this.w_VALPLA = g_PERVAL
      this.w_VALIVA = g_PERVAL
    endif
    * --- Dati IVA Annuali relativi all'acconto (si riferiscono all'anno precedente a quello della data di sistema)
    * --- Try
    local bErr_037BFB90
    bErr_037BFB90=bTrsErr
    this.Try_037BFB90()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
      * --- Write into DAT_IVAN
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DAT_IVAN_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DAT_IVAN_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DAT_IVAN_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"IAPEAIVA ="+cp_NullLink(cp_ToStrODBC(this.w_PEAIVA),'DAT_IVAN','IAPEAIVA');
        +",IAMACIVA ="+cp_NullLink(cp_ToStrODBC(this.w_MACIVA),'DAT_IVAN','IAMACIVA');
        +",IAMACIVE ="+cp_NullLink(cp_ToStrODBC(this.w_MACIVE),'DAT_IVAN','IAMACIVE');
        +",IADATVER ="+cp_NullLink(cp_ToStrODBC(this.w_DATVER),'DAT_IVAN','IADATVER');
        +",IACODBAN ="+cp_NullLink(cp_ToStrODBC(this.w_CODBAN),'DAT_IVAN','IACODBAN');
        +",IADESBAN ="+cp_NullLink(cp_ToStrODBC(this.w_DESBAN),'DAT_IVAN','IADESBAN');
        +",IAVALIVA ="+cp_NullLink(cp_ToStrODBC(this.w_VALIVA),'DAT_IVAN','IAVALIVA');
        +",IAACCIVA ="+cp_NullLink(cp_ToStrODBC(this.w_ACCIVA),'DAT_IVAN','IAACCIVA');
        +",IAVALPLA ="+cp_NullLink(cp_ToStrODBC(this.w_VALPLA),'DAT_IVAN','IAVALPLA');
        +",IAMINIVA ="+cp_NullLink(cp_ToStrODBC(this.w_MINIVA),'DAT_IVAN','IAMINIVA');
        +",IAMINIVE ="+cp_NullLink(cp_ToStrODBC(this.w_MINIVE),'DAT_IVAN','IAMINIVE');
        +",IAPLAMOB ="+cp_NullLink(cp_ToStrODBC(this.w_PLAMOB),'DAT_IVAN','IAPLAMOB');
            +i_ccchkf ;
        +" where ";
            +"IACODAZI = "+cp_ToStrODBC(this.w_CODAZI);
            +" and IA__ANNO = "+cp_ToStrODBC(this.w_ACANNO);
               )
      else
        update (i_cTable) set;
            IAPEAIVA = this.w_PEAIVA;
            ,IAMACIVA = this.w_MACIVA;
            ,IAMACIVE = this.w_MACIVE;
            ,IADATVER = this.w_DATVER;
            ,IACODBAN = this.w_CODBAN;
            ,IADESBAN = this.w_DESBAN;
            ,IAVALIVA = this.w_VALIVA;
            ,IAACCIVA = this.w_ACCIVA;
            ,IAVALPLA = this.w_VALPLA;
            ,IAMINIVA = this.w_MINIVA;
            ,IAMINIVE = this.w_MINIVE;
            ,IAPLAMOB = this.w_PLAMOB;
            &i_ccchkf. ;
         where;
            IACODAZI = this.w_CODAZI;
            and IA__ANNO = this.w_ACANNO;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    bTrsErr=bTrsErr or bErr_037BFB90
    * --- End
    * --- Se l'anno � maggiore del 2001 la valuta deve essere EURO
    if VAL(this.w___ANNO)>2001
      this.w_VALPLA = g_PERVAL
      this.w_VALIVA = g_PERVAL
    endif
    * --- Dati IVA Annuali (relativi all'anno della data di sistema)
    * --- Try
    local bErr_035F44D0
    bErr_035F44D0=bTrsErr
    this.Try_035F44D0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
      * --- Write into DAT_IVAN
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DAT_IVAN_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DAT_IVAN_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DAT_IVAN_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"IAMINIVA ="+cp_NullLink(cp_ToStrODBC(this.w_MINIVA),'DAT_IVAN','IAMINIVA');
        +",IAMINIVE ="+cp_NullLink(cp_ToStrODBC(this.w_MINIVE),'DAT_IVAN','IAMINIVE');
        +",IAPLAMOB ="+cp_NullLink(cp_ToStrODBC(this.w_PLAMOB),'DAT_IVAN','IAPLAMOB');
        +",IAVALPLA ="+cp_NullLink(cp_ToStrODBC(this.w_VALPLA),'DAT_IVAN','IAVALPLA');
        +",IAPLAINI ="+cp_NullLink(cp_ToStrODBC(this.w_PLAINI),'DAT_IVAN','IAPLAINI');
        +",IAVALIVA ="+cp_NullLink(cp_ToStrODBC(this.w_VALIVA),'DAT_IVAN','IAVALIVA');
        +",IAPEAIVA ="+cp_NullLink(cp_ToStrODBC(this.w_PEAIVA),'DAT_IVAN','IAPEAIVA');
        +",IAMACIVA ="+cp_NullLink(cp_ToStrODBC(this.w_MACIVA),'DAT_IVAN','IAMACIVA');
        +",IAMACIVE ="+cp_NullLink(cp_ToStrODBC(this.w_MACIVE),'DAT_IVAN','IAMACIVE');
            +i_ccchkf ;
        +" where ";
            +"IACODAZI = "+cp_ToStrODBC(this.w_CODAZI);
            +" and IA__ANNO = "+cp_ToStrODBC(this.w___ANNO);
               )
      else
        update (i_cTable) set;
            IAMINIVA = this.w_MINIVA;
            ,IAMINIVE = this.w_MINIVE;
            ,IAPLAMOB = this.w_PLAMOB;
            ,IAVALPLA = this.w_VALPLA;
            ,IAPLAINI = this.w_PLAINI;
            ,IAVALIVA = this.w_VALIVA;
            ,IAPEAIVA = this.w_PEAIVA;
            ,IAMACIVA = this.w_MACIVA;
            ,IAMACIVE = this.w_MACIVE;
            &i_ccchkf. ;
         where;
            IACODAZI = this.w_CODAZI;
            and IA__ANNO = this.w___ANNO;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    bTrsErr=bTrsErr or bErr_035F44D0
    * --- End
    * --- Dati Plafond
    vq_exec("..\PCON\EXE\QUERY\GSCV_B72.VQR",this,"PLAFOND")
    if RECCOUNT()>0
      SELECT PLAFOND
      GO TOP
      do while NOT EOF()
        this.w___ANNO = DI__ANNO
        this.w_PERIOD = DIPERIOD
        this.w_TOTESP = DITOTESP
        this.w_PLAUTI = DIPLAUTI
        this.w_PLADIS = DIPLADIS
        * --- Dati Plafond (Nella tabella Dati IVA Annuali deve essere presente la riga relativa all'azienda e all'anno)
        * --- Try
        local bErr_037BEC90
        bErr_037BEC90=bTrsErr
        this.Try_037BEC90()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_037BEC90
        * --- End
        * --- Try
        local bErr_0361B480
        bErr_0361B480=bTrsErr
        this.Try_0361B480()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          * --- Write into PLA_IVAN
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.PLA_IVAN_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PLA_IVAN_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.PLA_IVAN_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"DITOTESP ="+cp_NullLink(cp_ToStrODBC(this.w_TOTESP),'PLA_IVAN','DITOTESP');
            +",DIPLAUTI ="+cp_NullLink(cp_ToStrODBC(this.w_PLAUTI),'PLA_IVAN','DIPLAUTI');
            +",DIPLADIS ="+cp_NullLink(cp_ToStrODBC(this.w_PLADIS),'PLA_IVAN','DIPLADIS');
                +i_ccchkf ;
            +" where ";
                +"DICODAZI = "+cp_ToStrODBC(this.w_CODAZI);
                +" and DI__ANNO = "+cp_ToStrODBC(this.w___ANNO);
                +" and DIPERIOD = "+cp_ToStrODBC(this.w_PERIOD);
                   )
          else
            update (i_cTable) set;
                DITOTESP = this.w_TOTESP;
                ,DIPLAUTI = this.w_PLAUTI;
                ,DIPLADIS = this.w_PLADIS;
                &i_ccchkf. ;
             where;
                DICODAZI = this.w_CODAZI;
                and DI__ANNO = this.w___ANNO;
                and DIPERIOD = this.w_PERIOD;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        bTrsErr=bTrsErr or bErr_0361B480
        * --- End
        SELECT PLAFOND
        SKIP
      enddo
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = ah_Msgformat("Aggiornamento dati IVA annuali e plafond eseguito correttamente")
    return
  proc Try_037BFB90()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into DAT_IVAN
    i_nConn=i_TableProp[this.DAT_IVAN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DAT_IVAN_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DAT_IVAN_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"IACODAZI"+",IA__ANNO"+",IAPEAIVA"+",IAMACIVA"+",IAMACIVE"+",IADATVER"+",IACODBAN"+",IADESBAN"+",IAVALIVA"+",IAACCIVA"+",IAVALPLA"+",IAMINIVA"+",IAMINIVE"+",IAPLAMOB"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CODAZI),'DAT_IVAN','IACODAZI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ACANNO),'DAT_IVAN','IA__ANNO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PEAIVA),'DAT_IVAN','IAPEAIVA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MACIVA),'DAT_IVAN','IAMACIVA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MACIVE),'DAT_IVAN','IAMACIVE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DATVER),'DAT_IVAN','IADATVER');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODBAN),'DAT_IVAN','IACODBAN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DESBAN),'DAT_IVAN','IADESBAN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_VALIVA),'DAT_IVAN','IAVALIVA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ACCIVA),'DAT_IVAN','IAACCIVA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_VALPLA),'DAT_IVAN','IAVALPLA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MINIVA),'DAT_IVAN','IAMINIVA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MINIVE),'DAT_IVAN','IAMINIVE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PLAMOB),'DAT_IVAN','IAPLAMOB');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'IACODAZI',this.w_CODAZI,'IA__ANNO',this.w_ACANNO,'IAPEAIVA',this.w_PEAIVA,'IAMACIVA',this.w_MACIVA,'IAMACIVE',this.w_MACIVE,'IADATVER',this.w_DATVER,'IACODBAN',this.w_CODBAN,'IADESBAN',this.w_DESBAN,'IAVALIVA',this.w_VALIVA,'IAACCIVA',this.w_ACCIVA,'IAVALPLA',this.w_VALPLA,'IAMINIVA',this.w_MINIVA)
      insert into (i_cTable) (IACODAZI,IA__ANNO,IAPEAIVA,IAMACIVA,IAMACIVE,IADATVER,IACODBAN,IADESBAN,IAVALIVA,IAACCIVA,IAVALPLA,IAMINIVA,IAMINIVE,IAPLAMOB &i_ccchkf. );
         values (;
           this.w_CODAZI;
           ,this.w_ACANNO;
           ,this.w_PEAIVA;
           ,this.w_MACIVA;
           ,this.w_MACIVE;
           ,this.w_DATVER;
           ,this.w_CODBAN;
           ,this.w_DESBAN;
           ,this.w_VALIVA;
           ,this.w_ACCIVA;
           ,this.w_VALPLA;
           ,this.w_MINIVA;
           ,this.w_MINIVE;
           ,this.w_PLAMOB;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_035F44D0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into DAT_IVAN
    i_nConn=i_TableProp[this.DAT_IVAN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DAT_IVAN_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DAT_IVAN_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"IACODAZI"+",IA__ANNO"+",IAMINIVA"+",IAMINIVE"+",IAPLAMOB"+",IAVALPLA"+",IAPLAINI"+",IAVALIVA"+",IAPEAIVA"+",IAMACIVA"+",IAMACIVE"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CODAZI),'DAT_IVAN','IACODAZI');
      +","+cp_NullLink(cp_ToStrODBC(this.w___ANNO),'DAT_IVAN','IA__ANNO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MINIVA),'DAT_IVAN','IAMINIVA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MINIVE),'DAT_IVAN','IAMINIVE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PLAMOB),'DAT_IVAN','IAPLAMOB');
      +","+cp_NullLink(cp_ToStrODBC(this.w_VALPLA),'DAT_IVAN','IAVALPLA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PLAINI),'DAT_IVAN','IAPLAINI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_VALIVA),'DAT_IVAN','IAVALIVA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PEAIVA),'DAT_IVAN','IAPEAIVA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MACIVA),'DAT_IVAN','IAMACIVA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MACIVE),'DAT_IVAN','IAMACIVE');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'IACODAZI',this.w_CODAZI,'IA__ANNO',this.w___ANNO,'IAMINIVA',this.w_MINIVA,'IAMINIVE',this.w_MINIVE,'IAPLAMOB',this.w_PLAMOB,'IAVALPLA',this.w_VALPLA,'IAPLAINI',this.w_PLAINI,'IAVALIVA',this.w_VALIVA,'IAPEAIVA',this.w_PEAIVA,'IAMACIVA',this.w_MACIVA,'IAMACIVE',this.w_MACIVE)
      insert into (i_cTable) (IACODAZI,IA__ANNO,IAMINIVA,IAMINIVE,IAPLAMOB,IAVALPLA,IAPLAINI,IAVALIVA,IAPEAIVA,IAMACIVA,IAMACIVE &i_ccchkf. );
         values (;
           this.w_CODAZI;
           ,this.w___ANNO;
           ,this.w_MINIVA;
           ,this.w_MINIVE;
           ,this.w_PLAMOB;
           ,this.w_VALPLA;
           ,this.w_PLAINI;
           ,this.w_VALIVA;
           ,this.w_PEAIVA;
           ,this.w_MACIVA;
           ,this.w_MACIVE;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_037BEC90()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into DAT_IVAN
    i_nConn=i_TableProp[this.DAT_IVAN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DAT_IVAN_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DAT_IVAN_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"IACODAZI"+",IA__ANNO"+",IAVALPLA"+",IAVALIVA"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CODAZI),'DAT_IVAN','IACODAZI');
      +","+cp_NullLink(cp_ToStrODBC(this.w___ANNO),'DAT_IVAN','IA__ANNO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_VALPLA),'DAT_IVAN','IAVALPLA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_VALIVA),'DAT_IVAN','IAVALIVA');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'IACODAZI',this.w_CODAZI,'IA__ANNO',this.w___ANNO,'IAVALPLA',this.w_VALPLA,'IAVALIVA',this.w_VALIVA)
      insert into (i_cTable) (IACODAZI,IA__ANNO,IAVALPLA,IAVALIVA &i_ccchkf. );
         values (;
           this.w_CODAZI;
           ,this.w___ANNO;
           ,this.w_VALPLA;
           ,this.w_VALIVA;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_0361B480()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PLA_IVAN
    i_nConn=i_TableProp[this.PLA_IVAN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PLA_IVAN_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PLA_IVAN_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"DICODAZI"+",DI__ANNO"+",DIPERIOD"+",DITOTESP"+",DIPLAUTI"+",DIPLADIS"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CODAZI),'PLA_IVAN','DICODAZI');
      +","+cp_NullLink(cp_ToStrODBC(this.w___ANNO),'PLA_IVAN','DI__ANNO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PERIOD),'PLA_IVAN','DIPERIOD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TOTESP),'PLA_IVAN','DITOTESP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PLAUTI),'PLA_IVAN','DIPLAUTI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PLADIS),'PLA_IVAN','DIPLADIS');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'DICODAZI',this.w_CODAZI,'DI__ANNO',this.w___ANNO,'DIPERIOD',this.w_PERIOD,'DITOTESP',this.w_TOTESP,'DIPLAUTI',this.w_PLAUTI,'DIPLADIS',this.w_PLADIS)
      insert into (i_cTable) (DICODAZI,DI__ANNO,DIPERIOD,DITOTESP,DIPLAUTI,DIPLADIS &i_ccchkf. );
         values (;
           this.w_CODAZI;
           ,this.w___ANNO;
           ,this.w_PERIOD;
           ,this.w_TOTESP;
           ,this.w_PLAUTI;
           ,this.w_PLADIS;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='DAT_IVAN'
    this.cWorkTables[3]='PLA_IVAN'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
