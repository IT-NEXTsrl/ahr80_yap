* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscv_baw                                                        *
*              Valorizzazione unita di misura in listini e contratti           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][4]                                                       *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-12-01                                                      *
* Last revis.: 2008-04-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscv_baw",oParentObject,m.w_NHF)
return(i_retval)

define class tgscv_baw as StdBatch
  * --- Local variables
  w_NHF = 0
  w_NUMAGG = 0
  w_TMPC = space(10)
  w_TEMP = space(10)
  * --- WorkFile variables
  LIS_TINI_idx=0
  CON_TRAD_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Valorizzazione dei nuovi campi COUNIMIS e LIUNIMIS nelle tabella LIS_TINI, CON_TRAD
    * --- FIle di LOG
    * --- Try
    local bErr_04A289F8
    bErr_04A289F8=bTrsErr
    this.Try_04A289F8()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = AH_MsgFormat("ERRORE GENERICO - Impossibile aggiornare tabelle LIS_TINI e CON_TRAD")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_04A289F8
    * --- End
  endproc
  proc Try_04A289F8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Valorizzazione U.M. LIUNIMIS con ART_ICOL.ARUNMIS1
    * --- Write into LIS_TINI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.LIS_TINI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.LIS_TINI_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="LICODART,CPROWNUM"
      do vq_exec with 'GSCV_BAW',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.LIS_TINI_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="LIS_TINI.LICODART = _t2.LICODART";
              +" and "+"LIS_TINI.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"LIUNIMIS = _t2.LIUNIMIS";
          +i_ccchkf;
          +" from "+i_cTable+" LIS_TINI, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="LIS_TINI.LICODART = _t2.LICODART";
              +" and "+"LIS_TINI.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" LIS_TINI, "+i_cQueryTable+" _t2 set ";
          +"LIS_TINI.LIUNIMIS = _t2.LIUNIMIS";
          +Iif(Empty(i_ccchkf),"",",LIS_TINI.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="LIS_TINI.LICODART = t2.LICODART";
              +" and "+"LIS_TINI.CPROWNUM = t2.CPROWNUM";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" LIS_TINI set (";
          +"LIUNIMIS";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.LIUNIMIS";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="LIS_TINI.LICODART = _t2.LICODART";
              +" and "+"LIS_TINI.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" LIS_TINI set ";
          +"LIUNIMIS = _t2.LIUNIMIS";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".LICODART = "+i_cQueryTable+".LICODART";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"LIUNIMIS = (select LIUNIMIS from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Valorizzazione U.M. COUNIMIS  con ART_ICOL.ARUNMIS1
    * --- Write into CON_TRAD
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.CON_TRAD_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CON_TRAD_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="CONUMERO,CPROWNUM"
      do vq_exec with 'GSCV1BAW',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.CON_TRAD_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="CON_TRAD.CONUMERO = _t2.CONUMERO";
              +" and "+"CON_TRAD.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"COUNIMIS = _t2.COUNIMIS";
          +i_ccchkf;
          +" from "+i_cTable+" CON_TRAD, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="CON_TRAD.CONUMERO = _t2.CONUMERO";
              +" and "+"CON_TRAD.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CON_TRAD, "+i_cQueryTable+" _t2 set ";
          +"CON_TRAD.COUNIMIS = _t2.COUNIMIS";
          +Iif(Empty(i_ccchkf),"",",CON_TRAD.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="CON_TRAD.CONUMERO = t2.CONUMERO";
              +" and "+"CON_TRAD.CPROWNUM = t2.CPROWNUM";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CON_TRAD set (";
          +"COUNIMIS";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.COUNIMIS";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="CON_TRAD.CONUMERO = _t2.CONUMERO";
              +" and "+"CON_TRAD.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CON_TRAD set ";
          +"COUNIMIS = _t2.COUNIMIS";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".CONUMERO = "+i_cQueryTable+".CONUMERO";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"COUNIMIS = (select COUNIMIS from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    this.w_NUMAGG = this.w_NUMAGG + i_Rows
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    if this.w_NHF>=0
      this.w_TMPC = AH_MsgFormat("Aggiornamento unit� di misura nei listini e contratti terminata correttamente")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = AH_MsgFormat("Aggiornamento unit� di misura nei listini e contratti terminata correttamente")
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='LIS_TINI'
    this.cWorkTables[2]='CON_TRAD'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
