* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscv_brt                                                        *
*              Ricostruzione tabella                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_20]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-12-22                                                      *
* Last revis.: 2009-01-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_Table,w_Conn,bFkeyToo,w_op,w_CHIUDE
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscv_brt",oParentObject,m.w_Table,m.w_Conn,m.bFkeyToo,m.w_op,m.w_CHIUDE)
return(i_retval)

define class tgscv_brt as StdBatch
  * --- Local variables
  w_Table = space(10)
  w_Conn = 0
  bFkeyToo = .f.
  w_op = space(10)
  w_CHIUDE = space(1)
  w_PHNAME = space(200)
  w_OK = 0
  * --- WorkFile variables
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch per ricostruzione database
    *     
    *     Parametro 
    *     w_Table � il nome della tabella da ricostruire
    *     w_Conn � il numero della connessione da costruire 
    *     bFKeyToo se posto a .t. ricostruisce anche le tabelle collegate (default .t.)
    *     nel batch chiamante con istruzione del tipo:
    *     i_TableProp[this.TABELLA_idx,3]
    if this.w_Conn=0
      this.w_Conn = i_ServerConn[1,2]
    endif
    if EMPTY (NVL (this.w_OP,""))
      this.w_op = "all"
    endif
    if EMPTY (NVL (this.w_CHIUDE,""))
      this.w_CHIUDE = "N"
    endif
    * --- Ricostruzione database
    * --- Carica dizionario dati (per sicurezza)
    =cp_ReadXdc()
    this.w_PHNAME = i_dcx.GetPhTable(i_dcx.GetTableIdx( this.w_TABLE ), "xxx" )
     
 Local cMsg,xServers[1,3],nServers 
 cMsg=""
    if Type("msgwnd.caption")="U"
       
 public msgwnd 
 msgwnd=createobject("mkdbmsg") 
 msgwnd.show()
    endif
     
 sqlexec(i_ServerConn[1,2],"select * from cpazi where codazi="+cp_ToStrODBC(i_codazi),"_azi_")
    this.w_OK = db_CreateTableStruct( this.w_Table ,this.w_PHNAME , "" , this.w_Conn , i_DCX , @cMsg,this.w_OP,@xServers,@nServers )
    msgwnd.End(ah_Msgformat("Ricostruzione tabella di origine completata"))
    if Empty ( msgwnd.edt_err.value ) OR this.w_CHIUDE="S"
      * --- Se non ho errori chiudo la maschera
      msgwnd.btnok.click()
    endif
    if used("_azi_")
       
 select _azi_ 
 use
    endif
    * --- Ricostruisco le costraints delle tabelle che puntano alla tabella da ricostruire
    if this.bFkeyToo AND lower(this.w_OP)="all" 
      GSCV_BRI(this, this.w_TABLE , this.w_CONN )
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  endproc


  proc Init(oParentObject,w_Table,w_Conn,bFkeyToo,w_op,w_CHIUDE)
    this.w_Table=w_Table
    this.w_Conn=w_Conn
    this.bFkeyToo=bFkeyToo
    this.w_op=w_op
    this.w_CHIUDE=w_CHIUDE
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_Table,w_Conn,bFkeyToo,w_op,w_CHIUDE"
endproc
