* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscv_ba9                                                        *
*              Aggiornamento campi e_mail e web                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_45]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-09-29                                                      *
* Last revis.: 2006-12-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscv_ba9",oParentObject,m.w_NHF)
return(i_retval)

define class tgscv_ba9 as StdBatch
  * --- Local variables
  w_CFLD = space(250)
  w_ARCHIVIO = space(50)
  w_LOOP = 0
  w_NUM_FIELDS = 0
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_NHF0 = 0
  w_CFLD = space(250)
  w_ARCHIVIO = space(50)
  w_LOOP = 0
  w_NUM_FIELDS = 0
  * --- WorkFile variables
  DES_DIVE_idx=0
  SEDIAZIE_idx=0
  MAGAZZIN_idx=0
  CAN_TIER_idx=0
  CONTI_idx=0
  AZIENDA_idx=0
  RIPATMP1_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiornamento campi relativi all'indirizzo e-mail e web nelle seguenti gestioni:
    *     CLIENTI E FORNITORI (CONTI)
    *     SEDI CLIENTI/FORNITORI (DES_DIVE)
    *     CLIENTI NEGOZIO (CLI_VEND)
    *     NOMINATIVI (OFF_NOMI)
    *     NOMINATIVI/CONTATTI (NOM_CONT)
    *     MAGAZZINI (MAGAZZIN)
    *     DATI AZIENDA (AZIENDA)
    *     SEDI AZIENDA(SEDIAZIE)
    *     COMMESSE (CAN_TIER)
    * --- FIle di LOG
    if upper(CP_DBTYPE)="DB2"
      * --- Try
      local bErr_037BB840
      bErr_037BB840=bTrsErr
      this.Try_037BB840()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        * --- Gestisce log errori
        this.oParentObject.w_PMSG = Message()
        this.oParentObject.w_PESEOK = .F.
        if this.w_NHF>=0
          this.w_TMPC = this.oParentObject.w_PMSG
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
      bTrsErr=bTrsErr or bErr_037BB840
      * --- End
      * --- Try
      local bErr_0360DFD0
      bErr_0360DFD0=bTrsErr
      this.Try_0360DFD0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        * --- Gestisce log errori
        this.oParentObject.w_PMSG = Message()
        this.oParentObject.w_PESEOK = .F.
        if this.w_NHF>=0
          this.w_TMPC = this.oParentObject.w_PMSG
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
      bTrsErr=bTrsErr or bErr_0360DFD0
      * --- End
      * --- Try
      local bErr_035F4AD0
      bErr_035F4AD0=bTrsErr
      this.Try_035F4AD0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        * --- Gestisce log errori
        this.oParentObject.w_PMSG = Message()
        this.oParentObject.w_PESEOK = .F.
        if this.w_NHF>=0
          this.w_TMPC = this.oParentObject.w_PMSG
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
      bTrsErr=bTrsErr or bErr_035F4AD0
      * --- End
      * --- Try
      local bErr_03619530
      bErr_03619530=bTrsErr
      this.Try_03619530()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        * --- Gestisce log errori
        this.oParentObject.w_PMSG = Message()
        this.oParentObject.w_PESEOK = .F.
        if this.w_NHF>=0
          this.w_TMPC = this.oParentObject.w_PMSG
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
      bTrsErr=bTrsErr or bErr_03619530
      * --- End
      * --- Try
      local bErr_037C3550
      bErr_037C3550=bTrsErr
      this.Try_037C3550()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        * --- Gestisce log errori
        this.oParentObject.w_PMSG = Message()
        this.oParentObject.w_PESEOK = .F.
        if this.w_NHF>=0
          this.w_TMPC = this.oParentObject.w_PMSG
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
      bTrsErr=bTrsErr or bErr_037C3550
      * --- End
      * --- Try
      local bErr_035FECE0
      bErr_035FECE0=bTrsErr
      this.Try_035FECE0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        * --- Gestisce log errori
        this.oParentObject.w_PMSG = Message()
        this.oParentObject.w_PESEOK = .F.
        if this.w_NHF>=0
          this.w_TMPC = this.oParentObject.w_PMSG
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
      bTrsErr=bTrsErr or bErr_035FECE0
      * --- End
    else
      ah_ErrorMsg("Conversione prevista solo per database IBM DB2",,"")
      this.oParentObject.w_PESEOK = .T.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("Elaborazione terminata con successo")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
  endproc
  proc Try_037BB840()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    i_nConn=i_TableProp[this.DES_DIVE_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.DES_DIVE_idx,2])
    if i_nConn<>0
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if bTrsErr
        * --- Raise
        i_Error=MESSAGE()
        return
      else
        if this.w_NHF>=0
          this.w_TMPC = ah_Msgformat("Adeguamento chiave primaria tabella DES_DIVE eseguito correttamente%0ATTENZIONE: occorre andare in MANUTENZIONE DATABASE, selezionare la tabella%0DES_DIVE e premere <OK>")
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.w_TMPC = ah_Msgformat("Adeguamento chiave primaria tabella DES_DIVE eseguito correttamente%0ATTENZIONE: occorre andare in MANUTENZIONE DATABASE, selezionare la tabella%0DES_DIVE e premere <OK>")
    this.oParentObject.w_PMSG = this.w_TMPC
    return
  proc Try_0360DFD0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    i_nConn=i_TableProp[this.SEDIAZIE_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.SEDIAZIE_idx,2])
    if i_nConn<>0
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if bTrsErr
        * --- Raise
        i_Error=MESSAGE()
        return
      else
        if this.w_NHF>=0
          this.w_TMPC = ah_Msgformat("Adeguamento chiave primaria tabella SEDIAZIE eseguito correttamente%0ATTENZIONE: occorre andare in MANUTENZIONE DATABASE, selezionare la tabella%0SEDIAZIE e premere <OK>")
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.w_TMPC = ah_Msgformat("Adeguamento chiave primaria tabella SEDIAZIE eseguito correttamente%0ATTENZIONE: occorre andare in MANUTENZIONE DATABASE, selezionare la tabella%0SEDIAZIE e premere <OK>")
    this.oParentObject.w_PMSG = this.w_TMPC
    return
  proc Try_035F4AD0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    i_nConn=i_TableProp[this.MAGAZZIN_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2])
    if i_nConn<>0
      this.Pag4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if bTrsErr
        * --- Raise
        i_Error=MESSAGE()
        return
      else
        if this.w_NHF>=0
          this.w_TMPC = ah_Msgformat("Adeguamento chiave primaria tabella MAGAZZIN eseguito correttamente%0ATTENZIONE: occorre andare in MANUTENZIONE DATABASE, selezionare la tabella%0MAGAZZIN e premere <OK>")
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.w_TMPC = ah_Msgformat("Adeguamento chiave primaria tabella MAGAZZIN eseguito correttamente%0ATTENZIONE: occorre andare in MANUTENZIONE DATABASE, selezionare la tabella%0MAGAZZIN e premere <OK>")
    this.oParentObject.w_PMSG = this.w_TMPC
    return
  proc Try_03619530()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    i_nConn=i_TableProp[this.CAN_TIER_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2])
    if i_nConn<>0
      this.Pag5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if bTrsErr
        * --- Raise
        i_Error=MESSAGE()
        return
      else
        if this.w_NHF>=0
          this.w_TMPC = ah_Msgformat("Adeguamento chiave primaria tabella CAN_TIER eseguito correttamente%0ATTENZIONE: occorre andare in MANUTENZIONE DATABASE, selezionare la tabella%0CAN_TIER e premere <OK>")
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.w_TMPC = ah_Msgformat("Adeguamento chiave primaria tabella CAN_TIER eseguito correttamente%0ATTENZIONE: occorre andare in MANUTENZIONE DATABASE, selezionare la tabella%0CAN_TIER e premere <OK>")
    this.oParentObject.w_PMSG = this.w_TMPC
    return
  proc Try_037C3550()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    i_nConn=i_TableProp[this.CONTI_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
    if i_nConn<>0
      this.Pag6()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if bTrsErr
        * --- Raise
        i_Error=MESSAGE()
        return
      else
        if this.w_NHF>=0
          this.w_TMPC = ah_Msgformat("Adeguamento chiave primaria tabella CONTI eseguito correttamente%0ATTENZIONE: occorre andare in MANUTENZIONE DATABASE, selezionare la tabella%0CONTI e premere <OK>")
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.w_TMPC = ah_Msgformat("Adeguamento chiave primaria tabella CONTI eseguito correttamente%0ATTENZIONE: occorre andare in MANUTENZIONE DATABASE, selezionare la tabella%0CONTI e premere <OK>")
    this.oParentObject.w_PMSG = this.w_TMPC
    return
  proc Try_035FECE0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    i_nConn=i_TableProp[this.AZIENDA_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
    if i_nConn<>0
      this.Pag7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if bTrsErr
        * --- Raise
        i_Error=MESSAGE()
        return
      else
        if this.w_NHF>=0
          this.w_TMPC = ah_Msgformat("Adeguamento chiave primaria tabella AZIENDA eseguito correttamente%0ATTENZIONE: occorre andare in MANUTENZIONE DATABASE, selezionare la tabella%0AZIENDA e premere <OK>")
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.w_TMPC = ah_Msgformat("Adeguamento chiave primaria tabella AZIENDA eseguito correttamente%0ATTENZIONE: occorre andare in MANUTENZIONE DATABASE, selezionare la tabella%0AZIENDA e premere <OK>")
    this.oParentObject.w_PMSG = this.w_TMPC
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella DB2
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.DES_DIVE_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.DES_DIVE_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
          )
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    * --- Elimino la tabella
    if Not GSCV_BDT( this, "DES_DIVE" , i_nConn )
      * --- Raise
      i_Error="Error"
      return
    endif
    * --- Ricostruisco il database per la tabella
    GSCV_BRT(this, "DES_DIVE" , i_nConn , .T.)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into DES_DIVE
    i_nConn=i_TableProp[this.DES_DIVE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DES_DIVE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.DES_DIVE_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_Msg("Ripristinati valori di origine",.T.)
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    ah_Msg("Eliminata tabella temporanea tmp",.T.)
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella DB2
    * --- Carica dizionario dati (per sicurezza)
    pName="SEDIAZIE"
    pDatabaseType=i_ServerConn[1,6]
    * --- Costruisco una stringa contenente l'elenco dei campi in analisi...
    this.w_ARCHIVIO = "SEDIAZIE"
    this.w_CFLD = ""
    this.w_LOOP = 1
    this.w_NUM_FIELDS = i_dcx.GetFieldsCount(this.w_ARCHIVIO)
    do while this.w_LOOP<=this.w_NUM_FIELDS
      this.w_CFLD = this.w_cFld+iif(empty(this.w_cFld),"",",")+i_dcx.GetFieldName(this.w_ARCHIVIO, this.w_LOOP )
      this.w_LOOP = this.w_LOOP + 1
    enddo
    * --- in coda aggiungo CPCCCHK
    this.w_CFLD = this.w_cFld+",CPCCCHK"
    * --- Copiato codice generato da una SELECT INTO, perch� non � possibile
    *     passare a tale istruzioni l'elenco dei campi tramite una variabile (w_CFLD)
     
 i_nIdx=cp_AddTableDef("RIPATMP1") && aggiunge la definizione nella lista delle tabelle 
 i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato 
 i_nConn=i_TableProp[this.SEDIAZIE_idx,3] && recupera la connessione 
 i_cTable=cp_SetAzi(i_TableProp[this.SEDIAZIE_idx,2]) 
 cp_CreateTempTable(i_nConn,i_cTempTable, this.w_CFLD+" " ," from "+i_cTable )
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    * --- Elimino la tabella
    if Not GSCV_BDT( this, "SEDIAZIE" , i_nConn )
      * --- Raise
      i_Error="Error"
      return
    endif
    * --- Ricostruisco il database per la tabella
    GSCV_BRT(this, "SEDIAZIE" , i_nConn , .T.)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into SEDIAZIE
    i_nConn=i_TableProp[this.SEDIAZIE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SEDIAZIE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.SEDIAZIE_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_Msg("Ripristinati valori di origine",.T.)
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    ah_Msg("Eliminata tabella temporanea tmp",.T.)
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella DB2
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.MAGAZZIN_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
          )
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    * --- Elimino la tabella
    if Not GSCV_BDT( this, "MAGAZZIN" , i_nConn )
      * --- Raise
      i_Error="Error"
      return
    endif
    * --- Ricostruisco il database per la tabella
    GSCV_BRT(this, "MAGAZZIN" , i_nConn , .T.)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into MAGAZZIN
    i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.MAGAZZIN_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_Msg("Ripristinati valori di origine",.T.)
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    ah_Msg("Eliminata tabella temporanea tmp",.T.)
  endproc


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella DB2
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.CAN_TIER_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
          )
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    * --- Elimino la tabella
    if Not GSCV_BDT( this, "CAN_TIER" , i_nConn )
      * --- Raise
      i_Error="Error"
      return
    endif
    * --- Ricostruisco il database per la tabella
    GSCV_BRT(this, "CAN_TIER" , i_nConn , .T.)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into CAN_TIER
    i_nConn=i_TableProp[this.CAN_TIER_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.CAN_TIER_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_Msg("Ripristinati valori di origine",.T.)
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    ah_Msg("Eliminata tabella temporanea tmp",.T.)
  endproc


  procedure Pag6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella DB2
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.CONTI_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
          )
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    * --- Elimino la tabella
    if Not GSCV_BDT( this, "CONTI" , i_nConn )
      * --- Raise
      i_Error="Error"
      return
    endif
    * --- Ricostruisco il database per la tabella
    GSCV_BRT(this, "CONTI" , i_nConn , .T.)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into CONTI
    i_nConn=i_TableProp[this.CONTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.CONTI_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_Msg("Ripristinati valori di origine",.T.)
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    ah_Msg("Eliminata tabella temporanea tmp",.T.)
  endproc


  procedure Pag7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella DB2
    * --- Carica dizionario dati (per sicurezza)
    =cp_ReadXdc()
    pName="AZIENDA"
    pDatabaseType=i_ServerConn[1,6]
    * --- Costruisco una stringa contenente l'elenco dei campi in analisi...
    this.w_ARCHIVIO = "AZIENDA"
    this.w_CFLD = ""
    this.w_LOOP = 1
    this.w_NUM_FIELDS = i_dcx.GetFieldsCount(this.w_ARCHIVIO)
    do while this.w_LOOP<=this.w_NUM_FIELDS
      this.w_CFLD = this.w_cFld+iif(empty(this.w_cFld),"",",")+i_dcx.GetFieldName(this.w_ARCHIVIO, this.w_LOOP )
      this.w_LOOP = this.w_LOOP + 1
    enddo
    * --- in coda aggiungo CPCCCHK
    this.w_CFLD = this.w_cFld+",CPCCCHK"
    * --- Copiato codice generato da una SELECT INTO, perch� non � possibile
    *     passare a tale istruzioni l'elenco dei campi tramite una variabile (w_CFLD)
     
 i_nIdx=cp_AddTableDef("RIPATMP1") && aggiunge la definizione nella lista delle tabelle 
 i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato 
 i_nConn=i_TableProp[this.AZIENDA_idx,3] && recupera la connessione 
 i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2]) 
 cp_CreateTempTable(i_nConn,i_cTempTable, this.w_CFLD+" " ," from "+i_cTable )
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    * --- Elimino la tabella
    if Not GSCV_BDT( this, "AZIENDA" , i_nConn )
      * --- Raise
      i_Error="Error"
      return
    endif
    * --- Ricostruisco il database per la tabella
    GSCV_BRT(this, "AZIENDA" , i_nConn , .T.)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into AZIENDA
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.AZIENDA_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_Msg("Ripristinati valori di origine",.T.)
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    ah_Msg("Eliminata tabella temporanea tmp",.T.)
  endproc


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,7)]
    this.cWorkTables[1]='DES_DIVE'
    this.cWorkTables[2]='SEDIAZIE'
    this.cWorkTables[3]='MAGAZZIN'
    this.cWorkTables[4]='CAN_TIER'
    this.cWorkTables[5]='CONTI'
    this.cWorkTables[6]='AZIENDA'
    this.cWorkTables[7]='*RIPATMP1'
    return(this.OpenAllTables(7))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
