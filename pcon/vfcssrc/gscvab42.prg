* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvab42                                                        *
*              Rinomina files                                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_53]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-09-29                                                      *
* Last revis.: 2003-09-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvab42",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvab42 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_NHF0 = 0
  w_ELEM = 0
  w_FILENAME = space(254)
  * --- WorkFile variables
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Sostituisce gli spazi presenti nel nome dei files con '_'
    * --- FIle di LOG
     
 Dimension p_FileArray[11] 
 
 p_FileArray[1]="Files_X_Import\GRUPPI REGOLE INFOLINK.TXT" 
 p_FileArray[2]="Files_X_Import\REGOLE INFOLINK.TXT" 
 p_FileArray[3]="..\IRDR\REPORT\ACQUISTATO\REPORT ACQUISTATO.TXT" 
 p_FileArray[4]="..\IRDR\REPORT\VENDUTO\REPORT VENDUTO.TXT" 
 p_FileArray[5]="..\IRDR\REPORT\IMPEGNATO\REPORT IMPEGNATO.TXT" 
 p_FileArray[6]="..\IRDR\REPORT\ORDINATO\REPORT ORDINATO.TXT" 
 p_FileArray[7]="..\IRDR\REPORT\PRIMANOTA\REPORT PRIMANOTA.TXT" 
 p_FileArray[8]="..\IRDR\REPORT\ANALITICA\REPORT ANALITICA.TXT" 
 p_FileArray[9]="Files_X_Import\CONFIGURAZIONI IZCP.TXT" 
 p_FileArray[10]="Files_X_Import\ALTRI ENTI.TXT" 
 p_FileArray[11]="Files_X_Import\SEDI INAIL.TXT"
    this.w_ELEM = 1
    do while this.w_ELEM <= ALen(p_FileArray)
      this.w_FILENAME = STRTRAN(p_FileArray(this.w_ELEM)," ","_")
      if FILE(p_FileArray(this.w_ELEM))
        if FILE(this.w_FILENAME)
          Delete File ( this.w_FILENAME )
        endif
        Rename p_FileArray(this.w_ELEM) to ( this.w_FILENAME )
      endif
      this.w_ELEM = this.w_ELEM + 1
    enddo
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.w_TMPC = ah_Msgformat("Rinomina files eseguita correttamente%0")
    this.oParentObject.w_PMSG = this.w_TMPC
  endproc


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
