* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcc10                                                        *
*              Eliminazione campi nei dati LOGCNTFL obsoleti                   *
*                                                                              *
*      Author: Zucchetti TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_77]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-02-10                                                      *
* Last revis.: 2012-02-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcc10",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcc10 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_ROWS = 0
  w_LOTBLNAM = space(200)
  w_OK = .f.
  w_OK1 = .f.
  w_NUMFLD = 0
  w_LOOP = 0
  * --- WorkFile variables
  LOGCNTFL_idx=0
  OPAGGREG_idx=0
  REF_DETT_idx=0
  runtime_filters = 2

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- FIle di LOG
    *     Controllo Flussi
    * --- Try
    local bErr_04A82BB0
    bErr_04A82BB0=bTrsErr
    this.Try_04A82BB0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_msgformat("Impossibile aggiorna tabella OPAGGREG")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
      i_retcode = 'stop'
      return
    endif
    bTrsErr=bTrsErr or bErr_04A82BB0
    * --- End
    * --- Try
    local bErr_04A84110
    bErr_04A84110=bTrsErr
    this.Try_04A84110()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_msgformat("Impossibile aggiornare tabella LOGCNTFL")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
      i_retcode = 'stop'
      return
    endif
    bTrsErr=bTrsErr or bErr_04A84110
    * --- End
    * --- Try
    local bErr_04C85F28
    bErr_04C85F28=bTrsErr
    this.Try_04C85F28()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_msgformat("Impossibile aggiornare tabella REF_DETT")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
      i_retcode = 'stop'
      return
    endif
    bTrsErr=bTrsErr or bErr_04C85F28
    * --- End
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc
  proc Try_04A82BB0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Messaggio mail
    * --- Write into OPAGGREG
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.OPAGGREG_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OPAGGREG_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="OASERIAL"
      do vq_exec with 'GSCVCC10A',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.OPAGGREG_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="OPAGGREG.OASERIAL = _t2.OASERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"OAPIEMSG = _t2.OATXTMSG";
          +",OATXTMSG = _t2.OAPIEMSG";
          +i_ccchkf;
          +" from "+i_cTable+" OPAGGREG, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="OPAGGREG.OASERIAL = _t2.OASERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OPAGGREG, "+i_cQueryTable+" _t2 set ";
          +"OPAGGREG.OAPIEMSG = _t2.OATXTMSG";
          +",OPAGGREG.OATXTMSG = _t2.OAPIEMSG";
          +Iif(Empty(i_ccchkf),"",",OPAGGREG.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="OPAGGREG.OASERIAL = t2.OASERIAL";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OPAGGREG set (";
          +"OAPIEMSG,";
          +"OATXTMSG";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.OATXTMSG,";
          +"t2.OAPIEMSG";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="OPAGGREG.OASERIAL = _t2.OASERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OPAGGREG set ";
          +"OAPIEMSG = _t2.OATXTMSG";
          +",OATXTMSG = _t2.OAPIEMSG";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".OASERIAL = "+i_cQueryTable+".OASERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"OAPIEMSG = (select OATXTMSG from "+i_cQueryTable+" where "+i_cWhere+")";
          +",OATXTMSG = (select OAPIEMSG from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.w_TMPC = ah_msgformat("Aggiornamento tabella OPAGGREG completato%0")
    this.oParentObject.w_PMSG = this.oParentObject.w_PMSG + this.w_TMPC
    return
  proc Try_04A84110()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Descrizione campi
    * --- Write into LOGCNTFL
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.LOGCNTFL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.LOGCNTFL_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="LOSERIAL,LOSTAMP,LOREC_ID"
      do vq_exec with 'GSCVCC10B',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.LOGCNTFL_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="LOGCNTFL.LOSERIAL = _t2.LOSERIAL";
              +" and "+"LOGCNTFL.LOSTAMP = _t2.LOSTAMP";
              +" and "+"LOGCNTFL.LOREC_ID = _t2.LOREC_ID";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"LOROWLOG = _t2.CPROWNUM";
          +i_ccchkf;
          +" from "+i_cTable+" LOGCNTFL, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="LOGCNTFL.LOSERIAL = _t2.LOSERIAL";
              +" and "+"LOGCNTFL.LOSTAMP = _t2.LOSTAMP";
              +" and "+"LOGCNTFL.LOREC_ID = _t2.LOREC_ID";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" LOGCNTFL, "+i_cQueryTable+" _t2 set ";
          +"LOGCNTFL.LOROWLOG = _t2.CPROWNUM";
          +Iif(Empty(i_ccchkf),"",",LOGCNTFL.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="LOGCNTFL.LOSERIAL = t2.LOSERIAL";
              +" and "+"LOGCNTFL.LOSTAMP = t2.LOSTAMP";
              +" and "+"LOGCNTFL.LOREC_ID = t2.LOREC_ID";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" LOGCNTFL set (";
          +"LOROWLOG";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.CPROWNUM";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="LOGCNTFL.LOSERIAL = _t2.LOSERIAL";
              +" and "+"LOGCNTFL.LOSTAMP = _t2.LOSTAMP";
              +" and "+"LOGCNTFL.LOREC_ID = _t2.LOREC_ID";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" LOGCNTFL set ";
          +"LOROWLOG = _t2.CPROWNUM";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".LOSERIAL = "+i_cQueryTable+".LOSERIAL";
              +" and "+i_cTable+".LOSTAMP = "+i_cQueryTable+".LOSTAMP";
              +" and "+i_cTable+".LOREC_ID = "+i_cQueryTable+".LOREC_ID";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"LOROWLOG = (select CPROWNUM from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.w_TMPC = ah_msgformat("Aggiornamento tabella LOGCNTFL completato%0")
    this.oParentObject.w_PMSG = this.oParentObject.w_PMSG + this.w_TMPC
    return
  proc Try_04C85F28()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Descrizione campi
    * --- Write into REF_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.REF_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.REF_DETT_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="RESERIAL,CPROWNUM"
      do vq_exec with 'GSCVCC10C',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.REF_DETT_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="REF_DETT.RESERIAL = _t2.RESERIAL";
              +" and "+"REF_DETT.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"REFLDCOM = _t2.FLCOMMEN";
          +i_ccchkf;
          +" from "+i_cTable+" REF_DETT, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="REF_DETT.RESERIAL = _t2.RESERIAL";
              +" and "+"REF_DETT.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" REF_DETT, "+i_cQueryTable+" _t2 set ";
          +"REF_DETT.REFLDCOM = _t2.FLCOMMEN";
          +Iif(Empty(i_ccchkf),"",",REF_DETT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="REF_DETT.RESERIAL = t2.RESERIAL";
              +" and "+"REF_DETT.CPROWNUM = t2.CPROWNUM";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" REF_DETT set (";
          +"REFLDCOM";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.FLCOMMEN";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="REF_DETT.RESERIAL = _t2.RESERIAL";
              +" and "+"REF_DETT.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" REF_DETT set ";
          +"REFLDCOM = _t2.FLCOMMEN";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".RESERIAL = "+i_cQueryTable+".RESERIAL";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"REFLDCOM = (select FLCOMMEN from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.w_TMPC = ah_msgformat("Aggiornamento tabella REF_DETT completato%0")
    this.oParentObject.w_PMSG = this.oParentObject.w_PMSG + this.w_TMPC
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elimina campi obsoleti tabella LOGCNTFL
    *     LOFLDNAM
    *     LOTBLNAM
    this.w_OK1 = .T.
    this.w_OK = .T.
    if upper(CP_DBTYPE)<>"DB2"
      * --- Try
      local bErr_04A9AC50
      bErr_04A9AC50=bTrsErr
      this.Try_04A9AC50()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_OK = .F.
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_04A9AC50
      * --- End
      this.w_NUMFLD = 2
       
 Dimension ARFIELD(this.w_NUMFLD) 
 ARFIELD[1]="LOFLDNAM" 
 ARFIELD[2]="LOTBLNAM"
    else
      this.w_OK = .F.
      this.w_OK1 = .F.
    endif
    * --- Try
    local bErr_04A90A80
    bErr_04A90A80=bTrsErr
    this.Try_04A90A80()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_msgformat("ERRORE GENERICO - Impossibile eseguire aggiornamento richiesto")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_04A90A80
    * --- End
  endproc
  proc Try_04A9AC50()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Read from LOGCNTFL
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.LOGCNTFL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.LOGCNTFL_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "LOTBLNAM"+;
        " from "+i_cTable+" LOGCNTFL where ";
            +"LOSERIAL = "+cp_ToStrODBC("0000000001");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        LOTBLNAM;
        from (i_cTable) where;
            LOSERIAL = "0000000001";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_LOTBLNAM = NVL(cp_ToDate(_read_.LOTBLNAM),cp_NullValue(_read_.LOTBLNAM))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    return
  proc Try_04A90A80()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.w_LOOP = 0
    do while this.w_LOOP<this.w_NUMFLD
      this.w_ROWS = 0
      this.w_LOOP = this.w_LOOP + 1
      if this.w_OK
        i_nConn=i_TableProp[this.LOGCNTFL_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.LOGCNTFL_idx,2])
        this.w_ROWS = SQLExec(i_nConn,"ALTER TABLE "+i_cTable+" DROP COLUMN " + ARFIELD[this.w_LOOP] )
        if this.w_ROWS<0
          * --- Raise
          i_Error=Message()
          return
        else
          * --- Esecuzione ok
          this.oParentObject.w_PESEOK = .T.
          this.w_TMPC = ah_msgformat("Eliminazione campo %1 eseguita con successo%0",arfield[this.w_loop])
          this.oParentObject.w_PMSG = this.oParentObject.w_PMSG + this.w_TMPC
        endif
      else
        * --- Esecuzione ok
        this.oParentObject.w_PESEOK = .T.
        this.w_TMPC = ah_msgformat("Eliminazione campi da tabella LOGCNTFL eseguita con successo%0")
        this.oParentObject.w_PMSG = this.w_TMPC
        this.w_LOOP = this.w_NUMFLD+1
      endif
    enddo
    return

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='LOGCNTFL'
    this.cWorkTables[2]='OPAGGREG'
    this.cWorkTables[3]='REF_DETT'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
