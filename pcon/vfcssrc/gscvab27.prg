* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvab27                                                        *
*              Aggiorna qta importata/evasa documenti                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_69]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-02-27                                                      *
* Last revis.: 2006-12-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvab27",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvab27 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_MESS = space(100)
  w_DATINI = ctod("  /  /  ")
  w_DATFIN = ctod("  /  /  ")
  w_OKCONV = .f.
  * --- WorkFile variables
  DOC_DETT_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questa procedura Aggiorna:
    *      il campo QTA Importata nei Documenti generati che evadono  e che hanno tale campo a zero
    *      azzera qta evasa nei documenti non collegati a nessun documento con tale campo valorizzato
    * --- FIle di LOG
    this.w_OKCONV = .F.
    do GSCV_K27 with this
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if this.w_OKCONV
      * --- Try
      local bErr_04A96B10
      bErr_04A96B10=bTrsErr
      this.Try_04A96B10()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        * --- Gestisce log errori
        this.oParentObject.w_PMSG = Message()
        this.oParentObject.w_PESEOK = .F.
        if this.w_NHF>=0
          this.w_TMPC = ah_Msgformat("ERRORE GENERICO - impossibile aggiornare documenti")
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
      bTrsErr=bTrsErr or bErr_04A96B10
      * --- End
    else
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = ah_Msgformat("Operazione abbandonata")
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("Operazione abbandonata")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
  endproc
  proc Try_04A96B10()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Write into DOC_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="MVSERIAL,CPROWNUM,MVNUMRIF"
      do vq_exec with 'GSCVA2B27',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
              +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
              +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MVQTAIMP = _t2.MVQTAMOV";
          +",MVQTAIM1 = _t2.MVQTAUM1";
          +i_ccchkf;
          +" from "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
              +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
              +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 set ";
          +"DOC_DETT.MVQTAIMP = _t2.MVQTAMOV";
          +",DOC_DETT.MVQTAIM1 = _t2.MVQTAUM1";
          +Iif(Empty(i_ccchkf),"",",DOC_DETT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="DOC_DETT.MVSERIAL = t2.MVSERIAL";
              +" and "+"DOC_DETT.CPROWNUM = t2.CPROWNUM";
              +" and "+"DOC_DETT.MVNUMRIF = t2.MVNUMRIF";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set (";
          +"MVQTAIMP,";
          +"MVQTAIM1";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.MVQTAMOV,";
          +"t2.MVQTAUM1";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
              +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
              +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set ";
          +"MVQTAIMP = _t2.MVQTAMOV";
          +",MVQTAIM1 = _t2.MVQTAUM1";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
              +" and "+i_cTable+".MVNUMRIF = "+i_cQueryTable+".MVNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MVQTAIMP = (select MVQTAMOV from "+i_cQueryTable+" where "+i_cWhere+")";
          +",MVQTAIM1 = (select MVQTAUM1 from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    this.oParentObject.w_PMSG = ah_Msgformat("Aggiornamento qta importata eseguito correttamente su %1 righe" , Alltrim(Str( i_Rows )) )
    * --- Write into DOC_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="MVSERIAL,CPROWNUM,MVNUMRIF"
      do vq_exec with 'GSCVA1B27',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
              +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
              +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVQTAEVA ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAEVA');
      +",MVQTAEV1 ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAEV1');
      +",MVIMPEVA ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVIMPEVA');
      +",MVFLEVAS ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLEVAS');
      +",MVDATGEN ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("    -  -  ")),'DOC_DETT','MVDATGEN');
          +i_ccchkf;
          +" from "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
              +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
              +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 set ";
      +"DOC_DETT.MVQTAEVA ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAEVA');
      +",DOC_DETT.MVQTAEV1 ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAEV1');
      +",DOC_DETT.MVIMPEVA ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVIMPEVA');
      +",DOC_DETT.MVFLEVAS ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLEVAS');
      +",DOC_DETT.MVDATGEN ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("    -  -  ")),'DOC_DETT','MVDATGEN');
          +Iif(Empty(i_ccchkf),"",",DOC_DETT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="DOC_DETT.MVSERIAL = t2.MVSERIAL";
              +" and "+"DOC_DETT.CPROWNUM = t2.CPROWNUM";
              +" and "+"DOC_DETT.MVNUMRIF = t2.MVNUMRIF";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set (";
          +"MVQTAEVA,";
          +"MVQTAEV1,";
          +"MVIMPEVA,";
          +"MVFLEVAS,";
          +"MVDATGEN";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAEVA')+",";
          +cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAEV1')+",";
          +cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVIMPEVA')+",";
          +cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLEVAS')+",";
          +cp_NullLink(cp_ToStrODBC(cp_CharToDate("    -  -  ")),'DOC_DETT','MVDATGEN')+"";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
              +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
              +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set ";
      +"MVQTAEVA ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAEVA');
      +",MVQTAEV1 ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAEV1');
      +",MVIMPEVA ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVIMPEVA');
      +",MVFLEVAS ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLEVAS');
      +",MVDATGEN ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("    -  -  ")),'DOC_DETT','MVDATGEN');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
              +" and "+i_cTable+".MVNUMRIF = "+i_cQueryTable+".MVNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVQTAEVA ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAEVA');
      +",MVQTAEV1 ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAEV1');
      +",MVIMPEVA ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVIMPEVA');
      +",MVFLEVAS ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLEVAS');
      +",MVDATGEN ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("    -  -  ")),'DOC_DETT','MVDATGEN');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = this.oParentObject.w_PMSG + ah_Msgformat("%0Aggiornamento qta evasa eseguito correttamente su %1 righe", Alltrim(Str( i_Rows )) )
    * --- commit
    cp_EndTrs(.t.)
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='DOC_DETT'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
