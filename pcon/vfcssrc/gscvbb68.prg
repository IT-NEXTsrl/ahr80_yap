* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvbb68                                                        *
*              Aggiornamento dich produzione WIP non nett                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-10-27                                                      *
* Last revis.: 2011-06-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvbb68",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvbb68 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TMPNE = 0
  w_CONV = 0
  w_TMPC = space(10)
  w_TEMP = space(10)
  w_CODREL = space(10)
  w_MVEMERIC = space(1)
  w_OLCODODL = space(15)
  w_RIGAODL = 0
  w_MPSERIAL = space(10)
  w_RIGADICH = 0
  w_QTAPPO = 0
  w_MPQTAEVA = 0
  w_MPQTAEV1 = 0
  w_DPDATREG = ctod("  /  /  ")
  w_CAUSCA = space(5)
  w_FLINTE = space(1)
  w_FLVEAC = space(1)
  w_FLPPRO = space(1)
  w_FLVEAC = space(1)
  w_CRIVAL = space(2)
  w_STADOC = space(1)
  w_MPQTAEVA = 0
  w_MPQTAEV1 = 0
  w_DPFLEVAS = space(1)
  w_QTAPPO = 0
  w_QTAUM1 = 0
  w_QTAMOV = 0
  w_ROWS = 0
  w_DATABLOC = ctod("  /  /  ")
  w_OPERAZ = space(2)
  w_MVTIPDOC = space(5)
  w_MVDATDOC = ctod("  /  /  ")
  w_MVDATDIV = ctod("  /  /  ")
  w_MVANNPRO = space(4)
  w_MVNUMDOC = 0
  w_MVNUMEST = 0
  w_MVPRP = space(2)
  w_MVALFDOC = space(2)
  w_MVALFEST = space(2)
  w_MVPRD = space(2)
  w_MVDATCIV = ctod("  /  /  ")
  w_MVTCAMAG = space(5)
  w_MVTIPCON = space(1)
  w_MVANNDOC = space(4)
  w_MVTFRAGG = space(1)
  w_MVCODESE = space(4)
  w_MVSERIAL = space(10)
  w_MVCODUTE = 0
  w_MVCAUCON = space(5)
  w_MVFLACCO = space(1)
  w_MVNUMREG = 0
  w_MVFLINTE = space(1)
  w_MVDATREG = ctod("  /  /  ")
  w_MVFLVEAC = space(1)
  w_MVCLADOC = space(2)
  w_MVFLPROV = space(1)
  w_NUMSCO = 0
  w_DOCCAR = space(10)
  w_DOCSCA = space(10)
  w_nRecSel = 0
  w_nRecEla = 0
  w_nRecDoc = 0
  w_LNumErr = 0
  w_LOggErr = space(15)
  w_LErrore = space(80)
  w_LTesMes = space(0)
  w_MASK = .NULL.
  w_DPSERIAL = space(10)
  * --- WorkFile variables
  COM_VARI_idx=0
  DISTBASE_idx=0
  MAT_PROD_idx=0
  PAR_PROD_idx=0
  DIC_PROD_idx=0
  TIP_DOCU_idx=0
  TMPMODL_idx=0
  DELTA_idx=0
  AZIENDA_idx=0
  CONVERSI_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- FIle di LOG
    * --- Log Errori
    * --- Read from CONVERSI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CONVERSI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONVERSI_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "COCODREL"+;
        " from "+i_cTable+" CONVERSI where ";
            +"CONOMPRO = "+cp_ToStrODBC("GSCVBB68");
            +" and COESEGUI = "+cp_ToStrODBC("S");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        COCODREL;
        from (i_cTable) where;
            CONOMPRO = "GSCVBB68";
            and COESEGUI = "S";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CODREL = NVL(cp_ToDate(_read_.COCODREL),cp_NullValue(_read_.COCODREL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if EMPTY(this.w_CODREL)
      * --- Try
      local bErr_0376D2A0
      bErr_0376D2A0=bTrsErr
      this.Try_0376D2A0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- Gestisce log errori
        this.oParentObject.w_pMSG = Message()
        this.oParentObject.w_PESEOK = .F.
        if this.w_NHF>=0
          this.w_TMPC = "ERRORE - " + Message()
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
      bTrsErr=bTrsErr or bErr_0376D2A0
      * --- End
    else
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("Conversione gi� eseguita nella Rel. 4.0")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
      this.oParentObject.w_PESEOK = .T.
    endif
  endproc
  proc Try_0376D2A0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.oParentObject.w_PESEOK = .T.
    if this.w_NHF>=0
      this.w_TMPC = "Aggiornamento Dichiarazioni di produzione"
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    * --- legge la data di blocco dei movimenti di magazzino
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZCONMAG"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZCONMAG;
        from (i_cTable) where;
            AZCODAZI = i_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DATABLOC = NVL(cp_ToDate(_read_.AZCONMAG),cp_NullValue(_read_.AZCONMAG))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Legge i parametri di produzione
    * --- Read from PAR_PROD
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_PROD_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROD_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PPCAUSCA"+;
        " from "+i_cTable+" PAR_PROD where ";
            +"PPCODICE = "+cp_ToStrODBC("PP");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PPCAUSCA;
        from (i_cTable) where;
            PPCODICE = "PP";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CAUSCA = NVL(cp_ToDate(_read_.PPCAUSCA),cp_NullValue(_read_.PPCAUSCA))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Tabella temporanea di appoggio dove inserisco i seriali delle dichiarazioni
    *     per cui generare i documenti
    * --- Create temporary table DELTA
    i_nIdx=cp_AddTableDef('DELTA') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.DIC_PROD_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.DIC_PROD_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"DPSERIAL , DPDATREG "," from "+i_cTable;
          +" where 1=0";
          )
    this.DELTA_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Select from GSCVBB686
    do vq_exec with 'GSCVBB686',this,'_Curs_GSCVBB686','',.f.,.t.
    if used('_Curs_GSCVBB686')
      select _Curs_GSCVBB686
      locate for 1=1
      do while not(eof())
      this.w_MPSERIAL = _Curs_GSCVBB686.MPSERIAL
      this.w_DPDATREG = _Curs_GSCVBB686.DPDATREG
      this.w_OLCODODL = _Curs_GSCVBB686.MPSERODL
      WAIT WIND "Elaborazione Dichiarazione di produzione " + Alltrim(this.w_MPSERIAL) + " - ODL " + alltrim(this.w_OLCODODL) + "   ..." NOWAIT
      * --- Create temporary table TMPMODL
      i_nIdx=cp_AddTableDef('TMPMODL') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('..\PCON\EXE\QUERY\GSCVBB68',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.TMPMODL_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      * --- Select from GSCVBB685
      do vq_exec with 'GSCVBB685',this,'_Curs_GSCVBB685','',.f.,.t.
      if used('_Curs_GSCVBB685')
        select _Curs_GSCVBB685
        locate for 1=1
        do while not(eof())
        this.w_RIGADICH = _Curs_GSCVBB685.CPROWNUM
        this.w_OLCODODL = _Curs_GSCVBB685.MPSERODL
        this.w_RIGAODL = _Curs_GSCVBB685.MPROWODL
        this.w_DPFLEVAS = NVL(_Curs_GSCVBB685.MPFLEVAS , SPACE(1))
        this.w_QTAUM1 = _Curs_GSCVBB685.DPQTAPR1+_Curs_GSCVBB685.DPQTASC1
        * --- Select from TMPMODL
        i_nConn=i_TableProp[this.TMPMODL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPMODL_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select sum(MPQTAMOV) as QTAMOV  from "+i_cTable+" TMPMODL ";
              +" where MPSERODL="+cp_ToStrODBC(this.w_OLCODODL)+" and MPROWODL="+cp_ToStrODBC(this.w_RIGAODL)+" and MPSERIAL<"+cp_ToStrODBC(this.w_MPSERIAL)+"";
               ,"_Curs_TMPMODL")
        else
          select sum(MPQTAMOV) as QTAMOV from (i_cTable);
           where MPSERODL=this.w_OLCODODL and MPROWODL=this.w_RIGAODL and MPSERIAL<this.w_MPSERIAL;
            into cursor _Curs_TMPMODL
        endif
        if used('_Curs_TMPMODL')
          select _Curs_TMPMODL
          locate for 1=1
          do while not(eof())
          * --- Totale scarichi
          this.w_QTAMOV = nvl(_Curs_TMPMODL.QTAMOV,0)
            select _Curs_TMPMODL
            continue
          enddo
          use
        endif
        * --- Select from MAT_PROD
        i_nConn=i_TableProp[this.MAT_PROD_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MAT_PROD_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select sum(MPQTAMOV) as QTAMOV  from "+i_cTable+" MAT_PROD ";
              +" where MPSERODL="+cp_ToStrODBC(this.w_OLCODODL)+" and MPROWODL="+cp_ToStrODBC(this.w_RIGAODL)+" and MPSERIAL<"+cp_ToStrODBC(this.w_MPSERIAL)+"";
               ,"_Curs_MAT_PROD")
        else
          select sum(MPQTAMOV) as QTAMOV from (i_cTable);
           where MPSERODL=this.w_OLCODODL and MPROWODL=this.w_RIGAODL and MPSERIAL<this.w_MPSERIAL;
            into cursor _Curs_MAT_PROD
        endif
        if used('_Curs_MAT_PROD')
          select _Curs_MAT_PROD
          locate for 1=1
          do while not(eof())
          * --- Totale scarichi
          this.w_QTAMOV = this.w_QTAMOV + nvl(_Curs_MAT_PROD.QTAMOV,0)
            select _Curs_MAT_PROD
            continue
          enddo
          use
        endif
        this.w_QTAPPO = max(NVL(_Curs_GSCVBB685.MPCOEIMP, 0)* this.w_QTAUM1-(_Curs_GSCVBB685.OLQTAMOV-_Curs_GSCVBB685.OLQTASAL-this.w_QTAMOV),0)
        this.w_MPQTAEVA = this.w_QTAPPO
        this.w_MPQTAEV1 = this.w_QTAPPO
        this.w_DPFLEVAS = IIF(this.w_DPFLEVAS="S" , "S", " ")
        * --- Write into TMPMODL
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.TMPMODL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPMODL_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPMODL_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MPQTAEVA ="+cp_NullLink(cp_ToStrODBC(this.w_MPQTAEVA),'TMPMODL','MPQTAEVA');
          +",MPQTAEV1 ="+cp_NullLink(cp_ToStrODBC(this.w_MPQTAEV1),'TMPMODL','MPQTAEV1');
          +",MPFLEVAS ="+cp_NullLink(cp_ToStrODBC(this.w_DPFLEVAS),'TMPMODL','MPFLEVAS');
              +i_ccchkf ;
          +" where ";
              +"MPSERIAL = "+cp_ToStrODBC(this.w_MPSERIAL);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_RIGADICH);
                 )
        else
          update (i_cTable) set;
              MPQTAEVA = this.w_MPQTAEVA;
              ,MPQTAEV1 = this.w_MPQTAEV1;
              ,MPFLEVAS = this.w_DPFLEVAS;
              &i_ccchkf. ;
           where;
              MPSERIAL = this.w_MPSERIAL;
              and CPROWNUM = this.w_RIGADICH;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
          select _Curs_GSCVBB685
          continue
        enddo
        use
      endif
      * --- Try
      local bErr_03601500
      bErr_03601500=bTrsErr
      this.Try_03601500()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        if this.w_NHF>=0
          this.w_TMPC = "Errore durante aggiornamento dichiarazione - " + MESSAGE()
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
        * --- accept error
        bTrsErr=.f.
        this.w_TMPNE = this.w_TMPNE + 1
      endif
      bTrsErr=bTrsErr or bErr_03601500
      * --- End
        select _Curs_GSCVBB686
        continue
      enddo
      use
    endif
    * --- Drop temporary table TMPMODL
    i_nIdx=cp_GetTableDefIdx('TMPMODL')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPMODL')
    endif
    * --- Select from DELTA
    i_nConn=i_TableProp[this.DELTA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DELTA_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" DELTA ";
          +" order by DPSERIAL";
           ,"_Curs_DELTA")
    else
      select * from (i_cTable);
       order by DPSERIAL;
        into cursor _Curs_DELTA
    endif
    if used('_Curs_DELTA')
      select _Curs_DELTA
      locate for 1=1
      do while not(eof())
      this.w_DPSERIAL = _Curs_DELTA.DPSERIAL
      this.w_MPSERIAL = _Curs_DELTA.DPSERIAL
      this.w_DPDATREG = _Curs_DELTA.DPDATREG
      this.w_CRIVAL = "US"
      this.w_STADOC = " "
      this.w_OPERAZ = "SC"
      this.w_nRecSel = this.w_nRecSel + 1
      * --- Generazione Documento di Scarico
      vq_exec("..\COLA\EXE\QUERY\GSCO_BSP.VQR",this,"GENEORDI")
      WAIT WIND "Generazione Scarico Materiali per Produzione ..." NOWAIT
      this.w_MVTIPDOC = this.w_CAUSCA
      this.w_MVDATDOC = this.w_DPDATREG
      this.w_MVDATDIV = this.w_MVDATDOC
      this.w_MVDATREG = this.w_MVDATDOC
      this.w_MVDATCIV = this.w_MVDATDOC
      this.w_MVPRP = "NN"
      this.w_MVNUMDOC = 0
      this.w_MVFLPROV = "N"
      this.w_MVNUMREG = 0
      this.w_MVNUMEST = 0
      this.w_MVALFEST = "  "
      this.w_MVSERIAL = SPACE(10)
      this.w_MVCODESE = g_CODESE
      this.w_MVCODUTE = IIF(g_MAGUTE="S", 0, i_CODUTE)
      * --- Read from TIP_DOCU
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "TDALFDOC,TDPRODOC,TDCATDOC,TDFLVEAC,TDEMERIC,TDFLINTE,TDCAUMAG,TFFLRAGG,TDCAUCON,TDNUMSCO,TDFLACCO,TDFLPPRO"+;
          " from "+i_cTable+" TIP_DOCU where ";
              +"TDTIPDOC = "+cp_ToStrODBC(this.w_MVTIPDOC);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          TDALFDOC,TDPRODOC,TDCATDOC,TDFLVEAC,TDEMERIC,TDFLINTE,TDCAUMAG,TFFLRAGG,TDCAUCON,TDNUMSCO,TDFLACCO,TDFLPPRO;
          from (i_cTable) where;
              TDTIPDOC = this.w_MVTIPDOC;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_MVALFDOC = NVL(cp_ToDate(_read_.TDALFDOC),cp_NullValue(_read_.TDALFDOC))
        this.w_MVPRD = NVL(cp_ToDate(_read_.TDPRODOC),cp_NullValue(_read_.TDPRODOC))
        this.w_MVCLADOC = NVL(cp_ToDate(_read_.TDCATDOC),cp_NullValue(_read_.TDCATDOC))
        this.w_MVFLVEAC = NVL(cp_ToDate(_read_.TDFLVEAC),cp_NullValue(_read_.TDFLVEAC))
        this.w_MVEMERIC = NVL(cp_ToDate(_read_.TDEMERIC),cp_NullValue(_read_.TDEMERIC))
        this.w_MVFLINTE = NVL(cp_ToDate(_read_.TDFLINTE),cp_NullValue(_read_.TDFLINTE))
        this.w_MVTCAMAG = NVL(cp_ToDate(_read_.TDCAUMAG),cp_NullValue(_read_.TDCAUMAG))
        this.w_MVTFRAGG = NVL(cp_ToDate(_read_.TFFLRAGG),cp_NullValue(_read_.TFFLRAGG))
        this.w_MVCAUCON = NVL(cp_ToDate(_read_.TDCAUCON),cp_NullValue(_read_.TDCAUCON))
        this.w_NUMSCO = NVL(cp_ToDate(_read_.TDNUMSCO),cp_NullValue(_read_.TDNUMSCO))
        this.w_MVFLACCO = NVL(cp_ToDate(_read_.TDFLACCO),cp_NullValue(_read_.TDFLACCO))
        this.w_FLINTE = NVL(cp_ToDate(_read_.TDFLINTE),cp_NullValue(_read_.TDFLINTE))
        this.w_FLPPRO = NVL(cp_ToDate(_read_.TDFLPPRO),cp_NullValue(_read_.TDFLPPRO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_MVTIPCON = IIF(this.w_FLINTE $ "CF", this.w_FLINTE, " ")
      this.w_MVANNPRO = CALPRO(this.w_MVDATREG,this.w_MVCODESE,this.w_FLPPRO)
      this.w_MVANNDOC = STR(YEAR(this.w_MVDATDOC), 4, 0)
      * --- Lancia Batch di Generazione Documenti di Scarico
      do GSCO_BGD with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
        select _Curs_DELTA
        continue
      enddo
      use
    endif
    * --- Al termine lancia aggiornamento saldi di magazzino
    this.w_MASK = GSMA_KRS()
    this.w_MASK.w_FLCP = " "
    this.w_MASK.ECPSAVE()     
    do case
      case this.w_CONV>=0 AND this.w_NHF>=0 AND this.w_TMPNE=0
        this.oParentObject.w_PESEOK = .T.
        this.w_TMPC = "Procedura di Conversione eseguita Correttamente."
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        this.oParentObject.w_pMSG = this.w_TMPC
      case this.w_CONV>=0 AND this.w_NHF>=0 AND this.w_TMPNE>0
        this.oParentObject.w_PESEOK = .T.
        this.w_TMPC = "Procedura di Conversione Terminata Con Warning!"
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        this.oParentObject.w_pMSG = "Procedura di Conversione Terminata con Warning"+chr(13)+chr(10)
        this.oParentObject.w_pMSG = this.oParentObject.w_pMSG + "WARNING! - Non � stato possibile aggiornare tutte le dichiarazioni"
    endcase
    return
  proc Try_03601500()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Inserisce il dettaglio delle dichiarazioni di produzione
    * --- Insert into MAT_PROD
    i_nConn=i_TableProp[this.MAT_PROD_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MAT_PROD_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.TMPMODL_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable+" where MPSERIAL = "+cp_ToStrODBC(this.w_MPSERIAL)+"",this.MAT_PROD_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    this.w_ROWS = i_Rows
    if this.w_ROWS > 0
      * --- Inserisco i seriali nella tabella di appoggio
      * --- Insert into DELTA
      i_nConn=i_TableProp[this.DELTA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DELTA_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DELTA_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"DPSERIAL"+",DPDATREG"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_MPSERIAL),'DELTA','DPSERIAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DPDATREG),'DELTA','DPDATREG');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'DPSERIAL',this.w_MPSERIAL,'DPDATREG',this.w_DPDATREG)
        insert into (i_cTable) (DPSERIAL,DPDATREG &i_ccchkf. );
           values (;
             this.w_MPSERIAL;
             ,this.w_DPDATREG;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    this.w_CONV = this.w_CONV+1
    if this.w_NHF>=0
      this.w_TMPC = "Aggiornamento dischiarazione " + this.w_MPSERIAL
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,10)]
    this.cWorkTables[1]='COM_VARI'
    this.cWorkTables[2]='DISTBASE'
    this.cWorkTables[3]='MAT_PROD'
    this.cWorkTables[4]='PAR_PROD'
    this.cWorkTables[5]='DIC_PROD'
    this.cWorkTables[6]='TIP_DOCU'
    this.cWorkTables[7]='*TMPMODL'
    this.cWorkTables[8]='*DELTA'
    this.cWorkTables[9]='AZIENDA'
    this.cWorkTables[10]='CONVERSI'
    return(this.OpenAllTables(10))

  proc CloseCursors()
    if used('_Curs_GSCVBB686')
      use in _Curs_GSCVBB686
    endif
    if used('_Curs_GSCVBB685')
      use in _Curs_GSCVBB685
    endif
    if used('_Curs_TMPMODL')
      use in _Curs_TMPMODL
    endif
    if used('_Curs_MAT_PROD')
      use in _Curs_MAT_PROD
    endif
    if used('_Curs_DELTA')
      use in _Curs_DELTA
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
