* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscv_b92                                                        *
*              Aggiornamento data reg. scadenze diverse                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_23]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-12-02                                                      *
* Last revis.: 2002-12-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscv_b92",oParentObject,m.w_NHF)
return(i_retval)

define class tgscv_b92 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_DATDOC = ctod("  /  /  ")
  w_DATMIN = ctod("  /  /  ")
  w_CODICE = space(10)
  * --- WorkFile variables
  SCA_VARI_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questa procedura di conversione assegna il valore del campo SCDATREG
    * --- Try
    local bErr_0361E390
    bErr_0361E390=bTrsErr
    this.Try_0361E390()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE - impossibile aggiornare SCDATREG su tabella SCA_VARI" )
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_0361E390
    * --- End
  endproc
  proc Try_0361E390()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Aggiornamento  Data Registrazione Scadenze Diverse
    * --- Select from gscv_b92
    do vq_exec with 'gscv_b92',this,'_Curs_gscv_b92','',.f.,.t.
    if used('_Curs_gscv_b92')
      select _Curs_gscv_b92
      locate for 1=1
      do while not(eof())
      this.w_DATMIN = CP_TODATE(_Curs_gscv_b92.PTDATSCA)
      this.w_DATDOC = CP_TODATE(_Curs_gscv_b92.SCDATDOC)
      this.w_DATDOC = IIF(NOT EMPTY(this.w_DATDOC),this.w_DATDOC,this.w_DATMIN)
      this.w_CODICE = _Curs_gscv_b92.SCCODICE
      * --- Write into SCA_VARI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.SCA_VARI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SCA_VARI_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SCA_VARI_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"SCDATREG ="+cp_NullLink(cp_ToStrODBC(this.w_DATDOC),'SCA_VARI','SCDATREG');
            +i_ccchkf ;
        +" where ";
            +"SCCODICE = "+cp_ToStrODBC(this.w_CODICE);
            +" and SCCODSEC = "+cp_ToStrODBC(-1);
               )
      else
        update (i_cTable) set;
            SCDATREG = this.w_DATDOC;
            &i_ccchkf. ;
         where;
            SCCODICE = this.w_CODICE;
            and SCCODSEC = -1;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
        select _Curs_gscv_b92
        continue
      enddo
      use
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Aggiornamento campo SCDATREG su tabella SCA_VARI eseguito con successo")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = this.oParentObject.w_PMSG + this.w_TMPC 
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='SCA_VARI'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_gscv_b92')
      use in _Curs_gscv_b92
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
