* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscv_b96                                                        *
*              Aggiorna campo stato movimento nei mov. provvigione             *
*                                                                              *
*      Author: ZUCCHETTI TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_112]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-05-06                                                      *
* Last revis.: 2002-05-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscv_b96",oParentObject,m.w_NHF)
return(i_retval)

define class tgscv_b96 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_MESS = space(100)
  w_SERIAL = space(10)
  * --- WorkFile variables
  MOP_MAST_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questa procedura Aggiorna il campo Stato movimento nella manutenzione Provvigioni 
    * --- FIle di LOG
    * --- Try
    local bErr_0360B4E0
    bErr_0360B4E0=bTrsErr
    this.Try_0360B4E0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE GENERICO - impossibile aggiornare i movimenti di provvigione")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_0360B4E0
    * --- End
  endproc
  proc Try_0360B4E0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    vq_exec("..\pcon\exe\query\gsekqbin.vqr",this,"PROVV")
    if USED("PROVV")
      if Reccount ("PROVV")>0
        SELECT PROVV
        GO TOP
        SCAN 
        this.w_SERIAL = NVL(MPSERIAL,SPACE(10))
        * --- Write into MOP_MAST
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.MOP_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MOP_MAST_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.MOP_MAST_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MPAGSOSP ="+cp_NullLink(cp_ToStrODBC("C"),'MOP_MAST','MPAGSOSP');
              +i_ccchkf ;
          +" where ";
              +"MPSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                 )
        else
          update (i_cTable) set;
              MPAGSOSP = "C";
              &i_ccchkf. ;
           where;
              MPSERIAL = this.w_SERIAL;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        SELECT PROVV
        ENDSCAN
        * --- Esecuzione ok
        this.oParentObject.w_PESEOK = .T.
        this.oParentObject.w_PMSG = ah_Msgformat("Conversione eseguita correttamente")
      else
        * --- Gestisce log errori
        if this.w_NHF>=0
          this.w_TMPC = ah_Msgformat("ATTENZIONE: non esistono dati da convertire")
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
        * --- Esecuzione ok
        this.oParentObject.w_PESEOK = .T.
        this.oParentObject.w_PMSG = this.w_TMPC
      endif
      * --- Chiude cursore
      SELECT PROVV
      USE
    endif
    if bTrsErr
      * --- Raise
      i_Error=MESSAGE()
      return
    else
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("Elaborazione terminata con successo")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='MOP_MAST'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
