* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcb56                                                        *
*              Modifica del cpwarn - tabella FRA_MODE                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-01-14                                                      *
* Last revis.: 2009-09-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcb56",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcb56 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TMPC = space(10)
  w_TEMP = space(10)
  w_PROGRESS = 0
  w_COMPANYLIST = space(200)
  w_COMPANY = space(5)
  w_NEWCODE = space(50)
  w_OLDCODE = space(50)
  w_DAESEGUI = space(1)
  w_CODE = space(50)
  * --- WorkFile variables
  cpwarn_idx=0
  AZIENDA_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Modifica il progressivo di FRA_MODE in multiaziendale correggendo un progressivo incongruente
    *     La tabella FRA_MODE difatti ha gi� il check multicompany
    this.w_DAESEGUI = "N"
    this.w_OLDCODE = ALLTRIM("prog\FRAMO")
    * --- Try
    local bErr_035FD150
    bErr_035FD150=bTrsErr
    this.Try_035FD150()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_035FD150
    * --- End
    if this.w_DAESEGUI="S"
      * --- Try
      local bErr_035D1680
      bErr_035D1680=bTrsErr
      this.Try_035D1680()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        * --- Gestisce log errori
        this.oParentObject.w_PMSG = Message()
        this.oParentObject.w_PESEOK = .F.
        if this.w_NHF>=0
          this.w_TMPC = this.oParentObject.w_PMSG
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
      bTrsErr=bTrsErr or bErr_035D1680
      * --- End
    else
      * --- Esecuzione ok
      this.oParentObject.w_PESEOK = .T.
      this.oParentObject.w_PMSG = ah_Msgformat("Conversione gi� eseguita")
      if this.w_NHF>=0
        this.w_TMPC = this.oParentObject.w_PMSG
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
  endproc
  proc Try_035FD150()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Read from cpwarn
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.cpwarn_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.cpwarn_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "tablecode"+;
        " from "+i_cTable+" cpwarn where ";
            +"tablecode = "+cp_ToStrODBC(this.w_OLDCODE);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        tablecode;
        from (i_cTable) where;
            tablecode = this.w_OLDCODE;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CODE = NVL(cp_ToDate(_read_.tablecode),cp_NullValue(_read_.tablecode))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if i_ROWS>0
      this.w_DAESEGUI = "S"
    endif
    return
  proc Try_035D1680()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    this.w_PROGRESS = 0
    * --- Select from cpwarn
    i_nConn=i_TableProp[this.cpwarn_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.cpwarn_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" cpwarn ";
          +" where tablecode like '%FRAMO%'";
           ,"_Curs_cpwarn")
    else
      select * from (i_cTable);
       where tablecode like "%FRAMO%";
        into cursor _Curs_cpwarn
    endif
    if used('_Curs_cpwarn')
      select _Curs_cpwarn
      locate for 1=1
      do while not(eof())
      this.w_PROGRESS = _Curs_cpwarn.autonum
        select _Curs_cpwarn
        continue
      enddo
      use
    endif
    if this.w_PROGRESS<>0
      * --- Creo dei progressivi in cpwarn per ogni azienda che abbia record nella tabella FRA_MODE
      this.w_COMPANYLIST = ""
      * --- Select from AZIENDA
      i_nConn=i_TableProp[this.AZIENDA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select AZCODAZI  from "+i_cTable+" AZIENDA ";
             ,"_Curs_AZIENDA")
      else
        select AZCODAZI from (i_cTable);
          into cursor _Curs_AZIENDA
      endif
      if used('_Curs_AZIENDA')
        select _Curs_AZIENDA
        locate for 1=1
        do while not(eof())
        this.w_COMPANYLIST = this.w_COMPANYLIST + IIF(NOT EMPTY(this.w_COMPANYLIST),",","") + ALLTRIM(_Curs_AZIENDA.AZCODAZI)
          select _Curs_AZIENDA
          continue
        enddo
        use
      endif
      VQ_EXEC("..\PCON\EXE\QUERY\GSCVCB56", this, "FRAMODE")
      SELECT * FROM FRAMODE INTO CURSOR FRARAG GROUP BY CP_COMPANY
      SELECT FRARAG 
 GO TOP 
 SCAN
      this.w_COMPANY = ALLTRIM(NVL(CP_COMPANY, SPACE(5)))
      this.w_NEWCODE = "prog\FRAMO\'"+TRIM(this.w_COMPANY)+"'"
      * --- Insert into cpwarn
      i_nConn=i_TableProp[this.cpwarn_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.cpwarn_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.cpwarn_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"tablecode"+",autonum"+",warncode"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_NEWCODE),'cpwarn','tablecode');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PROGRESS),'cpwarn','autonum');
        +","+cp_NullLink(cp_ToStrODBC(this.w_COMPANY),'cpwarn','warncode');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'tablecode',this.w_NEWCODE,'autonum',this.w_PROGRESS,'warncode',this.w_COMPANY)
        insert into (i_cTable) (tablecode,autonum,warncode &i_ccchkf. );
           values (;
             this.w_NEWCODE;
             ,this.w_PROGRESS;
             ,this.w_COMPANY;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      ENDSCAN
      * --- Elimino il vecchio record nei progressivi
      * --- Delete from cpwarn
      i_nConn=i_TableProp[this.cpwarn_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.cpwarn_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"tablecode = "+cp_ToStrODBC(this.w_OLDCODE);
               )
      else
        delete from (i_cTable) where;
              tablecode = this.w_OLDCODE;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      if used("FRAMODE")
         
 SELECT FRAMODE 
 USE
      endif
      if used("FRARAG")
         
 SELECT FRARAG 
 USE
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    this.oParentObject.w_PESEOK = .T.
    this.w_TMPC = ah_Msgformat("Inserimento in cpwarn dei progressivi di FRA_MODE avvenuto correttamente")
    this.oParentObject.w_PMSG = ah_Msgformat("Conversione eseguita correttamente")
    if this.w_NHF>=0
      this.w_TMPC = this.oParentObject.w_PMSG
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='cpwarn'
    this.cWorkTables[2]='AZIENDA'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_cpwarn')
      use in _Curs_cpwarn
    endif
    if used('_Curs_AZIENDA')
      use in _Curs_AZIENDA
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
