* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcc09                                                        *
*              Aggiorna valore del campo elcodtab                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-02-02                                                      *
* Last revis.: 2012-02-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcc09",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcc09 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TMPC = space(10)
  w_TEMP = space(10)
  * --- WorkFile variables
  VAELEMEN_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questa procedura Aggiorna il campo ELCODTAB nella tabella VAELEMEN
    * --- FIle di LOG
    * --- Try
    local bErr_0381A8A8
    bErr_0381A8A8=bTrsErr
    this.Try_0381A8A8()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE %1 - impossibile aggiornare gli elementi", this.oParentObject.w_PMSG )
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_0381A8A8
    * --- End
  endproc
  proc Try_0381A8A8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Write into VAELEMEN
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.VAELEMEN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VAELEMEN_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="ELCODSTR,ELCODICE"
      do vq_exec with 'gscv_c09',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.VAELEMEN_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="VAELEMEN.ELCODSTR = _t2.ELCODSTR";
              +" and "+"VAELEMEN.ELCODICE = _t2.ELCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ELCODTAB = _t2.ELTABIMP";
          +i_ccchkf;
          +" from "+i_cTable+" VAELEMEN, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="VAELEMEN.ELCODSTR = _t2.ELCODSTR";
              +" and "+"VAELEMEN.ELCODICE = _t2.ELCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" VAELEMEN, "+i_cQueryTable+" _t2 set ";
          +"VAELEMEN.ELCODTAB = _t2.ELTABIMP";
          +Iif(Empty(i_ccchkf),"",",VAELEMEN.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="VAELEMEN.ELCODSTR = t2.ELCODSTR";
              +" and "+"VAELEMEN.ELCODICE = t2.ELCODICE";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" VAELEMEN set (";
          +"ELCODTAB";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.ELTABIMP";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="VAELEMEN.ELCODSTR = _t2.ELCODSTR";
              +" and "+"VAELEMEN.ELCODICE = _t2.ELCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" VAELEMEN set ";
          +"ELCODTAB = _t2.ELTABIMP";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".ELCODSTR = "+i_cQueryTable+".ELCODSTR";
              +" and "+i_cTable+".ELCODICE = "+i_cQueryTable+".ELCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ELCODTAB = (select ELTABIMP from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = this.w_TMPC
    * --- commit
    cp_EndTrs(.t.)
    this.w_TMPC = ah_Msgformat("Aggiornamento valori campo ELCODTAB nella tabella VAELEMEN")
    this.oParentObject.w_PMSG = ah_Msgformat("Conversione eseguita correttamente.")
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='VAELEMEN'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
