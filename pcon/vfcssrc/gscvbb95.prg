* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvbb95                                                        *
*              Aumenta la lunghezza delle ragioni sociali                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-05-21                                                      *
* Last revis.: 2009-02-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvbb95",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvbb95 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_MESS = space(100)
  w_GOON = .f.
  * --- WorkFile variables
  RIPATMP2_idx=0
  MODCPAG_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Procedura di conversione per DB2 (gli altri database riescono ad allargare i campi carattere senza bisogno di procedure di conversione)
    *     I campi allargati sono la reagione sociale nelle tabelle CONTI,AZIENDA,  CLI_VEND (� in gpos) 
    * --- variabile da testare
    this.w_GOON = .T.
    this.oParentObject.w_PESEOK = .T.
    * --- Se il daabase gestisce i varchar non serve la procedura di conversione
     
 i_nConn=i_TableProp[this.MODCPAG_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.MODCPAG_idx,2])
    A=SQLEXEC(i_nConn,"Select  CFCODFIS from "+i_cTable + " where 1=0","CURS")
    afields(DATI2,"CURS")
    if Used("CURS")
      Select ("CURS") 
 use
    endif
    if DATI2(1,3)=20
      this.w_GOON = .F.
    endif
    if this.w_GOON
      * --- Try
      local bErr_03628690
      bErr_03628690=bTrsErr
      this.Try_03628690()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- Gestisce log errori
        this.oParentObject.w_PMSG = Message()
        this.oParentObject.w_PESEOK = .F.
        if this.w_NHF>=0
          this.w_TMPC = this.oParentObject.w_PMSG
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
      bTrsErr=bTrsErr or bErr_03628690
      * --- End
    else
      this.oParentObject.w_PMSG = "La procedura di conversione non � stata eseguita poich� non pi� necessaria"
      this.w_TMPC = this.oParentObject.w_PMSG
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
  endproc
  proc Try_03628690()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Esecuzione ok
    if upper(CP_DBTYPE)="DB2"
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    this.oParentObject.w_PMSG = "Elaborazione terminata con successo"
    if this.w_NHF>=0
      this.w_TMPC = this.oParentObject.w_PMSG
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella DB2
    i_nConn=i_TableProp[this.MODCPAG_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.MODCPAG_idx,2])
    pName="MODCPAG"
    pDatabaseType=i_ServerConn[1,6]
    * --- Ricostruisco  la tabella prima di copiarne il contenuto  (si evita il rischio che la tabella copiata abbia dei campi in meno della tabella ricostruita)
    GSCV_BRT(this, "MODCPAG" , i_nConn , .T.)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Create temporary table RIPATMP2
    i_nIdx=cp_AddTableDef('RIPATMP2') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.MODCPAG_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.MODCPAG_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
          )
    this.RIPATMP2_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    wait window "Creata tabella temporanea di appoggio" NOWAIT
    * --- Elimino la tabella
    if Not GSCV_BDT( this, "MODCPAG" , i_nConn )
      * --- Raise
      i_Error="Error"
      return
    endif
    * --- Ricostruisco il database per la tabella
    GSCV_BRT(this, "MODCPAG" , i_nConn , .T.)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into MODCPAG
    i_nConn=i_TableProp[this.MODCPAG_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MODCPAG_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP2_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.MODCPAG_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP2
    i_nIdx=cp_GetTableDefIdx('RIPATMP2')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP2')
    endif
    wait window "Eliminata tabella temporanea TMP" NOWAIT
  endproc


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='*RIPATMP2'
    this.cWorkTables[2]='MODCPAG'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
