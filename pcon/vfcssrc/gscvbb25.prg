* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvbb25                                                        *
*              Aggiorna campo lmcodpia                                         *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-03-17                                                      *
* Last revis.: 2006-12-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvbb25",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvbb25 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TMPC = space(10)
  w_TEMP = space(10)
  w_NUMREC = 0
  w_CODPIAD = space(4)
  w_TIPCON = space(1)
  w_CODCON = space(15)
  w_CODSTU = space(6)
  * --- WorkFile variables
  STU_PARA_idx=0
  STUMPIAC_idx=0
  CONTI_idx=0
  STU_PIAC_idx=0
  DETTPDC_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna il campo codice Piano dei Conti LMCODPIA
    * --- FIle di LOG
    * --- Se il modulo LEMC � installato ma non attivato in dati Azienda, esco senza fare nulla
    if g_TRAEXP="N"
      if this.w_NHF>=0
        this.w_TMPC = "  "
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
      this.oParentObject.w_PESEOK = .T.
      this.oParentObject.w_PMSG = ah_msgformat("Conversione non eseguita, poich� tipologia trasferimento studio non selezionata")
      i_retcode = 'stop'
      return
    endif
    * --- Verifico se � presente un codice Piano  dei Conti in anagrafica "Trasferimetno studio"
    * --- Read from STU_PARA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.STU_PARA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STU_PARA_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "LMCODPDC"+;
        " from "+i_cTable+" STU_PARA where ";
            +"LMCODAZI = "+cp_ToStrODBC(i_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        LMCODPDC;
        from (i_cTable) where;
            LMCODAZI = i_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CODPIAD = NVL(cp_ToDate(_read_.LMCODPDC),cp_NullValue(_read_.LMCODPDC))
      use
    else
      * --- Error: sql sentence error.
      i_Error = 'Errore lettura codice Piano dei Conti'
      return
    endif
    select (i_nOldArea)
    if empty(this.w_CODPIAD)
      * --- Interrompo l'esecuzione del programma
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = ah_msgformat("Inserire un codice per il piano dei conti usato in tabella trasferimento studio")
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_msgformat("Inserire un codice per il piano dei conti usato in tabella trasferimento studio")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
      i_retcode = 'stop'
      return
    endif
    * --- Try
    local bErr_04A9DF80
    bErr_04A9DF80=bTrsErr
    this.Try_04A9DF80()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_msgformat("ERRORE GENERICO - Impossibile aggiornare tabella %1", "STUMPIAC ")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_04A9DF80
    * --- End
    if used("PIANI")
      select PIANI 
 use
    endif
  endproc
  proc Try_04A9DF80()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Selezione dei Piano dei Conti da aggiornare 
    Vq_Exec("GSCVBB25",this,"PIANI")
    this.w_NUMREC = reccount()
    * --- Verifico divergenza fra codice di default attribuito al PdC COGEN ed il nuovo codice del PdC
    if this.w_CODPIAD == "0000" and this.w_NUMREC >= 2
      Select PIANI
      locate for LMPROGRE="@@@@@@@@"
      if found()
        * --- Interrompo l'esecuzione del programma
        * --- Gestisce log errori
        this.oParentObject.w_PMSG = ah_msgformat("Codice per il piano dei conti usato in tabella trasferimento studio' non valido! Specificarne uno diverso da '0000'")
        this.oParentObject.w_PESEOK = .F.
        if this.w_NHF>=0
          this.w_TMPC = ah_msgformat("Codice per il piano dei conti usato in Tabella trasferimento studio' non valido! Specificarne uno diverso da '0000'")
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
        i_retcode = 'stop'
        return
      endif
    endif
    if used("PIANI") 
      do case
        case this.w_NUMREC = 1
          * --- Aggiornamento campo LMCODPIA per il Piano dei conti presente in azienda
          * --- Write into STUMPIAC
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.STUMPIAC_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.STUMPIAC_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.STUMPIAC_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"LMCODPIA ="+cp_NullLink(cp_ToStrODBC(this.w_CODPIAD),'STUMPIAC','LMCODPIA');
                +i_ccchkf ;
            +" where ";
                +"1 = "+cp_ToStrODBC(1);
                   )
          else
            update (i_cTable) set;
                LMCODPIA = this.w_CODPIAD;
                &i_ccchkf. ;
             where;
                1 = 1;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        case this.w_NUMREC >= 2
          * --- Devo leggere il codice da dare al vecchio Piano dei Conti
          *     Devo differenziare i due aggiornamenti (Vecchio/Nuovo Piano dei Conti)
          * --- Aggiornamento campo LMCODPIA per i Piano dei conti presenti in azienda
          * --- Write into STUMPIAC
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.STUMPIAC_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.STUMPIAC_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.STUMPIAC_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"LMCODPIA ="+cp_NullLink(cp_ToStrODBC("0000"),'STUMPIAC','LMCODPIA');
                +i_ccchkf ;
            +" where ";
                +"LMPROGRE = "+cp_ToStrODBC("@@@@@@@@");
                   )
          else
            update (i_cTable) set;
                LMCODPIA = "0000";
                &i_ccchkf. ;
             where;
                LMPROGRE = "@@@@@@@@";

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          * --- Write into STUMPIAC
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.STUMPIAC_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.STUMPIAC_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.STUMPIAC_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"LMCODPIA ="+cp_NullLink(cp_ToStrODBC(this.w_CODPIAD),'STUMPIAC','LMCODPIA');
                +i_ccchkf ;
            +" where ";
                +"LMPROGRE <> "+cp_ToStrODBC("@@@@@@@@");
                   )
          else
            update (i_cTable) set;
                LMCODPIA = this.w_CODPIAD;
                &i_ccchkf. ;
             where;
                LMPROGRE <> "@@@@@@@@";

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
      endcase
    endif
    * --- Modifico il codice sottoconto se composto da 5 caratteri,  anteponendo uno '0'.
    *     Solo per le tabelle CONTI e STUMPIAC.
    * --- Write into CONTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.CONTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="ANTIPCON,ANCODICE"
      do vq_exec with 'GSCVBB25_1',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTI_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="CONTI.ANTIPCON = _t2.ANTIPCON";
              +" and "+"CONTI.ANCODICE = _t2.ANCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ANCODSTU = _t2.ANCODSTU";
          +i_ccchkf;
          +" from "+i_cTable+" CONTI, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="CONTI.ANTIPCON = _t2.ANTIPCON";
              +" and "+"CONTI.ANCODICE = _t2.ANCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CONTI, "+i_cQueryTable+" _t2 set ";
          +"CONTI.ANCODSTU = _t2.ANCODSTU";
          +Iif(Empty(i_ccchkf),"",",CONTI.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="CONTI.ANTIPCON = t2.ANTIPCON";
              +" and "+"CONTI.ANCODICE = t2.ANCODICE";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CONTI set (";
          +"ANCODSTU";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.ANCODSTU";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="CONTI.ANTIPCON = _t2.ANTIPCON";
              +" and "+"CONTI.ANCODICE = _t2.ANCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CONTI set ";
          +"ANCODSTU = _t2.ANCODSTU";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".ANTIPCON = "+i_cQueryTable+".ANTIPCON";
              +" and "+i_cTable+".ANCODICE = "+i_cQueryTable+".ANCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ANCODSTU = (select ANCODSTU from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    if upper(CP_DBTYPE)="DB2"
      * --- Caso DB2
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      * --- Write into STU_PIAC
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.STU_PIAC_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.STU_PIAC_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="LMCODCON,LMPROGRE,LMCODSOT"
        do vq_exec with 'GSCVBB25_2',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.STU_PIAC_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="STU_PIAC.LMCODCON = _t2.LMCODCON";
                +" and "+"STU_PIAC.LMPROGRE = _t2.LMPROGRE";
                +" and "+"STU_PIAC.LMCODSOT = _t2.LMCODSOT";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"LMCODSOT = _t2.LMCODSOT2";
            +i_ccchkf;
            +" from "+i_cTable+" STU_PIAC, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="STU_PIAC.LMCODCON = _t2.LMCODCON";
                +" and "+"STU_PIAC.LMPROGRE = _t2.LMPROGRE";
                +" and "+"STU_PIAC.LMCODSOT = _t2.LMCODSOT";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" STU_PIAC, "+i_cQueryTable+" _t2 set ";
            +"STU_PIAC.LMCODSOT = _t2.LMCODSOT2";
            +Iif(Empty(i_ccchkf),"",",STU_PIAC.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="STU_PIAC.LMCODCON = t2.LMCODCON";
                +" and "+"STU_PIAC.LMPROGRE = t2.LMPROGRE";
                +" and "+"STU_PIAC.LMCODSOT = t2.LMCODSOT";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" STU_PIAC set (";
            +"LMCODSOT";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.LMCODSOT2";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="STU_PIAC.LMCODCON = _t2.LMCODCON";
                +" and "+"STU_PIAC.LMPROGRE = _t2.LMPROGRE";
                +" and "+"STU_PIAC.LMCODSOT = _t2.LMCODSOT";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" STU_PIAC set ";
            +"LMCODSOT = _t2.LMCODSOT2";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".LMCODCON = "+i_cQueryTable+".LMCODCON";
                +" and "+i_cTable+".LMPROGRE = "+i_cQueryTable+".LMPROGRE";
                +" and "+i_cTable+".LMCODSOT = "+i_cQueryTable+".LMCODSOT";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"LMCODSOT = (select LMCODSOT2 from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- Allineo anche gli estremi intervallo riportati nel conto
    * --- Write into STUMPIAC
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.STUMPIAC_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STUMPIAC_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="LMCODCON,LMPROGRE"
      do vq_exec with 'GSCVBB25_4',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.STUMPIAC_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="STUMPIAC.LMCODCON = _t2.LMCODCON";
              +" and "+"STUMPIAC.LMPROGRE = _t2.LMPROGRE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"LMMINCON = _t2.MINCON";
          +",LMMAXCON = _t2.MAXCON";
          +i_ccchkf;
          +" from "+i_cTable+" STUMPIAC, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="STUMPIAC.LMCODCON = _t2.LMCODCON";
              +" and "+"STUMPIAC.LMPROGRE = _t2.LMPROGRE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" STUMPIAC, "+i_cQueryTable+" _t2 set ";
          +"STUMPIAC.LMMINCON = _t2.MINCON";
          +",STUMPIAC.LMMAXCON = _t2.MAXCON";
          +Iif(Empty(i_ccchkf),"",",STUMPIAC.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="STUMPIAC.LMCODCON = t2.LMCODCON";
              +" and "+"STUMPIAC.LMPROGRE = t2.LMPROGRE";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" STUMPIAC set (";
          +"LMMINCON,";
          +"LMMAXCON";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.MINCON,";
          +"t2.MAXCON";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="STUMPIAC.LMCODCON = _t2.LMCODCON";
              +" and "+"STUMPIAC.LMPROGRE = _t2.LMPROGRE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" STUMPIAC set ";
          +"LMMINCON = _t2.MINCON";
          +",LMMAXCON = _t2.MAXCON";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".LMCODCON = "+i_cQueryTable+".LMCODCON";
              +" and "+i_cTable+".LMPROGRE = "+i_cQueryTable+".LMPROGRE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"LMMINCON = (select MINCON from "+i_cQueryTable+" where "+i_cWhere+")";
          +",LMMAXCON = (select MAXCON from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    if this.w_NHF>=0
      this.w_TMPC = ah_msgformat("Aggiornamento campo LMCODPIA tabella STUMPIAC avvenuto correttamente")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = ah_msgformat("Conversione eseguita correttamente")
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiornamento campo in DB2
    * --- Create temporary table DETTPDC
    i_nIdx=cp_AddTableDef('DETTPDC') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('GSCVBB25_3',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.DETTPDC_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    wait window "Creata tabella temporanea di appoggio" NOWAIT
    i_nConn=i_TableProp[this.STU_PIAC_idx,3]
    * --- Elimino la tabella
    if Not GSCV_BDT( this, "STU_PIAC" , i_nConn )
      * --- Raise
      i_Error="Error"
      return
    endif
    * --- Ricostruisco il database per la tabella
    GSCV_BRT(this, "STU_PIAC" , i_nConn , .T.)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Try
    local bErr_03894D18
    bErr_03894D18=bTrsErr
    this.Try_03894D18()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_03894D18
    * --- End
    wait window "Ripristinati valori di origine" NOWAIT
    * --- Elimina tabella TMP
    * --- Drop temporary table DETTPDC
    i_nIdx=cp_GetTableDefIdx('DETTPDC')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('DETTPDC')
    endif
  endproc
  proc Try_03894D18()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into STU_PIAC
    i_nConn=i_TableProp[this.STU_PIAC_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STU_PIAC_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.DETTPDC_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.STU_PIAC_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='STU_PARA'
    this.cWorkTables[2]='STUMPIAC'
    this.cWorkTables[3]='CONTI'
    this.cWorkTables[4]='STU_PIAC'
    this.cWorkTables[5]='*DETTPDC'
    return(this.OpenAllTables(5))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
