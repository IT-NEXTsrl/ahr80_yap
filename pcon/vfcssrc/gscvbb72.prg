* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvbb72                                                        *
*              Aggiorna flag movimenti C\C                                     *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-03-17                                                      *
* Last revis.: 2007-05-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvbb72",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvbb72 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TMPC = space(10)
  w_TEMP = space(10)
  * --- WorkFile variables
  CCM_DETT_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna flag valorizzati in modo errato rispetto alla causale di C\C
    * --- FIle di LOG
    * --- Try
    local bErr_038BA278
    bErr_038BA278=bTrsErr
    this.Try_038BA278()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = Ah_MsgFormat("ERRORE GENERICO - Impossibile aggiornare tabella %1", "CCM_DETT")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_038BA278
    * --- End
  endproc
  proc Try_038BA278()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Aggiorno Flag
    * --- Write into CCM_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.CCM_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CCM_DETT_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="CCSERIAL,CCROWRIF,CPROWNUM"
      do vq_exec with 'gscvbb72',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.CCM_DETT_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="CCM_DETT.CCSERIAL = _t2.CCSERIAL";
              +" and "+"CCM_DETT.CCROWRIF = _t2.CCROWRIF";
              +" and "+"CCM_DETT.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CCFLCRED = _t2.CCFLCRED";
          +",CCFLDEBI = _t2.CCFLDEBI";
          +i_ccchkf;
          +" from "+i_cTable+" CCM_DETT, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="CCM_DETT.CCSERIAL = _t2.CCSERIAL";
              +" and "+"CCM_DETT.CCROWRIF = _t2.CCROWRIF";
              +" and "+"CCM_DETT.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CCM_DETT, "+i_cQueryTable+" _t2 set ";
          +"CCM_DETT.CCFLCRED = _t2.CCFLCRED";
          +",CCM_DETT.CCFLDEBI = _t2.CCFLDEBI";
          +Iif(Empty(i_ccchkf),"",",CCM_DETT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="CCM_DETT.CCSERIAL = t2.CCSERIAL";
              +" and "+"CCM_DETT.CCROWRIF = t2.CCROWRIF";
              +" and "+"CCM_DETT.CPROWNUM = t2.CPROWNUM";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CCM_DETT set (";
          +"CCFLCRED,";
          +"CCFLDEBI";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.CCFLCRED,";
          +"t2.CCFLDEBI";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="CCM_DETT.CCSERIAL = _t2.CCSERIAL";
              +" and "+"CCM_DETT.CCROWRIF = _t2.CCROWRIF";
              +" and "+"CCM_DETT.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CCM_DETT set ";
          +"CCFLCRED = _t2.CCFLCRED";
          +",CCFLDEBI = _t2.CCFLDEBI";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".CCSERIAL = "+i_cQueryTable+".CCSERIAL";
              +" and "+i_cTable+".CCROWRIF = "+i_cQueryTable+".CCROWRIF";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CCFLCRED = (select CCFLCRED from "+i_cQueryTable+" where "+i_cWhere+")";
          +",CCFLDEBI = (select CCFLDEBI from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    if this.w_NHF>=0
      this.w_TMPC = Ah_MsgFormat("Aggiornamento campi CCFLCRED,CCFLDEBI avvenuto correttamente")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = Ah_MsgFormat("Conversione eseguita correttamente")
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='CCM_DETT'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
