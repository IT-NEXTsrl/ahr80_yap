* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvab40                                                        *
*              Conversione coeff impiego distinta base                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_23]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-10-27                                                      *
* Last revis.: 2004-10-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvab40",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvab40 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TMPNE = 0
  w_CONV = 0
  w_TMPC = space(10)
  w_TEMP = space(10)
  w_TBLNAME = space(10)
  w_OPERAZ = space(1)
  w_CORIFDIS = space(41)
  w_COCODCOM = space(41)
  w_CPROWNUM = 0
  w_CPROWORD = 0
  w_OLDCOEUM1 = 0
  w_COCOEUM1 = 0
  w_COCOEIMP = 0
  w_UNMIS3 = space(3)
  w_UNMIS1 = space(3)
  w_UNMIS2 = space(3)
  w_COUNMIS = space(3)
  w_OPERAT = space(1)
  w_OPERA3 = space(1)
  w_MOLTIP = 0
  w_MOLTI3 = 0
  w_DBDESCOM = space(40)
  w_CVNUMRIF = 0
  w_DBROWORD = 0
  * --- WorkFile variables
  COM_VARI_idx=0
  DISTBASE_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- FIle di LOG
    * --- Try
    local bErr_0361DD90
    bErr_0361DD90=bTrsErr
    this.Try_0361DD90()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_pMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_msgformat("ERRORE - %1", Message() )
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_0361DD90
    * --- End
    if Used ("TmpLog")
      Use in TmpLog
    endif
    if Used ("__tmp__")
      Use in __tmp__
    endif
  endproc
  proc Try_0361DD90()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.oParentObject.w_PESEOK = .T.
    if Used ("TmpLog")
      Use in TmpLog
    endif
    CREATE CURSOR TmpLog (TBLNAME C(10), CORIFDIS C(41),; 
 CVNUMRIF C(4), DBROWORD C(5), DBDESCOM C(40),; 
 COCODCOM C(41), NUMRIG C(10), OLDCOEUM N(12,5),; 
 NEWCOEUM N(12,5), TMESSAGE Memo, TOPERAZ C(1))
    this.w_TMPNE = 0
    * --- begin transaction
    cp_BeginTrs()
    this.w_TBLNAME = "DISTBASE"
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Aggiornamento componenti tabella DISTBASE")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    ah_Msg("Aggiornamento coeff. di impiego componenti in corso...",.T.)
    this.w_DBDESCOM = SPACE(40)
    this.w_CVNUMRIF = 0
    this.w_DBROWORD = 0
    * --- Select from GSCVA1B40
    do vq_exec with 'GSCVA1B40',this,'_Curs_GSCVA1B40','',.f.,.t.
    if used('_Curs_GSCVA1B40')
      select _Curs_GSCVA1B40
      locate for 1=1
      do while not(eof())
      SELECT _Curs_GSCVA1B40
      this.w_CORIFDIS = NVL(DBCODICE, SPACE(41))
      this.w_COCODCOM = NVL(DBCODCOM, SPACE(41))
      this.w_CPROWNUM = NVL(CPROWNUM,0)
      this.w_CVNUMRIF = this.w_CPROWNUM
      this.w_CPROWORD = NVL(CPROWORD,0)
      this.w_COCOEIMP = NVL(DBQTADIS,0)
      this.w_OLDCOEUM1 = NVL(DBCOEUM1,0)
      this.w_UNMIS3 = NVL(UNMIS3, SPACE(3))
      this.w_UNMIS1 = NVL(UNMIS1, SPACE(3))
      this.w_UNMIS2 = NVL(UNMIS2, SPACE(3))
      this.w_COUNMIS = NVL(COUNMIS, SPACE(3))
      this.w_OPERAT = NVL(OPERAT, SPACE(1))
      this.w_OPERA3 = NVL(OPERA3, SPACE(1))
      this.w_MOLTIP = NVL(MOLTIP,0)
      this.w_MOLTI3 = NVL(MOLTI3, 0)
      if this.w_COUNMIS==this.w_UNMIS1 or this.w_COUNMIS==this.w_UNMIS2 or this.w_COUNMIS==this.w_UNMIS3
        this.w_COCOEUM1 = CALMMLIS(this.w_COCOEIMP,this.w_UNMIS1+this.w_UNMIS2+this.w_UNMIS3+this.w_COUNMIS+ this.w_OPERAT+this.w_OPERA3+" "+"D",this.w_MOLTIP,this.w_MOLTI3,0)
        if this.w_COCOEUM1>0
          if this.w_COCOEUM1 <> this.w_OLDCOEUM1
            * --- Write into DISTBASE
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.DISTBASE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DISTBASE_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.DISTBASE_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"DBCOEUM1 ="+cp_NullLink(cp_ToStrODBC(this.w_COCOEUM1),'DISTBASE','DBCOEUM1');
                  +i_ccchkf ;
              +" where ";
                  +"DBCODICE = "+cp_ToStrODBC(this.w_CORIFDIS);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
                  +" and DBCODCOM = "+cp_ToStrODBC(this.w_COCODCOM);
                     )
            else
              update (i_cTable) set;
                  DBCOEUM1 = this.w_COCOEUM1;
                  &i_ccchkf. ;
               where;
                  DBCODICE = this.w_CORIFDIS;
                  and CPROWNUM = this.w_CPROWNUM;
                  and DBCODCOM = this.w_COCODCOM;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            this.w_CONV = this.w_CONV+1
            this.w_OPERAZ = "I"
            ah_Msg("Aggiornamento componente %1 in corso...",.T.,.F.,.F., alltrim(this.w_COCODCOM) )
            this.w_TMPC = ah_Msgformat("Coefficiente di impiego aggiornato")
            this.Pag2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        else
          this.w_TMPNE = this.w_TMPNE + 1
          this.w_OPERAZ = "E"
          this.w_TMPC = ah_Msgformat("Coefficiente di impiego pari a zero")
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      else
        this.w_TMPNE = this.w_TMPNE + 1
        this.w_OPERAZ = "E"
        this.w_TMPC = ah_Msgformat("Unit� di misura incongruente")
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
        select _Curs_GSCVA1B40
        continue
      enddo
      use
    endif
    this.w_TBLNAME = "COM_VARI"
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Aggiornamento componenti tabella COM_VARI")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    ah_Msg("Aggiornamento coeff. di impiego componenti in corso...",.T.)
    * --- Select from GSCVA2B40
    do vq_exec with 'GSCVA2B40',this,'_Curs_GSCVA2B40','',.f.,.t.
    if used('_Curs_GSCVA2B40')
      select _Curs_GSCVA2B40
      locate for 1=1
      do while not(eof())
      SELECT _Curs_GSCVA2B40
      this.w_CORIFDIS = NVL(DBCODICE, SPACE(41))
      this.w_COCODCOM = NVL(DBCODCOM, SPACE(41))
      this.w_DBDESCOM = NVL(DBDESCOM, SPACE(40))
      this.w_CVNUMRIF = NVL(CVNUMRIF,0)
      this.w_DBROWORD = NVL(DBROWORD,0)
      this.w_CPROWNUM = NVL(CPROWNUM,0)
      this.w_CPROWORD = NVL(CPROWORD,0)
      this.w_COCOEIMP = NVL(DBQTADIS,0)
      this.w_OLDCOEUM1 = NVL(DBCOEUM1,0)
      this.w_UNMIS3 = NVL(UNMIS3, SPACE(3))
      this.w_UNMIS1 = NVL(UNMIS1, SPACE(3))
      this.w_UNMIS2 = NVL(UNMIS2, SPACE(3))
      this.w_COUNMIS = NVL(COUNMIS, SPACE(3))
      this.w_OPERAT = NVL(OPERAT, SPACE(1))
      this.w_OPERA3 = NVL(OPERA3, SPACE(1))
      this.w_MOLTIP = NVL(MOLTIP,0)
      this.w_MOLTI3 = NVL(MOLTI3, 0)
      if this.w_COUNMIS==this.w_UNMIS1 or this.w_COUNMIS==this.w_UNMIS2 or this.w_COUNMIS==this.w_UNMIS3
        this.w_COCOEUM1 = CALMMLIS(this.w_COCOEIMP,this.w_UNMIS1+this.w_UNMIS2+this.w_UNMIS3+this.w_COUNMIS+ this.w_OPERAT+this.w_OPERA3+" "+"D",this.w_MOLTIP,this.w_MOLTI3,0)
        if this.w_COCOEUM1>0
          if this.w_COCOEUM1 <> this.w_OLDCOEUM1
            * --- Write into COM_VARI
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.COM_VARI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.COM_VARI_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.COM_VARI_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"CVCOEUM1 ="+cp_NullLink(cp_ToStrODBC(this.w_COCOEUM1),'COM_VARI','CVCOEUM1');
                  +i_ccchkf ;
              +" where ";
                  +"CVCODICE = "+cp_ToStrODBC(this.w_CORIFDIS);
                  +" and CVNUMRIF = "+cp_ToStrODBC(this.w_CVNUMRIF);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
                  +" and CVCODCOM = "+cp_ToStrODBC(this.w_COCODCOM);
                     )
            else
              update (i_cTable) set;
                  CVCOEUM1 = this.w_COCOEUM1;
                  &i_ccchkf. ;
               where;
                  CVCODICE = this.w_CORIFDIS;
                  and CVNUMRIF = this.w_CVNUMRIF;
                  and CPROWNUM = this.w_CPROWNUM;
                  and CVCODCOM = this.w_COCODCOM;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            this.w_CONV = this.w_CONV+1
            this.w_OPERAZ = "I"
            ah_Msg("Aggiornamento componente %1 in corso...",.T.,.F.,.F., alltrim(this.w_COCODCOM) )
            this.w_TMPC = Ah_Msgformat("Coefficiente di impiego aggiornato")
            this.Pag2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        else
          this.w_TMPNE = this.w_TMPNE + 1
          this.w_OPERAZ = "E"
          this.w_TMPC = ah_Msgformat("Coefficiente di impiego pari a zero")
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      else
        this.w_TMPNE = this.w_TMPNE + 1
        this.w_OPERAZ = "E"
        this.w_TMPC = ah_Msgformat("Unit� di misura incongruente")
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
        select _Curs_GSCVA2B40
        continue
      enddo
      use
    endif
    do case
      case this.w_CONV>=0 AND this.w_NHF>=0 AND this.w_TMPNE=0
        this.oParentObject.w_PESEOK = .T.
        this.w_TMPC = ah_Msgformat("Procedura di conversione eseguita correttamente")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        this.oParentObject.w_pMSG = this.w_TMPC
      case this.w_CONV>=0 AND this.w_NHF>=0 AND this.w_TMPNE>0
        this.oParentObject.w_PESEOK = .T.
        this.w_TMPC = ah_Msgformat("Procedura di conversione terminata con warning")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        this.oParentObject.w_pMSG = ah_Msgformat("Procedura di conversione terminata con warning%0WARNING - non � stato possibile aggiornare tutti i componenti")
    endcase
    * --- commit
    cp_EndTrs(.t.)
    l_Rowtmp=RECCOUNT("TmpLog")
    if l_Rowtmp=0
      INSERT INTO TmpLog (TBLNAME, CORIFDIS, CVNUMRIF, DBROWORD, DBDESCOM, COCODCOM, NUMRIG, OLDCOEUM, NEWCOEUM, TMESSAGE, TOPERAZ); 
 VALUES ("","","","", "", "", "", 0, 0, "", "Z")
    endif
    SELECT * FROM TmpLog INTO CURSOR __tmp__ ORDER BY TBLNAME DESC, TOPERAZ, CORIFDIS, DBROWORD, NUMRIG
    cp_chprn("..\pcon\exe\query\GSCVAB40", " ", this)
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if Used ("TmpLog")
      INSERT INTO TmpLog (TBLNAME, CORIFDIS, CVNUMRIF, DBROWORD, DBDESCOM, COCODCOM, NUMRIG,; 
 OLDCOEUM, NEWCOEUM, TMESSAGE, TOPERAZ); 
 VALUES (this.w_TBLNAME, alltrim(this.w_CORIFDIS), alltrim(str(this.w_CVNUMRIF)), alltrim(str(this.w_DBROWORD)), alltrim(this.w_DBDESCOM),; 
 alltrim(this.w_COCODCOM), alltrim(str(this.w_CPROWORD)), this.w_OLDCOEUM1, this.w_COCOEUM1, this.w_TMPC, ; 
 this.w_OPERAZ)
    endif
  endproc


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='COM_VARI'
    this.cWorkTables[2]='DISTBASE'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_GSCVA1B40')
      use in _Curs_GSCVA1B40
    endif
    if used('_Curs_GSCVA2B40')
      use in _Curs_GSCVA2B40
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
