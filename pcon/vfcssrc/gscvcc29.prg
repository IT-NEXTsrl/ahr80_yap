* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcc29                                                        *
*               Aggiornamento provenienza sulla generazione dei buoni di prelievo*
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2013-02-13                                                      *
* Last revis.: 2013-04-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcc29",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcc29 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TMPC = space(10)
  w_TEMP = space(10)
  * --- WorkFile variables
  RIFMGODL_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * ---  Aggiornamento provenienza sulla generazione dei buoni di prelievo
    * --- FIle di LOG
    * --- Try
    local bErr_037275F0
    bErr_037275F0=bTrsErr
    this.Try_037275F0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_msgformat("ERRORE GENERICO - Impossibile aggiornare la tabella RIFMGODL")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_037275F0
    * --- End
  endproc
  proc Try_037275F0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- I buoni di prelievo devono avere sempre provenienza interna, il default ad 'L' pu� provocare una anomalia in fase di cambio release se si passa alla release 6.5 o successive.
    *     Lo schema corretto � il seguente:
    *     PDTIPGEN='BP' -> Buoni di prelievo -> PDPROVEN='I'
    *     PDTIPGEN='OR' -> Ordine a formitore(ODA) -> PDPROVEN='E'
    *     PDTIPGEN='OR' -> Ordine a formitore(OCL) -> PDPROVEN='L'
    *     PDTIPGEN='DT' -> DDT di trasferimento a terzista -> PDPROVEN='L'
    * --- Write into RIFMGODL
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.RIFMGODL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.RIFMGODL_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.RIFMGODL_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PDPROVEN ="+cp_NullLink(cp_ToStrODBC("I"),'RIFMGODL','PDPROVEN');
          +i_ccchkf ;
      +" where ";
          +"PDTIPGEN = "+cp_ToStrODBC("BP");
          +" and PDPROVEN = "+cp_ToStrODBC("L");
             )
    else
      update (i_cTable) set;
          PDPROVEN = "I";
          &i_ccchkf. ;
       where;
          PDTIPGEN = "BP";
          and PDPROVEN = "L";

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    if this.w_NHF>=0
      this.w_TMPC = ah_msgformat("L'aggiornamento della provenienza sulla generazione dei buoni di prelievo � avvenuto correttamente.")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = ah_msgformat("Conversione eseguita correttamente")
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='RIFMGODL'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
