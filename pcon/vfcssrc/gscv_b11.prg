* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscv_b11                                                        *
*              Sostituzione listini cli/for (                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_56]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-06-04                                                      *
* Last revis.: 2001-06-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscv_b11",oParentObject,m.w_NHF)
return(i_retval)

define class tgscv_b11 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_CODEUR = space(3)
  w_DECEUR = 0
  w_CODLIR = space(3)
  w_OBSO = ctod("  /  /  ")
  w_TmpN = 0
  w_TMPC = space(10)
  w_TEMP = space(10)
  w_OK = .f.
  w_LISO1 = space(5)
  w_LISD1 = space(5)
  w_LISO2 = space(5)
  w_LISD2 = space(5)
  w_LISO3 = space(5)
  w_LISD3 = space(5)
  w_CLIENTI = space(1)
  w_FORNITORI = space(1)
  w_QUALCOSA = .f.
  w_TIPCON = space(1)
  * --- WorkFile variables
  AZIENDA_idx=0
  VALUTE_idx=0
  CONTI_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- FIle di LOG
    * --- Visibile dal padre
    * --- Variabili locali
    * --- Data Obsolescenza
    this.w_OBSO = cp_CharToDate("01-01-2002")
    * --- Leggo il codice valuta Euro e Lire
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZVALEUR,AZVALLIR"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZVALEUR,AZVALLIR;
        from (i_cTable) where;
            AZCODAZI = i_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CODEUR = NVL(cp_ToDate(_read_.AZVALEUR),cp_NullValue(_read_.AZVALEUR))
      this.w_CODLIR = NVL(cp_ToDate(_read_.AZVALLIR),cp_NullValue(_read_.AZVALLIR))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Controllo 1 - Devono essere caricati i dati azienda
    if Empty ( this.w_CODLIR ) Or Empty ( this.w_CODEUR )
      this.oParentObject.w_PMSG = ah_Msgformat("ERRORE 001 - codice Euro o valuta nazionale non definiti in dati azienda")
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE 001 - codice Euro o valuta nazionale non definiti in dati azienda")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
      i_retcode = 'stop'
      return
    endif
    * --- Esegue maschera
    this.w_OK = .F.
    do GSCV_K11 with this
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if not this.w_OK
      this.oParentObject.w_PMSG = Ah_Msgformat("ERRORE 002 - parametri di selezione non indicati o errati")
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = Ah_Msgformat("ERRORE 002 - parametri di selezione non indicati o errati")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    * --- Try
    local bErr_036019B0
    bErr_036019B0=bTrsErr
    this.Try_036019B0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      this.w_TMPC = ah_Msgformat("ERRORE GENERICO - operazione sospesa")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    bTrsErr=bTrsErr or bErr_036019B0
    * --- End
  endproc
  proc Try_036019B0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.w_QUALCOSA = .F.
    if not empty(this.w_LISO1) and not empty(this.w_LISD1)
      if this.w_CLIENTI="S"
        this.w_TIPCON = "C"
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      if this.w_QUALCOSA
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, " ")
      endif
      if this.w_FORNITORI="S"
        this.w_TIPCON = "F"
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    if this.w_QUALCOSA
      this.oParentObject.w_PMSG = ah_Msgformat("Elaborazione terminata con successo")
    else
      this.w_TMPC = ah_Msgformat("Non sono stati rilevati clienti/fornitori con listino da sostituire")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      this.oParentObject.w_PMSG = ah_Msgformat("Non sono stati rilevati clienti/fornitori con listino da sostituire")
    endif
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Select from CONTI
    i_nConn=i_TableProp[this.CONTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" CONTI ";
          +" where ANTIPCON= "+cp_ToStrODBC(this.w_TIPCON)+"";
           ,"_Curs_CONTI")
    else
      select * from (i_cTable);
       where ANTIPCON= this.w_TIPCON;
        into cursor _Curs_CONTI
    endif
    if used('_Curs_CONTI')
      select _Curs_CONTI
      locate for 1=1
      do while not(eof())
      if _Curs_CONTI.ANNUMLIS=this.w_LISO1
        * --- Aggiorna
        * --- Write into CONTI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ANNUMLIS ="+cp_NullLink(cp_ToStrODBC(this.w_LISD1),'CONTI','ANNUMLIS');
              +i_ccchkf ;
          +" where ";
              +"ANTIPCON = "+cp_ToStrODBC(_Curs_CONTI.ANTIPCON);
              +" and ANCODICE = "+cp_ToStrODBC(_Curs_CONTI.ANCODICE);
                 )
        else
          update (i_cTable) set;
              ANNUMLIS = this.w_LISD1;
              &i_ccchkf. ;
           where;
              ANTIPCON = _Curs_CONTI.ANTIPCON;
              and ANCODICE = _Curs_CONTI.ANCODICE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Annota in log
        if this.w_NHF>=0
          if this.w_TIPCON="C"
            this.w_TMPC = ah_Msgformat("Cliente: %1 - sostituito listino: <%2> con listino: <%3>",_Curs_CONTI.ANCODICE, this.w_LISO1, this.w_LISD1)
          else
            this.w_TMPC = ah_Msgformat("Fornitore: %1 - sostituito listino: <%2> con listino: <%3>",_Curs_CONTI.ANCODICE, this.w_LISO1, this.w_LISD1)
          endif
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
        this.w_QUALCOSA = .T.
      else
        if not empty(this.w_LISO2) and _Curs_CONTI.ANNUMLIS=this.w_LISO2
          * --- Aggiorna
          * --- Write into CONTI
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.CONTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTI_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"ANNUMLIS ="+cp_NullLink(cp_ToStrODBC(this.w_LISD2),'CONTI','ANNUMLIS');
                +i_ccchkf ;
            +" where ";
                +"ANTIPCON = "+cp_ToStrODBC(_Curs_CONTI.ANTIPCON);
                +" and ANCODICE = "+cp_ToStrODBC(_Curs_CONTI.ANCODICE);
                   )
          else
            update (i_cTable) set;
                ANNUMLIS = this.w_LISD2;
                &i_ccchkf. ;
             where;
                ANTIPCON = _Curs_CONTI.ANTIPCON;
                and ANCODICE = _Curs_CONTI.ANCODICE;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          * --- Annota in log
          if this.w_NHF>=0
            if this.w_TIPCON="C"
              this.w_TMPC = ah_Msgformat("Cliente: %1 - sostituito listino: <%2> con listino: <%3>",_Curs_CONTI.ANCODICE, this.w_LISO2, this.w_LISD2)
            else
              this.w_TMPC = ah_Msgformat("Fornitore: %1 - sostituito listino: <%2> con listino: <%3>",_Curs_CONTI.ANCODICE, this.w_LISO2, this.w_LISD2)
            endif
            this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
          endif
          this.w_QUALCOSA = .T.
        else
          if not empty(this.w_LISO3) and _Curs_CONTI.ANNUMLIS=this.w_LISO3
            * --- Aggiorna
            * --- Write into CONTI
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.CONTI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTI_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"ANNUMLIS ="+cp_NullLink(cp_ToStrODBC(this.w_LISD3),'CONTI','ANNUMLIS');
                  +i_ccchkf ;
              +" where ";
                  +"ANTIPCON = "+cp_ToStrODBC(_Curs_CONTI.ANTIPCON);
                  +" and ANCODICE = "+cp_ToStrODBC(_Curs_CONTI.ANCODICE);
                     )
            else
              update (i_cTable) set;
                  ANNUMLIS = this.w_LISD3;
                  &i_ccchkf. ;
               where;
                  ANTIPCON = _Curs_CONTI.ANTIPCON;
                  and ANCODICE = _Curs_CONTI.ANCODICE;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            * --- Annota in log
            if this.w_NHF>=0
              if this.w_TIPCON="C"
                this.w_TMPC = ah_Msgformat("Cliente: %1 - sostituito listino: <%2> con listino: <%3>",_Curs_CONTI.ANCODICE, this.w_LISO3, this.w_LISD3)
              else
                this.w_TMPC = ah_Msgformat("Fornitore: %1 - sostituito listino: <%2> con listino: <%3>",_Curs_CONTI.ANCODICE, this.w_LISO3, this.w_LISD3)
              endif
              this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
            endif
            this.w_QUALCOSA = .T.
          endif
        endif
      endif
        select _Curs_CONTI
        continue
      enddo
      use
    endif
  endproc


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='VALUTE'
    this.cWorkTables[3]='CONTI'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_CONTI')
      use in _Curs_CONTI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
