* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcb57                                                        *
*              Predisposizione tabella Tipi prestazioni e valorizzazione combo box nella tabella ART_ICOL*
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-04-03                                                      *
* Last revis.: 2011-01-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcb57",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcb57 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_NCONN = 0
  w_TIPPRE = space(10)
  w_TROVATA = .f.
  w_CodRel = space(15)
  w_ARCODART = space(20)
  * --- WorkFile variables
  TIPI_PRE_idx=0
  ART_ICOL_idx=0
  CONVERSI_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Predisposizione tabella Tipi prestazioni e valorizzazione combo box nella tabella ART_ICOL
    this.w_NCONN = i_TableProp[this.TIPI_PRE_idx,3] 
    this.w_CodRel = "7.0-M5902"
    * --- begin transaction
    cp_BeginTrs()
    * --- Try
    local bErr_047C38D0
    bErr_047C38D0=bTrsErr
    this.Try_047C38D0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("Errore generico - impossibile eseguire l'inserimento richiesto")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_047C38D0
    * --- End
  endproc
  proc Try_047C38D0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Legge se eseguita la conversione  7.0-M5902
    * --- Read from CONVERSI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CONVERSI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONVERSI_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "*"+;
        " from "+i_cTable+" CONVERSI where ";
            +"COCODREL = "+cp_ToStrODBC(this.w_CodRel);
            +" and COESEGUI = "+cp_ToStrODBC("S");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        *;
        from (i_cTable) where;
            COCODREL = this.w_CodRel;
            and COESEGUI = "S";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_TROVATA = i_Rows>0
    if NOT this.w_TROVATA
      * --- Inserimento tipo prestazione PREST
      * --- Read from TIPI_PRE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.TIPI_PRE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TIPI_PRE_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "TPCODICE"+;
          " from "+i_cTable+" TIPI_PRE where ";
              +"TPCODICE = "+cp_ToStrODBC("PREST");
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          TPCODICE;
          from (i_cTable) where;
              TPCODICE = "PREST";
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_TIPPRE = NVL(cp_ToDate(_read_.TPCODICE),cp_NullValue(_read_.TPCODICE))
        use
        if i_Rows=0
          * --- Insert into TIPI_PRE
          i_nConn=i_TableProp[this.TIPI_PRE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TIPI_PRE_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TIPI_PRE_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"TPCODICE"+",TPDESCRI"+",TPFLSTAM"+",TPORDSTA"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC("PREST"),'TIPI_PRE','TPCODICE');
            +","+cp_NullLink(cp_ToStrODBC("Prestazioni"),'TIPI_PRE','TPDESCRI');
            +","+cp_NullLink(cp_ToStrODBC("N"),'TIPI_PRE','TPFLSTAM');
            +","+cp_NullLink(cp_ToStrODBC(10),'TIPI_PRE','TPORDSTA');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'TPCODICE',"PREST",'TPDESCRI',"Prestazioni",'TPFLSTAM',"N",'TPORDSTA',10)
            insert into (i_cTable) (TPCODICE,TPDESCRI,TPFLSTAM,TPORDSTA &i_ccchkf. );
               values (;
                 "PREST";
                 ,"Prestazioni";
                 ,"N";
                 ,10;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        endif
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("Inserito il tipo prestazioni Prest")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
      * --- Valorizzazione campo ARTIPRES (combo) in tabella ART_ICOL
      * --- Select from ART_ICOL
      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select ARCODART  from "+i_cTable+" ART_ICOL ";
            +" where ARTIPART='FM' OR ARTIPART='FO' OR ARTIPART='DE' ";
             ,"_Curs_ART_ICOL")
      else
        select ARCODART from (i_cTable);
         where ARTIPART="FM" OR ARTIPART="FO" OR ARTIPART="DE" ;
          into cursor _Curs_ART_ICOL
      endif
      if used('_Curs_ART_ICOL')
        select _Curs_ART_ICOL
        locate for 1=1
        do while not(eof())
        this.w_ARCODART = _Curs_ART_ICOL.ARCODART
        * --- Write into ART_ICOL
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ART_ICOL_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ARTIPRES ="+cp_NullLink(cp_ToStrODBC("PREST"),'ART_ICOL','ARTIPRES');
              +i_ccchkf ;
          +" where ";
              +"ARCODART = "+cp_ToStrODBC(this.w_ARCODART);
                 )
        else
          update (i_cTable) set;
              ARTIPRES = "PREST";
              &i_ccchkf. ;
           where;
              ARCODART = this.w_ARCODART;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
          select _Curs_ART_ICOL
          continue
        enddo
        use
      endif
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("Valorizzata combo box nella tabella ART_ICOL")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = this.w_TMPC
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case w_DESCRI == "CORTE SUPREMA DI CASSAZIONE"
        this.w_TIPUFF = "A"
      case w_DESCRI == "CORTE D'APPELLO" OR w_DESCRI == "CORTE D' APPELLO" OR w_DESCRI == "CORTE DI APPELLO" 
        this.w_TIPUFF = "B"
      case w_DESCRI == "CORTE DI ASSISE" OR w_DESCRI == "CORTE D'ASSISE"
        this.w_TIPUFF = "C"
      case w_DESCRI == "CORTE DI ASSISE DI APPELLO" OR w_DESCRI == "CORTE D'ASSISE DI APPELLO"
        this.w_TIPUFF = "D"
      case w_DESCRI == "DIREZIONE NAZIONALE ANTIMAFIA"
        this.w_TIPUFF = "E"
      case w_DESCRI == "GIUDICE DI PACE"
        this.w_TIPUFF = "F"
      case w_DESCRI == "PROCURA GENERALE DELLA REPUBBLICA PRESSO LA CORTE SUPREMA DI CASSAZIONE" OR w_DESCRI == "PROCURA GENERALE DELLA REPUBBLICA PRESSO LA CASSAZIONE" 
        this.w_TIPUFF = "G"
      case w_DESCRI == "PROCURA GENERALE DELLA REPUBBLICA PRESSO LA CORTE D'APPELLO" OR w_DESCRI == "PROCURA GENERALE DELLA REPUBBLICA PRESSO LA CORTE DI APPELLO"
        this.w_TIPUFF = "H"
      case w_DESCRI == "PROCURA DELLA REPUBBLICA PRESSO IL TRIBUNALE" OR w_DESCRI == "PROCURA DELLA REPUBBLICA PRESSO IL TRIBUNALE ORDINARIO" OR w_DESCRI == "PROCURA DELLA REPUBBLICA PRESSO LA PRETURA CIRCONDARIALE" OR w_DESCRI == "PROCURA DELLA REPUBBLICA PRESSO IL TRIBUNALE ORDINARIO - GIUDICE UNICO"
        this.w_TIPUFF = "I"
      case w_DESCRI == "PROCURA DELLA REPUBBLICA PRESSO IL TRIBUNALE PER I MINORENNI" OR w_DESCRI == "PROCURA DELLA REPUBBLICA PRESSO IL TRIBUNALE DEI MINORI"
        this.w_TIPUFF = "L"
      case w_DESCRI == "SEZIONE DISTACCATA DI TRIBUNALE" OR w_DESCRI == "SEZIONE DISTACCATA PRETURA CIRCONDARIALE"
        this.w_TIPUFF = "M"
      case w_DESCRI == "TRIBUNALE" OR w_DESCRI == "TRIBUNALE ORDINARIO" OR w_DESCRI == "PRETURA CIRCONDARIALE" OR w_DESCRI == "TRIBUNALE ORDINARIO - GIUDICE UNICO"
        this.w_TIPUFF = "N"
      case w_DESCRI == "TRIBUNALE REGIONALE DELLE ACQUE PUBBLICHE"
        this.w_TIPUFF = "O"
      case w_DESCRI == "TRIBUNALE SUPERIORE DELLE ACQUE PUBBLICHE"
        this.w_TIPUFF = "P"
      case w_DESCRI == "TRIBUNALE DI SORVEGLIANZA"
        this.w_TIPUFF = "Q"
      case w_DESCRI == "TRIBUNALE PER I MINORENNI"
        this.w_TIPUFF = "R"
      case w_DESCRI == "UFFICIO DI SORVEGLIANZA"
        this.w_TIPUFF = "S"
    endcase
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case w_DESCRI == "Amm02" OR w_DESCRI=="Amm03" OR w_DESCRI=="Civ01" OR w_DESCRI=="Civ04" OR w_DESCRI=="Civ05" OR w_DESCRI=="Civ06" OR w_DESCRI=="Civ07" OR w_DESCRI=="Civ09" OR w_DESCRI=="Civ15" OR w_DESCRI=="Pen04" OR w_DESCRI=="Pen14"
        this.w_TIPUFF = "A"
      case w_DESCRI=="Civ03" OR w_DESCRI=="Pen01" OR w_DESCRI== "Civ11" OR w_DESCRI=="Ctri02" OR w_DESCRI=="Pen05"
        this.w_TIPUFF = "B"
      case w_DESCRI == "Pen03"
        this.w_TIPUFF = "C"
      case w_DESCRI == "Pen02"
        this.w_TIPUFF = "D"
      case w_DESCRI == "Pen25"
        this.w_TIPUFF = "E"
      case w_DESCRI == "Civ13" OR w_DESCRI=="Pen12"
        this.w_TIPUFF = "F"
      case w_DESCRI == "Civ22" OR w_DESCRI == "Pen23" 
        this.w_TIPUFF = "G"
      case w_DESCRI == "Civ21" OR w_DESCRI == "Pen22"
        this.w_TIPUFF = "H"
      case w_DESCRI == "Civ20" OR w_DESCRI == "Pen21"
        this.w_TIPUFF = "I"
      case w_DESCRI == "Civ23" OR w_DESCRI == "Pen24"
        this.w_TIPUFF = "L"
      case w_DESCRI == "Civ12" OR w_DESCRI == "Pen08" OR w_DESCRI=="Pen09"
        this.w_TIPUFF = "M"
      case w_DESCRI == "Civ17" OR w_DESCRI == "Amm01" OR w_DESCRI == "Amm04" OR w_DESCRI == "Civ02" OR w_DESCRI=="Civ08" OR w_DESCRI=="Civ10" OR w_DESCRI=="Civ14" OR w_DESCRI=="Ctri01" OR w_DESCRI=="Pen10" OR w_DESCRI=="Pen11" OR w_DESCRI=="Pen15" OR w_DESCRI=="Pen16" OR w_DESCRI=="Pen17" OR w_DESCRI=="Pen19"
        this.w_TIPUFF = "N"
      case w_DESCRI == "Civ18"
        this.w_TIPUFF = "O"
      case w_DESCRI == "Civ19"
        this.w_TIPUFF = "P"
      case w_DESCRI == "Pen20" OR w_DESCRI=="Pen07"
        this.w_TIPUFF = "Q"
      case w_DESCRI == "Civ16" OR w_DESCRI=="Pen18"
        this.w_TIPUFF = "R"
      case w_DESCRI == "Pen06" OR w_DESCRI=="Pen13"
        this.w_TIPUFF = "S"
    endcase
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case w_DESCRI == "AMM01"
        this.w_TIPUFF = "9R9"
      case w_DESCRI == "AMM03"
        this.w_TIPUFF = "9S9"
      case w_DESCRI == "AMM04"
        this.w_TIPUFF = "9T9"
      case w_DESCRI == "CIV03" OR w_DESCRI == "CIV11" OR w_DESCRI == "PEN01" OR w_DESCRI == "PEN05"
        this.w_TIPUFF = "Hxx"
      case w_DESCRI == "CIV09"
        this.w_TIPUFF = "SAE"
      case w_DESCRI == "CIV07" OR w_DESCRI == "PEN04"
        this.w_TIPUFF = "4AE"
      case w_DESCRI == "CIV10"
        this.w_TIPUFF = "Vxx"
      case w_DESCRI == "CIV13" OR w_DESCRI == "PEN12"
        this.w_TIPUFF = "9C3"
      case w_DESCRI == "PEN13" OR w_DESCRI == "PEN06"
        this.w_TIPUFF = "Sxx"
      case w_DESCRI == "CIV12" OR w_DESCRI == "PEN08" OR w_DESCRI == "PEN09" OR w_DESCRI == "CIV17" OR w_DESCRI == "PEN15" OR w_DESCRI == "PEN19"
        this.w_TIPUFF = "9BX"
      case w_DESCRI == "PEN07" OR w_DESCRI == "PEN20"
        this.w_TIPUFF = "Nxx"
      case w_DESCRI == "CIV16" OR w_DESCRI == "PEN18"
        this.w_TIPUFF = "Pxx"
      case w_DESCRI == "CIV17" OR w_DESCRI == "PEN17"
        this.w_TIPUFF = "1xx"
      case w_DESCRI == "CIV19"
        this.w_TIPUFF = "8AE"
    endcase
  endproc


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='TIPI_PRE'
    this.cWorkTables[2]='ART_ICOL'
    this.cWorkTables[3]='CONVERSI'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_ART_ICOL')
      use in _Curs_ART_ICOL
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
