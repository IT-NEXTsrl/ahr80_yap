* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvbb98                                                        *
*              AGGIORNA CAMPO TRFLMERF                                         *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-03-17                                                      *
* Last revis.: 2008-03-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvbb98",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvbb98 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TMPC = space(10)
  w_TEMP = space(10)
  * --- WorkFile variables
  COD_TRIB_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Valorizzazione campo TRFLMERF anagrafica COD_TRIB per Mod. F24
    * --- FIle di LOG
    * --- Try
    local bErr_0379FA80
    bErr_0379FA80=bTrsErr
    this.Try_0379FA80()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = "ERRORE GENERICO - Impossibile aggiornare tabella  COD_TRIB "
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_0379FA80
    * --- End
  endproc
  proc Try_0379FA80()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Write into COD_TRIB
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.COD_TRIB_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.COD_TRIB_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_SetAzi(i_TableProp[this.COD_TRIB_idx,2])
      i_cTempTable=cp_GetTempTableName(i_nConn)
      i_aIndex[1]='TRCODICE'
      cp_CreateTempTable(i_nConn,i_cTempTable,"TRCODICE "," from "+i_cQueryTable+" where TRCODICE in ('1001','1002','1004','1012',   '1013','1018','1019','1020','1024','1025','1028','1029',   '1030','1031','1032','1034','1035','1036','1038','1040',   '1045','1046','1047','1048','1050','1051','1052','1058')",.f.,@i_aIndex)
      i_cQueryTable=i_cTempTable
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.COD_TRIB_idx,i_nConn)
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="COD_TRIB.TRCODICE = _t2.TRCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"TRFLMERF ="+cp_NullLink(cp_ToStrODBC("S"),'COD_TRIB','TRFLMERF');
          +i_ccchkf;
          +" from "+i_cTable+" COD_TRIB, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="COD_TRIB.TRCODICE = _t2.TRCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" COD_TRIB, "+i_cQueryTable+" _t2 set ";
      +"COD_TRIB.TRFLMERF ="+cp_NullLink(cp_ToStrODBC("S"),'COD_TRIB','TRFLMERF');
          +Iif(Empty(i_ccchkf),"",",COD_TRIB.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="PostgreSQL"
        i_cWhere="COD_TRIB.TRCODICE = _t2.TRCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" COD_TRIB set ";
      +"TRFLMERF ="+cp_NullLink(cp_ToStrODBC("S"),'COD_TRIB','TRFLMERF');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".TRCODICE = "+i_cQueryTable+".TRCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"TRFLMERF ="+cp_NullLink(cp_ToStrODBC("S"),'COD_TRIB','TRFLMERF');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Write into COD_TRIB
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.COD_TRIB_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.COD_TRIB_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_SetAzi(i_TableProp[this.COD_TRIB_idx,2])
      i_cTempTable=cp_GetTempTableName(i_nConn)
      i_aIndex[1]='TRCODICE'
      cp_CreateTempTable(i_nConn,i_cTempTable,"TRCODICE "," from "+i_cQueryTable+" where TRCODICE in ('1102','1103','1124','1239','1242','1243','1245','1250',   '1301','1302','1312','1328','1601','1602','1902','1912','1913','1914','1916','1920','1921','1928',   '1964','1992','3802','3803','3815','3816','3840','3841','3860')",.f.,@i_aIndex)
      i_cQueryTable=i_cTempTable
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.COD_TRIB_idx,i_nConn)
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="COD_TRIB.TRCODICE = _t2.TRCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"TRFLMERF ="+cp_NullLink(cp_ToStrODBC("S"),'COD_TRIB','TRFLMERF');
          +i_ccchkf;
          +" from "+i_cTable+" COD_TRIB, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="COD_TRIB.TRCODICE = _t2.TRCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" COD_TRIB, "+i_cQueryTable+" _t2 set ";
      +"COD_TRIB.TRFLMERF ="+cp_NullLink(cp_ToStrODBC("S"),'COD_TRIB','TRFLMERF');
          +Iif(Empty(i_ccchkf),"",",COD_TRIB.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="PostgreSQL"
        i_cWhere="COD_TRIB.TRCODICE = _t2.TRCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" COD_TRIB set ";
      +"TRFLMERF ="+cp_NullLink(cp_ToStrODBC("S"),'COD_TRIB','TRFLMERF');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".TRCODICE = "+i_cQueryTable+".TRCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"TRFLMERF ="+cp_NullLink(cp_ToStrODBC("S"),'COD_TRIB','TRFLMERF');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Write into COD_TRIB
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.COD_TRIB_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.COD_TRIB_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_SetAzi(i_TableProp[this.COD_TRIB_idx,2])
      i_cTempTable=cp_GetTempTableName(i_nConn)
      i_aIndex[1]='TRCODICE'
      cp_CreateTempTable(i_nConn,i_cTempTable,"TRCODICE "," from "+i_cQueryTable+" where TRCODICE in ('1612','1613','1672','1680',   '1685','1686','1687','1688','1689','1690','1691','1692',   '1693','1694','1705','1706','1707','1712','1713','1901')",.f.,@i_aIndex)
      i_cQueryTable=i_cTempTable
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.COD_TRIB_idx,i_nConn)
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="COD_TRIB.TRCODICE = _t2.TRCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"TRFLMERF ="+cp_NullLink(cp_ToStrODBC("S"),'COD_TRIB','TRFLMERF');
          +i_ccchkf;
          +" from "+i_cTable+" COD_TRIB, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="COD_TRIB.TRCODICE = _t2.TRCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" COD_TRIB, "+i_cQueryTable+" _t2 set ";
      +"COD_TRIB.TRFLMERF ="+cp_NullLink(cp_ToStrODBC("S"),'COD_TRIB','TRFLMERF');
          +Iif(Empty(i_ccchkf),"",",COD_TRIB.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="PostgreSQL"
        i_cWhere="COD_TRIB.TRCODICE = _t2.TRCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" COD_TRIB set ";
      +"TRFLMERF ="+cp_NullLink(cp_ToStrODBC("S"),'COD_TRIB','TRFLMERF');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".TRCODICE = "+i_cQueryTable+".TRCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"TRFLMERF ="+cp_NullLink(cp_ToStrODBC("S"),'COD_TRIB','TRFLMERF');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Write into COD_TRIB
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.COD_TRIB_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.COD_TRIB_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_SetAzi(i_TableProp[this.COD_TRIB_idx,2])
      i_cTempTable=cp_GetTempTableName(i_nConn)
      i_aIndex[1]='TRCODICE'
      cp_CreateTempTable(i_nConn,i_cTempTable,"TRCODICE "," from "+i_cQueryTable+" where TRCODICE in ('4201','4330','4331','4630',   '4631','4730','4731','4930','4931','4932','4933','5004',   '5005','5006','5007','5008','5009','5019','5024','8906','8913')",.f.,@i_aIndex)
      i_cQueryTable=i_cTempTable
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.COD_TRIB_idx,i_nConn)
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="COD_TRIB.TRCODICE = _t2.TRCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"TRFLMERF ="+cp_NullLink(cp_ToStrODBC("S"),'COD_TRIB','TRFLMERF');
          +i_ccchkf;
          +" from "+i_cTable+" COD_TRIB, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="COD_TRIB.TRCODICE = _t2.TRCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" COD_TRIB, "+i_cQueryTable+" _t2 set ";
      +"COD_TRIB.TRFLMERF ="+cp_NullLink(cp_ToStrODBC("S"),'COD_TRIB','TRFLMERF');
          +Iif(Empty(i_ccchkf),"",",COD_TRIB.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="PostgreSQL"
        i_cWhere="COD_TRIB.TRCODICE = _t2.TRCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" COD_TRIB set ";
      +"TRFLMERF ="+cp_NullLink(cp_ToStrODBC("S"),'COD_TRIB','TRFLMERF');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".TRCODICE = "+i_cQueryTable+".TRCODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"TRFLMERF ="+cp_NullLink(cp_ToStrODBC("S"),'COD_TRIB','TRFLMERF');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    if this.w_NHF>=0
      this.w_TMPC = "Aggiornamento campo  TRFLMERF tabella COD_TRIB avvenuto correttamente. "
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = "Conversione eseguita correttamente"
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='COD_TRIB'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
