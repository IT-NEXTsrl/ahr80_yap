* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscv_b50                                                        *
*              Aggiornamento causali documenti                                 *
*                                                                              *
*      Author: Zucchetti Tam Spa                                               *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_16]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-08-29                                                      *
* Last revis.: 2001-08-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscv_b50",oParentObject,m.w_NHF)
return(i_retval)

define class tgscv_b50 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  * --- WorkFile variables
  TIP_DOCU_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- FIle di LOG
    * --- Try
    local bErr_037C1BD0
    bErr_037C1BD0=bTrsErr
    this.Try_037C1BD0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE - impossibile aggiornare TDFLRICE su tabella TIP_DOCU" )
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_037C1BD0
    * --- End
  endproc
  proc Try_037C1BD0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Aggiornamento  TDFLRICE
    * --- Select from TIP_DOCU
    i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select TDTIPDOC  from "+i_cTable+" TIP_DOCU ";
           ,"_Curs_TIP_DOCU")
    else
      select TDTIPDOC from (i_cTable);
        into cursor _Curs_TIP_DOCU
    endif
    if used('_Curs_TIP_DOCU')
      select _Curs_TIP_DOCU
      locate for 1=1
      do while not(eof())
      * --- Write into TIP_DOCU
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TIP_DOCU_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"TDFLRICE ="+cp_NullLink(cp_ToStrODBC(" "),'TIP_DOCU','TDFLRICE');
            +i_ccchkf ;
        +" where ";
            +"TDTIPDOC = "+cp_ToStrODBC(_Curs_TIP_DOCU.TDTIPDOC);
               )
      else
        update (i_cTable) set;
            TDFLRICE = " ";
            &i_ccchkf. ;
         where;
            TDTIPDOC = _Curs_TIP_DOCU.TDTIPDOC;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
        select _Curs_TIP_DOCU
        continue
      enddo
      use
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Aggiornamento campo TDFLRICE su tabella TIP_DOCU eseguito con successo")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = this.oParentObject.w_PMSG + this.w_TMPC 
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='TIP_DOCU'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_TIP_DOCU')
      use in _Curs_TIP_DOCU
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
