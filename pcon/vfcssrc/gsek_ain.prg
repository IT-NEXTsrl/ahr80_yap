* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsek_ain                                                        *
*              Elaborazione inventario Euro kit                                *
*                                                                              *
*      Author: TAM SOFTWARE & CODELAB                                          *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_130]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-06-04                                                      *
* Last revis.: 2008-10-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsek_ain"))

* --- Class definition
define class tgsek_ain as StdForm
  Top    = 30
  Left   = 27

  * --- Standard Properties
  Width  = 722
  Height = 317+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-10-07"
  HelpContextID=259398807
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=44

  * --- Constant Properties
  INVENTAR_IDX = 0
  MAGAZZIN_IDX = 0
  CATEGOMO_IDX = 0
  GRUMERC_IDX = 0
  FAM_ARTI_IDX = 0
  INVENTAR_IDX = 0
  ESERCIZI_IDX = 0
  VALUTE_IDX = 0
  cFile = "INVENTAR"
  cKeySelect = "INNUMINV,INCODESE"
  cKeyWhere  = "INNUMINV=this.w_INNUMINV and INCODESE=this.w_INCODESE"
  cKeyWhereODBC = '"INNUMINV="+cp_ToStrODBC(this.w_INNUMINV)';
      +'+" and INCODESE="+cp_ToStrODBC(this.w_INCODESE)';

  cKeyWhereODBCqualified = '"INVENTAR.INNUMINV="+cp_ToStrODBC(this.w_INNUMINV)';
      +'+" and INVENTAR.INCODESE="+cp_ToStrODBC(this.w_INCODESE)';

  cPrg = "gsek_ain"
  cComment = "Elaborazione inventario Euro kit"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_VALESE = space(3)
  w_INNUMINV = space(6)
  w_INDATMEM = ctod('  /  /  ')
  o_INDATMEM = ctod('  /  /  ')
  w_AZIENDA = space(10)
  w_INCODESE = space(4)
  w_INTIPINV = space(1)
  o_INTIPINV = space(1)
  w_INSTAINV = space(1)
  w_INELABOR = space(1)
  w_INDESINV = space(40)
  w_INCODMAG = space(5)
  w_MGDESMAG = space(30)
  w_COLL = space(5)
  o_COLL = space(5)
  w_INCATOMO = space(5)
  w_OMDESCRI = space(35)
  w_INGRUMER = space(5)
  w_GMDESCRI = space(35)
  w_INCODFAM = space(5)
  w_FADESCRI = space(35)
  w_INDATINV = ctod('  /  /  ')
  w_FLMESS = space(1)
  w_INMAGCOL = space(1)
  o_INMAGCOL = space(1)
  w_INFLGLCO = space(1)
  w_INFLGLSC = space(1)
  w_INFLGFCO = space(1)
  w_INESEPRE = space(4)
  w_INNUMPRE = space(6)
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_PRDATINV = ctod('  /  /  ')
  w_ESINIESE = ctod('  /  /  ')
  w_PRSTAINV = space(1)
  w_MGFISMAG = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_VALPRE = space(3)
  w_PRCATOMO = space(5)
  w_PRGRUMER = space(5)
  w_PRCODFAM = space(5)
  w_CHECK = .F.
  w_PRCODMAG = space(5)
  w_PRFINESE = ctod('  /  /  ')
  w_PRINIESE = ctod('  /  /  ')

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_INCODESE = this.W_INCODESE
  op_INNUMINV = this.W_INNUMINV
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'INVENTAR','gsek_ain')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsek_ainPag1","gsek_ain",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Elaborazione inventario euro kit")
      .Pages(1).HelpContextID = 231062534
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oINNUMINV_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[8]
    this.cWorkTables[1]='MAGAZZIN'
    this.cWorkTables[2]='CATEGOMO'
    this.cWorkTables[3]='GRUMERC'
    this.cWorkTables[4]='FAM_ARTI'
    this.cWorkTables[5]='INVENTAR'
    this.cWorkTables[6]='ESERCIZI'
    this.cWorkTables[7]='VALUTE'
    this.cWorkTables[8]='INVENTAR'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(8))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.INVENTAR_IDX,5],7]
    this.nPostItConn=i_TableProp[this.INVENTAR_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_INNUMINV = NVL(INNUMINV,space(6))
      .w_INCODESE = NVL(INCODESE,space(4))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from INVENTAR where INNUMINV=KeySet.INNUMINV
    *                            and INCODESE=KeySet.INCODESE
    *
    i_nConn = i_TableProp[this.INVENTAR_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('INVENTAR')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "INVENTAR.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' INVENTAR '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'INNUMINV',this.w_INNUMINV  ,'INCODESE',this.w_INCODESE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_VALESE = space(3)
        .w_AZIENDA = i_CODAZI
        .w_MGDESMAG = space(30)
        .w_COLL = space(5)
        .w_OMDESCRI = space(35)
        .w_GMDESCRI = space(35)
        .w_FADESCRI = space(35)
        .w_FLMESS = ' '
        .w_PRDATINV = ctod("  /  /  ")
        .w_ESINIESE = ctod("  /  /  ")
        .w_PRSTAINV = space(1)
        .w_MGFISMAG = space(1)
        .w_DATOBSO = ctod("  /  /  ")
        .w_VALPRE = space(3)
        .w_PRCATOMO = space(5)
        .w_PRGRUMER = space(5)
        .w_PRCODFAM = space(5)
        .w_CHECK = .f.
        .w_PRCODMAG = space(5)
        .w_PRFINESE = ctod("  /  /  ")
        .w_PRINIESE = ctod("  /  /  ")
        .w_INNUMINV = NVL(INNUMINV,space(6))
        .op_INNUMINV = .w_INNUMINV
        .w_INDATMEM = NVL(cp_ToDate(INDATMEM),ctod("  /  /  "))
        .w_INCODESE = NVL(INCODESE,space(4))
        .op_INCODESE = .w_INCODESE
          .link_1_5('Load')
        .w_INTIPINV = NVL(INTIPINV,space(1))
        .w_INSTAINV = NVL(INSTAINV,space(1))
        .w_INELABOR = NVL(INELABOR,space(1))
        .w_INDESINV = NVL(INDESINV,space(40))
        .w_INCODMAG = NVL(INCODMAG,space(5))
          .link_1_10('Load')
        .w_INCATOMO = NVL(INCATOMO,space(5))
          .link_1_13('Load')
        .w_INGRUMER = NVL(INGRUMER,space(5))
          .link_1_15('Load')
        .w_INCODFAM = NVL(INCODFAM,space(5))
          .link_1_17('Load')
        .w_INDATINV = NVL(cp_ToDate(INDATINV),ctod("  /  /  "))
        .w_INMAGCOL = NVL(INMAGCOL,space(1))
        .w_INFLGLCO = NVL(INFLGLCO,space(1))
        .w_INFLGLSC = NVL(INFLGLSC,space(1))
        .w_INFLGFCO = NVL(INFLGFCO,space(1))
        .w_INESEPRE = NVL(INESEPRE,space(4))
          .link_1_25('Load')
        .w_INNUMPRE = NVL(INNUMPRE,space(6))
          .link_1_26('Load')
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .oPgFrm.Page1.oPag.oObj_1_35.Calculate()
        .w_OBTEST = .w_INDATMEM
        .oPgFrm.Page1.oPag.oObj_1_40.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_47.Calculate()
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'INVENTAR')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_VALESE = space(3)
      .w_INNUMINV = space(6)
      .w_INDATMEM = ctod("  /  /  ")
      .w_AZIENDA = space(10)
      .w_INCODESE = space(4)
      .w_INTIPINV = space(1)
      .w_INSTAINV = space(1)
      .w_INELABOR = space(1)
      .w_INDESINV = space(40)
      .w_INCODMAG = space(5)
      .w_MGDESMAG = space(30)
      .w_COLL = space(5)
      .w_INCATOMO = space(5)
      .w_OMDESCRI = space(35)
      .w_INGRUMER = space(5)
      .w_GMDESCRI = space(35)
      .w_INCODFAM = space(5)
      .w_FADESCRI = space(35)
      .w_INDATINV = ctod("  /  /  ")
      .w_FLMESS = space(1)
      .w_INMAGCOL = space(1)
      .w_INFLGLCO = space(1)
      .w_INFLGLSC = space(1)
      .w_INFLGFCO = space(1)
      .w_INESEPRE = space(4)
      .w_INNUMPRE = space(6)
      .w_UTCC = 0
      .w_UTCV = 0
      .w_UTDC = ctot("")
      .w_UTDV = ctot("")
      .w_PRDATINV = ctod("  /  /  ")
      .w_ESINIESE = ctod("  /  /  ")
      .w_PRSTAINV = space(1)
      .w_MGFISMAG = space(1)
      .w_OBTEST = ctod("  /  /  ")
      .w_DATOBSO = ctod("  /  /  ")
      .w_VALPRE = space(3)
      .w_PRCATOMO = space(5)
      .w_PRGRUMER = space(5)
      .w_PRCODFAM = space(5)
      .w_CHECK = .f.
      .w_PRCODMAG = space(5)
      .w_PRFINESE = ctod("  /  /  ")
      .w_PRINIESE = ctod("  /  /  ")
      if .cFunction<>"Filter"
          .DoRTCalc(1,2,.f.)
        .w_INDATMEM = i_datsys
        .w_AZIENDA = i_CODAZI
        .w_INCODESE = g_CODESE
        .DoRTCalc(5,5,.f.)
          if not(empty(.w_INCODESE))
          .link_1_5('Full')
          endif
        .w_INTIPINV = 'G'
        .w_INSTAINV = 'P'
        .w_INELABOR = 'N'
          .DoRTCalc(9,9,.f.)
        .w_INCODMAG = g_MAGAZI
        .DoRTCalc(10,10,.f.)
          if not(empty(.w_INCODMAG))
          .link_1_10('Full')
          endif
          .DoRTCalc(11,12,.f.)
        .w_INCATOMO = space(5)
        .DoRTCalc(13,13,.f.)
          if not(empty(.w_INCATOMO))
          .link_1_13('Full')
          endif
          .DoRTCalc(14,14,.f.)
        .w_INGRUMER = space(5)
        .DoRTCalc(15,15,.f.)
          if not(empty(.w_INGRUMER))
          .link_1_15('Full')
          endif
          .DoRTCalc(16,16,.f.)
        .w_INCODFAM = space(5)
        .DoRTCalc(17,17,.f.)
          if not(empty(.w_INCODFAM))
          .link_1_17('Full')
          endif
          .DoRTCalc(18,18,.f.)
        .w_INDATINV = .w_INDATMEM
        .w_FLMESS = ' '
        .w_INMAGCOL = 'N'
        .w_INFLGLCO = iif(empty(nvl(.w_COLL,"")) and .w_INMAGCOL='S',.w_INFLGLCO,'N')
        .w_INFLGLSC = iif(empty(nvl(.w_COLL,"")) and .w_INMAGCOL='S',.w_INFLGLSC,'N')
        .w_INFLGFCO = iif(empty(nvl(.w_COLL,"")) and .w_INMAGCOL='S',.w_INFLGFCO,'N')
        .DoRTCalc(25,25,.f.)
          if not(empty(.w_INESEPRE))
          .link_1_25('Full')
          endif
        .w_INNUMPRE = iif(.w_INMAGCOL$'N|S',space(6),.w_INNUMPRE)
        .DoRTCalc(26,26,.f.)
          if not(empty(.w_INNUMPRE))
          .link_1_26('Full')
          endif
        .w_UTCC = i_CODUTE
        .w_UTCV = i_CODUTE
        .w_UTDC = i_DATSYS
        .w_UTDV = i_DATSYS
        .oPgFrm.Page1.oPag.oObj_1_35.Calculate()
          .DoRTCalc(31,34,.f.)
        .w_OBTEST = .w_INDATMEM
        .oPgFrm.Page1.oPag.oObj_1_40.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_47.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'INVENTAR')
    this.DoRTCalc(36,44,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.INVENTAR_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"SEINV","i_codazi,w_INCODESE,w_INNUMINV")
      .op_codazi = .w_codazi
      .op_INCODESE = .w_INCODESE
      .op_INNUMINV = .w_INNUMINV
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oINNUMINV_1_2.enabled = i_bVal
      .Page1.oPag.oINDATMEM_1_3.enabled = i_bVal
      .Page1.oPag.oINCODESE_1_5.enabled = i_bVal
      .Page1.oPag.oINTIPINV_1_6.enabled = i_bVal
      .Page1.oPag.oINSTAINV_1_7.enabled = i_bVal
      .Page1.oPag.oINDESINV_1_9.enabled = i_bVal
      .Page1.oPag.oINCODMAG_1_10.enabled = i_bVal
      .Page1.oPag.oINCATOMO_1_13.enabled = i_bVal
      .Page1.oPag.oINGRUMER_1_15.enabled = i_bVal
      .Page1.oPag.oINCODFAM_1_17.enabled = i_bVal
      .Page1.oPag.oINDATINV_1_19.enabled = i_bVal
      .Page1.oPag.oFLMESS_1_20.enabled = i_bVal
      .Page1.oPag.oINMAGCOL_1_21.enabled = i_bVal
      .Page1.oPag.oINFLGLCO_1_22.enabled = i_bVal
      .Page1.oPag.oINFLGLSC_1_23.enabled = i_bVal
      .Page1.oPag.oINFLGFCO_1_24.enabled = i_bVal
      .Page1.oPag.oINESEPRE_1_25.enabled = i_bVal
      .Page1.oPag.oINNUMPRE_1_26.enabled = i_bVal
      .Page1.oPag.oBtn_1_62.enabled = i_bVal
      .Page1.oPag.oObj_1_35.enabled = i_bVal
      .Page1.oPag.oObj_1_40.enabled = i_bVal
      .Page1.oPag.oObj_1_41.enabled = i_bVal
      .Page1.oPag.oObj_1_47.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oINNUMINV_1_2.enabled = .f.
        .Page1.oPag.oINCODESE_1_5.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oINNUMINV_1_2.enabled = .t.
        .Page1.oPag.oINCODESE_1_5.enabled = .t.
        .Page1.oPag.oINDATINV_1_19.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'INVENTAR',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.INVENTAR_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_INNUMINV,"INNUMINV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_INDATMEM,"INDATMEM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_INCODESE,"INCODESE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_INTIPINV,"INTIPINV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_INSTAINV,"INSTAINV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_INELABOR,"INELABOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_INDESINV,"INDESINV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_INCODMAG,"INCODMAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_INCATOMO,"INCATOMO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_INGRUMER,"INGRUMER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_INCODFAM,"INCODFAM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_INDATINV,"INDATINV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_INMAGCOL,"INMAGCOL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_INFLGLCO,"INFLGLCO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_INFLGLSC,"INFLGLSC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_INFLGFCO,"INFLGFCO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_INESEPRE,"INESEPRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_INNUMPRE,"INNUMPRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.INVENTAR_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2])
    i_lTable = "INVENTAR"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.INVENTAR_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.INVENTAR_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.INVENTAR_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"SEINV","i_codazi,w_INCODESE,w_INNUMINV")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into INVENTAR
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'INVENTAR')
        i_extval=cp_InsertValODBCExtFlds(this,'INVENTAR')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(INNUMINV,INDATMEM,INCODESE,INTIPINV,INSTAINV"+;
                  ",INELABOR,INDESINV,INCODMAG,INCATOMO,INGRUMER"+;
                  ",INCODFAM,INDATINV,INMAGCOL,INFLGLCO,INFLGLSC"+;
                  ",INFLGFCO,INESEPRE,INNUMPRE,UTCC,UTCV"+;
                  ",UTDC,UTDV "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_INNUMINV)+;
                  ","+cp_ToStrODBC(this.w_INDATMEM)+;
                  ","+cp_ToStrODBCNull(this.w_INCODESE)+;
                  ","+cp_ToStrODBC(this.w_INTIPINV)+;
                  ","+cp_ToStrODBC(this.w_INSTAINV)+;
                  ","+cp_ToStrODBC(this.w_INELABOR)+;
                  ","+cp_ToStrODBC(this.w_INDESINV)+;
                  ","+cp_ToStrODBCNull(this.w_INCODMAG)+;
                  ","+cp_ToStrODBCNull(this.w_INCATOMO)+;
                  ","+cp_ToStrODBCNull(this.w_INGRUMER)+;
                  ","+cp_ToStrODBCNull(this.w_INCODFAM)+;
                  ","+cp_ToStrODBC(this.w_INDATINV)+;
                  ","+cp_ToStrODBC(this.w_INMAGCOL)+;
                  ","+cp_ToStrODBC(this.w_INFLGLCO)+;
                  ","+cp_ToStrODBC(this.w_INFLGLSC)+;
                  ","+cp_ToStrODBC(this.w_INFLGFCO)+;
                  ","+cp_ToStrODBCNull(this.w_INESEPRE)+;
                  ","+cp_ToStrODBCNull(this.w_INNUMPRE)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'INVENTAR')
        i_extval=cp_InsertValVFPExtFlds(this,'INVENTAR')
        cp_CheckDeletedKey(i_cTable,0,'INNUMINV',this.w_INNUMINV,'INCODESE',this.w_INCODESE)
        INSERT INTO (i_cTable);
              (INNUMINV,INDATMEM,INCODESE,INTIPINV,INSTAINV,INELABOR,INDESINV,INCODMAG,INCATOMO,INGRUMER,INCODFAM,INDATINV,INMAGCOL,INFLGLCO,INFLGLSC,INFLGFCO,INESEPRE,INNUMPRE,UTCC,UTCV,UTDC,UTDV  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_INNUMINV;
                  ,this.w_INDATMEM;
                  ,this.w_INCODESE;
                  ,this.w_INTIPINV;
                  ,this.w_INSTAINV;
                  ,this.w_INELABOR;
                  ,this.w_INDESINV;
                  ,this.w_INCODMAG;
                  ,this.w_INCATOMO;
                  ,this.w_INGRUMER;
                  ,this.w_INCODFAM;
                  ,this.w_INDATINV;
                  ,this.w_INMAGCOL;
                  ,this.w_INFLGLCO;
                  ,this.w_INFLGLSC;
                  ,this.w_INFLGFCO;
                  ,this.w_INESEPRE;
                  ,this.w_INNUMPRE;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.INVENTAR_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.INVENTAR_IDX,i_nConn)
      *
      * update INVENTAR
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'INVENTAR')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " INDATMEM="+cp_ToStrODBC(this.w_INDATMEM)+;
             ",INTIPINV="+cp_ToStrODBC(this.w_INTIPINV)+;
             ",INSTAINV="+cp_ToStrODBC(this.w_INSTAINV)+;
             ",INELABOR="+cp_ToStrODBC(this.w_INELABOR)+;
             ",INDESINV="+cp_ToStrODBC(this.w_INDESINV)+;
             ",INCODMAG="+cp_ToStrODBCNull(this.w_INCODMAG)+;
             ",INCATOMO="+cp_ToStrODBCNull(this.w_INCATOMO)+;
             ",INGRUMER="+cp_ToStrODBCNull(this.w_INGRUMER)+;
             ",INCODFAM="+cp_ToStrODBCNull(this.w_INCODFAM)+;
             ",INDATINV="+cp_ToStrODBC(this.w_INDATINV)+;
             ",INMAGCOL="+cp_ToStrODBC(this.w_INMAGCOL)+;
             ",INFLGLCO="+cp_ToStrODBC(this.w_INFLGLCO)+;
             ",INFLGLSC="+cp_ToStrODBC(this.w_INFLGLSC)+;
             ",INFLGFCO="+cp_ToStrODBC(this.w_INFLGFCO)+;
             ",INESEPRE="+cp_ToStrODBCNull(this.w_INESEPRE)+;
             ",INNUMPRE="+cp_ToStrODBCNull(this.w_INNUMPRE)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'INVENTAR')
        i_cWhere = cp_PKFox(i_cTable  ,'INNUMINV',this.w_INNUMINV  ,'INCODESE',this.w_INCODESE  )
        UPDATE (i_cTable) SET;
              INDATMEM=this.w_INDATMEM;
             ,INTIPINV=this.w_INTIPINV;
             ,INSTAINV=this.w_INSTAINV;
             ,INELABOR=this.w_INELABOR;
             ,INDESINV=this.w_INDESINV;
             ,INCODMAG=this.w_INCODMAG;
             ,INCATOMO=this.w_INCATOMO;
             ,INGRUMER=this.w_INGRUMER;
             ,INCODFAM=this.w_INCODFAM;
             ,INDATINV=this.w_INDATINV;
             ,INMAGCOL=this.w_INMAGCOL;
             ,INFLGLCO=this.w_INFLGLCO;
             ,INFLGLSC=this.w_INFLGLSC;
             ,INFLGFCO=this.w_INFLGFCO;
             ,INESEPRE=this.w_INESEPRE;
             ,INNUMPRE=this.w_INNUMPRE;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.INVENTAR_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.INVENTAR_IDX,i_nConn)
      *
      * delete INVENTAR
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'INNUMINV',this.w_INNUMINV  ,'INCODESE',this.w_INCODESE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.INVENTAR_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,12,.t.)
        if .o_INTIPINV<>.w_INTIPINV
            .w_INCATOMO = space(5)
          .link_1_13('Full')
        endif
        .DoRTCalc(14,14,.t.)
        if .o_INTIPINV<>.w_INTIPINV
            .w_INGRUMER = space(5)
          .link_1_15('Full')
        endif
        .DoRTCalc(16,16,.t.)
        if .o_INTIPINV<>.w_INTIPINV
            .w_INCODFAM = space(5)
          .link_1_17('Full')
        endif
        .DoRTCalc(18,18,.t.)
        if .o_INDATMEM<>.w_INDATMEM
            .w_INDATINV = .w_INDATMEM
        endif
        .DoRTCalc(20,21,.t.)
        if .o_COLL<>.w_COLL.or. .o_INMAGCOL<>.w_INMAGCOL
            .w_INFLGLCO = iif(empty(nvl(.w_COLL,"")) and .w_INMAGCOL='S',.w_INFLGLCO,'N')
        endif
        if .o_COLL<>.w_COLL.or. .o_INMAGCOL<>.w_INMAGCOL
            .w_INFLGLSC = iif(empty(nvl(.w_COLL,"")) and .w_INMAGCOL='S',.w_INFLGLSC,'N')
        endif
        if .o_COLL<>.w_COLL.or. .o_INMAGCOL<>.w_INMAGCOL
            .w_INFLGFCO = iif(empty(nvl(.w_COLL,"")) and .w_INMAGCOL='S',.w_INFLGFCO,'N')
        endif
        .DoRTCalc(25,25,.t.)
        if .o_INMAGCOL<>.w_INMAGCOL
            .w_INNUMPRE = iif(.w_INMAGCOL$'N|S',space(6),.w_INNUMPRE)
          .link_1_26('Full')
        endif
        .DoRTCalc(27,27,.t.)
            .w_UTCV = i_CODUTE
        .DoRTCalc(29,29,.t.)
            .w_UTDV = i_DATSYS
        .oPgFrm.Page1.oPag.oObj_1_35.Calculate()
        .DoRTCalc(31,34,.t.)
        if .o_INDATMEM<>.w_INDATMEM
            .w_OBTEST = .w_INDATMEM
        endif
        .oPgFrm.Page1.oPag.oObj_1_40.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_47.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_codazi<>.w_codazi .or. .op_INCODESE<>.w_INCODESE
           cp_AskTableProg(this,i_nConn,"SEINV","i_codazi,w_INCODESE,w_INNUMINV")
          .op_INNUMINV = .w_INNUMINV
        endif
        .op_codazi = .w_codazi
        .op_INCODESE = .w_INCODESE
      endwith
      this.DoRTCalc(36,44,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_35.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_40.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_47.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oINCODESE_1_5.enabled = this.oPgFrm.Page1.oPag.oINCODESE_1_5.mCond()
    this.oPgFrm.Page1.oPag.oINCATOMO_1_13.enabled = this.oPgFrm.Page1.oPag.oINCATOMO_1_13.mCond()
    this.oPgFrm.Page1.oPag.oINGRUMER_1_15.enabled = this.oPgFrm.Page1.oPag.oINGRUMER_1_15.mCond()
    this.oPgFrm.Page1.oPag.oINCODFAM_1_17.enabled = this.oPgFrm.Page1.oPag.oINCODFAM_1_17.mCond()
    this.oPgFrm.Page1.oPag.oINFLGLCO_1_22.enabled = this.oPgFrm.Page1.oPag.oINFLGLCO_1_22.mCond()
    this.oPgFrm.Page1.oPag.oINFLGLSC_1_23.enabled = this.oPgFrm.Page1.oPag.oINFLGLSC_1_23.mCond()
    this.oPgFrm.Page1.oPag.oINFLGFCO_1_24.enabled = this.oPgFrm.Page1.oPag.oINFLGFCO_1_24.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_62.enabled = this.oPgFrm.Page1.oPag.oBtn_1_62.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oBtn_1_62.visible=!this.oPgFrm.Page1.oPag.oBtn_1_62.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_35.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_40.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_41.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_47.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=INCODESE
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_INCODESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_KES',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_INCODESE)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESVALNAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_AZIENDA;
                     ,'ESCODESE',trim(this.w_INCODESE))
          select ESCODAZI,ESCODESE,ESINIESE,ESVALNAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_INCODESE)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_INCODESE) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oINCODESE_1_5'),i_cWhere,'GSAR_KES',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_AZIENDA<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESVALNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESINIESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("La valuta di conto dell'esercizio deve essere Euro")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESVALNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESINIESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_INCODESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESVALNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_INCODESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_AZIENDA;
                       ,'ESCODESE',this.w_INCODESE)
            select ESCODAZI,ESCODESE,ESINIESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_INCODESE = NVL(_Link_.ESCODESE,space(4))
      this.w_ESINIESE = NVL(cp_ToDate(_Link_.ESINIESE),ctod("  /  /  "))
      this.w_VALESE = NVL(_Link_.ESVALNAZ,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_INCODESE = space(4)
      endif
      this.w_ESINIESE = ctod("  /  /  ")
      this.w_VALESE = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_VALESE=g_CODEUR
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("La valuta di conto dell'esercizio deve essere Euro")
        endif
        this.w_INCODESE = space(4)
        this.w_ESINIESE = ctod("  /  /  ")
        this.w_VALESE = space(3)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_INCODESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=INCODMAG
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_INCODMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_INCODMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGFISMAG,MGDTOBSO,MGMAGRAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_INCODMAG))
          select MGCODMAG,MGDESMAG,MGFISMAG,MGDTOBSO,MGMAGRAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_INCODMAG)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStrODBC(trim(this.w_INCODMAG)+"%");

            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGFISMAG,MGDTOBSO,MGMAGRAG";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStr(trim(this.w_INCODMAG)+"%");

            select MGCODMAG,MGDESMAG,MGFISMAG,MGDTOBSO,MGMAGRAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_INCODMAG) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oINCODMAG_1_10'),i_cWhere,'',"",'GSMA_AIM.MAGAZZIN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGFISMAG,MGDTOBSO,MGMAGRAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG,MGFISMAG,MGDTOBSO,MGMAGRAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_INCODMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGFISMAG,MGDTOBSO,MGMAGRAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_INCODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_INCODMAG)
            select MGCODMAG,MGDESMAG,MGFISMAG,MGDTOBSO,MGMAGRAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_INCODMAG = NVL(_Link_.MGCODMAG,space(5))
      this.w_MGDESMAG = NVL(_Link_.MGDESMAG,space(30))
      this.w_MGFISMAG = NVL(_Link_.MGFISMAG,space(1))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.MGDTOBSO),ctod("  /  /  "))
      this.w_COLL = NVL(_Link_.MGMAGRAG,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_INCODMAG = space(5)
      endif
      this.w_MGDESMAG = space(30)
      this.w_MGFISMAG = space(1)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_COLL = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_MGFISMAG="S") AND (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Magazzino inesistente oppure obsoleto")
        endif
        this.w_INCODMAG = space(5)
        this.w_MGDESMAG = space(30)
        this.w_MGFISMAG = space(1)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_COLL = space(5)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_INCODMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=INCATOMO
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_INCATOMO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AOM',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_INCATOMO)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_INCATOMO))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_INCATOMO)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_INCATOMO) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'oINCATOMO_1_13'),i_cWhere,'GSAR_AOM',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_INCATOMO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_INCATOMO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_INCATOMO)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_INCATOMO = NVL(_Link_.OMCODICE,space(5))
      this.w_OMDESCRI = NVL(cp_TransLoadField('_Link_.OMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_INCATOMO = space(5)
      endif
      this.w_OMDESCRI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_INCATOMO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=INGRUMER
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_INGRUMER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGM',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_INGRUMER)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_INGRUMER))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_INGRUMER)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_INGRUMER) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oINGRUMER_1_15'),i_cWhere,'GSAR_AGM',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_INGRUMER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_INGRUMER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_INGRUMER)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_INGRUMER = NVL(_Link_.GMCODICE,space(5))
      this.w_GMDESCRI = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_INGRUMER = space(5)
      endif
      this.w_GMDESCRI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_INGRUMER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=INCODFAM
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_INCODFAM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFA',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_INCODFAM)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_INCODFAM))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_INCODFAM)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_INCODFAM) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oINCODFAM_1_17'),i_cWhere,'GSAR_AFA',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_INCODFAM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_INCODFAM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_INCODFAM)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_INCODFAM = NVL(_Link_.FACODICE,space(5))
      this.w_FADESCRI = NVL(cp_TransLoadField('_Link_.FADESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_INCODFAM = space(5)
      endif
      this.w_FADESCRI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_INCODFAM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=INESEPRE
  func Link_1_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_INESEPRE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_KES',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_INESEPRE)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ,ESINIESE,ESFINESE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_AZIENDA;
                     ,'ESCODESE',trim(this.w_INESEPRE))
          select ESCODAZI,ESCODESE,ESVALNAZ,ESINIESE,ESFINESE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_INESEPRE)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_INESEPRE) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oINESEPRE_1_25'),i_cWhere,'GSAR_KES',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_AZIENDA<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ,ESINIESE,ESFINESE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESVALNAZ,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("La valuta di conto dell'esercizio deve essere in Lire")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ,ESINIESE,ESFINESE";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESVALNAZ,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_INESEPRE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ,ESINIESE,ESFINESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_INESEPRE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_AZIENDA;
                       ,'ESCODESE',this.w_INESEPRE)
            select ESCODAZI,ESCODESE,ESVALNAZ,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_INESEPRE = NVL(_Link_.ESCODESE,space(4))
      this.w_VALPRE = NVL(_Link_.ESVALNAZ,space(3))
      this.w_PRINIESE = NVL(cp_ToDate(_Link_.ESINIESE),ctod("  /  /  "))
      this.w_PRFINESE = NVL(cp_ToDate(_Link_.ESFINESE),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_INESEPRE = space(4)
      endif
      this.w_VALPRE = space(3)
      this.w_PRINIESE = ctod("  /  /  ")
      this.w_PRFINESE = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_VALPRE=g_CODLIR
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("La valuta di conto dell'esercizio deve essere in Lire")
        endif
        this.w_INESEPRE = space(4)
        this.w_VALPRE = space(3)
        this.w_PRINIESE = ctod("  /  /  ")
        this.w_PRFINESE = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_INESEPRE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=INNUMPRE
  func Link_1_26(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.INVENTAR_IDX,3]
    i_lTable = "INVENTAR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2], .t., this.INVENTAR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_INNUMPRE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsma_ain',True,'INVENTAR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" INNUMINV like "+cp_ToStrODBC(trim(this.w_INNUMPRE)+"%");
                   +" and INCODESE="+cp_ToStrODBC(this.w_INESEPRE);

          i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INDATINV,INSTAINV,INCATOMO,INGRUMER,INCODFAM,INCODMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by INCODESE,INNUMINV","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'INCODESE',this.w_INESEPRE;
                     ,'INNUMINV',trim(this.w_INNUMPRE))
          select INCODESE,INNUMINV,INDATINV,INSTAINV,INCATOMO,INGRUMER,INCODFAM,INCODMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by INCODESE,INNUMINV into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_INNUMPRE)==trim(_Link_.INNUMINV) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_INNUMPRE) and !this.bDontReportError
            deferred_cp_zoom('INVENTAR','*','INCODESE,INNUMINV',cp_AbsName(oSource.parent,'oINNUMPRE_1_26'),i_cWhere,'gsma_ain',"",'GSMA_AIP.INVENTAR_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_INESEPRE<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INDATINV,INSTAINV,INCATOMO,INGRUMER,INCODFAM,INCODMAG";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select INCODESE,INNUMINV,INDATINV,INSTAINV,INCATOMO,INGRUMER,INCODFAM,INCODMAG;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INDATINV,INSTAINV,INCATOMO,INGRUMER,INCODFAM,INCODMAG";
                     +" from "+i_cTable+" "+i_lTable+" where INNUMINV="+cp_ToStrODBC(oSource.xKey(2));
                     +" and INCODESE="+cp_ToStrODBC(this.w_INESEPRE);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'INCODESE',oSource.xKey(1);
                       ,'INNUMINV',oSource.xKey(2))
            select INCODESE,INNUMINV,INDATINV,INSTAINV,INCATOMO,INGRUMER,INCODFAM,INCODMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_INNUMPRE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INDATINV,INSTAINV,INCATOMO,INGRUMER,INCODFAM,INCODMAG";
                   +" from "+i_cTable+" "+i_lTable+" where INNUMINV="+cp_ToStrODBC(this.w_INNUMPRE);
                   +" and INCODESE="+cp_ToStrODBC(this.w_INESEPRE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'INCODESE',this.w_INESEPRE;
                       ,'INNUMINV',this.w_INNUMPRE)
            select INCODESE,INNUMINV,INDATINV,INSTAINV,INCATOMO,INGRUMER,INCODFAM,INCODMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_INNUMPRE = NVL(_Link_.INNUMINV,space(6))
      this.w_PRDATINV = NVL(cp_ToDate(_Link_.INDATINV),ctod("  /  /  "))
      this.w_PRSTAINV = NVL(_Link_.INSTAINV,space(1))
      this.w_PRCATOMO = NVL(_Link_.INCATOMO,space(5))
      this.w_PRGRUMER = NVL(_Link_.INGRUMER,space(5))
      this.w_PRCODFAM = NVL(_Link_.INCODFAM,space(5))
      this.w_PRCODMAG = NVL(_Link_.INCODMAG,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_INNUMPRE = space(6)
      endif
      this.w_PRDATINV = ctod("  /  /  ")
      this.w_PRSTAINV = space(1)
      this.w_PRCATOMO = space(5)
      this.w_PRGRUMER = space(5)
      this.w_PRCODFAM = space(5)
      this.w_PRCODMAG = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2])+'\'+cp_ToStr(_Link_.INCODESE,1)+'\'+cp_ToStr(_Link_.INNUMINV,1)
      cp_ShowWarn(i_cKey,this.INVENTAR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_INNUMPRE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oINNUMINV_1_2.value==this.w_INNUMINV)
      this.oPgFrm.Page1.oPag.oINNUMINV_1_2.value=this.w_INNUMINV
    endif
    if not(this.oPgFrm.Page1.oPag.oINDATMEM_1_3.value==this.w_INDATMEM)
      this.oPgFrm.Page1.oPag.oINDATMEM_1_3.value=this.w_INDATMEM
    endif
    if not(this.oPgFrm.Page1.oPag.oINCODESE_1_5.value==this.w_INCODESE)
      this.oPgFrm.Page1.oPag.oINCODESE_1_5.value=this.w_INCODESE
    endif
    if not(this.oPgFrm.Page1.oPag.oINTIPINV_1_6.RadioValue()==this.w_INTIPINV)
      this.oPgFrm.Page1.oPag.oINTIPINV_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oINSTAINV_1_7.RadioValue()==this.w_INSTAINV)
      this.oPgFrm.Page1.oPag.oINSTAINV_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oINELABOR_1_8.RadioValue()==this.w_INELABOR)
      this.oPgFrm.Page1.oPag.oINELABOR_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oINDESINV_1_9.value==this.w_INDESINV)
      this.oPgFrm.Page1.oPag.oINDESINV_1_9.value=this.w_INDESINV
    endif
    if not(this.oPgFrm.Page1.oPag.oINCODMAG_1_10.value==this.w_INCODMAG)
      this.oPgFrm.Page1.oPag.oINCODMAG_1_10.value=this.w_INCODMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oMGDESMAG_1_11.value==this.w_MGDESMAG)
      this.oPgFrm.Page1.oPag.oMGDESMAG_1_11.value=this.w_MGDESMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oINCATOMO_1_13.value==this.w_INCATOMO)
      this.oPgFrm.Page1.oPag.oINCATOMO_1_13.value=this.w_INCATOMO
    endif
    if not(this.oPgFrm.Page1.oPag.oOMDESCRI_1_14.value==this.w_OMDESCRI)
      this.oPgFrm.Page1.oPag.oOMDESCRI_1_14.value=this.w_OMDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oINGRUMER_1_15.value==this.w_INGRUMER)
      this.oPgFrm.Page1.oPag.oINGRUMER_1_15.value=this.w_INGRUMER
    endif
    if not(this.oPgFrm.Page1.oPag.oGMDESCRI_1_16.value==this.w_GMDESCRI)
      this.oPgFrm.Page1.oPag.oGMDESCRI_1_16.value=this.w_GMDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oINCODFAM_1_17.value==this.w_INCODFAM)
      this.oPgFrm.Page1.oPag.oINCODFAM_1_17.value=this.w_INCODFAM
    endif
    if not(this.oPgFrm.Page1.oPag.oFADESCRI_1_18.value==this.w_FADESCRI)
      this.oPgFrm.Page1.oPag.oFADESCRI_1_18.value=this.w_FADESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oINDATINV_1_19.value==this.w_INDATINV)
      this.oPgFrm.Page1.oPag.oINDATINV_1_19.value=this.w_INDATINV
    endif
    if not(this.oPgFrm.Page1.oPag.oFLMESS_1_20.RadioValue()==this.w_FLMESS)
      this.oPgFrm.Page1.oPag.oFLMESS_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oINMAGCOL_1_21.RadioValue()==this.w_INMAGCOL)
      this.oPgFrm.Page1.oPag.oINMAGCOL_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oINFLGLCO_1_22.RadioValue()==this.w_INFLGLCO)
      this.oPgFrm.Page1.oPag.oINFLGLCO_1_22.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oINFLGLSC_1_23.RadioValue()==this.w_INFLGLSC)
      this.oPgFrm.Page1.oPag.oINFLGLSC_1_23.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oINFLGFCO_1_24.RadioValue()==this.w_INFLGFCO)
      this.oPgFrm.Page1.oPag.oINFLGFCO_1_24.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oINESEPRE_1_25.value==this.w_INESEPRE)
      this.oPgFrm.Page1.oPag.oINESEPRE_1_25.value=this.w_INESEPRE
    endif
    if not(this.oPgFrm.Page1.oPag.oINNUMPRE_1_26.value==this.w_INNUMPRE)
      this.oPgFrm.Page1.oPag.oINNUMPRE_1_26.value=this.w_INNUMPRE
    endif
    if not(this.oPgFrm.Page1.oPag.oPRDATINV_1_33.value==this.w_PRDATINV)
      this.oPgFrm.Page1.oPag.oPRDATINV_1_33.value=this.w_PRDATINV
    endif
    cp_SetControlsValueExtFlds(this,'INVENTAR')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_INDATMEM))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oINDATMEM_1_3.SetFocus()
            i_bnoObbl = !empty(.w_INDATMEM)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_INCODESE)) or not(.w_VALESE=g_CODEUR))  and (.cFunction='Load')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oINCODESE_1_5.SetFocus()
            i_bnoObbl = !empty(.w_INCODESE)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La valuta di conto dell'esercizio deve essere Euro")
          case   (empty(.w_INDESINV))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oINDESINV_1_9.SetFocus()
            i_bnoObbl = !empty(.w_INDESINV)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_INCODMAG)) or not((.w_MGFISMAG="S") AND (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oINCODMAG_1_10.SetFocus()
            i_bnoObbl = !empty(.w_INCODMAG)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Magazzino inesistente oppure obsoleto")
          case   (empty(.w_INDATINV))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oINDATINV_1_19.SetFocus()
            i_bnoObbl = !empty(.w_INDATINV)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_INESEPRE)) or not(.w_VALPRE=g_CODLIR))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oINESEPRE_1_25.SetFocus()
            i_bnoObbl = !empty(.w_INESEPRE)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La valuta di conto dell'esercizio deve essere in Lire")
          case   (empty(.w_INNUMPRE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oINNUMPRE_1_26.SetFocus()
            i_bnoObbl = !empty(.w_INNUMPRE)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_INDATMEM = this.w_INDATMEM
    this.o_INTIPINV = this.w_INTIPINV
    this.o_COLL = this.w_COLL
    this.o_INMAGCOL = this.w_INMAGCOL
    return

enddefine

* --- Define pages as container
define class tgsek_ainPag1 as StdContainer
  Width  = 718
  height = 317
  stdWidth  = 718
  stdheight = 317
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oINNUMINV_1_2 as StdField with uid="BZDWYOOXVV",rtseq=2,rtrep=.f.,;
    cFormVar = "w_INNUMINV", cQueryName = "INNUMINV",nZero=6,;
    bObbl = .f. , nPag = 1, value=space(6), bMultilanguage =  .f.,;
    ToolTipText = "Numero progressivo dell'inventario",;
    HelpContextID = 228610012,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=141, Top=12, cSayPict='"999999"', cGetPict='"999999"', InputMask=replicate('X',6)

  add object oINDATMEM_1_3 as StdField with uid="ADCNAPESLT",rtseq=3,rtrep=.f.,;
    cFormVar = "w_INDATMEM", cQueryName = "INDATMEM",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di elaborazione dell'inventario",;
    HelpContextID = 235163693,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=265, Top=12

  add object oINCODESE_1_5 as StdField with uid="EYZFQUTPQR",rtseq=5,rtrep=.f.,;
    cFormVar = "w_INCODESE", cQueryName = "INNUMINV,INCODESE",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "La valuta di conto dell'esercizio deve essere Euro",;
    ToolTipText = "Codice esercizio di riferimento",;
    HelpContextID = 116809781,;
   bGlobalFont=.t.,;
    Height=21, Width=49, Left=454, Top=12, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", cZoomOnZoom="GSAR_KES", oKey_1_1="ESCODAZI", oKey_1_2="this.w_AZIENDA", oKey_2_1="ESCODESE", oKey_2_2="this.w_INCODESE"

  func oINCODESE_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  func oINCODESE_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oINCODESE_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oINCODESE_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_AZIENDA)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_AZIENDA)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oINCODESE_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_KES',"",'',this.parent.oContained
  endproc
  proc oINCODESE_1_5.mZoomOnZoom
    local i_obj
    i_obj=GSAR_KES()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ESCODAZI=w_AZIENDA
     i_obj.w_ESCODESE=this.parent.oContained.w_INCODESE
     i_obj.ecpSave()
  endproc


  add object oINTIPINV_1_6 as StdCombo with uid="QBGHLBOBBZ",rtseq=6,rtrep=.f.,left=579,top=12,width=88,height=21;
    , ToolTipText = "Considera tutti gli articoli o solo quelli che rispettano i criteri di selezione";
    , HelpContextID = 230993884;
    , cFormVar="w_INTIPINV",RowSource=""+"Globale,"+"Parziale", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oINTIPINV_1_6.RadioValue()
    return(iif(this.value =1,'G',;
    iif(this.value =2,'P',;
    space(1))))
  endfunc
  func oINTIPINV_1_6.GetRadio()
    this.Parent.oContained.w_INTIPINV = this.RadioValue()
    return .t.
  endfunc

  func oINTIPINV_1_6.SetRadio()
    this.Parent.oContained.w_INTIPINV=trim(this.Parent.oContained.w_INTIPINV)
    this.value = ;
      iif(this.Parent.oContained.w_INTIPINV=='G',1,;
      iif(this.Parent.oContained.w_INTIPINV=='P',2,;
      0))
  endfunc


  add object oINSTAINV_1_7 as StdCombo with uid="BFMZDBJFXG",rtseq=7,rtrep=.f.,left=579,top=45,width=95,height=21;
    , ToolTipText = "Indica se l'inventario � confermato o deve essere ulteriormente elaborato";
    , HelpContextID = 215982044;
    , cFormVar="w_INSTAINV",RowSource=""+"Provvisorio,"+"Confermato,"+"Storico", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oINSTAINV_1_7.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'D',;
    iif(this.value =3,'S',;
    space(1)))))
  endfunc
  func oINSTAINV_1_7.GetRadio()
    this.Parent.oContained.w_INSTAINV = this.RadioValue()
    return .t.
  endfunc

  func oINSTAINV_1_7.SetRadio()
    this.Parent.oContained.w_INSTAINV=trim(this.Parent.oContained.w_INSTAINV)
    this.value = ;
      iif(this.Parent.oContained.w_INSTAINV=='P',1,;
      iif(this.Parent.oContained.w_INSTAINV=='D',2,;
      iif(this.Parent.oContained.w_INSTAINV=='S',3,;
      0)))
  endfunc

  add object oINELABOR_1_8 as StdCheck with uid="TZIZEYDKCC",rtseq=8,rtrep=.f.,left=579, top=79, caption="Elaborato", enabled=.f.,;
    ToolTipText = "Se attivato indica che l'inventario � stato elaborato",;
    HelpContextID = 170475560,;
    cFormVar="w_INELABOR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oINELABOR_1_8.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oINELABOR_1_8.GetRadio()
    this.Parent.oContained.w_INELABOR = this.RadioValue()
    return .t.
  endfunc

  func oINELABOR_1_8.SetRadio()
    this.Parent.oContained.w_INELABOR=trim(this.Parent.oContained.w_INELABOR)
    this.value = ;
      iif(this.Parent.oContained.w_INELABOR=='S',1,;
      0)
  endfunc

  add object oINDESINV_1_9 as StdField with uid="JMHFXTPPFA",rtseq=9,rtrep=.f.,;
    cFormVar = "w_INDESINV", cQueryName = "INDESINV",;
    bObbl = .t. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione dell'inventario",;
    HelpContextID = 233811932,;
   bGlobalFont=.t.,;
    Height=21, Width=353, Left=141, Top=45, InputMask=replicate('X',40)

  add object oINCODMAG_1_10 as StdField with uid="PLGYRSYCSB",rtseq=10,rtrep=.f.,;
    cFormVar = "w_INCODMAG", cQueryName = "INCODMAG",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Magazzino inesistente oppure obsoleto",;
    ToolTipText = "Codice del magazzino da elaborare",;
    HelpContextID = 251027507,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=141, Top=90, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", oKey_1_1="MGCODMAG", oKey_1_2="this.w_INCODMAG"

  func oINCODMAG_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oINCODMAG_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oINCODMAG_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oINCODMAG_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'GSMA_AIM.MAGAZZIN_VZM',this.parent.oContained
  endproc

  add object oMGDESMAG_1_11 as StdField with uid="IILEAYXTFO",rtseq=11,rtrep=.f.,;
    cFormVar = "w_MGDESMAG", cQueryName = "MGDESMAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 235951859,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=213, Top=90, InputMask=replicate('X',30)

  add object oINCATOMO_1_13 as StdField with uid="XPAKCRLOPM",rtseq=13,rtrep=.f.,;
    cFormVar = "w_INCATOMO", cQueryName = "INCATOMO",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice della categoria omogenea da elaborare",;
    HelpContextID = 66822101,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=141, Top=118, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", cZoomOnZoom="GSAR_AOM", oKey_1_1="OMCODICE", oKey_1_2="this.w_INCATOMO"

  func oINCATOMO_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_INTIPINV = "P")
    endwith
   endif
  endfunc

  func oINCATOMO_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oINCATOMO_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oINCATOMO_1_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'oINCATOMO_1_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AOM',"",'',this.parent.oContained
  endproc
  proc oINCATOMO_1_13.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AOM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_OMCODICE=this.parent.oContained.w_INCATOMO
     i_obj.ecpSave()
  endproc

  add object oOMDESCRI_1_14 as StdField with uid="YLHZUVVYPO",rtseq=14,rtrep=.f.,;
    cFormVar = "w_OMDESCRI", cQueryName = "OMDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 135286993,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=213, Top=118, InputMask=replicate('X',35)

  add object oINGRUMER_1_15 as StdField with uid="EYOXYLCBRA",rtseq=15,rtrep=.f.,;
    cFormVar = "w_INGRUMER", cQueryName = "INGRUMER",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice del gruppo merceologico da elaborare",;
    HelpContextID = 232988712,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=141, Top=146, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", cZoomOnZoom="GSAR_AGM", oKey_1_1="GMCODICE", oKey_1_2="this.w_INGRUMER"

  func oINGRUMER_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_INTIPINV = "P")
    endwith
   endif
  endfunc

  func oINGRUMER_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oINGRUMER_1_15.ecpDrop(oSource)
    this.Parent.oContained.link_1_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oINGRUMER_1_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oINGRUMER_1_15'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGM',"",'',this.parent.oContained
  endproc
  proc oINGRUMER_1_15.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GMCODICE=this.parent.oContained.w_INGRUMER
     i_obj.ecpSave()
  endproc

  add object oGMDESCRI_1_16 as StdField with uid="IRUGVPUQKK",rtseq=16,rtrep=.f.,;
    cFormVar = "w_GMDESCRI", cQueryName = "GMDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 135287121,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=213, Top=146, InputMask=replicate('X',35)

  add object oINCODFAM_1_17 as StdField with uid="KFJRXGAAFI",rtseq=17,rtrep=.f.,;
    cFormVar = "w_INCODFAM", cQueryName = "INCODFAM",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice della famiglia da elaborare",;
    HelpContextID = 100032557,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=141, Top=174, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", cZoomOnZoom="GSAR_AFA", oKey_1_1="FACODICE", oKey_1_2="this.w_INCODFAM"

  func oINCODFAM_1_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_INTIPINV = "P")
    endwith
   endif
  endfunc

  func oINCODFAM_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oINCODFAM_1_17.ecpDrop(oSource)
    this.Parent.oContained.link_1_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oINCODFAM_1_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oINCODFAM_1_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFA',"",'',this.parent.oContained
  endproc
  proc oINCODFAM_1_17.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_FACODICE=this.parent.oContained.w_INCODFAM
     i_obj.ecpSave()
  endproc

  add object oFADESCRI_1_18 as StdField with uid="TLGVORLFBA",rtseq=18,rtrep=.f.,;
    cFormVar = "w_FADESCRI", cQueryName = "FADESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 135290209,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=213, Top=174, InputMask=replicate('X',35)

  add object oINDATINV_1_19 as StdField with uid="FAFHRZVGRX",rtseq=19,rtrep=.f.,;
    cFormVar = "w_INDATINV", cQueryName = "INDATINV",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data fino alla quale devono essere considerati i movimenti",;
    HelpContextID = 234598364,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=141, Top=231

  add object oFLMESS_1_20 as StdCheck with uid="IGHVPGCNYU",rtseq=20,rtrep=.f.,left=230, top=231, caption="Richieste a video",;
    ToolTipText = "Se attivo: visualizza eventuali richieste di conferma/errori, altrimenti conferma",;
    HelpContextID = 135250602,;
    cFormVar="w_FLMESS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLMESS_1_20.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oFLMESS_1_20.GetRadio()
    this.Parent.oContained.w_FLMESS = this.RadioValue()
    return .t.
  endfunc

  func oFLMESS_1_20.SetRadio()
    this.Parent.oContained.w_FLMESS=trim(this.Parent.oContained.w_FLMESS)
    this.value = ;
      iif(this.Parent.oContained.w_FLMESS=='S',1,;
      0)
  endfunc

  add object oINMAGCOL_1_21 as StdCheck with uid="AIQEETGZFS",rtseq=21,rtrep=.f.,left=579, top=104, caption="Quantit� totale",;
    ToolTipText = "Considera la quantit� di tutti i magazzini fiscali",;
    HelpContextID = 148095022,;
    cFormVar="w_INMAGCOL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oINMAGCOL_1_21.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oINMAGCOL_1_21.GetRadio()
    this.Parent.oContained.w_INMAGCOL = this.RadioValue()
    return .t.
  endfunc

  func oINMAGCOL_1_21.SetRadio()
    this.Parent.oContained.w_INMAGCOL=trim(this.Parent.oContained.w_INMAGCOL)
    this.value = ;
      iif(this.Parent.oContained.w_INMAGCOL=='S',1,;
      0)
  endfunc

  add object oINFLGLCO_1_22 as StdCheck with uid="DYAPSFHTNW",rtseq=22,rtrep=.f.,left=579, top=138, caption="LIFO continuo",;
    ToolTipText = "Se attivato calcola la valorizzazione con criterio LIFO continuo",;
    HelpContextID = 264843307,;
    cFormVar="w_INFLGLCO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oINFLGLCO_1_22.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oINFLGLCO_1_22.GetRadio()
    this.Parent.oContained.w_INFLGLCO = this.RadioValue()
    return .t.
  endfunc

  func oINFLGLCO_1_22.SetRadio()
    this.Parent.oContained.w_INFLGLCO=trim(this.Parent.oContained.w_INFLGLCO)
    this.value = ;
      iif(this.Parent.oContained.w_INFLGLCO=='S',1,;
      0)
  endfunc

  func oINFLGLCO_1_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(nvl(.w_COLL,"")) and .w_INMAGCOL='S')
    endwith
   endif
  endfunc

  add object oINFLGLSC_1_23 as StdCheck with uid="ISVITMGTDU",rtseq=23,rtrep=.f.,left=579, top=157, caption="LIFO a scatti",;
    ToolTipText = "Se attivato calcola la valorizzazione con criterio LIFO a scatti",;
    HelpContextID = 264843319,;
    cFormVar="w_INFLGLSC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oINFLGLSC_1_23.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oINFLGLSC_1_23.GetRadio()
    this.Parent.oContained.w_INFLGLSC = this.RadioValue()
    return .t.
  endfunc

  func oINFLGLSC_1_23.SetRadio()
    this.Parent.oContained.w_INFLGLSC=trim(this.Parent.oContained.w_INFLGLSC)
    this.value = ;
      iif(this.Parent.oContained.w_INFLGLSC=='S',1,;
      0)
  endfunc

  func oINFLGLSC_1_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(nvl(.w_COLL,"")) and .w_INMAGCOL='S')
    endwith
   endif
  endfunc

  add object oINFLGFCO_1_24 as StdCheck with uid="HYPVPZPUUV",rtseq=24,rtrep=.f.,left=579, top=176, caption="FIFO continuo",;
    ToolTipText = "Se attivato calcola la valorizzazione con criterio FIFO continuo",;
    HelpContextID = 97071147,;
    cFormVar="w_INFLGFCO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oINFLGFCO_1_24.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oINFLGFCO_1_24.GetRadio()
    this.Parent.oContained.w_INFLGFCO = this.RadioValue()
    return .t.
  endfunc

  func oINFLGFCO_1_24.SetRadio()
    this.Parent.oContained.w_INFLGFCO=trim(this.Parent.oContained.w_INFLGFCO)
    this.value = ;
      iif(this.Parent.oContained.w_INFLGFCO=='S',1,;
      0)
  endfunc

  func oINFLGFCO_1_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(nvl(.w_COLL,"")) and .w_INMAGCOL='S')
    endwith
   endif
  endfunc

  add object oINESEPRE_1_25 as StdField with uid="IGMAVRBFTU",rtseq=25,rtrep=.f.,;
    cFormVar = "w_INESEPRE", cQueryName = "INESEPRE",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "La valuta di conto dell'esercizio deve essere in Lire",;
    ToolTipText = "Esercizio dell'inventario precedente da considerare come valorizzazione iniziale",;
    HelpContextID = 199376949,;
   bGlobalFont=.t.,;
    Height=21, Width=49, Left=141, Top=292, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", cZoomOnZoom="GSAR_KES", oKey_1_1="ESCODAZI", oKey_1_2="this.w_AZIENDA", oKey_2_1="ESCODESE", oKey_2_2="this.w_INESEPRE"

  func oINESEPRE_1_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_25('Part',this)
      if .not. empty(.w_INNUMPRE)
        bRes2=.link_1_26('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oINESEPRE_1_25.ecpDrop(oSource)
    this.Parent.oContained.link_1_25('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oINESEPRE_1_25.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_AZIENDA)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_AZIENDA)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oINESEPRE_1_25'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_KES',"",'',this.parent.oContained
  endproc
  proc oINESEPRE_1_25.mZoomOnZoom
    local i_obj
    i_obj=GSAR_KES()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ESCODAZI=w_AZIENDA
     i_obj.w_ESCODESE=this.parent.oContained.w_INESEPRE
     i_obj.ecpSave()
  endproc

  add object oINNUMPRE_1_26 as StdField with uid="JCVUPNEKKI",rtseq=26,rtrep=.f.,;
    cFormVar = "w_INNUMPRE", cQueryName = "INNUMPRE",nZero=6,;
    bObbl = .t. , nPag = 1, value=space(6), bMultilanguage =  .f.,;
    ToolTipText = "Numero dell'inventario precedente da considerare come valorizzazione iniziale",;
    HelpContextID = 190820405,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=260, Top=292, cSayPict='v_ZR+"999999"', cGetPict='v_ZR+"999999"', InputMask=replicate('X',6), bHasZoom = .t. , cLinkFile="INVENTAR", cZoomOnZoom="gsma_ain", oKey_1_1="INCODESE", oKey_1_2="this.w_INESEPRE", oKey_2_1="INNUMINV", oKey_2_2="this.w_INNUMPRE"

  func oINNUMPRE_1_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_26('Part',this)
    endwith
    return bRes
  endfunc

  proc oINNUMPRE_1_26.ecpDrop(oSource)
    this.Parent.oContained.link_1_26('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oINNUMPRE_1_26.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.INVENTAR_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"INCODESE="+cp_ToStrODBC(this.Parent.oContained.w_INESEPRE)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"INCODESE="+cp_ToStr(this.Parent.oContained.w_INESEPRE)
    endif
    do cp_zoom with 'INVENTAR','*','INCODESE,INNUMINV',cp_AbsName(this.parent,'oINNUMPRE_1_26'),iif(empty(i_cWhere),.f.,i_cWhere),'gsma_ain',"",'GSMA_AIP.INVENTAR_VZM',this.parent.oContained
  endproc
  proc oINNUMPRE_1_26.mZoomOnZoom
    local i_obj
    i_obj=gsma_ain()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.INCODESE=w_INESEPRE
     i_obj.w_INNUMINV=this.parent.oContained.w_INNUMPRE
     i_obj.ecpSave()
  endproc

  add object oPRDATINV_1_33 as StdField with uid="AWDCKIQDNJ",rtseq=31,rtrep=.f.,;
    cFormVar = "w_PRDATINV", cQueryName = "PRDATINV",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data fino alla quale sono stati considerati i movimenti dell'inventario prec.",;
    HelpContextID = 234599500,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=414, Top=292


  add object oObj_1_35 as cp_runprogram with uid="LTSQRIFWLT",left=177, top=336, width=106,height=17,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='gsma_bie',;
    cEvent = "Delete start",;
    nPag=1;
    , ToolTipText = "Eliminazione figli articoli";
    , HelpContextID = 99474458


  add object oObj_1_40 as cp_runprogram with uid="GWXJYEEXIC",left=2, top=336, width=170,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSEK_BIN('INSERTED')",;
    cEvent = "Record Inserted",;
    nPag=1;
    , HelpContextID = 99474458


  add object oObj_1_41 as cp_runprogram with uid="EZUXQEMUAA",left=289, top=336, width=131,height=17,;
    caption='GSEK_BIN',;
   bGlobalFont=.t.,;
    prg="GSEK_BIN('UPDATED')",;
    cEvent = "Record Updated",;
    nPag=1;
    , HelpContextID = 129352884


  add object oObj_1_47 as cp_runprogram with uid="MAVOFZNODH",left=1, top=358, width=674,height=22,;
    caption='GSEK_BCI',;
   bGlobalFont=.t.,;
    prg="GSEK_BCI",;
    cEvent = "w_INCATOMO Changed,w_INGRUMER Changed,w_INCODFAM Changed,w_INNUMPRE Changed",;
    nPag=1;
    , ToolTipText = "Aggiorna la variabile check (calcola il check per innumpre)";
    , HelpContextID = 139082577


  add object oBtn_1_62 as StdButton with uid="GHOHCARKMI",left=666, top=269, width=48,height=45,;
    CpPicture="BMP\ELABORA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per elaborare i dati dell'inventario";
    , HelpContextID = 120677050;
    , Caption='\<Elabora';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_62.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_62.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return ( .w_INSTAINV<>'S')
      endwith
    endif
  endfunc

  func oBtn_1_62.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_INSTAINV ='S')
     endwith
    endif
  endfunc

  add object oStr_1_27 as StdString with uid="UEGIZBHUEA",Visible=.t., Left=13, Top=264,;
    Alignment=0, Width=384, Height=15,;
    Caption="Inventario precedente (solo inventari confermati)"  ;
  , bGlobalFont=.t.

  add object oStr_1_48 as StdString with uid="PJMFHQCBDY",Visible=.t., Left=10, Top=45,;
    Alignment=1, Width=129, Height=15,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_49 as StdString with uid="PUWHPYOYZY",Visible=.t., Left=10, Top=90,;
    Alignment=1, Width=129, Height=15,;
    Caption="Magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_50 as StdString with uid="SERWGHYFGP",Visible=.t., Left=10, Top=118,;
    Alignment=1, Width=129, Height=15,;
    Caption="Categoria omogenea:"  ;
  , bGlobalFont=.t.

  add object oStr_1_51 as StdString with uid="ZPKCQVLRTN",Visible=.t., Left=10, Top=146,;
    Alignment=1, Width=129, Height=15,;
    Caption="Gruppo merceologico:"  ;
  , bGlobalFont=.t.

  add object oStr_1_52 as StdString with uid="DYGDEOXAYG",Visible=.t., Left=10, Top=174,;
    Alignment=1, Width=129, Height=15,;
    Caption="Famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_53 as StdString with uid="QTOPCDKFZK",Visible=.t., Left=1, Top=233,;
    Alignment=1, Width=138, Height=15,;
    Caption="Movimenti fino al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_54 as StdString with uid="IZIYZJKIUZ",Visible=.t., Left=10, Top=12,;
    Alignment=1, Width=129, Height=15,;
    Caption="Numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_55 as StdString with uid="QURMWECYZE",Visible=.t., Left=208, Top=12,;
    Alignment=1, Width=53, Height=15,;
    Caption="Data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_56 as StdString with uid="BAZGPTYZUF",Visible=.t., Left=346, Top=12,;
    Alignment=1, Width=106, Height=15,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_57 as StdString with uid="WIZIJBXNJP",Visible=.t., Left=515, Top=12,;
    Alignment=1, Width=59, Height=15,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_58 as StdString with uid="WUWRRSDRCI",Visible=.t., Left=515, Top=45,;
    Alignment=1, Width=59, Height=15,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_59 as StdString with uid="TUTZNXAQDI",Visible=.t., Left=199, Top=292,;
    Alignment=1, Width=58, Height=15,;
    Caption="Numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_60 as StdString with uid="GUOUFXCBXT",Visible=.t., Left=347, Top=292,;
    Alignment=1, Width=65, Height=15,;
    Caption="Data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_61 as StdString with uid="IXWHEGMWCL",Visible=.t., Left=17, Top=292,;
    Alignment=1, Width=122, Height=15,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oBox_1_32 as StdBox with uid="AQPXYHBRRE",left=11, top=283, width=490,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsek_ain','INVENTAR','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".INNUMINV=INVENTAR.INNUMINV";
  +" and "+i_cAliasName2+".INCODESE=INVENTAR.INCODESE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
