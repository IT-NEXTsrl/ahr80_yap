* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscv_b49                                                        *
*              Aggiornamento causali di magazzino                              *
*                                                                              *
*      Author: Zucchetti Tam Spa                                               *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_12]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-08-29                                                      *
* Last revis.: 2001-08-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscv_b49",oParentObject,m.w_NHF)
return(i_retval)

define class tgscv_b49 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_FLELGM = space(1)
  * --- WorkFile variables
  CAM_AGAZ_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- FIle di LOG
    * --- Try
    local bErr_037C4750
    bErr_037C4750=bTrsErr
    this.Try_037C4750()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE - impossibile aggiornare CMAGGVAL su tabella CAM_AGAZ")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_037C4750
    * --- End
  endproc
  proc Try_037C4750()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Aggiornamento  CMAGGVAL
    * --- Select from CAM_AGAZ
    i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select CMCODICE, CMFLELGM  from "+i_cTable+" CAM_AGAZ ";
           ,"_Curs_CAM_AGAZ")
    else
      select CMCODICE, CMFLELGM from (i_cTable);
        into cursor _Curs_CAM_AGAZ
    endif
    if used('_Curs_CAM_AGAZ')
      select _Curs_CAM_AGAZ
      locate for 1=1
      do while not(eof())
      this.w_FLELGM = NVL(_Curs_CAM_AGAZ.CMFLELGM, " ")
      * --- Write into CAM_AGAZ
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.CAM_AGAZ_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"CMAGGVAL ="+cp_NullLink(cp_ToStrODBC(this.w_FLELGM),'CAM_AGAZ','CMAGGVAL');
            +i_ccchkf ;
        +" where ";
            +"CMCODICE = "+cp_ToStrODBC(_Curs_CAM_AGAZ.CMCODICE);
               )
      else
        update (i_cTable) set;
            CMAGGVAL = this.w_FLELGM;
            &i_ccchkf. ;
         where;
            CMCODICE = _Curs_CAM_AGAZ.CMCODICE;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
        select _Curs_CAM_AGAZ
        continue
      enddo
      use
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Aggiornamento campo CMAGGVAL su tabella CAM_AGAZ eseguito con successo")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = this.oParentObject.w_PMSG + this.w_TMPC 
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='CAM_AGAZ'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_CAM_AGAZ')
      use in _Curs_CAM_AGAZ
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
