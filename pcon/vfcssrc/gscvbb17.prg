* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvbb17                                                        *
*              Aggiornamento fatture differite                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_51]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-03-31                                                      *
* Last revis.: 2006-11-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvbb17",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvbb17 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_MESS = space(100)
  w_NUMAGG = 0
  w_CLADOC = space(2)
  w_DATMOD = ctod("  /  /  ")
  w_OK = .f.
  * --- WorkFile variables
  AHEPATCH_idx=0
  DOC_DETT_idx=0
  DOC_MAST_idx=0
  RAG_FATT_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Riparte gli sconti di piede se il documento non � stato
    *     oggetto della ripartizione degli sconti.
    * --- FIle di LOG
    * --- Try
    local bErr_04A97E90
    bErr_04A97E90=bTrsErr
    this.Try_04A97E90()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_MsgFormat("Errore generico - impossibile aggiornare documenti%0%1",this.oParentObject.w_PMSG )
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_04A97E90
    * --- End
  endproc
  proc Try_04A97E90()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Calcolo la data di installazione della Release 4.0
    * --- Read from AHEPATCH
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AHEPATCH_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AHEPATCH_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PTDATINS"+;
        " from "+i_cTable+" AHEPATCH where ";
            +"PTRELEAS  = "+cp_ToStrODBC("4.0");
            +" and PTNUMPAT = "+cp_ToStrODBC(0);
            +" and PTDESCRI = "+cp_ToStrODBC("Rel. 4.0");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PTDATINS;
        from (i_cTable) where;
            PTRELEAS  = "4.0";
            and PTNUMPAT = 0;
            and PTDESCRI = "Rel. 4.0";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DATMOD = NVL(cp_ToDate(_read_.PTDATINS),cp_NullValue(_read_.PTDATINS))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    VQ_EXEC("GSCVBB17.VQR", this , "__tmp__" )
    this.w_NUMAGG = Reccount()
    this.w_OK = .T.
    if used("__tmp__") And this.w_NUMAGG >0
      * --- begin transaction
      cp_BeginTrs()
      CP_CHPRN("..\PCON\EXE\QUERY\GSCVBB17.FRX")
      this.w_OK = .F.
      this.w_OK = ah_YesNo("Si desidera aggiornare il valore fiscale delle righe dei DDT?%0Rispondendo no la procedura di conversione potr� essere rilanciata")
      if this.w_OK
        * --- Write into DOC_DETT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.DOC_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
        if i_nConn<>0
          local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
          declare i_aIndex[1]
          i_cQueryTable=cp_getTempTableName(i_nConn)
          i_aIndex(1)="MVSERIAL,CPROWNUM,MVNUMRIF"
          do vq_exec with 'gscvbb17',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)	
          i_cDB=cp_GetDatabaseType(i_nConn)
          do case
          case i_cDB="SQLServer"
            i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                  +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
                  +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"MVIMPNAZ = _t2.AGGVAL";
              +i_ccchkf;
              +" from "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 where "+i_cWhere)
          case i_cDB="MySQL"
            i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                  +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
                  +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 set ";
              +"DOC_DETT.MVIMPNAZ = _t2.AGGVAL";
              +Iif(Empty(i_ccchkf),"",",DOC_DETT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
              +" where "+i_cWhere)
          case i_cDB="Oracle"
            i_cWhere="DOC_DETT.MVSERIAL = t2.MVSERIAL";
                  +" and "+"DOC_DETT.CPROWNUM = t2.CPROWNUM";
                  +" and "+"DOC_DETT.MVNUMRIF = t2.MVNUMRIF";
            
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set (";
              +"MVIMPNAZ";
              +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
              +"t2.AGGVAL";
              +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
              +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
          case i_cDB="PostgreSQL"
            i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                  +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
                  +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set ";
              +"MVIMPNAZ = _t2.AGGVAL";
              +i_ccchkf;
              +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
          otherwise
            i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
                  +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
                  +" and "+i_cTable+".MVNUMRIF = "+i_cQueryTable+".MVNUMRIF";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"MVIMPNAZ = (select AGGVAL from "+i_cQueryTable+" where "+i_cWhere+")";
              +i_ccchkf;
              +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          endcase
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Delete from RAG_FATT
        i_nConn=i_TableProp[this.RAG_FATT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.RAG_FATT_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          local i_cQueryTable,i_cWhere
          i_cQueryTable=cp_getTempTableName(i_nConn)
          i_cWhere=i_cTable+".FRSERFAT = "+i_cQueryTable+".FRSERFAT";
                +" and "+i_cTable+".FRROWFAT = "+i_cQueryTable+".FRROWFAT";
                +" and "+i_cTable+".FRNUMFAT = "+i_cQueryTable+".FRNUMFAT";
                +" and "+i_cTable+".FRSERDDT = "+i_cQueryTable+".FRSERDDT";
                +" and "+i_cTable+".FRROWDDT = "+i_cQueryTable+".FRROWDDT";
                +" and "+i_cTable+".FRNUMDDT = "+i_cQueryTable+".FRNUMDDT";
        
          do vq_exec with 'gscvbb17',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
                +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
      endif
      * --- commit
      cp_EndTrs(.t.)
    endif
    if Used("__tmp__")
       
 Use in __Tmp__
    endif
    if this.w_Ok
      if this.w_NHF>=0
        this.w_TMPC = ah_MsgFormat("Aggiornamento dettaglio documenti%0Variate %1 righe%0",ALLTRIM(STR(this.w_NUMAGG)))
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    else
      if this.w_NHF>=0
        this.w_TMPC = ah_MsgFormat("Nessun documento da aggiornare")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = this.w_OK
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='AHEPATCH'
    this.cWorkTables[2]='DOC_DETT'
    this.cWorkTables[3]='DOC_MAST'
    this.cWorkTables[4]='RAG_FATT'
    return(this.OpenAllTables(4))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
