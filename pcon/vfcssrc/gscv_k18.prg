* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscv_k18                                                        *
*              Tipologia/classe principali                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-01-24                                                      *
* Last revis.: 2009-07-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscv_k18",oParentObject))

* --- Class definition
define class tgscv_k18 as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 483
  Height = 221
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-07-21"
  HelpContextID=132724073
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=7

  * --- Constant Properties
  _IDX = 0
  TIP_ALLE_IDX = 0
  CLA_ALLE_IDX = 0
  cPrg = "gscv_k18"
  cComment = "Tipologia/classe principali"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_TIPPRIN = space(5)
  o_TIPPRIN = space(5)
  w_DESTIP = space(40)
  w_CLAPRIN = space(5)
  w_DESCLA = space(40)
  w_CONFOPE = space(10)
  w_UNIVOC = space(1)
  w_PUBWEB = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscv_k18Pag1","gscv_k18",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTIPPRIN_1_4
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='TIP_ALLE'
    this.cWorkTables[2]='CLA_ALLE'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIPPRIN=space(5)
      .w_DESTIP=space(40)
      .w_CLAPRIN=space(5)
      .w_DESCLA=space(40)
      .w_CONFOPE=space(10)
      .w_UNIVOC=space(1)
      .w_PUBWEB=space(1)
      .w_TIPPRIN=oParentObject.w_TIPPRIN
      .w_CLAPRIN=oParentObject.w_CLAPRIN
      .w_CONFOPE=oParentObject.w_CONFOPE
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_TIPPRIN))
          .link_1_4('Full')
        endif
        .DoRTCalc(2,3,.f.)
        if not(empty(.w_CLAPRIN))
          .link_1_6('Full')
        endif
          .DoRTCalc(4,4,.f.)
        .w_CONFOPE = 'S'
      .oPgFrm.Page1.oPag.oObj_1_13.Calculate(AH_MSGFORMAT ("N.B.: la classe indicata verr� utilizzata per gli allegati principali solo se "))
      .oPgFrm.Page1.oPag.oObj_1_14.Calculate(AH_MSGFORMAT ("per la loro estensione (archivio 'Estensioni Allegati') � stata specificata la stessa %0tipologia indicata nel campo sottostante."))
    endwith
    this.DoRTCalc(6,7,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_TIPPRIN=.w_TIPPRIN
      .oParentObject.w_CLAPRIN=.w_CLAPRIN
      .oParentObject.w_CONFOPE=.w_CONFOPE
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
        if .o_TIPPRIN<>.w_TIPPRIN
            .w_CLAPRIN = iif(! empty(.w_TIPPRIN),.w_CLAPRIN,'')
          .link_1_6('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate(AH_MSGFORMAT ("N.B.: la classe indicata verr� utilizzata per gli allegati principali solo se "))
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate(AH_MSGFORMAT ("per la loro estensione (archivio 'Estensioni Allegati') � stata specificata la stessa %0tipologia indicata nel campo sottostante."))
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(4,7,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate(AH_MSGFORMAT ("N.B.: la classe indicata verr� utilizzata per gli allegati principali solo se "))
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate(AH_MSGFORMAT ("per la loro estensione (archivio 'Estensioni Allegati') � stata specificata la stessa %0tipologia indicata nel campo sottostante."))
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_13.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_14.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=TIPPRIN
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_ALLE_IDX,3]
    i_lTable = "TIP_ALLE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_ALLE_IDX,2], .t., this.TIP_ALLE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_ALLE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPPRIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'TIP_ALLE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TACODICE like "+cp_ToStrODBC(trim(this.w_TIPPRIN)+"%");

          i_ret=cp_SQL(i_nConn,"select TACODICE,TADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TACODICE',trim(this.w_TIPPRIN))
          select TACODICE,TADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TIPPRIN)==trim(_Link_.TACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TIPPRIN) and !this.bDontReportError
            deferred_cp_zoom('TIP_ALLE','*','TACODICE',cp_AbsName(oSource.parent,'oTIPPRIN_1_4'),i_cWhere,'',"TIPOLOGIE ALLEGATI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TACODICE,TADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TACODICE',oSource.xKey(1))
            select TACODICE,TADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPPRIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TACODICE,TADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TACODICE="+cp_ToStrODBC(this.w_TIPPRIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TACODICE',this.w_TIPPRIN)
            select TACODICE,TADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPPRIN = NVL(_Link_.TACODICE,space(5))
      this.w_DESTIP = NVL(_Link_.TADESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_TIPPRIN = space(5)
      endif
      this.w_DESTIP = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_ALLE_IDX,2])+'\'+cp_ToStr(_Link_.TACODICE,1)
      cp_ShowWarn(i_cKey,this.TIP_ALLE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPPRIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CLAPRIN
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLA_ALLE_IDX,3]
    i_lTable = "CLA_ALLE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLA_ALLE_IDX,2], .t., this.CLA_ALLE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLA_ALLE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CLAPRIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CLA_ALLE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TACODCLA like "+cp_ToStrODBC(trim(this.w_CLAPRIN)+"%");
                   +" and TACODICE="+cp_ToStrODBC(this.w_TIPPRIN);

          i_ret=cp_SQL(i_nConn,"select TACODICE,TACODCLA,TACLADES,TAFLUNIC,TDPUBWEB";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TACODICE,TACODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TACODICE',this.w_TIPPRIN;
                     ,'TACODCLA',trim(this.w_CLAPRIN))
          select TACODICE,TACODCLA,TACLADES,TAFLUNIC,TDPUBWEB;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TACODICE,TACODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CLAPRIN)==trim(_Link_.TACODCLA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CLAPRIN) and !this.bDontReportError
            deferred_cp_zoom('CLA_ALLE','*','TACODICE,TACODCLA',cp_AbsName(oSource.parent,'oCLAPRIN_1_6'),i_cWhere,'',"CLASSI ALLEGATI",'GSCV_K18.CLA_ALLE_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPPRIN<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TACODICE,TACODCLA,TACLADES,TAFLUNIC,TDPUBWEB";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select TACODICE,TACODCLA,TACLADES,TAFLUNIC,TDPUBWEB;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Classe allegato non univoca oppure non pubblicabile su web")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TACODICE,TACODCLA,TACLADES,TAFLUNIC,TDPUBWEB";
                     +" from "+i_cTable+" "+i_lTable+" where TACODCLA="+cp_ToStrODBC(oSource.xKey(2));
                     +" and TACODICE="+cp_ToStrODBC(this.w_TIPPRIN);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TACODICE',oSource.xKey(1);
                       ,'TACODCLA',oSource.xKey(2))
            select TACODICE,TACODCLA,TACLADES,TAFLUNIC,TDPUBWEB;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CLAPRIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TACODICE,TACODCLA,TACLADES,TAFLUNIC,TDPUBWEB";
                   +" from "+i_cTable+" "+i_lTable+" where TACODCLA="+cp_ToStrODBC(this.w_CLAPRIN);
                   +" and TACODICE="+cp_ToStrODBC(this.w_TIPPRIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TACODICE',this.w_TIPPRIN;
                       ,'TACODCLA',this.w_CLAPRIN)
            select TACODICE,TACODCLA,TACLADES,TAFLUNIC,TDPUBWEB;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CLAPRIN = NVL(_Link_.TACODCLA,space(5))
      this.w_DESCLA = NVL(_Link_.TACLADES,space(40))
      this.w_UNIVOC = NVL(_Link_.TAFLUNIC,space(1))
      this.w_PUBWEB = NVL(_Link_.TDPUBWEB,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CLAPRIN = space(5)
      endif
      this.w_DESCLA = space(40)
      this.w_UNIVOC = space(1)
      this.w_PUBWEB = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=empty(.w_CLAPRIN) or (.w_UNIVOC='S' and .w_PUBWEB='S')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Classe allegato non univoca oppure non pubblicabile su web")
        endif
        this.w_CLAPRIN = space(5)
        this.w_DESCLA = space(40)
        this.w_UNIVOC = space(1)
        this.w_PUBWEB = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLA_ALLE_IDX,2])+'\'+cp_ToStr(_Link_.TACODICE,1)+'\'+cp_ToStr(_Link_.TACODCLA,1)
      cp_ShowWarn(i_cKey,this.CLA_ALLE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CLAPRIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTIPPRIN_1_4.value==this.w_TIPPRIN)
      this.oPgFrm.Page1.oPag.oTIPPRIN_1_4.value=this.w_TIPPRIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESTIP_1_5.value==this.w_DESTIP)
      this.oPgFrm.Page1.oPag.oDESTIP_1_5.value=this.w_DESTIP
    endif
    if not(this.oPgFrm.Page1.oPag.oCLAPRIN_1_6.value==this.w_CLAPRIN)
      this.oPgFrm.Page1.oPag.oCLAPRIN_1_6.value=this.w_CLAPRIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCLA_1_7.value==this.w_DESCLA)
      this.oPgFrm.Page1.oPag.oDESCLA_1_7.value=this.w_DESCLA
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(empty(.w_CLAPRIN) or (.w_UNIVOC='S' and .w_PUBWEB='S'))  and not(empty(.w_CLAPRIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCLAPRIN_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Classe allegato non univoca oppure non pubblicabile su web")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TIPPRIN = this.w_TIPPRIN
    return

enddefine

* --- Define pages as container
define class tgscv_k18Pag1 as StdContainer
  Width  = 479
  height = 221
  stdWidth  = 479
  stdheight = 221
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oTIPPRIN_1_4 as StdField with uid="WKEQTZHMBC",rtseq=1,rtrep=.f.,;
    cFormVar = "w_TIPPRIN", cQueryName = "TIPPRIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Tipologia allegato principale",;
    HelpContextID = 158590666,;
   bGlobalFont=.t.,;
    Height=21, Width=60, Left=116, Top=110, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_ALLE", oKey_1_1="TACODICE", oKey_1_2="this.w_TIPPRIN"

  func oTIPPRIN_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
      if .not. empty(.w_CLAPRIN)
        bRes2=.link_1_6('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oTIPPRIN_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTIPPRIN_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_ALLE','*','TACODICE',cp_AbsName(this.parent,'oTIPPRIN_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'',"TIPOLOGIE ALLEGATI",'',this.parent.oContained
  endproc

  add object oDESTIP_1_5 as StdField with uid="JEKVFKGBJV",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DESTIP", cQueryName = "DESTIP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 218121270,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=178, Top=110, InputMask=replicate('X',40)

  add object oCLAPRIN_1_6 as StdField with uid="XJCKHVMAJM",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CLAPRIN", cQueryName = "CLAPRIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Classe allegato non univoca oppure non pubblicabile su web",;
    ToolTipText = "Classe allegato principale",;
    HelpContextID = 158651610,;
   bGlobalFont=.t.,;
    Height=21, Width=60, Left=116, Top=140, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CLA_ALLE", oKey_1_1="TACODICE", oKey_1_2="this.w_TIPPRIN", oKey_2_1="TACODCLA", oKey_2_2="this.w_CLAPRIN"

  func oCLAPRIN_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oCLAPRIN_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCLAPRIN_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CLA_ALLE_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TACODICE="+cp_ToStrODBC(this.Parent.oContained.w_TIPPRIN)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TACODICE="+cp_ToStr(this.Parent.oContained.w_TIPPRIN)
    endif
    do cp_zoom with 'CLA_ALLE','*','TACODICE,TACODCLA',cp_AbsName(this.parent,'oCLAPRIN_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'',"CLASSI ALLEGATI",'GSCV_K18.CLA_ALLE_VZM',this.parent.oContained
  endproc

  add object oDESCLA_1_7 as StdField with uid="WUGSDMILSY",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DESCLA", cQueryName = "DESCLA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 31505354,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=178, Top=140, InputMask=replicate('X',40)


  add object oBtn_1_8 as StdButton with uid="ATTFGKAQEJ",left=370, top=170, width=48,height=45,;
    CpPicture="bmp\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare la selezione";
    , HelpContextID = 132723978;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_8.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_8.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not empty(.w_TIPPRIN) And Not Empty(.w_CLAPRIN))
      endwith
    endif
  endfunc


  add object oBtn_1_9 as StdButton with uid="KYQESPJULE",left=421, top=170, width=48,height=45,;
    CpPicture="bmp\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare la procedura di elaborazione allegati";
    , HelpContextID = 132723978;
    , Caption='\<Annulla';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_13 as cp_calclbl with uid="TQPHAKIJBH",left=12, top=31, width=451,height=25,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",;
    nPag=1;
    , HelpContextID = 45273574


  add object oObj_1_14 as cp_calclbl with uid="QQXIVGUERE",left=12, top=46, width=451,height=39,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",;
    nPag=1;
    , HelpContextID = 45273574

  add object oStr_1_1 as StdString with uid="CKDFXDYKAC",Visible=.t., Left=12, Top=14,;
    Alignment=0, Width=462, Height=18,;
    Caption="Sono richiesti la tipologia e la classe da utilizzare per gli allegati principali"  ;
  , bGlobalFont=.t.

  add object oStr_1_2 as StdString with uid="QZUYUJWZJK",Visible=.t., Left=2, Top=110,;
    Alignment=1, Width=113, Height=18,;
    Caption="Tipologia allegato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_3 as StdString with uid="ERYCNSXBHW",Visible=.t., Left=2, Top=141,;
    Alignment=1, Width=113, Height=18,;
    Caption="Classe allegato:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscv_k18','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
