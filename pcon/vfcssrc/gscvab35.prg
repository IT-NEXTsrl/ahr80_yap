* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvab35                                                        *
*              Aggiornamento combo addebito interessi                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_70]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-12-02                                                      *
* Last revis.: 2002-12-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvab35",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvab35 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_NUMREC = 0
  * --- WorkFile variables
  CON_TENZ_idx=0
  GES_PIAN_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questa procedura di conversione Aggiorna Combo Addebito Interessi
    *     Nella Manutenzione Contenzioso e nel Piano Contenziosi
    this.w_NUMREC = 0
    if g_SOLL="S"
      * --- Try
      local bErr_0360D370
      bErr_0360D370=bTrsErr
      this.Try_0360D370()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        * --- Gestisce log errori
        this.oParentObject.w_PMSG = Message()
        this.oParentObject.w_PESEOK = .F.
        if this.w_NHF>=0
          this.w_TMPC = ah_Msgformat("ERRORE - impossibile aggiornare combo addebito interessi" )
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
      bTrsErr=bTrsErr or bErr_0360D370
      * --- End
    endif
  endproc
  proc Try_0360D370()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- commit
    cp_EndTrs(.t.)
    * --- Write into CON_TENZ
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.CON_TENZ_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CON_TENZ_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.CON_TENZ_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"COADDINT ="+cp_NullLink(cp_ToStrODBC("N"),'CON_TENZ','COADDINT');
          +i_ccchkf ;
      +" where ";
          +"COADDINT = "+cp_ToStrODBC(" ");
             )
    else
      update (i_cTable) set;
          COADDINT = "N";
          &i_ccchkf. ;
       where;
          COADDINT = " ";

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    this.w_NUMREC = i_rows
    * --- Write into GES_PIAN
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.GES_PIAN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.GES_PIAN_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.GES_PIAN_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"GPADDINT ="+cp_NullLink(cp_ToStrODBC("N"),'GES_PIAN','GPADDINT');
          +i_ccchkf ;
      +" where ";
          +"GPADDINT = "+cp_ToStrODBC(" ");
             )
    else
      update (i_cTable) set;
          GPADDINT = "N";
          &i_ccchkf. ;
       where;
          GPADDINT = " ";

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    this.w_NUMREC = this.w_NUMREC + i_rows
    * --- Esecuzione ok
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Conversione eseguita correttamente su %1 righe%0" , Alltrim(Str( this.w_NUMREC )) )
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = this.oParentObject.w_PMSG + this.w_TMPC 
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='CON_TENZ'
    this.cWorkTables[2]='GES_PIAN'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
