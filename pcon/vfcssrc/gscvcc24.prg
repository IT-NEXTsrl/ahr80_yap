* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcc24                                                        *
*              Modifica struttura tabella log generatore documentale           *
*                                                                              *
*      Author: ZUCCHETTI S.p.A.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-12-01                                                      *
* Last revis.: 2012-10-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcc24",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcc24 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  * --- WorkFile variables
  RIPATMP1_idx=0
  PIA_SPED_idx=0
  DET_PIAS_idx=0
  RIPATMP2_idx=0
  runtime_filters = 2

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Procedura di conversione da eseguire solo se database DB2
    *     Aggiornato campo ANCOFISC
    * --- FIle di LOG
    * --- Try
    local bErr_035D0F00
    bErr_035D0F00=bTrsErr
    this.Try_035D0F00()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = this.oParentObject.w_PMSG
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_035D0F00
    * --- End
  endproc
  proc Try_035D0F00()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
     
 i_nConn=i_TableProp[this.PIA_SPED_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.PIA_SPED_idx,2])
     
 i_nConn2=i_TableProp[this.DET_PIAS_idx,3] 
 i_cTable2=cp_SetAzi(i_TableProp[this.DET_PIAS_idx,2])
    if i_nConn<>0
      * --- Create temporary table RIPATMP1
      i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      i_nConn=i_TableProp[this.PIA_SPED_idx,3] && recupera la connessione
      i_cTable=cp_SetAzi(i_TableProp[this.PIA_SPED_idx,2])
      cp_CreateTempTable(i_nConn,i_cTempTable,"* "," from "+i_cTable;
            )
      this.RIPATMP1_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      * --- Create temporary table RIPATMP2
      i_nIdx=cp_AddTableDef('RIPATMP2') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      i_nConn=i_TableProp[this.DET_PIAS_idx,3] && recupera la connessione
      i_cTable=cp_SetAzi(i_TableProp[this.DET_PIAS_idx,2])
      cp_CreateTempTable(i_nConn,i_cTempTable,"* "," from "+i_cTable;
            )
      this.RIPATMP2_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      ah_Msg("Creata tabella temporanea di appoggio",.T.)
      * --- Elimino la tabella
      if Not GSCV_BDT( this, "DET_PIAS" , i_nConn2 )
        * --- Raise
        i_Error="Error"
        return
      endif
      * --- Elimino la tabella
      if Not GSCV_BDT( this, "PIA_SPED" , i_nConn )
        * --- Raise
        i_Error="Error"
        return
      endif
      * --- Ricostruisco il database per la tabella
      GSCV_BRT(this, "PIA_SPED" , i_nConn , .T.)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Ricostruisco il database per la tabella
      GSCV_BRT(this, "DET_PIAS" , i_nConn2 , .T.)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Insert into PIA_SPED
      i_nConn=i_TableProp[this.PIA_SPED_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PIA_SPED_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
        i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.PIA_SPED_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Insert into DET_PIAS
      i_nConn=i_TableProp[this.DET_PIAS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DET_PIAS_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP2_idx,2])
        i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.DET_PIAS_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      ah_Msg("Ripristinati valori di origine",.T.)
      GSCV_BRI(this, "PIA_SPED" , i_nConn )
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      GSCV_BRI(this, "DET_PIAS" , i_nConn2 )
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Elimina tabella TMP
      * --- Drop temporary table RIPATMP1
      i_nIdx=cp_GetTableDefIdx('RIPATMP1')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('RIPATMP1')
      endif
      * --- Drop temporary table RIPATMP2
      i_nIdx=cp_GetTableDefIdx('RIPATMP2')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('RIPATMP2')
      endif
      ah_Msg("Eliminata tabella temporanea tmp",.T.)
      if bTrsErr
        * --- Raise
        i_Error=MESSAGE()
        return
      else
        if this.w_NHF>=0
          this.w_TMPC = Ah_Msgformat("Adeguamento campi tabelle PIA_SPED e DET_PIAS eseguito correttamente%0Occorre ripetere la procedura su ogni azienda")
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.w_TMPC = Ah_Msgformat("Adeguamento campi tabelle PIA_SPED e DET_PIAS eseguito correttamente%0Occorre ripetere la procedura su ogni azienda")
    this.oParentObject.w_PMSG = this.w_TMPC
    return

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='*RIPATMP1'
    this.cWorkTables[2]='PIA_SPED'
    this.cWorkTables[3]='DET_PIAS'
    this.cWorkTables[4]='*RIPATMP2'
    return(this.OpenAllTables(4))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
