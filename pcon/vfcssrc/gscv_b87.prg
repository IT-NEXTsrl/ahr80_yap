* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscv_b87                                                        *
*              Rep. e stampante in assoc. rep./device                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_358]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-10-18                                                      *
* Last revis.: 2001-10-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscv_b87",oParentObject,m.w_NHF)
return(i_retval)

define class tgscv_b87 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  * --- WorkFile variables
  RIPATMP1_idx=0
  CPUSRREP_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- FIle di LOG
    * --- Try
    local bErr_0360B360
    bErr_0360B360=bTrsErr
    this.Try_0360B360()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0 
        this.w_TMPC = this.oParentObject.w_PMSG
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_0360B360
    * --- End
  endproc
  proc Try_0360B360()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    i_nConn=i_TableProp[this.CPUSRREP_idx,3] 
 i_cTable=i_TableProp[this.CPUSRREP_idx,2]
    if i_nConn<>0
      do case
        case upper(CP_DBTYPE)="DB2"
          * --- Caso DB2
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        case upper(CP_DBTYPE)="ORACLE"
          ah_ErrorMsg("Conversione prevista per database IBM DB2",,"")
          * --- commit
          cp_EndTrs(.t.)
        otherwise
          ah_ErrorMsg("Conversione prevista per database IBM DB2",,"")
          * --- commit
          cp_EndTrs(.t.)
      endcase
      if bTrsErr
        * --- Raise
        i_Error=MESSAGE()
        return
      else
        if this.w_NHF>=0 AND upper(CP_DBTYPE)="DB2"
          this.w_TMPC = ah_Msgformat("Adeguamento campi REPASSO e DEVASSO tabella CPUSRREP eseguito correttamente%0")
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.w_TMPC = ah_Msgformat("Adeguamento campi REPASSO e DEVASSO tabella CPUSRREP eseguito correttamente%0")
    this.oParentObject.w_PMSG = this.w_TMPC
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella DB2
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.CPUSRREP_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.CPUSRREP_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
          )
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    ah_Msg("Creata tabella temporanea di appoggio",.T.)
    * --- Elimino la tabella
    if Not GSCV_BDT( this, "CPUSRREP" , i_nConn )
      * --- Raise
      i_Error="Error"
      return
    endif
    * --- Ricostruisco il database per la tabella
    GSCV_BRT(this, "CPUSRREP" , i_nConn , .T.)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into CPUSRREP
    i_nConn=i_TableProp[this.CPUSRREP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CPUSRREP_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.CPUSRREP_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_Msg("Ripristinati valori di origine",.T.)
    * --- Elimina tabella TMP
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    ah_Msg("Eliminata tabella temporanea tmp",.T.)
  endproc


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='*RIPATMP1'
    this.cWorkTables[2]='CPUSRREP'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
