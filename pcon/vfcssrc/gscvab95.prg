* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvab95                                                        *
*              Aggiornamento struttura piano conti                             *
*                                                                              *
*      Author: Giorgio Montali                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_82]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-02-27                                                      *
* Last revis.: 2003-02-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvab95",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvab95 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_MESS = space(100)
  * --- WorkFile variables
  STU_PIAC_idx=0
  STUMPIAC_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questa procedura Aggiorna:
    *     Campo LMPROGRE in STU_PIAC (Struttura Piano Conti Lemco) perch�
    *     chiave deve essere valorizzato
    * --- FIle di LOG
    * --- Try
    local bErr_0360A220
    bErr_0360A220=bTrsErr
    this.Try_0360A220()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = Ah_Msgformat("ERRORE GENERICO - impossibile aggiornare struttura piano conti trasferimento studio")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_0360A220
    * --- End
  endproc
  proc Try_0360A220()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Duplico le testate con progressivo vuote inserendo in LMPROGRE
    *     il valore @@@@@@@@, modifico il dettaglio ed infine elimino le testate
    *     con valori vuoti
    * --- Insert into STUMPIAC
    i_nConn=i_TableProp[this.STUMPIAC_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STUMPIAC_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.STUMPIAC_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"LMCODCON,LMDESCON,LMMINCON,LMMAXCON,LMTIPCON,LMCONCLI,LMCONORD,'@@@@@@@@' as LMPROGRE"," from "+i_cTempTable+" where LMPROGRE Is Null Or LMPROGRE='        '",this.STUMPIAC_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Per sicurezza write anche su campi Null (in realt� essendo chiave non � possibile
    *     che ci� accada)
    * --- Write into STU_PIAC
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.STU_PIAC_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STU_PIAC_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.STU_PIAC_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"LMPROGRE ="+cp_NullLink(cp_ToStrODBC("@@@@@@@@"),'STU_PIAC','LMPROGRE');
          +i_ccchkf ;
      +" where ";
          +"LMPROGRE = "+cp_ToStrODBC("        ");
             )
    else
      update (i_cTable) set;
          LMPROGRE = "@@@@@@@@";
          &i_ccchkf. ;
       where;
          LMPROGRE = "        ";

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Write into STU_PIAC
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.STU_PIAC_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STU_PIAC_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.STU_PIAC_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"LMPROGRE ="+cp_NullLink(cp_ToStrODBC("@@@@@@@@"),'STU_PIAC','LMPROGRE');
          +i_ccchkf ;
      +" where ";
          +"LMPROGRE is null ";
             )
    else
      update (i_cTable) set;
          LMPROGRE = "@@@@@@@@";
          &i_ccchkf. ;
       where;
          LMPROGRE is null;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Delete from STUMPIAC
    i_nConn=i_TableProp[this.STUMPIAC_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STUMPIAC_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"LMPROGRE is null ";
             )
    else
      delete from (i_cTable) where;
            LMPROGRE is null ;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from STUMPIAC
    i_nConn=i_TableProp[this.STUMPIAC_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STUMPIAC_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"LMPROGRE = "+cp_ToStrODBC("        ");
             )
    else
      delete from (i_cTable) where;
            LMPROGRE = "        ";

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = ah_Msgformat("Aggiornamento struttura piano conti trasferimento studio con successo")
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='STU_PIAC'
    this.cWorkTables[2]='STUMPIAC'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
