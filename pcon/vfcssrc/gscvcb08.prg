* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcb08                                                        *
*              Caricamento tabelle fattura CBI                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-06-27                                                      *
* Last revis.: 2009-01-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcb08",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcb08 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_FLVERBOS = .f.
  w_NUMFILTER = 0
  w_IMPERROR = .f.
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_OK = .f.
  w_cTableName = space(0)
  w_FLDFILTER = space(0)
  w_IMPERROR = .f.
  w_w_DBF = space(100)
  w_PATH = space(254)
  * --- WorkFile variables
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_cTableName = ""
    this.w_FLDFILTER = ""
    this.w_IMPERROR = .F.
    if Isahe()
      this.w_PATH = SYS(5)+SYS(2003)+"\..\DOC\DATI_ESTERNI\FAEL\"
    else
      this.w_PATH = iif(g_adhocone,"\",SYS(5)+SYS(2003)+"\") + "FILES_X_IMPORT\FAEL\"
    endif
    * --- Try
    local bErr_04718580
    bErr_04718580=bTrsErr
    this.Try_04718580()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      if Empty(this.oParentObject.w_PMSG)
        this.oParentObject.w_PMSG = Message()
      endif
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = this.oParentObject.w_PMSG
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_04718580
    * --- End
  endproc
  proc Try_04718580()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    ah_Msg("Inizio importazione tabelle",.T.)
    Filedbf = Alltrim(this.w_PATH)+"ENT_MAST.DBF"
    this.w_cTableName = "ENT_MAST"
    this.w_FLDFILTER = "ENCODICE"
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if !this.w_IMPERROR
      Filedbf = Alltrim(this.w_PATH)+"ENT_DETT.DBF"
      this.w_cTableName = "ENT_DETT"
      this.w_FLDFILTER = "ENCODICE,EN_TABLE"
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if !this.w_IMPERROR
      Filedbf = Alltrim(this.w_PATH)+"CAR_SPEC.DBF"
      this.w_cTableName = "CAR_SPEC"
      this.w_FLDFILTER = "CACODICE,CACODORI"
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if !this.w_IMPERROR
      Filedbf = Alltrim(this.w_PATH)+"VASTRUTT.DBF"
      this.w_cTableName = "VASTRUTT"
      this.w_FLDFILTER = "STCODICE"
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if  !this.w_IMPERROR
      Filedbf = Alltrim(this.w_PATH)+"VAPREDEF.DBF"
      this.w_cTableName = "VAPREDEF"
      this.w_FLDFILTER = "PRCODSTR,PRCODTAB,PR_CAMPO"
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if  !this.w_IMPERROR
      Filedbf = Alltrim(this.w_PATH)+"PAT_FILE.DBF"
      this.w_cTableName = "PAT_FILE"
      this.w_FLDFILTER = "PFCODSTR,PFPATTER"
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if !this.w_IMPERROR
      Filedbf = Alltrim(this.w_PATH)+"VAFORMAT.DBF"
      this.w_cTableName = "VAFORMAT"
      this.w_FLDFILTER = "FOCODSTR,FOCODICE"
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if !this.w_IMPERROR
        Filedbf = Alltrim(this.w_PATH)+"VADETTFO.DBF"
        this.w_cTableName = "VADETTFO"
        this.w_FLDFILTER = "DFCODSTR,DFCODFOR,CPROWNUM"
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    if !this.w_IMPERROR
      Filedbf = Alltrim(this.w_PATH)+"VAELEMEN.DBF"
      this.w_cTableName = "VAELEMEN"
      this.w_FLDFILTER = "ELCODSTR,ELCODICE"
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if !this.w_IMPERROR
      Filedbf = Alltrim(this.w_PATH)+"TRS_MAST.DBF"
      this.w_cTableName = "TRS_MAST"
      this.w_FLDFILTER = "TRCODICE"
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if !this.w_IMPERROR
        Filedbf = Alltrim(this.w_PATH)+"TRS_DETT.DBF"
        this.w_cTableName = "TRS_DETT"
        this.w_FLDFILTER = "TRCODICE,CPROWNUM"
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    if  !this.w_IMPERROR
      Filedbf = Alltrim(this.w_PATH)+"VATRASCO.DBF"
      this.w_cTableName = "VATRASCO"
      this.w_FLDFILTER = "TRCODSTR,TRCODENT,TRCODELE,TRCODCON,TRTIPCON,TRCODRAG"
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = ah_Msgformat("Elaborazione terminata con successo")
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if file(Filedbf)
      use (Filedbf) in 0 alias CursDBF
      if USED("CursDBF") 
        if reccount("CursDBF")<>0
          Select "CursDBF"
          go top
          do while !EOF()
            * --- Try
            local bErr_04751D50
            bErr_04751D50=bTrsErr
            this.Try_04751D50()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- accept error
              bTrsErr=.f.
              * --- Divido la stringa dei filtri divisa da virgole
              this.w_NUMFILTER = ALINES(ARRSPLIT, this.w_FLDFILTER, 5, ",")
              if this.w_NUMFILTER>0
                * --- Preparo l'array dei filtri
                PUBLIC ARRAY ARRFILTER(this.w_NUMFILTER ,2)
                do while this.w_NUMFILTER>0
                  ARRFILTER(this.w_NUMFILTER,1)= ARRSPLIT(this.w_NUMFILTER)
                  L_MAC = "IIF(VARTYPE(CursDBF."+ALLTRIM(ARRSPLIT(this.w_NUMFILTER))+")='C',ALLTRIM(CursDBF."+ALLTRIM(ARRSPLIT(this.w_numFILTER))+"),CursDBF."+ALLTRIM(ARRSPLIT(this.w_numFILTER))+")"
                  ARRFILTER(this.w_NUMFILTER,2)= &L_MAC
                  this.w_NUMFILTER = this.w_NUMFILTER - 1
                enddo
                if !EMPTY(writetable(this.w_cTableName, @ARRINS, @ARRFILTER, this, this.w_FLVERBOS))
                  this.w_IMPERROR = .T.
                  * --- transaction error
                  bTrsErr=.t.
                  i_TrsMsg=ah_MsgFormat("Errore aggiornamento tabella %1", this.w_cTableName)
                endif
              else
                this.w_IMPERROR = .T.
                * --- transaction error
                bTrsErr=.t.
                i_TrsMsg=ah_MsgFormat("Filtri aggiornamento tabelle %1 non specificati", this.w_cTableName)
              endif
            endif
            bTrsErr=bTrsErr or bErr_04751D50
            * --- End
            SELECT "CursDBF"
            SKIP
          enddo
        endif
        if USED("CursDBF")
          Select CursDBF 
 Use
        endif
      else
        * --- Raise
        i_Error="Attenzione file DBF non trovato"
        return
      endif
      RELEASE ARRINS
      RELEASE ARRFILTER
      RELEASE ARRSPLIT
      this.w_NUMFILTER = 0
    else
      this.oParentObject.w_PMSG = "Attenzione file " + alltrim(Filedbf) + " non trovato"
      * --- Raise
      i_Error="Attenzione file DBF non trovato"
      return
    endif
  endproc
  proc Try_04751D50()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    PUBLIC ARRAY ARRINS(1,1)
    if curtoarr("CursDBF",0,@ARRINS)<0
      * --- Raise
      i_Error="CreaArrErr"
      return
    endif
    if !EMPTY(inserttable(this.w_cTableName, @ARRINS, iif(this.w_FLVERBOS,this," "), this.w_FLVERBOS))
      * --- Raise
      i_Error="InsErr"
      return
    endif
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
