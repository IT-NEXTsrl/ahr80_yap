* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvbb19                                                        *
*              Ripartisce sconti                                               *
*                                                                              *
*      Author: Nicola Gorlandi                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_42]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-03-31                                                      *
* Last revis.: 2005-03-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvbb19",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvbb19 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_MESS = space(100)
  w_NUMAGG = 0
  w_CLADOC = space(2)
  w_DATMOD = ctod("  /  /  ")
  w_oERRORLOG = .NULL.
  w_SERIAL = space(10)
  w_CODIVA = space(5)
  w_CODIVE = space(5)
  w_VALMAG = 0
  w_DIFF = 0
  w_PERIVE = 0
  w_LINDIVE = 0
  w_TINDIVA = 0
  w_TPERIVA = 0
  w_ROWNUM = 0
  w_NUMRIF = 0
  w_ROWORD = 0
  w_IMPNAZ = 0
  * --- WorkFile variables
  AHEPATCH_idx=0
  DOC_DETT_idx=0
  DOC_MAST_idx=0
  VOCIIVA_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Verifica la quadaratura tra imponibile castelletto IVA e imponibile dettaglio
    *     documenti / corrispetivi da contabilizzare che hanno attivo il check scorporo a piede fattura.
    *     Se necessario aggiorna i campi MVVALMAG e MVIMPNAZ
    * --- FIle di LOG
    * --- Try
    local bErr_037E9390
    bErr_037E9390=bTrsErr
    this.Try_037E9390()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE GENERICO - impossibile aggiornare documenti%0%1", this.oParentObject.w_PMSG)
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_037E9390
    * --- End
  endproc
  proc Try_037E9390()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.w_NUMAGG = 0
    * --- Calcolo la data di installazione della 4.0. Prendo in considerazione i documenti con
    *     data modifica successiva alla data d'installazione della 4.0
    * --- Read from AHEPATCH
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AHEPATCH_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AHEPATCH_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PTDATINS"+;
        " from "+i_cTable+" AHEPATCH where ";
            +"PTRELEAS  = "+cp_ToStrODBC("4.0");
            +" and PTNUMPAT = "+cp_ToStrODBC(00);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PTDATINS;
        from (i_cTable) where;
            PTRELEAS  = "4.0";
            and PTNUMPAT = 00;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DATMOD = NVL(cp_ToDate(_read_.PTDATINS),cp_NullValue(_read_.PTDATINS))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Se installata 4.0
    if i_Rows>0
      * --- Stanzia l'oggetto per la gestione delle messaggistiche di errore
      this.w_oERRORLOG=createobject("AH_ErrorLog")
      * --- begin transaction
      cp_BeginTrs()
      * --- Recupero tutti i documenti che hanno il totale imponibile di riga
      *     diviso per aliquote (escluso omaggi), diverso dal totale imponibile presente
      *     per la corrispondente aliquota sul castelletto IVA.
      * --- Select from GSCVBB19
      do vq_exec with 'GSCVBB19',this,'_Curs_GSCVBB19','',.f.,.t.
      if used('_Curs_GSCVBB19')
        select _Curs_GSCVBB19
        locate for 1=1
        do while not(eof())
        this.w_SERIAL = _Curs_GSCVBB19.MVSERIAL
        this.w_CODIVA = Nvl( _Curs_GSCVBB19.CODIVA , "" )
        this.w_CODIVE = Nvl( _Curs_GSCVBB19.MVCODIVE , "" )
        if not Empty( this.w_CODIVE ) 
          this.w_TINDIVA = 0
          this.w_TPERIVA = 0
          * --- Read from VOCIIVA
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.VOCIIVA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "IVPERIVA,IVPERIND"+;
              " from "+i_cTable+" VOCIIVA where ";
                  +"IVCODIVA = "+cp_ToStrODBC(this.w_CODIVE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              IVPERIVA,IVPERIND;
              from (i_cTable) where;
                  IVCODIVA = this.w_CODIVE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_PERIVE = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
            this.w_LINDIVE = NVL(cp_ToDate(_read_.IVPERIND),cp_NullValue(_read_.IVPERIND))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        else
          this.w_PERIVE = 0
          this.w_LINDIVE = 0
          * --- Read from VOCIIVA
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.VOCIIVA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "IVPERIVA,IVPERIND"+;
              " from "+i_cTable+" VOCIIVA where ";
                  +"IVCODIVA = "+cp_ToStrODBC(this.w_CODIVA);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              IVPERIVA,IVPERIND;
              from (i_cTable) where;
                  IVCODIVA = this.w_CODIVA;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_TPERIVA = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
            this.w_TINDIVA = NVL(cp_ToDate(_read_.IVPERIND),cp_NullValue(_read_.IVPERIND))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        this.w_DIFF = _Curs_GSCVBB19.IMPONI - _Curs_GSCVBB19.VALMAG
        * --- Cerco la prima riga non descrittiva, non omaggio e che abbia il codice IVA selezionato.
        *     Se MVCODIVE � pieno allora non svolgo il filtro sul codice IVA..
        * --- Select from DOC_DETT
        i_nConn=i_TableProp[this.DOC_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select CPROWNUM, MVNUMRIF,MVCODIVA,MVVALMAG,CPROWORD, MVVALMAG  from "+i_cTable+" DOC_DETT ";
              +" where MVSERIAL="+cp_ToStrODBC(this.w_SERIAL)+" And MVTIPRIG<>'D' And MVFLOMAG='X'";
               ,"_Curs_DOC_DETT")
        else
          select CPROWNUM, MVNUMRIF,MVCODIVA,MVVALMAG,CPROWORD, MVVALMAG from (i_cTable);
           where MVSERIAL=this.w_SERIAL And MVTIPRIG<>"D" And MVFLOMAG="X";
            into cursor _Curs_DOC_DETT
        endif
        if used('_Curs_DOC_DETT')
          select _Curs_DOC_DETT
          locate for 1=1
          do while not(eof())
          * --- Se riga con il codice IVA cercato oppure codice IVA impostato in testata
          *     aggiorno la riga ed esco.
          if Not Empty( this.w_CODIVE ) Or _Curs_DOC_DETT.MVCODIVA = this.w_CODIVA
            this.w_ROWNUM = _Curs_DOC_DETT.CPROWNUM
            this.w_NUMRIF = _Curs_DOC_DETT.MVNUMRIF
            this.w_ROWORD = _Curs_DOC_DETT.CPROWORD
            this.w_VALMAG = _Curs_DOC_DETT.MVVALMAG + this.w_DIFF
            this.w_IMPNAZ = CAIMPNAZ(_Curs_GSCVBB19.MVFLVEAC, this.w_VALMAG, _Curs_GSCVBB19.MVCAOVAL, _Curs_GSCVBB19.CAONAZ, _Curs_GSCVBB19.MVDATDOC, _Curs_GSCVBB19.MVVALNAZ, _Curs_GSCVBB19.MVCODVAL, this.w_CODIVE, this.w_PERIVE, this.w_LINDIVE, Nvl(this.w_TPERIVA,0), Nvl(this.w_TINDIVA,0) )
            * --- Non vado ad aggiornare il valore di commessa e l'ultimo acquisto.
            *     La differenza in questi casi � minima!
            * --- Write into DOC_DETT
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.DOC_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"MVVALMAG ="+cp_NullLink(cp_ToStrODBC(this.w_VALMAG),'DOC_DETT','MVVALMAG');
              +",MVIMPNAZ ="+cp_NullLink(cp_ToStrODBC(this.w_IMPNAZ),'DOC_DETT','MVIMPNAZ');
                  +i_ccchkf ;
              +" where ";
                  +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWNUM);
                  +" and MVNUMRIF = "+cp_ToStrODBC(this.w_NUMRIF);
                     )
            else
              update (i_cTable) set;
                  MVVALMAG = this.w_VALMAG;
                  ,MVIMPNAZ = this.w_IMPNAZ;
                  &i_ccchkf. ;
               where;
                  MVSERIAL = this.w_SERIAL;
                  and CPROWNUM = this.w_ROWNUM;
                  and MVNUMRIF = this.w_NUMRIF;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            if _Curs_GSCVBB19.MVFLVEAC="V"
              this.w_oERRORLOG.AddMsgLog("Ciclo vendite causale %1 registrazione num: %2 del: %3", _Curs_GSCVBB19.MVTIPDOC, STR(_Curs_GSCVBB19.MVNUMREG,6,0), DTOC(_Curs_GSCVBB19.MVDATREG) )     
            else
              this.w_oERRORLOG.AddMsgLog("Ciclo acquisti causale %1 registrazione num: %2 del: %3", _Curs_GSCVBB19.MVTIPDOC, STR(_Curs_GSCVBB19.MVNUMREG,6,0), DTOC(_Curs_GSCVBB19.MVDATREG) )     
            endif
            * --- Non vado a rileggere la valuta del documento, mostro gli importi sempre con picture della valuta di conto
            this.w_oERRORLOG.AddMsgLog("Rivalorizzata riga: %1 per un importo pari a %2", Alltrim(Str( this.w_ROWORD )), Alltrim(Tran( this.w_DIFF , v_PV[38+VVL] )))     
            this.w_NUMAGG = this.w_NUMAGG + 1
            Exit
          endif
            select _Curs_DOC_DETT
            continue
          enddo
          use
        endif
          select _Curs_GSCVBB19
          continue
        enddo
        use
      endif
      * --- commit
      cp_EndTrs(.t.)
      this.w_oERRORLOG.PrintLog(this.oParentObject,"Errori riscontrati",.F.,)     
    endif
    this.w_TMPC = Ah_Msgformat("- Aggiornamento dettaglio documenti%0Variate %1 righe", ALLTRIM(STR(this.w_NUMAGG)) )
    if this.w_NHF>=0
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    this.oParentObject.w_PMSG = this.w_TMPC
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='AHEPATCH'
    this.cWorkTables[2]='DOC_DETT'
    this.cWorkTables[3]='DOC_MAST'
    this.cWorkTables[4]='VOCIIVA'
    return(this.OpenAllTables(4))

  proc CloseCursors()
    if used('_Curs_GSCVBB19')
      use in _Curs_GSCVBB19
    endif
    if used('_Curs_DOC_DETT')
      use in _Curs_DOC_DETT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
