* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcc59                                                        *
*              Aggiornamento Path destinazione AQDTFILE                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2015-09-25                                                      *
* Last revis.: 2015-09-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcc59",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcc59 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_AQDTFILE = space(254)
  * --- WorkFile variables
  INF_AQM_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- File di LOG
    * --- Try
    local bErr_038B1838
    bErr_038B1838=bTrsErr
    this.Try_038B1838()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.w_PMSG = Message()
      this.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE - impossibile aggiornare AQDTFILE su tabella INF_AQM")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_038B1838
    * --- End
  endproc
  proc Try_038B1838()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Aggiornamento INF_AQM
    * --- Select from INF_AQM
    i_nConn=i_TableProp[this.INF_AQM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INF_AQM_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select AQDTFILE  from "+i_cTable+" INF_AQM ";
          +" where AQFORTYP='efExcel'";
           ,"_Curs_INF_AQM")
    else
      select AQDTFILE from (i_cTable);
       where AQFORTYP="efExcel";
        into cursor _Curs_INF_AQM
    endif
    if used('_Curs_INF_AQM')
      select _Curs_INF_AQM
      locate for 1=1
      do while not(eof())
      this.w_AQDTFILE = FORCEEXT(_Curs_INF_AQM.AQDTFILE,".XLSX")
      * --- Write into INF_AQM
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.INF_AQM_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.INF_AQM_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.INF_AQM_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"AQDTFILE ="+cp_NullLink(cp_ToStrODBC(this.w_AQDTFILE),'INF_AQM','AQDTFILE');
            +i_ccchkf ;
        +" where ";
            +"AQDTFILE = "+cp_ToStrODBC(_Curs_INF_AQM.AQDTFILE);
               )
      else
        update (i_cTable) set;
            AQDTFILE = this.w_AQDTFILE;
            &i_ccchkf. ;
         where;
            AQDTFILE = _Curs_INF_AQM.AQDTFILE;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
        select _Curs_INF_AQM
        continue
      enddo
      use
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    if this.w_NHF>=0
      this.w_TMPC = ah_Msgformat("Aggiornamento campo AQDTFILE su tabella INF_AQM eseguito con successo")
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    this.oParentObject.w_PESEOK = .T.
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='INF_AQM'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_INF_AQM')
      use in _Curs_INF_AQM
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
