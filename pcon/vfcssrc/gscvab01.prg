* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvab01                                                        *
*              Aggiornamento campo azienda                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_350]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-10-18                                                      *
* Last revis.: 2001-10-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvab01",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvab01 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_CFLD = space(250)
  w_ARCHIVIO = space(50)
  w_LOOP = 0
  w_NUM_FIELDS = 0
  * --- WorkFile variables
  AZIENDA_idx=0
  TMPMOVIMAST_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiornamento AZIENDA campo AZIVACAR per DB2
    * --- Try
    local bErr_03601050
    bErr_03601050=bTrsErr
    this.Try_03601050()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0 
        this.w_TMPC = this.oParentObject.w_PMSG
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_03601050
    * --- End
  endproc
  proc Try_03601050()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    i_nConn=i_TableProp[this.AZIENDA_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
    if i_nConn<>0
      do case
        case upper(CP_DBTYPE)="DB2"
          * --- Caso DB2
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        case upper(CP_DBTYPE)="ORACLE"
          * --- commit
          cp_EndTrs(.t.)
          ah_ErrorMsg("Conversione prevista solo per database IBM DB2",,"")
        otherwise
          * --- commit
          cp_EndTrs(.t.)
          ah_ErrorMsg("Conversione prevista solo per database IBM DB2",,"")
      endcase
      if bTrsErr
        * --- Raise
        i_Error=MESSAGE()
        return
      else
        if this.w_NHF>=0 AND upper(CP_DBTYPE)="DB2"
          this.w_TMPC = ah_Msgformat("Adeguamento campo AZIVACAR tabella AZIENDA eseguito correttamente%0")
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.w_TMPC = ah_Msgformat("Adeguamento campo AZIVACAR tabella AZIENDA eseguito correttamente%0")
    this.oParentObject.w_PMSG = this.w_TMPC
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte tabella DB2
    * --- Carica dizionario dati (per sicurezza)
    =cp_ReadXdc()
    pName="AZIENDA"
    pDatabaseType=i_ServerConn[1,6]
    * --- Costruisco una stringa contenente l'elenco dei campi in analisi...
    do case
      case g_APPLICATION = "ADHOC REVOLUTION" 
        this.w_ARCHIVIO = "AZIENDA"
        this.w_CFLD = ""
        this.w_LOOP = 1
        this.w_NUM_FIELDS = i_dcx.GetFieldsCount(this.w_ARCHIVIO)
        do while this.w_LOOP<=this.w_NUM_FIELDS
          this.w_CFLD = this.w_cFld+iif(empty(this.w_cFld),"",",")+i_dcx.GetFieldName(this.w_ARCHIVIO, this.w_LOOP )
          this.w_LOOP = this.w_LOOP + 1
        enddo
        * --- in coda aggiungo CPCCCHK
        this.w_CFLD = this.w_cFld+",CPCCCHK"
        * --- Occorre creare un temporaneo fittizio per poter instanziare in modo
        *     corretto le strutture dati Painter
        * --- Create temporary table TMPMOVIMAST
        i_nIdx=cp_AddTableDef('TMPMOVIMAST') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        i_nConn=i_TableProp[this.AZIENDA_idx,3] && recupera la connessione
        i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
        cp_CreateTempTable(i_nConn,i_cTempTable,"AZCODAZI "," from "+i_cTable;
              +" where 1=0";
              )
        this.TMPMOVIMAST_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        * --- Drop temporary table TMPMOVIMAST
        i_nIdx=cp_GetTableDefIdx('TMPMOVIMAST')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TMPMOVIMAST')
        endif
        * --- Copiato codice generato da una SELECT INTO, perch� non � possibile
        *     passare a tale istruzioni l'elenco dei campi tramite una variabile (w_CFLD)
         
 i_nIdx=cp_AddTableDef("TMPMOVIMAST") && aggiunge la definizione nella lista delle tabelle 
 i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato 
 i_nConn=i_TableProp[this.AZIENDA_idx,3] && recupera la connessione 
 i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2]) 
 cp_CreateTempTable(i_nConn,i_cTempTable, this.w_CFLD+" " ," from "+i_cTable )
        ah_Msg("Creata tabella temporanea di appoggio",.T.)
      case g_APPLICATION = "ad hoc ENTERPRISE"
        * --- Create temporary table TMPMOVIMAST
        i_nIdx=cp_AddTableDef('TMPMOVIMAST') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        i_nConn=i_TableProp[this.AZIENDA_idx,3] && recupera la connessione
        i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
        cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
              )
        this.TMPMOVIMAST_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        ah_Msg("Creata tabella temporanea di appoggio",.T.)
    endcase
    * --- Elimino la tabella
    if Not GSCV_BDT( this, "AZIENDA" , i_nConn )
      * --- Raise
      i_Error="Error"
      return
    endif
    * --- Ricostruisco il database per la tabella
    GSCV_BRT(this, "AZIENDA" , i_nConn , .T.)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Insert into AZIENDA
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.TMPMOVIMAST_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.AZIENDA_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_Msg("Ripristinati valori di origine",.T.)
    * --- Elimina tabella TMP
    * --- Drop temporary table TMPMOVIMAST
    i_nIdx=cp_GetTableDefIdx('TMPMOVIMAST')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPMOVIMAST')
    endif
    ah_Msg("Eliminata tabella temporanea tmp",.T.)
  endproc


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='*TMPMOVIMAST'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
