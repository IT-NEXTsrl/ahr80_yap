* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvab65                                                        *
*              Conversione campo moindcli tab. zactkey                         *
*                                                                              *
*      Author: Zucchetti S.p.A.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_23]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-03-01                                                      *
* Last revis.: 2005-03-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvab65",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvab65 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_CODREL = space(15)
  * --- WorkFile variables
  ZACTKEY_idx=0
  RIPATMP1_idx=0
  CONVERSI_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Procedura di conversione per aumentare la dimensione del campo 'MOINDCLI' della
    *     tabella ZACTKEY di CPZ. Le operazioni contenuti vengono eseguite solo se il database [
    *     DB2
    * --- Read from CONVERSI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CONVERSI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONVERSI_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "COCODREL"+;
        " from "+i_cTable+" CONVERSI where ";
            +"CONOMPRO = "+cp_ToStrODBC("GSCVAB65");
            +" and COESEGUI = "+cp_ToStrODBC("S");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        COCODREL;
        from (i_cTable) where;
            CONOMPRO = "GSCVAB65";
            and COESEGUI = "S";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CODREL = NVL(cp_ToDate(_read_.COCODREL),cp_NullValue(_read_.COCODREL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Try
    local bErr_0361DB50
    bErr_0361DB50=bTrsErr
    this.Try_0361DB50()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Recupero il Nome fisico della tabella temporanea 
      *     per indicarla nel Log in caso di errore
      this.w_TMPC = cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = ah_Msgformat("%1%0Tabella temporanea contenente i dati: %2", Message(), Alltrim(this.w_TMPC) )
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = this.oParentObject.w_PMSG
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_0361DB50
    * --- End
  endproc
  proc Try_0361DB50()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    if Empty(this.w_CODREL) 
      * --- Converte tabella DB2
      * --- Create temporary table RIPATMP1
      i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      i_nConn=i_TableProp[this.ZACTKEY_idx,3] && recupera la connessione
      i_cTable=cp_SetAzi(i_TableProp[this.ZACTKEY_idx,2])
      cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
            )
      this.RIPATMP1_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      ah_Msg("Creata tabella temporanea di appoggio",.T.)
      * --- Elimino la tabella
       i_nConn=i_TableProp[this.ZACTKEY_idx,3]
      if Not GSCV_BDT( this, "ZACTKEY" , i_nConn)
        * --- Raise
        i_Error="Error"
        return
      endif
      * --- Ricostruisco il database per la tabella
      GSCV_BRT(this, "ZACTKEY" , i_nConn , .t.)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Select from RIPATMP1
      i_nConn=i_TableProp[this.RIPATMP1_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" RIPATMP1 ";
             ,"_Curs_RIPATMP1")
      else
        select * from (i_cTable);
          into cursor _Curs_RIPATMP1
      endif
      if used('_Curs_RIPATMP1')
        select _Curs_RIPATMP1
        locate for 1=1
        do while not(eof())
        * --- Try
        local bErr_0361DAC0
        bErr_0361DAC0=bTrsErr
        this.Try_0361DAC0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_0361DAC0
        * --- End
          select _Curs_RIPATMP1
          continue
        enddo
        use
      endif
      ah_Msg("Ripristinati valori di origine",.T.)
      * --- Elimina tabella TMP
      * --- Drop temporary table RIPATMP1
      i_nIdx=cp_GetTableDefIdx('RIPATMP1')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('RIPATMP1')
      endif
      ah_Msg("Eliminata tabella temporanea tmp",.T.)
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    if Empty(this.w_CODREL)
      this.w_TMPC = ah_Msgformat("Adeguamento campo MOINDCLI tabella ZACTKEY eseguito correttamente%0Occorre ripetere la procedura su ogni azienda")
      this.oParentObject.w_PMSG = this.w_TMPC
    else
      this.oParentObject.w_pMSG = ah_Msgformat("Adeguamento campo MOINDCLI tabella ZACTKEY gi� eseguito nella Rel. 2.2.%0")
    endif
    return
  proc Try_0361DAC0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into ZACTKEY
    i_nConn=i_TableProp[this.ZACTKEY_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ZACTKEY_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ZACTKEY_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"MOCODICE"+",MORAGCLI"+",MOINDCLI"+",MOPIVCLI"+",MORAGINS"+",MOPIVINS"+",MONUMREL"+",MOMAXUTE"+",MOLISMOD"+",MOMATPRO"+",MOTIPDBF"+",MONOMDBF"+",MOACTKE1"+",MOACTKE2"+",MOACTKE3"+",MOACTKE4"+",MOADD001"+",MOADD002"+",MOADD003"+",MOADD004"+",MOADD005"+",MOADD006"+",MOADD007"+",MOADD008"+",MOADD009"+",MOADD010"+",MOADD011"+",MOADD012"+",MOADD013"+",MOADD014"+",MOADD015"+",MOADDATT"+",MOADDPRI"+",MOMANUTE"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MOCODICE),'ZACTKEY','MOCODICE');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MORAGCLI),'ZACTKEY','MORAGCLI');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MOINDCLI),'ZACTKEY','MOINDCLI');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MOPIVCLI),'ZACTKEY','MOPIVCLI');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MORAGINS),'ZACTKEY','MORAGINS');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MOPIVINS),'ZACTKEY','MOPIVINS');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MONUMREL),'ZACTKEY','MONUMREL');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MOMAXUTE),'ZACTKEY','MOMAXUTE');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MOLISMOD),'ZACTKEY','MOLISMOD');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MOMATPRO),'ZACTKEY','MOMATPRO');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MOTIPDBF),'ZACTKEY','MOTIPDBF');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MONOMDBF),'ZACTKEY','MONOMDBF');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MOACTKE1),'ZACTKEY','MOACTKE1');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MOACTKE2),'ZACTKEY','MOACTKE2');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MOACTKE3),'ZACTKEY','MOACTKE3');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MOACTKE4),'ZACTKEY','MOACTKE4');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MOADD001),'ZACTKEY','MOADD001');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MOADD002),'ZACTKEY','MOADD002');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MOADD003),'ZACTKEY','MOADD003');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MOADD004),'ZACTKEY','MOADD004');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MOADD005),'ZACTKEY','MOADD005');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MOADD006),'ZACTKEY','MOADD006');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MOADD007),'ZACTKEY','MOADD007');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MOADD008),'ZACTKEY','MOADD008');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MOADD009),'ZACTKEY','MOADD009');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MOADD010),'ZACTKEY','MOADD010');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MOADD011),'ZACTKEY','MOADD011');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MOADD012),'ZACTKEY','MOADD012');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MOADD013),'ZACTKEY','MOADD013');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MOADD014),'ZACTKEY','MOADD014');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MOADD015),'ZACTKEY','MOADD015');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MOADDATT),'ZACTKEY','MOADDATT');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MOADDPRI),'ZACTKEY','MOADDPRI');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_RIPATMP1.MOMANUTE),'ZACTKEY','MOMANUTE');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'MOCODICE',_Curs_RIPATMP1.MOCODICE,'MORAGCLI',_Curs_RIPATMP1.MORAGCLI,'MOINDCLI',_Curs_RIPATMP1.MOINDCLI,'MOPIVCLI',_Curs_RIPATMP1.MOPIVCLI,'MORAGINS',_Curs_RIPATMP1.MORAGINS,'MOPIVINS',_Curs_RIPATMP1.MOPIVINS,'MONUMREL',_Curs_RIPATMP1.MONUMREL,'MOMAXUTE',_Curs_RIPATMP1.MOMAXUTE,'MOLISMOD',_Curs_RIPATMP1.MOLISMOD,'MOMATPRO',_Curs_RIPATMP1.MOMATPRO,'MOTIPDBF',_Curs_RIPATMP1.MOTIPDBF,'MONOMDBF',_Curs_RIPATMP1.MONOMDBF)
      insert into (i_cTable) (MOCODICE,MORAGCLI,MOINDCLI,MOPIVCLI,MORAGINS,MOPIVINS,MONUMREL,MOMAXUTE,MOLISMOD,MOMATPRO,MOTIPDBF,MONOMDBF,MOACTKE1,MOACTKE2,MOACTKE3,MOACTKE4,MOADD001,MOADD002,MOADD003,MOADD004,MOADD005,MOADD006,MOADD007,MOADD008,MOADD009,MOADD010,MOADD011,MOADD012,MOADD013,MOADD014,MOADD015,MOADDATT,MOADDPRI,MOMANUTE &i_ccchkf. );
         values (;
           _Curs_RIPATMP1.MOCODICE;
           ,_Curs_RIPATMP1.MORAGCLI;
           ,_Curs_RIPATMP1.MOINDCLI;
           ,_Curs_RIPATMP1.MOPIVCLI;
           ,_Curs_RIPATMP1.MORAGINS;
           ,_Curs_RIPATMP1.MOPIVINS;
           ,_Curs_RIPATMP1.MONUMREL;
           ,_Curs_RIPATMP1.MOMAXUTE;
           ,_Curs_RIPATMP1.MOLISMOD;
           ,_Curs_RIPATMP1.MOMATPRO;
           ,_Curs_RIPATMP1.MOTIPDBF;
           ,_Curs_RIPATMP1.MONOMDBF;
           ,_Curs_RIPATMP1.MOACTKE1;
           ,_Curs_RIPATMP1.MOACTKE2;
           ,_Curs_RIPATMP1.MOACTKE3;
           ,_Curs_RIPATMP1.MOACTKE4;
           ,_Curs_RIPATMP1.MOADD001;
           ,_Curs_RIPATMP1.MOADD002;
           ,_Curs_RIPATMP1.MOADD003;
           ,_Curs_RIPATMP1.MOADD004;
           ,_Curs_RIPATMP1.MOADD005;
           ,_Curs_RIPATMP1.MOADD006;
           ,_Curs_RIPATMP1.MOADD007;
           ,_Curs_RIPATMP1.MOADD008;
           ,_Curs_RIPATMP1.MOADD009;
           ,_Curs_RIPATMP1.MOADD010;
           ,_Curs_RIPATMP1.MOADD011;
           ,_Curs_RIPATMP1.MOADD012;
           ,_Curs_RIPATMP1.MOADD013;
           ,_Curs_RIPATMP1.MOADD014;
           ,_Curs_RIPATMP1.MOADD015;
           ,_Curs_RIPATMP1.MOADDATT;
           ,_Curs_RIPATMP1.MOADDPRI;
           ,_Curs_RIPATMP1.MOMANUTE;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='ZACTKEY'
    this.cWorkTables[2]='*RIPATMP1'
    this.cWorkTables[3]='CONVERSI'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_RIPATMP1')
      use in _Curs_RIPATMP1
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
