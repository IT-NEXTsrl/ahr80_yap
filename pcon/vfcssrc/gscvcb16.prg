* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcb16                                                        *
*              Valorizzazione nuovi campi traduzione da vecchie tabelle traduzione*
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-02-23                                                      *
* Last revis.: 2009-02-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcb16",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcb16 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TMPC = space(10)
  w_TEMP = space(10)
  w_I = 0
  w_RESULT = 0
  * --- WorkFile variables
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Valorizzazione nuovi campi traduzione da vecchie tabelle traduzione
    * --- Try
    local bErr_03015418
    bErr_03015418=bTrsErr
    this.Try_03015418()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_msgformat("Errore generico - Impossibile eseguire aggiornamento richiesto")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_03015418
    * --- End
  endproc
  proc Try_03015418()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Aggiorno tabella pagamenti
    this.oParentObject.w_PESEOK = .T.
    if ALEN(i_aCpLangs)=0
      this.w_TMPC = ah_msgformat("Non sono state definite lingue con traduzione del dato")
    else
      FOR this.w_I=1 TO ALEN(i_aCpLangs)
      if not empty(i_aCpLangs[this.w_I])
        this.w_RESULT = GSCVCB15(this,"PAG_AMEN","PADESCRI","PACODICE","TRADPAGA","LGDESCOD","LGCODICE",i_aCpLangs[this.w_I])
        if this.w_RESULT<0
          this.w_TMPC = ah_msgformat("Aggiornamento tabella pagamenti lingua %1 non eseguito", i_aCpLangs[this.w_I])
          this.oParentObject.w_PESEOK = .f.
        else
          this.w_TMPC = ah_msgformat("Aggiornamento tabella pagamenti lingua %1 eseguito", i_aCpLangs[this.w_I])
        endif
        this.oParentObject.w_PMSG = this.w_TMPC
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        this.w_RESULT = GSCVCB15(this,"MOD_PAGA","MPDESCRI","MPCODICE","TRADMODP","LGDESCRI","LGTIPPAG",i_aCpLangs[this.w_I])
        if this.w_RESULT<0
          this.oParentObject.w_PESEOK = .f.
          this.w_TMPC = ah_msgformat("Aggiornamento tabella tipi di pagamento lingua %1 non eseguito", i_aCpLangs[this.w_I])
        else
          this.w_TMPC = ah_msgformat("Aggiornamento tabella tipi di pagamento lingua %1 eseguito", i_aCpLangs[this.w_I])
        endif
        this.oParentObject.w_PMSG = this.w_TMPC
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        this.w_RESULT = GSCVCB15(this,"PORTI","PODESPOR","POCODPOR","TRADPORT","LGDESCRI","LGCODPOR",i_aCpLangs[this.w_I])
        if this.w_RESULT<0
          this.oParentObject.w_PESEOK = .f.
          this.w_TMPC = ah_msgformat("Aggiornamento tabella porti lingua %1 non eseguito", i_aCpLangs[this.w_I])
        else
          this.w_TMPC = ah_msgformat("Aggiornamento tabella porti lingua %1 eseguito", i_aCpLangs[this.w_I])
        endif
        this.oParentObject.w_PMSG = this.w_TMPC
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        this.w_RESULT = GSCVCB15(this,"ASPETTO","ASDESASP","ASCODASP","TRADASPE","LGDESCRI","LGCODASP",i_aCpLangs[this.w_I])
        if this.w_RESULT<0
          this.oParentObject.w_PESEOK = .f.
          this.w_TMPC = ah_msgformat("Aggiornamento tabella aspetto esteriore dei beni lingua %1 non eseguito", i_aCpLangs[this.w_I])
        else
          this.w_TMPC = ah_msgformat("Aggiornamento tabella aspetto esteriore dei beni lingua %1 eseguito", i_aCpLangs[this.w_I])
        endif
        this.oParentObject.w_PMSG = this.w_TMPC
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        this.w_RESULT = GSCVCB15(this,"MODASPED","SPDESSPE","SPCODSPE","TRADSPED","LGDESCRI","LGCODSPE",i_aCpLangs[this.w_I])
        if this.w_RESULT<0
          this.oParentObject.w_PESEOK = .f.
          this.w_TMPC = ah_msgformat("Aggiornamento tabella spedizioni lingua %1 non eseguito", i_aCpLangs[this.w_I])
        else
          this.w_TMPC = ah_msgformat("Aggiornamento tabella spedizioni lingua %1 eseguito", i_aCpLangs[this.w_I])
        endif
        this.oParentObject.w_PMSG = this.w_TMPC
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        this.w_RESULT = GSCVCB15(this,"TIPICONF","TCDESCRI","TCCODICE","TRADCONF","TCDESCOD","TCCODICE",i_aCpLangs[this.w_I])
        if this.w_RESULT<0
          this.oParentObject.w_PESEOK = .f.
          this.w_TMPC = ah_msgformat("Aggiornamento tabella tipi confezioni lingua %1 non eseguito", i_aCpLangs[this.w_I])
        else
          this.w_TMPC = ah_msgformat("Aggiornamento tabella tipi confezioni lingua %1 eseguito", i_aCpLangs[this.w_I])
        endif
        this.oParentObject.w_PMSG = this.w_TMPC
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        this.w_RESULT = GSCVCB15(this,"FAM_ARTI","FADESCRI","FACODICE","TRADFAMI","TFDESCOD","TFCODICE",i_aCpLangs[this.w_I])
        if this.w_RESULT<0
          this.oParentObject.w_PESEOK = .f.
          this.w_TMPC = ah_msgformat("Aggiornamento tabella famiglie articoli lingua %1 non eseguito", i_aCpLangs[this.w_I])
        else
          this.w_TMPC = ah_msgformat("Aggiornamento tabella famiglie articoli lingua %1 eseguito", i_aCpLangs[this.w_I])
        endif
        this.oParentObject.w_PMSG = this.w_TMPC
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        this.w_RESULT = GSCVCB15(this,"GRUMERC","GMDESCRI","GMCODICE","TRADGRUM","TGDESCOD","TGCODICE",i_aCpLangs[this.w_I])
        if this.w_RESULT<0
          this.oParentObject.w_PESEOK = .f.
          this.w_TMPC = ah_msgformat("Aggiornamento tabella gruppi merceologici lingua %1 non eseguito", i_aCpLangs[this.w_I])
        else
          this.w_TMPC = ah_msgformat("Aggiornamento tabella gruppi merceologici lingua %1 eseguito", i_aCpLangs[this.w_I])
        endif
        this.oParentObject.w_PMSG = this.w_TMPC
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        this.w_RESULT = GSCVCB15(this,"CATEGOMO","OMDESCRI","OMCODICE","TRADCOMO","TMDESCOD","TMCODICE",i_aCpLangs[this.w_I])
        if this.w_RESULT<0
          this.oParentObject.w_PESEOK = .f.
          this.w_TMPC = ah_msgformat("Aggiornamento tabella categorie omogenee lingua %1 non eseguito", i_aCpLangs[this.w_I])
        else
          this.w_TMPC = ah_msgformat("Aggiornamento tabella categorie omogenee lingua %1 eseguito", i_aCpLangs[this.w_I])
        endif
        this.oParentObject.w_PMSG = this.w_TMPC
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
      ENDFOR
      if this.oParentObject.w_PESEOK
        this.w_TMPC = ah_msgformat("Procedura di conversione eseguita")
      else
        this.w_TMPC = ah_msgformat("Procedura di conversione eseguita con errori")
      endif
    endif
    this.oParentObject.w_PMSG = this.w_TMPC
    this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
