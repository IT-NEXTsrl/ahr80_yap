* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvbb26                                                        *
*              Popola tabella totalizzatori IVA                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_416]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-10-18                                                      *
* Last revis.: 2006-12-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvbb26",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvbb26 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_DESCRI = space(80)
  * --- WorkFile variables
  TAB_FONT_idx=0
  TAB_MISU_idx=0
  TAB_DIME_idx=0
  DETCOMBO_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiornamento tabelle TAB_DIME,TAB-MISU,TAB_FONT,DETCOMBO
    *     relative ai totalizzatori IVA
    * --- Try
    local bErr_0375C290
    bErr_0375C290=bTrsErr
    this.Try_0375C290()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0 
        this.w_TMPC = this.oParentObject.w_PMSG
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_0375C290
    * --- End
  endproc
  proc Try_0375C290()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Read from TAB_FONT
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.TAB_FONT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TAB_FONT_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "*"+;
        " from "+i_cTable+" TAB_FONT where ";
            +"FOCODICE = "+cp_ToStrODBC("PRIMANVQR");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        *;
        from (i_cTable) where;
            FOCODICE = "PRIMANVQR";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if i_rows=0
      Dimension FONTI(8,4) 
 FONTI[ 1,1]="ALTRIQUAD " 
 FONTI[ 1,2]="query\GSCG5BST" 
 FONTI[ 1,3]=ah_Msgformat("Altri quadri%1", space(28) ) 
 FONTI[ 1,4]="Q" 
 FONTI[ 2,1]="PERIODICA " 
 FONTI[ 2,2]="query\GSCG2BST" 
 FONTI[ 2,3]=ah_Msgformat("Manutenzione IVA periodica%1", space(14)) 
 FONTI[ 2,4]="Q" 
 FONTI[ 3,1]="PLAFOND   " 
 FONTI[ 3,2]="query\GSCG4BST" 
 FONTI[ 3,3]=ah_Msgformat("PLAFOND annuale%1", space(25)) 
 FONTI[ 3,4]="Q" 
 FONTI[ 4,1]="PRIMANOTA " 
 FONTI[ 4,2]="PNT_IVA" 
 FONTI[ 4,3]=ah_Msgformat("PRIMANOTA tabella%1", space (23)) 
 FONTI[ 4,4]="T" 
 FONTI[ 5,1]="PRIMANVQR " 
 FONTI[ 5,2]="query\GSCG_BST" 
 FONTI[ 5,3]=ah_Msgformat("Registrazioni IVA%1", space(23)) 
 FONTI[ 5,4]="Q" 
 FONTI[ 6,1]="SEPINDOPE " 
 FONTI[ 6,2]="query\GSCG3BST" 
 FONTI[ 6,3]=ah_Msgformat("Separata indic. operaz. effettuate") 
 FONTI[ 6,4]="Q" 
 FONTI[ 7,1]="SALDIIVA  " 
 FONTI[ 7,2]="query\GSCG1BST" 
 FONTI[ 7,3]=ah_Msgformat("Saldi IVA%1" , space(31)) 
 FONTI[ 7,4]="Q" 
 FONTI[ 8,1]="CREDDIVA  " 
 FONTI[ 8,2]="query\GSCG6BST" 
 FONTI[ 8,3]=ah_Msgformat("Credito IVA anno precedente") 
 FONTI[ 8,4]="Q"
      For i=1 to 8
      * --- Insert into TAB_FONT
      i_nConn=i_TableProp[this.TAB_FONT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TAB_FONT_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TAB_FONT_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"FOCODICE"+",FO__NOME"+",FODESCRI"+",FO__TIPO"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(FONTI[I,1]),'TAB_FONT','FOCODICE');
        +","+cp_NullLink(cp_ToStrODBC(FONTI[I,2]),'TAB_FONT','FO__NOME');
        +","+cp_NullLink(cp_ToStrODBC(FONTI[I,3]),'TAB_FONT','FODESCRI');
        +","+cp_NullLink(cp_ToStrODBC(FONTI[I,4]),'TAB_FONT','FO__TIPO');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'FOCODICE',FONTI[I,1],'FO__NOME',FONTI[I,2],'FODESCRI',FONTI[I,3],'FO__TIPO',FONTI[I,4])
        insert into (i_cTable) (FOCODICE,FO__NOME,FODESCRI,FO__TIPO &i_ccchkf. );
           values (;
             FONTI[I,1];
             ,FONTI[I,2];
             ,FONTI[I,3];
             ,FONTI[I,4];
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error='Errore inserimento fonti'
        return
      endif
      endfor
      Dimension DIM(63,9) 
 DIM[ 1,1]="PRIMANVQR " 
 DIM[ 1,2]="ACQINT         " 
 DIM[ 1,3]="VP1/VP2/VP3                                                                     " 
 DIM[ 1,4]="C" 
 DIM[ 1,5]="CC" 
 DIM[ 1,6]="                                                  " 
 DIM[ 1,7]= 0 
 DIM[ 1,8]= cp_CharToDate("  -  -  ") 
 DIM[ 1,9]="               " 
 DIM[ 2,1]="SALDIIVA  " 
 DIM[ 2,2]="ACQINT         " 
 DIM[ 2,3]="VP1/VP2/VP3                                                                     " 
 DIM[ 2,4]="C" 
 DIM[ 2,5]="CC" 
 DIM[ 2,6]="                                                  " 
 DIM[ 2,7]= 0 
 DIM[ 2,8]= cp_CharToDate("  -  -  ") 
 DIM[ 2,9]="               " 
 DIM[ 3,1]="SEPINDOPE " 
 DIM[ 3,2]="ANNDIC         " 
 DIM[ 3,3]=ah_Msgformat("Anno dichiarazione%1", space(62)) 
 DIM[ 3,4]="C" 
 DIM[ 3,5]="AN" 
 DIM[ 3,6]="                                                  " 
 DIM[ 3,7]= 0 
 DIM[ 3,8]= cp_CharToDate("  -  -  ") 
 DIM[ 3,9]="               " 
 DIM[ 4,1]="ALTRIQUAD " 
 DIM[ 4,2]="ANNO           " 
 DIM[ 4,3]=ah_Msgformat("Anno di elaborazione%1", space(60) ) 
 DIM[ 4,4]="C" 
 DIM[ 4,5]="AN" 
 DIM[ 4,6]="                                                  " 
 DIM[ 4,7]= 0 
 DIM[ 4,8]= cp_CharToDate("  -  -  ") 
 DIM[ 4,9]="               " 
 DIM[ 5,1]="PRIMANVQR " 
 DIM[ 5,2]="ANNODOC        " 
 DIM[ 5,3]=ah_Msgformat("Anno documento%1", space(66)) 
 DIM[ 5,4]="N" 
 DIM[ 5,5]="AN" 
 DIM[ 5,6]="                                                  " 
 DIM[ 5,7]= 0 
 DIM[ 5,8]= cp_CharToDate("  -  -  ") 
 DIM[ 5,9]="               " 
 DIM[ 6,1]="PLAFOND   " 
 DIM[ 6,2]="ANNOEL         " 
 DIM[ 6,3]=ah_Msgformat("Anno di elaborazione%1", space(60) ) 
 DIM[ 6,4]="C" 
 DIM[ 6,5]="AN" 
 DIM[ 6,6]="                                                  " 
 DIM[ 6,7]= 0 
 DIM[ 6,8]= cp_CharToDate("  -  -  ") 
 DIM[ 6,9]="               " 
 DIM[ 7,1]="PRIMANVQR " 
 DIM[ 7,2]="BOLIVA         " 
 DIM[ 7,3]=ah_Msgformat("Bollo su importi esenti%1" , space(57)) 
 DIM[ 7,4]="C" 
 DIM[ 7,5]="CC" 
 DIM[ 7,6]="                                                  " 
 DIM[ 7,7]= 0 
 DIM[ 7,8]= cp_CharToDate("  -  -  ") 
 DIM[ 7,9]="               " 
 DIM[ 8,1]="SALDIIVA  " 
 DIM[ 8,2]="BOLIVA         " 
 DIM[ 8,3]=ah_Msgformat("Bollo su importi esenti%1" , space(57)) 
 DIM[ 8,4]="C" 
 DIM[ 8,5]="CC" 
 DIM[ 8,6]="                                                  " 
 DIM[ 8,7]= 0 
 DIM[ 8,8]= cp_CharToDate("  -  -  ") 
 DIM[ 8,9]="               " 
 DIM[ 9,1]="PRIMANVQR " 
 DIM[ 9,2]="CODCAU         " 
 DIM[ 9,3]=ah_Msgformat("Causale contabile%1", space(63)) 
 DIM[ 9,4]="C" 
 DIM[ 9,5]="LK" 
 DIM[ 9,6]="                                                  " 
 DIM[ 9,7]= 0 
 DIM[ 9,8]= cp_CharToDate("  -  -  ") 
 DIM[ 9,9]="CAU_CONT       " 
 DIM[ 10,1]="PRIMANVQR " 
 DIM[ 10,2]="CODINT         " 
 DIM[ 10,3]=ah_Msgformat("Cod.IVA acq.CEE%1", space(65)) 
 DIM[ 10,4]="C" 
 DIM[ 10,5]="LK" 
 DIM[ 10,6]="                                                  " 
 DIM[ 10,7]= 0 
 DIM[ 10,8]= cp_CharToDate("  -  -  ") 
 DIM[ 10,9]="VOCIIVA        " 
 DIM[ 11,1]="SALDIIVA  " 
 DIM[ 11,2]="CODINT         " 
 DIM[ 11,3]=ah_Msgformat("Cod.IVA acq.CEE%1", space(65)) 
 DIM[ 11,4]="C" 
 DIM[ 11,5]="LK" 
 DIM[ 11,6]="                                                  " 
 DIM[ 11,7]= 0 
 DIM[ 11,8]= cp_CharToDate("  -  -  ") 
 DIM[ 11,9]="VOCIIVA        " 
 DIM[ 12,1]="PRIMANVQR " 
 DIM[ 12,2]="CODIVA         " 
 DIM[ 12,3]=ah_Msgformat("Codice IVA%1", space(70)) 
 DIM[ 12,4]="C" 
 DIM[ 12,5]="LK" 
 DIM[ 12,6]="                                                  " 
 DIM[ 12,7]= 0 
 DIM[ 12,8]= cp_CharToDate("  -  -  ") 
 DIM[ 12,9]="VOCIIVA        " 
 DIM[ 13,1]="SALDIIVA  " 
 DIM[ 13,2]="CODIVA         " 
 DIM[ 13,3]=ah_Msgformat("Codice IVA%1", space(70)) 
 DIM[ 13,4]="C" 
 DIM[ 13,5]="LK" 
 DIM[ 13,6]="                                                  " 
 DIM[ 13,7]= 0 
 DIM[ 13,8]= cp_CharToDate("  -  -  ") 
 DIM[ 13,9]="VOCIIVA        " 
 DIM[ 14,1]="PRIMANVQR " 
 DIM[ 14,2]="CONTRO         " 
 DIM[ 14,3]=ah_Msgformat("Conto contropartita studio%1", space(54)) 
 DIM[ 14,4]="C" 
 DIM[ 14,5]="LK" 
 DIM[ 14,6]="                                                  " 
 DIM[ 14,7]= 0 
 DIM[ 14,8]= cp_CharToDate("  -  -  ") 
 DIM[ 14,9]="CONTI" 
 DIM[ 15,1]="PRIMANVQR " 
 DIM[ 15,2]="DATANA         " 
 DIM[ 15,3]=ah_Msgformat("Op. attive%1", space(70)) 
 DIM[ 15,4]="N" 
 DIM[ 15,5]="CC" 
 DIM[ 15,6]="                                                  " 
 DIM[ 15,7]= 0 
 DIM[ 15,8]= cp_CharToDate("  -  -  ") 
 DIM[ 15,9]="               " 
 DIM[ 16,1]="SALDIIVA  " 
 DIM[ 16,2]="DATANA         " 
 DIM[ 16,3]=ah_Msgformat("Op. attive%1", space(70)) 
 DIM[ 16,4]="N" 
 DIM[ 16,5]="CC" 
 DIM[ 16,6]="                                                  " 
 DIM[ 16,7]= 0 
 DIM[ 16,8]= cp_CharToDate("  -  -  ") 
 DIM[ 16,9]="               " 
 DIM[ 17,1]="PRIMANVQR " 
 DIM[ 17,2]="DATANP         " 
 DIM[ 17,3]=ah_Msgformat("Op. passive%1", space(69)) 
 DIM[ 17,4]="N" 
 DIM[ 17,5]="CC" 
 DIM[ 17,6]="                                                  " 
 DIM[ 17,7]= 0 
 DIM[ 17,8]= cp_CharToDate("  -  -  ") 
 DIM[ 17,9]="               " 
 DIM[ 18,1]="SALDIIVA  " 
 DIM[ 18,2]="DATANP         " 
 DIM[ 18,3]=ah_Msgformat("Op. passive%1", space(69)) 
 DIM[ 18,4]="N" 
 DIM[ 18,5]="CC" 
 DIM[ 18,6]="                                                  " 
 DIM[ 18,7]= 0 
 DIM[ 18,8]= cp_CharToDate("  -  -  ") 
 DIM[ 18,9]="               " 
 DIM[ 19,1]="PRIMANVQR " 
 DIM[ 19,2]="COMIVAFI       " 
 DIM[ 19,3]=ah_Msgformat("Competenza IVA%1",space(43)) 
 DIM[ 19,4]="D" 
 DIM[ 19,5]="CF" 
 DIM[ 19,6]="                                                  " 
 DIM[ 19,7]= 0 
 DIM[ 19,8]= cp_CharToDate("  -  -  ") 
 DIM[ 19,9]="               " 
 DIM[ 20,1]="PRIMANVQR " 
 DIM[ 20,2]="COMIVAIN       " 
 DIM[ 20,3]=ah_Msgformat("Competenza IVA%1",space(43)) 
 DIM[ 20,4]="D" 
 DIM[ 20,5]="CI" 
 DIM[ 20,6]="                                                  " 
 DIM[ 20,7]= 0 
 DIM[ 20,8]= cp_CharToDate("  -  -  ") 
 DIM[ 20,9]="               " 
 DIM[ 21,1]="SEPINDOPE " 
 DIM[ 21,2]="FINANNO        " 
 DIM[ 21,3]=ah_Msgformat("Data competenza IVA finale%1" , space(57)) 
 DIM[ 21,4]="D" 
 DIM[ 21,5]="CF" 
 DIM[ 21,6]="                                                  " 
 DIM[ 21,7]= 0 
 DIM[ 21,8]= cp_CharToDate("  -  -  ") 
 DIM[ 21,9]="               " 
 DIM[ 22,1]="PRIMANVQR " 
 DIM[ 22,2]="FLVAFF         " 
 DIM[ 22,3]=ah_Msgformat("Volume di affari%1", space(64)) 
 DIM[ 22,4]="C" 
 DIM[ 22,5]="CC" 
 DIM[ 22,6]="                                                  " 
 DIM[ 22,7]= 0 
 DIM[ 22,8]= cp_CharToDate("  -  -  ") 
 DIM[ 22,9]="               " 
 DIM[ 23,1]="SALDIIVA  " 
 DIM[ 23,2]="FLVAFF         " 
 DIM[ 23,3]=ah_Msgformat("Volume di affari%1", space(64)) 
 DIM[ 23,4]="C" 
 DIM[ 23,5]="CC" 
 DIM[ 23,6]="                                                  " 
 DIM[ 23,7]= 0 
 DIM[ 23,8]= cp_CharToDate("  -  -  ") 
 DIM[ 23,9]="               " 
 DIM[ 24,1]="PRIMANVQR " 
 DIM[ 24,2]="IMPEXP         " 
 DIM[ 24,3]=ah_Msgformat("Esportazioni%1", space(68)) 
 DIM[ 24,4]="C" 
 DIM[ 24,5]="CC" 
 DIM[ 24,6]="                                                  " 
 DIM[ 24,7]= 0 
 DIM[ 24,8]= cp_CharToDate("  -  -  ") 
 DIM[ 24,9]="               " 
 DIM[ 25,1]="SALDIIVA  " 
 DIM[ 25,2]="IMPEXP         " 
 DIM[ 25,3]=ah_Msgformat("Esportazioni%1", space(68)) 
 DIM[ 25,4]="C" 
 DIM[ 25,5]="CC" 
 DIM[ 25,6]="                                                  " 
 DIM[ 25,7]= 0 
 DIM[ 25,8]= cp_CharToDate("  -  -  ") 
 DIM[ 25,9]="               " 
 DIM[ 26,1]="SEPINDOPE " 
 DIM[ 26,2]="INIANNO        " 
 DIM[ 26,3]=ah_Msgformat("Data competenza IVA iniziale%1",space(56)) 
 DIM[ 26,4]="D" 
 DIM[ 26,5]="CI" 
 DIM[ 26,6]="                                                  " 
 DIM[ 26,7]= 0 
 DIM[ 26,8]= cp_CharToDate("  -  -  ") 
 DIM[ 26,9]="               " 
 DIM[ 27,1]="PRIMANOTA " 
 DIM[ 27,2]="IVCODIVA       " 
 DIM[ 27,3]=ah_Msgformat("Codice IVA%1",space(70)) 
 DIM[ 27,4]="C" 
 DIM[ 27,5]="LK" 
 DIM[ 27,6]="                                                  " 
 DIM[ 27,7]= 0 
 DIM[ 27,8]= cp_CharToDate("  -  -  ") 
 DIM[ 27,9]="VOCIIVA        " 
 DIM[ 28,1]="PRIMANOTA " 
 DIM[ 28,2]="IVNUMREG       " 
 DIM[ 28,3]=ah_Msgformat("Numero registro%1",space(66)) 
 DIM[ 28,4]="N" 
 DIM[ 28,5]="LB" 
 DIM[ 28,6]="                                                  " 
 DIM[ 28,7]= 0 
 DIM[ 28,8]= cp_CharToDate("  -  -  ") 
 DIM[ 28,9]="               " 
 DIM[ 29,1]="PRIMANVQR " 
 DIM[ 29,2]="MACIVA         " 
 DIM[ 29,3]=ah_Msgformat("Monte acquisti%1",space(66)) 
 DIM[ 29,4]="C" 
 DIM[ 29,5]="CC" 
 DIM[ 29,6]="                                                  " 
 DIM[ 29,7]= 0 
 DIM[ 29,8]= cp_CharToDate("  -  -  ") 
 DIM[ 29,9]="               " 
 DIM[ 30,1]="SALDIIVA  " 
 DIM[ 30,2]="MACIVA         " 
 DIM[ 30,3]=ah_Msgformat("Monte acquisti%1",space(66)) 
 DIM[ 30,4]="C" 
 DIM[ 30,5]="CC" 
 DIM[ 30,6]="                                                  " 
 DIM[ 30,7]= 0 
 DIM[ 30,8]= cp_CharToDate("  -  -  ") 
 DIM[ 30,9]="               " 
 DIM[ 31,1]="PRIMANVQR " 
 DIM[ 31,2]="NUMREG         " 
 DIM[ 31,3]=ah_Msgformat("Numero registro IVA%1",space(61)) 
 DIM[ 31,4]="N" 
 DIM[ 31,5]="LB" 
 DIM[ 31,6]="                                                  " 
 DIM[ 31,7]= 0 
 DIM[ 31,8]= cp_CharToDate("  -  -  ") 
 DIM[ 31,9]="               " 
 DIM[ 32,1]="PRIMANVQR " 
 DIM[ 32,2]="PERCOM         " 
 DIM[ 32,3]=ah_Msgformat("% Compensazione%1",space(65)) 
 DIM[ 32,4]="N" 
 DIM[ 32,5]="LB" 
 DIM[ 32,6]="                                                  " 
 DIM[ 32,7]= 0 
 DIM[ 32,8]= cp_CharToDate("  -  -  ") 
 DIM[ 32,9]="               " 
 DIM[ 33,1]="SALDIIVA  " 
 DIM[ 33,2]="PERCOM         " 
 DIM[ 33,3]=ah_Msgformat("% Compensazione%1",space(65)) 
 DIM[ 33,4]="N" 
 DIM[ 33,5]="LB" 
 DIM[ 33,6]="                                                  " 
 DIM[ 33,7]= 0 
 DIM[ 33,8]= cp_CharToDate("  -  -  ") 
 DIM[ 33,9]="               " 
 DIM[ 34,1]="PRIMANVQR " 
 DIM[ 34,2]="PERIND         " 
 DIM[ 34,3]=ah_Msgformat("% Indetraibile%1",space(66)) 
 DIM[ 34,4]="N" 
 DIM[ 34,5]="LB" 
 DIM[ 34,6]="                                                  " 
 DIM[ 34,7]= 0 
 DIM[ 34,8]= cp_CharToDate("  -  -  ") 
 DIM[ 34,9]="               " 
 DIM[ 35,1]="SALDIIVA  " 
 DIM[ 35,2]="PERIND         " 
 DIM[ 35,3]=ah_Msgformat("% Indetraibile%1",space(66)) 
 DIM[ 35,4]="N" 
 DIM[ 35,5]="LB" 
 DIM[ 35,6]="                                                  " 
 DIM[ 35,7]= 0 
 DIM[ 35,8]= cp_CharToDate("  -  -  ") 
 DIM[ 35,9]="               " 
 DIM[ 36,1]="PRIMANVQR " 
 DIM[ 36,2]="PERIVA         " 
 DIM[ 36,3]=ah_Msgformat("Percent. IVA%1",space(68)) 
 DIM[ 36,4]="N" 
 DIM[ 36,5]="LB" 
 DIM[ 36,6]="                                                  " 
 DIM[ 36,7]= 0 
 DIM[ 36,8]= cp_CharToDate("  -  -  ") 
 DIM[ 36,9]="               " 
 DIM[ 37,1]="SALDIIVA  " 
 DIM[ 37,2]="PERIVA         " 
 DIM[ 37,3]=ah_Msgformat("Percent. IVA%1",space(68)) 
 DIM[ 37,4]="N" 
 DIM[ 37,5]="LB" 
 DIM[ 37,6]="                                                  " 
 DIM[ 37,7]= 0 
 DIM[ 37,8]= cp_CharToDate("  -  -  ") 
 DIM[ 37,9]="               " 
 DIM[ 38,1]="PRIMANVQR " 
 DIM[ 38,2]="PLAIVA         " 
 DIM[ 38,3]=ah_Msgformat("PLAFOND S/N%1",space(69)) 
 DIM[ 38,4]="C" 
 DIM[ 38,5]="CC" 
 DIM[ 38,6]="                                                  " 
 DIM[ 38,7]= 0 
 DIM[ 38,8]= cp_CharToDate("  -  -  ") 
 DIM[ 38,9]="               " 
 DIM[ 39,1]="SALDIIVA  " 
 DIM[ 39,2]="PLAIVA         " 
 DIM[ 39,3]=ah_Msgformat("PLAFOND S/N%1",space(69)) 
 DIM[ 39,4]="C" 
 DIM[ 39,5]="CC" 
 DIM[ 39,6]="                                                  " 
 DIM[ 39,7]= 0 
 DIM[ 39,8]= cp_CharToDate("  -  -  ") 
 DIM[ 39,9]="               " 
 DIM[ 40,1]="PRIMANOTA " 
 DIM[ 40,2]="IVTIPREG       " 
 DIM[ 40,3]=ah_Msgformat("Tipo registro IVA%1",space(61)) 
 DIM[ 40,4]="C" 
 DIM[ 40,5]="CC" 
 DIM[ 40,6]="                                                  " 
 DIM[ 40,7]= 0 
 DIM[ 40,8]= cp_CharToDate("  -  -  ") 
 DIM[ 40,9]="CAU_CONT       " 
 DIM[ 41,1]="PRIMANVQR " 
 DIM[ 41,2]="PROIVA         " 
 DIM[ 41,3]=ah_Msgformat("Prorata%1",space(73)) 
 DIM[ 41,4]="C" 
 DIM[ 41,5]="CC" 
 DIM[ 41,6]="                                                  " 
 DIM[ 41,7]= 0 
 DIM[ 41,8]= cp_CharToDate("  -  -  ") 
 DIM[ 41,9]="               " 
 DIM[ 42,1]="SALDIIVA  " 
 DIM[ 42,2]="PROIVA         " 
 DIM[ 42,3]=ah_Msgformat("Prorata%1",space(73)) 
 DIM[ 42,4]="C" 
 DIM[ 42,5]="CC" 
 DIM[ 42,6]="                                                  " 
 DIM[ 42,7]= 0 
 DIM[ 42,8]= cp_CharToDate("  -  -  ") 
 DIM[ 42,9]="               " 
 DIM[ 43,1]="PRIMANVQR " 
 DIM[ 43,2]="REVCHA         " 
 DIM[ 43,3]=ah_Msgformat("Reverse Charge%1",space(66)) 
 DIM[ 43,4]="C" 
 DIM[ 43,5]="CC" 
 DIM[ 43,6]="                                                  " 
 DIM[ 43,7]= 0 
 DIM[ 43,8]= cp_CharToDate("  -  -  ") 
 DIM[ 43,9]="               " 
 DIM[ 44,1]="SALDIIVA  " 
 DIM[ 44,2]="REVCHA         " 
 DIM[ 44,3]=ah_Msgformat("Reverse Charge%1",space(66)) 
 DIM[ 44,4]="C" 
 DIM[ 44,5]="CC" 
 DIM[ 44,6]="                                                  " 
 DIM[ 44,7]= 0 
 DIM[ 44,8]= cp_CharToDate("  -  -  ") 
 DIM[ 44,9]="               " 
 DIM[ 45,1]="PRIMANVQR " 
 DIM[ 45,2]="TIPAGR         " 
 DIM[ 45,3]=ah_Msgformat("Tipo acq.agricolo%1",space(63)) 
 DIM[ 45,4]="C" 
 DIM[ 45,5]="CC" 
 DIM[ 45,6]="                                                  " 
 DIM[ 45,7]= 0 
 DIM[ 45,8]= cp_CharToDate("  -  -  ") 
 DIM[ 45,9]="               " 
 DIM[ 46,1]="SALDIIVA  " 
 DIM[ 46,2]="TIPAGR         " 
 DIM[ 46,3]=ah_Msgformat("Tipo acq.agricolo%1",space(63)) 
 DIM[ 46,4]="C" 
 DIM[ 46,5]="CC" 
 DIM[ 46,6]="                                                  " 
 DIM[ 46,7]= 0 
 DIM[ 46,8]= cp_CharToDate("  -  -  ") 
 DIM[ 46,9]="               " 
 DIM[ 47,1]="PRIMANVQR " 
 DIM[ 47,2]="TIPBEN         " 
 DIM[ 47,3]=ah_Msgformat("Tipologia beni%1",space(66)) 
 DIM[ 47,4]="C" 
 DIM[ 47,5]="CC" 
 DIM[ 47,6]="                                                  " 
 DIM[ 47,7]= 0 
 DIM[ 47,8]= cp_CharToDate("  -  -  ") 
 DIM[ 47,9]="               " 
 DIM[ 48,1]="SALDIIVA  " 
 DIM[ 48,2]="TIPBEN         " 
 DIM[ 48,3]=ah_Msgformat("Tipologia beni%1",space(66)) 
 DIM[ 48,4]="C" 
 DIM[ 48,5]="CC" 
 DIM[ 48,6]="                                                  " 
 DIM[ 48,7]= 0 
 DIM[ 48,8]= cp_CharToDate("  -  -  ") 
 DIM[ 48,9]="               " 
 DIM[ 49,1]="PRIMANVQR " 
 DIM[ 49,2]="TIPPLA         " 
 DIM[ 49,3]=ah_Msgformat("PLAFOND%1",space(73)) 
 DIM[ 49,4]="C" 
 DIM[ 49,5]="CC" 
 DIM[ 49,6]="                                                  " 
 DIM[ 49,7]= 0 
 DIM[ 49,8]= cp_CharToDate("  -  -  ") 
 DIM[ 49,9]="               " 
 DIM[ 50,1]="SALDIIVA  " 
 DIM[ 50,2]="TIPPLA         " 
 DIM[ 50,3]=ah_Msgformat("PLAFOND%1",space(73)) 
 DIM[ 50,4]="C" 
 DIM[ 50,5]="CC" 
 DIM[ 50,6]="                                                  " 
 DIM[ 50,7]= 0 
 DIM[ 50,8]= cp_CharToDate("  -  -  ") 
 DIM[ 50,9]="               " 
 DIM[ 51,1]="PRIMANVQR " 
 DIM[ 51,2]="TIPREG         " 
 DIM[ 51,3]=ah_Msgformat("Tipo registro IVA%1",space(13)) 
 DIM[ 51,4]="C" 
 DIM[ 51,5]="CC" 
 DIM[ 51,6]="                                                  " 
 DIM[ 51,7]= 0 
 DIM[ 51,8]= cp_CharToDate("  -  -  ") 
 DIM[ 51,9]="               " 
 DIM[ 52,1]="SALDIIVA  " 
 DIM[ 52,2]="TR__ANNO       " 
 DIM[ 52,3]=ah_Msgformat("Anno saldi%1", space(70)) 
 DIM[ 52,4]="C" 
 DIM[ 52,5]="AN" 
 DIM[ 52,6]="                                                  " 
 DIM[ 52,7]= 0 
 DIM[ 52,8]= cp_CharToDate("  -  -  ") 
 DIM[ 52,9]="               " 
 DIM[ 53,1]="SALDIIVA  " 
 DIM[ 53,2]="TRNUMPER       " 
 DIM[ 53,3]=ah_Msgformat("Periodo 1.4;1.12%1",space(61)) 
 DIM[ 53,4]="N" 
 DIM[ 53,5]="PE" 
 DIM[ 53,6]="                                                  " 
 DIM[ 53,7]= 0 
 DIM[ 53,8]= cp_CharToDate("  -  -  ") 
 DIM[ 53,9]="               " 
 DIM[ 54,1]="SALDIIVA  " 
 DIM[ 54,2]="TRNUMREG       " 
 DIM[ 54,3]=ah_Msgformat("Numero Registro%1",space(65)) 
 DIM[ 54,4]="N" 
 DIM[ 54,5]="LB" 
 DIM[ 54,6]="                                                  " 
 DIM[ 54,7]= 0 
 DIM[ 54,8]= cp_CharToDate("  -  -  ") 
 DIM[ 54,9]="               " 
 DIM[ 55,1]="SALDIIVA  " 
 DIM[ 55,2]="TRTIPREG       " 
 DIM[ 55,3]=ah_Msgformat("Tipo registro%1",space(67)) 
 DIM[ 55,4]="C" 
 DIM[ 55,5]="CC" 
 DIM[ 55,6]="                                                  " 
 DIM[ 55,7]= 0 
 DIM[ 55,8]= cp_CharToDate("  -  -  ") 
 DIM[ 55,9]="               " 
 DIM[ 56,1]="PERIODICA " 
 DIM[ 56,2]="VP__ANNO       " 
 DIM[ 56,3]=ah_Msgformat("Anno elaborazione%1",space(63)) 
 DIM[ 56,4]="C" 
 DIM[ 56,5]="AN" 
 DIM[ 56,6]="                                                  " 
 DIM[ 56,7]= 0 
 DIM[ 56,8]= cp_CharToDate("  -  -  ") 
 DIM[ 56,9]="               " 
 DIM[ 57,1]="PERIODICA " 
 DIM[ 57,2]="VPCODATT       " 
 DIM[ 57,3]=ah_Msgformat("Codice attivit�%1", space(65)) 
 DIM[ 57,4]="C" 
 DIM[ 57,5]="AT" 
 DIM[ 57,6]="                                                  " 
 DIM[ 57,7]= 0 
 DIM[ 57,8]= cp_CharToDate("  -  -  ") 
 DIM[ 57,9]="               " 
 DIM[ 58,1]="PERIODICA " 
 DIM[ 58,2]="VPPERIOD       " 
 DIM[ 58,3]=ah_Msgformat("Periodo elaborazione%1",space(60)) 
 DIM[ 58,4]="N" 
 DIM[ 58,5]="PE" 
 DIM[ 58,6]="                                                  " 
 DIM[ 58,7]= 0 
 DIM[ 58,8]= cp_CharToDate("  -  -  ") 
 DIM[ 58,9]="               " 
 DIM[ 59,1]="PRIMANVQR " 
 DIM[ 59,2]="CODATT       " 
 DIM[ 59,3]=ah_Msgformat("Codice attivit�%1",space(1)) 
 DIM[ 59,4]="C" 
 DIM[ 59,5]="AT" 
 DIM[ 59,6]="                                                  " 
 DIM[ 59,7]= 0 
 DIM[ 59,8]= cp_CharToDate("  -  -  ") 
 DIM[ 59,9]="               " 
 DIM[ 60,1]="CREDDIVA " 
 DIM[ 60,2]="CI__ANNO " 
 DIM[ 60,3]=ah_Msgformat("Anno solare%1",space(69)) 
 DIM[ 60,4]="C" 
 DIM[ 60,5]="AN" 
 DIM[ 60,6]="                                                  " 
 DIM[ 60,7]= 0 
 DIM[ 60,8]= cp_CharToDate("  -  -  ") 
 DIM[ 60,9]="               " 
 DIM[ 61,1]="CREDDIVA " 
 DIM[ 61,2]="CINUMPER" 
 DIM[ 61,3]=ah_Msgformat("Numero Periodo%1",space(1)) 
 DIM[ 61,4]="N" 
 DIM[ 61,5]="PE" 
 DIM[ 61,6]="                                                  " 
 DIM[ 61,7]= 0 
 DIM[ 61,8]= cp_CharToDate("  -  -  ") 
 DIM[ 61,9]="               " 
 DIM[ 62,1]="PERIODICA " 
 DIM[ 62,2]="VPDICSOC" 
 DIM[ 62,3]=ah_Msgformat("Societ� del gruppo%1",space(1)) 
 DIM[ 62,4]="C" 
 DIM[ 62,5]="CC" 
 DIM[ 62,6]="                                                  " 
 DIM[ 62,7]= 0 
 DIM[ 62,8]= cp_CharToDate("  -  -  ") 
 DIM[ 62,9]="               " 
 DIM[ 63,1]="SALDIIVA " 
 DIM[ 63,2]="CODATT" 
 DIM[ 63,3]=ah_Msgformat("Codice attivit�%1",space(1)) 
 DIM[ 63,4]="C" 
 DIM[ 63,5]="AT" 
 DIM[ 63,6]="                                                  " 
 DIM[ 63,7]= 0 
 DIM[ 63,8]= cp_CharToDate("  -  -  ") 
 DIM[ 63,9]="               "
      For i =1 to 63
      this.w_DESCRI = left(DIM[I,3],80)
      * --- Insert into TAB_DIME
      i_nConn=i_TableProp[this.TAB_DIME_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TAB_DIME_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TAB_DIME_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"DI_FONTE"+",DICODICE"+",DIDESCRI"+",DITIPDAT"+",DITIPDIM"+",DIVALCHR"+",DIVALNUM"+",DIVALDAT"+",DIARLINK"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(DIM[I,1]),'TAB_DIME','DI_FONTE');
        +","+cp_NullLink(cp_ToStrODBC(DIM[I,2]),'TAB_DIME','DICODICE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DESCRI),'TAB_DIME','DIDESCRI');
        +","+cp_NullLink(cp_ToStrODBC(DIM[I,4]),'TAB_DIME','DITIPDAT');
        +","+cp_NullLink(cp_ToStrODBC(DIM[I,5]),'TAB_DIME','DITIPDIM');
        +","+cp_NullLink(cp_ToStrODBC(DIM[I,6]),'TAB_DIME','DIVALCHR');
        +","+cp_NullLink(cp_ToStrODBC(DIM[I,7]),'TAB_DIME','DIVALNUM');
        +","+cp_NullLink(cp_ToStrODBC(DIM[I,8]),'TAB_DIME','DIVALDAT');
        +","+cp_NullLink(cp_ToStrODBC(DIM[I,9]),'TAB_DIME','DIARLINK');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'DI_FONTE',DIM[I,1],'DICODICE',DIM[I,2],'DIDESCRI',this.w_DESCRI,'DITIPDAT',DIM[I,4],'DITIPDIM',DIM[I,5],'DIVALCHR',DIM[I,6],'DIVALNUM',DIM[I,7],'DIVALDAT',DIM[I,8],'DIARLINK',DIM[I,9])
        insert into (i_cTable) (DI_FONTE,DICODICE,DIDESCRI,DITIPDAT,DITIPDIM,DIVALCHR,DIVALNUM,DIVALDAT,DIARLINK &i_ccchkf. );
           values (;
             DIM[I,1];
             ,DIM[I,2];
             ,this.w_DESCRI;
             ,DIM[I,4];
             ,DIM[I,5];
             ,DIM[I,6];
             ,DIM[I,7];
             ,DIM[I,8];
             ,DIM[I,9];
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error='Errore inserimento dimensioni'
        return
      endif
      endfor
      DIMENSION MIS(173,3) 
 MIS[ 1,1]="PLAFOND   " 
 MIS[ 1,2]="ESPC1          " 
 MIS[ 1,3]=ah_Msgformat("Esportazioni%1",space(59)) 
 MIS[ 2,1]="PLAFOND   " 
 MIS[ 2,2]="ESPC10         " 
 MIS[ 2,3]=ah_Msgformat("Esportazioni%1",space(59)) 
 MIS[ 3,1]="PLAFOND   " 
 MIS[ 3,2]="ESPC11         " 
 MIS[ 3,3]=ah_Msgformat("Esportazioni%1",space(59)) 
 MIS[ 4,1]="PLAFOND   " 
 MIS[ 4,2]="ESPC12         " 
 MIS[ 4,3]=ah_Msgformat("Esportazioni%1",space(59)) 
 MIS[ 5,1]="PLAFOND   " 
 MIS[ 5,2]="ESPC2          " 
 MIS[ 5,3]=ah_Msgformat("Esportazioni%1",space(59)) 
 MIS[ 6,1]="PLAFOND   " 
 MIS[ 6,2]="ESPC3          " 
 MIS[ 6,3]=ah_Msgformat("Esportazioni%1",space(59)) 
 MIS[ 7,1]="PLAFOND   " 
 MIS[ 7,2]="ESPC4          " 
 MIS[ 7,3]=ah_Msgformat("Esportazioni%1",space(59)) 
 MIS[ 8,1]="PLAFOND   " 
 MIS[ 8,2]="ESPC5          " 
 MIS[ 8,3]=ah_Msgformat("Esportazioni%1",space(59)) 
 MIS[ 9,1]="PLAFOND   " 
 MIS[ 9,2]="ESPC6          " 
 MIS[ 9,3]=ah_Msgformat("Esportazioni%1",space(59)) 
 MIS[ 10,1]="PLAFOND   " 
 MIS[ 10,2]="ESPC7          " 
 MIS[ 10,3]=ah_Msgformat("Esportazioni%1",space(59)) 
 MIS[ 11,1]="PLAFOND   " 
 MIS[ 11,2]="ESPC8          " 
 MIS[ 11,3]=ah_Msgformat("Esportazioni%1",space(59)) 
 MIS[ 12,1]="PLAFOND   " 
 MIS[ 12,2]="ESPC9          " 
 MIS[ 12,3]=ah_Msgformat("Esportazioni%1",space(59)) 
 MIS[ 13,1]="PLAFOND   " 
 MIS[ 13,2]="ESPP1          " 
 MIS[ 13,3]=ah_Msgformat("Esportazioni anno prec%1",space(58)) 
 MIS[ 14,1]="PLAFOND   " 
 MIS[ 14,2]="ESPP10         " 
 MIS[ 14,3]=ah_Msgformat("Esportazioni anno prec%1",space(58)) 
 MIS[ 15,1]="PLAFOND   " 
 MIS[ 15,2]="ESPP11         " 
 MIS[ 15,3]=ah_Msgformat("Esportazioni anno prec%1",space(58)) 
 MIS[ 16,1]="PLAFOND   " 
 MIS[ 16,2]="ESPP12         " 
 MIS[ 16,3]=ah_Msgformat("Esportazioni anno prec%1",space(58)) 
 MIS[ 17,1]="PLAFOND   " 
 MIS[ 17,2]="ESPP2          " 
 MIS[ 17,3]=ah_Msgformat("Esportazioni anno prec%1",space(58)) 
 MIS[ 18,1]="PLAFOND   " 
 MIS[ 18,2]="ESPP3          " 
 MIS[ 18,3]=ah_Msgformat("Esportazioni anno prec%1",space(58)) 
 MIS[ 19,1]="PLAFOND   " 
 MIS[ 19,2]="ESPP4          " 
 MIS[ 19,3]=ah_Msgformat("Esportazioni anno prec%1",space(58)) 
 MIS[ 20,1]="PLAFOND   " 
 MIS[ 20,2]="ESPP5          " 
 MIS[ 20,3]=ah_Msgformat("Esportazioni anno prec%1",space(58)) 
 MIS[ 21,1]="PLAFOND   " 
 MIS[ 21,2]="ESPP6          " 
 MIS[ 21,3]=ah_Msgformat("Esportazioni anno prec%1",space(58)) 
 MIS[ 22,1]="PLAFOND   " 
 MIS[ 22,2]="ESPP7          " 
 MIS[ 22,3]=ah_Msgformat("Esportazioni anno prec%1",space(58)) 
 MIS[ 23,1]="PLAFOND   " 
 MIS[ 23,2]="ESPP8          " 
 MIS[ 23,3]=ah_Msgformat("Esportazioni anno prec%1",space(58)) 
 MIS[ 24,1]="PLAFOND   " 
 MIS[ 24,2]="ESPP9          " 
 MIS[ 24,3]=ah_Msgformat("Esportazioni anno prec%1",space(58)) 
 MIS[ 25,1]="SEPINDOPE " 
 MIS[ 25,2]="IMPCORR        " 
 MIS[ 25,3]=ah_Msgformat("Oper. imponibili verso consumatori finali imponibile%1",space(28)) 
 MIS[ 26,1]="SEPINDOPE " 
 MIS[ 26,2]="IMPO01         " 
 MIS[ 26,3]=ah_Msgformat("Abruzzo imponibile%1", space(60)) 
 MIS[ 27,1]="SEPINDOPE " 
 MIS[ 27,2]="IMPO02         " 
 MIS[ 27,3]=ah_Msgformat("Basilicata imponibile%1", space(57)) 
 MIS[ 28,1]="SEPINDOPE " 
 MIS[ 28,2]="IMPO03         " 
 MIS[ 28,3]=ah_Msgformat("Bolzano imponibile%1", space(60)) 
 MIS[ 29,1]="SEPINDOPE " 
 MIS[ 29,2]="IMPO04         " 
 MIS[ 29,3]=ah_Msgformat("Calabria imponibile%1", space(60)) 
 MIS[ 30,1]="SEPINDOPE " 
 MIS[ 30,2]="IMPO05         " 
 MIS[ 30,3]=ah_Msgformat("Campania imponibile%1", space(59)) 
 MIS[ 31,1]="SEPINDOPE " 
 MIS[ 31,2]="IMPO06         " 
 MIS[ 31,3]=ah_Msgformat("Emilia romagna imponibile%1", space(53)) 
 MIS[ 32,1]="SEPINDOPE " 
 MIS[ 32,2]="IMPO07         " 
 MIS[ 32,3]=ah_Msgformat("Friuli venezia giulia imponibile%1", space(46)) 
 MIS[ 33,1]="SEPINDOPE " 
 MIS[ 33,2]="IMPO08         " 
 MIS[ 33,3]=ah_Msgformat("Lazio imponibile%1", space(62)) 
 MIS[ 34,1]="SEPINDOPE " 
 MIS[ 34,2]="IMPO09         " 
 MIS[ 34,3]=ah_Msgformat("Liguria imponibile%1", space(60)) 
 MIS[ 35,1]="SEPINDOPE " 
 MIS[ 35,2]="IMPO10         " 
 MIS[ 35,3]=ah_Msgformat("Lombardia imponibile%1", space(58)) 
 MIS[ 36,1]="SEPINDOPE " 
 MIS[ 36,2]="IMPO11         " 
 MIS[ 36,3]=ah_Msgformat("Marche imponibile%1", space(61)) 
 MIS[ 37,1]="SEPINDOPE " 
 MIS[ 37,2]="IMPO12         " 
 MIS[ 37,3]=ah_Msgformat("Molise imponibile%1", space(61)) 
 MIS[ 38,1]="SEPINDOPE " 
 MIS[ 38,2]="IMPO13         " 
 MIS[ 38,3]=ah_Msgformat("Piemonte imponibile%1", space(59)) 
 MIS[ 39,1]="SEPINDOPE " 
 MIS[ 39,2]="IMPO14         " 
 MIS[ 39,3]=ah_Msgformat("Puglia imponibile%1", space(61)) 
 MIS[ 40,1]="SEPINDOPE " 
 MIS[ 40,2]="IMPO15         " 
 MIS[ 40,3]=ah_Msgformat("Sardegna imponibile%1", space(59)) 
 MIS[ 41,1]="SEPINDOPE " 
 MIS[ 41,2]="IMPO16         " 
 MIS[ 41,3]=ah_Msgformat("Sicilia imponibile%1", space(60)) 
 MIS[ 42,1]="SEPINDOPE " 
 MIS[ 42,2]="IMPO17         " 
 MIS[ 42,3]=ah_Msgformat("Toscana imponibile%1", space(60)) 
 MIS[ 43,1]="SEPINDOPE " 
 MIS[ 43,2]="IMPO18         " 
 MIS[ 43,3]=ah_Msgformat("Trento imponibile%1", space(61)) 
 MIS[ 44,1]="SEPINDOPE " 
 MIS[ 44,2]="IMPO19         " 
 MIS[ 44,3]=ah_Msgformat("Umbria imponibile%1", space(61)) 
 MIS[ 45,1]="SEPINDOPE " 
 MIS[ 45,2]="IMPO20         " 
 MIS[ 45,3]=ah_Msgformat("Valle d'aosta imponibile%1", space(54)) 
 MIS[ 46,1]="SEPINDOPE " 
 MIS[ 46,2]="IMPO21         " 
 MIS[ 46,3]=ah_Msgformat("Veneto imponibile%1", space(61)) 
 MIS[ 47,1]="SEPINDOPE " 
 MIS[ 47,2]="IMPVEN         " 
 MIS[ 47,3]=ah_Msgformat("Oper. imponibili verso soggetti IVA imponibile%1",space(34)) 
 MIS[ 48,1]="SEPINDOPE " 
 MIS[ 48,2]="IVA01          " 
 MIS[ 48,3]=ah_Msgformat("Abruzzo imposta%1",space(63)) 
 MIS[ 49,1]="SEPINDOPE " 
 MIS[ 49,2]="IVA02          " 
 MIS[ 49,3]=ah_Msgformat("Basilicata imposta%1",space(60)) 
 MIS[ 50,1]="SEPINDOPE " 
 MIS[ 50,2]="IVA03          " 
 MIS[ 50,3]=ah_Msgformat("Bolzano imposta%1",space(63)) 
 MIS[ 51,1]="SEPINDOPE " 
 MIS[ 51,2]="IVA04          " 
 MIS[ 51,3]=ah_Msgformat("Calabria imposta%1",space(62)) 
 MIS[ 52,1]="SEPINDOPE " 
 MIS[ 52,2]="IVA05          " 
 MIS[ 52,3]=ah_Msgformat("Campania imposta%1",space(62)) 
 MIS[ 53,1]="SEPINDOPE " 
 MIS[ 53,2]="IVA06          " 
 MIS[ 53,3]=ah_Msgformat("Emilia romagna imposta%1",space(56)) 
 MIS[ 54,1]="SEPINDOPE " 
 MIS[ 54,2]="IVA07          " 
 MIS[ 54,3]=ah_Msgformat("Friuli venezia giulia imposta%1",space(49)) 
 MIS[ 55,1]="SEPINDOPE " 
 MIS[ 55,2]="IVA08          " 
 MIS[ 55,3]=ah_Msgformat("Lazio imposta%1",space(65)) 
 MIS[ 56,1]="SEPINDOPE " 
 MIS[ 56,2]="IVA09          " 
 MIS[ 56,3]=ah_Msgformat("Liguria imposta%1",space(63)) 
 MIS[ 57,1]="SEPINDOPE " 
 MIS[ 57,2]="IVA10          " 
 MIS[ 57,3]=ah_Msgformat("Lombardia imposta%1",space(61)) 
 MIS[ 58,1]="SEPINDOPE " 
 MIS[ 58,2]="IVA11          " 
 MIS[ 58,3]=ah_Msgformat("Marche imposta%1",space(64)) 
 MIS[ 59,1]="SEPINDOPE " 
 MIS[ 59,2]="IVA12          " 
 MIS[ 59,3]=ah_Msgformat("Molise imposta%1",space(64)) 
 MIS[ 60,1]="SEPINDOPE " 
 MIS[ 60,2]="IVA13          " 
 MIS[ 60,3]=ah_Msgformat("Piemonte imposta%1",space(62)) 
 MIS[ 61,1]="SEPINDOPE " 
 MIS[ 61,2]="IVA14          " 
 MIS[ 61,3]=ah_Msgformat("Puglia imposta%1",space(64)) 
 MIS[ 62,1]="SEPINDOPE " 
 MIS[ 62,2]="IVA15          " 
 MIS[ 62,3]=ah_Msgformat("Sardegna imposta%1",space(62)) 
 MIS[ 63,1]="SEPINDOPE " 
 MIS[ 63,2]="IVA16          " 
 MIS[ 63,3]=ah_Msgformat("Sicilia imposta%1",space(65)) 
 MIS[ 64,1]="SEPINDOPE " 
 MIS[ 64,2]="IVA17          " 
 MIS[ 64,3]=ah_Msgformat("Toscana imposta%1",space(65)) 
 MIS[ 65,1]="SEPINDOPE " 
 MIS[ 65,2]="IVA18          " 
 MIS[ 65,3]=ah_Msgformat("Trento imposta%1",space(66)) 
 MIS[ 66,1]="SEPINDOPE " 
 MIS[ 66,2]="IVA19          " 
 MIS[ 66,3]=ah_Msgformat("Umbria imposta%1",space(64)) 
 MIS[ 67,1]="SEPINDOPE " 
 MIS[ 67,2]="IVA20          " 
 MIS[ 67,3]=ah_Msgformat("Valle d'aosta imposta%1",space(59)) 
 MIS[ 68,1]="SEPINDOPE " 
 MIS[ 68,2]="IVA21          " 
 MIS[ 68,3]=ah_Msgformat("Veneto imposta%1",space(66)) 
 MIS[ 69,1]="SEPINDOPE " 
 MIS[ 69,2]="IVACORR        " 
 MIS[ 69,3]=ah_Msgformat("Oper. imponibili verso consumatori finali imposta%1",space(32)) 
 MIS[ 70,1]="SEPINDOPE " 
 MIS[ 70,2]="IVAVEN         " 
 MIS[ 70,3]=ah_Msgformat("Oper. imponibili verso soggetti IVA imposta%1",space(37)) 
 MIS[ 71,1]="PRIMANOTA " 
 MIS[ 71,2]="IVIMPIVA       " 
 MIS[ 71,3]=ah_Msgformat("Imposta%1", space(73)) 
 MIS[ 72,1]="PRIMANVQR " 
 MIS[ 72,2]="IVIMPIVA       " 
 MIS[ 72,3]=ah_Msgformat("Imposta%1", space(73)) 
 MIS[ 73,1]="PRIMANOTA " 
 MIS[ 73,2]="IVIMPONI       " 
 MIS[ 73,3]=ah_Msgformat("Imponibile IVA%1",space(66)) 
 MIS[ 74,1]="PRIMANVQR " 
 MIS[ 74,2]="IVIMPONI       " 
 MIS[ 74,3]=ah_Msgformat("Imponibile IVA%1",space(66)) 
 MIS[ 75,1]="SEPINDOPE " 
 MIS[ 75,2]="TOTIMP         " 
 MIS[ 75,3]=ah_Msgformat("Totale operazioni imponibili imponibile%1", space(41)) 
 MIS[ 76,1]="SEPINDOPE " 
 MIS[ 76,2]="TOTIVA         " 
 MIS[ 76,3]=ah_Msgformat("Totale operazioni imponibili imposta%1", space(44)) 
 MIS[ 77,1]="SALDIIVA  " 
 MIS[ 77,2]="TRIMPFAD       " 
 MIS[ 77,3]=ah_Msgformat("Impon.fat.esig.diff.%1",space(60)) 
 MIS[ 78,1]="SALDIIVA  " 
 MIS[ 78,2]="TRIMPIND       " 
 MIS[ 78,3]=ah_Msgformat("Impon.inc.esig.diff.%1",space(60)) 
 MIS[ 79,1]="SALDIIVA  " 
 MIS[ 79,2]="TRIMPPRE       " 
 MIS[ 79,3]=ah_Msgformat("Impon.per.preced.%1",space(63)) 
 MIS[ 80,1]="SALDIIVA  " 
 MIS[ 80,2]="TRIMPSEG       " 
 MIS[ 80,3]=ah_Msgformat("Impon.per.seguente%1",space(62)) 
 MIS[ 81,1]="SALDIIVA  " 
 MIS[ 81,2]="TRIMPSTA       " 
 MIS[ 81,3]=ah_Msgformat("Impon.doc.periodo%1",space(63)) 
 MIS[ 82,1]="SALDIIVA  " 
 MIS[ 82,2]="TRIVAFAD       " 
 MIS[ 82,3]=ah_Msgformat("IVA det.fat.esig.dif%1",space(60)) 
 MIS[ 83,1]="SALDIIVA  " 
 MIS[ 83,2]="TRIVAIND       " 
 MIS[ 83,3]=ah_Msgformat("IVA det.inc.esig.dif%1",space(60)) 
 MIS[ 84,1]="SALDIIVA  " 
 MIS[ 84,2]="TRIVAPRE       " 
 MIS[ 84,3]=ah_Msgformat("IVA det.per.preced.%1",space(61)) 
 MIS[ 85,1]="SALDIIVA  " 
 MIS[ 85,2]="TRIVASEG       " 
 MIS[ 85,3]=ah_Msgformat("IVA det.per.seguente%1",space(60)) 
 MIS[ 86,1]="SALDIIVA  " 
 MIS[ 86,2]="TRIVASTA       " 
 MIS[ 86,3]=ah_Msgformat("IVA det.doc.periodo%1",space(61)) 
 MIS[ 87,1]="SALDIIVA  " 
 MIS[ 87,2]="TRIVIFAD       " 
 MIS[ 87,3]=ah_Msgformat("IVA indet.fatt.esig.%1",space(60)) 
 MIS[ 88,1]="SALDIIVA  " 
 MIS[ 88,2]="TRIVIIND       " 
 MIS[ 88,3]=ah_Msgformat("IVA indet.inc.esig.d%1",space(60)) 
 MIS[ 89,1]="SALDIIVA  " 
 MIS[ 89,2]="TRIVIPRE       " 
 MIS[ 89,3]=ah_Msgformat("IVA indet.per.preced%1",space(60)) 
 MIS[ 90,1]="SALDIIVA  " 
 MIS[ 90,2]="TRIVISEG       " 
 MIS[ 90,3]=ah_Msgformat("IVA indet.per.seguen%1",space(60)) 
 MIS[ 91,1]="SALDIIVA  " 
 MIS[ 91,2]="TRIVISTA       " 
 MIS[ 91,3]=ah_Msgformat("IVA indet. doc.perio%1",space(60)) 
 MIS[ 92,1]="SALDIIVA  " 
 MIS[ 92,2]="TRMACFAD       " 
 MIS[ 92,3]=ah_Msgformat("Monte acq.fat.es.dif%1",space(60)) 
 MIS[ 93,1]="SALDIIVA  " 
 MIS[ 93,2]="TRMACIND       " 
 MIS[ 93,3]=ah_Msgformat("Monte acq.inc.esig.d%1",space(60)) 
 MIS[ 94,1]="SALDIIVA  " 
 MIS[ 94,2]="TRMACPRE       " 
 MIS[ 94,3]=ah_Msgformat("Monte acq.per.preced%1",space(60)) 
 MIS[ 95,1]="SALDIIVA  " 
 MIS[ 95,2]="TRMACSEG       " 
 MIS[ 95,3]=ah_Msgformat("Monte acq.per.seguen%1",space(60)) 
 MIS[ 96,1]="SALDIIVA  " 
 MIS[ 96,2]="TRMACSTA       " 
 MIS[ 96,3]=ah_Msgformat("Monte acq.doc.period%1",space(60)) 
 MIS[ 97,1]="PLAFOND   " 
 MIS[ 97,2]="UIMPORT1       " 
 MIS[ 97,3]=ah_Msgformat("Importazioni%1",space(68)) 
 MIS[ 98,1]="PLAFOND   " 
 MIS[ 98,2]="UIMPORT10      " 
 MIS[ 98,3]=ah_Msgformat("Importazioni%1",space(68)) 
 MIS[ 99,1]="PLAFOND   " 
 MIS[ 99,2]="UIMPORT11      " 
 MIS[ 99,3]=ah_Msgformat("Importazioni%1",space(68)) 
 MIS[ 100,1]="PLAFOND   " 
 MIS[ 100,2]="UIMPORT12      " 
 MIS[ 100,3]=ah_Msgformat("Importazioni%1",space(68)) 
 MIS[ 101,1]="PLAFOND   " 
 MIS[ 101,2]="UIMPORT2       " 
 MIS[ 101,3]=ah_Msgformat("Importazioni%1",space(68)) 
 MIS[ 102,1]="PLAFOND   " 
 MIS[ 102,2]="UIMPORT3       " 
 MIS[ 102,3]=ah_Msgformat("Importazioni%1",space(68)) 
 MIS[ 103,1]="PLAFOND   " 
 MIS[ 103,2]="UIMPORT4       " 
 MIS[ 103,3]=ah_Msgformat("Importazioni%1",space(68)) 
 MIS[ 104,1]="PLAFOND   " 
 MIS[ 104,2]="UIMPORT5       " 
 MIS[ 104,3]=ah_Msgformat("Importazioni%1",space(68)) 
 MIS[ 105,1]="PLAFOND   " 
 MIS[ 105,2]="UIMPORT6       " 
 MIS[ 105,3]=ah_Msgformat("Importazioni%1",space(68)) 
 MIS[ 106,1]="PLAFOND   " 
 MIS[ 106,2]="UIMPORT7       " 
 MIS[ 106,3]=ah_Msgformat("Importazioni%1",space(68)) 
 MIS[ 107,1]="PLAFOND   " 
 MIS[ 107,2]="UIMPORT8       " 
 MIS[ 107,3]=ah_Msgformat("Importazioni%1",space(68)) 
 MIS[ 108,1]="PLAFOND   " 
 MIS[ 108,2]="UIMPORT9       " 
 MIS[ 108,3]=ah_Msgformat("Importazioni%1",space(68)) 
 MIS[ 109,1]="PLAFOND   " 
 MIS[ 109,2]="UINTRA1        " 
 MIS[ 109,3]=ah_Msgformat("Acquisti interni o intracomunitari%1",space(46)) 
 MIS[ 110,1]="PLAFOND   " 
 MIS[ 110,2]="UINTRA10       " 
 MIS[ 110,3]=ah_Msgformat("Acquisti interni o intracomunitari%1",space(46)) 
 MIS[ 111,1]="PLAFOND   " 
 MIS[ 111,2]="UINTRA11       " 
 MIS[ 111,3]=ah_Msgformat("Acquisti interni o intracomunitari%1",space(46)) 
 MIS[ 112,1]="PLAFOND   " 
 MIS[ 112,2]="UINTRA12       " 
 MIS[ 112,3]=ah_Msgformat("Acquisti interni o intracomunitari%1",space(46)) 
 MIS[ 113,1]="PLAFOND   " 
 MIS[ 113,2]="UINTRA2        " 
 MIS[ 113,3]=ah_Msgformat("Acquisti interni o intracomunitari%1",space(46)) 
 MIS[ 114,1]="PLAFOND   " 
 MIS[ 114,2]="UINTRA3        " 
 MIS[ 114,3]=ah_Msgformat("Acquisti interni o intracomunitari%1",space(46)) 
 MIS[ 115,1]="PLAFOND   " 
 MIS[ 115,2]="UINTRA4        " 
 MIS[ 115,3]=ah_Msgformat("Acquisti interni o intracomunitari%1",space(46)) 
 MIS[ 116,1]="PLAFOND   " 
 MIS[ 116,2]="UINTRA5        " 
 MIS[ 116,3]=ah_Msgformat("Acquisti interni o intracomunitari%1",space(46)) 
 MIS[ 117,1]="PLAFOND   " 
 MIS[ 117,2]="UINTRA6        " 
 MIS[ 117,3]=ah_Msgformat("Acquisti interni o intracomunitari%1",space(46)) 
 MIS[ 118,1]="PLAFOND   " 
 MIS[ 118,2]="UINTRA7        " 
 MIS[ 118,3]=ah_Msgformat("Acquisti interni o intracomunitari%1",space(46)) 
 MIS[ 119,1]="PLAFOND   " 
 MIS[ 119,2]="UINTRA8        " 
 MIS[ 119,3]=ah_Msgformat("Acquisti interni o intracomunitari%1",space(46)) 
 MIS[ 120,1]="PLAFOND   " 
 MIS[ 120,2]="UINTRA9        " 
 MIS[ 120,3]=ah_Msgformat("Acquisti interni o intracomunitari%1",space(46)) 
 MIS[ 121,1]="PLAFOND   " 
 MIS[ 121,2]="VAC1           " 
 MIS[ 121,3]=ah_Msgformat("Volume di affari%1",space(64)) 
 MIS[ 122,1]="PLAFOND   " 
 MIS[ 122,2]="VAC10          " 
 MIS[ 122,3]=ah_Msgformat("Volume di affari%1",space(64)) 
 MIS[ 123,1]="PLAFOND   " 
 MIS[ 123,2]="VAC11          " 
 MIS[ 123,3]=ah_Msgformat("Volume di affari%1",space(64)) 
 MIS[ 124,1]="PLAFOND   " 
 MIS[ 124,2]="VAC12          " 
 MIS[ 124,3]=ah_Msgformat("Volume di affari%1",space(64)) 
 MIS[ 125,1]="PLAFOND   " 
 MIS[ 125,2]="VAC2           " 
 MIS[ 125,3]=ah_Msgformat("Volume di affari%1",space(64)) 
 MIS[ 126,1]="PLAFOND   " 
 MIS[ 126,2]="VAC3           " 
 MIS[ 126,3]=ah_Msgformat("Volume di affari%1",space(64)) 
 MIS[ 127,1]="PLAFOND   " 
 MIS[ 127,2]="VAC4           " 
 MIS[ 127,3]=ah_Msgformat("Volume di affari%1",space(64)) 
 MIS[ 128,1]="PLAFOND   " 
 MIS[ 128,2]="VAC5           " 
 MIS[ 128,3]=ah_Msgformat("Volume di affari%1",space(64)) 
 MIS[ 129,1]="PLAFOND   " 
 MIS[ 129,2]="VAC6           " 
 MIS[ 129,3]=ah_Msgformat("Volume di affari%1",space(64)) 
 MIS[ 130,1]="PLAFOND   " 
 MIS[ 130,2]="VAC7           " 
 MIS[ 130,3]=ah_Msgformat("Volume di affari%1",space(64)) 
 MIS[ 131,1]="PLAFOND   " 
 MIS[ 131,2]="VAC8           " 
 MIS[ 131,3]=ah_Msgformat("Volume di affari%1",space(64)) 
 MIS[ 132,1]="PLAFOND   " 
 MIS[ 132,2]="VAC9           " 
 MIS[ 132,3]=ah_Msgformat("Volume di affari%1",space(64)) 
 MIS[ 133,1]="PLAFOND   " 
 MIS[ 133,2]="VAP1           " 
 MIS[ 133,3]=ah_Msgformat("Volume di affari anno prec%1",space(54)) 
 MIS[ 134,1]="PLAFOND   " 
 MIS[ 134,2]="VAP10          " 
 MIS[ 134,3]=ah_Msgformat("Volume di affari anno prec%1",space(54)) 
 MIS[ 135,1]="PLAFOND   " 
 MIS[ 135,2]="VAP11          " 
 MIS[ 135,3]=ah_Msgformat("Volume di affari anno prec%1",space(54)) 
 MIS[ 136,1]="PLAFOND   " 
 MIS[ 136,2]="VAP12          " 
 MIS[ 136,3]=ah_Msgformat("Volume di affari anno prec%1",space(54)) 
 MIS[ 137,1]="PLAFOND   " 
 MIS[ 137,2]="VAP2           " 
 MIS[ 137,3]=ah_Msgformat("Volume di affari anno prec%1",space(54)) 
 MIS[ 138,1]="PLAFOND   " 
 MIS[ 138,2]="VAP3           " 
 MIS[ 138,3]=ah_Msgformat("Volume di affari anno prec%1",space(54)) 
 MIS[ 139,1]="PLAFOND   " 
 MIS[ 139,2]="VAP4           " 
 MIS[ 139,3]=ah_Msgformat("Volume di affari anno prec%1",space(54)) 
 MIS[ 140,1]="PLAFOND   " 
 MIS[ 140,2]="VAP5           " 
 MIS[ 140,3]=ah_Msgformat("Volume di affari anno prec%1",space(54)) 
 MIS[ 141,1]="PLAFOND   " 
 MIS[ 141,2]="VAP6           " 
 MIS[ 141,3]=ah_Msgformat("Volume di affari anno prec%1",space(54)) 
 MIS[ 142,1]="PLAFOND   " 
 MIS[ 142,2]="VAP7           " 
 MIS[ 142,3]=ah_Msgformat("Volume di affari anno prec%1",space(54)) 
 MIS[ 143,1]="PLAFOND   " 
 MIS[ 143,2]="VAP8           " 
 MIS[ 143,3]=ah_Msgformat("Volume di affari anno prec%1",space(54)) 
 MIS[ 144,1]="PLAFOND   " 
 MIS[ 144,2]="VAP9           " 
 MIS[ 144,3]=ah_Msgformat("Volume di affari anno prec%1",space(54)) 
 MIS[ 145,1]="ALTRIQUAD " 
 MIS[ 145,2]="VF16           " 
 MIS[ 145,3]=ah_Msgformat("Acquisti effettuati nell'anno ma con imposta esigibile in anni successivi%1",space(7)) 
 MIS[ 146,1]="ALTRIQUAD " 
 MIS[ 146,2]="VF17           " 
 MIS[ 146,3]=ah_Msgformat("Acquisti effettuati in anni precedenti ma con imposta esigibile nell'anno%1",space(7)) 
 MIS[ 147,1]="ALTRIQUAD " 
 MIS[ 147,2]="VP37           " 
 MIS[ 147,3]=ah_Msgformat("Operazioni effettuate nell'anno ma con imposta esigibile in anni successivi%1",space(5)) 
 MIS[ 148,1]="ALTRIQUAD " 
 MIS[ 148,2]="VP38           " 
 MIS[ 148,3]=ah_Msgformat("Operazioni effettuate in anni precedenti ma con imposta esigibile nell'anno%1",space(5)) 
 MIS[ 149,1]="PERIODICA " 
 MIS[ 149,2]="VPACQINT       " 
 MIS[ 149,3]=ah_Msgformat("VP2 - di cui acquisti Intra%1",space(53)) 
 MIS[ 150,1]="PERIODICA " 
 MIS[ 150,2]="VPCESINT       " 
 MIS[ 150,3]=ah_Msgformat("VP1 - di cui cessioni Intra%1",space(53)) 
 MIS[ 151,1]="PERIODICA " 
 MIS[ 151,2]="VPCRERIM       " 
 MIS[ 151,3]=ah_Msgformat("VP31 - credito chiesto a rimborso%1",space(47)) 
 MIS[ 152,1]="PERIODICA " 
 MIS[ 152,2]="VPCREUTI       " 
 MIS[ 152,3]=ah_Msgformat("VP32 - credito da utilizzare in compensazione con mod. F24%1",space(22)) 
 MIS[ 153,1]="PERIODICA " 
 MIS[ 153,2]="VPIMPO10       " 
 MIS[ 153,3]=ah_Msgformat("VP15 - debito/credito riportato dal periodo preced.%1",space(29)) 
 MIS[ 154,1]="PERIODICA " 
 MIS[ 154,2]="VPIMPO11       " 
 MIS[ 154,3]=ah_Msgformat("VP16 - credito IVA compensabile in detrazione%1", space(35)) 
 MIS[ 155,1]="PERIODICA " 
 MIS[ 155,2]="VPIMPO12       " 
 MIS[ 155,3]=ah_Msgformat("VP17 - IVA dovuta/credito per il periodo%1",space(40)) 
 MIS[ 156,1]="PERIODICA " 
 MIS[ 156,2]="VPIMPO13       " 
 MIS[ 156,3]=ah_Msgformat("VP18 - crediti speciali di imposta detratti%1",space(37)) 
 MIS[ 157,1]="PERIODICA " 
 MIS[ 157,2]="VPIMPO14       " 
 MIS[ 157,3]=ah_Msgformat("VP19 - interessi dovuti per liquidazioni trimestrali%1", space(28)) 
 MIS[ 158,1]="PERIODICA " 
 MIS[ 158,2]="VPIMPO15       " 
 MIS[ 158,3]=ah_Msgformat("VP20 - acconto versato%1",space(58)) 
 MIS[ 159,1]="PERIODICA " 
 MIS[ 159,2]="VPIMPO16       " 
 MIS[ 159,3]=ah_Msgformat("VP21 - importo da versare (o da trasferire)%1",space(37)) 
 MIS[ 160,1]="PERIODICA " 
 MIS[ 160,2]="VPIMPON3       " 
 MIS[ 160,3]=ah_Msgformat("VP3 - imponibile oro/argento%1", space(52)) 
 MIS[ 161,1]="PERIODICA " 
 MIS[ 161,2]="VPIMPOR1       " 
 MIS[ 161,3]=ah_Msgformat("VP1 - operazioni attive%1",space(56)) 
 MIS[ 162,1]="PERIODICA " 
 MIS[ 162,2]="VPIMPOR2       " 
 MIS[ 162,3]=ah_Msgformat("VP2 - operazioni passive%1",space(56)) 
 MIS[ 163,1]="PERIODICA " 
 MIS[ 163,2]="VPIMPOR5       " 
 MIS[ 163,3]=ah_Msgformat("VP10 - IVA esigibile periodo%1",space(52)) 
 MIS[ 164,1]="PERIODICA " 
 MIS[ 164,2]="VPIMPOR6       " 
 MIS[ 164,3]=ah_Msgformat("VP11 - IVA detratta periodo%1",space(53)) 
 MIS[ 165,1]="PERIODICA " 
 MIS[ 165,2]="VPIMPOR7       " 
 MIS[ 165,3]=ah_Msgformat("VP12 - IVA a debito/credito del periodo%1",space(41)) 
 MIS[ 166,1]="PERIODICA " 
 MIS[ 166,2]="VPIMPOR8       " 
 MIS[ 166,3]=ah_Msgformat("VP13 - variazioni di imposta x periodi precedenti%1",space(31)) 
 MIS[ 167,1]="PERIODICA " 
 MIS[ 167,2]="VPIMPOR9       " 
 MIS[ 167,3]=ah_Msgformat("VP14 - IVA non versata o in vers. in eccesso da prec.dich.%1",space(22)) 
 MIS[ 168,1]="PERIODICA " 
 MIS[ 168,2]="VPIMPOS3       " 
 MIS[ 168,3]=ah_Msgformat("VP3 - imposta oro/argento%1",space(55)) 
 MIS[ 169,1]="PERIODICA " 
 MIS[ 169,2]="VPIMPVER       " 
 MIS[ 169,3]=ah_Msgformat("VP22 - importo versato%1",space(58)) 
 MIS[ 170,1]="PLAFOND   " 
 MIS[ 170,2]="PLAINI        " 
 MIS[ 170,3]=ah_Msgformat("Plafond iniziale%1",space(58)) 
 MIS[ 171,1]="CREDDIVA " 
 MIS[ 171,2]="CICREF24 " 
 MIS[ 171,3]=ah_Msgformat("IVAF24 - altri tributi%1",space(58)) 
 MIS[ 172,1]="CREDDIVA" 
 MIS[ 172,2]="CICRERIM " 
 MIS[ 172,3]=ah_Msgformat("Rimborso%1",space(72)) 
 MIS[ 173,1]="CREDDIVA" 
 MIS[ 173,2]="CICREINI " 
 MIS[ 173,3]=ah_Msgformat("Credito iniziale%1",space(64)) 
      For i =1 to 173
      this.w_DESCRI = left(MIS[I,3],80)
      * --- Insert into TAB_MISU
      i_nConn=i_TableProp[this.TAB_MISU_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TAB_MISU_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TAB_MISU_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"MI_FONTE"+",MICODICE"+",MIDESCRI"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(MIS[I,1]),'TAB_MISU','MI_FONTE');
        +","+cp_NullLink(cp_ToStrODBC(MIS[I,2]),'TAB_MISU','MICODICE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DESCRI),'TAB_MISU','MIDESCRI');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'MI_FONTE',MIS[I,1],'MICODICE',MIS[I,2],'MIDESCRI',this.w_DESCRI)
        insert into (i_cTable) (MI_FONTE,MICODICE,MIDESCRI &i_ccchkf. );
           values (;
             MIS[I,1];
             ,MIS[I,2];
             ,this.w_DESCRI;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error='Errore inserimento misure'
        return
      endif
      endfor
      DIMENSION COM(96,8) 
 COM[1,1]="PRIMANVQR " 
 COM[1,2]="ACQINT         " 
 COM[1,3]= 3 
 COM[1,4]="C" 
 COM[1,5]="I                                                 " 
 COM[1,6]= 0 
 COM[1,7]= cp_CharToDate("  -  -  ") 
 COM[1,8]=ah_Msgformat("Importazioni oro%1",space(64)) 
 COM[2,1]="PRIMANVQR " 
 COM[2,2]="ACQINT         " 
 COM[2,3]= 1 
 COM[2,4]="C" 
 COM[2,5]="N                                                 " 
 COM[2,6]= 0 
 COM[2,7]= cp_CharToDate("  -  -  ") 
 COM[2,8]=ah_Msgformat("Acquisti INTRA%1",space(66)) 
 COM[3,1]="PRIMANVQR " 
 COM[3,2]="ACQINT         " 
 COM[3,3]= 2 
 COM[3,4]="C" 
 COM[3,5]="C                                                 " 
 COM[3,6]= 0 
 COM[3,7]= cp_CharToDate("  -  -  ") 
 COM[3,8]=ah_Msgformat("Cessioni INTRA%1",space(66)) 
 COM[4,1]="PRIMANVQR " 
 COM[4,2]="ACQINT         " 
 COM[4,3]= 4 
 COM[4,4]="C" 
 COM[4,5]="O                                                 " 
 COM[4,6]= 0 
 COM[4,7]= cp_CharToDate("  -  -  ") 
 COM[4,8]=ah_Msgformat("Acq.+ imp. oro%1",space(66)) 
 COM[5,1]="PRIMANVQR " 
 COM[5,2]="BOLIVA         " 
 COM[5,3]= 1 
 COM[5,4]="C" 
 COM[5,5]="S                                                 " 
 COM[5,6]= 0 
 COM[5,7]= cp_CharToDate("  -  -  ") 
 COM[5,8]=ah_Msgformat("Bollo su importi esenti S%1",space(55)) 
 COM[6,1]="PRIMANVQR " 
 COM[6,2]="BOLIVA         " 
 COM[6,3]= 2 
 COM[6,4]="C" 
 COM[6,5]="N                                                 " 
 COM[6,6]= 0 
 COM[6,7]= cp_CharToDate("  -  -  ") 
 COM[6,8]=ah_Msgformat("Bollo su importi esenti N%1",space(55)) 
 COM[7,1]="PRIMANVQR " 
 COM[7,2]="DATANA         " 
 COM[7,3]= 1 
 COM[7,4]="N" 
 COM[7,5]="                                                  " 
 COM[7,6]= 1 
 COM[7,7]= cp_CharToDate("  -  -  ") 
 COM[7,8]=ah_Msgformat("Nessuno%1",space(73)) 
 COM[8,1]="PRIMANVQR " 
 COM[8,2]="DATANA         " 
 COM[8,3]= 2 
 COM[8,4]="N" 
 COM[8,5]="                                                  " 
 COM[8,6]= 2 
 COM[8,7]= cp_CharToDate("  -  -  ") 
 COM[8,8]=ah_Msgformat("Oper. non imponibili%1",space(60)) 
 COM[9,1]="PRIMANVQR " 
 COM[9,2]="DATANA         " 
 COM[9,3]= 3 
 COM[9,4]="N" 
 COM[9,5]="                                                  " 
 COM[9,6]= 3 
 COM[9,7]= cp_CharToDate("  -  -  ") 
 COM[9,8]=ah_Msgformat("Operazioni esenti%1",space(63)) 
 COM[10,1]="PRIMANVQR " 
 COM[10,2]="DATANA         " 
 COM[10,3]= 4 
 COM[10,4]="N" 
 COM[10,5]="                                                  " 
 COM[10,6]= 4 
 COM[10,7]= cp_CharToDate("  -  -  ") 
 COM[10,8]=ah_Msgformat("Cessioni intracom. beni%1",space(58)) 
 COM[11,1]="PRIMANVQR " 
 COM[11,2]="DATANP         " 
 COM[11,3]= 1 
 COM[11,4]="N" 
 COM[11,5]="                                                  " 
 COM[11,6]= 1 
 COM[11,7]= cp_CharToDate("  -  -  ") 
 COM[11,8]=ah_Msgformat("Nessuno%1",space(73)) 
 COM[12,1]="PRIMANVQR " 
 COM[12,2]="DATANP         " 
 COM[12,3]= 2 
 COM[12,4]="N" 
 COM[12,5]="                                                  " 
 COM[12,6]= 2 
 COM[12,7]= cp_CharToDate("  -  -  ") 
 COM[12,8]=ah_Msgformat("Oper. non imponibili%1",space(60)) 
 COM[13,1]="PRIMANVQR " 
 COM[13,2]="DATANP         " 
 COM[13,3]= 3 
 COM[13,4]="N" 
 COM[13,5]="                                                  " 
 COM[13,6]= 3 
 COM[13,7]= cp_CharToDate("  -  -  ") 
 COM[13,8]=ah_Msgformat("Operazioni esenti%1",space(63)) 
 COM[14,1]="PRIMANVQR " 
 COM[14,2]="DATANP         " 
 COM[14,3]= 4 
 COM[14,4]="N" 
 COM[14,5]="                                                  " 
 COM[14,6]= 4 
 COM[14,7]= cp_CharToDate("  -  -  ") 
 COM[14,8]=ah_Msgformat("Acquisti INTRA di beni%1", space(58)) 
 COM[15,1]="PRIMANVQR " 
 COM[15,2]="DATANP         " 
 COM[15,3]= 5 
 COM[15,4]="N" 
 COM[15,5]="                                                  " 
 COM[15,6]= 5 
 COM[15,7]= cp_CharToDate("  -  -  ") 
 COM[15,8]=ah_Msgformat("Acquisti INTRA di servizi%1", space(55)) 
 COM[16,1]="PRIMANVQR " 
 COM[16,2]="DATANP         " 
 COM[16,3]= 6 
 COM[16,4]="N" 
 COM[16,5]="                                                  " 
 COM[16,6]= 6 
 COM[16,7]= cp_CharToDate("  -  -  ") 
 COM[16,8]=ah_Msgformat("Imp. di oro e argen.%1",space(60)) 
 COM[17,1]="PRIMANVQR " 
 COM[17,2]="DATANP         " 
 COM[17,3]= 7 
 COM[17,4]="N" 
 COM[17,5]="                                                  " 
 COM[17,6]= 7 
 COM[17,7]= cp_CharToDate("  -  -  ") 
 COM[17,8]=ah_Msgformat("Imp. INTRA oro e arg.%1",space(59)) 
 COM[18,1]="PRIMANVQR " 
 COM[18,2]="DATANP         " 
 COM[18,3]= 8 
 COM[18,4]="N" 
 COM[18,5]="                                                  " 
 COM[18,6]= 8 
 COM[18,7]= cp_CharToDate("  -  -  ") 
 COM[18,8]=ah_Msgformat("Imp. rottami/mat. rec.%1",space(58)) 
 COM[19,1]="PRIMANVQR " 
 COM[19,2]="DATANP         " 
 COM[19,3]= 9 
 COM[19,4]="N" 
 COM[19,5]="                                                  " 
 COM[19,6]= 9 
 COM[19,7]= cp_CharToDate("  -  -  ") 
 COM[19,8]=ah_Msgformat("Imp. INTRA rottami/mat. rec.%1",space(52)) 
 COM[20,1]="PRIMANVQR " 
 COM[20,2]="FLVAFF         " 
 COM[20,3]= 1 
 COM[20,4]="C" 
 COM[20,5]="E                                                 " 
 COM[20,6]= 0 
 COM[20,7]= cp_CharToDate("  -  -  ") 
 COM[20,8]=ah_Msgformat("Escluso%1",space(73)) 
 COM[21,1]="PRIMANVQR " 
 COM[21,2]="FLVAFF         " 
 COM[21,3]= 2 
 COM[21,4]="C" 
 COM[21,5]="V                                                 " 
 COM[21,6]= 0 
 COM[21,7]= cp_CharToDate("  -  -  ") 
 COM[21,8]=ah_Msgformat("Escluso V.A.%1",space(68)) 
 COM[22,1]="PRIMANVQR " 
 COM[22,2]="FLVAFF         " 
 COM[22,3]= 3 
 COM[22,4]="C" 
 COM[22,5]="N                                                 " 
 COM[22,6]= 0 
 COM[22,7]= cp_CharToDate("  -  -  ") 
 COM[22,8]=ah_Msgformat("Normale%1",space(73)) 
 COM[23,1]="PRIMANVQR " 
 COM[23,2]="IMPEXP         " 
 COM[23,3]= 1 
 COM[23,4]="C" 
 COM[23,5]="S                                                 " 
 COM[23,6]= 0 
 COM[23,7]= cp_CharToDate("  -  -  ") 
 COM[23,8]=ah_Msgformat("Esportazioni S%1",space(66)) 
 COM[24,1]="PRIMANVQR " 
 COM[24,2]="IMPEXP         " 
 COM[24,3]= 2 
 COM[24,4]="C" 
 COM[24,5]="N                                                 " 
 COM[24,6]= 0 
 COM[24,7]= cp_CharToDate("  -  -  ") 
 COM[24,8]=ah_Msgformat("Esportazioni N%1",space(66)) 
 COM[25,1]="PRIMANVQR " 
 COM[25,2]="MACIVA         " 
 COM[25,3]= 1 
 COM[25,4]="C" 
 COM[25,5]="S                                                 " 
 COM[25,6]= 0 
 COM[25,7]= cp_CharToDate("  -  -  ") 
 COM[25,8]=ah_Msgformat("Monte acquisti S%1",space(64)) 
 COM[26,1]="PRIMANVQR " 
 COM[26,2]="MACIVA         " 
 COM[26,3]= 2 
 COM[26,4]="C" 
 COM[26,5]="N                                                 " 
 COM[26,6]= 0 
 COM[26,7]= cp_CharToDate("  -  -  ") 
 COM[26,8]=ah_Msgformat("Monte acquisti N%1",space(64)) 
 COM[27,1]="PRIMANVQR " 
 COM[27,2]="PLAIVA         " 
 COM[27,3]= 1 
 COM[27,4]="C" 
 COM[27,5]="S                                                 " 
 COM[27,6]= 0 
 COM[27,7]= cp_CharToDate("  -  -  ") 
 COM[27,8]=ah_Msgformat("Acquisti PLAFOND%1",space(64)) 
 COM[28,1]="PRIMANVQR " 
 COM[28,2]="PLAIVA         " 
 COM[28,3]= 2 
 COM[28,4]="C" 
 COM[28,5]="N                                                 " 
 COM[28,6]= 0 
 COM[28,7]= cp_CharToDate("  -  -  ") 
 COM[28,8]=ah_Msgformat("Acquisti PLAFOND%1",space(64)) 
 COM[29,1]="PRIMANVQR " 
 COM[29,2]="PROIVA         " 
 COM[29,3]= 1 
 COM[29,4]="C" 
 COM[29,5]="S                                                 " 
 COM[29,6]= 0 
 COM[29,7]= cp_CharToDate("  -  -  ") 
 COM[29,8]=ah_Msgformat("Op. esente E partecipa al calcolo%1",space(47)) 
 COM[30,1]="PRIMANVQR " 
 COM[30,2]="PROIVA         " 
 COM[30,3]= 2 
 COM[30,4]="C" 
 COM[30,5]="N                                                 " 
 COM[30,6]= 0 
 COM[30,7]= cp_CharToDate("  -  -  ") 
 COM[30,8]=ah_Msgformat("Non partecipa%1",space(67)) 
 COM[31,1]="PRIMANVQR " 
 COM[31,2]="REVCHA         " 
 COM[31,3]= 1 
 COM[31,4]="C" 
 COM[31,5]="S                                                 " 
 COM[31,6]= 0 
 COM[31,7]= cp_CharToDate("  -  -  ") 
 COM[31,8]=ah_Msgformat("Reverse Charge S%1",space(64)) 
 COM[32,1]="PRIMANVQR " 
 COM[32,2]="REVCHA         " 
 COM[32,3]= 2 
 COM[32,4]="C" 
 COM[32,5]="N                                                 " 
 COM[32,6]= 0 
 COM[32,7]= cp_CharToDate("  -  -  ") 
 COM[32,8]=ah_Msgformat("Reverse Charge N%1",space(64)) 
 COM[33,1]="PRIMANVQR " 
 COM[33,2]="TIPAGR         " 
 COM[33,3]= 1 
 COM[33,4]="C" 
 COM[33,5]="N                                                 " 
 COM[33,6]= 0 
 COM[33,7]= cp_CharToDate("  -  -  ") 
 COM[33,8]=ah_Msgformat("Normale%1",space(73)) 
 COM[34,1]="PRIMANVQR " 
 COM[34,2]="TIPAGR         " 
 COM[34,3]= 2 
 COM[34,4]="C" 
 COM[34,5]="A                                                 " 
 COM[34,6]= 0 
 COM[34,7]= cp_CharToDate("  -  -  ") 
 COM[34,8]=ah_Msgformat("Agevolata%1",space(71)) 
 COM[35,1]="PRIMANVQR " 
 COM[35,2]="TIPBEN         " 
 COM[35,3]= 1 
 COM[35,4]="C" 
 COM[35,5]="R                                                 " 
 COM[35,6]= 0 
 COM[35,7]= cp_CharToDate("  -  -  ") 
 COM[35,8]=ah_Msgformat("Beni destinati alla rivendita%1",space(51)) 
 COM[36,1]="PRIMANVQR " 
 COM[36,2]="TIPBEN         " 
 COM[36,3]= 2 
 COM[36,4]="C" 
 COM[36,5]="A                                                 " 
 COM[36,6]= 0 
 COM[36,7]= cp_CharToDate("  -  -  ") 
 COM[36,8]=ah_Msgformat("Beni ammortizzabili%1",space(61)) 
 COM[37,1]="PRIMANVQR " 
 COM[37,2]="TIPBEN         " 
 COM[37,3]= 3 
 COM[37,4]="C" 
 COM[37,5]="S                                                 " 
 COM[37,6]= 0 
 COM[37,7]= cp_CharToDate("  -  -  ") 
 COM[37,8]=ah_Msgformat("Beni strumentali non ammortizzabili%1",space(45)) 
 COM[38,1]="PRIMANVQR " 
 COM[38,2]="TIPBEN         " 
 COM[38,3]= 4 
 COM[38,4]="C" 
 COM[38,5]="N                                                 " 
 COM[38,6]= 0 
 COM[38,7]= cp_CharToDate("  -  -  ") 
 COM[38,8]=ah_Msgformat("Nessun cumulo%1",space(67)) 
 COM[39,1]="PRIMANVQR " 
 COM[39,2]="TIPPLA         " 
 COM[39,3]= 1 
 COM[39,4]="C" 
 COM[39,5]="A                                                 " 
 COM[39,6]= 0 
 COM[39,7]= cp_CharToDate("  -  -  ") 
 COM[39,8]=ah_Msgformat("Acquisti interni e INTRA%1",space(56)) 
 COM[40,1]="PRIMANVQR " 
 COM[40,2]="TIPPLA         " 
 COM[40,3]= 2 
 COM[40,4]="C" 
 COM[40,5]="T                                                 " 
 COM[40,6]= 0 
 COM[40,7]= cp_CharToDate("  -  -  ") 
 COM[40,8]=ah_Msgformat("Importazioni%1",space(68)) 
 COM[41,1]="PRIMANVQR " 
 COM[41,2]="TIPREG         " 
 COM[41,3]= 1 
 COM[41,4]="C" 
 COM[41,5]="V                                                 " 
 COM[41,6]= 0 
 COM[41,7]= cp_CharToDate("  -  -  ") 
 COM[41,8]=ah_Msgformat("Vendite%1",space(73)) 
 COM[42,1]="PRIMANVQR " 
 COM[42,2]="TIPREG         " 
 COM[42,3]= 2 
 COM[42,4]="C" 
 COM[42,5]="A                                                 " 
 COM[42,6]= 0 
 COM[42,7]= cp_CharToDate("  -  -  ") 
 COM[42,8]=ah_Msgformat("Acquisti%1",space(72)) 
 COM[43,1]="PRIMANVQR " 
 COM[43,2]="TIPREG         " 
 COM[43,3]= 3 
 COM[43,4]="C" 
 COM[43,5]="C                                                 " 
 COM[43,6]= 0 
 COM[43,7]= cp_CharToDate("  -  -  ") 
 COM[43,8]=Ah_Msgformat("Corrispettivi%1",space(67)) 
 COM[44,1]="PRIMANVQR " 
 COM[44,2]="TIPREG         " 
 COM[44,3]= 4 
 COM[44,4]="C" 
 COM[44,5]="E                                                 " 
 COM[44,6]= 0 
 COM[44,7]= cp_CharToDate("  -  -  ") 
 COM[44,8]=ah_Msgformat("Ventilazione%1",space(68)) 
 COM[45,1]="SALDIIVA " 
 COM[45,2]="ACQINT         " 
 COM[45,3]= 3 
 COM[45,4]="C" 
 COM[45,5]="I                                                 " 
 COM[45,6]= 0 
 COM[45,7]= cp_CharToDate("  -  -  ") 
 COM[45,8]=ah_Msgformat("Importazioni oro%1",space(64)) 
 COM[46,1]="SALDIIVA " 
 COM[46,2]="ACQINT         " 
 COM[46,3]= 1 
 COM[46,4]="C" 
 COM[46,5]="N                                                 " 
 COM[46,6]= 0 
 COM[46,7]= cp_CharToDate("  -  -  ") 
 COM[46,8]=ah_Msgformat("Acquisti INTRA%1",space(66)) 
 COM[47,1]="SALDIIVA " 
 COM[47,2]="ACQINT         " 
 COM[47,3]= 2 
 COM[47,4]="C" 
 COM[47,5]="C                                                 " 
 COM[47,6]= 0 
 COM[47,7]= cp_CharToDate("  -  -  ") 
 COM[47,8]=ah_Msgformat("Cessioni INTRA%1",space(66)) 
 COM[48,1]="SALDIIVA " 
 COM[48,2]="ACQINT         " 
 COM[48,3]= 4 
 COM[48,4]="C" 
 COM[48,5]="O                                                 " 
 COM[48,6]= 0 
 COM[48,7]= cp_CharToDate("  -  -  ") 
 COM[48,8]=ah_Msgformat("Acq.+ imp. oro%1",space(66)) 
 COM[49,1]="SALDIIVA " 
 COM[49,2]="BOLIVA         " 
 COM[49,3]= 1 
 COM[49,4]="C" 
 COM[49,5]="S                                                 " 
 COM[49,6]= 0 
 COM[49,7]= cp_CharToDate("  -  -  ") 
 COM[49,8]=ah_Msgformat("Bollo su importi esenti S%1",space(55)) 
 COM[50,1]="SALDIIVA " 
 COM[50,2]="BOLIVA         " 
 COM[50,3]= 2 
 COM[50,4]="C" 
 COM[50,5]="N                                                 " 
 COM[50,6]= 0 
 COM[50,7]= cp_CharToDate("  -  -  ") 
 COM[50,8]=ah_Msgformat("Bollo su importi esenti N%1",space(55)) 
 COM[51,1]="SALDIIVA " 
 COM[51,2]="DATANA         " 
 COM[51,3]= 1 
 COM[51,4]="N" 
 COM[51,5]="                                                  " 
 COM[51,6]= 1 
 COM[51,7]= cp_CharToDate("  -  -  ") 
 COM[51,8]=ah_Msgformat("Nessuno%1",space(73)) 
 COM[52,1]="SALDIIVA " 
 COM[52,2]="DATANA         " 
 COM[52,3]= 2 
 COM[52,4]="N" 
 COM[52,5]="                                                  " 
 COM[52,6]= 2 
 COM[52,7]= cp_CharToDate("  -  -  ") 
 COM[52,8]=ah_Msgformat("Oper. non imponibili%1",space(60)) 
 COM[53,1]="SALDIIVA " 
 COM[53,2]="DATANA         " 
 COM[53,3]= 3 
 COM[53,4]="N" 
 COM[53,5]="                                                  " 
 COM[53,6]= 3 
 COM[53,7]= cp_CharToDate("  -  -  ") 
 COM[53,8]=ah_Msgformat("Operazioni esenti%1",space(63)) 
 COM[54,1]="SALDIIVA " 
 COM[54,2]="DATANA         " 
 COM[54,3]= 4 
 COM[54,4]="N" 
 COM[54,5]="                                                  " 
 COM[54,6]= 4 
 COM[54,7]= cp_CharToDate("  -  -  ") 
 COM[54,8]=ah_Msgformat("Cessioni intracom. beni%1",space(59)) 
 COM[55,1]="SALDIIVA " 
 COM[55,2]="DATANP         " 
 COM[55,3]= 1 
 COM[55,4]="N" 
 COM[55,5]="                                                  " 
 COM[55,6]= 1 
 COM[55,7]= cp_CharToDate("  -  -  ") 
 COM[55,8]=ah_Msgformat("Nessuno%1",space(73)) 
 COM[56,1]="SALDIIVA " 
 COM[56,2]="DATANP         " 
 COM[56,3]= 2 
 COM[56,4]="N" 
 COM[56,5]="                                                  " 
 COM[56,6]= 2 
 COM[56,7]= cp_CharToDate("  -  -  ") 
 COM[56,8]=ah_Msgformat("Oper. non imponibili%1",space(60)) 
 COM[57,1]="SALDIIVA " 
 COM[57,2]="DATANP         " 
 COM[57,3]= 3 
 COM[57,4]="N" 
 COM[57,5]="                                                  " 
 COM[57,6]= 3 
 COM[57,7]= cp_CharToDate("  -  -  ") 
 COM[57,8]=ah_Msgformat("Operazioni esenti%1",space(63)) 
 COM[58,1]="SALDIIVA " 
 COM[58,2]="DATANP         " 
 COM[58,3]= 4 
 COM[58,4]="N" 
 COM[58,5]="                                                  " 
 COM[58,6]= 4 
 COM[58,7]= cp_CharToDate("  -  -  ") 
 COM[58,8]=ah_Msgformat("Acquisti INTRA di beni%1", space(58)) 
 COM[59,1]="SALDIIVA " 
 COM[59,2]="DATANP         " 
 COM[59,3]= 5 
 COM[59,4]="N" 
 COM[59,5]="                                                  " 
 COM[59,6]= 5 
 COM[59,7]= cp_CharToDate("  -  -  ") 
 COM[59,8]=ah_Msgformat("Acquisti INTRA di servizi%1", space(55)) 
 COM[60,1]="SALDIIVA " 
 COM[60,2]="DATANP         " 
 COM[60,3]= 6 
 COM[60,4]="N" 
 COM[60,5]="                                                  " 
 COM[60,6]= 6 
 COM[60,7]= cp_CharToDate("  -  -  ") 
 COM[60,8]=ah_Msgformat("Imp. di oro e argen.%1",space(60)) 
 COM[61,1]="SALDIIVA " 
 COM[61,2]="DATANP         " 
 COM[61,3]= 7 
 COM[61,4]="N" 
 COM[61,5]="                                                  " 
 COM[61,6]= 7 
 COM[61,7]= cp_CharToDate("  -  -  ") 
 COM[61,8]=ah_Msgformat("Imp. INTRA oro e arg.%1",space(59)) 
 COM[62,1]="SALDIIVA " 
 COM[62,2]="DATANP         " 
 COM[62,3]= 8 
 COM[62,4]="N" 
 COM[62,5]="                                                  " 
 COM[62,6]= 8 
 COM[62,7]= cp_CharToDate("  -  -  ") 
 COM[62,8]=ah_Msgformat("Imp. rottami/mat. rec.%1",space(58)) 
 COM[63,1]="SALDIIVA " 
 COM[63,2]="DATANP         " 
 COM[63,3]= 9 
 COM[63,4]="N" 
 COM[63,5]="                                                  " 
 COM[63,6]= 9 
 COM[63,7]= cp_CharToDate("  -  -  ") 
 COM[63,8]=ah_Msgformat("Imp. INTRA rottami/mat. rec.%1",space(52)) 
 COM[64,1]="SALDIIVA " 
 COM[64,2]="FLVAFF         " 
 COM[64,3]= 1 
 COM[64,4]="C" 
 COM[64,5]="E                                                 " 
 COM[64,6]= 0 
 COM[64,7]= cp_CharToDate("  -  -  ") 
 COM[64,8]=ah_Msgformat("Escluso%1",space(73)) 
 COM[65,1]="SALDIIVA " 
 COM[65,2]="FLVAFF         " 
 COM[65,3]= 2 
 COM[65,4]="C" 
 COM[65,5]="V                                                 " 
 COM[65,6]= 0 
 COM[65,7]= cp_CharToDate("  -  -  ") 
 COM[65,8]=ah_Msgformat("Escluso V.A.%1",space(68)) 
 COM[66,1]="SALDIIVA " 
 COM[66,2]="FLVAFF         " 
 COM[66,3]= 3 
 COM[66,4]="C" 
 COM[66,5]="N                                                 " 
 COM[66,6]= 0 
 COM[66,7]= cp_CharToDate("  -  -  ") 
 COM[66,8]=ah_Msgformat("Normale%1",space(73)) 
 COM[67,1]="SALDIIVA " 
 COM[67,2]="IMPEXP         " 
 COM[67,3]= 1 
 COM[67,4]="C" 
 COM[67,5]="S                                                 " 
 COM[67,6]= 0 
 COM[67,7]= cp_CharToDate("  -  -  ") 
 COM[67,8]=ah_Msgformat("Esportazioni S%1",space(66)) 
 COM[68,1]="SALDIIVA " 
 COM[68,2]="IMPEXP         " 
 COM[68,3]= 2 
 COM[68,4]="C" 
 COM[68,5]="N                                                 " 
 COM[68,6]= 0 
 COM[68,7]= cp_CharToDate("  -  -  ") 
 COM[68,8]=ah_Msgformat("Esportazioni N%1",space(66)) 
 COM[69,1]="SALDIIVA " 
 COM[69,2]="MACIVA         " 
 COM[69,3]= 1 
 COM[69,4]="C" 
 COM[69,5]="S                                                 " 
 COM[69,6]= 0 
 COM[69,7]= cp_CharToDate("  -  -  ") 
 COM[69,8]=ah_Msgformat("Monte acquisti S%1",space(64)) 
 COM[70,1]="SALDIIVA " 
 COM[70,2]="MACIVA         " 
 COM[70,3]= 2 
 COM[70,4]="C" 
 COM[70,5]="N                                                 " 
 COM[70,6]= 0 
 COM[70,7]= cp_CharToDate("  -  -  ") 
 COM[70,8]=ah_Msgformat("Monte acquisti N%1",space(64)) 
 COM[71,1]="SALDIIVA " 
 COM[71,2]="PLAIVA         " 
 COM[71,3]= 1 
 COM[71,4]="C" 
 COM[71,5]="S                                                 " 
 COM[71,6]= 0 
 COM[71,7]= cp_CharToDate("  -  -  ") 
 COM[71,8]=ah_Msgformat("Acquisti PLAFOND%1",space(64)) 
 COM[72,1]="SALDIIVA " 
 COM[72,2]="PLAIVA         " 
 COM[72,3]= 2 
 COM[72,4]="C" 
 COM[72,5]="N                                                 " 
 COM[72,6]= 0 
 COM[72,7]= cp_CharToDate("  -  -  ") 
 COM[72,8]=ah_Msgformat("Acquisti PLAFOND%1",space(64)) 
 COM[73,1]="SALDIIVA " 
 COM[73,2]="PROIVA         " 
 COM[73,3]= 1 
 COM[73,4]="C" 
 COM[73,5]="S                                                 " 
 COM[73,6]= 0 
 COM[73,7]= cp_CharToDate("  -  -  ") 
 COM[73,8]=ah_Msgformat("Op. esente E partecipa al calcolo%1",space(47)) 
 COM[74,1]="SALDIIVA " 
 COM[74,2]="PROIVA         " 
 COM[74,3]= 2 
 COM[74,4]="C" 
 COM[74,5]="N                                                 " 
 COM[74,6]= 0 
 COM[74,7]= cp_CharToDate("  -  -  ") 
 COM[74,8]=ah_Msgformat("Non partecipa%1",space(67)) 
 COM[75,1]="SALDIIVA " 
 COM[75,2]="REVCHA         " 
 COM[75,3]= 1 
 COM[75,4]="C" 
 COM[75,5]="S                                                 " 
 COM[75,6]= 0 
 COM[75,7]= cp_CharToDate("  -  -  ") 
 COM[75,8]=ah_Msgformat("Reverse Charge S%1",space(64)) 
 COM[76,1]="SALDIIVA " 
 COM[76,2]="REVCHA         " 
 COM[76,3]= 2 
 COM[76,4]="C" 
 COM[76,5]="N                                                 " 
 COM[76,6]= 0 
 COM[76,7]= cp_CharToDate("  -  -  ") 
 COM[76,8]=ah_Msgformat("Reverse Charge N%1",space(64)) 
 COM[77,1]="SALDIIVA " 
 COM[77,2]="TIPAGR         " 
 COM[77,3]= 1 
 COM[77,4]="C" 
 COM[77,5]="N                                                 " 
 COM[77,6]= 0 
 COM[77,7]= cp_CharToDate("  -  -  ") 
 COM[77,8]=ah_Msgformat("Normale%1",space(73)) 
 COM[78,1]="SALDIIVA " 
 COM[78,2]="TIPAGR         " 
 COM[78,3]= 2 
 COM[78,4]="C" 
 COM[78,5]="A                                                 " 
 COM[78,6]= 0 
 COM[78,7]= cp_CharToDate("  -  -  ") 
 COM[78,8]=ah_Msgformat("Agevolata%1",space(71)) 
 COM[79,1]="SALDIIVA " 
 COM[79,2]="TIPBEN         " 
 COM[79,3]= 1 
 COM[79,4]="C" 
 COM[79,5]="R                                                 " 
 COM[79,6]= 0 
 COM[79,7]= cp_CharToDate("  -  -  ") 
 COM[79,8]=ah_Msgformat("Beni destinati alla rivendita%1",space(51)) 
 COM[80,1]="SALDIIVA " 
 COM[80,2]="TIPBEN         " 
 COM[80,3]= 2 
 COM[80,4]="C" 
 COM[80,5]="A                                                 " 
 COM[80,6]= 0 
 COM[80,7]= cp_CharToDate("  -  -  ") 
 COM[80,8]=ah_Msgformat("Beni ammortizzabili%1",space(61)) 
 COM[81,1]="SALDIIVA " 
 COM[81,2]="TIPBEN         " 
 COM[81,3]= 3 
 COM[81,4]="C" 
 COM[81,5]="S                                                 " 
 COM[81,6]= 0 
 COM[81,7]= cp_CharToDate("  -  -  ") 
 COM[81,8]=ah_Msgformat("Beni strumentali non ammortizzabili%1",space(45)) 
 COM[82,1]="SALDIIVA " 
 COM[82,2]="TIPBEN         " 
 COM[82,3]= 4 
 COM[82,4]="C" 
 COM[82,5]="N                                                 " 
 COM[82,6]= 0 
 COM[82,7]= cp_CharToDate("  -  -  ") 
 COM[82,8]=ah_Msgformat("Nessun cumulo%1",space(67)) 
 COM[83,1]="SALDIIVA " 
 COM[83,2]="TIPPLA         " 
 COM[83,3]= 1 
 COM[83,4]="C" 
 COM[83,5]="A                                                 " 
 COM[83,6]= 0 
 COM[83,7]= cp_CharToDate("  -  -  ") 
 COM[83,8]=ah_Msgformat("Acquisti interni e INTRA%1",space(56)) 
 COM[84,1]="SALDIIVA " 
 COM[84,2]="TIPPLA         " 
 COM[84,3]= 2 
 COM[84,4]="C" 
 COM[84,5]="T                                                 " 
 COM[84,6]= 0 
 COM[84,7]= cp_CharToDate("  -  -  ") 
 COM[84,8]=ah_Msgformat("Importazioni%1",space(68)) 
 COM[85,1]="SALDIIVA  " 
 COM[85,2]="TRTIPREG       " 
 COM[85,3]= 1 
 COM[85,4]="C" 
 COM[85,5]="V                                                 " 
 COM[85,6]= 0 
 COM[85,7]= cp_CharToDate("  -  -  ") 
 COM[85,8]=ah_Msgformat("Vendite%1",space(73)) 
 COM[86,1]="SALDIIVA  " 
 COM[86,2]="TRTIPREG       " 
 COM[86,3]= 2 
 COM[86,4]="C" 
 COM[86,5]="A                                                 " 
 COM[86,6]= 0 
 COM[86,7]= cp_CharToDate("  -  -  ") 
 COM[86,8]=ah_Msgformat("Acquisti%1",space(72)) 
 COM[87,1]="SALDIIVA  " 
 COM[87,2]="TRTIPREG       " 
 COM[87,3]= 3 
 COM[87,4]="C" 
 COM[87,5]="C                                                 " 
 COM[87,6]= 0 
 COM[87,7]= cp_CharToDate("  -  -  ") 
 COM[87,8]=Ah_Msgformat("Corrispettivi%1",space(67)) 
 COM[88,1]="SALDIIVA  " 
 COM[88,2]="TRTIPREG       " 
 COM[88,3]= 4 
 COM[88,4]="C" 
 COM[88,5]="E                                                 " 
 COM[88,6]= 0 
 COM[88,7]= cp_CharToDate("  -  -  ") 
 COM[88,8]=ah_Msgformat("Ventilazione%1",space(68)) 
 COM[89,1]="PRIMANOTA " 
 COM[89,2]="IVTIPREG       " 
 COM[89,3]= 1 
 COM[89,4]="C" 
 COM[89,5]="V                                                 " 
 COM[89,6]= 0 
 COM[89,7]= cp_CharToDate("  -  -  ") 
 COM[89,8]=ah_Msgformat("Vendite%1",space(73)) 
 COM[90,1]="PRIMANOTA " 
 COM[90,2]="IVTIPREG       " 
 COM[90,3]= 2 
 COM[90,4]="C" 
 COM[90,5]="A                                                 " 
 COM[90,6]= 0 
 COM[90,7]= cp_CharToDate("  -  -  ") 
 COM[90,8]=ah_Msgformat("Acquisti%1",space(72)) 
 COM[91,1]="PRIMANOTA " 
 COM[91,2]="IVTIPREG       " 
 COM[91,3]= 3 
 COM[91,4]="C" 
 COM[91,5]="C                                                 " 
 COM[91,6]= 0 
 COM[91,7]= cp_CharToDate("  -  -  ") 
 COM[91,8]=Ah_Msgformat("Corrispettivi%1",space(67)) 
 COM[92,1]="PRIMANOTA " 
 COM[92,2]="IVTIPREG       " 
 COM[92,3]= 4 
 COM[92,4]="C" 
 COM[92,5]="E                                                 " 
 COM[92,6]= 0 
 COM[92,7]= cp_CharToDate("  -  -  ") 
 COM[92,8]=ah_Msgformat("Ventilazione%1",space(68)) 
 COM[93,1]="PRIMANVQR " 
 COM[93,2]="DATANA         " 
 COM[93,3]= 5 
 COM[93,4]="N" 
 COM[93,5]="                                                  " 
 COM[93,6]= 5 
 COM[93,7]= cp_CharToDate("  -  -  ") 
 COM[93,8]=ah_Msgformat("Cessioni intracom. servizi%1",space(54)) 
 COM[94,1]="SALDIIVA " 
 COM[94,2]="DATANA         " 
 COM[94,3]= 5 
 COM[94,4]="N" 
 COM[94,5]="                                                  " 
 COM[94,6]= 5 
 COM[94,7]= cp_CharToDate("  -  -  ") 
 COM[94,8]=ah_Msgformat("Cessioni intracom. servizi%1",space(54)) 
 COM[95,1]="PERIODICA " 
 COM[95,2]="VPDICSOC    " 
 COM[95,3]= 1 
 COM[95,4]="C" 
 COM[95,5]="S                                                 " 
 COM[95,6]= 0 
 COM[95,7]= cp_CharToDate("  -  -  ") 
 COM[95,8]=ah_Msgformat("Societ� del gruppo S%1",space(57)) 
 COM[96,1]="PERIODICA " 
 COM[96,2]="VPDICSOC    " 
 COM[96,3]= 2 
 COM[96,4]="C" 
 COM[96,5]="N                                                 " 
 COM[96,6]= 0 
 COM[96,7]= cp_CharToDate("  -  -  ") 
 COM[96,8]=ah_Msgformat("Societ� del gruppo N%1",space(57)) 
      For i =1 to 96
      this.w_DESCRI = left(COM[I,8],80)
      * --- Insert into DETCOMBO
      i_nConn=i_TableProp[this.DETCOMBO_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DETCOMBO_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DETCOMBO_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"DC_FONTE"+",DCCODDIS"+",CPROWNUM"+",DCTIPCOM"+",DCVALCHR"+",DCVALNUM"+",DCVALDAT"+",DCDESCRI"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(COM[I,1]),'DETCOMBO','DC_FONTE');
        +","+cp_NullLink(cp_ToStrODBC(COM[I,2]),'DETCOMBO','DCCODDIS');
        +","+cp_NullLink(cp_ToStrODBC(COM[I,3]),'DETCOMBO','CPROWNUM');
        +","+cp_NullLink(cp_ToStrODBC(COM[I,4]),'DETCOMBO','DCTIPCOM');
        +","+cp_NullLink(cp_ToStrODBC(COM[I,5]),'DETCOMBO','DCVALCHR');
        +","+cp_NullLink(cp_ToStrODBC(COM[I,6]),'DETCOMBO','DCVALNUM');
        +","+cp_NullLink(cp_ToStrODBC(COM[I,7]),'DETCOMBO','DCVALDAT');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DESCRI),'DETCOMBO','DCDESCRI');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'DC_FONTE',COM[I,1],'DCCODDIS',COM[I,2],'CPROWNUM',COM[I,3],'DCTIPCOM',COM[I,4],'DCVALCHR',COM[I,5],'DCVALNUM',COM[I,6],'DCVALDAT',COM[I,7],'DCDESCRI',this.w_DESCRI)
        insert into (i_cTable) (DC_FONTE,DCCODDIS,CPROWNUM,DCTIPCOM,DCVALCHR,DCVALNUM,DCVALDAT,DCDESCRI &i_ccchkf. );
           values (;
             COM[I,1];
             ,COM[I,2];
             ,COM[I,3];
             ,COM[I,4];
             ,COM[I,5];
             ,COM[I,6];
             ,COM[I,7];
             ,this.w_DESCRI;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error='Errore inserimento dettaglio combo'
        return
      endif
      endfor
      * --- Esecuzione ok
      this.oParentObject.w_PESEOK = .T.
      this.w_TMPC = ah_Msgformat("Inserimento tabelle fonti, dimensioni misure eseguito correttamente%0")
    else
      * --- Esecuzione ok
      this.oParentObject.w_PESEOK = .T.
      this.w_TMPC = ah_Msgformat("Inserimento tabelle fonti, dimensioni misure eseguito gi� in altre aziende%0")
    endif
    this.oParentObject.w_PMSG = this.w_TMPC
    if this.w_NHF>=0 
      this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='TAB_FONT'
    this.cWorkTables[2]='TAB_MISU'
    this.cWorkTables[3]='TAB_DIME'
    this.cWorkTables[4]='DETCOMBO'
    return(this.OpenAllTables(4))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
