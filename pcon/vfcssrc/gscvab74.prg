* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvab74                                                        *
*              Magazzino di riga nel pos                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_7]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-03-31                                                      *
* Last revis.: 2005-03-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvab74",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvab74 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_MESS = space(100)
  * --- WorkFile variables
  CORDRISP_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questa procedura riempie il campo MDMAGRIG (magazzino di riga) del dettaglio 
    *     vendita negozio con il codice magazzino di testata
    * --- FIle di LOG
    * --- Try
    local bErr_04A936F0
    bErr_04A936F0=bTrsErr
    this.Try_04A936F0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE GENERICO - impossibile aggiornare il dettaglio vendite negozio")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_04A936F0
    * --- End
  endproc
  proc Try_04A936F0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Write into CORDRISP
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.CORDRISP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CORDRISP_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="MDSERIAL"
      do vq_exec with 'GSCVAB74',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.CORDRISP_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="CORDRISP.MDSERIAL = _t2.MDSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MDMAGRIG = _t2.CODMAG";
      +",MDNUMRIF ="+cp_NullLink(cp_ToStrODBC(-30),'CORDRISP','MDNUMRIF');
          +i_ccchkf;
          +" from "+i_cTable+" CORDRISP, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="CORDRISP.MDSERIAL = _t2.MDSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CORDRISP, "+i_cQueryTable+" _t2 set ";
          +"CORDRISP.MDMAGRIG = _t2.CODMAG";
      +",CORDRISP.MDNUMRIF ="+cp_NullLink(cp_ToStrODBC(-30),'CORDRISP','MDNUMRIF');
          +Iif(Empty(i_ccchkf),"",",CORDRISP.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="CORDRISP.MDSERIAL = t2.MDSERIAL";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CORDRISP set (";
          +"MDMAGRIG,";
          +"MDNUMRIF";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.CODMAG,";
          +cp_NullLink(cp_ToStrODBC(-30),'CORDRISP','MDNUMRIF')+"";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="CORDRISP.MDSERIAL = _t2.MDSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CORDRISP set ";
          +"MDMAGRIG = _t2.CODMAG";
      +",MDNUMRIF ="+cp_NullLink(cp_ToStrODBC(-30),'CORDRISP','MDNUMRIF');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".MDSERIAL = "+i_cQueryTable+".MDSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MDMAGRIG = (select CODMAG from "+i_cQueryTable+" where "+i_cWhere+")";
      +",MDNUMRIF ="+cp_NullLink(cp_ToStrODBC(-30),'CORDRISP','MDNUMRIF');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    if bTrsErr
      * --- Raise
      i_Error=MESSAGE()
      return
    else
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("Elaborazione terminata con successo")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='CORDRISP'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
