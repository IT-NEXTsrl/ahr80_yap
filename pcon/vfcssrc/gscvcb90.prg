* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcb90                                                        *
*              Aggiorna valore campo manutenzione elenchi INTRA                *
*                                                                              *
*      Author: Zucchetti S.p.A.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-11-30                                                      *
* Last revis.: 2011-03-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcb90",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcb90 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_CODESE = space(4)
  * --- WorkFile variables
  PNT_DETT_idx=0
  RIPATMP1_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna PNFLSAD in registrazioni provvisorie
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('GSCVCB90_1',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Try
    local bErr_04C85598
    bErr_04C85598=bTrsErr
    this.Try_04C85598()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("Errore generico - impossibile eseguire aggiornamento richiesto")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_04C85598
    * --- End
  endproc
  proc Try_04C85598()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Write into PNT_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PNT_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="PNSERIAL"
      do vq_exec with 'GSCVCB90',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.PNT_DETT_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="PNT_DETT.PNSERIAL = _t2.PNSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PNFLSALD ="+cp_NullLink(cp_ToStrODBC(" "),'PNT_DETT','PNFLSALD');
          +i_ccchkf;
          +" from "+i_cTable+" PNT_DETT, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="PNT_DETT.PNSERIAL = _t2.PNSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" PNT_DETT, "+i_cQueryTable+" _t2 set ";
      +"PNT_DETT.PNFLSALD ="+cp_NullLink(cp_ToStrODBC(" "),'PNT_DETT','PNFLSALD');
          +Iif(Empty(i_ccchkf),"",",PNT_DETT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="PNT_DETT.PNSERIAL = t2.PNSERIAL";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" PNT_DETT set (";
          +"PNFLSALD";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +cp_NullLink(cp_ToStrODBC(" "),'PNT_DETT','PNFLSALD')+"";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="PNT_DETT.PNSERIAL = _t2.PNSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" PNT_DETT set ";
      +"PNFLSALD ="+cp_NullLink(cp_ToStrODBC(" "),'PNT_DETT','PNFLSALD');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".PNSERIAL = "+i_cQueryTable+".PNSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PNFLSALD ="+cp_NullLink(cp_ToStrODBC(" "),'PNT_DETT','PNFLSALD');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    this.w_TMPC = ah_Msgformat("Aggiornamento campo PNFLSALD eseguita con successo")
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = this.w_TMPC
    * --- commit
    cp_EndTrs(.t.)
    * --- Ricostruisco i saldi
    * --- Select from RIPATMP1
    i_nConn=i_TableProp[this.RIPATMP1_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" RIPATMP1 ";
           ,"_Curs_RIPATMP1")
    else
      select * from (i_cTable);
        into cursor _Curs_RIPATMP1
    endif
    if used('_Curs_RIPATMP1')
      select _Curs_RIPATMP1
      locate for 1=1
      do while not(eof())
      this.w_CODESE = _Curs_RIPATMP1.PNCODESE
      do GSCG_BRS with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
        select _Curs_RIPATMP1
        continue
      enddo
      use
    endif
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='PNT_DETT'
    this.cWorkTables[2]='*RIPATMP1'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_RIPATMP1')
      use in _Curs_RIPATMP1
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
