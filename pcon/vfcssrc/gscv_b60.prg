* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscv_b60                                                        *
*              Aggiorna U.M. documenti da produzione                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_55]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-01-17                                                      *
* Last revis.: 2003-01-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscv_b60",oParentObject,m.w_NHF)
return(i_retval)

define class tgscv_b60 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_MESS = space(100)
  w_ORIGIN = space(1)
  w_TIPDOC = space(15)
  w_NUMDOC = 0
  w_ALFDOC = space(10)
  w_DATDOC = space(2)
  w_SERRIF = space(15)
  w_NURIG = 0
  w_RIGRIF = 0
  w_CODICE = space(20)
  w_UNIMIS = space(3)
  w_UNMIS1 = space(3)
  w_UNMIS2 = space(3)
  w_UNMIS3 = space(3)
  w_OPER12 = space(1)
  w_OPER13 = space(1)
  w_MOLT12 = 0
  w_MOLT13 = 0
  w_QTAMOV = 0
  w_QTAUM1 = 0
  w_TEST12 = 0
  w_TEST13 = 0
  w_NEWUMI = space(3)
  * --- WorkFile variables
  DOC_DETT_idx=0
  ODL_DETT_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questa procedura Aggiusta il codice U.M. in caso di Documenti Generati da Materiali ODL/OCL
    * --- FIle di LOG
    * --- Try
    local bErr_035D0F00
    bErr_035D0F00=bTrsErr
    this.Try_035D0F00()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("ERRORE GENERICO - impossibile aggiornare documenti e/o materiali ODL-OCL")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_035D0F00
    * --- End
  endproc
  proc Try_035D0F00()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    VQ_EXEC("..\PCON\EXE\QUERY\GSCV_Q60.VQR", this, "ELABOR")
    if USED("ELABOR")
      if Reccount ("ELABOR")>0
        SELECT ELABOR
        GO TOP
        SCAN FOR NOT EMPTY(NVL(SERRIF, "")) AND NVL(ORIGIN, "Z") $ "DIL" AND NVL(RIGRIF, 0)<>0
        this.w_ORIGIN = NVL(ORIGIN, "D")
        this.w_TIPDOC = NVL(TIPDOC, SPACE(15))
        this.w_NUMDOC = NVL(NUMDOC, 0)
        this.w_ALFDOC = NVL(ALFDOC, Space(10))
        this.w_DATDOC = CP_TODATE(DATDOC)
        this.w_SERRIF = SERRIF
        this.w_NURIG = NVL(NURIG, 0)
        this.w_RIGRIF = RIGRIF
        this.w_CODICE = NVL(CODICE, SPACE(20))
        this.w_UNIMIS = NVL(UNIMIS, SPACE(3))
        this.w_UNMIS1 = NVL(UNMIS1, SPACE(3))
        this.w_UNMIS2 = NVL(UNMIS2, SPACE(3))
        this.w_UNMIS3 = NVL(UNMIS3, SPACE(3))
        this.w_OPER12 = NVL(OPER12, " ")
        this.w_OPER13 = NVL(OPER13, " ")
        this.w_MOLT12 = NVL(MOLT12, 0)
        this.w_MOLT13 = NVL(MOLT13, 0)
        this.w_QTAMOV = NVL(QTAMOV, 0)
        this.w_QTAUM1 = NVL(QTAUM1, 0)
        this.w_NEWUMI = this.w_UNIMIS
        * --- Calcola Il Moltiplicatore
        this.w_TEST13 = 0
        if NOT EMPTY(this.w_UNMIS3) AND this.w_OPER13 $ "/*" AND this.w_MOLT13<>0
          if this.w_OPER13="/"
            this.w_TEST13 = this.w_QTAMOV * this.w_MOLT13
          else
            this.w_TEST13 = this.w_QTAMOV / this.w_MOLT13
          endif
          if VAL(LEFT(STR(this.w_TEST13,15,6),11))=VAL(LEFT(STR(this.w_QTAUM1,15,6),11))
            this.w_NEWUMI = this.w_UNMIS3
          endif
        endif
        if this.w_TEST13=0 AND NOT EMPTY(this.w_UNMIS2) AND this.w_OPER12 $ "/*" AND this.w_MOLT12<>0
          if this.w_OPER12="/"
            this.w_TEST12 = this.w_QTAMOV * this.w_MOLT12
          else
            this.w_TEST12 = this.w_QTAMOV / this.w_MOLT12
          endif
          if VAL(LEFT(STR(this.w_TEST12,15,6),11))=VAL(LEFT(STR(this.w_QTAUM1,15,6),11))
            this.w_NEWUMI = this.w_UNMIS2
          endif
        endif
        if this.w_NEWUMI<>this.w_UNIMIS
          if this.w_ORIGIN="D"
            this.w_SERRIF = LEFT(this.w_SERRIF, 10)
            this.w_NUMDOC = NVL(NUMDOC, 0)
            this.w_ALFDOC = NVL(ALFDOC, Space(10))
            this.w_DATDOC = CP_TODATE(DATDOC)
            this.w_CODICE = NVL(CODICE, SPACE(20))
            ah_Msg("Aggiornamento riga %1 documento %2 %3 - del %4",.T.,.F.,.F., STR(this.w_NURIG), ALLTR(STR(this.w_NUMDOC,15)), Alltrim(this.w_ALFDOC), DTOC(this.w_DATDOC) )
            * --- Write into DOC_DETT
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.DOC_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"MVUNIMIS ="+cp_NullLink(cp_ToStrODBC(this.w_NEWUMI),'DOC_DETT','MVUNIMIS');
                  +i_ccchkf ;
              +" where ";
                  +"MVSERIAL = "+cp_ToStrODBC(this.w_SERRIF);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.w_RIGRIF);
                     )
            else
              update (i_cTable) set;
                  MVUNIMIS = this.w_NEWUMI;
                  &i_ccchkf. ;
               where;
                  MVSERIAL = this.w_SERRIF;
                  and CPROWNUM = this.w_RIGRIF;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          else
            ah_Msg("Aggiornamento riga %1 ODL/OCL %2 - del %3",.T.,.F.,.F., STR(this.w_NURIG), ALLTR(STR(this.w_NUMDOC,15)), DTOC(this.w_DATDOC))
            * --- Write into ODL_DETT
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.ODL_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_DETT_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"OLUNIMIS ="+cp_NullLink(cp_ToStrODBC(this.w_NEWUMI),'ODL_DETT','OLUNIMIS');
                  +i_ccchkf ;
              +" where ";
                  +"OLCODODL = "+cp_ToStrODBC(this.w_SERRIF);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.w_RIGRIF);
                     )
            else
              update (i_cTable) set;
                  OLUNIMIS = this.w_NEWUMI;
                  &i_ccchkf. ;
               where;
                  OLCODODL = this.w_SERRIF;
                  and CPROWNUM = this.w_RIGRIF;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
        endif
        SELECT ELABOR
        ENDSCAN
        * --- Esecuzione ok
        this.oParentObject.w_PESEOK = .T.
        this.oParentObject.w_PMSG = ah_Msgformat("Conversione eseguita correttamente")
      else
        * --- Gestisce log errori
        if this.w_NHF>=0
          this.w_TMPC = ah_Msgformat("ATTENZIONE: non esistono dati da aggiornare")
          this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
        endif
        * --- Esecuzione ok
        this.oParentObject.w_PESEOK = .T.
        this.oParentObject.w_PMSG = this.w_TMPC
      endif
      * --- Chiude cursore
      SELECT ELABOR
      USE
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='DOC_DETT'
    this.cWorkTables[2]='ODL_DETT'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
