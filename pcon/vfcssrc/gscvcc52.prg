* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscvcc52                                                        *
*              Aggiornamento chiave tabella risorse ART_TEMP                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-10-21                                                      *
* Last revis.: 2015-05-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NHF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscvcc52",oParentObject,m.w_NHF)
return(i_retval)

define class tgscvcc52 as StdBatch
  * --- Local variables
  w_NHF = 0
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_COMMDEFA = space(15)
  w_DATINI = ctod("  /  /  ")
  w_DATFIN = ctod("  /  /  ")
  w_FraseSQL = space(200)
  w_iRows = 0
  * --- WorkFile variables
  ART_TEMP_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiornamento chiave tabella risorse ART_TEMP
    * --- FIle di LOG
    * --- Try
    local bErr_039CE6F8
    bErr_039CE6F8=bTrsErr
    this.Try_039CE6F8()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Gestisce log errori
      this.oParentObject.w_PMSG = Message()
      this.oParentObject.w_PESEOK = .F.
      if this.w_NHF>=0
        this.w_TMPC = ah_Msgformat("Errore generico - impossibile eseguire aggiornamento richiesto")
        this.w_TEMP = SUPPSRVP("WRITELOG",this.w_NHF, this.w_TMPC)
      endif
    endif
    bTrsErr=bTrsErr or bErr_039CE6F8
    * --- End
  endproc
  proc Try_039CE6F8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    ah_msg("Aggiornamento in corso..." )
    GSCV_BDC(this,"xxx", "ART_TEMP")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    GSCV_BDC(this,i_CODAZI, "ART_TEMP")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    i_nConn=i_TableProp[this.ART_TEMP_idx,3]
    * --- commit
    cp_EndTrs(.t.)
    if Not GSCV_BDT( This, "ART_TEMP" , i_nConn )
      * --- Raise
      i_Error="Error"
      return
    endif
    i_nConn=i_TableProp[this.ART_TEMP_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
    GSCV_BRT(this,"ART_TEMP" , i_nConn , .T.)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_TMPC = ah_Msgformat("Aggiornamento eseguito con successo")
    * --- Esecuzione ok
    this.oParentObject.w_PESEOK = .T.
    this.oParentObject.w_PMSG = this.w_TMPC
    return


  proc Init(oParentObject,w_NHF)
    this.w_NHF=w_NHF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='ART_TEMP'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NHF"
endproc
