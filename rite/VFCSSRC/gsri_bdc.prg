* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_bdc                                                        *
*              Registra distinta IRPEF/INPS                                    *
*                                                                              *
*      Author: Zucchetti spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][207]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-04-12                                                      *
* Last revis.: 2007-12-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_SERIAL,w_BOTTONE
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsri_bdc",oParentObject,m.w_SERIAL,m.w_BOTTONE)
return(i_retval)

define class tgsri_bdc as StdBatch
  * --- Local variables
  w_SERIAL = space(10)
  w_BOTTONE = .NULL.
  w_VPSERIAL = space(10)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Conferma Distinta IRPEF e INPS (da GSRI_AVP e GSRI_AVN)
    * --- Questo batch viene richiamato dal bottone di conferma presente sull'anagrafica Distinte
    * --- Memorizza la distinta che l'operatore sta' caricando e rientra in stato di variazione.
    * --- Parametri
    * --- Serial della distinta
    * --- Nome del bottone che ha lanciato questo batch
    * --- Variabili
    * --- Controllo se sono presenti gli eventuali campi obbligatori
    if !this.oParentObject.CheckForm()
      i_retcode = 'stop'
      return
    endif
    this.w_VPSERIAL = this.w_SERIAL
    * --- Segnala l'esecuzione dell'ecpSave dovuto a batch e non per F10
    this.oParentObject.w_BOT = "X"
    * --- Memorizzazione Distinta
    this.oParentobject.NotifyEvent("Save")
    this.oParentobject.GotFocus()
    this.oParentobject.ecpSave()
    this.oParentobject.ecpQuery()
    this.oParentObject.w_BOT = ""
    * --- Ricalcolo Serial
    this.oParentObject.w_VPSERIAL = this.w_SERIAL
    * --- Creazione cursore delle chiavi
    this.oParentobject.QueryKeySet( "VPSERIAL="+cp_ToStrODBC(this.w_VPSERIAL) , "" )
    * --- Caricamento record ed accesso in modifica
    this.oParentobject.LoadRecWarn()
    this.oParentobject.NotifyEvent("Edit")
    this.oParentobject.ecpEdit()
    * --- Posizionamento sulla seconda pagina
    this.oParentobject.oPgFrm.ActivePage=2
    * --- In una prima release il bottone della memorizzazione era attivo anche in variazione
    * --- e la procedura rientrava sulla prima pagina della distinta con il focus sul bottone
    * --- ---
    * --- TmpMetodo = "this.oParentobject.oPgFrm.page1.oPag."+this.w_Bottone+".SetFocus()"
    * --- &TmpMetodo
  endproc


  proc Init(oParentObject,w_SERIAL,w_BOTTONE)
    this.w_SERIAL=w_SERIAL
    this.w_BOTTONE=w_BOTTONE
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_SERIAL,w_BOTTONE"
endproc
