* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri5bft                                                        *
*              Gen. file telematico 770/2007                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-06-11                                                      *
* Last revis.: 2011-02-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsri5bft",oParentObject)
return(i_retval)

define class tgsri5bft as StdBatch
  * --- Local variables
  w_INDIRIZZOEMAIL = space(100)
  FileTelematico = .NULL.
  w_nMod1 = 0
  w_ContaRec = 0
  CauPre = space(1)
  w_NUMCERTIF = 0
  w_CALCOLI = 0
  w_FLAGEUR = space(1)
  w_nMod2 = 0
  w_CFAzi = space(16)
  w_nRECRITE1 = 0
  w_nRECRITE3 = 0
  w_nMod3 = 0
  w_DataAppo = space(8)
  w_nRECRITE2 = 0
  w_OPERAZ = space(1)
  w_SOSPENDI = .f.
  w_OLDPERCF = space(16)
  w_OLDPER = space(15)
  w_PRIMOPERC = .f.
  w_PERCIPCF = space(16)
  w_MESS = space(100)
  w_DATCOMPL = .f.
  w_MRCODCON = space(15)
  w_ANDESCRI = space(40)
  w_PERCESTE = space(15)
  w_CFPEREST = space(16)
  w_LOCALEST = space(30)
  w_INDIREST = space(35)
  w_NAZIOEST = space(3)
  w_PROVIN = space(2)
  w_LAVAUTONO = space(1)
  w_NACODEST = space(5)
  w_CAUPRE = space(1)
  w_ANFLSGRE = space(1)
  w_ANNO = space(4)
  w_PERCIN = space(15)
  w_CODVAL = space(3)
  w_DATFIN = ctod("  /  /  ")
  w_DECIMI = space(1)
  w_PERCFIN = space(15)
  w_DATINI = ctod("  /  /  ")
  w_VALUTA = space(1)
  w_NumPerSC = 0
  w_NumPerSE = 0
  w_tSOMTOTASC = 0
  w_tSOMTOTASE = 0
  w_tNONSOGGSC = 0
  w_tIMPONISE = 0
  w_tIMPONISC = 0
  w_tMRTOTIM1SE = 0
  w_tMRTOTIM1SC = 0
  w_REGAGE = space(1)
  w_EREDE = space(1)
  w_EVEECCEZ = space(1)
  * --- WorkFile variables
  AZIENDA_idx=0
  TRI_BUTI_idx=0
  VEP_RITE_idx=0
  CONTI_idx=0
  DES_DIVE_idx=0
  NAZIONI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Le dichiarazione del caller sono a Pag.8
    * --- Generazione File Telematico 770
    * --- Controllo che le date di selezione siano all'interno dello stesso anno
    if STR(YEAR(this.oParentObject.w_DATFIN),4,0)<>STR(YEAR(this.oParentObject.w_DATINI),4,0)
      this.w_MESS = "L'intervallo di date deve essere interno ad un anno solare."+chr(13)
      ah_ErrorMsg(this.w_MESS,"stop","")
      i_retcode = 'stop'
      return
    endif
    * --- Eseguo la CheckForm per verificare i campi obbligatori
    this.oParentObject.bUpdated = .t.
    if !this.oParentObject.CheckForm()
      i_retcode = 'stop'
      return
    endif
    * --- Controllo obbligatoriet� del C.F. nella sez. domicialio notifica atti
    if empty(this.oParentObject.w_CFDOMNOT) and (! empty(this.oParentObject.w_CMDOMNOT) or ! empty(this.oParentObject.w_PRDOMNOT) or ! empty(this.oParentObject.w_INDOMNOT))
      ah_ErrorMsg("Manca il codice fiscale nella sezione domicilio per notifica degli atti","stop","")
      i_retcode = 'stop'
      return
    endif
    * --- Controllo sui valori obbligatori e bloccanti
    if this.oParentObject.w_PERAZI<>"S"
      if not empty(this.oParentObject.w_SESIGLA)
        if empty(this.oParentObject.w_SEINDIRI2)
          ah_ErrorMsg("Manca indirizzo della sede legale","stop","")
          i_retcode = 'stop'
          return
        endif
        if empty(this.oParentObject.w_SECODCOM)
          ah_ErrorMsg("Manca codice comune della sede legale","stop","")
          i_retcode = 'stop'
          return
        endif
      endif
      if not empty(this.oParentObject.w_SERSIGLA) or not empty(this.oParentObject.w_SERINDIR) or not empty(this.oParentObject.w_SERCAP)
        if empty(this.oParentObject.w_SERCOMUN)
          ah_ErrorMsg("Manca comune del domicilio fiscale","stop","")
          i_retcode = 'stop'
          return
        endif
      endif
      if not empty(this.oParentObject.w_SERCOMUN) or not empty(this.oParentObject.w_SERINDIR) or not empty(this.oParentObject.w_SERCAP)
        if empty(this.oParentObject.w_SERSIGLA)
          ah_ErrorMsg("Manca provincia del domicilio fiscale","stop","")
          i_retcode = 'stop'
          return
        endif
      endif
      if not empty(this.oParentObject.w_SERCOMUN) or not empty(this.oParentObject.w_SERSIGLA) or not empty(this.oParentObject.w_SERCAP)
        if empty(this.oParentObject.w_SERINDIR)
          ah_ErrorMsg("Manca indirizzo del domicilio fiscale","stop","")
          i_retcode = 'stop'
          return
        endif
        if empty(this.oParentObject.w_SERCODCOM)
          ah_ErrorMsg("Manca codice comune del domicilio fiscale","stop","")
          i_retcode = 'stop'
          return
        endif
      endif
      if isalpha(this.oParentObject.w_RFCODFIS) and (empty(this.oParentObject.w_RFCOGNOME) OR empty(this.oParentObject.w_RFNOME) OR empty(this.oParentObject.w_RFSESSO) OR empty(this.oParentObject.w_RFDATANASC) OR empty(this.oParentObject.w_RFCOMNAS))
        ah_ErrorMsg("Mancano alcuni dati relativi al rappresentante firmatario della dichiarazione","stop","")
        i_retcode = 'stop'
        return
      endif
      if (not empty(this.oParentObject.w_RFSIGLA)) AND (this.oParentObject.w_RFSIGLA<>"EE")
        if empty(this.oParentObject.w_RFCOMUNE) OR empty(this.oParentObject.w_RFINDIRIZ)
          ah_ErrorMsg("Mancano alcuni dati relativi alla residenza del rappresentante firmatario","stop","")
          i_retcode = 'stop'
          return
        endif
      endif
      if empty(this.oParentObject.w_RFCODFIS)
        ah_ErrorMsg("Manca il codice fiscale/partita IVA del rappresentante firmatario della dichiarazione","stop","")
        i_retcode = 'stop'
        return
      endif
    else
      if not empty(this.oParentObject.w_RESIGLA) and (this.oParentObject.w_RESIGLA<>"EE")
        if empty(this.oParentObject.w_INDIRIZ)
          ah_ErrorMsg("Manca l'indirizzo del domicilio fiscale del dichiarante","stop","")
          i_retcode = 'stop'
          return
        endif
        if empty(this.oParentObject.w_RECODCOM)
          ah_ErrorMsg("Manca codice comune del domicilio fiscale del dichiarante","stop","")
          i_retcode = 'stop'
          return
        endif
      endif
    endif
    if inlist(this.oParentObject.w_RFCODCAR,"3","4")
      if empty(this.oParentObject.w_RFDATFAL)
        ah_ErrorMsg("Manca la data di apertura fallimento","stop","")
        i_retcode = 'stop'
        return
      endif
    endif
    * --- Verifico in caso di P.I. la presenza del campo denominazione  nei dati relativi al rappresentante firmatario della dichiarazione
    if val(this.oParentObject.w_RFCODFIS) <> 0 and empty(this.oParentObject.w_RFDENOMI)
      ah_ErrorMsg("Manca la denominazione del rappresentante firmatario della dichiarazione","stop","")
      i_retcode = 'stop'
      return
    endif
    if this.oParentObject.w_Sez4="1" and empty(this.oParentObject.w_RFCFINS2)
      ah_ErrorMsg("Manca il codice fiscale del soggetto che presenta la restante parte della dichiarazione","stop","")
      i_retcode = 'stop'
      return
    endif
    * --- Controllo dati relativi all'impegno  presentazione telematica
    this.oParentObject.w_IMPTRTEL = iif(empty(this.oParentObject.w_IMPTRTEL),"0",this.oParentObject.w_IMPTRTEL)
    if (this.oParentObject.w_IMPTRTEL<>"0") or (this.oParentObject.w_FIRMINT<>"0") or not empty(this.oParentObject.w_RFDATIMP) ;
      or not empty(this.oParentObject.w_CODFISIN) or this.oParentObject.w_TIPFORN$("0305")
      if empty(this.oParentObject.w_RFDATIMP) OR empty(this.oParentObject.w_CODFISIN) 
        ah_ErrorMsg("Mancano dati relativi al C.A.F. o alla data impegno trasmissione","stop","")
        i_retcode = 'stop'
        return
      endif
      if this.oParentObject.w_IMPTRTEL="0" 
        ah_ErrorMsg("Manca impegno a trasmettere o non sono state selezionate modalit� alternative","stop","")
        i_retcode = 'stop'
        return
      endif
    endif
    * --- Controllo formato numerico dei numeri telefonici
    if not empty(this.oParentObject.w_TELEFONO)
      for lettera = 65 to 90
      if chr(lettera) $ this.oParentObject.w_TELEFONO
        ah_ErrorMsg("Numero di telefono relativo alla persona fisica errato!.","stop","")
        i_retcode = 'stop'
        return
      endif
      endfor
    endif
    if not empty(this.oParentObject.w_SETELEFONO)
      for lettera = 65 to 90
      if chr(lettera) $ this.oParentObject.w_SETELEFONO
        ah_ErrorMsg("Numero di telefono relativo ad altri soggetti errato!.","stop","")
        i_retcode = 'stop'
        return
      endif
      endfor
    endif
    if not empty(this.oParentObject.w_RFTELEFONO)
      for lettera = 65 to 90
      if chr(lettera) $ this.oParentObject.w_RFTELEFONO
        ah_ErrorMsg("Numero di telefono relativo al rappresentante firmatario errato!","stop","")
        i_retcode = 'stop'
        return
      endif
      endfor
    endif
    * --- Controllo valorizzazione campo eventi eccezionali
    if this.oParentObject.w_FLAGEVE="01" and not this.oParentObject.w_CODEVECC $ "1-3-4"
      ah_ErrorMsg("Eventi eccezionali non impostati!","stop","")
      i_retcode = 'stop'
      return
    endif
    * --- Fine controllo valori obbligatori e bloccanti
    * --- Questo giro lo faccio per ingannare i Batches standard che lancio successivamente
    this.w_ANNO = this.oParentObject.w_ANNO
    this.w_PERCIN = this.oParentObject.w_PERCIN
    this.w_CODVAL = this.oParentObject.w_CODVAL
    this.w_DATFIN = this.oParentObject.w_DATFIN
    this.w_DECIMI = this.oParentObject.w_DECIMI
    this.w_PERCFIN = this.oParentObject.w_PERCFIN
    this.w_DATINI = this.oParentObject.w_DATINI
    this.w_VALUTA = this.oParentObject.w_VALUTA
    * --- Codice Fiscale dell'Azienda Corrente
    this.w_CFAzi = this.oParentObject.w_FOCODFIS
    this.w_OPERAZ = this.oParentObject.w_TIPOPERAZ
    this.w_FLAGEUR = this.oParentObject.w_FLAGEURO
    this.w_SOSPENDI = .F.
    ah_Msg("Preparazione cursori in corso...",.T.)
    * --- Lancio i batches standard che creano i cursori
    do GSRI_BST with this
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    do GSRI_BSC with this
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Ex chiamata a GSRI_BSE
    * --- Controllo se sono state abbinate ritenute con condizioni diverse
    if this.w_SOSPENDI
      i_retcode = 'stop'
      return
    endif
    * --- Conto i modelli totali della dichiarazione
    if used("RITE1")
      select RITE1
      this.w_nMod1 = iif(mod(reccount(),5)=0,int(reccount()/5),int(reccount()/5)+1)
      Select MRCODCON from RITE1 Group By MRCODCON into cursor TEMP
      Select TEMP
      this.w_nRECRITE1 = reccount()
      Select COUNT(*) As RIGHE, COUNT(*) / 5 As REGIST from RITE1 Group By MRCODCON having RIGHE > 5 into cursor TEMP
      if reccount() > 0
        Select TEMP 
 Go Top 
 Scan
        if REGIST - int(REGIST) <>0
          do case
            case Int(REGIST) = 1
              this.w_nRECRITE1 = this.w_nRECRITE1 + 1
            case Int(REGIST) = 2
              this.w_nRECRITE1 = this.w_nRECRITE1 + 2
            case Int(REGIST) = 3
              this.w_nRECRITE1 = this.w_nRECRITE1 + 3
            case Int(REGIST) = 4
              this.w_nRECRITE1 = this.w_nRECRITE1 + 4
            case Int(REGIST) = 5
              this.w_nRECRITE1 = this.w_nRECRITE1 + 5
            case Int(REGIST) = 6
              this.w_nRECRITE1 = this.w_nRECRITE1 + 6
          endcase
        endif
        ENDSCAN
      endif
      if used("TEMP")
        Select TEMP
        use
      endif
    endif
    if used("RITE2")
      select RITE2
      this.w_nMod2 = iif(mod(reccount(),10)=0,int(reccount()/10),int(reccount()/10)+1)
      this.w_nRECRITE2 = reccount()
    endif
    if used("RITE3")
      select RITE3
      this.w_nMod3 = iif(mod(reccount(),13)=0,int(reccount()/13),int(reccount()/13)+1)
      this.w_nRECRITE3 = reccount()
    endif
    * --- Calcolo numero di certificazioni di lavoro autonomo
    this.w_NUMCERTIF = this.w_nRECRITE1
    this.oParentObject.w_NUMCERTIF2 = this.w_NUMCERTIF
    if (this.w_nRECRITE1=0) and (this.w_nRECRITE3=0)
      i_retcode = 'stop'
      return
    endif
    * --- Creo loggetto
    this.FileTelematico = CreateObject("FileTelematico",alltrim(this.oParentObject.w_DirName)+alltrim(this.oParentObject.w_FileName),this)
    * --- Valorizzo il numero totale di modelli
    this.FileTelematico.nModelli = Max(this.w_nMod1,this.w_nMod2,this.w_nMod3)
    * --- Riga di testata
    ah_Msg("Fase 2: record tipo 'A'...",.T.)
    this.Pag7()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Record di Tipo "B"
    ah_Msg("Fase 2: record tipo 'B'...",.T.)
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    ah_Msg("Fase 2: record tipo 'E' ed 'H'...",.T.)
    * --- Record di Tipo "E" ed "H"
    create cursor LISTAPERC (CODCON C(15),DENOMI C(40),CODFIS C(1),COGDEN C(1),NOME C(1),SESSO C(1), ; 
 DATNAS C(1),COMNAS C(1),COMUN C(1))
    if used("RITE1") and used("RITE3")
      do while !eof("RITE3") and (this.oParentObject.w_QST1="1" or this.oParentObject.w_QST12="1" or this.oParentObject.w_QST14="1")
        this.Pag6()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.FileTelematico.ScriviRecordE()     
        * --- Incremento il numero di modelli
        this.FileTelematico.nModelloE = this.FileTelematico.nModelloE + 1
      enddo
      this.w_DATCOMPL = .T.
      do while !eof("RITE1")
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.FileTelematico.ScriviRecordH()     
        * --- Incremento il numero di modelli
        this.FileTelematico.nModelloH = this.FileTelematico.nModelloH + 1
      enddo
      if ! this.w_DATCOMPL
        this.w_MESS = "Dati anagrafici percipienti mancanti"+chr(13)+"Impossibile generare il file"+chr(13)+"Vuoi visualizzare la stampa di verifica?"
        if ah_YesNo(this.w_MESS)
          Select CODCON,DENOMI,MAX(CODFIS) as CODFIS,MAX(COGDEN) as COGDEN,MAX(NOME) as NOME, ; 
 MAX(SESSO) as SESSO, MAX(DATNAS) as DATNAS,MAX(COMNAS) as COMNAS,MAX(COMUN) as COMUN ; 
 from LISTAPERC into cursor __TMP__ group by CODCON,DENOMI order by CODCON
          CP_CHPRN("..\RITE\EXE\QUERY\GSRI1BFT.FRX")
          this.FileTelematico.CancellaFile()     
          i_retcode = 'stop'
          return
        else
          this.FileTelematico.CancellaFile()     
          i_retcode = 'stop'
          return
        endif
      endif
    endif
    * --- Riga di chiusura
    ah_Msg("Fase 2: record tipo 'Z'...",.T.)
    this.FileTelematico.RecordZ()     
    if used("RITE1")
      select RITE1
      use
    endif
    if used("RITE2")
      select RITE2
      use
    endif
    if used("RITE3")
      select RITE3
      use
    endif
    if used("LISTAPERC")
      select LISTAPERC
      use
    endif
    if used("__TMP__")
      select __TMP__
      use
    endif
    wait clear
    ah_ErrorMsg("Il file '"+alltrim(this.oParentObject.w_DirName)+alltrim(this.oParentObject.w_FileName)+"' � stato generato.","!","")
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Record B - Frontespizio
    this.FileTelematico.RecordB("B")     
    * --- Da vedere come e se verificarne l'appartenenza all'anagrafe tributaria
    this.FileTelematico.RecordB(this.w_CFAzi)     
    this.FileTelematico.RecordB(right("00000000" + alltrim(str(this.FileTelematico.nModello)),8))     
    this.FileTelematico.RecordB(space(3))     
    this.FileTelematico.RecordB(space(25))     
    this.FileTelematico.RecordB(space(20))     
    this.FileTelematico.RecordB("05006900962     ")     
    * --- Campo 8 - Flag conferma
    this.FileTelematico.RecordB(left(alltrim(this.oParentObject.w_FLAGCONF)+"0",1))     
    * --- Campo 8 - Flag Conferma
    this.FileTelematico.RecordB(left(alltrim(this.oParentObject.w_FLAGCOR)+"0",1))     
    * --- Campo 10 - Filler
    this.FileTelematico.RecordB(space(1))     
    * --- Campo 11 - Flag Dichiarazione Integrativa
    this.FileTelematico.RecordB(left(alltrim(this.oParentObject.w_FLAGINT)+"0",1))     
    * --- Campo 12 - Flag eventi eccezionali
    this.FileTelematico.RecordB(this.oParentObject.w_CODEVECC)     
    * --- Dati del frontespizio
    * --- Dati del Contribuente
    this.FileTelematico.RecordB(this.oParentObject.w_FOCOGNOME)     
    this.FileTelematico.RecordB(this.oParentObject.w_FONOME)     
    this.FileTelematico.RecordB(this.oParentObject.w_FODENOMINA)     
    * --- Campi dal 16-18  - Filler Alfanumerico 
    this.FileTelematico.RecordB(space(13))     
    this.FileTelematico.RecordB(iif(this.oParentObject.w_PERAZI="S",this.oParentObject.w_CODATT,this.oParentObject.w_SECODATT))     
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZ_EMAIL"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZ_EMAIL;
        from (i_cTable) where;
            AZCODAZI = this.oParentObject.w_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_INDIRIZZOEMAIL = NVL(cp_ToDate(_read_.AZ_EMAIL),cp_NullValue(_read_.AZ_EMAIL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Campo 20
    this.FileTelematico.RecordB(upper(left(this.w_INDIRIZZOEMAIL+space(100),100)))     
    this.FileTelematico.RecordB(iif(this.oParentObject.w_PERAZI="S",this.oParentObject.w_TELEFONO,this.oParentObject.w_SETELEFONO))     
    * --- fax non gestito
    this.FileTelematico.RecordB(space(12))     
    * --- Persona fisica
    this.FileTelematico.RecordB(this.oParentObject.w_COMUNE)     
    this.FileTelematico.RecordB(this.oParentObject.w_SIGLA)     
    this.w_DataAppo = right("00"+alltrim(str(day(this.oParentObject.w_DATANASC))),2)+right("00"+alltrim(str(month(this.oParentObject.w_DATANASC))),2)+str(year(this.oParentObject.w_DATANASC),4,0)
    this.FileTelematico.RecordB(iif(!empty(this.oParentObject.w_DATANASC),this.w_DataAppo,repl("0",8)))     
    this.FileTelematico.RecordB(IIF(Empty(this.oParentObject.w_SESSO)," ",this.oParentObject.w_SESSO))     
    * --- Residenza anagrafica campi 27-31
    this.FileTelematico.RecordB(this.oParentObject.w_RECOMUNE)     
    this.FileTelematico.RecordB(this.oParentObject.w_RESIGLA)     
    this.FileTelematico.RecordB(this.oParentObject.w_INDIRIZ)     
    this.FileTelematico.RecordB(iif(empty(this.oParentObject.w_CAP),"00000",Left(this.oParentObject.w_CAP,5)))     
    * --- Campo 31
    this.FileTelematico.RecordB(left(alltrim(this.oParentObject.w_RECODCOM)+space(4),4))     
    this.w_DataAppo = right("00"+alltrim(str(day(this.oParentObject.w_VARRES))),2)+right("00"+alltrim(str(month(this.oParentObject.w_VARRES))),2)+str(year(this.oParentObject.w_VARRES),4,0)
    this.FileTelematico.RecordB(iif(!empty(this.oParentObject.w_VARRES),this.w_DataAppo,repl("0",8)))     
    * --- -----------------------------------------------------------------------------------------------------------------
    *     Altri soggetti
    this.FileTelematico.RecordB(iif(!empty(this.oParentObject.w_NATGIU),RIGHT("00"+ALLTRIM(this.oParentObject.w_NATGIU),2),"00"))     
    this.w_DataAppo = right("00"+alltrim(str(month(this.oParentObject.w_SEVARSED))),2)+str(year(this.oParentObject.w_SEVARSED),4,0)
    this.FileTelematico.RecordB(iif(!empty(this.oParentObject.w_SEVARSED),this.w_DataAppo,repl("0",6)))     
    this.FileTelematico.RecordB(this.oParentObject.w_SECOMUNE)     
    this.FileTelematico.RecordB(this.oParentObject.w_SESIGLA)     
    this.FileTelematico.RecordB(this.oParentObject.w_SEINDIRI2)     
    this.FileTelematico.RecordB(iif(empty(this.oParentObject.w_SECAP),"00000",Left(this.oParentObject.w_SECAP,5)))     
    * --- Campo 39
    this.FileTelematico.RecordB(left(alltrim(this.oParentObject.w_SECODCOM)+space(4),4))     
    this.w_DataAppo = right("00"+alltrim(str(month(this.oParentObject.w_SEVARDOM))),2)+str(year(this.oParentObject.w_SEVARDOM),4,0)
    this.FileTelematico.RecordB(iif(!empty(this.oParentObject.w_SEVARDOM),this.w_DataAppo,repl("0",6)))     
    this.FileTelematico.RecordB(this.oParentObject.w_SERCOMUN)     
    this.FileTelematico.RecordB(this.oParentObject.w_SERSIGLA)     
    this.FileTelematico.RecordB(this.oParentObject.w_SERINDIR)     
    this.FileTelematico.RecordB(iif(empty(this.oParentObject.w_SERCAP),"00000",Left(this.oParentObject.w_SERCAP,5)))     
    * --- Campo 45
    this.FileTelematico.RecordB(left(alltrim(this.oParentObject.w_SERCODCOM)+space(4),4))     
    this.FileTelematico.RecordB(iif(!empty(this.oParentObject.w_STATO),this.oParentObject.w_STATO,"0"))     
    this.FileTelematico.RecordB(iif(!empty(this.oParentObject.w_SITUAZ),this.oParentObject.w_SITUAZ,"0"))     
    this.FileTelematico.RecordB(iif(empty(this.oParentObject.w_CODFISDA),"00000000000",this.oParentObject.w_CODFISDA))     
    * --- Campi 49-53
    this.FileTelematico.RecordB(space(75))     
    * --- --------------------------------------------------------------------------------------------------------------
    *     Firma della dichiarazione
    * --- Campo 54
    this.FileTelematico.RecordB(iif(EMPTY(NVL(this.oParentObject.w_FIRMDICH,"0")),"0",this.oParentObject.w_FIRMDICH))     
    * --- Campo 55
    this.FileTelematico.RecordB(iif(EMPTY(NVL(this.oParentObject.w_CFINCCON," ")),space(16),this.oParentObject.w_CFINCCON))     
    * --- Campo 56
    * --- codice soggetto 
    this.FileTelematico.RecordB(this.oParentObject.w_CODSOGG)     
    * --- Filler campo 57
    this.FileTelematico.RecordB(iif(EMPTY(NVL(this.oParentObject.w_FIRMPRES,"0")),"0",this.oParentObject.w_FIRMPRES))     
    * --- Filler campi 58 - 74
    this.FileTelematico.RecordB(space(17))     
    * --- ------------------------------------- redazione della dichiarazione ----------------------------------------------------
    *     Dati Sezioni I - II - III- IV Trasmissione modello 770 Semplificato
    * --- Campo 75
    do case
      case this.oParentObject.w_Sez1="1"
        this.FileTelematico.RecordB("1")     
      case this.oParentObject.w_Sez2="1"
        this.FileTelematico.RecordB("2")     
      case this.oParentObject.w_Sez4="1"
        this.FileTelematico.RecordB("4")     
      otherwise
        * --- Volutamente metto zero per generare un errore in fase di verifica modello
        this.FileTelematico.RecordB("0")     
    endcase
    * --- Campo 76 N� certificazioni lavoro dipendente
    this.FileTelematico.RecordB(repl("0", 8))     
    * --- Campo 77 N� certificazioni lavoro autonomo
    this.FileTelematico.RecordB(right("00000000" + iif(this.oParentObject.w_SEZ1="1" or this.oParentObject.w_SEZ2="1" or this.oParentObject.w_SEZ4="1",alltrim(str(this.w_NUMCERTIF)),""), 8))     
    * --- Campo 78
    if this.oParentObject.w_Sez2="1"
      this.FileTelematico.RecordB("0")     
    else
      this.FileTelematico.RecordB(left(alltrim(str(val(this.oParentObject.w_qST1)+val(this.oParentObject.w_qST14)))+"0",1))     
    endif
    * --- Campo 79
    if this.oParentObject.w_Sez2="1"
      this.FileTelematico.RecordB("0")     
    else
      this.FileTelematico.RecordB(left(alltrim(str(val(this.oParentObject.w_qSX1)+val(this.oParentObject.w_qSX14)))+"0",1))     
    endif
    * --- Campo 80
    if this.oParentObject.w_Sez2="1"
      this.FileTelematico.RecordB("0")     
    else
      this.FileTelematico.RecordB(left(alltrim(str(val(this.oParentObject.w_q771)+val(this.oParentObject.w_q774)))+"0",1))     
    endif
    * --- Campo 81
    if this.oParentObject.w_Sez4="1"
      this.FileTelematico.RecordB(this.oParentObject.w_RFCFINS2)     
    else
      this.FileTelematico.RecordB(space(16))     
    endif
    * --- -----------------------------------------------------------------------------------------------------------------
    *     Dichiarazione Correttiva nei termini e integrativa "parziale"
    * --- Campo 82
    this.FileTelematico.RecordB(RIGHT("00000000000000000"+alltrim(this.oParentObject.w_IDESTA),17))     
    this.FileTelematico.RecordB(RIGHT("000000"+alltrim(this.oParentObject.w_PROIFT),6))     
    * --- --------------------------------------------------------------------------------------------------------------------------------------------------------
    *     Dati relativi alle dichiarazioni correttive o integrative "parziali" e alle dichiarazioni su pi� invii
    if this.oParentObject.w_FLAGCOR= "1" or this.oParentObject.w_FLAGINT = "1"
      * --- Campo 84
      this.FileTelematico.RecordB(left(alltrim(str(val(this.oParentObject.w_qST1)+val(this.oParentObject.w_qST12)+val(this.oParentObject.w_qST14)))+"0",1))     
      * --- Campo 85
      this.FileTelematico.RecordB(left(alltrim(str(val(this.oParentObject.w_qSX1)+val(this.oParentObject.w_qSX12)+val(this.oParentObject.w_qSX14)))+"0",1))     
      * --- Campo 86
      this.FileTelematico.RecordB(Repl("0",8))     
      * --- Campo 87
      this.FileTelematico.RecordB(RIGHT("00000000"+alltrim(STR(this.w_NUMCERTIF,8,0)),8))     
    else
      this.FileTelematico.RecordB(Repl("0",18))     
    endif
    * --- Campo 88 - Filler
    this.FileTelematico.RecordB(space(157))     
    * --- -------------------------------------------------------------------------------------------------------------------
    *     Sez. Domicilio per la notificazione degli atti
    * --- Campo 89
    this.FileTelematico.RecordB(left(alltrim(this.oParentObject.w_CFDOMNOT)+space(16),16))     
    this.FileTelematico.RecordB(this.oParentObject.w_UFDOMNOT)     
    this.FileTelematico.RecordB(this.oParentObject.w_CODOMNOT)     
    this.FileTelematico.RecordB(this.oParentObject.w_NODOMNOT)     
    this.FileTelematico.RecordB(this.oParentObject.w_CMDOMNOT)     
    this.FileTelematico.RecordB(this.oParentObject.w_PRDOMNOT)     
    this.FileTelematico.RecordB(this.oParentObject.w_CCDOMNOT)     
    this.FileTelematico.RecordB(right("00000"+alltrim(this.oParentObject.w_CPDOMNOT),5))     
    this.FileTelematico.RecordB(this.oParentObject.w_VPDOMNOT)     
    this.FileTelematico.RecordB(this.oParentObject.w_INDOMNOT)     
    this.FileTelematico.RecordB(this.oParentObject.w_NCDOMNOT)     
    this.FileTelematico.RecordB(this.oParentObject.w_FRDOMNOT)     
    this.FileTelematico.RecordB(this.oParentObject.w_SEDOMNOT)     
    this.FileTelematico.RecordB(right("000"+alltrim(this.oParentObject.w_CEDOMNOT),3))     
    this.FileTelematico.RecordB(this.oParentObject.w_SFDOMNOT)     
    this.FileTelematico.RecordB(this.oParentObject.w_LRDOMNOT)     
    * --- Campo 105
    this.FileTelematico.RecordB(this.oParentObject.w_IEDOMNOT)     
    * --- -------------------------------------------------------------------------------------------------------------------
    *     Dati relativi al rappresentante firmatario della dichiarazione
    *     Riservato a chi presenta la dichiarazione per altri
    this.FileTelematico.RecordB(this.oParentObject.w_RFCODFIS)     
    this.FileTelematico.RecordB(right("00"+alltrim(this.oParentObject.w_RFCODCAR),2))     
    * --- Campo 108 
    this.w_DataAppo = right("00"+alltrim(str(day(this.oParentObject.w_RFDATCAR))),2)+right("00"+alltrim(str(month(this.oParentObject.w_RFDATCAR))),2)+str(year(this.oParentObject.w_RFDATCAR),4,0)
    this.FileTelematico.RecordB(iif(!empty(this.oParentObject.w_RFDATCAR),this.w_DataAppo,repl("0",8)))     
    this.FileTelematico.RecordB(space(11))     
    * --- Diversifico la compilazione in base al valore presente in RFCODFIS (Numerico o Alfanumerico)
    if isalpha(this.oParentObject.w_RFCODFIS)
      * --- Campo  110 - Denominazione
      this.FileTelematico.RecordB(space(60))     
      this.FileTelematico.RecordB(this.oParentObject.w_RFCOGNOME)     
      this.FileTelematico.RecordB(this.oParentObject.w_RFNOME)     
      this.FileTelematico.RecordB(Left ( this.oParentObject.w_RFSESSO+Space(1) ,1 ))     
      this.w_DataAppo = right("00"+alltrim(str(day(this.oParentObject.w_RFDATANASC))),2)+right("00"+alltrim(str(month(this.oParentObject.w_RFDATANASC))),2)+str(year(this.oParentObject.w_RFDATANASC),4,0)
      this.FileTelematico.RecordB(iif(!empty(this.oParentObject.w_RFDATANASC),this.w_DataAppo,repl("0",8)))     
      this.FileTelematico.RecordB(this.oParentObject.w_RFCOMNAS)     
      this.FileTelematico.RecordB(this.oParentObject.w_RFSIGNAS)     
    else
      * --- Campo  110 - Denominazione
      this.FileTelematico.RecordB(left(this.oParentObject.w_RFDENOMI+space(60),60))     
      * --- Campi  111 - 113
      this.FileTelematico.RecordB(space(45))     
      * --- Campo 114
      this.FileTelematico.RecordB(repl("0",8))     
      * --- Campi 115 - 116
      this.FileTelematico.RecordB(space(42))     
    endif
    * --- Campo 117
    this.FileTelematico.RecordB(this.oParentObject.w_RFCOMUNE)     
    this.FileTelematico.RecordB(this.oParentObject.w_RFSIGLA)     
    this.FileTelematico.RecordB(iif(empty(this.oParentObject.w_RFCAP),"00000",Left(this.oParentObject.w_RFCAP,5)))     
    this.FileTelematico.RecordB(this.oParentObject.w_RFINDIRIZ)     
    this.FileTelematico.RecordB(this.oParentObject.w_RFTELEFONO)     
    * --- Campo 122 - Data di apertura fallimento
    this.w_DataAppo = right("00"+alltrim(str(day(this.oParentObject.w_RFDATFAL))),2)+right("00"+alltrim(str(month(this.oParentObject.w_RFDATFAL))),2)+str(year(this.oParentObject.w_RFDATFAL),4,0)
    this.FileTelematico.RecordB(iif(!empty(this.oParentObject.w_RFDATFAL),this.w_DataAppo,repl("0",8)))     
    * --- Filler campi 123 - 126
    this.FileTelematico.RecordB(space(11))     
    * --- ----------------------------------------------------------------------------------------------------------------
    *     Impegno alla presentazione telematica
    * --- Campo 127
    this.FileTelematico.RecordB(this.oParentObject.w_CODFISIN)     
    * --- Campo 128
    this.FileTelematico.RecordB(right("00000"+iif(this.oParentObject.w_NUMCAF<>0,ALLTRIM(str(this.oParentObject.w_NUMCAF,5,0)),"00000"),5))     
    * --- Campo 129
    this.FileTelematico.RecordB(left(alltrim(this.oParentObject.w_IMPTRTEL)+"0",1))     
    * --- Campo 130
    this.w_DataAppo = right("00"+alltrim(str(day(this.oParentObject.w_RFDATIMP))),2)+right("00"+alltrim(str(month(this.oParentObject.w_RFDATIMP))),2)+right(STR(year(this.oParentObject.w_RFDATIMP),4,0),4)
    this.FileTelematico.RecordB(iif(!empty(this.w_DataAppo) and val(this.w_DataAppo)<>0,this.w_DataAppo,repl("0",8)))     
    * --- Campo 131
    this.FileTelematico.RecordB(this.oParentObject.w_FIRMINT)     
    * --- ------------------------------------------------------------------------------------------------------------
    *     Visto di conformit�
    * --- Campo 132
    this.FileTelematico.RecordB(iif(this.oParentObject.w_SELCAF="1",this.oParentObject.w_CFRESCAF,space(16)))     
    * --- Campo 133
    this.FileTelematico.RecordB(IIF(EMPTY(this.oParentObject.w_CFDELCAF),REPL("0",11),this.oParentObject.w_CFDELCAF))     
    * --- Campo 134
    this.FileTelematico.RecordB(iif(this.oParentObject.w_SELCAF="2",this.oParentObject.w_CFRESCAF,space(16)))     
    * --- Campo 135
    this.FileTelematico.RecordB(this.oParentObject.w_FLAGFIR)     
    * --- ----------------------------------------------------------------------------------------------------------------------------
    *     Sezione Riservata al servizio telematico
    * --- Campi Filler 136 - 150 
    this.FileTelematico.RecordB(space(112))     
    * --- ----------------------------------------------------------------------------------------------------------------------------
    *     Ultimi tre caratteri di controllo
    this.FileTelematico.RecordB("A")     
    * --- Scrittura del recordB
    this.FileTelematico.RecordB("",.t.,.t.)     
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_PRIMOPERC = .T.
    this.w_OLDPERCF = "*"
    this.w_OLDPER = ""
    * --- Record H
    select RITE1
    do while !eof()
      select RITE1
      if this.w_OLDPERCF<>NVL(ANCODFIS,"") OR this.w_OLDPER<>MRCODCON
        if NOT(this.w_PRIMOPERC)
          * --- Nuovo record per variazione percipiente --- Record 'H'
          * --- Incremento il numero di Modello
          this.FileTelematico.nRigoAU = 1
          this.FileTelematico.nModAU = this.FileTelematico.nModAU + 1
          exit
        endif
        this.w_MRCODCON = NVL(MRCODCON,"")
        this.w_ANDESCRI = NVL(ANDESCRI,"")
        * --- Verifico se percipiente estero
        * --- Read from CONTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ANFLGEST,ANCOFISC,ANLOCALI,ANINDIRI,ANNAZION,AN_EREDE,ANEVEECC"+;
            " from "+i_cTable+" CONTI where ";
                +"ANTIPCON = "+cp_ToStrODBC("F");
                +" and ANCODICE = "+cp_ToStrODBC(this.w_MRCODCON);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ANFLGEST,ANCOFISC,ANLOCALI,ANINDIRI,ANNAZION,AN_EREDE,ANEVEECC;
            from (i_cTable) where;
                ANTIPCON = "F";
                and ANCODICE = this.w_MRCODCON;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_PERCESTE = NVL(cp_ToDate(_read_.ANFLGEST),cp_NullValue(_read_.ANFLGEST))
          this.w_CFPEREST = NVL(cp_ToDate(_read_.ANCOFISC),cp_NullValue(_read_.ANCOFISC))
          this.w_LOCALEST = NVL(cp_ToDate(_read_.ANLOCALI),cp_NullValue(_read_.ANLOCALI))
          this.w_INDIREST = NVL(cp_ToDate(_read_.ANINDIRI),cp_NullValue(_read_.ANINDIRI))
          this.w_NAZIOEST = NVL(cp_ToDate(_read_.ANNAZION),cp_NullValue(_read_.ANNAZION))
          this.w_EREDE = NVL(cp_ToDate(_read_.AN_EREDE),cp_NullValue(_read_.AN_EREDE))
          this.w_EVEECCEZ = NVL(cp_ToDate(_read_.ANEVEECC),cp_NullValue(_read_.ANEVEECC))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if EMPTY(nvl(ANCODFIS,""))
          this.w_DATCOMPL = .F.
          INSERT INTO LISTAPERC VALUES (this.w_MRCODCON,this.w_ANDESCRI,"1","0","0","0","0","0","0")
          select RITE1
        endif
        if iif(ANPERFIS="S",EMPTY(nvl(ANCOGNOM,"")),EMPTY(nvl(ANDESCRI,"")))
          this.w_DATCOMPL = .F.
          INSERT INTO LISTAPERC VALUES (this.w_MRCODCON,this.w_ANDESCRI,"0","1","0","0","0","0","0")
          select RITE1
        endif
        if EMPTY(nvl(ANLOCALI,""))
          this.w_DATCOMPL = .F.
          INSERT INTO LISTAPERC VALUES (this.w_MRCODCON,this.w_ANDESCRI,"0","0","0","0","0","0","1")
          select RITE1
        endif
        if EMPTY(nvl(AN__NOME,"")) and ANPERFIS="S"
          this.w_DATCOMPL = .F.
          INSERT INTO LISTAPERC VALUES (this.w_MRCODCON,this.w_ANDESCRI,"0","0","1","0","0","0","0")
          select RITE1
        endif
        if EMPTY(nvl(AN_SESSO,"")) and ANPERFIS="S"
          this.w_DATCOMPL = .F.
          INSERT INTO LISTAPERC VALUES (this.w_MRCODCON,this.w_ANDESCRI,"0","0","0","1","0","0","0")
          select RITE1
        endif
        if EMPTY(nvl(ANDATNAS,"")) and ANPERFIS="S"
          this.w_DATCOMPL = .F.
          INSERT INTO LISTAPERC VALUES (this.w_MRCODCON,this.w_ANDESCRI,"0","0","0","0","1","0","0")
          select RITE1
        endif
        if EMPTY(nvl(ANLOCNAS,"")) and ANPERFIS="S"
          this.w_DATCOMPL = .F.
          INSERT INTO LISTAPERC VALUES (this.w_MRCODCON,this.w_ANDESCRI,"","0","0","0","0","1","0")
          select RITE1
        endif
        if nvl(this.w_PERCESTE,"")<> "S" 
          this.FileTelematico.RecordH("AU","001",NVL(ANCODFIS,""))     
        endif
        * --- Posizione 30 record posizionale
        this.w_ANFLSGRE = NVL(ANFLSGRE,"")
        if NVL(this.w_PERCESTE,"")="S" AND this.w_ANFLSGRE="S"
          this.w_PERCIPCF = space(16)
        else
          this.w_PERCIPCF = IIF(Not Empty(this.w_CFPEREST),upper(left(this.w_CFPEREST,16)),NVL(ANCODFIS,""))
        endif
        this.FileTelematico.RecordH("AU","002",iif(ANPERFIS="S",upper(NVL(ANCOGNOM,"")),upper(NVL(ANDESCRI,""))))     
        this.FileTelematico.RecordH("AU","003",iif(ANPERFIS="S",upper(NVL(AN__NOME,"")),""))     
        this.FileTelematico.RecordH("AU","004",iif(ANPERFIS="S",upper(NVL(AN_SESSO,"")),""))     
        this.FileTelematico.RecordH("AU","005",iif(ANPERFIS="S",NVL(ANDATNAS,CTOD("  -  -  ")),""))     
        this.FileTelematico.RecordH("AU","006",iif(ANPERFIS="S",upper(NVL(ANLOCNAS,"")),""))     
        this.FileTelematico.RecordH("AU","007",iif(ANPERFIS="S",upper(NVL(ANPRONAS,"")),""))     
        this.w_OLDPERCF = NVL(ANCODFIS,"")
        this.w_OLDPER = NVL(MRCODCON,"")
        this.w_PRIMOPERC = .F.
        this.w_PROVIN = UPPER(NVL(ANPROVIN,""))
        * --- Leggo i dati della residenza
        * --- Select from DES_DIVE
        i_nConn=i_TableProp[this.DES_DIVE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DES_DIVE_idx,2],.t.,this.DES_DIVE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select DDLOCALI,DDPROVIN,DDCODNAZ,DDINDIRI  from "+i_cTable+" DES_DIVE ";
              +" where DDTIPCON='F' and DDCODICE="+cp_ToStrODBC(this.w_MRCODCON)+" and DDTIPRIF = 'RE' ";
               ,"_Curs_DES_DIVE")
        else
          select DDLOCALI,DDPROVIN,DDCODNAZ,DDINDIRI from (i_cTable);
           where DDTIPCON="F" and DDCODICE=this.w_MRCODCON and DDTIPRIF = "RE" ;
            into cursor _Curs_DES_DIVE
        endif
        if used('_Curs_DES_DIVE')
          select _Curs_DES_DIVE
          locate for 1=1
          do while not(eof())
          this.w_LOCALEST = _Curs_DES_DIVE.DDLOCALI
          this.w_INDIREST = _Curs_DES_DIVE.DDINDIRI
          this.w_NAZIOEST = _Curs_DES_DIVE.DDCODNAZ
          this.w_PROVIN = _Curs_DES_DIVE.DDPROVIN
          Exit
            select _Curs_DES_DIVE
            continue
          enddo
          use
        endif
        Select RITE1
        * --- Riservato ai percipienti esteri
        if this.w_PERCESTE = "S"
          if this.w_EREDE = "S"
            this.FileTelematico.RecordH("AU","012","               1")     
          endif
          if this.w_EVEECCEZ <> "0"
            this.FileTelematico.RecordH("AU","013",this.w_EVEECCEZ)     
          endif
          * --- Gestisco i campi riservati al percipiente estero
          * --- Codice Fiscale estero
          this.FileTelematico.RecordH("AU","014",IIF(NOT EMPTY(this.w_CFPEREST),UPPER(this.w_CFPEREST),UPPER(NVL(ANCODFIS,""))))     
          * --- Localit� di residenza estera
          this.FileTelematico.RecordH("AU","015",UPPER(this.w_LOCALEST))     
          * --- Via e numero civico
          this.FileTelematico.RecordH("AU","016",UPPER(this.w_INDIREST))     
          * --- Codice stato estero
          * --- Read from NAZIONI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.NAZIONI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.NAZIONI_idx,2],.t.,this.NAZIONI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "NACODEST"+;
              " from "+i_cTable+" NAZIONI where ";
                  +"NACODNAZ = "+cp_ToStrODBC(this.w_NAZIOEST);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              NACODEST;
              from (i_cTable) where;
                  NACODNAZ = this.w_NAZIOEST;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_NACODEST = NVL(cp_ToDate(_read_.NACODEST),cp_NullValue(_read_.NACODEST))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.FileTelematico.RecordH("AU","017",space(13)+RIGHT(repl("0",3)+UPPER(alltrim(this.w_NACODEST)),3))     
        else
          this.FileTelematico.RecordH("AU","008",upper(NVL(this.w_LOCALEST,"")))     
          this.FileTelematico.RecordH("AU","009",upper(this.w_PROVIN))     
          this.FileTelematico.RecordH("AU","011",upper(NVL(this.w_INDIREST,"")))     
          if this.w_EREDE = "S"
            this.FileTelematico.RecordH("AU","012","               1")     
          endif
          if this.w_EVEECCEZ <> "0"
            this.FileTelematico.RecordH("AU","013",this.w_EVEECCEZ)     
          endif
        endif
      endif
      this.w_CAUPRE = UPPER(NVL(TRCAUPRE," "))
      * --- Dati relativi alle somme erogate
      this.FileTelematico.RecordH("AU","018",upper(NVL(TRCAUPRE,"")))     
      this.FileTelematico.RecordH("AU","019","            "+alltrim(str(year(this.oParentObject.w_DATFIN))))     
      this.FileTelematico.RecordH("AU","021",IIF(NVL(SOMTOTA,0) < 0,0,NVL(SOMTOTA,0)))     
      if this.w_PERCESTE = "S" and ANFLSGRE="S"
        this.FileTelematico.RecordH("AU","022",IIF(NVL(NONSOGG,0) < 0,0,NVL(NONSOGG,0)))     
      else
        this.FileTelematico.RecordH("AU","023",IIF(NVL(NONSOGG,0) < 0,0,NVL(NONSOGG,0)))     
      endif
      this.FileTelematico.RecordH("AU","024",IIF(NVL(IMPONI,0) < 0,0,NVL(IMPONI,0)))     
      this.FileTelematico.RecordH("AU","025",IIF(NVL(MRTOTIM1,0) < 0,0,NVL(MRTOTIM1,0)))     
      if this.w_CAUPRE $ "M-V-C"
        select MRCODCON,SUM(MRTOTIM2) as MRTOTIM2,SUM(MRIMPPER) as MRIMPPER,TRCAUPRE ; 
 from RITE1 where TRCAUPRE=this.w_CAUPRE and MRCODCON=this.w_MRCODCON group by MRCODCON,TRCAUPRE into cursor TOTPRE
        this.FileTelematico.RecordH("AU","033",IIF(NVL(TOTPRE.MRTOTIM2,0) -NVL(TOTPRE.MRIMPPER,0) < 0, 0,NVL(TOTPRE.MRTOTIM2,0) -NVL(TOTPRE.MRIMPPER,0)),.F.,.T.)     
        this.FileTelematico.RecordH("AU","034",IIF(NVL(TOTPRE.MRIMPPER,0) < 0,0,NVL(TOTPRE.MRIMPPER,0)),.F.,.T.)     
        * --- Elimino il cursore dei totali per causale prestazione
        if used("TOTPRE")
          select TOTPRE
          use
        endif
      endif
      Select RITE1
      this.FileTelematico.RecordH("AU","035",IIF(NVL(MRSPERIM,0) < 0,0,NVL(MRSPERIM,0)),.F.,.T.)     
      if this.w_CAUPRE $ "X-Y" AND NVL(RITERIMB,0) < 0
        this.FileTelematico.RecordH("AU","036",ABS(RITERIMB),.F.,.T.)     
      endif
      * --- Gestioni importi pre e post fallimento
      if ! empty(this.oParentObject.w_RFDATFAL)
        this.FileTelematico.RecordH("AU","037",IIF(NVL(IMPREFALL,0) < 0,0,NVL(IMPREFALL,0)))     
        this.FileTelematico.RecordH("AU","038",IIF(NVL(IMPOSFALL,0) < 0,0,NVL(IMPOSFALL,0)))     
      endif
      * --- Incremento il numero di Rigo
      this.FileTelematico.nRigoAU = this.FileTelematico.nRigoAU + 1
      if this.FileTelematico.nRigoAU > 5
        * --- Incremento il numero di Modello
        this.FileTelematico.nRigoAU = 1
        this.FileTelematico.nModAU = this.FileTelematico.nModAU + 1
        skip in RITE1
        exit
      endif
      skip in RITE1
    enddo
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Record E - Quadro SE
    select RITE2
    do while !eof()
      select RITE2
      this.FileTelematico.RecordE("SE","001",ANCODFIS)     
      this.FileTelematico.RecordE("SE","002",iif(ANPERFIS="S",ANCOGNOM,ANDESCRI))     
      this.FileTelematico.RecordE("SE","003",iif(ANPERFIS="S",AN__NOME,""))     
      this.FileTelematico.RecordE("SE","004",iif(ANPERFIS="S",AN_SESSO,""))     
      this.FileTelematico.RecordE("SE","005",iif(ANPERFIS="S",ANDATNAS,""))     
      this.FileTelematico.RecordE("SE","006",iif(ANPERFIS="S",ANLOCNAS,""))     
      this.FileTelematico.RecordE("SE","007",iif(ANPERFIS="S",ANPRONAS,""))     
      this.FileTelematico.RecordE("SE","008",ANLOCALI)     
      this.FileTelematico.RecordE("SE","009",ANPROVIN)     
      this.FileTelematico.RecordE("SE","010",ANINDIRI)     
      * --- Incremento il numero di Rigo
      this.FileTelematico.nRigoSE = this.FileTelematico.nRigoSE + 1
      if this.FileTelematico.nRigoSE > 10
        * --- Incremento il numero di Modello
        this.FileTelematico.nRigoSE = 2
        this.FileTelematico.nModSE = this.FileTelematico.nModSE + 1
        exit
      endif
      skip in RITE2
    enddo
  endproc


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Record E - Quadro SS
    * --- Quadro SS (SC)
    select count(*) from RITE1 group by ANCODFIS into cursor _quadross_
    this.w_NumPerSC = _tally
    select sum(SOMTOTA) as tSOMTOTA,sum(NONSOGG) as tNONSOGG,sum(IMPONI) as tIMPONI,sum(MRTOTIM1) as tMRTOTIM1 ;
    from RITE1 into cursor _quadross_
    this.w_tSOMTOTASC = _quadross_.tSOMTOTA
    this.w_tNONSOGGSC = _quadross_.tNONSOGG
    this.w_tIMPONISC = _quadross_.tIMPONI
    this.w_tMRTOTIM1SC = _quadross_.tMRTOTIM1
    * --- Quadro SS (SE)
    select count(*) from RITE2 group by ANCODFIS into cursor _quadross_
    this.w_NumPerSE = _tally
    select sum(SOMTOTA) as tSOMTOTA,sum(IMPONI) as tIMPONI,sum(MRTOTIM1) as tMRTOTIM1 from RITE2 into cursor _quadross_
    this.w_tSOMTOTASE = _quadross_.tSOMTOTA
    this.w_tIMPONISE = _quadross_.tIMPONI
    this.w_tMRTOTIM1SE = _quadross_.tMRTOTIM1
    if used("_quadross_")
      select _quadross_
      use
    endif
    this.FileTelematico.RecordE("SS003","001",this.w_NumPerSC,.t.)     
    this.FileTelematico.RecordE("SS003","002",this.w_tSOMTOTASC,.t.)     
    this.FileTelematico.RecordE("SS003","003",this.w_tNONSOGGSC,.t.)     
    this.FileTelematico.RecordE("SS003","005",this.w_tIMPONISC,.t.)     
    this.FileTelematico.RecordE("SS003","006",this.w_tMRTOTIM1SC,.t.)     
    this.FileTelematico.RecordE("SS005","001",this.w_NumPerSE,.t.)     
    this.FileTelematico.RecordE("SS005","002",this.w_tSOMTOTASE,.t.)     
    this.FileTelematico.RecordE("SS005","005",this.w_tIMPONISE,.t.)     
    this.FileTelematico.RecordE("SS005","006",this.w_tMRTOTIM1SE,.t.)     
  endproc


  procedure Pag6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Record E - Quadro ST
    select RITE3
    do while !eof()
      select RITE3
      * --- Regime agevolato
      this.w_REGAGE = NVL(VPREGAGE,"")
      * --- Se c'� il flag regime agevolato il quadro ST non deve essere stampato
      if this.w_REGAGE<>"S"
        this.FileTelematico.RecordE("ST","001",iif("A"$VP__NOTE OR "B"$VP__NOTE OR "M"$VP__NOTE OR "D"$VP__NOTE OR "E"$VP__NOTE OR ALLTRIM(MRCODTRI)="1713","          "+"122006","          "+right("00"+alltrim(str(MESE)),2)+str(ANNO,4,0)))     
        this.w_CALCOLI = SOMTOTA-VPRITECC-VPRITCOM
        if VPCAUPRE # "X" and VPCAUPRE # "Y" and (SOMTOTA < 0 or VPRITECC < 0 or VPRITCOM < 0 or this.w_CALCOLI < 0 or VPIMPINT < 0)
          this.w_MESS = "Attenzione: presenza di importi negativi nel prospetto st."+CHR(13)+"Impossibile generare il file"
          ah_ErrorMsg(this.w_MESS,"stop","")
          this.FileTelematico.CancellaFile()     
          i_retcode = 'stop'
          return
        endif
        this.FileTelematico.RecordE("ST","002",IIF(SOMTOTA < 0,0,SOMTOTA))     
        if !("M"$VP__NOTE)
          this.FileTelematico.RecordE("ST","003",IIF(VPRITECC < 0,0,VPRITECC))     
        endif
        if !("M"$VP__NOTE)
          this.FileTelematico.RecordE("ST","004",IIF(VPRITCOM < 0,0,VPRITCOM))     
        endif
        this.FileTelematico.RecordE("ST","005",IIF(VPIMPVER < 0,0,VPIMPVER))     
        this.FileTelematico.RecordE("ST","006",IIF(VPIMPINT < 0,0,VPIMPINT))     
        * --- Ravvedimento
        if VPRAVOPE="S"
          this.FileTelematico.RecordE("ST","007","               1")     
        endif
        this.FileTelematico.RecordE("ST","008",VP__NOTE)     
        this.FileTelematico.RecordE("ST","009",RIGHT("0000"+ALLTRIM(MRCODTRI),4)+space(12),.f.,.t.)     
        if VPTIPVER = "T"
          this.FileTelematico.RecordE("ST","010","               1")     
        endif
        this.FileTelematico.RecordE("ST","011",iif(VPCODREG<>0,"              "+right("00"+iif(vpcodreg>9,STR(VPCODREG,2,0),str(vpcodreg,1,0)),2),""))     
        * --- Campo 12
        if ! empty(nvl(VPDATVER,""))
          this.w_DataAppo = right("00"+alltrim(str(day(VPDATVER))),2)+right("00"+alltrim(str(month(VPDATVER))),2)+str(year(VPDATVER),4,0)
          this.FileTelematico.RecordE("ST","012",right("                "+alltrim(this.w_DataAppo),16))     
        endif
        * --- Incremento il numero di Rigo
        this.FileTelematico.nRigoST = this.FileTelematico.nRigoST + 1
        if this.FileTelematico.nRigoST > 13
          * --- Incremento il numero di Modello
          this.FileTelematico.nRigoST = 2
          this.FileTelematico.nModST = this.FileTelematico.nModST + 1
          skip in RITE3
          exit
        endif
        skip in RITE3
      else
        skip in RITE3
      endif
    enddo
  endproc


  procedure Pag7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Record A - Frontespizio
    this.FileTelematico.RecordA("A")     
    this.FileTelematico.RecordA(space(14))     
    this.FileTelematico.RecordA("77S07")     
    this.FileTelematico.RecordA(this.oParentObject.w_TIPFORN)     
    if empty(this.oParentObject.w_CODFISIN)
      if this.oParentObject.w_RFCODCAR = "2" OR this.oParentObject.w_RFCODCAR ="3" OR this.oParentObject.w_RFCODCAR ="4" OR this.oParentObject.w_RFCODCAR ="5" OR this.oParentObject.w_RFCODCAR ="7" OR this.oParentObject.w_RFCODCAR ="11"
        * --- Cod. fiscale firmatario
        this.FileTelematico.RecordA(this.oParentObject.w_RFCODFIS)     
      else
        * --- Cod. fiscale contribuente
        this.FileTelematico.RecordA(this.w_CFAzi)     
      endif
    else
      * --- Cod. fiscale intermediario
      this.FileTelematico.RecordA(this.oParentObject.w_CODFISIN)     
    endif
    * --- Spazio non utilizzato
    this.FileTelematico.RecordA(space(483))     
    * --- Dichiarazione su pi� invii
    this.FileTelematico.RecordA("0000")     
    this.FileTelematico.RecordA("0000")     
    * --- Chiudo la riga
    this.FileTelematico.RecordA(space(1897-len(this.FileTelematico.RigaA))+"A")     
    this.FileTelematico.RecordA("",.t.,.t.)     
  endproc


  procedure Pag8
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Campi aggiunti per 770/2007
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,6)]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='TRI_BUTI'
    this.cWorkTables[3]='VEP_RITE'
    this.cWorkTables[4]='CONTI'
    this.cWorkTables[5]='DES_DIVE'
    this.cWorkTables[6]='NAZIONI'
    return(this.OpenAllTables(6))

  proc CloseCursors()
    if used('_Curs_DES_DIVE')
      use in _Curs_DES_DIVE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- gsri5bft
  enddefine
  
  func NoPosValue(pQRC,pValore,flagEur,pRounded)
  local ValorePlus
  do case
  case type('pValore') = 'C'
    if !empty(pValore)
      if len(alltrim(pValore)) > 16 && scrivo il dato su pi� campi
        ValorePlus = pQRC+left(pValore,16)
        pValore = substr(pValore,17)
        do while !empty(alltrim(pValore))
          ValorePlus = ValorePlus + pQRC+'+'+left(pValore,15)+space(15-iif(len(pValore)>15,15,len(pValore)))
          pValore = substr(pValore,17)
        enddo
        return ValorePlus
      else
        return (pQRC+left(pValore+space(16),16))
      endif
    else
      return ''
    endif
  case type('pValore') = 'N'
    if pValore <> 0
      * troncamento delle migliaia
      if (flagEur = '1')
             * troncamento all'unit� di euro
             if pRounded
               * Arrotondo
               return (pQRC+right(space(16)+alltrim(str(cp_round(pValore,0))),16))
             else
               * Solo parte intera
               return (pQRC+right(space(16)+alltrim(str(int(pValore))),16))
             endif           
        else
             return (pQRC+right(space(16)+alltrim(str(int(pValore/1000))),16))
      endif
    else
      return ''
    endif
  case type('pValore') = 'D'
    if !empty(pValore)
      return (pQRC+right(space(16)+left(dtoc(pValore),2)+substr(dtoc(pValore),4,2)+right(dtoc(pValore),4),16))
    else
      return ''
    endif
  case type('pValore') = 'T'
    if !empty(pValore)
      return (pQRC+right(space(16)+left(dtoc(ttod(pValore)),2)+substr(dtoc(ttod(pValore)),4,2)+right(dtoc(ttod(pValore)),4),16))
    else
      return ''
    endif
  otherwise
    ah_Msg("Tipo dati non definito:"+type("pvalore"),.T.)
    return ''
  endcase
  endfunc
  
  define class FileTelematico as Custom
    oParentObject = .null.
    NomeFile = ''
    RigaA = ''
    RigaB = ''
    RigaE = ''
    PosRigaE = ''
    RigaH = ''
    PosRigaH = ''
    nRigoAU = 1 && Parte da 1
    nRigoSE = 2 && Parte da 2
    nRigoST = 2 && Parte da 2
    nModello = 1 
    nModelloE = 1
    nModelloH = 1
    nModEOverChar = 0
    nModHOverChar = 0
    nModelli = 1
    nModAU = 1
    nModSE = 1
    nModST = 1
    nRecTipoE = 1
    nRecTipoH = 1
  
  proc Init(pNomeFile,oParent)
    strtofile('',pNomeFile)
    this.NomeFile = pNomeFile
    this.oParentObject = oParent
  endproc
  
  proc RecordA(pValore, pACapo,pScrivi)
    this.RigaA =   this.RigaA + upper(pValore)
    if pACapo
        this.RigaA =   this.RigaA + chr(13) + chr(10)
    endif
    if pScrivi
      strtofile(this.RigaA,this.NomeFile,.t.)
      this.RigaA = ''
    endif
  endproc
  
  proc RecordB(pValore, pACapo,pScrivi)
    this.RigaB =   this.RigaB + upper(pValore)
    if pACapo
        this.RigaB =   this.RigaB + chr(13) + chr(10)
    endif
    if pScrivi
      strtofile(this.RigaB,this.NomeFile,.t.)
      this.RigaB = ''
    endif
  endproc
  
  proc RecordEpos &&campi posizionali
    this.PosRigaE = 'E'
    this.PosRigaE = this.PosRigaE + this.oParentObject.w_CFAzi
    this.PosRigaE = this.PosRigaE + right('00000000' + alltrim(str(this.nModelloE)),8) && numero modulo
    this.PosRigaE = this.PosRigaE + space(3)
    this.PosRigaE = this.PosRigaE + right(' '+this.oParentObject.w_OPERAZ,1)
    this.PosRigaE = this.PosRigaE + space(24)
    this.PosRigaE = this.PosRigaE + space(20)  
    this.PosRigaE = this.PosRigaE + '05006900962     ' && PIVA ZUCCHETTI
  endproc
  
  proc RecordE(pQ,pC,pValore,pRigaFissa,pNX)
  local nRiga
  if !pRigaFissa
    nRiga = 'this.nRigo'+pQ
    this.RigaE =   this.RigaE + NoPosValue(pQ+right('000'+alltrim(Str(&nRiga)),3)+pC,pValore,oParentObject.w_FLAGEURO)
  else
    if !pNX
      this.RigaE =   this.RigaE + NoPosValue(pQ+pC,pValore,this.oParentObject.w_FLAGEURO)
    else
      this.RigaE =   this.RigaE + pValore
    endif
  endif
  endproc
  
  proc ScriviRecordE
    this.RecordEpos()
    strtofile(this.PosRigaE+left(this.RigaE,1800)+     space(iif(int(len(this.RigaE)/1800)=0,1800-len(this.RigaE)+8,8))+     'A'+chr(13)+chr(10),this.NomeFile,.t.)
    do while len(alltrim(this.RigaE)) > 1800
       this.RigaE = substr(this.RigaE,1801)
       strtofile(this.PosRigaE+left(this.RigaE,1800)+space(iif(int(len(this.RigaE)/1800)=0,1800-len(this.RigaE)+8,8))+'A'+chr(13)+chr(10),this.NomeFile,.t.)
       this.nRecTipoE = this.nRecTipoE + 1
       this.nModEOverChar = this.nModEOverChar + 1  
    enddo
    this.RigaE = ''
  endproc
  
  proc RecordHpos &&campi posizionali
    this.PosRigaH = 'H'
    this.PosRigaH = this.PosRigaH + this.oParentObject.w_CFAzi
    this.PosRigaH = this.PosRigaH + right('00000000' + alltrim(str(this.nModelloH)),8) && numero modulo
    this.PosRigaH = this.PosRigaH + space(3)
    this.PosRigaH = this.PosRigaH + right(' '+this.oParentObject.w_OPERAZ,1)
    this.PosRigaH = this.PosRigaH + this.oParentObject.w_PERCIPCF &&CF del Percipiente
    this.PosRigaH = this.PosRigaH + space(28)  
    this.PosRigaH = this.PosRigaH + '05006900962     ' && PIVA ZUCCHETTI
  endproc
  
  
  proc RecordH(pQ,pC,pValore,pRigaFissa,Rounded)
  local nRiga
  if !pRigaFissa
    if val(pc) > 11
        nRiga = 'this.nRigo'+pQ
    else 
        nRiga='1'
    endif    
    this.RigaH =   this.RigaH + NoPosValue(pQ+right('000'+alltrim(Str(&nRiga)),3)+pC,pValore,oParentObject.w_FLAGEURO,Rounded)
  else
    this.RigaH =   this.RigaH + NoPosValue(pQ+pC,pValore,oParentObject.w_FLAGEURO,Rounded)
  endif
  endproc
  
  
  proc ScriviRecordH
    this.RecordHpos()
    strtofile(this.PosRigaH+left(this.RigaH,1800)+     space(iif(int(len(this.RigaH)/1800)=0,1800-len(this.RigaH)+8,8))+     'A'+chr(13)+chr(10),this.NomeFile,.t.)
    do while len(alltrim(this.RigaH)) > 1800
       this.RigaH = substr(this.RigaH,1801)
       strtofile(this.PosRigaH+left(this.RigaH,1800)+space(iif(int(len(this.RigaH)/1800)=0,1800-len(this.RigaH)+8,8))+'A'+chr(13)+chr(10),this.NomeFile,.t.)
       this.nRecTipoH = this.nRecTipoH + 1
       this.nModelloH = this.nModelloH + 1
       this.nModHOverChar = this.nModHOverChar + 1
    enddo
    this.RigaH = ''
  endproc
  
  Proc CancellaFile
    erase(this.NomeFile)
  endproc  
  
  Proc RecordZ
  local Riga
    Riga = 'Z'
    Riga = Riga + space(14)
    Riga = Riga + '000000001'
    Riga = Riga +  right('000000000' + alltrim(str(this.nModelloE+this.nModEOverChar-1)),9)
    Riga = Riga + '000000000' && Numero record tipo F
    Riga = Riga + '000000000' && Numero record tipo G
    Riga = Riga +  right('000000000' + alltrim(str(this.nModelloH+this.nModHOverChar-1)),9)
    Riga = Riga + space(1837)
    Riga = Riga + 'A'
    Riga = Riga + chr(13) + chr(10)
    strtofile(Riga,this.NomeFile,.t.)
  endproc
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
