* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri1sdh                                                        *
*              Aggiornamento certificazioni                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2016-01-21                                                      *
* Last revis.: 2016-02-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsri1sdh",oParentObject))

* --- Class definition
define class tgsri1sdh as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 376
  Height = 231
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-02-15"
  HelpContextID=77067881
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=2

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsri1sdh"
  cComment = "Aggiornamento certificazioni"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CHDATSTA = ctod('  /  /  ')
  w_CONFERMA = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsri1sdhPag1","gsri1sdh",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCHDATSTA_1_6
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CHDATSTA=ctod("  /  /  ")
      .w_CONFERMA=space(1)
      .w_CHDATSTA=oParentObject.w_CHDATSTA
      .w_CONFERMA=oParentObject.w_CONFERMA
    endwith
    this.DoRTCalc(1,2,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_CHDATSTA=.w_CHDATSTA
      .oParentObject.w_CONFERMA=.w_CONFERMA
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,2,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCHDATSTA_1_6.value==this.w_CHDATSTA)
      this.oPgFrm.Page1.oPag.oCHDATSTA_1_6.value=this.w_CHDATSTA
    endif
    if not(this.oPgFrm.Page1.oPag.oCONFERMA_1_7.value==this.w_CONFERMA)
      this.oPgFrm.Page1.oPag.oCONFERMA_1_7.value=this.w_CONFERMA
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CHDATSTA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCHDATSTA_1_6.SetFocus()
            i_bnoObbl = !empty(.w_CHDATSTA)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_CONFERMA)) or not(.w_CONFERMA $ 'SN'))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCONFERMA_1_7.SetFocus()
            i_bnoObbl = !empty(.w_CONFERMA)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare S o N per confermare o meno l'elaborazione")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsri1sdhPag1 as StdContainer
  Width  = 372
  height = 231
  stdWidth  = 372
  stdheight = 231
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCHDATSTA_1_6 as StdField with uid="VHPIDJIFCK",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CHDATSTA", cQueryName = "CHDATSTA",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 202533273,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=193, Top=127

  add object oCONFERMA_1_7 as StdField with uid="JXSKASAOKO",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CONFERMA", cQueryName = "CONFERMA",;
    bObbl = .t. , nPag = 1, value=space(1), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare S o N per confermare o meno l'elaborazione",;
    ToolTipText = "S per confermare N per annullare",;
    HelpContextID = 33766759,;
   bGlobalFont=.t.,;
    Height=21, Width=16, Left=193, Top=177, cSayPict='"!"', cGetPict='"!"', InputMask=replicate('X',1)

  func oCONFERMA_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_CONFERMA $ 'SN')
    endwith
    return bRes
  endfunc


  add object oBtn_1_9 as StdButton with uid="YAULKWWGND",left=275, top=177, width=48,height=45,;
    CpPicture="bmp\ok.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 77039130;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_1 as StdString with uid="ENHICDEWMW",Visible=.t., Left=37, Top=11,;
    Alignment=2, Width=241, Height=18,;
    Caption="Certificazioni uniche"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_2 as StdString with uid="XSROSVSNBI",Visible=.t., Left=11, Top=40,;
    Alignment=0, Width=75, Height=15,;
    Caption="Attenzione"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_3 as StdString with uid="WHMQGVPNJD",Visible=.t., Left=29, Top=62,;
    Alignment=0, Width=305, Height=18,;
    Caption="Confermando l'aggiornamento le singole certificazioni"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="ZWWOBFCXPJ",Visible=.t., Left=29, Top=88,;
    Alignment=0, Width=324, Height=18,;
    Caption="diverranno definitive e verr� valorizzata la data di stampa"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="WIDBQJJJEA",Visible=.t., Left=67, Top=129,;
    Alignment=1, Width=122, Height=18,;
    Caption="Data di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="XFWAAGAXDQ",Visible=.t., Left=10, Top=177,;
    Alignment=1, Width=179, Height=15,;
    Caption="Confermi aggiornamento (S/N)?:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsri1sdh','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
