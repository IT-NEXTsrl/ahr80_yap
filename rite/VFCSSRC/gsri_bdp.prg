* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_bdp                                                        *
*              Elimina rif.partita                                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][98][VRS_26]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-05-15                                                      *
* Last revis.: 2006-01-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsri_bdp",oParentObject)
return(i_retval)

define class tgsri_bdp as StdBatch
  * --- Local variables
  w_MRRIFCON = space(10)
  w_MRRIFORD = 0
  w_MRRIFNUM = 0
  w_RIFCON = space(10)
  w_RIFORD = 0
  w_RIFNUM = 0
  * --- WorkFile variables
  PAR_TITE_idx=0
  MOV_RITE_idx=0
  VEP_RITE_idx=0
  VEN_RITE_idx=0
  DEP_RITE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Toglie Riferimento partita Associata (da GSRI_MDP)
    * --- Elimina dettagli Partite
    this.w_MRRIFORD = this.oParentObject.oParentObject.w_MRRIFORD
    this.w_MRRIFNUM = this.oParentObject.oParentObject.w_MRRIFNUM
    this.w_MRRIFCON = this.oParentObject.oParentObject.w_MRRIFCON
    if this.w_MRRIFORD=0 AND this.w_MRRIFNUM=0
      SELECT (this.oParentObject.cTrsName)
      this.w_RIFCON = t_DPRIFCON
      this.w_RIFORD = t_DPRIFORD
      this.w_RIFNUM = t_DPRIFNUM
      * --- Write into PAR_TITE
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.PAR_TITE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_TITE_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"PTFLRITE ="+cp_NullLink(cp_ToStrODBC(0),'PAR_TITE','PTFLRITE');
            +i_ccchkf ;
        +" where ";
            +"PTSERIAL = "+cp_ToStrODBC(this.w_RIFCON);
            +" and PTROWORD = "+cp_ToStrODBC(this.w_RIFORD);
            +" and CPROWNUM = "+cp_ToStrODBC(this.w_RIFNUM);
               )
      else
        update (i_cTable) set;
            PTFLRITE = 0;
            &i_ccchkf. ;
         where;
            PTSERIAL = this.w_RIFCON;
            and PTROWORD = this.w_RIFORD;
            and CPROWNUM = this.w_RIFNUM;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='PAR_TITE'
    this.cWorkTables[2]='MOV_RITE'
    this.cWorkTables[3]='VEP_RITE'
    this.cWorkTables[4]='VEN_RITE'
    this.cWorkTables[5]='DEP_RITE'
    return(this.OpenAllTables(5))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
