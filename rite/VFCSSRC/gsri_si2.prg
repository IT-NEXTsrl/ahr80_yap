* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_si2                                                        *
*              Zoom documenti                                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_19]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-05-16                                                      *
* Last revis.: 2007-12-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsri_si2",oParentObject))

* --- Class definition
define class tgsri_si2 as StdForm
  Top    = 11
  Left   = 101

  * --- Standard Properties
  Width  = 315
  Height = 190
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-12-14"
  HelpContextID=258602601
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=7

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsri_si2"
  cComment = "Zoom documenti"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_GIAVER2 = space(10)
  w_STATO2 = space(10)
  w_NREGIS2 = 0
  w_NDISTI2 = space(10)
  w_ESERCI2 = space(4)
  w_DATADI2 = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  w_RAGRU = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsri_si2Pag1","gsri_si2",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_RAGRU = this.oPgFrm.Pages(1).oPag.RAGRU
    DoDefault()
    proc Destroy()
      this.w_RAGRU = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_GIAVER2=space(10)
      .w_STATO2=space(10)
      .w_NREGIS2=0
      .w_NDISTI2=space(10)
      .w_ESERCI2=space(4)
      .w_DATADI2=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_GIAVER2=oParentObject.w_GIAVER2
      .w_STATO2=oParentObject.w_STATO2
      .w_NREGIS2=oParentObject.w_NREGIS2
      .w_NDISTI2=oParentObject.w_NDISTI2
      .w_ESERCI2=oParentObject.w_ESERCI2
      .w_DATADI2=oParentObject.w_DATADI2
      .w_DATFIN=oParentObject.w_DATFIN
      .oPgFrm.Page1.oPag.RAGRU.Calculate()
          .DoRTCalc(1,2,.f.)
        .w_NREGIS2 = Nvl(.w_RAGRU.getVar('VPNUMREG'),0)
        .w_NDISTI2 = Nvl(.w_RAGRU.getVar('VPSERIAL'),Space(10))
        .w_ESERCI2 = Nvl( .w_RAGRU.getVar('VPCODESE') ,Space(4))
        .w_DATADI2 = Nvl(.w_RAGRU.getVar('VPDATREG'),cp_CharToDate('  /  /    '))
      .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
        .w_DATFIN = Nvl(.w_RAGRU.getVar('VPDATFIN'),cp_CharToDate('  /  /    '))
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_GIAVER2=.w_GIAVER2
      .oParentObject.w_STATO2=.w_STATO2
      .oParentObject.w_NREGIS2=.w_NREGIS2
      .oParentObject.w_NDISTI2=.w_NDISTI2
      .oParentObject.w_ESERCI2=.w_ESERCI2
      .oParentObject.w_DATADI2=.w_DATADI2
      .oParentObject.w_DATFIN=.w_DATFIN
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.RAGRU.Calculate()
        .DoRTCalc(1,2,.t.)
            .w_NREGIS2 = Nvl(.w_RAGRU.getVar('VPNUMREG'),0)
            .w_NDISTI2 = Nvl(.w_RAGRU.getVar('VPSERIAL'),Space(10))
            .w_ESERCI2 = Nvl( .w_RAGRU.getVar('VPCODESE') ,Space(4))
            .w_DATADI2 = Nvl(.w_RAGRU.getVar('VPDATREG'),cp_CharToDate('  /  /    '))
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
            .w_DATFIN = Nvl(.w_RAGRU.getVar('VPDATFIN'),cp_CharToDate('  /  /    '))
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.RAGRU.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.RAGRU.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_8.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsri_si2Pag1 as StdContainer
  Width  = 311
  height = 190
  stdWidth  = 311
  stdheight = 190
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object RAGRU as cp_zoombox with uid="YBLDZRIWOW",left=5, top=4, width=300,height=182,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="VEN_RITE",cZoomFile="INP3",bOptions=.t.,bAdvOptions=.t.,bReadOnly=.t.,bQueryOnLoad=.t.,cZoomOnZoom="",bRetriveAllRows=.f.,cMenuFile="",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 187830502


  add object oObj_1_8 as cp_runprogram with uid="HCMJQTZUHO",left=2, top=195, width=205,height=17,;
    caption='GSRI_BCM',;
   bGlobalFont=.t.,;
    prg="GSRI_BCM",;
    cEvent = "w_ragru selected",;
    nPag=1;
    , HelpContextID = 148144563
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsri_si2','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
