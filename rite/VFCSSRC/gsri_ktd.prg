* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_ktd                                                        *
*              Compilazione GLA                                                *
*                                                                              *
*      Author: TAM Software & CODELAB                                          *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [146] [VRS_75]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-04-06                                                      *
* Last revis.: 2009-12-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsri_ktd",oParentObject))

* --- Class definition
define class tgsri_ktd as StdForm
  Top    = 23
  Left   = 35

  * --- Standard Properties
  Width  = 607
  Height = 238+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-12-18"
  HelpContextID=82441833
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=16

  * --- Constant Properties
  _IDX = 0
  VALUTE_IDX = 0
  ESERCIZI_IDX = 0
  AZIENDA_IDX = 0
  cPrg = "gsri_ktd"
  cComment = "Compilazione GLA"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CODAZI = space(5)
  w_ANNO = 0
  w_DATFIN = ctod('  /  /  ')
  w_ESERCI = space(4)
  w_VALNAZ = space(3)
  w_ISTAT = 0
  w_ISTAT1 = 0
  w_DENUNCIA = space(1)
  w_INCLTRIM = space(1)
  w_NOMEFILE = space(40)
  w_RAGSOC = space(50)
  w_CODPART = space(16)
  w_COGNOM = space(40)
  w_TELEFO = space(15)
  w_EMAIL = space(30)
  w_AZ_EMAIL = space(50)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsri_ktdPag1","gsri_ktd",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Compilazione GLA")
      .Pages(2).addobject("oPag","tgsri_ktdPag2","gsri_ktd",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Dati utente")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oANNO_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='VALUTE'
    this.cWorkTables[2]='ESERCIZI'
    this.cWorkTables[3]='AZIENDA'
    return(this.OpenAllTables(3))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI=space(5)
      .w_ANNO=0
      .w_DATFIN=ctod("  /  /  ")
      .w_ESERCI=space(4)
      .w_VALNAZ=space(3)
      .w_ISTAT=0
      .w_ISTAT1=0
      .w_DENUNCIA=space(1)
      .w_INCLTRIM=space(1)
      .w_NOMEFILE=space(40)
      .w_RAGSOC=space(50)
      .w_CODPART=space(16)
      .w_COGNOM=space(40)
      .w_TELEFO=space(15)
      .w_EMAIL=space(30)
      .w_AZ_EMAIL=space(50)
        .w_CODAZI = i_Codazi
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODAZI))
          .link_1_1('Full')
        endif
        .w_ANNO = year(i_datsys)
        .w_DATFIN = cp_CharToDate('31-12-2000')
        .w_ESERCI = calceser(.w_DATFIN, space(4))
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_ESERCI))
          .link_1_4('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_14.Calculate(AH_Msgformat(IIF(.w_ANNO>2000,'Euro',IIF(.w_VALNAZ=G_CODLIR,'Lire','Euro'))))
          .DoRTCalc(5,7,.f.)
        .w_DENUNCIA = 'P'
          .DoRTCalc(9,9,.f.)
        .w_NOMEFILE = left(sys(5)+sys(2003)+"\INPSCO"+space(40),40)
    endwith
    this.DoRTCalc(11,16,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
            .w_CODAZI = i_Codazi
          .link_1_1('Full')
        .DoRTCalc(2,3,.t.)
          .link_1_4('Full')
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate(AH_Msgformat(IIF(.w_ANNO>2000,'Euro',IIF(.w_VALNAZ=G_CODLIR,'Lire','Euro'))))
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(5,16,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate(AH_Msgformat(IIF(.w_ANNO>2000,'Euro',IIF(.w_VALNAZ=G_CODLIR,'Lire','Euro'))))
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_20.enabled = this.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oINCLTRIM_1_16.visible=!this.oPgFrm.Page1.oPag.oINCLTRIM_1_16.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_14.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZ_EMAIL";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_CODAZI)
            select AZCODAZI,AZ_EMAIL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.AZCODAZI,space(5))
      this.w_AZ_EMAIL = NVL(_Link_.AZ_EMAIL,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_AZ_EMAIL = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ESERCI
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ESERCI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ESERCI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_ESERCI);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_ESERCI)
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ESERCI = NVL(_Link_.ESCODESE,space(4))
      this.w_VALNAZ = NVL(_Link_.ESVALNAZ,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_ESERCI = space(4)
      endif
      this.w_VALNAZ = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ESERCI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oANNO_1_2.value==this.w_ANNO)
      this.oPgFrm.Page1.oPag.oANNO_1_2.value=this.w_ANNO
    endif
    if not(this.oPgFrm.Page1.oPag.oISTAT_1_6.value==this.w_ISTAT)
      this.oPgFrm.Page1.oPag.oISTAT_1_6.value=this.w_ISTAT
    endif
    if not(this.oPgFrm.Page1.oPag.oISTAT1_1_7.value==this.w_ISTAT1)
      this.oPgFrm.Page1.oPag.oISTAT1_1_7.value=this.w_ISTAT1
    endif
    if not(this.oPgFrm.Page1.oPag.oDENUNCIA_1_15.RadioValue()==this.w_DENUNCIA)
      this.oPgFrm.Page1.oPag.oDENUNCIA_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oINCLTRIM_1_16.RadioValue()==this.w_INCLTRIM)
      this.oPgFrm.Page1.oPag.oINCLTRIM_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNOMEFILE_1_18.value==this.w_NOMEFILE)
      this.oPgFrm.Page1.oPag.oNOMEFILE_1_18.value=this.w_NOMEFILE
    endif
    if not(this.oPgFrm.Page2.oPag.oRAGSOC_2_6.value==this.w_RAGSOC)
      this.oPgFrm.Page2.oPag.oRAGSOC_2_6.value=this.w_RAGSOC
    endif
    if not(this.oPgFrm.Page2.oPag.oCODPART_2_7.value==this.w_CODPART)
      this.oPgFrm.Page2.oPag.oCODPART_2_7.value=this.w_CODPART
    endif
    if not(this.oPgFrm.Page2.oPag.oCOGNOM_2_8.value==this.w_COGNOM)
      this.oPgFrm.Page2.oPag.oCOGNOM_2_8.value=this.w_COGNOM
    endif
    if not(this.oPgFrm.Page2.oPag.oTELEFO_2_9.value==this.w_TELEFO)
      this.oPgFrm.Page2.oPag.oTELEFO_2_9.value=this.w_TELEFO
    endif
    if not(this.oPgFrm.Page2.oPag.oEMAIL_2_10.value==this.w_EMAIL)
      this.oPgFrm.Page2.oPag.oEMAIL_2_10.value=this.w_EMAIL
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_ANNO)) or not(.w_ANNO>=2000))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oANNO_1_2.SetFocus()
            i_bnoObbl = !empty(.w_ANNO)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("L'anno selezionato � incogruente")
          case   (empty(.w_ISTAT))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oISTAT_1_6.SetFocus()
            i_bnoObbl = !empty(.w_ISTAT)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_NOMEFILE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNOMEFILE_1_18.SetFocus()
            i_bnoObbl = !empty(.w_NOMEFILE)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsri_ktdPag1 as StdContainer
  Width  = 603
  height = 239
  stdWidth  = 603
  stdheight = 239
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oANNO_1_2 as StdField with uid="KFFBXFNENT",rtseq=2,rtrep=.f.,;
    cFormVar = "w_ANNO", cQueryName = "ANNO",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "L'anno selezionato � incogruente",;
    ToolTipText = "Anno dichiarazione del GLA (dall'anno 2000 in poi)",;
    HelpContextID = 76923898,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=221, Top=70, cSayPict='"9999"', cGetPict='"9999"'

  func oANNO_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_ANNO>=2000)
    endwith
    return bRes
  endfunc

  add object oISTAT_1_6 as StdField with uid="WIZMDYFHRK",rtseq=6,rtrep=.f.,;
    cFormVar = "w_ISTAT", cQueryName = "ISTAT",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Codice ISTAT dell'attivit� svolta",;
    HelpContextID = 258170490,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=221, Top=103

  add object oISTAT1_1_7 as StdField with uid="RRXUBTIKMS",rtseq=7,rtrep=.f.,;
    cFormVar = "w_ISTAT1", cQueryName = "ISTAT1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Codice ISTAT eventuale seconda attivit� svolta",;
    HelpContextID = 241393274,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=533, Top=103


  add object oObj_1_14 as cp_calclbl with uid="LTWWZTGHBR",left=533, top=70, width=67,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",;
    cEvent = "w_ANNO Changed",;
    nPag=1;
    , HelpContextID = 95555814


  add object oDENUNCIA_1_15 as StdCombo with uid="YJTGFNOZLE",rtseq=8,rtrep=.f.,left=221,top=136,width=86,height=21;
    , ToolTipText = "Tipo della denuncia ( P: principale S: sostitutiva)";
    , HelpContextID = 212847753;
    , cFormVar="w_DENUNCIA",RowSource=""+"Principale,"+"Sostitutiva", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDENUNCIA_1_15.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'S',;
    space(1))))
  endfunc
  func oDENUNCIA_1_15.GetRadio()
    this.Parent.oContained.w_DENUNCIA = this.RadioValue()
    return .t.
  endfunc

  func oDENUNCIA_1_15.SetRadio()
    this.Parent.oContained.w_DENUNCIA=trim(this.Parent.oContained.w_DENUNCIA)
    this.value = ;
      iif(this.Parent.oContained.w_DENUNCIA=='P',1,;
      iif(this.Parent.oContained.w_DENUNCIA=='S',2,;
      0))
  endfunc

  add object oINCLTRIM_1_16 as StdCheck with uid="OQQAZZFBYT",rtseq=9,rtrep=.f.,left=343, top=136, caption="Inclusione primo trimestre 2000",;
    ToolTipText = "Check di inclusione primo trimestre 2000",;
    HelpContextID = 223965997,;
    cFormVar="w_INCLTRIM", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oINCLTRIM_1_16.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oINCLTRIM_1_16.GetRadio()
    this.Parent.oContained.w_INCLTRIM = this.RadioValue()
    return .t.
  endfunc

  func oINCLTRIM_1_16.SetRadio()
    this.Parent.oContained.w_INCLTRIM=trim(this.Parent.oContained.w_INCLTRIM)
    this.value = ;
      iif(this.Parent.oContained.w_INCLTRIM=='S',1,;
      0)
  endfunc

  func oINCLTRIM_1_16.mHide()
    with this.Parent.oContained
      return (.w_ANNO>2000)
    endwith
  endfunc

  add object oNOMEFILE_1_18 as StdField with uid="CYDZFQRMOG",rtseq=10,rtrep=.f.,;
    cFormVar = "w_NOMEFILE", cQueryName = "NOMEFILE",;
    bObbl = .t. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Nome del file ASCII da generare",;
    HelpContextID = 121623013,;
   bGlobalFont=.t.,;
    Height=21, Width=355, Left=221, Top=167, InputMask=replicate('X',40)


  add object oBtn_1_19 as StdButton with uid="BVCEQIRHFY",left=581, top=167, width=20,height=18,;
    caption="...", nPag=1;
    , HelpContextID = 82240810;
  , bGlobalFont=.t.

    proc oBtn_1_19.Click()
      with this.Parent.oContained
        .w_NOMEFILE=left(cp_getdir(sys(5)+sys(2003),"Percorso di destinazione")+"INPSCO"+space(40),40)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_20 as StdButton with uid="MGXPFVZZOE",left=502, top=194, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per inizio elaborazione";
    , HelpContextID = 82413082;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_20.Click()
      with this.Parent.oContained
        do GSRI_BTD with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_20.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_ISTAT<>0)
      endwith
    endif
  endfunc


  add object oBtn_1_21 as StdButton with uid="WONLLGUZCO",left=553, top=194, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 75124410;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_21.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_8 as StdString with uid="MLKDHVQCFP",Visible=.t., Left=10, Top=15,;
    Alignment=0, Width=400, Height=15,;
    Caption="Trasmissione su supporto magnetico del modello GLA"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="KIGHOVQMNY",Visible=.t., Left=10, Top=36,;
    Alignment=0, Width=400, Height=15,;
    Caption="Contributo di cui all'art. 2 L. 8.8.95, n. 335."  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="OPBEOUOVMD",Visible=.t., Left=3, Top=70,;
    Alignment=1, Width=216, Height=15,;
    Caption="Anno corresponsione compenso:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="JNFSAEZVBB",Visible=.t., Left=3, Top=103,;
    Alignment=1, Width=216, Height=15,;
    Caption="Codice ISTAT attivit� svolta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="SNWGPJYHAH",Visible=.t., Left=277, Top=103,;
    Alignment=1, Width=251, Height=15,;
    Caption="Codice ISTAT seconda attivit� svolta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="TQPEOFDZLI",Visible=.t., Left=26, Top=136,;
    Alignment=1, Width=193, Height=15,;
    Caption="Tipo denuncia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="GISNWOVGWY",Visible=.t., Left=26, Top=167,;
    Alignment=1, Width=193, Height=15,;
    Caption="Nome file:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="RLUYYMVLIX",Visible=.t., Left=277, Top=67,;
    Alignment=1, Width=251, Height=18,;
    Caption="Valuta della dichiarazione:"  ;
  , bGlobalFont=.t.
enddefine
define class tgsri_ktdPag2 as StdContainer
  Width  = 603
  height = 239
  stdWidth  = 603
  stdheight = 239
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oRAGSOC_2_6 as StdField with uid="WWHRPQXZLB",rtseq=11,rtrep=.f.,;
    cFormVar = "w_RAGSOC", cQueryName = "RAGSOC",;
    bObbl = .f. , nPag = 2, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Ragione sociale dell'utente",;
    HelpContextID = 56475670,;
   bGlobalFont=.t.,;
    Height=21, Width=341, Left=224, Top=34, InputMask=replicate('X',50)

  add object oCODPART_2_7 as StdField with uid="ZFAELKQHSX",rtseq=12,rtrep=.f.,;
    cFormVar = "w_CODPART", cQueryName = "CODPART",;
    bObbl = .f. , nPag = 2, value=space(16), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale o parita IVA dell'utente",;
    HelpContextID = 24812838,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=224, Top=70, InputMask=replicate('X',16)

  add object oCOGNOM_2_8 as StdField with uid="JMZRZTGIYZ",rtseq=13,rtrep=.f.,;
    cFormVar = "w_COGNOM", cQueryName = "COGNOM",;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Cognome e nome persona di riferimento",;
    HelpContextID = 223923494,;
   bGlobalFont=.t.,;
    Height=21, Width=296, Left=224, Top=106, InputMask=replicate('X',40)

  add object oTELEFO_2_9 as StdField with uid="QZUVTNPRGH",rtseq=14,rtrep=.f.,;
    cFormVar = "w_TELEFO", cQueryName = "TELEFO",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Numero telefono di riferimento",;
    HelpContextID = 247469110,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=224, Top=142, InputMask=replicate('X',15)

  add object oEMAIL_2_10 as StdField with uid="BSLMGYGKBN",rtseq=15,rtrep=.f.,;
    cFormVar = "w_EMAIL", cQueryName = "EMAIL",;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo e-mail",;
    HelpContextID = 266114234,;
   bGlobalFont=.t.,;
    Height=21, Width=226, Left=224, Top=178, InputMask=replicate('X',30)

  add object oStr_2_1 as StdString with uid="STXDWSZPTO",Visible=.t., Left=40, Top=34,;
    Alignment=1, Width=181, Height=15,;
    Caption="Ragione sociale:"  ;
  , bGlobalFont=.t.

  add object oStr_2_2 as StdString with uid="EVLBVDYFWY",Visible=.t., Left=40, Top=70,;
    Alignment=1, Width=181, Height=15,;
    Caption="Codice fiscale o partita IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_2_3 as StdString with uid="MGZXULVTUU",Visible=.t., Left=9, Top=106,;
    Alignment=1, Width=212, Height=15,;
    Caption="Cognome e nome di riferimento:"  ;
  , bGlobalFont=.t.

  add object oStr_2_4 as StdString with uid="USTJQWWIWE",Visible=.t., Left=9, Top=142,;
    Alignment=1, Width=212, Height=15,;
    Caption="Numero telefono di riferimento:"  ;
  , bGlobalFont=.t.

  add object oStr_2_5 as StdString with uid="UJSJTVWAYY",Visible=.t., Left=40, Top=178,;
    Alignment=1, Width=181, Height=15,;
    Caption="Indirizzo E-mail:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsri_ktd','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
