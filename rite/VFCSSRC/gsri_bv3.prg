* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_bv3                                                        *
*              Cancellazione distinta IRPEF                                    *
*                                                                              *
*      Author: Zucchetti spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][98][VRS_8]                                               *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-01-10                                                      *
* Last revis.: 2006-01-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsri_bv3",oParentObject)
return(i_retval)

define class tgsri_bv3 as StdBatch
  * --- Local variables
  w_CHIAVE = space(10)
  w_SERIALE = space(10)
  * --- WorkFile variables
  VEP_RITE_idx=0
  VEN_RITE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch lanciato dal batch Gscg_Bvi.
    * --- Questo batch permetta la cancellazione del campo Vpverf24 dalle distinte
    * --- importate all'interno del F24 se viene cancellato il modello F24.
    * --- Seriale del modello F24
    * --- Leggo il seriale dai modello F24 dell'erario
    this.w_SERIALE = This.OparentObject.Oparentobject.w_Mfserial
    * --- Select from VEP_RITE
    i_nConn=i_TableProp[this.VEP_RITE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VEP_RITE_idx,2],.t.,this.VEP_RITE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select VPSERIAL  from "+i_cTable+" VEP_RITE ";
          +" where "+cp_ToStrODBC(this.w_SERIALE)+"=VPVERF24";
           ,"_Curs_VEP_RITE")
    else
      select VPSERIAL from (i_cTable);
       where this.w_SERIALE=VPVERF24;
        into cursor _Curs_VEP_RITE
    endif
    if used('_Curs_VEP_RITE')
      select _Curs_VEP_RITE
      locate for 1=1
      do while not(eof())
      this.w_CHIAVE = _Curs_VEP_RITE.VPSERIAL
      * --- Write into VEP_RITE
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.VEP_RITE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VEP_RITE_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.VEP_RITE_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"VPVERF24 ="+cp_NullLink(cp_ToStrODBC(SPACE(10)),'VEP_RITE','VPVERF24');
            +i_ccchkf ;
        +" where ";
            +"VPSERIAL = "+cp_ToStrODBC(this.w_CHIAVE);
               )
      else
        update (i_cTable) set;
            VPVERF24 = SPACE(10);
            &i_ccchkf. ;
         where;
            VPSERIAL = this.w_CHIAVE;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
        select _Curs_VEP_RITE
        continue
      enddo
      use
    endif
    * --- Select from VEN_RITE
    i_nConn=i_TableProp[this.VEN_RITE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VEN_RITE_idx,2],.t.,this.VEN_RITE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select VPSERIAL  from "+i_cTable+" VEN_RITE ";
          +" where "+cp_ToStrODBC(this.w_SERIALE)+"=VPVERF24";
           ,"_Curs_VEN_RITE")
    else
      select VPSERIAL from (i_cTable);
       where this.w_SERIALE=VPVERF24;
        into cursor _Curs_VEN_RITE
    endif
    if used('_Curs_VEN_RITE')
      select _Curs_VEN_RITE
      locate for 1=1
      do while not(eof())
      this.w_CHIAVE = _Curs_VEN_RITE.VPSERIAL
      * --- Write into VEN_RITE
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.VEN_RITE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VEN_RITE_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.VEN_RITE_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"VPVERF24 ="+cp_NullLink(cp_ToStrODBC(SPACE(10)),'VEN_RITE','VPVERF24');
            +i_ccchkf ;
        +" where ";
            +"VPSERIAL = "+cp_ToStrODBC(this.w_CHIAVE);
               )
      else
        update (i_cTable) set;
            VPVERF24 = SPACE(10);
            &i_ccchkf. ;
         where;
            VPSERIAL = this.w_CHIAVE;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
        select _Curs_VEN_RITE
        continue
      enddo
      use
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='VEP_RITE'
    this.cWorkTables[2]='VEN_RITE'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_VEP_RITE')
      use in _Curs_VEP_RITE
    endif
    if used('_Curs_VEN_RITE')
      use in _Curs_VEN_RITE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
