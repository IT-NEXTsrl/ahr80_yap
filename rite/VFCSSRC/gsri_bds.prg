* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_bds                                                        *
*              Seleziona/deseleziona ritenute                                  *
*                                                                              *
*      Author: Zucchetti spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][207]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-04-12                                                      *
* Last revis.: 1999-04-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsri_bds",oParentObject)
return(i_retval)

define class tgsri_bds as StdBatch
  * --- Local variables
  oggetto = .NULL.
  w_ZOOM = space(10)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Seleziona/Deseleziona i Movimenti Ritenute in Distinta (Lanciato da GSRI_AVP e GSRI_AVN)
    if g_APPLICATION="ADHOC REVOLUTION"
      this.w_ZOOM = this.oParentObject.w_CalcZoom
    else
      this.w_ZOOM = this.oParentObject.w_Elazoom
    endif
    * --- Consente la Cancellazione scadenze dalla Distinta solo se no Definitiva
    * --- Seleziona/Deseleziona Tutto
    if this.oParentObject.w_VPSTATUS<>"D"
      NC = this.w_Zoom.cCursor
      if this.oParentObject.w_SELEZI="S"
        UPDATE &NC SET XCHK = 1
        this.oParentObject.w_FLSELE = 1
      else
        UPDATE &NC SET XCHK = 0
        this.oParentObject.w_FLSELE = 0
      endif
    else
      ah_errormsg("Opzione consentita solo su distinta non definitiva")
      this.oParentObject.w_SELEZI = IIF(this.oParentObject.w_SELEZI="D", "S", "D")
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
