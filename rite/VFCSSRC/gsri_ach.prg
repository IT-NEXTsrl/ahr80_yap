* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_ach                                                        *
*              Certificazione unica                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-06-22                                                      *
* Last revis.: 2018-05-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsri_ach"))

* --- Class definition
define class tgsri_ach as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 865
  Height = 653+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2018-05-14"
  HelpContextID=109704809
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=136

  * --- Constant Properties
  CUDTETRH_IDX = 0
  CONTI_IDX = 0
  ENTI_COM_IDX = 0
  NAZIONI_IDX = 0
  GENDTEST_IDX = 0
  cFile = "CUDTETRH"
  cKeySelect = "CHSERIAL"
  cKeyWhere  = "CHSERIAL=this.w_CHSERIAL"
  cKeyWhereODBC = '"CHSERIAL="+cp_ToStrODBC(this.w_CHSERIAL)';

  cKeyWhereODBCqualified = '"CUDTETRH.CHSERIAL="+cp_ToStrODBC(this.w_CHSERIAL)';

  cPrg = "gsri_ach"
  cComment = "Certificazione unica"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CHSERIAL = space(10)
  w_OBTEST = ctod('  /  /  ')
  w_CH__ANNO = 0
  o_CH__ANNO = 0
  w_CHTIPCON = space(1)
  w_CHCODCON = space(15)
  o_CHCODCON = space(15)
  w_CHTIPDAT = space(1)
  w_ANCAURIT = space(2)
  w_ANCODFIS = space(16)
  w_ANPERFIS = space(1)
  w_ANDESCRI = space(40)
  w_ANCOGNOM = space(40)
  w_AN__NOME = space(20)
  w_AN_SESSO = space(1)
  w_ANDATNAS = ctod('  /  /  ')
  w_ANLOCNAS = space(30)
  w_ANPRONAS = space(2)
  w_ANCATPAR = space(2)
  w_ANEVEECC = space(1)
  w_ANFLGEST = space(1)
  w_ANLOCALI = space(30)
  w_ANPROVIN = space(2)
  w_ANCODCOM = space(4)
  w_ANCOFISC = space(25)
  w_ANINDIRI = space(35)
  w_ANNAZION = space(3)
  w_NACODEST = space(5)
  w_ANRITENU = space(1)
  w_ANFLSGRE = space(1)
  o_ANFLSGRE = space(1)
  w_ANSCHUMA = space(1)
  w_CHPERFIS = space(1)
  o_CHPERFIS = space(1)
  w_CHPEREST = space(1)
  o_CHPEREST = space(1)
  w_CAUPRE = space(2)
  o_CAUPRE = space(2)
  w_CAUPRE_2016 = space(2)
  o_CAUPRE_2016 = space(2)
  w_CAUPRE_2017 = space(2)
  o_CAUPRE_2017 = space(2)
  w_CHCAUPRE = space(2)
  o_CHCAUPRE = space(2)
  w_CHTIPOPE = space(1)
  o_CHTIPOPE = space(1)
  w_CHSCLGEN = space(1)
  w_CHCODFIS = space(16)
  w_CHDESCRI = space(60)
  w_CHCOGNOM = space(50)
  w_CH__NOME = space(50)
  w_CH_SESSO = space(1)
  w_CHDATNAS = ctod('  /  /  ')
  w_CHCOMEST = space(30)
  w_CHPRONAS = space(2)
  w_EVEECC = space(1)
  o_EVEECC = space(1)
  w_EVEECC_2016 = space(1)
  o_EVEECC_2016 = space(1)
  w_EVEECC_2017 = space(1)
  o_EVEECC_2017 = space(1)
  w_CHEVEECC = space(1)
  w_CATPAR = space(1)
  o_CATPAR = space(1)
  w_CATPAR_2016 = space(1)
  o_CATPAR_2016 = space(1)
  w_CHCATPAR = space(2)
  w_CHCOMUNE = space(30)
  w_CHPROVIN = space(2)
  w_CHCODCOM = space(4)
  w_CHFUSCOM = space(4)
  w_CHSTAEST = space(3)
  w_CHCITEST = space(30)
  w_CHINDEST = space(35)
  w_CHCOFISC = space(25)
  w_CHDAVERI = space(1)
  w_HASEVCOP = space(50)
  w_HASEVENT = .F.
  w_CHSCHUMA = space(1)
  w_CHTOT002 = 0
  w_CHTOT003 = space(1)
  o_CHTOT003 = space(1)
  w_CHTOT004 = 0
  o_CHTOT004 = 0
  w_CHTOT005 = 0
  o_CHTOT005 = 0
  w_TOT006 = space(1)
  o_TOT006 = space(1)
  w_TOT006_2016 = space(1)
  o_TOT006_2016 = space(1)
  w_TOT006_2017 = space(1)
  o_TOT006_2017 = space(1)
  w_CHTOT006 = space(1)
  w_CHTOT008 = 0
  o_CHTOT008 = 0
  w_CHTOT009 = 0
  o_CHTOT009 = 0
  w_CHTOT018 = 0
  o_CHTOT018 = 0
  w_CHTOT019 = 0
  o_CHTOT019 = 0
  w_CHTOT020 = 0
  o_CHTOT020 = 0
  w_CHTOT021 = 0
  o_CHTOT021 = 0
  w_CHTOT041 = 0
  o_CHTOT041 = 0
  w_CHTOT042 = 0
  o_CHTOT042 = 0
  w_CHFISTRA = space(16)
  w_CHT1T002 = 0
  w_CHT1T003 = space(1)
  o_CHT1T003 = space(1)
  w_CHT1T005 = 0
  o_CHT1T005 = 0
  w_TOTM1006 = space(1)
  o_TOTM1006 = space(1)
  w_TOTM1006_2016 = space(1)
  o_TOTM1006_2016 = space(1)
  w_TOTM1006_2017 = space(1)
  o_TOTM1006_2017 = space(1)
  w_CHT1T006 = space(1)
  w_CHT1T018 = 0
  o_CHT1T018 = 0
  w_CHT1T019 = 0
  o_CHT1T019 = 0
  w_CHT2T002 = 0
  w_CHT2T003 = space(1)
  o_CHT2T003 = space(1)
  w_CHT2T005 = 0
  o_CHT2T005 = 0
  w_TOTM2006_2016 = space(1)
  o_TOTM2006_2016 = space(1)
  w_TOTM2006 = space(1)
  o_TOTM2006 = space(1)
  w_TOTM2006_2017 = space(1)
  o_TOTM2006_2017 = space(1)
  w_CHT2T006 = space(1)
  w_CHT2T018 = 0
  o_CHT2T018 = 0
  w_CHT2T019 = 0
  o_CHT2T019 = 0
  w_CHATT1MOD = space(1)
  o_CHATT1MOD = space(1)
  w_CHTOT034 = 0
  w_CHTOT035 = 0
  w_CHPROMOD = 0
  w_CHPR1MOD = 0
  w_CHPR2MOD = 0
  w_GTSERIAL = space(10)
  w_GDSERIAL = space(10)
  w_GDPROCER = 0
  w_CHPROTEC = space(17)
  w_CHPRODOC = 0
  w_CHDATEST = space(10)
  w_CHDATSTA = ctod('  /  /  ')
  w_CHTIPREC = space(1)
  w_CHTOT010 = 0
  w_CHTOT011 = 0
  w_CHTOT012 = 0
  w_CHTOT013 = 0
  w_CHTOT014 = 0
  w_CHTOT015 = 0
  w_CHTOT016 = 0
  w_CHTOT017 = 0
  w_TIPOCONT = space(1)
  w_CHATT2MOD = space(1)
  o_CHATT2MOD = space(1)
  w_CHCERDEF = space(1)
  w_VAR_DATI = space(1)
  w_STATO = space(1)
  w_CHATT3MOD = space(1)
  o_CHATT3MOD = space(1)
  w_CHT3T002 = 0
  w_CHT3T003 = space(1)
  o_CHT3T003 = space(1)
  w_CHT3T005 = 0
  o_CHT3T005 = 0
  w_TOTM3006_2016 = space(1)
  o_TOTM3006_2016 = space(1)
  w_TOTM3006_2017 = space(1)
  o_TOTM3006_2017 = space(1)
  w_CHT3T006 = space(1)
  w_CHT3T018 = 0
  o_CHT3T018 = 0
  w_CHT3T019 = 0
  o_CHT3T019 = 0
  w_CHPR3MOD = 0

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_CHSERIAL = this.W_CHSERIAL

  * --- Children pointers
  GSRI_MCH = .NULL.
  GSRI_MPR = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsri_ach
   * --- Gestiti i soli eventi Modifica, cancellazione, inserimento
  func HasCPEvents(i_cOp)
   if this.cFunction='Query'
  	  	If Upper(i_cop)='ECPEDIT' Or Upper(i_cop)='ECPDELETE'
  	  		* esegue controllo se movimento bloccato se da problemi si pu� chiamare direttamente il Batch
          This.w_HASEVCOP=i_cop
  	  		This.NotifyEvent('HasEvent')
  	  		Return ( This.w_HASEVENT )
       Else
  	  		Return(.t.)
  	  	endif
  	Else
  	  	* --- Se l'evento non � modifica/cancellazione/inserimento prosegue
        Return(.t.)
    Endif
   EndFunc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=4, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'CUDTETRH','gsri_ach')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsri_achPag1","gsri_ach",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Record D")
      .Pages(1).HelpContextID = 84649050
      .Pages(2).addobject("oPag","tgsri_achPag2","gsri_ach",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Record H")
      .Pages(2).HelpContextID = 84649054
      .Pages(3).addobject("oPag","tgsri_achPag3","gsri_ach",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Movimenti ritenute")
      .Pages(3).HelpContextID = 228093569
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='ENTI_COM'
    this.cWorkTables[3]='NAZIONI'
    this.cWorkTables[4]='GENDTEST'
    this.cWorkTables[5]='CUDTETRH'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(5))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CUDTETRH_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CUDTETRH_IDX,3]
  return

  function CreateChildren()
    this.GSRI_MCH = CREATEOBJECT('stdDynamicChild',this,'GSRI_MCH',this.oPgFrm.Page3.oPag.oLinkPC_3_1)
    this.GSRI_MPR = CREATEOBJECT('stdLazyChild',this,'GSRI_MPR')
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSRI_MCH)
      this.GSRI_MCH.DestroyChildrenChain()
      this.GSRI_MCH=.NULL.
    endif
    this.oPgFrm.Page3.oPag.RemoveObject('oLinkPC_3_1')
    if !ISNULL(this.GSRI_MPR)
      this.GSRI_MPR.DestroyChildrenChain()
      this.GSRI_MPR=.NULL.
    endif
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSRI_MCH.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSRI_MPR.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSRI_MCH.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSRI_MPR.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSRI_MCH.NewDocument()
    this.GSRI_MPR.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSRI_MCH.SetKey(;
            .w_CHSERIAL,"CHSERIAL";
            )
      this.GSRI_MPR.SetKey(;
            .w_CHSERIAL,"DPSERIAL";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSRI_MCH.ChangeRow(this.cRowID+'      1',1;
             ,.w_CHSERIAL,"CHSERIAL";
             )
      .WriteTo_GSRI_MCH()
      .GSRI_MPR.ChangeRow(this.cRowID+'      1',1;
             ,.w_CHSERIAL,"DPSERIAL";
             )
      .WriteTo_GSRI_MPR()
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSRI_MCH)
        i_f=.GSRI_MCH.BuildFilter()
        if !(i_f==.GSRI_MCH.cQueryFilter)
          i_fnidx=.GSRI_MCH.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSRI_MCH.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSRI_MCH.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSRI_MCH.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSRI_MCH.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

procedure WriteTo_GSRI_MCH()
  if at('gsri_mch',lower(this.GSRI_MCH.class))<>0
    if this.GSRI_MCH.w_ANNO_RITE<>this.w_CH__ANNO
      this.GSRI_MCH.w_ANNO_RITE = this.w_CH__ANNO
      this.GSRI_MCH.mCalc(.t.)
    endif
  endif
  return

procedure WriteTo_GSRI_MPR()
  if at('gsri_mpr',lower(this.GSRI_MPR.class))<>0
    if this.GSRI_MPR.cnt.w_CH__ANNO<>this.w_CH__ANNO
      this.GSRI_MPR.cnt.w_CH__ANNO = this.w_CH__ANNO
      this.GSRI_MPR.cnt.mCalc(.t.)
    endif
  endif
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_CHSERIAL = NVL(CHSERIAL,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_5_joined
    link_1_5_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from CUDTETRH where CHSERIAL=KeySet.CHSERIAL
    *
    i_nConn = i_TableProp[this.CUDTETRH_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CUDTETRH_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CUDTETRH')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CUDTETRH.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' CUDTETRH '
      link_1_5_joined=this.AddJoinedLink_1_5(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'CHSERIAL',this.w_CHSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_OBTEST = CTOD('01-01-1900')
        .w_ANCAURIT = space(2)
        .w_ANCODFIS = space(16)
        .w_ANPERFIS = space(1)
        .w_ANDESCRI = space(40)
        .w_ANCOGNOM = space(40)
        .w_AN__NOME = space(20)
        .w_AN_SESSO = space(1)
        .w_ANDATNAS = ctod("  /  /  ")
        .w_ANLOCNAS = space(30)
        .w_ANPRONAS = space(2)
        .w_ANCATPAR = space(2)
        .w_ANEVEECC = space(1)
        .w_ANFLGEST = space(1)
        .w_ANLOCALI = space(30)
        .w_ANPROVIN = space(2)
        .w_ANCODCOM = space(4)
        .w_ANCOFISC = space(25)
        .w_ANINDIRI = space(35)
        .w_ANNAZION = space(3)
        .w_NACODEST = space(5)
        .w_ANRITENU = space(1)
        .w_ANFLSGRE = space(1)
        .w_ANSCHUMA = space(1)
        .w_HASEVCOP = space(50)
        .w_HASEVENT = .f.
        .w_GTSERIAL = .w_CHSERIAL
        .w_GDSERIAL = space(10)
        .w_GDPROCER = 0
        .w_TIPOCONT = 'D'
        .w_VAR_DATI = 'N'
        .w_STATO = ' '
        .w_CHSERIAL = NVL(CHSERIAL,space(10))
        .op_CHSERIAL = .w_CHSERIAL
        .w_CH__ANNO = NVL(CH__ANNO,0)
        .w_CHTIPCON = NVL(CHTIPCON,space(1))
        .w_CHCODCON = NVL(CHCODCON,space(15))
          if link_1_5_joined
            this.w_CHCODCON = NVL(ANCODICE105,NVL(this.w_CHCODCON,space(15)))
            this.w_ANDESCRI = NVL(ANDESCRI105,space(40))
            this.w_ANCAURIT = NVL(ANCAURIT105,space(2))
            this.w_ANCODFIS = NVL(ANCODFIS105,space(16))
            this.w_ANPERFIS = NVL(ANPERFIS105,space(1))
            this.w_ANCOGNOM = NVL(ANCOGNOM105,space(40))
            this.w_AN__NOME = NVL(AN__NOME105,space(20))
            this.w_AN_SESSO = NVL(AN_SESSO105,space(1))
            this.w_ANDATNAS = NVL(cp_ToDate(ANDATNAS105),ctod("  /  /  "))
            this.w_ANLOCNAS = NVL(ANLOCNAS105,space(30))
            this.w_ANPRONAS = NVL(ANPRONAS105,space(2))
            this.w_ANCATPAR = NVL(ANCATPAR105,space(2))
            this.w_ANEVEECC = NVL(ANEVEECC105,space(1))
            this.w_ANFLGEST = NVL(ANFLGEST105,space(1))
            this.w_ANLOCALI = NVL(ANLOCALI105,space(30))
            this.w_ANPROVIN = NVL(ANPROVIN105,space(2))
            this.w_ANCODCOM = NVL(ANCODCOM105,space(4))
            this.w_ANCOFISC = NVL(ANCOFISC105,space(25))
            this.w_ANINDIRI = NVL(ANINDIRI105,space(35))
            this.w_ANNAZION = NVL(ANNAZION105,space(3))
            this.w_ANRITENU = NVL(ANRITENU105,space(1))
            this.w_ANFLSGRE = NVL(ANFLSGRE105,space(1))
            this.w_ANSCHUMA = NVL(ANSCHUMA105,space(1))
          else
          .link_1_5('Load')
          endif
        .w_CHTIPDAT = NVL(CHTIPDAT,space(1))
          .link_1_25('Load')
        .w_CHPERFIS = NVL(CHPERFIS,space(1))
        .w_CHPEREST = NVL(CHPEREST,space(1))
        .w_CAUPRE = IIF(.w_ANCAURIT='F' OR .w_ANCAURIT=='ZO' OR .w_ANCAURIT='J' OR .w_ANCAURIT='K' OR EMPTY(.w_ANCAURIT),'A',.w_ANCAURIT)
        .w_CAUPRE_2016 = IIF(.w_ANCAURIT= 'F' OR Alltrim(.w_ANCAURIT) == 'Z' OR .w_ANCAURIT='J' OR .w_ANCAURIT='K' OR EMPTY(.w_ANCAURIT),'A',.w_ANCAURIT)
        .w_CAUPRE_2017 = IIF(Alltrim(.w_ANCAURIT) == 'Z' OR EMPTY(.w_ANCAURIT),'A',.w_ANCAURIT)
        .w_CHCAUPRE = NVL(CHCAUPRE,space(2))
        .w_CHTIPOPE = NVL(CHTIPOPE,space(1))
        .w_CHSCLGEN = NVL(CHSCLGEN,space(1))
        .w_CHCODFIS = NVL(CHCODFIS,space(16))
        .w_CHDESCRI = NVL(CHDESCRI,space(60))
        .w_CHCOGNOM = NVL(CHCOGNOM,space(50))
        .w_CH__NOME = NVL(CH__NOME,space(50))
        .w_CH_SESSO = NVL(CH_SESSO,space(1))
        .w_CHDATNAS = NVL(cp_ToDate(CHDATNAS),ctod("  /  /  "))
        .w_CHCOMEST = NVL(CHCOMEST,space(30))
        .w_CHPRONAS = NVL(CHPRONAS,space(2))
        .w_EVEECC = iif(.w_ANEVEECC $ '134',.w_ANEVEECC,'0')
        .w_EVEECC_2016 = iif(.w_ANEVEECC $ '193AB',IIF(.w_ANEVEECC $ '13',.w_ANEVEECC,IIF(.w_ANEVEECC='9','2',IIF(.w_ANEVEECC='A','4','6'))),'0')
        .w_EVEECC_2017 = iif(.w_ANEVEECC $ '193ACD',IIF(.w_ANEVEECC $ '13',.w_ANEVEECC,IIF(.w_ANEVEECC='9','2',IIF(.w_ANEVEECC='A','4',IIF(.w_ANEVEECC='C','5','8')))),'0')
        .w_CHEVEECC = NVL(CHEVEECC,space(1))
        .w_CATPAR = iif(.w_ANCATPAR = 'Z1' OR .w_ANCATPAR = 'Z3','  ',.w_ANCATPAR)
        .w_CATPAR_2016 = iif(.w_ANCATPAR = 'Z1','  ',.w_ANCATPAR)
        .w_CHCATPAR = NVL(CHCATPAR,space(2))
        .w_CHCOMUNE = NVL(CHCOMUNE,space(30))
        .w_CHPROVIN = NVL(CHPROVIN,space(2))
        .w_CHCODCOM = NVL(CHCODCOM,space(4))
          * evitabile
          *.link_1_75('Load')
        .w_CHFUSCOM = NVL(CHFUSCOM,space(4))
          * evitabile
          *.link_1_76('Load')
        .w_CHSTAEST = NVL(CHSTAEST,space(3))
        .w_CHCITEST = NVL(CHCITEST,space(30))
        .w_CHINDEST = NVL(CHINDEST,space(35))
        .w_CHCOFISC = NVL(CHCOFISC,space(25))
        .w_CHDAVERI = NVL(CHDAVERI,space(1))
        .w_CHSCHUMA = NVL(CHSCHUMA,space(1))
        .w_CHTOT002 = NVL(CHTOT002,0)
        .w_CHTOT003 = NVL(CHTOT003,space(1))
        .w_CHTOT004 = NVL(CHTOT004,0)
        .w_CHTOT005 = NVL(CHTOT005,0)
        .w_TOT006 = iif(.w_CHTOT005=0 OR  (.w_CHPEREST='S' AND .w_ANFLSGRE='S'),' ','3')
        .w_TOT006_2016 = iif(.w_CHTOT005=0 OR  (.w_CHPEREST='S' AND .w_ANFLSGRE='S'),' ','6')
        .w_TOT006_2017 = iif(.w_CHTOT005=0 OR  (.w_CHPEREST='S' AND .w_ANFLSGRE='S'),' ','7')
        .w_CHTOT006 = NVL(CHTOT006,space(1))
        .w_CHTOT008 = NVL(CHTOT008,0)
        .w_CHTOT009 = NVL(CHTOT009,0)
        .w_CHTOT018 = NVL(CHTOT018,0)
        .w_CHTOT019 = NVL(CHTOT019,0)
        .w_CHTOT020 = NVL(CHTOT020,0)
        .w_CHTOT021 = NVL(CHTOT021,0)
        .w_CHTOT041 = NVL(CHTOT041,0)
        .w_CHTOT042 = NVL(CHTOT042,0)
        .w_CHFISTRA = NVL(CHFISTRA,space(16))
        .w_CHT1T002 = NVL(CHT1T002,0)
        .w_CHT1T003 = NVL(CHT1T003,space(1))
        .w_CHT1T005 = NVL(CHT1T005,0)
        .w_TOTM1006 = iif(.w_CHT1T005=0 OR  (.w_CHPEREST='S' AND .w_ANFLSGRE='S') OR .w_CHATT1MOD='N' ,' ','3')
        .w_TOTM1006_2016 = iif(.w_CHT1T005=0 OR  (.w_CHPEREST='S' AND .w_ANFLSGRE='S') OR .w_CHATT1MOD='N' ,' ','6')
        .w_TOTM1006_2017 = iif(.w_CHT1T005=0 OR  (.w_CHPEREST='S' AND .w_ANFLSGRE='S') OR .w_CHATT1MOD='N' ,' ','7')
        .w_CHT1T006 = NVL(CHT1T006,space(1))
        .w_CHT1T018 = NVL(CHT1T018,0)
        .w_CHT1T019 = NVL(CHT1T019,0)
        .w_CHT2T002 = NVL(CHT2T002,0)
        .w_CHT2T003 = NVL(CHT2T003,space(1))
        .w_CHT2T005 = NVL(CHT2T005,0)
        .w_TOTM2006_2016 = iif(.w_CHT2T005=0 OR  (.w_CHPEREST='S' AND .w_ANFLSGRE='S')  OR .w_CHATT2MOD='N',' ','6')
        .w_TOTM2006 = iif(.w_CHT2T005=0 OR  (.w_CHPEREST='S' AND .w_ANFLSGRE='S')  OR .w_CHATT2MOD='N',' ','3')
        .w_TOTM2006_2017 = iif(.w_CHT2T005=0 OR  (.w_CHPEREST='S' AND .w_ANFLSGRE='S')  OR .w_CHATT2MOD='N',' ','7')
        .w_CHT2T006 = NVL(CHT2T006,space(1))
        .w_CHT2T018 = NVL(CHT2T018,0)
        .w_CHT2T019 = NVL(CHT2T019,0)
        .w_CHATT1MOD = NVL(CHATT1MOD,space(1))
        .w_CHTOT034 = NVL(CHTOT034,0)
        .w_CHTOT035 = NVL(CHTOT035,0)
        .w_CHPROMOD = NVL(CHPROMOD,0)
        .w_CHPR1MOD = NVL(CHPR1MOD,0)
        .w_CHPR2MOD = NVL(CHPR2MOD,0)
        .oPgFrm.Page1.oPag.oObj_1_94.Calculate()
          .link_1_95('Load')
        .w_CHPROTEC = NVL(CHPROTEC,space(17))
        .w_CHPRODOC = NVL(CHPRODOC,0)
        .w_CHDATEST = NVL(CHDATEST,space(10))
        .w_CHDATSTA = NVL(cp_ToDate(CHDATSTA),ctod("  /  /  "))
        .w_CHTIPREC = NVL(CHTIPREC,space(1))
        .w_CHTOT010 = NVL(CHTOT010,0)
        .w_CHTOT011 = NVL(CHTOT011,0)
        .w_CHTOT012 = NVL(CHTOT012,0)
        .w_CHTOT013 = NVL(CHTOT013,0)
        .w_CHTOT014 = NVL(CHTOT014,0)
        .w_CHTOT015 = NVL(CHTOT015,0)
        .w_CHTOT016 = NVL(CHTOT016,0)
        .w_CHTOT017 = NVL(CHTOT017,0)
        .w_CHATT2MOD = NVL(CHATT2MOD,space(1))
        .w_CHCERDEF = NVL(CHCERDEF,space(1))
        .w_CHATT3MOD = NVL(CHATT3MOD,space(1))
        .w_CHT3T002 = NVL(CHT3T002,0)
        .w_CHT3T003 = NVL(CHT3T003,space(1))
        .w_CHT3T005 = NVL(CHT3T005,0)
        .w_TOTM3006_2016 = iif(.w_CHT3T005=0 OR (.w_CHPEREST='S' AND .w_ANFLSGRE='S') OR .w_CHATT3MOD='N',' ','6')
        .w_TOTM3006_2017 = iif(.w_CHT3T005=0 OR (.w_CHPEREST='S' AND .w_ANFLSGRE='S') OR .w_CHATT3MOD='N',' ','7')
        .w_CHT3T006 = NVL(CHT3T006,space(1))
        .w_CHT3T018 = NVL(CHT3T018,0)
        .w_CHT3T019 = NVL(CHT3T019,0)
        .w_CHPR3MOD = NVL(CHPR3MOD,0)
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'CUDTETRH')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_103.enabled = this.oPgFrm.Page1.oPag.oBtn_1_103.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_104.enabled = this.oPgFrm.Page1.oPag.oBtn_1_104.mCond()
      this.oPgFrm.Page2.oPag.oBtn_2_71.enabled = this.oPgFrm.Page2.oPag.oBtn_2_71.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gsri_ach
    This.NotifyEvent('ValorizzazioneStato')
    This.mEnableControls()
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CHSERIAL = space(10)
      .w_OBTEST = ctod("  /  /  ")
      .w_CH__ANNO = 0
      .w_CHTIPCON = space(1)
      .w_CHCODCON = space(15)
      .w_CHTIPDAT = space(1)
      .w_ANCAURIT = space(2)
      .w_ANCODFIS = space(16)
      .w_ANPERFIS = space(1)
      .w_ANDESCRI = space(40)
      .w_ANCOGNOM = space(40)
      .w_AN__NOME = space(20)
      .w_AN_SESSO = space(1)
      .w_ANDATNAS = ctod("  /  /  ")
      .w_ANLOCNAS = space(30)
      .w_ANPRONAS = space(2)
      .w_ANCATPAR = space(2)
      .w_ANEVEECC = space(1)
      .w_ANFLGEST = space(1)
      .w_ANLOCALI = space(30)
      .w_ANPROVIN = space(2)
      .w_ANCODCOM = space(4)
      .w_ANCOFISC = space(25)
      .w_ANINDIRI = space(35)
      .w_ANNAZION = space(3)
      .w_NACODEST = space(5)
      .w_ANRITENU = space(1)
      .w_ANFLSGRE = space(1)
      .w_ANSCHUMA = space(1)
      .w_CHPERFIS = space(1)
      .w_CHPEREST = space(1)
      .w_CAUPRE = space(2)
      .w_CAUPRE_2016 = space(2)
      .w_CAUPRE_2017 = space(2)
      .w_CHCAUPRE = space(2)
      .w_CHTIPOPE = space(1)
      .w_CHSCLGEN = space(1)
      .w_CHCODFIS = space(16)
      .w_CHDESCRI = space(60)
      .w_CHCOGNOM = space(50)
      .w_CH__NOME = space(50)
      .w_CH_SESSO = space(1)
      .w_CHDATNAS = ctod("  /  /  ")
      .w_CHCOMEST = space(30)
      .w_CHPRONAS = space(2)
      .w_EVEECC = space(1)
      .w_EVEECC_2016 = space(1)
      .w_EVEECC_2017 = space(1)
      .w_CHEVEECC = space(1)
      .w_CATPAR = space(1)
      .w_CATPAR_2016 = space(1)
      .w_CHCATPAR = space(2)
      .w_CHCOMUNE = space(30)
      .w_CHPROVIN = space(2)
      .w_CHCODCOM = space(4)
      .w_CHFUSCOM = space(4)
      .w_CHSTAEST = space(3)
      .w_CHCITEST = space(30)
      .w_CHINDEST = space(35)
      .w_CHCOFISC = space(25)
      .w_CHDAVERI = space(1)
      .w_HASEVCOP = space(50)
      .w_HASEVENT = .f.
      .w_CHSCHUMA = space(1)
      .w_CHTOT002 = 0
      .w_CHTOT003 = space(1)
      .w_CHTOT004 = 0
      .w_CHTOT005 = 0
      .w_TOT006 = space(1)
      .w_TOT006_2016 = space(1)
      .w_TOT006_2017 = space(1)
      .w_CHTOT006 = space(1)
      .w_CHTOT008 = 0
      .w_CHTOT009 = 0
      .w_CHTOT018 = 0
      .w_CHTOT019 = 0
      .w_CHTOT020 = 0
      .w_CHTOT021 = 0
      .w_CHTOT041 = 0
      .w_CHTOT042 = 0
      .w_CHFISTRA = space(16)
      .w_CHT1T002 = 0
      .w_CHT1T003 = space(1)
      .w_CHT1T005 = 0
      .w_TOTM1006 = space(1)
      .w_TOTM1006_2016 = space(1)
      .w_TOTM1006_2017 = space(1)
      .w_CHT1T006 = space(1)
      .w_CHT1T018 = 0
      .w_CHT1T019 = 0
      .w_CHT2T002 = 0
      .w_CHT2T003 = space(1)
      .w_CHT2T005 = 0
      .w_TOTM2006_2016 = space(1)
      .w_TOTM2006 = space(1)
      .w_TOTM2006_2017 = space(1)
      .w_CHT2T006 = space(1)
      .w_CHT2T018 = 0
      .w_CHT2T019 = 0
      .w_CHATT1MOD = space(1)
      .w_CHTOT034 = 0
      .w_CHTOT035 = 0
      .w_CHPROMOD = 0
      .w_CHPR1MOD = 0
      .w_CHPR2MOD = 0
      .w_GTSERIAL = space(10)
      .w_GDSERIAL = space(10)
      .w_GDPROCER = 0
      .w_CHPROTEC = space(17)
      .w_CHPRODOC = 0
      .w_CHDATEST = space(10)
      .w_CHDATSTA = ctod("  /  /  ")
      .w_CHTIPREC = space(1)
      .w_CHTOT010 = 0
      .w_CHTOT011 = 0
      .w_CHTOT012 = 0
      .w_CHTOT013 = 0
      .w_CHTOT014 = 0
      .w_CHTOT015 = 0
      .w_CHTOT016 = 0
      .w_CHTOT017 = 0
      .w_TIPOCONT = space(1)
      .w_CHATT2MOD = space(1)
      .w_CHCERDEF = space(1)
      .w_VAR_DATI = space(1)
      .w_STATO = space(1)
      .w_CHATT3MOD = space(1)
      .w_CHT3T002 = 0
      .w_CHT3T003 = space(1)
      .w_CHT3T005 = 0
      .w_TOTM3006_2016 = space(1)
      .w_TOTM3006_2017 = space(1)
      .w_CHT3T006 = space(1)
      .w_CHT3T018 = 0
      .w_CHT3T019 = 0
      .w_CHPR3MOD = 0
      if .cFunction<>"Filter"
          .DoRTCalc(1,1,.f.)
        .w_OBTEST = CTOD('01-01-1900')
        .w_CH__ANNO = year(i_DATSYS)-1
        .w_CHTIPCON = 'F'
        .DoRTCalc(5,5,.f.)
          if not(empty(.w_CHCODCON))
          .link_1_5('Full')
          endif
        .w_CHTIPDAT = 'M'
        .DoRTCalc(7,25,.f.)
          if not(empty(.w_ANNAZION))
          .link_1_25('Full')
          endif
          .DoRTCalc(26,29,.f.)
        .w_CHPERFIS = .w_ANPERFIS
        .w_CHPEREST = .w_ANFLGEST
        .w_CAUPRE = IIF(.w_ANCAURIT='F' OR .w_ANCAURIT=='ZO' OR .w_ANCAURIT='J' OR .w_ANCAURIT='K' OR EMPTY(.w_ANCAURIT),'A',.w_ANCAURIT)
        .w_CAUPRE_2016 = IIF(.w_ANCAURIT= 'F' OR Alltrim(.w_ANCAURIT) == 'Z' OR .w_ANCAURIT='J' OR .w_ANCAURIT='K' OR EMPTY(.w_ANCAURIT),'A',.w_ANCAURIT)
        .w_CAUPRE_2017 = IIF(Alltrim(.w_ANCAURIT) == 'Z' OR EMPTY(.w_ANCAURIT),'A',.w_ANCAURIT)
          .DoRTCalc(35,35,.f.)
        .w_CHTIPOPE = 'O'
        .w_CHSCLGEN = 'N'
        .w_CHCODFIS = Upper(.w_ANCODFIS)
        .w_CHDESCRI = Upper(.w_ANDESCRI)
        .w_CHCOGNOM = IIF(.w_CHPERFIS='S',Upper(.w_ANCOGNOM),SPACE(50))
        .w_CH__NOME = IIF(.w_CHPERFIS='S',Upper(.w_AN__NOME),SPACE(50))
        .w_CH_SESSO = iif(.w_CHPERFIS='S' and not empty(.w_AN_SESSO), .w_AN_SESSO, 'M')
        .w_CHDATNAS = IIF(.w_CHPERFIS='S',.w_ANDATNAS,CTOD('  -  -   '))
        .w_CHCOMEST = IIF(.w_CHPERFIS='S',Upper(.w_ANLOCNAS),SPACE(30))
        .w_CHPRONAS = IIF(.w_CHPERFIS='S',Upper(.w_ANPRONAS),SPACE(2))
        .w_EVEECC = iif(.w_ANEVEECC $ '134',.w_ANEVEECC,'0')
        .w_EVEECC_2016 = iif(.w_ANEVEECC $ '193AB',IIF(.w_ANEVEECC $ '13',.w_ANEVEECC,IIF(.w_ANEVEECC='9','2',IIF(.w_ANEVEECC='A','4','6'))),'0')
        .w_EVEECC_2017 = iif(.w_ANEVEECC $ '193ACD',IIF(.w_ANEVEECC $ '13',.w_ANEVEECC,IIF(.w_ANEVEECC='9','2',IIF(.w_ANEVEECC='A','4',IIF(.w_ANEVEECC='C','5','8')))),'0')
          .DoRTCalc(49,49,.f.)
        .w_CATPAR = iif(.w_ANCATPAR = 'Z1' OR .w_ANCATPAR = 'Z3','  ',.w_ANCATPAR)
        .w_CATPAR_2016 = iif(.w_ANCATPAR = 'Z1','  ',.w_ANCATPAR)
          .DoRTCalc(52,52,.f.)
        .w_CHCOMUNE = IIF(.w_CHPEREST<>'S',Upper(.w_ANLOCALI),SPACE(30))
        .w_CHPROVIN = IIF(.w_CHPEREST<>'S',Upper(.w_ANPROVIN),SPACE(2))
        .w_CHCODCOM = IIF(.w_CHPEREST<>'S' AND .w_CHCAUPRE='N',Upper(.w_ANCODCOM),SPACE(4))
        .DoRTCalc(55,55,.f.)
          if not(empty(.w_CHCODCOM))
          .link_1_75('Full')
          endif
        .w_CHFUSCOM = Space(4)
        .DoRTCalc(56,56,.f.)
          if not(empty(.w_CHFUSCOM))
          .link_1_76('Full')
          endif
        .w_CHSTAEST = IIF(.w_CHPEREST='S',Upper(RIGHT(Alltrim(.w_NACODEST),3)),SPACE(3))
        .w_CHCITEST = IIF(.w_CHPEREST='S',Upper(.w_ANLOCALI),SPACE(30))
        .w_CHINDEST = IIF(.w_CHPEREST='S',Upper(.w_ANINDIRI),SPACE(35))
        .w_CHCOFISC = iif(.w_Chperest='S',Upper(iif(Empty(.w_Ancofisc), .w_Ancodfis, .w_Ancofisc)),Space(25))
        .w_CHDAVERI = iif(.w_Chtipope $ 'SA','S','N')
          .DoRTCalc(62,63,.f.)
        .w_CHSCHUMA = .w_ANSCHUMA
        .w_CHTOT002 = iif((Alltrim(.w_Chcaupre) $ 'G-H-I' Or .w_Chtot003='S') ,iif(Empty(.w_Chtot002),.w_Ch__Anno,.w_Chtot002),0)
          .DoRTCalc(66,66,.f.)
        .w_CHTOT004 = iif(.w_Chtot004<0,0,.w_Chtot004)
        .w_CHTOT005 = iif(.w_Chtot005<0,0,.w_Chtot005)
        .w_TOT006 = iif(.w_CHTOT005=0 OR  (.w_CHPEREST='S' AND .w_ANFLSGRE='S'),' ','3')
        .w_TOT006_2016 = iif(.w_CHTOT005=0 OR  (.w_CHPEREST='S' AND .w_ANFLSGRE='S'),' ','6')
        .w_TOT006_2017 = iif(.w_CHTOT005=0 OR  (.w_CHPEREST='S' AND .w_ANFLSGRE='S'),' ','7')
          .DoRTCalc(72,72,.f.)
        .w_CHTOT008 = iif(.w_Chtot008<0,0,.w_Chtot008)
        .w_CHTOT009 = iif(.w_Chtot009<0,0,.w_Chtot009)
        .w_CHTOT018 = iif(.w_Chtot018<0,0,.w_Chtot018)
        .w_CHTOT019 = iif(.w_Chtot019<0,0,.w_Chtot019)
        .w_CHTOT020 = iif(.w_Chtot020<0,0,.w_Chtot020)
        .w_CHTOT021 = iif(.w_Chtot021>=0 And Alltrim(.w_Chcaupre) $ 'X-Y',.w_Chtot021,0)
        .w_CHTOT041 = iif(.w_Chtot041<0,0,.w_Chtot041)
        .w_CHTOT042 = iif(.w_Chtot042<0,0,.w_Chtot042)
        .w_CHFISTRA = Space(16)
        .w_CHT1T002 = iif((Alltrim(.w_Chcaupre) $ 'G-H-I' Or .w_Cht1t003='S') And .w_Chatt1Mod='S',iif(Empty(.w_Cht1t002),.w_Ch__Anno,.w_Cht1t002),0)
        .w_CHT1T003 = 'N'
        .w_CHT1T005 = iif(.w_Cht1t005<0 Or .w_Chatt1Mod='N',0,.w_Cht1t005)
        .w_TOTM1006 = iif(.w_CHT1T005=0 OR  (.w_CHPEREST='S' AND .w_ANFLSGRE='S') OR .w_CHATT1MOD='N' ,' ','3')
        .w_TOTM1006_2016 = iif(.w_CHT1T005=0 OR  (.w_CHPEREST='S' AND .w_ANFLSGRE='S') OR .w_CHATT1MOD='N' ,' ','6')
        .w_TOTM1006_2017 = iif(.w_CHT1T005=0 OR  (.w_CHPEREST='S' AND .w_ANFLSGRE='S') OR .w_CHATT1MOD='N' ,' ','7')
          .DoRTCalc(88,88,.f.)
        .w_CHT1T018 = iif(.w_Cht1t018<0 Or .w_Chatt1Mod='N',0,.w_Cht1t018)
        .w_CHT1T019 = iif(.w_Cht1t019<0 Or .w_Chatt1Mod='N',0,.w_Cht1t019)
        .w_CHT2T002 = iif((Alltrim(.w_Chcaupre) $ 'G-H-I' Or .w_Cht2t003='S') And .w_Chatt2Mod='S',iif(Empty(.w_Cht2t002),.w_Ch__Anno,.w_Cht2t002),0)
        .w_CHT2T003 = 'N'
        .w_CHT2T005 = iif(.w_Cht2t005<0 Or .w_Chatt2Mod='N',0,.w_Cht2t005)
        .w_TOTM2006_2016 = iif(.w_CHT2T005=0 OR  (.w_CHPEREST='S' AND .w_ANFLSGRE='S')  OR .w_CHATT2MOD='N',' ','6')
        .w_TOTM2006 = iif(.w_CHT2T005=0 OR  (.w_CHPEREST='S' AND .w_ANFLSGRE='S')  OR .w_CHATT2MOD='N',' ','3')
        .w_TOTM2006_2017 = iif(.w_CHT2T005=0 OR  (.w_CHPEREST='S' AND .w_ANFLSGRE='S')  OR .w_CHATT2MOD='N',' ','7')
          .DoRTCalc(97,97,.f.)
        .w_CHT2T018 = iif(.w_Cht2t018<0 Or .w_Chatt2Mod='N',0,.w_Cht2t018)
        .w_CHT2T019 = iif(.w_Cht2t019<0 Or .w_Chatt2Mod='N',0,.w_Cht2t019)
        .w_CHATT1MOD = 'N'
          .DoRTCalc(101,102,.f.)
        .w_CHPROMOD = 1
        .w_CHPR1MOD = iif(.w_CHATT1MOD='S',2,0)
        .w_CHPR2MOD = iif(.w_CHATT2MOD='S',3,0)
        .oPgFrm.Page1.oPag.oObj_1_94.Calculate()
        .w_GTSERIAL = .w_CHSERIAL
        .DoRTCalc(106,106,.f.)
          if not(empty(.w_GTSERIAL))
          .link_1_95('Full')
          endif
          .DoRTCalc(107,112,.f.)
        .w_CHTIPREC = ' '
          .DoRTCalc(114,121,.f.)
        .w_TIPOCONT = 'D'
        .w_CHATT2MOD = 'N'
        .w_CHCERDEF = 'N'
        .w_VAR_DATI = 'N'
        .w_STATO = ' '
        .w_CHATT3MOD = 'N'
        .w_CHT3T002 = iif((Alltrim(.w_Chcaupre) $ 'G-H-I' Or .w_Cht3t003='S') And .w_Chatt3Mod='S',iif(Empty(.w_Cht3t002),.w_Ch__Anno,.w_Cht3t002),0)
        .w_CHT3T003 = 'N'
        .w_CHT3T005 = iif(.w_Cht3t005<0 Or .w_Chatt3Mod='N',0,.w_Cht3t005)
        .w_TOTM3006_2016 = iif(.w_CHT3T005=0 OR (.w_CHPEREST='S' AND .w_ANFLSGRE='S') OR .w_CHATT3MOD='N',' ','6')
        .w_TOTM3006_2017 = iif(.w_CHT3T005=0 OR (.w_CHPEREST='S' AND .w_ANFLSGRE='S') OR .w_CHATT3MOD='N',' ','7')
          .DoRTCalc(133,133,.f.)
        .w_CHT3T018 = iif(.w_Cht3t018<0 Or .w_Chatt3Mod='N',0,.w_Cht3t018)
        .w_CHT3T019 = iif(.w_Cht3t019<0 Or .w_Chatt3Mod='N',0,.w_Cht3t019)
        .w_CHPR3MOD = iif(.w_CHATT3MOD='S',4,0)
      endif
    endwith
    cp_BlankRecExtFlds(this,'CUDTETRH')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_103.enabled = this.oPgFrm.Page1.oPag.oBtn_1_103.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_104.enabled = this.oPgFrm.Page1.oPag.oBtn_1_104.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_71.enabled = this.oPgFrm.Page2.oPag.oBtn_2_71.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CUDTETRH_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CUDTETRH_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"CHSER","i_codazi,w_CHSERIAL")
      .op_codazi = .w_codazi
      .op_CHSERIAL = .w_CHSERIAL
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oCH__ANNO_1_3.enabled = i_bVal
      .Page1.oPag.oCHCODCON_1_5.enabled = i_bVal
      .Page1.oPag.oCAUPRE_1_32.enabled = i_bVal
      .Page1.oPag.oCAUPRE_2016_1_33.enabled = i_bVal
      .Page1.oPag.oCAUPRE_2017_1_34.enabled = i_bVal
      .Page1.oPag.oCHTIPOPE_1_37.enabled = i_bVal
      .Page1.oPag.oCHSCLGEN_1_38.enabled = i_bVal
      .Page1.oPag.oCHCODFIS_1_39.enabled = i_bVal
      .Page1.oPag.oCHDESCRI_1_46.enabled = i_bVal
      .Page1.oPag.oCHCOGNOM_1_47.enabled = i_bVal
      .Page1.oPag.oCH__NOME_1_48.enabled = i_bVal
      .Page1.oPag.oCH_SESSO_1_52.enabled = i_bVal
      .Page1.oPag.oCHDATNAS_1_53.enabled = i_bVal
      .Page1.oPag.oCHCOMEST_1_56.enabled = i_bVal
      .Page1.oPag.oCHPRONAS_1_58.enabled = i_bVal
      .Page1.oPag.oEVEECC_1_62.enabled = i_bVal
      .Page1.oPag.oEVEECC_2016_1_63.enabled = i_bVal
      .Page1.oPag.oEVEECC_2017_1_64.enabled = i_bVal
      .Page1.oPag.oCATPAR_1_67.enabled = i_bVal
      .Page1.oPag.oCATPAR_2016_1_68.enabled = i_bVal
      .Page1.oPag.oCHCOMUNE_1_72.enabled = i_bVal
      .Page1.oPag.oCHPROVIN_1_73.enabled = i_bVal
      .Page1.oPag.oCHCODCOM_1_75.enabled = i_bVal
      .Page1.oPag.oCHFUSCOM_1_76.enabled = i_bVal
      .Page1.oPag.oCHSTAEST_1_80.enabled = i_bVal
      .Page1.oPag.oCHCITEST_1_81.enabled = i_bVal
      .Page1.oPag.oCHINDEST_1_82.enabled = i_bVal
      .Page1.oPag.oCHCOFISC_1_86.enabled = i_bVal
      .Page1.oPag.oCHDAVERI_1_90.enabled = i_bVal
      .Page2.oPag.oCHTOT002_2_13.enabled = i_bVal
      .Page2.oPag.oCHTOT003_2_14.enabled = i_bVal
      .Page2.oPag.oCHTOT004_2_15.enabled = i_bVal
      .Page2.oPag.oCHTOT005_2_16.enabled = i_bVal
      .Page2.oPag.oTOT006_2_17.enabled = i_bVal
      .Page2.oPag.oTOT006_2016_2_18.enabled = i_bVal
      .Page2.oPag.oTOT006_2017_2_19.enabled = i_bVal
      .Page2.oPag.oCHTOT008_2_22.enabled = i_bVal
      .Page2.oPag.oCHTOT009_2_23.enabled = i_bVal
      .Page2.oPag.oCHTOT018_2_24.enabled = i_bVal
      .Page2.oPag.oCHTOT019_2_25.enabled = i_bVal
      .Page2.oPag.oCHTOT020_2_26.enabled = i_bVal
      .Page2.oPag.oCHTOT021_2_27.enabled = i_bVal
      .Page2.oPag.oCHTOT041_2_28.enabled = i_bVal
      .Page2.oPag.oCHTOT042_2_29.enabled = i_bVal
      .Page2.oPag.oCHFISTRA_2_30.enabled = i_bVal
      .Page2.oPag.oCHT1T002_2_31.enabled = i_bVal
      .Page2.oPag.oCHT1T003_2_32.enabled = i_bVal
      .Page2.oPag.oCHT1T005_2_33.enabled = i_bVal
      .Page2.oPag.oTOTM1006_2_34.enabled = i_bVal
      .Page2.oPag.oTOTM1006_2016_2_35.enabled = i_bVal
      .Page2.oPag.oTOTM1006_2017_2_36.enabled = i_bVal
      .Page2.oPag.oCHT1T018_2_39.enabled = i_bVal
      .Page2.oPag.oCHT1T019_2_40.enabled = i_bVal
      .Page2.oPag.oCHT2T002_2_41.enabled = i_bVal
      .Page2.oPag.oCHT2T003_2_42.enabled = i_bVal
      .Page2.oPag.oCHT2T005_2_43.enabled = i_bVal
      .Page2.oPag.oTOTM2006_2016_2_44.enabled = i_bVal
      .Page2.oPag.oTOTM2006_2_45.enabled = i_bVal
      .Page2.oPag.oTOTM2006_2017_2_46.enabled = i_bVal
      .Page2.oPag.oCHT2T018_2_49.enabled = i_bVal
      .Page2.oPag.oCHT2T019_2_50.enabled = i_bVal
      .Page2.oPag.oCHATT1MOD_2_51.enabled = i_bVal
      .Page2.oPag.oCHATT2MOD_2_78.enabled = i_bVal
      .Page1.oPag.oCHCERDEF_1_110.enabled = i_bVal
      .Page2.oPag.oVAR_DATI_2_84.enabled = i_bVal
      .Page2.oPag.oCHATT3MOD_2_88.enabled = i_bVal
      .Page2.oPag.oCHT3T002_2_90.enabled = i_bVal
      .Page2.oPag.oCHT3T003_2_91.enabled = i_bVal
      .Page2.oPag.oCHT3T005_2_92.enabled = i_bVal
      .Page2.oPag.oTOTM3006_2016_2_93.enabled = i_bVal
      .Page2.oPag.oTOTM3006_2017_2_94.enabled = i_bVal
      .Page2.oPag.oCHT3T018_2_97.enabled = i_bVal
      .Page2.oPag.oCHT3T019_2_98.enabled = i_bVal
      .Page1.oPag.oBtn_1_103.enabled = .Page1.oPag.oBtn_1_103.mCond()
      .Page1.oPag.oBtn_1_104.enabled = .Page1.oPag.oBtn_1_104.mCond()
      .Page2.oPag.oBtn_2_71.enabled = .Page2.oPag.oBtn_2_71.mCond()
      .Page1.oPag.oObj_1_94.enabled = i_bVal
      if i_cOp = "Query"
        .Page1.oPag.oCH__ANNO_1_3.enabled = .t.
      endif
    endwith
    this.GSRI_MCH.SetStatus(i_cOp)
    this.GSRI_MPR.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'CUDTETRH',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSRI_MCH.SetChildrenStatus(i_cOp)
  *  this.GSRI_MPR.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CUDTETRH_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHSERIAL,"CHSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CH__ANNO,"CH__ANNO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHTIPCON,"CHTIPCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHCODCON,"CHCODCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHTIPDAT,"CHTIPDAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHPERFIS,"CHPERFIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHPEREST,"CHPEREST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHCAUPRE,"CHCAUPRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHTIPOPE,"CHTIPOPE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHSCLGEN,"CHSCLGEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHCODFIS,"CHCODFIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHDESCRI,"CHDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHCOGNOM,"CHCOGNOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CH__NOME,"CH__NOME",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CH_SESSO,"CH_SESSO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHDATNAS,"CHDATNAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHCOMEST,"CHCOMEST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHPRONAS,"CHPRONAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHEVEECC,"CHEVEECC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHCATPAR,"CHCATPAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHCOMUNE,"CHCOMUNE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHPROVIN,"CHPROVIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHCODCOM,"CHCODCOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHFUSCOM,"CHFUSCOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHSTAEST,"CHSTAEST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHCITEST,"CHCITEST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHINDEST,"CHINDEST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHCOFISC,"CHCOFISC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHDAVERI,"CHDAVERI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHSCHUMA,"CHSCHUMA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHTOT002,"CHTOT002",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHTOT003,"CHTOT003",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHTOT004,"CHTOT004",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHTOT005,"CHTOT005",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHTOT006,"CHTOT006",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHTOT008,"CHTOT008",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHTOT009,"CHTOT009",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHTOT018,"CHTOT018",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHTOT019,"CHTOT019",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHTOT020,"CHTOT020",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHTOT021,"CHTOT021",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHTOT041,"CHTOT041",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHTOT042,"CHTOT042",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHFISTRA,"CHFISTRA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHT1T002,"CHT1T002",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHT1T003,"CHT1T003",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHT1T005,"CHT1T005",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHT1T006,"CHT1T006",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHT1T018,"CHT1T018",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHT1T019,"CHT1T019",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHT2T002,"CHT2T002",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHT2T003,"CHT2T003",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHT2T005,"CHT2T005",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHT2T006,"CHT2T006",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHT2T018,"CHT2T018",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHT2T019,"CHT2T019",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHATT1MOD,"CHATT1MOD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHTOT034,"CHTOT034",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHTOT035,"CHTOT035",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHPROMOD,"CHPROMOD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHPR1MOD,"CHPR1MOD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHPR2MOD,"CHPR2MOD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHPROTEC,"CHPROTEC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHPRODOC,"CHPRODOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHDATEST,"CHDATEST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHDATSTA,"CHDATSTA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHTIPREC,"CHTIPREC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHTOT010,"CHTOT010",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHTOT011,"CHTOT011",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHTOT012,"CHTOT012",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHTOT013,"CHTOT013",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHTOT014,"CHTOT014",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHTOT015,"CHTOT015",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHTOT016,"CHTOT016",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHTOT017,"CHTOT017",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHATT2MOD,"CHATT2MOD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHCERDEF,"CHCERDEF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHATT3MOD,"CHATT3MOD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHT3T002,"CHT3T002",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHT3T003,"CHT3T003",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHT3T005,"CHT3T005",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHT3T006,"CHT3T006",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHT3T018,"CHT3T018",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHT3T019,"CHT3T019",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHPR3MOD,"CHPR3MOD",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.CUDTETRH_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CUDTETRH_IDX,2])
    i_lTable = "CUDTETRH"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.CUDTETRH_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CUDTETRH_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CUDTETRH_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.CUDTETRH_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"CHSER","i_codazi,w_CHSERIAL")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into CUDTETRH
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CUDTETRH')
        i_extval=cp_InsertValODBCExtFlds(this,'CUDTETRH')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(CHSERIAL,CH__ANNO,CHTIPCON,CHCODCON,CHTIPDAT"+;
                  ",CHPERFIS,CHPEREST,CHCAUPRE,CHTIPOPE,CHSCLGEN"+;
                  ",CHCODFIS,CHDESCRI,CHCOGNOM,CH__NOME,CH_SESSO"+;
                  ",CHDATNAS,CHCOMEST,CHPRONAS,CHEVEECC,CHCATPAR"+;
                  ",CHCOMUNE,CHPROVIN,CHCODCOM,CHFUSCOM,CHSTAEST"+;
                  ",CHCITEST,CHINDEST,CHCOFISC,CHDAVERI,CHSCHUMA"+;
                  ",CHTOT002,CHTOT003,CHTOT004,CHTOT005,CHTOT006"+;
                  ",CHTOT008,CHTOT009,CHTOT018,CHTOT019,CHTOT020"+;
                  ",CHTOT021,CHTOT041,CHTOT042,CHFISTRA,CHT1T002"+;
                  ",CHT1T003,CHT1T005,CHT1T006,CHT1T018,CHT1T019"+;
                  ",CHT2T002,CHT2T003,CHT2T005,CHT2T006,CHT2T018"+;
                  ",CHT2T019,CHATT1MOD,CHTOT034,CHTOT035,CHPROMOD"+;
                  ",CHPR1MOD,CHPR2MOD,CHPROTEC,CHPRODOC,CHDATEST"+;
                  ",CHDATSTA,CHTIPREC,CHTOT010,CHTOT011,CHTOT012"+;
                  ",CHTOT013,CHTOT014,CHTOT015,CHTOT016,CHTOT017"+;
                  ",CHATT2MOD,CHCERDEF,CHATT3MOD,CHT3T002,CHT3T003"+;
                  ",CHT3T005,CHT3T006,CHT3T018,CHT3T019,CHPR3MOD "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_CHSERIAL)+;
                  ","+cp_ToStrODBC(this.w_CH__ANNO)+;
                  ","+cp_ToStrODBC(this.w_CHTIPCON)+;
                  ","+cp_ToStrODBCNull(this.w_CHCODCON)+;
                  ","+cp_ToStrODBC(this.w_CHTIPDAT)+;
                  ","+cp_ToStrODBC(this.w_CHPERFIS)+;
                  ","+cp_ToStrODBC(this.w_CHPEREST)+;
                  ","+cp_ToStrODBC(this.w_CHCAUPRE)+;
                  ","+cp_ToStrODBC(this.w_CHTIPOPE)+;
                  ","+cp_ToStrODBC(this.w_CHSCLGEN)+;
                  ","+cp_ToStrODBC(this.w_CHCODFIS)+;
                  ","+cp_ToStrODBC(this.w_CHDESCRI)+;
                  ","+cp_ToStrODBC(this.w_CHCOGNOM)+;
                  ","+cp_ToStrODBC(this.w_CH__NOME)+;
                  ","+cp_ToStrODBC(this.w_CH_SESSO)+;
                  ","+cp_ToStrODBC(this.w_CHDATNAS)+;
                  ","+cp_ToStrODBC(this.w_CHCOMEST)+;
                  ","+cp_ToStrODBC(this.w_CHPRONAS)+;
                  ","+cp_ToStrODBC(this.w_CHEVEECC)+;
                  ","+cp_ToStrODBC(this.w_CHCATPAR)+;
                  ","+cp_ToStrODBC(this.w_CHCOMUNE)+;
                  ","+cp_ToStrODBC(this.w_CHPROVIN)+;
                  ","+cp_ToStrODBCNull(this.w_CHCODCOM)+;
                  ","+cp_ToStrODBCNull(this.w_CHFUSCOM)+;
                  ","+cp_ToStrODBC(this.w_CHSTAEST)+;
                  ","+cp_ToStrODBC(this.w_CHCITEST)+;
                  ","+cp_ToStrODBC(this.w_CHINDEST)+;
                  ","+cp_ToStrODBC(this.w_CHCOFISC)+;
                  ","+cp_ToStrODBC(this.w_CHDAVERI)+;
                  ","+cp_ToStrODBC(this.w_CHSCHUMA)+;
                  ","+cp_ToStrODBC(this.w_CHTOT002)+;
                  ","+cp_ToStrODBC(this.w_CHTOT003)+;
                  ","+cp_ToStrODBC(this.w_CHTOT004)+;
                  ","+cp_ToStrODBC(this.w_CHTOT005)+;
                  ","+cp_ToStrODBC(this.w_CHTOT006)+;
                  ","+cp_ToStrODBC(this.w_CHTOT008)+;
                  ","+cp_ToStrODBC(this.w_CHTOT009)+;
                  ","+cp_ToStrODBC(this.w_CHTOT018)+;
                  ","+cp_ToStrODBC(this.w_CHTOT019)+;
                  ","+cp_ToStrODBC(this.w_CHTOT020)+;
                  ","+cp_ToStrODBC(this.w_CHTOT021)+;
                  ","+cp_ToStrODBC(this.w_CHTOT041)+;
                  ","+cp_ToStrODBC(this.w_CHTOT042)+;
                  ","+cp_ToStrODBC(this.w_CHFISTRA)+;
                  ","+cp_ToStrODBC(this.w_CHT1T002)+;
                  ","+cp_ToStrODBC(this.w_CHT1T003)+;
                  ","+cp_ToStrODBC(this.w_CHT1T005)+;
                  ","+cp_ToStrODBC(this.w_CHT1T006)+;
                  ","+cp_ToStrODBC(this.w_CHT1T018)+;
                  ","+cp_ToStrODBC(this.w_CHT1T019)+;
                  ","+cp_ToStrODBC(this.w_CHT2T002)+;
                  ","+cp_ToStrODBC(this.w_CHT2T003)+;
                  ","+cp_ToStrODBC(this.w_CHT2T005)+;
                  ","+cp_ToStrODBC(this.w_CHT2T006)+;
                  ","+cp_ToStrODBC(this.w_CHT2T018)+;
                  ","+cp_ToStrODBC(this.w_CHT2T019)+;
                  ","+cp_ToStrODBC(this.w_CHATT1MOD)+;
                  ","+cp_ToStrODBC(this.w_CHTOT034)+;
                  ","+cp_ToStrODBC(this.w_CHTOT035)+;
                  ","+cp_ToStrODBC(this.w_CHPROMOD)+;
                  ","+cp_ToStrODBC(this.w_CHPR1MOD)+;
                  ","+cp_ToStrODBC(this.w_CHPR2MOD)+;
                  ","+cp_ToStrODBC(this.w_CHPROTEC)+;
                  ","+cp_ToStrODBC(this.w_CHPRODOC)+;
                  ","+cp_ToStrODBC(this.w_CHDATEST)+;
                  ","+cp_ToStrODBC(this.w_CHDATSTA)+;
                  ","+cp_ToStrODBC(this.w_CHTIPREC)+;
                  ","+cp_ToStrODBC(this.w_CHTOT010)+;
                  ","+cp_ToStrODBC(this.w_CHTOT011)+;
                  ","+cp_ToStrODBC(this.w_CHTOT012)+;
                  ","+cp_ToStrODBC(this.w_CHTOT013)+;
                  ","+cp_ToStrODBC(this.w_CHTOT014)+;
                  ","+cp_ToStrODBC(this.w_CHTOT015)+;
                  ","+cp_ToStrODBC(this.w_CHTOT016)+;
                  ","+cp_ToStrODBC(this.w_CHTOT017)+;
                  ","+cp_ToStrODBC(this.w_CHATT2MOD)+;
                  ","+cp_ToStrODBC(this.w_CHCERDEF)+;
                  ","+cp_ToStrODBC(this.w_CHATT3MOD)+;
                  ","+cp_ToStrODBC(this.w_CHT3T002)+;
                  ","+cp_ToStrODBC(this.w_CHT3T003)+;
                  ","+cp_ToStrODBC(this.w_CHT3T005)+;
                  ","+cp_ToStrODBC(this.w_CHT3T006)+;
                  ","+cp_ToStrODBC(this.w_CHT3T018)+;
                  ","+cp_ToStrODBC(this.w_CHT3T019)+;
                  ","+cp_ToStrODBC(this.w_CHPR3MOD)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CUDTETRH')
        i_extval=cp_InsertValVFPExtFlds(this,'CUDTETRH')
        cp_CheckDeletedKey(i_cTable,0,'CHSERIAL',this.w_CHSERIAL)
        INSERT INTO (i_cTable);
              (CHSERIAL,CH__ANNO,CHTIPCON,CHCODCON,CHTIPDAT,CHPERFIS,CHPEREST,CHCAUPRE,CHTIPOPE,CHSCLGEN,CHCODFIS,CHDESCRI,CHCOGNOM,CH__NOME,CH_SESSO,CHDATNAS,CHCOMEST,CHPRONAS,CHEVEECC,CHCATPAR,CHCOMUNE,CHPROVIN,CHCODCOM,CHFUSCOM,CHSTAEST,CHCITEST,CHINDEST,CHCOFISC,CHDAVERI,CHSCHUMA,CHTOT002,CHTOT003,CHTOT004,CHTOT005,CHTOT006,CHTOT008,CHTOT009,CHTOT018,CHTOT019,CHTOT020,CHTOT021,CHTOT041,CHTOT042,CHFISTRA,CHT1T002,CHT1T003,CHT1T005,CHT1T006,CHT1T018,CHT1T019,CHT2T002,CHT2T003,CHT2T005,CHT2T006,CHT2T018,CHT2T019,CHATT1MOD,CHTOT034,CHTOT035,CHPROMOD,CHPR1MOD,CHPR2MOD,CHPROTEC,CHPRODOC,CHDATEST,CHDATSTA,CHTIPREC,CHTOT010,CHTOT011,CHTOT012,CHTOT013,CHTOT014,CHTOT015,CHTOT016,CHTOT017,CHATT2MOD,CHCERDEF,CHATT3MOD,CHT3T002,CHT3T003,CHT3T005,CHT3T006,CHT3T018,CHT3T019,CHPR3MOD  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_CHSERIAL;
                  ,this.w_CH__ANNO;
                  ,this.w_CHTIPCON;
                  ,this.w_CHCODCON;
                  ,this.w_CHTIPDAT;
                  ,this.w_CHPERFIS;
                  ,this.w_CHPEREST;
                  ,this.w_CHCAUPRE;
                  ,this.w_CHTIPOPE;
                  ,this.w_CHSCLGEN;
                  ,this.w_CHCODFIS;
                  ,this.w_CHDESCRI;
                  ,this.w_CHCOGNOM;
                  ,this.w_CH__NOME;
                  ,this.w_CH_SESSO;
                  ,this.w_CHDATNAS;
                  ,this.w_CHCOMEST;
                  ,this.w_CHPRONAS;
                  ,this.w_CHEVEECC;
                  ,this.w_CHCATPAR;
                  ,this.w_CHCOMUNE;
                  ,this.w_CHPROVIN;
                  ,this.w_CHCODCOM;
                  ,this.w_CHFUSCOM;
                  ,this.w_CHSTAEST;
                  ,this.w_CHCITEST;
                  ,this.w_CHINDEST;
                  ,this.w_CHCOFISC;
                  ,this.w_CHDAVERI;
                  ,this.w_CHSCHUMA;
                  ,this.w_CHTOT002;
                  ,this.w_CHTOT003;
                  ,this.w_CHTOT004;
                  ,this.w_CHTOT005;
                  ,this.w_CHTOT006;
                  ,this.w_CHTOT008;
                  ,this.w_CHTOT009;
                  ,this.w_CHTOT018;
                  ,this.w_CHTOT019;
                  ,this.w_CHTOT020;
                  ,this.w_CHTOT021;
                  ,this.w_CHTOT041;
                  ,this.w_CHTOT042;
                  ,this.w_CHFISTRA;
                  ,this.w_CHT1T002;
                  ,this.w_CHT1T003;
                  ,this.w_CHT1T005;
                  ,this.w_CHT1T006;
                  ,this.w_CHT1T018;
                  ,this.w_CHT1T019;
                  ,this.w_CHT2T002;
                  ,this.w_CHT2T003;
                  ,this.w_CHT2T005;
                  ,this.w_CHT2T006;
                  ,this.w_CHT2T018;
                  ,this.w_CHT2T019;
                  ,this.w_CHATT1MOD;
                  ,this.w_CHTOT034;
                  ,this.w_CHTOT035;
                  ,this.w_CHPROMOD;
                  ,this.w_CHPR1MOD;
                  ,this.w_CHPR2MOD;
                  ,this.w_CHPROTEC;
                  ,this.w_CHPRODOC;
                  ,this.w_CHDATEST;
                  ,this.w_CHDATSTA;
                  ,this.w_CHTIPREC;
                  ,this.w_CHTOT010;
                  ,this.w_CHTOT011;
                  ,this.w_CHTOT012;
                  ,this.w_CHTOT013;
                  ,this.w_CHTOT014;
                  ,this.w_CHTOT015;
                  ,this.w_CHTOT016;
                  ,this.w_CHTOT017;
                  ,this.w_CHATT2MOD;
                  ,this.w_CHCERDEF;
                  ,this.w_CHATT3MOD;
                  ,this.w_CHT3T002;
                  ,this.w_CHT3T003;
                  ,this.w_CHT3T005;
                  ,this.w_CHT3T006;
                  ,this.w_CHT3T018;
                  ,this.w_CHT3T019;
                  ,this.w_CHPR3MOD;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.CUDTETRH_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CUDTETRH_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.CUDTETRH_IDX,i_nConn)
      *
      * update CUDTETRH
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'CUDTETRH')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " CH__ANNO="+cp_ToStrODBC(this.w_CH__ANNO)+;
             ",CHTIPCON="+cp_ToStrODBC(this.w_CHTIPCON)+;
             ",CHCODCON="+cp_ToStrODBCNull(this.w_CHCODCON)+;
             ",CHTIPDAT="+cp_ToStrODBC(this.w_CHTIPDAT)+;
             ",CHPERFIS="+cp_ToStrODBC(this.w_CHPERFIS)+;
             ",CHPEREST="+cp_ToStrODBC(this.w_CHPEREST)+;
             ",CHCAUPRE="+cp_ToStrODBC(this.w_CHCAUPRE)+;
             ",CHTIPOPE="+cp_ToStrODBC(this.w_CHTIPOPE)+;
             ",CHSCLGEN="+cp_ToStrODBC(this.w_CHSCLGEN)+;
             ",CHCODFIS="+cp_ToStrODBC(this.w_CHCODFIS)+;
             ",CHDESCRI="+cp_ToStrODBC(this.w_CHDESCRI)+;
             ",CHCOGNOM="+cp_ToStrODBC(this.w_CHCOGNOM)+;
             ",CH__NOME="+cp_ToStrODBC(this.w_CH__NOME)+;
             ",CH_SESSO="+cp_ToStrODBC(this.w_CH_SESSO)+;
             ",CHDATNAS="+cp_ToStrODBC(this.w_CHDATNAS)+;
             ",CHCOMEST="+cp_ToStrODBC(this.w_CHCOMEST)+;
             ",CHPRONAS="+cp_ToStrODBC(this.w_CHPRONAS)+;
             ",CHEVEECC="+cp_ToStrODBC(this.w_CHEVEECC)+;
             ",CHCATPAR="+cp_ToStrODBC(this.w_CHCATPAR)+;
             ",CHCOMUNE="+cp_ToStrODBC(this.w_CHCOMUNE)+;
             ",CHPROVIN="+cp_ToStrODBC(this.w_CHPROVIN)+;
             ",CHCODCOM="+cp_ToStrODBCNull(this.w_CHCODCOM)+;
             ",CHFUSCOM="+cp_ToStrODBCNull(this.w_CHFUSCOM)+;
             ",CHSTAEST="+cp_ToStrODBC(this.w_CHSTAEST)+;
             ",CHCITEST="+cp_ToStrODBC(this.w_CHCITEST)+;
             ",CHINDEST="+cp_ToStrODBC(this.w_CHINDEST)+;
             ",CHCOFISC="+cp_ToStrODBC(this.w_CHCOFISC)+;
             ",CHDAVERI="+cp_ToStrODBC(this.w_CHDAVERI)+;
             ",CHSCHUMA="+cp_ToStrODBC(this.w_CHSCHUMA)+;
             ",CHTOT002="+cp_ToStrODBC(this.w_CHTOT002)+;
             ",CHTOT003="+cp_ToStrODBC(this.w_CHTOT003)+;
             ",CHTOT004="+cp_ToStrODBC(this.w_CHTOT004)+;
             ",CHTOT005="+cp_ToStrODBC(this.w_CHTOT005)+;
             ",CHTOT006="+cp_ToStrODBC(this.w_CHTOT006)+;
             ",CHTOT008="+cp_ToStrODBC(this.w_CHTOT008)+;
             ",CHTOT009="+cp_ToStrODBC(this.w_CHTOT009)+;
             ",CHTOT018="+cp_ToStrODBC(this.w_CHTOT018)+;
             ",CHTOT019="+cp_ToStrODBC(this.w_CHTOT019)+;
             ",CHTOT020="+cp_ToStrODBC(this.w_CHTOT020)+;
             ",CHTOT021="+cp_ToStrODBC(this.w_CHTOT021)+;
             ",CHTOT041="+cp_ToStrODBC(this.w_CHTOT041)+;
             ",CHTOT042="+cp_ToStrODBC(this.w_CHTOT042)+;
             ",CHFISTRA="+cp_ToStrODBC(this.w_CHFISTRA)+;
             ",CHT1T002="+cp_ToStrODBC(this.w_CHT1T002)+;
             ",CHT1T003="+cp_ToStrODBC(this.w_CHT1T003)+;
             ",CHT1T005="+cp_ToStrODBC(this.w_CHT1T005)+;
             ",CHT1T006="+cp_ToStrODBC(this.w_CHT1T006)+;
             ",CHT1T018="+cp_ToStrODBC(this.w_CHT1T018)+;
             ",CHT1T019="+cp_ToStrODBC(this.w_CHT1T019)+;
             ",CHT2T002="+cp_ToStrODBC(this.w_CHT2T002)+;
             ",CHT2T003="+cp_ToStrODBC(this.w_CHT2T003)+;
             ",CHT2T005="+cp_ToStrODBC(this.w_CHT2T005)+;
             ",CHT2T006="+cp_ToStrODBC(this.w_CHT2T006)+;
             ",CHT2T018="+cp_ToStrODBC(this.w_CHT2T018)+;
             ",CHT2T019="+cp_ToStrODBC(this.w_CHT2T019)+;
             ",CHATT1MOD="+cp_ToStrODBC(this.w_CHATT1MOD)+;
             ",CHTOT034="+cp_ToStrODBC(this.w_CHTOT034)+;
             ",CHTOT035="+cp_ToStrODBC(this.w_CHTOT035)+;
             ",CHPROMOD="+cp_ToStrODBC(this.w_CHPROMOD)+;
             ",CHPR1MOD="+cp_ToStrODBC(this.w_CHPR1MOD)+;
             ",CHPR2MOD="+cp_ToStrODBC(this.w_CHPR2MOD)+;
             ",CHPROTEC="+cp_ToStrODBC(this.w_CHPROTEC)+;
             ",CHPRODOC="+cp_ToStrODBC(this.w_CHPRODOC)+;
             ",CHDATEST="+cp_ToStrODBC(this.w_CHDATEST)+;
             ",CHDATSTA="+cp_ToStrODBC(this.w_CHDATSTA)+;
             ",CHTIPREC="+cp_ToStrODBC(this.w_CHTIPREC)+;
             ",CHTOT010="+cp_ToStrODBC(this.w_CHTOT010)+;
             ",CHTOT011="+cp_ToStrODBC(this.w_CHTOT011)+;
             ",CHTOT012="+cp_ToStrODBC(this.w_CHTOT012)+;
             ",CHTOT013="+cp_ToStrODBC(this.w_CHTOT013)+;
             ",CHTOT014="+cp_ToStrODBC(this.w_CHTOT014)+;
             ",CHTOT015="+cp_ToStrODBC(this.w_CHTOT015)+;
             ",CHTOT016="+cp_ToStrODBC(this.w_CHTOT016)+;
             ",CHTOT017="+cp_ToStrODBC(this.w_CHTOT017)+;
             ",CHATT2MOD="+cp_ToStrODBC(this.w_CHATT2MOD)+;
             ",CHCERDEF="+cp_ToStrODBC(this.w_CHCERDEF)+;
             ",CHATT3MOD="+cp_ToStrODBC(this.w_CHATT3MOD)+;
             ",CHT3T002="+cp_ToStrODBC(this.w_CHT3T002)+;
             ",CHT3T003="+cp_ToStrODBC(this.w_CHT3T003)+;
             ",CHT3T005="+cp_ToStrODBC(this.w_CHT3T005)+;
             ",CHT3T006="+cp_ToStrODBC(this.w_CHT3T006)+;
             ",CHT3T018="+cp_ToStrODBC(this.w_CHT3T018)+;
             ",CHT3T019="+cp_ToStrODBC(this.w_CHT3T019)+;
             ",CHPR3MOD="+cp_ToStrODBC(this.w_CHPR3MOD)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'CUDTETRH')
        i_cWhere = cp_PKFox(i_cTable  ,'CHSERIAL',this.w_CHSERIAL  )
        UPDATE (i_cTable) SET;
              CH__ANNO=this.w_CH__ANNO;
             ,CHTIPCON=this.w_CHTIPCON;
             ,CHCODCON=this.w_CHCODCON;
             ,CHTIPDAT=this.w_CHTIPDAT;
             ,CHPERFIS=this.w_CHPERFIS;
             ,CHPEREST=this.w_CHPEREST;
             ,CHCAUPRE=this.w_CHCAUPRE;
             ,CHTIPOPE=this.w_CHTIPOPE;
             ,CHSCLGEN=this.w_CHSCLGEN;
             ,CHCODFIS=this.w_CHCODFIS;
             ,CHDESCRI=this.w_CHDESCRI;
             ,CHCOGNOM=this.w_CHCOGNOM;
             ,CH__NOME=this.w_CH__NOME;
             ,CH_SESSO=this.w_CH_SESSO;
             ,CHDATNAS=this.w_CHDATNAS;
             ,CHCOMEST=this.w_CHCOMEST;
             ,CHPRONAS=this.w_CHPRONAS;
             ,CHEVEECC=this.w_CHEVEECC;
             ,CHCATPAR=this.w_CHCATPAR;
             ,CHCOMUNE=this.w_CHCOMUNE;
             ,CHPROVIN=this.w_CHPROVIN;
             ,CHCODCOM=this.w_CHCODCOM;
             ,CHFUSCOM=this.w_CHFUSCOM;
             ,CHSTAEST=this.w_CHSTAEST;
             ,CHCITEST=this.w_CHCITEST;
             ,CHINDEST=this.w_CHINDEST;
             ,CHCOFISC=this.w_CHCOFISC;
             ,CHDAVERI=this.w_CHDAVERI;
             ,CHSCHUMA=this.w_CHSCHUMA;
             ,CHTOT002=this.w_CHTOT002;
             ,CHTOT003=this.w_CHTOT003;
             ,CHTOT004=this.w_CHTOT004;
             ,CHTOT005=this.w_CHTOT005;
             ,CHTOT006=this.w_CHTOT006;
             ,CHTOT008=this.w_CHTOT008;
             ,CHTOT009=this.w_CHTOT009;
             ,CHTOT018=this.w_CHTOT018;
             ,CHTOT019=this.w_CHTOT019;
             ,CHTOT020=this.w_CHTOT020;
             ,CHTOT021=this.w_CHTOT021;
             ,CHTOT041=this.w_CHTOT041;
             ,CHTOT042=this.w_CHTOT042;
             ,CHFISTRA=this.w_CHFISTRA;
             ,CHT1T002=this.w_CHT1T002;
             ,CHT1T003=this.w_CHT1T003;
             ,CHT1T005=this.w_CHT1T005;
             ,CHT1T006=this.w_CHT1T006;
             ,CHT1T018=this.w_CHT1T018;
             ,CHT1T019=this.w_CHT1T019;
             ,CHT2T002=this.w_CHT2T002;
             ,CHT2T003=this.w_CHT2T003;
             ,CHT2T005=this.w_CHT2T005;
             ,CHT2T006=this.w_CHT2T006;
             ,CHT2T018=this.w_CHT2T018;
             ,CHT2T019=this.w_CHT2T019;
             ,CHATT1MOD=this.w_CHATT1MOD;
             ,CHTOT034=this.w_CHTOT034;
             ,CHTOT035=this.w_CHTOT035;
             ,CHPROMOD=this.w_CHPROMOD;
             ,CHPR1MOD=this.w_CHPR1MOD;
             ,CHPR2MOD=this.w_CHPR2MOD;
             ,CHPROTEC=this.w_CHPROTEC;
             ,CHPRODOC=this.w_CHPRODOC;
             ,CHDATEST=this.w_CHDATEST;
             ,CHDATSTA=this.w_CHDATSTA;
             ,CHTIPREC=this.w_CHTIPREC;
             ,CHTOT010=this.w_CHTOT010;
             ,CHTOT011=this.w_CHTOT011;
             ,CHTOT012=this.w_CHTOT012;
             ,CHTOT013=this.w_CHTOT013;
             ,CHTOT014=this.w_CHTOT014;
             ,CHTOT015=this.w_CHTOT015;
             ,CHTOT016=this.w_CHTOT016;
             ,CHTOT017=this.w_CHTOT017;
             ,CHATT2MOD=this.w_CHATT2MOD;
             ,CHCERDEF=this.w_CHCERDEF;
             ,CHATT3MOD=this.w_CHATT3MOD;
             ,CHT3T002=this.w_CHT3T002;
             ,CHT3T003=this.w_CHT3T003;
             ,CHT3T005=this.w_CHT3T005;
             ,CHT3T006=this.w_CHT3T006;
             ,CHT3T018=this.w_CHT3T018;
             ,CHT3T019=this.w_CHT3T019;
             ,CHPR3MOD=this.w_CHPR3MOD;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSRI_MCH : Saving
      this.GSRI_MCH.ChangeRow(this.cRowID+'      1',0;
             ,this.w_CHSERIAL,"CHSERIAL";
             )
      this.GSRI_MCH.mReplace()
      * --- GSRI_MPR : Saving
      this.GSRI_MPR.ChangeRow(this.cRowID+'      1',0;
             ,this.w_CHSERIAL,"DPSERIAL";
             )
      this.GSRI_MPR.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSRI_MCH : Deleting
    this.GSRI_MCH.ChangeRow(this.cRowID+'      1',0;
           ,this.w_CHSERIAL,"CHSERIAL";
           )
    this.GSRI_MCH.mDelete()
    * --- GSRI_MPR : Deleting
    this.GSRI_MPR.ChangeRow(this.cRowID+'      1',0;
           ,this.w_CHSERIAL,"DPSERIAL";
           )
    this.GSRI_MPR.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.CUDTETRH_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CUDTETRH_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.CUDTETRH_IDX,i_nConn)
      *
      * delete CUDTETRH
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'CHSERIAL',this.w_CHSERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CUDTETRH_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CUDTETRH_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,24,.t.)
          .link_1_25('Full')
        .DoRTCalc(26,29,.t.)
        if .o_CHCODCON<>.w_CHCODCON
            .w_CHPERFIS = .w_ANPERFIS
        endif
        if .o_CHCODCON<>.w_CHCODCON
            .w_CHPEREST = .w_ANFLGEST
        endif
        if .o_CHCODCON<>.w_CHCODCON.or. .o_CH__ANNO<>.w_CH__ANNO
            .w_CAUPRE = IIF(.w_ANCAURIT='F' OR .w_ANCAURIT=='ZO' OR .w_ANCAURIT='J' OR .w_ANCAURIT='K' OR EMPTY(.w_ANCAURIT),'A',.w_ANCAURIT)
        endif
        if .o_CH__ANNO<>.w_CH__ANNO.or. .o_CHCODCON<>.w_CHCODCON
            .w_CAUPRE_2016 = IIF(.w_ANCAURIT= 'F' OR Alltrim(.w_ANCAURIT) == 'Z' OR .w_ANCAURIT='J' OR .w_ANCAURIT='K' OR EMPTY(.w_ANCAURIT),'A',.w_ANCAURIT)
        endif
        if .o_CH__ANNO<>.w_CH__ANNO.or. .o_CHCODCON<>.w_CHCODCON
            .w_CAUPRE_2017 = IIF(Alltrim(.w_ANCAURIT) == 'Z' OR EMPTY(.w_ANCAURIT),'A',.w_ANCAURIT)
        endif
        if .o_CH__ANNO<>.w_CH__ANNO.or. .o_CAUPRE<>.w_CAUPRE.or. .o_CAUPRE_2016<>.w_CAUPRE_2016.or. .o_CAUPRE_2017<>.w_CAUPRE_2017
          .Calculate_ITYOIUGTQG()
        endif
        .DoRTCalc(35,37,.t.)
        if .o_CHCODCON<>.w_CHCODCON.or. .o_CHPEREST<>.w_CHPEREST
            .w_CHCODFIS = Upper(.w_ANCODFIS)
        endif
        if .o_CHCODCON<>.w_CHCODCON.or. .o_CHPERFIS<>.w_CHPERFIS
            .w_CHDESCRI = Upper(.w_ANDESCRI)
        endif
        if .o_CHCODCON<>.w_CHCODCON.or. .o_CHPERFIS<>.w_CHPERFIS
            .w_CHCOGNOM = IIF(.w_CHPERFIS='S',Upper(.w_ANCOGNOM),SPACE(50))
        endif
        if .o_CHCODCON<>.w_CHCODCON.or. .o_CHPERFIS<>.w_CHPERFIS
            .w_CH__NOME = IIF(.w_CHPERFIS='S',Upper(.w_AN__NOME),SPACE(50))
        endif
        if .o_CHCODCON<>.w_CHCODCON.or. .o_CHPERFIS<>.w_CHPERFIS
            .w_CH_SESSO = iif(.w_CHPERFIS='S' and not empty(.w_AN_SESSO), .w_AN_SESSO, 'M')
        endif
        if .o_CHCODCON<>.w_CHCODCON.or. .o_CHPERFIS <>.w_CHPERFIS 
            .w_CHDATNAS = IIF(.w_CHPERFIS='S',.w_ANDATNAS,CTOD('  -  -   '))
        endif
        if .o_CHCODCON<>.w_CHCODCON.or. .o_CHPERFIS<>.w_CHPERFIS
            .w_CHCOMEST = IIF(.w_CHPERFIS='S',Upper(.w_ANLOCNAS),SPACE(30))
        endif
        if .o_CHCODCON<>.w_CHCODCON.or. .o_CHPERFIS<>.w_CHPERFIS
            .w_CHPRONAS = IIF(.w_CHPERFIS='S',Upper(.w_ANPRONAS),SPACE(2))
        endif
        if .o_CHCODCON<>.w_CHCODCON.or. .o_CH__ANNO<>.w_CH__ANNO
            .w_EVEECC = iif(.w_ANEVEECC $ '134',.w_ANEVEECC,'0')
        endif
        if .o_CHCODCON<>.w_CHCODCON.or. .o_CH__ANNO<>.w_CH__ANNO
            .w_EVEECC_2016 = iif(.w_ANEVEECC $ '193AB',IIF(.w_ANEVEECC $ '13',.w_ANEVEECC,IIF(.w_ANEVEECC='9','2',IIF(.w_ANEVEECC='A','4','6'))),'0')
        endif
        if .o_CHCODCON<>.w_CHCODCON.or. .o_CH__ANNO<>.w_CH__ANNO
            .w_EVEECC_2017 = iif(.w_ANEVEECC $ '193ACD',IIF(.w_ANEVEECC $ '13',.w_ANEVEECC,IIF(.w_ANEVEECC='9','2',IIF(.w_ANEVEECC='A','4',IIF(.w_ANEVEECC='C','5','8')))),'0')
        endif
        if .o_CH__ANNO<>.w_CH__ANNO.or. .o_EVEECC<>.w_EVEECC.or. .o_EVEECC_2016<>.w_EVEECC_2016.or. .o_EVEECC_2017<>.w_EVEECC_2017
          .Calculate_GEKLRYYVGZ()
        endif
        .DoRTCalc(49,49,.t.)
        if .o_CH__ANNO<>.w_CH__ANNO.or. .o_CHCODCON<>.w_CHCODCON
            .w_CATPAR = iif(.w_ANCATPAR = 'Z1' OR .w_ANCATPAR = 'Z3','  ',.w_ANCATPAR)
        endif
        if .o_CH__ANNO<>.w_CH__ANNO.or. .o_CHCODCON<>.w_CHCODCON
            .w_CATPAR_2016 = iif(.w_ANCATPAR = 'Z1','  ',.w_ANCATPAR)
        endif
        if .o_CH__ANNO<>.w_CH__ANNO.or. .o_CATPAR<>.w_CATPAR.or. .o_CATPAR_2016<>.w_CATPAR_2016
          .Calculate_LMYABYCNHX()
        endif
        .DoRTCalc(52,52,.t.)
        if .o_CHCODCON<>.w_CHCODCON.or. .o_CHPERFIS<>.w_CHPERFIS
            .w_CHCOMUNE = IIF(.w_CHPEREST<>'S',Upper(.w_ANLOCALI),SPACE(30))
        endif
        if .o_CHCODCON<>.w_CHCODCON.or. .o_CHPEREST<>.w_CHPEREST
            .w_CHPROVIN = IIF(.w_CHPEREST<>'S',Upper(.w_ANPROVIN),SPACE(2))
        endif
        if .o_CHCODCON<>.w_CHCODCON.or. .o_CHPEREST<>.w_CHPEREST.or. .o_CHCAUPRE<>.w_CHCAUPRE
            .w_CHCODCOM = IIF(.w_CHPEREST<>'S' AND .w_CHCAUPRE='N',Upper(.w_ANCODCOM),SPACE(4))
          .link_1_75('Full')
        endif
        if .o_CHCODCON<>.w_CHCODCON.or. .o_CHPEREST<>.w_CHPEREST.or. .o_CHCAUPRE<>.w_CHCAUPRE
            .w_CHFUSCOM = Space(4)
          .link_1_76('Full')
        endif
        if .o_CHCODCON<>.w_CHCODCON.or. .o_CHPEREST<>.w_CHPEREST
            .w_CHSTAEST = IIF(.w_CHPEREST='S',Upper(RIGHT(Alltrim(.w_NACODEST),3)),SPACE(3))
        endif
        if .o_CHCODCON<>.w_CHCODCON.or. .o_CHPEREST<>.w_CHPEREST
            .w_CHCITEST = IIF(.w_CHPEREST='S',Upper(.w_ANLOCALI),SPACE(30))
        endif
        if .o_CHCODCON<>.w_CHCODCON.or. .o_CHPEREST<>.w_CHPEREST
            .w_CHINDEST = IIF(.w_CHPEREST='S',Upper(.w_ANINDIRI),SPACE(35))
        endif
        if .o_CHCODCON<>.w_CHCODCON.or. .o_CHPEREST<>.w_CHPEREST
            .w_CHCOFISC = iif(.w_Chperest='S',Upper(iif(Empty(.w_Ancofisc), .w_Ancodfis, .w_Ancofisc)),Space(25))
        endif
        if .o_Chtipope<>.w_Chtipope
            .w_CHDAVERI = iif(.w_Chtipope $ 'SA','S','N')
        endif
        .DoRTCalc(62,63,.t.)
        if .o_CHCODCON<>.w_CHCODCON
            .w_CHSCHUMA = .w_ANSCHUMA
        endif
        if  .o_CH__ANNO<>.w_CH__ANNO
          .WriteTo_GSRI_MCH()
        endif
        if .o_Chcaupre<>.w_Chcaupre.or. .o_Chtot003<>.w_Chtot003.or. .o_Ch__Anno<>.w_Ch__Anno.or. .o_Caupre<>.w_Caupre.or. .o_Caupre_2016<>.w_Caupre_2016.or. .o_Caupre_2017<>.w_Caupre_2017
            .w_CHTOT002 = iif((Alltrim(.w_Chcaupre) $ 'G-H-I' Or .w_Chtot003='S') ,iif(Empty(.w_Chtot002),.w_Ch__Anno,.w_Chtot002),0)
        endif
        .DoRTCalc(66,66,.t.)
        if .o_Chtot004<>.w_Chtot004
            .w_CHTOT004 = iif(.w_Chtot004<0,0,.w_Chtot004)
        endif
        if .o_Chtot005<>.w_Chtot005
            .w_CHTOT005 = iif(.w_Chtot005<0,0,.w_Chtot005)
        endif
        if .o_CHTOT005<>.w_CHTOT005.or. .o_CHPEREST<>.w_CHPEREST.or. .o_ANFLSGRE<>.w_ANFLSGRE.or. .o_CH__ANNO<>.w_CH__ANNO
            .w_TOT006 = iif(.w_CHTOT005=0 OR  (.w_CHPEREST='S' AND .w_ANFLSGRE='S'),' ','3')
        endif
        if .o_CHTOT005<>.w_CHTOT005.or. .o_CHPEREST<>.w_CHPEREST.or. .o_ANFLSGRE<>.w_ANFLSGRE.or. .o_CH__ANNO<>.w_CH__ANNO
            .w_TOT006_2016 = iif(.w_CHTOT005=0 OR  (.w_CHPEREST='S' AND .w_ANFLSGRE='S'),' ','6')
        endif
        if .o_CHTOT005<>.w_CHTOT005.or. .o_CHPEREST<>.w_CHPEREST.or. .o_ANFLSGRE<>.w_ANFLSGRE.or. .o_CH__ANNO<>.w_CH__ANNO
            .w_TOT006_2017 = iif(.w_CHTOT005=0 OR  (.w_CHPEREST='S' AND .w_ANFLSGRE='S'),' ','7')
        endif
        if .o_CH__ANNO<>.w_CH__ANNO.or. .o_TOT006<>.w_TOT006.or. .o_TOT006_2016<>.w_TOT006_2016.or. .o_TOT006_2017<>.w_TOT006_2017
          .Calculate_OOXWLXZHEC()
        endif
        .DoRTCalc(72,72,.t.)
        if .o_Chtot008<>.w_Chtot008
            .w_CHTOT008 = iif(.w_Chtot008<0,0,.w_Chtot008)
        endif
        if .o_Chtot009<>.w_Chtot009
            .w_CHTOT009 = iif(.w_Chtot009<0,0,.w_Chtot009)
        endif
        if .o_Chtot018<>.w_Chtot018
            .w_CHTOT018 = iif(.w_Chtot018<0,0,.w_Chtot018)
        endif
        if .o_Chtot019<>.w_Chtot019
            .w_CHTOT019 = iif(.w_Chtot019<0,0,.w_Chtot019)
        endif
        if .o_Chtot020<>.w_Chtot020
            .w_CHTOT020 = iif(.w_Chtot020<0,0,.w_Chtot020)
        endif
        if .o_Chtot021<>.w_Chtot021.or. .o_Chcaupre<>.w_Chcaupre
            .w_CHTOT021 = iif(.w_Chtot021>=0 And Alltrim(.w_Chcaupre) $ 'X-Y',.w_Chtot021,0)
        endif
        if .o_Chtot041<>.w_Chtot041
            .w_CHTOT041 = iif(.w_Chtot041<0,0,.w_Chtot041)
        endif
        if .o_Chtot042<>.w_Chtot042
            .w_CHTOT042 = iif(.w_Chtot042<0,0,.w_Chtot042)
        endif
        if .o_CHCODCON<>.w_CHCODCON.or. .o_CHPEREST<>.w_CHPEREST
            .w_CHFISTRA = Space(16)
        endif
        Local l_Dep1,l_Dep2
         l_Dep1= .o_Chcaupre<>.w_Chcaupre .or. .o_Cht1t003<>.w_Cht1t003 .or. .o_Chatt1Mod<>.w_Chatt1Mod .or. .o_Ch__Anno<>.w_Ch__Anno .or. .o_Caupre<>.w_Caupre
        l_Dep2= .o_Caupre_2016<>.w_Caupre_2016 .or. .o_Caupre_2017<>.w_Caupre_2017
        if m.l_Dep1 .or. m.l_Dep2
            .w_CHT1T002 = iif((Alltrim(.w_Chcaupre) $ 'G-H-I' Or .w_Cht1t003='S') And .w_Chatt1Mod='S',iif(Empty(.w_Cht1t002),.w_Ch__Anno,.w_Cht1t002),0)
        endif
        if .o_Chatt1mod<>.w_Chatt1mod
            .w_CHT1T003 = 'N'
        endif
        if .o_Cht1t005<>.w_Cht1t005.or. .o_Chatt1mod<>.w_Chatt1mod
            .w_CHT1T005 = iif(.w_Cht1t005<0 Or .w_Chatt1Mod='N',0,.w_Cht1t005)
        endif
        if .o_CHT1T005<>.w_CHT1T005.or. .o_CHPEREST<>.w_CHPEREST.or. .o_ANFLSGRE<>.w_ANFLSGRE.or. .o_CH__ANNO<>.w_CH__ANNO.or. .o_CHATT1MOD<>.w_CHATT1MOD
            .w_TOTM1006 = iif(.w_CHT1T005=0 OR  (.w_CHPEREST='S' AND .w_ANFLSGRE='S') OR .w_CHATT1MOD='N' ,' ','3')
        endif
        if .o_CHT1T005<>.w_CHT1T005.or. .o_CHPEREST<>.w_CHPEREST.or. .o_ANFLSGRE<>.w_ANFLSGRE.or. .o_CH__ANNO<>.w_CH__ANNO.or. .o_CHATT1MOD<>.w_CHATT1MOD
            .w_TOTM1006_2016 = iif(.w_CHT1T005=0 OR  (.w_CHPEREST='S' AND .w_ANFLSGRE='S') OR .w_CHATT1MOD='N' ,' ','6')
        endif
        if .o_CHT1T005<>.w_CHT1T005.or. .o_CHPEREST<>.w_CHPEREST.or. .o_ANFLSGRE<>.w_ANFLSGRE.or. .o_CH__ANNO<>.w_CH__ANNO.or. .o_CHATT1MOD<>.w_CHATT1MOD
            .w_TOTM1006_2017 = iif(.w_CHT1T005=0 OR  (.w_CHPEREST='S' AND .w_ANFLSGRE='S') OR .w_CHATT1MOD='N' ,' ','7')
        endif
        if .o_CH__ANNO<>.w_CH__ANNO.or. .o_TOTM1006<>.w_TOTM1006.or. .o_TOTM1006_2016<>.w_TOTM1006_2016.or. .o_TOTM1006_2017<>.w_TOTM1006_2017
          .Calculate_PXTBQMNVHX()
        endif
        .DoRTCalc(88,88,.t.)
        if .o_Cht1t018<>.w_Cht1t018.or. .o_Chatt1mod<>.w_Chatt1mod
            .w_CHT1T018 = iif(.w_Cht1t018<0 Or .w_Chatt1Mod='N',0,.w_Cht1t018)
        endif
        if .o_Cht1t019<>.w_Cht1t019.or. .o_Chatt1Mod<>.w_Chatt1Mod
            .w_CHT1T019 = iif(.w_Cht1t019<0 Or .w_Chatt1Mod='N',0,.w_Cht1t019)
        endif
        Local l_Dep1,l_Dep2
         l_Dep1= .o_Chcaupre<>.w_Chcaupre .or. .o_Cht2t003<>.w_Cht2t003 .or. .o_Chatt2Mod<>.w_Chatt2Mod .or. .o_Ch__Anno<>.w_Ch__Anno .or. .o_Caupre<>.w_Caupre
        l_Dep2= .o_Caupre_2016<>.w_Caupre_2016 .or. .o_Caupre_2017<>.w_Caupre_2017
        if m.l_Dep1 .or. m.l_Dep2
            .w_CHT2T002 = iif((Alltrim(.w_Chcaupre) $ 'G-H-I' Or .w_Cht2t003='S') And .w_Chatt2Mod='S',iif(Empty(.w_Cht2t002),.w_Ch__Anno,.w_Cht2t002),0)
        endif
        if .o_CHATT2MOD<>.w_CHATT2MOD
            .w_CHT2T003 = 'N'
        endif
        if .o_Cht2t005<>.w_Cht2t005.or. .o_Chatt2Mod<>.w_Chatt2Mod
            .w_CHT2T005 = iif(.w_Cht2t005<0 Or .w_Chatt2Mod='N',0,.w_Cht2t005)
        endif
        if .o_CHT2T005<>.w_CHT2T005.or. .o_CHPEREST<>.w_CHPEREST.or. .o_ANFLSGRE<>.w_ANFLSGRE.or. .o_CH__ANNO<>.w_CH__ANNO.or. .o_CHATT2MOD<>.w_CHATT2MOD
            .w_TOTM2006_2016 = iif(.w_CHT2T005=0 OR  (.w_CHPEREST='S' AND .w_ANFLSGRE='S')  OR .w_CHATT2MOD='N',' ','6')
        endif
        if .o_CHT2T005<>.w_CHT2T005.or. .o_CHPEREST<>.w_CHPEREST.or. .o_ANFLSGRE<>.w_ANFLSGRE.or. .o_CH__ANNO<>.w_CH__ANNO.or. .o_CHATT2MOD<>.w_CHATT2MOD
            .w_TOTM2006 = iif(.w_CHT2T005=0 OR  (.w_CHPEREST='S' AND .w_ANFLSGRE='S')  OR .w_CHATT2MOD='N',' ','3')
        endif
        if .o_CHT2T005<>.w_CHT2T005.or. .o_CHPEREST<>.w_CHPEREST.or. .o_ANFLSGRE<>.w_ANFLSGRE.or. .o_CH__ANNO<>.w_CH__ANNO.or. .o_CHATT2MOD<>.w_CHATT2MOD
            .w_TOTM2006_2017 = iif(.w_CHT2T005=0 OR  (.w_CHPEREST='S' AND .w_ANFLSGRE='S')  OR .w_CHATT2MOD='N',' ','7')
        endif
        if .o_CH__ANNO<>.w_CH__ANNO.or. .o_TOTM2006<>.w_TOTM2006.or. .o_TOTM2006_2016<>.w_TOTM2006_2016.or. .o_TOTM2006_2017<>.w_TOTM2006_2017
          .Calculate_BDTKCXAIKO()
        endif
        .DoRTCalc(97,97,.t.)
        if .o_Cht2t018<>.w_Cht2t018.or. .o_Chatt2Mod<>.w_Chatt2Mod
            .w_CHT2T018 = iif(.w_Cht2t018<0 Or .w_Chatt2Mod='N',0,.w_Cht2t018)
        endif
        if .o_Cht2t019<>.w_Cht2t019.or. .o_Chatt2Mod<>.w_Chatt2Mod
            .w_CHT2T019 = iif(.w_Cht2t019<0 Or .w_Chatt2Mod='N',0,.w_Cht2t019)
        endif
        .DoRTCalc(100,103,.t.)
        if .o_CHATT1MOD<>.w_CHATT1MOD
            .w_CHPR1MOD = iif(.w_CHATT1MOD='S',2,0)
        endif
        if .o_CHATT2MOD<>.w_CHATT2MOD
            .w_CHPR2MOD = iif(.w_CHATT2MOD='S',3,0)
        endif
        .oPgFrm.Page1.oPag.oObj_1_94.Calculate()
          .link_1_95('Full')
        if  .o_CH__ANNO<>.w_CH__ANNO
          .WriteTo_GSRI_MPR()
        endif
        .DoRTCalc(107,126,.t.)
        if .o_CH__ANNO<>.w_CH__ANNO
            .w_CHATT3MOD = 'N'
        endif
        Local l_Dep1,l_Dep2
         l_Dep1= .o_Chcaupre<>.w_Chcaupre .or. .o_Cht3t003<>.w_Cht3t003 .or. .o_Chatt3Mod<>.w_Chatt3Mod .or. .o_Ch__Anno<>.w_Ch__Anno .or. .o_Caupre<>.w_Caupre
        l_Dep2= .o_Caupre_2016<>.w_Caupre_2016 .or. .o_Caupre_2017<>.w_Caupre_2017
        if m.l_Dep1 .or. m.l_Dep2
            .w_CHT3T002 = iif((Alltrim(.w_Chcaupre) $ 'G-H-I' Or .w_Cht3t003='S') And .w_Chatt3Mod='S',iif(Empty(.w_Cht3t002),.w_Ch__Anno,.w_Cht3t002),0)
        endif
        if .o_CHATT3MOD<>.w_CHATT3MOD.or. .o_CH__ANNO<>.w_CH__ANNO
            .w_CHT3T003 = 'N'
        endif
        if .o_Cht3t005<>.w_Cht3t005.or. .o_Chatt3Mod<>.w_Chatt3Mod.or. .o_CH__ANNO<>.w_CH__ANNO
            .w_CHT3T005 = iif(.w_Cht3t005<0 Or .w_Chatt3Mod='N',0,.w_Cht3t005)
        endif
        if .o_CHT3T005<>.w_CHT3T005.or. .o_CHPEREST<>.w_CHPEREST.or. .o_ANFLSGRE<>.w_ANFLSGRE.or. .o_CH__ANNO<>.w_CH__ANNO.or. .o_CHATT3MOD<>.w_CHATT3MOD
            .w_TOTM3006_2016 = iif(.w_CHT3T005=0 OR (.w_CHPEREST='S' AND .w_ANFLSGRE='S') OR .w_CHATT3MOD='N',' ','6')
        endif
        if .o_CHT3T005<>.w_CHT3T005.or. .o_CHPEREST<>.w_CHPEREST.or. .o_ANFLSGRE<>.w_ANFLSGRE.or. .o_CH__ANNO<>.w_CH__ANNO.or. .o_CHATT3MOD<>.w_CHATT3MOD
            .w_TOTM3006_2017 = iif(.w_CHT3T005=0 OR (.w_CHPEREST='S' AND .w_ANFLSGRE='S') OR .w_CHATT3MOD='N',' ','7')
        endif
        if .o_CH__ANNO<>.w_CH__ANNO.or. .o_TOTM3006_2016<>.w_TOTM3006_2016.or. .o_TOTM3006_2017<>.w_TOTM3006_2017
          .Calculate_ZWZAQRHRNS()
        endif
        .DoRTCalc(133,133,.t.)
        if .o_Cht3t018<>.w_Cht3t018.or. .o_Chatt3Mod<>.w_Chatt3Mod.or. .o_CH__ANNO<>.w_CH__ANNO
            .w_CHT3T018 = iif(.w_Cht3t018<0 Or .w_Chatt3Mod='N',0,.w_Cht3t018)
        endif
        if .o_Cht3t019<>.w_Cht3t019.or. .o_Chatt3Mod<>.w_Chatt3Mod.or. .o_CH__ANNO<>.w_CH__ANNO
            .w_CHT3T019 = iif(.w_Cht3t019<0 Or .w_Chatt3Mod='N',0,.w_Cht3t019)
        endif
        if .o_CHATT3MOD<>.w_CHATT3MOD.or. .o_CH__ANNO<>.w_CH__ANNO
            .w_CHPR3MOD = iif(.w_CHATT3MOD='S',4,0)
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_codazi<>.w_codazi
           cp_AskTableProg(this,i_nConn,"CHSER","i_codazi,w_CHSERIAL")
          .op_CHSERIAL = .w_CHSERIAL
        endif
        .op_codazi = .w_codazi
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_94.Calculate()
    endwith
  return

  proc Calculate_ITYOIUGTQG()
    with this
          * --- Valorizzo campo CHCAUPRE
          .w_CHCAUPRE = iif(.w_CH__ANNO<2016,.w_CAUPRE,iif(.w_CH__ANNO=2016,.w_CAUPRE_2016,.w_CAUPRE_2017))
    endwith
  endproc
  proc Calculate_GEKLRYYVGZ()
    with this
          * --- Valorizzo campo CHEVEECC
          .w_CHEVEECC = iif(.w_CH__ANNO<2016,.w_EVEECC,iif(.w_CH__ANNO=2016,.w_EVEECC_2016,.w_EVEECC_2017))
    endwith
  endproc
  proc Calculate_LMYABYCNHX()
    with this
          * --- Valorizzo campo CHCATPAR
          .w_CHCATPAR = iif(.w_CH__ANNO<2016,.w_CATPAR,.w_CATPAR_2016)
    endwith
  endproc
  proc Calculate_OOXWLXZHEC()
    with this
          * --- Valorizzo campo CHTOT006
          .w_CHTOT006 = iif(.w_CH__ANNO<2016,.w_TOT006,iif(.w_CH__ANNO=2016,.w_TOT006_2016,.w_TOT006_2017))
    endwith
  endproc
  proc Calculate_PXTBQMNVHX()
    with this
          * --- Valorizzo campo CHT1T006
          .w_CHT1T006 = iif(.w_CH__ANNO<2016,.w_TOTM1006,iif(.w_CH__ANNO=2016,.w_TOTM1006_2016,.w_TOTM1006_2017))
    endwith
  endproc
  proc Calculate_BDTKCXAIKO()
    with this
          * --- Valorizzo campo CHT2T006
          .w_CHT2T006 = iif(.w_CH__ANNO<2016,.w_TOTM2006,iif(.w_CH__ANNO=2016,.w_TOTM2006_2016,.w_TOTM2006_2017))
    endwith
  endproc
  proc Calculate_XARRQFLIQK()
    with this
          * --- Valorizzazione stato certificazione
          Gsri_bvs(this;
             )
    endwith
  endproc
  proc Calculate_ZWZAQRHRNS()
    with this
          * --- Valorizzo campo CHT3T006
          .w_CHT3T006 = iif(.w_CH__ANNO=2016,.w_TOTM3006_2016,.w_TOTM3006_2017)
    endwith
  endproc
  proc Calculate_LFLBLFKCYE()
    with this
          * --- Valorizzo Causale prestazione,Categorie particolari ed eventi eccezionali
          .w_CAUPRE = .w_CHCAUPRE
          .w_CAUPRE_2016 = .w_CHCAUPRE
          .w_CAUPRE_2017 = .w_CHCAUPRE
          .w_CATPAR = .w_CHCATPAR
          .w_CATPAR_2016 = .w_CHCATPAR
          .w_EVEECC = .w_CHEVEECC
          .w_EVEECC_2016 = .w_CHEVEECC
          .w_EVEECC_2017 = .w_CHEVEECC
    endwith
  endproc
  proc Calculate_ZRBKDLBKWS()
    with this
          * --- Valorizzo codice somme non soggette
          .w_TOT006 = .w_CHTOT006
          .w_TOT006_2016 = .w_CHTOT006
          .w_TOT006_2017 = .w_CHTOT006
          .w_TOTM1006 = .w_CHT1T006
          .w_TOTM1006_2016 = .w_CHT1T006
          .w_TOTM1006_2017 = .w_CHT1T006
          .w_TOTM2006 = .w_CHT2T006
          .w_TOTM2006_2016 = .w_CHT2T006
          .w_TOTM2006_2017 = .w_CHT2T006
          .w_TOTM3006_2016 = .w_CHT3T006
          .w_TOTM3006_2017 = .w_CHT3T006
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCH__ANNO_1_3.enabled = this.oPgFrm.Page1.oPag.oCH__ANNO_1_3.mCond()
    this.oPgFrm.Page1.oPag.oCHCODCON_1_5.enabled = this.oPgFrm.Page1.oPag.oCHCODCON_1_5.mCond()
    this.oPgFrm.Page1.oPag.oCAUPRE_1_32.enabled = this.oPgFrm.Page1.oPag.oCAUPRE_1_32.mCond()
    this.oPgFrm.Page1.oPag.oCAUPRE_2016_1_33.enabled = this.oPgFrm.Page1.oPag.oCAUPRE_2016_1_33.mCond()
    this.oPgFrm.Page1.oPag.oCAUPRE_2017_1_34.enabled = this.oPgFrm.Page1.oPag.oCAUPRE_2017_1_34.mCond()
    this.oPgFrm.Page1.oPag.oCHTIPOPE_1_37.enabled = this.oPgFrm.Page1.oPag.oCHTIPOPE_1_37.mCond()
    this.oPgFrm.Page1.oPag.oCHCODFIS_1_39.enabled = this.oPgFrm.Page1.oPag.oCHCODFIS_1_39.mCond()
    this.oPgFrm.Page1.oPag.oCHDESCRI_1_46.enabled = this.oPgFrm.Page1.oPag.oCHDESCRI_1_46.mCond()
    this.oPgFrm.Page1.oPag.oCHCOGNOM_1_47.enabled = this.oPgFrm.Page1.oPag.oCHCOGNOM_1_47.mCond()
    this.oPgFrm.Page1.oPag.oCH__NOME_1_48.enabled = this.oPgFrm.Page1.oPag.oCH__NOME_1_48.mCond()
    this.oPgFrm.Page1.oPag.oCH_SESSO_1_52.enabled = this.oPgFrm.Page1.oPag.oCH_SESSO_1_52.mCond()
    this.oPgFrm.Page1.oPag.oCHDATNAS_1_53.enabled = this.oPgFrm.Page1.oPag.oCHDATNAS_1_53.mCond()
    this.oPgFrm.Page1.oPag.oCHCOMEST_1_56.enabled = this.oPgFrm.Page1.oPag.oCHCOMEST_1_56.mCond()
    this.oPgFrm.Page1.oPag.oCHPRONAS_1_58.enabled = this.oPgFrm.Page1.oPag.oCHPRONAS_1_58.mCond()
    this.oPgFrm.Page1.oPag.oEVEECC_1_62.enabled = this.oPgFrm.Page1.oPag.oEVEECC_1_62.mCond()
    this.oPgFrm.Page1.oPag.oEVEECC_2016_1_63.enabled = this.oPgFrm.Page1.oPag.oEVEECC_2016_1_63.mCond()
    this.oPgFrm.Page1.oPag.oEVEECC_2017_1_64.enabled = this.oPgFrm.Page1.oPag.oEVEECC_2017_1_64.mCond()
    this.oPgFrm.Page1.oPag.oCATPAR_1_67.enabled = this.oPgFrm.Page1.oPag.oCATPAR_1_67.mCond()
    this.oPgFrm.Page1.oPag.oCATPAR_2016_1_68.enabled = this.oPgFrm.Page1.oPag.oCATPAR_2016_1_68.mCond()
    this.oPgFrm.Page1.oPag.oCHCOMUNE_1_72.enabled = this.oPgFrm.Page1.oPag.oCHCOMUNE_1_72.mCond()
    this.oPgFrm.Page1.oPag.oCHPROVIN_1_73.enabled = this.oPgFrm.Page1.oPag.oCHPROVIN_1_73.mCond()
    this.oPgFrm.Page1.oPag.oCHCODCOM_1_75.enabled = this.oPgFrm.Page1.oPag.oCHCODCOM_1_75.mCond()
    this.oPgFrm.Page1.oPag.oCHFUSCOM_1_76.enabled = this.oPgFrm.Page1.oPag.oCHFUSCOM_1_76.mCond()
    this.oPgFrm.Page1.oPag.oCHSTAEST_1_80.enabled = this.oPgFrm.Page1.oPag.oCHSTAEST_1_80.mCond()
    this.oPgFrm.Page1.oPag.oCHCITEST_1_81.enabled = this.oPgFrm.Page1.oPag.oCHCITEST_1_81.mCond()
    this.oPgFrm.Page1.oPag.oCHINDEST_1_82.enabled = this.oPgFrm.Page1.oPag.oCHINDEST_1_82.mCond()
    this.oPgFrm.Page1.oPag.oCHCOFISC_1_86.enabled = this.oPgFrm.Page1.oPag.oCHCOFISC_1_86.mCond()
    this.oPgFrm.Page1.oPag.oCHDAVERI_1_90.enabled = this.oPgFrm.Page1.oPag.oCHDAVERI_1_90.mCond()
    this.oPgFrm.Page2.oPag.oCHTOT002_2_13.enabled = this.oPgFrm.Page2.oPag.oCHTOT002_2_13.mCond()
    this.oPgFrm.Page2.oPag.oCHTOT003_2_14.enabled = this.oPgFrm.Page2.oPag.oCHTOT003_2_14.mCond()
    this.oPgFrm.Page2.oPag.oCHTOT004_2_15.enabled = this.oPgFrm.Page2.oPag.oCHTOT004_2_15.mCond()
    this.oPgFrm.Page2.oPag.oCHTOT005_2_16.enabled = this.oPgFrm.Page2.oPag.oCHTOT005_2_16.mCond()
    this.oPgFrm.Page2.oPag.oTOT006_2_17.enabled = this.oPgFrm.Page2.oPag.oTOT006_2_17.mCond()
    this.oPgFrm.Page2.oPag.oTOT006_2016_2_18.enabled = this.oPgFrm.Page2.oPag.oTOT006_2016_2_18.mCond()
    this.oPgFrm.Page2.oPag.oTOT006_2017_2_19.enabled = this.oPgFrm.Page2.oPag.oTOT006_2017_2_19.mCond()
    this.oPgFrm.Page2.oPag.oCHTOT008_2_22.enabled = this.oPgFrm.Page2.oPag.oCHTOT008_2_22.mCond()
    this.oPgFrm.Page2.oPag.oCHTOT009_2_23.enabled = this.oPgFrm.Page2.oPag.oCHTOT009_2_23.mCond()
    this.oPgFrm.Page2.oPag.oCHTOT018_2_24.enabled = this.oPgFrm.Page2.oPag.oCHTOT018_2_24.mCond()
    this.oPgFrm.Page2.oPag.oCHTOT019_2_25.enabled = this.oPgFrm.Page2.oPag.oCHTOT019_2_25.mCond()
    this.oPgFrm.Page2.oPag.oCHTOT020_2_26.enabled = this.oPgFrm.Page2.oPag.oCHTOT020_2_26.mCond()
    this.oPgFrm.Page2.oPag.oCHTOT021_2_27.enabled = this.oPgFrm.Page2.oPag.oCHTOT021_2_27.mCond()
    this.oPgFrm.Page2.oPag.oCHTOT041_2_28.enabled = this.oPgFrm.Page2.oPag.oCHTOT041_2_28.mCond()
    this.oPgFrm.Page2.oPag.oCHTOT042_2_29.enabled = this.oPgFrm.Page2.oPag.oCHTOT042_2_29.mCond()
    this.oPgFrm.Page2.oPag.oCHFISTRA_2_30.enabled = this.oPgFrm.Page2.oPag.oCHFISTRA_2_30.mCond()
    this.oPgFrm.Page2.oPag.oCHT1T002_2_31.enabled = this.oPgFrm.Page2.oPag.oCHT1T002_2_31.mCond()
    this.oPgFrm.Page2.oPag.oCHT1T003_2_32.enabled = this.oPgFrm.Page2.oPag.oCHT1T003_2_32.mCond()
    this.oPgFrm.Page2.oPag.oCHT1T005_2_33.enabled = this.oPgFrm.Page2.oPag.oCHT1T005_2_33.mCond()
    this.oPgFrm.Page2.oPag.oTOTM1006_2_34.enabled = this.oPgFrm.Page2.oPag.oTOTM1006_2_34.mCond()
    this.oPgFrm.Page2.oPag.oTOTM1006_2016_2_35.enabled = this.oPgFrm.Page2.oPag.oTOTM1006_2016_2_35.mCond()
    this.oPgFrm.Page2.oPag.oTOTM1006_2017_2_36.enabled = this.oPgFrm.Page2.oPag.oTOTM1006_2017_2_36.mCond()
    this.oPgFrm.Page2.oPag.oCHT1T018_2_39.enabled = this.oPgFrm.Page2.oPag.oCHT1T018_2_39.mCond()
    this.oPgFrm.Page2.oPag.oCHT1T019_2_40.enabled = this.oPgFrm.Page2.oPag.oCHT1T019_2_40.mCond()
    this.oPgFrm.Page2.oPag.oCHT2T002_2_41.enabled = this.oPgFrm.Page2.oPag.oCHT2T002_2_41.mCond()
    this.oPgFrm.Page2.oPag.oCHT2T003_2_42.enabled = this.oPgFrm.Page2.oPag.oCHT2T003_2_42.mCond()
    this.oPgFrm.Page2.oPag.oCHT2T005_2_43.enabled = this.oPgFrm.Page2.oPag.oCHT2T005_2_43.mCond()
    this.oPgFrm.Page2.oPag.oTOTM2006_2016_2_44.enabled = this.oPgFrm.Page2.oPag.oTOTM2006_2016_2_44.mCond()
    this.oPgFrm.Page2.oPag.oTOTM2006_2_45.enabled = this.oPgFrm.Page2.oPag.oTOTM2006_2_45.mCond()
    this.oPgFrm.Page2.oPag.oTOTM2006_2017_2_46.enabled = this.oPgFrm.Page2.oPag.oTOTM2006_2017_2_46.mCond()
    this.oPgFrm.Page2.oPag.oCHT2T018_2_49.enabled = this.oPgFrm.Page2.oPag.oCHT2T018_2_49.mCond()
    this.oPgFrm.Page2.oPag.oCHT2T019_2_50.enabled = this.oPgFrm.Page2.oPag.oCHT2T019_2_50.mCond()
    this.oPgFrm.Page2.oPag.oCHATT1MOD_2_51.enabled = this.oPgFrm.Page2.oPag.oCHATT1MOD_2_51.mCond()
    this.oPgFrm.Page2.oPag.oCHATT2MOD_2_78.enabled = this.oPgFrm.Page2.oPag.oCHATT2MOD_2_78.mCond()
    this.oPgFrm.Page2.oPag.oCHATT3MOD_2_88.enabled = this.oPgFrm.Page2.oPag.oCHATT3MOD_2_88.mCond()
    this.oPgFrm.Page2.oPag.oCHT3T002_2_90.enabled = this.oPgFrm.Page2.oPag.oCHT3T002_2_90.mCond()
    this.oPgFrm.Page2.oPag.oCHT3T003_2_91.enabled = this.oPgFrm.Page2.oPag.oCHT3T003_2_91.mCond()
    this.oPgFrm.Page2.oPag.oCHT3T005_2_92.enabled = this.oPgFrm.Page2.oPag.oCHT3T005_2_92.mCond()
    this.oPgFrm.Page2.oPag.oTOTM3006_2016_2_93.enabled = this.oPgFrm.Page2.oPag.oTOTM3006_2016_2_93.mCond()
    this.oPgFrm.Page2.oPag.oTOTM3006_2017_2_94.enabled = this.oPgFrm.Page2.oPag.oTOTM3006_2017_2_94.mCond()
    this.oPgFrm.Page2.oPag.oCHT3T018_2_97.enabled = this.oPgFrm.Page2.oPag.oCHT3T018_2_97.mCond()
    this.oPgFrm.Page2.oPag.oCHT3T019_2_98.enabled = this.oPgFrm.Page2.oPag.oCHT3T019_2_98.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_103.enabled = this.oPgFrm.Page1.oPag.oBtn_1_103.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_104.enabled = this.oPgFrm.Page1.oPag.oBtn_1_104.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_71.enabled = this.oPgFrm.Page2.oPag.oBtn_2_71.mCond()
    this.oPgFrm.Page2.oPag.oLinkPC_2_62.enabled = this.oPgFrm.Page2.oPag.oLinkPC_2_62.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCAUPRE_1_32.visible=!this.oPgFrm.Page1.oPag.oCAUPRE_1_32.mHide()
    this.oPgFrm.Page1.oPag.oCAUPRE_2016_1_33.visible=!this.oPgFrm.Page1.oPag.oCAUPRE_2016_1_33.mHide()
    this.oPgFrm.Page1.oPag.oCAUPRE_2017_1_34.visible=!this.oPgFrm.Page1.oPag.oCAUPRE_2017_1_34.mHide()
    this.oPgFrm.Page1.oPag.oEVEECC_1_62.visible=!this.oPgFrm.Page1.oPag.oEVEECC_1_62.mHide()
    this.oPgFrm.Page1.oPag.oEVEECC_2016_1_63.visible=!this.oPgFrm.Page1.oPag.oEVEECC_2016_1_63.mHide()
    this.oPgFrm.Page1.oPag.oEVEECC_2017_1_64.visible=!this.oPgFrm.Page1.oPag.oEVEECC_2017_1_64.mHide()
    this.oPgFrm.Page1.oPag.oCATPAR_1_67.visible=!this.oPgFrm.Page1.oPag.oCATPAR_1_67.mHide()
    this.oPgFrm.Page1.oPag.oCATPAR_2016_1_68.visible=!this.oPgFrm.Page1.oPag.oCATPAR_2016_1_68.mHide()
    this.oPgFrm.Page1.oPag.oCHFUSCOM_1_76.visible=!this.oPgFrm.Page1.oPag.oCHFUSCOM_1_76.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_4.visible=!this.oPgFrm.Page2.oPag.oStr_2_4.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_10.visible=!this.oPgFrm.Page2.oPag.oStr_2_10.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_12.visible=!this.oPgFrm.Page2.oPag.oStr_2_12.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_89.visible=!this.oPgFrm.Page1.oPag.oStr_1_89.mHide()
    this.oPgFrm.Page2.oPag.oTOT006_2_17.visible=!this.oPgFrm.Page2.oPag.oTOT006_2_17.mHide()
    this.oPgFrm.Page2.oPag.oTOT006_2016_2_18.visible=!this.oPgFrm.Page2.oPag.oTOT006_2016_2_18.mHide()
    this.oPgFrm.Page2.oPag.oTOT006_2017_2_19.visible=!this.oPgFrm.Page2.oPag.oTOT006_2017_2_19.mHide()
    this.oPgFrm.Page2.oPag.oTOTM1006_2_34.visible=!this.oPgFrm.Page2.oPag.oTOTM1006_2_34.mHide()
    this.oPgFrm.Page2.oPag.oTOTM1006_2016_2_35.visible=!this.oPgFrm.Page2.oPag.oTOTM1006_2016_2_35.mHide()
    this.oPgFrm.Page2.oPag.oTOTM1006_2017_2_36.visible=!this.oPgFrm.Page2.oPag.oTOTM1006_2017_2_36.mHide()
    this.oPgFrm.Page2.oPag.oTOTM2006_2016_2_44.visible=!this.oPgFrm.Page2.oPag.oTOTM2006_2016_2_44.mHide()
    this.oPgFrm.Page2.oPag.oTOTM2006_2_45.visible=!this.oPgFrm.Page2.oPag.oTOTM2006_2_45.mHide()
    this.oPgFrm.Page2.oPag.oTOTM2006_2017_2_46.visible=!this.oPgFrm.Page2.oPag.oTOTM2006_2017_2_46.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_60.visible=!this.oPgFrm.Page2.oPag.oStr_2_60.mHide()
    this.oPgFrm.Page2.oPag.oGDPROCER_2_61.visible=!this.oPgFrm.Page2.oPag.oGDPROCER_2_61.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_97.visible=!this.oPgFrm.Page1.oPag.oStr_1_97.mHide()
    this.oPgFrm.Page1.oPag.oCHPROTEC_1_98.visible=!this.oPgFrm.Page1.oPag.oCHPROTEC_1_98.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_99.visible=!this.oPgFrm.Page1.oPag.oStr_1_99.mHide()
    this.oPgFrm.Page1.oPag.oCHPRODOC_1_100.visible=!this.oPgFrm.Page1.oPag.oCHPRODOC_1_100.mHide()
    this.oPgFrm.Page1.oPag.oCHDATSTA_1_102.visible=!this.oPgFrm.Page1.oPag.oCHDATSTA_1_102.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_103.visible=!this.oPgFrm.Page1.oPag.oBtn_1_103.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_104.visible=!this.oPgFrm.Page1.oPag.oBtn_1_104.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_106.visible=!this.oPgFrm.Page1.oPag.oStr_1_106.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_107.visible=!this.oPgFrm.Page1.oPag.oStr_1_107.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_108.visible=!this.oPgFrm.Page1.oPag.oStr_1_108.mHide()
    this.oPgFrm.Page1.oPag.oSTATO_1_112.visible=!this.oPgFrm.Page1.oPag.oSTATO_1_112.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_113.visible=!this.oPgFrm.Page1.oPag.oStr_1_113.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_89.visible=!this.oPgFrm.Page2.oPag.oStr_2_89.mHide()
    this.oPgFrm.Page2.oPag.oTOTM3006_2016_2_93.visible=!this.oPgFrm.Page2.oPag.oTOTM3006_2016_2_93.mHide()
    this.oPgFrm.Page2.oPag.oTOTM3006_2017_2_94.visible=!this.oPgFrm.Page2.oPag.oTOTM3006_2017_2_94.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_115.visible=!this.oPgFrm.Page1.oPag.oStr_1_115.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsri_ach
    * instanzio il figlio della seconda pagina immediatamente
    IF Upper(CEVENT)='INIT'
      if Upper(this.GSRI_MCH.class)='STDDYNAMICCHILD'
        This.oPgFrm.Pages[3].opag.uienable(.T.)
        This.oPgFrm.ActivePage=1
      Endif
      if Upper(this.GSRI_MPR.class)='STDDYNAMICCHILD'
        This.oPgFrm.Pages[2].opag.uienable(.T.)
        This.oPgFrm.ActivePage=1
      Endif
    Endif
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Blank")
          .Calculate_ITYOIUGTQG()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Blank")
          .Calculate_GEKLRYYVGZ()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Blank")
          .Calculate_LMYABYCNHX()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Blank")
          .Calculate_OOXWLXZHEC()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Blank")
          .Calculate_PXTBQMNVHX()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Blank")
          .Calculate_BDTKCXAIKO()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_94.Event(cEvent)
        if lower(cEvent)==lower("ValorizzazioneStato")
          .Calculate_XARRQFLIQK()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Blank")
          .Calculate_ZWZAQRHRNS()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Load")
          .Calculate_LFLBLFKCYE()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Load")
          .Calculate_ZRBKDLBKWS()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CHCODCON
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CHCODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFR',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CHCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_CHTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCAURIT,ANCODFIS,ANPERFIS,ANCOGNOM,AN__NOME,AN_SESSO,ANDATNAS,ANLOCNAS,ANPRONAS,ANCATPAR,ANEVEECC,ANFLGEST,ANLOCALI,ANPROVIN,ANCODCOM,ANCOFISC,ANINDIRI,ANNAZION,ANRITENU,ANFLSGRE,ANSCHUMA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_CHTIPCON;
                     ,'ANCODICE',trim(this.w_CHCODCON))
          select ANTIPCON,ANCODICE,ANDESCRI,ANCAURIT,ANCODFIS,ANPERFIS,ANCOGNOM,AN__NOME,AN_SESSO,ANDATNAS,ANLOCNAS,ANPRONAS,ANCATPAR,ANEVEECC,ANFLGEST,ANLOCALI,ANPROVIN,ANCODCOM,ANCOFISC,ANINDIRI,ANNAZION,ANRITENU,ANFLSGRE,ANSCHUMA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CHCODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CHCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_CHTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCAURIT,ANCODFIS,ANPERFIS,ANCOGNOM,AN__NOME,AN_SESSO,ANDATNAS,ANLOCNAS,ANPRONAS,ANCATPAR,ANEVEECC,ANFLGEST,ANLOCALI,ANPROVIN,ANCODCOM,ANCOFISC,ANINDIRI,ANNAZION,ANRITENU,ANFLSGRE,ANSCHUMA";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CHCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_CHTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANCAURIT,ANCODFIS,ANPERFIS,ANCOGNOM,AN__NOME,AN_SESSO,ANDATNAS,ANLOCNAS,ANPRONAS,ANCATPAR,ANEVEECC,ANFLGEST,ANLOCALI,ANPROVIN,ANCODCOM,ANCOFISC,ANINDIRI,ANNAZION,ANRITENU,ANFLSGRE,ANSCHUMA;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CHCODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCHCODCON_1_5'),i_cWhere,'GSAR_AFR',"Elenco fornitori",'GSRI_ADH.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CHTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCAURIT,ANCODFIS,ANPERFIS,ANCOGNOM,AN__NOME,AN_SESSO,ANDATNAS,ANLOCNAS,ANPRONAS,ANCATPAR,ANEVEECC,ANFLGEST,ANLOCALI,ANPROVIN,ANCODCOM,ANCOFISC,ANINDIRI,ANNAZION,ANRITENU,ANFLSGRE,ANSCHUMA";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANCAURIT,ANCODFIS,ANPERFIS,ANCOGNOM,AN__NOME,AN_SESSO,ANDATNAS,ANLOCNAS,ANPRONAS,ANCATPAR,ANEVEECC,ANFLGEST,ANLOCALI,ANPROVIN,ANCODCOM,ANCOFISC,ANINDIRI,ANNAZION,ANRITENU,ANFLSGRE,ANSCHUMA;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice fornitore inesistente o non soggetto a ritenute")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCAURIT,ANCODFIS,ANPERFIS,ANCOGNOM,AN__NOME,AN_SESSO,ANDATNAS,ANLOCNAS,ANPRONAS,ANCATPAR,ANEVEECC,ANFLGEST,ANLOCALI,ANPROVIN,ANCODCOM,ANCOFISC,ANINDIRI,ANNAZION,ANRITENU,ANFLSGRE,ANSCHUMA";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_CHTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANCAURIT,ANCODFIS,ANPERFIS,ANCOGNOM,AN__NOME,AN_SESSO,ANDATNAS,ANLOCNAS,ANPRONAS,ANCATPAR,ANEVEECC,ANFLGEST,ANLOCALI,ANPROVIN,ANCODCOM,ANCOFISC,ANINDIRI,ANNAZION,ANRITENU,ANFLSGRE,ANSCHUMA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CHCODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCAURIT,ANCODFIS,ANPERFIS,ANCOGNOM,AN__NOME,AN_SESSO,ANDATNAS,ANLOCNAS,ANPRONAS,ANCATPAR,ANEVEECC,ANFLGEST,ANLOCALI,ANPROVIN,ANCODCOM,ANCOFISC,ANINDIRI,ANNAZION,ANRITENU,ANFLSGRE,ANSCHUMA";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CHCODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_CHTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_CHTIPCON;
                       ,'ANCODICE',this.w_CHCODCON)
            select ANTIPCON,ANCODICE,ANDESCRI,ANCAURIT,ANCODFIS,ANPERFIS,ANCOGNOM,AN__NOME,AN_SESSO,ANDATNAS,ANLOCNAS,ANPRONAS,ANCATPAR,ANEVEECC,ANFLGEST,ANLOCALI,ANPROVIN,ANCODCOM,ANCOFISC,ANINDIRI,ANNAZION,ANRITENU,ANFLSGRE,ANSCHUMA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CHCODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_ANDESCRI = NVL(_Link_.ANDESCRI,space(40))
      this.w_ANCAURIT = NVL(_Link_.ANCAURIT,space(2))
      this.w_ANCODFIS = NVL(_Link_.ANCODFIS,space(16))
      this.w_ANPERFIS = NVL(_Link_.ANPERFIS,space(1))
      this.w_ANCOGNOM = NVL(_Link_.ANCOGNOM,space(40))
      this.w_AN__NOME = NVL(_Link_.AN__NOME,space(20))
      this.w_AN_SESSO = NVL(_Link_.AN_SESSO,space(1))
      this.w_ANDATNAS = NVL(cp_ToDate(_Link_.ANDATNAS),ctod("  /  /  "))
      this.w_ANLOCNAS = NVL(_Link_.ANLOCNAS,space(30))
      this.w_ANPRONAS = NVL(_Link_.ANPRONAS,space(2))
      this.w_ANCATPAR = NVL(_Link_.ANCATPAR,space(2))
      this.w_ANEVEECC = NVL(_Link_.ANEVEECC,space(1))
      this.w_ANFLGEST = NVL(_Link_.ANFLGEST,space(1))
      this.w_ANLOCALI = NVL(_Link_.ANLOCALI,space(30))
      this.w_ANPROVIN = NVL(_Link_.ANPROVIN,space(2))
      this.w_ANCODCOM = NVL(_Link_.ANCODCOM,space(4))
      this.w_ANCOFISC = NVL(_Link_.ANCOFISC,space(25))
      this.w_ANINDIRI = NVL(_Link_.ANINDIRI,space(35))
      this.w_ANNAZION = NVL(_Link_.ANNAZION,space(3))
      this.w_ANRITENU = NVL(_Link_.ANRITENU,space(1))
      this.w_ANFLSGRE = NVL(_Link_.ANFLSGRE,space(1))
      this.w_ANSCHUMA = NVL(_Link_.ANSCHUMA,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CHCODCON = space(15)
      endif
      this.w_ANDESCRI = space(40)
      this.w_ANCAURIT = space(2)
      this.w_ANCODFIS = space(16)
      this.w_ANPERFIS = space(1)
      this.w_ANCOGNOM = space(40)
      this.w_AN__NOME = space(20)
      this.w_AN_SESSO = space(1)
      this.w_ANDATNAS = ctod("  /  /  ")
      this.w_ANLOCNAS = space(30)
      this.w_ANPRONAS = space(2)
      this.w_ANCATPAR = space(2)
      this.w_ANEVEECC = space(1)
      this.w_ANFLGEST = space(1)
      this.w_ANLOCALI = space(30)
      this.w_ANPROVIN = space(2)
      this.w_ANCODCOM = space(4)
      this.w_ANCOFISC = space(25)
      this.w_ANINDIRI = space(35)
      this.w_ANNAZION = space(3)
      this.w_ANRITENU = space(1)
      this.w_ANFLSGRE = space(1)
      this.w_ANSCHUMA = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_ANRITENU $ 'CS'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice fornitore inesistente o non soggetto a ritenute")
        endif
        this.w_CHCODCON = space(15)
        this.w_ANDESCRI = space(40)
        this.w_ANCAURIT = space(2)
        this.w_ANCODFIS = space(16)
        this.w_ANPERFIS = space(1)
        this.w_ANCOGNOM = space(40)
        this.w_AN__NOME = space(20)
        this.w_AN_SESSO = space(1)
        this.w_ANDATNAS = ctod("  /  /  ")
        this.w_ANLOCNAS = space(30)
        this.w_ANPRONAS = space(2)
        this.w_ANCATPAR = space(2)
        this.w_ANEVEECC = space(1)
        this.w_ANFLGEST = space(1)
        this.w_ANLOCALI = space(30)
        this.w_ANPROVIN = space(2)
        this.w_ANCODCOM = space(4)
        this.w_ANCOFISC = space(25)
        this.w_ANINDIRI = space(35)
        this.w_ANNAZION = space(3)
        this.w_ANRITENU = space(1)
        this.w_ANFLSGRE = space(1)
        this.w_ANSCHUMA = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CHCODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_5(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 23 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+23<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_5.ANCODICE as ANCODICE105"+ ",link_1_5.ANDESCRI as ANDESCRI105"+ ",link_1_5.ANCAURIT as ANCAURIT105"+ ",link_1_5.ANCODFIS as ANCODFIS105"+ ",link_1_5.ANPERFIS as ANPERFIS105"+ ",link_1_5.ANCOGNOM as ANCOGNOM105"+ ",link_1_5.AN__NOME as AN__NOME105"+ ",link_1_5.AN_SESSO as AN_SESSO105"+ ",link_1_5.ANDATNAS as ANDATNAS105"+ ",link_1_5.ANLOCNAS as ANLOCNAS105"+ ",link_1_5.ANPRONAS as ANPRONAS105"+ ",link_1_5.ANCATPAR as ANCATPAR105"+ ",link_1_5.ANEVEECC as ANEVEECC105"+ ",link_1_5.ANFLGEST as ANFLGEST105"+ ",link_1_5.ANLOCALI as ANLOCALI105"+ ",link_1_5.ANPROVIN as ANPROVIN105"+ ",link_1_5.ANCODCOM as ANCODCOM105"+ ",link_1_5.ANCOFISC as ANCOFISC105"+ ",link_1_5.ANINDIRI as ANINDIRI105"+ ",link_1_5.ANNAZION as ANNAZION105"+ ",link_1_5.ANRITENU as ANRITENU105"+ ",link_1_5.ANFLSGRE as ANFLSGRE105"+ ",link_1_5.ANSCHUMA as ANSCHUMA105"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_5 on CUDTETRH.CHCODCON=link_1_5.ANCODICE"+" and CUDTETRH.CHTIPCON=link_1_5.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+23
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_5"
          i_cKey=i_cKey+'+" and CUDTETRH.CHCODCON=link_1_5.ANCODICE(+)"'+'+" and CUDTETRH.CHTIPCON=link_1_5.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+23
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ANNAZION
  func Link_1_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.NAZIONI_IDX,3]
    i_lTable = "NAZIONI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2], .t., this.NAZIONI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANNAZION) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANNAZION)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NACODNAZ,NACODEST";
                   +" from "+i_cTable+" "+i_lTable+" where NACODNAZ="+cp_ToStrODBC(this.w_ANNAZION);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NACODNAZ',this.w_ANNAZION)
            select NACODNAZ,NACODEST;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANNAZION = NVL(_Link_.NACODNAZ,space(3))
      this.w_NACODEST = NVL(_Link_.NACODEST,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_ANNAZION = space(3)
      endif
      this.w_NACODEST = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])+'\'+cp_ToStr(_Link_.NACODNAZ,1)
      cp_ShowWarn(i_cKey,this.NAZIONI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANNAZION Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CHCODCOM
  func Link_1_75(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ENTI_COM_IDX,3]
    i_lTable = "ENTI_COM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ENTI_COM_IDX,2], .t., this.ENTI_COM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ENTI_COM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CHCODCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('Gscg_Aec',True,'ENTI_COM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ECCODICE like "+cp_ToStrODBC(trim(this.w_CHCODCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select ECCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ECCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ECCODICE',trim(this.w_CHCODCOM))
          select ECCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ECCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CHCODCOM)==trim(_Link_.ECCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CHCODCOM) and !this.bDontReportError
            deferred_cp_zoom('ENTI_COM','*','ECCODICE',cp_AbsName(oSource.parent,'oCHCODCOM_1_75'),i_cWhere,'Gscg_Aec',"Codice enti/comuni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ECCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where ECCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ECCODICE',oSource.xKey(1))
            select ECCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CHCODCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ECCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where ECCODICE="+cp_ToStrODBC(this.w_CHCODCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ECCODICE',this.w_CHCODCOM)
            select ECCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CHCODCOM = NVL(_Link_.ECCODICE,space(4))
    else
      if i_cCtrl<>'Load'
        this.w_CHCODCOM = space(4)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ENTI_COM_IDX,2])+'\'+cp_ToStr(_Link_.ECCODICE,1)
      cp_ShowWarn(i_cKey,this.ENTI_COM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CHCODCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CHFUSCOM
  func Link_1_76(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ENTI_COM_IDX,3]
    i_lTable = "ENTI_COM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ENTI_COM_IDX,2], .t., this.ENTI_COM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ENTI_COM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CHFUSCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('Gscg_Aec',True,'ENTI_COM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ECCODICE like "+cp_ToStrODBC(trim(this.w_CHFUSCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select ECCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ECCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ECCODICE',trim(this.w_CHFUSCOM))
          select ECCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ECCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CHFUSCOM)==trim(_Link_.ECCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CHFUSCOM) and !this.bDontReportError
            deferred_cp_zoom('ENTI_COM','*','ECCODICE',cp_AbsName(oSource.parent,'oCHFUSCOM_1_76'),i_cWhere,'Gscg_Aec',"Codice enti/comuni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ECCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where ECCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ECCODICE',oSource.xKey(1))
            select ECCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CHFUSCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ECCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where ECCODICE="+cp_ToStrODBC(this.w_CHFUSCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ECCODICE',this.w_CHFUSCOM)
            select ECCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CHFUSCOM = NVL(_Link_.ECCODICE,space(4))
    else
      if i_cCtrl<>'Load'
        this.w_CHFUSCOM = space(4)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ENTI_COM_IDX,2])+'\'+cp_ToStr(_Link_.ECCODICE,1)
      cp_ShowWarn(i_cKey,this.ENTI_COM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CHFUSCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GTSERIAL
  func Link_1_95(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GENDTEST_IDX,3]
    i_lTable = "GENDTEST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GENDTEST_IDX,2], .t., this.GENDTEST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GENDTEST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GTSERIAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GTSERIAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GDSEREST,GDSERIAL,GDPROCER";
                   +" from "+i_cTable+" "+i_lTable+" where GDSEREST="+cp_ToStrODBC(this.w_GTSERIAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GDSEREST',this.w_GTSERIAL)
            select GDSEREST,GDSERIAL,GDPROCER;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GTSERIAL = NVL(_Link_.GDSEREST,space(10))
      this.w_GDSERIAL = NVL(_Link_.GDSERIAL,space(10))
      this.w_GDPROCER = NVL(_Link_.GDPROCER,0)
    else
      if i_cCtrl<>'Load'
        this.w_GTSERIAL = space(10)
      endif
      this.w_GDSERIAL = space(10)
      this.w_GDPROCER = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GENDTEST_IDX,2])+'\'+cp_ToStr(_Link_.GDSEREST,1)
      cp_ShowWarn(i_cKey,this.GENDTEST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GTSERIAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCH__ANNO_1_3.value==this.w_CH__ANNO)
      this.oPgFrm.Page1.oPag.oCH__ANNO_1_3.value=this.w_CH__ANNO
    endif
    if not(this.oPgFrm.Page1.oPag.oCHCODCON_1_5.value==this.w_CHCODCON)
      this.oPgFrm.Page1.oPag.oCHCODCON_1_5.value=this.w_CHCODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oCHPERFIS_1_30.RadioValue()==this.w_CHPERFIS)
      this.oPgFrm.Page1.oPag.oCHPERFIS_1_30.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCHPEREST_1_31.RadioValue()==this.w_CHPEREST)
      this.oPgFrm.Page1.oPag.oCHPEREST_1_31.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCAUPRE_1_32.RadioValue()==this.w_CAUPRE)
      this.oPgFrm.Page1.oPag.oCAUPRE_1_32.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCAUPRE_2016_1_33.RadioValue()==this.w_CAUPRE_2016)
      this.oPgFrm.Page1.oPag.oCAUPRE_2016_1_33.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCAUPRE_2017_1_34.RadioValue()==this.w_CAUPRE_2017)
      this.oPgFrm.Page1.oPag.oCAUPRE_2017_1_34.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCHTIPOPE_1_37.RadioValue()==this.w_CHTIPOPE)
      this.oPgFrm.Page1.oPag.oCHTIPOPE_1_37.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCHSCLGEN_1_38.RadioValue()==this.w_CHSCLGEN)
      this.oPgFrm.Page1.oPag.oCHSCLGEN_1_38.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCHCODFIS_1_39.value==this.w_CHCODFIS)
      this.oPgFrm.Page1.oPag.oCHCODFIS_1_39.value=this.w_CHCODFIS
    endif
    if not(this.oPgFrm.Page1.oPag.oCHDESCRI_1_46.value==this.w_CHDESCRI)
      this.oPgFrm.Page1.oPag.oCHDESCRI_1_46.value=this.w_CHDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCHCOGNOM_1_47.value==this.w_CHCOGNOM)
      this.oPgFrm.Page1.oPag.oCHCOGNOM_1_47.value=this.w_CHCOGNOM
    endif
    if not(this.oPgFrm.Page1.oPag.oCH__NOME_1_48.value==this.w_CH__NOME)
      this.oPgFrm.Page1.oPag.oCH__NOME_1_48.value=this.w_CH__NOME
    endif
    if not(this.oPgFrm.Page1.oPag.oCH_SESSO_1_52.RadioValue()==this.w_CH_SESSO)
      this.oPgFrm.Page1.oPag.oCH_SESSO_1_52.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCHDATNAS_1_53.value==this.w_CHDATNAS)
      this.oPgFrm.Page1.oPag.oCHDATNAS_1_53.value=this.w_CHDATNAS
    endif
    if not(this.oPgFrm.Page1.oPag.oCHCOMEST_1_56.value==this.w_CHCOMEST)
      this.oPgFrm.Page1.oPag.oCHCOMEST_1_56.value=this.w_CHCOMEST
    endif
    if not(this.oPgFrm.Page1.oPag.oCHPRONAS_1_58.value==this.w_CHPRONAS)
      this.oPgFrm.Page1.oPag.oCHPRONAS_1_58.value=this.w_CHPRONAS
    endif
    if not(this.oPgFrm.Page1.oPag.oEVEECC_1_62.RadioValue()==this.w_EVEECC)
      this.oPgFrm.Page1.oPag.oEVEECC_1_62.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oEVEECC_2016_1_63.RadioValue()==this.w_EVEECC_2016)
      this.oPgFrm.Page1.oPag.oEVEECC_2016_1_63.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oEVEECC_2017_1_64.RadioValue()==this.w_EVEECC_2017)
      this.oPgFrm.Page1.oPag.oEVEECC_2017_1_64.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCATPAR_1_67.RadioValue()==this.w_CATPAR)
      this.oPgFrm.Page1.oPag.oCATPAR_1_67.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCATPAR_2016_1_68.RadioValue()==this.w_CATPAR_2016)
      this.oPgFrm.Page1.oPag.oCATPAR_2016_1_68.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCHCOMUNE_1_72.value==this.w_CHCOMUNE)
      this.oPgFrm.Page1.oPag.oCHCOMUNE_1_72.value=this.w_CHCOMUNE
    endif
    if not(this.oPgFrm.Page1.oPag.oCHPROVIN_1_73.value==this.w_CHPROVIN)
      this.oPgFrm.Page1.oPag.oCHPROVIN_1_73.value=this.w_CHPROVIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCHCODCOM_1_75.value==this.w_CHCODCOM)
      this.oPgFrm.Page1.oPag.oCHCODCOM_1_75.value=this.w_CHCODCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oCHFUSCOM_1_76.value==this.w_CHFUSCOM)
      this.oPgFrm.Page1.oPag.oCHFUSCOM_1_76.value=this.w_CHFUSCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oCHSTAEST_1_80.value==this.w_CHSTAEST)
      this.oPgFrm.Page1.oPag.oCHSTAEST_1_80.value=this.w_CHSTAEST
    endif
    if not(this.oPgFrm.Page1.oPag.oCHCITEST_1_81.value==this.w_CHCITEST)
      this.oPgFrm.Page1.oPag.oCHCITEST_1_81.value=this.w_CHCITEST
    endif
    if not(this.oPgFrm.Page1.oPag.oCHINDEST_1_82.value==this.w_CHINDEST)
      this.oPgFrm.Page1.oPag.oCHINDEST_1_82.value=this.w_CHINDEST
    endif
    if not(this.oPgFrm.Page1.oPag.oCHCOFISC_1_86.value==this.w_CHCOFISC)
      this.oPgFrm.Page1.oPag.oCHCOFISC_1_86.value=this.w_CHCOFISC
    endif
    if not(this.oPgFrm.Page1.oPag.oCHDAVERI_1_90.RadioValue()==this.w_CHDAVERI)
      this.oPgFrm.Page1.oPag.oCHDAVERI_1_90.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCHSCHUMA_1_93.RadioValue()==this.w_CHSCHUMA)
      this.oPgFrm.Page1.oPag.oCHSCHUMA_1_93.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCHTOT002_2_13.value==this.w_CHTOT002)
      this.oPgFrm.Page2.oPag.oCHTOT002_2_13.value=this.w_CHTOT002
    endif
    if not(this.oPgFrm.Page2.oPag.oCHTOT003_2_14.RadioValue()==this.w_CHTOT003)
      this.oPgFrm.Page2.oPag.oCHTOT003_2_14.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCHTOT004_2_15.value==this.w_CHTOT004)
      this.oPgFrm.Page2.oPag.oCHTOT004_2_15.value=this.w_CHTOT004
    endif
    if not(this.oPgFrm.Page2.oPag.oCHTOT005_2_16.value==this.w_CHTOT005)
      this.oPgFrm.Page2.oPag.oCHTOT005_2_16.value=this.w_CHTOT005
    endif
    if not(this.oPgFrm.Page2.oPag.oTOT006_2_17.RadioValue()==this.w_TOT006)
      this.oPgFrm.Page2.oPag.oTOT006_2_17.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTOT006_2016_2_18.RadioValue()==this.w_TOT006_2016)
      this.oPgFrm.Page2.oPag.oTOT006_2016_2_18.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTOT006_2017_2_19.RadioValue()==this.w_TOT006_2017)
      this.oPgFrm.Page2.oPag.oTOT006_2017_2_19.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCHTOT008_2_22.value==this.w_CHTOT008)
      this.oPgFrm.Page2.oPag.oCHTOT008_2_22.value=this.w_CHTOT008
    endif
    if not(this.oPgFrm.Page2.oPag.oCHTOT009_2_23.value==this.w_CHTOT009)
      this.oPgFrm.Page2.oPag.oCHTOT009_2_23.value=this.w_CHTOT009
    endif
    if not(this.oPgFrm.Page2.oPag.oCHTOT018_2_24.value==this.w_CHTOT018)
      this.oPgFrm.Page2.oPag.oCHTOT018_2_24.value=this.w_CHTOT018
    endif
    if not(this.oPgFrm.Page2.oPag.oCHTOT019_2_25.value==this.w_CHTOT019)
      this.oPgFrm.Page2.oPag.oCHTOT019_2_25.value=this.w_CHTOT019
    endif
    if not(this.oPgFrm.Page2.oPag.oCHTOT020_2_26.value==this.w_CHTOT020)
      this.oPgFrm.Page2.oPag.oCHTOT020_2_26.value=this.w_CHTOT020
    endif
    if not(this.oPgFrm.Page2.oPag.oCHTOT021_2_27.value==this.w_CHTOT021)
      this.oPgFrm.Page2.oPag.oCHTOT021_2_27.value=this.w_CHTOT021
    endif
    if not(this.oPgFrm.Page2.oPag.oCHTOT041_2_28.value==this.w_CHTOT041)
      this.oPgFrm.Page2.oPag.oCHTOT041_2_28.value=this.w_CHTOT041
    endif
    if not(this.oPgFrm.Page2.oPag.oCHTOT042_2_29.value==this.w_CHTOT042)
      this.oPgFrm.Page2.oPag.oCHTOT042_2_29.value=this.w_CHTOT042
    endif
    if not(this.oPgFrm.Page2.oPag.oCHFISTRA_2_30.value==this.w_CHFISTRA)
      this.oPgFrm.Page2.oPag.oCHFISTRA_2_30.value=this.w_CHFISTRA
    endif
    if not(this.oPgFrm.Page2.oPag.oCHT1T002_2_31.value==this.w_CHT1T002)
      this.oPgFrm.Page2.oPag.oCHT1T002_2_31.value=this.w_CHT1T002
    endif
    if not(this.oPgFrm.Page2.oPag.oCHT1T003_2_32.RadioValue()==this.w_CHT1T003)
      this.oPgFrm.Page2.oPag.oCHT1T003_2_32.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCHT1T005_2_33.value==this.w_CHT1T005)
      this.oPgFrm.Page2.oPag.oCHT1T005_2_33.value=this.w_CHT1T005
    endif
    if not(this.oPgFrm.Page2.oPag.oTOTM1006_2_34.RadioValue()==this.w_TOTM1006)
      this.oPgFrm.Page2.oPag.oTOTM1006_2_34.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTOTM1006_2016_2_35.RadioValue()==this.w_TOTM1006_2016)
      this.oPgFrm.Page2.oPag.oTOTM1006_2016_2_35.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTOTM1006_2017_2_36.RadioValue()==this.w_TOTM1006_2017)
      this.oPgFrm.Page2.oPag.oTOTM1006_2017_2_36.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCHT1T018_2_39.value==this.w_CHT1T018)
      this.oPgFrm.Page2.oPag.oCHT1T018_2_39.value=this.w_CHT1T018
    endif
    if not(this.oPgFrm.Page2.oPag.oCHT1T019_2_40.value==this.w_CHT1T019)
      this.oPgFrm.Page2.oPag.oCHT1T019_2_40.value=this.w_CHT1T019
    endif
    if not(this.oPgFrm.Page2.oPag.oCHT2T002_2_41.value==this.w_CHT2T002)
      this.oPgFrm.Page2.oPag.oCHT2T002_2_41.value=this.w_CHT2T002
    endif
    if not(this.oPgFrm.Page2.oPag.oCHT2T003_2_42.RadioValue()==this.w_CHT2T003)
      this.oPgFrm.Page2.oPag.oCHT2T003_2_42.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCHT2T005_2_43.value==this.w_CHT2T005)
      this.oPgFrm.Page2.oPag.oCHT2T005_2_43.value=this.w_CHT2T005
    endif
    if not(this.oPgFrm.Page2.oPag.oTOTM2006_2016_2_44.RadioValue()==this.w_TOTM2006_2016)
      this.oPgFrm.Page2.oPag.oTOTM2006_2016_2_44.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTOTM2006_2_45.RadioValue()==this.w_TOTM2006)
      this.oPgFrm.Page2.oPag.oTOTM2006_2_45.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTOTM2006_2017_2_46.RadioValue()==this.w_TOTM2006_2017)
      this.oPgFrm.Page2.oPag.oTOTM2006_2017_2_46.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCHT2T018_2_49.value==this.w_CHT2T018)
      this.oPgFrm.Page2.oPag.oCHT2T018_2_49.value=this.w_CHT2T018
    endif
    if not(this.oPgFrm.Page2.oPag.oCHT2T019_2_50.value==this.w_CHT2T019)
      this.oPgFrm.Page2.oPag.oCHT2T019_2_50.value=this.w_CHT2T019
    endif
    if not(this.oPgFrm.Page2.oPag.oCHATT1MOD_2_51.RadioValue()==this.w_CHATT1MOD)
      this.oPgFrm.Page2.oPag.oCHATT1MOD_2_51.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCHPROMOD_2_55.value==this.w_CHPROMOD)
      this.oPgFrm.Page2.oPag.oCHPROMOD_2_55.value=this.w_CHPROMOD
    endif
    if not(this.oPgFrm.Page2.oPag.oCHPR1MOD_2_57.value==this.w_CHPR1MOD)
      this.oPgFrm.Page2.oPag.oCHPR1MOD_2_57.value=this.w_CHPR1MOD
    endif
    if not(this.oPgFrm.Page2.oPag.oCHPR2MOD_2_59.value==this.w_CHPR2MOD)
      this.oPgFrm.Page2.oPag.oCHPR2MOD_2_59.value=this.w_CHPR2MOD
    endif
    if not(this.oPgFrm.Page2.oPag.oGDPROCER_2_61.value==this.w_GDPROCER)
      this.oPgFrm.Page2.oPag.oGDPROCER_2_61.value=this.w_GDPROCER
    endif
    if not(this.oPgFrm.Page1.oPag.oCHPROTEC_1_98.value==this.w_CHPROTEC)
      this.oPgFrm.Page1.oPag.oCHPROTEC_1_98.value=this.w_CHPROTEC
    endif
    if not(this.oPgFrm.Page1.oPag.oCHPRODOC_1_100.value==this.w_CHPRODOC)
      this.oPgFrm.Page1.oPag.oCHPRODOC_1_100.value=this.w_CHPRODOC
    endif
    if not(this.oPgFrm.Page1.oPag.oCHDATSTA_1_102.value==this.w_CHDATSTA)
      this.oPgFrm.Page1.oPag.oCHDATSTA_1_102.value=this.w_CHDATSTA
    endif
    if not(this.oPgFrm.Page2.oPag.oCHATT2MOD_2_78.RadioValue()==this.w_CHATT2MOD)
      this.oPgFrm.Page2.oPag.oCHATT2MOD_2_78.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCHCERDEF_1_110.RadioValue()==this.w_CHCERDEF)
      this.oPgFrm.Page1.oPag.oCHCERDEF_1_110.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oVAR_DATI_2_84.RadioValue()==this.w_VAR_DATI)
      this.oPgFrm.Page2.oPag.oVAR_DATI_2_84.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSTATO_1_112.RadioValue()==this.w_STATO)
      this.oPgFrm.Page1.oPag.oSTATO_1_112.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCHATT3MOD_2_88.RadioValue()==this.w_CHATT3MOD)
      this.oPgFrm.Page2.oPag.oCHATT3MOD_2_88.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCHT3T002_2_90.value==this.w_CHT3T002)
      this.oPgFrm.Page2.oPag.oCHT3T002_2_90.value=this.w_CHT3T002
    endif
    if not(this.oPgFrm.Page2.oPag.oCHT3T003_2_91.RadioValue()==this.w_CHT3T003)
      this.oPgFrm.Page2.oPag.oCHT3T003_2_91.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCHT3T005_2_92.value==this.w_CHT3T005)
      this.oPgFrm.Page2.oPag.oCHT3T005_2_92.value=this.w_CHT3T005
    endif
    if not(this.oPgFrm.Page2.oPag.oTOTM3006_2016_2_93.RadioValue()==this.w_TOTM3006_2016)
      this.oPgFrm.Page2.oPag.oTOTM3006_2016_2_93.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTOTM3006_2017_2_94.RadioValue()==this.w_TOTM3006_2017)
      this.oPgFrm.Page2.oPag.oTOTM3006_2017_2_94.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCHT3T018_2_97.value==this.w_CHT3T018)
      this.oPgFrm.Page2.oPag.oCHT3T018_2_97.value=this.w_CHT3T018
    endif
    if not(this.oPgFrm.Page2.oPag.oCHT3T019_2_98.value==this.w_CHT3T019)
      this.oPgFrm.Page2.oPag.oCHT3T019_2_98.value=this.w_CHT3T019
    endif
    if not(this.oPgFrm.Page2.oPag.oCHPR3MOD_2_100.value==this.w_CHPR3MOD)
      this.oPgFrm.Page2.oPag.oCHPR3MOD_2_100.value=this.w_CHPR3MOD
    endif
    cp_SetControlsValueExtFlds(this,'CUDTETRH')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_CHCODCON)) or not(.w_ANRITENU $ 'CS'))  and (.cFunction='Load')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCHCODCON_1_5.SetFocus()
            i_bnoObbl = !empty(.w_CHCODCON)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice fornitore inesistente o non soggetto a ritenute")
          case   (empty(.w_CAUPRE))  and not(.w_Ch__Anno>=2016)  and (.cFunction='Load' Or .w_Chtipdat='M' )
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAUPRE_1_32.SetFocus()
            i_bnoObbl = !empty(.w_CAUPRE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CAUPRE_2016))  and not(.w_Ch__Anno<>2016)  and (.cFunction='Load' Or .w_Chtipdat='M' )
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAUPRE_2016_1_33.SetFocus()
            i_bnoObbl = !empty(.w_CAUPRE_2016)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CAUPRE_2017))  and not(.w_Ch__Anno<2017)  and (.cFunction='Load' Or .w_Chtipdat='M' )
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAUPRE_2017_1_34.SetFocus()
            i_bnoObbl = !empty(.w_CAUPRE_2017)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CHDESCRI))  and (.cFunction<>'Query' And  .w_CHPERFIS<>'S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCHDESCRI_1_46.SetFocus()
            i_bnoObbl = !empty(.w_CHDESCRI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CHCOGNOM))  and (.cFunction<>'Query' AND .w_CHPERFIS='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCHCOGNOM_1_47.SetFocus()
            i_bnoObbl = !empty(.w_CHCOGNOM)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CH__NOME))  and (.cFunction<>'Query' AND .w_CHPERFIS='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCH__NOME_1_48.SetFocus()
            i_bnoObbl = !empty(.w_CH__NOME)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CH_SESSO))  and (.cFunction<>'Query' AND .w_CHPERFIS='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCH_SESSO_1_52.SetFocus()
            i_bnoObbl = !empty(.w_CH_SESSO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CHDATNAS))  and (.cFunction<>'Query' AND .w_CHPERFIS='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCHDATNAS_1_53.SetFocus()
            i_bnoObbl = !empty(.w_CHDATNAS)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CHCOMEST))  and (.cFunction<>'Query' AND .w_CHPERFIS='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCHCOMEST_1_56.SetFocus()
            i_bnoObbl = !empty(.w_CHCOMEST)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_EVEECC))  and not(.w_Ch__Anno>=2016)  and (.cFunction<>'Query' )
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oEVEECC_1_62.SetFocus()
            i_bnoObbl = !empty(.w_EVEECC)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_EVEECC_2016))  and not(.w_Ch__Anno<>2016)  and (.cFunction<>'Query' )
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oEVEECC_2016_1_63.SetFocus()
            i_bnoObbl = !empty(.w_EVEECC_2016)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_EVEECC_2017))  and not(.w_Ch__Anno<=2016)  and (.cFunction<>'Query' )
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oEVEECC_2017_1_64.SetFocus()
            i_bnoObbl = !empty(.w_EVEECC_2017)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CHSTAEST))  and (.cFunction<>'Query' And .w_CHPEREST='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCHSTAEST_1_80.SetFocus()
            i_bnoObbl = !empty(.w_CHSTAEST)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_Chtot004>=0)  and (.w_Var_Dati='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCHTOT004_2_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Non � possibile inserire importi negativi")
          case   not(.w_Chtot005>=0)  and (.w_Var_Dati='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCHTOT005_2_16.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Non � possibile inserire importi negativi")
          case   not(.w_Chtot008>=0)  and (.w_Var_Dati='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCHTOT008_2_22.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Non � possibile inserire importi negativi")
          case   not(.w_Chtot009>=0)  and (.w_Var_Dati='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCHTOT009_2_23.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Non � possibile inserire importi negativi")
          case   not(.w_Chtot018>=0)  and (.w_Var_Dati='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCHTOT018_2_24.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Non � possibile inserire importi negativi")
          case   not(.w_Chtot019>=0)  and (.w_Var_Dati='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCHTOT019_2_25.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Non � possibile inserire importi negativi")
          case   not(.w_Chtot020>=0)  and (.w_Var_Dati='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCHTOT020_2_26.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Non � possibile inserire importi negativi")
          case   not(.w_Chtot021>=0)  and (Alltrim(.w_Chcaupre) $ 'X-Y' And .w_Var_Dati='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCHTOT021_2_27.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Non � possibile inserire importi negativi")
          case   not(.w_Chtot041>=0)  and (.w_Var_Dati='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCHTOT041_2_28.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Non � possibile inserire importi negativi")
          case   not(.w_Chtot042>=0)  and (.w_Var_Dati='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCHTOT042_2_29.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Non � possibile inserire importi negativi")
          case   not(.w_Cht1t005>=0)  and (.w_CHATT1MOD='S' And .w_Var_Dati='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCHT1T005_2_33.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Non � possibile inserire importi negativi")
          case   not(.w_Cht1t018>=0)  and (.w_CHATT1MOD='S' And .w_Var_Dati='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCHT1T018_2_39.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Non � possibile inserire importi negativi")
          case   not(.w_Cht1t019>=0)  and (.w_CHATT1MOD='S' And .w_Var_Dati='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCHT1T019_2_40.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Non � possibile inserire importi negativi")
          case   not(.w_Cht2t005>=0)  and (.w_CHATT2MOD='S' And .w_Var_Dati='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCHT2T005_2_43.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Non � possibile inserire importi negativi ")
          case   not(.w_Cht2t018>=0)  and (.w_CHATT2MOD='S' And .w_Var_Dati='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCHT2T018_2_49.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_Cht2t019>=0)  and (.w_CHATT2MOD='S' And .w_Var_Dati='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCHT2T019_2_50.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Non � possibile inserire importi negativi")
          case   not(.w_Cht3t005>=0)  and (.w_CHATT3MOD='S' And .w_Var_Dati='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCHT3T005_2_92.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Non � possibile inserire importi negativi ")
          case   not(.w_Cht3t018>=0)  and (.w_CHATT3MOD='S' And .w_Var_Dati='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCHT3T018_2_97.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_Cht3t019>=0)  and (.w_CHATT3MOD='S' And .w_Var_Dati='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCHT3T019_2_98.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Non � possibile inserire importi negativi")
        endcase
      endif
      *i_bRes = i_bRes .and. .GSRI_MCH.CheckForm()
      if i_bres
        i_bres=  .GSRI_MCH.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=3
        endif
      endif
      *i_bRes = i_bRes .and. .GSRI_MPR.CheckForm()
      if i_bres
        i_bres=  .GSRI_MPR.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=2
        endif
      endif
      * --- Area Manuale = Check Form
      * --- gsri_ach
      *--- Controllo obbligatoriet� data fine periodo di riferimento
      if This.w_Chcaupre='N' and This.w_Chperest<>'S' and (Empty(This.w_Chcomune) Or Empty(This.w_Chprovin) Or Empty(This.w_Chcodcom))
         ah_errormsg("Comune provincia e codice comune obbligatori%0se la causale prestazione vale N",16)
         i_bRes=.f.
      Endif
      if i_bRes=.t. And This.w_Chatt1Mod='N' And This.w_Chatt2Mod='S'
         ah_errormsg("Non � possibile attivare il modulo 3 in assenza del modulo 2",16)
         i_bRes=.f.
      Endif
      if i_bRes=.t. And This.w_Var_Dati='S' And This.w_Chcerdef<>'S'
          if Ah_YesNo('Si vuole rendere definitiva la certificazione?')
            This.w_Chcerdef='S'
          Endif
      Endif
      if i_bRes=.t. And Empty(This.w_Chcodfis) And This.w_Chperest<>'S' 
         ah_errormsg("Codice fiscale obbligatorio",16)
         i_bRes=.f.
      Endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CH__ANNO = this.w_CH__ANNO
    this.o_CHCODCON = this.w_CHCODCON
    this.o_ANFLSGRE = this.w_ANFLSGRE
    this.o_CHPERFIS = this.w_CHPERFIS
    this.o_CHPEREST = this.w_CHPEREST
    this.o_CAUPRE = this.w_CAUPRE
    this.o_CAUPRE_2016 = this.w_CAUPRE_2016
    this.o_CAUPRE_2017 = this.w_CAUPRE_2017
    this.o_CHCAUPRE = this.w_CHCAUPRE
    this.o_CHTIPOPE = this.w_CHTIPOPE
    this.o_EVEECC = this.w_EVEECC
    this.o_EVEECC_2016 = this.w_EVEECC_2016
    this.o_EVEECC_2017 = this.w_EVEECC_2017
    this.o_CATPAR = this.w_CATPAR
    this.o_CATPAR_2016 = this.w_CATPAR_2016
    this.o_CHTOT003 = this.w_CHTOT003
    this.o_CHTOT004 = this.w_CHTOT004
    this.o_CHTOT005 = this.w_CHTOT005
    this.o_TOT006 = this.w_TOT006
    this.o_TOT006_2016 = this.w_TOT006_2016
    this.o_TOT006_2017 = this.w_TOT006_2017
    this.o_CHTOT008 = this.w_CHTOT008
    this.o_CHTOT009 = this.w_CHTOT009
    this.o_CHTOT018 = this.w_CHTOT018
    this.o_CHTOT019 = this.w_CHTOT019
    this.o_CHTOT020 = this.w_CHTOT020
    this.o_CHTOT021 = this.w_CHTOT021
    this.o_CHTOT041 = this.w_CHTOT041
    this.o_CHTOT042 = this.w_CHTOT042
    this.o_CHT1T003 = this.w_CHT1T003
    this.o_CHT1T005 = this.w_CHT1T005
    this.o_TOTM1006 = this.w_TOTM1006
    this.o_TOTM1006_2016 = this.w_TOTM1006_2016
    this.o_TOTM1006_2017 = this.w_TOTM1006_2017
    this.o_CHT1T018 = this.w_CHT1T018
    this.o_CHT1T019 = this.w_CHT1T019
    this.o_CHT2T003 = this.w_CHT2T003
    this.o_CHT2T005 = this.w_CHT2T005
    this.o_TOTM2006_2016 = this.w_TOTM2006_2016
    this.o_TOTM2006 = this.w_TOTM2006
    this.o_TOTM2006_2017 = this.w_TOTM2006_2017
    this.o_CHT2T018 = this.w_CHT2T018
    this.o_CHT2T019 = this.w_CHT2T019
    this.o_CHATT1MOD = this.w_CHATT1MOD
    this.o_CHATT2MOD = this.w_CHATT2MOD
    this.o_CHATT3MOD = this.w_CHATT3MOD
    this.o_CHT3T003 = this.w_CHT3T003
    this.o_CHT3T005 = this.w_CHT3T005
    this.o_TOTM3006_2016 = this.w_TOTM3006_2016
    this.o_TOTM3006_2017 = this.w_TOTM3006_2017
    this.o_CHT3T018 = this.w_CHT3T018
    this.o_CHT3T019 = this.w_CHT3T019
    * --- GSRI_MCH : Depends On
    this.GSRI_MCH.SaveDependsOn()
    * --- GSRI_MPR : Depends On
    this.GSRI_MPR.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgsri_achPag1 as StdContainer
  Width  = 861
  height = 653
  stdWidth  = 861
  stdheight = 653
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCH__ANNO_1_3 as StdField with uid="HYNPMMDCOL",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CH__ANNO", cQueryName = "CH__ANNO",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Anno di riferimento",;
    HelpContextID = 199968373,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=99, Top=14, cSayPict='"9999"', cGetPict='"9999"'

  func oCH__ANNO_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  add object oCHCODCON_1_5 as StdField with uid="EAHXPIIFWI",rtseq=5,rtrep=.f.,;
    cFormVar = "w_CHCODCON", cQueryName = "CHCODCON",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice fornitore inesistente o non soggetto a ritenute",;
    ToolTipText = "Codice conto",;
    HelpContextID = 17401460,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=325, Top=14, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_AFR", oKey_1_1="ANTIPCON", oKey_1_2="this.w_CHTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CHCODCON"

  func oCHCODCON_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  func oCHCODCON_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oCHCODCON_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCHCODCON_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_CHTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_CHTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCHCODCON_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFR',"Elenco fornitori",'GSRI_ADH.CONTI_VZM',this.parent.oContained
  endproc
  proc oCHCODCON_1_5.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_CHTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CHCODCON
     i_obj.ecpSave()
  endproc

  add object oCHPERFIS_1_30 as StdCheck with uid="PJITHOZBWZ",rtseq=30,rtrep=.f.,left=490, top=12, caption="Persona fisica", enabled=.f.,;
    ToolTipText = "Se attivo: il fornitore � una persona fisica",;
    HelpContextID = 186624391,;
    cFormVar="w_CHPERFIS", bObbl = .f. , nPag = 1;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oCHPERFIS_1_30.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCHPERFIS_1_30.GetRadio()
    this.Parent.oContained.w_CHPERFIS = this.RadioValue()
    return .t.
  endfunc

  func oCHPERFIS_1_30.SetRadio()
    this.Parent.oContained.w_CHPERFIS=trim(this.Parent.oContained.w_CHPERFIS)
    this.value = ;
      iif(this.Parent.oContained.w_CHPERFIS=='S',1,;
      0)
  endfunc

  add object oCHPEREST_1_31 as StdCheck with uid="HGJTZNOYNK",rtseq=31,rtrep=.f.,left=671, top=12, caption="Percipiente estero", enabled=.f.,;
    ToolTipText = "Percipiente estero",;
    HelpContextID = 65033850,;
    cFormVar="w_CHPEREST", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCHPEREST_1_31.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCHPEREST_1_31.GetRadio()
    this.Parent.oContained.w_CHPEREST = this.RadioValue()
    return .t.
  endfunc

  func oCHPEREST_1_31.SetRadio()
    this.Parent.oContained.w_CHPEREST=trim(this.Parent.oContained.w_CHPEREST)
    this.value = ;
      iif(this.Parent.oContained.w_CHPEREST=='S',1,;
      0)
  endfunc


  add object oCAUPRE_1_32 as StdCombo with uid="CWTVWQSDOO",rtseq=32,rtrep=.f.,left=328,top=55,width=90,height=22;
    , ToolTipText = "Valore predefinito causale prestazione";
    , HelpContextID = 65773350;
    , cFormVar="w_CAUPRE",RowSource=""+"Tipo A,"+"Tipo B,"+"Tipo C,"+"Tipo D,"+"Tipo E,"+"Tipo G,"+"Tipo H,"+"Tipo I,"+"Tipo L,"+"Tipo L1,"+"Tipo M,"+"Tipo M1,"+"Tipo M2,"+"Tipo N,"+"Tipo O,"+"Tipo O1,"+"Tipo P,"+"Tipo Q,"+"Tipo R,"+"Tipo S,"+"Tipo T,"+"Tipo U,"+"Tipo V,"+"Tipo V1,"+"Tipo V2,"+"Tipo W,"+"Tipo X,"+"Tipo Y,"+"Tipo Z", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oCAUPRE_1_32.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'B',;
    iif(this.value =3,'C',;
    iif(this.value =4,'D',;
    iif(this.value =5,'E',;
    iif(this.value =6,'G',;
    iif(this.value =7,'H',;
    iif(this.value =8,'I',;
    iif(this.value =9,'L',;
    iif(this.value =10,'L1',;
    iif(this.value =11,'M',;
    iif(this.value =12,'M1',;
    iif(this.value =13,'M2',;
    iif(this.value =14,'N',;
    iif(this.value =15,'O',;
    iif(this.value =16,'O1',;
    iif(this.value =17,'P',;
    iif(this.value =18,'Q',;
    iif(this.value =19,'R',;
    iif(this.value =20,'S',;
    iif(this.value =21,'T',;
    iif(this.value =22,'U',;
    iif(this.value =23,'V',;
    iif(this.value =24,'V1',;
    iif(this.value =25,'V2',;
    iif(this.value =26,'W',;
    iif(this.value =27,'X',;
    iif(this.value =28,'Y',;
    iif(this.value =29,'Z',;
    space(2)))))))))))))))))))))))))))))))
  endfunc
  func oCAUPRE_1_32.GetRadio()
    this.Parent.oContained.w_CAUPRE = this.RadioValue()
    return .t.
  endfunc

  func oCAUPRE_1_32.SetRadio()
    this.Parent.oContained.w_CAUPRE=trim(this.Parent.oContained.w_CAUPRE)
    this.value = ;
      iif(this.Parent.oContained.w_CAUPRE=='A',1,;
      iif(this.Parent.oContained.w_CAUPRE=='B',2,;
      iif(this.Parent.oContained.w_CAUPRE=='C',3,;
      iif(this.Parent.oContained.w_CAUPRE=='D',4,;
      iif(this.Parent.oContained.w_CAUPRE=='E',5,;
      iif(this.Parent.oContained.w_CAUPRE=='G',6,;
      iif(this.Parent.oContained.w_CAUPRE=='H',7,;
      iif(this.Parent.oContained.w_CAUPRE=='I',8,;
      iif(this.Parent.oContained.w_CAUPRE=='L',9,;
      iif(this.Parent.oContained.w_CAUPRE=='L1',10,;
      iif(this.Parent.oContained.w_CAUPRE=='M',11,;
      iif(this.Parent.oContained.w_CAUPRE=='M1',12,;
      iif(this.Parent.oContained.w_CAUPRE=='M2',13,;
      iif(this.Parent.oContained.w_CAUPRE=='N',14,;
      iif(this.Parent.oContained.w_CAUPRE=='O',15,;
      iif(this.Parent.oContained.w_CAUPRE=='O1',16,;
      iif(this.Parent.oContained.w_CAUPRE=='P',17,;
      iif(this.Parent.oContained.w_CAUPRE=='Q',18,;
      iif(this.Parent.oContained.w_CAUPRE=='R',19,;
      iif(this.Parent.oContained.w_CAUPRE=='S',20,;
      iif(this.Parent.oContained.w_CAUPRE=='T',21,;
      iif(this.Parent.oContained.w_CAUPRE=='U',22,;
      iif(this.Parent.oContained.w_CAUPRE=='V',23,;
      iif(this.Parent.oContained.w_CAUPRE=='V1',24,;
      iif(this.Parent.oContained.w_CAUPRE=='V2',25,;
      iif(this.Parent.oContained.w_CAUPRE=='W',26,;
      iif(this.Parent.oContained.w_CAUPRE=='X',27,;
      iif(this.Parent.oContained.w_CAUPRE=='Y',28,;
      iif(this.Parent.oContained.w_CAUPRE=='Z',29,;
      0)))))))))))))))))))))))))))))
  endfunc

  func oCAUPRE_1_32.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load' Or .w_Chtipdat='M' )
    endwith
   endif
  endfunc

  func oCAUPRE_1_32.mHide()
    with this.Parent.oContained
      return (.w_Ch__Anno>=2016)
    endwith
  endfunc


  add object oCAUPRE_2016_1_33 as StdCombo with uid="VNVMOGRAFD",rtseq=33,rtrep=.f.,left=328,top=55,width=90,height=22;
    , ToolTipText = "Valore predefinito causale prestazione";
    , HelpContextID = 66007896;
    , cFormVar="w_CAUPRE_2016",RowSource=""+"Tipo A,"+"Tipo B,"+"Tipo C,"+"Tipo D,"+"Tipo E,"+"Tipo G,"+"Tipo H,"+"Tipo I,"+"Tipo L,"+"Tipo L1,"+"Tipo M,"+"Tipo M1,"+"Tipo M2,"+"Tipo N,"+"Tipo O,"+"Tipo O1,"+"Tipo P,"+"Tipo Q,"+"Tipo R,"+"Tipo S,"+"Tipo T,"+"Tipo U,"+"Tipo V,"+"Tipo V1,"+"Tipo V2,"+"Tipo W,"+"Tipo X,"+"Tipo Y,"+"Tipo ZO", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oCAUPRE_2016_1_33.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'B',;
    iif(this.value =3,'C',;
    iif(this.value =4,'D',;
    iif(this.value =5,'E',;
    iif(this.value =6,'G',;
    iif(this.value =7,'H',;
    iif(this.value =8,'I',;
    iif(this.value =9,'L',;
    iif(this.value =10,'L1',;
    iif(this.value =11,'M',;
    iif(this.value =12,'M1',;
    iif(this.value =13,'M2',;
    iif(this.value =14,'N',;
    iif(this.value =15,'O',;
    iif(this.value =16,'O1',;
    iif(this.value =17,'P',;
    iif(this.value =18,'Q',;
    iif(this.value =19,'R',;
    iif(this.value =20,'S',;
    iif(this.value =21,'T',;
    iif(this.value =22,'U',;
    iif(this.value =23,'V',;
    iif(this.value =24,'V1',;
    iif(this.value =25,'V2',;
    iif(this.value =26,'W',;
    iif(this.value =27,'X',;
    iif(this.value =28,'Y',;
    iif(this.value =29,'ZO',;
    space(2)))))))))))))))))))))))))))))))
  endfunc
  func oCAUPRE_2016_1_33.GetRadio()
    this.Parent.oContained.w_CAUPRE_2016 = this.RadioValue()
    return .t.
  endfunc

  func oCAUPRE_2016_1_33.SetRadio()
    this.Parent.oContained.w_CAUPRE_2016=trim(this.Parent.oContained.w_CAUPRE_2016)
    this.value = ;
      iif(this.Parent.oContained.w_CAUPRE_2016=='A',1,;
      iif(this.Parent.oContained.w_CAUPRE_2016=='B',2,;
      iif(this.Parent.oContained.w_CAUPRE_2016=='C',3,;
      iif(this.Parent.oContained.w_CAUPRE_2016=='D',4,;
      iif(this.Parent.oContained.w_CAUPRE_2016=='E',5,;
      iif(this.Parent.oContained.w_CAUPRE_2016=='G',6,;
      iif(this.Parent.oContained.w_CAUPRE_2016=='H',7,;
      iif(this.Parent.oContained.w_CAUPRE_2016=='I',8,;
      iif(this.Parent.oContained.w_CAUPRE_2016=='L',9,;
      iif(this.Parent.oContained.w_CAUPRE_2016=='L1',10,;
      iif(this.Parent.oContained.w_CAUPRE_2016=='M',11,;
      iif(this.Parent.oContained.w_CAUPRE_2016=='M1',12,;
      iif(this.Parent.oContained.w_CAUPRE_2016=='M2',13,;
      iif(this.Parent.oContained.w_CAUPRE_2016=='N',14,;
      iif(this.Parent.oContained.w_CAUPRE_2016=='O',15,;
      iif(this.Parent.oContained.w_CAUPRE_2016=='O1',16,;
      iif(this.Parent.oContained.w_CAUPRE_2016=='P',17,;
      iif(this.Parent.oContained.w_CAUPRE_2016=='Q',18,;
      iif(this.Parent.oContained.w_CAUPRE_2016=='R',19,;
      iif(this.Parent.oContained.w_CAUPRE_2016=='S',20,;
      iif(this.Parent.oContained.w_CAUPRE_2016=='T',21,;
      iif(this.Parent.oContained.w_CAUPRE_2016=='U',22,;
      iif(this.Parent.oContained.w_CAUPRE_2016=='V',23,;
      iif(this.Parent.oContained.w_CAUPRE_2016=='V1',24,;
      iif(this.Parent.oContained.w_CAUPRE_2016=='V2',25,;
      iif(this.Parent.oContained.w_CAUPRE_2016=='W',26,;
      iif(this.Parent.oContained.w_CAUPRE_2016=='X',27,;
      iif(this.Parent.oContained.w_CAUPRE_2016=='Y',28,;
      iif(this.Parent.oContained.w_CAUPRE_2016=='ZO',29,;
      0)))))))))))))))))))))))))))))
  endfunc

  func oCAUPRE_2016_1_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load' Or .w_Chtipdat='M' )
    endwith
   endif
  endfunc

  func oCAUPRE_2016_1_33.mHide()
    with this.Parent.oContained
      return (.w_Ch__Anno<>2016)
    endwith
  endfunc


  add object oCAUPRE_2017_1_34 as StdCombo with uid="KSBFGRPYFO",rtseq=34,rtrep=.f.,left=328,top=55,width=90,height=22;
    , ToolTipText = "Valore predefinito causale prestazione";
    , HelpContextID = 66011992;
    , cFormVar="w_CAUPRE_2017",RowSource=""+"Tipo A,"+"Tipo B,"+"Tipo C,"+"Tipo D,"+"Tipo E,"+"Tipo F,"+"Tipo G,"+"Tipo H,"+"Tipo I,"+"Tipo J,"+"Tipo K,"+"Tipo L,"+"Tipo L1,"+"Tipo M,"+"Tipo M1,"+"Tipo M2,"+"Tipo N,"+"Tipo O,"+"Tipo O1,"+"Tipo P,"+"Tipo Q,"+"Tipo R,"+"Tipo S,"+"Tipo T,"+"Tipo U,"+"Tipo V,"+"Tipo V1,"+"Tipo V2,"+"Tipo W,"+"Tipo X,"+"Tipo Y,"+"Tipo ZO", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oCAUPRE_2017_1_34.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'B',;
    iif(this.value =3,'C',;
    iif(this.value =4,'D',;
    iif(this.value =5,'E',;
    iif(this.value =6,'F',;
    iif(this.value =7,'G',;
    iif(this.value =8,'H',;
    iif(this.value =9,'I',;
    iif(this.value =10,'J',;
    iif(this.value =11,'K',;
    iif(this.value =12,'L',;
    iif(this.value =13,'L1',;
    iif(this.value =14,'M',;
    iif(this.value =15,'M1',;
    iif(this.value =16,'M2',;
    iif(this.value =17,'N',;
    iif(this.value =18,'O',;
    iif(this.value =19,'O1',;
    iif(this.value =20,'P',;
    iif(this.value =21,'Q',;
    iif(this.value =22,'R',;
    iif(this.value =23,'S',;
    iif(this.value =24,'T',;
    iif(this.value =25,'U',;
    iif(this.value =26,'V',;
    iif(this.value =27,'V1',;
    iif(this.value =28,'V2',;
    iif(this.value =29,'W',;
    iif(this.value =30,'X',;
    iif(this.value =31,'Y',;
    iif(this.value =32,'ZO',;
    space(2))))))))))))))))))))))))))))))))))
  endfunc
  func oCAUPRE_2017_1_34.GetRadio()
    this.Parent.oContained.w_CAUPRE_2017 = this.RadioValue()
    return .t.
  endfunc

  func oCAUPRE_2017_1_34.SetRadio()
    this.Parent.oContained.w_CAUPRE_2017=trim(this.Parent.oContained.w_CAUPRE_2017)
    this.value = ;
      iif(this.Parent.oContained.w_CAUPRE_2017=='A',1,;
      iif(this.Parent.oContained.w_CAUPRE_2017=='B',2,;
      iif(this.Parent.oContained.w_CAUPRE_2017=='C',3,;
      iif(this.Parent.oContained.w_CAUPRE_2017=='D',4,;
      iif(this.Parent.oContained.w_CAUPRE_2017=='E',5,;
      iif(this.Parent.oContained.w_CAUPRE_2017=='F',6,;
      iif(this.Parent.oContained.w_CAUPRE_2017=='G',7,;
      iif(this.Parent.oContained.w_CAUPRE_2017=='H',8,;
      iif(this.Parent.oContained.w_CAUPRE_2017=='I',9,;
      iif(this.Parent.oContained.w_CAUPRE_2017=='J',10,;
      iif(this.Parent.oContained.w_CAUPRE_2017=='K',11,;
      iif(this.Parent.oContained.w_CAUPRE_2017=='L',12,;
      iif(this.Parent.oContained.w_CAUPRE_2017=='L1',13,;
      iif(this.Parent.oContained.w_CAUPRE_2017=='M',14,;
      iif(this.Parent.oContained.w_CAUPRE_2017=='M1',15,;
      iif(this.Parent.oContained.w_CAUPRE_2017=='M2',16,;
      iif(this.Parent.oContained.w_CAUPRE_2017=='N',17,;
      iif(this.Parent.oContained.w_CAUPRE_2017=='O',18,;
      iif(this.Parent.oContained.w_CAUPRE_2017=='O1',19,;
      iif(this.Parent.oContained.w_CAUPRE_2017=='P',20,;
      iif(this.Parent.oContained.w_CAUPRE_2017=='Q',21,;
      iif(this.Parent.oContained.w_CAUPRE_2017=='R',22,;
      iif(this.Parent.oContained.w_CAUPRE_2017=='S',23,;
      iif(this.Parent.oContained.w_CAUPRE_2017=='T',24,;
      iif(this.Parent.oContained.w_CAUPRE_2017=='U',25,;
      iif(this.Parent.oContained.w_CAUPRE_2017=='V',26,;
      iif(this.Parent.oContained.w_CAUPRE_2017=='V1',27,;
      iif(this.Parent.oContained.w_CAUPRE_2017=='V2',28,;
      iif(this.Parent.oContained.w_CAUPRE_2017=='W',29,;
      iif(this.Parent.oContained.w_CAUPRE_2017=='X',30,;
      iif(this.Parent.oContained.w_CAUPRE_2017=='Y',31,;
      iif(this.Parent.oContained.w_CAUPRE_2017=='ZO',32,;
      0))))))))))))))))))))))))))))))))
  endfunc

  func oCAUPRE_2017_1_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load' Or .w_Chtipdat='M' )
    endwith
   endif
  endfunc

  func oCAUPRE_2017_1_34.mHide()
    with this.Parent.oContained
      return (.w_Ch__Anno<2017)
    endwith
  endfunc


  add object oCHTIPOPE_1_37 as StdCombo with uid="AVPSHLDLXQ",rtseq=36,rtrep=.f.,left=514,top=55,width=131,height=21;
    , ToolTipText = "Indica il tipo di operazione";
    , HelpContextID = 230987371;
    , cFormVar="w_CHTIPOPE",RowSource=""+"Ordinario,"+"Sostitutiva,"+"Annullamento", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCHTIPOPE_1_37.RadioValue()
    return(iif(this.value =1,'O',;
    iif(this.value =2,'S',;
    iif(this.value =3,'A',;
    space(1)))))
  endfunc
  func oCHTIPOPE_1_37.GetRadio()
    this.Parent.oContained.w_CHTIPOPE = this.RadioValue()
    return .t.
  endfunc

  func oCHTIPOPE_1_37.SetRadio()
    this.Parent.oContained.w_CHTIPOPE=trim(this.Parent.oContained.w_CHTIPOPE)
    this.value = ;
      iif(this.Parent.oContained.w_CHTIPOPE=='O',1,;
      iif(this.Parent.oContained.w_CHTIPOPE=='S',2,;
      iif(this.Parent.oContained.w_CHTIPOPE=='A',3,;
      0)))
  endfunc

  func oCHTIPOPE_1_37.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  add object oCHSCLGEN_1_38 as StdCheck with uid="FZBDCCDEUF",rtseq=37,rtrep=.f.,left=671, top=87, caption="Escludi da generazione",;
    ToolTipText = "Se attivo, la certificazione sar� esclusa dalla generazione file telematico",;
    HelpContextID = 176257420,;
    cFormVar="w_CHSCLGEN", bObbl = .f. , nPag = 1;
    , tabstop=.f.;
   , bGlobalFont=.t.


  func oCHSCLGEN_1_38.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCHSCLGEN_1_38.GetRadio()
    this.Parent.oContained.w_CHSCLGEN = this.RadioValue()
    return .t.
  endfunc

  func oCHSCLGEN_1_38.SetRadio()
    this.Parent.oContained.w_CHSCLGEN=trim(this.Parent.oContained.w_CHSCLGEN)
    this.value = ;
      iif(this.Parent.oContained.w_CHSCLGEN=='S',1,;
      0)
  endfunc

  add object oCHCODFIS_1_39 as StdField with uid="BJWDILJOQP",rtseq=38,rtrep=.f.,;
    cFormVar = "w_CHCODFIS", cQueryName = "CHCODFIS",;
    bObbl = .f. , nPag = 1, value=space(16), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale",;
    HelpContextID = 200702343,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=8, Top=210, cSayPict='REPLICATE("!",16)', cGetPict='REPLICATE("!",16)', InputMask=replicate('X',16)

  func oCHCODFIS_1_39.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query')
    endwith
   endif
  endfunc

  add object oCHDESCRI_1_46 as StdField with uid="EPFAFIXABF",rtseq=39,rtrep=.f.,;
    cFormVar = "w_CHDESCRI", cQueryName = "CHDESCRI",;
    bObbl = .t. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Denominazione,ditta o ragione sociale",;
    HelpContextID = 32478831,;
   bGlobalFont=.t.,;
    Height=21, Width=433, Left=8, Top=261, cSayPict="REPL('!',60)", cGetPict="REPL('!',60)", InputMask=replicate('X',60)

  func oCHDESCRI_1_46.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' And  .w_CHPERFIS<>'S')
    endwith
   endif
  endfunc

  add object oCHCOGNOM_1_47 as StdField with uid="VBWHBYPETD",rtseq=40,rtrep=.f.,;
    cFormVar = "w_CHCOGNOM", cQueryName = "CHCOGNOM",;
    bObbl = .t. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Cognome",;
    HelpContextID = 205096563,;
   bGlobalFont=.t.,;
    Height=21, Width=321, Left=8, Top=312, cSayPict="REPL('!',50)", cGetPict="REPL('!',50)", InputMask=replicate('X',50)

  func oCHCOGNOM_1_47.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' AND .w_CHPERFIS='S')
    endwith
   endif
  endfunc

  add object oCH__NOME_1_48 as StdField with uid="TZTOWOZVVJ",rtseq=41,rtrep=.f.,;
    cFormVar = "w_CH__NOME", cQueryName = "CH__NOME",;
    bObbl = .t. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Nome",;
    HelpContextID = 230377067,;
   bGlobalFont=.t.,;
    Height=21, Width=321, Left=345, Top=312, cSayPict="REPL('!',50)", cGetPict="REPL('!',50)", InputMask=replicate('X',50)

  func oCH__NOME_1_48.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' AND .w_CHPERFIS='S')
    endwith
   endif
  endfunc


  add object oCH_SESSO_1_52 as StdCombo with uid="UFMSDHWBVZ",rtseq=42,rtrep=.f.,left=719,top=312,width=91,height=21;
    , HelpContextID = 18826869;
    , cFormVar="w_CH_SESSO",RowSource=""+"Maschio,"+"Femmina", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oCH_SESSO_1_52.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oCH_SESSO_1_52.GetRadio()
    this.Parent.oContained.w_CH_SESSO = this.RadioValue()
    return .t.
  endfunc

  func oCH_SESSO_1_52.SetRadio()
    this.Parent.oContained.w_CH_SESSO=trim(this.Parent.oContained.w_CH_SESSO)
    this.value = ;
      iif(this.Parent.oContained.w_CH_SESSO=='M',1,;
      iif(this.Parent.oContained.w_CH_SESSO=='F',2,;
      0))
  endfunc

  func oCH_SESSO_1_52.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' AND .w_CHPERFIS='S')
    endwith
   endif
  endfunc

  add object oCHDATNAS_1_53 as StdField with uid="VTKSSEXLQD",rtseq=43,rtrep=.f.,;
    cFormVar = "w_CHDATNAS", cQueryName = "CHDATNAS",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di nascita",;
    HelpContextID = 217814649,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=8, Top=366

  func oCHDATNAS_1_53.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' AND .w_CHPERFIS='S')
    endwith
   endif
  endfunc

  add object oCHCOMEST_1_56 as StdField with uid="OEGUAGVHVP",rtseq=44,rtrep=.f.,;
    cFormVar = "w_CHCOMEST", cQueryName = "CHCOMEST",;
    bObbl = .t. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Comune (o stato estero) di nascita",;
    HelpContextID = 60393082,;
   bGlobalFont=.t.,;
    Height=21, Width=321, Left=345, Top=366, cSayPict="REPL('!',30)", cGetPict="REPL('!',30)", InputMask=replicate('X',30), bHasZoom = .t. 

  func oCHCOMEST_1_56.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' AND .w_CHPERFIS='S')
    endwith
   endif
  endfunc

  proc oCHCOMEST_1_56.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C","",".w_CHCOMEST",".w_CHPRONAS")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oCHPRONAS_1_58 as StdField with uid="HCPUHURXUB",rtseq=45,rtrep=.f.,;
    cFormVar = "w_CHPRONAS", cQueryName = "CHPRONAS",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Provincia di nascita",;
    HelpContextID = 213735033,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=719, Top=366, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2), bHasZoom = .t. 

  func oCHPRONAS_1_58.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' AND .w_CHPERFIS='S')
    endwith
   endif
  endfunc

  proc oCHPRONAS_1_58.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"P","","",".w_CHPRONAS")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc


  add object oEVEECC_1_62 as StdCombo with uid="TZQYETJOUI",rtseq=46,rtrep=.f.,left=214,top=420,width=548,height=21;
    , ToolTipText = "Eventi eccezionali";
    , HelpContextID = 15709254;
    , cFormVar="w_EVEECC",RowSource=""+"0 - Nessuno,"+"1 - Contribuenti vittime di richieste estorsive ex. Art.20 Co.2 legge n. 44/99,"+"3 - Contribuenti residenti al 12/2/2011 nel comune di Lampedusa e Linosa (migranti Nord Africa),"+"4 - Contribuenti colpiti da altri eventi eccezionali", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oEVEECC_1_62.RadioValue()
    return(iif(this.value =1,'0',;
    iif(this.value =2,'1',;
    iif(this.value =3,'3',;
    iif(this.value =4,'4',;
    space(1))))))
  endfunc
  func oEVEECC_1_62.GetRadio()
    this.Parent.oContained.w_EVEECC = this.RadioValue()
    return .t.
  endfunc

  func oEVEECC_1_62.SetRadio()
    this.Parent.oContained.w_EVEECC=trim(this.Parent.oContained.w_EVEECC)
    this.value = ;
      iif(this.Parent.oContained.w_EVEECC=='0',1,;
      iif(this.Parent.oContained.w_EVEECC=='1',2,;
      iif(this.Parent.oContained.w_EVEECC=='3',3,;
      iif(this.Parent.oContained.w_EVEECC=='4',4,;
      0))))
  endfunc

  func oEVEECC_1_62.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' )
    endwith
   endif
  endfunc

  func oEVEECC_1_62.mHide()
    with this.Parent.oContained
      return (.w_Ch__Anno>=2016)
    endwith
  endfunc


  add object oEVEECC_2016_1_63 as StdCombo with uid="WJJWKVJLDV",rtseq=47,rtrep=.f.,left=214,top=420,width=548,height=21;
    , ToolTipText = "Eventi eccezionali";
    , HelpContextID = 15943800;
    , cFormVar="w_EVEECC_2016",RowSource=""+"0 - Nessuno,"+"1 - Contribuenti vittime di richieste estorsive ex. Art.20 Co.2 legge n. 44/99,"+"2 - Soggetti colpiti dagli eventi sismici del 24/08/2016 (Abruzzo Lazio Marche Umbria),"+"3 - Contribuenti residenti al 12/2/2011 nel comune di Lampedusa e Linosa (migranti Nord Africa),"+"4 - Soggetti colpiti dagli eventi sismici dell'ottobre 2016 (Abruzzo Lazio Marche Umbria),"+"6 - Contribuenti colpiti da altri eventi eccezionali (Certificazione unica e 770)", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oEVEECC_2016_1_63.RadioValue()
    return(iif(this.value =1,'0',;
    iif(this.value =2,'1',;
    iif(this.value =3,'2',;
    iif(this.value =4,'3',;
    iif(this.value =5,'4',;
    iif(this.value =6,'6',;
    space(1))))))))
  endfunc
  func oEVEECC_2016_1_63.GetRadio()
    this.Parent.oContained.w_EVEECC_2016 = this.RadioValue()
    return .t.
  endfunc

  func oEVEECC_2016_1_63.SetRadio()
    this.Parent.oContained.w_EVEECC_2016=trim(this.Parent.oContained.w_EVEECC_2016)
    this.value = ;
      iif(this.Parent.oContained.w_EVEECC_2016=='0',1,;
      iif(this.Parent.oContained.w_EVEECC_2016=='1',2,;
      iif(this.Parent.oContained.w_EVEECC_2016=='2',3,;
      iif(this.Parent.oContained.w_EVEECC_2016=='3',4,;
      iif(this.Parent.oContained.w_EVEECC_2016=='4',5,;
      iif(this.Parent.oContained.w_EVEECC_2016=='6',6,;
      0))))))
  endfunc

  func oEVEECC_2016_1_63.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' )
    endwith
   endif
  endfunc

  func oEVEECC_2016_1_63.mHide()
    with this.Parent.oContained
      return (.w_Ch__Anno<>2016)
    endwith
  endfunc


  add object oEVEECC_2017_1_64 as StdCombo with uid="GPQREOUZEG",rtseq=48,rtrep=.f.,left=214,top=420,width=548,height=21;
    , ToolTipText = "Eventi eccezionali";
    , HelpContextID = 15947896;
    , cFormVar="w_EVEECC_2017",RowSource=""+"0 - Nessuno,"+"1 - Contribuenti vittime di richieste estorsive ex. Art.20 Co.2 legge n. 44/99,"+"2 - Soggetti colpiti dagli eventi sismici del 24/08/2016 (Abruzzo Lazio Marche Umbria),"+"3 - Contribuenti residenti al 12/2/2011 nel comune di Lampedusa e Linosa (migranti Nord Africa),"+"4 - Soggetti colpiti dagli eventi sismici dell'ottobre 2016 (Abruzzo Lazio Marche Umbria),"+"5 - Soggetti colpiti dagli eventi sismici di gennaio 2017,"+"8 - Contribuenti colpiti da altri eventi eccezionali (Certificazione unica e 770)", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oEVEECC_2017_1_64.RadioValue()
    return(iif(this.value =1,'0',;
    iif(this.value =2,'1',;
    iif(this.value =3,'2',;
    iif(this.value =4,'3',;
    iif(this.value =5,'4',;
    iif(this.value =6,'5',;
    iif(this.value =7,'8',;
    space(1)))))))))
  endfunc
  func oEVEECC_2017_1_64.GetRadio()
    this.Parent.oContained.w_EVEECC_2017 = this.RadioValue()
    return .t.
  endfunc

  func oEVEECC_2017_1_64.SetRadio()
    this.Parent.oContained.w_EVEECC_2017=trim(this.Parent.oContained.w_EVEECC_2017)
    this.value = ;
      iif(this.Parent.oContained.w_EVEECC_2017=='0',1,;
      iif(this.Parent.oContained.w_EVEECC_2017=='1',2,;
      iif(this.Parent.oContained.w_EVEECC_2017=='2',3,;
      iif(this.Parent.oContained.w_EVEECC_2017=='3',4,;
      iif(this.Parent.oContained.w_EVEECC_2017=='4',5,;
      iif(this.Parent.oContained.w_EVEECC_2017=='5',6,;
      iif(this.Parent.oContained.w_EVEECC_2017=='8',7,;
      0)))))))
  endfunc

  func oEVEECC_2017_1_64.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' )
    endwith
   endif
  endfunc

  func oEVEECC_2017_1_64.mHide()
    with this.Parent.oContained
      return (.w_Ch__Anno<=2016)
    endwith
  endfunc


  add object oCATPAR_1_67 as StdCombo with uid="FAKWCVEHJC",value=28,rtseq=50,rtrep=.f.,left=8,top=419,width=95,height=21;
    , ToolTipText = "Categorie particolari";
    , HelpContextID = 266047270;
    , cFormVar="w_CATPAR",RowSource=""+"A,"+"B,"+"C,"+"D,"+"E,"+"F,"+"G,"+"H,"+"K,"+"L,"+"M,"+"N,"+"P,"+"Q,"+"R,"+"S,"+"T,"+"T1,"+"T2,"+"T3,"+"T4,"+"U,"+"V,"+"W,"+"Y,"+"Z,"+"Z2,"+"Non gestito", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCATPAR_1_67.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'B',;
    iif(this.value =3,'C',;
    iif(this.value =4,'D',;
    iif(this.value =5,'E',;
    iif(this.value =6,'F',;
    iif(this.value =7,'G',;
    iif(this.value =8,'H',;
    iif(this.value =9,'K',;
    iif(this.value =10,'L',;
    iif(this.value =11,'M',;
    iif(this.value =12,'N',;
    iif(this.value =13,'P',;
    iif(this.value =14,'Q',;
    iif(this.value =15,'R',;
    iif(this.value =16,'S',;
    iif(this.value =17,'T',;
    iif(this.value =18,'T1',;
    iif(this.value =19,'T2',;
    iif(this.value =20,'T3',;
    iif(this.value =21,'T4',;
    iif(this.value =22,'U',;
    iif(this.value =23,'V',;
    iif(this.value =24,'W',;
    iif(this.value =25,'Y',;
    iif(this.value =26,'Z',;
    iif(this.value =27,'Z2',;
    iif(this.value =28,'  ',;
    space(1))))))))))))))))))))))))))))))
  endfunc
  func oCATPAR_1_67.GetRadio()
    this.Parent.oContained.w_CATPAR = this.RadioValue()
    return .t.
  endfunc

  func oCATPAR_1_67.SetRadio()
    this.Parent.oContained.w_CATPAR=trim(this.Parent.oContained.w_CATPAR)
    this.value = ;
      iif(this.Parent.oContained.w_CATPAR=='A',1,;
      iif(this.Parent.oContained.w_CATPAR=='B',2,;
      iif(this.Parent.oContained.w_CATPAR=='C',3,;
      iif(this.Parent.oContained.w_CATPAR=='D',4,;
      iif(this.Parent.oContained.w_CATPAR=='E',5,;
      iif(this.Parent.oContained.w_CATPAR=='F',6,;
      iif(this.Parent.oContained.w_CATPAR=='G',7,;
      iif(this.Parent.oContained.w_CATPAR=='H',8,;
      iif(this.Parent.oContained.w_CATPAR=='K',9,;
      iif(this.Parent.oContained.w_CATPAR=='L',10,;
      iif(this.Parent.oContained.w_CATPAR=='M',11,;
      iif(this.Parent.oContained.w_CATPAR=='N',12,;
      iif(this.Parent.oContained.w_CATPAR=='P',13,;
      iif(this.Parent.oContained.w_CATPAR=='Q',14,;
      iif(this.Parent.oContained.w_CATPAR=='R',15,;
      iif(this.Parent.oContained.w_CATPAR=='S',16,;
      iif(this.Parent.oContained.w_CATPAR=='T',17,;
      iif(this.Parent.oContained.w_CATPAR=='T1',18,;
      iif(this.Parent.oContained.w_CATPAR=='T2',19,;
      iif(this.Parent.oContained.w_CATPAR=='T3',20,;
      iif(this.Parent.oContained.w_CATPAR=='T4',21,;
      iif(this.Parent.oContained.w_CATPAR=='U',22,;
      iif(this.Parent.oContained.w_CATPAR=='V',23,;
      iif(this.Parent.oContained.w_CATPAR=='W',24,;
      iif(this.Parent.oContained.w_CATPAR=='Y',25,;
      iif(this.Parent.oContained.w_CATPAR=='Z',26,;
      iif(this.Parent.oContained.w_CATPAR=='Z2',27,;
      iif(this.Parent.oContained.w_CATPAR=='',28,;
      0))))))))))))))))))))))))))))
  endfunc

  func oCATPAR_1_67.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' )
    endwith
   endif
  endfunc

  func oCATPAR_1_67.mHide()
    with this.Parent.oContained
      return (.w_Ch__Anno>=2016)
    endwith
  endfunc


  add object oCATPAR_2016_1_68 as StdCombo with uid="RCWLZXDLAR",value=29,rtseq=51,rtrep=.f.,left=8,top=419,width=95,height=21;
    , ToolTipText = "Categorie particolari";
    , HelpContextID = 266281816;
    , cFormVar="w_CATPAR_2016",RowSource=""+"A,"+"B,"+"C,"+"D,"+"E,"+"F,"+"G,"+"H,"+"K,"+"L,"+"M,"+"N,"+"P,"+"Q,"+"R,"+"S,"+"T,"+"T1,"+"T2,"+"T3,"+"T4,"+"U,"+"V,"+"W,"+"Y,"+"Z,"+"Z2,"+"Z3,"+"Non gestito", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCATPAR_2016_1_68.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'B',;
    iif(this.value =3,'C',;
    iif(this.value =4,'D',;
    iif(this.value =5,'E',;
    iif(this.value =6,'F',;
    iif(this.value =7,'G',;
    iif(this.value =8,'H',;
    iif(this.value =9,'K',;
    iif(this.value =10,'L',;
    iif(this.value =11,'M',;
    iif(this.value =12,'N',;
    iif(this.value =13,'P',;
    iif(this.value =14,'Q',;
    iif(this.value =15,'R',;
    iif(this.value =16,'S',;
    iif(this.value =17,'T',;
    iif(this.value =18,'T1',;
    iif(this.value =19,'T2',;
    iif(this.value =20,'T3',;
    iif(this.value =21,'T4',;
    iif(this.value =22,'U',;
    iif(this.value =23,'V',;
    iif(this.value =24,'W',;
    iif(this.value =25,'Y',;
    iif(this.value =26,'Z',;
    iif(this.value =27,'Z2',;
    iif(this.value =28,'Z3',;
    iif(this.value =29,'  ',;
    space(1)))))))))))))))))))))))))))))))
  endfunc
  func oCATPAR_2016_1_68.GetRadio()
    this.Parent.oContained.w_CATPAR_2016 = this.RadioValue()
    return .t.
  endfunc

  func oCATPAR_2016_1_68.SetRadio()
    this.Parent.oContained.w_CATPAR_2016=trim(this.Parent.oContained.w_CATPAR_2016)
    this.value = ;
      iif(this.Parent.oContained.w_CATPAR_2016=='A',1,;
      iif(this.Parent.oContained.w_CATPAR_2016=='B',2,;
      iif(this.Parent.oContained.w_CATPAR_2016=='C',3,;
      iif(this.Parent.oContained.w_CATPAR_2016=='D',4,;
      iif(this.Parent.oContained.w_CATPAR_2016=='E',5,;
      iif(this.Parent.oContained.w_CATPAR_2016=='F',6,;
      iif(this.Parent.oContained.w_CATPAR_2016=='G',7,;
      iif(this.Parent.oContained.w_CATPAR_2016=='H',8,;
      iif(this.Parent.oContained.w_CATPAR_2016=='K',9,;
      iif(this.Parent.oContained.w_CATPAR_2016=='L',10,;
      iif(this.Parent.oContained.w_CATPAR_2016=='M',11,;
      iif(this.Parent.oContained.w_CATPAR_2016=='N',12,;
      iif(this.Parent.oContained.w_CATPAR_2016=='P',13,;
      iif(this.Parent.oContained.w_CATPAR_2016=='Q',14,;
      iif(this.Parent.oContained.w_CATPAR_2016=='R',15,;
      iif(this.Parent.oContained.w_CATPAR_2016=='S',16,;
      iif(this.Parent.oContained.w_CATPAR_2016=='T',17,;
      iif(this.Parent.oContained.w_CATPAR_2016=='T1',18,;
      iif(this.Parent.oContained.w_CATPAR_2016=='T2',19,;
      iif(this.Parent.oContained.w_CATPAR_2016=='T3',20,;
      iif(this.Parent.oContained.w_CATPAR_2016=='T4',21,;
      iif(this.Parent.oContained.w_CATPAR_2016=='U',22,;
      iif(this.Parent.oContained.w_CATPAR_2016=='V',23,;
      iif(this.Parent.oContained.w_CATPAR_2016=='W',24,;
      iif(this.Parent.oContained.w_CATPAR_2016=='Y',25,;
      iif(this.Parent.oContained.w_CATPAR_2016=='Z',26,;
      iif(this.Parent.oContained.w_CATPAR_2016=='Z2',27,;
      iif(this.Parent.oContained.w_CATPAR_2016=='Z3',28,;
      iif(this.Parent.oContained.w_CATPAR_2016=='',29,;
      0)))))))))))))))))))))))))))))
  endfunc

  func oCATPAR_2016_1_68.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' )
    endwith
   endif
  endfunc

  func oCATPAR_2016_1_68.mHide()
    with this.Parent.oContained
      return (.w_Ch__Anno<2016)
    endwith
  endfunc

  add object oCHCOMUNE_1_72 as StdField with uid="UZXJOQKVMN",rtseq=53,rtrep=.f.,;
    cFormVar = "w_CHCOMUNE", cQueryName = "CHCOMUNE",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Comune ",;
    HelpContextID = 60393067,;
   bGlobalFont=.t.,;
    Height=21, Width=230, Left=8, Top=473, cSayPict="REPL('!',30)", cGetPict="REPL('!',30)", InputMask=replicate('X',30), bHasZoom = .t. 

  func oCHCOMUNE_1_72.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' )
    endwith
   endif
  endfunc

  proc oCHCOMUNE_1_72.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C","",".w_CHCOMUNE",".w_CHPROVIN")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oCHPROVIN_1_73 as StdField with uid="WJHWGIYJEO",rtseq=54,rtrep=.f.,;
    cFormVar = "w_CHPROVIN", cQueryName = "CHPROVIN",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Provincia",;
    HelpContextID = 188918156,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=345, Top=473, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2), bHasZoom = .t. 

  func oCHPROVIN_1_73.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' )
    endwith
   endif
  endfunc

  proc oCHPROVIN_1_73.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"P","","",".w_CHPROVIN")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oCHCODCOM_1_75 as StdField with uid="JPPHEZDBPD",rtseq=55,rtrep=.f.,;
    cFormVar = "w_CHCODCOM", cQueryName = "CHCODCOM",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Codice comune ",;
    HelpContextID = 17401459,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=481, Top=473, cSayPict='"!!!!"', cGetPict='"!!!!"', InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ENTI_COM", cZoomOnZoom="Gscg_Aec", oKey_1_1="ECCODICE", oKey_1_2="this.w_CHCODCOM"

  func oCHCODCOM_1_75.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query'  AND .w_CHCAUPRE='N')
    endwith
   endif
  endfunc

  func oCHCODCOM_1_75.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_75('Part',this)
    endwith
    return bRes
  endfunc

  proc oCHCODCOM_1_75.ecpDrop(oSource)
    this.Parent.oContained.link_1_75('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCHCODCOM_1_75.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ENTI_COM','*','ECCODICE',cp_AbsName(this.parent,'oCHCODCOM_1_75'),iif(empty(i_cWhere),.f.,i_cWhere),'Gscg_Aec',"Codice enti/comuni",'',this.parent.oContained
  endproc
  proc oCHCODCOM_1_75.mZoomOnZoom
    local i_obj
    i_obj=Gscg_Aec()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ECCODICE=this.parent.oContained.w_CHCODCOM
     i_obj.ecpSave()
  endproc

  add object oCHFUSCOM_1_76 as StdField with uid="QCQTTRKXMJ",rtseq=56,rtrep=.f.,;
    cFormVar = "w_CHFUSCOM", cQueryName = "CHFUSCOM",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Codice fusione comuni",;
    HelpContextID = 33535603,;
   bGlobalFont=.t.,;
    Height=21, Width=63, Left=598, Top=473, cSayPict='"!!!!"', cGetPict='"!!!!"', InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ENTI_COM", cZoomOnZoom="Gscg_Aec", oKey_1_1="ECCODICE", oKey_1_2="this.w_CHFUSCOM"

  func oCHFUSCOM_1_76.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query'  AND .w_CHCAUPRE='N')
    endwith
   endif
  endfunc

  func oCHFUSCOM_1_76.mHide()
    with this.Parent.oContained
      return (.w_Ch__Anno<2017)
    endwith
  endfunc

  func oCHFUSCOM_1_76.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_76('Part',this)
    endwith
    return bRes
  endfunc

  proc oCHFUSCOM_1_76.ecpDrop(oSource)
    this.Parent.oContained.link_1_76('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCHFUSCOM_1_76.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ENTI_COM','*','ECCODICE',cp_AbsName(this.parent,'oCHFUSCOM_1_76'),iif(empty(i_cWhere),.f.,i_cWhere),'Gscg_Aec',"Codice enti/comuni",'',this.parent.oContained
  endproc
  proc oCHFUSCOM_1_76.mZoomOnZoom
    local i_obj
    i_obj=Gscg_Aec()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ECCODICE=this.parent.oContained.w_CHFUSCOM
     i_obj.ecpSave()
  endproc

  add object oCHSTAEST_1_80 as StdField with uid="JZJZNJGHQI",rtseq=57,rtrep=.f.,;
    cFormVar = "w_CHSTAEST", cQueryName = "CHSTAEST",nZero=3,;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice stato estero",;
    HelpContextID = 48203386,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=481, Top=621, cSayPict='"999"', cGetPict='"999"', InputMask=replicate('X',3)

  func oCHSTAEST_1_80.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' And .w_CHPEREST='S')
    endwith
   endif
  endfunc

  add object oCHCITEST_1_81 as StdField with uid="VZATVQHIMD",rtseq=58,rtrep=.f.,;
    cFormVar = "w_CHCITEST", cQueryName = "CHCITEST",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Localit� di residenza estera",;
    HelpContextID = 67339898,;
   bGlobalFont=.t.,;
    Height=21, Width=230, Left=481, Top=564, cSayPict='REPLICATE("!",30)', cGetPict='REPLICATE("!",30)', InputMask=replicate('X',30)

  func oCHCITEST_1_81.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' AND .w_CHPEREST='S')
    endwith
   endif
  endfunc

  add object oCHINDEST_1_82 as StdField with uid="PPBQNCJMIR",rtseq=59,rtrep=.f.,;
    cFormVar = "w_CHINDEST", cQueryName = "CHINDEST",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo estero",;
    HelpContextID = 50914938,;
   bGlobalFont=.t.,;
    Height=21, Width=321, Left=13, Top=621, cSayPict="REPL('!',35)", cGetPict="REPL('!',35)", InputMask=replicate('X',35)

  func oCHINDEST_1_82.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' And .w_CHPEREST='S')
    endwith
   endif
  endfunc

  add object oCHCOFISC_1_86 as StdField with uid="NEICLLJLQF",rtseq=60,rtrep=.f.,;
    cFormVar = "w_CHCOFISC", cQueryName = "CHCOFISC",;
    bObbl = .f. , nPag = 1, value=space(25), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale",;
    HelpContextID = 120161897,;
   bGlobalFont=.t.,;
    Height=21, Width=202, Left=11, Top=564, cSayPict='REPLICATE("!",25)', cGetPict='REPLICATE("!",25)', InputMask=replicate('X',25)

  func oCHCOFISC_1_86.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' AND .w_CHPEREST='S')
    endwith
   endif
  endfunc

  add object oCHDAVERI_1_90 as StdCheck with uid="CGOCJPGUTC",rtseq=61,rtrep=.f.,left=671, top=62, caption="Da verificare",;
    ToolTipText = "Se attivo, la certificazione � da verificare",;
    HelpContextID = 68916847,;
    cFormVar="w_CHDAVERI", bObbl = .f. , nPag = 1;
    , tabstop=.f.;
   , bGlobalFont=.t.


  func oCHDAVERI_1_90.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCHDAVERI_1_90.GetRadio()
    this.Parent.oContained.w_CHDAVERI = this.RadioValue()
    return .t.
  endfunc

  func oCHDAVERI_1_90.SetRadio()
    this.Parent.oContained.w_CHDAVERI=trim(this.Parent.oContained.w_CHDAVERI)
    this.value = ;
      iif(this.Parent.oContained.w_CHDAVERI=='S',1,;
      0)
  endfunc

  func oCHDAVERI_1_90.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Chtipope='O' Or Not Empty(.w_Chdatest))
    endwith
   endif
  endfunc

  add object oCHSCHUMA_1_93 as StdCheck with uid="KLCFMTVBIW",rtseq=64,rtrep=.f.,left=671, top=37, caption="Non residenti Schumacker", enabled=.f.,;
    ToolTipText = "Non residente Schumacker",;
    HelpContextID = 54429287,;
    cFormVar="w_CHSCHUMA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCHSCHUMA_1_93.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCHSCHUMA_1_93.GetRadio()
    this.Parent.oContained.w_CHSCHUMA = this.RadioValue()
    return .t.
  endfunc

  func oCHSCHUMA_1_93.SetRadio()
    this.Parent.oContained.w_CHSCHUMA=trim(this.Parent.oContained.w_CHSCHUMA)
    this.value = ;
      iif(this.Parent.oContained.w_CHSCHUMA=='S',1,;
      0)
  endfunc


  add object oObj_1_94 as cp_runprogram with uid="NBFRWAJSRD",left=-1, top=668, width=172,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSRI_BHC(w_HASEVCOP)",;
    cEvent = "HasEvent",;
    nPag=1;
    , ToolTipText = "Richiamato dall'area manuale declare (hascpevent)";
    , HelpContextID = 68292838

  add object oCHPROTEC_1_98 as StdField with uid="EDQSXLYFHF",rtseq=109,rtrep=.f.,;
    cFormVar = "w_CHPROTEC", cQueryName = "CHPROTEC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(17), bMultilanguage =  .f.,;
    ToolTipText = "Identificativo dell'invio",;
    HelpContextID = 222472599,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=325, Top=97, cSayPict='"99999999999999999"', cGetPict='"99999999999999999"', InputMask=replicate('X',17), Alignment=1

  func oCHPROTEC_1_98.mHide()
    with this.Parent.oContained
      return (Empty(.w_Gdserial))
    endwith
  endfunc

  add object oCHPRODOC_1_100 as StdField with uid="XVWTCPJEPM",rtseq=110,rtrep=.f.,;
    cFormVar = "w_CHPRODOC", cQueryName = "CHPRODOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Progressivo attribuito dal servizio telematico alla singola certificazione unica",;
    HelpContextID = 45962857,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=482, Top=97, cSayPict='"999999"', cGetPict='"999999"'

  func oCHPRODOC_1_100.mHide()
    with this.Parent.oContained
      return (Empty(.w_Gdserial))
    endwith
  endfunc

  add object oCHDATSTA_1_102 as StdField with uid="VXSONYPPLP",rtseq=112,rtrep=.f.,;
    cFormVar = "w_CHDATSTA", cQueryName = "CHDATSTA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 235170201,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=325, Top=134

  func oCHDATSTA_1_102.mHide()
    with this.Parent.oContained
      return (Empty(.w_Chdatsta))
    endwith
  endfunc


  add object oBtn_1_103 as StdButton with uid="WVWNIOAYSH",left=3, top=117, width=48,height=45,;
    CpPicture="BMP\VERIFICA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per accedere alla generazione file certificazione unica";
    , HelpContextID = 36920711;
    , tabstop=.f., caption='\<File Gen.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_103.Click()
      with this.Parent.oContained
        GSRI_BDH(this.Parent.oContained,"U")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_103.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_GDSERIAL)        )
      endwith
    endif
  endfunc

  func oBtn_1_103.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (EMPTY(.w_GDSERIAL))
     endwith
    endif
  endfunc


  add object oBtn_1_104 as StdButton with uid="ITGRFWZYWT",left=55, top=117, width=48,height=45,;
    CpPicture="bmp\AGGIUNGI.BMP", caption="", nPag=1;
    , ToolTipText = "Permette di visualizzare la certificazione di origine";
    , HelpContextID = 36920711;
    , tabstop=.f., caption='\<Collegato';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_104.Click()
      with this.Parent.oContained
        GSRI_BDH(this.Parent.oContained,"D")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_104.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_CHDATEST) )
      endwith
    endif
  endfunc

  func oBtn_1_104.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (EMPTY(.w_CHDATEST))
     endwith
    endif
  endfunc

  add object oCHCERDEF_1_110 as StdCheck with uid="MBUAAWPKWN",rtseq=124,rtrep=.f.,left=671, top=112, caption="Certificazione definitiva",;
    ToolTipText = "Se attivo, la certificazione sar� definitiva",;
    HelpContextID = 220232084,;
    cFormVar="w_CHCERDEF", bObbl = .f. , nPag = 1;
    , tabstop=.f.;
   , bGlobalFont=.t.


  func oCHCERDEF_1_110.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCHCERDEF_1_110.GetRadio()
    this.Parent.oContained.w_CHCERDEF = this.RadioValue()
    return .t.
  endfunc

  func oCHCERDEF_1_110.SetRadio()
    this.Parent.oContained.w_CHCERDEF=trim(this.Parent.oContained.w_CHCERDEF)
    this.value = ;
      iif(this.Parent.oContained.w_CHCERDEF=='S',1,;
      0)
  endfunc


  add object oSTATO_1_112 as StdCombo with uid="CZQYRPGAZR",rtseq=126,rtrep=.f.,left=529,top=134,width=109,height=21, enabled=.f.;
    , HelpContextID = 21073114;
    , cFormVar="w_STATO",RowSource=""+"Annullata,"+"Sostituita,"+"Comunicata", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSTATO_1_112.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'S',;
    iif(this.value =3,'C',;
    space(1)))))
  endfunc
  func oSTATO_1_112.GetRadio()
    this.Parent.oContained.w_STATO = this.RadioValue()
    return .t.
  endfunc

  func oSTATO_1_112.SetRadio()
    this.Parent.oContained.w_STATO=trim(this.Parent.oContained.w_STATO)
    this.value = ;
      iif(this.Parent.oContained.w_STATO=='A',1,;
      iif(this.Parent.oContained.w_STATO=='S',2,;
      iif(this.Parent.oContained.w_STATO=='C',3,;
      0)))
  endfunc

  func oSTATO_1_112.mHide()
    with this.Parent.oContained
      return (Empty(.w_Stato))
    endwith
  endfunc

  add object oStr_1_40 as StdString with uid="SJCUFHTJAM",Visible=.t., Left=29, Top=16,;
    Alignment=1, Width=66, Height=18,;
    Caption="Anno:"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="DSZQYEUOKT",Visible=.t., Left=303, Top=175,;
    Alignment=0, Width=306, Height=19,;
    Caption="Dati relativi al percipiente delle somme"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_43 as StdString with uid="ELWHFLJCPX",Visible=.t., Left=255, Top=16,;
    Alignment=1, Width=66, Height=18,;
    Caption="Fornitore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="RINPZJSKLJ",Visible=.t., Left=181, Top=57,;
    Alignment=1, Width=140, Height=18,;
    Caption="Causale prestazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="BSYUVARGFL",Visible=.t., Left=11, Top=192,;
    Alignment=0, Width=116, Height=17,;
    Caption="Codice fiscale"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_49 as StdString with uid="OBGOXQVJWR",Visible=.t., Left=11, Top=242,;
    Alignment=0, Width=130, Height=17,;
    Caption="Ragione sociale"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_50 as StdString with uid="KOKISJQUOL",Visible=.t., Left=11, Top=294,;
    Alignment=0, Width=130, Height=17,;
    Caption="Cognome"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_51 as StdString with uid="GZLERJIXDA",Visible=.t., Left=345, Top=298,;
    Alignment=0, Width=76, Height=13,;
    Caption="Nome"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_54 as StdString with uid="ZDKIULOHFL",Visible=.t., Left=11, Top=349,;
    Alignment=0, Width=76, Height=13,;
    Caption="Data di nascita"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_55 as StdString with uid="BBXIREUHWZ",Visible=.t., Left=719, Top=294,;
    Alignment=0, Width=76, Height=17,;
    Caption="Sesso"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_57 as StdString with uid="CTCAOZXFVH",Visible=.t., Left=345, Top=349,;
    Alignment=0, Width=223, Height=13,;
    Caption="Comune (o stato estero) di nascita"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_59 as StdString with uid="DMKPRROUYT",Visible=.t., Left=719, Top=349,;
    Alignment=0, Width=71, Height=13,;
    Caption="Provincia"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_60 as StdString with uid="LDIRJGPIQF",Visible=.t., Left=11, Top=400,;
    Alignment=0, Width=101, Height=17,;
    Caption="Categorie particolari"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_61 as StdString with uid="NXQLRFYAQM",Visible=.t., Left=214, Top=400,;
    Alignment=0, Width=90, Height=17,;
    Caption="Eventi eccezionali"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_71 as StdString with uid="CBIDZKEZDH",Visible=.t., Left=11, Top=456,;
    Alignment=0, Width=76, Height=17,;
    Caption="Comune"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_74 as StdString with uid="ZGFEXDSQJP",Visible=.t., Left=345, Top=456,;
    Alignment=0, Width=71, Height=17,;
    Caption="Provincia"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_77 as StdString with uid="ANHHASVODD",Visible=.t., Left=481, Top=456,;
    Alignment=0, Width=83, Height=17,;
    Caption="Codice comune"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_78 as StdString with uid="JYMPJPJYDE",Visible=.t., Left=303, Top=515,;
    Alignment=0, Width=306, Height=19,;
    Caption="Riservato ai percipienti esteri"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_83 as StdString with uid="NOWYYKZWAZ",Visible=.t., Left=481, Top=605,;
    Alignment=0, Width=83, Height=13,;
    Caption="Stato estero"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_84 as StdString with uid="WQAUIMBAAE",Visible=.t., Left=481, Top=544,;
    Alignment=0, Width=137, Height=17,;
    Caption="Localit� di residenza estera"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_85 as StdString with uid="HGABSVUXLY",Visible=.t., Left=13, Top=601,;
    Alignment=0, Width=97, Height=17,;
    Caption="Indirizzo estero"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_87 as StdString with uid="XXZKENNKMI",Visible=.t., Left=11, Top=547,;
    Alignment=0, Width=204, Height=17,;
    Caption="Codice identificativo fiscale"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_88 as StdString with uid="YKWHYQEIBN",Visible=.t., Left=428, Top=57,;
    Alignment=1, Width=82, Height=18,;
    Caption="Tipo operaz.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_89 as StdString with uid="CAMXGTNEUO",Visible=.t., Left=18, Top=67,;
    Alignment=0, Width=162, Height=18,;
    Caption="INSERIMENTO MANUALE"    , ForeColor=RGB(255,0,0);
  ;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_89.mHide()
    with this.Parent.oContained
      return (.w_CHTIPDAT='A')
    endwith
  endfunc

  add object oStr_1_97 as StdString with uid="ATOTGNMBHD",Visible=.t., Left=201, Top=100,;
    Alignment=0, Width=120, Height=18,;
    Caption="Identificativo dell'invio:"  ;
  , bGlobalFont=.t.

  func oStr_1_97.mHide()
    with this.Parent.oContained
      return (Empty(.w_Gdserial))
    endwith
  endfunc

  add object oStr_1_99 as StdString with uid="RFEXXCOTSI",Visible=.t., Left=469, Top=97,;
    Alignment=0, Width=18, Height=18,;
    Caption="-"  ;
  , bGlobalFont=.t.

  func oStr_1_99.mHide()
    with this.Parent.oContained
      return (Empty(.w_Gdserial))
    endwith
  endfunc

  add object oStr_1_106 as StdString with uid="LZPDFTTQZK",Visible=.t., Left=233, Top=137,;
    Alignment=1, Width=88, Height=18,;
    Caption="Stampata il:"  ;
  , bGlobalFont=.t.

  func oStr_1_106.mHide()
    with this.Parent.oContained
      return (Empty(.w_Chdatsta))
    endwith
  endfunc

  add object oStr_1_107 as StdString with uid="SJYATLBCQQ",Visible=.t., Left=20, Top=93,;
    Alignment=0, Width=160, Height=18,;
    Caption="CERTIFICAZIONE ACCOLTA"    , ForeColor=RGB(0,0,255);
  ;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_107.mHide()
    with this.Parent.oContained
      return (.w_Chtiprec<>'R')
    endwith
  endfunc

  add object oStr_1_108 as StdString with uid="SMSWQKERHC",Visible=.t., Left=20, Top=93,;
    Alignment=0, Width=165, Height=18,;
    Caption="CERTIFICAZIONE RESPINTA"    , ForeColor=RGB(255,0,0);
  ;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_108.mHide()
    with this.Parent.oContained
      return (.w_Chtiprec<>'Q')
    endwith
  endfunc

  add object oStr_1_113 as StdString with uid="QRBJUPAIZY",Visible=.t., Left=411, Top=137,;
    Alignment=1, Width=115, Height=18,;
    Caption="Certificazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_113.mHide()
    with this.Parent.oContained
      return (Empty(.w_Stato))
    endwith
  endfunc

  add object oStr_1_115 as StdString with uid="VSGZELWNXO",Visible=.t., Left=598, Top=456,;
    Alignment=0, Width=83, Height=17,;
    Caption="Fusione comune"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_115.mHide()
    with this.Parent.oContained
      return (.w_Ch__Anno<2017)
    endwith
  endfunc

  add object oBox_1_41 as StdBox with uid="RDMWYOEEFE",left=3, top=166, width=846,height=341

  add object oBox_1_79 as StdBox with uid="YLZIQKVHOK",left=3, top=504, width=846,height=149
enddefine
define class tgsri_achPag2 as StdContainer
  Width  = 861
  height = 653
  stdWidth  = 861
  stdheight = 653
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCHTOT002_2_13 as StdField with uid="FQQFFWJJCH",rtseq=65,rtrep=.f.,;
    cFormVar = "w_CHTOT002", cQueryName = "CHTOT002",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Anno in cui � sorto il diritto alla percezione",;
    HelpContextID = 252352088,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=105, Top=84, cSayPict='"9999"', cGetPict='"9999"'

  func oCHTOT002_2_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((Alltrim(.w_Chcaupre) $ 'G-H-I' Or .w_Chtot003='S') And .w_Var_Dati='S')
    endwith
   endif
  endfunc

  add object oCHTOT003_2_14 as StdCheck with uid="FFLFRSIXYA",rtseq=66,rtrep=.f.,left=192, top=83, caption="Anticipazione",;
    ToolTipText = "Anticipazione",;
    HelpContextID = 252352089,;
    cFormVar="w_CHTOT003", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oCHTOT003_2_14.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCHTOT003_2_14.GetRadio()
    this.Parent.oContained.w_CHTOT003 = this.RadioValue()
    return .t.
  endfunc

  func oCHTOT003_2_14.SetRadio()
    this.Parent.oContained.w_CHTOT003=trim(this.Parent.oContained.w_CHTOT003)
    this.value = ;
      iif(this.Parent.oContained.w_CHTOT003=='S',1,;
      0)
  endfunc

  func oCHTOT003_2_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Var_Dati='S')
    endwith
   endif
  endfunc

  add object oCHTOT004_2_15 as StdField with uid="RIGUXYQZMJ",rtseq=67,rtrep=.f.,;
    cFormVar = "w_CHTOT004", cQueryName = "CHTOT004",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Non � possibile inserire importi negativi",;
    ToolTipText = "Ammontare lordo corrisposto",;
    HelpContextID = 252352090,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=316, Top=84, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  func oCHTOT004_2_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Var_Dati='S')
    endwith
   endif
  endfunc

  func oCHTOT004_2_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_Chtot004>=0)
    endwith
    return bRes
  endfunc

  add object oCHTOT005_2_16 as StdField with uid="DICFWXAPUC",rtseq=68,rtrep=.f.,;
    cFormVar = "w_CHTOT005", cQueryName = "CHTOT005",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Non � possibile inserire importi negativi",;
    ToolTipText = "Somme non soggette",;
    HelpContextID = 252352091,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=509, Top=84, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  func oCHTOT005_2_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Var_Dati='S')
    endwith
   endif
  endfunc

  func oCHTOT005_2_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_Chtot005>=0)
    endwith
    return bRes
  endfunc


  add object oTOT006_2_17 as StdCombo with uid="TKBYPLVJKZ",rtseq=69,rtrep=.f.,left=6,top=130,width=306,height=21;
    , ToolTipText = "Codice somme non soggette";
    , HelpContextID = 44801590;
    , cFormVar="w_TOT006",RowSource=""+"1 - Compensi docenti/ricercatori,"+"2 - Lavoratori con beneficio fiscale ,"+"3 - Erogazione di altri redditi non sogg. a ritenuta", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oTOT006_2_17.RadioValue()
    return(iif(this.value =1,'1',;
    iif(this.value =2,'2',;
    iif(this.value =3,'3',;
    ' '))))
  endfunc
  func oTOT006_2_17.GetRadio()
    this.Parent.oContained.w_TOT006 = this.RadioValue()
    return .t.
  endfunc

  func oTOT006_2_17.SetRadio()
    this.Parent.oContained.w_TOT006=trim(this.Parent.oContained.w_TOT006)
    this.value = ;
      iif(this.Parent.oContained.w_TOT006=='1',1,;
      iif(this.Parent.oContained.w_TOT006=='2',2,;
      iif(this.Parent.oContained.w_TOT006=='3',3,;
      0)))
  endfunc

  func oTOT006_2_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Var_Dati='S')
    endwith
   endif
  endfunc

  func oTOT006_2_17.mHide()
    with this.Parent.oContained
      return (.w_CHTOT005=0 OR (.w_CHPEREST='S' AND .w_ANFLSGRE='S') OR .w_CH__ANNO>=2016)
    endwith
  endfunc


  add object oTOT006_2016_2_18 as StdCombo with uid="CFRZBYMVHG",value=1,rtseq=70,rtrep=.f.,left=6,top=130,width=306,height=21;
    , ToolTipText = "Codice somme non soggette";
    , HelpContextID = 45036136;
    , cFormVar="w_TOT006_2016",RowSource=""+"Nessuno,"+"1 - Compensi docenti/ricercatori Legge n. 2 del 28 gennaio 2009,"+"2 - Lavoratori con beneficio fiscale ex art. 3 Legge 30 dicembre 2010/238,"+"5 - Compensi soggetti art.16 del D.Igs. n. 147 del 2015,"+"6 - Erogazione di altri redditi non soggetti a ritenuta", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oTOT006_2016_2_18.RadioValue()
    return(iif(this.value =1,' ',;
    iif(this.value =2,'1',;
    iif(this.value =3,'2',;
    iif(this.value =4,'5',;
    iif(this.value =5,'6',;
    ' '))))))
  endfunc
  func oTOT006_2016_2_18.GetRadio()
    this.Parent.oContained.w_TOT006_2016 = this.RadioValue()
    return .t.
  endfunc

  func oTOT006_2016_2_18.SetRadio()
    this.Parent.oContained.w_TOT006_2016=trim(this.Parent.oContained.w_TOT006_2016)
    this.value = ;
      iif(this.Parent.oContained.w_TOT006_2016=='',1,;
      iif(this.Parent.oContained.w_TOT006_2016=='1',2,;
      iif(this.Parent.oContained.w_TOT006_2016=='2',3,;
      iif(this.Parent.oContained.w_TOT006_2016=='5',4,;
      iif(this.Parent.oContained.w_TOT006_2016=='6',5,;
      0)))))
  endfunc

  func oTOT006_2016_2_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Var_Dati='S')
    endwith
   endif
  endfunc

  func oTOT006_2016_2_18.mHide()
    with this.Parent.oContained
      return (.w_CHTOT005=0 OR (.w_CHPEREST='S' AND .w_ANFLSGRE='S') OR .w_CH__ANNO<>2016)
    endwith
  endfunc


  add object oTOT006_2017_2_19 as StdCombo with uid="CPXVNDZOAS",value=1,rtseq=71,rtrep=.f.,left=6,top=130,width=306,height=21;
    , ToolTipText = "Codice somme non soggette";
    , HelpContextID = 45040232;
    , cFormVar="w_TOT006_2017",RowSource=""+"Nessuno,"+"1 - Compensi docenti/ricercatori Legge n. 2 del 28 gennaio 2009,"+"2 - Lavoratori con beneficio fiscale ex art. 3 Legge 30 dicembre 2010/238,"+"5 - Compensi soggetti art.16 del D.Igs. n. 147 del 2015,"+"6 - Assegni di servizio civile,"+"7 - Erogazione di altri redditi non soggetti a ritenuta", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oTOT006_2017_2_19.RadioValue()
    return(iif(this.value =1,' ',;
    iif(this.value =2,'1',;
    iif(this.value =3,'2',;
    iif(this.value =4,'5',;
    iif(this.value =5,'6',;
    iif(this.value =6,'7',;
    ' ')))))))
  endfunc
  func oTOT006_2017_2_19.GetRadio()
    this.Parent.oContained.w_TOT006_2017 = this.RadioValue()
    return .t.
  endfunc

  func oTOT006_2017_2_19.SetRadio()
    this.Parent.oContained.w_TOT006_2017=trim(this.Parent.oContained.w_TOT006_2017)
    this.value = ;
      iif(this.Parent.oContained.w_TOT006_2017=='',1,;
      iif(this.Parent.oContained.w_TOT006_2017=='1',2,;
      iif(this.Parent.oContained.w_TOT006_2017=='2',3,;
      iif(this.Parent.oContained.w_TOT006_2017=='5',4,;
      iif(this.Parent.oContained.w_TOT006_2017=='6',5,;
      iif(this.Parent.oContained.w_TOT006_2017=='7',6,;
      0))))))
  endfunc

  func oTOT006_2017_2_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Var_Dati='S')
    endwith
   endif
  endfunc

  func oTOT006_2017_2_19.mHide()
    with this.Parent.oContained
      return (.w_CHTOT005=0 OR (.w_CHPEREST='S' AND .w_ANFLSGRE='S') OR .w_CH__ANNO<2017)
    endwith
  endfunc

  add object oCHTOT008_2_22 as StdField with uid="WOECNUYPNB",rtseq=73,rtrep=.f.,;
    cFormVar = "w_CHTOT008", cQueryName = "CHTOT008",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Non � possibile inserire importi negativi",;
    ToolTipText = "Imponibile",;
    HelpContextID = 252352094,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=316, Top=131, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  func oCHTOT008_2_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Var_Dati='S')
    endwith
   endif
  endfunc

  func oCHTOT008_2_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_Chtot008>=0)
    endwith
    return bRes
  endfunc

  add object oCHTOT009_2_23 as StdField with uid="TXSKUPXCUQ",rtseq=74,rtrep=.f.,;
    cFormVar = "w_CHTOT009", cQueryName = "CHTOT009",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Non � possibile inserire importi negativi",;
    ToolTipText = "Ritenute a titolo di acconto",;
    HelpContextID = 252352095,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=509, Top=131, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  func oCHTOT009_2_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Var_Dati='S')
    endwith
   endif
  endfunc

  func oCHTOT009_2_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_Chtot009>=0)
    endwith
    return bRes
  endfunc

  add object oCHTOT018_2_24 as StdField with uid="FLSRODIUDD",rtseq=75,rtrep=.f.,;
    cFormVar = "w_CHTOT018", cQueryName = "CHTOT018",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Non � possibile inserire importi negativi",;
    ToolTipText = "Imponibile anni precedenti",;
    HelpContextID = 252352094,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=6, Top=178, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  func oCHTOT018_2_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Var_Dati='S')
    endwith
   endif
  endfunc

  func oCHTOT018_2_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_Chtot018>=0)
    endwith
    return bRes
  endfunc

  add object oCHTOT019_2_25 as StdField with uid="FCIXDYKBTM",rtseq=76,rtrep=.f.,;
    cFormVar = "w_CHTOT019", cQueryName = "CHTOT019",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Non � possibile inserire importi negativi",;
    ToolTipText = "Ritenute operate anni precedenti",;
    HelpContextID = 252352095,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=316, Top=178, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  func oCHTOT019_2_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Var_Dati='S')
    endwith
   endif
  endfunc

  func oCHTOT019_2_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_Chtot019>=0)
    endwith
    return bRes
  endfunc

  add object oCHTOT020_2_26 as StdField with uid="AKADZSUJRK",rtseq=77,rtrep=.f.,;
    cFormVar = "w_CHTOT020", cQueryName = "CHTOT020",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Non � possibile inserire importi negativi",;
    ToolTipText = "Spese rimborsate",;
    HelpContextID = 252352086,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=509, Top=180, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  func oCHTOT020_2_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Var_Dati='S')
    endwith
   endif
  endfunc

  func oCHTOT020_2_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_Chtot020>=0)
    endwith
    return bRes
  endfunc

  add object oCHTOT021_2_27 as StdField with uid="SCFXNOWBXL",rtseq=78,rtrep=.f.,;
    cFormVar = "w_CHTOT021", cQueryName = "CHTOT021",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Non � possibile inserire importi negativi",;
    ToolTipText = "Ritenute rimborsate",;
    HelpContextID = 252352087,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=665, Top=180, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  func oCHTOT021_2_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Alltrim(.w_Chcaupre) $ 'X-Y' And .w_Var_Dati='S')
    endwith
   endif
  endfunc

  func oCHTOT021_2_27.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_Chtot021>=0)
    endwith
    return bRes
  endfunc

  add object oCHTOT041_2_28 as StdField with uid="MNOLXHPIRY",rtseq=79,rtrep=.f.,;
    cFormVar = "w_CHTOT041", cQueryName = "CHTOT041",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Non � possibile inserire importi negativi",;
    ToolTipText = "Somme corrisposte prima della data del fallimento",;
    HelpContextID = 252352087,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=6, Top=224, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  func oCHTOT041_2_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Var_Dati='S')
    endwith
   endif
  endfunc

  func oCHTOT041_2_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_Chtot041>=0)
    endwith
    return bRes
  endfunc

  add object oCHTOT042_2_29 as StdField with uid="IDARNZTAEH",rtseq=80,rtrep=.f.,;
    cFormVar = "w_CHTOT042", cQueryName = "CHTOT042",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Non � possibile inserire importi negativi",;
    ToolTipText = "Somme corrisposte dal curatore/commissario",;
    HelpContextID = 252352088,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=316, Top=226, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  func oCHTOT042_2_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Var_Dati='S')
    endwith
   endif
  endfunc

  func oCHTOT042_2_29.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_Chtot042>=0)
    endwith
    return bRes
  endfunc

  add object oCHFISTRA_2_30 as StdField with uid="RBKAWFPGJG",rtseq=81,rtrep=.f.,;
    cFormVar = "w_CHFISTRA", cQueryName = "CHFISTRA",;
    bObbl = .f. , nPag = 2, value=space(16), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale operazioni straordinarie",;
    HelpContextID = 49526375,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=665, Top=226, cSayPict='REPLICATE("!",16)', cGetPict='REPLICATE("!",16)', InputMask=replicate('X',16)

  func oCHFISTRA_2_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Var_Dati='S')
    endwith
   endif
  endfunc

  add object oCHT1T002_2_31 as StdField with uid="JVGOTIPVDY",rtseq=82,rtrep=.f.,;
    cFormVar = "w_CHT1T002", cQueryName = "CHT1T002",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Anno in cui � sorto il diritto alla percezione",;
    HelpContextID = 250386008,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=115, Top=314, cSayPict='"9999"', cGetPict='"9999"'

  func oCHT1T002_2_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Chatt1mod='S' And (Alltrim(.w_Chcaupre) $ 'G-H-I' Or .w_Cht1t003='S') And .w_Var_Dati='S')
    endwith
   endif
  endfunc

  add object oCHT1T003_2_32 as StdCheck with uid="RCSSYNIROE",rtseq=83,rtrep=.f.,left=192, top=312, caption="Anticipazione",;
    ToolTipText = "Anticipazione",;
    HelpContextID = 250386009,;
    cFormVar="w_CHT1T003", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oCHT1T003_2_32.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCHT1T003_2_32.GetRadio()
    this.Parent.oContained.w_CHT1T003 = this.RadioValue()
    return .t.
  endfunc

  func oCHT1T003_2_32.SetRadio()
    this.Parent.oContained.w_CHT1T003=trim(this.Parent.oContained.w_CHT1T003)
    this.value = ;
      iif(this.Parent.oContained.w_CHT1T003=='S',1,;
      0)
  endfunc

  func oCHT1T003_2_32.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Chatt1mod='S' And .w_Var_Dati='S')
    endwith
   endif
  endfunc

  add object oCHT1T005_2_33 as StdField with uid="BVTMTTGCFW",rtseq=84,rtrep=.f.,;
    cFormVar = "w_CHT1T005", cQueryName = "CHT1T005",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Non � possibile inserire importi negativi",;
    ToolTipText = "Somme non soggette",;
    HelpContextID = 250386011,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=316, Top=314, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  func oCHT1T005_2_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CHATT1MOD='S' And .w_Var_Dati='S')
    endwith
   endif
  endfunc

  func oCHT1T005_2_33.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_Cht1t005>=0)
    endwith
    return bRes
  endfunc


  add object oTOTM1006_2_34 as StdCombo with uid="UQPGHEDHHP",rtseq=85,rtrep=.f.,left=509,top=313,width=306,height=21;
    , ToolTipText = "Codice somme non soggette";
    , HelpContextID = 215522924;
    , cFormVar="w_TOTM1006",RowSource=""+"1 - Compensi docenti/ricercatori,"+"2 - Lavoratori con beneficio fiscale ,"+"3 - Erogazione di altri redditi non sogg. a ritenuta", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oTOTM1006_2_34.RadioValue()
    return(iif(this.value =1,'1',;
    iif(this.value =2,'2',;
    iif(this.value =3,'3',;
    ' '))))
  endfunc
  func oTOTM1006_2_34.GetRadio()
    this.Parent.oContained.w_TOTM1006 = this.RadioValue()
    return .t.
  endfunc

  func oTOTM1006_2_34.SetRadio()
    this.Parent.oContained.w_TOTM1006=trim(this.Parent.oContained.w_TOTM1006)
    this.value = ;
      iif(this.Parent.oContained.w_TOTM1006=='1',1,;
      iif(this.Parent.oContained.w_TOTM1006=='2',2,;
      iif(this.Parent.oContained.w_TOTM1006=='3',3,;
      0)))
  endfunc

  func oTOTM1006_2_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Var_Dati='S')
    endwith
   endif
  endfunc

  func oTOTM1006_2_34.mHide()
    with this.Parent.oContained
      return (.w_CHT1T005=0 OR (.w_CHPEREST='S' AND .w_ANFLSGRE='S') OR .w_CHATT1MOD='N' OR .w_CH__ANNO>=2016)
    endwith
  endfunc


  add object oTOTM1006_2016_2_35 as StdCombo with uid="BKROSJVIHO",value=1,rtseq=86,rtrep=.f.,left=509,top=313,width=306,height=21;
    , ToolTipText = "Codice somme non soggette";
    , HelpContextID = 7132764;
    , cFormVar="w_TOTM1006_2016",RowSource=""+"Nessuno,"+"1 - Compensi docenti/ricercatori Legge n. 2 del 28 gennaio 2009,"+"2 - Lavoratori con beneficio fiscale ex art. 3 Legge 30 dicembre 2010/238,"+"5 - Compensi soggetti art.16 del D.Igs. n. 147 del 2015,"+"6 - Erogazione di altri redditi non soggetti a ritenuta", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oTOTM1006_2016_2_35.RadioValue()
    return(iif(this.value =1,' ',;
    iif(this.value =2,'1',;
    iif(this.value =3,'2',;
    iif(this.value =4,'5',;
    iif(this.value =5,'6',;
    ' '))))))
  endfunc
  func oTOTM1006_2016_2_35.GetRadio()
    this.Parent.oContained.w_TOTM1006_2016 = this.RadioValue()
    return .t.
  endfunc

  func oTOTM1006_2016_2_35.SetRadio()
    this.Parent.oContained.w_TOTM1006_2016=trim(this.Parent.oContained.w_TOTM1006_2016)
    this.value = ;
      iif(this.Parent.oContained.w_TOTM1006_2016=='',1,;
      iif(this.Parent.oContained.w_TOTM1006_2016=='1',2,;
      iif(this.Parent.oContained.w_TOTM1006_2016=='2',3,;
      iif(this.Parent.oContained.w_TOTM1006_2016=='5',4,;
      iif(this.Parent.oContained.w_TOTM1006_2016=='6',5,;
      0)))))
  endfunc

  func oTOTM1006_2016_2_35.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Var_Dati='S')
    endwith
   endif
  endfunc

  func oTOTM1006_2016_2_35.mHide()
    with this.Parent.oContained
      return (.w_CHT1T005=0 OR (.w_CHPEREST='S' AND .w_ANFLSGRE='S') OR .w_CHATT1MOD='N' OR .w_CH__ANNO<>2016)
    endwith
  endfunc


  add object oTOTM1006_2017_2_36 as StdCombo with uid="OLYTVFWGJM",value=1,rtseq=87,rtrep=.f.,left=509,top=313,width=306,height=21;
    , ToolTipText = "Codice somme non soggette";
    , HelpContextID = 8181340;
    , cFormVar="w_TOTM1006_2017",RowSource=""+"Nessuno,"+"1 - Compensi docenti/ricercatori Legge n. 2 del 28 gennaio 2009,"+"2 - Lavoratori con beneficio fiscale ex art. 3 Legge 30 dicembre 2010/238,"+"5 - Compensi soggetti art.16 del D.Igs. n. 147 del 2015,"+"6 - Assegni di servizio civile,"+"7 - Erogazione di altri redditi non soggetti a ritenuta", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oTOTM1006_2017_2_36.RadioValue()
    return(iif(this.value =1,' ',;
    iif(this.value =2,'1',;
    iif(this.value =3,'2',;
    iif(this.value =4,'5',;
    iif(this.value =5,'6',;
    iif(this.value =6,'7',;
    ' ')))))))
  endfunc
  func oTOTM1006_2017_2_36.GetRadio()
    this.Parent.oContained.w_TOTM1006_2017 = this.RadioValue()
    return .t.
  endfunc

  func oTOTM1006_2017_2_36.SetRadio()
    this.Parent.oContained.w_TOTM1006_2017=trim(this.Parent.oContained.w_TOTM1006_2017)
    this.value = ;
      iif(this.Parent.oContained.w_TOTM1006_2017=='',1,;
      iif(this.Parent.oContained.w_TOTM1006_2017=='1',2,;
      iif(this.Parent.oContained.w_TOTM1006_2017=='2',3,;
      iif(this.Parent.oContained.w_TOTM1006_2017=='5',4,;
      iif(this.Parent.oContained.w_TOTM1006_2017=='6',5,;
      iif(this.Parent.oContained.w_TOTM1006_2017=='7',6,;
      0))))))
  endfunc

  func oTOTM1006_2017_2_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Var_Dati='S')
    endwith
   endif
  endfunc

  func oTOTM1006_2017_2_36.mHide()
    with this.Parent.oContained
      return (.w_CHT1T005=0 OR (.w_CHPEREST='S' AND .w_ANFLSGRE='S') OR .w_CHATT1MOD='N' OR .w_CH__ANNO<2017)
    endwith
  endfunc

  add object oCHT1T018_2_39 as StdField with uid="QVBNHDGJBP",rtseq=89,rtrep=.f.,;
    cFormVar = "w_CHT1T018", cQueryName = "CHT1T018",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Non � possibile inserire importi negativi",;
    ToolTipText = "Imponibile anni precedenti",;
    HelpContextID = 250386014,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=6, Top=364, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  func oCHT1T018_2_39.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CHATT1MOD='S' And .w_Var_Dati='S')
    endwith
   endif
  endfunc

  func oCHT1T018_2_39.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_Cht1t018>=0)
    endwith
    return bRes
  endfunc

  add object oCHT1T019_2_40 as StdField with uid="GXTMUFDLXU",rtseq=90,rtrep=.f.,;
    cFormVar = "w_CHT1T019", cQueryName = "CHT1T019",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Non � possibile inserire importi negativi",;
    ToolTipText = "Ritenute operate anni precedenti",;
    HelpContextID = 250386015,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=316, Top=364, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  func oCHT1T019_2_40.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CHATT1MOD='S' And .w_Var_Dati='S')
    endwith
   endif
  endfunc

  func oCHT1T019_2_40.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_Cht1t019>=0)
    endwith
    return bRes
  endfunc

  add object oCHT2T002_2_41 as StdField with uid="UGZNZFSAIR",rtseq=91,rtrep=.f.,;
    cFormVar = "w_CHT2T002", cQueryName = "CHT2T002",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Anno in cui � sorto il diritto alla percezione",;
    HelpContextID = 250451544,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=115, Top=448, cSayPict='"9999"', cGetPict='"9999"'

  func oCHT2T002_2_41.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Chatt2mod='S' And (Alltrim(.w_Chcaupre) $ 'G-H-I' Or .w_Cht2t003='S') And .w_Var_Dati='S')
    endwith
   endif
  endfunc

  add object oCHT2T003_2_42 as StdCheck with uid="DLRMRISLFN",rtseq=92,rtrep=.f.,left=192, top=447, caption="Anticipazione",;
    ToolTipText = "Anticipazione",;
    HelpContextID = 250451545,;
    cFormVar="w_CHT2T003", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oCHT2T003_2_42.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCHT2T003_2_42.GetRadio()
    this.Parent.oContained.w_CHT2T003 = this.RadioValue()
    return .t.
  endfunc

  func oCHT2T003_2_42.SetRadio()
    this.Parent.oContained.w_CHT2T003=trim(this.Parent.oContained.w_CHT2T003)
    this.value = ;
      iif(this.Parent.oContained.w_CHT2T003=='S',1,;
      0)
  endfunc

  func oCHT2T003_2_42.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CHATT2MOD='S' And .w_Var_Dati='S')
    endwith
   endif
  endfunc

  add object oCHT2T005_2_43 as StdField with uid="DZOKWIGIEX",rtseq=93,rtrep=.f.,;
    cFormVar = "w_CHT2T005", cQueryName = "CHT2T005",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Non � possibile inserire importi negativi ",;
    ToolTipText = "Somme non soggette",;
    HelpContextID = 250451547,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=316, Top=448, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  func oCHT2T005_2_43.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CHATT2MOD='S' And .w_Var_Dati='S')
    endwith
   endif
  endfunc

  func oCHT2T005_2_43.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_Cht2t005>=0)
    endwith
    return bRes
  endfunc


  add object oTOTM2006_2016_2_44 as StdCombo with uid="FGNXBCVHNV",value=1,rtseq=94,rtrep=.f.,left=509,top=447,width=306,height=21;
    , ToolTipText = "Codice somme non soggette";
    , HelpContextID = 8181340;
    , cFormVar="w_TOTM2006_2016",RowSource=""+"Nessuno,"+"1 - Compensi docenti/ricercatori Legge n. 2 del 28 gennaio 2009,"+"2 - Lavoratori con beneficio fiscale ex art. 3 Legge 30 dicembre 2010/238,"+"5 - Compensi soggetti art.16 del D.Igs. n. 147 del 2015,"+"6 - Erogazione di altri redditi non soggetti a ritenuta", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oTOTM2006_2016_2_44.RadioValue()
    return(iif(this.value =1,' ',;
    iif(this.value =2,'1',;
    iif(this.value =3,'2',;
    iif(this.value =4,'5',;
    iif(this.value =5,'6',;
    ' '))))))
  endfunc
  func oTOTM2006_2016_2_44.GetRadio()
    this.Parent.oContained.w_TOTM2006_2016 = this.RadioValue()
    return .t.
  endfunc

  func oTOTM2006_2016_2_44.SetRadio()
    this.Parent.oContained.w_TOTM2006_2016=trim(this.Parent.oContained.w_TOTM2006_2016)
    this.value = ;
      iif(this.Parent.oContained.w_TOTM2006_2016=='',1,;
      iif(this.Parent.oContained.w_TOTM2006_2016=='1',2,;
      iif(this.Parent.oContained.w_TOTM2006_2016=='2',3,;
      iif(this.Parent.oContained.w_TOTM2006_2016=='5',4,;
      iif(this.Parent.oContained.w_TOTM2006_2016=='6',5,;
      0)))))
  endfunc

  func oTOTM2006_2016_2_44.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Var_Dati='S')
    endwith
   endif
  endfunc

  func oTOTM2006_2016_2_44.mHide()
    with this.Parent.oContained
      return (.w_CHT2T005=0 OR (.w_CHPEREST='S' AND .w_ANFLSGRE='S') OR .w_CHATT2MOD='N' OR .w_CH__ANNO<>2016)
    endwith
  endfunc


  add object oTOTM2006_2_45 as StdCombo with uid="MAYCUXFCDH",rtseq=95,rtrep=.f.,left=509,top=447,width=306,height=21;
    , ToolTipText = "Codice somme non soggette";
    , HelpContextID = 216571500;
    , cFormVar="w_TOTM2006",RowSource=""+"1 - Compensi docenti/ricercatori,"+"2 - Lavoratori con beneficio fiscale ,"+"3 - Erogazione di altri redditi non sogg. a ritenuta", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oTOTM2006_2_45.RadioValue()
    return(iif(this.value =1,'1',;
    iif(this.value =2,'2',;
    iif(this.value =3,'3',;
    ' '))))
  endfunc
  func oTOTM2006_2_45.GetRadio()
    this.Parent.oContained.w_TOTM2006 = this.RadioValue()
    return .t.
  endfunc

  func oTOTM2006_2_45.SetRadio()
    this.Parent.oContained.w_TOTM2006=trim(this.Parent.oContained.w_TOTM2006)
    this.value = ;
      iif(this.Parent.oContained.w_TOTM2006=='1',1,;
      iif(this.Parent.oContained.w_TOTM2006=='2',2,;
      iif(this.Parent.oContained.w_TOTM2006=='3',3,;
      0)))
  endfunc

  func oTOTM2006_2_45.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Var_Dati='S')
    endwith
   endif
  endfunc

  func oTOTM2006_2_45.mHide()
    with this.Parent.oContained
      return (.w_CHT2T005=0 OR (.w_CHPEREST='S' AND .w_ANFLSGRE='S') OR .w_CHATT2MOD='N' OR .w_CH__ANNO>=2016)
    endwith
  endfunc


  add object oTOTM2006_2017_2_46 as StdCombo with uid="KVXFMAQQRK",value=1,rtseq=96,rtrep=.f.,left=509,top=447,width=306,height=21;
    , ToolTipText = "Codice somme non soggette";
    , HelpContextID = 9229916;
    , cFormVar="w_TOTM2006_2017",RowSource=""+"Nessuno,"+"1 - Compensi docenti/ricercatori Legge n. 2 del 28 gennaio 2009,"+"2 - Lavoratori con beneficio fiscale ex art. 3 Legge 30 dicembre 2010/238,"+"5 - Compensi soggetti art.16 del D.Igs. n. 147 del 2015,"+"6 - Assegni di servizio civile,"+"7 - Erogazione di altri redditi non soggetti a ritenuta", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oTOTM2006_2017_2_46.RadioValue()
    return(iif(this.value =1,' ',;
    iif(this.value =2,'1',;
    iif(this.value =3,'2',;
    iif(this.value =4,'5',;
    iif(this.value =5,'6',;
    iif(this.value =6,'7',;
    ' ')))))))
  endfunc
  func oTOTM2006_2017_2_46.GetRadio()
    this.Parent.oContained.w_TOTM2006_2017 = this.RadioValue()
    return .t.
  endfunc

  func oTOTM2006_2017_2_46.SetRadio()
    this.Parent.oContained.w_TOTM2006_2017=trim(this.Parent.oContained.w_TOTM2006_2017)
    this.value = ;
      iif(this.Parent.oContained.w_TOTM2006_2017=='',1,;
      iif(this.Parent.oContained.w_TOTM2006_2017=='1',2,;
      iif(this.Parent.oContained.w_TOTM2006_2017=='2',3,;
      iif(this.Parent.oContained.w_TOTM2006_2017=='5',4,;
      iif(this.Parent.oContained.w_TOTM2006_2017=='6',5,;
      iif(this.Parent.oContained.w_TOTM2006_2017=='7',6,;
      0))))))
  endfunc

  func oTOTM2006_2017_2_46.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Var_Dati='S')
    endwith
   endif
  endfunc

  func oTOTM2006_2017_2_46.mHide()
    with this.Parent.oContained
      return (.w_CHT2T005=0 OR (.w_CHPEREST='S' AND .w_ANFLSGRE='S') OR .w_CHATT2MOD='N' OR .w_CH__ANNO<2017)
    endwith
  endfunc

  add object oCHT2T018_2_49 as StdField with uid="VXYDIKWJPM",rtseq=98,rtrep=.f.,;
    cFormVar = "w_CHT2T018", cQueryName = "CHT2T018",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    HelpContextID = 250451550,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=6, Top=493, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  func oCHT2T018_2_49.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CHATT2MOD='S' And .w_Var_Dati='S')
    endwith
   endif
  endfunc

  func oCHT2T018_2_49.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_Cht2t018>=0)
    endwith
    return bRes
  endfunc

  add object oCHT2T019_2_50 as StdField with uid="YWUPPSAEVB",rtseq=99,rtrep=.f.,;
    cFormVar = "w_CHT2T019", cQueryName = "CHT2T019",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Non � possibile inserire importi negativi",;
    HelpContextID = 250451551,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=316, Top=493, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  func oCHT2T019_2_50.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CHATT2MOD='S' And .w_Var_Dati='S')
    endwith
   endif
  endfunc

  func oCHT2T019_2_50.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_Cht2t019>=0)
    endwith
    return bRes
  endfunc

  add object oCHATT1MOD_2_51 as StdCheck with uid="HVCWSFLDPN",rtseq=100,rtrep=.f.,left=6, top=265, caption="Attivazione modulo",;
    ToolTipText = "Se attivo, consente di inserire un nuovo modulo",;
    HelpContextID = 267490635,;
    cFormVar="w_CHATT1MOD", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oCHATT1MOD_2_51.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCHATT1MOD_2_51.GetRadio()
    this.Parent.oContained.w_CHATT1MOD = this.RadioValue()
    return .t.
  endfunc

  func oCHATT1MOD_2_51.SetRadio()
    this.Parent.oContained.w_CHATT1MOD=trim(this.Parent.oContained.w_CHATT1MOD)
    this.value = ;
      iif(this.Parent.oContained.w_CHATT1MOD=='S',1,;
      0)
  endfunc

  func oCHATT1MOD_2_51.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Var_Dati='S')
    endwith
   endif
  endfunc

  add object oCHPROMOD_2_55 as StdField with uid="JRXWPGQGZP",rtseq=103,rtrep=.f.,;
    cFormVar = "w_CHPROMOD", cQueryName = "CHPROMOD",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero modello ",;
    HelpContextID = 196957802,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=6, Top=84, cSayPict='"99999999"', cGetPict='"99999999"'

  add object oCHPR1MOD_2_57 as StdField with uid="PFCOMNVOZQ",rtseq=104,rtrep=.f.,;
    cFormVar = "w_CHPR1MOD", cQueryName = "CHPR1MOD",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero modello",;
    HelpContextID = 165500522,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=6, Top=314, cSayPict='"99999999"', cGetPict='"99999999"'

  add object oCHPR2MOD_2_59 as StdField with uid="XMODLKAVWX",rtseq=105,rtrep=.f.,;
    cFormVar = "w_CHPR2MOD", cQueryName = "CHPR2MOD",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero modello",;
    HelpContextID = 166549098,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=6, Top=448, cSayPict='"99999999"', cGetPict='"99999999"'

  add object oGDPROCER_2_61 as StdField with uid="TGLIIKBLPY",rtseq=108,rtrep=.f.,;
    cFormVar = "w_GDPROCER", cQueryName = "GDPROCER",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    HelpContextID = 239250760,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=185, Top=36, cSayPict='"99999"', cGetPict='"99999"'

  func oGDPROCER_2_61.mHide()
    with this.Parent.oContained
      return (Empty(.w_Gdserial))
    endwith
  endfunc


  add object oLinkPC_2_62 as StdButton with uid="RDHGPOSXSW",left=807, top=604, width=48,height=45,;
    CpPicture="bmp\wf_conferma.bmp", caption="", nPag=2;
    , ToolTipText = "Premere per accedere ai dati previdenziali";
    , HelpContextID = 195814651;
    , TabStop=.f., Caption='\<Dati prev.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oLinkPC_2_62.Click()
      this.Parent.oContained.GSRI_MPR.LinkPCClick()
      this.Parent.oContained.WriteTo_GSRI_MPR()
    endproc

  func oLinkPC_2_62.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_Var_Dati='S' Or .cFunction='Query')
      endwith
    endif
  endfunc


  add object oBtn_2_71 as StdButton with uid="JUNDBKOVZE",left=807, top=84, width=48,height=45,;
    CpPicture="bmp\wf_nuovo.bmp", caption="", nPag=2;
    , ToolTipText = "Premere per inserire gli altri dati fiscali";
    , HelpContextID = 36920711;
    , tabstop=.f., caption='\<Dati fisc.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_71.Click()
      do GSRI_KCH with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_71.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_Var_Dati='S' Or .cFunction='Query')
      endwith
    endif
  endfunc

  add object oCHATT2MOD_2_78 as StdCheck with uid="QINGLYELHK",rtseq=123,rtrep=.f.,left=6, top=399, caption="Attivazione modulo",;
    ToolTipText = "Se attivo, consente di inserire un nuovo modulo",;
    HelpContextID = 250713419,;
    cFormVar="w_CHATT2MOD", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oCHATT2MOD_2_78.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCHATT2MOD_2_78.GetRadio()
    this.Parent.oContained.w_CHATT2MOD = this.RadioValue()
    return .t.
  endfunc

  func oCHATT2MOD_2_78.SetRadio()
    this.Parent.oContained.w_CHATT2MOD=trim(this.Parent.oContained.w_CHATT2MOD)
    this.value = ;
      iif(this.Parent.oContained.w_CHATT2MOD=='S',1,;
      0)
  endfunc

  func oCHATT2MOD_2_78.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Var_Dati='S')
    endwith
   endif
  endfunc

  add object oVAR_DATI_2_84 as StdCheck with uid="LJYZSETNAP",rtseq=125,rtrep=.f.,left=687, top=21, caption="Variazione dati",;
    ToolTipText = "Se attivo, sar� possibile modificare i dati relativi al record H",;
    HelpContextID = 253391007,;
    cFormVar="w_VAR_DATI", bObbl = .f. , nPag = 2;
    , tabstop=.f.;
   , bGlobalFont=.t.


  func oVAR_DATI_2_84.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oVAR_DATI_2_84.GetRadio()
    this.Parent.oContained.w_VAR_DATI = this.RadioValue()
    return .t.
  endfunc

  func oVAR_DATI_2_84.SetRadio()
    this.Parent.oContained.w_VAR_DATI=trim(this.Parent.oContained.w_VAR_DATI)
    this.value = ;
      iif(this.Parent.oContained.w_VAR_DATI=='S',1,;
      0)
  endfunc

  add object oCHATT3MOD_2_88 as StdCheck with uid="ISICETTLPO",rtseq=127,rtrep=.f.,left=6, top=527, caption="Attivazione modulo",;
    ToolTipText = "Se attivo, consente di inserire un nuovo modulo",;
    HelpContextID = 233936203,;
    cFormVar="w_CHATT3MOD", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oCHATT3MOD_2_88.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCHATT3MOD_2_88.GetRadio()
    this.Parent.oContained.w_CHATT3MOD = this.RadioValue()
    return .t.
  endfunc

  func oCHATT3MOD_2_88.SetRadio()
    this.Parent.oContained.w_CHATT3MOD=trim(this.Parent.oContained.w_CHATT3MOD)
    this.value = ;
      iif(this.Parent.oContained.w_CHATT3MOD=='S',1,;
      0)
  endfunc

  func oCHATT3MOD_2_88.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Var_Dati='S' And .w_CH__ANNO>=2016)
    endwith
   endif
  endfunc

  add object oCHT3T002_2_90 as StdField with uid="RJFLIKDMQH",rtseq=128,rtrep=.f.,;
    cFormVar = "w_CHT3T002", cQueryName = "CHT3T002",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Anno in cui � sorto il diritto alla percezione",;
    HelpContextID = 250517080,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=112, Top=573, cSayPict='"9999"', cGetPict='"9999"'

  func oCHT3T002_2_90.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Chatt3mod='S' And (Alltrim(.w_Chcaupre) $ 'G-H-I' Or .w_Cht3t003='S') And .w_Var_Dati='S')
    endwith
   endif
  endfunc

  add object oCHT3T003_2_91 as StdCheck with uid="BBWBLFDADX",rtseq=129,rtrep=.f.,left=189, top=572, caption="Anticipazione",;
    ToolTipText = "Anticipazione",;
    HelpContextID = 250517081,;
    cFormVar="w_CHT3T003", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oCHT3T003_2_91.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCHT3T003_2_91.GetRadio()
    this.Parent.oContained.w_CHT3T003 = this.RadioValue()
    return .t.
  endfunc

  func oCHT3T003_2_91.SetRadio()
    this.Parent.oContained.w_CHT3T003=trim(this.Parent.oContained.w_CHT3T003)
    this.value = ;
      iif(this.Parent.oContained.w_CHT3T003=='S',1,;
      0)
  endfunc

  func oCHT3T003_2_91.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CHATT3MOD='S' And .w_Var_Dati='S')
    endwith
   endif
  endfunc

  add object oCHT3T005_2_92 as StdField with uid="TBSLDIQWPG",rtseq=130,rtrep=.f.,;
    cFormVar = "w_CHT3T005", cQueryName = "CHT3T005",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Non � possibile inserire importi negativi ",;
    ToolTipText = "Somme non soggette",;
    HelpContextID = 250517083,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=313, Top=573, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  func oCHT3T005_2_92.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CHATT3MOD='S' And .w_Var_Dati='S')
    endwith
   endif
  endfunc

  func oCHT3T005_2_92.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_Cht3t005>=0)
    endwith
    return bRes
  endfunc


  add object oTOTM3006_2016_2_93 as StdCombo with uid="MFQXPEBUSY",value=1,rtseq=131,rtrep=.f.,left=508,top=575,width=306,height=21;
    , ToolTipText = "Codice somme non soggette";
    , HelpContextID = 9229916;
    , cFormVar="w_TOTM3006_2016",RowSource=""+"Nessuno,"+"1 - Compensi docenti/ricercatori Legge n. 2 del 28 gennaio 2009,"+"2 - Lavoratori con beneficio fiscale ex art. 3 Legge 30 dicembre 2010/238,"+"5 - Compensi soggetti art.16 del D.Igs. n. 147 del 2015,"+"6 - Erogazione di altri redditi non soggetti a ritenuta", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oTOTM3006_2016_2_93.RadioValue()
    return(iif(this.value =1,' ',;
    iif(this.value =2,'1',;
    iif(this.value =3,'2',;
    iif(this.value =4,'5',;
    iif(this.value =5,'6',;
    ' '))))))
  endfunc
  func oTOTM3006_2016_2_93.GetRadio()
    this.Parent.oContained.w_TOTM3006_2016 = this.RadioValue()
    return .t.
  endfunc

  func oTOTM3006_2016_2_93.SetRadio()
    this.Parent.oContained.w_TOTM3006_2016=trim(this.Parent.oContained.w_TOTM3006_2016)
    this.value = ;
      iif(this.Parent.oContained.w_TOTM3006_2016=='',1,;
      iif(this.Parent.oContained.w_TOTM3006_2016=='1',2,;
      iif(this.Parent.oContained.w_TOTM3006_2016=='2',3,;
      iif(this.Parent.oContained.w_TOTM3006_2016=='5',4,;
      iif(this.Parent.oContained.w_TOTM3006_2016=='6',5,;
      0)))))
  endfunc

  func oTOTM3006_2016_2_93.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Var_Dati='S')
    endwith
   endif
  endfunc

  func oTOTM3006_2016_2_93.mHide()
    with this.Parent.oContained
      return (.w_CHT3T005=0 OR (.w_CHPEREST='S' AND .w_ANFLSGRE='S') OR .w_CHATT3MOD='N' OR .w_CH__ANNO<>2016)
    endwith
  endfunc


  add object oTOTM3006_2017_2_94 as StdCombo with uid="USLIUFMCBD",value=1,rtseq=132,rtrep=.f.,left=508,top=575,width=306,height=21;
    , ToolTipText = "Codice somme non soggette";
    , HelpContextID = 10278492;
    , cFormVar="w_TOTM3006_2017",RowSource=""+"Nessuno,"+"1 - Compensi docenti/ricercatori Legge n. 2 del 28 gennaio 2009,"+"2 - Lavoratori con beneficio fiscale ex art. 3 Legge 30 dicembre 2010/238,"+"5 - Compensi soggetti art.16 del D.Igs. n. 147 del 2015,"+"6 - Assegni di servizio civile,"+"7 - Erogazione di altri redditi non soggetti a ritenuta", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oTOTM3006_2017_2_94.RadioValue()
    return(iif(this.value =1,' ',;
    iif(this.value =2,'1',;
    iif(this.value =3,'2',;
    iif(this.value =4,'5',;
    iif(this.value =5,'6',;
    iif(this.value =6,'7',;
    ' ')))))))
  endfunc
  func oTOTM3006_2017_2_94.GetRadio()
    this.Parent.oContained.w_TOTM3006_2017 = this.RadioValue()
    return .t.
  endfunc

  func oTOTM3006_2017_2_94.SetRadio()
    this.Parent.oContained.w_TOTM3006_2017=trim(this.Parent.oContained.w_TOTM3006_2017)
    this.value = ;
      iif(this.Parent.oContained.w_TOTM3006_2017=='',1,;
      iif(this.Parent.oContained.w_TOTM3006_2017=='1',2,;
      iif(this.Parent.oContained.w_TOTM3006_2017=='2',3,;
      iif(this.Parent.oContained.w_TOTM3006_2017=='5',4,;
      iif(this.Parent.oContained.w_TOTM3006_2017=='6',5,;
      iif(this.Parent.oContained.w_TOTM3006_2017=='7',6,;
      0))))))
  endfunc

  func oTOTM3006_2017_2_94.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Var_Dati='S')
    endwith
   endif
  endfunc

  func oTOTM3006_2017_2_94.mHide()
    with this.Parent.oContained
      return (.w_CHT3T005=0 OR (.w_CHPEREST='S' AND .w_ANFLSGRE='S') OR .w_CHATT3MOD='N' OR .w_CH__ANNO<2017)
    endwith
  endfunc

  add object oCHT3T018_2_97 as StdField with uid="BERJEGEURF",rtseq=134,rtrep=.f.,;
    cFormVar = "w_CHT3T018", cQueryName = "CHT3T018",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    HelpContextID = 250517086,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=3, Top=618, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  func oCHT3T018_2_97.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CHATT3MOD='S' And .w_Var_Dati='S')
    endwith
   endif
  endfunc

  func oCHT3T018_2_97.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_Cht3t018>=0)
    endwith
    return bRes
  endfunc

  add object oCHT3T019_2_98 as StdField with uid="EMAWTUNHZE",rtseq=135,rtrep=.f.,;
    cFormVar = "w_CHT3T019", cQueryName = "CHT3T019",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Non � possibile inserire importi negativi",;
    HelpContextID = 250517087,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=313, Top=618, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  func oCHT3T019_2_98.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CHATT3MOD='S' And .w_Var_Dati='S')
    endwith
   endif
  endfunc

  func oCHT3T019_2_98.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_Cht3t019>=0)
    endwith
    return bRes
  endfunc

  add object oCHPR3MOD_2_100 as StdField with uid="VZCLYRLPWZ",rtseq=136,rtrep=.f.,;
    cFormVar = "w_CHPR3MOD", cQueryName = "CHPR3MOD",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero modello",;
    HelpContextID = 167597674,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=3, Top=573, cSayPict='"99999999"', cGetPict='"99999999"'

  add object oStr_2_1 as StdString with uid="YYMJTWBDUY",Visible=.t., Left=316, Top=63,;
    Alignment=0, Width=175, Height=18,;
    Caption="Ammontare lordo corrisposto"  ;
  , bGlobalFont=.t.

  add object oStr_2_2 as StdString with uid="FOWBOZRHVU",Visible=.t., Left=5, Top=10,;
    Alignment=0, Width=306, Height=19,;
    Caption="Dati relativi alle somme erogate"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_3 as StdString with uid="UGJJMMROVZ",Visible=.t., Left=512, Top=63,;
    Alignment=0, Width=160, Height=18,;
    Caption="Somme non soggette"  ;
  , bGlobalFont=.t.

  add object oStr_2_4 as StdString with uid="YHCFGDMUZO",Visible=.t., Left=6, Top=111,;
    Alignment=0, Width=160, Height=18,;
    Caption="Codice"  ;
  , bGlobalFont=.t.

  func oStr_2_4.mHide()
    with this.Parent.oContained
      return (.w_CHTOT005=0 OR (.w_CHPEREST='S' AND .w_ANFLSGRE='S'))
    endwith
  endfunc

  add object oStr_2_5 as StdString with uid="WZHQHWDYEB",Visible=.t., Left=316, Top=111,;
    Alignment=0, Width=175, Height=18,;
    Caption="Imponibile"  ;
  , bGlobalFont=.t.

  add object oStr_2_6 as StdString with uid="MNYJGJHCOV",Visible=.t., Left=512, Top=111,;
    Alignment=0, Width=175, Height=18,;
    Caption="Ritenute a titolo di acconto"  ;
  , bGlobalFont=.t.

  add object oStr_2_7 as StdString with uid="OFYQZUXHVR",Visible=.t., Left=512, Top=158,;
    Alignment=0, Width=141, Height=18,;
    Caption="Spese rimborsate"  ;
  , bGlobalFont=.t.

  add object oStr_2_8 as StdString with uid="KHDZAPVLVZ",Visible=.t., Left=665, Top=158,;
    Alignment=0, Width=173, Height=18,;
    Caption="Ritenute rimborsate"  ;
  , bGlobalFont=.t.

  add object oStr_2_9 as StdString with uid="RIBJBHKEMH",Visible=.t., Left=316, Top=292,;
    Alignment=0, Width=160, Height=18,;
    Caption="Somme non soggette"  ;
  , bGlobalFont=.t.

  add object oStr_2_10 as StdString with uid="UZCWPBGEEB",Visible=.t., Left=512, Top=294,;
    Alignment=0, Width=160, Height=18,;
    Caption="Codice"  ;
  , bGlobalFont=.t.

  func oStr_2_10.mHide()
    with this.Parent.oContained
      return (.w_CHT1T005=0 OR (.w_CHPEREST='S' AND .w_ANFLSGRE='S') OR .w_CHATT1MOD='N')
    endwith
  endfunc

  add object oStr_2_11 as StdString with uid="TGHDQRRLZT",Visible=.t., Left=316, Top=427,;
    Alignment=0, Width=160, Height=18,;
    Caption="Somme non soggette"  ;
  , bGlobalFont=.t.

  add object oStr_2_12 as StdString with uid="EVQUFBXCQO",Visible=.t., Left=512, Top=427,;
    Alignment=0, Width=160, Height=18,;
    Caption="Codice"  ;
  , bGlobalFont=.t.

  func oStr_2_12.mHide()
    with this.Parent.oContained
      return (.w_CHT2T005=0 OR (.w_CHPEREST='S' AND .w_ANFLSGRE='S') OR .w_CHATT2MOD='N')
    endwith
  endfunc

  add object oStr_2_54 as StdString with uid="ZYYVOHSWFW",Visible=.t., Left=6, Top=61,;
    Alignment=0, Width=48, Height=19,;
    Caption="Mod. N."  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_56 as StdString with uid="KVSOGOEGEP",Visible=.t., Left=8, Top=291,;
    Alignment=0, Width=48, Height=19,;
    Caption="Mod. N."  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_58 as StdString with uid="VYXJBFPLKD",Visible=.t., Left=8, Top=426,;
    Alignment=0, Width=48, Height=19,;
    Caption="Mod. N."  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_60 as StdString with uid="FLEFQMJWLD",Visible=.t., Left=7, Top=38,;
    Alignment=1, Width=173, Height=18,;
    Caption="Progressivo Certificazione:"  ;
  , bGlobalFont=.t.

  func oStr_2_60.mHide()
    with this.Parent.oContained
      return (Empty(.w_Gdserial))
    endwith
  endfunc

  add object oStr_2_72 as StdString with uid="CGRJPBYGYW",Visible=.t., Left=105, Top=63,;
    Alignment=0, Width=28, Height=18,;
    Caption="Anno"  ;
  , bGlobalFont=.t.

  add object oStr_2_73 as StdString with uid="NJIXPCCCXB",Visible=.t., Left=6, Top=156,;
    Alignment=0, Width=166, Height=18,;
    Caption="Imponibile anni precedenti"  ;
  , bGlobalFont=.t.

  add object oStr_2_74 as StdString with uid="FCJUOADUBA",Visible=.t., Left=316, Top=156,;
    Alignment=0, Width=191, Height=18,;
    Caption="Ritenute operate anni precedenti"  ;
  , bGlobalFont=.t.

  add object oStr_2_75 as StdString with uid="TAQDRKTVEM",Visible=.t., Left=119, Top=292,;
    Alignment=0, Width=28, Height=18,;
    Caption="Anno"  ;
  , bGlobalFont=.t.

  add object oStr_2_76 as StdString with uid="PZWYUAFZLR",Visible=.t., Left=6, Top=342,;
    Alignment=0, Width=166, Height=18,;
    Caption="Imponibile anni precedenti"  ;
  , bGlobalFont=.t.

  add object oStr_2_77 as StdString with uid="PBZVARNUDT",Visible=.t., Left=316, Top=342,;
    Alignment=0, Width=191, Height=18,;
    Caption="Ritenute operate anni precedenti"  ;
  , bGlobalFont=.t.

  add object oStr_2_79 as StdString with uid="WDNDBTOWVT",Visible=.t., Left=119, Top=427,;
    Alignment=0, Width=28, Height=18,;
    Caption="Anno"  ;
  , bGlobalFont=.t.

  add object oStr_2_80 as StdString with uid="KQDLBZVTSR",Visible=.t., Left=6, Top=472,;
    Alignment=0, Width=166, Height=18,;
    Caption="Imponibile anni precedenti"  ;
  , bGlobalFont=.t.

  add object oStr_2_81 as StdString with uid="DROYBXZWRO",Visible=.t., Left=316, Top=472,;
    Alignment=0, Width=191, Height=18,;
    Caption="Ritenute operate anni precedenti"  ;
  , bGlobalFont=.t.

  add object oStr_2_85 as StdString with uid="MSNYLTYOME",Visible=.t., Left=6, Top=204,;
    Alignment=0, Width=279, Height=18,;
    Caption="Somme corrisposte prima della data del fallimento"  ;
  , bGlobalFont=.t.

  add object oStr_2_86 as StdString with uid="RBZPYFEOCE",Visible=.t., Left=316, Top=204,;
    Alignment=0, Width=279, Height=18,;
    Caption="Somme corrisposte dal curatore/commissario"  ;
  , bGlobalFont=.t.

  add object oStr_2_87 as StdString with uid="DCHIATNUXT",Visible=.t., Left=313, Top=552,;
    Alignment=0, Width=160, Height=18,;
    Caption="Somme non soggette"  ;
  , bGlobalFont=.t.

  add object oStr_2_89 as StdString with uid="GEIAPYRQYL",Visible=.t., Left=512, Top=552,;
    Alignment=0, Width=160, Height=18,;
    Caption="Codice"  ;
  , bGlobalFont=.t.

  func oStr_2_89.mHide()
    with this.Parent.oContained
      return (.w_CHT3T005=0 OR (.w_CHPEREST='S' AND .w_ANFLSGRE='S') OR .w_CHATT3MOD='N')
    endwith
  endfunc

  add object oStr_2_99 as StdString with uid="IJOOOXRVVX",Visible=.t., Left=8, Top=551,;
    Alignment=0, Width=48, Height=19,;
    Caption="Mod. N."  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_101 as StdString with uid="ZCQRHEVRMB",Visible=.t., Left=116, Top=552,;
    Alignment=0, Width=28, Height=18,;
    Caption="Anno"  ;
  , bGlobalFont=.t.

  add object oStr_2_102 as StdString with uid="SXVREAPDLH",Visible=.t., Left=3, Top=597,;
    Alignment=0, Width=166, Height=18,;
    Caption="Imponibile anni precedenti"  ;
  , bGlobalFont=.t.

  add object oStr_2_103 as StdString with uid="CZVWEPJGSE",Visible=.t., Left=313, Top=597,;
    Alignment=0, Width=191, Height=18,;
    Caption="Ritenute operate anni precedenti"  ;
  , bGlobalFont=.t.

  add object oStr_2_106 as StdString with uid="FPMSMIJDYZ",Visible=.t., Left=665, Top=204,;
    Alignment=0, Width=241, Height=18,;
    Caption="Codice fiscale operaz. straordinarie"  ;
  , bGlobalFont=.t.

  add object oBox_2_82 as StdBox with uid="HESDYCNOIB",left=1, top=258, width=859,height=1

  add object oBox_2_83 as StdBox with uid="GBBMEPVXTS",left=-1, top=391, width=861,height=1

  add object oBox_2_104 as StdBox with uid="LAFLKCOAHK",left=-4, top=520, width=865,height=1
enddefine
define class tgsri_achPag3 as StdContainer
  Width  = 861
  height = 653
  stdWidth  = 861
  stdheight = 653
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_3_1 as stdDynamicChildContainer with uid="IZGAQGXMLI",left=-2, top=3, width=850, height=556, bOnScreen=.t.;

  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gsri_mch",lower(this.oContained.GSRI_MCH.class))=0
        this.oContained.GSRI_MCH.createrealchild()
        this.oContained.WriteTo_GSRI_MCH()
      endif
      this.createchildren=.f.
    endif
  endproc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsri_ach','CUDTETRH','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".CHSERIAL=CUDTETRH.CHSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
