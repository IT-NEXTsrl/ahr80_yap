* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_bft                                                        *
*              Gen. file telematico 770                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][98][VRS_735]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-06-11                                                      *
* Last revis.: 2011-02-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsri_bft",oParentObject)
return(i_retval)

define class tgsri_bft as StdBatch
  * --- Local variables
  w_INDIRIZZOEMAIL = space(100)
  FileTelematico = .NULL.
  w_nMod1 = 0
  w_ContaRec = 0
  CauPre = space(1)
  w_NUMCERTIF = 0
  w_CALCOLI = 0
  w_FLAGEUR = space(1)
  w_nMod2 = 0
  w_CFAzi = space(16)
  w_nRECRITE1 = 0
  w_nRECRITE3 = 0
  w_nMod3 = 0
  w_DataAppo = space(8)
  w_nRECRITE2 = 0
  w_OPERAZ = space(1)
  w_SOSPENDI = .f.
  w_OLDPERC = space(16)
  w_PRIMOPERC = .f.
  w_ANNO = space(4)
  w_PERCIN = space(15)
  w_CODVAL = space(3)
  w_DATFIN = ctod("  /  /  ")
  w_DECIMI = space(1)
  w_PERCFIN = space(15)
  w_DATINI = ctod("  /  /  ")
  w_VALUTA = space(1)
  w_NumPerSC = 0
  w_NumPerSE = 0
  w_tSOMTOTASC = 0
  w_tSOMTOTASE = 0
  w_tNONSOGGSC = 0
  w_tIMPONISE = 0
  w_tIMPONISC = 0
  w_tMRTOTIM1SE = 0
  w_tMRTOTIM1SC = 0
  * --- WorkFile variables
  AZIENDA_idx=0
  VEP_RITE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Generazione File Telematico 770/2002 (da GSRI_SFT)
    * --- Le dichiarazione del caller sono a Pag.8
    * --- Controllo che le date di selezione siano all'interno dello stesso anno
    if STR(YEAR(this.oParentObject.w_DATFIN),4,0)<>STR(YEAR(this.oParentObject.w_DATINI),4,0)
      this.w_MESS = "L'intervallo di date deve essere interno ad un anno solare"
      ah_ErrorMsg(this.w_MESS,"","")
      i_retcode = 'stop'
      return
    endif
    * --- Controllo sui valori obbligatori e bloccanti
    if this.oParentObject.w_PERAZI<>"S"
      if not empty(this.oParentObject.w_SESIGLA)
        if empty(this.oParentObject.w_SEINDIRI2)
          ah_ErrorMsg("Manca indirizzo della sede legale","!","")
          i_retcode = 'stop'
          return
        endif
      endif
      if not empty(this.oParentObject.w_SERSIGLA) or not empty(this.oParentObject.w_SERINDIR) or not empty(this.oParentObject.w_SERCAP)
        if empty(this.oParentObject.w_SERCOMUN)
          ah_ErrorMsg("Manca comune del domicilio fiscale","!","")
          i_retcode = 'stop'
          return
        endif
      endif
      if not empty(this.oParentObject.w_SERCOMUN) or not empty(this.oParentObject.w_SERINDIR) or not empty(this.oParentObject.w_SERCAP)
        if empty(this.oParentObject.w_SERSIGLA)
          ah_ErrorMsg("Manca provincia del domicilio fiscale","!","")
          i_retcode = 'stop'
          return
        endif
      endif
      if not empty(this.oParentObject.w_SERCOMUN) or not empty(this.oParentObject.w_SERSIGLA) or not empty(this.oParentObject.w_SERCAP)
        if empty(this.oParentObject.w_SERINDIR)
          ah_ErrorMsg("Manca indirizzo del domicilio fiscale","!","")
          i_retcode = 'stop'
          return
        endif
      endif
    endif
    if this.oParentObject.w_PERAZI<>"S"
      if empty(this.oParentObject.w_RFCODFIS) OR empty(this.oParentObject.w_RFCOGNOME) OR empty(this.oParentObject.w_RFNOME) OR empty(this.oParentObject.w_RFSESSO) OR empty(this.oParentObject.w_RFDATANASC) OR empty(this.oParentObject.w_RFCOMNAS)
        ah_ErrorMsg("Mancano alcuni dati relativi al rappresentante firmatario della dichiarazione","!","")
        i_retcode = 'stop'
        return
      endif
      if (not empty(this.oParentObject.w_RFSIGLA)) AND (this.oParentObject.w_RFSIGLA<>"EE")
        if empty(this.oParentObject.w_RFCOMUNE) OR empty(this.oParentObject.w_RFINDIRIZ)
          ah_ErrorMsg("Mancano alcuni dati relativi alla residenza del rappresentante firmatario","!","")
          i_retcode = 'stop'
          return
        endif
      endif
    endif
    if this.oParentObject.w_PERAZI="S"
      if not empty(this.oParentObject.w_RESIGLA) and (this.oParentObject.w_RESIGLA<>"EE")
        if empty(this.oParentObject.w_INDIRIZ)
          ah_ErrorMsg("Manca l'indirizzo del domicilio fiscale del dichiarante","!","")
          i_retcode = 'stop'
          return
        endif
      endif
    endif
    * --- Controllo dati relativi all'impegno  presentazione telematica
    if not empty(this.oParentObject.w_NUMCAF) or (this.oParentObject.w_IMPTRTEL<>"0") or (this.oParentObject.w_IMPTRSOG<>"0") or (this.oParentObject.w_FIRMINT<>"0") or not empty(this.oParentObject.w_RFDATIMP) ;
      or not empty(this.oParentObject.w_CODFISIN)
      if empty(this.oParentObject.w_RFDATIMP) OR empty(this.oParentObject.w_CODFISIN)
        ah_ErrorMsg("Mancano dati relativi al C.A.F. o alla data impegno trasmissione","!","")
        i_retcode = 'stop'
        return
      endif
      if ((this.oParentObject.w_IMPTRTEL="0") and (this.oParentObject.w_IMPTRSOG="0")) or ((this.oParentObject.w_IMPTRTEL="1") and (this.oParentObject.w_IMPTRSOG="1"))
        ah_ErrorMsg("Manca impegno a trasmettere o non sono state selezionate modalit� alternative","!","")
        i_retcode = 'stop'
        return
      endif
    endif
    * --- Controllo formato numerico dei numeri telefonici
    if not empty(this.oParentObject.w_TELEFONO)
      for lettera = 65 to 90
      if chr(lettera) $ this.oParentObject.w_TELEFONO
        ah_ErrorMsg("Numero di telefono incongruente!","!","")
        i_retcode = 'stop'
        return
      endif
      endfor
    endif
    if not empty(this.oParentObject.w_TELEFAX)
      for lettera = 65 to 90
      if chr(lettera) $ this.oParentObject.w_TELEFAX
        ah_ErrorMsg("Numero di FAX incongruente!","!","")
        i_retcode = 'stop'
        return
      endif
      endfor
    endif
    if not empty(this.oParentObject.w_RFTELEFONO)
      for lettera = 65 to 90
      if chr(lettera) $ this.oParentObject.w_RFTELEFONO
        ah_ErrorMsg("Numero di telefono relativo al rappresentante firmatario errato!","!","")
        i_retcode = 'stop'
        return
      endif
      endfor
    endif
    * --- Fine controllo valori obbligatori e bloccanti
    * --- Questo giro lo faccio per ingannare i Batches standard che lancio successivamente
    this.w_ANNO = this.oParentObject.w_ANNO
    this.w_PERCIN = this.oParentObject.w_PERCIN
    this.w_CODVAL = this.oParentObject.w_CODVAL
    this.w_DATFIN = this.oParentObject.w_DATFIN
    this.w_DECIMI = this.oParentObject.w_DECIMI
    this.w_PERCFIN = this.oParentObject.w_PERCFIN
    this.w_DATINI = this.oParentObject.w_DATINI
    this.w_VALUTA = this.oParentObject.w_VALUTA
    * --- Codice Fiscale dell'Azienda Corrente
    this.w_CFAzi = this.oParentObject.w_FOCODFIS
    this.w_OPERAZ = this.oParentObject.w_TIPOPERAZ
    this.w_FLAGEUR = this.oParentObject.w_FLAGEURO
    this.w_SOSPENDI = .F.
    ah_Msg("Preparazione cursori in corso...",.T.)
    * --- Lancio i batches standard che creano i cursori
    do GSRI_BST with this
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    do GSRI_BSC with this
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Ex chiamata a GSRI_BSE
    * --- Controllo se sono state abbinate ritenute con condizioni diverse
    if this.w_SOSPENDI
      i_retcode = 'stop'
      return
    endif
    * --- Conto i modelli totali della dichiarazione
    if used("RITE1")
      select RITE1
      this.w_nMod1 = iif(mod(reccount(),5)=0,int(reccount()/5),int(reccount()/5)+1)
      Select MRCODCON from RITE1 Group By MRCODCON into cursor TEMP
      Select TEMP
      this.w_nRECRITE1 = reccount()
      if used("TEMP")
        Select TEMP
        use
      endif
    endif
    if used("RITE2")
      select RITE2
      this.w_nMod2 = iif(mod(reccount(),10)=0,int(reccount()/10),int(reccount()/10)+1)
      this.w_nRECRITE2 = reccount()
    endif
    if used("RITE3")
      select RITE3
      this.w_nMod3 = iif(mod(reccount(),13)=0,int(reccount()/13),int(reccount()/13)+1)
      this.w_nRECRITE3 = reccount()
    endif
    * --- Calcolo numero di certificazioni di lavoro autonomo
    this.w_NUMCERTIF = this.w_nRECRITE1
    if (this.w_nRECRITE1=0) and (this.w_nRECRITE3=0)
      i_retcode = 'stop'
      return
    endif
    * --- Creo loggetto
    this.FileTelematico = CreateObject("FileTelematico",alltrim(this.oParentObject.w_DirName)+alltrim(this.oParentObject.w_FileName),this)
    * --- Valorizzo il numero totale di modelli
    this.FileTelematico.nModelli = Max(this.w_nMod1,this.w_nMod2,this.w_nMod3)
    * --- Riga di testata
    ah_Msg("Fase 2: record tipo 'A'...",.T.)
    this.Pag7()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Record di Tipo "B"
    ah_Msg("Fase 2: record tipo 'B'...",.T.)
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    ah_Msg("Fase 2: record tipo 'E' ed 'H'...",.T.)
    * --- Record di Tipo "E" ed "H"
    if used("RITE1") and used("RITE3")
      do while !eof("RITE3")
        this.Pag6()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.FileTelematico.ScriviRecordE()     
        * --- Incremento il numero di modelli
        this.FileTelematico.nModelloE = this.FileTelematico.nModelloE + 1
      enddo
      do while !eof("RITE1")
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.FileTelematico.ScriviRecordH()     
        * --- Incremento il numero di modelli
        this.FileTelematico.nModelloH = this.FileTelematico.nModelloH + 1
      enddo
    endif
    * --- Riga di chiusura
    ah_Msg("Fase 2: record tipo 'Z'...",.T.)
    this.FileTelematico.RecordZ()     
    if used("RITE1")
      select RITE1
      use
    endif
    if used("RITE2")
      select RITE2
      use
    endif
    if used("RITE3")
      select RITE3
      use
    endif
    wait clear
    ah_ErrorMsg("Il file %1 %2 � stato generato","!","",alltrim(this.oParentObject.w_DirName),alltrim(this.oParentObject.w_FileName))
    * --- Riassegna successivo nome file
    this.oParentObject.w_FileName = left(time(),2)+substr(time(),4,2)+right(time(),2)+".770"
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Record B - Frontespizio
    this.FileTelematico.RecordB("B")     
    this.FileTelematico.RecordB(this.w_CFAzi)     
    this.FileTelematico.RecordB(right(space(8) + alltrim(str(this.FileTelematico.nModello)),8))     
    this.FileTelematico.RecordB(space(3))     
    this.FileTelematico.RecordB(space(25))     
    this.FileTelematico.RecordB(space(20))     
    this.FileTelematico.RecordB("05006900962     ")     
    this.FileTelematico.RecordB(this.oParentObject.w_FLAGCONF)     
    this.FileTelematico.RecordB(iif(this.oParentObject.w_FLAGEURO="1","0","1"))     
    * --- Importi espressi in migliaia di lire
    this.FileTelematico.RecordB(iif(this.oParentObject.w_FLAGEURO="1","1","0"))     
    * --- Valuta EURO
    this.FileTelematico.RecordB(this.oParentObject.w_FLAGCOR)     
    this.FileTelematico.RecordB(this.oParentObject.w_FLAGINT)     
    this.FileTelematico.RecordB(space(7))     
    this.FileTelematico.RecordB(" " + right(this.oParentObject.w_FLAGEVE,1))     
    this.FileTelematico.RecordB(space(2))     
    this.FileTelematico.RecordB(space(7))     
    * --- Dati del frontespizio
    this.FileTelematico.RecordB(this.oParentObject.w_FOCOGNOME)     
    this.FileTelematico.RecordB(this.oParentObject.w_FONOME)     
    this.FileTelematico.RecordB(this.oParentObject.w_FODENOMINA)     
    this.FileTelematico.RecordB(space(15))     
    this.FileTelematico.RecordB(this.oParentObject.w_CODATT)     
    * --- campo 33
    this.FileTelematico.RecordB(this.oParentObject.w_TELEFONO)     
    this.FileTelematico.RecordB(this.oParentObject.w_TELEFAX)     
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZ_EMAIL"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZ_EMAIL;
        from (i_cTable) where;
            AZCODAZI = this.oParentObject.w_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_INDIRIZZOEMAIL = NVL(cp_ToDate(_read_.AZ_EMAIL),cp_NullValue(_read_.AZ_EMAIL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- campo 35
    this.FileTelematico.RecordB(upper(left(this.w_INDIRIZZOEMAIL+space(100),100)))     
    this.FileTelematico.RecordB(space(9))     
    * --- Persona fisica
    this.FileTelematico.RecordB(this.oParentObject.w_COMUNE)     
    this.FileTelematico.RecordB(this.oParentObject.w_SIGLA)     
    this.w_DataAppo = right("00"+alltrim(str(day(this.oParentObject.w_DATANASC))),2)+right("00"+alltrim(str(month(this.oParentObject.w_DATANASC))),2)+str(year(this.oParentObject.w_DATANASC),4,0)
    this.FileTelematico.RecordB(iif(!empty(this.oParentObject.w_DATANASC),this.w_DataAppo,space(8)))     
    * --- campo 40
    this.FileTelematico.RecordB(this.oParentObject.w_SESSO)     
    this.FileTelematico.RecordB(" ")     
    this.FileTelematico.RecordB(this.oParentObject.w_RECOMUNE)     
    this.FileTelematico.RecordB(this.oParentObject.w_RESIGLA)     
    this.FileTelematico.RecordB(this.oParentObject.w_INDIRIZ)     
    this.FileTelematico.RecordB(right(space(5) + alltrim(this.oParentObject.w_CAP), 5))     
    this.w_DataAppo = right("00"+alltrim(str(day(this.oParentObject.w_VARRES))),2)+right("00"+alltrim(str(month(this.oParentObject.w_VARRES))),2)+str(year(this.oParentObject.w_VARRES),4,0)
    this.FileTelematico.RecordB(iif(!empty(this.oParentObject.w_VARRES),this.w_DataAppo,space(8)))     
    * --- Filler campi 47 - 48
    this.FileTelematico.RecordB("  ")     
    * --- Filler campi 49 - 55
    this.FileTelematico.RecordB(space(110))     
    * --- Filler campo  56
    this.FileTelematico.RecordB("   ")     
    * --- Filler campi 57 - 59
    this.FileTelematico.RecordB(space(83))     
    * --- Filler campi 60 - 61
    this.FileTelematico.RecordB("  ")     
    * --- Filler campo 62
    this.FileTelematico.RecordB(space(10))     
    * --- Altri soggetti
    this.w_DataAppo = right("00"+alltrim(str(month(this.oParentObject.w_SEVARSED))),2)+str(year(this.oParentObject.w_SEVARSED),4,0)
    this.FileTelematico.RecordB(iif(!empty(this.oParentObject.w_SEVARSED),this.w_DataAppo,space(6)))     
    this.FileTelematico.RecordB(this.oParentObject.w_SECOMUNE)     
    this.FileTelematico.RecordB(this.oParentObject.w_SESIGLA)     
    this.FileTelematico.RecordB(this.oParentObject.w_SEINDIRI2)     
    this.FileTelematico.RecordB(right(space(5) + alltrim(this.oParentObject.w_SECAP), 5))     
    this.w_DataAppo = right("00"+alltrim(str(month(this.oParentObject.w_SEVARDOM))),2)+str(year(this.oParentObject.w_SEVARDOM),4,0)
    this.FileTelematico.RecordB(iif(!empty(this.oParentObject.w_SEVARDOM),this.w_DataAppo,space(6)))     
    this.FileTelematico.RecordB(this.oParentObject.w_SERCOMUN)     
    this.FileTelematico.RecordB(this.oParentObject.w_SERSIGLA)     
    this.FileTelematico.RecordB(this.oParentObject.w_SERINDIR)     
    this.FileTelematico.RecordB(right(space(5) + alltrim(this.oParentObject.w_SERCAP), 5))     
    * --- Campi 73 - 74
    this.FileTelematico.RecordB(space(16))     
    this.FileTelematico.RecordB(iif(!empty(this.oParentObject.w_STATO),this.oParentObject.w_STATO," "))     
    this.FileTelematico.RecordB(right("  " + alltrim(this.oParentObject.w_NATGIU), 2))     
    this.FileTelematico.RecordB(iif(!empty(this.oParentObject.w_SITUAZ),this.oParentObject.w_SITUAZ," "))     
    * --- Campo  78
    this.FileTelematico.RecordB(" ")     
    * --- Campo  79
    this.FileTelematico.RecordB(space(24))     
    * --- Campo 80
    this.FileTelematico.RecordB("   ")     
    * --- Campo  81
    this.FileTelematico.RecordB(space(20))     
    * --- Campi  82 -84
    this.FileTelematico.RecordB(space(14))     
    this.FileTelematico.RecordB(this.oParentObject.w_CODFISDA)     
    this.FileTelematico.RecordB(space(51))     
    * --- Dati relativi al rappresentante firmatario della dichiarazione
    this.FileTelematico.RecordB(this.oParentObject.w_RFCODFIS)     
    this.FileTelematico.RecordB(space(11))     
    this.FileTelematico.RecordB(right("  "+alltrim(this.oParentObject.w_RFCODCAR),2))     
    this.FileTelematico.RecordB(space(9))     
    this.FileTelematico.RecordB(this.oParentObject.w_RFCOGNOME)     
    this.FileTelematico.RecordB(this.oParentObject.w_RFNOME)     
    this.FileTelematico.RecordB(this.oParentObject.w_RFSESSO)     
    this.w_DataAppo = right("00"+alltrim(str(day(this.oParentObject.w_RFDATANASC))),2)+right("00"+alltrim(str(month(this.oParentObject.w_RFDATANASC))),2)+str(year(this.oParentObject.w_RFDATANASC),4,0)
    this.FileTelematico.RecordB(iif(!empty(this.oParentObject.w_RFDATANASC),this.w_DataAppo,space(8)))     
    this.FileTelematico.RecordB(this.oParentObject.w_RFCOMNAS)     
    this.FileTelematico.RecordB(this.oParentObject.w_RFSIGNAS)     
    this.FileTelematico.RecordB(this.oParentObject.w_RFCOMUNE)     
    this.FileTelematico.RecordB(this.oParentObject.w_RFSIGLA)     
    this.FileTelematico.RecordB(right(space(5) + alltrim(this.oParentObject.w_RFCAP), 5))     
    this.FileTelematico.RecordB(this.oParentObject.w_RFINDIRIZ)     
    this.FileTelematico.RecordB(this.oParentObject.w_RFTELEFONO)     
    * --- Filler campi 103 - 106
    this.FileTelematico.RecordB(space(18))     
    * --- Filler campo 107
    this.FileTelematico.RecordB(space(10))     
    * --- Firma della dichiarazione
    this.FileTelematico.RecordB(this.oParentObject.w_FIRMDICH)     
    * --- Filler campi 109 - 110
    this.FileTelematico.RecordB(space(2))     
    * --- Campo 111
    this.FileTelematico.RecordB(this.oParentObject.w_FIRMPRES)     
    * --- Filler campi 112 - 146
    this.FileTelematico.RecordB(space(37))     
    * --- Sostituti
    * --- Filler campi 147 - 157
    this.FileTelematico.RecordB(space(11))     
    * --- Sezione I
    this.FileTelematico.RecordB(this.oParentObject.w_qST1)     
    * --- Campo 159
    this.FileTelematico.RecordB(this.oParentObject.w_qSX1)     
    * --- Campo 160
    this.FileTelematico.RecordB(space(8))     
    * --- Campo 161
    this.FileTelematico.RecordB(RIGHT(space(8)+STR(this.w_NUMCERTIF,8,0),8))     
    * --- Filler campo 162
    this.FileTelematico.RecordB(space(3))     
    * --- Impegno alla presentazione telematica
    this.FileTelematico.RecordB(this.oParentObject.w_CODFISIN)     
    * --- Campo 164
    this.FileTelematico.RecordB(right(space(5)+iif(this.oParentObject.w_NUMCAF<>0,str(this.oParentObject.w_NUMCAF,5,0), space(5)),5))     
    * --- Campo 165
    this.FileTelematico.RecordB(this.oParentObject.w_IMPTRTEL)     
    * --- Campo 166
    this.FileTelematico.RecordB(this.oParentObject.w_IMPTRSOG)     
    * --- Campo 167
    this.w_DataAppo = right("00"+alltrim(str(day(this.oParentObject.w_RFDATIMP))),2) + right("00"+alltrim(str(month(this.oParentObject.w_RFDATIMP))),2) + right(STR(year(this.oParentObject.w_RFDATIMP),4,0),4)
    this.FileTelematico.RecordB(iif(!empty(this.w_DataAppo) and val(this.w_DataAppo)<>0,this.w_DataAppo,space(8)))     
    * --- Campo 168
    this.FileTelematico.RecordB(this.oParentObject.w_FIRMINT)     
    * --- Campo 169
    this.FileTelematico.RecordB(space(4))     
    * --- Visto di conformit�
    * --- Campo 170
    this.FileTelematico.RecordB(this.oParentObject.w_CFRESCAF)     
    * --- Campo 171
    this.FileTelematico.RecordB(this.oParentObject.w_VISTOA35)     
    * --- Campo 172
    this.FileTelematico.RecordB(this.oParentObject.w_FLAGFIR)     
    * --- Campo Filler 173
    this.FileTelematico.RecordB(space(5))     
    * --- Campo Filler 174
    this.FileTelematico.RecordB(space(16))     
    * --- Campo Filler 175 - 176
    this.FileTelematico.RecordB(space(2))     
    * --- Campo Filler 177
    this.FileTelematico.RecordB(space(5))     
    * --- Dichiarazione Correttiva nei termini e integrativa "parziale"
    this.FileTelematico.RecordB(right(space(17) + alltrim(STR(this.oParentObject.w_IDESTA,17,0)), 17))     
    this.FileTelematico.RecordB(right(space(6) + alltrim(STR(this.oParentObject.w_PROIFT,6,0)), 6))     
    * --- Campo 180
    this.FileTelematico.RecordB(this.oParentObject.w_qST1)     
    * --- Campo 181
    this.FileTelematico.RecordB(this.oParentObject.w_qSX1)     
    * --- Campo 182
    this.FileTelematico.RecordB(space(8))     
    * --- Campo 183
    this.FileTelematico.RecordB(RIGHT(SPACE(8)+alltrim(STR(this.w_NUMCERTIF,8,0)),8))     
    * --- Spazio non utilizzato
    * --- Campi 184 - 185
    this.FileTelematico.RecordB(space(351))     
    * --- Campo 186
    this.FileTelematico.RecordB(space(20))     
    * --- Campi 187 - 189
    this.FileTelematico.RecordB(space(15))     
    * --- Campo 190
    this.FileTelematico.RecordB(space(1))     
    * --- Campi 191 - 194
    this.FileTelematico.RecordB(space(4))     
    * --- Campo 195
    this.FileTelematico.RecordB(space(14))     
    * --- Chiudo la riga
    this.FileTelematico.RecordB(space(1897-len(this.FileTelematico.RigaB))+"A")     
    * --- Scrittura del recordB
    this.FileTelematico.RecordB("",.t.,.t.)     
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_PRIMOPERC = .T.
    this.w_OLDPERC = ""
    * --- Record H
    select RITE1
    do while !eof()
      select RITE1
      if this.w_OLDPERC<>ANCODFIS
        if NOT(this.w_PRIMOPERC)
          * --- Nuovo record per variazione percipiente --- Record 'H'
          * --- Incremento il numero di Modello
          this.FileTelematico.nRigoAU = 1
          this.FileTelematico.nModAU = this.FileTelematico.nModAU + 1
          exit
        endif
        if EMPTY(nvl(ANCODFIS,""))
          ah_ErrorMsg("Codice fiscale percipiente non presente, impossibile generare il file","!","")
          this.FileTelematico.CancellaFile()     
          i_retcode = 'stop'
          return
        endif
        this.FileTelematico.RecordH("AU","001",ANCODFIS)     
        this.FileTelematico.RecordH("AU","002",iif(ANPERFIS="S",upper(NVL(ANCOGNOM,"")),upper(NVL(ANDESCRI,""))))     
        this.FileTelematico.RecordH("AU","003",iif(ANPERFIS="S",upper(NVL(AN__NOME,"")),""))     
        this.FileTelematico.RecordH("AU","004",iif(ANPERFIS="S",upper(NVL(AN_SESSO,"")),""))     
        this.FileTelematico.RecordH("AU","005",iif(ANPERFIS="S",NVL(ANDATNAS,cp_CharToDate("  -  -  ")),""))     
        this.FileTelematico.RecordH("AU","006",iif(ANPERFIS="S",upper(NVL(ANLOCNAS,"")),""))     
        this.FileTelematico.RecordH("AU","007",iif(ANPERFIS="S",upper(NVL(ANPRONAS,"")),""))     
        this.FileTelematico.RecordH("AU","008",upper(NVL(ANLOCALI,"")))     
        this.FileTelematico.RecordH("AU","009",upper(NVL(ANPROVIN,"")))     
        this.FileTelematico.RecordH("AU","011",upper(NVL(ANINDIRI,"")))     
        this.w_OLDPERC = NVL(ANCODFIS,"")
        this.w_PRIMOPERC = .F.
      endif
      * --- Riservato ai percipienti esteri
      * --- Dati relativi alle somme erogate
      this.FileTelematico.RecordH("AU","016",upper(NVL(TRCAUPRE,"")))     
      this.FileTelematico.RecordH("AU","017","            "+alltrim(str(year(this.oParentObject.w_DATFIN))))     
      this.FileTelematico.RecordH("AU","019",NVL(SOMTOTA,0))     
      this.FileTelematico.RecordH("AU","021",NVL(NONSOGG,0))     
      this.FileTelematico.RecordH("AU","022",NVL(IMPONI,0))     
      this.FileTelematico.RecordH("AU","023",NVL(MRTOTIM1,0))     
      * --- Incremento il numero di Rigo
      this.FileTelematico.nRigoAU = this.FileTelematico.nRigoAU + 1
      if this.FileTelematico.nRigoAU > 5
        * --- Incremento il numero di Modello
        this.FileTelematico.nRigoAU = 1
        this.FileTelematico.nModAU = this.FileTelematico.nModAU + 1
        skip in RITE1
        exit
      endif
      skip in RITE1
    enddo
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Record E - Quadro SE
    select RITE2
    do while !eof()
      select RITE2
      this.FileTelematico.RecordE("SE","001",ANCODFIS)     
      this.FileTelematico.RecordE("SE","002",iif(ANPERFIS="S",ANCOGNOM,ANDESCRI))     
      this.FileTelematico.RecordE("SE","003",iif(ANPERFIS="S",AN__NOME,""))     
      this.FileTelematico.RecordE("SE","004",iif(ANPERFIS="S",AN_SESSO,""))     
      this.FileTelematico.RecordE("SE","005",iif(ANPERFIS="S",ANDATNAS,""))     
      this.FileTelematico.RecordE("SE","006",iif(ANPERFIS="S",ANLOCNAS,""))     
      this.FileTelematico.RecordE("SE","007",iif(ANPERFIS="S",ANPRONAS,""))     
      this.FileTelematico.RecordE("SE","008",ANLOCALI)     
      this.FileTelematico.RecordE("SE","009",ANPROVIN)     
      this.FileTelematico.RecordE("SE","010",ANINDIRI)     
      * --- Incremento il numero di Rigo
      this.FileTelematico.nRigoSE = this.FileTelematico.nRigoSE + 1
      if this.FileTelematico.nRigoSE > 10
        * --- Incremento il numero di Modello
        this.FileTelematico.nRigoSE = 2
        this.FileTelematico.nModSE = this.FileTelematico.nModSE + 1
        exit
      endif
      skip in RITE2
    enddo
  endproc


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Record E - Quadro SS
    * --- Quadro SS (SC)
    select count(*) from RITE1 group by ANCODFIS into cursor _quadross_
    this.w_NumPerSC = _tally
    select sum(SOMTOTA) as tSOMTOTA,sum(NONSOGG) as tNONSOGG,sum(IMPONI) as tIMPONI,sum(MRTOTIM1) as tMRTOTIM1 ;
    from RITE1 into cursor _quadross_
    this.w_tSOMTOTASC = _quadross_.tSOMTOTA
    this.w_tNONSOGGSC = _quadross_.tNONSOGG
    this.w_tIMPONISC = _quadross_.tIMPONI
    this.w_tMRTOTIM1SC = _quadross_.tMRTOTIM1
    * --- Quadro SS (SE)
    select count(*) from RITE2 group by ANCODFIS into cursor _quadross_
    this.w_NumPerSE = _tally
    select sum(SOMTOTA) as tSOMTOTA,sum(IMPONI) as tIMPONI,sum(MRTOTIM1) as tMRTOTIM1 from RITE2 into cursor _quadross_
    this.w_tSOMTOTASE = _quadross_.tSOMTOTA
    this.w_tIMPONISE = _quadross_.tIMPONI
    this.w_tMRTOTIM1SE = _quadross_.tMRTOTIM1
    if used("_quadross_")
      select _quadross_
      use
    endif
    this.FileTelematico.RecordE("SS003","001",this.w_NumPerSC,.t.)     
    this.FileTelematico.RecordE("SS003","002",this.w_tSOMTOTASC,.t.)     
    this.FileTelematico.RecordE("SS003","003",this.w_tNONSOGGSC,.t.)     
    this.FileTelematico.RecordE("SS003","005",this.w_tIMPONISC,.t.)     
    this.FileTelematico.RecordE("SS003","006",this.w_tMRTOTIM1SC,.t.)     
    this.FileTelematico.RecordE("SS005","001",this.w_NumPerSE,.t.)     
    this.FileTelematico.RecordE("SS005","002",this.w_tSOMTOTASE,.t.)     
    this.FileTelematico.RecordE("SS005","005",this.w_tIMPONISE,.t.)     
    this.FileTelematico.RecordE("SS005","006",this.w_tMRTOTIM1SE,.t.)     
  endproc


  procedure Pag6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Record E - Quadro ST
    select RITE3
    do while !eof()
      select RITE3
      this.FileTelematico.RecordE("ST","001",iif("A"$VP__NOTE OR "B"$VP__NOTE,"          "+"122001","          "+right("00"+alltrim(str(MESE)),2)+str(ANNO,4,0)))     
      this.FileTelematico.RecordE("ST","002",SOMTOTA)     
      this.FileTelematico.RecordE("ST","003",VPRITECC)     
      this.FileTelematico.RecordE("ST","004",VPRITCOM)     
      this.w_CALCOLI = SOMTOTA-VPRITECC-VPRITCOM
      this.FileTelematico.RecordE("ST","005",this.w_CALCOLI)     
      this.FileTelematico.RecordE("ST","006",VPIMPINT)     
      this.FileTelematico.RecordE("ST","007",VP__NOTE)     
      this.FileTelematico.RecordE("ST","008",VPEVEECC)     
      this.FileTelematico.RecordE("ST","009",iif((VPVALVER="EUR" AND this.oParentObject.w_FLAGEURO="1") OR (VPVALVER<>"EUR" AND this.oParentObject.w_FLAGEURO="0") ,"","               1"))     
      this.FileTelematico.RecordE("ST","010",MRCODTRI)     
      this.FileTelematico.RecordE("ST","011",iif(VPARTICO<>0,"              "+right("00"+iif(vpartico>9,STR(VPARTICO,2,0),str(vpartico,1,0)),2),""))     
      * --- Campo 12
      if VPTIPVER = "T"
        this.FileTelematico.RecordE("ST","012","               1")     
      endif
      this.FileTelematico.RecordE("ST","013",iif(VPCODREG<>0,"              "+right("00"+iif(vpcodreg>9,STR(VPCODREG,2,0),str(vpcodreg,1,0)),2),""))     
      * --- Incremento il numero di Rigo
      this.FileTelematico.nRigoST = this.FileTelematico.nRigoST + 1
      if this.FileTelematico.nRigoST > 13
        * --- Incremento il numero di Modello
        this.FileTelematico.nRigoST = 2
        this.FileTelematico.nModST = this.FileTelematico.nModST + 1
        skip in RITE3
        exit
      endif
      skip in RITE3
    enddo
  endproc


  procedure Pag7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Record A - Frontespizio
    this.FileTelematico.RecordA("A")     
    this.FileTelematico.RecordA(space(14))     
    this.FileTelematico.RecordA("77S02")     
    this.FileTelematico.RecordA(this.oParentObject.w_TIPFORN)     
    this.FileTelematico.RecordA(iif(empty(this.oParentObject.w_CODFISIN),this.w_CFAzi,this.oParentObject.w_CODFISIN))     
    * --- Dati riservati al fornitore persona fisica
    this.FileTelematico.RecordA(iif(this.oParentObject.w_TIPFORN="01" OR this.oParentObject.w_TIPFORN="02",this.oParentObject.w_FOCOGNOME,SPACE(24)))     
    this.FileTelematico.RecordA(iif(this.oParentObject.w_TIPFORN="01" OR this.oParentObject.w_TIPFORN="02",this.oParentObject.w_FONOME,SPACE(20)))     
    this.FileTelematico.RecordA(iif(empty(this.oParentObject.w_FOSESSO),space(1),this.oParentObject.w_FOSESSO))     
    this.w_DataAppo = right("00"+alltrim(str(day(this.oParentObject.w_FODATANASC))),2)+right("00"+alltrim(str(month(this.oParentObject.w_FODATANASC))),2)+str(year(this.oParentObject.w_FODATANASC),4,0)
    this.FileTelematico.RecordA(iif(!empty(this.oParentObject.w_FODATANASC),this.w_DataAppo, space(8)))     
    this.FileTelematico.RecordA(this.oParentObject.w_FOCOMUNE)     
    this.FileTelematico.RecordA(this.oParentObject.w_FOSIGLA)     
    this.FileTelematico.RecordA(this.oParentObject.w_FORECOMUNE)     
    this.FileTelematico.RecordA(this.oParentObject.w_FORESIGLA)     
    this.FileTelematico.RecordA(this.oParentObject.w_FOINDIRIZ)     
    this.FileTelematico.RecordA(iif(!empty(this.oParentObject.w_FOCAP),this.oParentObject.w_FOCAP, space(5)))     
    * --- Dati riservati al fornitore persona non fisica
    this.FileTelematico.RecordA(iif(this.oParentObject.w_TIPFORN="01" OR this.oParentObject.w_TIPFORN="02",this.oParentObject.w_FODENOMINA,SPACE(60)))     
    this.FileTelematico.RecordA(this.oParentObject.w_FOSECOMUNE)     
    this.FileTelematico.RecordA(this.oParentObject.w_FOSESIGLA)     
    this.FileTelematico.RecordA(this.oParentObject.w_FOSEINDIRI2)     
    this.FileTelematico.RecordA(iif(!empty(this.oParentObject.w_FOSECAP),this.oParentObject.w_FOSECAP,space(5)))     
    this.FileTelematico.RecordA(this.oParentObject.w_FOSERCOMUN)     
    this.FileTelematico.RecordA(this.oParentObject.w_FOSERSIGLA)     
    this.FileTelematico.RecordA(this.oParentObject.w_FOSERINDIR)     
    this.FileTelematico.RecordA(iif(!empty(this.oParentObject.w_FOSERCAP), this.oParentObject.w_FOSERCAP, space(5)))     
    * --- Campi per ufficio perifierico del C.A.F.
    this.FileTelematico.RecordA(space(40))     
    this.FileTelematico.RecordA(space(2))     
    this.FileTelematico.RecordA(space(35))     
    this.FileTelematico.RecordA(space(5))     
    * --- Dichiarazione su pi� invii
    this.FileTelematico.RecordA("   1")     
    this.FileTelematico.RecordA("   1")     
    * --- Chiudo la riga
    this.FileTelematico.RecordA(space(1897-len(this.FileTelematico.RigaA))+"A")     
    this.FileTelematico.RecordA("",.t.,.t.)     
  endproc


  procedure Pag8
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='VEP_RITE'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- gsri_bft
  enddefine
  
  func NoPosValue(pQRC,pValore,flagEur)
  local ValorePlus
  do case
  case type('pValore') = 'C'
    if !empty(pValore)
      if len(alltrim(pValore)) > 16 && scrivo il dato su pi� campi
        ValorePlus = pQRC+left(pValore,16)
        pValore = substr(pValore,17)
        do while !empty(alltrim(pValore))
          ValorePlus = ValorePlus + pQRC+'+'+left(pValore,15)+space(15-iif(len(pValore)>15,15,len(pValore)))
          pValore = substr(pValore,17)
        enddo
        return ValorePlus
      else
        return (pQRC+left(pValore+space(16),16))
      endif
    else
      return ''
    endif
  case type('pValore') = 'N'
    if pValore <> 0
      * troncamento delle migliaia
      if (flagEur = '1')
             * troncamento all'unit� di euro
             return (pQRC+right(space(16)+alltrim(str(int(pValore))),16))
        else
             return (pQRC+right(space(16)+alltrim(str(int(pValore/1000))),16))
      endif
    else
      return ''
    endif
  case type('pValore') = 'D'
    if !empty(pValore)
      return (pQRC+right(space(16)+left(dtoc(pValore),2)+substr(dtoc(pValore),4,2)+right(dtoc(pValore),4),16))
    else
      return ''
    endif
  case type('pValore') = 'T'
    if !empty(pValore)
      return (pQRC+right(space(16)+left(dtoc(ttod(pValore)),2)+substr(dtoc(ttod(pValore)),4,2)+right(dtoc(ttod(pValore)),4),16))
    else
      return ''
    endif
  otherwise
    ah_Msg("Tipo dati non definito: %1",type("pvalore"),.T.)
    return ''
  endcase
  endfunc
  
  define class FileTelematico as Custom
    oParentObject = .null.
    NomeFile = ''
    RigaA = ''
    RigaB = ''
    RigaE = ''
    PosRigaE = ''
    RigaH = ''
    PosRigaH = ''
    nRigoAU = 1 && Parte da 1
    nRigoSE = 2 && Parte da 2
    nRigoST = 2 && Parte da 2
    nModello = 1 
    nModelloE = 1
    nModelloH = 1
    nModelli = 1
    nModAU = 1
    nModSE = 1
    nModST = 1
    nRecTipoE = 1
    nRecTipoH = 1
  
  proc Init(pNomeFile,oParent)
    strtofile('',pNomeFile)
    this.NomeFile = pNomeFile
    this.oParentObject = oParent
  endproc
  
  proc RecordA(pValore, pACapo,pScrivi)
    this.RigaA =   this.RigaA + upper(pValore)
    if pACapo
        this.RigaA =   this.RigaA + chr(13) + chr(10)
    endif
    if pScrivi
      strtofile(this.RigaA,this.NomeFile,.t.)
      this.RigaA = ''
    endif
  endproc
  
  proc RecordB(pValore, pACapo,pScrivi)
    this.RigaB =   this.RigaB + upper(pValore)
    if pACapo
        this.RigaB =   this.RigaB + chr(13) + chr(10)
    endif
    if pScrivi
      strtofile(this.RigaB,this.NomeFile,.t.)
      this.RigaB = ''
    endif
  endproc
  
  proc RecordEpos &&campi posizionali
    this.PosRigaE = 'E'
    this.PosRigaE = this.PosRigaE + this.oParentObject.w_CFAzi
    this.PosRigaE = this.PosRigaE + right(space(8) + alltrim(str(this.nModelloE)),8) && numero modulo
    this.PosRigaE = this.PosRigaE + space(3)
    this.PosRigaE = this.PosRigaE + this.oParentObject.w_OPERAZ
    this.PosRigaE = this.PosRigaE + space(24)
    this.PosRigaE = this.PosRigaE + space(20)  
    this.PosRigaE = this.PosRigaE + '05006900962     ' && PIVA ZUCCHETTI
  endproc
  
  proc RecordE(pQ,pC,pValore,pRigaFissa)
  local nRiga
  if !pRigaFissa
    nRiga = 'this.nRigo'+pQ
    this.RigaE =   this.RigaE + NoPosValue(pQ+right('000'+alltrim(Str(&nRiga)),3)+pC,pValore,oParentObject.w_FLAGEURO)
  else
    this.RigaE =   this.RigaE + NoPosValue(pQ+pC,pValore,this.oParentObject.w_FLAGEURO)
  endif
  endproc
  
  proc ScriviRecordE
    this.RecordEpos()
    strtofile(this.PosRigaE+left(this.RigaE,1800)+     space(iif(int(len(this.RigaE)/1800)=0,1800-len(this.RigaE)+8,8))+     'A'+chr(13)+chr(10),this.NomeFile,.t.)
    do while len(alltrim(this.RigaE)) > 1800
       this.RigaE = substr(this.RigaE,1801)
       strtofile(this.PosRigaE+left(this.RigaE,1800)+space(iif(int(len(this.RigaE)/1800)=0,1800-len(this.RigaE)+8,8))+'A'+chr(13)+chr(10),this.NomeFile,.t.)
       this.nRecTipoE = this.nRecTipoE + 1
    enddo
    this.RigaE = ''
  endproc
  
  proc RecordHpos &&campi posizionali
    this.PosRigaH = 'H'
    this.PosRigaH = this.PosRigaH + this.oParentObject.w_CFAzi
    this.PosRigaH = this.PosRigaH + right(space(8) + alltrim(str(this.nModelloH)),8) && numero modulo
    this.PosRigaH = this.PosRigaH + space(3)
    this.PosRigaH = this.PosRigaH + this.oParentObject.w_OPERAZ
    this.PosRigaH = this.PosRigaH + space(16) &&CF del Percipiente
    this.PosRigaH = this.PosRigaH + space(28)  
    this.PosRigaH = this.PosRigaH + '05006900962     ' && PIVA ZUCCHETTI
  endproc
  
  
  proc RecordH(pQ,pC,pValore,pRigaFissa)
  local nRiga
  if !pRigaFissa
    if val(pc) > 11
        nRiga = 'this.nRigo'+pQ
    else 
        nRiga='1'
    endif    
    this.RigaH =   this.RigaH + NoPosValue(pQ+right('000'+alltrim(Str(&nRiga)),3)+pC,pValore,oParentObject.w_FLAGEURO)
  else
    this.RigaH =   this.RigaH + NoPosValue(pQ+pC,pValore,oParentObject.w_FLAGEURO)
  endif
  endproc
  
  
  proc ScriviRecordH
    this.RecordHpos()
    strtofile(this.PosRigaH+left(this.RigaH,1800)+     space(iif(int(len(this.RigaH)/1800)=0,1800-len(this.RigaH)+8,8))+     'A'+chr(13)+chr(10),this.NomeFile,.t.)
    do while len(alltrim(this.RigaH)) > 1800
       this.RigaH = substr(this.RigaH,1801)
       strtofile(this.PosRigaH+left(this.RigaH,1800)+space(iif(int(len(this.RigaH)/1800)=0,1800-len(this.RigaH)+8,8))+'A'+chr(13)+chr(10),this.NomeFile,.t.)
       this.nRecTipoH = this.nRecTipoH + 1
    enddo
    this.RigaH = ''
  endproc
  
  Proc CancellaFile
    erase(this.NomeFile)
  endproc  
  
  Proc RecordZ
  local Riga
    Riga = 'Z'
    Riga = Riga + space(14)
    Riga = Riga + space(8)+'1'
    Riga = Riga +  right(space(9) + alltrim(str(this.nModelloE-1)),9)
    Riga = Riga + space(9) && Numero record tipo F
    Riga = Riga + space(9) && Numero record tipo G
    Riga = Riga +  right(space(9) + alltrim(str(this.nModelloH-1)),9)
    Riga = Riga + space(1837)
    Riga = Riga + 'A'
    Riga = Riga + chr(13) + chr(10)
    strtofile(Riga,this.NomeFile,.t.)
  endproc
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
