* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_kmr                                                        *
*              Visualizza movimenti ritenute                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2013-12-09                                                      *
* Last revis.: 2014-11-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsri_kmr",oParentObject))

* --- Class definition
define class tgsri_kmr as StdForm
  Top    = 9
  Left   = 10

  * --- Standard Properties
  Width  = 732
  Height = 410+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-11-10"
  HelpContextID=199882345
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=21

  * --- Constant Properties
  _IDX = 0
  CONTI_IDX = 0
  PNT_MAST_IDX = 0
  TRI_BUTI_IDX = 0
  cPrg = "gsri_kmr"
  cComment = "Visualizza movimenti ritenute"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_TIPRIT = space(1)
  w_CODCON = space(15)
  w_DESCRI = space(40)
  w_DREGINI = ctod('  /  /  ')
  w_DREGFIN = ctod('  /  /  ')
  w_DOCINI = 0
  w_ALFDOCINI = space(10)
  w_DOCFIN = 0
  w_SERIAL = space(10)
  w_ORIGINE = space(10)
  w_RIFCON = space(10)
  w_ALFDOCFIN = space(10)
  w_DADOCINI = ctod('  /  /  ')
  w_DADOCFIN = ctod('  /  /  ')
  w_FLACON = space(1)
  w_VPCODTRI = space(5)
  w_DESTRI = space(35)
  w_VPCODTR1 = space(5)
  w_DESTRI1 = space(35)
  w_TIPCON = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_Zoom = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsri_kmrPag1","gsri_kmr",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Selezioni")
      .Pages(2).addobject("oPag","tgsri_kmrPag2","gsri_kmr",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Selezioni aggiuntive")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODCON_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- gsri_kmr
    WITH THIS.PARENT
     DO CASE
             CASE OPARENTOBJECT = 'A'
                   .cComment = "Visualizza movimenti ritenute operate"
             CASE OPARENTOBJECT  = 'V'
                   .cComment = "Visualizza movimenti ritenute subite"
       ENDCASE
    ENDWITH
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_Zoom = this.oPgFrm.Pages(1).oPag.Zoom
    DoDefault()
    proc Destroy()
      this.w_Zoom = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='PNT_MAST'
    this.cWorkTables[3]='TRI_BUTI'
    return(this.OpenAllTables(3))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIPRIT=space(1)
      .w_CODCON=space(15)
      .w_DESCRI=space(40)
      .w_DREGINI=ctod("  /  /  ")
      .w_DREGFIN=ctod("  /  /  ")
      .w_DOCINI=0
      .w_ALFDOCINI=space(10)
      .w_DOCFIN=0
      .w_SERIAL=space(10)
      .w_ORIGINE=space(10)
      .w_RIFCON=space(10)
      .w_ALFDOCFIN=space(10)
      .w_DADOCINI=ctod("  /  /  ")
      .w_DADOCFIN=ctod("  /  /  ")
      .w_FLACON=space(1)
      .w_DESTRI=space(35)
      .w_DESTRI1=space(35)
      .w_TIPCON=space(1)
      .w_OBTEST=ctod("  /  /  ")
        .w_TIPRIT = this.oparentobject
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODCON))
          .link_1_2('Full')
        endif
          .DoRTCalc(3,8,.f.)
        .w_SERIAL = .w_Zoom.getVar('MRSERIAL')
        .w_ORIGINE = .w_Zoom.getVar('PTSERIAL')
        .w_RIFCON = .w_Zoom.getVar('PTSERRIF')
          .DoRTCalc(12,17,.f.)
        .w_TIPCON = IIF(THIS.OPARENTOBJECT='A','F','C')
      .oPgFrm.Page1.oPag.Zoom.Calculate()
        .w_OBTEST = I_DATSYS
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_28.enabled = this.oPgFrm.Page1.oPag.oBtn_1_28.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_29.enabled = this.oPgFrm.Page1.oPag.oBtn_1_29.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_31.enabled = this.oPgFrm.Page1.oPag.oBtn_1_31.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_32.enabled = this.oPgFrm.Page1.oPag.oBtn_1_32.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
            .w_TIPRIT = this.oparentobject
        .DoRTCalc(2,8,.t.)
            .w_SERIAL = .w_Zoom.getVar('MRSERIAL')
            .w_ORIGINE = .w_Zoom.getVar('PTSERIAL')
            .w_RIFCON = .w_Zoom.getVar('PTSERRIF')
        .DoRTCalc(12,19,.t.)
            .w_TIPCON = IIF(THIS.OPARENTOBJECT='A','F','C')
        .oPgFrm.Page1.oPag.Zoom.Calculate()
            .w_OBTEST = I_DATSYS
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.Zoom.Calculate()
    endwith
  return

  proc Calculate_QPLKJGKBIB()
    with this
          * --- apre i movimenti
          .w_OPEN = OPENGEST('A',"GSRI_AMR('"+.w_TIPRIT+"')",'MRSERIAL',.w_SERIAL)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_31.enabled = this.oPgFrm.Page1.oPag.oBtn_1_31.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_32.enabled = this.oPgFrm.Page1.oPag.oBtn_1_32.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_3.visible=!this.oPgFrm.Page1.oPag.oStr_1_3.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_4.visible=!this.oPgFrm.Page1.oPag.oStr_1_4.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_4.visible=!this.oPgFrm.Page2.oPag.oStr_2_4.mHide()
    this.oPgFrm.Page2.oPag.oVPCODTR1_2_5.visible=!this.oPgFrm.Page2.oPag.oVPCODTR1_2_5.mHide()
    this.oPgFrm.Page2.oPag.oDESTRI1_2_6.visible=!this.oPgFrm.Page2.oPag.oDESTRI1_2_6.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.Zoom.Event(cEvent)
        if lower(cEvent)==lower("w_zoom selected")
          .Calculate_QPLKJGKBIB()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsri_kmr
    IF CEVENT='APREGEST'
       IF G_APPLICATION='ADHOC REVOLUTION'
         IF NOT EMPTY(NVL(this.w_RIFCON,''))
           OPENGEST('A','GSCG_MPN','PNSERIAL',this.w_RIFCON)
         ELSE
           AH_ERRORMSG('Documento di origine non presente: movimento ritenute caricato manualmente')
         ENDIF
       ELSE
         IF NOT EMPTY(NVL(this.w_ORIGINE,''))
           OPENGEST('A','GSCG_MPN','PNSERIAL',this.w_ORIGINE)
         ELSE
           AH_ERRORMSG('Documento di origine non presente: movimento ritenute caricato manualmente')
         ENDIF
       ENDIF
    ENDIF
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODCON
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_CODCON))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODCON_1_2'),i_cWhere,'GSAR_BZC',"",'GSRI_AMR.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_CODCON)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCRI = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODCON = space(15)
      endif
      this.w_DESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VPCODTRI
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TRI_BUTI_IDX,3]
    i_lTable = "TRI_BUTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2], .t., this.TRI_BUTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VPCODTRI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATB',True,'TRI_BUTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODTRI like "+cp_ToStrODBC(trim(this.w_VPCODTRI)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODTRI,TRDESTRI,TRFLACON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODTRI","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODTRI',trim(this.w_VPCODTRI))
          select TRCODTRI,TRDESTRI,TRFLACON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODTRI into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_VPCODTRI)==trim(_Link_.TRCODTRI) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_VPCODTRI) and !this.bDontReportError
            deferred_cp_zoom('TRI_BUTI','*','TRCODTRI',cp_AbsName(oSource.parent,'oVPCODTRI_2_1'),i_cWhere,'GSAR_ATB',"Tributi",'GSRI1AMR.TRI_BUTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODTRI,TRDESTRI,TRFLACON";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODTRI="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODTRI',oSource.xKey(1))
            select TRCODTRI,TRDESTRI,TRFLACON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VPCODTRI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODTRI,TRDESTRI,TRFLACON";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODTRI="+cp_ToStrODBC(this.w_VPCODTRI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODTRI',this.w_VPCODTRI)
            select TRCODTRI,TRDESTRI,TRFLACON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VPCODTRI = NVL(_Link_.TRCODTRI,space(5))
      this.w_DESTRI = NVL(_Link_.TRDESTRI,space(35))
      this.w_FLACON = NVL(_Link_.TRFLACON,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_VPCODTRI = space(5)
      endif
      this.w_DESTRI = space(35)
      this.w_FLACON = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FLACON='R'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_VPCODTRI = space(5)
        this.w_DESTRI = space(35)
        this.w_FLACON = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2])+'\'+cp_ToStr(_Link_.TRCODTRI,1)
      cp_ShowWarn(i_cKey,this.TRI_BUTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VPCODTRI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VPCODTR1
  func Link_2_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TRI_BUTI_IDX,3]
    i_lTable = "TRI_BUTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2], .t., this.TRI_BUTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VPCODTR1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATB',True,'TRI_BUTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODTRI like "+cp_ToStrODBC(trim(this.w_VPCODTR1)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODTRI,TRDESTRI,TRFLACON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODTRI","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODTRI',trim(this.w_VPCODTR1))
          select TRCODTRI,TRDESTRI,TRFLACON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODTRI into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_VPCODTR1)==trim(_Link_.TRCODTRI) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_VPCODTR1) and !this.bDontReportError
            deferred_cp_zoom('TRI_BUTI','*','TRCODTRI',cp_AbsName(oSource.parent,'oVPCODTR1_2_5'),i_cWhere,'GSAR_ATB',"Tributi",'GSRI2AMR.TRI_BUTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODTRI,TRDESTRI,TRFLACON";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODTRI="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODTRI',oSource.xKey(1))
            select TRCODTRI,TRDESTRI,TRFLACON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VPCODTR1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODTRI,TRDESTRI,TRFLACON";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODTRI="+cp_ToStrODBC(this.w_VPCODTR1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODTRI',this.w_VPCODTR1)
            select TRCODTRI,TRDESTRI,TRFLACON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VPCODTR1 = NVL(_Link_.TRCODTRI,space(5))
      this.w_DESTRI1 = NVL(_Link_.TRDESTRI,space(35))
      this.w_FLACON = NVL(_Link_.TRFLACON,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_VPCODTR1 = space(5)
      endif
      this.w_DESTRI1 = space(35)
      this.w_FLACON = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FLACON<>'R'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_VPCODTR1 = space(5)
        this.w_DESTRI1 = space(35)
        this.w_FLACON = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2])+'\'+cp_ToStr(_Link_.TRCODTRI,1)
      cp_ShowWarn(i_cKey,this.TRI_BUTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VPCODTR1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODCON_1_2.value==this.w_CODCON)
      this.oPgFrm.Page1.oPag.oCODCON_1_2.value=this.w_CODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_5.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_5.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDREGINI_1_6.value==this.w_DREGINI)
      this.oPgFrm.Page1.oPag.oDREGINI_1_6.value=this.w_DREGINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDREGFIN_1_7.value==this.w_DREGFIN)
      this.oPgFrm.Page1.oPag.oDREGFIN_1_7.value=this.w_DREGFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDOCINI_1_8.value==this.w_DOCINI)
      this.oPgFrm.Page1.oPag.oDOCINI_1_8.value=this.w_DOCINI
    endif
    if not(this.oPgFrm.Page1.oPag.oALFDOCINI_1_9.value==this.w_ALFDOCINI)
      this.oPgFrm.Page1.oPag.oALFDOCINI_1_9.value=this.w_ALFDOCINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDOCFIN_1_10.value==this.w_DOCFIN)
      this.oPgFrm.Page1.oPag.oDOCFIN_1_10.value=this.w_DOCFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oALFDOCFIN_1_14.value==this.w_ALFDOCFIN)
      this.oPgFrm.Page1.oPag.oALFDOCFIN_1_14.value=this.w_ALFDOCFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDADOCINI_1_15.value==this.w_DADOCINI)
      this.oPgFrm.Page1.oPag.oDADOCINI_1_15.value=this.w_DADOCINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDADOCFIN_1_17.value==this.w_DADOCFIN)
      this.oPgFrm.Page1.oPag.oDADOCFIN_1_17.value=this.w_DADOCFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oVPCODTRI_2_1.value==this.w_VPCODTRI)
      this.oPgFrm.Page2.oPag.oVPCODTRI_2_1.value=this.w_VPCODTRI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESTRI_2_3.value==this.w_DESTRI)
      this.oPgFrm.Page2.oPag.oDESTRI_2_3.value=this.w_DESTRI
    endif
    if not(this.oPgFrm.Page2.oPag.oVPCODTR1_2_5.value==this.w_VPCODTR1)
      this.oPgFrm.Page2.oPag.oVPCODTR1_2_5.value=this.w_VPCODTR1
    endif
    if not(this.oPgFrm.Page2.oPag.oDESTRI1_2_6.value==this.w_DESTRI1)
      this.oPgFrm.Page2.oPag.oDESTRI1_2_6.value=this.w_DESTRI1
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_FLACON='R')  and not(empty(.w_VPCODTRI))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oVPCODTRI_2_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_FLACON<>'R')  and not(.w_TIPCON='C')  and not(empty(.w_VPCODTR1))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oVPCODTR1_2_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsri_kmrPag1 as StdContainer
  Width  = 728
  height = 410
  stdWidth  = 728
  stdheight = 410
  resizeXpos=663
  resizeYpos=242
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODCON_1_2 as StdField with uid="ZXXOPIYHRR",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CODCON", cQueryName = "CODCON",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice del fornitore / cliente",;
    HelpContextID = 145908442,;
   bGlobalFont=.t.,;
    Height=21, Width=134, Left=154, Top=9, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODCON"

  func oCODCON_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCON_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCON_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODCON_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"",'GSRI_AMR.CONTI_VZM',this.parent.oContained
  endproc
  proc oCODCON_1_2.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CODCON
     i_obj.ecpSave()
  endproc

  add object oDESCRI_1_5 as StdField with uid="RYXGJIVWZO",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 226589898,;
   bGlobalFont=.t.,;
    Height=21, Width=356, Left=299, Top=9, InputMask=replicate('X',40)

  add object oDREGINI_1_6 as StdField with uid="NQNJUWAZII",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DREGINI", cQueryName = "",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data registrazione iniziale",;
    HelpContextID = 116502582,;
   bGlobalFont=.t.,;
    Height=21, Width=71, Left=154, Top=37

  add object oDREGFIN_1_7 as StdField with uid="IIJOYWFVAO",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DREGFIN", cQueryName = "",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data registrazione finale",;
    HelpContextID = 238964682,;
   bGlobalFont=.t.,;
    Height=21, Width=71, Left=154, Top=65

  add object oDOCINI_1_8 as StdField with uid="LSQHZYETLJ",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DOCINI", cQueryName = "",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Num. documento iniziale",;
    HelpContextID = 230453962,;
   bGlobalFont=.t.,;
    Height=21, Width=134, Left=299, Top=37, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  add object oALFDOCINI_1_9 as StdField with uid="KQPZBRBEMC",rtseq=7,rtrep=.f.,;
    cFormVar = "w_ALFDOCINI", cQueryName = "ALFDOCINI",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie documento iniziale",;
    HelpContextID = 206487268,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=443, Top=37, cSayPict='"!!!!!!!!!!"', cGetPict='"!!!!!!!!!!"', InputMask=replicate('X',10)

  add object oDOCFIN_1_10 as StdField with uid="FUPBGJNRAL",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DOCFIN", cQueryName = "",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Num.documento finale",;
    HelpContextID = 152007370,;
   bGlobalFont=.t.,;
    Height=21, Width=134, Left=299, Top=65, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  add object oALFDOCFIN_1_14 as StdField with uid="VUFHHQNABY",rtseq=12,rtrep=.f.,;
    cFormVar = "w_ALFDOCFIN", cQueryName = "ALFDOCFIN",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie documento finale",;
    HelpContextID = 206487343,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=443, Top=65, cSayPict='"!!!!!!!!!!"', cGetPict='"!!!!!!!!!!"', InputMask=replicate('X',10)

  add object oDADOCINI_1_15 as StdField with uid="NHWMIQKBDH",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DADOCINI", cQueryName = "",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data emissione del documento iniziale",;
    HelpContextID = 241594497,;
   bGlobalFont=.t.,;
    Height=21, Width=71, Left=584, Top=37

  add object oDADOCFIN_1_17 as StdField with uid="FPZLEDSVVG",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DADOCFIN", cQueryName = "",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data emissione del documento finale",;
    HelpContextID = 244944772,;
   bGlobalFont=.t.,;
    Height=21, Width=71, Left=584, Top=65


  add object Zoom as cp_zoombox with uid="UDDJAIFUNC",left=6, top=97, width=716,height=257,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="MOV_RITE",cZoomFile="GSRI_KMR",bOptions=.f.,bQueryOnLoad=.f.,bReadOnly=.f.,bAdvOptions=.t.,cMenuFile="",cZoomOnZoom="",bRetriveAllRows=.f.,bQueryOnDblClick=.t.,bNoZoomGridShape=.f.,;
    cEvent = "Esegui",;
    nPag=1;
    , HelpContextID = 246550758


  add object oBtn_1_28 as StdButton with uid="ISKAWSSZTY",left=674, top=9, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Riesegue la ricerca con le nuove selezioni";
    , HelpContextID = 245475350;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_28.Click()
      with this.Parent.oContained
        .notifyevent('Esegui')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_29 as StdButton with uid="ZVARLVJFTI",left=674, top=361, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 192564922;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_29.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_31 as StdButton with uid="XFXJCJDEUZ",left=6, top=361, width=48,height=45,;
    CpPicture="bmp\origine.bmp", caption="", nPag=1;
    , ToolTipText = "Visualizza il documento di origine";
    , HelpContextID = 152309990;
    , Caption='\<Origine';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_31.Click()
      this.parent.oContained.NotifyEvent("APREGEST")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_31.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_SERIAL))
      endwith
    endif
  endfunc


  add object oBtn_1_32 as StdButton with uid="KWTCKIBCGY",left=59, top=361, width=48,height=45,;
    CpPicture="BMP\Modifica.BMP", caption="", nPag=1;
    , ToolTipText = "Modifica il movimento ritenute";
    , HelpContextID = 65388071;
    , Caption='\<Modifica';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_32.Click()
      with this.Parent.oContained
        OPENGEST('M',"GSRI_AMR('"+.w_TIPRIT+"')",'MRSERIAL',.w_SERIAL)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_32.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_SERIAL))
      endwith
    endif
  endfunc

  add object oStr_1_3 as StdString with uid="FMWEBFSOFJ",Visible=.t., Left=31, Top=9,;
    Alignment=1, Width=120, Height=18,;
    Caption="Cliente:"  ;
  , bGlobalFont=.t.

  func oStr_1_3.mHide()
    with this.Parent.oContained
      return (.w_TIPCON='F')
    endwith
  endfunc

  add object oStr_1_4 as StdString with uid="AQUEIZAIYI",Visible=.t., Left=49, Top=9,;
    Alignment=1, Width=102, Height=18,;
    Caption="Fornitore/percip.:"  ;
  , bGlobalFont=.t.

  func oStr_1_4.mHide()
    with this.Parent.oContained
      return (.w_TIPCON='C')
    endwith
  endfunc

  add object oStr_1_16 as StdString with uid="ZFMMCGQFYR",Visible=.t., Left=216, Top=37,;
    Alignment=1, Width=80, Height=18,;
    Caption="Da doc. n.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="PRGEKJAAUK",Visible=.t., Left=227, Top=69,;
    Alignment=1, Width=69, Height=18,;
    Caption="A doc. n.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="SFISTTMXWK",Visible=.t., Left=537, Top=37,;
    Alignment=1, Width=44, Height=18,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="PSDUVBPRJL",Visible=.t., Left=537, Top=69,;
    Alignment=1, Width=44, Height=18,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="WENEBWFHJK",Visible=.t., Left=432, Top=40,;
    Alignment=2, Width=14, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="YZKMSGASPL",Visible=.t., Left=432, Top=69,;
    Alignment=2, Width=14, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="VAQQTVQCJN",Visible=.t., Left=31, Top=37,;
    Alignment=1, Width=120, Height=18,;
    Caption="Da data registrazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="MSYFITATCU",Visible=.t., Left=40, Top=65,;
    Alignment=1, Width=111, Height=18,;
    Caption="A data registrazione:"  ;
  , bGlobalFont=.t.
enddefine
define class tgsri_kmrPag2 as StdContainer
  Width  = 728
  height = 410
  stdWidth  = 728
  stdheight = 410
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oVPCODTRI_2_1 as StdField with uid="NJZWPOVLXD",rtseq=16,rtrep=.f.,;
    cFormVar = "w_VPCODTRI", cQueryName = "VPCODTRI",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice tributo IRPEF",;
    HelpContextID = 212438943,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=145, Top=14, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TRI_BUTI", cZoomOnZoom="GSAR_ATB", oKey_1_1="TRCODTRI", oKey_1_2="this.w_VPCODTRI"

  func oVPCODTRI_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oVPCODTRI_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oVPCODTRI_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TRI_BUTI','*','TRCODTRI',cp_AbsName(this.parent,'oVPCODTRI_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATB',"Tributi",'GSRI1AMR.TRI_BUTI_VZM',this.parent.oContained
  endproc
  proc oVPCODTRI_2_1.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODTRI=this.parent.oContained.w_VPCODTRI
     i_obj.ecpSave()
  endproc

  add object oDESTRI_2_3 as StdField with uid="NPFZBBCZHG",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DESTRI", cQueryName = "DESTRI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 225475786,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=216, Top=14, InputMask=replicate('X',35)

  add object oVPCODTR1_2_5 as StdField with uid="YLGYOXHIKM",rtseq=18,rtrep=.f.,;
    cFormVar = "w_VPCODTR1", cQueryName = "VPCODTR1",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice tributo contributi previdenziali",;
    HelpContextID = 212438919,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=145, Top=42, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TRI_BUTI", cZoomOnZoom="GSAR_ATB", oKey_1_1="TRCODTRI", oKey_1_2="this.w_VPCODTR1"

  func oVPCODTR1_2_5.mHide()
    with this.Parent.oContained
      return (.w_TIPCON='C')
    endwith
  endfunc

  func oVPCODTR1_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oVPCODTR1_2_5.ecpDrop(oSource)
    this.Parent.oContained.link_2_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oVPCODTR1_2_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TRI_BUTI','*','TRCODTRI',cp_AbsName(this.parent,'oVPCODTR1_2_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATB',"Tributi",'GSRI2AMR.TRI_BUTI_VZM',this.parent.oContained
  endproc
  proc oVPCODTR1_2_5.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODTRI=this.parent.oContained.w_VPCODTR1
     i_obj.ecpSave()
  endproc

  add object oDESTRI1_2_6 as StdField with uid="HIEHKSAEDY",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DESTRI1", cQueryName = "DESTRI1",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 225475786,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=216, Top=42, InputMask=replicate('X',35)

  func oDESTRI1_2_6.mHide()
    with this.Parent.oContained
      return (.w_TIPCON='C')
    endwith
  endfunc

  add object oStr_2_2 as StdString with uid="RYBHTIBIYP",Visible=.t., Left=25, Top=14,;
    Alignment=1, Width=117, Height=18,;
    Caption="Codice tributo IRPEF:"  ;
  , bGlobalFont=.t.

  add object oStr_2_4 as StdString with uid="VHAHHSKYKU",Visible=.t., Left=8, Top=42,;
    Alignment=1, Width=134, Height=18,;
    Caption="Codice tributo prev.:"  ;
  , bGlobalFont=.t.

  func oStr_2_4.mHide()
    with this.Parent.oContained
      return (.w_TIPCON='C')
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsri_kmr','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
