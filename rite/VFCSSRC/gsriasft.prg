* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsriasft                                                        *
*              GEN. FILE TELEMATICO 770/2010                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-06-11                                                      *
* Last revis.: 2010-06-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- gsriasft
L_comb1='Persona fisica'
L_comb2='Altri Soggetti'

L_descri1='1) Rappr. legale o negoziale'
L_descri2='2) Rappr. di minore - curatore eredit�'
L_descri3='3) Curatore fallimentare'
L_descri4='4) Commissario liquidatore'
L_descri5='5) Commissario giudiziale'
L_descri6='6) Rappr. fiscale di soggetto non residente'
L_descri7='7) Erede'
L_descri8='8) Liquidatore volontario'
L_descri9='9) Sogg. dich. IVA operaz. straord.'
L_descri10='10) Rappr. fisc. sogg. non resid. D.L.331/1993'
L_descri11='11) Tutore'
L_descri12='12) Liquid. volont. ditta indiv.'
L_descri13='13) Ammin. condominio'
L_descri14='14) Sogg. per conto P.A.'
L_descri15='15) Comm. liquid. P.A.'

g_LoadFuncButton=.F.
* --- Fine Area Manuale
return(createobject("tgsriasft",oParentObject))

* --- Class definition
define class tgsriasft as StdForm
  Top    = 4
  Left   = 23

  * --- Standard Properties
  Width  = 734
  Height = 545+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2010-06-17"
  HelpContextID=40367721
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=171

  * --- Constant Properties
  _IDX = 0
  VALUTE_IDX = 0
  CONTI_IDX = 0
  AZIENDA_IDX = 0
  ESERCIZI_IDX = 0
  SEDIAZIE_IDX = 0
  TITOLARI_IDX = 0
  cPrg = "gsriasft"
  cComment = "GEN. FILE TELEMATICO 770/2010"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODAZI = space(5)
  o_CODAZI = space(5)
  w_PERAZI = space(1)
  o_PERAZI = space(1)
  w_ROWNUM = 0
  w_PERFIS = space(5)
  w_TIPSL = space(2)
  w_SEDILEG = space(5)
  w_TIPFS = space(2)
  w_SEDIFIS = space(5)
  w_CodFisAzi = space(16)
  w_PERAZI2 = space(1)
  w_FOCOGNOME = space(24)
  o_FOCOGNOME = space(24)
  w_FONOME = space(20)
  o_FONOME = space(20)
  w_FODENOMINA = space(60)
  o_FODENOMINA = space(60)
  w_FOCODFIS = space(16)
  o_FOCODFIS = space(16)
  w_FOCODSOS = space(16)
  w_FLAGEURO = space(1)
  w_FLAGEVE = space(2)
  o_FLAGEVE = space(2)
  w_CODEVECC = space(1)
  w_FLAGCONF = space(1)
  w_Forza1 = .F.
  o_Forza1 = .F.
  w_FLAGCOR = space(1)
  o_FLAGCOR = space(1)
  w_FLAGINT = space(1)
  o_FLAGINT = space(1)
  w_FLAGCIPAR = space(1)
  o_FLAGCIPAR = space(1)
  w_TIPOPERAZ = space(1)
  w_AIDATINV   = ctod('  /  /  ')
  w_DATANASC = ctod('  /  /  ')
  o_DATANASC = ctod('  /  /  ')
  w_COMUNE = space(40)
  o_COMUNE = space(40)
  w_SIGLA = space(2)
  o_SIGLA = space(2)
  w_SESSO = space(1)
  o_SESSO = space(1)
  w_VARRES = ctod('  /  /  ')
  w_RECOMUNE = space(40)
  o_RECOMUNE = space(40)
  w_RESIGLA = space(2)
  o_RESIGLA = space(2)
  w_RECODCOM = space(4)
  w_INDIRIZ = space(35)
  o_INDIRIZ = space(35)
  w_CAP = space(5)
  o_CAP = space(5)
  w_CODATT = space(6)
  w_TELEFONO = space(12)
  w_SEVARSED = ctod('  /  /  ')
  w_SECOMUNE = space(40)
  o_SECOMUNE = space(40)
  w_SESIGLA = space(2)
  o_SESIGLA = space(2)
  w_SEINDIRI2 = space(35)
  o_SEINDIRI2 = space(35)
  w_SECAP = space(5)
  o_SECAP = space(5)
  w_SECODCOM = space(4)
  w_SEVARDOM = ctod('  /  /  ')
  w_SERCOMUN = space(40)
  o_SERCOMUN = space(40)
  w_SERSIGLA = space(2)
  o_SERSIGLA = space(2)
  w_SERCODCOM = space(4)
  w_SERCAP = space(5)
  o_SERCAP = space(5)
  w_SERINDIR = space(35)
  o_SERINDIR = space(35)
  w_SECODATT = space(6)
  w_SETELEFONO = space(12)
  w_NATGIU = space(2)
  w_STATO = space(1)
  w_SITUAZ = space(1)
  w_CODFISDA = space(11)
  w_ANNO = space(4)
  w_ESERCIZIO = space(4)
  w_VALUTAESE = space(3)
  w_VALUTA = space(1)
  w_CODVAL = space(3)
  w_decimi = 0
  w_TIPCON = space(1)
  w_RITE = space(1)
  w_TIPCLF = space(1)
  w_COGNOME = space(24)
  w_NOME = space(20)
  o_NOME = space(20)
  w_DENOMINA = space(60)
  w_ONLUS = space(1)
  w_SETATT = space(2)
  w_Forza2 = .F.
  o_Forza2 = .F.
  w_RFCODFIS = space(16)
  w_RFCODCAR = space(2)
  o_RFCODCAR = space(2)
  w_RFDENOMI = space(60)
  o_RFDENOMI = space(60)
  w_RFCOGNOME = space(24)
  o_RFCOGNOME = space(24)
  w_RFNOME = space(20)
  w_RFSESSO = space(1)
  w_RFDATANASC = ctod('  /  /  ')
  w_RFCOMNAS = space(40)
  w_RFSIGNAS = space(2)
  w_RFSIGLA = space(3)
  o_RFSIGLA = space(3)
  w_RFCAP = space(24)
  w_RFCOMUNE = space(24)
  w_RFINDIRIZ = space(35)
  w_RFTELEFONO = space(12)
  w_RFFISSED = space(11)
  w_RFDATCAR = ctod('  /  /  ')
  w_RFDATFAL = ctod('  /  /  ')
  w_SEZ1 = space(1)
  o_SEZ1 = space(1)
  w_qSS1 = space(1)
  w_qST1 = space(1)
  w_qSV1 = space(1)
  w_qSX1 = space(1)
  w_q771 = space(1)
  w_SEZ2 = space(1)
  o_SEZ2 = space(1)
  w_qSS12 = space(1)
  w_SEZ3 = space(1)
  w_qSS13 = space(1)
  w_qST13 = space(1)
  w_qSV13 = space(1)
  w_qSX13 = space(1)
  w_q773 = space(1)
  w_SEZ4 = space(1)
  o_SEZ4 = space(1)
  w_qSS14 = space(1)
  w_qST14 = space(1)
  w_qSX14 = space(1)
  w_q774 = space(1)
  w_RFCFINS2 = space(13)
  w_NUMCERTIF2 = 0
  w_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  o_DATFIN = ctod('  /  /  ')
  w_DirName = space(200)
  w_FileName = space(30)
  w_IDESTA = space(17)
  w_PROIFT = space(6)
  w_TIPFORN = space(2)
  o_TIPFORN = space(2)
  w_FODATANASC = ctod('  /  /  ')
  w_FOSESSO = space(1)
  w_FOCOMUNE = space(40)
  w_FOSIGLA = space(2)
  w_FORECOMUNE = space(40)
  w_FORESIGLA = space(2)
  w_FOCAP = space(5)
  w_FOINDIRIZ = space(35)
  w_FOSECOMUNE = space(40)
  w_FOSESIGLA = space(2)
  w_FOSECAP = space(5)
  w_FOSEINDIRI2 = space(35)
  w_FOSERCOMUN = space(40)
  w_FOSERSIGLA = space(2)
  w_FOSERCAP = space(5)
  w_FOSERINDIR = space(35)
  w_SELCAF = space(10)
  o_SELCAF = space(10)
  w_CFRESCAF = space(16)
  w_CFDELCAF = space(11)
  w_FLAGFIR = space(1)
  w_CODAZI = space(5)
  w_PERCIN = space(15)
  w_DESCINI = space(40)
  w_PERCFIN = space(15)
  w_DESCFIN = space(40)
  w_OBTEST = ctod('  /  /  ')
  w_FIRMDICH = space(1)
  w_FIRMPRES = space(1)
  w_CFINCCON = space(16)
  o_CFINCCON = space(16)
  w_FIRMPRCS = space(1)
  o_FIRMPRCS = space(1)
  w_INVAVTEL = space(1)
  o_INVAVTEL = space(1)
  w_CODSOGG = space(1)
  w_CFPRESCS = space(16)
  o_CFPRESCS = space(16)
  w_NUMCAF = 0
  w_RFDATIMP = ctod('  /  /  ')
  w_CODFISIN = space(16)
  o_CODFISIN = space(16)
  w_IMPTRTEL = space(1)
  o_IMPTRTEL = space(1)
  w_FIRMINT = space(1)
  w_RICAVTEL = space(1)
  w_CFDOMNOT = space(16)
  w_UFDOMNOT = space(60)
  w_CODOMNOT = space(24)
  w_NODOMNOT = space(20)
  w_CMDOMNOT = space(40)
  w_PRDOMNOT = space(2)
  w_CCDOMNOT = space(4)
  w_CPDOMNOT = space(5)
  w_VPDOMNOT = space(15)
  w_INDOMNOT = space(35)
  w_NCDOMNOT = space(10)
  w_FRDOMNOT = space(35)
  w_SEDOMNOT = space(24)
  o_SEDOMNOT = space(24)
  w_CEDOMNOT = space(3)
  w_SFDOMNOT = space(24)
  w_LRDOMNOT = space(24)
  w_IEDOMNOT = space(35)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=5, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsriasftPag1","gsriasft",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pers.fis./altri sogg")
      .Pages(2).addobject("oPag","tgsriasftPag2","gsriasft",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Rapp.firmatario")
      .Pages(3).addobject("oPag","tgsriasftPag3","gsriasft",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Firma / imp. trasmis.")
      .Pages(4).addobject("oPag","tgsriasftPag4","gsriasft",4)
      .Pages(4).oPag.Visible=.t.
      .Pages(4).Caption=cp_Translate("Fornitore file")
      .Pages(5).addobject("oPag","tgsriasftPag5","gsriasft",5)
      .Pages(5).oPag.Visible=.t.
      .Pages(5).Caption=cp_Translate("File telematico")
      .Pages(5).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oFOCOGNOME_1_11
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='VALUTE'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='AZIENDA'
    this.cWorkTables[4]='ESERCIZI'
    this.cWorkTables[5]='SEDIAZIE'
    this.cWorkTables[6]='TITOLARI'
    return(this.OpenAllTables(6))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gsriasft
    *this.opgfrm.tabstyle = 1
    this.opgfrm.page1.caption = 'Persone Fisiche / Altri Soggetti'
    this.opgfrm.page2.caption = 'Rappresentante Firmatario'
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI=space(5)
      .w_PERAZI=space(1)
      .w_ROWNUM=0
      .w_PERFIS=space(5)
      .w_TIPSL=space(2)
      .w_SEDILEG=space(5)
      .w_TIPFS=space(2)
      .w_SEDIFIS=space(5)
      .w_CodFisAzi=space(16)
      .w_PERAZI2=space(1)
      .w_FOCOGNOME=space(24)
      .w_FONOME=space(20)
      .w_FODENOMINA=space(60)
      .w_FOCODFIS=space(16)
      .w_FOCODSOS=space(16)
      .w_FLAGEURO=space(1)
      .w_FLAGEVE=space(2)
      .w_CODEVECC=space(1)
      .w_FLAGCONF=space(1)
      .w_Forza1=.f.
      .w_FLAGCOR=space(1)
      .w_FLAGINT=space(1)
      .w_FLAGCIPAR=space(1)
      .w_TIPOPERAZ=space(1)
      .w_AIDATINV  =ctod("  /  /  ")
      .w_DATANASC=ctod("  /  /  ")
      .w_COMUNE=space(40)
      .w_SIGLA=space(2)
      .w_SESSO=space(1)
      .w_VARRES=ctod("  /  /  ")
      .w_RECOMUNE=space(40)
      .w_RESIGLA=space(2)
      .w_RECODCOM=space(4)
      .w_INDIRIZ=space(35)
      .w_CAP=space(5)
      .w_CODATT=space(6)
      .w_TELEFONO=space(12)
      .w_SEVARSED=ctod("  /  /  ")
      .w_SECOMUNE=space(40)
      .w_SESIGLA=space(2)
      .w_SEINDIRI2=space(35)
      .w_SECAP=space(5)
      .w_SECODCOM=space(4)
      .w_SEVARDOM=ctod("  /  /  ")
      .w_SERCOMUN=space(40)
      .w_SERSIGLA=space(2)
      .w_SERCODCOM=space(4)
      .w_SERCAP=space(5)
      .w_SERINDIR=space(35)
      .w_SECODATT=space(6)
      .w_SETELEFONO=space(12)
      .w_NATGIU=space(2)
      .w_STATO=space(1)
      .w_SITUAZ=space(1)
      .w_CODFISDA=space(11)
      .w_ANNO=space(4)
      .w_ESERCIZIO=space(4)
      .w_VALUTAESE=space(3)
      .w_VALUTA=space(1)
      .w_CODVAL=space(3)
      .w_decimi=0
      .w_TIPCON=space(1)
      .w_RITE=space(1)
      .w_TIPCLF=space(1)
      .w_COGNOME=space(24)
      .w_NOME=space(20)
      .w_DENOMINA=space(60)
      .w_ONLUS=space(1)
      .w_SETATT=space(2)
      .w_Forza2=.f.
      .w_RFCODFIS=space(16)
      .w_RFCODCAR=space(2)
      .w_RFDENOMI=space(60)
      .w_RFCOGNOME=space(24)
      .w_RFNOME=space(20)
      .w_RFSESSO=space(1)
      .w_RFDATANASC=ctod("  /  /  ")
      .w_RFCOMNAS=space(40)
      .w_RFSIGNAS=space(2)
      .w_RFSIGLA=space(3)
      .w_RFCAP=space(24)
      .w_RFCOMUNE=space(24)
      .w_RFINDIRIZ=space(35)
      .w_RFTELEFONO=space(12)
      .w_RFFISSED=space(11)
      .w_RFDATCAR=ctod("  /  /  ")
      .w_RFDATFAL=ctod("  /  /  ")
      .w_SEZ1=space(1)
      .w_qSS1=space(1)
      .w_qST1=space(1)
      .w_qSV1=space(1)
      .w_qSX1=space(1)
      .w_q771=space(1)
      .w_SEZ2=space(1)
      .w_qSS12=space(1)
      .w_SEZ3=space(1)
      .w_qSS13=space(1)
      .w_qST13=space(1)
      .w_qSV13=space(1)
      .w_qSX13=space(1)
      .w_q773=space(1)
      .w_SEZ4=space(1)
      .w_qSS14=space(1)
      .w_qST14=space(1)
      .w_qSX14=space(1)
      .w_q774=space(1)
      .w_RFCFINS2=space(13)
      .w_NUMCERTIF2=0
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_DirName=space(200)
      .w_FileName=space(30)
      .w_IDESTA=space(17)
      .w_PROIFT=space(6)
      .w_TIPFORN=space(2)
      .w_FODATANASC=ctod("  /  /  ")
      .w_FOSESSO=space(1)
      .w_FOCOMUNE=space(40)
      .w_FOSIGLA=space(2)
      .w_FORECOMUNE=space(40)
      .w_FORESIGLA=space(2)
      .w_FOCAP=space(5)
      .w_FOINDIRIZ=space(35)
      .w_FOSECOMUNE=space(40)
      .w_FOSESIGLA=space(2)
      .w_FOSECAP=space(5)
      .w_FOSEINDIRI2=space(35)
      .w_FOSERCOMUN=space(40)
      .w_FOSERSIGLA=space(2)
      .w_FOSERCAP=space(5)
      .w_FOSERINDIR=space(35)
      .w_SELCAF=space(10)
      .w_CFRESCAF=space(16)
      .w_CFDELCAF=space(11)
      .w_FLAGFIR=space(1)
      .w_CODAZI=space(5)
      .w_PERCIN=space(15)
      .w_DESCINI=space(40)
      .w_PERCFIN=space(15)
      .w_DESCFIN=space(40)
      .w_OBTEST=ctod("  /  /  ")
      .w_FIRMDICH=space(1)
      .w_FIRMPRES=space(1)
      .w_CFINCCON=space(16)
      .w_FIRMPRCS=space(1)
      .w_INVAVTEL=space(1)
      .w_CODSOGG=space(1)
      .w_CFPRESCS=space(16)
      .w_NUMCAF=0
      .w_RFDATIMP=ctod("  /  /  ")
      .w_CODFISIN=space(16)
      .w_IMPTRTEL=space(1)
      .w_FIRMINT=space(1)
      .w_RICAVTEL=space(1)
      .w_CFDOMNOT=space(16)
      .w_UFDOMNOT=space(60)
      .w_CODOMNOT=space(24)
      .w_NODOMNOT=space(20)
      .w_CMDOMNOT=space(40)
      .w_PRDOMNOT=space(2)
      .w_CCDOMNOT=space(4)
      .w_CPDOMNOT=space(5)
      .w_VPDOMNOT=space(15)
      .w_INDOMNOT=space(35)
      .w_NCDOMNOT=space(10)
      .w_FRDOMNOT=space(35)
      .w_SEDOMNOT=space(24)
      .w_CEDOMNOT=space(3)
      .w_SFDOMNOT=space(24)
      .w_LRDOMNOT=space(24)
      .w_IEDOMNOT=space(35)
        .w_CODAZI = i_codazi
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODAZI))
          .link_1_1('Full')
        endif
          .DoRTCalc(2,2,.f.)
        .w_ROWNUM = 1
        .w_PERFIS = iif(.w_PERAZI='S',i_CODAZI,' ')
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_PERFIS))
          .link_1_4('Full')
        endif
        .w_TIPSL = 'SL'
        .w_SEDILEG = iif(.w_PERAZI<>'S',i_CODAZI,' ')
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_SEDILEG))
          .link_1_6('Full')
        endif
        .w_TIPFS = 'DF'
        .w_SEDIFIS = iif(.w_PERAZI<>'S',i_CODAZI,' ')
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_SEDIFIS))
          .link_1_8('Full')
        endif
          .DoRTCalc(9,9,.f.)
        .w_PERAZI2 = IsPerFis(.w_FOCOGNOME,.w_FONOME,.w_FODENOMINA)
        .w_FOCOGNOME = iif(.w_Perazi='S',left(.w_FOCOGNOME+SPACE(24),24),space(24))
        .w_FONOME = iif(.w_Perazi='S',left(.w_FONOME+SPACE(20),20),space(20))
        .w_FODENOMINA = iif(.w_Perazi<>'S',left(.w_FODENOMINA+space(60),60),space(60))
        .w_FOCODFIS = iif(empty(.w_FOCODFIS),.w_CodFisAzi,.w_FOCODFIS)
          .DoRTCalc(15,15,.f.)
        .w_FLAGEURO = '1'
        .w_FLAGEVE = '00'
        .w_CODEVECC = iif(.w_FLAGEVE='01',.w_CODEVECC,' ')
        .w_FLAGCONF = '0'
        .w_Forza1 = iif(.w_Perazi<>'S',.w_Forza1,.f.)
        .w_FLAGCOR = iif(.w_FLAGINT='1','0',.w_FLAGCOR)
        .w_FLAGINT = iif(.w_FLAGCOR='1','0',.w_FLAGINT)
        .w_FLAGCIPAR = iif(.w_FLAGINT='1' or .w_FLAGCOR='1',.w_FLAGCIPAR,'0')
        .w_TIPOPERAZ = ' '
        .w_AIDATINV   = i_DATSYS
        .w_DATANASC = iif(.w_Perazi='S' or .w_Forza1,.w_DATANASC,CTOD('  -  -  '))
        .w_COMUNE = iif(.w_Perazi='S' or .w_Forza1,left(.w_COMUNE+space(40),40),space(40))
        .w_SIGLA = iif(.w_Perazi='S' or .w_Forza1,.w_SIGLA,'  ')
        .w_SESSO = iif(.w_Perazi='S' or .w_Forza1,.w_SESSO,' ')
        .w_VARRES = iif(.w_Perazi='S' or .w_Forza1,.w_VARRES,CTOD('  -  -  '))
        .w_RECOMUNE = iif(.w_Perazi='S' or .w_Forza1,left(.w_RECOMUNE+space(40),40),space(40))
        .w_RESIGLA = iif(.w_Perazi='S' or .w_Forza1,.w_RESIGLA,'  ')
        .w_RECODCOM = iif(.w_Perazi='S' or .w_Forza1,.w_RECODCOM,Space(4))
        .w_INDIRIZ = iif(.w_Perazi='S' or .w_Forza1,left(.w_INDIRIZ+space(35),35),space(35))
        .w_CAP = iif(.w_Perazi='S' or .w_Forza1,.w_CAP,space(5))
        .w_CODATT = iif(.w_Perazi='S' or .w_Forza1,IIF(EMPTY(.w_CODATT),IIF(g_ATTIVI='S', space(6), LEFT(Alltrim(CALATTSO(g_CATAZI,Alltrim(STR(year(i_DATSYS))),12)),6)),.w_CODATT),SPACE(5))
        .w_TELEFONO = iif(.w_Perazi='S' or .w_Forza1,left(.w_TELEFONO,12),space(12))
        .w_SEVARSED = iif(.w_Perazi<>'S' or .w_Forza1,.w_SEVARSED,CTOD('  -  -  '))
        .w_SECOMUNE = iif(.w_Perazi<>'S' or .w_Forza1,left(.w_SECOMUNE+space(40),40),space(40))
        .w_SESIGLA = iif(.w_Perazi<>'S' or .w_Forza1,.w_SESIGLA,'  ')
        .w_SEINDIRI2 = iif(.w_Perazi<>'S' or .w_Forza1,left(.w_SEINDIRI2+space(35),35),space(35))
        .w_SECAP = iif(.w_Perazi<>'S' or .w_Forza1,.w_SECAP,space(5))
        .w_SECODCOM = iif(.w_Perazi<>'S' or .w_Forza1,.w_SECODCOM,Space(4))
        .w_SEVARDOM = iif(.w_Perazi<>'S' or .w_Forza1,.w_SEVARDOM,CTOD('  -  -  '))
        .w_SERCOMUN = iif(.w_Perazi<>'S' or .w_Forza1,left(.w_SERCOMUN+space(40),40),space(40))
        .w_SERSIGLA = iif(.w_Perazi<>'S' or .w_Forza1,.w_SERSIGLA,'  ')
        .w_SERCODCOM = iif(.w_Perazi<>'S' or .w_Forza1,.w_SERCODCOM,Space(4))
        .w_SERCAP = iif(.w_Perazi<>'S' or .w_Forza1,.w_SERCAP,space(5))
        .w_SERINDIR = iif(.w_Perazi<>'S' or .w_Forza1,left(.w_SERINDIR+space(35),35),space(35))
        .w_SECODATT = iif(.w_Perazi<>'S' or .w_Forza1,IIF(EMPTY(.w_SECODATT),IIF(g_ATTIVI='S', space(6), LEFT(Alltrim(CALATTSO(g_CATAZI,Alltrim(STR(year(i_DATSYS))),12)),6)),.w_SECODATT),space(5))
        .w_SETELEFONO = iif(.w_Perazi<>'S' or .w_Forza1,left(.w_SETELEFONO,12),space(12))
        .w_NATGIU = iif(.w_Perazi<>'S' or .w_Forza1,.w_NATGIU,'  ')
        .w_STATO = iif(.w_Perazi<>'S' or .w_Forza1,.w_STATO,' ')
        .w_SITUAZ = iif(.w_Perazi<>'S' or .w_Forza1,.w_SITUAZ,' ')
        .w_CODFISDA = iif(.w_Perazi<>'S' or .w_Forza1,.w_CODFISDA,space(11))
        .w_ANNO = STR(YEAR(i_DATSYS),4,0)
        .w_ESERCIZIO = calceser(.w_DATFIN)
        .DoRTCalc(57,57,.f.)
        if not(empty(.w_ESERCIZIO))
          .link_1_57('Full')
        endif
          .DoRTCalc(58,58,.f.)
        .w_VALUTA = IIF(.w_VALUTAESE=g_CODLIR,'L','E')
        .w_CODVAL = iif(.w_flageuro='0',g_codlir,g_codeur)
        .DoRTCalc(60,60,.f.)
        if not(empty(.w_CODVAL))
          .link_1_60('Full')
        endif
          .DoRTCalc(61,61,.f.)
        .w_TIPCON = 'F'
          .DoRTCalc(63,67,.f.)
        .w_ONLUS = iif(.w_Perazi<>'S' or .w_Forza1,.w_ONLUS,' ')
        .w_SETATT = iif(.w_Perazi<>'S' or .w_Forza1,.w_SETATT,'  ')
        .w_Forza2 = iif(.w_Perazi<>'S',.w_Forza2,.f.)
        .w_RFCODFIS = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFCODFIS,space(16))
        .w_RFCODCAR = iif(.w_Perazi<>'S' or .w_Forza2,'1','  ')
        .w_RFDENOMI = iif(empty(.w_RFCOGNOME) and (.w_Perazi<>'S' or .w_Forza2),.w_RFDENOMI,space(60))
        .w_RFCOGNOME = iif(empty(.w_RFDENOMI) and (.w_Perazi<>'S' or .w_Forza2) ,.w_RFCOGNOME,space(24))
        .w_RFNOME = iif(empty(.w_RFDENOMI) and (.w_Perazi<>'S' or .w_Forza2),.w_RFNOME,space(20))
        .w_RFSESSO = iif(empty(.w_RFDENOMI) and (.w_Perazi<>'S' or .w_Forza2),.w_RFSESSO,' ')
        .w_RFDATANASC = iif(empty(.w_RFDENOMI) and (.w_Perazi<>'S' or .w_Forza2),.w_RFDATANASC,CTOD('  -  -  '))
        .w_RFCOMNAS = iif(empty(.w_RFDENOMI) and (.w_Perazi<>'S' or .w_Forza2),.w_RFCOMNAS,space(40))
        .w_RFSIGNAS = iif(empty(.w_RFDENOMI) and (.w_Perazi<>'S' or .w_Forza2),.w_RFSIGNAS,'  ')
        .w_RFSIGLA = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFSIGLA,space(3))
        .w_RFCAP = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFCAP,space(24))
        .w_RFCOMUNE = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFCOMUNE,space(40))
        .w_RFINDIRIZ = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFINDIRIZ,space(35))
        .w_RFTELEFONO = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFTELEFONO,space(12))
        .w_RFFISSED = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFFISSED,space(11))
        .w_RFDATCAR = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFDATCAR,CTOD('  -  -  '))
        .w_RFDATFAL = iif(inlist(.w_RFCODCAR,'3','4'),.w_RFDATFAL,ctod('  -  -    '))
        .w_SEZ1 = iif(.w_SEZ2='1' or .w_SEZ4='1','0','1')
        .w_qSS1 = iif(.w_SEZ1='1',.w_qSS1,'0')
        .w_qST1 = iif(.w_SEZ1='1','1','0')
        .w_qSV1 = iif(.w_SEZ1='1',.w_qSV1,'0')
        .w_qSX1 = iif(.w_SEZ1='1',.w_qSX1,'0')
        .w_q771 = '0'
        .w_SEZ2 = iif(.w_SEZ1='1' or .w_SEZ4='1','0','1')
        .w_qSS12 = iif(.w_SEZ2='1',.w_qSS12,'0')
        .w_SEZ3 = '0'
        .w_qSS13 = '0'
        .w_qST13 = '0'
        .w_qSV13 = '0'
        .w_qSX13 = '0'
        .w_q773 = '0'
        .w_SEZ4 = iif(.w_SEZ1='1' or .w_SEZ2='1','0','1')
        .w_qSS14 = iif(.w_SEZ4='1',.w_qSS14,'0')
        .w_qST14 = iif(.w_SEZ4='1','1','0')
        .w_qSX14 = iif(.w_SEZ4='1',.w_qSX14,'0')
        .w_q774 = '0'
        .w_RFCFINS2 = iif(.w_SEZ4='1',.w_RFCFINS2,space(13))
        .w_NUMCERTIF2 = 0
        .w_DATINI = CTOD("01-01-2009")
        .w_DATFIN = CTOD("31-12-2009")
        .w_DirName = sys(5)+sys(2003)+'\'
        .w_FileName = alltrim(.w_FOCODFIS)+'_77S10.77S'
        .w_IDESTA = ' '
        .w_PROIFT = ' '
        .w_TIPFORN = '01'
        .w_FODATANASC = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI='S',.w_DATANASC,CTOD('  -  -  '))
        .w_FOSESSO = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI='S',.w_SESSO,' ')
        .w_FOCOMUNE = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI='S',.w_COMUNE,space(40))
        .w_FOSIGLA = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI='S',.w_SIGLA,'  ')
        .w_FORECOMUNE = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI='S',.w_RECOMUNE,space(40))
        .w_FORESIGLA = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI='S',.w_RESIGLA,'  ')
        .w_FOCAP = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI='S',.w_CAP,space(5))
        .w_FOINDIRIZ = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI='S',.w_INDIRIZ,space(35))
        .w_FOSECOMUNE = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI<>'S',.w_SECOMUNE,space(40))
        .w_FOSESIGLA = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI<>'S',.w_SESIGLA,'  ')
        .w_FOSECAP = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI<>'S',.w_SECAP,space(5))
        .w_FOSEINDIRI2 = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI<>'S',.w_SEINDIRI2,space(35))
        .w_FOSERCOMUN = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI<>'S',.w_SERCOMUN,space(40))
        .w_FOSERSIGLA = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI<>'S',.w_SERSIGLA,'  ')
        .w_FOSERCAP = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI<>'S',.w_SERCAP,space(5))
        .w_FOSERINDIR = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI<>'S',.w_SERINDIR,space(35))
        .w_SELCAF = '1'
        .w_CFRESCAF = SPACE(16)
        .w_CFDELCAF = iif(.w_SELCAF <> '1',SPACE(11),.w_CFDELCAF)
        .w_FLAGFIR = '0'
        .w_CODAZI = i_codazi
        .DoRTCalc(137,137,.f.)
        if not(empty(.w_PERCIN))
          .link_5_24('Full')
        endif
        .DoRTCalc(138,139,.f.)
        if not(empty(.w_PERCFIN))
          .link_5_26('Full')
        endif
          .DoRTCalc(140,140,.f.)
        .w_OBTEST = .w_DATINI
        .w_FIRMDICH = iif(.w_SEZ1='1',.w_FIRMDICH,'0')
        .w_FIRMPRES = '0'
          .DoRTCalc(144,144,.f.)
        .w_FIRMPRCS = iif(!empty(.w_CFPRESCS),'1','0')
        .w_INVAVTEL = iif(.w_IMPTRTEL <> '0',.w_INVAVTEL,'0')
        .w_CODSOGG = iif(! empty(.w_CFINCCON),.w_CODSOGG,'0')
        .w_CFPRESCS = iif(.w_FIRMPRCS='1',.w_CFPRESCS,space(16))
        .w_NUMCAF = 0
        .w_RFDATIMP = CTOD('  -  -  ')
        .w_CODFISIN = space(16)
        .w_IMPTRTEL = '0'
        .w_FIRMINT = '0'
        .w_RICAVTEL = iif(.w_INVAVTEL='1',.w_RICAVTEL,'0')
        .w_CFDOMNOT = SPACE(16)
        .w_UFDOMNOT = space(60)
        .w_CODOMNOT = space(24)
        .w_NODOMNOT = space(20)
        .w_CMDOMNOT = iif(! empty(.w_SEDOMNOT),space(40),.w_CMDOMNOT)
        .w_PRDOMNOT = iif(! empty(.w_SEDOMNOT),space(2),.w_PRDOMNOT)
        .w_CCDOMNOT = iif(! empty(.w_SEDOMNOT),space(4),.w_CCDOMNOT)
        .w_CPDOMNOT = iif(! empty(.w_SEDOMNOT),space(5),.w_CPDOMNOT)
        .w_VPDOMNOT = iif(! empty(.w_SEDOMNOT),space(15),.w_VPDOMNOT)
        .w_INDOMNOT = iif(! empty(.w_SEDOMNOT),space(35),.w_INDOMNOT)
        .w_NCDOMNOT = iif(! empty(.w_SEDOMNOT),space(10),.w_NCDOMNOT)
        .w_FRDOMNOT = iif(! empty(.w_SEDOMNOT),space(35),.w_FRDOMNOT)
        .w_SEDOMNOT = space(24)
        .w_CEDOMNOT = iif(empty(.w_SEDOMNOT),space(3),.w_CEDOMNOT)
        .w_SFDOMNOT = iif(empty(.w_SEDOMNOT),space(24),.w_SFDOMNOT)
        .w_LRDOMNOT = iif(empty(.w_SEDOMNOT),space(24),.w_LRDOMNOT)
        .w_IEDOMNOT = iif(empty(.w_SEDOMNOT),space(35),.w_IEDOMNOT)
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page5.oPag.oBtn_5_4.enabled = this.oPgFrm.Page5.oPag.oBtn_5_4.mCond()
    this.oPgFrm.Page5.oPag.oBtn_5_20.enabled = this.oPgFrm.Page5.oPag.oBtn_5_20.mCond()
    this.oPgFrm.Page5.oPag.oBtn_5_21.enabled = this.oPgFrm.Page5.oPag.oBtn_5_21.mCond()
    this.oPgFrm.Page5.oPag.oBtn_5_22.enabled = this.oPgFrm.Page5.oPag.oBtn_5_22.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        if .o_CODAZI<>.w_CODAZI
          .link_1_1('Full')
        endif
        .DoRTCalc(2,3,.t.)
        if .o_PERAZI<>.w_PERAZI
            .w_PERFIS = iif(.w_PERAZI='S',i_CODAZI,' ')
          .link_1_4('Full')
        endif
        .DoRTCalc(5,5,.t.)
        if .o_PERAZI<>.w_PERAZI
            .w_SEDILEG = iif(.w_PERAZI<>'S',i_CODAZI,' ')
          .link_1_6('Full')
        endif
        .DoRTCalc(7,7,.t.)
        if .o_PERAZI<>.w_PERAZI
            .w_SEDIFIS = iif(.w_PERAZI<>'S',i_CODAZI,' ')
          .link_1_8('Full')
        endif
        .DoRTCalc(9,9,.t.)
            .w_PERAZI2 = IsPerFis(.w_FOCOGNOME,.w_FONOME,.w_FODENOMINA)
        if .o_PERAZI<>.w_PERAZI
            .w_FOCOGNOME = iif(.w_Perazi='S',left(.w_FOCOGNOME+SPACE(24),24),space(24))
        endif
        if .o_PERAZI<>.w_PERAZI
            .w_FONOME = iif(.w_Perazi='S',left(.w_FONOME+SPACE(20),20),space(20))
        endif
        if .o_PERAZI<>.w_PERAZI
            .w_FODENOMINA = iif(.w_Perazi<>'S',left(.w_FODENOMINA+space(60),60),space(60))
        endif
        if .o_FOCODFIS<>.w_FOCODFIS
            .w_FOCODFIS = iif(empty(.w_FOCODFIS),.w_CodFisAzi,.w_FOCODFIS)
        endif
        .DoRTCalc(15,17,.t.)
        if .o_FLAGEVE<>.w_FLAGEVE
            .w_CODEVECC = iif(.w_FLAGEVE='01',.w_CODEVECC,' ')
        endif
        .DoRTCalc(19,19,.t.)
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA
            .w_Forza1 = iif(.w_Perazi<>'S',.w_Forza1,.f.)
        endif
        if .o_FLAGINT<>.w_FLAGINT
            .w_FLAGCOR = iif(.w_FLAGINT='1','0',.w_FLAGCOR)
        endif
        if .o_FLAGCOR<>.w_FLAGCOR
            .w_FLAGINT = iif(.w_FLAGCOR='1','0',.w_FLAGINT)
        endif
        if .o_FLAGINT<>.w_FLAGINT.or. .o_FLAGCOR<>.w_FLAGCOR
            .w_FLAGCIPAR = iif(.w_FLAGINT='1' or .w_FLAGCOR='1',.w_FLAGCIPAR,'0')
        endif
        if .o_FLAGCIPAR<>.w_FLAGCIPAR
            .w_TIPOPERAZ = ' '
        endif
        .DoRTCalc(25,25,.t.)
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_DATANASC = iif(.w_Perazi='S' or .w_Forza1,.w_DATANASC,CTOD('  -  -  '))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_COMUNE = iif(.w_Perazi='S' or .w_Forza1,left(.w_COMUNE+space(40),40),space(40))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_SIGLA = iif(.w_Perazi='S' or .w_Forza1,.w_SIGLA,'  ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_NOME<>.w_NOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_SESSO = iif(.w_Perazi='S' or .w_Forza1,.w_SESSO,' ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_VARRES = iif(.w_Perazi='S' or .w_Forza1,.w_VARRES,CTOD('  -  -  '))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_RECOMUNE = iif(.w_Perazi='S' or .w_Forza1,left(.w_RECOMUNE+space(40),40),space(40))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_RESIGLA = iif(.w_Perazi='S' or .w_Forza1,.w_RESIGLA,'  ')
        endif
        if .o_Forza1<>.w_Forza1
            .w_RECODCOM = iif(.w_Perazi='S' or .w_Forza1,.w_RECODCOM,Space(4))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_INDIRIZ = iif(.w_Perazi='S' or .w_Forza1,left(.w_INDIRIZ+space(35),35),space(35))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_CAP = iif(.w_Perazi='S' or .w_Forza1,.w_CAP,space(5))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_CODATT = iif(.w_Perazi='S' or .w_Forza1,IIF(EMPTY(.w_CODATT),IIF(g_ATTIVI='S', space(6), LEFT(Alltrim(CALATTSO(g_CATAZI,Alltrim(STR(year(i_DATSYS))),12)),6)),.w_CODATT),SPACE(5))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_TELEFONO = iif(.w_Perazi='S' or .w_Forza1,left(.w_TELEFONO,12),space(12))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_SEVARSED = iif(.w_Perazi<>'S' or .w_Forza1,.w_SEVARSED,CTOD('  -  -  '))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_SECOMUNE = iif(.w_Perazi<>'S' or .w_Forza1,left(.w_SECOMUNE+space(40),40),space(40))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_SESIGLA = iif(.w_Perazi<>'S' or .w_Forza1,.w_SESIGLA,'  ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA
            .w_SEINDIRI2 = iif(.w_Perazi<>'S' or .w_Forza1,left(.w_SEINDIRI2+space(35),35),space(35))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_SECAP = iif(.w_Perazi<>'S' or .w_Forza1,.w_SECAP,space(5))
        endif
        if .o_Forza1<>.w_Forza1
            .w_SECODCOM = iif(.w_Perazi<>'S' or .w_Forza1,.w_SECODCOM,Space(4))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_SEVARDOM = iif(.w_Perazi<>'S' or .w_Forza1,.w_SEVARDOM,CTOD('  -  -  '))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_SERCOMUN = iif(.w_Perazi<>'S' or .w_Forza1,left(.w_SERCOMUN+space(40),40),space(40))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_SERSIGLA = iif(.w_Perazi<>'S' or .w_Forza1,.w_SERSIGLA,'  ')
        endif
        if .o_Forza1<>.w_Forza1
            .w_SERCODCOM = iif(.w_Perazi<>'S' or .w_Forza1,.w_SERCODCOM,Space(4))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_SERCAP = iif(.w_Perazi<>'S' or .w_Forza1,.w_SERCAP,space(5))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_SERINDIR = iif(.w_Perazi<>'S' or .w_Forza1,left(.w_SERINDIR+space(35),35),space(35))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_SECODATT = iif(.w_Perazi<>'S' or .w_Forza1,IIF(EMPTY(.w_SECODATT),IIF(g_ATTIVI='S', space(6), LEFT(Alltrim(CALATTSO(g_CATAZI,Alltrim(STR(year(i_DATSYS))),12)),6)),.w_SECODATT),space(5))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_SETELEFONO = iif(.w_Perazi<>'S' or .w_Forza1,left(.w_SETELEFONO,12),space(12))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_NATGIU = iif(.w_Perazi<>'S' or .w_Forza1,.w_NATGIU,'  ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_STATO = iif(.w_Perazi<>'S' or .w_Forza1,.w_STATO,' ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_SITUAZ = iif(.w_Perazi<>'S' or .w_Forza1,.w_SITUAZ,' ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_CODFISDA = iif(.w_Perazi<>'S' or .w_Forza1,.w_CODFISDA,space(11))
        endif
        .DoRTCalc(56,56,.t.)
        if .o_DATFIN<>.w_DATFIN
            .w_ESERCIZIO = calceser(.w_DATFIN)
          .link_1_57('Full')
        endif
        .DoRTCalc(58,58,.t.)
            .w_VALUTA = IIF(.w_VALUTAESE=g_CODLIR,'L','E')
            .w_CODVAL = iif(.w_flageuro='0',g_codlir,g_codeur)
          .link_1_60('Full')
        .DoRTCalc(61,67,.t.)
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_ONLUS = iif(.w_Perazi<>'S' or .w_Forza1,.w_ONLUS,' ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_SETATT = iif(.w_Perazi<>'S' or .w_Forza1,.w_SETATT,'  ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA
            .w_Forza2 = iif(.w_Perazi<>'S',.w_Forza2,.f.)
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza2<>.w_Forza2
            .w_RFCODFIS = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFCODFIS,space(16))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza2<>.w_Forza2
            .w_RFCODCAR = iif(.w_Perazi<>'S' or .w_Forza2,'1','  ')
        endif
        if .o_RFCOGNOME<>.w_RFCOGNOME.or. .o_Forza2<>.w_Forza2
            .w_RFDENOMI = iif(empty(.w_RFCOGNOME) and (.w_Perazi<>'S' or .w_Forza2),.w_RFDENOMI,space(60))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza2<>.w_Forza2.or. .o_RFDENOMI<>.w_RFDENOMI
            .w_RFCOGNOME = iif(empty(.w_RFDENOMI) and (.w_Perazi<>'S' or .w_Forza2) ,.w_RFCOGNOME,space(24))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza2<>.w_Forza2.or. .o_RFDENOMI<>.w_RFDENOMI
            .w_RFNOME = iif(empty(.w_RFDENOMI) and (.w_Perazi<>'S' or .w_Forza2),.w_RFNOME,space(20))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza2<>.w_Forza2.or. .o_RFDENOMI<>.w_RFDENOMI
            .w_RFSESSO = iif(empty(.w_RFDENOMI) and (.w_Perazi<>'S' or .w_Forza2),.w_RFSESSO,' ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza2<>.w_Forza2.or. .o_RFDENOMI<>.w_RFDENOMI
            .w_RFDATANASC = iif(empty(.w_RFDENOMI) and (.w_Perazi<>'S' or .w_Forza2),.w_RFDATANASC,CTOD('  -  -  '))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza2<>.w_Forza2.or. .o_RFDENOMI<>.w_RFDENOMI
            .w_RFCOMNAS = iif(empty(.w_RFDENOMI) and (.w_Perazi<>'S' or .w_Forza2),.w_RFCOMNAS,space(40))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza2<>.w_Forza2.or. .o_RFDENOMI<>.w_RFDENOMI
            .w_RFSIGNAS = iif(empty(.w_RFDENOMI) and (.w_Perazi<>'S' or .w_Forza2),.w_RFSIGNAS,'  ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza2<>.w_Forza2.or. .o_RFDENOMI<>.w_RFDENOMI
            .w_RFSIGLA = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFSIGLA,space(3))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza2<>.w_Forza2.or. .o_RFDENOMI<>.w_RFDENOMI
            .w_RFCAP = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFCAP,space(24))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza2<>.w_Forza2.or. .o_RFDENOMI<>.w_RFDENOMI
            .w_RFCOMUNE = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFCOMUNE,space(40))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza2<>.w_Forza2.or. .o_RFDENOMI<>.w_RFDENOMI
            .w_RFINDIRIZ = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFINDIRIZ,space(35))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza2<>.w_Forza2.or. .o_RFDENOMI<>.w_RFDENOMI
            .w_RFTELEFONO = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFTELEFONO,space(12))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_RFFISSED = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFFISSED,space(11))
        endif
        if .o_Forza2<>.w_Forza2
            .w_RFDATCAR = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFDATCAR,CTOD('  -  -  '))
        endif
        if .o_Forza2<>.w_Forza2.or. .o_RFCODCAR<>.w_RFCODCAR
            .w_RFDATFAL = iif(inlist(.w_RFCODCAR,'3','4'),.w_RFDATFAL,ctod('  -  -    '))
        endif
        if .o_SEZ2<>.w_SEZ2.or. .o_SEZ4<>.w_SEZ4
            .w_SEZ1 = iif(.w_SEZ2='1' or .w_SEZ4='1','0','1')
        endif
        if .o_SEZ1<>.w_SEZ1.or. .o_SEZ2<>.w_SEZ2.or. .o_SEZ4<>.w_SEZ4
            .w_qSS1 = iif(.w_SEZ1='1',.w_qSS1,'0')
        endif
        if .o_SEZ1<>.w_SEZ1.or. .o_SEZ2<>.w_SEZ2.or. .o_SEZ4<>.w_SEZ4
            .w_qST1 = iif(.w_SEZ1='1','1','0')
        endif
        if .o_SEZ1<>.w_SEZ1.or. .o_SEZ2<>.w_SEZ2.or. .o_SEZ4<>.w_SEZ4
            .w_qSV1 = iif(.w_SEZ1='1',.w_qSV1,'0')
        endif
        if .o_SEZ1<>.w_SEZ1.or. .o_SEZ2<>.w_SEZ2.or. .o_SEZ4<>.w_SEZ4
            .w_qSX1 = iif(.w_SEZ1='1',.w_qSX1,'0')
        endif
        if .o_SEZ1<>.w_SEZ1.or. .o_SEZ2<>.w_SEZ2.or. .o_SEZ4<>.w_SEZ4
            .w_q771 = '0'
        endif
        if .o_SEZ1<>.w_SEZ1.or. .o_SEZ4<>.w_SEZ4
            .w_SEZ2 = iif(.w_SEZ1='1' or .w_SEZ4='1','0','1')
        endif
        if .o_SEZ1<>.w_SEZ1.or. .o_SEZ2<>.w_SEZ2.or. .o_SEZ4<>.w_SEZ4
            .w_qSS12 = iif(.w_SEZ2='1',.w_qSS12,'0')
        endif
        .DoRTCalc(96,100,.t.)
        if .o_SEZ1<>.w_SEZ1.or. .o_SEZ2<>.w_SEZ2.or. .o_SEZ4<>.w_SEZ4
            .w_q773 = '0'
        endif
        if .o_SEZ1<>.w_SEZ1.or. .o_SEZ2<>.w_SEZ2
            .w_SEZ4 = iif(.w_SEZ1='1' or .w_SEZ2='1','0','1')
        endif
        if .o_SEZ1<>.w_SEZ1.or. .o_SEZ2<>.w_SEZ2.or. .o_SEZ4<>.w_SEZ4
            .w_qSS14 = iif(.w_SEZ4='1',.w_qSS14,'0')
        endif
        if .o_SEZ2<>.w_SEZ2.or. .o_SEZ1<>.w_SEZ1.or. .o_SEZ4<>.w_SEZ4
            .w_qST14 = iif(.w_SEZ4='1','1','0')
        endif
        if .o_SEZ2<>.w_SEZ2.or. .o_SEZ1<>.w_SEZ1.or. .o_SEZ4<>.w_SEZ4
            .w_qSX14 = iif(.w_SEZ4='1',.w_qSX14,'0')
        endif
        if .o_SEZ1<>.w_SEZ1.or. .o_SEZ2<>.w_SEZ2.or. .o_SEZ4<>.w_SEZ4
            .w_q774 = '0'
        endif
        if .o_SEZ2<>.w_SEZ2.or. .o_SEZ1<>.w_SEZ1
            .w_RFCFINS2 = iif(.w_SEZ4='1',.w_RFCFINS2,space(13))
        endif
        .DoRTCalc(108,112,.t.)
        if .o_FLAGCOR<>.w_FLAGCOR.or. .o_FLAGINT<>.w_FLAGINT
            .w_IDESTA = ' '
        endif
        if .o_FLAGCOR<>.w_FLAGCOR.or. .o_FLAGINT<>.w_FLAGINT
            .w_PROIFT = ' '
        endif
        .DoRTCalc(115,115,.t.)
        if .o_TIPFORN<>.w_TIPFORN.or. .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_DATANASC<>.w_DATANASC
            .w_FODATANASC = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI='S',.w_DATANASC,CTOD('  -  -  '))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_TIPFORN<>.w_TIPFORN.or. .o_SESSO<>.w_SESSO
            .w_FOSESSO = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI='S',.w_SESSO,' ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_TIPFORN<>.w_TIPFORN.or. .o_COMUNE<>.w_COMUNE
            .w_FOCOMUNE = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI='S',.w_COMUNE,space(40))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_TIPFORN<>.w_TIPFORN.or. .o_SIGLA<>.w_SIGLA
            .w_FOSIGLA = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI='S',.w_SIGLA,'  ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_TIPFORN<>.w_TIPFORN.or. .o_RECOMUNE<>.w_RECOMUNE
            .w_FORECOMUNE = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI='S',.w_RECOMUNE,space(40))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_TIPFORN<>.w_TIPFORN.or. .o_RESIGLA<>.w_RESIGLA
            .w_FORESIGLA = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI='S',.w_RESIGLA,'  ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_TIPFORN<>.w_TIPFORN.or. .o_CAP<>.w_CAP
            .w_FOCAP = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI='S',.w_CAP,space(5))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_TIPFORN<>.w_TIPFORN.or. .o_INDIRIZ<>.w_INDIRIZ
            .w_FOINDIRIZ = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI='S',.w_INDIRIZ,space(35))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_TIPFORN<>.w_TIPFORN.or. .o_SECOMUNE<>.w_SECOMUNE
            .w_FOSECOMUNE = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI<>'S',.w_SECOMUNE,space(40))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_TIPFORN<>.w_TIPFORN.or. .o_SESIGLA<>.w_SESIGLA
            .w_FOSESIGLA = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI<>'S',.w_SESIGLA,'  ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_TIPFORN<>.w_TIPFORN.or. .o_SECAP<>.w_SECAP
            .w_FOSECAP = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI<>'S',.w_SECAP,space(5))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_TIPFORN<>.w_TIPFORN.or. .o_SEINDIRI2<>.w_SEINDIRI2
            .w_FOSEINDIRI2 = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI<>'S',.w_SEINDIRI2,space(35))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_TIPFORN<>.w_TIPFORN.or. .o_SERCOMUN<>.w_SERCOMUN
            .w_FOSERCOMUN = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI<>'S',.w_SERCOMUN,space(40))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_TIPFORN<>.w_TIPFORN.or. .o_SERSIGLA<>.w_SERSIGLA
            .w_FOSERSIGLA = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI<>'S',.w_SERSIGLA,'  ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_TIPFORN<>.w_TIPFORN.or. .o_SERCAP<>.w_SERCAP
            .w_FOSERCAP = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI<>'S',.w_SERCAP,space(5))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_TIPFORN<>.w_TIPFORN.or. .o_SERINDIR<>.w_SERINDIR
            .w_FOSERINDIR = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI<>'S',.w_SERINDIR,space(35))
        endif
        .DoRTCalc(132,133,.t.)
        if .o_SELCAF<>.w_SELCAF
            .w_CFDELCAF = iif(.w_SELCAF <> '1',SPACE(11),.w_CFDELCAF)
        endif
        .DoRTCalc(135,135,.t.)
            .w_CODAZI = i_codazi
          .link_5_24('Full')
        .DoRTCalc(138,138,.t.)
          .link_5_26('Full')
        .DoRTCalc(140,141,.t.)
        if .o_SEZ1<>.w_SEZ1.or. .o_SEZ2<>.w_SEZ2
            .w_FIRMDICH = iif(.w_SEZ1='1',.w_FIRMDICH,'0')
        endif
        .DoRTCalc(143,144,.t.)
        if .o_CFPRESCS<>.w_CFPRESCS
            .w_FIRMPRCS = iif(!empty(.w_CFPRESCS),'1','0')
        endif
        if .o_IMPTRTEL<>.w_IMPTRTEL
            .w_INVAVTEL = iif(.w_IMPTRTEL <> '0',.w_INVAVTEL,'0')
        endif
        if .o_CFINCCON<>.w_CFINCCON
            .w_CODSOGG = iif(! empty(.w_CFINCCON),.w_CODSOGG,'0')
        endif
        if .o_FIRMPRCS<>.w_FIRMPRCS
            .w_CFPRESCS = iif(.w_FIRMPRCS='1',.w_CFPRESCS,space(16))
        endif
        .DoRTCalc(149,150,.t.)
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_TIPFORN<>.w_TIPFORN.or. .o_FOCODFIS<>.w_FOCODFIS
            .w_CODFISIN = space(16)
        endif
        .DoRTCalc(152,153,.t.)
        if .o_INVAVTEL<>.w_INVAVTEL
            .w_RICAVTEL = iif(.w_INVAVTEL='1',.w_RICAVTEL,'0')
        endif
        .DoRTCalc(155,158,.t.)
        if .o_SEDOMNOT<>.w_SEDOMNOT
            .w_CMDOMNOT = iif(! empty(.w_SEDOMNOT),space(40),.w_CMDOMNOT)
        endif
        if .o_SEDOMNOT<>.w_SEDOMNOT
            .w_PRDOMNOT = iif(! empty(.w_SEDOMNOT),space(2),.w_PRDOMNOT)
        endif
        if .o_SEDOMNOT<>.w_SEDOMNOT
            .w_CCDOMNOT = iif(! empty(.w_SEDOMNOT),space(4),.w_CCDOMNOT)
        endif
        if .o_SEDOMNOT<>.w_SEDOMNOT
            .w_CPDOMNOT = iif(! empty(.w_SEDOMNOT),space(5),.w_CPDOMNOT)
        endif
        if .o_SEDOMNOT<>.w_SEDOMNOT
            .w_VPDOMNOT = iif(! empty(.w_SEDOMNOT),space(15),.w_VPDOMNOT)
        endif
        if .o_SEDOMNOT<>.w_SEDOMNOT
            .w_INDOMNOT = iif(! empty(.w_SEDOMNOT),space(35),.w_INDOMNOT)
        endif
        if .o_SEDOMNOT<>.w_SEDOMNOT
            .w_NCDOMNOT = iif(! empty(.w_SEDOMNOT),space(10),.w_NCDOMNOT)
        endif
        if .o_SEDOMNOT<>.w_SEDOMNOT
            .w_FRDOMNOT = iif(! empty(.w_SEDOMNOT),space(35),.w_FRDOMNOT)
        endif
        .DoRTCalc(167,167,.t.)
        if .o_SEDOMNOT<>.w_SEDOMNOT
            .w_CEDOMNOT = iif(empty(.w_SEDOMNOT),space(3),.w_CEDOMNOT)
        endif
        if .o_SEDOMNOT<>.w_SEDOMNOT
            .w_SFDOMNOT = iif(empty(.w_SEDOMNOT),space(24),.w_SFDOMNOT)
        endif
        if .o_SEDOMNOT<>.w_SEDOMNOT
            .w_LRDOMNOT = iif(empty(.w_SEDOMNOT),space(24),.w_LRDOMNOT)
        endif
        if .o_SEDOMNOT<>.w_SEDOMNOT
            .w_IEDOMNOT = iif(empty(.w_SEDOMNOT),space(35),.w_IEDOMNOT)
        endif
        if .o_RFSIGLA<>.w_RFSIGLA
          .Calculate_BMAYIIGHNA()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_BMAYIIGHNA()
    with this
          * --- Sbianca campi relativi allo stato 
          .w_RFCAP = space(24)
          .w_RFCOMUNE = space(24)
          .w_RFINDIRIZ = space(35)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oFOCOGNOME_1_11.enabled = this.oPgFrm.Page1.oPag.oFOCOGNOME_1_11.mCond()
    this.oPgFrm.Page1.oPag.oFONOME_1_12.enabled = this.oPgFrm.Page1.oPag.oFONOME_1_12.mCond()
    this.oPgFrm.Page1.oPag.oFODENOMINA_1_13.enabled = this.oPgFrm.Page1.oPag.oFODENOMINA_1_13.mCond()
    this.oPgFrm.Page1.oPag.oCODEVECC_1_18.enabled = this.oPgFrm.Page1.oPag.oCODEVECC_1_18.mCond()
    this.oPgFrm.Page1.oPag.oFLAGCIPAR_1_23.enabled = this.oPgFrm.Page1.oPag.oFLAGCIPAR_1_23.mCond()
    this.oPgFrm.Page1.oPag.oTIPOPERAZ_1_24.enabled = this.oPgFrm.Page1.oPag.oTIPOPERAZ_1_24.mCond()
    this.oPgFrm.Page1.oPag.oDATANASC_1_26.enabled = this.oPgFrm.Page1.oPag.oDATANASC_1_26.mCond()
    this.oPgFrm.Page1.oPag.oCOMUNE_1_27.enabled = this.oPgFrm.Page1.oPag.oCOMUNE_1_27.mCond()
    this.oPgFrm.Page1.oPag.oSIGLA_1_28.enabled = this.oPgFrm.Page1.oPag.oSIGLA_1_28.mCond()
    this.oPgFrm.Page1.oPag.oSESSO_1_29.enabled = this.oPgFrm.Page1.oPag.oSESSO_1_29.mCond()
    this.oPgFrm.Page1.oPag.oVARRES_1_30.enabled = this.oPgFrm.Page1.oPag.oVARRES_1_30.mCond()
    this.oPgFrm.Page1.oPag.oRECOMUNE_1_31.enabled = this.oPgFrm.Page1.oPag.oRECOMUNE_1_31.mCond()
    this.oPgFrm.Page1.oPag.oRESIGLA_1_32.enabled = this.oPgFrm.Page1.oPag.oRESIGLA_1_32.mCond()
    this.oPgFrm.Page1.oPag.oRECODCOM_1_33.enabled = this.oPgFrm.Page1.oPag.oRECODCOM_1_33.mCond()
    this.oPgFrm.Page1.oPag.oINDIRIZ_1_34.enabled = this.oPgFrm.Page1.oPag.oINDIRIZ_1_34.mCond()
    this.oPgFrm.Page1.oPag.oCAP_1_35.enabled = this.oPgFrm.Page1.oPag.oCAP_1_35.mCond()
    this.oPgFrm.Page1.oPag.oCODATT_1_36.enabled = this.oPgFrm.Page1.oPag.oCODATT_1_36.mCond()
    this.oPgFrm.Page1.oPag.oTELEFONO_1_37.enabled = this.oPgFrm.Page1.oPag.oTELEFONO_1_37.mCond()
    this.oPgFrm.Page1.oPag.oSEVARSED_1_38.enabled = this.oPgFrm.Page1.oPag.oSEVARSED_1_38.mCond()
    this.oPgFrm.Page1.oPag.oSECOMUNE_1_39.enabled = this.oPgFrm.Page1.oPag.oSECOMUNE_1_39.mCond()
    this.oPgFrm.Page1.oPag.oSESIGLA_1_40.enabled = this.oPgFrm.Page1.oPag.oSESIGLA_1_40.mCond()
    this.oPgFrm.Page1.oPag.oSEINDIRI2_1_41.enabled = this.oPgFrm.Page1.oPag.oSEINDIRI2_1_41.mCond()
    this.oPgFrm.Page1.oPag.oSECAP_1_42.enabled = this.oPgFrm.Page1.oPag.oSECAP_1_42.mCond()
    this.oPgFrm.Page1.oPag.oSECODCOM_1_43.enabled = this.oPgFrm.Page1.oPag.oSECODCOM_1_43.mCond()
    this.oPgFrm.Page1.oPag.oSEVARDOM_1_44.enabled = this.oPgFrm.Page1.oPag.oSEVARDOM_1_44.mCond()
    this.oPgFrm.Page1.oPag.oSERCOMUN_1_45.enabled = this.oPgFrm.Page1.oPag.oSERCOMUN_1_45.mCond()
    this.oPgFrm.Page1.oPag.oSERSIGLA_1_46.enabled = this.oPgFrm.Page1.oPag.oSERSIGLA_1_46.mCond()
    this.oPgFrm.Page1.oPag.oSERCODCOM_1_47.enabled = this.oPgFrm.Page1.oPag.oSERCODCOM_1_47.mCond()
    this.oPgFrm.Page1.oPag.oSERCAP_1_48.enabled = this.oPgFrm.Page1.oPag.oSERCAP_1_48.mCond()
    this.oPgFrm.Page1.oPag.oSERINDIR_1_49.enabled = this.oPgFrm.Page1.oPag.oSERINDIR_1_49.mCond()
    this.oPgFrm.Page1.oPag.oSECODATT_1_50.enabled = this.oPgFrm.Page1.oPag.oSECODATT_1_50.mCond()
    this.oPgFrm.Page1.oPag.oSETELEFONO_1_51.enabled = this.oPgFrm.Page1.oPag.oSETELEFONO_1_51.mCond()
    this.oPgFrm.Page1.oPag.oNATGIU_1_52.enabled = this.oPgFrm.Page1.oPag.oNATGIU_1_52.mCond()
    this.oPgFrm.Page1.oPag.oSTATO_1_53.enabled = this.oPgFrm.Page1.oPag.oSTATO_1_53.mCond()
    this.oPgFrm.Page1.oPag.oSITUAZ_1_54.enabled = this.oPgFrm.Page1.oPag.oSITUAZ_1_54.mCond()
    this.oPgFrm.Page1.oPag.oCODFISDA_1_55.enabled = this.oPgFrm.Page1.oPag.oCODFISDA_1_55.mCond()
    this.oPgFrm.Page2.oPag.oRFCODFIS_2_2.enabled = this.oPgFrm.Page2.oPag.oRFCODFIS_2_2.mCond()
    this.oPgFrm.Page2.oPag.oRFCODCAR_2_3.enabled = this.oPgFrm.Page2.oPag.oRFCODCAR_2_3.mCond()
    this.oPgFrm.Page2.oPag.oRFCOGNOME_2_5.enabled = this.oPgFrm.Page2.oPag.oRFCOGNOME_2_5.mCond()
    this.oPgFrm.Page2.oPag.oRFNOME_2_6.enabled = this.oPgFrm.Page2.oPag.oRFNOME_2_6.mCond()
    this.oPgFrm.Page2.oPag.oRFSESSO_2_7.enabled = this.oPgFrm.Page2.oPag.oRFSESSO_2_7.mCond()
    this.oPgFrm.Page2.oPag.oRFDATANASC_2_8.enabled = this.oPgFrm.Page2.oPag.oRFDATANASC_2_8.mCond()
    this.oPgFrm.Page2.oPag.oRFCOMNAS_2_9.enabled = this.oPgFrm.Page2.oPag.oRFCOMNAS_2_9.mCond()
    this.oPgFrm.Page2.oPag.oRFSIGNAS_2_10.enabled = this.oPgFrm.Page2.oPag.oRFSIGNAS_2_10.mCond()
    this.oPgFrm.Page2.oPag.oRFSIGLA_2_11.enabled = this.oPgFrm.Page2.oPag.oRFSIGLA_2_11.mCond()
    this.oPgFrm.Page2.oPag.oRFCAP_2_12.enabled = this.oPgFrm.Page2.oPag.oRFCAP_2_12.mCond()
    this.oPgFrm.Page2.oPag.oRFCOMUNE_2_13.enabled = this.oPgFrm.Page2.oPag.oRFCOMUNE_2_13.mCond()
    this.oPgFrm.Page2.oPag.oRFINDIRIZ_2_14.enabled = this.oPgFrm.Page2.oPag.oRFINDIRIZ_2_14.mCond()
    this.oPgFrm.Page2.oPag.oRFTELEFONO_2_15.enabled = this.oPgFrm.Page2.oPag.oRFTELEFONO_2_15.mCond()
    this.oPgFrm.Page2.oPag.oRFFISSED_2_16.enabled = this.oPgFrm.Page2.oPag.oRFFISSED_2_16.mCond()
    this.oPgFrm.Page2.oPag.oRFDATCAR_2_17.enabled = this.oPgFrm.Page2.oPag.oRFDATCAR_2_17.mCond()
    this.oPgFrm.Page2.oPag.oRFDATFAL_2_18.enabled = this.oPgFrm.Page2.oPag.oRFDATFAL_2_18.mCond()
    this.oPgFrm.Page2.oPag.oqSS1_2_20.enabled = this.oPgFrm.Page2.oPag.oqSS1_2_20.mCond()
    this.oPgFrm.Page2.oPag.oqST1_2_21.enabled = this.oPgFrm.Page2.oPag.oqST1_2_21.mCond()
    this.oPgFrm.Page2.oPag.oqSV1_2_22.enabled = this.oPgFrm.Page2.oPag.oqSV1_2_22.mCond()
    this.oPgFrm.Page2.oPag.oqSX1_2_23.enabled = this.oPgFrm.Page2.oPag.oqSX1_2_23.mCond()
    this.oPgFrm.Page2.oPag.oq771_2_24.enabled = this.oPgFrm.Page2.oPag.oq771_2_24.mCond()
    this.oPgFrm.Page2.oPag.oqSS12_2_26.enabled = this.oPgFrm.Page2.oPag.oqSS12_2_26.mCond()
    this.oPgFrm.Page2.oPag.oSEZ3_2_27.enabled = this.oPgFrm.Page2.oPag.oSEZ3_2_27.mCond()
    this.oPgFrm.Page2.oPag.oqSS13_2_28.enabled = this.oPgFrm.Page2.oPag.oqSS13_2_28.mCond()
    this.oPgFrm.Page2.oPag.oqST13_2_29.enabled = this.oPgFrm.Page2.oPag.oqST13_2_29.mCond()
    this.oPgFrm.Page2.oPag.oqSV13_2_30.enabled = this.oPgFrm.Page2.oPag.oqSV13_2_30.mCond()
    this.oPgFrm.Page2.oPag.oqSX13_2_31.enabled = this.oPgFrm.Page2.oPag.oqSX13_2_31.mCond()
    this.oPgFrm.Page2.oPag.oq773_2_32.enabled = this.oPgFrm.Page2.oPag.oq773_2_32.mCond()
    this.oPgFrm.Page2.oPag.oqSS14_2_34.enabled = this.oPgFrm.Page2.oPag.oqSS14_2_34.mCond()
    this.oPgFrm.Page2.oPag.oqST14_2_35.enabled = this.oPgFrm.Page2.oPag.oqST14_2_35.mCond()
    this.oPgFrm.Page2.oPag.oqSX14_2_36.enabled = this.oPgFrm.Page2.oPag.oqSX14_2_36.mCond()
    this.oPgFrm.Page2.oPag.oq774_2_37.enabled = this.oPgFrm.Page2.oPag.oq774_2_37.mCond()
    this.oPgFrm.Page2.oPag.oRFCFINS2_2_38.enabled = this.oPgFrm.Page2.oPag.oRFCFINS2_2_38.mCond()
    this.oPgFrm.Page5.oPag.oIDESTA_5_6.enabled = this.oPgFrm.Page5.oPag.oIDESTA_5_6.mCond()
    this.oPgFrm.Page5.oPag.oPROIFT_5_7.enabled = this.oPgFrm.Page5.oPag.oPROIFT_5_7.mCond()
    this.oPgFrm.Page4.oPag.oFODATANASC_4_2.enabled = this.oPgFrm.Page4.oPag.oFODATANASC_4_2.mCond()
    this.oPgFrm.Page4.oPag.oFOSESSO_4_3.enabled = this.oPgFrm.Page4.oPag.oFOSESSO_4_3.mCond()
    this.oPgFrm.Page4.oPag.oFOCOMUNE_4_4.enabled = this.oPgFrm.Page4.oPag.oFOCOMUNE_4_4.mCond()
    this.oPgFrm.Page4.oPag.oFOSIGLA_4_5.enabled = this.oPgFrm.Page4.oPag.oFOSIGLA_4_5.mCond()
    this.oPgFrm.Page4.oPag.oFORECOMUNE_4_6.enabled = this.oPgFrm.Page4.oPag.oFORECOMUNE_4_6.mCond()
    this.oPgFrm.Page4.oPag.oFORESIGLA_4_7.enabled = this.oPgFrm.Page4.oPag.oFORESIGLA_4_7.mCond()
    this.oPgFrm.Page4.oPag.oFOCAP_4_8.enabled = this.oPgFrm.Page4.oPag.oFOCAP_4_8.mCond()
    this.oPgFrm.Page4.oPag.oFOINDIRIZ_4_9.enabled = this.oPgFrm.Page4.oPag.oFOINDIRIZ_4_9.mCond()
    this.oPgFrm.Page4.oPag.oFOSECOMUNE_4_11.enabled = this.oPgFrm.Page4.oPag.oFOSECOMUNE_4_11.mCond()
    this.oPgFrm.Page4.oPag.oFOSESIGLA_4_12.enabled = this.oPgFrm.Page4.oPag.oFOSESIGLA_4_12.mCond()
    this.oPgFrm.Page4.oPag.oFOSECAP_4_13.enabled = this.oPgFrm.Page4.oPag.oFOSECAP_4_13.mCond()
    this.oPgFrm.Page4.oPag.oFOSEINDIRI2_4_14.enabled = this.oPgFrm.Page4.oPag.oFOSEINDIRI2_4_14.mCond()
    this.oPgFrm.Page4.oPag.oFOSERCOMUN_4_15.enabled = this.oPgFrm.Page4.oPag.oFOSERCOMUN_4_15.mCond()
    this.oPgFrm.Page4.oPag.oFOSERSIGLA_4_16.enabled = this.oPgFrm.Page4.oPag.oFOSERSIGLA_4_16.mCond()
    this.oPgFrm.Page4.oPag.oFOSERCAP_4_17.enabled = this.oPgFrm.Page4.oPag.oFOSERCAP_4_17.mCond()
    this.oPgFrm.Page4.oPag.oFOSERINDIR_4_18.enabled = this.oPgFrm.Page4.oPag.oFOSERINDIR_4_18.mCond()
    this.oPgFrm.Page3.oPag.oFIRMPRCS_3_4.enabled = this.oPgFrm.Page3.oPag.oFIRMPRCS_3_4.mCond()
    this.oPgFrm.Page3.oPag.oINVAVTEL_3_5.enabled = this.oPgFrm.Page3.oPag.oINVAVTEL_3_5.mCond()
    this.oPgFrm.Page3.oPag.oCODSOGG_3_6.enabled = this.oPgFrm.Page3.oPag.oCODSOGG_3_6.mCond()
    this.oPgFrm.Page3.oPag.oRICAVTEL_3_13.enabled = this.oPgFrm.Page3.oPag.oRICAVTEL_3_13.mCond()
    this.oPgFrm.Page3.oPag.oCMDOMNOT_3_35.enabled = this.oPgFrm.Page3.oPag.oCMDOMNOT_3_35.mCond()
    this.oPgFrm.Page3.oPag.oPRDOMNOT_3_37.enabled = this.oPgFrm.Page3.oPag.oPRDOMNOT_3_37.mCond()
    this.oPgFrm.Page3.oPag.oCCDOMNOT_3_39.enabled = this.oPgFrm.Page3.oPag.oCCDOMNOT_3_39.mCond()
    this.oPgFrm.Page3.oPag.oCPDOMNOT_3_41.enabled = this.oPgFrm.Page3.oPag.oCPDOMNOT_3_41.mCond()
    this.oPgFrm.Page3.oPag.oVPDOMNOT_3_43.enabled = this.oPgFrm.Page3.oPag.oVPDOMNOT_3_43.mCond()
    this.oPgFrm.Page3.oPag.oINDOMNOT_3_45.enabled = this.oPgFrm.Page3.oPag.oINDOMNOT_3_45.mCond()
    this.oPgFrm.Page3.oPag.oNCDOMNOT_3_47.enabled = this.oPgFrm.Page3.oPag.oNCDOMNOT_3_47.mCond()
    this.oPgFrm.Page3.oPag.oFRDOMNOT_3_49.enabled = this.oPgFrm.Page3.oPag.oFRDOMNOT_3_49.mCond()
    this.oPgFrm.Page3.oPag.oCEDOMNOT_3_53.enabled = this.oPgFrm.Page3.oPag.oCEDOMNOT_3_53.mCond()
    this.oPgFrm.Page3.oPag.oSFDOMNOT_3_55.enabled = this.oPgFrm.Page3.oPag.oSFDOMNOT_3_55.mCond()
    this.oPgFrm.Page3.oPag.oLRDOMNOT_3_57.enabled = this.oPgFrm.Page3.oPag.oLRDOMNOT_3_57.mCond()
    this.oPgFrm.Page3.oPag.oIEDOMNOT_3_59.enabled = this.oPgFrm.Page3.oPag.oIEDOMNOT_3_59.mCond()
    this.oPgFrm.Page5.oPag.oBtn_5_21.enabled = this.oPgFrm.Page5.oPag.oBtn_5_21.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page2.oPag.oRFDATFAL_2_18.visible=!this.oPgFrm.Page2.oPag.oRFDATFAL_2_18.mHide()
    this.oPgFrm.Page4.oPag.oCFDELCAF_4_21.visible=!this.oPgFrm.Page4.oPag.oCFDELCAF_4_21.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_66.visible=!this.oPgFrm.Page2.oPag.oStr_2_66.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_46.visible=!this.oPgFrm.Page4.oPag.oStr_4_46.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsriasft
    *Disabilito etichetta relativa alla sezione III
    this.opgfrm.pages(2).opag.oStr_2_60.enabled=.f.
    this.opgfrm.pages(2).opag.oStr_2_61.enabled=.f.
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsriasft
    if cevent ='Done'
      g_LoadFuncButton = .T.
    endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZCOFAZI,AZRAGAZI,AZPERAZI,AZIVACAR,AZIVACOF";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_CODAZI)
            select AZCODAZI,AZCOFAZI,AZRAGAZI,AZPERAZI,AZIVACAR,AZIVACOF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.AZCODAZI,space(5))
      this.w_CodFisAzi = NVL(_Link_.AZCOFAZI,space(16))
      this.w_FODENOMINA = NVL(_Link_.AZRAGAZI,space(60))
      this.w_PERAZI = NVL(_Link_.AZPERAZI,space(1))
      this.w_RFCODCAR = NVL(_Link_.AZIVACAR,space(2))
      this.w_RFCODFIS = NVL(_Link_.AZIVACOF,space(16))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_CodFisAzi = space(16)
      this.w_FODENOMINA = space(60)
      this.w_PERAZI = space(1)
      this.w_RFCODCAR = space(2)
      this.w_RFCODFIS = space(16)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PERFIS
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TITOLARI_IDX,3]
    i_lTable = "TITOLARI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TITOLARI_IDX,2], .t., this.TITOLARI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TITOLARI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PERFIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PERFIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TTCODAZI,TTDATNAS,TT_SESSO,TTLUONAS,TTPRONAS,TTLOCTIT,TTPROTIT,TTINDIRI,TTCAPTIT,TTTELEFO,TTCOGTIT,TTNOMTIT";
                   +" from "+i_cTable+" "+i_lTable+" where TTCODAZI="+cp_ToStrODBC(this.w_PERFIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TTCODAZI',this.w_PERFIS)
            select TTCODAZI,TTDATNAS,TT_SESSO,TTLUONAS,TTPRONAS,TTLOCTIT,TTPROTIT,TTINDIRI,TTCAPTIT,TTTELEFO,TTCOGTIT,TTNOMTIT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PERFIS = NVL(_Link_.TTCODAZI,space(5))
      this.w_DATANASC = NVL(cp_ToDate(_Link_.TTDATNAS),ctod("  /  /  "))
      this.w_SESSO = NVL(_Link_.TT_SESSO,space(1))
      this.w_COMUNE = NVL(_Link_.TTLUONAS,space(40))
      this.w_SIGLA = NVL(_Link_.TTPRONAS,space(2))
      this.w_RECOMUNE = NVL(_Link_.TTLOCTIT,space(40))
      this.w_RESIGLA = NVL(_Link_.TTPROTIT,space(2))
      this.w_INDIRIZ = NVL(_Link_.TTINDIRI,space(35))
      this.w_CAP = NVL(_Link_.TTCAPTIT,space(5))
      this.w_TELEFONO = NVL(_Link_.TTTELEFO,space(12))
      this.w_FOCOGNOME = NVL(_Link_.TTCOGTIT,space(24))
      this.w_FONOME = NVL(_Link_.TTNOMTIT,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_PERFIS = space(5)
      endif
      this.w_DATANASC = ctod("  /  /  ")
      this.w_SESSO = space(1)
      this.w_COMUNE = space(40)
      this.w_SIGLA = space(2)
      this.w_RECOMUNE = space(40)
      this.w_RESIGLA = space(2)
      this.w_INDIRIZ = space(35)
      this.w_CAP = space(5)
      this.w_TELEFONO = space(12)
      this.w_FOCOGNOME = space(24)
      this.w_FONOME = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TITOLARI_IDX,2])+'\'+cp_ToStr(_Link_.TTCODAZI,1)
      cp_ShowWarn(i_cKey,this.TITOLARI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PERFIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SEDILEG
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SEDIAZIE_IDX,3]
    i_lTable = "SEDIAZIE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2], .t., this.SEDIAZIE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SEDILEG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SEDILEG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SETIPRIF,SECODAZI,SELOCALI,SEPROVIN,SEINDIRI,SE___CAP";
                   +" from "+i_cTable+" "+i_lTable+" where SECODAZI="+cp_ToStrODBC(this.w_SEDILEG);
                   +" and SETIPRIF="+cp_ToStrODBC(this.w_TIPSL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SETIPRIF',this.w_TIPSL;
                       ,'SECODAZI',this.w_SEDILEG)
            select SETIPRIF,SECODAZI,SELOCALI,SEPROVIN,SEINDIRI,SE___CAP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SEDILEG = NVL(_Link_.SECODAZI,space(5))
      this.w_SECOMUNE = NVL(_Link_.SELOCALI,space(40))
      this.w_SESIGLA = NVL(_Link_.SEPROVIN,space(2))
      this.w_SEINDIRI2 = NVL(_Link_.SEINDIRI,space(35))
      this.w_SECAP = NVL(_Link_.SE___CAP,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_SEDILEG = space(5)
      endif
      this.w_SECOMUNE = space(40)
      this.w_SESIGLA = space(2)
      this.w_SEINDIRI2 = space(35)
      this.w_SECAP = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2])+'\'+cp_ToStr(_Link_.SETIPRIF,1)+'\'+cp_ToStr(_Link_.SECODAZI,1)
      cp_ShowWarn(i_cKey,this.SEDIAZIE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SEDILEG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SEDIFIS
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SEDIAZIE_IDX,3]
    i_lTable = "SEDIAZIE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2], .t., this.SEDIAZIE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SEDIFIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SEDIFIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SETIPRIF,SECODAZI,SELOCALI,SEPROVIN,SEINDIRI,SE___CAP,SETELEFO,SENATGIU";
                   +" from "+i_cTable+" "+i_lTable+" where SECODAZI="+cp_ToStrODBC(this.w_SEDIFIS);
                   +" and SETIPRIF="+cp_ToStrODBC(this.w_TIPFS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SETIPRIF',this.w_TIPFS;
                       ,'SECODAZI',this.w_SEDIFIS)
            select SETIPRIF,SECODAZI,SELOCALI,SEPROVIN,SEINDIRI,SE___CAP,SETELEFO,SENATGIU;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SEDIFIS = NVL(_Link_.SECODAZI,space(5))
      this.w_SERCOMUN = NVL(_Link_.SELOCALI,space(40))
      this.w_SERSIGLA = NVL(_Link_.SEPROVIN,space(2))
      this.w_SERINDIR = NVL(_Link_.SEINDIRI,space(35))
      this.w_SERCAP = NVL(_Link_.SE___CAP,space(5))
      this.w_SETELEFONO = NVL(_Link_.SETELEFO,space(12))
      this.w_NATGIU = NVL(_Link_.SENATGIU,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_SEDIFIS = space(5)
      endif
      this.w_SERCOMUN = space(40)
      this.w_SERSIGLA = space(2)
      this.w_SERINDIR = space(35)
      this.w_SERCAP = space(5)
      this.w_SETELEFONO = space(12)
      this.w_NATGIU = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2])+'\'+cp_ToStr(_Link_.SETIPRIF,1)+'\'+cp_ToStr(_Link_.SECODAZI,1)
      cp_ShowWarn(i_cKey,this.SEDIAZIE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SEDIFIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ESERCIZIO
  func Link_1_57(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ESERCIZIO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ESERCIZIO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_ESERCIZIO);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_ESERCIZIO)
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ESERCIZIO = NVL(_Link_.ESCODESE,space(4))
      this.w_VALUTAESE = NVL(_Link_.ESVALNAZ,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_ESERCIZIO = space(4)
      endif
      this.w_VALUTAESE = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ESERCIZIO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODVAL
  func Link_1_60(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_CODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_CODVAL)
            select VACODVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_decimi = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_CODVAL = space(3)
      endif
      this.w_decimi = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PERCIN
  func Link_5_24(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PERCIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PERCIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU,ANTIPCLF";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_PERCIN);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_PERCIN)
            select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU,ANTIPCLF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PERCIN = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCINI = NVL(_Link_.ANDESCRI,space(40))
      this.w_RITE = NVL(_Link_.ANRITENU,space(1))
      this.w_TIPCLF = NVL(_Link_.ANTIPCLF,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PERCIN = space(15)
      endif
      this.w_DESCINI = space(40)
      this.w_RITE = space(1)
      this.w_TIPCLF = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((EMPTY(.w_PERCFIN)) OR (.w_PERCIN<=.w_PERCFIN)) AND (.w_RITE $ 'CS' AND .w_TIPCLF<>'A')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice fornitore inesistente o obsoleto o non soggetto a ritenute o agente")
        endif
        this.w_PERCIN = space(15)
        this.w_DESCINI = space(40)
        this.w_RITE = space(1)
        this.w_TIPCLF = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PERCIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PERCFIN
  func Link_5_26(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PERCFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PERCFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU,ANTIPCLF";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_PERCFIN);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_PERCFIN)
            select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU,ANTIPCLF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PERCFIN = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCFIN = NVL(_Link_.ANDESCRI,space(40))
      this.w_RITE = NVL(_Link_.ANRITENU,space(1))
      this.w_TIPCLF = NVL(_Link_.ANTIPCLF,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PERCFIN = space(15)
      endif
      this.w_DESCFIN = space(40)
      this.w_RITE = space(1)
      this.w_TIPCLF = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((.w_PERCFIN>=.w_PERCIN) OR (EMPTY(.w_PERCIN))) AND (.w_RITE $ 'CS' AND .w_TIPCLF<>'A')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice fornitore inesistente o obsoleto o non soggetto a ritenute o agente")
        endif
        this.w_PERCFIN = space(15)
        this.w_DESCFIN = space(40)
        this.w_RITE = space(1)
        this.w_TIPCLF = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PERCFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oFOCOGNOME_1_11.value==this.w_FOCOGNOME)
      this.oPgFrm.Page1.oPag.oFOCOGNOME_1_11.value=this.w_FOCOGNOME
    endif
    if not(this.oPgFrm.Page1.oPag.oFONOME_1_12.value==this.w_FONOME)
      this.oPgFrm.Page1.oPag.oFONOME_1_12.value=this.w_FONOME
    endif
    if not(this.oPgFrm.Page1.oPag.oFODENOMINA_1_13.value==this.w_FODENOMINA)
      this.oPgFrm.Page1.oPag.oFODENOMINA_1_13.value=this.w_FODENOMINA
    endif
    if not(this.oPgFrm.Page1.oPag.oFOCODFIS_1_14.value==this.w_FOCODFIS)
      this.oPgFrm.Page1.oPag.oFOCODFIS_1_14.value=this.w_FOCODFIS
    endif
    if not(this.oPgFrm.Page1.oPag.oFOCODSOS_1_15.value==this.w_FOCODSOS)
      this.oPgFrm.Page1.oPag.oFOCODSOS_1_15.value=this.w_FOCODSOS
    endif
    if not(this.oPgFrm.Page1.oPag.oFLAGEVE_1_17.RadioValue()==this.w_FLAGEVE)
      this.oPgFrm.Page1.oPag.oFLAGEVE_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODEVECC_1_18.RadioValue()==this.w_CODEVECC)
      this.oPgFrm.Page1.oPag.oCODEVECC_1_18.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLAGCONF_1_19.RadioValue()==this.w_FLAGCONF)
      this.oPgFrm.Page1.oPag.oFLAGCONF_1_19.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oForza1_1_20.RadioValue()==this.w_Forza1)
      this.oPgFrm.Page1.oPag.oForza1_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLAGCOR_1_21.RadioValue()==this.w_FLAGCOR)
      this.oPgFrm.Page1.oPag.oFLAGCOR_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLAGINT_1_22.RadioValue()==this.w_FLAGINT)
      this.oPgFrm.Page1.oPag.oFLAGINT_1_22.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLAGCIPAR_1_23.RadioValue()==this.w_FLAGCIPAR)
      this.oPgFrm.Page1.oPag.oFLAGCIPAR_1_23.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOPERAZ_1_24.RadioValue()==this.w_TIPOPERAZ)
      this.oPgFrm.Page1.oPag.oTIPOPERAZ_1_24.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDATANASC_1_26.value==this.w_DATANASC)
      this.oPgFrm.Page1.oPag.oDATANASC_1_26.value=this.w_DATANASC
    endif
    if not(this.oPgFrm.Page1.oPag.oCOMUNE_1_27.value==this.w_COMUNE)
      this.oPgFrm.Page1.oPag.oCOMUNE_1_27.value=this.w_COMUNE
    endif
    if not(this.oPgFrm.Page1.oPag.oSIGLA_1_28.value==this.w_SIGLA)
      this.oPgFrm.Page1.oPag.oSIGLA_1_28.value=this.w_SIGLA
    endif
    if not(this.oPgFrm.Page1.oPag.oSESSO_1_29.RadioValue()==this.w_SESSO)
      this.oPgFrm.Page1.oPag.oSESSO_1_29.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oVARRES_1_30.value==this.w_VARRES)
      this.oPgFrm.Page1.oPag.oVARRES_1_30.value=this.w_VARRES
    endif
    if not(this.oPgFrm.Page1.oPag.oRECOMUNE_1_31.value==this.w_RECOMUNE)
      this.oPgFrm.Page1.oPag.oRECOMUNE_1_31.value=this.w_RECOMUNE
    endif
    if not(this.oPgFrm.Page1.oPag.oRESIGLA_1_32.value==this.w_RESIGLA)
      this.oPgFrm.Page1.oPag.oRESIGLA_1_32.value=this.w_RESIGLA
    endif
    if not(this.oPgFrm.Page1.oPag.oRECODCOM_1_33.value==this.w_RECODCOM)
      this.oPgFrm.Page1.oPag.oRECODCOM_1_33.value=this.w_RECODCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oINDIRIZ_1_34.value==this.w_INDIRIZ)
      this.oPgFrm.Page1.oPag.oINDIRIZ_1_34.value=this.w_INDIRIZ
    endif
    if not(this.oPgFrm.Page1.oPag.oCAP_1_35.value==this.w_CAP)
      this.oPgFrm.Page1.oPag.oCAP_1_35.value=this.w_CAP
    endif
    if not(this.oPgFrm.Page1.oPag.oCODATT_1_36.value==this.w_CODATT)
      this.oPgFrm.Page1.oPag.oCODATT_1_36.value=this.w_CODATT
    endif
    if not(this.oPgFrm.Page1.oPag.oTELEFONO_1_37.value==this.w_TELEFONO)
      this.oPgFrm.Page1.oPag.oTELEFONO_1_37.value=this.w_TELEFONO
    endif
    if not(this.oPgFrm.Page1.oPag.oSEVARSED_1_38.value==this.w_SEVARSED)
      this.oPgFrm.Page1.oPag.oSEVARSED_1_38.value=this.w_SEVARSED
    endif
    if not(this.oPgFrm.Page1.oPag.oSECOMUNE_1_39.value==this.w_SECOMUNE)
      this.oPgFrm.Page1.oPag.oSECOMUNE_1_39.value=this.w_SECOMUNE
    endif
    if not(this.oPgFrm.Page1.oPag.oSESIGLA_1_40.value==this.w_SESIGLA)
      this.oPgFrm.Page1.oPag.oSESIGLA_1_40.value=this.w_SESIGLA
    endif
    if not(this.oPgFrm.Page1.oPag.oSEINDIRI2_1_41.value==this.w_SEINDIRI2)
      this.oPgFrm.Page1.oPag.oSEINDIRI2_1_41.value=this.w_SEINDIRI2
    endif
    if not(this.oPgFrm.Page1.oPag.oSECAP_1_42.value==this.w_SECAP)
      this.oPgFrm.Page1.oPag.oSECAP_1_42.value=this.w_SECAP
    endif
    if not(this.oPgFrm.Page1.oPag.oSECODCOM_1_43.value==this.w_SECODCOM)
      this.oPgFrm.Page1.oPag.oSECODCOM_1_43.value=this.w_SECODCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oSEVARDOM_1_44.value==this.w_SEVARDOM)
      this.oPgFrm.Page1.oPag.oSEVARDOM_1_44.value=this.w_SEVARDOM
    endif
    if not(this.oPgFrm.Page1.oPag.oSERCOMUN_1_45.value==this.w_SERCOMUN)
      this.oPgFrm.Page1.oPag.oSERCOMUN_1_45.value=this.w_SERCOMUN
    endif
    if not(this.oPgFrm.Page1.oPag.oSERSIGLA_1_46.value==this.w_SERSIGLA)
      this.oPgFrm.Page1.oPag.oSERSIGLA_1_46.value=this.w_SERSIGLA
    endif
    if not(this.oPgFrm.Page1.oPag.oSERCODCOM_1_47.value==this.w_SERCODCOM)
      this.oPgFrm.Page1.oPag.oSERCODCOM_1_47.value=this.w_SERCODCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oSERCAP_1_48.value==this.w_SERCAP)
      this.oPgFrm.Page1.oPag.oSERCAP_1_48.value=this.w_SERCAP
    endif
    if not(this.oPgFrm.Page1.oPag.oSERINDIR_1_49.value==this.w_SERINDIR)
      this.oPgFrm.Page1.oPag.oSERINDIR_1_49.value=this.w_SERINDIR
    endif
    if not(this.oPgFrm.Page1.oPag.oSECODATT_1_50.value==this.w_SECODATT)
      this.oPgFrm.Page1.oPag.oSECODATT_1_50.value=this.w_SECODATT
    endif
    if not(this.oPgFrm.Page1.oPag.oSETELEFONO_1_51.value==this.w_SETELEFONO)
      this.oPgFrm.Page1.oPag.oSETELEFONO_1_51.value=this.w_SETELEFONO
    endif
    if not(this.oPgFrm.Page1.oPag.oNATGIU_1_52.value==this.w_NATGIU)
      this.oPgFrm.Page1.oPag.oNATGIU_1_52.value=this.w_NATGIU
    endif
    if not(this.oPgFrm.Page1.oPag.oSTATO_1_53.RadioValue()==this.w_STATO)
      this.oPgFrm.Page1.oPag.oSTATO_1_53.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSITUAZ_1_54.RadioValue()==this.w_SITUAZ)
      this.oPgFrm.Page1.oPag.oSITUAZ_1_54.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODFISDA_1_55.value==this.w_CODFISDA)
      this.oPgFrm.Page1.oPag.oCODFISDA_1_55.value=this.w_CODFISDA
    endif
    if not(this.oPgFrm.Page2.oPag.oForza2_2_1.RadioValue()==this.w_Forza2)
      this.oPgFrm.Page2.oPag.oForza2_2_1.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oRFCODFIS_2_2.value==this.w_RFCODFIS)
      this.oPgFrm.Page2.oPag.oRFCODFIS_2_2.value=this.w_RFCODFIS
    endif
    if not(this.oPgFrm.Page2.oPag.oRFCODCAR_2_3.RadioValue()==this.w_RFCODCAR)
      this.oPgFrm.Page2.oPag.oRFCODCAR_2_3.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oRFCOGNOME_2_5.value==this.w_RFCOGNOME)
      this.oPgFrm.Page2.oPag.oRFCOGNOME_2_5.value=this.w_RFCOGNOME
    endif
    if not(this.oPgFrm.Page2.oPag.oRFNOME_2_6.value==this.w_RFNOME)
      this.oPgFrm.Page2.oPag.oRFNOME_2_6.value=this.w_RFNOME
    endif
    if not(this.oPgFrm.Page2.oPag.oRFSESSO_2_7.RadioValue()==this.w_RFSESSO)
      this.oPgFrm.Page2.oPag.oRFSESSO_2_7.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oRFDATANASC_2_8.value==this.w_RFDATANASC)
      this.oPgFrm.Page2.oPag.oRFDATANASC_2_8.value=this.w_RFDATANASC
    endif
    if not(this.oPgFrm.Page2.oPag.oRFCOMNAS_2_9.value==this.w_RFCOMNAS)
      this.oPgFrm.Page2.oPag.oRFCOMNAS_2_9.value=this.w_RFCOMNAS
    endif
    if not(this.oPgFrm.Page2.oPag.oRFSIGNAS_2_10.value==this.w_RFSIGNAS)
      this.oPgFrm.Page2.oPag.oRFSIGNAS_2_10.value=this.w_RFSIGNAS
    endif
    if not(this.oPgFrm.Page2.oPag.oRFSIGLA_2_11.value==this.w_RFSIGLA)
      this.oPgFrm.Page2.oPag.oRFSIGLA_2_11.value=this.w_RFSIGLA
    endif
    if not(this.oPgFrm.Page2.oPag.oRFCAP_2_12.value==this.w_RFCAP)
      this.oPgFrm.Page2.oPag.oRFCAP_2_12.value=this.w_RFCAP
    endif
    if not(this.oPgFrm.Page2.oPag.oRFCOMUNE_2_13.value==this.w_RFCOMUNE)
      this.oPgFrm.Page2.oPag.oRFCOMUNE_2_13.value=this.w_RFCOMUNE
    endif
    if not(this.oPgFrm.Page2.oPag.oRFINDIRIZ_2_14.value==this.w_RFINDIRIZ)
      this.oPgFrm.Page2.oPag.oRFINDIRIZ_2_14.value=this.w_RFINDIRIZ
    endif
    if not(this.oPgFrm.Page2.oPag.oRFTELEFONO_2_15.value==this.w_RFTELEFONO)
      this.oPgFrm.Page2.oPag.oRFTELEFONO_2_15.value=this.w_RFTELEFONO
    endif
    if not(this.oPgFrm.Page2.oPag.oRFFISSED_2_16.value==this.w_RFFISSED)
      this.oPgFrm.Page2.oPag.oRFFISSED_2_16.value=this.w_RFFISSED
    endif
    if not(this.oPgFrm.Page2.oPag.oRFDATCAR_2_17.value==this.w_RFDATCAR)
      this.oPgFrm.Page2.oPag.oRFDATCAR_2_17.value=this.w_RFDATCAR
    endif
    if not(this.oPgFrm.Page2.oPag.oRFDATFAL_2_18.value==this.w_RFDATFAL)
      this.oPgFrm.Page2.oPag.oRFDATFAL_2_18.value=this.w_RFDATFAL
    endif
    if not(this.oPgFrm.Page2.oPag.oSEZ1_2_19.RadioValue()==this.w_SEZ1)
      this.oPgFrm.Page2.oPag.oSEZ1_2_19.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oqSS1_2_20.RadioValue()==this.w_qSS1)
      this.oPgFrm.Page2.oPag.oqSS1_2_20.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oqST1_2_21.RadioValue()==this.w_qST1)
      this.oPgFrm.Page2.oPag.oqST1_2_21.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oqSV1_2_22.RadioValue()==this.w_qSV1)
      this.oPgFrm.Page2.oPag.oqSV1_2_22.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oqSX1_2_23.RadioValue()==this.w_qSX1)
      this.oPgFrm.Page2.oPag.oqSX1_2_23.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oq771_2_24.RadioValue()==this.w_q771)
      this.oPgFrm.Page2.oPag.oq771_2_24.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oSEZ2_2_25.RadioValue()==this.w_SEZ2)
      this.oPgFrm.Page2.oPag.oSEZ2_2_25.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oqSS12_2_26.RadioValue()==this.w_qSS12)
      this.oPgFrm.Page2.oPag.oqSS12_2_26.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oSEZ3_2_27.RadioValue()==this.w_SEZ3)
      this.oPgFrm.Page2.oPag.oSEZ3_2_27.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oqSS13_2_28.RadioValue()==this.w_qSS13)
      this.oPgFrm.Page2.oPag.oqSS13_2_28.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oqST13_2_29.RadioValue()==this.w_qST13)
      this.oPgFrm.Page2.oPag.oqST13_2_29.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oqSV13_2_30.RadioValue()==this.w_qSV13)
      this.oPgFrm.Page2.oPag.oqSV13_2_30.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oqSX13_2_31.RadioValue()==this.w_qSX13)
      this.oPgFrm.Page2.oPag.oqSX13_2_31.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oq773_2_32.RadioValue()==this.w_q773)
      this.oPgFrm.Page2.oPag.oq773_2_32.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oSEZ4_2_33.RadioValue()==this.w_SEZ4)
      this.oPgFrm.Page2.oPag.oSEZ4_2_33.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oqSS14_2_34.RadioValue()==this.w_qSS14)
      this.oPgFrm.Page2.oPag.oqSS14_2_34.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oqST14_2_35.RadioValue()==this.w_qST14)
      this.oPgFrm.Page2.oPag.oqST14_2_35.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oqSX14_2_36.RadioValue()==this.w_qSX14)
      this.oPgFrm.Page2.oPag.oqSX14_2_36.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oq774_2_37.RadioValue()==this.w_q774)
      this.oPgFrm.Page2.oPag.oq774_2_37.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oRFCFINS2_2_38.value==this.w_RFCFINS2)
      this.oPgFrm.Page2.oPag.oRFCFINS2_2_38.value=this.w_RFCFINS2
    endif
    if not(this.oPgFrm.Page5.oPag.oDATINI_5_1.value==this.w_DATINI)
      this.oPgFrm.Page5.oPag.oDATINI_5_1.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page5.oPag.oDATFIN_5_2.value==this.w_DATFIN)
      this.oPgFrm.Page5.oPag.oDATFIN_5_2.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page5.oPag.oDirName_5_3.value==this.w_DirName)
      this.oPgFrm.Page5.oPag.oDirName_5_3.value=this.w_DirName
    endif
    if not(this.oPgFrm.Page5.oPag.oFileName_5_5.value==this.w_FileName)
      this.oPgFrm.Page5.oPag.oFileName_5_5.value=this.w_FileName
    endif
    if not(this.oPgFrm.Page5.oPag.oIDESTA_5_6.value==this.w_IDESTA)
      this.oPgFrm.Page5.oPag.oIDESTA_5_6.value=this.w_IDESTA
    endif
    if not(this.oPgFrm.Page5.oPag.oPROIFT_5_7.value==this.w_PROIFT)
      this.oPgFrm.Page5.oPag.oPROIFT_5_7.value=this.w_PROIFT
    endif
    if not(this.oPgFrm.Page4.oPag.oTIPFORN_4_1.RadioValue()==this.w_TIPFORN)
      this.oPgFrm.Page4.oPag.oTIPFORN_4_1.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oFODATANASC_4_2.value==this.w_FODATANASC)
      this.oPgFrm.Page4.oPag.oFODATANASC_4_2.value=this.w_FODATANASC
    endif
    if not(this.oPgFrm.Page4.oPag.oFOSESSO_4_3.RadioValue()==this.w_FOSESSO)
      this.oPgFrm.Page4.oPag.oFOSESSO_4_3.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oFOCOMUNE_4_4.value==this.w_FOCOMUNE)
      this.oPgFrm.Page4.oPag.oFOCOMUNE_4_4.value=this.w_FOCOMUNE
    endif
    if not(this.oPgFrm.Page4.oPag.oFOSIGLA_4_5.value==this.w_FOSIGLA)
      this.oPgFrm.Page4.oPag.oFOSIGLA_4_5.value=this.w_FOSIGLA
    endif
    if not(this.oPgFrm.Page4.oPag.oFORECOMUNE_4_6.value==this.w_FORECOMUNE)
      this.oPgFrm.Page4.oPag.oFORECOMUNE_4_6.value=this.w_FORECOMUNE
    endif
    if not(this.oPgFrm.Page4.oPag.oFORESIGLA_4_7.value==this.w_FORESIGLA)
      this.oPgFrm.Page4.oPag.oFORESIGLA_4_7.value=this.w_FORESIGLA
    endif
    if not(this.oPgFrm.Page4.oPag.oFOCAP_4_8.value==this.w_FOCAP)
      this.oPgFrm.Page4.oPag.oFOCAP_4_8.value=this.w_FOCAP
    endif
    if not(this.oPgFrm.Page4.oPag.oFOINDIRIZ_4_9.value==this.w_FOINDIRIZ)
      this.oPgFrm.Page4.oPag.oFOINDIRIZ_4_9.value=this.w_FOINDIRIZ
    endif
    if not(this.oPgFrm.Page4.oPag.oFOSECOMUNE_4_11.value==this.w_FOSECOMUNE)
      this.oPgFrm.Page4.oPag.oFOSECOMUNE_4_11.value=this.w_FOSECOMUNE
    endif
    if not(this.oPgFrm.Page4.oPag.oFOSESIGLA_4_12.value==this.w_FOSESIGLA)
      this.oPgFrm.Page4.oPag.oFOSESIGLA_4_12.value=this.w_FOSESIGLA
    endif
    if not(this.oPgFrm.Page4.oPag.oFOSECAP_4_13.value==this.w_FOSECAP)
      this.oPgFrm.Page4.oPag.oFOSECAP_4_13.value=this.w_FOSECAP
    endif
    if not(this.oPgFrm.Page4.oPag.oFOSEINDIRI2_4_14.value==this.w_FOSEINDIRI2)
      this.oPgFrm.Page4.oPag.oFOSEINDIRI2_4_14.value=this.w_FOSEINDIRI2
    endif
    if not(this.oPgFrm.Page4.oPag.oFOSERCOMUN_4_15.value==this.w_FOSERCOMUN)
      this.oPgFrm.Page4.oPag.oFOSERCOMUN_4_15.value=this.w_FOSERCOMUN
    endif
    if not(this.oPgFrm.Page4.oPag.oFOSERSIGLA_4_16.value==this.w_FOSERSIGLA)
      this.oPgFrm.Page4.oPag.oFOSERSIGLA_4_16.value=this.w_FOSERSIGLA
    endif
    if not(this.oPgFrm.Page4.oPag.oFOSERCAP_4_17.value==this.w_FOSERCAP)
      this.oPgFrm.Page4.oPag.oFOSERCAP_4_17.value=this.w_FOSERCAP
    endif
    if not(this.oPgFrm.Page4.oPag.oFOSERINDIR_4_18.value==this.w_FOSERINDIR)
      this.oPgFrm.Page4.oPag.oFOSERINDIR_4_18.value=this.w_FOSERINDIR
    endif
    if not(this.oPgFrm.Page4.oPag.oSELCAF_4_19.RadioValue()==this.w_SELCAF)
      this.oPgFrm.Page4.oPag.oSELCAF_4_19.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oCFRESCAF_4_20.value==this.w_CFRESCAF)
      this.oPgFrm.Page4.oPag.oCFRESCAF_4_20.value=this.w_CFRESCAF
    endif
    if not(this.oPgFrm.Page4.oPag.oCFDELCAF_4_21.value==this.w_CFDELCAF)
      this.oPgFrm.Page4.oPag.oCFDELCAF_4_21.value=this.w_CFDELCAF
    endif
    if not(this.oPgFrm.Page4.oPag.oFLAGFIR_4_22.RadioValue()==this.w_FLAGFIR)
      this.oPgFrm.Page4.oPag.oFLAGFIR_4_22.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oFIRMDICH_3_1.RadioValue()==this.w_FIRMDICH)
      this.oPgFrm.Page3.oPag.oFIRMDICH_3_1.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oFIRMPRES_3_2.RadioValue()==this.w_FIRMPRES)
      this.oPgFrm.Page3.oPag.oFIRMPRES_3_2.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oCFINCCON_3_3.value==this.w_CFINCCON)
      this.oPgFrm.Page3.oPag.oCFINCCON_3_3.value=this.w_CFINCCON
    endif
    if not(this.oPgFrm.Page3.oPag.oFIRMPRCS_3_4.RadioValue()==this.w_FIRMPRCS)
      this.oPgFrm.Page3.oPag.oFIRMPRCS_3_4.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oINVAVTEL_3_5.RadioValue()==this.w_INVAVTEL)
      this.oPgFrm.Page3.oPag.oINVAVTEL_3_5.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oCODSOGG_3_6.RadioValue()==this.w_CODSOGG)
      this.oPgFrm.Page3.oPag.oCODSOGG_3_6.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oCFPRESCS_3_7.value==this.w_CFPRESCS)
      this.oPgFrm.Page3.oPag.oCFPRESCS_3_7.value=this.w_CFPRESCS
    endif
    if not(this.oPgFrm.Page3.oPag.oNUMCAF_3_8.value==this.w_NUMCAF)
      this.oPgFrm.Page3.oPag.oNUMCAF_3_8.value=this.w_NUMCAF
    endif
    if not(this.oPgFrm.Page3.oPag.oRFDATIMP_3_9.value==this.w_RFDATIMP)
      this.oPgFrm.Page3.oPag.oRFDATIMP_3_9.value=this.w_RFDATIMP
    endif
    if not(this.oPgFrm.Page3.oPag.oCODFISIN_3_10.value==this.w_CODFISIN)
      this.oPgFrm.Page3.oPag.oCODFISIN_3_10.value=this.w_CODFISIN
    endif
    if not(this.oPgFrm.Page3.oPag.oIMPTRTEL_3_11.RadioValue()==this.w_IMPTRTEL)
      this.oPgFrm.Page3.oPag.oIMPTRTEL_3_11.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oFIRMINT_3_12.RadioValue()==this.w_FIRMINT)
      this.oPgFrm.Page3.oPag.oFIRMINT_3_12.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oRICAVTEL_3_13.RadioValue()==this.w_RICAVTEL)
      this.oPgFrm.Page3.oPag.oRICAVTEL_3_13.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oCFDOMNOT_3_27.value==this.w_CFDOMNOT)
      this.oPgFrm.Page3.oPag.oCFDOMNOT_3_27.value=this.w_CFDOMNOT
    endif
    if not(this.oPgFrm.Page3.oPag.oUFDOMNOT_3_29.value==this.w_UFDOMNOT)
      this.oPgFrm.Page3.oPag.oUFDOMNOT_3_29.value=this.w_UFDOMNOT
    endif
    if not(this.oPgFrm.Page3.oPag.oCODOMNOT_3_30.value==this.w_CODOMNOT)
      this.oPgFrm.Page3.oPag.oCODOMNOT_3_30.value=this.w_CODOMNOT
    endif
    if not(this.oPgFrm.Page3.oPag.oNODOMNOT_3_33.value==this.w_NODOMNOT)
      this.oPgFrm.Page3.oPag.oNODOMNOT_3_33.value=this.w_NODOMNOT
    endif
    if not(this.oPgFrm.Page3.oPag.oCMDOMNOT_3_35.value==this.w_CMDOMNOT)
      this.oPgFrm.Page3.oPag.oCMDOMNOT_3_35.value=this.w_CMDOMNOT
    endif
    if not(this.oPgFrm.Page3.oPag.oPRDOMNOT_3_37.value==this.w_PRDOMNOT)
      this.oPgFrm.Page3.oPag.oPRDOMNOT_3_37.value=this.w_PRDOMNOT
    endif
    if not(this.oPgFrm.Page3.oPag.oCCDOMNOT_3_39.value==this.w_CCDOMNOT)
      this.oPgFrm.Page3.oPag.oCCDOMNOT_3_39.value=this.w_CCDOMNOT
    endif
    if not(this.oPgFrm.Page3.oPag.oCPDOMNOT_3_41.value==this.w_CPDOMNOT)
      this.oPgFrm.Page3.oPag.oCPDOMNOT_3_41.value=this.w_CPDOMNOT
    endif
    if not(this.oPgFrm.Page3.oPag.oVPDOMNOT_3_43.value==this.w_VPDOMNOT)
      this.oPgFrm.Page3.oPag.oVPDOMNOT_3_43.value=this.w_VPDOMNOT
    endif
    if not(this.oPgFrm.Page3.oPag.oINDOMNOT_3_45.value==this.w_INDOMNOT)
      this.oPgFrm.Page3.oPag.oINDOMNOT_3_45.value=this.w_INDOMNOT
    endif
    if not(this.oPgFrm.Page3.oPag.oNCDOMNOT_3_47.value==this.w_NCDOMNOT)
      this.oPgFrm.Page3.oPag.oNCDOMNOT_3_47.value=this.w_NCDOMNOT
    endif
    if not(this.oPgFrm.Page3.oPag.oFRDOMNOT_3_49.value==this.w_FRDOMNOT)
      this.oPgFrm.Page3.oPag.oFRDOMNOT_3_49.value=this.w_FRDOMNOT
    endif
    if not(this.oPgFrm.Page3.oPag.oSEDOMNOT_3_51.value==this.w_SEDOMNOT)
      this.oPgFrm.Page3.oPag.oSEDOMNOT_3_51.value=this.w_SEDOMNOT
    endif
    if not(this.oPgFrm.Page3.oPag.oCEDOMNOT_3_53.value==this.w_CEDOMNOT)
      this.oPgFrm.Page3.oPag.oCEDOMNOT_3_53.value=this.w_CEDOMNOT
    endif
    if not(this.oPgFrm.Page3.oPag.oSFDOMNOT_3_55.value==this.w_SFDOMNOT)
      this.oPgFrm.Page3.oPag.oSFDOMNOT_3_55.value=this.w_SFDOMNOT
    endif
    if not(this.oPgFrm.Page3.oPag.oLRDOMNOT_3_57.value==this.w_LRDOMNOT)
      this.oPgFrm.Page3.oPag.oLRDOMNOT_3_57.value=this.w_LRDOMNOT
    endif
    if not(this.oPgFrm.Page3.oPag.oIEDOMNOT_3_59.value==this.w_IEDOMNOT)
      this.oPgFrm.Page3.oPag.oIEDOMNOT_3_59.value=this.w_IEDOMNOT
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_FOCOGNOME))  and (empty(.w_FODENOMINA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFOCOGNOME_1_11.SetFocus()
            i_bnoObbl = !empty(.w_FOCOGNOME)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_FONOME))  and (empty(.w_FODENOMINA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFONOME_1_12.SetFocus()
            i_bnoObbl = !empty(.w_FONOME)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_FODENOMINA)) or not(not empty(.w_FODENOMINA)))  and (empty(.w_FOCOGNOME) and empty(.w_FONOME))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFODENOMINA_1_13.SetFocus()
            i_bnoObbl = !empty(.w_FODENOMINA)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_FOCODFIS)) or not(iif(.w_PERAZI = 'S',chkcfp(alltrim(.w_FOCODFIS),'CF'),chkcfp(alltrim(.w_FOCODFIS),'PI'))))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFOCODFIS_1_14.SetFocus()
            i_bnoObbl = !empty(.w_FOCODFIS)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Lunghezza codice fiscale o partita IVA non corretta")
          case   not(.w_FOCODFIS <> .w_FOCODSOS and iif(!empty(.w_FOCODSOS),iif(len(alltrim(.w_FOCODSOS))=16,chkcfp(alltrim(.w_FOCODSOS),'CF'),chkcfp(alltrim(.w_FOCODSOS),'PI')),.t.))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFOCODSOS_1_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice fiscale del sostituto d'imposta deve essere diverso dal codice fiscale del dichiarante")
          case   not(.w_FLAGCIPAR='1' and not empty(.w_TIPOPERAZ))  and (.w_FLAGCIPAR='1')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTIPOPERAZ_1_24.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire il tipo dell'operazione")
          case   (empty(.w_DATANASC))  and (.w_Perazi='S' or .w_Forza1)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATANASC_1_26.SetFocus()
            i_bnoObbl = !empty(.w_DATANASC)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_COMUNE))  and (.w_Perazi='S' or .w_Forza1)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOMUNE_1_27.SetFocus()
            i_bnoObbl = !empty(.w_COMUNE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_SESSO))  and (.w_Perazi='S' or .w_Forza1)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSESSO_1_29.SetFocus()
            i_bnoObbl = !empty(.w_SESSO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_VARRES >= cp_chartodate('01-01-2009') or empty(.w_VARRES))  and (.w_Perazi='S' or .w_Forza1)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oVARRES_1_30.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data non pu� essere inferiore al 01.01.2009")
          case   (empty(.w_RECOMUNE))  and (.w_Perazi='S' or .w_Forza1)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRECOMUNE_1_31.SetFocus()
            i_bnoObbl = !empty(.w_RECOMUNE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(! empty(.w_RESIGLA))  and (.w_Perazi='S' or .w_Forza1)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRESIGLA_1_32.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire la sigla della provincia di residenza anagrafica o di domicilio fiscale")
          case   not(! empty(.w_CAP))  and (.w_Perazi='S' or .w_Forza1)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAP_1_35.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire il C.A.P. della residenza anagrafica o del domicilio fiscale del fornitore")
          case   not(at(' ',alltrim(.w_TELEFONO))=0 and at('/',alltrim(.w_TELEFONO))=0 and at('-',alltrim(.w_TELEFONO))=0)  and (.w_Perazi='S' or .w_Forza1)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTELEFONO_1_37.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Non inserire caratteri separatori tra prefisso e numero")
          case   not(.w_SEVARSED >= cp_chartodate('01-01-09') or empty(.w_SEVARSED))  and (.w_Perazi<>'S' or .w_Forza1)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSEVARSED_1_38.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data non pu� essere inferiore al 01.01.2009")
          case   (empty(.w_SECOMUNE))  and (.w_Perazi<>'S' or .w_Forza1)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSECOMUNE_1_39.SetFocus()
            i_bnoObbl = !empty(.w_SECOMUNE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(! empty(.w_SECAP))  and (.w_Perazi<>'S' or .w_Forza1)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSECAP_1_42.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire il C.A.P. della sede legale del fornitore")
          case   not(.w_SEVARDOM >= cp_chartodate('01-01-09') or empty(.w_SEVARDOM))  and (.w_Perazi<>'S' or .w_Forza1)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSEVARDOM_1_44.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data non pu� essere inferiore al 01.01.2009")
          case   not(at(' ',alltrim(.w_SETELEFONO))=0 and at('/',alltrim(.w_SETELEFONO))=0 and at('-',alltrim(.w_SETELEFONO))=0)  and (.w_Perazi<>'S' or .w_Forza1)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSETELEFONO_1_51.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Non inserire caratteri separatori tra prefisso e numero")
          case   not((val(.w_NATGIU)>=1 and val(.w_NATGIU)<=44) OR (val(.w_NATGIU)>=50 and val(.w_NATGIU)<=58))  and (.w_Perazi<>'S' or .w_Forza1)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNATGIU_1_52.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valore non ammesso per natura giuridica")
          case   not(iif(not empty(.w_CODFISDA),chkcfp(alltrim(.w_CODFISDA),'PI'),.T.))  and (.w_Perazi<>'S' or .w_Forza1)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODFISDA_1_55.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(iif(not empty(.w_RFCODFIS),chkcfp(alltrim(.w_RFCODFIS),'CF'),.T.))  and (.w_PERAZI <> 'S' or (.w_Forza2 and .w_PERAZI='S'))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oRFCODFIS_2_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_RFCODCAR))  and (.w_PERAZI <> 'S' or .w_Forza2)
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oRFCODCAR_2_3.SetFocus()
            i_bnoObbl = !empty(.w_RFCODCAR)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(at(' ',alltrim(.w_RFTELEFONO))=0 and at('/',alltrim(.w_RFTELEFONO))=0 and at('-',alltrim(.w_RFTELEFONO))=0)  and (.w_Perazi<>'S' or .w_Forza2)
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oRFTELEFONO_2_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Non inserire caratteri separatori tra prefisso e numero")
          case   not(iif(not empty(.w_RFCFINS2),chkcfp(alltrim(.w_RFCFINS2),'CF'),iif(.w_SEZ4='1',.F.,.T.) ))  and (.w_SEZ4='1')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oRFCFINS2_2_38.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire il codice fiscale dell'intermediario della sezione IV")
          case   ((empty(.w_DATINI)) or not((.w_DATFIN>=.w_DATINI) AND (.w_DATINI > CTOD("31-12-08"))))
            .oPgFrm.ActivePage = 5
            .oPgFrm.Page5.oPag.oDATINI_5_1.SetFocus()
            i_bnoObbl = !empty(.w_DATINI)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � maggiore della data finale o fuori periodo")
          case   ((empty(.w_DATFIN)) or not((.w_DATFIN>=.w_DATINI) AND (.w_DATFIN < CTOD("01-01-2010"))))
            .oPgFrm.ActivePage = 5
            .oPgFrm.Page5.oPag.oDATFIN_5_2.SetFocus()
            i_bnoObbl = !empty(.w_DATFIN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � maggiore della data finale o fuori periodo")
          case   not((.w_Perazi='S' and  .w_TIPFORN<>'06' ) or .w_Perazi<>'S')
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oTIPFORN_4_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Selezione non consentita con tipo fornitore persona fisica")
          case   not(iif(not empty(.w_CFRESCAF),chkcfp(alltrim(.w_CFRESCAF),'CF'),.T.))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oCFRESCAF_4_20.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(iif(not empty(.w_CFDELCAF),chkcfp(alltrim(.w_CFDELCAF),'PI'),.T.))  and not(.w_SELCAF <> '1')
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oCFDELCAF_4_21.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(((EMPTY(.w_PERCFIN)) OR (.w_PERCIN<=.w_PERCFIN)) AND (.w_RITE $ 'CS' AND .w_TIPCLF<>'A'))  and not(empty(.w_PERCIN))
            .oPgFrm.ActivePage = 5
            .oPgFrm.Page5.oPag.oPERCIN_5_24.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice fornitore inesistente o obsoleto o non soggetto a ritenute o agente")
          case   not(((.w_PERCFIN>=.w_PERCIN) OR (EMPTY(.w_PERCIN))) AND (.w_RITE $ 'CS' AND .w_TIPCLF<>'A'))  and not(empty(.w_PERCFIN))
            .oPgFrm.ActivePage = 5
            .oPgFrm.Page5.oPag.oPERCFIN_5_26.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice fornitore inesistente o obsoleto o non soggetto a ritenute o agente")
          case   not(iif(not empty(.w_CFINCCON),chkcfp(alltrim(.w_CFINCCON),'CF'),.T.))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oCFINCCON_3_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(iif(not empty(.w_CFPRESCS),chkcfp(alltrim(.w_CFPRESCS),'CF'),.T.))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oCFPRESCS_3_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(iif(not empty(.w_CODFISIN),chkcfp(alltrim(.w_CODFISIN),'CF'),.T.))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oCODFISIN_3_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(iif(not empty(.w_CFDOMNOT),chkcfp(alltrim(.w_CFDOMNOT),'CF'),.T.))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oCFDOMNOT_3_27.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODAZI = this.w_CODAZI
    this.o_PERAZI = this.w_PERAZI
    this.o_FOCOGNOME = this.w_FOCOGNOME
    this.o_FONOME = this.w_FONOME
    this.o_FODENOMINA = this.w_FODENOMINA
    this.o_FOCODFIS = this.w_FOCODFIS
    this.o_FLAGEVE = this.w_FLAGEVE
    this.o_Forza1 = this.w_Forza1
    this.o_FLAGCOR = this.w_FLAGCOR
    this.o_FLAGINT = this.w_FLAGINT
    this.o_FLAGCIPAR = this.w_FLAGCIPAR
    this.o_DATANASC = this.w_DATANASC
    this.o_COMUNE = this.w_COMUNE
    this.o_SIGLA = this.w_SIGLA
    this.o_SESSO = this.w_SESSO
    this.o_RECOMUNE = this.w_RECOMUNE
    this.o_RESIGLA = this.w_RESIGLA
    this.o_INDIRIZ = this.w_INDIRIZ
    this.o_CAP = this.w_CAP
    this.o_SECOMUNE = this.w_SECOMUNE
    this.o_SESIGLA = this.w_SESIGLA
    this.o_SEINDIRI2 = this.w_SEINDIRI2
    this.o_SECAP = this.w_SECAP
    this.o_SERCOMUN = this.w_SERCOMUN
    this.o_SERSIGLA = this.w_SERSIGLA
    this.o_SERCAP = this.w_SERCAP
    this.o_SERINDIR = this.w_SERINDIR
    this.o_NOME = this.w_NOME
    this.o_Forza2 = this.w_Forza2
    this.o_RFCODCAR = this.w_RFCODCAR
    this.o_RFDENOMI = this.w_RFDENOMI
    this.o_RFCOGNOME = this.w_RFCOGNOME
    this.o_RFSIGLA = this.w_RFSIGLA
    this.o_SEZ1 = this.w_SEZ1
    this.o_SEZ2 = this.w_SEZ2
    this.o_SEZ4 = this.w_SEZ4
    this.o_DATFIN = this.w_DATFIN
    this.o_TIPFORN = this.w_TIPFORN
    this.o_SELCAF = this.w_SELCAF
    this.o_CFINCCON = this.w_CFINCCON
    this.o_FIRMPRCS = this.w_FIRMPRCS
    this.o_INVAVTEL = this.w_INVAVTEL
    this.o_CFPRESCS = this.w_CFPRESCS
    this.o_CODFISIN = this.w_CODFISIN
    this.o_IMPTRTEL = this.w_IMPTRTEL
    this.o_SEDOMNOT = this.w_SEDOMNOT
    return

enddefine

* --- Define pages as container
define class tgsriasftPag1 as StdContainer
  Width  = 730
  height = 545
  stdWidth  = 730
  stdheight = 545
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oFOCOGNOME_1_11 as StdField with uid="SZIDZXIFTU",rtseq=11,rtrep=.f.,;
    cFormVar = "w_FOCOGNOME", cQueryName = "FOCOGNOME",;
    bObbl = .t. , nPag = 1, value=space(24), bMultilanguage =  .f.,;
    ToolTipText = "Cognome del fornitore",;
    HelpContextID = 262434317,;
   bGlobalFont=.t.,;
    Height=21, Width=177, Left=101, Top=10, cSayPict="repl('!',24)", cGetPict="repl('!',24)", InputMask=replicate('X',24)

  func oFOCOGNOME_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_FODENOMINA))
    endwith
   endif
  endfunc

  add object oFONOME_1_12 as StdField with uid="NPXPQLRYZB",rtseq=12,rtrep=.f.,;
    cFormVar = "w_FONOME", cQueryName = "FONOME",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Nome del fornitore",;
    HelpContextID = 138658474,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=325, Top=10, cSayPict="repl('!',20)", cGetPict="repl('!',20)", InputMask=replicate('X',20)

  func oFONOME_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_FODENOMINA))
    endwith
   endif
  endfunc

  add object oFODENOMINA_1_13 as StdField with uid="WNCFJWGGXA",rtseq=13,rtrep=.f.,;
    cFormVar = "w_FODENOMINA", cQueryName = "FODENOMINA",;
    bObbl = .t. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Denominazione del fornitore",;
    HelpContextID = 238951553,;
   bGlobalFont=.t.,;
    Height=21, Width=592, Left=101, Top=34, cSayPict="repl('!',60)", cGetPict="repl('!',60)", InputMask=replicate('X',60)

  func oFODENOMINA_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_FOCOGNOME) and empty(.w_FONOME))
    endwith
   endif
  endfunc

  func oFODENOMINA_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (not empty(.w_FODENOMINA))
    endwith
    return bRes
  endfunc

  add object oFOCODFIS_1_14 as StdField with uid="NQSHPZVXNW",rtseq=14,rtrep=.f.,;
    cFormVar = "w_FOCODFIS", cQueryName = "FOCODFIS",;
    bObbl = .t. , nPag = 1, value=space(16), bMultilanguage =  .f.,;
    sErrorMsg = "Lunghezza codice fiscale o partita IVA non corretta",;
    HelpContextID = 131363415,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=101, Top=58, cSayPict="repl('!',16)", cGetPict="repl('!',16)", InputMask=replicate('X',16)

  func oFOCODFIS_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (iif(.w_PERAZI = 'S',chkcfp(alltrim(.w_FOCODFIS),'CF'),chkcfp(alltrim(.w_FOCODFIS),'PI')))
    endwith
    return bRes
  endfunc

  add object oFOCODSOS_1_15 as StdField with uid="MWHDNMTQGN",rtseq=15,rtrep=.f.,;
    cFormVar = "w_FOCODSOS", cQueryName = "FOCODSOS",;
    bObbl = .f. , nPag = 1, value=space(16), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice fiscale del sostituto d'imposta deve essere diverso dal codice fiscale del dichiarante",;
    ToolTipText = "Codice fiscale del sostituto d'imposta, da compilare solo in caso di operazioni straodinarie e successioni",;
    HelpContextID = 181695063,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=547, Top=58, cSayPict="repl('!',16)", cGetPict="repl('!',16)", InputMask=replicate('X',16), tabstop=.F.

  func oFOCODSOS_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_FOCODFIS <> .w_FOCODSOS and iif(!empty(.w_FOCODSOS),iif(len(alltrim(.w_FOCODSOS))=16,chkcfp(alltrim(.w_FOCODSOS),'CF'),chkcfp(alltrim(.w_FOCODSOS),'PI')),.t.))
    endwith
    return bRes
  endfunc

  add object oFLAGEVE_1_17 as StdCheck with uid="WYTEXMRWTY",rtseq=17,rtrep=.f.,left=40, top=121, caption="Eventi eccezionali",;
    HelpContextID = 137587286,;
    cFormVar="w_FLAGEVE", bObbl = .f. , nPag = 1;
    , tabstop =.f.;
   , bGlobalFont=.t.


  func oFLAGEVE_1_17.RadioValue()
    return(iif(this.value =1,'01',;
    '00'))
  endfunc
  func oFLAGEVE_1_17.GetRadio()
    this.Parent.oContained.w_FLAGEVE = this.RadioValue()
    return .t.
  endfunc

  func oFLAGEVE_1_17.SetRadio()
    this.Parent.oContained.w_FLAGEVE=trim(this.Parent.oContained.w_FLAGEVE)
    this.value = ;
      iif(this.Parent.oContained.w_FLAGEVE=='01',1,;
      0)
  endfunc


  add object oCODEVECC_1_18 as StdCombo with uid="BYLGKOJBJM",value=1,rtseq=18,rtrep=.f.,left=40,top=174,width=380,height=21;
    , ToolTipText = "Eventi eccezionali";
    , HelpContextID = 138517865;
    , cFormVar="w_CODEVECC",RowSource=""+"0 - Nessuno,"+"1 - Contribuenti vittime di richieste estorsive ex. Art.20 Co.2 legge n. 44/99,"+"3 - Contribuenti residenti al 6/4/2009 nei comuni Abruzzo (sisma),"+"5 - Altri eventi eccezionali", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCODEVECC_1_18.RadioValue()
    return(iif(this.value =1,' ',;
    iif(this.value =2,'1',;
    iif(this.value =3,'3',;
    iif(this.value =4,'5',;
    space(1))))))
  endfunc
  func oCODEVECC_1_18.GetRadio()
    this.Parent.oContained.w_CODEVECC = this.RadioValue()
    return .t.
  endfunc

  func oCODEVECC_1_18.SetRadio()
    this.Parent.oContained.w_CODEVECC=trim(this.Parent.oContained.w_CODEVECC)
    this.value = ;
      iif(this.Parent.oContained.w_CODEVECC=='',1,;
      iif(this.Parent.oContained.w_CODEVECC=='1',2,;
      iif(this.Parent.oContained.w_CODEVECC=='3',3,;
      iif(this.Parent.oContained.w_CODEVECC=='5',4,;
      0))))
  endfunc

  func oCODEVECC_1_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLAGEVE='01')
    endwith
   endif
  endfunc

  add object oFLAGCONF_1_19 as StdCheck with uid="EXJMLGCXGO",rtseq=19,rtrep=.f.,left=312, top=121, caption="Conferma",;
    HelpContextID = 250385764,;
    cFormVar="w_FLAGCONF", bObbl = .f. , nPag = 1;
    , tabstop =.f.;
   , bGlobalFont=.t.


  func oFLAGCONF_1_19.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oFLAGCONF_1_19.GetRadio()
    this.Parent.oContained.w_FLAGCONF = this.RadioValue()
    return .t.
  endfunc

  func oFLAGCONF_1_19.SetRadio()
    this.Parent.oContained.w_FLAGCONF=trim(this.Parent.oContained.w_FLAGCONF)
    this.value = ;
      iif(this.Parent.oContained.w_FLAGCONF=='1',1,;
      0)
  endfunc

  add object oForza1_1_20 as StdCheck with uid="WYTWDOWGTW",rtseq=20,rtrep=.f.,left=312, top=147, caption="Forza editing",;
    HelpContextID = 181822122,;
    cFormVar="w_Forza1", bObbl = .f. , nPag = 1;
    , tabstop = .f.;
   , bGlobalFont=.t.


  func oForza1_1_20.RadioValue()
    return(iif(this.value =1,.t.,;
    .f.))
  endfunc
  func oForza1_1_20.GetRadio()
    this.Parent.oContained.w_Forza1 = this.RadioValue()
    return .t.
  endfunc

  func oForza1_1_20.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_Forza1==.t.,1,;
      0)
  endfunc

  add object oFLAGCOR_1_21 as StdCheck with uid="KAIBPTXLER",rtseq=21,rtrep=.f.,left=472, top=121, caption="Correttiva",;
    HelpContextID = 18049622,;
    cFormVar="w_FLAGCOR", bObbl = .f. , nPag = 1;
    , tabstop =.f.;
   , bGlobalFont=.t.


  func oFLAGCOR_1_21.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oFLAGCOR_1_21.GetRadio()
    this.Parent.oContained.w_FLAGCOR = this.RadioValue()
    return .t.
  endfunc

  func oFLAGCOR_1_21.SetRadio()
    this.Parent.oContained.w_FLAGCOR=trim(this.Parent.oContained.w_FLAGCOR)
    this.value = ;
      iif(this.Parent.oContained.w_FLAGCOR=='1',1,;
      0)
  endfunc

  add object oFLAGINT_1_22 as StdCheck with uid="SSJZDRXGKM",rtseq=22,rtrep=.f.,left=472, top=147, caption="Integrativa",;
    HelpContextID = 7563862,;
    cFormVar="w_FLAGINT", bObbl = .f. , nPag = 1;
    , tabstop =.f.;
   , bGlobalFont=.t.


  func oFLAGINT_1_22.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oFLAGINT_1_22.GetRadio()
    this.Parent.oContained.w_FLAGINT = this.RadioValue()
    return .t.
  endfunc

  func oFLAGINT_1_22.SetRadio()
    this.Parent.oContained.w_FLAGINT=trim(this.Parent.oContained.w_FLAGINT)
    this.value = ;
      iif(this.Parent.oContained.w_FLAGINT=='1',1,;
      0)
  endfunc

  add object oFLAGCIPAR_1_23 as StdCheck with uid="GXCNAWBXKB",rtseq=23,rtrep=.f.,left=472, top=173, caption="Correttiva/Integrativa parziale",;
    ToolTipText = "Se attivo: dichiarazione correttiva/integrativa parziale",;
    HelpContextID = 82612297,;
    cFormVar="w_FLAGCIPAR", bObbl = .f. , nPag = 1;
    , tabstop =.f.;
   , bGlobalFont=.t.


  func oFLAGCIPAR_1_23.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oFLAGCIPAR_1_23.GetRadio()
    this.Parent.oContained.w_FLAGCIPAR = this.RadioValue()
    return .t.
  endfunc

  func oFLAGCIPAR_1_23.SetRadio()
    this.Parent.oContained.w_FLAGCIPAR=trim(this.Parent.oContained.w_FLAGCIPAR)
    this.value = ;
      iif(this.Parent.oContained.w_FLAGCIPAR=='1',1,;
      0)
  endfunc

  func oFLAGCIPAR_1_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLAGINT='1' or .w_FLAGCOR='1')
    endwith
   endif
  endfunc


  add object oTIPOPERAZ_1_24 as StdCombo with uid="FVZJLSCZCJ",value=4,rtseq=24,rtrep=.f.,left=579,top=144,width=114,height=21;
    , tabstop =.f.;
    , ToolTipText = "Tipo operazioni possibili in correzione o integrazione";
    , HelpContextID = 132931095;
    , cFormVar="w_TIPOPERAZ",RowSource=""+"Inserimento,"+"Aggiornamento,"+"Cancellazione,"+"Nessuna", bObbl = .f. , nPag = 1;
    , sErrorMsg = "Inserire il tipo dell'operazione";
  , bGlobalFont=.t.


  func oTIPOPERAZ_1_24.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'A',;
    iif(this.value =3,'C',;
    iif(this.value =4,' ',;
    ' ')))))
  endfunc
  func oTIPOPERAZ_1_24.GetRadio()
    this.Parent.oContained.w_TIPOPERAZ = this.RadioValue()
    return .t.
  endfunc

  func oTIPOPERAZ_1_24.SetRadio()
    this.Parent.oContained.w_TIPOPERAZ=trim(this.Parent.oContained.w_TIPOPERAZ)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOPERAZ=='I',1,;
      iif(this.Parent.oContained.w_TIPOPERAZ=='A',2,;
      iif(this.Parent.oContained.w_TIPOPERAZ=='C',3,;
      iif(this.Parent.oContained.w_TIPOPERAZ=='',4,;
      0))))
  endfunc

  func oTIPOPERAZ_1_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLAGCIPAR='1')
    endwith
   endif
  endfunc

  func oTIPOPERAZ_1_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_FLAGCIPAR='1' and not empty(.w_TIPOPERAZ))
    endwith
    return bRes
  endfunc

  add object oDATANASC_1_26 as StdField with uid="WGYOTWHLZZ",rtseq=26,rtrep=.f.,;
    cFormVar = "w_DATANASC", cQueryName = "DATANASC",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di nascita del fornitore",;
    HelpContextID = 62820217,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=11, Top=234

  func oDATANASC_1_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' or .w_Forza1)
    endwith
   endif
  endfunc

  add object oCOMUNE_1_27 as StdField with uid="QDXVFBYYJG",rtseq=27,rtrep=.f.,;
    cFormVar = "w_COMUNE", cQueryName = "COMUNE",;
    bObbl = .t. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune (o stato estero) di nascita del fornitore",;
    HelpContextID = 137220826,;
   bGlobalFont=.t.,;
    Height=21, Width=319, Left=121, Top=234, cSayPict='repl("!",40)', cGetPict='repl("!",40)', InputMask=replicate('X',40)

  func oCOMUNE_1_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' or .w_Forza1)
    endwith
   endif
  endfunc

  add object oSIGLA_1_28 as StdField with uid="AQSAXFLTQI",rtseq=28,rtrep=.f.,;
    cFormVar = "w_SIGLA", cQueryName = "SIGLA",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia di nascita del fornitore",;
    HelpContextID = 235354074,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=448, Top=234, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2)

  func oSIGLA_1_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' or .w_Forza1)
    endwith
   endif
  endfunc


  add object oSESSO_1_29 as StdCombo with uid="NCCDEVHYQI",rtseq=29,rtrep=.f.,left=523,top=235,width=92,height=21;
    , height = 21;
    , ToolTipText = "Sesso del fornitore";
    , HelpContextID = 220167130;
    , cFormVar="w_SESSO",RowSource=""+"Maschio,"+"Femmina", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oSESSO_1_29.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oSESSO_1_29.GetRadio()
    this.Parent.oContained.w_SESSO = this.RadioValue()
    return .t.
  endfunc

  func oSESSO_1_29.SetRadio()
    this.Parent.oContained.w_SESSO=trim(this.Parent.oContained.w_SESSO)
    this.value = ;
      iif(this.Parent.oContained.w_SESSO=='M',1,;
      iif(this.Parent.oContained.w_SESSO=='F',2,;
      0))
  endfunc

  func oSESSO_1_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' or .w_Forza1)
    endwith
   endif
  endfunc

  add object oVARRES_1_30 as StdField with uid="LCJAHFQKSB",rtseq=30,rtrep=.f.,;
    cFormVar = "w_VARRES", cQueryName = "VARRES",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data non pu� essere inferiore al 01.01.2009",;
    ToolTipText = "Data della variazione",;
    HelpContextID = 88043606,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=11, Top=271

  func oVARRES_1_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' or .w_Forza1)
    endwith
   endif
  endfunc

  func oVARRES_1_30.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_VARRES >= cp_chartodate('01-01-2009') or empty(.w_VARRES))
    endwith
    return bRes
  endfunc

  add object oRECOMUNE_1_31 as StdField with uid="OZIDXVLWTW",rtseq=31,rtrep=.f.,;
    cFormVar = "w_RECOMUNE", cQueryName = "RECOMUNE",;
    bObbl = .t. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune di residenza anagrafica o di domicilio fiscale del fornitore",;
    HelpContextID = 138705829,;
   bGlobalFont=.t.,;
    Height=21, Width=319, Left=121, Top=271, cSayPict='repl("!",40)', cGetPict='repl("!",40)', InputMask=replicate('X',40)

  func oRECOMUNE_1_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' or .w_Forza1)
    endwith
   endif
  endfunc

  add object oRESIGLA_1_32 as StdField with uid="SUWJKWJLTK",rtseq=32,rtrep=.f.,;
    cFormVar = "w_RESIGLA", cQueryName = "RESIGLA",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Inserire la sigla della provincia di residenza anagrafica o di domicilio fiscale",;
    ToolTipText = "Sigla della provincia di residenza anagrafica o di domicilio fiscale",;
    HelpContextID = 240550934,;
   bGlobalFont=.t.,;
    Height=21, Width=32, Left=448, Top=271, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2)

  func oRESIGLA_1_32.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' or .w_Forza1)
    endwith
   endif
  endfunc

  func oRESIGLA_1_32.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (! empty(.w_RESIGLA))
    endwith
    return bRes
  endfunc

  add object oRECODCOM_1_33 as StdField with uid="NRKWZXCBJQ",rtseq=33,rtrep=.f.,;
    cFormVar = "w_RECODCOM", cQueryName = "RECODCOM",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Codice comune ",;
    HelpContextID = 181697437,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=523, Top=271, cSayPict="repl('!',4)", cGetPict="repl('!',4)", InputMask=replicate('X',4)

  func oRECODCOM_1_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' or .w_Forza1)
    endwith
   endif
  endfunc

  add object oINDIRIZ_1_34 as StdField with uid="ZFPHCRDCWK",rtseq=34,rtrep=.f.,;
    cFormVar = "w_INDIRIZ", cQueryName = "INDIRIZ",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo ( frazione, via e numero civico) di residenza anagrafica o domicilio",;
    HelpContextID = 66741114,;
   bGlobalFont=.t.,;
    Height=21, Width=319, Left=121, Top=306, cSayPict='repl("!",35)', cGetPict='repl("!",35)', InputMask=replicate('X',35)

  func oINDIRIZ_1_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' or .w_Forza1)
    endwith
   endif
  endfunc

  add object oCAP_1_35 as StdField with uid="MDJULVDKRQ",rtseq=35,rtrep=.f.,;
    cFormVar = "w_CAP", cQueryName = "CAP",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Inserire il C.A.P. della residenza anagrafica o del domicilio fiscale del fornitore",;
    ToolTipText = "C.A.P. della residenza anagrafica o del domicilio fiscale del fornitore",;
    HelpContextID = 40022234,;
   bGlobalFont=.t.,;
    Height=21, Width=56, Left=448, Top=306, cSayPict="'99999'", cGetPict="'99999'", InputMask=replicate('X',5)

  func oCAP_1_35.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' or .w_Forza1)
    endwith
   endif
  endfunc

  func oCAP_1_35.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (! empty(.w_CAP))
    endwith
    return bRes
  endfunc

  add object oCODATT_1_36 as StdField with uid="UFNDOTUYER",rtseq=36,rtrep=.f.,;
    cFormVar = "w_CODATT", cQueryName = "CODATT",;
    bObbl = .f. , nPag = 1, value=space(6), bMultilanguage =  .f.,;
    ToolTipText = "Codice attivit�",;
    HelpContextID = 119381286,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=523, Top=306, cSayPict='repl("!",6)', cGetPict='repl("!",6)', InputMask=replicate('X',6), bHasZoom = .t. 

  func oCODATT_1_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' or .w_Forza1)
    endwith
   endif
  endfunc

  proc oCODATT_1_36.mZoom
    vx_exec("gsri8sft.vzm",this.Parent.oContained)
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oTELEFONO_1_37 as StdField with uid="QVUZNFWEXA",rtseq=37,rtrep=.f.,;
    cFormVar = "w_TELEFONO", cQueryName = "TELEFONO",;
    bObbl = .f. , nPag = 1, value=space(12), bMultilanguage =  .f.,;
    sErrorMsg = "Non inserire caratteri separatori tra prefisso e numero",;
    ToolTipText = "Numero di telefono, inserire solo numeri",;
    HelpContextID = 247327611,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=602, Top=306, cSayPict="repl('!',12)", cGetPict="repl('!',12)", InputMask=replicate('X',12)

  func oTELEFONO_1_37.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' or .w_Forza1)
    endwith
   endif
  endfunc

  func oTELEFONO_1_37.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (at(' ',alltrim(.w_TELEFONO))=0 and at('/',alltrim(.w_TELEFONO))=0 and at('-',alltrim(.w_TELEFONO))=0)
    endwith
    return bRes
  endfunc

  add object oSEVARSED_1_38 as StdField with uid="MKYAOPUZVB",rtseq=38,rtrep=.f.,;
    cFormVar = "w_SEVARSED", cQueryName = "SEVARSED",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data non pu� essere inferiore al 01.01.2009",;
    ToolTipText = "Data variazione sede legale",;
    HelpContextID = 100578410,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=11, Top=374

  func oSEVARSED_1_38.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza1)
    endwith
   endif
  endfunc

  func oSEVARSED_1_38.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_SEVARSED >= cp_chartodate('01-01-09') or empty(.w_SEVARSED))
    endwith
    return bRes
  endfunc

  add object oSECOMUNE_1_39 as StdField with uid="NFZSOTAWKE",rtseq=39,rtrep=.f.,;
    cFormVar = "w_SECOMUNE", cQueryName = "SECOMUNE",;
    bObbl = .t. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune della sede legale del fornitore",;
    HelpContextID = 138705813,;
   bGlobalFont=.t.,;
    Height=21, Width=319, Left=121, Top=374, cSayPict='repl("!",40)', cGetPict='repl("!",40)', InputMask=replicate('X',40)

  func oSECOMUNE_1_39.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza1)
    endwith
   endif
  endfunc

  add object oSESIGLA_1_40 as StdField with uid="OOQMIUMTFG",rtseq=40,rtrep=.f.,;
    cFormVar = "w_SESIGLA", cQueryName = "SESIGLA",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia della sede legale del fornitore",;
    HelpContextID = 240550950,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=448, Top=374, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2)

  func oSESIGLA_1_40.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza1)
    endwith
   endif
  endfunc

  add object oSEINDIRI2_1_41 as StdField with uid="OXQZLOKZQT",rtseq=41,rtrep=.f.,;
    cFormVar = "w_SEINDIRI2", cQueryName = "SEINDIRI2",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo della sede legale del fornitore",;
    HelpContextID = 187361167,;
   bGlobalFont=.t.,;
    Height=21, Width=319, Left=121, Top=408, cSayPict='repl("!",35)', cGetPict='repl("!",35)', InputMask=replicate('X',35)

  func oSEINDIRI2_1_41.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza1)
    endwith
   endif
  endfunc

  add object oSECAP_1_42 as StdField with uid="AVSZVWBCSZ",rtseq=42,rtrep=.f.,;
    cFormVar = "w_SECAP", cQueryName = "SECAP",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Inserire il C.A.P. della sede legale del fornitore",;
    ToolTipText = "C.A.P. della sede legale del fornitore",;
    HelpContextID = 220363738,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=448, Top=408, cSayPict="'99999'", cGetPict="'99999'", InputMask=replicate('X',5)

  func oSECAP_1_42.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza1)
    endwith
   endif
  endfunc

  func oSECAP_1_42.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (! empty(.w_SECAP))
    endwith
    return bRes
  endfunc

  add object oSECODCOM_1_43 as StdField with uid="PSHALDQRCE",rtseq=43,rtrep=.f.,;
    cFormVar = "w_SECODCOM", cQueryName = "SECODCOM",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Codice comune",;
    HelpContextID = 181697421,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=534, Top=408, cSayPict="repl('!',4)", cGetPict="repl('!',4)", InputMask=replicate('X',4)

  func oSECODCOM_1_43.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza1)
    endwith
   endif
  endfunc

  add object oSEVARDOM_1_44 as StdField with uid="CYDZEIXVFX",rtseq=44,rtrep=.f.,;
    cFormVar = "w_SEVARDOM", cQueryName = "SEVARDOM",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data non pu� essere inferiore al 01.01.2009",;
    ToolTipText = "Data variazione domicilio fiscale",;
    HelpContextID = 151079821,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=11, Top=442

  func oSEVARDOM_1_44.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza1)
    endwith
   endif
  endfunc

  func oSEVARDOM_1_44.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_SEVARDOM >= cp_chartodate('01-01-09') or empty(.w_SEVARDOM))
    endwith
    return bRes
  endfunc

  add object oSERCOMUN_1_45 as StdField with uid="KMAFMNRNPP",rtseq=45,rtrep=.f.,;
    cFormVar = "w_SERCOMUN", cQueryName = "SERCOMUN",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune di domicilio fiscale del fornitore",;
    HelpContextID = 265319540,;
   bGlobalFont=.t.,;
    Height=21, Width=319, Left=121, Top=442, cSayPict='repl("!",40)', cGetPict='repl("!",40)', InputMask=replicate('X',40)

  func oSERCOMUN_1_45.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza1)
    endwith
   endif
  endfunc

  add object oSERSIGLA_1_46 as StdField with uid="TGOHNSUWWG",rtseq=46,rtrep=.f.,;
    cFormVar = "w_SERSIGLA", cQueryName = "SERSIGLA",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia di domicilio fiscale del fornitore",;
    HelpContextID = 109022105,;
   bGlobalFont=.t.,;
    Height=21, Width=32, Left=448, Top=442, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2)

  func oSERSIGLA_1_46.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza1)
    endwith
   endif
  endfunc

  add object oSERCODCOM_1_47 as StdField with uid="SCNIBQOVNV",rtseq=47,rtrep=.f.,;
    cFormVar = "w_SERCODCOM", cQueryName = "SERCODCOM",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Codice comune",;
    HelpContextID = 114325829,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=534, Top=442, cSayPict="repl('!',4)", cGetPict="repl('!',4)", InputMask=replicate('X',4)

  func oSERCODCOM_1_47.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza1)
    endwith
   endif
  endfunc

  add object oSERCAP_1_48 as StdField with uid="KHVCIKTTRJ",rtseq=48,rtrep=.f.,;
    cFormVar = "w_SERCAP", cQueryName = "SERCAP",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "C.A.P. del domicilio fiscale del fornitore",;
    HelpContextID = 32535590,;
   bGlobalFont=.t.,;
    Height=21, Width=56, Left=624, Top=442, cSayPict="'99999'", cGetPict="'99999'", InputMask=replicate('X',5)

  func oSERCAP_1_48.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza1)
    endwith
   endif
  endfunc

  add object oSERINDIR_1_49 as StdField with uid="ZEWEROQCEA",rtseq=49,rtrep=.f.,;
    cFormVar = "w_SERINDIR", cQueryName = "SERINDIR",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo ( frazione, via e numero civico) di domicilio fiscale del fornitore",;
    HelpContextID = 154766216,;
   bGlobalFont=.t.,;
    Height=21, Width=319, Left=121, Top=476, cSayPict='repl("!",35)', cGetPict='repl("!",35)', InputMask=replicate('X',35)

  func oSERINDIR_1_49.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza1)
    endwith
   endif
  endfunc

  add object oSECODATT_1_50 as StdField with uid="RQBOLJQSDR",rtseq=50,rtrep=.f.,;
    cFormVar = "w_SECODATT", cQueryName = "SECODATT",;
    bObbl = .f. , nPag = 1, value=space(6), bMultilanguage =  .f.,;
    ToolTipText = "Codice attivit�",;
    HelpContextID = 53183610,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=448, Top=476, cSayPict='repl("!",6)', cGetPict='repl("!",6)', InputMask=replicate('X',6), bHasZoom = .t. 

  func oSECODATT_1_50.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza1)
    endwith
   endif
  endfunc

  proc oSECODATT_1_50.mZoom
    vx_exec("gsri8sft1.vzm",this.Parent.oContained)
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oSETELEFONO_1_51 as StdField with uid="TDMFKZTMEM",rtseq=51,rtrep=.f.,;
    cFormVar = "w_SETELEFONO", cQueryName = "SETELEFONO",;
    bObbl = .f. , nPag = 1, value=space(12), bMultilanguage =  .f.,;
    sErrorMsg = "Non inserire caratteri separatori tra prefisso e numero",;
    ToolTipText = "Numero di telefono, inserire solo numeri",;
    HelpContextID = 128116821,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=534, Top=476, cSayPict="repl('!',12)", cGetPict="repl('!',12)", InputMask=replicate('X',12)

  func oSETELEFONO_1_51.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza1)
    endwith
   endif
  endfunc

  func oSETELEFONO_1_51.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (at(' ',alltrim(.w_SETELEFONO))=0 and at('/',alltrim(.w_SETELEFONO))=0 and at('-',alltrim(.w_SETELEFONO))=0)
    endwith
    return bRes
  endfunc

  add object oNATGIU_1_52 as StdField with uid="GGFCANZHTS",rtseq=52,rtrep=.f.,;
    cFormVar = "w_NATGIU", cQueryName = "NATGIU",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Valore non ammesso per natura giuridica",;
    ToolTipText = "Tipologia natura giuridica",;
    HelpContextID = 125079510,;
    FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=26, Left=644, Top=476, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2)

  func oNATGIU_1_52.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza1)
    endwith
   endif
  endfunc

  func oNATGIU_1_52.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((val(.w_NATGIU)>=1 and val(.w_NATGIU)<=44) OR (val(.w_NATGIU)>=50 and val(.w_NATGIU)<=58))
    endwith
    return bRes
  endfunc


  add object oSTATO_1_53 as StdCombo with uid="HVJLCFQSUG",value=5,rtseq=53,rtrep=.f.,left=121,top=511,width=128,height=21;
    , height = 21;
    , ToolTipText = "Stato della societ� o ente.";
    , HelpContextID = 220171482;
    , cFormVar="w_STATO",RowSource=""+"1 - Soggetto in normale attivit�,"+"2 - Soggetto in liquidazione per cessazione attivit�,"+"3 - Soggetto in fallimento o in liquidazione coatta amministrativa,"+"4 - Soggetto estinto,"+"Non selezionato", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSTATO_1_53.RadioValue()
    return(iif(this.value =1,'1',;
    iif(this.value =2,'2',;
    iif(this.value =3,'3',;
    iif(this.value =4,'4',;
    iif(this.value =5,' ',;
    space(1)))))))
  endfunc
  func oSTATO_1_53.GetRadio()
    this.Parent.oContained.w_STATO = this.RadioValue()
    return .t.
  endfunc

  func oSTATO_1_53.SetRadio()
    this.Parent.oContained.w_STATO=trim(this.Parent.oContained.w_STATO)
    this.value = ;
      iif(this.Parent.oContained.w_STATO=='1',1,;
      iif(this.Parent.oContained.w_STATO=='2',2,;
      iif(this.Parent.oContained.w_STATO=='3',3,;
      iif(this.Parent.oContained.w_STATO=='4',4,;
      iif(this.Parent.oContained.w_STATO=='',5,;
      0)))))
  endfunc

  func oSTATO_1_53.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza1)
    endwith
   endif
  endfunc


  add object oSITUAZ_1_54 as StdCombo with uid="QWWEIZUDRN",value=1,rtseq=54,rtrep=.f.,left=257,top=511,width=183,height=21;
    , height = 21;
    , ToolTipText = "Situazione della societ� o ente";
    , HelpContextID = 201496614;
    , cFormVar="w_SITUAZ",RowSource=""+"Non selezionata,"+"1 - Periodo imposta inizio liquidazione,"+"2 - Periodo imposta successivo a periodo liquidazione o fallimento,"+"3 - Periodo imposta termine liquidazione,"+"5 - Periodo imposta avvenuta trasformazione,"+"6 - Periodo normale di imposta", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSITUAZ_1_54.RadioValue()
    return(iif(this.value =1,' ',;
    iif(this.value =2,'1',;
    iif(this.value =3,'2',;
    iif(this.value =4,'3',;
    iif(this.value =5,'5',;
    iif(this.value =6,'6',;
    space(1))))))))
  endfunc
  func oSITUAZ_1_54.GetRadio()
    this.Parent.oContained.w_SITUAZ = this.RadioValue()
    return .t.
  endfunc

  func oSITUAZ_1_54.SetRadio()
    this.Parent.oContained.w_SITUAZ=trim(this.Parent.oContained.w_SITUAZ)
    this.value = ;
      iif(this.Parent.oContained.w_SITUAZ=='',1,;
      iif(this.Parent.oContained.w_SITUAZ=='1',2,;
      iif(this.Parent.oContained.w_SITUAZ=='2',3,;
      iif(this.Parent.oContained.w_SITUAZ=='3',4,;
      iif(this.Parent.oContained.w_SITUAZ=='5',5,;
      iif(this.Parent.oContained.w_SITUAZ=='6',6,;
      0))))))
  endfunc

  func oSITUAZ_1_54.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza1)
    endwith
   endif
  endfunc

  add object oCODFISDA_1_55 as StdField with uid="XSNKRKYERI",rtseq=55,rtrep=.f.,;
    cFormVar = "w_CODFISDA", cQueryName = "CODFISDA",;
    bObbl = .f. , nPag = 1, value=space(11), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale del dicastero di appartenenza",;
    HelpContextID = 91397479,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=448, Top=511, cSayPict='repl("!",11)', cGetPict='repl("!",11)', InputMask=replicate('X',11)

  func oCODFISDA_1_55.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza1)
    endwith
   endif
  endfunc

  func oCODFISDA_1_55.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (iif(not empty(.w_CODFISDA),chkcfp(alltrim(.w_CODFISDA),'PI'),.T.))
    endwith
    return bRes
  endfunc

  add object oStr_1_65 as StdString with uid="PZCKHMGKWT",Visible=.t., Left=40, Top=11,;
    Alignment=1, Width=58, Height=15,;
    Caption="Cognome:"  ;
  , bGlobalFont=.t.

  add object oStr_1_66 as StdString with uid="MIJLOSOMKI",Visible=.t., Left=285, Top=10,;
    Alignment=1, Width=37, Height=15,;
    Caption="Nome:"  ;
  , bGlobalFont=.t.

  add object oStr_1_68 as StdString with uid="TZKQRFHOUY",Visible=.t., Left=9, Top=97,;
    Alignment=0, Width=204, Height=15,;
    Caption="Tipo di dichiarazione"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_69 as StdString with uid="UCIEAMMITE",Visible=.t., Left=8, Top=35,;
    Alignment=1, Width=90, Height=15,;
    Caption="Denominazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_70 as StdString with uid="HLIVBEVOMR",Visible=.t., Left=8, Top=60,;
    Alignment=1, Width=90, Height=15,;
    Caption="Codice fiscale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_75 as StdString with uid="EFRWRPPCTM",Visible=.t., Left=11, Top=199,;
    Alignment=0, Width=204, Height=15,;
    Caption="Persona fisica"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_76 as StdString with uid="HRDBHHFBYE",Visible=.t., Left=121, Top=258,;
    Alignment=0, Width=223, Height=13,;
    Caption="Comune di residenza o domicilio fiscale"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_77 as StdString with uid="ZDKIULOHFL",Visible=.t., Left=11, Top=221,;
    Alignment=0, Width=103, Height=13,;
    Caption="Data di nascita"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_78 as StdString with uid="CTCAOZXFVH",Visible=.t., Left=121, Top=221,;
    Alignment=0, Width=223, Height=13,;
    Caption="Comune di nascita"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_79 as StdString with uid="VETKYUGDVO",Visible=.t., Left=448, Top=221,;
    Alignment=0, Width=48, Height=13,;
    Caption="Provincia"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_80 as StdString with uid="FWEPYXGQZJ",Visible=.t., Left=11, Top=258,;
    Alignment=0, Width=87, Height=17,;
    Caption="Data variazione"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_81 as StdString with uid="HVQAZPYBNO",Visible=.t., Left=448, Top=258,;
    Alignment=0, Width=48, Height=13,;
    Caption="Provincia"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_82 as StdString with uid="ATTHQLRPRN",Visible=.t., Left=121, Top=293,;
    Alignment=0, Width=216, Height=13,;
    Caption="Indirizzo, frazione, via e numero civico"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_83 as StdString with uid="KSQHILFMHS",Visible=.t., Left=523, Top=221,;
    Alignment=0, Width=48, Height=13,;
    Caption="Sesso"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_84 as StdString with uid="IVLKBJFSJW",Visible=.t., Left=448, Top=293,;
    Alignment=0, Width=56, Height=13,;
    Caption="CAP"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_85 as StdString with uid="YZOMBVBHCP",Visible=.t., Left=523, Top=293,;
    Alignment=0, Width=72, Height=17,;
    Caption="Cod.attivit�"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_86 as StdString with uid="AIZMGLVLMZ",Visible=.t., Left=602, Top=293,;
    Alignment=0, Width=48, Height=13,;
    Caption="Telefono"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_90 as StdString with uid="DEKDZLFCML",Visible=.t., Left=11, Top=345,;
    Alignment=0, Width=228, Height=15,;
    Caption="Altri soggetti"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_91 as StdString with uid="EBKJKEHJKK",Visible=.t., Left=11, Top=361,;
    Alignment=0, Width=76, Height=13,;
    Caption="Sede legale"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_92 as StdString with uid="RZVEDQJFMM",Visible=.t., Left=121, Top=361,;
    Alignment=0, Width=76, Height=13,;
    Caption="Comune"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_93 as StdString with uid="UNXIIROKSZ",Visible=.t., Left=624, Top=463,;
    Alignment=0, Width=69, Height=13,;
    Caption="Nat. giuridica"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_94 as StdString with uid="UQKLITXGSW",Visible=.t., Left=448, Top=361,;
    Alignment=0, Width=48, Height=13,;
    Caption="Provincia"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_95 as StdString with uid="EXNBDAOCWO",Visible=.t., Left=121, Top=395,;
    Alignment=0, Width=216, Height=13,;
    Caption="Indirizzo, frazione, via e numero civico"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_96 as StdString with uid="WLXGABIKNW",Visible=.t., Left=448, Top=395,;
    Alignment=0, Width=56, Height=13,;
    Caption="CAP"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_97 as StdString with uid="RMDNMKOJQJ",Visible=.t., Left=11, Top=429,;
    Alignment=0, Width=76, Height=13,;
    Caption="Dom. fiscale"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_98 as StdString with uid="ZVHEBJFODD",Visible=.t., Left=121, Top=429,;
    Alignment=0, Width=76, Height=13,;
    Caption="Comune"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_99 as StdString with uid="VJNNNJGRSB",Visible=.t., Left=448, Top=429,;
    Alignment=0, Width=48, Height=13,;
    Caption="Provincia"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_100 as StdString with uid="XMGFDNXRGY",Visible=.t., Left=121, Top=463,;
    Alignment=0, Width=216, Height=13,;
    Caption="Indirizzo, frazione, via e numero civico"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_101 as StdString with uid="CNBFEAFDFD",Visible=.t., Left=624, Top=429,;
    Alignment=0, Width=56, Height=13,;
    Caption="CAP"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_102 as StdString with uid="DYEPGPKKPJ",Visible=.t., Left=448, Top=463,;
    Alignment=0, Width=84, Height=13,;
    Caption="Codice attivit�"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_103 as StdString with uid="ZUHOUEPINF",Visible=.t., Left=534, Top=463,;
    Alignment=0, Width=47, Height=13,;
    Caption="Telefono"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_104 as StdString with uid="TVXFVOYTZB",Visible=.t., Left=121, Top=498,;
    Alignment=0, Width=48, Height=13,;
    Caption="Stato"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_105 as StdString with uid="GPUYEQERVJ",Visible=.t., Left=257, Top=498,;
    Alignment=0, Width=64, Height=13,;
    Caption="Situazione"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_106 as StdString with uid="ARBZPVKLZS",Visible=.t., Left=448, Top=497,;
    Alignment=0, Width=224, Height=13,;
    Caption="Codice fiscale del dicastero di appartenenza"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_107 as StdString with uid="BBSCZKFXTA",Visible=.t., Left=579, Top=130,;
    Alignment=0, Width=89, Height=13,;
    Caption="Tipo operazione"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_109 as StdString with uid="HPMMFXHZSB",Visible=.t., Left=523, Top=258,;
    Alignment=0, Width=91, Height=17,;
    Caption="Cod. comune"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_110 as StdString with uid="DLHBLVEZGX",Visible=.t., Left=534, Top=395,;
    Alignment=0, Width=91, Height=13,;
    Caption="Cod. comune"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_111 as StdString with uid="SNVZONYPPB",Visible=.t., Left=534, Top=429,;
    Alignment=0, Width=91, Height=13,;
    Caption="Cod. comune"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_112 as StdString with uid="ZHPUEMRERJ",Visible=.t., Left=329, Top=60,;
    Alignment=1, Width=215, Height=15,;
    Caption="Codice fiscale sostituto d'imposta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_113 as StdString with uid="JTEPEFQRBC",Visible=.t., Left=40, Top=158,;
    Alignment=0, Width=90, Height=14,;
    Caption="Eventi eccezionali"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_1_67 as StdBox with uid="YPBURJVQFL",left=7, top=114, width=681,height=2

  add object oBox_1_74 as StdBox with uid="TFBVYLMEDX",left=7, top=214, width=681,height=2

  add object oBox_1_89 as StdBox with uid="HJLYLCJQUK",left=7, top=360, width=681,height=2
enddefine
define class tgsriasftPag2 as StdContainer
  Width  = 730
  height = 545
  stdWidth  = 730
  stdheight = 545
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oForza2_2_1 as StdCheck with uid="ANPGYPQFYE",rtseq=70,rtrep=.f.,left=591, top=6, caption="Forza editing",;
    HelpContextID = 165044906,;
    cFormVar="w_Forza2", bObbl = .f. , nPag = 2;
    , TABSTOP=.F.;
   , bGlobalFont=.t.


  func oForza2_2_1.RadioValue()
    return(iif(this.value =1,.t.,;
    .f.))
  endfunc
  func oForza2_2_1.GetRadio()
    this.Parent.oContained.w_Forza2 = this.RadioValue()
    return .t.
  endfunc

  func oForza2_2_1.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_Forza2==.t.,1,;
      0)
  endfunc

  add object oRFCODFIS_2_2 as StdField with uid="MTXMRCWFQH",rtseq=71,rtrep=.f.,;
    cFormVar = "w_RFCODFIS", cQueryName = "RFCODFIS",;
    bObbl = .f. , nPag = 2, value=space(16), bMultilanguage =  .f.,;
    HelpContextID = 131365527,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=14, Top=44, cSayPict='repl("!",16)', cGetPict='repl("!",16)', InputMask=replicate('X',16)

  func oRFCODFIS_2_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PERAZI <> 'S' or (.w_Forza2 and .w_PERAZI='S'))
    endwith
   endif
  endfunc

  func oRFCODFIS_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (iif(not empty(.w_RFCODFIS),chkcfp(alltrim(.w_RFCODFIS),'CF'),.T.))
    endwith
    return bRes
  endfunc


  add object oRFCODCAR_2_3 as StdCombo with uid="YYMESQRZRO",rtseq=72,rtrep=.f.,left=203,top=44,width=286,height=21;
    , ToolTipText = "Codice carica";
    , HelpContextID = 86738280;
    , cFormVar="w_RFCODCAR",RowSource=""+""+l_descri1+","+""+l_descri2+","+""+l_descri3+","+""+l_descri4+","+""+l_descri5+","+""+l_descri6+","+""+l_descri7+","+""+l_descri8+","+""+l_descri9+","+""+l_descri10+","+""+l_descri11+","+""+l_descri12+","+""+l_descri13+","+""+l_descri14+","+""+l_descri15+"", bObbl = .t. , nPag = 2;
  , bGlobalFont=.t.


  func oRFCODCAR_2_3.RadioValue()
    return(iif(this.value =1,'1',;
    iif(this.value =2,'2',;
    iif(this.value =3,'3',;
    iif(this.value =4,'4',;
    iif(this.value =5,'5',;
    iif(this.value =6,'6',;
    iif(this.value =7,'7',;
    iif(this.value =8,'8',;
    iif(this.value =9,'9',;
    iif(this.value =10,'10',;
    iif(this.value =11,'11',;
    iif(this.value =12,'12',;
    iif(this.value =13,'13',;
    iif(this.value =14,'14',;
    iif(this.value =15,'15',;
    '  '))))))))))))))))
  endfunc
  func oRFCODCAR_2_3.GetRadio()
    this.Parent.oContained.w_RFCODCAR = this.RadioValue()
    return .t.
  endfunc

  func oRFCODCAR_2_3.SetRadio()
    this.Parent.oContained.w_RFCODCAR=trim(this.Parent.oContained.w_RFCODCAR)
    this.value = ;
      iif(this.Parent.oContained.w_RFCODCAR=='1',1,;
      iif(this.Parent.oContained.w_RFCODCAR=='2',2,;
      iif(this.Parent.oContained.w_RFCODCAR=='3',3,;
      iif(this.Parent.oContained.w_RFCODCAR=='4',4,;
      iif(this.Parent.oContained.w_RFCODCAR=='5',5,;
      iif(this.Parent.oContained.w_RFCODCAR=='6',6,;
      iif(this.Parent.oContained.w_RFCODCAR=='7',7,;
      iif(this.Parent.oContained.w_RFCODCAR=='8',8,;
      iif(this.Parent.oContained.w_RFCODCAR=='9',9,;
      iif(this.Parent.oContained.w_RFCODCAR=='10',10,;
      iif(this.Parent.oContained.w_RFCODCAR=='11',11,;
      iif(this.Parent.oContained.w_RFCODCAR=='12',12,;
      iif(this.Parent.oContained.w_RFCODCAR=='13',13,;
      iif(this.Parent.oContained.w_RFCODCAR=='14',14,;
      iif(this.Parent.oContained.w_RFCODCAR=='15',15,;
      0)))))))))))))))
  endfunc

  func oRFCODCAR_2_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PERAZI <> 'S' or .w_Forza2)
    endwith
   endif
  endfunc

  add object oRFCOGNOME_2_5 as StdField with uid="TUTTYIGKSZ",rtseq=74,rtrep=.f.,;
    cFormVar = "w_RFCOGNOME", cQueryName = "RFCOGNOME",;
    bObbl = .f. , nPag = 2, value=space(24), bMultilanguage =  .f.,;
    ToolTipText = "Cognome del rappresentante",;
    HelpContextID = 262436429,;
   bGlobalFont=.t.,;
    Height=21, Width=177, Left=14, Top=79, cSayPict='repl("!",24)', cGetPict='repl("!",24)', InputMask=replicate('X',24)

  func oRFCOGNOME_2_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_RFDENOMI) and (.w_Perazi<>'S' or .w_Forza2))
    endwith
   endif
  endfunc

  add object oRFNOME_2_6 as StdField with uid="RKLEAKPRVC",rtseq=75,rtrep=.f.,;
    cFormVar = "w_RFNOME", cQueryName = "RFNOME",;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Nome del rappresentante",;
    HelpContextID = 138660586,;
   bGlobalFont=.t.,;
    Height=21, Width=160, Left=234, Top=79, cSayPict='repl("!",20)', cGetPict='repl("!",20)', InputMask=replicate('X',20)

  func oRFNOME_2_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_RFDENOMI) and (.w_Perazi<>'S' or .w_Forza2))
    endwith
   endif
  endfunc


  add object oRFSESSO_2_7 as StdCombo with uid="WGXEPZGXVZ",value=1,rtseq=76,rtrep=.f.,left=415,top=79,width=114,height=21;
    , height = 21;
    , ToolTipText = "Sesso del rappresentante";
    , HelpContextID = 166558442;
    , cFormVar="w_RFSESSO",RowSource=""+"Non selezionato,"+"Maschio,"+"Femmina", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oRFSESSO_2_7.RadioValue()
    return(iif(this.value =1,' ',;
    iif(this.value =2,'M',;
    iif(this.value =3,'F',;
    space(1)))))
  endfunc
  func oRFSESSO_2_7.GetRadio()
    this.Parent.oContained.w_RFSESSO = this.RadioValue()
    return .t.
  endfunc

  func oRFSESSO_2_7.SetRadio()
    this.Parent.oContained.w_RFSESSO=trim(this.Parent.oContained.w_RFSESSO)
    this.value = ;
      iif(this.Parent.oContained.w_RFSESSO=='',1,;
      iif(this.Parent.oContained.w_RFSESSO=='M',2,;
      iif(this.Parent.oContained.w_RFSESSO=='F',3,;
      0)))
  endfunc

  func oRFSESSO_2_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_RFDENOMI) and (.w_Perazi<>'S' or .w_Forza2))
    endwith
   endif
  endfunc

  add object oRFDATANASC_2_8 as StdField with uid="AUOENXXQZR",rtseq=77,rtrep=.f.,;
    cFormVar = "w_RFDATANASC", cQueryName = "RFDATANASC",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di nascita del rappresentante",;
    HelpContextID = 199369337,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=14, Top=115

  func oRFDATANASC_2_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_RFDENOMI) and (.w_Perazi<>'S' or .w_Forza2))
    endwith
   endif
  endfunc

  add object oRFCOMNAS_2_9 as StdField with uid="RPGGCLVJHX",rtseq=78,rtrep=.f.,;
    cFormVar = "w_RFCOMNAS", cQueryName = "RFCOMNAS",;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune (o stato estero) di nascita del rappresentante",;
    HelpContextID = 12289385,;
   bGlobalFont=.t.,;
    Height=21, Width=260, Left=133, Top=115, cSayPict='repl("!",40)', cGetPict='repl("!",40)', InputMask=replicate('X',40)

  func oRFCOMNAS_2_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_RFDENOMI) and (.w_Perazi<>'S' or .w_Forza2))
    endwith
   endif
  endfunc

  add object oRFSIGNAS_2_10 as StdField with uid="DAVHPWZQVP",rtseq=79,rtrep=.f.,;
    cFormVar = "w_RFSIGNAS", cQueryName = "RFSIGNAS",;
    bObbl = .f. , nPag = 2, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia di nascita del rappresentante",;
    HelpContextID = 5670249,;
   bGlobalFont=.t.,;
    Height=21, Width=32, Left=413, Top=115, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2)

  func oRFSIGNAS_2_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_RFDENOMI) and (.w_Perazi<>'S' or .w_Forza2))
    endwith
   endif
  endfunc

  add object oRFSIGLA_2_11 as StdField with uid="XRZZRJMUPC",rtseq=80,rtrep=.f.,;
    cFormVar = "w_RFSIGLA", cQueryName = "RFSIGLA",;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice stato estero",;
    HelpContextID = 240551190,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=14, Top=154, cSayPict='"999"', cGetPict='"999"', InputMask=replicate('X',3)

  func oRFSIGLA_2_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza2)
    endwith
   endif
  endfunc

  add object oRFCAP_2_12 as StdField with uid="HCWCMZDMZH",rtseq=81,rtrep=.f.,;
    cFormVar = "w_RFCAP", cQueryName = "RFCAP",;
    bObbl = .f. , nPag = 2, value=space(24), bMultilanguage =  .f.,;
    ToolTipText = "Stato federato, provincia, contea",;
    HelpContextID = 220363498,;
   bGlobalFont=.t.,;
    Height=21, Width=181, Left=133, Top=154, cSayPict='repl("!",24)', cGetPict='repl("!",24)', InputMask=replicate('X',24)

  func oRFCAP_2_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((.w_Perazi<>'S' or .w_Forza2 ) AND NOT EMPTY (.w_RFSIGLA))
    endwith
   endif
  endfunc

  add object oRFCOMUNE_2_13 as StdField with uid="DDNELJFMGC",rtseq=82,rtrep=.f.,;
    cFormVar = "w_RFCOMUNE", cQueryName = "RFCOMUNE",;
    bObbl = .f. , nPag = 2, value=space(24), bMultilanguage =  .f.,;
    ToolTipText = "Localit� di residenza anagrafica o di domicilio fiscale del rappresentante",;
    HelpContextID = 138705573,;
   bGlobalFont=.t.,;
    Height=21, Width=174, Left=344, Top=154, cSayPict='repl("!",24)', cGetPict='repl("!",24)', InputMask=replicate('X',24)

  func oRFCOMUNE_2_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((.w_Perazi<>'S' or .w_Forza2)  AND NOT EMPTY (.w_RFSIGLA))
    endwith
   endif
  endfunc

  add object oRFINDIRIZ_2_14 as StdField with uid="HCOWWPHROD",rtseq=83,rtrep=.f.,;
    cFormVar = "w_RFINDIRIZ", cQueryName = "RFINDIRIZ",;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo ( frazione, via e numero civico) di residenza anagrafica o domicilio",;
    HelpContextID = 187362047,;
   bGlobalFont=.t.,;
    Height=21, Width=296, Left=14, Top=197, cSayPict='repl("!",35)', cGetPict='repl("!",35)', InputMask=replicate('X',35)

  func oRFINDIRIZ_2_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((.w_Perazi<>'S' or .w_Forza2)  AND NOT EMPTY (.w_RFSIGLA))
    endwith
   endif
  endfunc

  add object oRFTELEFONO_2_15 as StdField with uid="MMGWOVEOZI",rtseq=84,rtrep=.f.,;
    cFormVar = "w_RFTELEFONO", cQueryName = "RFTELEFONO",;
    bObbl = .f. , nPag = 2, value=space(12), bMultilanguage =  .f.,;
    sErrorMsg = "Non inserire caratteri separatori tra prefisso e numero",;
    HelpContextID = 128117061,;
   bGlobalFont=.t.,;
    Height=21, Width=104, Left=364, Top=197, cSayPict="repl('!',12)", cGetPict="repl('!',12)", InputMask=replicate('X',12)

  func oRFTELEFONO_2_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza2)
    endwith
   endif
  endfunc

  func oRFTELEFONO_2_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (at(' ',alltrim(.w_RFTELEFONO))=0 and at('/',alltrim(.w_RFTELEFONO))=0 and at('-',alltrim(.w_RFTELEFONO))=0)
    endwith
    return bRes
  endfunc

  add object oRFFISSED_2_16 as StdField with uid="ZDHVWJJZYC",rtseq=85,rtrep=.f.,;
    cFormVar = "w_RFFISSED", cQueryName = "RFFISSED",;
    bObbl = .f. , nPag = 2, value=space(11), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale societ� o ente dichiarante",;
    HelpContextID = 102085978,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=495, Top=197, cSayPict='repl("!",11)', cGetPict='repl("!",11)', InputMask=replicate('X',11)

  func oRFFISSED_2_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza2)
    endwith
   endif
  endfunc

  add object oRFDATCAR_2_17 as StdField with uid="VJTTUCHDUN",rtseq=86,rtrep=.f.,;
    cFormVar = "w_RFDATCAR", cQueryName = "RFDATCAR",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data decorrenza carica",;
    HelpContextID = 102602088,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=16, Top=239

  func oRFDATCAR_2_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PERAZI <> 'S' or .w_Forza2)
    endwith
   endif
  endfunc

  add object oRFDATFAL_2_18 as StdField with uid="YBFSONZHOQ",rtseq=87,rtrep=.f.,;
    cFormVar = "w_RFDATFAL", cQueryName = "RFDATFAL",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di apertura fallimento",;
    HelpContextID = 152933730,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=221, Top=239

  func oRFDATFAL_2_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PERAZI <> 'S' or .w_Forza2)
    endwith
   endif
  endfunc

  func oRFDATFAL_2_18.mHide()
    with this.Parent.oContained
      return (! inlist(.w_RFCODCAR,'3','4'))
    endwith
  endfunc

  add object oSEZ1_2_19 as StdCheck with uid="IATICRAGRO",rtseq=88,rtrep=.f.,left=16, top=309, caption="Sezione I - trasmissione integrale modello 770 semplificato",;
    HelpContextID = 36768730,;
    cFormVar="w_SEZ1", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oSEZ1_2_19.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oSEZ1_2_19.GetRadio()
    this.Parent.oContained.w_SEZ1 = this.RadioValue()
    return .t.
  endfunc

  func oSEZ1_2_19.SetRadio()
    this.Parent.oContained.w_SEZ1=trim(this.Parent.oContained.w_SEZ1)
    this.value = ;
      iif(this.Parent.oContained.w_SEZ1=='1',1,;
      0)
  endfunc

  add object oqSS1_2_20 as StdCheck with uid="FDPHJSPFOT",rtseq=89,rtrep=.f.,left=542, top=312, caption="SS",;
    HelpContextID = 36793338,;
    cFormVar="w_qSS1", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oqSS1_2_20.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oqSS1_2_20.GetRadio()
    this.Parent.oContained.w_qSS1 = this.RadioValue()
    return .t.
  endfunc

  func oqSS1_2_20.SetRadio()
    this.Parent.oContained.w_qSS1=trim(this.Parent.oContained.w_qSS1)
    this.value = ;
      iif(this.Parent.oContained.w_qSS1=='1',1,;
      0)
  endfunc

  func oqSS1_2_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SEZ1='1')
    endwith
   endif
  endfunc

  add object oqST1_2_21 as StdCheck with uid="BGCLFWVZAL",rtseq=90,rtrep=.f.,left=591, top=312, caption="ST",;
    HelpContextID = 36789242,;
    cFormVar="w_qST1", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oqST1_2_21.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oqST1_2_21.GetRadio()
    this.Parent.oContained.w_qST1 = this.RadioValue()
    return .t.
  endfunc

  func oqST1_2_21.SetRadio()
    this.Parent.oContained.w_qST1=trim(this.Parent.oContained.w_qST1)
    this.value = ;
      iif(this.Parent.oContained.w_qST1=='1',1,;
      0)
  endfunc

  func oqST1_2_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SEZ1='1')
    endwith
   endif
  endfunc

  add object oqSV1_2_22 as StdCheck with uid="WAOGOXBGJE",rtseq=91,rtrep=.f.,left=640, top=312, caption="SV",;
    HelpContextID = 36781050,;
    cFormVar="w_qSV1", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oqSV1_2_22.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oqSV1_2_22.GetRadio()
    this.Parent.oContained.w_qSV1 = this.RadioValue()
    return .t.
  endfunc

  func oqSV1_2_22.SetRadio()
    this.Parent.oContained.w_qSV1=trim(this.Parent.oContained.w_qSV1)
    this.value = ;
      iif(this.Parent.oContained.w_qSV1=='1',1,;
      0)
  endfunc

  func oqSV1_2_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SEZ1='1')
    endwith
   endif
  endfunc

  add object oqSX1_2_23 as StdCheck with uid="ZXSRWVYXVB",rtseq=92,rtrep=.f.,left=689, top=312, caption="SX",;
    HelpContextID = 36772858,;
    cFormVar="w_qSX1", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oqSX1_2_23.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oqSX1_2_23.GetRadio()
    this.Parent.oContained.w_qSX1 = this.RadioValue()
    return .t.
  endfunc

  func oqSX1_2_23.SetRadio()
    this.Parent.oContained.w_qSX1=trim(this.Parent.oContained.w_qSX1)
    this.value = ;
      iif(this.Parent.oContained.w_qSX1=='1',1,;
      0)
  endfunc

  func oqSX1_2_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SEZ1='1')
    endwith
   endif
  endfunc

  add object oq771_2_24 as StdCheck with uid="KKOIUQJLMI",rtseq=93,rtrep=.f.,left=542, top=333, caption="770 ordinario 2009",;
    HelpContextID = 36915194,;
    cFormVar="w_q771", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oq771_2_24.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oq771_2_24.GetRadio()
    this.Parent.oContained.w_q771 = this.RadioValue()
    return .t.
  endfunc

  func oq771_2_24.SetRadio()
    this.Parent.oContained.w_q771=trim(this.Parent.oContained.w_q771)
    this.value = ;
      iif(this.Parent.oContained.w_q771=='1',1,;
      0)
  endfunc

  func oq771_2_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SEZ1='1')
    endwith
   endif
  endfunc

  add object oSEZ2_2_25 as StdCheck with uid="SEIYBKNVNP",rtseq=94,rtrep=.f.,left=16, top=360, caption="Sezione II - trasmissione modello 770 semplificato in due parti",;
    HelpContextID = 36703194,;
    cFormVar="w_SEZ2", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oSEZ2_2_25.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oSEZ2_2_25.GetRadio()
    this.Parent.oContained.w_SEZ2 = this.RadioValue()
    return .t.
  endfunc

  func oSEZ2_2_25.SetRadio()
    this.Parent.oContained.w_SEZ2=trim(this.Parent.oContained.w_SEZ2)
    this.value = ;
      iif(this.Parent.oContained.w_SEZ2=='1',1,;
      0)
  endfunc

  add object oqSS12_2_26 as StdCheck with uid="ZLYMXAXWCP",rtseq=95,rtrep=.f.,left=542, top=363, caption="SS",;
    HelpContextID = 252799994,;
    cFormVar="w_qSS12", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oqSS12_2_26.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oqSS12_2_26.GetRadio()
    this.Parent.oContained.w_qSS12 = this.RadioValue()
    return .t.
  endfunc

  func oqSS12_2_26.SetRadio()
    this.Parent.oContained.w_qSS12=trim(this.Parent.oContained.w_qSS12)
    this.value = ;
      iif(this.Parent.oContained.w_qSS12=='1',1,;
      0)
  endfunc

  func oqSS12_2_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SEZ2='1')
    endwith
   endif
  endfunc

  add object oSEZ3_2_27 as StdCheck with uid="JCOMOCOMMG",rtseq=96,rtrep=.f.,left=16, top=396, caption="Sezione III - trasmissione modello 770 semplificato per le sole",;
    HelpContextID = 36637658,;
    cFormVar="w_SEZ3", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oSEZ3_2_27.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oSEZ3_2_27.GetRadio()
    this.Parent.oContained.w_SEZ3 = this.RadioValue()
    return .t.
  endfunc

  func oSEZ3_2_27.SetRadio()
    this.Parent.oContained.w_SEZ3=trim(this.Parent.oContained.w_SEZ3)
    this.value = ;
      iif(this.Parent.oContained.w_SEZ3=='1',1,;
      0)
  endfunc

  func oSEZ3_2_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.F.)
    endwith
   endif
  endfunc

  add object oqSS13_2_28 as StdCheck with uid="DUXDQHEYEX",rtseq=97,rtrep=.f.,left=542, top=399, caption="SS",;
    HelpContextID = 251751418,;
    cFormVar="w_qSS13", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oqSS13_2_28.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oqSS13_2_28.GetRadio()
    this.Parent.oContained.w_qSS13 = this.RadioValue()
    return .t.
  endfunc

  func oqSS13_2_28.SetRadio()
    this.Parent.oContained.w_qSS13=trim(this.Parent.oContained.w_qSS13)
    this.value = ;
      iif(this.Parent.oContained.w_qSS13=='1',1,;
      0)
  endfunc

  func oqSS13_2_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.F.)
    endwith
   endif
  endfunc

  add object oqST13_2_29 as StdCheck with uid="DQLFPXPMQP",rtseq=98,rtrep=.f.,left=591, top=399, caption="ST",;
    HelpContextID = 251747322,;
    cFormVar="w_qST13", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oqST13_2_29.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oqST13_2_29.GetRadio()
    this.Parent.oContained.w_qST13 = this.RadioValue()
    return .t.
  endfunc

  func oqST13_2_29.SetRadio()
    this.Parent.oContained.w_qST13=trim(this.Parent.oContained.w_qST13)
    this.value = ;
      iif(this.Parent.oContained.w_qST13=='1',1,;
      0)
  endfunc

  func oqST13_2_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.F.)
    endwith
   endif
  endfunc

  add object oqSV13_2_30 as StdCheck with uid="JJAKEQMPZS",rtseq=99,rtrep=.f.,left=640, top=399, caption="SV",;
    HelpContextID = 251739130,;
    cFormVar="w_qSV13", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oqSV13_2_30.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oqSV13_2_30.GetRadio()
    this.Parent.oContained.w_qSV13 = this.RadioValue()
    return .t.
  endfunc

  func oqSV13_2_30.SetRadio()
    this.Parent.oContained.w_qSV13=trim(this.Parent.oContained.w_qSV13)
    this.value = ;
      iif(this.Parent.oContained.w_qSV13=='1',1,;
      0)
  endfunc

  func oqSV13_2_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.F.)
    endwith
   endif
  endfunc

  add object oqSX13_2_31 as StdCheck with uid="CTCKRHAZLH",rtseq=100,rtrep=.f.,left=689, top=399, caption="SX",;
    HelpContextID = 251730938,;
    cFormVar="w_qSX13", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oqSX13_2_31.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oqSX13_2_31.GetRadio()
    this.Parent.oContained.w_qSX13 = this.RadioValue()
    return .t.
  endfunc

  func oqSX13_2_31.SetRadio()
    this.Parent.oContained.w_qSX13=trim(this.Parent.oContained.w_qSX13)
    this.value = ;
      iif(this.Parent.oContained.w_qSX13=='1',1,;
      0)
  endfunc

  func oqSX13_2_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.F.)
    endwith
   endif
  endfunc

  add object oq773_2_32 as StdCheck with uid="RFGQSXMJHK",rtseq=101,rtrep=.f.,left=542, top=425, caption="770 ordinario 2009",;
    HelpContextID = 36784122,;
    cFormVar="w_q773", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oq773_2_32.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oq773_2_32.GetRadio()
    this.Parent.oContained.w_q773 = this.RadioValue()
    return .t.
  endfunc

  func oq773_2_32.SetRadio()
    this.Parent.oContained.w_q773=trim(this.Parent.oContained.w_q773)
    this.value = ;
      iif(this.Parent.oContained.w_q773=='1',1,;
      0)
  endfunc

  func oq773_2_32.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.f.)
    endwith
   endif
  endfunc

  add object oSEZ4_2_33 as StdCheck with uid="GYUOHRLOVF",rtseq=102,rtrep=.f.,left=16, top=454, caption="Sezione IV - trasmissione modello 770 semplificato per le sole",;
    HelpContextID = 36572122,;
    cFormVar="w_SEZ4", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oSEZ4_2_33.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oSEZ4_2_33.GetRadio()
    this.Parent.oContained.w_SEZ4 = this.RadioValue()
    return .t.
  endfunc

  func oSEZ4_2_33.SetRadio()
    this.Parent.oContained.w_SEZ4=trim(this.Parent.oContained.w_SEZ4)
    this.value = ;
      iif(this.Parent.oContained.w_SEZ4=='1',1,;
      0)
  endfunc

  add object oqSS14_2_34 as StdCheck with uid="NVZNOPVLBE",rtseq=103,rtrep=.f.,left=542, top=457, caption="SS",;
    HelpContextID = 250702842,;
    cFormVar="w_qSS14", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oqSS14_2_34.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oqSS14_2_34.GetRadio()
    this.Parent.oContained.w_qSS14 = this.RadioValue()
    return .t.
  endfunc

  func oqSS14_2_34.SetRadio()
    this.Parent.oContained.w_qSS14=trim(this.Parent.oContained.w_qSS14)
    this.value = ;
      iif(this.Parent.oContained.w_qSS14=='1',1,;
      0)
  endfunc

  func oqSS14_2_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SEZ4='1')
    endwith
   endif
  endfunc

  add object oqST14_2_35 as StdCheck with uid="PJKXDUVTCT",rtseq=104,rtrep=.f.,left=591, top=457, caption="ST",;
    HelpContextID = 250698746,;
    cFormVar="w_qST14", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oqST14_2_35.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oqST14_2_35.GetRadio()
    this.Parent.oContained.w_qST14 = this.RadioValue()
    return .t.
  endfunc

  func oqST14_2_35.SetRadio()
    this.Parent.oContained.w_qST14=trim(this.Parent.oContained.w_qST14)
    this.value = ;
      iif(this.Parent.oContained.w_qST14=='1',1,;
      0)
  endfunc

  func oqST14_2_35.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SEZ4='1')
    endwith
   endif
  endfunc

  add object oqSX14_2_36 as StdCheck with uid="INCRNISXXR",rtseq=105,rtrep=.f.,left=689, top=457, caption="SX",;
    HelpContextID = 250682362,;
    cFormVar="w_qSX14", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oqSX14_2_36.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oqSX14_2_36.GetRadio()
    this.Parent.oContained.w_qSX14 = this.RadioValue()
    return .t.
  endfunc

  func oqSX14_2_36.SetRadio()
    this.Parent.oContained.w_qSX14=trim(this.Parent.oContained.w_qSX14)
    this.value = ;
      iif(this.Parent.oContained.w_qSX14=='1',1,;
      0)
  endfunc

  func oqSX14_2_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SEZ4='1')
    endwith
   endif
  endfunc

  add object oq774_2_37 as StdCheck with uid="UWZUSBDPCN",rtseq=106,rtrep=.f.,left=542, top=482, caption="770 ordinario 2009",;
    HelpContextID = 36718586,;
    cFormVar="w_q774", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oq774_2_37.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oq774_2_37.GetRadio()
    this.Parent.oContained.w_q774 = this.RadioValue()
    return .t.
  endfunc

  func oq774_2_37.SetRadio()
    this.Parent.oContained.w_q774=trim(this.Parent.oContained.w_q774)
    this.value = ;
      iif(this.Parent.oContained.w_q774=='1',1,;
      0)
  endfunc

  func oq774_2_37.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SEZ4='1')
    endwith
   endif
  endfunc

  add object oRFCFINS2_2_38 as StdField with uid="ZFMCVGTPTY",rtseq=107,rtrep=.f.,;
    cFormVar = "w_RFCFINS2", cQueryName = "RFCFINS2",;
    bObbl = .f. , nPag = 2, value=space(13), bMultilanguage =  .f.,;
    sErrorMsg = "Inserire il codice fiscale dell'intermediario della sezione IV",;
    ToolTipText = "Codice fiscale dell'intermediario della sezione IV",;
    HelpContextID = 7505224,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=452, Top=511, cSayPict="repl('!',16)", cGetPict="repl('!',16)", InputMask=replicate('X',13)

  func oRFCFINS2_2_38.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SEZ4='1')
    endwith
   endif
  endfunc

  func oRFCFINS2_2_38.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (iif(not empty(.w_RFCFINS2),chkcfp(alltrim(.w_RFCFINS2),'CF'),iif(.w_SEZ4='1',.F.,.T.) ))
    endwith
    return bRes
  endfunc

  add object oStr_2_39 as StdString with uid="XHNHYLLXLG",Visible=.t., Left=386, Top=314,;
    Alignment=1, Width=152, Height=18,;
    Caption="Quadri della dichiarazione:"  ;
  , bGlobalFont=.t.

  add object oStr_2_41 as StdString with uid="FTEQCYTUWC",Visible=.t., Left=11, Top=291,;
    Alignment=0, Width=204, Height=18,;
    Caption="Redazione della dichiarazione"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_43 as StdString with uid="FHMRICXWEG",Visible=.t., Left=11, Top=14,;
    Alignment=0, Width=335, Height=15,;
    Caption="Dati relativi al rappresentante firmatario della dichiarazione"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_44 as StdString with uid="RULZHJZXRW",Visible=.t., Left=14, Top=31,;
    Alignment=0, Width=112, Height=13,;
    Caption="Codice fiscale"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_45 as StdString with uid="USKIUPDQMV",Visible=.t., Left=203, Top=31,;
    Alignment=0, Width=84, Height=13,;
    Caption="Codice carica"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_46 as StdString with uid="GPZTYMBFPO",Visible=.t., Left=133, Top=102,;
    Alignment=0, Width=104, Height=13,;
    Caption="Comune di nascita"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_47 as StdString with uid="RCZFFIGQZJ",Visible=.t., Left=14, Top=67,;
    Alignment=0, Width=76, Height=13,;
    Caption="Cognome"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_48 as StdString with uid="BXGOIXINBX",Visible=.t., Left=234, Top=67,;
    Alignment=0, Width=76, Height=13,;
    Caption="Nome"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_49 as StdString with uid="XMDUKEDFTN",Visible=.t., Left=415, Top=66,;
    Alignment=0, Width=76, Height=13,;
    Caption="Sesso"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_50 as StdString with uid="VNNLUPWWEO",Visible=.t., Left=14, Top=102,;
    Alignment=0, Width=103, Height=13,;
    Caption="Data di nascita"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_51 as StdString with uid="LCBMVTEUCO",Visible=.t., Left=413, Top=102,;
    Alignment=0, Width=48, Height=13,;
    Caption="Provincia"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_52 as StdString with uid="ECENBGSUXY",Visible=.t., Left=344, Top=138,;
    Alignment=0, Width=120, Height=17,;
    Caption="Localit� di residenza"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_53 as StdString with uid="XDOCDGJPIJ",Visible=.t., Left=14, Top=180,;
    Alignment=0, Width=77, Height=17,;
    Caption="Indirizzo estero"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_54 as StdString with uid="XJBBNGRGLQ",Visible=.t., Left=364, Top=180,;
    Alignment=0, Width=98, Height=17,;
    Caption="Telefono o cellulare"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_55 as StdString with uid="TXZKOFBIQL",Visible=.t., Left=386, Top=365,;
    Alignment=1, Width=153, Height=18,;
    Caption="Quadri della dichiarazione:"  ;
  , bGlobalFont=.t.

  add object oStr_2_57 as StdString with uid="OXYILKHLHG",Visible=.t., Left=16, Top=513,;
    Alignment=1, Width=432, Height=18,;
    Caption="Codice fiscale del soggetto che presenta la restante parte della dichiarazione:"  ;
  , bGlobalFont=.t.

  add object oStr_2_59 as StdString with uid="FEVBZISFXP",Visible=.t., Left=386, Top=401,;
    Alignment=1, Width=155, Height=18,;
    Caption="Quadri della dichiarazione:"  ;
  , bGlobalFont=.t.

  add object oStr_2_60 as StdString with uid="TUOIRRRLKC",Visible=.t., Left=100, Top=415,;
    Alignment=0, Width=212, Height=18,;
    Caption="comunicazioni dati lavoro dipendente"  ;
  , bGlobalFont=.t.

  add object oStr_2_61 as StdString with uid="MUSADUKPGF",Visible=.t., Left=390, Top=459,;
    Alignment=1, Width=151, Height=18,;
    Caption="Quadri della dichiarazione:"  ;
  , bGlobalFont=.t.

  add object oStr_2_63 as StdString with uid="DWKAXCLKMG",Visible=.t., Left=101, Top=473,;
    Alignment=0, Width=281, Height=18,;
    Caption="comunicazioni dati certificazioni lavoro autonomo"  ;
  , bGlobalFont=.t.

  add object oStr_2_64 as StdString with uid="PJKPYIOJZC",Visible=.t., Left=0, Top=583,;
    Alignment=0, Width=896, Height=18,;
    Caption="Se si modifica la sequenza degli elementi del form o si varia il numero (inserimento o cancellazione item), si deve modificare l'area manuale: notify event init."  ;
  , bGlobalFont=.t.

  add object oStr_2_65 as StdString with uid="GURHHHOVKL",Visible=.t., Left=16, Top=222,;
    Alignment=0, Width=116, Height=17,;
    Caption="Data decorrenza carica"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_66 as StdString with uid="PMSDRIMIWM",Visible=.t., Left=221, Top=222,;
    Alignment=0, Width=118, Height=17,;
    Caption="Data apertura fallimento"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_2_66.mHide()
    with this.Parent.oContained
      return (! inlist(.w_RFCODCAR,'3','4'))
    endwith
  endfunc

  add object oStr_2_67 as StdString with uid="OPYNETEGSR",Visible=.t., Left=14, Top=138,;
    Alignment=0, Width=96, Height=17,;
    Caption="Codice stato estero"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_68 as StdString with uid="FTNBMIHIZO",Visible=.t., Left=133, Top=138,;
    Alignment=0, Width=158, Height=17,;
    Caption="Stato federato, provincia, contea"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_69 as StdString with uid="GUUUGZUKJQ",Visible=.t., Left=495, Top=180,;
    Alignment=0, Width=224, Height=17,;
    Caption="Codice fiscale societ� o ente dichiarante"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_2_40 as StdBox with uid="FZZKXTTQTF",left=11, top=308, width=683,height=2

  add object oBox_2_42 as StdBox with uid="LEBGSAUSDO",left=7, top=29, width=683,height=2

  add object oBox_2_56 as StdBox with uid="LDVQJNYPWN",left=11, top=360, width=683,height=2

  add object oBox_2_58 as StdBox with uid="YNIXMVKZXY",left=11, top=395, width=683,height=2

  add object oBox_2_62 as StdBox with uid="JYXRYDOTBH",left=11, top=453, width=683,height=2
enddefine
define class tgsriasftPag3 as StdContainer
  Width  = 730
  height = 545
  stdWidth  = 730
  stdheight = 545
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oFIRMDICH_3_1 as StdCheck with uid="ZFJVFMHAUQ",rtseq=142,rtrep=.f.,left=14, top=31, caption="Firma dichiarante",;
    ToolTipText = "Firma del dichiarante",;
    HelpContextID = 187332510,;
    cFormVar="w_FIRMDICH", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oFIRMDICH_3_1.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oFIRMDICH_3_1.GetRadio()
    this.Parent.oContained.w_FIRMDICH = this.RadioValue()
    return .t.
  endfunc

  func oFIRMDICH_3_1.SetRadio()
    this.Parent.oContained.w_FIRMDICH=trim(this.Parent.oContained.w_FIRMDICH)
    this.value = ;
      iif(this.Parent.oContained.w_FIRMDICH=='1',1,;
      0)
  endfunc

  add object oFIRMPRES_3_2 as StdCheck with uid="XOLWNXUWLK",rtseq=143,rtrep=.f.,left=14, top=53, caption="Firma dell'incaricato della relazione di revisione",;
    ToolTipText = "Firma dell'incaricato della relazione di revisione",;
    HelpContextID = 82474921,;
    cFormVar="w_FIRMPRES", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oFIRMPRES_3_2.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oFIRMPRES_3_2.GetRadio()
    this.Parent.oContained.w_FIRMPRES = this.RadioValue()
    return .t.
  endfunc

  func oFIRMPRES_3_2.SetRadio()
    this.Parent.oContained.w_FIRMPRES=trim(this.Parent.oContained.w_FIRMPRES)
    this.value = ;
      iif(this.Parent.oContained.w_FIRMPRES=='1',1,;
      0)
  endfunc

  add object oCFINCCON_3_3 as StdField with uid="CABYVRIXJS",rtseq=144,rtrep=.f.,;
    cFormVar = "w_CFINCCON", cQueryName = "CFINCCON",;
    bObbl = .f. , nPag = 3, value=space(16), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale dell'incaricato della relazione di revisione",;
    HelpContextID = 182786956,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=14, Top=89, cSayPict='repl("!",16)', cGetPict='repl("!",16)', InputMask=replicate('X',16)

  func oCFINCCON_3_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (iif(not empty(.w_CFINCCON),chkcfp(alltrim(.w_CFINCCON),'CF'),.T.))
    endwith
    return bRes
  endfunc

  add object oFIRMPRCS_3_4 as StdCheck with uid="YNGOABMKVA",rtseq=145,rtrep=.f.,left=14, top=129, caption="Firma del presidente del collegio sindacale",;
    ToolTipText = "Firma del presidente del collegio sindacale",;
    HelpContextID = 82474921,;
    cFormVar="w_FIRMPRCS", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oFIRMPRCS_3_4.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oFIRMPRCS_3_4.GetRadio()
    this.Parent.oContained.w_FIRMPRCS = this.RadioValue()
    return .t.
  endfunc

  func oFIRMPRCS_3_4.SetRadio()
    this.Parent.oContained.w_FIRMPRCS=trim(this.Parent.oContained.w_FIRMPRCS)
    this.value = ;
      iif(this.Parent.oContained.w_FIRMPRCS=='1',1,;
      0)
  endfunc

  func oFIRMPRCS_3_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_CFPRESCS))
    endwith
   endif
  endfunc

  add object oINVAVTEL_3_5 as StdCheck with uid="BRAWFSFUUQ",rtseq=146,rtrep=.f.,left=350, top=33, caption="Invio avviso telematico all'intermediario",;
    ToolTipText = "Se attivo, il contribuente invia richiesta all'intermediario di verificare i controlli effettuati sulla dichiarazione",;
    HelpContextID = 121552082,;
    cFormVar="w_INVAVTEL", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oINVAVTEL_3_5.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oINVAVTEL_3_5.GetRadio()
    this.Parent.oContained.w_INVAVTEL = this.RadioValue()
    return .t.
  endfunc

  func oINVAVTEL_3_5.SetRadio()
    this.Parent.oContained.w_INVAVTEL=trim(this.Parent.oContained.w_INVAVTEL)
    this.value = ;
      iif(this.Parent.oContained.w_INVAVTEL=='1',1,;
      0)
  endfunc

  func oINVAVTEL_3_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_IMPTRTEL <> '0')
    endwith
   endif
  endfunc


  add object oCODSOGG_3_6 as StdCombo with uid="OGOPZFOIFE",rtseq=147,rtrep=.f.,left=350,top=89,width=330,height=21;
    , HelpContextID = 165649702;
    , cFormVar="w_CODSOGG",RowSource=""+"1 - Controllo effettuato da un revisore contabile,"+"2 - Controllo effettuato dal presidente della societ� di rev.,"+"0 - Nessuno", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oCODSOGG_3_6.RadioValue()
    return(iif(this.value =1,'1',;
    iif(this.value =2,'2',;
    iif(this.value =3,'0',;
    space(1)))))
  endfunc
  func oCODSOGG_3_6.GetRadio()
    this.Parent.oContained.w_CODSOGG = this.RadioValue()
    return .t.
  endfunc

  func oCODSOGG_3_6.SetRadio()
    this.Parent.oContained.w_CODSOGG=trim(this.Parent.oContained.w_CODSOGG)
    this.value = ;
      iif(this.Parent.oContained.w_CODSOGG=='1',1,;
      iif(this.Parent.oContained.w_CODSOGG=='2',2,;
      iif(this.Parent.oContained.w_CODSOGG=='0',3,;
      0)))
  endfunc

  func oCODSOGG_3_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (! empty(.w_CFINCCON))
    endwith
   endif
  endfunc

  add object oCFPRESCS_3_7 as StdField with uid="GUCWEXEYBU",rtseq=148,rtrep=.f.,;
    cFormVar = "w_CFPRESCS", cQueryName = "CFPRESCS",;
    bObbl = .f. , nPag = 3, value=space(16), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale del presidente del collegio sindacale",;
    HelpContextID = 88036473,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=350, Top=127, cSayPict='repl("!",16)', cGetPict='repl("!",16)', InputMask=replicate('X',16)

  func oCFPRESCS_3_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (iif(not empty(.w_CFPRESCS),chkcfp(alltrim(.w_CFPRESCS),'CF'),.T.))
    endwith
    return bRes
  endfunc

  add object oNUMCAF_3_8 as StdField with uid="KVCYFTJAQL",rtseq=149,rtrep=.f.,;
    cFormVar = "w_NUMCAF", cQueryName = "NUMCAF",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero iscrizione all'albo del C.A.F.",;
    HelpContextID = 135253034,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=14, Top=187, cSayPict='"99999"', cGetPict='"99999"'

  add object oRFDATIMP_3_9 as StdField with uid="UVYTNIYLIS",rtseq=150,rtrep=.f.,;
    cFormVar = "w_RFDATIMP", cQueryName = "RFDATIMP",;
    bObbl = .f. , nPag = 3, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data impegno trasmissione fornitura",;
    HelpContextID = 65170074,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=14, Top=229

  add object oCODFISIN_3_10 as StdField with uid="OIWLMRKWII",rtseq=151,rtrep=.f.,;
    cFormVar = "w_CODFISIN", cQueryName = "CODFISIN",;
    bObbl = .f. , nPag = 3, value=space(16), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale dell'intermediario che effettua la trasmissione",;
    HelpContextID = 177037964,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=14, Top=270, cSayPict="repl('!',16)", cGetPict="repl('!',16)", InputMask=replicate('X',16)

  func oCODFISIN_3_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (iif(not empty(.w_CODFISIN),chkcfp(alltrim(.w_CODFISIN),'CF'),.T.))
    endwith
    return bRes
  endfunc


  add object oIMPTRTEL_3_11 as StdCombo with uid="UWMCFDUCPC",rtseq=152,rtrep=.f.,left=350,top=184,width=327,height=21;
    , ToolTipText = "Impegno trasmissione telematica della dichiarazione";
    , HelpContextID = 118578130;
    , cFormVar="w_IMPTRTEL",RowSource=""+"1 - Dichiarazione predisposta dal contribuente,"+"2 - Dichiarazione predisposta da chi effettua l'invio,"+"0 - Nessuno", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oIMPTRTEL_3_11.RadioValue()
    return(iif(this.value =1,'1',;
    iif(this.value =2,'2',;
    iif(this.value =3,'0',;
    space(1)))))
  endfunc
  func oIMPTRTEL_3_11.GetRadio()
    this.Parent.oContained.w_IMPTRTEL = this.RadioValue()
    return .t.
  endfunc

  func oIMPTRTEL_3_11.SetRadio()
    this.Parent.oContained.w_IMPTRTEL=trim(this.Parent.oContained.w_IMPTRTEL)
    this.value = ;
      iif(this.Parent.oContained.w_IMPTRTEL=='1',1,;
      iif(this.Parent.oContained.w_IMPTRTEL=='2',2,;
      iif(this.Parent.oContained.w_IMPTRTEL=='0',3,;
      0)))
  endfunc

  add object oFIRMINT_3_12 as StdCheck with uid="PJOQBGGUFU",rtseq=153,rtrep=.f.,left=350, top=229, caption="Firma intermediario",;
    ToolTipText = "Firma intermediario",;
    HelpContextID = 8025942,;
    cFormVar="w_FIRMINT", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oFIRMINT_3_12.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oFIRMINT_3_12.GetRadio()
    this.Parent.oContained.w_FIRMINT = this.RadioValue()
    return .t.
  endfunc

  func oFIRMINT_3_12.SetRadio()
    this.Parent.oContained.w_FIRMINT=trim(this.Parent.oContained.w_FIRMINT)
    this.value = ;
      iif(this.Parent.oContained.w_FIRMINT=='1',1,;
      0)
  endfunc

  add object oRICAVTEL_3_13 as StdCheck with uid="GQSRGLMKLR",rtseq=154,rtrep=.f.,left=350, top=253, caption="Ricezione avviso telematico",;
    ToolTipText = "Se attivo, l'intermediario accetta di effettuare verifiche per conto del contribuente sulla sua dichiarazione",;
    HelpContextID = 121473122,;
    cFormVar="w_RICAVTEL", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oRICAVTEL_3_13.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oRICAVTEL_3_13.GetRadio()
    this.Parent.oContained.w_RICAVTEL = this.RadioValue()
    return .t.
  endfunc

  func oRICAVTEL_3_13.SetRadio()
    this.Parent.oContained.w_RICAVTEL=trim(this.Parent.oContained.w_RICAVTEL)
    this.value = ;
      iif(this.Parent.oContained.w_RICAVTEL=='1',1,;
      0)
  endfunc

  func oRICAVTEL_3_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_INVAVTEL='1')
    endwith
   endif
  endfunc

  add object oCFDOMNOT_3_27 as StdField with uid="TVFLDVYQST",rtseq=155,rtrep=.f.,;
    cFormVar = "w_CFDOMNOT", cQueryName = "CFDOMNOT",;
    bObbl = .f. , nPag = 3, value=space(16), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale domicilio notificazione",;
    HelpContextID = 256142214,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=14, Top=334, cSayPict="repl('!',16)", cGetPict="repl('!',16)", InputMask=replicate('X',16)

  func oCFDOMNOT_3_27.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (iif(not empty(.w_CFDOMNOT),chkcfp(alltrim(.w_CFDOMNOT),'CF'),.T.))
    endwith
    return bRes
  endfunc

  add object oUFDOMNOT_3_29 as StdField with uid="UCCYYODLGV",rtseq=156,rtrep=.f.,;
    cFormVar = "w_UFDOMNOT", cQueryName = "UFDOMNOT",;
    bObbl = .f. , nPag = 3, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Ufficio o ragione sociale",;
    HelpContextID = 256141926,;
   bGlobalFont=.t.,;
    Height=21, Width=332, Left=244, Top=334, cSayPict='repl("!",60)', cGetPict='repl("!",60)', InputMask=replicate('X',60)

  add object oCODOMNOT_3_30 as StdField with uid="SMLCYFWQFT",rtseq=157,rtrep=.f.,;
    cFormVar = "w_CODOMNOT", cQueryName = "CODOMNOT",;
    bObbl = .f. , nPag = 3, value=space(24), bMultilanguage =  .f.,;
    ToolTipText = "Cognome ",;
    HelpContextID = 256139910,;
   bGlobalFont=.t.,;
    Height=21, Width=181, Left=16, Top=373, cSayPict='repl("!",24)', cGetPict='repl("!",24)', InputMask=replicate('X',24)

  add object oNODOMNOT_3_33 as StdField with uid="AQAEGIQHVF",rtseq=158,rtrep=.f.,;
    cFormVar = "w_NODOMNOT", cQueryName = "NODOMNOT",;
    bObbl = .f. , nPag = 3, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Nome",;
    HelpContextID = 256139734,;
   bGlobalFont=.t.,;
    Height=21, Width=181, Left=244, Top=373, cSayPict='repl("!",20)', cGetPict='repl("!",20)', InputMask=replicate('X',20)

  add object oCMDOMNOT_3_35 as StdField with uid="WENACEUWEI",rtseq=159,rtrep=.f.,;
    cFormVar = "w_CMDOMNOT", cQueryName = "CMDOMNOT",;
    bObbl = .f. , nPag = 3, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune",;
    HelpContextID = 256140422,;
   bGlobalFont=.t.,;
    Height=21, Width=307, Left=16, Top=411, cSayPict='repl("!",40)', cGetPict='repl("!",40)', InputMask=replicate('X',40)

  func oCMDOMNOT_3_35.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_SEDOMNOT))
    endwith
   endif
  endfunc

  add object oPRDOMNOT_3_37 as StdField with uid="ZUQBZAKIKH",rtseq=160,rtrep=.f.,;
    cFormVar = "w_PRDOMNOT", cQueryName = "PRDOMNOT",;
    bObbl = .f. , nPag = 3, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Provincia",;
    HelpContextID = 256138934,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=424, Top=411, cSayPict='repl("!",2)', cGetPict='repl("!",2)', InputMask=replicate('X',2)

  func oPRDOMNOT_3_37.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_SEDOMNOT))
    endwith
   endif
  endfunc

  add object oCCDOMNOT_3_39 as StdField with uid="PRVNNYDANU",rtseq=161,rtrep=.f.,;
    cFormVar = "w_CCDOMNOT", cQueryName = "CCDOMNOT",;
    bObbl = .f. , nPag = 3, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Codice comune",;
    HelpContextID = 256142982,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=514, Top=411, cSayPict='repl("!",4)', cGetPict='repl("!",4)', InputMask=replicate('X',4)

  func oCCDOMNOT_3_39.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_SEDOMNOT))
    endwith
   endif
  endfunc

  add object oCPDOMNOT_3_41 as StdField with uid="UCGYWPBBRM",rtseq=162,rtrep=.f.,;
    cFormVar = "w_CPDOMNOT", cQueryName = "CPDOMNOT",;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "C.A.P.",;
    HelpContextID = 256139654,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=611, Top=411, cSayPict='repl("!",5)', cGetPict='repl("!",5)', InputMask=replicate('X',5)

  func oCPDOMNOT_3_41.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_SEDOMNOT))
    endwith
   endif
  endfunc

  add object oVPDOMNOT_3_43 as StdField with uid="HCUPIJJQOR",rtseq=163,rtrep=.f.,;
    cFormVar = "w_VPDOMNOT", cQueryName = "VPDOMNOT",;
    bObbl = .f. , nPag = 3, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Tipologia (Via, piazza, ecc..)",;
    HelpContextID = 256139350,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=16, Top=449, cSayPict='repl("!",15)', cGetPict='repl("!",15)', InputMask=replicate('X',15)

  func oVPDOMNOT_3_43.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_SEDOMNOT))
    endwith
   endif
  endfunc

  add object oINDOMNOT_3_45 as StdField with uid="OJCNUKIHUB",rtseq=164,rtrep=.f.,;
    cFormVar = "w_INDOMNOT", cQueryName = "INDOMNOT",;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo",;
    HelpContextID = 256140070,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=157, Top=449, cSayPict='repl("!",35)', cGetPict='repl("!",35)', InputMask=replicate('X',35)

  func oINDOMNOT_3_45.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_SEDOMNOT))
    endwith
   endif
  endfunc

  add object oNCDOMNOT_3_47 as StdField with uid="UPQZYAJQIJ",rtseq=165,rtrep=.f.,;
    cFormVar = "w_NCDOMNOT", cQueryName = "NCDOMNOT",;
    bObbl = .f. , nPag = 3, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Numero civico",;
    HelpContextID = 256142806,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=424, Top=449, cSayPict="repl('9',10)", cGetPict="repl('9',10)", InputMask=replicate('X',10)

  func oNCDOMNOT_3_47.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_SEDOMNOT))
    endwith
   endif
  endfunc

  add object oFRDOMNOT_3_49 as StdField with uid="YRHNZULXDI",rtseq=166,rtrep=.f.,;
    cFormVar = "w_FRDOMNOT", cQueryName = "FRDOMNOT",;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Frazione",;
    HelpContextID = 256139094,;
   bGlobalFont=.t.,;
    Height=21, Width=177, Left=514, Top=449, cSayPict='repl("!",35)', cGetPict='repl("!",35)', InputMask=replicate('X',35)

  func oFRDOMNOT_3_49.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_SEDOMNOT))
    endwith
   endif
  endfunc

  add object oSEDOMNOT_3_51 as StdField with uid="AIZBPMFKHR",rtseq=167,rtrep=.f.,;
    cFormVar = "w_SEDOMNOT", cQueryName = "SEDOMNOT",;
    bObbl = .f. , nPag = 3, value=space(24), bMultilanguage =  .f.,;
    ToolTipText = "Stato estero",;
    HelpContextID = 256142214,;
   bGlobalFont=.t.,;
    Height=21, Width=181, Left=16, Top=486, cSayPict='repl("!",24)', cGetPict='repl("!",24)', InputMask=replicate('X',24)

  add object oCEDOMNOT_3_53 as StdField with uid="XEVCXPFOUY",rtseq=168,rtrep=.f.,;
    cFormVar = "w_CEDOMNOT", cQueryName = "CEDOMNOT",;
    bObbl = .f. , nPag = 3, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice stato estero",;
    HelpContextID = 256142470,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=209, Top=486, cSayPict='repl("!",3)', cGetPict='repl("!",3)', InputMask=replicate('X',3)

  func oCEDOMNOT_3_53.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (! empty(.w_SEDOMNOT))
    endwith
   endif
  endfunc

  add object oSFDOMNOT_3_55 as StdField with uid="RXTSJGDQKK",rtseq=169,rtrep=.f.,;
    cFormVar = "w_SFDOMNOT", cQueryName = "SFDOMNOT",;
    bObbl = .f. , nPag = 3, value=space(24), bMultilanguage =  .f.,;
    ToolTipText = "Stato federato, provincia, contea",;
    HelpContextID = 256141958,;
   bGlobalFont=.t.,;
    Height=21, Width=195, Left=307, Top=488, cSayPict='repl("!",24)', cGetPict='repl("!",24)', InputMask=replicate('X',24)

  func oSFDOMNOT_3_55.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (! empty(.w_SEDOMNOT))
    endwith
   endif
  endfunc

  add object oLRDOMNOT_3_57 as StdField with uid="YNYYXTWCWO",rtseq=170,rtrep=.f.,;
    cFormVar = "w_LRDOMNOT", cQueryName = "LRDOMNOT",;
    bObbl = .f. , nPag = 3, value=space(24), bMultilanguage =  .f.,;
    ToolTipText = "Localit� di residenza",;
    HelpContextID = 256138998,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=16, Top=523, cSayPict='repl("!",24)', cGetPict='repl("!",24)', InputMask=replicate('X',24)

  func oLRDOMNOT_3_57.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (! empty(.w_SEDOMNOT))
    endwith
   endif
  endfunc

  add object oIEDOMNOT_3_59 as StdField with uid="NYGJYJFLAI",rtseq=171,rtrep=.f.,;
    cFormVar = "w_IEDOMNOT", cQueryName = "IEDOMNOT",;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo estero",;
    HelpContextID = 256142374,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=307, Top=523, cSayPict='repl("!",35)', cGetPict='repl("!",35)', InputMask=replicate('X',35)

  func oIEDOMNOT_3_59.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (! empty(.w_SEDOMNOT))
    endwith
   endif
  endfunc

  add object oStr_3_14 as StdString with uid="LQRAAIJAJN",Visible=.t., Left=14, Top=212,;
    Alignment=0, Width=164, Height=13,;
    Caption="Data dell'impegno a trasmettere"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_15 as StdString with uid="ELNVKYREGV",Visible=.t., Left=6, Top=151,;
    Alignment=0, Width=233, Height=15,;
    Caption="Impegno alla presentazione telematica"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_17 as StdString with uid="EMANNTQNBZ",Visible=.t., Left=14, Top=171,;
    Alignment=0, Width=203, Height=13,;
    Caption="Numero di iscrizione all'albo del C.A.F."  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_18 as StdString with uid="BFGGPKVGZN",Visible=.t., Left=6, Top=12,;
    Alignment=0, Width=145, Height=18,;
    Caption="Firma della dichiarazione"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_20 as StdString with uid="CSGBUBGTCE",Visible=.t., Left=14, Top=74,;
    Alignment=0, Width=290, Height=17,;
    Caption="Codice fiscale dell'incaricato della relazione di revisione"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_21 as StdString with uid="AJIXNHKAAD",Visible=.t., Left=14, Top=255,;
    Alignment=0, Width=114, Height=17,;
    Caption="Cod. fis. intermediario"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_22 as StdString with uid="NMLWFHTSQA",Visible=.t., Left=350, Top=171,;
    Alignment=0, Width=287, Height=13,;
    Caption="Impegno a trasmettere in via telematica la dichiarazione"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_23 as StdString with uid="JHWTBRLPBO",Visible=.t., Left=350, Top=74,;
    Alignment=0, Width=220, Height=17,;
    Caption="Soggetto "  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_24 as StdString with uid="EMBIIFQUGR",Visible=.t., Left=6, Top=298,;
    Alignment=0, Width=233, Height=15,;
    Caption="Domicilio per la notificazione degli atti"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_26 as StdString with uid="PEYDIWZLYW",Visible=.t., Left=14, Top=318,;
    Alignment=0, Width=114, Height=17,;
    Caption="Codice fiscale"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_28 as StdString with uid="YYOXOZASLU",Visible=.t., Left=14, Top=358,;
    Alignment=0, Width=114, Height=17,;
    Caption="Cognome (o ufficio)"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_31 as StdString with uid="SSWUCRFBLX",Visible=.t., Left=244, Top=318,;
    Alignment=0, Width=119, Height=17,;
    Caption="Ufficio o ragione sociale"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_32 as StdString with uid="IPLAPUMPAR",Visible=.t., Left=244, Top=358,;
    Alignment=0, Width=114, Height=17,;
    Caption="Nome"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_34 as StdString with uid="DSEPOLTRHY",Visible=.t., Left=16, Top=396,;
    Alignment=0, Width=114, Height=17,;
    Caption="Comune"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_36 as StdString with uid="SWUYJBGWBS",Visible=.t., Left=424, Top=396,;
    Alignment=0, Width=73, Height=17,;
    Caption="Provincia"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_38 as StdString with uid="NPFUARVPHW",Visible=.t., Left=514, Top=396,;
    Alignment=0, Width=91, Height=17,;
    Caption="Cod. comune"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_40 as StdString with uid="SDCOZRDJUW",Visible=.t., Left=611, Top=396,;
    Alignment=0, Width=77, Height=17,;
    Caption="Cap"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_42 as StdString with uid="EFHPOFEGBQ",Visible=.t., Left=16, Top=434,;
    Alignment=0, Width=114, Height=17,;
    Caption="Via, piazza, ecc."  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_44 as StdString with uid="PCSKEAJMOD",Visible=.t., Left=157, Top=434,;
    Alignment=0, Width=114, Height=17,;
    Caption="Indirizzo"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_46 as StdString with uid="CUPENILMME",Visible=.t., Left=424, Top=434,;
    Alignment=0, Width=74, Height=17,;
    Caption="N� civico"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_48 as StdString with uid="SMFFNOYWBN",Visible=.t., Left=514, Top=434,;
    Alignment=0, Width=113, Height=17,;
    Caption="Frazione"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_50 as StdString with uid="YPXNYDSWBI",Visible=.t., Left=16, Top=471,;
    Alignment=0, Width=114, Height=17,;
    Caption="Stato estero"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_52 as StdString with uid="VNBGVATZQX",Visible=.t., Left=209, Top=471,;
    Alignment=0, Width=92, Height=17,;
    Caption="Cod. stato estero"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_54 as StdString with uid="PKZDMDHUSW",Visible=.t., Left=307, Top=471,;
    Alignment=0, Width=171, Height=17,;
    Caption="Stato federato, provincia, contea"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_56 as StdString with uid="DAVYANSVOQ",Visible=.t., Left=16, Top=508,;
    Alignment=0, Width=114, Height=17,;
    Caption="Localit� di residenza"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_58 as StdString with uid="TIRLPBRTUX",Visible=.t., Left=307, Top=508,;
    Alignment=0, Width=114, Height=17,;
    Caption="Indirizzo estero"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_60 as StdString with uid="TTNYLJYRPZ",Visible=.t., Left=350, Top=112,;
    Alignment=0, Width=260, Height=17,;
    Caption="Codice fiscale del presidente del collegio sindacale"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_3_16 as StdBox with uid="DTCNDAKETU",left=6, top=166, width=672,height=2

  add object oBox_3_19 as StdBox with uid="KJABZYZADQ",left=6, top=27, width=672,height=2

  add object oBox_3_25 as StdBox with uid="IKSZIQYOTM",left=6, top=313, width=672,height=2
enddefine
define class tgsriasftPag4 as StdContainer
  Width  = 730
  height = 545
  stdWidth  = 730
  stdheight = 545
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oTIPFORN_4_1 as StdCombo with uid="TMIIFPZQPN",rtseq=115,rtrep=.f.,left=103,top=14,width=262,height=21;
    , height = 21;
    , ToolTipText = "Tipo fornitore";
    , HelpContextID = 187475914;
    , cFormVar="w_TIPFORN",RowSource=""+"01: Soggetti che inviano le dichiarazioni,"+"06: Amministrazione dello stato,"+"10: C.A.F. dip. e pens C.A.F. imp art3 C 2 altri int.", bObbl = .f. , nPag = 4;
    , sErrorMsg = "Selezione non consentita con tipo fornitore persona fisica";
  , bGlobalFont=.t.


  func oTIPFORN_4_1.RadioValue()
    return(iif(this.value =1,'01',;
    iif(this.value =2,'06',;
    iif(this.value =3,'10',;
    space(2)))))
  endfunc
  func oTIPFORN_4_1.GetRadio()
    this.Parent.oContained.w_TIPFORN = this.RadioValue()
    return .t.
  endfunc

  func oTIPFORN_4_1.SetRadio()
    this.Parent.oContained.w_TIPFORN=trim(this.Parent.oContained.w_TIPFORN)
    this.value = ;
      iif(this.Parent.oContained.w_TIPFORN=='01',1,;
      iif(this.Parent.oContained.w_TIPFORN=='06',2,;
      iif(this.Parent.oContained.w_TIPFORN=='10',3,;
      0)))
  endfunc

  func oTIPFORN_4_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_Perazi='S' and  .w_TIPFORN<>'06' ) or .w_Perazi<>'S')
    endwith
    return bRes
  endfunc

  add object oFODATANASC_4_2 as StdField with uid="DDIBJMNRLH",rtseq=116,rtrep=.f.,;
    cFormVar = "w_FODATANASC", cQueryName = "FODATANASC",;
    bObbl = .f. , nPag = 4, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di nascita del fornitore",;
    HelpContextID = 199367225,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=54, Top=101

  func oFODATANASC_4_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' or .w_TIPFORN$('0305060910'))
    endwith
   endif
  endfunc


  add object oFOSESSO_4_3 as StdCombo with uid="EZVQYTQPNM",value=1,rtseq=117,rtrep=.f.,left=167,top=101,width=96,height=21;
    , height = 21;
    , ToolTipText = "Sesso del fornitore";
    , HelpContextID = 166556330;
    , cFormVar="w_FOSESSO",RowSource=""+"Non selezionato,"+"Maschio,"+"Femmina", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oFOSESSO_4_3.RadioValue()
    return(iif(this.value =1,' ',;
    iif(this.value =2,'M',;
    iif(this.value =3,'F',;
    space(1)))))
  endfunc
  func oFOSESSO_4_3.GetRadio()
    this.Parent.oContained.w_FOSESSO = this.RadioValue()
    return .t.
  endfunc

  func oFOSESSO_4_3.SetRadio()
    this.Parent.oContained.w_FOSESSO=trim(this.Parent.oContained.w_FOSESSO)
    this.value = ;
      iif(this.Parent.oContained.w_FOSESSO=='',1,;
      iif(this.Parent.oContained.w_FOSESSO=='M',2,;
      iif(this.Parent.oContained.w_FOSESSO=='F',3,;
      0)))
  endfunc

  func oFOSESSO_4_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S'  or .w_TIPFORN$('0305060910'))
    endwith
   endif
  endfunc

  add object oFOCOMUNE_4_4 as StdField with uid="QAAPEJDMEJ",rtseq=118,rtrep=.f.,;
    cFormVar = "w_FOCOMUNE", cQueryName = "FOCOMUNE",;
    bObbl = .f. , nPag = 4, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune (o stato estero) di nascita del fornitore",;
    HelpContextID = 138703461,;
   bGlobalFont=.t.,;
    Height=21, Width=320, Left=276, Top=101, cSayPict="repl('!',40)", cGetPict="repl('!',40)", InputMask=replicate('X',40)

  func oFOCOMUNE_4_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' or .w_TIPFORN$('0305060910'))
    endwith
   endif
  endfunc

  add object oFOSIGLA_4_5 as StdField with uid="PWEJHDBSSF",rtseq=119,rtrep=.f.,;
    cFormVar = "w_FOSIGLA", cQueryName = "FOSIGLA",;
    bObbl = .f. , nPag = 4, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia di nascita del fornitore",;
    HelpContextID = 240553302,;
   bGlobalFont=.t.,;
    Height=21, Width=32, Left=604, Top=101, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2)

  func oFOSIGLA_4_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' or .w_TIPFORN$('0305060910'))
    endwith
   endif
  endfunc

  add object oFORECOMUNE_4_6 as StdField with uid="RGOQWOEVVH",rtseq=120,rtrep=.f.,;
    cFormVar = "w_FORECOMUNE", cQueryName = "FORECOMUNE",;
    bObbl = .f. , nPag = 4, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune di residenza anagrafica o di domicilio fiscale del fornitore",;
    HelpContextID = 250427509,;
   bGlobalFont=.t.,;
    Height=21, Width=360, Left=54, Top=134, cSayPict="repl('!',40)", cGetPict="repl('!',40)", InputMask=replicate('X',40)

  func oFORECOMUNE_4_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' or .w_TIPFORN$('0305060910'))
    endwith
   endif
  endfunc

  add object oFORESIGLA_4_7 as StdField with uid="MJCFSUINVZ",rtseq=121,rtrep=.f.,;
    cFormVar = "w_FORESIGLA", cQueryName = "FORESIGLA",;
    bObbl = .f. , nPag = 4, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia di residenza anagrafica o di domicilio fiscale",;
    HelpContextID = 202539442,;
   bGlobalFont=.t.,;
    Height=21, Width=32, Left=431, Top=134, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2)

  func oFORESIGLA_4_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' or .w_TIPFORN$('0305060910'))
    endwith
   endif
  endfunc

  add object oFOCAP_4_8 as StdField with uid="TTYPCQBXFI",rtseq=122,rtrep=.f.,;
    cFormVar = "w_FOCAP", cQueryName = "FOCAP",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "C.A.P. della residenza anagrafica o del domicilio fiscale del fornitore",;
    HelpContextID = 220361386,;
   bGlobalFont=.t.,;
    Height=21, Width=56, Left=495, Top=134, cSayPict="'99999'", cGetPict="'99999'", InputMask=replicate('X',5)

  func oFOCAP_4_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' or .w_TIPFORN$('0305060910'))
    endwith
   endif
  endfunc

  add object oFOINDIRIZ_4_9 as StdField with uid="ZFPYJFYYCZ",rtseq=123,rtrep=.f.,;
    cFormVar = "w_FOINDIRIZ", cQueryName = "FOINDIRIZ",;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo ( frazione, via e numero civico) di residenza anagrafica o domicilio",;
    HelpContextID = 187364159,;
   bGlobalFont=.t.,;
    Height=21, Width=261, Left=54, Top=167, cSayPict="repl('!',35)", cGetPict="repl('!',35)", InputMask=replicate('X',35)

  func oFOINDIRIZ_4_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' or .w_TIPFORN$('0305060910'))
    endwith
   endif
  endfunc

  add object oFOSECOMUNE_4_11 as StdField with uid="FTDECCKCEA",rtseq=124,rtrep=.f.,;
    cFormVar = "w_FOSECOMUNE", cQueryName = "FOSECOMUNE",;
    bObbl = .f. , nPag = 4, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune della sede legale del fornitore",;
    HelpContextID = 250423413,;
   bGlobalFont=.t.,;
    Height=21, Width=360, Left=54, Top=261, cSayPict="repl('!',40)", cGetPict="repl('!',40)", InputMask=replicate('X',40)

  func oFOSECOMUNE_4_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_TIPFORN$('0305060910'))
    endwith
   endif
  endfunc

  add object oFOSESIGLA_4_12 as StdField with uid="QBWKROZPRH",rtseq=125,rtrep=.f.,;
    cFormVar = "w_FOSESIGLA", cQueryName = "FOSESIGLA",;
    bObbl = .f. , nPag = 4, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia della sede legale del fornitore",;
    HelpContextID = 202543538,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=431, Top=261, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2)

  func oFOSESIGLA_4_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_TIPFORN$('0305060910'))
    endwith
   endif
  endfunc

  add object oFOSECAP_4_13 as StdField with uid="UHFFPYICUM",rtseq=126,rtrep=.f.,;
    cFormVar = "w_FOSECAP", cQueryName = "FOSECAP",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "C.A.P. della sede legale del fornitore",;
    HelpContextID = 216887978,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=495, Top=261, cSayPict="'99999'", cGetPict="'99999'", InputMask=replicate('X',5)

  func oFOSECAP_4_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_TIPFORN$('0305060910'))
    endwith
   endif
  endfunc

  add object oFOSEINDIRI2_4_14 as StdField with uid="UCUBGSOZHZ",rtseq=127,rtrep=.f.,;
    cFormVar = "w_FOSEINDIRI2", cQueryName = "FOSEINDIRI2",;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo della sede legale del fornitore",;
    HelpContextID = 7732159,;
   bGlobalFont=.t.,;
    Height=21, Width=261, Left=54, Top=294, cSayPict="repl('!',35)", cGetPict="repl('!',35)", InputMask=replicate('X',35)

  func oFOSEINDIRI2_4_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_TIPFORN$('0305060910'))
    endwith
   endif
  endfunc

  add object oFOSERCOMUN_4_15 as StdField with uid="VRBXTIUTMI",rtseq=128,rtrep=.f.,;
    cFormVar = "w_FOSERCOMUN", cQueryName = "FOSERCOMUN",;
    bObbl = .f. , nPag = 4, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune di domicilio fiscale del fornitore",;
    HelpContextID = 167583501,;
   bGlobalFont=.t.,;
    Height=21, Width=360, Left=54, Top=327, cSayPict="repl('!',40)", cGetPict="repl('!',40)", InputMask=replicate('X',40)

  func oFOSERCOMUN_4_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_TIPFORN$('0305060910'))
    endwith
   endif
  endfunc

  add object oFOSERSIGLA_4_16 as StdField with uid="RALWYRLTJY",rtseq=129,rtrep=.f.,;
    cFormVar = "w_FOSERSIGLA", cQueryName = "FOSERSIGLA",;
    bObbl = .f. , nPag = 4, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia di domicilio fiscale del fornitore",;
    HelpContextID = 167586979,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=432, Top=327, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2)

  func oFOSERSIGLA_4_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_TIPFORN$('0305060910'))
    endwith
   endif
  endfunc

  add object oFOSERCAP_4_17 as StdField with uid="UZATTOELQF",rtseq=130,rtrep=.f.,;
    cFormVar = "w_FOSERCAP", cQueryName = "FOSERCAP",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "C.A.P. del domicilio fiscale del fornitore",;
    HelpContextID = 100830630,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=495, Top=327, cSayPict="'99999'", cGetPict="'99999'", InputMask=replicate('X',5)

  func oFOSERCAP_4_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_TIPFORN$('0305060910'))
    endwith
   endif
  endfunc

  add object oFOSERINDIR_4_18 as StdField with uid="OCERQWQXAX",rtseq=131,rtrep=.f.,;
    cFormVar = "w_FOSERINDIR", cQueryName = "FOSERINDIR",;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo ( frazione, via e numero civico) di domicilio fiscale del fornitore",;
    HelpContextID = 66919382,;
   bGlobalFont=.t.,;
    Height=21, Width=261, Left=54, Top=360, cSayPict="repl('!',35)", cGetPict="repl('!',35)", InputMask=replicate('X',35)

  func oFOSERINDIR_4_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_TIPFORN$('0305060910'))
    endwith
   endif
  endfunc


  add object oSELCAF_4_19 as StdCombo with uid="LYTTOZMRSR",rtseq=132,rtrep=.f.,left=21,top=440,width=118,height=21;
    , ToolTipText = "Selezione CAF o Professionista";
    , HelpContextID = 135261146;
    , cFormVar="w_SELCAF",RowSource=""+"CAF,"+"Professionista", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oSELCAF_4_19.RadioValue()
    return(iif(this.value =1,'1',;
    iif(this.value =2,'2',;
    space(10))))
  endfunc
  func oSELCAF_4_19.GetRadio()
    this.Parent.oContained.w_SELCAF = this.RadioValue()
    return .t.
  endfunc

  func oSELCAF_4_19.SetRadio()
    this.Parent.oContained.w_SELCAF=trim(this.Parent.oContained.w_SELCAF)
    this.value = ;
      iif(this.Parent.oContained.w_SELCAF=='1',1,;
      iif(this.Parent.oContained.w_SELCAF=='2',2,;
      0))
  endfunc

  add object oCFRESCAF_4_20 as StdField with uid="TRSLJXUKSK",rtseq=133,rtrep=.f.,;
    cFormVar = "w_CFRESCAF", cQueryName = "CFRESCAF",;
    bObbl = .f. , nPag = 4, value=space(16), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale",;
    HelpContextID = 101872748,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=224, Top=440, cSayPict="repl('!',16)", cGetPict="repl('!',16)", InputMask=replicate('X',16)

  func oCFRESCAF_4_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (iif(not empty(.w_CFRESCAF),chkcfp(alltrim(.w_CFRESCAF),'CF'),.T.))
    endwith
    return bRes
  endfunc

  add object oCFDELCAF_4_21 as StdField with uid="RRZPSOFPPW",rtseq=134,rtrep=.f.,;
    cFormVar = "w_CFDELCAF", cQueryName = "CFDELCAF",;
    bObbl = .f. , nPag = 4, value=space(11), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale",;
    HelpContextID = 94475372,;
   bGlobalFont=.t.,;
    Height=21, Width=104, Left=21, Top=485, cSayPict="repl('!',11)", cGetPict="repl('!',11)", InputMask=replicate('X',11)

  func oCFDELCAF_4_21.mHide()
    with this.Parent.oContained
      return (.w_SELCAF <> '1')
    endwith
  endfunc

  func oCFDELCAF_4_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (iif(not empty(.w_CFDELCAF),chkcfp(alltrim(.w_CFDELCAF),'PI'),.T.))
    endwith
    return bRes
  endfunc

  add object oFLAGFIR_4_22 as StdCheck with uid="GMLRFHNQTY",rtseq=135,rtrep=.f.,left=224, top=485, caption="Flag firma del riquadro visto di conformit�",;
    ToolTipText = "Flag firma riquadro 'visto conformit�'",;
    HelpContextID = 188967510,;
    cFormVar="w_FLAGFIR", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oFLAGFIR_4_22.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oFLAGFIR_4_22.GetRadio()
    this.Parent.oContained.w_FLAGFIR = this.RadioValue()
    return .t.
  endfunc

  func oFLAGFIR_4_22.SetRadio()
    this.Parent.oContained.w_FLAGFIR=trim(this.Parent.oContained.w_FLAGFIR)
    this.value = ;
      iif(this.Parent.oContained.w_FLAGFIR=='1',1,;
      0)
  endfunc

  add object oStr_4_24 as StdString with uid="DCPHCCFCXL",Visible=.t., Left=15, Top=68,;
    Alignment=0, Width=204, Height=15,;
    Caption="Persona fisica"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_25 as StdString with uid="YGYYBXXOBR",Visible=.t., Left=15, Top=228,;
    Alignment=0, Width=228, Height=15,;
    Caption="Altri soggetti"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_26 as StdString with uid="CCNGTZXXUE",Visible=.t., Left=54, Top=121,;
    Alignment=0, Width=223, Height=13,;
    Caption="Comune di residenza o domicilio fiscale"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_27 as StdString with uid="AKNIDKFOUL",Visible=.t., Left=54, Top=88,;
    Alignment=0, Width=103, Height=13,;
    Caption="Data di nascita"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_28 as StdString with uid="XXYSRZPODK",Visible=.t., Left=276, Top=88,;
    Alignment=0, Width=223, Height=13,;
    Caption="Comune di nascita"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_29 as StdString with uid="GTEVNWAGWC",Visible=.t., Left=604, Top=88,;
    Alignment=0, Width=48, Height=13,;
    Caption="Provincia"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_30 as StdString with uid="AVMJOZOAMZ",Visible=.t., Left=54, Top=248,;
    Alignment=0, Width=223, Height=13,;
    Caption="Comune della sede legale"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_31 as StdString with uid="ARKFUFVBKV",Visible=.t., Left=431, Top=121,;
    Alignment=0, Width=48, Height=13,;
    Caption="Provincia"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_32 as StdString with uid="OWDEPTUCER",Visible=.t., Left=54, Top=154,;
    Alignment=0, Width=216, Height=13,;
    Caption="Indirizzo, frazione, via e numero civico"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_33 as StdString with uid="ASIOLJGQNU",Visible=.t., Left=167, Top=88,;
    Alignment=0, Width=48, Height=13,;
    Caption="Sesso"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_34 as StdString with uid="RFSIWFUVPA",Visible=.t., Left=495, Top=121,;
    Alignment=0, Width=56, Height=13,;
    Caption="CAP"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_35 as StdString with uid="MDEDFCKUHY",Visible=.t., Left=11, Top=14,;
    Alignment=1, Width=87, Height=15,;
    Caption="Tipo fornitore:"  ;
  , bGlobalFont=.t.

  add object oStr_4_36 as StdString with uid="HNBYHSXXZT",Visible=.t., Left=431, Top=248,;
    Alignment=0, Width=48, Height=13,;
    Caption="Provincia"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_37 as StdString with uid="GQYCDLXSEK",Visible=.t., Left=54, Top=347,;
    Alignment=0, Width=149, Height=13,;
    Caption="Indirizzo di domicilio fiscale"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_38 as StdString with uid="XRGWLJIBWO",Visible=.t., Left=431, Top=314,;
    Alignment=0, Width=48, Height=13,;
    Caption="Provincia"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_39 as StdString with uid="BCAUNHALPM",Visible=.t., Left=495, Top=248,;
    Alignment=0, Width=56, Height=13,;
    Caption="CAP"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_40 as StdString with uid="OXXFNCZRJN",Visible=.t., Left=495, Top=314,;
    Alignment=0, Width=56, Height=13,;
    Caption="CAP"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_41 as StdString with uid="ZFKUQNCSTW",Visible=.t., Left=54, Top=281,;
    Alignment=0, Width=223, Height=13,;
    Caption="Indirizzo della sede legale"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_42 as StdString with uid="TNQFMKZSKA",Visible=.t., Left=54, Top=314,;
    Alignment=0, Width=223, Height=13,;
    Caption="Comune di domicilio fiscale"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_43 as StdString with uid="CSLGHNVXQQ",Visible=.t., Left=15, Top=406,;
    Alignment=0, Width=228, Height=15,;
    Caption="Visto di conformit�"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_45 as StdString with uid="PFUNCMOJXV",Visible=.t., Left=224, Top=426,;
    Alignment=0, Width=300, Height=13,;
    Caption="Codice fiscale del responsabile del C.A.F. o professionista"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_46 as StdString with uid="XSXIBZURHT",Visible=.t., Left=21, Top=471,;
    Alignment=0, Width=192, Height=13,;
    Caption="Codice fiscale del C.A.F."  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_4_46.mHide()
    with this.Parent.oContained
      return (.w_SELCAF <> '1')
    endwith
  endfunc

  add object oStr_4_47 as StdString with uid="NDTJKEXSFR",Visible=.t., Left=21, Top=426,;
    Alignment=0, Width=168, Height=13,;
    Caption="Tipologia"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_48 as StdString with uid="AHIYNFTIDM",Visible=.t., Left=21, Top=521,;
    Alignment=0, Width=395, Height=18,;
    Caption="Si rilascia il visto di conformit� ai sensi art. 35 del D.lgs.  n. 241/1997"  ;
  , bGlobalFont=.t.

  add object oBox_4_10 as StdBox with uid="BTOQXYGKKW",left=11, top=82, width=672,height=2

  add object oBox_4_23 as StdBox with uid="QVBCKAWCXU",left=11, top=244, width=672,height=2

  add object oBox_4_44 as StdBox with uid="FWAQWRIPNX",left=11, top=421, width=672,height=2
enddefine
define class tgsriasftPag5 as StdContainer
  Width  = 730
  height = 545
  stdWidth  = 730
  stdheight = 545
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDATINI_5_1 as StdField with uid="JPXZKEKVIP",rtseq=109,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .t. , nPag = 5, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � maggiore della data finale o fuori periodo",;
    ToolTipText = "Data iniziale di stampa",;
    HelpContextID = 70873290,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=121, Top=39

  func oDATINI_5_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_DATFIN>=.w_DATINI) AND (.w_DATINI > CTOD("31-12-08")))
    endwith
    return bRes
  endfunc

  add object oDATFIN_5_2 as StdField with uid="FNNEPIJJDP",rtseq=110,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .t. , nPag = 5, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � maggiore della data finale o fuori periodo",;
    ToolTipText = "Data finale di stampa",;
    HelpContextID = 7573302,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=266, Top=39

  func oDATFIN_5_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_DATFIN>=.w_DATINI) AND (.w_DATFIN < CTOD("01-01-2010")))
    endwith
    return bRes
  endfunc

  add object oDirName_5_3 as StdField with uid="EJPBDNOIEF",rtseq=111,rtrep=.f.,;
    cFormVar = "w_DirName", cQueryName = "DirName",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(200), bMultilanguage =  .f.,;
    HelpContextID = 16619318,;
   bGlobalFont=.t.,;
    Height=21, Width=446, Left=102, Top=149, InputMask=replicate('X',200)


  add object oBtn_5_4 as StdButton with uid="QIOPRPPDCF",left=550, top=149, width=18,height=21,;
    caption="...", nPag=5;
    , HelpContextID = 40166698;
    , tabstop = .f.;
  , bGlobalFont=.t.

    proc oBtn_5_4.Click()
      with this.Parent.oContained
        do SfogliaDir with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oFileName_5_5 as StdField with uid="OOIIRZMLFQ",rtseq=112,rtrep=.f.,;
    cFormVar = "w_FileName", cQueryName = "FileName",;
    bObbl = .f. , nPag = 5, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 203147333,;
   bGlobalFont=.t.,;
    Height=21, Width=212, Left=102, Top=171, InputMask=replicate('X',30)

  add object oIDESTA_5_6 as StdField with uid="QWRFCHFTQX",rtseq=113,rtrep=.f.,;
    cFormVar = "w_IDESTA", cQueryName = "IDESTA",;
    bObbl = .f. , nPag = 5, value=space(17), bMultilanguage =  .f.,;
    HelpContextID = 198204794,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=316, Top=292, InputMask=replicate('X',17), inputmask='99999999999999999'

  func oIDESTA_5_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLAGCOR = '1' or .w_FLAGINT = '1')
    endwith
   endif
  endfunc

  add object oPROIFT_5_7 as StdField with uid="COXZWAWVRC",rtseq=114,rtrep=.f.,;
    cFormVar = "w_PROIFT", cQueryName = "PROIFT",;
    bObbl = .f. , nPag = 5, value=space(6), bMultilanguage =  .f.,;
    HelpContextID = 105271542,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=316, Top=314, InputMask=replicate('X',6), inputmask='999999'

  func oPROIFT_5_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLAGCOR = '1' or .w_FLAGINT = '1')
    endwith
   endif
  endfunc


  add object oBtn_5_20 as StdButton with uid="UJKRZMFCSW",left=516, top=487, width=48,height=45,;
    CpPicture="bmp\STAMPA.BMP", caption="", nPag=5;
    , ToolTipText = "Premere per eseguire la stampa";
    , HelpContextID = 50578198;
    , caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_5_20.Click()
      with this.Parent.oContained
        do gsri7bsp with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_5_21 as StdButton with uid="QFDCFAAMDM",left=572, top=487, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=5;
    , ToolTipText = "Premere per generare file telematico ";
    , HelpContextID = 50578198;
    , tabstop = .f., caption='\<Genera';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_5_21.Click()
      with this.Parent.oContained
        do ..\BLACKBOX\GSUT8BFT with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_5_21.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (isInoltel())
      endwith
    endif
  endfunc


  add object oBtn_5_22 as StdButton with uid="JRQIJZOZZF",left=628, top=487, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=5;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 50578198;
    , tabstop = .f., caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_5_22.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_5_8 as StdString with uid="LCHEPLEODA",Visible=.t., Left=54, Top=39,;
    Alignment=1, Width=64, Height=15,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_5_9 as StdString with uid="GUHHFLLZER",Visible=.t., Left=209, Top=39,;
    Alignment=1, Width=56, Height=15,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_5_10 as StdString with uid="QDPZXPECZB",Visible=.t., Left=39, Top=149,;
    Alignment=1, Width=58, Height=15,;
    Caption="Directory:"  ;
  , bGlobalFont=.t.

  add object oStr_5_11 as StdString with uid="FKJYZDRMHF",Visible=.t., Left=29, Top=171,;
    Alignment=1, Width=69, Height=15,;
    Caption="Nome file:"  ;
  , bGlobalFont=.t.

  add object oStr_5_12 as StdString with uid="TKFFKWNXKB",Visible=.t., Left=5, Top=17,;
    Alignment=0, Width=163, Height=15,;
    Caption="Parametri di selezione"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_5_15 as StdString with uid="KLZWVWGXPN",Visible=.t., Left=5, Top=116,;
    Alignment=0, Width=204, Height=15,;
    Caption="File telematico"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_5_17 as StdString with uid="XKVUVPRKKQ",Visible=.t., Left=5, Top=260,;
    Alignment=0, Width=304, Height=15,;
    Caption="Protocollo telematico della dichiarazione da sostituire"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_5_18 as StdString with uid="ETMYQQHNMJ",Visible=.t., Left=7, Top=292,;
    Alignment=1, Width=305, Height=15,;
    Caption="Identificativo assegnato dal servizio telematico all'invio:"  ;
  , bGlobalFont=.t.

  add object oStr_5_19 as StdString with uid="ZMKICUMICZ",Visible=.t., Left=12, Top=314,;
    Alignment=1, Width=300, Height=15,;
    Caption="Progressivo dichiarazione all'interno del file inviato:"  ;
  , bGlobalFont=.t.

  add object oBox_5_13 as StdBox with uid="EWLUHYXJCA",left=1, top=32, width=676,height=2

  add object oBox_5_14 as StdBox with uid="ALLGPWEGAO",left=1, top=131, width=676,height=2

  add object oBox_5_16 as StdBox with uid="FUJGWUZXRO",left=1, top=275, width=676,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsriasft','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsriasft
proc SfogliaDir (parent)
local PathName, oField
  PathName = Cp_GetDir()
  if !empty(PathName)
    parent.w_DirName = PathName
  endif
endproc

func IsPerFis(Cognome,Nome,Denominazione)
do case
case empty(Cognome) and empty(Nome) and empty(Denominazione)
  return 'N'
case !empty(Cognome) or !empty(Nome)
  return 'S'
case !empty(Denominazione)
  return 'N'
endcase
* --- Fine Area Manuale
