* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_adh                                                        *
*              Dati estratti record H                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-06-22                                                      *
* Last revis.: 2015-07-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsri_adh"))

* --- Class definition
define class tgsri_adh as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 856
  Height = 559+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-07-30"
  HelpContextID=92927593
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=86

  * --- Constant Properties
  DATESTRH_IDX = 0
  CONTI_IDX = 0
  ENTI_COM_IDX = 0
  NAZIONI_IDX = 0
  GEDETTEL_IDX = 0
  cFile = "DATESTRH"
  cKeySelect = "RHSERIAL"
  cKeyWhere  = "RHSERIAL=this.w_RHSERIAL"
  cKeyWhereODBC = '"RHSERIAL="+cp_ToStrODBC(this.w_RHSERIAL)';

  cKeyWhereODBCqualified = '"DATESTRH.RHSERIAL="+cp_ToStrODBC(this.w_RHSERIAL)';

  cPrg = "gsri_adh"
  cComment = "Dati estratti record H"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_RHSERIAL = space(10)
  w_OBTEST = ctod('  /  /  ')
  w_RH__ANNO = 0
  w_RHTIPCON = space(1)
  w_RHCODCON = space(15)
  o_RHCODCON = space(15)
  w_RHTIPDAT = space(1)
  w_ANCAURIT = space(2)
  w_ANCODFIS = space(16)
  w_ANPERFIS = space(1)
  w_ANDESCRI = space(40)
  w_ANCOGNOM = space(40)
  w_AN__NOME = space(20)
  w_AN_SESSO = space(1)
  w_ANDATNAS = ctod('  /  /  ')
  w_ANLOCNAS = space(30)
  w_ANPRONAS = space(2)
  w_ANCATPAR = space(2)
  w_ANEVEECC = space(1)
  w_ANFLGEST = space(1)
  w_ANLOCALI = space(30)
  w_ANPROVIN = space(2)
  w_ANCODCOM = space(4)
  w_ANCOFISC = space(25)
  w_ANINDIRI = space(35)
  w_ANNAZION = space(3)
  w_NACODEST = space(5)
  w_ANRITENU = space(1)
  w_ANFLSGRE = space(1)
  w_RHPERFIS = space(1)
  o_RHPERFIS = space(1)
  w_RHPEREST = space(1)
  o_RHPEREST = space(1)
  w_RHCAUPRE = space(2)
  o_RHCAUPRE = space(2)
  w_RHTIPOPE = space(1)
  w_RHSCLGEN = space(1)
  w_RHCODFIS = space(16)
  w_RHDESCRI = space(60)
  w_RHCOGNOM = space(50)
  w_RH__NOME = space(50)
  w_RH_SESSO = space(1)
  w_RHDATNAS = ctod('  /  /  ')
  w_RHCOMEST = space(30)
  w_RHPRONAS = space(2)
  w_RHEVEECC = space(1)
  w_RHCATPAR = space(2)
  w_RHCOMUNE = space(30)
  w_RHPROVIN = space(2)
  w_RHCODCOM = space(4)
  w_RHSTAEST = space(3)
  w_RHCITEST = space(30)
  w_RHINDEST = space(35)
  w_RHCOFISC = space(25)
  w_RECORD2 = .F.
  w_RECORD3 = .F.
  w_AU001004 = 0
  w_AU001005 = 0
  w_AU001006 = space(1)
  w_AU001008 = 0
  w_AU001009 = 0
  w_AU001020 = 0
  w_AU001021 = 0
  w_AU001022 = 0
  w_AU001023 = 0
  w_AU002004 = 0
  w_AU002005 = 0
  w_AU002008 = 0
  w_AU002009 = 0
  w_AU002020 = 0
  w_AU002021 = 0
  w_AU002022 = 0
  w_AU002023 = 0
  w_AU002006 = space(1)
  w_AU003004 = 0
  w_AU003005 = 0
  w_AU003008 = 0
  w_AU003009 = 0
  w_AU003020 = 0
  w_AU003021 = 0
  w_AU003022 = 0
  w_AU003023 = 0
  w_AU003006 = space(1)
  w_RHDAVERI = space(1)
  w_HASEVCOP = space(50)
  w_GDDATEST = space(10)
  w_GDTIPEST = space(1)
  w_HASEVENT = .F.
  w_GTSERIAL = space(10)
  w_GDSERIAL = space(10)

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_RHSERIAL = this.W_RHSERIAL

  * --- Children pointers
  GSRI_MDH = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsri_adh
   * --- Gestiti i soli eventi Modifica, cancellazione, inserimento
  func HasCPEvents(i_cOp)
   if this.cFunction='Query'
  	  	If Upper(i_cop)='ECPEDIT' Or Upper(i_cop)='ECPDELETE'
  	  		* esegue controllo se movimento bloccato se da problemi si pu� chiamare direttamente il Batch
          This.w_HASEVCOP=i_cop
  	  		This.NotifyEvent('HasEvent')
  	  		Return ( This.w_HASEVENT )
       Else
  	  		Return(.t.)
  	  	endif
  	Else
  	  	* --- Se l'evento non � modifica/cancellazione/inserimento prosegue
        Return(.t.)
    Endif
   EndFunc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=4, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'DATESTRH','gsri_adh')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsri_adhPag1","gsri_adh",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Dati anagrafici")
      .Pages(1).HelpContextID = 132331527
      .Pages(2).addobject("oPag","tgsri_adhPag2","gsri_adh",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Record H")
      .Pages(2).HelpContextID = 101426270
      .Pages(3).addobject("oPag","tgsri_adhPag3","gsri_adh",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Movimenti ritenute")
      .Pages(3).HelpContextID = 211316353
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='ENTI_COM'
    this.cWorkTables[3]='NAZIONI'
    this.cWorkTables[4]='GEDETTEL'
    this.cWorkTables[5]='DATESTRH'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(5))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.DATESTRH_IDX,5],7]
    this.nPostItConn=i_TableProp[this.DATESTRH_IDX,3]
  return

  function CreateChildren()
    this.GSRI_MDH = CREATEOBJECT('stdDynamicChild',this,'GSRI_MDH',this.oPgFrm.Page3.oPag.oLinkPC_3_1)
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSRI_MDH)
      this.GSRI_MDH.DestroyChildrenChain()
      this.GSRI_MDH=.NULL.
    endif
    this.oPgFrm.Page3.oPag.RemoveObject('oLinkPC_3_1')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSRI_MDH.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSRI_MDH.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSRI_MDH.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSRI_MDH.SetKey(;
            .w_RHSERIAL,"RHSERIAL";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSRI_MDH.ChangeRow(this.cRowID+'      1',1;
             ,.w_RHSERIAL,"RHSERIAL";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSRI_MDH)
        i_f=.GSRI_MDH.BuildFilter()
        if !(i_f==.GSRI_MDH.cQueryFilter)
          i_fnidx=.GSRI_MDH.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSRI_MDH.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSRI_MDH.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSRI_MDH.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSRI_MDH.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_RHSERIAL = NVL(RHSERIAL,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_5_joined
    link_1_5_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from DATESTRH where RHSERIAL=KeySet.RHSERIAL
    *
    i_nConn = i_TableProp[this.DATESTRH_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DATESTRH_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('DATESTRH')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "DATESTRH.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' DATESTRH '
      link_1_5_joined=this.AddJoinedLink_1_5(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'RHSERIAL',this.w_RHSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_OBTEST = CTOD('01-01-1900')
        .w_ANCAURIT = space(2)
        .w_ANCODFIS = space(16)
        .w_ANPERFIS = space(1)
        .w_ANDESCRI = space(40)
        .w_ANCOGNOM = space(40)
        .w_AN__NOME = space(20)
        .w_AN_SESSO = space(1)
        .w_ANDATNAS = ctod("  /  /  ")
        .w_ANLOCNAS = space(30)
        .w_ANPRONAS = space(2)
        .w_ANCATPAR = space(2)
        .w_ANEVEECC = space(1)
        .w_ANFLGEST = space(1)
        .w_ANLOCALI = space(30)
        .w_ANPROVIN = space(2)
        .w_ANCODCOM = space(4)
        .w_ANCOFISC = space(25)
        .w_ANINDIRI = space(35)
        .w_ANNAZION = space(3)
        .w_NACODEST = space(5)
        .w_ANRITENU = space(1)
        .w_ANFLSGRE = space(1)
        .w_RECORD2 = .f.
        .w_RECORD3 = .f.
        .w_AU001004 = 0
        .w_AU001005 = 0
        .w_AU001006 = space(1)
        .w_AU001008 = 0
        .w_AU001009 = 0
        .w_AU001020 = 0
        .w_AU001021 = 0
        .w_AU001022 = 0
        .w_AU001023 = 0
        .w_AU002004 = 0
        .w_AU002005 = 0
        .w_AU002008 = 0
        .w_AU002009 = 0
        .w_AU002020 = 0
        .w_AU002021 = 0
        .w_AU002022 = 0
        .w_AU002023 = 0
        .w_AU002006 = space(1)
        .w_AU003004 = 0
        .w_AU003005 = 0
        .w_AU003008 = 0
        .w_AU003009 = 0
        .w_AU003020 = 0
        .w_AU003021 = 0
        .w_AU003022 = 0
        .w_AU003023 = 0
        .w_AU003006 = space(1)
        .w_HASEVCOP = space(50)
        .w_GDDATEST = .w_Rhserial
        .w_GDTIPEST = 'H'
        .w_HASEVENT = .f.
        .w_GTSERIAL = .w_RHSERIAL
        .w_GDSERIAL = space(10)
        .w_RHSERIAL = NVL(RHSERIAL,space(10))
        .op_RHSERIAL = .w_RHSERIAL
        .w_RH__ANNO = NVL(RH__ANNO,0)
        .w_RHTIPCON = NVL(RHTIPCON,space(1))
        .w_RHCODCON = NVL(RHCODCON,space(15))
          if link_1_5_joined
            this.w_RHCODCON = NVL(ANCODICE105,NVL(this.w_RHCODCON,space(15)))
            this.w_ANCAURIT = NVL(ANCAURIT105,space(2))
            this.w_ANCODFIS = NVL(ANCODFIS105,space(16))
            this.w_ANPERFIS = NVL(ANPERFIS105,space(1))
            this.w_ANDESCRI = NVL(ANDESCRI105,space(40))
            this.w_ANCOGNOM = NVL(ANCOGNOM105,space(40))
            this.w_AN__NOME = NVL(AN__NOME105,space(20))
            this.w_AN_SESSO = NVL(AN_SESSO105,space(1))
            this.w_ANDATNAS = NVL(cp_ToDate(ANDATNAS105),ctod("  /  /  "))
            this.w_ANLOCNAS = NVL(ANLOCNAS105,space(30))
            this.w_ANPRONAS = NVL(ANPRONAS105,space(2))
            this.w_ANCATPAR = NVL(ANCATPAR105,space(2))
            this.w_ANEVEECC = NVL(ANEVEECC105,space(1))
            this.w_ANFLGEST = NVL(ANFLGEST105,space(1))
            this.w_ANLOCALI = NVL(ANLOCALI105,space(30))
            this.w_ANPROVIN = NVL(ANPROVIN105,space(2))
            this.w_ANCODCOM = NVL(ANCODCOM105,space(4))
            this.w_ANCOFISC = NVL(ANCOFISC105,space(25))
            this.w_ANINDIRI = NVL(ANINDIRI105,space(35))
            this.w_ANNAZION = NVL(ANNAZION105,space(3))
            this.w_ANRITENU = NVL(ANRITENU105,space(1))
            this.w_ANFLSGRE = NVL(ANFLSGRE105,space(1))
          else
          .link_1_5('Load')
          endif
        .w_RHTIPDAT = NVL(RHTIPDAT,space(1))
          .link_1_25('Load')
        .w_RHPERFIS = NVL(RHPERFIS,space(1))
        .w_RHPEREST = NVL(RHPEREST,space(1))
        .w_RHCAUPRE = NVL(RHCAUPRE,space(2))
        .w_RHTIPOPE = NVL(RHTIPOPE,space(1))
        .w_RHSCLGEN = NVL(RHSCLGEN,space(1))
        .w_RHCODFIS = NVL(RHCODFIS,space(16))
        .w_RHDESCRI = NVL(RHDESCRI,space(60))
        .w_RHCOGNOM = NVL(RHCOGNOM,space(50))
        .w_RH__NOME = NVL(RH__NOME,space(50))
        .w_RH_SESSO = NVL(RH_SESSO,space(1))
        .w_RHDATNAS = NVL(cp_ToDate(RHDATNAS),ctod("  /  /  "))
        .w_RHCOMEST = NVL(RHCOMEST,space(30))
        .w_RHPRONAS = NVL(RHPRONAS,space(2))
        .w_RHEVEECC = NVL(RHEVEECC,space(1))
        .w_RHCATPAR = NVL(RHCATPAR,space(2))
        .w_RHCOMUNE = NVL(RHCOMUNE,space(30))
        .w_RHPROVIN = NVL(RHPROVIN,space(2))
        .w_RHCODCOM = NVL(RHCODCOM,space(4))
          * evitabile
          *.link_1_63('Load')
        .w_RHSTAEST = NVL(RHSTAEST,space(3))
        .w_RHCITEST = NVL(RHCITEST,space(30))
        .w_RHINDEST = NVL(RHINDEST,space(35))
        .w_RHCOFISC = NVL(RHCOFISC,space(25))
        .w_RHDAVERI = NVL(RHDAVERI,space(1))
        .oPgFrm.Page1.oPag.oObj_1_80.Calculate()
          .link_1_84('Load')
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'DATESTRH')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_86.enabled = this.oPgFrm.Page1.oPag.oBtn_1_86.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_RHSERIAL = space(10)
      .w_OBTEST = ctod("  /  /  ")
      .w_RH__ANNO = 0
      .w_RHTIPCON = space(1)
      .w_RHCODCON = space(15)
      .w_RHTIPDAT = space(1)
      .w_ANCAURIT = space(2)
      .w_ANCODFIS = space(16)
      .w_ANPERFIS = space(1)
      .w_ANDESCRI = space(40)
      .w_ANCOGNOM = space(40)
      .w_AN__NOME = space(20)
      .w_AN_SESSO = space(1)
      .w_ANDATNAS = ctod("  /  /  ")
      .w_ANLOCNAS = space(30)
      .w_ANPRONAS = space(2)
      .w_ANCATPAR = space(2)
      .w_ANEVEECC = space(1)
      .w_ANFLGEST = space(1)
      .w_ANLOCALI = space(30)
      .w_ANPROVIN = space(2)
      .w_ANCODCOM = space(4)
      .w_ANCOFISC = space(25)
      .w_ANINDIRI = space(35)
      .w_ANNAZION = space(3)
      .w_NACODEST = space(5)
      .w_ANRITENU = space(1)
      .w_ANFLSGRE = space(1)
      .w_RHPERFIS = space(1)
      .w_RHPEREST = space(1)
      .w_RHCAUPRE = space(2)
      .w_RHTIPOPE = space(1)
      .w_RHSCLGEN = space(1)
      .w_RHCODFIS = space(16)
      .w_RHDESCRI = space(60)
      .w_RHCOGNOM = space(50)
      .w_RH__NOME = space(50)
      .w_RH_SESSO = space(1)
      .w_RHDATNAS = ctod("  /  /  ")
      .w_RHCOMEST = space(30)
      .w_RHPRONAS = space(2)
      .w_RHEVEECC = space(1)
      .w_RHCATPAR = space(2)
      .w_RHCOMUNE = space(30)
      .w_RHPROVIN = space(2)
      .w_RHCODCOM = space(4)
      .w_RHSTAEST = space(3)
      .w_RHCITEST = space(30)
      .w_RHINDEST = space(35)
      .w_RHCOFISC = space(25)
      .w_RECORD2 = .f.
      .w_RECORD3 = .f.
      .w_AU001004 = 0
      .w_AU001005 = 0
      .w_AU001006 = space(1)
      .w_AU001008 = 0
      .w_AU001009 = 0
      .w_AU001020 = 0
      .w_AU001021 = 0
      .w_AU001022 = 0
      .w_AU001023 = 0
      .w_AU002004 = 0
      .w_AU002005 = 0
      .w_AU002008 = 0
      .w_AU002009 = 0
      .w_AU002020 = 0
      .w_AU002021 = 0
      .w_AU002022 = 0
      .w_AU002023 = 0
      .w_AU002006 = space(1)
      .w_AU003004 = 0
      .w_AU003005 = 0
      .w_AU003008 = 0
      .w_AU003009 = 0
      .w_AU003020 = 0
      .w_AU003021 = 0
      .w_AU003022 = 0
      .w_AU003023 = 0
      .w_AU003006 = space(1)
      .w_RHDAVERI = space(1)
      .w_HASEVCOP = space(50)
      .w_GDDATEST = space(10)
      .w_GDTIPEST = space(1)
      .w_HASEVENT = .f.
      .w_GTSERIAL = space(10)
      .w_GDSERIAL = space(10)
      if .cFunction<>"Filter"
          .DoRTCalc(1,1,.f.)
        .w_OBTEST = CTOD('01-01-1900')
        .w_RH__ANNO = year(i_DATSYS)-1
        .w_RHTIPCON = 'F'
        .DoRTCalc(5,5,.f.)
          if not(empty(.w_RHCODCON))
          .link_1_5('Full')
          endif
        .w_RHTIPDAT = 'M'
        .DoRTCalc(7,25,.f.)
          if not(empty(.w_ANNAZION))
          .link_1_25('Full')
          endif
          .DoRTCalc(26,28,.f.)
        .w_RHPERFIS = .w_ANPERFIS
        .w_RHPEREST = .w_ANFLGEST
        .w_RHCAUPRE = IIF(.w_ANCAURIT='F' OR EMPTY(.w_ANCAURIT),'A',.w_ANCAURIT)
        .w_RHTIPOPE = 'O'
        .w_RHSCLGEN = 'N'
        .w_RHCODFIS = iif(.w_RHPEREST<>'S',Upper(.w_ANCODFIS),SPACE(16))
        .w_RHDESCRI = Upper(.w_ANDESCRI)
        .w_RHCOGNOM = IIF(.w_RHPERFIS='S',Upper(.w_ANCOGNOM),SPACE(50))
        .w_RH__NOME = IIF(.w_RHPERFIS='S',Upper(.w_AN__NOME),SPACE(50))
        .w_RH_SESSO = iif(.w_RHPERFIS='S' and not empty(.w_AN_SESSO), .w_AN_SESSO, 'M')
        .w_RHDATNAS = IIF(.w_RHPERFIS='S',.w_ANDATNAS,CTOD('  -  -   '))
        .w_RHCOMEST = IIF(.w_RHPERFIS='S',Upper(.w_ANLOCNAS),SPACE(30))
        .w_RHPRONAS = IIF(.w_RHPERFIS='S',Upper(.w_ANPRONAS),SPACE(2))
        .w_RHEVEECC = iif(.w_ANEVEECC $ '138',.w_ANEVEECC,'0')
        .w_RHCATPAR = iif(.w_ANCATPAR == 'Z' OR .w_ANCATPAR = 'Z1' OR .w_ANCATPAR = 'Z2',.w_ANCATPAR,'  ')
        .w_RHCOMUNE = IIF(.w_RHPEREST<>'S',Upper(.w_ANLOCALI),SPACE(30))
        .w_RHPROVIN = IIF(.w_RHPEREST<>'S',Upper(.w_ANPROVIN),SPACE(2))
        .w_RHCODCOM = IIF(.w_RHPEREST<>'S' AND .w_RHCAUPRE='N',Upper(.w_ANCODCOM),SPACE(4))
        .DoRTCalc(46,46,.f.)
          if not(empty(.w_RHCODCOM))
          .link_1_63('Full')
          endif
        .w_RHSTAEST = IIF(.w_RHPEREST='S',Upper(RIGHT(Alltrim(.w_NACODEST),3)),SPACE(3))
        .w_RHCITEST = IIF(.w_RHPEREST='S',Upper(.w_ANLOCALI),SPACE(30))
        .w_RHINDEST = IIF(.w_RHPEREST='S',Upper(.w_ANINDIRI),SPACE(35))
        .w_RHCOFISC = iif(.w_Rhperest='S',Upper(iif(Empty(.w_Ancofisc), .w_Ancodfis, .w_Ancofisc)),Space(25))
        .w_RECORD2 = .f.
        .w_RECORD3 = .f.
          .DoRTCalc(53,79,.f.)
        .w_RHDAVERI = 'N'
        .oPgFrm.Page1.oPag.oObj_1_80.Calculate()
          .DoRTCalc(81,81,.f.)
        .w_GDDATEST = .w_Rhserial
        .w_GDTIPEST = 'H'
          .DoRTCalc(84,84,.f.)
        .w_GTSERIAL = .w_RHSERIAL
        .DoRTCalc(85,85,.f.)
          if not(empty(.w_GTSERIAL))
          .link_1_84('Full')
          endif
      endif
    endwith
    cp_BlankRecExtFlds(this,'DATESTRH')
    this.DoRTCalc(86,86,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_86.enabled = this.oPgFrm.Page1.oPag.oBtn_1_86.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DATESTRH_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DATESTRH_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"RHSER","i_codazi,w_RHSERIAL")
      .op_codazi = .w_codazi
      .op_RHSERIAL = .w_RHSERIAL
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oRH__ANNO_1_3.enabled = i_bVal
      .Page1.oPag.oRHCODCON_1_5.enabled = i_bVal
      .Page1.oPag.oRHCAUPRE_1_31.enabled = i_bVal
      .Page1.oPag.oRHTIPOPE_1_32.enabled = i_bVal
      .Page1.oPag.oRHSCLGEN_1_33.enabled = i_bVal
      .Page1.oPag.oRHCODFIS_1_34.enabled = i_bVal
      .Page1.oPag.oRHDESCRI_1_41.enabled = i_bVal
      .Page1.oPag.oRHCOGNOM_1_42.enabled = i_bVal
      .Page1.oPag.oRH__NOME_1_43.enabled = i_bVal
      .Page1.oPag.oRH_SESSO_1_47.enabled = i_bVal
      .Page1.oPag.oRHDATNAS_1_48.enabled = i_bVal
      .Page1.oPag.oRHCOMEST_1_51.enabled = i_bVal
      .Page1.oPag.oRHPRONAS_1_53.enabled = i_bVal
      .Page1.oPag.oRHEVEECC_1_57.enabled = i_bVal
      .Page1.oPag.oRHCATPAR_1_58.enabled = i_bVal
      .Page1.oPag.oRHCOMUNE_1_60.enabled = i_bVal
      .Page1.oPag.oRHPROVIN_1_61.enabled = i_bVal
      .Page1.oPag.oRHCODCOM_1_63.enabled = i_bVal
      .Page1.oPag.oRHSTAEST_1_67.enabled = i_bVal
      .Page1.oPag.oRHCITEST_1_68.enabled = i_bVal
      .Page1.oPag.oRHINDEST_1_69.enabled = i_bVal
      .Page1.oPag.oRHCOFISC_1_73.enabled = i_bVal
      .Page1.oPag.oRHDAVERI_1_78.enabled = i_bVal
      .Page1.oPag.oBtn_1_86.enabled = .Page1.oPag.oBtn_1_86.mCond()
      .Page1.oPag.oObj_1_80.enabled = i_bVal
    endwith
    this.GSRI_MDH.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'DATESTRH',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSRI_MDH.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.DATESTRH_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RHSERIAL,"RHSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RH__ANNO,"RH__ANNO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RHTIPCON,"RHTIPCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RHCODCON,"RHCODCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RHTIPDAT,"RHTIPDAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RHPERFIS,"RHPERFIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RHPEREST,"RHPEREST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RHCAUPRE,"RHCAUPRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RHTIPOPE,"RHTIPOPE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RHSCLGEN,"RHSCLGEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RHCODFIS,"RHCODFIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RHDESCRI,"RHDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RHCOGNOM,"RHCOGNOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RH__NOME,"RH__NOME",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RH_SESSO,"RH_SESSO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RHDATNAS,"RHDATNAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RHCOMEST,"RHCOMEST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RHPRONAS,"RHPRONAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RHEVEECC,"RHEVEECC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RHCATPAR,"RHCATPAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RHCOMUNE,"RHCOMUNE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RHPROVIN,"RHPROVIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RHCODCOM,"RHCODCOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RHSTAEST,"RHSTAEST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RHCITEST,"RHCITEST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RHINDEST,"RHINDEST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RHCOFISC,"RHCOFISC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RHDAVERI,"RHDAVERI",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.DATESTRH_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DATESTRH_IDX,2])
    i_lTable = "DATESTRH"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.DATESTRH_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DATESTRH_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DATESTRH_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.DATESTRH_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"RHSER","i_codazi,w_RHSERIAL")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into DATESTRH
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'DATESTRH')
        i_extval=cp_InsertValODBCExtFlds(this,'DATESTRH')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(RHSERIAL,RH__ANNO,RHTIPCON,RHCODCON,RHTIPDAT"+;
                  ",RHPERFIS,RHPEREST,RHCAUPRE,RHTIPOPE,RHSCLGEN"+;
                  ",RHCODFIS,RHDESCRI,RHCOGNOM,RH__NOME,RH_SESSO"+;
                  ",RHDATNAS,RHCOMEST,RHPRONAS,RHEVEECC,RHCATPAR"+;
                  ",RHCOMUNE,RHPROVIN,RHCODCOM,RHSTAEST,RHCITEST"+;
                  ",RHINDEST,RHCOFISC,RHDAVERI "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_RHSERIAL)+;
                  ","+cp_ToStrODBC(this.w_RH__ANNO)+;
                  ","+cp_ToStrODBC(this.w_RHTIPCON)+;
                  ","+cp_ToStrODBCNull(this.w_RHCODCON)+;
                  ","+cp_ToStrODBC(this.w_RHTIPDAT)+;
                  ","+cp_ToStrODBC(this.w_RHPERFIS)+;
                  ","+cp_ToStrODBC(this.w_RHPEREST)+;
                  ","+cp_ToStrODBC(this.w_RHCAUPRE)+;
                  ","+cp_ToStrODBC(this.w_RHTIPOPE)+;
                  ","+cp_ToStrODBC(this.w_RHSCLGEN)+;
                  ","+cp_ToStrODBC(this.w_RHCODFIS)+;
                  ","+cp_ToStrODBC(this.w_RHDESCRI)+;
                  ","+cp_ToStrODBC(this.w_RHCOGNOM)+;
                  ","+cp_ToStrODBC(this.w_RH__NOME)+;
                  ","+cp_ToStrODBC(this.w_RH_SESSO)+;
                  ","+cp_ToStrODBC(this.w_RHDATNAS)+;
                  ","+cp_ToStrODBC(this.w_RHCOMEST)+;
                  ","+cp_ToStrODBC(this.w_RHPRONAS)+;
                  ","+cp_ToStrODBC(this.w_RHEVEECC)+;
                  ","+cp_ToStrODBC(this.w_RHCATPAR)+;
                  ","+cp_ToStrODBC(this.w_RHCOMUNE)+;
                  ","+cp_ToStrODBC(this.w_RHPROVIN)+;
                  ","+cp_ToStrODBCNull(this.w_RHCODCOM)+;
                  ","+cp_ToStrODBC(this.w_RHSTAEST)+;
                  ","+cp_ToStrODBC(this.w_RHCITEST)+;
                  ","+cp_ToStrODBC(this.w_RHINDEST)+;
                  ","+cp_ToStrODBC(this.w_RHCOFISC)+;
                  ","+cp_ToStrODBC(this.w_RHDAVERI)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'DATESTRH')
        i_extval=cp_InsertValVFPExtFlds(this,'DATESTRH')
        cp_CheckDeletedKey(i_cTable,0,'RHSERIAL',this.w_RHSERIAL)
        INSERT INTO (i_cTable);
              (RHSERIAL,RH__ANNO,RHTIPCON,RHCODCON,RHTIPDAT,RHPERFIS,RHPEREST,RHCAUPRE,RHTIPOPE,RHSCLGEN,RHCODFIS,RHDESCRI,RHCOGNOM,RH__NOME,RH_SESSO,RHDATNAS,RHCOMEST,RHPRONAS,RHEVEECC,RHCATPAR,RHCOMUNE,RHPROVIN,RHCODCOM,RHSTAEST,RHCITEST,RHINDEST,RHCOFISC,RHDAVERI  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_RHSERIAL;
                  ,this.w_RH__ANNO;
                  ,this.w_RHTIPCON;
                  ,this.w_RHCODCON;
                  ,this.w_RHTIPDAT;
                  ,this.w_RHPERFIS;
                  ,this.w_RHPEREST;
                  ,this.w_RHCAUPRE;
                  ,this.w_RHTIPOPE;
                  ,this.w_RHSCLGEN;
                  ,this.w_RHCODFIS;
                  ,this.w_RHDESCRI;
                  ,this.w_RHCOGNOM;
                  ,this.w_RH__NOME;
                  ,this.w_RH_SESSO;
                  ,this.w_RHDATNAS;
                  ,this.w_RHCOMEST;
                  ,this.w_RHPRONAS;
                  ,this.w_RHEVEECC;
                  ,this.w_RHCATPAR;
                  ,this.w_RHCOMUNE;
                  ,this.w_RHPROVIN;
                  ,this.w_RHCODCOM;
                  ,this.w_RHSTAEST;
                  ,this.w_RHCITEST;
                  ,this.w_RHINDEST;
                  ,this.w_RHCOFISC;
                  ,this.w_RHDAVERI;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.DATESTRH_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DATESTRH_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.DATESTRH_IDX,i_nConn)
      *
      * update DATESTRH
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'DATESTRH')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " RH__ANNO="+cp_ToStrODBC(this.w_RH__ANNO)+;
             ",RHTIPCON="+cp_ToStrODBC(this.w_RHTIPCON)+;
             ",RHCODCON="+cp_ToStrODBCNull(this.w_RHCODCON)+;
             ",RHTIPDAT="+cp_ToStrODBC(this.w_RHTIPDAT)+;
             ",RHPERFIS="+cp_ToStrODBC(this.w_RHPERFIS)+;
             ",RHPEREST="+cp_ToStrODBC(this.w_RHPEREST)+;
             ",RHCAUPRE="+cp_ToStrODBC(this.w_RHCAUPRE)+;
             ",RHTIPOPE="+cp_ToStrODBC(this.w_RHTIPOPE)+;
             ",RHSCLGEN="+cp_ToStrODBC(this.w_RHSCLGEN)+;
             ",RHCODFIS="+cp_ToStrODBC(this.w_RHCODFIS)+;
             ",RHDESCRI="+cp_ToStrODBC(this.w_RHDESCRI)+;
             ",RHCOGNOM="+cp_ToStrODBC(this.w_RHCOGNOM)+;
             ",RH__NOME="+cp_ToStrODBC(this.w_RH__NOME)+;
             ",RH_SESSO="+cp_ToStrODBC(this.w_RH_SESSO)+;
             ",RHDATNAS="+cp_ToStrODBC(this.w_RHDATNAS)+;
             ",RHCOMEST="+cp_ToStrODBC(this.w_RHCOMEST)+;
             ",RHPRONAS="+cp_ToStrODBC(this.w_RHPRONAS)+;
             ",RHEVEECC="+cp_ToStrODBC(this.w_RHEVEECC)+;
             ",RHCATPAR="+cp_ToStrODBC(this.w_RHCATPAR)+;
             ",RHCOMUNE="+cp_ToStrODBC(this.w_RHCOMUNE)+;
             ",RHPROVIN="+cp_ToStrODBC(this.w_RHPROVIN)+;
             ",RHCODCOM="+cp_ToStrODBCNull(this.w_RHCODCOM)+;
             ",RHSTAEST="+cp_ToStrODBC(this.w_RHSTAEST)+;
             ",RHCITEST="+cp_ToStrODBC(this.w_RHCITEST)+;
             ",RHINDEST="+cp_ToStrODBC(this.w_RHINDEST)+;
             ",RHCOFISC="+cp_ToStrODBC(this.w_RHCOFISC)+;
             ",RHDAVERI="+cp_ToStrODBC(this.w_RHDAVERI)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'DATESTRH')
        i_cWhere = cp_PKFox(i_cTable  ,'RHSERIAL',this.w_RHSERIAL  )
        UPDATE (i_cTable) SET;
              RH__ANNO=this.w_RH__ANNO;
             ,RHTIPCON=this.w_RHTIPCON;
             ,RHCODCON=this.w_RHCODCON;
             ,RHTIPDAT=this.w_RHTIPDAT;
             ,RHPERFIS=this.w_RHPERFIS;
             ,RHPEREST=this.w_RHPEREST;
             ,RHCAUPRE=this.w_RHCAUPRE;
             ,RHTIPOPE=this.w_RHTIPOPE;
             ,RHSCLGEN=this.w_RHSCLGEN;
             ,RHCODFIS=this.w_RHCODFIS;
             ,RHDESCRI=this.w_RHDESCRI;
             ,RHCOGNOM=this.w_RHCOGNOM;
             ,RH__NOME=this.w_RH__NOME;
             ,RH_SESSO=this.w_RH_SESSO;
             ,RHDATNAS=this.w_RHDATNAS;
             ,RHCOMEST=this.w_RHCOMEST;
             ,RHPRONAS=this.w_RHPRONAS;
             ,RHEVEECC=this.w_RHEVEECC;
             ,RHCATPAR=this.w_RHCATPAR;
             ,RHCOMUNE=this.w_RHCOMUNE;
             ,RHPROVIN=this.w_RHPROVIN;
             ,RHCODCOM=this.w_RHCODCOM;
             ,RHSTAEST=this.w_RHSTAEST;
             ,RHCITEST=this.w_RHCITEST;
             ,RHINDEST=this.w_RHINDEST;
             ,RHCOFISC=this.w_RHCOFISC;
             ,RHDAVERI=this.w_RHDAVERI;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSRI_MDH : Saving
      this.GSRI_MDH.ChangeRow(this.cRowID+'      1',0;
             ,this.w_RHSERIAL,"RHSERIAL";
             )
      this.GSRI_MDH.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSRI_MDH : Deleting
    this.GSRI_MDH.ChangeRow(this.cRowID+'      1',0;
           ,this.w_RHSERIAL,"RHSERIAL";
           )
    this.GSRI_MDH.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.DATESTRH_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DATESTRH_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.DATESTRH_IDX,i_nConn)
      *
      * delete DATESTRH
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'RHSERIAL',this.w_RHSERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DATESTRH_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DATESTRH_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,24,.t.)
          .link_1_25('Full')
        .DoRTCalc(26,28,.t.)
        if .o_RHCODCON<>.w_RHCODCON
            .w_RHPERFIS = .w_ANPERFIS
        endif
        if .o_RHCODCON<>.w_RHCODCON
            .w_RHPEREST = .w_ANFLGEST
        endif
        if .o_RHCODCON<>.w_RHCODCON
            .w_RHCAUPRE = IIF(.w_ANCAURIT='F' OR EMPTY(.w_ANCAURIT),'A',.w_ANCAURIT)
        endif
        .DoRTCalc(32,33,.t.)
        if .o_RHCODCON<>.w_RHCODCON.or. .o_RHPEREST<>.w_RHPEREST
            .w_RHCODFIS = iif(.w_RHPEREST<>'S',Upper(.w_ANCODFIS),SPACE(16))
        endif
        if .o_RHCODCON<>.w_RHCODCON.or. .o_RHPERFIS<>.w_RHPERFIS
            .w_RHDESCRI = Upper(.w_ANDESCRI)
        endif
        if .o_RHCODCON<>.w_RHCODCON.or. .o_RHPERFIS<>.w_RHPERFIS
            .w_RHCOGNOM = IIF(.w_RHPERFIS='S',Upper(.w_ANCOGNOM),SPACE(50))
        endif
        if .o_RHCODCON<>.w_RHCODCON.or. .o_RHPERFIS<>.w_RHPERFIS
            .w_RH__NOME = IIF(.w_RHPERFIS='S',Upper(.w_AN__NOME),SPACE(50))
        endif
        if .o_RHCODCON<>.w_RHCODCON.or. .o_RHPERFIS<>.w_RHPERFIS
            .w_RH_SESSO = iif(.w_RHPERFIS='S' and not empty(.w_AN_SESSO), .w_AN_SESSO, 'M')
        endif
        if .o_RHCODCON<>.w_RHCODCON.or. .o_RHPERFIS <>.w_RHPERFIS 
            .w_RHDATNAS = IIF(.w_RHPERFIS='S',.w_ANDATNAS,CTOD('  -  -   '))
        endif
        if .o_RHCODCON<>.w_RHCODCON.or. .o_RHPERFIS<>.w_RHPERFIS
            .w_RHCOMEST = IIF(.w_RHPERFIS='S',Upper(.w_ANLOCNAS),SPACE(30))
        endif
        if .o_RHCODCON<>.w_RHCODCON.or. .o_RHPERFIS<>.w_RHPERFIS
            .w_RHPRONAS = IIF(.w_RHPERFIS='S',Upper(.w_ANPRONAS),SPACE(2))
        endif
        if .o_RHCODCON<>.w_RHCODCON
            .w_RHEVEECC = iif(.w_ANEVEECC $ '138',.w_ANEVEECC,'0')
        endif
        if .o_RHCODCON<>.w_RHCODCON
            .w_RHCATPAR = iif(.w_ANCATPAR == 'Z' OR .w_ANCATPAR = 'Z1' OR .w_ANCATPAR = 'Z2',.w_ANCATPAR,'  ')
        endif
        if .o_RHCODCON<>.w_RHCODCON.or. .o_RHPERFIS<>.w_RHPERFIS
            .w_RHCOMUNE = IIF(.w_RHPEREST<>'S',Upper(.w_ANLOCALI),SPACE(30))
        endif
        if .o_RHCODCON<>.w_RHCODCON.or. .o_RHPEREST<>.w_RHPEREST
            .w_RHPROVIN = IIF(.w_RHPEREST<>'S',Upper(.w_ANPROVIN),SPACE(2))
        endif
        if .o_RHCODCON<>.w_RHCODCON.or. .o_RHPEREST<>.w_RHPEREST.or. .o_RHCAUPRE<>.w_RHCAUPRE
            .w_RHCODCOM = IIF(.w_RHPEREST<>'S' AND .w_RHCAUPRE='N',Upper(.w_ANCODCOM),SPACE(4))
          .link_1_63('Full')
        endif
        if .o_RHCODCON<>.w_RHCODCON.or. .o_RHPEREST<>.w_RHPEREST
            .w_RHSTAEST = IIF(.w_RHPEREST='S',Upper(RIGHT(Alltrim(.w_NACODEST),3)),SPACE(3))
        endif
        if .o_RHCODCON<>.w_RHCODCON.or. .o_RHPEREST<>.w_RHPEREST
            .w_RHCITEST = IIF(.w_RHPEREST='S',Upper(.w_ANLOCALI),SPACE(30))
        endif
        if .o_RHCODCON<>.w_RHCODCON.or. .o_RHPEREST<>.w_RHPEREST
            .w_RHINDEST = IIF(.w_RHPEREST='S',Upper(.w_ANINDIRI),SPACE(35))
        endif
        if .o_RHCODCON<>.w_RHCODCON.or. .o_RHPEREST<>.w_RHPEREST
            .w_RHCOFISC = iif(.w_Rhperest='S',Upper(iif(Empty(.w_Ancofisc), .w_Ancodfis, .w_Ancofisc)),Space(25))
        endif
        .oPgFrm.Page1.oPag.oObj_1_80.Calculate()
        .DoRTCalc(51,84,.t.)
          .link_1_84('Full')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_codazi<>.w_codazi
           cp_AskTableProg(this,i_nConn,"RHSER","i_codazi,w_RHSERIAL")
          .op_RHSERIAL = .w_RHSERIAL
        endif
        .op_codazi = .w_codazi
      endwith
      this.DoRTCalc(86,86,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_80.Calculate()
    endwith
  return

  proc Calculate_DAOWAKGBUR()
    with this
          * --- Calcolo totali
          Gsri_Bdh(this;
              ,'T';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oRH__ANNO_1_3.enabled = this.oPgFrm.Page1.oPag.oRH__ANNO_1_3.mCond()
    this.oPgFrm.Page1.oPag.oRHCODCON_1_5.enabled = this.oPgFrm.Page1.oPag.oRHCODCON_1_5.mCond()
    this.oPgFrm.Page1.oPag.oRHCAUPRE_1_31.enabled = this.oPgFrm.Page1.oPag.oRHCAUPRE_1_31.mCond()
    this.oPgFrm.Page1.oPag.oRHTIPOPE_1_32.enabled = this.oPgFrm.Page1.oPag.oRHTIPOPE_1_32.mCond()
    this.oPgFrm.Page1.oPag.oRHCODFIS_1_34.enabled = this.oPgFrm.Page1.oPag.oRHCODFIS_1_34.mCond()
    this.oPgFrm.Page1.oPag.oRHDESCRI_1_41.enabled = this.oPgFrm.Page1.oPag.oRHDESCRI_1_41.mCond()
    this.oPgFrm.Page1.oPag.oRHCOGNOM_1_42.enabled = this.oPgFrm.Page1.oPag.oRHCOGNOM_1_42.mCond()
    this.oPgFrm.Page1.oPag.oRH__NOME_1_43.enabled = this.oPgFrm.Page1.oPag.oRH__NOME_1_43.mCond()
    this.oPgFrm.Page1.oPag.oRH_SESSO_1_47.enabled = this.oPgFrm.Page1.oPag.oRH_SESSO_1_47.mCond()
    this.oPgFrm.Page1.oPag.oRHDATNAS_1_48.enabled = this.oPgFrm.Page1.oPag.oRHDATNAS_1_48.mCond()
    this.oPgFrm.Page1.oPag.oRHCOMEST_1_51.enabled = this.oPgFrm.Page1.oPag.oRHCOMEST_1_51.mCond()
    this.oPgFrm.Page1.oPag.oRHPRONAS_1_53.enabled = this.oPgFrm.Page1.oPag.oRHPRONAS_1_53.mCond()
    this.oPgFrm.Page1.oPag.oRHEVEECC_1_57.enabled = this.oPgFrm.Page1.oPag.oRHEVEECC_1_57.mCond()
    this.oPgFrm.Page1.oPag.oRHCATPAR_1_58.enabled = this.oPgFrm.Page1.oPag.oRHCATPAR_1_58.mCond()
    this.oPgFrm.Page1.oPag.oRHCOMUNE_1_60.enabled = this.oPgFrm.Page1.oPag.oRHCOMUNE_1_60.mCond()
    this.oPgFrm.Page1.oPag.oRHPROVIN_1_61.enabled = this.oPgFrm.Page1.oPag.oRHPROVIN_1_61.mCond()
    this.oPgFrm.Page1.oPag.oRHCODCOM_1_63.enabled = this.oPgFrm.Page1.oPag.oRHCODCOM_1_63.mCond()
    this.oPgFrm.Page1.oPag.oRHSTAEST_1_67.enabled = this.oPgFrm.Page1.oPag.oRHSTAEST_1_67.mCond()
    this.oPgFrm.Page1.oPag.oRHCITEST_1_68.enabled = this.oPgFrm.Page1.oPag.oRHCITEST_1_68.mCond()
    this.oPgFrm.Page1.oPag.oRHINDEST_1_69.enabled = this.oPgFrm.Page1.oPag.oRHINDEST_1_69.mCond()
    this.oPgFrm.Page1.oPag.oRHCOFISC_1_73.enabled = this.oPgFrm.Page1.oPag.oRHCOFISC_1_73.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_86.enabled = this.oPgFrm.Page1.oPag.oBtn_1_86.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page2.oPag.oStr_2_8.visible=!this.oPgFrm.Page2.oPag.oStr_2_8.mHide()
    this.oPgFrm.Page2.oPag.oAU001006_2_9.visible=!this.oPgFrm.Page2.oPag.oAU001006_2_9.mHide()
    this.oPgFrm.Page2.oPag.oAU002004_2_22.visible=!this.oPgFrm.Page2.oPag.oAU002004_2_22.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_23.visible=!this.oPgFrm.Page2.oPag.oStr_2_23.mHide()
    this.oPgFrm.Page2.oPag.oAU002005_2_24.visible=!this.oPgFrm.Page2.oPag.oAU002005_2_24.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_25.visible=!this.oPgFrm.Page2.oPag.oStr_2_25.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_26.visible=!this.oPgFrm.Page2.oPag.oStr_2_26.mHide()
    this.oPgFrm.Page2.oPag.oAU002008_2_27.visible=!this.oPgFrm.Page2.oPag.oAU002008_2_27.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_28.visible=!this.oPgFrm.Page2.oPag.oStr_2_28.mHide()
    this.oPgFrm.Page2.oPag.oAU002009_2_29.visible=!this.oPgFrm.Page2.oPag.oAU002009_2_29.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_30.visible=!this.oPgFrm.Page2.oPag.oStr_2_30.mHide()
    this.oPgFrm.Page2.oPag.oAU002020_2_31.visible=!this.oPgFrm.Page2.oPag.oAU002020_2_31.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_32.visible=!this.oPgFrm.Page2.oPag.oStr_2_32.mHide()
    this.oPgFrm.Page2.oPag.oAU002021_2_33.visible=!this.oPgFrm.Page2.oPag.oAU002021_2_33.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_34.visible=!this.oPgFrm.Page2.oPag.oStr_2_34.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_35.visible=!this.oPgFrm.Page2.oPag.oStr_2_35.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_36.visible=!this.oPgFrm.Page2.oPag.oStr_2_36.mHide()
    this.oPgFrm.Page2.oPag.oAU002022_2_37.visible=!this.oPgFrm.Page2.oPag.oAU002022_2_37.mHide()
    this.oPgFrm.Page2.oPag.oAU002023_2_38.visible=!this.oPgFrm.Page2.oPag.oAU002023_2_38.mHide()
    this.oPgFrm.Page2.oPag.oAU002006_2_39.visible=!this.oPgFrm.Page2.oPag.oAU002006_2_39.mHide()
    this.oPgFrm.Page2.oPag.oAU003004_2_40.visible=!this.oPgFrm.Page2.oPag.oAU003004_2_40.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_41.visible=!this.oPgFrm.Page2.oPag.oStr_2_41.mHide()
    this.oPgFrm.Page2.oPag.oAU003005_2_42.visible=!this.oPgFrm.Page2.oPag.oAU003005_2_42.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_43.visible=!this.oPgFrm.Page2.oPag.oStr_2_43.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_44.visible=!this.oPgFrm.Page2.oPag.oStr_2_44.mHide()
    this.oPgFrm.Page2.oPag.oAU003008_2_45.visible=!this.oPgFrm.Page2.oPag.oAU003008_2_45.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_46.visible=!this.oPgFrm.Page2.oPag.oStr_2_46.mHide()
    this.oPgFrm.Page2.oPag.oAU003009_2_47.visible=!this.oPgFrm.Page2.oPag.oAU003009_2_47.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_48.visible=!this.oPgFrm.Page2.oPag.oStr_2_48.mHide()
    this.oPgFrm.Page2.oPag.oAU003020_2_49.visible=!this.oPgFrm.Page2.oPag.oAU003020_2_49.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_50.visible=!this.oPgFrm.Page2.oPag.oStr_2_50.mHide()
    this.oPgFrm.Page2.oPag.oAU003021_2_51.visible=!this.oPgFrm.Page2.oPag.oAU003021_2_51.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_52.visible=!this.oPgFrm.Page2.oPag.oStr_2_52.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_53.visible=!this.oPgFrm.Page2.oPag.oStr_2_53.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_54.visible=!this.oPgFrm.Page2.oPag.oStr_2_54.mHide()
    this.oPgFrm.Page2.oPag.oAU003022_2_55.visible=!this.oPgFrm.Page2.oPag.oAU003022_2_55.mHide()
    this.oPgFrm.Page2.oPag.oAU003023_2_56.visible=!this.oPgFrm.Page2.oPag.oAU003023_2_56.mHide()
    this.oPgFrm.Page2.oPag.oAU003006_2_57.visible=!this.oPgFrm.Page2.oPag.oAU003006_2_57.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_77.visible=!this.oPgFrm.Page1.oPag.oStr_1_77.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_86.visible=!this.oPgFrm.Page1.oPag.oBtn_1_86.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsri_adh
    * instanzio il figlio della seconda pagina immediatamente
    IF Upper(CEVENT)='INIT'
      if Upper(this.GSRI_MDH.class)='STDDYNAMICCHILD'
        This.oPgFrm.Pages[3].opag.uienable(.T.)
        This.oPgFrm.ActivePage=1
      Endif
    Endif
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("ActivatePage 2")
          .Calculate_DAOWAKGBUR()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_80.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=RHCODCON
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RHCODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFR',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_RHCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_RHTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANCAURIT,ANCODFIS,ANPERFIS,ANDESCRI,ANCOGNOM,AN__NOME,AN_SESSO,ANDATNAS,ANLOCNAS,ANPRONAS,ANCATPAR,ANEVEECC,ANFLGEST,ANLOCALI,ANPROVIN,ANCODCOM,ANCOFISC,ANINDIRI,ANNAZION,ANRITENU,ANFLSGRE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_RHTIPCON;
                     ,'ANCODICE',trim(this.w_RHCODCON))
          select ANTIPCON,ANCODICE,ANCAURIT,ANCODFIS,ANPERFIS,ANDESCRI,ANCOGNOM,AN__NOME,AN_SESSO,ANDATNAS,ANLOCNAS,ANPRONAS,ANCATPAR,ANEVEECC,ANFLGEST,ANLOCALI,ANPROVIN,ANCODCOM,ANCOFISC,ANINDIRI,ANNAZION,ANRITENU,ANFLSGRE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RHCODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RHCODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oRHCODCON_1_5'),i_cWhere,'GSAR_AFR',"Elenco fornitori",'GSRI_ADH.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_RHTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANCAURIT,ANCODFIS,ANPERFIS,ANDESCRI,ANCOGNOM,AN__NOME,AN_SESSO,ANDATNAS,ANLOCNAS,ANPRONAS,ANCATPAR,ANEVEECC,ANFLGEST,ANLOCALI,ANPROVIN,ANCODCOM,ANCOFISC,ANINDIRI,ANNAZION,ANRITENU,ANFLSGRE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANCAURIT,ANCODFIS,ANPERFIS,ANDESCRI,ANCOGNOM,AN__NOME,AN_SESSO,ANDATNAS,ANLOCNAS,ANPRONAS,ANCATPAR,ANEVEECC,ANFLGEST,ANLOCALI,ANPROVIN,ANCODCOM,ANCOFISC,ANINDIRI,ANNAZION,ANRITENU,ANFLSGRE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice fornitore inesistente o non soggetto a ritenute")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANCAURIT,ANCODFIS,ANPERFIS,ANDESCRI,ANCOGNOM,AN__NOME,AN_SESSO,ANDATNAS,ANLOCNAS,ANPRONAS,ANCATPAR,ANEVEECC,ANFLGEST,ANLOCALI,ANPROVIN,ANCODCOM,ANCOFISC,ANINDIRI,ANNAZION,ANRITENU,ANFLSGRE";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_RHTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANCAURIT,ANCODFIS,ANPERFIS,ANDESCRI,ANCOGNOM,AN__NOME,AN_SESSO,ANDATNAS,ANLOCNAS,ANPRONAS,ANCATPAR,ANEVEECC,ANFLGEST,ANLOCALI,ANPROVIN,ANCODCOM,ANCOFISC,ANINDIRI,ANNAZION,ANRITENU,ANFLSGRE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RHCODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANCAURIT,ANCODFIS,ANPERFIS,ANDESCRI,ANCOGNOM,AN__NOME,AN_SESSO,ANDATNAS,ANLOCNAS,ANPRONAS,ANCATPAR,ANEVEECC,ANFLGEST,ANLOCALI,ANPROVIN,ANCODCOM,ANCOFISC,ANINDIRI,ANNAZION,ANRITENU,ANFLSGRE";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_RHCODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_RHTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_RHTIPCON;
                       ,'ANCODICE',this.w_RHCODCON)
            select ANTIPCON,ANCODICE,ANCAURIT,ANCODFIS,ANPERFIS,ANDESCRI,ANCOGNOM,AN__NOME,AN_SESSO,ANDATNAS,ANLOCNAS,ANPRONAS,ANCATPAR,ANEVEECC,ANFLGEST,ANLOCALI,ANPROVIN,ANCODCOM,ANCOFISC,ANINDIRI,ANNAZION,ANRITENU,ANFLSGRE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RHCODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_ANCAURIT = NVL(_Link_.ANCAURIT,space(2))
      this.w_ANCODFIS = NVL(_Link_.ANCODFIS,space(16))
      this.w_ANPERFIS = NVL(_Link_.ANPERFIS,space(1))
      this.w_ANDESCRI = NVL(_Link_.ANDESCRI,space(40))
      this.w_ANCOGNOM = NVL(_Link_.ANCOGNOM,space(40))
      this.w_AN__NOME = NVL(_Link_.AN__NOME,space(20))
      this.w_AN_SESSO = NVL(_Link_.AN_SESSO,space(1))
      this.w_ANDATNAS = NVL(cp_ToDate(_Link_.ANDATNAS),ctod("  /  /  "))
      this.w_ANLOCNAS = NVL(_Link_.ANLOCNAS,space(30))
      this.w_ANPRONAS = NVL(_Link_.ANPRONAS,space(2))
      this.w_ANCATPAR = NVL(_Link_.ANCATPAR,space(2))
      this.w_ANEVEECC = NVL(_Link_.ANEVEECC,space(1))
      this.w_ANFLGEST = NVL(_Link_.ANFLGEST,space(1))
      this.w_ANLOCALI = NVL(_Link_.ANLOCALI,space(30))
      this.w_ANPROVIN = NVL(_Link_.ANPROVIN,space(2))
      this.w_ANCODCOM = NVL(_Link_.ANCODCOM,space(4))
      this.w_ANCOFISC = NVL(_Link_.ANCOFISC,space(25))
      this.w_ANINDIRI = NVL(_Link_.ANINDIRI,space(35))
      this.w_ANNAZION = NVL(_Link_.ANNAZION,space(3))
      this.w_ANRITENU = NVL(_Link_.ANRITENU,space(1))
      this.w_ANFLSGRE = NVL(_Link_.ANFLSGRE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_RHCODCON = space(15)
      endif
      this.w_ANCAURIT = space(2)
      this.w_ANCODFIS = space(16)
      this.w_ANPERFIS = space(1)
      this.w_ANDESCRI = space(40)
      this.w_ANCOGNOM = space(40)
      this.w_AN__NOME = space(20)
      this.w_AN_SESSO = space(1)
      this.w_ANDATNAS = ctod("  /  /  ")
      this.w_ANLOCNAS = space(30)
      this.w_ANPRONAS = space(2)
      this.w_ANCATPAR = space(2)
      this.w_ANEVEECC = space(1)
      this.w_ANFLGEST = space(1)
      this.w_ANLOCALI = space(30)
      this.w_ANPROVIN = space(2)
      this.w_ANCODCOM = space(4)
      this.w_ANCOFISC = space(25)
      this.w_ANINDIRI = space(35)
      this.w_ANNAZION = space(3)
      this.w_ANRITENU = space(1)
      this.w_ANFLSGRE = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_ANRITENU $ 'CS'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice fornitore inesistente o non soggetto a ritenute")
        endif
        this.w_RHCODCON = space(15)
        this.w_ANCAURIT = space(2)
        this.w_ANCODFIS = space(16)
        this.w_ANPERFIS = space(1)
        this.w_ANDESCRI = space(40)
        this.w_ANCOGNOM = space(40)
        this.w_AN__NOME = space(20)
        this.w_AN_SESSO = space(1)
        this.w_ANDATNAS = ctod("  /  /  ")
        this.w_ANLOCNAS = space(30)
        this.w_ANPRONAS = space(2)
        this.w_ANCATPAR = space(2)
        this.w_ANEVEECC = space(1)
        this.w_ANFLGEST = space(1)
        this.w_ANLOCALI = space(30)
        this.w_ANPROVIN = space(2)
        this.w_ANCODCOM = space(4)
        this.w_ANCOFISC = space(25)
        this.w_ANINDIRI = space(35)
        this.w_ANNAZION = space(3)
        this.w_ANRITENU = space(1)
        this.w_ANFLSGRE = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RHCODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_5(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 22 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+22<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_5.ANCODICE as ANCODICE105"+ ",link_1_5.ANCAURIT as ANCAURIT105"+ ",link_1_5.ANCODFIS as ANCODFIS105"+ ",link_1_5.ANPERFIS as ANPERFIS105"+ ",link_1_5.ANDESCRI as ANDESCRI105"+ ",link_1_5.ANCOGNOM as ANCOGNOM105"+ ",link_1_5.AN__NOME as AN__NOME105"+ ",link_1_5.AN_SESSO as AN_SESSO105"+ ",link_1_5.ANDATNAS as ANDATNAS105"+ ",link_1_5.ANLOCNAS as ANLOCNAS105"+ ",link_1_5.ANPRONAS as ANPRONAS105"+ ",link_1_5.ANCATPAR as ANCATPAR105"+ ",link_1_5.ANEVEECC as ANEVEECC105"+ ",link_1_5.ANFLGEST as ANFLGEST105"+ ",link_1_5.ANLOCALI as ANLOCALI105"+ ",link_1_5.ANPROVIN as ANPROVIN105"+ ",link_1_5.ANCODCOM as ANCODCOM105"+ ",link_1_5.ANCOFISC as ANCOFISC105"+ ",link_1_5.ANINDIRI as ANINDIRI105"+ ",link_1_5.ANNAZION as ANNAZION105"+ ",link_1_5.ANRITENU as ANRITENU105"+ ",link_1_5.ANFLSGRE as ANFLSGRE105"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_5 on DATESTRH.RHCODCON=link_1_5.ANCODICE"+" and DATESTRH.RHTIPCON=link_1_5.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+22
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_5"
          i_cKey=i_cKey+'+" and DATESTRH.RHCODCON=link_1_5.ANCODICE(+)"'+'+" and DATESTRH.RHTIPCON=link_1_5.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+22
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ANNAZION
  func Link_1_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.NAZIONI_IDX,3]
    i_lTable = "NAZIONI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2], .t., this.NAZIONI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANNAZION) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANNAZION)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NACODNAZ,NACODEST";
                   +" from "+i_cTable+" "+i_lTable+" where NACODNAZ="+cp_ToStrODBC(this.w_ANNAZION);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NACODNAZ',this.w_ANNAZION)
            select NACODNAZ,NACODEST;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANNAZION = NVL(_Link_.NACODNAZ,space(3))
      this.w_NACODEST = NVL(_Link_.NACODEST,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_ANNAZION = space(3)
      endif
      this.w_NACODEST = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])+'\'+cp_ToStr(_Link_.NACODNAZ,1)
      cp_ShowWarn(i_cKey,this.NAZIONI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANNAZION Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RHCODCOM
  func Link_1_63(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ENTI_COM_IDX,3]
    i_lTable = "ENTI_COM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ENTI_COM_IDX,2], .t., this.ENTI_COM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ENTI_COM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RHCODCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('Gscg_Aec',True,'ENTI_COM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ECCODICE like "+cp_ToStrODBC(trim(this.w_RHCODCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select ECCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ECCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ECCODICE',trim(this.w_RHCODCOM))
          select ECCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ECCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RHCODCOM)==trim(_Link_.ECCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RHCODCOM) and !this.bDontReportError
            deferred_cp_zoom('ENTI_COM','*','ECCODICE',cp_AbsName(oSource.parent,'oRHCODCOM_1_63'),i_cWhere,'Gscg_Aec',"Codice enti/comuni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ECCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where ECCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ECCODICE',oSource.xKey(1))
            select ECCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RHCODCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ECCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where ECCODICE="+cp_ToStrODBC(this.w_RHCODCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ECCODICE',this.w_RHCODCOM)
            select ECCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RHCODCOM = NVL(_Link_.ECCODICE,space(4))
    else
      if i_cCtrl<>'Load'
        this.w_RHCODCOM = space(4)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ENTI_COM_IDX,2])+'\'+cp_ToStr(_Link_.ECCODICE,1)
      cp_ShowWarn(i_cKey,this.ENTI_COM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RHCODCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GTSERIAL
  func Link_1_84(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GEDETTEL_IDX,3]
    i_lTable = "GEDETTEL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GEDETTEL_IDX,2], .t., this.GEDETTEL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GEDETTEL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GTSERIAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GTSERIAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GDTIPEST,GDDATEST,GDSERIAL";
                   +" from "+i_cTable+" "+i_lTable+" where GDDATEST="+cp_ToStrODBC(this.w_GTSERIAL);
                   +" and GDTIPEST="+cp_ToStrODBC(this.w_GDTIPEST);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GDTIPEST',this.w_GDTIPEST;
                       ,'GDDATEST',this.w_GTSERIAL)
            select GDTIPEST,GDDATEST,GDSERIAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GTSERIAL = NVL(_Link_.GDDATEST,space(10))
      this.w_GDSERIAL = NVL(_Link_.GDSERIAL,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_GTSERIAL = space(10)
      endif
      this.w_GDSERIAL = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GEDETTEL_IDX,2])+'\'+cp_ToStr(_Link_.GDTIPEST,1)+'\'+cp_ToStr(_Link_.GDDATEST,1)
      cp_ShowWarn(i_cKey,this.GEDETTEL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GTSERIAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oRH__ANNO_1_3.value==this.w_RH__ANNO)
      this.oPgFrm.Page1.oPag.oRH__ANNO_1_3.value=this.w_RH__ANNO
    endif
    if not(this.oPgFrm.Page1.oPag.oRHCODCON_1_5.value==this.w_RHCODCON)
      this.oPgFrm.Page1.oPag.oRHCODCON_1_5.value=this.w_RHCODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oRHPERFIS_1_29.RadioValue()==this.w_RHPERFIS)
      this.oPgFrm.Page1.oPag.oRHPERFIS_1_29.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oRHPEREST_1_30.RadioValue()==this.w_RHPEREST)
      this.oPgFrm.Page1.oPag.oRHPEREST_1_30.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oRHCAUPRE_1_31.RadioValue()==this.w_RHCAUPRE)
      this.oPgFrm.Page1.oPag.oRHCAUPRE_1_31.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oRHTIPOPE_1_32.RadioValue()==this.w_RHTIPOPE)
      this.oPgFrm.Page1.oPag.oRHTIPOPE_1_32.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oRHSCLGEN_1_33.RadioValue()==this.w_RHSCLGEN)
      this.oPgFrm.Page1.oPag.oRHSCLGEN_1_33.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oRHCODFIS_1_34.value==this.w_RHCODFIS)
      this.oPgFrm.Page1.oPag.oRHCODFIS_1_34.value=this.w_RHCODFIS
    endif
    if not(this.oPgFrm.Page1.oPag.oRHDESCRI_1_41.value==this.w_RHDESCRI)
      this.oPgFrm.Page1.oPag.oRHDESCRI_1_41.value=this.w_RHDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oRHCOGNOM_1_42.value==this.w_RHCOGNOM)
      this.oPgFrm.Page1.oPag.oRHCOGNOM_1_42.value=this.w_RHCOGNOM
    endif
    if not(this.oPgFrm.Page1.oPag.oRH__NOME_1_43.value==this.w_RH__NOME)
      this.oPgFrm.Page1.oPag.oRH__NOME_1_43.value=this.w_RH__NOME
    endif
    if not(this.oPgFrm.Page1.oPag.oRH_SESSO_1_47.RadioValue()==this.w_RH_SESSO)
      this.oPgFrm.Page1.oPag.oRH_SESSO_1_47.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oRHDATNAS_1_48.value==this.w_RHDATNAS)
      this.oPgFrm.Page1.oPag.oRHDATNAS_1_48.value=this.w_RHDATNAS
    endif
    if not(this.oPgFrm.Page1.oPag.oRHCOMEST_1_51.value==this.w_RHCOMEST)
      this.oPgFrm.Page1.oPag.oRHCOMEST_1_51.value=this.w_RHCOMEST
    endif
    if not(this.oPgFrm.Page1.oPag.oRHPRONAS_1_53.value==this.w_RHPRONAS)
      this.oPgFrm.Page1.oPag.oRHPRONAS_1_53.value=this.w_RHPRONAS
    endif
    if not(this.oPgFrm.Page1.oPag.oRHEVEECC_1_57.RadioValue()==this.w_RHEVEECC)
      this.oPgFrm.Page1.oPag.oRHEVEECC_1_57.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oRHCATPAR_1_58.RadioValue()==this.w_RHCATPAR)
      this.oPgFrm.Page1.oPag.oRHCATPAR_1_58.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oRHCOMUNE_1_60.value==this.w_RHCOMUNE)
      this.oPgFrm.Page1.oPag.oRHCOMUNE_1_60.value=this.w_RHCOMUNE
    endif
    if not(this.oPgFrm.Page1.oPag.oRHPROVIN_1_61.value==this.w_RHPROVIN)
      this.oPgFrm.Page1.oPag.oRHPROVIN_1_61.value=this.w_RHPROVIN
    endif
    if not(this.oPgFrm.Page1.oPag.oRHCODCOM_1_63.value==this.w_RHCODCOM)
      this.oPgFrm.Page1.oPag.oRHCODCOM_1_63.value=this.w_RHCODCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oRHSTAEST_1_67.value==this.w_RHSTAEST)
      this.oPgFrm.Page1.oPag.oRHSTAEST_1_67.value=this.w_RHSTAEST
    endif
    if not(this.oPgFrm.Page1.oPag.oRHCITEST_1_68.value==this.w_RHCITEST)
      this.oPgFrm.Page1.oPag.oRHCITEST_1_68.value=this.w_RHCITEST
    endif
    if not(this.oPgFrm.Page1.oPag.oRHINDEST_1_69.value==this.w_RHINDEST)
      this.oPgFrm.Page1.oPag.oRHINDEST_1_69.value=this.w_RHINDEST
    endif
    if not(this.oPgFrm.Page1.oPag.oRHCOFISC_1_73.value==this.w_RHCOFISC)
      this.oPgFrm.Page1.oPag.oRHCOFISC_1_73.value=this.w_RHCOFISC
    endif
    if not(this.oPgFrm.Page2.oPag.oAU001004_2_3.value==this.w_AU001004)
      this.oPgFrm.Page2.oPag.oAU001004_2_3.value=this.w_AU001004
    endif
    if not(this.oPgFrm.Page2.oPag.oAU001005_2_6.value==this.w_AU001005)
      this.oPgFrm.Page2.oPag.oAU001005_2_6.value=this.w_AU001005
    endif
    if not(this.oPgFrm.Page2.oPag.oAU001006_2_9.RadioValue()==this.w_AU001006)
      this.oPgFrm.Page2.oPag.oAU001006_2_9.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oAU001008_2_10.value==this.w_AU001008)
      this.oPgFrm.Page2.oPag.oAU001008_2_10.value=this.w_AU001008
    endif
    if not(this.oPgFrm.Page2.oPag.oAU001009_2_12.value==this.w_AU001009)
      this.oPgFrm.Page2.oPag.oAU001009_2_12.value=this.w_AU001009
    endif
    if not(this.oPgFrm.Page2.oPag.oAU001020_2_14.value==this.w_AU001020)
      this.oPgFrm.Page2.oPag.oAU001020_2_14.value=this.w_AU001020
    endif
    if not(this.oPgFrm.Page2.oPag.oAU001021_2_16.value==this.w_AU001021)
      this.oPgFrm.Page2.oPag.oAU001021_2_16.value=this.w_AU001021
    endif
    if not(this.oPgFrm.Page2.oPag.oAU001022_2_20.value==this.w_AU001022)
      this.oPgFrm.Page2.oPag.oAU001022_2_20.value=this.w_AU001022
    endif
    if not(this.oPgFrm.Page2.oPag.oAU001023_2_21.value==this.w_AU001023)
      this.oPgFrm.Page2.oPag.oAU001023_2_21.value=this.w_AU001023
    endif
    if not(this.oPgFrm.Page2.oPag.oAU002004_2_22.value==this.w_AU002004)
      this.oPgFrm.Page2.oPag.oAU002004_2_22.value=this.w_AU002004
    endif
    if not(this.oPgFrm.Page2.oPag.oAU002005_2_24.value==this.w_AU002005)
      this.oPgFrm.Page2.oPag.oAU002005_2_24.value=this.w_AU002005
    endif
    if not(this.oPgFrm.Page2.oPag.oAU002008_2_27.value==this.w_AU002008)
      this.oPgFrm.Page2.oPag.oAU002008_2_27.value=this.w_AU002008
    endif
    if not(this.oPgFrm.Page2.oPag.oAU002009_2_29.value==this.w_AU002009)
      this.oPgFrm.Page2.oPag.oAU002009_2_29.value=this.w_AU002009
    endif
    if not(this.oPgFrm.Page2.oPag.oAU002020_2_31.value==this.w_AU002020)
      this.oPgFrm.Page2.oPag.oAU002020_2_31.value=this.w_AU002020
    endif
    if not(this.oPgFrm.Page2.oPag.oAU002021_2_33.value==this.w_AU002021)
      this.oPgFrm.Page2.oPag.oAU002021_2_33.value=this.w_AU002021
    endif
    if not(this.oPgFrm.Page2.oPag.oAU002022_2_37.value==this.w_AU002022)
      this.oPgFrm.Page2.oPag.oAU002022_2_37.value=this.w_AU002022
    endif
    if not(this.oPgFrm.Page2.oPag.oAU002023_2_38.value==this.w_AU002023)
      this.oPgFrm.Page2.oPag.oAU002023_2_38.value=this.w_AU002023
    endif
    if not(this.oPgFrm.Page2.oPag.oAU002006_2_39.RadioValue()==this.w_AU002006)
      this.oPgFrm.Page2.oPag.oAU002006_2_39.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oAU003004_2_40.value==this.w_AU003004)
      this.oPgFrm.Page2.oPag.oAU003004_2_40.value=this.w_AU003004
    endif
    if not(this.oPgFrm.Page2.oPag.oAU003005_2_42.value==this.w_AU003005)
      this.oPgFrm.Page2.oPag.oAU003005_2_42.value=this.w_AU003005
    endif
    if not(this.oPgFrm.Page2.oPag.oAU003008_2_45.value==this.w_AU003008)
      this.oPgFrm.Page2.oPag.oAU003008_2_45.value=this.w_AU003008
    endif
    if not(this.oPgFrm.Page2.oPag.oAU003009_2_47.value==this.w_AU003009)
      this.oPgFrm.Page2.oPag.oAU003009_2_47.value=this.w_AU003009
    endif
    if not(this.oPgFrm.Page2.oPag.oAU003020_2_49.value==this.w_AU003020)
      this.oPgFrm.Page2.oPag.oAU003020_2_49.value=this.w_AU003020
    endif
    if not(this.oPgFrm.Page2.oPag.oAU003021_2_51.value==this.w_AU003021)
      this.oPgFrm.Page2.oPag.oAU003021_2_51.value=this.w_AU003021
    endif
    if not(this.oPgFrm.Page2.oPag.oAU003022_2_55.value==this.w_AU003022)
      this.oPgFrm.Page2.oPag.oAU003022_2_55.value=this.w_AU003022
    endif
    if not(this.oPgFrm.Page2.oPag.oAU003023_2_56.value==this.w_AU003023)
      this.oPgFrm.Page2.oPag.oAU003023_2_56.value=this.w_AU003023
    endif
    if not(this.oPgFrm.Page2.oPag.oAU003006_2_57.RadioValue()==this.w_AU003006)
      this.oPgFrm.Page2.oPag.oAU003006_2_57.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oRHDAVERI_1_78.RadioValue()==this.w_RHDAVERI)
      this.oPgFrm.Page1.oPag.oRHDAVERI_1_78.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'DATESTRH')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_RHCODCON)) or not(.w_ANRITENU $ 'CS'))  and (.cFunction='Load')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRHCODCON_1_5.SetFocus()
            i_bnoObbl = !empty(.w_RHCODCON)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice fornitore inesistente o non soggetto a ritenute")
          case   (empty(.w_RHCAUPRE))  and (.cFunction='Load')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRHCAUPRE_1_31.SetFocus()
            i_bnoObbl = !empty(.w_RHCAUPRE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_RHCODFIS))  and (.cFunction<>'Query' And .w_RHPEREST<>'S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRHCODFIS_1_34.SetFocus()
            i_bnoObbl = !empty(.w_RHCODFIS)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_RHDESCRI))  and (.cFunction<>'Query' And  .w_RHPERFIS<>'S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRHDESCRI_1_41.SetFocus()
            i_bnoObbl = !empty(.w_RHDESCRI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_RHCOGNOM))  and (.cFunction<>'Query' AND .w_RHPERFIS='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRHCOGNOM_1_42.SetFocus()
            i_bnoObbl = !empty(.w_RHCOGNOM)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_RH__NOME))  and (.cFunction<>'Query' AND .w_RHPERFIS='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRH__NOME_1_43.SetFocus()
            i_bnoObbl = !empty(.w_RH__NOME)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_RH_SESSO))  and (.cFunction<>'Query' AND .w_RHPERFIS='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRH_SESSO_1_47.SetFocus()
            i_bnoObbl = !empty(.w_RH_SESSO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_RHDATNAS))  and (.cFunction<>'Query' AND .w_RHPERFIS='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRHDATNAS_1_48.SetFocus()
            i_bnoObbl = !empty(.w_RHDATNAS)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_RHCOMEST))  and (.cFunction<>'Query' AND .w_RHPERFIS='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRHCOMEST_1_51.SetFocus()
            i_bnoObbl = !empty(.w_RHCOMEST)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_RHEVEECC))  and (.cFunction<>'Query' )
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRHEVEECC_1_57.SetFocus()
            i_bnoObbl = !empty(.w_RHEVEECC)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_RHCOMUNE))  and (.cFunction<>'Query' AND .w_RHPEREST<>'S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRHCOMUNE_1_60.SetFocus()
            i_bnoObbl = !empty(.w_RHCOMUNE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_RHPROVIN))  and (.cFunction<>'Query' AND .w_RHPEREST<>'S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRHPROVIN_1_61.SetFocus()
            i_bnoObbl = !empty(.w_RHPROVIN)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_RHCODCOM))  and (.cFunction<>'Query' AND .w_RHPEREST<>'S' AND .w_RHCAUPRE='N')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRHCODCOM_1_63.SetFocus()
            i_bnoObbl = !empty(.w_RHCODCOM)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_RHSTAEST))  and (.cFunction<>'Query' And .w_RHPEREST='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRHSTAEST_1_67.SetFocus()
            i_bnoObbl = !empty(.w_RHSTAEST)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_RHCOFISC))  and (.cFunction<>'Query' AND .w_RHPEREST='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRHCOFISC_1_73.SetFocus()
            i_bnoObbl = !empty(.w_RHCOFISC)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      *i_bRes = i_bRes .and. .GSRI_MDH.CheckForm()
      if i_bres
        i_bres=  .GSRI_MDH.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=3
        endif
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_RHCODCON = this.w_RHCODCON
    this.o_RHPERFIS = this.w_RHPERFIS
    this.o_RHPEREST = this.w_RHPEREST
    this.o_RHCAUPRE = this.w_RHCAUPRE
    * --- GSRI_MDH : Depends On
    this.GSRI_MDH.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgsri_adhPag1 as StdContainer
  Width  = 852
  height = 559
  stdWidth  = 852
  stdheight = 559
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oRH__ANNO_1_3 as StdField with uid="HYNPMMDCOL",rtseq=3,rtrep=.f.,;
    cFormVar = "w_RH__ANNO", cQueryName = "RH__ANNO",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Anno di riferimento",;
    HelpContextID = 216745829,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=99, Top=18, cSayPict='"9999"', cGetPict='"9999"'

  func oRH__ANNO_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  add object oRHCODCON_1_5 as StdField with uid="EAHXPIIFWI",rtseq=5,rtrep=.f.,;
    cFormVar = "w_RHCODCON", cQueryName = "RHCODCON",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice fornitore inesistente o non soggetto a ritenute",;
    ToolTipText = "Codice conto",;
    HelpContextID = 34178916,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=325, Top=18, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_AFR", oKey_1_1="ANTIPCON", oKey_1_2="this.w_RHTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_RHCODCON"

  func oRHCODCON_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  func oRHCODCON_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oRHCODCON_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRHCODCON_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_RHTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_RHTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oRHCODCON_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFR',"Elenco fornitori",'GSRI_ADH.CONTI_VZM',this.parent.oContained
  endproc
  proc oRHCODCON_1_5.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_RHTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_RHCODCON
     i_obj.ecpSave()
  endproc

  add object oRHPERFIS_1_29 as StdCheck with uid="PJITHOZBWZ",rtseq=29,rtrep=.f.,left=481, top=20, caption="Persona fisica", enabled=.f.,;
    ToolTipText = "Se attivo: il fornitore � una persona fisica",;
    HelpContextID = 169846935,;
    cFormVar="w_RHPERFIS", bObbl = .f. , nPag = 1;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oRHPERFIS_1_29.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oRHPERFIS_1_29.GetRadio()
    this.Parent.oContained.w_RHPERFIS = this.RadioValue()
    return .t.
  endfunc

  func oRHPERFIS_1_29.SetRadio()
    this.Parent.oContained.w_RHPERFIS=trim(this.Parent.oContained.w_RHPERFIS)
    this.value = ;
      iif(this.Parent.oContained.w_RHPERFIS=='S',1,;
      0)
  endfunc

  add object oRHPEREST_1_30 as StdCheck with uid="HGJTZNOYNK",rtseq=30,rtrep=.f.,left=662, top=20, caption="Percipiente estero", enabled=.f.,;
    ToolTipText = "Percipiente estero",;
    HelpContextID = 81811306,;
    cFormVar="w_RHPEREST", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oRHPEREST_1_30.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oRHPEREST_1_30.GetRadio()
    this.Parent.oContained.w_RHPEREST = this.RadioValue()
    return .t.
  endfunc

  func oRHPEREST_1_30.SetRadio()
    this.Parent.oContained.w_RHPEREST=trim(this.Parent.oContained.w_RHPEREST)
    this.value = ;
      iif(this.Parent.oContained.w_RHPEREST=='S',1,;
      0)
  endfunc


  add object oRHCAUPRE_1_31 as StdCombo with uid="WSDCCQWIEQ",rtseq=31,rtrep=.f.,left=325,top=50,width=90,height=22;
    , ToolTipText = "Valore predefinito causale prestazione";
    , HelpContextID = 755547;
    , cFormVar="w_RHCAUPRE",RowSource=""+"Tipo A,"+"Tipo B,"+"Tipo C,"+"Tipo D,"+"Tipo E,"+"Tipo G,"+"Tipo H,"+"Tipo I,"+"Tipo L,"+"Tipo L1,"+"Tipo M,"+"Tipo M1,"+"Tipo N,"+"Tipo O,"+"Tipo O1,"+"Tipo P,"+"Tipo Q,"+"Tipo R,"+"Tipo S,"+"Tipo T,"+"Tipo U,"+"Tipo V,"+"Tipo V1,"+"Tipo W,"+"Tipo X,"+"Tipo Y,"+"Tipo Z", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oRHCAUPRE_1_31.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'B',;
    iif(this.value =3,'C',;
    iif(this.value =4,'D',;
    iif(this.value =5,'E',;
    iif(this.value =6,'G',;
    iif(this.value =7,'H',;
    iif(this.value =8,'I',;
    iif(this.value =9,'L',;
    iif(this.value =10,'L1',;
    iif(this.value =11,'M',;
    iif(this.value =12,'M1',;
    iif(this.value =13,'N',;
    iif(this.value =14,'O',;
    iif(this.value =15,'O1',;
    iif(this.value =16,'P',;
    iif(this.value =17,'Q',;
    iif(this.value =18,'R',;
    iif(this.value =19,'S',;
    iif(this.value =20,'T',;
    iif(this.value =21,'U',;
    iif(this.value =22,'V',;
    iif(this.value =23,'V1',;
    iif(this.value =24,'W',;
    iif(this.value =25,'X',;
    iif(this.value =26,'Y',;
    iif(this.value =27,'Z',;
    space(2)))))))))))))))))))))))))))))
  endfunc
  func oRHCAUPRE_1_31.GetRadio()
    this.Parent.oContained.w_RHCAUPRE = this.RadioValue()
    return .t.
  endfunc

  func oRHCAUPRE_1_31.SetRadio()
    this.Parent.oContained.w_RHCAUPRE=trim(this.Parent.oContained.w_RHCAUPRE)
    this.value = ;
      iif(this.Parent.oContained.w_RHCAUPRE=='A',1,;
      iif(this.Parent.oContained.w_RHCAUPRE=='B',2,;
      iif(this.Parent.oContained.w_RHCAUPRE=='C',3,;
      iif(this.Parent.oContained.w_RHCAUPRE=='D',4,;
      iif(this.Parent.oContained.w_RHCAUPRE=='E',5,;
      iif(this.Parent.oContained.w_RHCAUPRE=='G',6,;
      iif(this.Parent.oContained.w_RHCAUPRE=='H',7,;
      iif(this.Parent.oContained.w_RHCAUPRE=='I',8,;
      iif(this.Parent.oContained.w_RHCAUPRE=='L',9,;
      iif(this.Parent.oContained.w_RHCAUPRE=='L1',10,;
      iif(this.Parent.oContained.w_RHCAUPRE=='M',11,;
      iif(this.Parent.oContained.w_RHCAUPRE=='M1',12,;
      iif(this.Parent.oContained.w_RHCAUPRE=='N',13,;
      iif(this.Parent.oContained.w_RHCAUPRE=='O',14,;
      iif(this.Parent.oContained.w_RHCAUPRE=='O1',15,;
      iif(this.Parent.oContained.w_RHCAUPRE=='P',16,;
      iif(this.Parent.oContained.w_RHCAUPRE=='Q',17,;
      iif(this.Parent.oContained.w_RHCAUPRE=='R',18,;
      iif(this.Parent.oContained.w_RHCAUPRE=='S',19,;
      iif(this.Parent.oContained.w_RHCAUPRE=='T',20,;
      iif(this.Parent.oContained.w_RHCAUPRE=='U',21,;
      iif(this.Parent.oContained.w_RHCAUPRE=='V',22,;
      iif(this.Parent.oContained.w_RHCAUPRE=='V1',23,;
      iif(this.Parent.oContained.w_RHCAUPRE=='W',24,;
      iif(this.Parent.oContained.w_RHCAUPRE=='X',25,;
      iif(this.Parent.oContained.w_RHCAUPRE=='Y',26,;
      iif(this.Parent.oContained.w_RHCAUPRE=='Z',27,;
      0)))))))))))))))))))))))))))
  endfunc

  func oRHCAUPRE_1_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc


  add object oRHTIPOPE_1_32 as StdCombo with uid="AVPSHLDLXQ",rtseq=32,rtrep=.f.,left=514,top=50,width=131,height=22;
    , ToolTipText = "Indica il tipo di operazione nelle dichiarazioni parziali del modello 770";
    , HelpContextID = 247764827;
    , cFormVar="w_RHTIPOPE",RowSource=""+"Ordinario,"+"Aggiornamento,"+"Inserimento,"+"Cancellazione", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oRHTIPOPE_1_32.RadioValue()
    return(iif(this.value =1,'O',;
    iif(this.value =2,'A',;
    iif(this.value =3,'I',;
    iif(this.value =4,'C',;
    space(1))))))
  endfunc
  func oRHTIPOPE_1_32.GetRadio()
    this.Parent.oContained.w_RHTIPOPE = this.RadioValue()
    return .t.
  endfunc

  func oRHTIPOPE_1_32.SetRadio()
    this.Parent.oContained.w_RHTIPOPE=trim(this.Parent.oContained.w_RHTIPOPE)
    this.value = ;
      iif(this.Parent.oContained.w_RHTIPOPE=='O',1,;
      iif(this.Parent.oContained.w_RHTIPOPE=='A',2,;
      iif(this.Parent.oContained.w_RHTIPOPE=='I',3,;
      iif(this.Parent.oContained.w_RHTIPOPE=='C',4,;
      0))))
  endfunc

  func oRHTIPOPE_1_32.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  add object oRHSCLGEN_1_33 as StdCheck with uid="FZBDCCDEUF",rtseq=33,rtrep=.f.,left=662, top=70, caption="Escludi da generazione",;
    ToolTipText = "Se attivo, esclude i dati estratti dalla generazione",;
    HelpContextID = 159479964,;
    cFormVar="w_RHSCLGEN", bObbl = .f. , nPag = 1;
    , tabstop=.f.;
   , bGlobalFont=.t.


  func oRHSCLGEN_1_33.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oRHSCLGEN_1_33.GetRadio()
    this.Parent.oContained.w_RHSCLGEN = this.RadioValue()
    return .t.
  endfunc

  func oRHSCLGEN_1_33.SetRadio()
    this.Parent.oContained.w_RHSCLGEN=trim(this.Parent.oContained.w_RHSCLGEN)
    this.value = ;
      iif(this.Parent.oContained.w_RHSCLGEN=='S',1,;
      0)
  endfunc

  add object oRHCODFIS_1_34 as StdField with uid="BJWDILJOQP",rtseq=34,rtrep=.f.,;
    cFormVar = "w_RHCODFIS", cQueryName = "RHCODFIS",;
    bObbl = .t. , nPag = 1, value=space(16), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale",;
    HelpContextID = 183924887,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=8, Top=146, cSayPict='REPLICATE("!",16)', cGetPict='REPLICATE("!",16)', InputMask=replicate('X',16)

  func oRHCODFIS_1_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' And .w_RHPEREST<>'S')
    endwith
   endif
  endfunc

  add object oRHDESCRI_1_41 as StdField with uid="EPFAFIXABF",rtseq=35,rtrep=.f.,;
    cFormVar = "w_RHDESCRI", cQueryName = "RHDESCRI",;
    bObbl = .t. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Denominazione,ditta o ragione sociale",;
    HelpContextID = 49256287,;
   bGlobalFont=.t.,;
    Height=21, Width=433, Left=8, Top=196, cSayPict="REPL('!',60)", cGetPict="REPL('!',60)", InputMask=replicate('X',60)

  func oRHDESCRI_1_41.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' And  .w_RHPERFIS<>'S')
    endwith
   endif
  endfunc

  add object oRHCOGNOM_1_42 as StdField with uid="VBWHBYPETD",rtseq=36,rtrep=.f.,;
    cFormVar = "w_RHCOGNOM", cQueryName = "RHCOGNOM",;
    bObbl = .t. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Cognome",;
    HelpContextID = 221874019,;
   bGlobalFont=.t.,;
    Height=21, Width=321, Left=8, Top=246, cSayPict="REPL('!',50)", cGetPict="REPL('!',50)", InputMask=replicate('X',50)

  func oRHCOGNOM_1_42.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' AND .w_RHPERFIS='S')
    endwith
   endif
  endfunc

  add object oRH__NOME_1_43 as StdField with uid="TZTOWOZVVJ",rtseq=37,rtrep=.f.,;
    cFormVar = "w_RH__NOME", cQueryName = "RH__NOME",;
    bObbl = .t. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Nome",;
    HelpContextID = 247154523,;
   bGlobalFont=.t.,;
    Height=21, Width=321, Left=345, Top=246, cSayPict="REPL('!',50)", cGetPict="REPL('!',50)", InputMask=replicate('X',50)

  func oRH__NOME_1_43.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' AND .w_RHPERFIS='S')
    endwith
   endif
  endfunc


  add object oRH_SESSO_1_47 as StdCombo with uid="UFMSDHWBVZ",rtseq=38,rtrep=.f.,left=719,top=246,width=91,height=22;
    , HelpContextID = 35604325;
    , cFormVar="w_RH_SESSO",RowSource=""+"Maschio,"+"Femmina", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oRH_SESSO_1_47.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oRH_SESSO_1_47.GetRadio()
    this.Parent.oContained.w_RH_SESSO = this.RadioValue()
    return .t.
  endfunc

  func oRH_SESSO_1_47.SetRadio()
    this.Parent.oContained.w_RH_SESSO=trim(this.Parent.oContained.w_RH_SESSO)
    this.value = ;
      iif(this.Parent.oContained.w_RH_SESSO=='M',1,;
      iif(this.Parent.oContained.w_RH_SESSO=='F',2,;
      0))
  endfunc

  func oRH_SESSO_1_47.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' AND .w_RHPERFIS='S')
    endwith
   endif
  endfunc

  add object oRHDATNAS_1_48 as StdField with uid="VTKSSEXLQD",rtseq=39,rtrep=.f.,;
    cFormVar = "w_RHDATNAS", cQueryName = "RHDATNAS",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di nascita",;
    HelpContextID = 234592105,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=8, Top=296

  func oRHDATNAS_1_48.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' AND .w_RHPERFIS='S')
    endwith
   endif
  endfunc

  add object oRHCOMEST_1_51 as StdField with uid="OEGUAGVHVP",rtseq=40,rtrep=.f.,;
    cFormVar = "w_RHCOMEST", cQueryName = "RHCOMEST",;
    bObbl = .t. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Comune (o stato estero) di nascita",;
    HelpContextID = 77170538,;
   bGlobalFont=.t.,;
    Height=21, Width=321, Left=345, Top=296, cSayPict="REPL('!',30)", cGetPict="REPL('!',30)", InputMask=replicate('X',30), bHasZoom = .t. 

  func oRHCOMEST_1_51.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' AND .w_RHPERFIS='S')
    endwith
   endif
  endfunc

  proc oRHCOMEST_1_51.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C","",".w_RHCOMEST",".w_RHPRONAS")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oRHPRONAS_1_53 as StdField with uid="HCPUHURXUB",rtseq=41,rtrep=.f.,;
    cFormVar = "w_RHPRONAS", cQueryName = "RHPRONAS",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Provincia di nascita",;
    HelpContextID = 230512489,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=719, Top=296, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2), bHasZoom = .t. 

  func oRHPRONAS_1_53.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' AND .w_RHPERFIS='S')
    endwith
   endif
  endfunc

  proc oRHPRONAS_1_53.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"P","","",".w_RHPRONAS")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc


  add object oRHEVEECC_1_57 as StdCombo with uid="KHTCGZBVHM",rtseq=42,rtrep=.f.,left=214,top=345,width=548,height=22;
    , ToolTipText = "Eventi eccezionali";
    , HelpContextID = 69248857;
    , cFormVar="w_RHEVEECC",RowSource=""+"0 - Nessuno,"+"1 - Contribuenti vittime di richieste estorsive ex. Art.20 Co.2 legge n. 44/99,"+"3 - Contribuenti residenti al 12/2/2011 nel comune di Lampedusa e Linosa (migranti Nord Africa),"+"8 - Contribuenti colpiti da altri eventi eccezionali", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oRHEVEECC_1_57.RadioValue()
    return(iif(this.value =1,'0',;
    iif(this.value =2,'1',;
    iif(this.value =3,'3',;
    iif(this.value =4,'8',;
    space(1))))))
  endfunc
  func oRHEVEECC_1_57.GetRadio()
    this.Parent.oContained.w_RHEVEECC = this.RadioValue()
    return .t.
  endfunc

  func oRHEVEECC_1_57.SetRadio()
    this.Parent.oContained.w_RHEVEECC=trim(this.Parent.oContained.w_RHEVEECC)
    this.value = ;
      iif(this.Parent.oContained.w_RHEVEECC=='0',1,;
      iif(this.Parent.oContained.w_RHEVEECC=='1',2,;
      iif(this.Parent.oContained.w_RHEVEECC=='3',3,;
      iif(this.Parent.oContained.w_RHEVEECC=='8',4,;
      0))))
  endfunc

  func oRHEVEECC_1_57.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' )
    endwith
   endif
  endfunc


  add object oRHCATPAR_1_58 as StdCombo with uid="VMSQKVGLON",value=4,rtseq=43,rtrep=.f.,left=8,top=345,width=95,height=22;
    , ToolTipText = "Categorie particolari";
    , HelpContextID = 268142440;
    , cFormVar="w_RHCATPAR",RowSource=""+"Z,"+"Z1,"+"Z2,"+"Non gestito", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oRHCATPAR_1_58.RadioValue()
    return(iif(this.value =1,'Z',;
    iif(this.value =2,'Z1',;
    iif(this.value =3,'Z2',;
    iif(this.value =4,'  ',;
    space(2))))))
  endfunc
  func oRHCATPAR_1_58.GetRadio()
    this.Parent.oContained.w_RHCATPAR = this.RadioValue()
    return .t.
  endfunc

  func oRHCATPAR_1_58.SetRadio()
    this.Parent.oContained.w_RHCATPAR=trim(this.Parent.oContained.w_RHCATPAR)
    this.value = ;
      iif(this.Parent.oContained.w_RHCATPAR=='Z',1,;
      iif(this.Parent.oContained.w_RHCATPAR=='Z1',2,;
      iif(this.Parent.oContained.w_RHCATPAR=='Z2',3,;
      iif(this.Parent.oContained.w_RHCATPAR=='',4,;
      0))))
  endfunc

  func oRHCATPAR_1_58.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' )
    endwith
   endif
  endfunc

  add object oRHCOMUNE_1_60 as StdField with uid="UZXJOQKVMN",rtseq=44,rtrep=.f.,;
    cFormVar = "w_RHCOMUNE", cQueryName = "RHCOMUNE",;
    bObbl = .t. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Comune ",;
    HelpContextID = 77170523,;
   bGlobalFont=.t.,;
    Height=21, Width=230, Left=8, Top=395, cSayPict="REPL('!',30)", cGetPict="REPL('!',30)", InputMask=replicate('X',30), bHasZoom = .t. 

  func oRHCOMUNE_1_60.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' AND .w_RHPEREST<>'S')
    endwith
   endif
  endfunc

  proc oRHCOMUNE_1_60.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C","",".w_RHCOMUNE",".w_RHPROVIN")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oRHPROVIN_1_61 as StdField with uid="WJHWGIYJEO",rtseq=45,rtrep=.f.,;
    cFormVar = "w_RHPROVIN", cQueryName = "RHPROVIN",;
    bObbl = .t. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Provincia",;
    HelpContextID = 172140700,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=345, Top=395, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2), bHasZoom = .t. 

  func oRHPROVIN_1_61.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' AND .w_RHPEREST<>'S')
    endwith
   endif
  endfunc

  proc oRHPROVIN_1_61.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"P","","",".w_RHPROVIN")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oRHCODCOM_1_63 as StdField with uid="JPPHEZDBPD",rtseq=46,rtrep=.f.,;
    cFormVar = "w_RHCODCOM", cQueryName = "RHCODCOM",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Codice comune ",;
    HelpContextID = 34178915,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=481, Top=395, cSayPict='"!!!!"', cGetPict='"!!!!"', InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ENTI_COM", cZoomOnZoom="Gscg_Aec", oKey_1_1="ECCODICE", oKey_1_2="this.w_RHCODCOM"

  func oRHCODCOM_1_63.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' AND .w_RHPEREST<>'S' AND .w_RHCAUPRE='N')
    endwith
   endif
  endfunc

  func oRHCODCOM_1_63.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_63('Part',this)
    endwith
    return bRes
  endfunc

  proc oRHCODCOM_1_63.ecpDrop(oSource)
    this.Parent.oContained.link_1_63('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRHCODCOM_1_63.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ENTI_COM','*','ECCODICE',cp_AbsName(this.parent,'oRHCODCOM_1_63'),iif(empty(i_cWhere),.f.,i_cWhere),'Gscg_Aec',"Codice enti/comuni",'',this.parent.oContained
  endproc
  proc oRHCODCOM_1_63.mZoomOnZoom
    local i_obj
    i_obj=Gscg_Aec()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ECCODICE=this.parent.oContained.w_RHCODCOM
     i_obj.ecpSave()
  endproc

  add object oRHSTAEST_1_67 as StdField with uid="JZJZNJGHQI",rtseq=47,rtrep=.f.,;
    cFormVar = "w_RHSTAEST", cQueryName = "RHSTAEST",nZero=3,;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice stato estero",;
    HelpContextID = 64980842,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=481, Top=527, cSayPict='"999"', cGetPict='"999"', InputMask=replicate('X',3)

  func oRHSTAEST_1_67.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' And .w_RHPEREST='S')
    endwith
   endif
  endfunc

  add object oRHCITEST_1_68 as StdField with uid="VZATVQHIMD",rtseq=48,rtrep=.f.,;
    cFormVar = "w_RHCITEST", cQueryName = "RHCITEST",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Localit� di residenza estera",;
    HelpContextID = 84117354,;
   bGlobalFont=.t.,;
    Height=21, Width=230, Left=481, Top=476, cSayPict='REPLICATE("!",30)', cGetPict='REPLICATE("!",30)', InputMask=replicate('X',30)

  func oRHCITEST_1_68.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' AND .w_RHPEREST='S')
    endwith
   endif
  endfunc

  add object oRHINDEST_1_69 as StdField with uid="PPBQNCJMIR",rtseq=49,rtrep=.f.,;
    cFormVar = "w_RHINDEST", cQueryName = "RHINDEST",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo estero della sede legale",;
    HelpContextID = 67692394,;
   bGlobalFont=.t.,;
    Height=21, Width=321, Left=13, Top=527, cSayPict="REPL('!',35)", cGetPict="REPL('!',35)", InputMask=replicate('X',35)

  func oRHINDEST_1_69.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' And .w_RHPEREST='S')
    endwith
   endif
  endfunc

  add object oRHCOFISC_1_73 as StdField with uid="NEICLLJLQF",rtseq=50,rtrep=.f.,;
    cFormVar = "w_RHCOFISC", cQueryName = "RHCOFISC",;
    bObbl = .t. , nPag = 1, value=space(25), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale",;
    HelpContextID = 136939353,;
   bGlobalFont=.t.,;
    Height=21, Width=202, Left=11, Top=476, cSayPict='REPLICATE("!",25)', cGetPict='REPLICATE("!",25)', InputMask=replicate('X',25)

  func oRHCOFISC_1_73.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' AND .w_RHPEREST='S')
    endwith
   endif
  endfunc

  add object oRHDAVERI_1_78 as StdCheck with uid="CGOCJPGUTC",rtseq=80,rtrep=.f.,left=662, top=45, caption="Da verificare",;
    ToolTipText = "Se attivo, i dati estratti sono da verificare",;
    HelpContextID = 85694303,;
    cFormVar="w_RHDAVERI", bObbl = .f. , nPag = 1;
    , tabstop=.f.;
   , bGlobalFont=.t.


  func oRHDAVERI_1_78.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oRHDAVERI_1_78.GetRadio()
    this.Parent.oContained.w_RHDAVERI = this.RadioValue()
    return .t.
  endfunc

  func oRHDAVERI_1_78.SetRadio()
    this.Parent.oContained.w_RHDAVERI=trim(this.Parent.oContained.w_RHDAVERI)
    this.value = ;
      iif(this.Parent.oContained.w_RHDAVERI=='S',1,;
      0)
  endfunc


  add object oObj_1_80 as cp_runprogram with uid="NBFRWAJSRD",left=175, top=577, width=172,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSRI_BHE(w_HASEVCOP)",;
    cEvent = "HasEvent",;
    nPag=1;
    , ToolTipText = "Richiamato dall'area manuale declare (hascpevent)";
    , HelpContextID = 85070054


  add object oBtn_1_86 as StdButton with uid="WVWNIOAYSH",left=798, top=11, width=48,height=45,;
    CpPicture="BMP\VERIFICA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per accedere alla generazione file 770";
    , HelpContextID = 53697927;
    , tabstop=.f., caption='\<File 770';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_86.Click()
      with this.Parent.oContained
        GSRI_BDH(this.Parent.oContained,"G")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_86.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_GDSERIAL))
      endwith
    endif
  endfunc

  func oBtn_1_86.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (EMPTY(.w_GDSERIAL))
     endwith
    endif
  endfunc

  add object oStr_1_35 as StdString with uid="SJCUFHTJAM",Visible=.t., Left=29, Top=20,;
    Alignment=1, Width=66, Height=18,;
    Caption="Anno:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="DSZQYEUOKT",Visible=.t., Left=303, Top=121,;
    Alignment=0, Width=306, Height=19,;
    Caption="Dati relativi al percipiente delle somme"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_38 as StdString with uid="ELWHFLJCPX",Visible=.t., Left=255, Top=20,;
    Alignment=1, Width=66, Height=18,;
    Caption="Fornitore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="RINPZJSKLJ",Visible=.t., Left=181, Top=52,;
    Alignment=1, Width=140, Height=18,;
    Caption="Causale prestazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="BSYUVARGFL",Visible=.t., Left=11, Top=128,;
    Alignment=0, Width=116, Height=17,;
    Caption="Codice fiscale"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_44 as StdString with uid="OBGOXQVJWR",Visible=.t., Left=11, Top=177,;
    Alignment=0, Width=130, Height=17,;
    Caption="Ragione sociale"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_45 as StdString with uid="KOKISJQUOL",Visible=.t., Left=11, Top=228,;
    Alignment=0, Width=130, Height=17,;
    Caption="Cognome"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_46 as StdString with uid="GZLERJIXDA",Visible=.t., Left=345, Top=232,;
    Alignment=0, Width=76, Height=13,;
    Caption="Nome"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_49 as StdString with uid="ZDKIULOHFL",Visible=.t., Left=11, Top=279,;
    Alignment=0, Width=76, Height=13,;
    Caption="Data di nascita"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_50 as StdString with uid="BBXIREUHWZ",Visible=.t., Left=719, Top=228,;
    Alignment=0, Width=76, Height=17,;
    Caption="Sesso"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_52 as StdString with uid="CTCAOZXFVH",Visible=.t., Left=345, Top=279,;
    Alignment=0, Width=223, Height=13,;
    Caption="Comune (o stato estero) di nascita"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_54 as StdString with uid="DMKPRROUYT",Visible=.t., Left=719, Top=275,;
    Alignment=0, Width=71, Height=17,;
    Caption="Provincia"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_55 as StdString with uid="LDIRJGPIQF",Visible=.t., Left=11, Top=326,;
    Alignment=0, Width=101, Height=17,;
    Caption="Categorie particolari"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_56 as StdString with uid="NXQLRFYAQM",Visible=.t., Left=214, Top=326,;
    Alignment=0, Width=90, Height=17,;
    Caption="Eventi eccezionali"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_59 as StdString with uid="CBIDZKEZDH",Visible=.t., Left=11, Top=378,;
    Alignment=0, Width=76, Height=17,;
    Caption="Comune"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_62 as StdString with uid="ZGFEXDSQJP",Visible=.t., Left=345, Top=378,;
    Alignment=0, Width=71, Height=17,;
    Caption="Provincia"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_64 as StdString with uid="ANHHASVODD",Visible=.t., Left=481, Top=378,;
    Alignment=0, Width=83, Height=17,;
    Caption="Codice comune"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_65 as StdString with uid="JYMPJPJYDE",Visible=.t., Left=303, Top=433,;
    Alignment=0, Width=306, Height=19,;
    Caption="Riservato ai percipienti esteri"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_70 as StdString with uid="NOWYYKZWAZ",Visible=.t., Left=481, Top=511,;
    Alignment=0, Width=83, Height=13,;
    Caption="Stato estero"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_71 as StdString with uid="WQAUIMBAAE",Visible=.t., Left=481, Top=456,;
    Alignment=0, Width=137, Height=17,;
    Caption="Localit� di residenza estera"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_72 as StdString with uid="HGABSVUXLY",Visible=.t., Left=13, Top=507,;
    Alignment=0, Width=97, Height=17,;
    Caption="Via e numero civico"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_74 as StdString with uid="XXZKENNKMI",Visible=.t., Left=11, Top=459,;
    Alignment=0, Width=204, Height=17,;
    Caption="Codice identificativo fiscale"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_76 as StdString with uid="YKWHYQEIBN",Visible=.t., Left=428, Top=52,;
    Alignment=1, Width=82, Height=18,;
    Caption="Tipo operaz.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_77 as StdString with uid="CAMXGTNEUO",Visible=.t., Left=18, Top=52,;
    Alignment=0, Width=162, Height=18,;
    Caption="INSERIMENTO MANUALE"    , ForeColor=RGB(255,0,0);
  ;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_77.mHide()
    with this.Parent.oContained
      return (.w_RHTIPDAT='A')
    endwith
  endfunc

  add object oBox_1_36 as StdBox with uid="RDMWYOEEFE",left=3, top=110, width=846,height=316

  add object oBox_1_66 as StdBox with uid="YLZIQKVHOK",left=3, top=424, width=846,height=133
enddefine
define class tgsri_adhPag2 as StdContainer
  Width  = 852
  height = 559
  stdWidth  = 852
  stdheight = 559
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oAU001004_2_3 as StdField with uid="NACAKQWPDU",rtseq=53,rtrep=.f.,;
    cFormVar = "w_AU001004", cQueryName = "AU001004",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Ammontare lordo corrisposto",;
    HelpContextID = 230253370,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=6, Top=62, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  add object oAU001005_2_6 as StdField with uid="NRGQDADTYC",rtseq=54,rtrep=.f.,;
    cFormVar = "w_AU001005", cQueryName = "AU001005",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Somme non soggette",;
    HelpContextID = 230253371,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=6, Top=112, cSayPict="v_PV(80)", cGetPict="v_GV(80)"


  add object oAU001006_2_9 as StdCombo with uid="GNYVIQEYYH",rtseq=55,rtrep=.f.,left=6,top=161,width=266,height=22, enabled=.f.;
    , ToolTipText = "Codice somme non soggette";
    , HelpContextID = 230253372;
    , cFormVar="w_AU001006",RowSource=""+"1 - Compensi docenti/ricercatori,"+"2 - Lavoratori con beneficio fiscale ,"+"3 - Erogazione di altri redditi non sogg. a ritenuta", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oAU001006_2_9.RadioValue()
    return(iif(this.value =1,'1',;
    iif(this.value =2,'2',;
    iif(this.value =3,'3',;
    ' '))))
  endfunc
  func oAU001006_2_9.GetRadio()
    this.Parent.oContained.w_AU001006 = this.RadioValue()
    return .t.
  endfunc

  func oAU001006_2_9.SetRadio()
    this.Parent.oContained.w_AU001006=trim(this.Parent.oContained.w_AU001006)
    this.value = ;
      iif(this.Parent.oContained.w_AU001006=='1',1,;
      iif(this.Parent.oContained.w_AU001006=='2',2,;
      iif(this.Parent.oContained.w_AU001006=='3',3,;
      0)))
  endfunc

  func oAU001006_2_9.mHide()
    with this.Parent.oContained
      return (.w_AU001005=0 OR (.w_RHPEREST='S' AND .w_ANFLSGRE='S'))
    endwith
  endfunc

  add object oAU001008_2_10 as StdField with uid="CIPRLYFQFW",rtseq=56,rtrep=.f.,;
    cFormVar = "w_AU001008", cQueryName = "AU001008",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Imponibile",;
    HelpContextID = 230253374,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=6, Top=211, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  add object oAU001009_2_12 as StdField with uid="DAANLVFWRU",rtseq=57,rtrep=.f.,;
    cFormVar = "w_AU001009", cQueryName = "AU001009",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Ritenute a titolo di acconto",;
    HelpContextID = 230253375,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=6, Top=261, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  add object oAU001020_2_14 as StdField with uid="PXUBBAYOIT",rtseq=58,rtrep=.f.,;
    cFormVar = "w_AU001020", cQueryName = "AU001020",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Contributi previdenziali a carico del soggetto erogante",;
    HelpContextID = 230253366,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=6, Top=311, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  add object oAU001021_2_16 as StdField with uid="VYCYYSNKAM",rtseq=59,rtrep=.f.,;
    cFormVar = "w_AU001021", cQueryName = "AU001021",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Contributi previdenziali a carico del percipiente",;
    HelpContextID = 230253367,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=6, Top=361, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  add object oAU001022_2_20 as StdField with uid="QLPIMJKEMK",rtseq=60,rtrep=.f.,;
    cFormVar = "w_AU001022", cQueryName = "AU001022",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Spese rimborsate",;
    HelpContextID = 230253368,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=6, Top=411, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  add object oAU001023_2_21 as StdField with uid="JULWOXGRFD",rtseq=61,rtrep=.f.,;
    cFormVar = "w_AU001023", cQueryName = "AU001023",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Ritenute rimborsate",;
    HelpContextID = 230253369,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=6, Top=461, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  add object oAU002004_2_22 as StdField with uid="JVZSCQMGRG",rtseq=62,rtrep=.f.,;
    cFormVar = "w_AU002004", cQueryName = "AU002004",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Ammontare lordo corrisposto",;
    HelpContextID = 231301946,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=296, Top=62, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  func oAU002004_2_22.mHide()
    with this.Parent.oContained
      return (!.w_Record2)
    endwith
  endfunc

  add object oAU002005_2_24 as StdField with uid="XPPMMWLPSZ",rtseq=63,rtrep=.f.,;
    cFormVar = "w_AU002005", cQueryName = "AU002005",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Somme non soggette",;
    HelpContextID = 231301947,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=296, Top=112, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  func oAU002005_2_24.mHide()
    with this.Parent.oContained
      return (!.w_Record2)
    endwith
  endfunc

  add object oAU002008_2_27 as StdField with uid="YQLOQNZUQH",rtseq=64,rtrep=.f.,;
    cFormVar = "w_AU002008", cQueryName = "AU002008",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Imponibile",;
    HelpContextID = 231301950,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=296, Top=211, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  func oAU002008_2_27.mHide()
    with this.Parent.oContained
      return (!.w_Record2)
    endwith
  endfunc

  add object oAU002009_2_29 as StdField with uid="BKEOGZXKGE",rtseq=65,rtrep=.f.,;
    cFormVar = "w_AU002009", cQueryName = "AU002009",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Ritenute a titolo di acconto",;
    HelpContextID = 231301951,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=296, Top=261, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  func oAU002009_2_29.mHide()
    with this.Parent.oContained
      return (!.w_Record2)
    endwith
  endfunc

  add object oAU002020_2_31 as StdField with uid="EVRJRISXDI",rtseq=66,rtrep=.f.,;
    cFormVar = "w_AU002020", cQueryName = "AU002020",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Contributi previdenziali a carico del soggetto erogante",;
    HelpContextID = 231301942,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=296, Top=311, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  func oAU002020_2_31.mHide()
    with this.Parent.oContained
      return (!.w_Record2)
    endwith
  endfunc

  add object oAU002021_2_33 as StdField with uid="UQAOKTUCWX",rtseq=67,rtrep=.f.,;
    cFormVar = "w_AU002021", cQueryName = "AU002021",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Contributi previdenziali a carico del percipiente",;
    HelpContextID = 231301943,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=296, Top=361, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  func oAU002021_2_33.mHide()
    with this.Parent.oContained
      return (!.w_Record2)
    endwith
  endfunc

  add object oAU002022_2_37 as StdField with uid="NTXGOXIKPQ",rtseq=68,rtrep=.f.,;
    cFormVar = "w_AU002022", cQueryName = "AU002022",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Spese rimborsate",;
    HelpContextID = 231301944,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=296, Top=411, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  func oAU002022_2_37.mHide()
    with this.Parent.oContained
      return (!.w_Record2)
    endwith
  endfunc

  add object oAU002023_2_38 as StdField with uid="RLTQQMOPSL",rtseq=69,rtrep=.f.,;
    cFormVar = "w_AU002023", cQueryName = "AU002023",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Ritenute rimborsate",;
    HelpContextID = 231301945,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=296, Top=461, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  func oAU002023_2_38.mHide()
    with this.Parent.oContained
      return (!.w_Record2)
    endwith
  endfunc


  add object oAU002006_2_39 as StdCombo with uid="GIWQGIMGEY",rtseq=70,rtrep=.f.,left=296,top=161,width=266,height=22, enabled=.f.;
    , ToolTipText = "Codice somme non soggette";
    , HelpContextID = 231301948;
    , cFormVar="w_AU002006",RowSource=""+"1 - Compensi docenti/ricercatori,"+"2 - Lavoratori con beneficio fiscale ,"+"3 - Erogazione di altri redditi non sogg. a ritenuta", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oAU002006_2_39.RadioValue()
    return(iif(this.value =1,'1',;
    iif(this.value =2,'2',;
    iif(this.value =3,'3',;
    ' '))))
  endfunc
  func oAU002006_2_39.GetRadio()
    this.Parent.oContained.w_AU002006 = this.RadioValue()
    return .t.
  endfunc

  func oAU002006_2_39.SetRadio()
    this.Parent.oContained.w_AU002006=trim(this.Parent.oContained.w_AU002006)
    this.value = ;
      iif(this.Parent.oContained.w_AU002006=='1',1,;
      iif(this.Parent.oContained.w_AU002006=='2',2,;
      iif(this.Parent.oContained.w_AU002006=='3',3,;
      0)))
  endfunc

  func oAU002006_2_39.mHide()
    with this.Parent.oContained
      return (.w_AU002005=0 Or !.w_Record2)
    endwith
  endfunc

  add object oAU003004_2_40 as StdField with uid="RKRRUUAPET",rtseq=71,rtrep=.f.,;
    cFormVar = "w_AU003004", cQueryName = "AU003004",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Ammontare lordo corrisposto",;
    HelpContextID = 232350522,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=573, Top=62, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  func oAU003004_2_40.mHide()
    with this.Parent.oContained
      return (!.w_Record3)
    endwith
  endfunc

  add object oAU003005_2_42 as StdField with uid="EKGSWXNMCK",rtseq=72,rtrep=.f.,;
    cFormVar = "w_AU003005", cQueryName = "AU003005",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Somme non soggette",;
    HelpContextID = 232350523,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=573, Top=112, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  func oAU003005_2_42.mHide()
    with this.Parent.oContained
      return (!.w_Record3)
    endwith
  endfunc

  add object oAU003008_2_45 as StdField with uid="AVIBMHIGDC",rtseq=73,rtrep=.f.,;
    cFormVar = "w_AU003008", cQueryName = "AU003008",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Imponibile",;
    HelpContextID = 232350526,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=573, Top=211, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  func oAU003008_2_45.mHide()
    with this.Parent.oContained
      return (!.w_Record3)
    endwith
  endfunc

  add object oAU003009_2_47 as StdField with uid="ILYBXARKXD",rtseq=74,rtrep=.f.,;
    cFormVar = "w_AU003009", cQueryName = "AU003009",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Ritenute a titolo di acconto",;
    HelpContextID = 232350527,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=573, Top=261, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  func oAU003009_2_47.mHide()
    with this.Parent.oContained
      return (!.w_Record3)
    endwith
  endfunc

  add object oAU003020_2_49 as StdField with uid="IQFYJFNZXB",rtseq=75,rtrep=.f.,;
    cFormVar = "w_AU003020", cQueryName = "AU003020",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Contributi previdenziali a carico del soggetto erogante",;
    HelpContextID = 232350518,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=573, Top=311, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  func oAU003020_2_49.mHide()
    with this.Parent.oContained
      return (!.w_Record3)
    endwith
  endfunc

  add object oAU003021_2_51 as StdField with uid="SCSOHULGJR",rtseq=76,rtrep=.f.,;
    cFormVar = "w_AU003021", cQueryName = "AU003021",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Contributi previdenziali a carico del percipiente",;
    HelpContextID = 232350519,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=573, Top=361, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  func oAU003021_2_51.mHide()
    with this.Parent.oContained
      return (!.w_Record3)
    endwith
  endfunc

  add object oAU003022_2_55 as StdField with uid="GCLUQTOTHW",rtseq=77,rtrep=.f.,;
    cFormVar = "w_AU003022", cQueryName = "AU003022",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Spese rimborsate",;
    HelpContextID = 232350520,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=573, Top=411, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  func oAU003022_2_55.mHide()
    with this.Parent.oContained
      return (!.w_Record3)
    endwith
  endfunc

  add object oAU003023_2_56 as StdField with uid="KUZLRHPLQF",rtseq=78,rtrep=.f.,;
    cFormVar = "w_AU003023", cQueryName = "AU003023",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Ritenute rimborsate",;
    HelpContextID = 232350521,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=573, Top=461, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  func oAU003023_2_56.mHide()
    with this.Parent.oContained
      return (!.w_Record3)
    endwith
  endfunc


  add object oAU003006_2_57 as StdCombo with uid="UVJGFUQKGX",rtseq=79,rtrep=.f.,left=573,top=161,width=266,height=22, enabled=.f.;
    , ToolTipText = "Codice somme non soggette";
    , HelpContextID = 232350524;
    , cFormVar="w_AU003006",RowSource=""+"1 - Compensi docenti/ricercatori,"+"2 - Lavoratori con beneficio fiscale ,"+"3 - Erogazione di altri redditi non sogg. a ritenuta", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oAU003006_2_57.RadioValue()
    return(iif(this.value =1,'1',;
    iif(this.value =2,'2',;
    iif(this.value =3,'3',;
    ' '))))
  endfunc
  func oAU003006_2_57.GetRadio()
    this.Parent.oContained.w_AU003006 = this.RadioValue()
    return .t.
  endfunc

  func oAU003006_2_57.SetRadio()
    this.Parent.oContained.w_AU003006=trim(this.Parent.oContained.w_AU003006)
    this.value = ;
      iif(this.Parent.oContained.w_AU003006=='1',1,;
      iif(this.Parent.oContained.w_AU003006=='2',2,;
      iif(this.Parent.oContained.w_AU003006=='3',3,;
      0)))
  endfunc

  func oAU003006_2_57.mHide()
    with this.Parent.oContained
      return (.w_AU003005=0 Or !.w_Record3)
    endwith
  endfunc

  add object oStr_2_4 as StdString with uid="YYMJTWBDUY",Visible=.t., Left=6, Top=41,;
    Alignment=0, Width=175, Height=18,;
    Caption="Ammontare lordo corrisposto"  ;
  , bGlobalFont=.t.

  add object oStr_2_5 as StdString with uid="FOWBOZRHVU",Visible=.t., Left=5, Top=10,;
    Alignment=0, Width=306, Height=19,;
    Caption="Dati relativi alle somme erogate"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_7 as StdString with uid="UGJJMMROVZ",Visible=.t., Left=6, Top=94,;
    Alignment=0, Width=160, Height=18,;
    Caption="Somme non soggette"  ;
  , bGlobalFont=.t.

  add object oStr_2_8 as StdString with uid="YHCFGDMUZO",Visible=.t., Left=6, Top=141,;
    Alignment=0, Width=160, Height=18,;
    Caption="Codice"  ;
  , bGlobalFont=.t.

  func oStr_2_8.mHide()
    with this.Parent.oContained
      return (.w_AU001005=0 OR (.w_RHPEREST='S' AND .w_ANFLSGRE='S'))
    endwith
  endfunc

  add object oStr_2_11 as StdString with uid="WZHQHWDYEB",Visible=.t., Left=6, Top=190,;
    Alignment=0, Width=175, Height=18,;
    Caption="Imponibile"  ;
  , bGlobalFont=.t.

  add object oStr_2_13 as StdString with uid="MNYJGJHCOV",Visible=.t., Left=6, Top=240,;
    Alignment=0, Width=175, Height=18,;
    Caption="Ritenute a titolo di acconto"  ;
  , bGlobalFont=.t.

  add object oStr_2_15 as StdString with uid="IBJUCLXHCZ",Visible=.t., Left=6, Top=289,;
    Alignment=0, Width=254, Height=18,;
    Caption="Contributi prev. a carico del sogg. erogante"  ;
  , bGlobalFont=.t.

  add object oStr_2_17 as StdString with uid="RXGVUXAJOG",Visible=.t., Left=6, Top=339,;
    Alignment=0, Width=187, Height=18,;
    Caption="Contributi prev. a carico del perc."  ;
  , bGlobalFont=.t.

  add object oStr_2_18 as StdString with uid="OFYQZUXHVR",Visible=.t., Left=6, Top=391,;
    Alignment=0, Width=173, Height=18,;
    Caption="Spese rimborsate"  ;
  , bGlobalFont=.t.

  add object oStr_2_19 as StdString with uid="KHDZAPVLVZ",Visible=.t., Left=6, Top=441,;
    Alignment=0, Width=173, Height=18,;
    Caption="Ritenute rimborsate"  ;
  , bGlobalFont=.t.

  add object oStr_2_23 as StdString with uid="MWJEJIEKKR",Visible=.t., Left=296, Top=41,;
    Alignment=0, Width=175, Height=18,;
    Caption="Ammontare lordo corrisposto"  ;
  , bGlobalFont=.t.

  func oStr_2_23.mHide()
    with this.Parent.oContained
      return (!.w_Record2)
    endwith
  endfunc

  add object oStr_2_25 as StdString with uid="RIBJBHKEMH",Visible=.t., Left=296, Top=94,;
    Alignment=0, Width=160, Height=18,;
    Caption="Somme non soggette"  ;
  , bGlobalFont=.t.

  func oStr_2_25.mHide()
    with this.Parent.oContained
      return (!.w_Record2)
    endwith
  endfunc

  add object oStr_2_26 as StdString with uid="UZCWPBGEEB",Visible=.t., Left=296, Top=141,;
    Alignment=0, Width=160, Height=18,;
    Caption="Codice"  ;
  , bGlobalFont=.t.

  func oStr_2_26.mHide()
    with this.Parent.oContained
      return (.w_AU002005=0 Or !.w_Record2)
    endwith
  endfunc

  add object oStr_2_28 as StdString with uid="TELVECPJZX",Visible=.t., Left=296, Top=190,;
    Alignment=0, Width=175, Height=18,;
    Caption="Imponibile"  ;
  , bGlobalFont=.t.

  func oStr_2_28.mHide()
    with this.Parent.oContained
      return (!.w_Record2)
    endwith
  endfunc

  add object oStr_2_30 as StdString with uid="PTAIOQPLAQ",Visible=.t., Left=296, Top=240,;
    Alignment=0, Width=175, Height=18,;
    Caption="Ritenute a titolo di acconto"  ;
  , bGlobalFont=.t.

  func oStr_2_30.mHide()
    with this.Parent.oContained
      return (!.w_Record2)
    endwith
  endfunc

  add object oStr_2_32 as StdString with uid="QRLGSRMIWL",Visible=.t., Left=296, Top=289,;
    Alignment=0, Width=254, Height=18,;
    Caption="Contributi prev. a carico del sogg. erogante"  ;
  , bGlobalFont=.t.

  func oStr_2_32.mHide()
    with this.Parent.oContained
      return (!.w_Record2)
    endwith
  endfunc

  add object oStr_2_34 as StdString with uid="RADLOQUTKV",Visible=.t., Left=296, Top=339,;
    Alignment=0, Width=187, Height=18,;
    Caption="Contributi prev. a carico del perc."  ;
  , bGlobalFont=.t.

  func oStr_2_34.mHide()
    with this.Parent.oContained
      return (!.w_Record2)
    endwith
  endfunc

  add object oStr_2_35 as StdString with uid="PACOOPXTEF",Visible=.t., Left=296, Top=391,;
    Alignment=0, Width=173, Height=18,;
    Caption="Spese rimborsate"  ;
  , bGlobalFont=.t.

  func oStr_2_35.mHide()
    with this.Parent.oContained
      return (!.w_Record2)
    endwith
  endfunc

  add object oStr_2_36 as StdString with uid="WKCNRFWJQE",Visible=.t., Left=296, Top=441,;
    Alignment=0, Width=173, Height=18,;
    Caption="Ritenute rimborsate"  ;
  , bGlobalFont=.t.

  func oStr_2_36.mHide()
    with this.Parent.oContained
      return (!.w_Record2)
    endwith
  endfunc

  add object oStr_2_41 as StdString with uid="XULROIOEKM",Visible=.t., Left=573, Top=41,;
    Alignment=0, Width=175, Height=18,;
    Caption="Ammontare lordo corrisposto"  ;
  , bGlobalFont=.t.

  func oStr_2_41.mHide()
    with this.Parent.oContained
      return (!.w_Record3)
    endwith
  endfunc

  add object oStr_2_43 as StdString with uid="TGHDQRRLZT",Visible=.t., Left=573, Top=94,;
    Alignment=0, Width=160, Height=18,;
    Caption="Somme non soggette"  ;
  , bGlobalFont=.t.

  func oStr_2_43.mHide()
    with this.Parent.oContained
      return (!.w_Record3)
    endwith
  endfunc

  add object oStr_2_44 as StdString with uid="EVQUFBXCQO",Visible=.t., Left=573, Top=141,;
    Alignment=0, Width=160, Height=18,;
    Caption="Codice"  ;
  , bGlobalFont=.t.

  func oStr_2_44.mHide()
    with this.Parent.oContained
      return (.w_AU003005=0 Or !.w_Record3)
    endwith
  endfunc

  add object oStr_2_46 as StdString with uid="SSYSITSPGW",Visible=.t., Left=573, Top=190,;
    Alignment=0, Width=175, Height=18,;
    Caption="Imponibile"  ;
  , bGlobalFont=.t.

  func oStr_2_46.mHide()
    with this.Parent.oContained
      return (!.w_Record3)
    endwith
  endfunc

  add object oStr_2_48 as StdString with uid="QIHUOCNZOV",Visible=.t., Left=573, Top=240,;
    Alignment=0, Width=175, Height=18,;
    Caption="Ritenute a titolo di acconto"  ;
  , bGlobalFont=.t.

  func oStr_2_48.mHide()
    with this.Parent.oContained
      return (!.w_Record3)
    endwith
  endfunc

  add object oStr_2_50 as StdString with uid="EYZUTKCCVD",Visible=.t., Left=573, Top=289,;
    Alignment=0, Width=254, Height=18,;
    Caption="Contributi prev. a carico del sogg. erogante"  ;
  , bGlobalFont=.t.

  func oStr_2_50.mHide()
    with this.Parent.oContained
      return (!.w_Record3)
    endwith
  endfunc

  add object oStr_2_52 as StdString with uid="UFJREGCDQQ",Visible=.t., Left=573, Top=339,;
    Alignment=0, Width=187, Height=18,;
    Caption="Contributi prev. a carico del perc."  ;
  , bGlobalFont=.t.

  func oStr_2_52.mHide()
    with this.Parent.oContained
      return (!.w_Record3)
    endwith
  endfunc

  add object oStr_2_53 as StdString with uid="LEQFOYVDBN",Visible=.t., Left=573, Top=391,;
    Alignment=0, Width=173, Height=18,;
    Caption="Spese rimborsate"  ;
  , bGlobalFont=.t.

  func oStr_2_53.mHide()
    with this.Parent.oContained
      return (!.w_Record3)
    endwith
  endfunc

  add object oStr_2_54 as StdString with uid="QBMBEMIVDF",Visible=.t., Left=573, Top=441,;
    Alignment=0, Width=173, Height=18,;
    Caption="Ritenute rimborsate"  ;
  , bGlobalFont=.t.

  func oStr_2_54.mHide()
    with this.Parent.oContained
      return (!.w_Record3)
    endwith
  endfunc
enddefine
define class tgsri_adhPag3 as StdContainer
  Width  = 852
  height = 559
  stdWidth  = 852
  stdheight = 559
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_3_1 as stdDynamicChildContainer with uid="KIVRNWUCAK",left=1, top=14, width=782, height=514, bOnScreen=.t.;

  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gsri_mdh",lower(this.oContained.GSRI_MDH.class))=0
        this.oContained.GSRI_MDH.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsri_adh','DATESTRH','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".RHSERIAL=DATESTRH.RHSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
