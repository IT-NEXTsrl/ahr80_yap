* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_btr                                                        *
*              Controlli in cancellazione                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][98][VRS_6]                                               *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-05-25                                                      *
* Last revis.: 2007-07-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsri_btr",oParentObject)
return(i_retval)

define class tgsri_btr as StdBatch
  * --- Local variables
  w_CODTRI = space(5)
  w_MESS = space(100)
  w_CODAZI = space(5)
  * --- WorkFile variables
  TAB_RITE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Eseguito da GSAR_ATB alla Delete end
    this.w_CODAZI = i_CODAZI
    * --- Select from TAB_RITE
    i_nConn=i_TableProp[this.TAB_RITE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TAB_RITE_idx,2],.t.,this.TAB_RITE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select TRCODTRI  from "+i_cTable+" TAB_RITE ";
          +" where TRCODTRI="+cp_ToStrODBC(this.oParentObject.w_TRCODTRI)+" AND TRCODAZI="+cp_ToStrODBC(this.w_CODAZI)+"";
           ,"_Curs_TAB_RITE")
    else
      select TRCODTRI from (i_cTable);
       where TRCODTRI=this.oParentObject.w_TRCODTRI AND TRCODAZI=this.w_CODAZI;
        into cursor _Curs_TAB_RITE
    endif
    if used('_Curs_TAB_RITE')
      select _Curs_TAB_RITE
      locate for 1=1
      do while not(eof())
      this.w_CODTRI = _Curs_TAB_RITE.TRCODTRI
      if _Curs_TAB_RITE.TRTIPRIT="V"
        this.w_MESS = ah_msgformat("Impossibile cancellare, codice tributo %1 presente nella tabella ritenute subite!",this.w_CODTRI)
      else
        this.w_MESS = ah_msgformat("Impossibile cancellare, codice tributo %1 presente nella tabella ritenute operate!",this.w_CODTRI)
      endif
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=this.w_MESS
        select _Curs_TAB_RITE
        continue
      enddo
      use
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='TAB_RITE'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_TAB_RITE')
      use in _Curs_TAB_RITE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
