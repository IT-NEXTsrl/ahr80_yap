* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_bv2                                                        *
*              Aggiornamento IRPEF                                             *
*                                                                              *
*      Author: Zucchetti spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][98][VRS_13]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-01-10                                                      *
* Last revis.: 2006-01-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsri_bv2",oParentObject)
return(i_retval)

define class tgsri_bv2 as StdBatch
  * --- Local variables
  w_CURS = space(10)
  w_CURS1 = space(10)
  w_CURS2 = space(10)
  w_RIFDISTI = space(10)
  w_SERIALE = space(10)
  w_IRPEF = .f.
  w_INPS = .f.
  w_PREVID = .f.
  * --- WorkFile variables
  VEP_RITE_idx=0
  VEN_RITE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch lanciato dal batch Gscg_Bvi
    * --- Questo batch aggiorna il campo VepverF24 ed il campo Vptipver
    * --- in base alle righe importate nel modello F24 se lanciato dalla pagina Erario.
    * --- Aggiorna solo il campo VepverF24 se lanciato dalla pagina INPS
    * --- Seriale del modello F24
    this.w_IRPEF = This.OparentObject.Oparentobject.w_Importa
    this.w_INPS = This.OparentObject.Oparentobject.w_Impinps
    this.w_PREVID = This.OparentObject.Oparentobject.w_Impprev
    this.w_CURS = This.OparentObject.Oparentobject.w_Genera
    this.w_CURS1 = This.OparentObject.Oparentobject.w_Geninps
    this.w_CURS2 = This.OparentObject.Oparentobject.w_Genprev
    this.w_SERIALE = This.OparentObject.Oparentobject.w_Mfserial
    if this.w_IRPEF=.T.
      Select Vpserial From (this.w_Curs) into cursor Aggiorna group by Vpserial
      Select Aggiorna
      Go Top
      this.w_RIFDISTI = "XXXXXXXXXX"
      Scan
      this.w_RIFDISTI = Aggiorna.Vpserial
      * --- Write into VEP_RITE
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.VEP_RITE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VEP_RITE_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.VEP_RITE_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"VPVERF24 ="+cp_NullLink(cp_ToStrODBC(this.w_SERIALE),'VEP_RITE','VPVERF24');
        +",VPTIPVER ="+cp_NullLink(cp_ToStrODBC("F"),'VEP_RITE','VPTIPVER');
            +i_ccchkf ;
        +" where ";
            +"VPSERIAL = "+cp_ToStrODBC(this.w_RIFDISTI);
               )
      else
        update (i_cTable) set;
            VPVERF24 = this.w_SERIALE;
            ,VPTIPVER = "F";
            &i_ccchkf. ;
         where;
            VPSERIAL = this.w_RIFDISTI;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      Endscan
      if used (this.w_Curs)
        select (this.w_Curs)
        use
      endif
      if used ("AGGIORNA")
        select AGGIORNA
        use
      endif
    endif
    if this.w_INPS=.T.
      Select Vpserial From (this.w_Curs1) into cursor Aggiorna group by Vpserial
      Select Aggiorna
      Go Top
      this.w_RIFDISTI = "XXXXXXXXXX"
      Scan
      this.w_RIFDISTI = Aggiorna.Vpserial
      * --- Write into VEN_RITE
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.VEN_RITE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VEN_RITE_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.VEN_RITE_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"VPVERF24 ="+cp_NullLink(cp_ToStrODBC(this.w_SERIALE),'VEN_RITE','VPVERF24');
            +i_ccchkf ;
        +" where ";
            +"VPSERIAL = "+cp_ToStrODBC(this.w_RIFDISTI);
               )
      else
        update (i_cTable) set;
            VPVERF24 = this.w_SERIALE;
            &i_ccchkf. ;
         where;
            VPSERIAL = this.w_RIFDISTI;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      Endscan
      if used (this.w_Curs1)
        select (this.w_Curs1)
        use
      endif
      if used ("AGGIORNA")
        select AGGIORNA
        use
      endif
    endif
    if this.w_PREVID=.T.
      Select Vpserial From (this.w_Curs2) into cursor Aggiorna group by Vpserial
      Select Aggiorna
      Go Top
      this.w_RIFDISTI = "XXXXXXXXXX"
      Scan
      this.w_RIFDISTI = Aggiorna.Vpserial
      * --- Write into VEN_RITE
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.VEN_RITE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VEN_RITE_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.VEN_RITE_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"VPVERF24 ="+cp_NullLink(cp_ToStrODBC(this.w_SERIALE),'VEN_RITE','VPVERF24');
            +i_ccchkf ;
        +" where ";
            +"VPSERIAL = "+cp_ToStrODBC(this.w_RIFDISTI);
               )
      else
        update (i_cTable) set;
            VPVERF24 = this.w_SERIALE;
            &i_ccchkf. ;
         where;
            VPSERIAL = this.w_RIFDISTI;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      Endscan
      if used (this.w_Curs2)
        select (this.w_Curs2)
        use
      endif
      if used ("AGGIORNA")
        select AGGIORNA
        use
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='VEP_RITE'
    this.cWorkTables[2]='VEN_RITE'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
