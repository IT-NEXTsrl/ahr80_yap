* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri2scs                                                        *
*              Modifica dati                                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-12-21                                                      *
* Last revis.: 2015-03-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsri2scs",oParentObject))

* --- Class definition
define class tgsri2scs as StdForm
  Top    = 3
  Left   = 10

  * --- Standard Properties
  Width  = 768
  Height = 541+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-03-09"
  HelpContextID=93779561
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=37

  * --- Constant Properties
  _IDX = 0
  COC_MAST_IDX = 0
  VALUTE_IDX = 0
  ENTI_COM_IDX = 0
  CONTI_IDX = 0
  cPrg = "gsri2scs"
  cComment = "Modifica dati"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_TIPCAS = space(1)
  w_CONFERMA = .F.
  w_MRTIPCON = space(1)
  w_CDPROTEC = space(17)
  o_CDPROTEC = space(17)
  w_CDPRODOC = 0
  w_CDCODFIS = space(16)
  w_CDPERFIS = space(1)
  w_CDCOGNOM = space(60)
  w_CD__NOME = space(50)
  w_CD_SESSO = space(1)
  w_CDDATNAS = ctod('  /  /  ')
  w_CDCOMNAS = space(30)
  w_CDPRONAS = space(2)
  w_CDCATPAR = space(2)
  w_CDEVEECC = space(1)
  w_CDCOMFIS = space(33)
  w_CDPROFIS = space(2)
  w_CDCOMUNE = space(4)
  w_CDCAUPRE = space(2)
  w_CDAMMCOR = 0
  w_CDSNSOGG = 0
  o_CDSNSOGG = 0
  w_CDCODICE = space(1)
  w_CDIMPONI = 0
  w_CDRITACC = 0
  w_CDSOGERO = 0
  w_CDPERCIP = 0
  w_CDSPERIM = 0
  w_CDRITRIM = 0
  w_ATTMOD1 = space(1)
  o_ATTMOD1 = space(1)
  w_CDPROMOD = 0
  w_CDSNSOGG1 = 0
  o_CDSNSOGG1 = 0
  w_CDCODICE1 = space(1)
  w_ATTMOD2 = space(1)
  o_ATTMOD2 = space(1)
  w_CDFPERCI = space(15)
  w_CODSNS = space(1)
  w_CDSNSOGG2 = 0
  o_CDSNSOGG2 = 0
  w_CDCODICE2 = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsri2scsPag1","gsri2scs",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Record di tipo D")
      .Pages(2).addobject("oPag","tgsri2scsPag2","gsri2scs",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Record di tipo H")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCDPROTEC_1_4
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='COC_MAST'
    this.cWorkTables[2]='VALUTE'
    this.cWorkTables[3]='ENTI_COM'
    this.cWorkTables[4]='CONTI'
    return(this.OpenAllTables(4))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gsri2scs
    This.bUpdated=.T.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIPCAS=space(1)
      .w_CONFERMA=.f.
      .w_MRTIPCON=space(1)
      .w_CDPROTEC=space(17)
      .w_CDPRODOC=0
      .w_CDCODFIS=space(16)
      .w_CDPERFIS=space(1)
      .w_CDCOGNOM=space(60)
      .w_CD__NOME=space(50)
      .w_CD_SESSO=space(1)
      .w_CDDATNAS=ctod("  /  /  ")
      .w_CDCOMNAS=space(30)
      .w_CDPRONAS=space(2)
      .w_CDCATPAR=space(2)
      .w_CDEVEECC=space(1)
      .w_CDCOMFIS=space(33)
      .w_CDPROFIS=space(2)
      .w_CDCOMUNE=space(4)
      .w_CDCAUPRE=space(2)
      .w_CDAMMCOR=0
      .w_CDSNSOGG=0
      .w_CDCODICE=space(1)
      .w_CDIMPONI=0
      .w_CDRITACC=0
      .w_CDSOGERO=0
      .w_CDPERCIP=0
      .w_CDSPERIM=0
      .w_CDRITRIM=0
      .w_ATTMOD1=space(1)
      .w_CDPROMOD=0
      .w_CDSNSOGG1=0
      .w_CDCODICE1=space(1)
      .w_ATTMOD2=space(1)
      .w_CDFPERCI=space(15)
      .w_CODSNS=space(1)
      .w_CDSNSOGG2=0
      .w_CDCODICE2=space(1)
      .w_TIPCAS=oParentObject.w_TIPCAS
      .w_CONFERMA=oParentObject.w_CONFERMA
      .w_CDPROTEC=oParentObject.w_CDPROTEC
      .w_CDPRODOC=oParentObject.w_CDPRODOC
      .w_CDCODFIS=oParentObject.w_CDCODFIS
      .w_CDPERFIS=oParentObject.w_CDPERFIS
      .w_CDCOGNOM=oParentObject.w_CDCOGNOM
      .w_CD__NOME=oParentObject.w_CD__NOME
      .w_CD_SESSO=oParentObject.w_CD_SESSO
      .w_CDDATNAS=oParentObject.w_CDDATNAS
      .w_CDCOMNAS=oParentObject.w_CDCOMNAS
      .w_CDPRONAS=oParentObject.w_CDPRONAS
      .w_CDCATPAR=oParentObject.w_CDCATPAR
      .w_CDEVEECC=oParentObject.w_CDEVEECC
      .w_CDCOMFIS=oParentObject.w_CDCOMFIS
      .w_CDPROFIS=oParentObject.w_CDPROFIS
      .w_CDCOMUNE=oParentObject.w_CDCOMUNE
      .w_CDCAUPRE=oParentObject.w_CDCAUPRE
      .w_CDAMMCOR=oParentObject.w_CDAMMCOR
      .w_CDSNSOGG=oParentObject.w_CDSNSOGG
      .w_CDCODICE=oParentObject.w_CDCODICE
      .w_CDIMPONI=oParentObject.w_CDIMPONI
      .w_CDRITACC=oParentObject.w_CDRITACC
      .w_CDSOGERO=oParentObject.w_CDSOGERO
      .w_CDPERCIP=oParentObject.w_CDPERCIP
      .w_CDSPERIM=oParentObject.w_CDSPERIM
      .w_CDRITRIM=oParentObject.w_CDRITRIM
      .w_ATTMOD1=oParentObject.w_ATTMOD1
      .w_CDPROMOD=oParentObject.w_CDPROMOD
      .w_CDSNSOGG1=oParentObject.w_CDSNSOGG1
      .w_CDCODICE1=oParentObject.w_CDCODICE1
      .w_ATTMOD2=oParentObject.w_ATTMOD2
      .w_CDFPERCI=oParentObject.w_CDFPERCI
      .w_CDSNSOGG2=oParentObject.w_CDSNSOGG2
      .w_CDCODICE2=oParentObject.w_CDCODICE2
          .DoRTCalc(1,1,.f.)
        .w_CONFERMA = .t.
        .w_MRTIPCON = 'F'
        .DoRTCalc(4,18,.f.)
        if not(empty(.w_CDCOMUNE))
          .link_1_39('Full')
        endif
        .DoRTCalc(19,34,.f.)
        if not(empty(.w_CDFPERCI))
          .link_1_40('Full')
        endif
    endwith
    this.DoRTCalc(35,37,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_6.enabled = this.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsri2scs
      endproc
      proc ecpQuit()
      if ah_yesno('Attenzione, chiudendo la maschera%0nessun dato verr� salvato.%0Si vuole continuare?')
        * --- Move without activating controls
        this.cFunction='Filter'
        this.__dummy__.enabled=.t.
        this.__dummy__.Setfocus()
        if !this.OkToQuit()
          this.__dummy__.enabled=.f.
          this.cFunction = "Edit"
          this.oFirstControl.SetFocus()
          return
        endif
            this.Hide()
            this.NotifyEvent("Edit Aborted")
            this.NotifyEvent("Done")
            this.Release()
     endif
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_TIPCAS=.w_TIPCAS
      .oParentObject.w_CONFERMA=.w_CONFERMA
      .oParentObject.w_CDPROTEC=.w_CDPROTEC
      .oParentObject.w_CDPRODOC=.w_CDPRODOC
      .oParentObject.w_CDCODFIS=.w_CDCODFIS
      .oParentObject.w_CDPERFIS=.w_CDPERFIS
      .oParentObject.w_CDCOGNOM=.w_CDCOGNOM
      .oParentObject.w_CD__NOME=.w_CD__NOME
      .oParentObject.w_CD_SESSO=.w_CD_SESSO
      .oParentObject.w_CDDATNAS=.w_CDDATNAS
      .oParentObject.w_CDCOMNAS=.w_CDCOMNAS
      .oParentObject.w_CDPRONAS=.w_CDPRONAS
      .oParentObject.w_CDCATPAR=.w_CDCATPAR
      .oParentObject.w_CDEVEECC=.w_CDEVEECC
      .oParentObject.w_CDCOMFIS=.w_CDCOMFIS
      .oParentObject.w_CDPROFIS=.w_CDPROFIS
      .oParentObject.w_CDCOMUNE=.w_CDCOMUNE
      .oParentObject.w_CDCAUPRE=.w_CDCAUPRE
      .oParentObject.w_CDAMMCOR=.w_CDAMMCOR
      .oParentObject.w_CDSNSOGG=.w_CDSNSOGG
      .oParentObject.w_CDCODICE=.w_CDCODICE
      .oParentObject.w_CDIMPONI=.w_CDIMPONI
      .oParentObject.w_CDRITACC=.w_CDRITACC
      .oParentObject.w_CDSOGERO=.w_CDSOGERO
      .oParentObject.w_CDPERCIP=.w_CDPERCIP
      .oParentObject.w_CDSPERIM=.w_CDSPERIM
      .oParentObject.w_CDRITRIM=.w_CDRITRIM
      .oParentObject.w_ATTMOD1=.w_ATTMOD1
      .oParentObject.w_CDPROMOD=.w_CDPROMOD
      .oParentObject.w_CDSNSOGG1=.w_CDSNSOGG1
      .oParentObject.w_CDCODICE1=.w_CDCODICE1
      .oParentObject.w_ATTMOD2=.w_ATTMOD2
      .oParentObject.w_CDFPERCI=.w_CDFPERCI
      .oParentObject.w_CDSNSOGG2=.w_CDSNSOGG2
      .oParentObject.w_CDCODICE2=.w_CDCODICE2
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
            .w_CONFERMA = .t.
        .DoRTCalc(3,4,.t.)
        if .o_Cdprotec<>.w_Cdprotec
            .w_CDPRODOC = 0
        endif
        .DoRTCalc(6,21,.t.)
        if .o_CDSNSOGG<>.w_CDSNSOGG
            .w_CDCODICE = iif(.w_Cdsnsogg<>0,iif(Not Empty(.w_Cdcodice),.w_Cdcodice,iif(Not Empty(.w_Codsns),.w_Codsns,'3')),' ')
        endif
        .DoRTCalc(23,28,.t.)
        if .o_Cdsnsogg1<>.w_Cdsnsogg1
            .w_ATTMOD1 = iif(.w_Cdsnsogg1<>0,.w_Attmod1,'N')
        endif
        .DoRTCalc(30,30,.t.)
        if .o_Attmod1<>.w_Attmod1
            .w_CDSNSOGG1 = iif(.w_Attmod1='S',.w_Cdsnsogg1,0)
        endif
        if .o_Attmod1<>.w_Attmod1.or. .o_Cdsnsogg1<>.w_Cdsnsogg1
            .w_CDCODICE1 = iif(.w_Attmod1='S' And .w_Cdsnsogg1<>0,iif(Not Empty(.w_Cdcodice1),.w_Cdcodice1,iif(Not Empty(.w_Codsns),.w_Codsns,'3')),' ')
        endif
        if .o_Cdsnsogg2<>.w_Cdsnsogg2
            .w_ATTMOD2 = iif(.w_Cdsnsogg2<>0,.w_Attmod2,'N')
        endif
          .link_1_40('Full')
        .DoRTCalc(35,35,.t.)
        if .o_Attmod2<>.w_Attmod2
            .w_CDSNSOGG2 = iif(.w_Attmod2='S',.w_Cdsnsogg2,0)
        endif
        if .o_Attmod2<>.w_Attmod2.or. .o_Cdsnsogg2<>.w_Cdsnsogg2
            .w_CDCODICE2 = iif(.w_Attmod2='S' And .w_Cdsnsogg2<>0,iif(Not Empty(.w_Cdcodice2),.w_Cdcodice2,iif(Not Empty(.w_Codsns),.w_Codsns,'3')),' ')
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCD__NOME_1_19.enabled = this.oPgFrm.Page1.oPag.oCD__NOME_1_19.mCond()
    this.oPgFrm.Page1.oPag.oCD_SESSO_1_21.enabled = this.oPgFrm.Page1.oPag.oCD_SESSO_1_21.mCond()
    this.oPgFrm.Page1.oPag.oCDDATNAS_1_23.enabled = this.oPgFrm.Page1.oPag.oCDDATNAS_1_23.mCond()
    this.oPgFrm.Page1.oPag.oCDCOMNAS_1_25.enabled = this.oPgFrm.Page1.oPag.oCDCOMNAS_1_25.mCond()
    this.oPgFrm.Page1.oPag.oCDPRONAS_1_27.enabled = this.oPgFrm.Page1.oPag.oCDPRONAS_1_27.mCond()
    this.oPgFrm.Page2.oPag.oCDSNSOGG1_2_26.enabled = this.oPgFrm.Page2.oPag.oCDSNSOGG1_2_26.mCond()
    this.oPgFrm.Page2.oPag.oCDSNSOGG2_2_33.enabled = this.oPgFrm.Page2.oPag.oCDSNSOGG2_2_33.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_6.enabled = this.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCDPROTEC_1_4.visible=!this.oPgFrm.Page1.oPag.oCDPROTEC_1_4.mHide()
    this.oPgFrm.Page1.oPag.oCDPRODOC_1_5.visible=!this.oPgFrm.Page1.oPag.oCDPRODOC_1_5.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_8.visible=!this.oPgFrm.Page1.oPag.oStr_1_8.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_9.visible=!this.oPgFrm.Page1.oPag.oStr_1_9.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_14.visible=!this.oPgFrm.Page1.oPag.oStr_1_14.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_16.visible=!this.oPgFrm.Page1.oPag.oStr_1_16.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_3.visible=!this.oPgFrm.Page2.oPag.oStr_2_3.mHide()
    this.oPgFrm.Page2.oPag.oCDCODICE_2_14.visible=!this.oPgFrm.Page2.oPag.oCDCODICE_2_14.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_24.visible=!this.oPgFrm.Page2.oPag.oStr_2_24.mHide()
    this.oPgFrm.Page2.oPag.oCDCODICE1_2_27.visible=!this.oPgFrm.Page2.oPag.oCDCODICE1_2_27.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_31.visible=!this.oPgFrm.Page2.oPag.oStr_2_31.mHide()
    this.oPgFrm.Page2.oPag.oCDCODICE2_2_34.visible=!this.oPgFrm.Page2.oPag.oCDCODICE2_2_34.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CDCOMUNE
  func Link_1_39(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ENTI_COM_IDX,3]
    i_lTable = "ENTI_COM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ENTI_COM_IDX,2], .t., this.ENTI_COM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ENTI_COM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CDCOMUNE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_AEC',True,'ENTI_COM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ECCODICE like "+cp_ToStrODBC(trim(this.w_CDCOMUNE)+"%");

          i_ret=cp_SQL(i_nConn,"select ECCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ECCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ECCODICE',trim(this.w_CDCOMUNE))
          select ECCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ECCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CDCOMUNE)==trim(_Link_.ECCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CDCOMUNE) and !this.bDontReportError
            deferred_cp_zoom('ENTI_COM','*','ECCODICE',cp_AbsName(oSource.parent,'oCDCOMUNE_1_39'),i_cWhere,'GSCG_AEC',"CODICE ENTI/COMUNI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ECCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where ECCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ECCODICE',oSource.xKey(1))
            select ECCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CDCOMUNE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ECCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where ECCODICE="+cp_ToStrODBC(this.w_CDCOMUNE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ECCODICE',this.w_CDCOMUNE)
            select ECCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CDCOMUNE = NVL(_Link_.ECCODICE,space(4))
    else
      if i_cCtrl<>'Load'
        this.w_CDCOMUNE = space(4)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ENTI_COM_IDX,2])+'\'+cp_ToStr(_Link_.ECCODICE,1)
      cp_ShowWarn(i_cKey,this.ENTI_COM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CDCOMUNE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CDFPERCI
  func Link_1_40(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CDFPERCI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CDFPERCI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANCODSNS";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CDFPERCI);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_MRTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_MRTIPCON;
                       ,'ANCODICE',this.w_CDFPERCI)
            select ANTIPCON,ANCODICE,ANCODSNS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CDFPERCI = NVL(_Link_.ANCODICE,space(15))
      this.w_CODSNS = NVL(_Link_.ANCODSNS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CDFPERCI = space(15)
      endif
      this.w_CODSNS = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CDFPERCI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCDPROTEC_1_4.value==this.w_CDPROTEC)
      this.oPgFrm.Page1.oPag.oCDPROTEC_1_4.value=this.w_CDPROTEC
    endif
    if not(this.oPgFrm.Page1.oPag.oCDPRODOC_1_5.value==this.w_CDPRODOC)
      this.oPgFrm.Page1.oPag.oCDPRODOC_1_5.value=this.w_CDPRODOC
    endif
    if not(this.oPgFrm.Page1.oPag.oCDCODFIS_1_13.value==this.w_CDCODFIS)
      this.oPgFrm.Page1.oPag.oCDCODFIS_1_13.value=this.w_CDCODFIS
    endif
    if not(this.oPgFrm.Page1.oPag.oCDCOGNOM_1_17.value==this.w_CDCOGNOM)
      this.oPgFrm.Page1.oPag.oCDCOGNOM_1_17.value=this.w_CDCOGNOM
    endif
    if not(this.oPgFrm.Page1.oPag.oCD__NOME_1_19.value==this.w_CD__NOME)
      this.oPgFrm.Page1.oPag.oCD__NOME_1_19.value=this.w_CD__NOME
    endif
    if not(this.oPgFrm.Page1.oPag.oCD_SESSO_1_21.RadioValue()==this.w_CD_SESSO)
      this.oPgFrm.Page1.oPag.oCD_SESSO_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCDDATNAS_1_23.value==this.w_CDDATNAS)
      this.oPgFrm.Page1.oPag.oCDDATNAS_1_23.value=this.w_CDDATNAS
    endif
    if not(this.oPgFrm.Page1.oPag.oCDCOMNAS_1_25.value==this.w_CDCOMNAS)
      this.oPgFrm.Page1.oPag.oCDCOMNAS_1_25.value=this.w_CDCOMNAS
    endif
    if not(this.oPgFrm.Page1.oPag.oCDPRONAS_1_27.value==this.w_CDPRONAS)
      this.oPgFrm.Page1.oPag.oCDPRONAS_1_27.value=this.w_CDPRONAS
    endif
    if not(this.oPgFrm.Page1.oPag.oCDCATPAR_1_29.RadioValue()==this.w_CDCATPAR)
      this.oPgFrm.Page1.oPag.oCDCATPAR_1_29.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCDEVEECC_1_30.RadioValue()==this.w_CDEVEECC)
      this.oPgFrm.Page1.oPag.oCDEVEECC_1_30.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCDCOMFIS_1_35.value==this.w_CDCOMFIS)
      this.oPgFrm.Page1.oPag.oCDCOMFIS_1_35.value=this.w_CDCOMFIS
    endif
    if not(this.oPgFrm.Page1.oPag.oCDPROFIS_1_37.value==this.w_CDPROFIS)
      this.oPgFrm.Page1.oPag.oCDPROFIS_1_37.value=this.w_CDPROFIS
    endif
    if not(this.oPgFrm.Page1.oPag.oCDCOMUNE_1_39.value==this.w_CDCOMUNE)
      this.oPgFrm.Page1.oPag.oCDCOMUNE_1_39.value=this.w_CDCOMUNE
    endif
    if not(this.oPgFrm.Page2.oPag.oCDCAUPRE_2_11.RadioValue()==this.w_CDCAUPRE)
      this.oPgFrm.Page2.oPag.oCDCAUPRE_2_11.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCDAMMCOR_2_12.value==this.w_CDAMMCOR)
      this.oPgFrm.Page2.oPag.oCDAMMCOR_2_12.value=this.w_CDAMMCOR
    endif
    if not(this.oPgFrm.Page2.oPag.oCDSNSOGG_2_13.value==this.w_CDSNSOGG)
      this.oPgFrm.Page2.oPag.oCDSNSOGG_2_13.value=this.w_CDSNSOGG
    endif
    if not(this.oPgFrm.Page2.oPag.oCDCODICE_2_14.RadioValue()==this.w_CDCODICE)
      this.oPgFrm.Page2.oPag.oCDCODICE_2_14.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCDIMPONI_2_15.value==this.w_CDIMPONI)
      this.oPgFrm.Page2.oPag.oCDIMPONI_2_15.value=this.w_CDIMPONI
    endif
    if not(this.oPgFrm.Page2.oPag.oCDRITACC_2_16.value==this.w_CDRITACC)
      this.oPgFrm.Page2.oPag.oCDRITACC_2_16.value=this.w_CDRITACC
    endif
    if not(this.oPgFrm.Page2.oPag.oCDSOGERO_2_17.value==this.w_CDSOGERO)
      this.oPgFrm.Page2.oPag.oCDSOGERO_2_17.value=this.w_CDSOGERO
    endif
    if not(this.oPgFrm.Page2.oPag.oCDPERCIP_2_18.value==this.w_CDPERCIP)
      this.oPgFrm.Page2.oPag.oCDPERCIP_2_18.value=this.w_CDPERCIP
    endif
    if not(this.oPgFrm.Page2.oPag.oCDSPERIM_2_19.value==this.w_CDSPERIM)
      this.oPgFrm.Page2.oPag.oCDSPERIM_2_19.value=this.w_CDSPERIM
    endif
    if not(this.oPgFrm.Page2.oPag.oCDRITRIM_2_20.value==this.w_CDRITRIM)
      this.oPgFrm.Page2.oPag.oCDRITRIM_2_20.value=this.w_CDRITRIM
    endif
    if not(this.oPgFrm.Page2.oPag.oATTMOD1_2_21.RadioValue()==this.w_ATTMOD1)
      this.oPgFrm.Page2.oPag.oATTMOD1_2_21.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCDPROMOD_2_23.value==this.w_CDPROMOD)
      this.oPgFrm.Page2.oPag.oCDPROMOD_2_23.value=this.w_CDPROMOD
    endif
    if not(this.oPgFrm.Page2.oPag.oCDSNSOGG1_2_26.value==this.w_CDSNSOGG1)
      this.oPgFrm.Page2.oPag.oCDSNSOGG1_2_26.value=this.w_CDSNSOGG1
    endif
    if not(this.oPgFrm.Page2.oPag.oCDCODICE1_2_27.RadioValue()==this.w_CDCODICE1)
      this.oPgFrm.Page2.oPag.oCDCODICE1_2_27.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oATTMOD2_2_28.RadioValue()==this.w_ATTMOD2)
      this.oPgFrm.Page2.oPag.oATTMOD2_2_28.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCDSNSOGG2_2_33.value==this.w_CDSNSOGG2)
      this.oPgFrm.Page2.oPag.oCDSNSOGG2_2_33.value=this.w_CDSNSOGG2
    endif
    if not(this.oPgFrm.Page2.oPag.oCDCODICE2_2_34.RadioValue()==this.w_CDCODICE2)
      this.oPgFrm.Page2.oPag.oCDCODICE2_2_34.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_CDPROTEC)) or not(Val(.w_Cdprotec)<>0))  and not(.w_TIPCAS='O')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCDPROTEC_1_4.SetFocus()
            i_bnoObbl = !empty(.w_CDPROTEC)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CDPRODOC))  and not(.w_TIPCAS='O')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCDPRODOC_1_5.SetFocus()
            i_bnoObbl = !empty(.w_CDPRODOC)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CDCODFIS))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCDCODFIS_1_13.SetFocus()
            i_bnoObbl = !empty(.w_CDCODFIS)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CDCOGNOM))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCDCOGNOM_1_17.SetFocus()
            i_bnoObbl = !empty(.w_CDCOGNOM)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CD__NOME))  and (.w_Cdperfis='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCD__NOME_1_19.SetFocus()
            i_bnoObbl = !empty(.w_CD__NOME)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CD_SESSO))  and (.w_Cdperfis='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCD_SESSO_1_21.SetFocus()
            i_bnoObbl = !empty(.w_CD_SESSO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CDDATNAS))  and (.w_Cdperfis='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCDDATNAS_1_23.SetFocus()
            i_bnoObbl = !empty(.w_CDDATNAS)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CDCOMNAS))  and (.w_Cdperfis='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCDCOMNAS_1_25.SetFocus()
            i_bnoObbl = !empty(.w_CDCOMNAS)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CDPROTEC = this.w_CDPROTEC
    this.o_CDSNSOGG = this.w_CDSNSOGG
    this.o_ATTMOD1 = this.w_ATTMOD1
    this.o_CDSNSOGG1 = this.w_CDSNSOGG1
    this.o_ATTMOD2 = this.w_ATTMOD2
    this.o_CDSNSOGG2 = this.w_CDSNSOGG2
    return

enddefine

* --- Define pages as container
define class tgsri2scsPag1 as StdContainer
  Width  = 764
  height = 541
  stdWidth  = 764
  stdheight = 541
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCDPROTEC_1_4 as StdField with uid="XFZXBWGJXS",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CDPROTEC", cQueryName = "CDPROTEC",;
    bObbl = .t. , nPag = 1, value=space(17), bMultilanguage =  .f.,;
    ToolTipText = "Identificativo dell'invio",;
    HelpContextID = 61887081,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=160, Top=33, cSayPict='"99999999999999999"', cGetPict='"99999999999999999"', InputMask=replicate('X',17), Alignment=1

  func oCDPROTEC_1_4.mHide()
    with this.Parent.oContained
      return (.w_TIPCAS='O')
    endwith
  endfunc

  func oCDPROTEC_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (Val(.w_Cdprotec)<>0)
    endwith
    return bRes
  endfunc

  add object oCDPRODOC_1_5 as StdField with uid="BVQNORKVAD",rtseq=5,rtrep=.f.,;
    cFormVar = "w_CDPRODOC", cQueryName = "CDPRODOC",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Progressivo attribuito dal servizio telematico alla singola C.U.",;
    HelpContextID = 206548375,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=318, Top=33, cSayPict='"999999"', cGetPict='"999999"'

  func oCDPRODOC_1_5.mHide()
    with this.Parent.oContained
      return (.w_TIPCAS='O')
    endwith
  endfunc


  add object oBtn_1_6 as StdButton with uid="JNTOFUXNOR",left=647, top=492, width=48,height=45,;
    CpPicture="BMP\ok.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per aggiornare";
    , HelpContextID = 265601814;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_6.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_6.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_Tipcas='O' Or (Not Empty(.w_Cdprotec) And Not Empty(.w_Cdprodoc)))
      endwith
    endif
  endfunc


  add object oBtn_1_7 as StdButton with uid="RGOUFEOBUS",left=700, top=492, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 265601814;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_7.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oCDCODFIS_1_13 as StdField with uid="EJHDJJARFO",rtseq=6,rtrep=.f.,;
    cFormVar = "w_CDCODFIS", cQueryName = "CDCODFIS",;
    bObbl = .t. , nPag = 1, value=space(16), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale",;
    HelpContextID = 83657337,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=160, Top=117, cSayPict='REPL("!",16)', cGetPict='REPL("!",16)', InputMask=replicate('X',16)

  add object oCDCOGNOM_1_17 as StdField with uid="BTDRQKMQUT",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CDCOGNOM", cQueryName = "CDCOGNOM",;
    bObbl = .t. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Cognome o denominazione",;
    HelpContextID = 47414669,;
   bGlobalFont=.t.,;
    Height=21, Width=433, Left=160, Top=151, InputMask=replicate('X',60)

  add object oCD__NOME_1_19 as StdField with uid="LAICDVCLNL",rtseq=9,rtrep=.f.,;
    cFormVar = "w_CD__NOME", cQueryName = "CD__NOME",;
    bObbl = .t. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Nome",;
    HelpContextID = 22134165,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=160, Top=185, InputMask=replicate('X',50)

  func oCD__NOME_1_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Cdperfis='S')
    endwith
   endif
  endfunc


  add object oCD_SESSO_1_21 as StdCombo with uid="EJWWFZQGVI",rtseq=10,rtrep=.f.,left=676,top=184,width=77,height=22;
    , ToolTipText = "Sesso";
    , HelpContextID = 34751093;
    , cFormVar="w_CD_SESSO",RowSource=""+"Maschio,"+"Femmina", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oCD_SESSO_1_21.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oCD_SESSO_1_21.GetRadio()
    this.Parent.oContained.w_CD_SESSO = this.RadioValue()
    return .t.
  endfunc

  func oCD_SESSO_1_21.SetRadio()
    this.Parent.oContained.w_CD_SESSO=trim(this.Parent.oContained.w_CD_SESSO)
    this.value = ;
      iif(this.Parent.oContained.w_CD_SESSO=='M',1,;
      iif(this.Parent.oContained.w_CD_SESSO=='F',2,;
      0))
  endfunc

  func oCD_SESSO_1_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Cdperfis='S')
    endwith
   endif
  endfunc

  add object oCDDATNAS_1_23 as StdField with uid="EAEKXWPQUL",rtseq=11,rtrep=.f.,;
    cFormVar = "w_CDDATNAS", cQueryName = "CDDATNAS",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di nascita",;
    HelpContextID = 34696583,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=160, Top=232

  func oCDDATNAS_1_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Cdperfis='S')
    endwith
   endif
  endfunc

  add object oCDCOMNAS_1_25 as StdField with uid="JCIKSGHZZV",rtseq=12,rtrep=.f.,;
    cFormVar = "w_CDCOMNAS", cQueryName = "CDCOMNAS",;
    bObbl = .t. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Comune o ( Stato estero di nascita)",;
    HelpContextID = 41123207,;
   bGlobalFont=.t.,;
    Height=21, Width=237, Left=362, Top=232, InputMask=replicate('X',30), bHasZoom = .t. 

  func oCDCOMNAS_1_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Cdperfis='S')
    endwith
   endif
  endfunc

  proc oCDCOMNAS_1_25.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C","",".w_Cdcomnas",".w_Cdpronas")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oCDPRONAS_1_27 as StdField with uid="DUEWLLBPIO",rtseq=13,rtrep=.f.,;
    cFormVar = "w_CDPRONAS", cQueryName = "CDPRONAS",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Provincia di nascita",;
    HelpContextID = 38776199,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=676, Top=231, InputMask=replicate('X',2), bHasZoom = .t. 

  func oCDPRONAS_1_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Cdperfis='S')
    endwith
   endif
  endfunc

  proc oCDPRONAS_1_27.mZoom
      with this.Parent.oContained
        Gsar_Bxc(this.Parent.oContained,"P","","",".w_Cdpronas")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc


  add object oCDCATPAR_1_29 as StdCombo with uid="EXGDXQTPFM",value=29,rtseq=14,rtrep=.f.,left=160,top=282,width=92,height=22;
    , ToolTipText = "Categorie particolare";
    , HelpContextID = 1146248;
    , cFormVar="w_CDCATPAR",RowSource=""+"A,"+"B,"+"C,"+"D,"+"E,"+"F,"+"G,"+"H,"+"K,"+"L,"+"M,"+"N,"+"P,"+"Q,"+"R,"+"S,"+"T,"+"T1,"+"T2,"+"T3,"+"T4,"+"U,"+"V,"+"W,"+"Y,"+"Z,"+"Z2,"+"Z3,"+"Non gestito", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCDCATPAR_1_29.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'B',;
    iif(this.value =3,'C',;
    iif(this.value =4,'D',;
    iif(this.value =5,'E',;
    iif(this.value =6,'F',;
    iif(this.value =7,'G',;
    iif(this.value =8,'H',;
    iif(this.value =9,'K',;
    iif(this.value =10,'L',;
    iif(this.value =11,'M',;
    iif(this.value =12,'N',;
    iif(this.value =13,'P',;
    iif(this.value =14,'Q',;
    iif(this.value =15,'R',;
    iif(this.value =16,'S',;
    iif(this.value =17,'T',;
    iif(this.value =18,'T1',;
    iif(this.value =19,'T2',;
    iif(this.value =20,'T3',;
    iif(this.value =21,'T4',;
    iif(this.value =22,'U',;
    iif(this.value =23,'V',;
    iif(this.value =24,'W',;
    iif(this.value =25,'Y',;
    iif(this.value =26,'Z',;
    iif(this.value =27,'Z2',;
    iif(this.value =28,'Z3',;
    iif(this.value =29,'  ',;
    space(2)))))))))))))))))))))))))))))))
  endfunc
  func oCDCATPAR_1_29.GetRadio()
    this.Parent.oContained.w_CDCATPAR = this.RadioValue()
    return .t.
  endfunc

  func oCDCATPAR_1_29.SetRadio()
    this.Parent.oContained.w_CDCATPAR=trim(this.Parent.oContained.w_CDCATPAR)
    this.value = ;
      iif(this.Parent.oContained.w_CDCATPAR=='A',1,;
      iif(this.Parent.oContained.w_CDCATPAR=='B',2,;
      iif(this.Parent.oContained.w_CDCATPAR=='C',3,;
      iif(this.Parent.oContained.w_CDCATPAR=='D',4,;
      iif(this.Parent.oContained.w_CDCATPAR=='E',5,;
      iif(this.Parent.oContained.w_CDCATPAR=='F',6,;
      iif(this.Parent.oContained.w_CDCATPAR=='G',7,;
      iif(this.Parent.oContained.w_CDCATPAR=='H',8,;
      iif(this.Parent.oContained.w_CDCATPAR=='K',9,;
      iif(this.Parent.oContained.w_CDCATPAR=='L',10,;
      iif(this.Parent.oContained.w_CDCATPAR=='M',11,;
      iif(this.Parent.oContained.w_CDCATPAR=='N',12,;
      iif(this.Parent.oContained.w_CDCATPAR=='P',13,;
      iif(this.Parent.oContained.w_CDCATPAR=='Q',14,;
      iif(this.Parent.oContained.w_CDCATPAR=='R',15,;
      iif(this.Parent.oContained.w_CDCATPAR=='S',16,;
      iif(this.Parent.oContained.w_CDCATPAR=='T',17,;
      iif(this.Parent.oContained.w_CDCATPAR=='T1',18,;
      iif(this.Parent.oContained.w_CDCATPAR=='T2',19,;
      iif(this.Parent.oContained.w_CDCATPAR=='T3',20,;
      iif(this.Parent.oContained.w_CDCATPAR=='T4',21,;
      iif(this.Parent.oContained.w_CDCATPAR=='U',22,;
      iif(this.Parent.oContained.w_CDCATPAR=='V',23,;
      iif(this.Parent.oContained.w_CDCATPAR=='W',24,;
      iif(this.Parent.oContained.w_CDCATPAR=='Y',25,;
      iif(this.Parent.oContained.w_CDCATPAR=='Z',26,;
      iif(this.Parent.oContained.w_CDCATPAR=='Z2',27,;
      iif(this.Parent.oContained.w_CDCATPAR=='Z3',28,;
      iif(this.Parent.oContained.w_CDCATPAR=='',29,;
      0)))))))))))))))))))))))))))))
  endfunc


  add object oCDEVEECC_1_30 as StdCombo with uid="DRSSLYTMGC",rtseq=15,rtrep=.f.,left=160,top=334,width=515,height=22;
    , ToolTipText = "Eventi eccezionali";
    , HelpContextID = 68395625;
    , cFormVar="w_CDEVEECC",RowSource=""+"0 - Nessuno,"+"1 - Contribuenti vittime di richieste estorsive ex. Art.20 Co.2 legge n. 44/99,"+"3 - Contribuenti residenti al 12/2/2011 nel comune di Lampedusa e Linosa (migranti Nord Africa),"+"8 - Contribuenti colpiti da altri eventi eccezionali", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCDEVEECC_1_30.RadioValue()
    return(iif(this.value =1,'0',;
    iif(this.value =2,'1',;
    iif(this.value =3,'3',;
    iif(this.value =4,'8',;
    space(1))))))
  endfunc
  func oCDEVEECC_1_30.GetRadio()
    this.Parent.oContained.w_CDEVEECC = this.RadioValue()
    return .t.
  endfunc

  func oCDEVEECC_1_30.SetRadio()
    this.Parent.oContained.w_CDEVEECC=trim(this.Parent.oContained.w_CDEVEECC)
    this.value = ;
      iif(this.Parent.oContained.w_CDEVEECC=='0',1,;
      iif(this.Parent.oContained.w_CDEVEECC=='1',2,;
      iif(this.Parent.oContained.w_CDEVEECC=='3',3,;
      iif(this.Parent.oContained.w_CDEVEECC=='8',4,;
      0))))
  endfunc

  add object oCDCOMFIS_1_35 as StdField with uid="AUPFFWNNYN",rtseq=16,rtrep=.f.,;
    cFormVar = "w_CDCOMFIS", cQueryName = "CDCOMFIS",;
    bObbl = .f. , nPag = 1, value=space(33), bMultilanguage =  .f.,;
    ToolTipText = "Comune",;
    HelpContextID = 93094521,;
   bGlobalFont=.t.,;
    Height=21, Width=244, Left=160, Top=434, InputMask=replicate('X',33), bHasZoom = .t. 

  proc oCDCOMFIS_1_35.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C"," ",".w_Cdcomfis",".w_Cdprofis")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oCDPROFIS_1_37 as StdField with uid="ICVDAIFJVD",rtseq=17,rtrep=.f.,;
    cFormVar = "w_CDPROFIS", cQueryName = "CDPROFIS",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Provincia",;
    HelpContextID = 95441529,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=499, Top=434, InputMask=replicate('X',2), bHasZoom = .t. 

  proc oCDPROFIS_1_37.mZoom
      with this.Parent.oContained
        Gsar_Bxc(this.Parent.oContained,"P","","",".w_Cdprofis")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oCDCOMUNE_1_39 as StdField with uid="XIFTZFDBLT",rtseq=18,rtrep=.f.,;
    cFormVar = "w_CDCOMUNE", cQueryName = "CDCOMUNE",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Codice comune",;
    HelpContextID = 192118165,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=676, Top=434, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ENTI_COM", cZoomOnZoom="GSCG_AEC", oKey_1_1="ECCODICE", oKey_1_2="this.w_CDCOMUNE"

  func oCDCOMUNE_1_39.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_39('Part',this)
    endwith
    return bRes
  endfunc

  proc oCDCOMUNE_1_39.ecpDrop(oSource)
    this.Parent.oContained.link_1_39('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCDCOMUNE_1_39.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ENTI_COM','*','ECCODICE',cp_AbsName(this.parent,'oCDCOMUNE_1_39'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_AEC',"CODICE ENTI/COMUNI",'',this.parent.oContained
  endproc
  proc oCDCOMUNE_1_39.mZoomOnZoom
    local i_obj
    i_obj=GSCG_AEC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ECCODICE=this.parent.oContained.w_CDCOMUNE
     i_obj.ecpSave()
  endproc

  add object oStr_1_8 as StdString with uid="BMLKAAVFQY",Visible=.t., Left=3, Top=36,;
    Alignment=1, Width=152, Height=18,;
    Caption="Identificativo dell'invio:"  ;
  , bGlobalFont=.t.

  func oStr_1_8.mHide()
    with this.Parent.oContained
      return (.w_TIPCAS='O')
    endwith
  endfunc

  add object oStr_1_9 as StdString with uid="RKSOLNRTSH",Visible=.t., Left=305, Top=34,;
    Alignment=0, Width=12, Height=18,;
    Caption="-"  ;
  , bGlobalFont=.t.

  func oStr_1_9.mHide()
    with this.Parent.oContained
      return (.w_TIPCAS='O')
    endwith
  endfunc

  add object oStr_1_10 as StdString with uid="LAZNWDOOFN",Visible=.t., Left=3, Top=82,;
    Alignment=0, Width=202, Height=18,;
    Caption="Dati anagrafici del percipiente"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_12 as StdString with uid="YRJKCBLGZR",Visible=.t., Left=31, Top=120,;
    Alignment=1, Width=124, Height=18,;
    Caption="Codice fiscale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="ZZZNBEYOJB",Visible=.t., Left=39, Top=154,;
    Alignment=1, Width=116, Height=18,;
    Caption="Denominazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_14.mHide()
    with this.Parent.oContained
      return (.w_Cdperfis='S')
    endwith
  endfunc

  add object oStr_1_16 as StdString with uid="BOYUVGOQSG",Visible=.t., Left=64, Top=154,;
    Alignment=1, Width=91, Height=18,;
    Caption="Cognome:"  ;
  , bGlobalFont=.t.

  func oStr_1_16.mHide()
    with this.Parent.oContained
      return (.w_Cdperfis<>'S')
    endwith
  endfunc

  add object oStr_1_18 as StdString with uid="OAWIPUIYYU",Visible=.t., Left=97, Top=187,;
    Alignment=1, Width=58, Height=18,;
    Caption="Nome:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="BODPPQLYPO",Visible=.t., Left=591, Top=187,;
    Alignment=1, Width=81, Height=18,;
    Caption="Sesso:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="TESNAHSQNS",Visible=.t., Left=43, Top=233,;
    Alignment=1, Width=112, Height=18,;
    Caption="Data di nascita:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="ABCSRERQVX",Visible=.t., Left=261, Top=233,;
    Alignment=1, Width=96, Height=18,;
    Caption="Comune:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="OZEZIIZOXG",Visible=.t., Left=604, Top=235,;
    Alignment=1, Width=68, Height=18,;
    Caption="Provincia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="YVFFJNTNBQ",Visible=.t., Left=18, Top=284,;
    Alignment=1, Width=137, Height=18,;
    Caption="Categorie particolari:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="XJWYBCMSUW",Visible=.t., Left=18, Top=336,;
    Alignment=1, Width=137, Height=18,;
    Caption="Eventi eccezionali:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="RDZHCUPZYO",Visible=.t., Left=0, Top=399,;
    Alignment=0, Width=202, Height=18,;
    Caption="Domicilio fiscale"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_34 as StdString with uid="OHKCDQIXTC",Visible=.t., Left=77, Top=436,;
    Alignment=1, Width=78, Height=18,;
    Caption="Comune:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="POABSZUQLR",Visible=.t., Left=411, Top=436,;
    Alignment=1, Width=83, Height=18,;
    Caption="Provincia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="SGLEJTINOP",Visible=.t., Left=559, Top=436,;
    Alignment=1, Width=113, Height=18,;
    Caption="Codice comune:"  ;
  , bGlobalFont=.t.

  add object oBox_1_11 as StdBox with uid="JYBJJJGNRF",left=2, top=99, width=761,height=2

  add object oBox_1_33 as StdBox with uid="SEGDSOULJE",left=2, top=416, width=761,height=2
enddefine
define class tgsri2scsPag2 as StdContainer
  Width  = 764
  height = 541
  stdWidth  = 764
  stdheight = 541
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oCDCAUPRE_2_11 as StdCombo with uid="NAMBPADIOB",rtseq=19,rtrep=.f.,left=320,top=19,width=94,height=22;
    , ToolTipText = "Causale prestazione";
    , HelpContextID = 268337771;
    , cFormVar="w_CDCAUPRE",RowSource=""+"Tipo A,"+"Tipo B,"+"Tipo C,"+"Tipo D,"+"Tipo E,"+"Tipo G,"+"Tipo H,"+"Tipo I,"+"Tipo L,"+"Tipo L1,"+"Tipo M,"+"Tipo M1,"+"Tipo N,"+"Tipo O,"+"Tipo O1,"+"Tipo P,"+"Tipo Q,"+"Tipo R,"+"Tipo S,"+"Tipo T,"+"Tipo U,"+"Tipo V,"+"Tipo V1,"+"Tipo W,"+"Tipo X,"+"Tipo Y,"+"Tipo Z", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oCDCAUPRE_2_11.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'B',;
    iif(this.value =3,'C',;
    iif(this.value =4,'D',;
    iif(this.value =5,'E',;
    iif(this.value =6,'G',;
    iif(this.value =7,'H',;
    iif(this.value =8,'I',;
    iif(this.value =9,'L',;
    iif(this.value =10,'L1',;
    iif(this.value =11,'M',;
    iif(this.value =12,'M1',;
    iif(this.value =13,'N',;
    iif(this.value =14,'O',;
    iif(this.value =15,'O1',;
    iif(this.value =16,'P',;
    iif(this.value =17,'Q',;
    iif(this.value =18,'R',;
    iif(this.value =19,'S',;
    iif(this.value =20,'T',;
    iif(this.value =21,'U',;
    iif(this.value =22,'V',;
    iif(this.value =23,'V1',;
    iif(this.value =24,'W',;
    iif(this.value =25,'X',;
    iif(this.value =26,'Y',;
    iif(this.value =27,'Z',;
    space(2)))))))))))))))))))))))))))))
  endfunc
  func oCDCAUPRE_2_11.GetRadio()
    this.Parent.oContained.w_CDCAUPRE = this.RadioValue()
    return .t.
  endfunc

  func oCDCAUPRE_2_11.SetRadio()
    this.Parent.oContained.w_CDCAUPRE=trim(this.Parent.oContained.w_CDCAUPRE)
    this.value = ;
      iif(this.Parent.oContained.w_CDCAUPRE=='A',1,;
      iif(this.Parent.oContained.w_CDCAUPRE=='B',2,;
      iif(this.Parent.oContained.w_CDCAUPRE=='C',3,;
      iif(this.Parent.oContained.w_CDCAUPRE=='D',4,;
      iif(this.Parent.oContained.w_CDCAUPRE=='E',5,;
      iif(this.Parent.oContained.w_CDCAUPRE=='G',6,;
      iif(this.Parent.oContained.w_CDCAUPRE=='H',7,;
      iif(this.Parent.oContained.w_CDCAUPRE=='I',8,;
      iif(this.Parent.oContained.w_CDCAUPRE=='L',9,;
      iif(this.Parent.oContained.w_CDCAUPRE=='L1',10,;
      iif(this.Parent.oContained.w_CDCAUPRE=='M',11,;
      iif(this.Parent.oContained.w_CDCAUPRE=='M1',12,;
      iif(this.Parent.oContained.w_CDCAUPRE=='N',13,;
      iif(this.Parent.oContained.w_CDCAUPRE=='O',14,;
      iif(this.Parent.oContained.w_CDCAUPRE=='O1',15,;
      iif(this.Parent.oContained.w_CDCAUPRE=='P',16,;
      iif(this.Parent.oContained.w_CDCAUPRE=='Q',17,;
      iif(this.Parent.oContained.w_CDCAUPRE=='R',18,;
      iif(this.Parent.oContained.w_CDCAUPRE=='S',19,;
      iif(this.Parent.oContained.w_CDCAUPRE=='T',20,;
      iif(this.Parent.oContained.w_CDCAUPRE=='U',21,;
      iif(this.Parent.oContained.w_CDCAUPRE=='V',22,;
      iif(this.Parent.oContained.w_CDCAUPRE=='V1',23,;
      iif(this.Parent.oContained.w_CDCAUPRE=='W',24,;
      iif(this.Parent.oContained.w_CDCAUPRE=='X',25,;
      iif(this.Parent.oContained.w_CDCAUPRE=='Y',26,;
      iif(this.Parent.oContained.w_CDCAUPRE=='Z',27,;
      0)))))))))))))))))))))))))))
  endfunc

  add object oCDAMMCOR_2_12 as StdField with uid="OJTWKFWFLH",rtseq=20,rtrep=.f.,;
    cFormVar = "w_CDAMMCOR", cQueryName = "CDAMMCOR",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Ammontare lordo corrisposto",;
    HelpContextID = 225811848,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=320, Top=51, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  add object oCDSNSOGG_2_13 as StdField with uid="ICTONQOLFV",rtseq=21,rtrep=.f.,;
    cFormVar = "w_CDSNSOGG", cQueryName = "CDSNSOGG",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Altre somme non soggette a ritenuta",;
    HelpContextID = 250380909,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=320, Top=84, cSayPict="v_PV(80)", cGetPict="v_GV(80)"


  add object oCDCODICE_2_14 as StdCombo with uid="EGRUZJYCNF",rtseq=22,rtrep=.f.,left=320,top=113,width=414,height=22;
    , ToolTipText = "Codice somme non soggette";
    , HelpContextID = 133988971;
    , cFormVar="w_CDCODICE",RowSource=""+"1 - Compensi docenti/ricercatori Legge n. 2 del 28 gennaio 2009,"+"2 - Lavoratori con beneficio fiscale ex art. 3 Legge 30 dicembre 2010/238,"+"3 - Erogazione di altri redditi non soggetti a ritenuta", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oCDCODICE_2_14.RadioValue()
    return(iif(this.value =1,'1',;
    iif(this.value =2,'2',;
    iif(this.value =3,'3',;
    ' '))))
  endfunc
  func oCDCODICE_2_14.GetRadio()
    this.Parent.oContained.w_CDCODICE = this.RadioValue()
    return .t.
  endfunc

  func oCDCODICE_2_14.SetRadio()
    this.Parent.oContained.w_CDCODICE=trim(this.Parent.oContained.w_CDCODICE)
    this.value = ;
      iif(this.Parent.oContained.w_CDCODICE=='1',1,;
      iif(this.Parent.oContained.w_CDCODICE=='2',2,;
      iif(this.Parent.oContained.w_CDCODICE=='3',3,;
      0)))
  endfunc

  func oCDCODICE_2_14.mHide()
    with this.Parent.oContained
      return (.w_Cdsnsogg=0)
    endwith
  endfunc

  add object oCDIMPONI_2_15 as StdField with uid="TTHVRNAZJI",rtseq=23,rtrep=.f.,;
    cFormVar = "w_CDIMPONI", cQueryName = "CDIMPONI",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Imponibile",;
    HelpContextID = 21306769,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=320, Top=146, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  add object oCDRITACC_2_16 as StdField with uid="AHVJWSKEHW",rtseq=24,rtrep=.f.,;
    cFormVar = "w_CDRITACC", cQueryName = "CDRITACC",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Ritenute a titolo di acconto",;
    HelpContextID = 16216681,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=320, Top=178, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  add object oCDSOGERO_2_17 as StdField with uid="PBVFOUFQRI",rtseq=25,rtrep=.f.,;
    cFormVar = "w_CDSOGERO", cQueryName = "CDSOGERO",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Contributi previdenziali a carico del soggetto erogante",;
    HelpContextID = 70091381,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=320, Top=210, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  add object oCDPERCIP_2_18 as StdField with uid="GOEVPLLMTP",rtseq=26,rtrep=.f.,;
    cFormVar = "w_CDPERCIP", cQueryName = "CDPERCIP",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Contributi previdenziali a carico del percipiente",;
    HelpContextID = 47403638,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=320, Top=242, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  add object oCDSPERIM_2_19 as StdField with uid="EAMAAVJDHB",rtseq=27,rtrep=.f.,;
    cFormVar = "w_CDSPERIM", cQueryName = "CDSPERIM",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Spese rimborsate",;
    HelpContextID = 250707341,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=320, Top=274, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  add object oCDRITRIM_2_20 as StdField with uid="OMMGMJMZOD",rtseq=28,rtrep=.f.,;
    cFormVar = "w_CDRITRIM", cQueryName = "CDRITRIM",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Ritenute rimborsate",;
    HelpContextID = 235441549,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=320, Top=306, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  add object oATTMOD1_2_21 as StdCheck with uid="ENKZRAPRIW",rtseq=29,rtrep=.f.,left=9, top=358, caption="Nuovo modulo ",;
    ToolTipText = "Se attivo, consente di inserire i dati della relativa sezione",;
    HelpContextID = 206855674,;
    cFormVar="w_ATTMOD1", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oATTMOD1_2_21.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oATTMOD1_2_21.GetRadio()
    this.Parent.oContained.w_ATTMOD1 = this.RadioValue()
    return .t.
  endfunc

  func oATTMOD1_2_21.SetRadio()
    this.Parent.oContained.w_ATTMOD1=trim(this.Parent.oContained.w_ATTMOD1)
    this.value = ;
      iif(this.Parent.oContained.w_ATTMOD1=='S',1,;
      0)
  endfunc

  add object oCDPROMOD_2_23 as StdField with uid="XBNWSPFETS",rtseq=30,rtrep=.f.,;
    cFormVar = "w_CDPROMOD", cQueryName = "CDPROMOD",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    HelpContextID = 55553430,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=70, Top=22, cSayPict='"999999999"', cGetPict='"999999999"'

  add object oCDSNSOGG1_2_26 as StdField with uid="TBFRTZPJHX",rtseq=31,rtrep=.f.,;
    cFormVar = "w_CDSNSOGG1", cQueryName = "CDSNSOGG1",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Altre somme non soggette a ritenuta",;
    HelpContextID = 250381693,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=320, Top=383, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  func oCDSNSOGG1_2_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Attmod1='S')
    endwith
   endif
  endfunc


  add object oCDCODICE1_2_27 as StdCombo with uid="GRNRJGDLSR",rtseq=32,rtrep=.f.,left=320,top=420,width=414,height=22;
    , ToolTipText = "Codice somme non soggette";
    , HelpContextID = 133989755;
    , cFormVar="w_CDCODICE1",RowSource=""+"1 - Compensi docenti/ricercatori Legge n. 2 del 28 gennaio 2009,"+"2 - Lavoratori con beneficio fiscale ex art. 3 Legge 30 dicembre 2010/238,"+"3 - Erogazione di altri redditi non soggetti a ritenuta", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oCDCODICE1_2_27.RadioValue()
    return(iif(this.value =1,'1',;
    iif(this.value =2,'2',;
    iif(this.value =3,'3',;
    ' '))))
  endfunc
  func oCDCODICE1_2_27.GetRadio()
    this.Parent.oContained.w_CDCODICE1 = this.RadioValue()
    return .t.
  endfunc

  func oCDCODICE1_2_27.SetRadio()
    this.Parent.oContained.w_CDCODICE1=trim(this.Parent.oContained.w_CDCODICE1)
    this.value = ;
      iif(this.Parent.oContained.w_CDCODICE1=='1',1,;
      iif(this.Parent.oContained.w_CDCODICE1=='2',2,;
      iif(this.Parent.oContained.w_CDCODICE1=='3',3,;
      0)))
  endfunc

  func oCDCODICE1_2_27.mHide()
    with this.Parent.oContained
      return (.w_Cdsnsogg1=0 Or .w_Attmod1<>'S')
    endwith
  endfunc

  add object oATTMOD2_2_28 as StdCheck with uid="EYZOXVNEAR",rtseq=33,rtrep=.f.,left=9, top=447, caption="Nuovo modulo ",;
    ToolTipText = "Se attivo, consente di inserire i dati della relativa sezione",;
    HelpContextID = 61579782,;
    cFormVar="w_ATTMOD2", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oATTMOD2_2_28.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oATTMOD2_2_28.GetRadio()
    this.Parent.oContained.w_ATTMOD2 = this.RadioValue()
    return .t.
  endfunc

  func oATTMOD2_2_28.SetRadio()
    this.Parent.oContained.w_ATTMOD2=trim(this.Parent.oContained.w_ATTMOD2)
    this.value = ;
      iif(this.Parent.oContained.w_ATTMOD2=='S',1,;
      0)
  endfunc

  add object oCDSNSOGG2_2_33 as StdField with uid="JFDZCJXJMJ",rtseq=36,rtrep=.f.,;
    cFormVar = "w_CDSNSOGG2", cQueryName = "CDSNSOGG2",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Altre somme non soggette a ritenuta",;
    HelpContextID = 250381709,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=320, Top=479, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  func oCDSNSOGG2_2_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Attmod2='S')
    endwith
   endif
  endfunc


  add object oCDCODICE2_2_34 as StdCombo with uid="KDSCLDLXWD",rtseq=37,rtrep=.f.,left=320,top=515,width=414,height=22;
    , ToolTipText = "Codice somme non soggette";
    , HelpContextID = 133989771;
    , cFormVar="w_CDCODICE2",RowSource=""+"1 - Compensi docenti/ricercatori Legge n. 2 del 28 gennaio 2009,"+"2 - Lavoratori con beneficio fiscale ex art. 3 Legge 30 dicembre 2010/238,"+"3 - Erogazione di altri redditi non soggetti a ritenuta", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oCDCODICE2_2_34.RadioValue()
    return(iif(this.value =1,'1',;
    iif(this.value =2,'2',;
    iif(this.value =3,'3',;
    ' '))))
  endfunc
  func oCDCODICE2_2_34.GetRadio()
    this.Parent.oContained.w_CDCODICE2 = this.RadioValue()
    return .t.
  endfunc

  func oCDCODICE2_2_34.SetRadio()
    this.Parent.oContained.w_CDCODICE2=trim(this.Parent.oContained.w_CDCODICE2)
    this.value = ;
      iif(this.Parent.oContained.w_CDCODICE2=='1',1,;
      iif(this.Parent.oContained.w_CDCODICE2=='2',2,;
      iif(this.Parent.oContained.w_CDCODICE2=='3',3,;
      0)))
  endfunc

  func oCDCODICE2_2_34.mHide()
    with this.Parent.oContained
      return (.w_Cdsnsogg2=0 Or .w_Attmod2<>'S')
    endwith
  endfunc

  add object oStr_2_1 as StdString with uid="OHOMJKSAEQ",Visible=.t., Left=149, Top=22,;
    Alignment=1, Width=167, Height=18,;
    Caption="Causale prestazione:"  ;
  , bGlobalFont=.t.

  add object oStr_2_2 as StdString with uid="KHGZLJJDGO",Visible=.t., Left=134, Top=54,;
    Alignment=1, Width=182, Height=18,;
    Caption="Ammontare lordo corrisposto:"  ;
  , bGlobalFont=.t.

  add object oStr_2_3 as StdString with uid="LGGWGFJSOG",Visible=.t., Left=234, Top=115,;
    Alignment=1, Width=82, Height=18,;
    Caption="Codice:"  ;
  , bGlobalFont=.t.

  func oStr_2_3.mHide()
    with this.Parent.oContained
      return (.w_Cdsnsogg=0)
    endwith
  endfunc

  add object oStr_2_4 as StdString with uid="RZHSIELJLF",Visible=.t., Left=95, Top=86,;
    Alignment=1, Width=221, Height=18,;
    Caption="Altre somme non soggette a ritenuta:"  ;
  , bGlobalFont=.t.

  add object oStr_2_5 as StdString with uid="WXXEJXFQNU",Visible=.t., Left=95, Top=148,;
    Alignment=1, Width=221, Height=18,;
    Caption="Imponibile:"  ;
  , bGlobalFont=.t.

  add object oStr_2_6 as StdString with uid="ZIKMIHTLBR",Visible=.t., Left=146, Top=181,;
    Alignment=1, Width=170, Height=18,;
    Caption="Ritenute a titolo di acconto:"  ;
  , bGlobalFont=.t.

  add object oStr_2_7 as StdString with uid="ZDQQMYBDKX",Visible=.t., Left=46, Top=212,;
    Alignment=1, Width=270, Height=18,;
    Caption="Contributi prev. a carico del sogg. erogante:"  ;
  , bGlobalFont=.t.

  add object oStr_2_8 as StdString with uid="HRIZOGPPDO",Visible=.t., Left=46, Top=245,;
    Alignment=1, Width=270, Height=18,;
    Caption="Contributi prev. a carico del percipiente:"  ;
  , bGlobalFont=.t.

  add object oStr_2_9 as StdString with uid="MHENGXQEVI",Visible=.t., Left=146, Top=278,;
    Alignment=1, Width=170, Height=18,;
    Caption="Spese rimborsate:"  ;
  , bGlobalFont=.t.

  add object oStr_2_10 as StdString with uid="JNPIPLEKIO",Visible=.t., Left=146, Top=308,;
    Alignment=1, Width=170, Height=18,;
    Caption="Ritenute rimborsate:"  ;
  , bGlobalFont=.t.

  add object oStr_2_22 as StdString with uid="TVYFTSKYVV",Visible=.t., Left=6, Top=23,;
    Alignment=1, Width=57, Height=18,;
    Caption="Mod. N.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_24 as StdString with uid="AIEEAEYVQV",Visible=.t., Left=234, Top=423,;
    Alignment=1, Width=82, Height=18,;
    Caption="Codice:"  ;
  , bGlobalFont=.t.

  func oStr_2_24.mHide()
    with this.Parent.oContained
      return (.w_Cdsnsogg1=0 Or .w_Attmod1<>'S')
    endwith
  endfunc

  add object oStr_2_25 as StdString with uid="VDBZSPSBLX",Visible=.t., Left=95, Top=385,;
    Alignment=1, Width=221, Height=18,;
    Caption="Altre somme non soggette a ritenuta:"  ;
  , bGlobalFont=.t.

  add object oStr_2_30 as StdString with uid="CMRQOOQHMJ",Visible=.t., Left=4, Top=331,;
    Alignment=0, Width=202, Height=18,;
    Caption="Altri moduli"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_31 as StdString with uid="CUSWVCQDJN",Visible=.t., Left=234, Top=519,;
    Alignment=1, Width=82, Height=18,;
    Caption="Codice:"  ;
  , bGlobalFont=.t.

  func oStr_2_31.mHide()
    with this.Parent.oContained
      return (.w_Cdsnsogg2=0 Or .w_Attmod2<>'S')
    endwith
  endfunc

  add object oStr_2_32 as StdString with uid="TWOCGOVTDK",Visible=.t., Left=95, Top=481,;
    Alignment=1, Width=221, Height=18,;
    Caption="Altre somme non soggette a ritenuta:"  ;
  , bGlobalFont=.t.

  add object oBox_2_29 as StdBox with uid="ZPVGJVTGHK",left=-1, top=349, width=765,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsri2scs','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
