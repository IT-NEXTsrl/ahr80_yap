* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_bde                                                        *
*              Manutenzione dati estratti C.U.                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2015-12-28                                                      *
* Last revis.: 2016-02-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParame
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsri_bde",oParentObject,m.pParame)
return(i_retval)

define class tgsri_bde as StdBatch
  * --- Local variables
  pParame = space(1)
  w_OBJECT = .NULL.
  w_Zoom = .NULL.
  w_Conferma = .f.
  w_Cdprotec = space(17)
  w_Cdprodoc = 0
  w_Chserial = space(10)
  w_Gdserest = space(10)
  w_Tipope = space(1)
  w_Serial = 0
  w_nCert = 0
  w_OldVersione = .f.
  w_Numser = 0
  w_Chdatest = space(10)
  w_Aggser = space(10)
  w_Ch__Anno = 0
  w_Chcodcon = space(15)
  w_SerialeDatoEstratto = space(10)
  w_NumCerOrd = 0
  w_NumCerRett = 0
  w_MessErr = .f.
  * --- WorkFile variables
  CUDTETRH_idx=0
  GENDTEST_idx=0
  TMPDATIESTRATTI_idx=0
  CUDTETDH_idx=0
  CUDDPTDH_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch lanciato dalla maschera di manutenzione dati estratti (Gsri_Kde)
    this.w_Zoom = This.oParentObject.w_ZoomAg
    this.w_Cdprotec = This.oParentObject.w_Chprotec
    this.w_Cdprodoc = This.oParentObject.w_Chprodoc
    do case
      case this.pParame="D"
        * --- Visualizzazione dato estratto
        this.w_OBJECT = GSRI_ACH()
        if !(this.w_OBJECT.bSec1)
          i_retcode = 'stop'
          return
        endif
        this.w_OBJECT.EcpFilter()     
        * --- Valorizzo i campi chiave
        this.w_OBJECT.w_CHSERIAL = This.oParentObject.w_Chserial
        * --- Carico il record richiesto
        this.w_OBJECT.EcpSave()     
      case this.pParame="V"
        this.w_Chserial = This.oParentObject.w_Chserial
        * --- Verifico che l'utente abbia selezionato solo un record
        Select * from (this.w_Zoom.cCursor) Where Xchk=1 into cursor Temp
        if Reccount("Temp")=1
          this.w_Conferma = .F.
          do Gsri1Scs with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if this.w_Conferma
            * --- Write into CUDTETRH
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.CUDTETRH_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CUDTETRH_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.CUDTETRH_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"CHPROTEC ="+cp_NullLink(cp_ToStrODBC(this.w_CDPROTEC),'CUDTETRH','CHPROTEC');
              +",CHPRODOC ="+cp_NullLink(cp_ToStrODBC(this.w_CDPRODOC),'CUDTETRH','CHPRODOC');
              +",CHTIPREC ="+cp_NullLink(cp_ToStrODBC("R"),'CUDTETRH','CHTIPREC');
                  +i_ccchkf ;
              +" where ";
                  +"CHSERIAL = "+cp_ToStrODBC(this.w_CHSERIAL);
                     )
            else
              update (i_cTable) set;
                  CHPROTEC = this.w_CDPROTEC;
                  ,CHPRODOC = this.w_CDPRODOC;
                  ,CHTIPREC = "R";
                  &i_ccchkf. ;
               where;
                  CHSERIAL = this.w_CHSERIAL;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            * --- Rilancio l'elaborazione dello zoom
            Ah_ErrorMsg("Aggiornamento completato",64,"")
            * --- Effettuo questa operazione per evitare che lo zoom si rovini
             
 public w_TIMER 
 w_TIMER = createobject("CalendarTimer", This.oParentObject,"Aggiorna",.T.) 
 w_TIMER.Interval = 100
          endif
        else
          Ah_ErrorMsg("E' possibile selezionare solo una certificazione")
        endif
        use in Select ("Temp")
      case this.pParame="S" or this.pParame="A" or this.pParame="C"
        this.w_Tipope = this.pParame
        if this.pParame="C"
          * --- Verifico che l'utente abbia selezionato solo certificazioni inserite in una 
          *     generazione
          Select * from (this.w_Zoom.cCursor) Where Xchk=1 And (Gen<>"S" Or Not Empty(Nvl(Chprotec," "))) into cursor Temp
          if Reccount("Temp")>0
            Ah_ErrorMsg("E' possibile selezionare solo certificazioni generate prive di identificativo dell'invio ")
            use in Select ("Temp")
            i_retcode = 'stop'
            return
          endif
        else
          * --- Verifico che l'utente abbia selezionato solo certificazioni inserite in una 
          *     generazione
          Select * from (this.w_Zoom.cCursor) Where Xchk=1 And Empty(Nvl(Chprotec," ")) into cursor Temp
          if Reccount("Temp")>0
            Ah_ErrorMsg("E' possibile selezionare solo certificazioni con identificativo dell'invio valorizzato")
            use in Select ("Temp")
            i_retcode = 'stop'
            return
          endif
        endif
        use in Select ("Temp")
        Select * from (this.w_Zoom.cCursor) Where Xchk=1 And Not Empty(Nvl(Datrett," ")) And Nvl(CertMod,"N")="N" into cursor Temp
        if Reccount("Temp")>0
          Ah_ErrorMsg("E' possibile selezionare solo certificazioni non legate%0ad altre certificazioni di rettifica")
          use in Select ("Temp")
          i_retcode = 'stop'
          return
        endif
        use in Select ("Temp")
        if this.pParame="C"
          * --- Verifico che l'utente non abbia selezionato certificazioni di tipo operazioni
          *     differenti
          Select Count(*) As NumCerOrd from (this.w_Zoom.cCursor) Where Xchk=1 And Nvl(Tipope,"O")="O" into cursor Temp_Ord
          Select Temp_Ord
          this.w_NumCerOrd = Nvl(Temp_Ord.NumCerOrd,0)
          Select Count(*) As NumCerRett from (this.w_Zoom.cCursor) Where Xchk=1 And Nvl(Tipope,"O") $ "AS" into cursor Temp_Rett
          Select Temp_Rett
          this.w_NumCerRett = Nvl(Temp_Rett.NumCerRett,0)
          use in Select ("Temp_Ord")
          use in Select ("Temp_Rett")
          if this.w_NumCerRett<>0 And this.w_NumCerOrd<>0
            Ah_ErrorMsg("E' possibile selezionare solo certificazioni con medesimo tipo operazione")
            i_retcode = 'stop'
            return
          endif
        endif
        Select Distinct Chserial from (this.w_Zoom.cCursor) Where Xchk=1 into cursor Temp
        Select ("Temp")
        this.w_nCert = reccount()
        * --- Creo un cursore temporaneo con l'elenco delle certificazioni da sostituire/annullare
        Curtotab("Temp","Tmptotrech")
        * --- Create temporary table TMPDATIESTRATTI
        i_nIdx=cp_AddTableDef('TMPDATIESTRATTI') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('..\rite\exe\query\gsri1kde',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.TMPDATIESTRATTI_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        * --- Nel caso di certificazione respinta di tipo sostitutivo o annullamento non
        *     deve essere creata in automatico la certificazione
        if this.pParame="C" And this.w_NumCerRett<>0
          * --- Write into CUDTETRH
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.CUDTETRH_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CUDTETRH_idx,2])
          if i_nConn<>0
            local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
            declare i_aIndex[1]
            i_cQueryTable=cp_getTempTableName(i_nConn)
            i_aIndex(1)="CHSERIAL"
            do vq_exec with 'Gsri6kde',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.CUDTETRH_idx,i_nConn)	
            i_cDB=cp_GetDatabaseType(i_nConn)
            do case
            case i_cDB="SQLServer"
              i_cWhere="CUDTETRH.CHSERIAL = _t2.CHSERIAL";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"CHTIPREC = _t2.CHTIPREC";
                +i_ccchkf;
                +" from "+i_cTable+" CUDTETRH, "+i_cQueryTable+" _t2 where "+i_cWhere)
            case i_cDB="MySQL"
              i_cWhere="CUDTETRH.CHSERIAL = _t2.CHSERIAL";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CUDTETRH, "+i_cQueryTable+" _t2 set ";
                +"CUDTETRH.CHTIPREC = _t2.CHTIPREC";
                +Iif(Empty(i_ccchkf),"",",CUDTETRH.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                +" where "+i_cWhere)
            case i_cDB="Oracle"
              i_cWhere="CUDTETRH.CHSERIAL = t2.CHSERIAL";
              
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CUDTETRH set (";
                +"CHTIPREC";
                +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                +"t2.CHTIPREC";
                +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
            case i_cDB="PostgreSQL"
              i_cWhere="CUDTETRH.CHSERIAL = _t2.CHSERIAL";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CUDTETRH set ";
                +"CHTIPREC = _t2.CHTIPREC";
                +i_ccchkf;
                +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
            otherwise
              i_cWhere=i_cTable+".CHSERIAL = "+i_cQueryTable+".CHSERIAL";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"CHTIPREC = (select CHTIPREC from "+i_cQueryTable+" where "+i_cWhere+")";
                +i_ccchkf;
                +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
            endcase
            cp_DropTempTable(i_nConn,i_cQueryTable)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          Ah_ErrorMsg("Aggiornamento completato",64,"")
           
 public w_TIMER 
 w_TIMER = createobject("CalendarTimer", This.oParentObject,"Aggiorna",.T.) 
 w_TIMER.Interval = 100
        else
          * --- Solo nel caso di certificazione respinta avverto l'utente se vuole proseguire
          *     l'elaborazione
          if (this.pParame="C" And Ah_yesNo("Si vuole procedere con la duplicazione delle rispettive certificazioni?")) Or this.pParame="S" Or this.pParame="A"
            * --- Try
            local bErr_00E8CC20
            bErr_00E8CC20=bTrsErr
            this.Try_00E8CC20()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- rollback
              bTrsErr=.t.
              cp_EndTrs(.t.)
              * --- accept error
              bTrsErr=.f.
              ah_ErrorMsg("Errore nella creazione dei dati estratti (%1)", "!", "", i_trsmsg)
            endif
            bTrsErr=bTrsErr or bErr_00E8CC20
            * --- End
            * --- Rilancio l'elaborazione dello zoom
            do case
              case this.pParame="S"
                Ah_ErrorMsg("Create %1 certificazione di tipo sostituzione",,,Alltrim(Str(this.w_nCert)))
              case this.pParame="A"
                Ah_ErrorMsg("Create %1 certificazioni di tipo annullamento",,,Alltrim(Str(this.w_nCert)))
              case this.pParame="C"
                Ah_ErrorMsg("Create %1 certificazioni",,,Alltrim(Str(this.w_nCert)))
            endcase
            * --- Effettuo questa operazione per evitare che lo zoom si rovini
             
 public w_TIMER 
 w_TIMER = createobject("CalendarTimer", This.oParentObject,"Aggiorna",.T.) 
 w_TIMER.Interval = 100
          endif
        endif
        * --- Drop temporary table TMPTOTRECH
        i_nIdx=cp_GetTableDefIdx('TMPTOTRECH')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TMPTOTRECH')
        endif
        * --- Drop temporary table TMPDATIESTRATTI
        i_nIdx=cp_GetTableDefIdx('TMPDATIESTRATTI')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TMPDATIESTRATTI')
        endif
        use in Select ("Temp")
      case this.pParame="E"
        * --- Verifico che l'utente abbia selezionato solo un record
        Select * from (this.w_Zoom.cCursor) Where Xchk=1 into cursor Temp
        if Reccount("Temp")=1
          Select Temp
          Go Top
          this.w_Chcodcon = Temp.Chcodcon
          this.w_Ch__Anno = Temp.Ch__Anno
          this.w_SerialeDatoEstratto = Temp.Chserial
          this.w_Conferma = .F.
          do Gsri1Kde with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if this.w_Conferma And Not Empty(this.w_Chserial)
            * --- Write into CUDTETRH
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.CUDTETRH_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CUDTETRH_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.CUDTETRH_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"CHDATEST ="+cp_NullLink(cp_ToStrODBC(this.w_CHSERIAL),'CUDTETRH','CHDATEST');
              +",CHDAVERI ="+cp_NullLink(cp_ToStrODBC("N"),'CUDTETRH','CHDAVERI');
                  +i_ccchkf ;
              +" where ";
                  +"CHSERIAL = "+cp_ToStrODBC(this.w_SerialeDatoEstratto);
                     )
            else
              update (i_cTable) set;
                  CHDATEST = this.w_CHSERIAL;
                  ,CHDAVERI = "N";
                  &i_ccchkf. ;
               where;
                  CHSERIAL = this.w_SerialeDatoEstratto;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            * --- Rilancio l'elaborazione dello zoom
            Ah_ErrorMsg("Aggiornamento completato",64,"")
            * --- Effettuo questa operazione per evitare che lo zoom si rovini
             
 public w_TIMER 
 w_TIMER = createobject("CalendarTimer", This.oParentObject,"Aggiorna",.T.) 
 w_TIMER.Interval = 100
          endif
        else
          Ah_ErrorMsg("E' possibile selezionare solo una certificazione")
          use in Select ("Temp")
        endif
      case this.pParame="I"
        Select * from (this.w_Zoom.cCursor) Where Xchk=1 into cursor Temp
        if Reccount("Temp")=1
          use in Select ("Temp")
          Select * from (this.w_Zoom.cCursor) Where Xchk=1 And Not Empty(Nvl(Datrett," ")) into cursor Temp
          * --- Devo verificare che il dato estratto non sia stato associato ad un altro dato estratto
          if Reccount("Temp")>0
            Ah_ErrorMsg("E' possibile selezionare solo certificazioni non legate%0ad altre certificazioni di rettifica")
            use in Select ("Temp")
            i_retcode = 'stop'
            return
          else
            use in Select ("Temp")
            Select * from (this.w_Zoom.cCursor) Where Xchk=1 And Empty(Nvl(Chtiprec," ")) into cursor Temp
            if Reccount("Temp")>0
              Ah_ErrorMsg("E' possibile selezionare solo certificazioni accolte o respinte")
              use in Select ("Temp")
              i_retcode = 'stop'
              return
            endif
            use in Select ("Temp")
            Select * from (this.w_Zoom.cCursor) Where Xchk=1 into cursor Temp
            * --- Nel caso in cui la certificazione sia "Respinta" ed il tipo operazione
            *     sia di tipo 'Sostitutiva/Annullamento", devo verificare se la certificazione
            *     padre abbia un altro dato estratto associato
            this.w_MessErr = .f.
            Select Temp
            Go Top
            Scan for Nvl(Tiprec," ")="Q" And Tipope $ "AS" And Not Empty(Chdatest) And ! this.w_MessErr
            this.w_Chdatest = Temp.Chdatest
            * --- Select from CUDTETRH
            i_nConn=i_TableProp[this.CUDTETRH_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CUDTETRH_idx,2])
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select * from "+i_cTable+" CUDTETRH ";
                  +" where CHDATEST="+cp_ToStrODBC(this.w_Chdatest)+" And CHTIPREC<>'Q'";
                   ,"_Curs_CUDTETRH")
            else
              select * from (i_cTable);
               where CHDATEST=this.w_Chdatest And CHTIPREC<>"Q";
                into cursor _Curs_CUDTETRH
            endif
            if used('_Curs_CUDTETRH')
              select _Curs_CUDTETRH
              locate for 1=1
              do while not(eof())
              this.w_Chserial = _Curs_CUDTETRH.CHSERIAL
              this.w_MessErr = .t.
                select _Curs_CUDTETRH
                continue
              enddo
              use
            endif
            Endscan
            if this.w_MessErr
              Ah_ErrorMsg("Esiste almeno una certificazione non respinta")
            else
              Select Temp
              Go Top
              Scan
              this.w_Chserial = Temp.Chserial
              * --- Write into CUDTETRH
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.CUDTETRH_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.CUDTETRH_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.CUDTETRH_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"CHPROTEC ="+cp_NullLink(cp_ToStrODBC(Space(17)),'CUDTETRH','CHPROTEC');
                +",CHPRODOC ="+cp_NullLink(cp_ToStrODBC(0),'CUDTETRH','CHPRODOC');
                +",CHTIPREC ="+cp_NullLink(cp_ToStrODBC(" "),'CUDTETRH','CHTIPREC');
                    +i_ccchkf ;
                +" where ";
                    +"CHSERIAL = "+cp_ToStrODBC(this.w_CHSERIAL);
                       )
              else
                update (i_cTable) set;
                    CHPROTEC = Space(17);
                    ,CHPRODOC = 0;
                    ,CHTIPREC = " ";
                    &i_ccchkf. ;
                 where;
                    CHSERIAL = this.w_CHSERIAL;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
              Endscan
              Ah_ErrorMsg("Aggiornamento completato",64,"")
               
 public w_TIMER 
 w_TIMER = createobject("CalendarTimer", This.oParentObject,"Aggiorna",.T.) 
 w_TIMER.Interval = 100
            endif
          endif
          use in Select ("Temp")
        else
          Ah_ErrorMsg("E' possibile selezionare solo una certificazione")
        endif
    endcase
  endproc
  proc Try_00E8CC20()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    AddMsgNL(Space(5)+"Generazione progressivi",this)
    * --- Verifico la versione del database 
    this.w_Chserial = space(10)
    w_Conn=i_TableProp[this.CUDTETRH_IDX, 3] 
 cp_NextTableProg(this, w_Conn, "CHSER", "i_codazi,w_Chserial")
    this.w_Serial = VAL(this.w_Chserial)-1
    this.w_Chserial = cp_StrZeroPad( this.w_Serial+this.w_nCert ,10)
    cp_NextTableProg(this, w_Conn, "CHSER", "i_codazi,w_CHSERIAL")
    if upper(CP_DBTYPE)="SQLSERVER"
      sqlexec(w_Conn,"Select convert(char,SERVERPROPERTY('productversion')) As Version", "ChkVersion")
      if USED("ChkVersion") AND VAL(LEFT(ChkVersion.Version,AT(".",ChkVersion.Version)-1)) < 9
        this.w_Oldversione = .t.
      endif
    else
      if TYPE("g_ORACLEVERS")="C" AND g_ORACLEVERS="8"
        this.w_Oldversione = .t.
      endif
    endif
    * --- Genero progressivo seriale
    if this.w_Oldversione
      * --- Select from GSRI3KDE
      do vq_exec with 'GSRI3KDE',this,'_Curs_GSRI3KDE','',.f.,.t.
      if used('_Curs_GSRI3KDE')
        select _Curs_GSRI3KDE
        locate for 1=1
        do while not(eof())
        this.w_Numser = this.w_Numser + 1
        this.w_Chdatest = _Curs_GSRI3KDE.Chdatest
        this.w_Aggser = cp_StrZeroPad(this.w_Numser + this.w_Serial, 10)
        * --- Write into TMPDATIESTRATTI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.TMPDATIESTRATTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPDATIESTRATTI_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPDATIESTRATTI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CHSERIAL ="+cp_NullLink(cp_ToStrODBC(this.w_AGGSER),'TMPDATIESTRATTI','CHSERIAL');
              +i_ccchkf ;
          +" where ";
              +"CHDATEST = "+cp_ToStrODBC(this.w_CHDATEST);
                 )
        else
          update (i_cTable) set;
              CHSERIAL = this.w_AGGSER;
              &i_ccchkf. ;
           where;
              CHDATEST = this.w_CHDATEST;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
          select _Curs_GSRI3KDE
          continue
        enddo
        use
      endif
    else
      * --- Write into TMPDATIESTRATTI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMPDATIESTRATTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPDATIESTRATTI_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="CHDATEST"
        do vq_exec with 'GSRI2KDE',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPDATIESTRATTI_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="TMPDATIESTRATTI.CHDATEST = _t2.CHDATEST";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CHSERIAL = _t2.CHSERIAL";
            +i_ccchkf;
            +" from "+i_cTable+" TMPDATIESTRATTI, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="TMPDATIESTRATTI.CHDATEST = _t2.CHDATEST";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI, "+i_cQueryTable+" _t2 set ";
            +"TMPDATIESTRATTI.CHSERIAL = _t2.CHSERIAL";
            +Iif(Empty(i_ccchkf),"",",TMPDATIESTRATTI.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="TMPDATIESTRATTI.CHDATEST = t2.CHDATEST";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI set (";
            +"CHSERIAL";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.CHSERIAL";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="TMPDATIESTRATTI.CHDATEST = _t2.CHDATEST";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI set ";
            +"CHSERIAL = _t2.CHSERIAL";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".CHDATEST = "+i_cQueryTable+".CHDATEST";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CHSERIAL = (select CHSERIAL from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- Insert into CUDTETRH
    i_nConn=i_TableProp[this.CUDTETRH_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CUDTETRH_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"Gsri4kde",this.CUDTETRH_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Insert into CUDDPTDH
    i_nConn=i_TableProp[this.CUDDPTDH_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CUDDPTDH_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\rite\exe\query\gsri8kde",this.CUDDPTDH_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Nel caso di record Scartati devo andare a valorizzare nei record selezionati
    *     il flag Respinto
    if this.pParame="C"
      * --- Write into CUDTETRH
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.CUDTETRH_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CUDTETRH_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="CHSERIAL"
        do vq_exec with 'Gsri6kde',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CUDTETRH_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="CUDTETRH.CHSERIAL = _t2.CHSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CHTIPREC = _t2.CHTIPREC";
            +i_ccchkf;
            +" from "+i_cTable+" CUDTETRH, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="CUDTETRH.CHSERIAL = _t2.CHSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CUDTETRH, "+i_cQueryTable+" _t2 set ";
            +"CUDTETRH.CHTIPREC = _t2.CHTIPREC";
            +Iif(Empty(i_ccchkf),"",",CUDTETRH.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="CUDTETRH.CHSERIAL = t2.CHSERIAL";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CUDTETRH set (";
            +"CHTIPREC";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.CHTIPREC";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="CUDTETRH.CHSERIAL = _t2.CHSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CUDTETRH set ";
            +"CHTIPREC = _t2.CHTIPREC";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".CHSERIAL = "+i_cQueryTable+".CHSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CHTIPREC = (select CHTIPREC from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  proc Init(oParentObject,pParame)
    this.pParame=pParame
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='CUDTETRH'
    this.cWorkTables[2]='GENDTEST'
    this.cWorkTables[3]='*TMPDATIESTRATTI'
    this.cWorkTables[4]='CUDTETDH'
    this.cWorkTables[5]='CUDDPTDH'
    return(this.OpenAllTables(5))

  proc CloseCursors()
    if used('_Curs_GSRI3KDE')
      use in _Curs_GSRI3KDE
    endif
    if used('_Curs_CUDTETRH')
      use in _Curs_CUDTETRH
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParame"
endproc
