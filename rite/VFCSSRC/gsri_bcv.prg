* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_bcv                                                        *
*              Aggiornamento zoom                                              *
*                                                                              *
*      Author: Zucchetti spa                                                   *
*      Client: Zucchetti TAM                                                   *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][207]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-09-13                                                      *
* Last revis.: 2000-09-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsri_bcv",oParentObject)
return(i_retval)

define class tgsri_bcv as StdBatch
  * --- Local variables
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Procedura lanciata per poter azzerrare i dati dello zoom da GSRI_SCV e GSRI_SIN
    * --- Azzero le variabili all'interno della maschera.
    this.oParentObject.w_NREGIS = 0
    this.oParentObject.w_DATADI = Cp_CharToDate("  -  -  ")
    this.oParentObject.w_ESERCI = SPACE(4)
    this.oParentObject.w_NDISTI = SPACE(10)
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
