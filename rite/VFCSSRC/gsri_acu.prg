* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_acu                                                        *
*              Certificazione Unica                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2015-01-19                                                      *
* Last revis.: 2018-02-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- gsri_acu
PARAMETERS pTipCer

L_descri1='1) Rappr. legale o negoziale'
L_descri2='2) Rappr. di minore - curatore eredit�'
L_descri3='3) Curatore fallimentare'
L_descri4='4) Commissario liquidatore'
L_descri5='5) Commissario giudiziale'
L_descri6='6) Rappr. fiscale di soggetto non residente'
L_descri7='7) Erede'
L_descri8='8) Liquidatore volontario'
L_descri9='9) Sogg. dich. IVA operaz. straord.'
L_descri10='10) Rappr. fisc. sogg. non resid. D.L.331/1993'
L_descri11='11) Tutore'
L_descri12='12) Liquid. volont. ditta indiv.'
L_descri13='13) Ammin. condominio'
L_descri14='14) Sogg. per conto P.A.'
L_descri15='15) Comm. liquid. P.A.'
* --- Fine Area Manuale
return(createobject("tgsri_acu"))

* --- Class definition
define class tgsri_acu as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 787
  Height = 429+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2018-02-08"
  HelpContextID=109704809
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=76

  * --- Constant Properties
  GENCOMUN_IDX = 0
  AZIENDA_IDX = 0
  TITOLARI_IDX = 0
  DAT_RAPP_IDX = 0
  cFile = "GENCOMUN"
  cKeySelect = "CUSERIAL"
  cKeyWhere  = "CUSERIAL=this.w_CUSERIAL"
  cKeyWhereODBC = '"CUSERIAL="+cp_ToStrODBC(this.w_CUSERIAL)';

  cKeyWhereODBCqualified = '"GENCOMUN.CUSERIAL="+cp_ToStrODBC(this.w_CUSERIAL)';

  cPrg = "gsri_acu"
  cComment = "Certificazione Unica"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CUSERIAL = space(10)
  w_CUDATCER = ctod('  /  /  ')
  w_CUDESAGG = space(254)
  w_CODAZI = space(5)
  w_AZCOFAZI = space(16)
  w_AZTELFAX = space(18)
  w_AZ_EMAIL = space(50)
  w_AZPERAZI = space(1)
  w_AZLOCAZI = space(30)
  w_AZPROAZI = space(2)
  w_AZCAPAZI = space(9)
  w_AZINDAZI = space(35)
  w_CODAZI2 = space(5)
  w_COGTIT1 = space(25)
  w_NOMTIT1 = space(25)
  w_TTLOCTIT = space(30)
  w_TTPROTIT = space(2)
  w_TTCAPTIT = space(9)
  w_TTINDIRI = space(35)
  w_TTTELEFO = space(18)
  w_CUCODFIS = space(16)
  o_CUCODFIS = space(16)
  w_CUCODFSI = space(16)
  w_CUCOGNOM = space(24)
  w_CU__NOME = space(20)
  w_CURAGSOC = space(60)
  w_CUCOMUNE = space(30)
  w_CUPRORES = space(2)
  w_CU___CAP = space(9)
  w_CUINDIRI = space(35)
  w_CUNUMFAX = space(12)
  w_CU__MAIL = space(100)
  w_CUDATFIR = ctod('  /  /  ')
  w_CUCODINT = space(16)
  o_CUCODINT = space(16)
  w_CUIMPTRA = space(1)
  w_CUDATIMP = ctod('  /  /  ')
  w_CUFIRINT = space(1)
  w_CURAPFIR = space(1)
  o_CURAPFIR = space(1)
  w_RAPFIRM = space(10)
  w_RFCODFIS = space(16)
  w_RFCOGNOM = space(40)
  w_RF__NOME = space(40)
  w_RFCODCAR = space(2)
  w_CUCFISOT = space(16)
  w_CUCODFED = space(11)
  w_CUCODCAR = space(2)
  o_CUCODCAR = space(2)
  w_CUCOGSOT = space(24)
  w_CUNOMSOT = space(20)
  w_CURAPFAL = ctod('  /  /  ')
  w_CUCODATT = space(6)
  w_CUCODSED = space(3)
  w_CUNUMCER = 0
  w_CUFIRDIC = space(1)
  w_CUTIPCER = space(5)
  w_DirName = space(200)
  w_CUDATINI = ctod('  /  /  ')
  o_CUDATINI = ctod('  /  /  ')
  w_CUDATFIN = ctod('  /  /  ')
  w_CU__ANNO = 0
  o_CU__ANNO = 0
  w_CUDOMFIS = space(1)
  w_CUSOGEST = space(1)
  w_CUAGRCER = space(1)
  o_CUAGRCER = space(1)
  w_CUEVEECC = space(1)
  w_CUNOMFIL = space(254)
  o_CUNOMFIL = space(254)
  w_CUTIPFOR = space(2)
  o_CUTIPFOR = space(2)
  w_CUTIPCAS = space(1)
  o_CUTIPCAS = space(1)
  w_CUCERSIN = space(1)
  o_CUCERSIN = space(1)
  w_CUTIPCAS = space(1)
  w_CUCERSIN = space(1)
  w_AIDATINV = ctod('  /  /  ')
  w_TIPCER = space(5)
  w_CUCERANN = space(1)
  w_CUCERSOS = space(1)
  w_HASEVCOP = space(50)
  w_HASEVENT = .F.
  w_TIPOCONT = space(1)
  w_EVEECC_2016 = space(1)
  o_EVEECC_2016 = space(1)
  w_EVEECC_2017 = space(1)
  o_EVEECC_2017 = space(1)

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_Cuserial = this.W_Cuserial

  * --- Children pointers
  Gsri_Mcu = .NULL.
  Gsri_Mcr = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsri_acu
  * ---- Parametro Gestione
  pTipCer='    '
   * --- Gestiti i soli eventi Modifica, cancellazione, inserimento
  func HasCPEvents(i_cOp)
   if this.cFunction='Query'
  	  	if Upper(i_cop)=='ECPDELETE'
  	  		* esegue controllo se movimento bloccato se da problemi si pu� chiamare direttamente il Batch
          This.w_HASEVCOP=i_cop
  	  		This.NotifyEvent('HasEvent')
  	  		Return ( This.w_HASEVENT )
        Else
  	  		Return(.t.)
  	  	endif
  	Else
  	  	* --- Se l'evento non � modifica/cancellazione/inserimento prosegue
        Return(.t.)
    Endif
   EndFunc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=5, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'GENCOMUN','gsri_acu')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsri_acuPag1","gsri_acu",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Dati del contribuente")
      .Pages(1).HelpContextID = 261664428
      .Pages(2).addobject("oPag","tgsri_acuPag2","gsri_acu",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Rappresentante firmatario")
      .Pages(2).HelpContextID = 191515562
      .Pages(3).addobject("oPag","tgsri_acuPag3","gsri_acu",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Firma/Impegno a trasmettere")
      .Pages(3).HelpContextID = 45073895
      .Pages(4).addobject("oPag","tgsri_acuPag4","gsri_acu",4)
      .Pages(4).oPag.Visible=.t.
      .Pages(4).Caption=cp_Translate("File telematico")
      .Pages(4).HelpContextID = 101926126
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- gsri_acu
    Getblackbversion()
    * --- Imposta il Titolo della Finestra della Gestione in Base al tipo
    WITH THIS.PARENT
       IF Type('pTipCer')='U' Or EMPTY(pTipCer)
          .pTipRIT = 'CUR15'
       ELSE
          .pTipCer = pTipCer
       ENDIF
       DO CASE
             CASE pTipCer = 'CUR15'
                   .cComment = CP_TRANSLATE('CERTIFICAZIONE UNICA 2015')
                   .cAutoZoom = 'DEFAULT'
             CASE pTipCer = 'CUR16'
                   .cComment = CP_TRANSLATE('Generazione file telematico')
                   .cAutoZoom = 'GSRI_ACU'
       ENDCASE
    ENDWIT
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='TITOLARI'
    this.cWorkTables[3]='DAT_RAPP'
    this.cWorkTables[4]='GENCOMUN'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(4))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.GENCOMUN_IDX,5],7]
    this.nPostItConn=i_TableProp[this.GENCOMUN_IDX,3]
  return

  function CreateChildren()
    this.Gsri_Mcu = CREATEOBJECT('stdDynamicChild',this,'Gsri_Mcu',this.oPgFrm.Page4.oPag.oLinkPC_4_23)
    this.Gsri_Mcr = CREATEOBJECT('stdDynamicChild',this,'Gsri_Mcr',this.oPgFrm.Page4.oPag.oLinkPC_4_35)
    return

  procedure DestroyChildren()
    if !ISNULL(this.Gsri_Mcu)
      this.Gsri_Mcu.DestroyChildrenChain()
      this.Gsri_Mcu=.NULL.
    endif
    this.oPgFrm.Page4.oPag.RemoveObject('oLinkPC_4_23')
    if !ISNULL(this.Gsri_Mcr)
      this.Gsri_Mcr.DestroyChildrenChain()
      this.Gsri_Mcr=.NULL.
    endif
    this.oPgFrm.Page4.oPag.RemoveObject('oLinkPC_4_35')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.Gsri_Mcu.IsAChildUpdated()
      i_bRes = i_bRes .or. this.Gsri_Mcr.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.Gsri_Mcu.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.Gsri_Mcr.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.Gsri_Mcu.NewDocument()
    this.Gsri_Mcr.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.Gsri_Mcu.SetKey(;
            .w_CUSERIAL,"CDSERIAL";
            )
      this.Gsri_Mcr.SetKey(;
            .w_CUSERIAL,"GDSERIAL";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .Gsri_Mcu.ChangeRow(this.cRowID+'      1',1;
             ,.w_CUSERIAL,"CDSERIAL";
             )
      .Gsri_Mcr.ChangeRow(this.cRowID+'      1',1;
             ,.w_CUSERIAL,"GDSERIAL";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.Gsri_Mcu)
        i_f=.Gsri_Mcu.BuildFilter()
        if !(i_f==.Gsri_Mcu.cQueryFilter)
          i_fnidx=.Gsri_Mcu.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.Gsri_Mcu.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.Gsri_Mcu.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.Gsri_Mcu.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.Gsri_Mcu.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.Gsri_Mcr)
        i_f=.Gsri_Mcr.BuildFilter()
        if !(i_f==.Gsri_Mcr.cQueryFilter)
          i_fnidx=.Gsri_Mcr.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.Gsri_Mcr.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.Gsri_Mcr.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.Gsri_Mcr.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.Gsri_Mcr.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_CUSERIAL = NVL(CUSERIAL,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from GENCOMUN where CUSERIAL=KeySet.CUSERIAL
    *
    i_nConn = i_TableProp[this.GENCOMUN_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.GENCOMUN_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('GENCOMUN')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "GENCOMUN.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' GENCOMUN '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'CUSERIAL',this.w_CUSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_CODAZI = i_CODAZI
        .w_AZCOFAZI = space(16)
        .w_AZTELFAX = space(18)
        .w_AZ_EMAIL = space(50)
        .w_AZPERAZI = space(1)
        .w_AZLOCAZI = space(30)
        .w_AZPROAZI = space(2)
        .w_AZCAPAZI = space(9)
        .w_AZINDAZI = space(35)
        .w_CODAZI2 = i_CODAZI
        .w_COGTIT1 = space(25)
        .w_NOMTIT1 = space(25)
        .w_TTLOCTIT = space(30)
        .w_TTPROTIT = space(2)
        .w_TTCAPTIT = space(9)
        .w_TTINDIRI = space(35)
        .w_TTTELEFO = space(18)
        .w_RAPFIRM = i_Codazi
        .w_RFCODFIS = space(16)
        .w_RFCOGNOM = space(40)
        .w_RF__NOME = space(40)
        .w_RFCODCAR = space(2)
        .w_DirName = sys(5)+sys(2003)+'\'
        .w_AIDATINV = i_DATSYS
        .w_HASEVCOP = space(50)
        .w_HASEVENT = .f.
        .w_TIPOCONT = 'G'
        .w_CUSERIAL = NVL(CUSERIAL,space(10))
        .op_CUSERIAL = .w_CUSERIAL
        .w_CUDATCER = NVL(cp_ToDate(CUDATCER),ctod("  /  /  "))
        .w_CUDESAGG = NVL(CUDESAGG,space(254))
          .link_1_4('Load')
          .link_1_13('Load')
        .w_CUCODFIS = NVL(CUCODFIS,space(16))
        .w_CUCODFSI = NVL(CUCODFSI,space(16))
        .w_CUCOGNOM = NVL(CUCOGNOM,space(24))
        .w_CU__NOME = NVL(CU__NOME,space(20))
        .w_CURAGSOC = NVL(CURAGSOC,space(60))
        .w_CUCOMUNE = NVL(CUCOMUNE,space(30))
        .w_CUPRORES = NVL(CUPRORES,space(2))
        .w_CU___CAP = NVL(CU___CAP,space(9))
        .w_CUINDIRI = NVL(CUINDIRI,space(35))
        .w_CUNUMFAX = NVL(CUNUMFAX,space(12))
        .w_CU__MAIL = NVL(CU__MAIL,space(100))
        .w_CUDATFIR = NVL(cp_ToDate(CUDATFIR),ctod("  /  /  "))
        .w_CUCODINT = NVL(CUCODINT,space(16))
        .w_CUIMPTRA = NVL(CUIMPTRA,space(1))
        .w_CUDATIMP = NVL(cp_ToDate(CUDATIMP),ctod("  /  /  "))
        .w_CUFIRINT = NVL(CUFIRINT,space(1))
        .w_CURAPFIR = NVL(CURAPFIR,space(1))
          .link_2_2('Load')
        .w_CUCFISOT = NVL(CUCFISOT,space(16))
        .w_CUCODFED = NVL(CUCODFED,space(11))
        .w_CUCODCAR = NVL(CUCODCAR,space(2))
        .w_CUCOGSOT = NVL(CUCOGSOT,space(24))
        .w_CUNOMSOT = NVL(CUNOMSOT,space(20))
        .w_CURAPFAL = NVL(cp_ToDate(CURAPFAL),ctod("  /  /  "))
        .w_CUCODATT = NVL(CUCODATT,space(6))
        .w_CUCODSED = NVL(CUCODSED,space(3))
        .w_CUNUMCER = NVL(CUNUMCER,0)
        .w_CUFIRDIC = NVL(CUFIRDIC,space(1))
        .w_CUTIPCER = NVL(CUTIPCER,space(5))
        .w_CUDATINI = NVL(cp_ToDate(CUDATINI),ctod("  /  /  "))
        .w_CUDATFIN = NVL(cp_ToDate(CUDATFIN),ctod("  /  /  "))
        .w_CU__ANNO = NVL(CU__ANNO,0)
        .w_CUDOMFIS = NVL(CUDOMFIS,space(1))
        .w_CUSOGEST = NVL(CUSOGEST,space(1))
        .w_CUAGRCER = NVL(CUAGRCER,space(1))
        .w_CUEVEECC = NVL(CUEVEECC,space(1))
        .w_CUNOMFIL = NVL(CUNOMFIL,space(254))
        .w_CUTIPFOR = NVL(CUTIPFOR,space(2))
        .w_CUTIPCAS = NVL(CUTIPCAS,space(1))
        .w_CUCERSIN = NVL(CUCERSIN,space(1))
        .w_CUTIPCAS = NVL(CUTIPCAS,space(1))
        .w_CUCERSIN = NVL(CUCERSIN,space(1))
        .w_TIPCER = THIS.pTipCer
        .w_CUCERANN = NVL(CUCERANN,space(1))
        .w_CUCERSOS = NVL(CUCERSOS,space(1))
        .oPgFrm.Page1.oPag.oObj_1_56.Calculate()
        .w_EVEECC_2016 = '0'
        .w_EVEECC_2017 = '0'
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'GENCOMUN')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page4.oPag.oBtn_4_24.enabled = this.oPgFrm.Page4.oPag.oBtn_4_24.mCond()
      this.oPgFrm.Page4.oPag.oBtn_4_36.enabled = this.oPgFrm.Page4.oPag.oBtn_4_36.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CUSERIAL = space(10)
      .w_CUDATCER = ctod("  /  /  ")
      .w_CUDESAGG = space(254)
      .w_CODAZI = space(5)
      .w_AZCOFAZI = space(16)
      .w_AZTELFAX = space(18)
      .w_AZ_EMAIL = space(50)
      .w_AZPERAZI = space(1)
      .w_AZLOCAZI = space(30)
      .w_AZPROAZI = space(2)
      .w_AZCAPAZI = space(9)
      .w_AZINDAZI = space(35)
      .w_CODAZI2 = space(5)
      .w_COGTIT1 = space(25)
      .w_NOMTIT1 = space(25)
      .w_TTLOCTIT = space(30)
      .w_TTPROTIT = space(2)
      .w_TTCAPTIT = space(9)
      .w_TTINDIRI = space(35)
      .w_TTTELEFO = space(18)
      .w_CUCODFIS = space(16)
      .w_CUCODFSI = space(16)
      .w_CUCOGNOM = space(24)
      .w_CU__NOME = space(20)
      .w_CURAGSOC = space(60)
      .w_CUCOMUNE = space(30)
      .w_CUPRORES = space(2)
      .w_CU___CAP = space(9)
      .w_CUINDIRI = space(35)
      .w_CUNUMFAX = space(12)
      .w_CU__MAIL = space(100)
      .w_CUDATFIR = ctod("  /  /  ")
      .w_CUCODINT = space(16)
      .w_CUIMPTRA = space(1)
      .w_CUDATIMP = ctod("  /  /  ")
      .w_CUFIRINT = space(1)
      .w_CURAPFIR = space(1)
      .w_RAPFIRM = space(10)
      .w_RFCODFIS = space(16)
      .w_RFCOGNOM = space(40)
      .w_RF__NOME = space(40)
      .w_RFCODCAR = space(2)
      .w_CUCFISOT = space(16)
      .w_CUCODFED = space(11)
      .w_CUCODCAR = space(2)
      .w_CUCOGSOT = space(24)
      .w_CUNOMSOT = space(20)
      .w_CURAPFAL = ctod("  /  /  ")
      .w_CUCODATT = space(6)
      .w_CUCODSED = space(3)
      .w_CUNUMCER = 0
      .w_CUFIRDIC = space(1)
      .w_CUTIPCER = space(5)
      .w_DirName = space(200)
      .w_CUDATINI = ctod("  /  /  ")
      .w_CUDATFIN = ctod("  /  /  ")
      .w_CU__ANNO = 0
      .w_CUDOMFIS = space(1)
      .w_CUSOGEST = space(1)
      .w_CUAGRCER = space(1)
      .w_CUEVEECC = space(1)
      .w_CUNOMFIL = space(254)
      .w_CUTIPFOR = space(2)
      .w_CUTIPCAS = space(1)
      .w_CUCERSIN = space(1)
      .w_CUTIPCAS = space(1)
      .w_CUCERSIN = space(1)
      .w_AIDATINV = ctod("  /  /  ")
      .w_TIPCER = space(5)
      .w_CUCERANN = space(1)
      .w_CUCERSOS = space(1)
      .w_HASEVCOP = space(50)
      .w_HASEVENT = .f.
      .w_TIPOCONT = space(1)
      .w_EVEECC_2016 = space(1)
      .w_EVEECC_2017 = space(1)
      if .cFunction<>"Filter"
          .DoRTCalc(1,1,.f.)
        .w_CUDATCER = i_Datsys
          .DoRTCalc(3,3,.f.)
        .w_CODAZI = i_CODAZI
        .DoRTCalc(4,4,.f.)
          if not(empty(.w_CODAZI))
          .link_1_4('Full')
          endif
          .DoRTCalc(5,12,.f.)
        .w_CODAZI2 = i_CODAZI
        .DoRTCalc(13,13,.f.)
          if not(empty(.w_CODAZI2))
          .link_1_13('Full')
          endif
          .DoRTCalc(14,20,.f.)
        .w_CUCODFIS = IIF(LEN(ALLTRIM(.w_AZCOFAZI))<=16, LEFT(ALLTRIM(.w_AZCOFAZI),16), RIGHT(ALLTRIM(.w_AZCOFAZI),16))
          .DoRTCalc(22,22,.f.)
        .w_CUCOGNOM = LEFT(iif(.w_AZPERAZI='S', UPPER(.w_COGTIT1), space(24)), 24)
        .w_CU__NOME = LEFT(iif(.w_AZPERAZI='S', UPPER(.w_NOMTIT1), space(20)), 20)
        .w_CURAGSOC = LEFT(iif(.w_AZPERAZI<>'S', UPPER(g_RAGAZI), space(60)), 60)
        .w_CUCOMUNE = Upper(Left(iif(.w_Azperazi='S', .w_Ttloctit, .w_Azlocazi),30))
        .w_CUPRORES = Upper(iif(.w_Azperazi='S', .w_Ttprotit, .w_Azproazi))
        .w_CU___CAP = Upper(Left(iif(.w_Azperazi='S', .w_Ttcaptit, .w_Azcapazi),9))
        .w_CUINDIRI = Upper(Left(iif(.w_Azperazi='S', .w_Ttindiri, .w_Azindazi),35))
        .w_CUNUMFAX = Left(iif(.w_Azperazi='S', .w_Tttelefo, .w_Aztelfax),12)
        .w_CU__MAIL = left(UPPER(.w_AZ_EMAIL), 100)
        .w_CUDATFIR = i_Datsys
        .w_CUCODINT = Space(16)
        .w_CUIMPTRA = '1'
        .w_CUDATIMP = Ctod('  -  -    ')
        .w_CUFIRINT = '0'
        .w_CURAPFIR = iif(.w_Azperazi<>'S','S','N')
        .w_RAPFIRM = i_Codazi
        .DoRTCalc(38,38,.f.)
          if not(empty(.w_RAPFIRM))
          .link_2_2('Full')
          endif
          .DoRTCalc(39,42,.f.)
        .w_CUCFISOT = iif(.w_CuRapFir='S',.w_Rfcodfis,Space(16))
        .w_CUCODFED = Space(11)
        .w_CUCODCAR = iif(.w_CuRapFir='N','1',.w_Rfcodcar)
        .w_CUCOGSOT = iif(.w_CuRapFir='N',space(20),Left(.w_Rfcognom,24))
        .w_CUNOMSOT = iif(.w_CuRapFir='N',space(20),Left(.w_Rf__Nome,20))
          .DoRTCalc(48,48,.f.)
        .w_CUCODATT = iif(Empty(.w_Cucodatt),iif(g_Attivi='S', Space(6), Left(Alltrim(Calattso(g_Catazi,Alltrim(Str(Year(i_Datsys))),12)),6)),.w_Cucodatt)
          .DoRTCalc(50,51,.f.)
        .w_CUFIRDIC = '1'
        .w_CUTIPCER = THIS.pTipCer
        .w_DirName = sys(5)+sys(2003)+'\'
        .w_CUDATINI = Date(2014,01,01)
        .w_CUDATFIN = Date(2014,12,31)
        .w_CU__ANNO = year(i_DATSYS)-1
        .w_CUDOMFIS = 'S'
        .w_CUSOGEST = 'N'
        .w_CUAGRCER = 'N'
          .DoRTCalc(61,62,.f.)
        .w_CUTIPFOR = '01'
        .w_CUTIPCAS = 'O'
        .w_CUCERSIN = 'N'
        .w_CUTIPCAS = 'O'
        .w_CUCERSIN = iif(.w_Cutipcas $ 'SA','S','N')
        .w_AIDATINV = i_DATSYS
        .w_TIPCER = THIS.pTipCer
        .w_CUCERANN = 'N'
        .w_CUCERSOS = 'N'
        .oPgFrm.Page1.oPag.oObj_1_56.Calculate()
          .DoRTCalc(72,73,.f.)
        .w_TIPOCONT = 'G'
        .w_EVEECC_2016 = '0'
        .w_EVEECC_2017 = '0'
      endif
    endwith
    cp_BlankRecExtFlds(this,'GENCOMUN')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page4.oPag.oBtn_4_24.enabled = this.oPgFrm.Page4.oPag.oBtn_4_24.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_36.enabled = this.oPgFrm.Page4.oPag.oBtn_4_36.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.GENCOMUN_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.GENCOMUN_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"CEUNI","i_codazi,w_Cuserial")
      .op_codazi = .w_codazi
      .op_Cuserial = .w_Cuserial
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oCUDATCER_1_2.enabled = i_bVal
      .Page1.oPag.oCUDESAGG_1_3.enabled = i_bVal
      .Page1.oPag.oCUCODFIS_1_21.enabled = i_bVal
      .Page1.oPag.oCUCOGNOM_1_23.enabled = i_bVal
      .Page1.oPag.oCU__NOME_1_24.enabled = i_bVal
      .Page1.oPag.oCURAGSOC_1_25.enabled = i_bVal
      .Page1.oPag.oCUCOMUNE_1_26.enabled = i_bVal
      .Page1.oPag.oCUPRORES_1_27.enabled = i_bVal
      .Page1.oPag.oCU___CAP_1_28.enabled = i_bVal
      .Page1.oPag.oCUINDIRI_1_29.enabled = i_bVal
      .Page1.oPag.oCUNUMFAX_1_30.enabled = i_bVal
      .Page1.oPag.oCU__MAIL_1_31.enabled = i_bVal
      .Page3.oPag.oCUDATFIR_3_3.enabled = i_bVal
      .Page3.oPag.oCUCODINT_3_4.enabled = i_bVal
      .Page3.oPag.oCUIMPTRA_3_5.enabled = i_bVal
      .Page3.oPag.oCUDATIMP_3_7.enabled = i_bVal
      .Page3.oPag.oCUFIRINT_3_10.enabled = i_bVal
      .Page2.oPag.oCURAPFIR_2_1.enabled = i_bVal
      .Page2.oPag.oCUCFISOT_2_9.enabled = i_bVal
      .Page2.oPag.oCUCODFED_2_11.enabled = i_bVal
      .Page2.oPag.oCUCODCAR_2_12.enabled = i_bVal
      .Page2.oPag.oCUCOGSOT_2_14.enabled = i_bVal
      .Page2.oPag.oCUNOMSOT_2_15.enabled = i_bVal
      .Page1.oPag.oCUCODATT_1_44.enabled = i_bVal
      .Page1.oPag.oCUCODSED_1_46.enabled = i_bVal
      .Page4.oPag.oCUDATINI_4_7.enabled = i_bVal
      .Page4.oPag.oCUDATFIN_4_9.enabled = i_bVal
      .Page4.oPag.oCU__ANNO_4_10.enabled = i_bVal
      .Page4.oPag.oCUDOMFIS_4_11.enabled = i_bVal
      .Page4.oPag.oCUSOGEST_4_12.enabled = i_bVal
      .Page4.oPag.oCUAGRCER_4_13.enabled = i_bVal
      .Page4.oPag.oCUNOMFIL_4_16.enabled = i_bVal
      .Page4.oPag.oCUTIPFOR_4_20.enabled = i_bVal
      .Page4.oPag.oCUTIPCAS_4_27.enabled = i_bVal
      .Page4.oPag.oCUCERSIN_4_28.enabled = i_bVal
      .Page4.oPag.oCUCERANN_4_33.enabled = i_bVal
      .Page4.oPag.oCUCERSOS_4_34.enabled = i_bVal
      .Page4.oPag.oEVEECC_2016_4_39.enabled = i_bVal
      .Page4.oPag.oEVEECC_2017_4_40.enabled = i_bVal
      .Page4.oPag.oBtn_4_19.enabled = i_bVal
      .Page4.oPag.oBtn_4_22.enabled = i_bVal
      .Page4.oPag.oBtn_4_24.enabled = .Page4.oPag.oBtn_4_24.mCond()
      .Page4.oPag.oBtn_4_29.enabled = i_bVal
      .Page4.oPag.oBtn_4_31.enabled = i_bVal
      .Page4.oPag.oBtn_4_36.enabled = .Page4.oPag.oBtn_4_36.mCond()
      .Page4.oPag.oBtn_4_38.enabled = i_bVal
      .Page4.oPag.oBtn_4_43.enabled = i_bVal
      .Page1.oPag.oObj_1_56.enabled = i_bVal
      if i_cOp = "Query"
        .Page1.oPag.oCUDATCER_1_2.enabled = .t.
      endif
    endwith
    this.Gsri_Mcu.SetStatus(i_cOp)
    this.Gsri_Mcr.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'GENCOMUN',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.Gsri_Mcu.SetChildrenStatus(i_cOp)
  *  this.Gsri_Mcr.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.GENCOMUN_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CUSERIAL,"CUSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CUDATCER,"CUDATCER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CUDESAGG,"CUDESAGG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CUCODFIS,"CUCODFIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CUCODFSI,"CUCODFSI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CUCOGNOM,"CUCOGNOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CU__NOME,"CU__NOME",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CURAGSOC,"CURAGSOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CUCOMUNE,"CUCOMUNE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CUPRORES,"CUPRORES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CU___CAP,"CU___CAP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CUINDIRI,"CUINDIRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CUNUMFAX,"CUNUMFAX",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CU__MAIL,"CU__MAIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CUDATFIR,"CUDATFIR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CUCODINT,"CUCODINT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CUIMPTRA,"CUIMPTRA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CUDATIMP,"CUDATIMP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CUFIRINT,"CUFIRINT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CURAPFIR,"CURAPFIR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CUCFISOT,"CUCFISOT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CUCODFED,"CUCODFED",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CUCODCAR,"CUCODCAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CUCOGSOT,"CUCOGSOT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CUNOMSOT,"CUNOMSOT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CURAPFAL,"CURAPFAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CUCODATT,"CUCODATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CUCODSED,"CUCODSED",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CUNUMCER,"CUNUMCER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CUFIRDIC,"CUFIRDIC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CUTIPCER,"CUTIPCER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CUDATINI,"CUDATINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CUDATFIN,"CUDATFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CU__ANNO,"CU__ANNO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CUDOMFIS,"CUDOMFIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CUSOGEST,"CUSOGEST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CUAGRCER,"CUAGRCER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CUEVEECC,"CUEVEECC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CUNOMFIL,"CUNOMFIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CUTIPFOR,"CUTIPFOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CUTIPCAS,"CUTIPCAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CUCERSIN,"CUCERSIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CUTIPCAS,"CUTIPCAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CUCERSIN,"CUCERSIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CUCERANN,"CUCERANN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CUCERSOS,"CUCERSOS",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.GENCOMUN_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.GENCOMUN_IDX,2])
    i_lTable = "GENCOMUN"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.GENCOMUN_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.GENCOMUN_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.GENCOMUN_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.GENCOMUN_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"CEUNI","i_codazi,w_Cuserial")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into GENCOMUN
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'GENCOMUN')
        i_extval=cp_InsertValODBCExtFlds(this,'GENCOMUN')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(CUSERIAL,CUDATCER,CUDESAGG,CUCODFIS,CUCODFSI"+;
                  ",CUCOGNOM,CU__NOME,CURAGSOC,CUCOMUNE,CUPRORES"+;
                  ",CU___CAP,CUINDIRI,CUNUMFAX,CU__MAIL,CUDATFIR"+;
                  ",CUCODINT,CUIMPTRA,CUDATIMP,CUFIRINT,CURAPFIR"+;
                  ",CUCFISOT,CUCODFED,CUCODCAR,CUCOGSOT,CUNOMSOT"+;
                  ",CURAPFAL,CUCODATT,CUCODSED,CUNUMCER,CUFIRDIC"+;
                  ",CUTIPCER,CUDATINI,CUDATFIN,CU__ANNO,CUDOMFIS"+;
                  ",CUSOGEST,CUAGRCER,CUEVEECC,CUNOMFIL,CUTIPFOR"+;
                  ",CUTIPCAS,CUCERSIN,CUCERANN,CUCERSOS "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_CUSERIAL)+;
                  ","+cp_ToStrODBC(this.w_CUDATCER)+;
                  ","+cp_ToStrODBC(this.w_CUDESAGG)+;
                  ","+cp_ToStrODBC(this.w_CUCODFIS)+;
                  ","+cp_ToStrODBC(this.w_CUCODFSI)+;
                  ","+cp_ToStrODBC(this.w_CUCOGNOM)+;
                  ","+cp_ToStrODBC(this.w_CU__NOME)+;
                  ","+cp_ToStrODBC(this.w_CURAGSOC)+;
                  ","+cp_ToStrODBC(this.w_CUCOMUNE)+;
                  ","+cp_ToStrODBC(this.w_CUPRORES)+;
                  ","+cp_ToStrODBC(this.w_CU___CAP)+;
                  ","+cp_ToStrODBC(this.w_CUINDIRI)+;
                  ","+cp_ToStrODBC(this.w_CUNUMFAX)+;
                  ","+cp_ToStrODBC(this.w_CU__MAIL)+;
                  ","+cp_ToStrODBC(this.w_CUDATFIR)+;
                  ","+cp_ToStrODBC(this.w_CUCODINT)+;
                  ","+cp_ToStrODBC(this.w_CUIMPTRA)+;
                  ","+cp_ToStrODBC(this.w_CUDATIMP)+;
                  ","+cp_ToStrODBC(this.w_CUFIRINT)+;
                  ","+cp_ToStrODBC(this.w_CURAPFIR)+;
                  ","+cp_ToStrODBC(this.w_CUCFISOT)+;
                  ","+cp_ToStrODBC(this.w_CUCODFED)+;
                  ","+cp_ToStrODBC(this.w_CUCODCAR)+;
                  ","+cp_ToStrODBC(this.w_CUCOGSOT)+;
                  ","+cp_ToStrODBC(this.w_CUNOMSOT)+;
                  ","+cp_ToStrODBC(this.w_CURAPFAL)+;
                  ","+cp_ToStrODBC(this.w_CUCODATT)+;
                  ","+cp_ToStrODBC(this.w_CUCODSED)+;
                  ","+cp_ToStrODBC(this.w_CUNUMCER)+;
                  ","+cp_ToStrODBC(this.w_CUFIRDIC)+;
                  ","+cp_ToStrODBC(this.w_CUTIPCER)+;
                  ","+cp_ToStrODBC(this.w_CUDATINI)+;
                  ","+cp_ToStrODBC(this.w_CUDATFIN)+;
                  ","+cp_ToStrODBC(this.w_CU__ANNO)+;
                  ","+cp_ToStrODBC(this.w_CUDOMFIS)+;
                  ","+cp_ToStrODBC(this.w_CUSOGEST)+;
                  ","+cp_ToStrODBC(this.w_CUAGRCER)+;
                  ","+cp_ToStrODBC(this.w_CUEVEECC)+;
                  ","+cp_ToStrODBC(this.w_CUNOMFIL)+;
                  ","+cp_ToStrODBC(this.w_CUTIPFOR)+;
                  ","+cp_ToStrODBC(this.w_CUTIPCAS)+;
                  ","+cp_ToStrODBC(this.w_CUCERSIN)+;
                  ","+cp_ToStrODBC(this.w_CUCERANN)+;
                  ","+cp_ToStrODBC(this.w_CUCERSOS)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'GENCOMUN')
        i_extval=cp_InsertValVFPExtFlds(this,'GENCOMUN')
        cp_CheckDeletedKey(i_cTable,0,'CUSERIAL',this.w_CUSERIAL)
        INSERT INTO (i_cTable);
              (CUSERIAL,CUDATCER,CUDESAGG,CUCODFIS,CUCODFSI,CUCOGNOM,CU__NOME,CURAGSOC,CUCOMUNE,CUPRORES,CU___CAP,CUINDIRI,CUNUMFAX,CU__MAIL,CUDATFIR,CUCODINT,CUIMPTRA,CUDATIMP,CUFIRINT,CURAPFIR,CUCFISOT,CUCODFED,CUCODCAR,CUCOGSOT,CUNOMSOT,CURAPFAL,CUCODATT,CUCODSED,CUNUMCER,CUFIRDIC,CUTIPCER,CUDATINI,CUDATFIN,CU__ANNO,CUDOMFIS,CUSOGEST,CUAGRCER,CUEVEECC,CUNOMFIL,CUTIPFOR,CUTIPCAS,CUCERSIN,CUCERANN,CUCERSOS  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_CUSERIAL;
                  ,this.w_CUDATCER;
                  ,this.w_CUDESAGG;
                  ,this.w_CUCODFIS;
                  ,this.w_CUCODFSI;
                  ,this.w_CUCOGNOM;
                  ,this.w_CU__NOME;
                  ,this.w_CURAGSOC;
                  ,this.w_CUCOMUNE;
                  ,this.w_CUPRORES;
                  ,this.w_CU___CAP;
                  ,this.w_CUINDIRI;
                  ,this.w_CUNUMFAX;
                  ,this.w_CU__MAIL;
                  ,this.w_CUDATFIR;
                  ,this.w_CUCODINT;
                  ,this.w_CUIMPTRA;
                  ,this.w_CUDATIMP;
                  ,this.w_CUFIRINT;
                  ,this.w_CURAPFIR;
                  ,this.w_CUCFISOT;
                  ,this.w_CUCODFED;
                  ,this.w_CUCODCAR;
                  ,this.w_CUCOGSOT;
                  ,this.w_CUNOMSOT;
                  ,this.w_CURAPFAL;
                  ,this.w_CUCODATT;
                  ,this.w_CUCODSED;
                  ,this.w_CUNUMCER;
                  ,this.w_CUFIRDIC;
                  ,this.w_CUTIPCER;
                  ,this.w_CUDATINI;
                  ,this.w_CUDATFIN;
                  ,this.w_CU__ANNO;
                  ,this.w_CUDOMFIS;
                  ,this.w_CUSOGEST;
                  ,this.w_CUAGRCER;
                  ,this.w_CUEVEECC;
                  ,this.w_CUNOMFIL;
                  ,this.w_CUTIPFOR;
                  ,this.w_CUTIPCAS;
                  ,this.w_CUCERSIN;
                  ,this.w_CUCERANN;
                  ,this.w_CUCERSOS;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.GENCOMUN_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.GENCOMUN_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.GENCOMUN_IDX,i_nConn)
      *
      * update GENCOMUN
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'GENCOMUN')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " CUDATCER="+cp_ToStrODBC(this.w_CUDATCER)+;
             ",CUDESAGG="+cp_ToStrODBC(this.w_CUDESAGG)+;
             ",CUCODFIS="+cp_ToStrODBC(this.w_CUCODFIS)+;
             ",CUCODFSI="+cp_ToStrODBC(this.w_CUCODFSI)+;
             ",CUCOGNOM="+cp_ToStrODBC(this.w_CUCOGNOM)+;
             ",CU__NOME="+cp_ToStrODBC(this.w_CU__NOME)+;
             ",CURAGSOC="+cp_ToStrODBC(this.w_CURAGSOC)+;
             ",CUCOMUNE="+cp_ToStrODBC(this.w_CUCOMUNE)+;
             ",CUPRORES="+cp_ToStrODBC(this.w_CUPRORES)+;
             ",CU___CAP="+cp_ToStrODBC(this.w_CU___CAP)+;
             ",CUINDIRI="+cp_ToStrODBC(this.w_CUINDIRI)+;
             ",CUNUMFAX="+cp_ToStrODBC(this.w_CUNUMFAX)+;
             ",CU__MAIL="+cp_ToStrODBC(this.w_CU__MAIL)+;
             ",CUDATFIR="+cp_ToStrODBC(this.w_CUDATFIR)+;
             ",CUCODINT="+cp_ToStrODBC(this.w_CUCODINT)+;
             ",CUIMPTRA="+cp_ToStrODBC(this.w_CUIMPTRA)+;
             ",CUDATIMP="+cp_ToStrODBC(this.w_CUDATIMP)+;
             ",CUFIRINT="+cp_ToStrODBC(this.w_CUFIRINT)+;
             ",CURAPFIR="+cp_ToStrODBC(this.w_CURAPFIR)+;
             ",CUCFISOT="+cp_ToStrODBC(this.w_CUCFISOT)+;
             ",CUCODFED="+cp_ToStrODBC(this.w_CUCODFED)+;
             ",CUCODCAR="+cp_ToStrODBC(this.w_CUCODCAR)+;
             ",CUCOGSOT="+cp_ToStrODBC(this.w_CUCOGSOT)+;
             ",CUNOMSOT="+cp_ToStrODBC(this.w_CUNOMSOT)+;
             ",CURAPFAL="+cp_ToStrODBC(this.w_CURAPFAL)+;
             ",CUCODATT="+cp_ToStrODBC(this.w_CUCODATT)+;
             ",CUCODSED="+cp_ToStrODBC(this.w_CUCODSED)+;
             ",CUNUMCER="+cp_ToStrODBC(this.w_CUNUMCER)+;
             ",CUFIRDIC="+cp_ToStrODBC(this.w_CUFIRDIC)+;
             ",CUTIPCER="+cp_ToStrODBC(this.w_CUTIPCER)+;
             ",CUDATINI="+cp_ToStrODBC(this.w_CUDATINI)+;
             ",CUDATFIN="+cp_ToStrODBC(this.w_CUDATFIN)+;
             ",CU__ANNO="+cp_ToStrODBC(this.w_CU__ANNO)+;
             ",CUDOMFIS="+cp_ToStrODBC(this.w_CUDOMFIS)+;
             ",CUSOGEST="+cp_ToStrODBC(this.w_CUSOGEST)+;
             ",CUAGRCER="+cp_ToStrODBC(this.w_CUAGRCER)+;
             ",CUEVEECC="+cp_ToStrODBC(this.w_CUEVEECC)+;
             ",CUNOMFIL="+cp_ToStrODBC(this.w_CUNOMFIL)+;
             ",CUTIPFOR="+cp_ToStrODBC(this.w_CUTIPFOR)+;
             ",CUTIPCAS="+cp_ToStrODBC(this.w_CUTIPCAS)+;
             ",CUCERSIN="+cp_ToStrODBC(this.w_CUCERSIN)+;
             ",CUCERANN="+cp_ToStrODBC(this.w_CUCERANN)+;
             ",CUCERSOS="+cp_ToStrODBC(this.w_CUCERSOS)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'GENCOMUN')
        i_cWhere = cp_PKFox(i_cTable  ,'CUSERIAL',this.w_CUSERIAL  )
        UPDATE (i_cTable) SET;
              CUDATCER=this.w_CUDATCER;
             ,CUDESAGG=this.w_CUDESAGG;
             ,CUCODFIS=this.w_CUCODFIS;
             ,CUCODFSI=this.w_CUCODFSI;
             ,CUCOGNOM=this.w_CUCOGNOM;
             ,CU__NOME=this.w_CU__NOME;
             ,CURAGSOC=this.w_CURAGSOC;
             ,CUCOMUNE=this.w_CUCOMUNE;
             ,CUPRORES=this.w_CUPRORES;
             ,CU___CAP=this.w_CU___CAP;
             ,CUINDIRI=this.w_CUINDIRI;
             ,CUNUMFAX=this.w_CUNUMFAX;
             ,CU__MAIL=this.w_CU__MAIL;
             ,CUDATFIR=this.w_CUDATFIR;
             ,CUCODINT=this.w_CUCODINT;
             ,CUIMPTRA=this.w_CUIMPTRA;
             ,CUDATIMP=this.w_CUDATIMP;
             ,CUFIRINT=this.w_CUFIRINT;
             ,CURAPFIR=this.w_CURAPFIR;
             ,CUCFISOT=this.w_CUCFISOT;
             ,CUCODFED=this.w_CUCODFED;
             ,CUCODCAR=this.w_CUCODCAR;
             ,CUCOGSOT=this.w_CUCOGSOT;
             ,CUNOMSOT=this.w_CUNOMSOT;
             ,CURAPFAL=this.w_CURAPFAL;
             ,CUCODATT=this.w_CUCODATT;
             ,CUCODSED=this.w_CUCODSED;
             ,CUNUMCER=this.w_CUNUMCER;
             ,CUFIRDIC=this.w_CUFIRDIC;
             ,CUTIPCER=this.w_CUTIPCER;
             ,CUDATINI=this.w_CUDATINI;
             ,CUDATFIN=this.w_CUDATFIN;
             ,CU__ANNO=this.w_CU__ANNO;
             ,CUDOMFIS=this.w_CUDOMFIS;
             ,CUSOGEST=this.w_CUSOGEST;
             ,CUAGRCER=this.w_CUAGRCER;
             ,CUEVEECC=this.w_CUEVEECC;
             ,CUNOMFIL=this.w_CUNOMFIL;
             ,CUTIPFOR=this.w_CUTIPFOR;
             ,CUTIPCAS=this.w_CUTIPCAS;
             ,CUCERSIN=this.w_CUCERSIN;
             ,CUCERANN=this.w_CUCERANN;
             ,CUCERSOS=this.w_CUCERSOS;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- Gsri_Mcu : Saving
      this.Gsri_Mcu.ChangeRow(this.cRowID+'      1',0;
             ,this.w_CUSERIAL,"CDSERIAL";
             )
      this.Gsri_Mcu.mReplace()
      * --- Gsri_Mcr : Saving
      this.Gsri_Mcr.ChangeRow(this.cRowID+'      1',0;
             ,this.w_CUSERIAL,"GDSERIAL";
             )
      this.Gsri_Mcr.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- Gsri_Mcu : Deleting
    this.Gsri_Mcu.ChangeRow(this.cRowID+'      1',0;
           ,this.w_CUSERIAL,"CDSERIAL";
           )
    this.Gsri_Mcu.mDelete()
    * --- Gsri_Mcr : Deleting
    this.Gsri_Mcr.ChangeRow(this.cRowID+'      1',0;
           ,this.w_CUSERIAL,"GDSERIAL";
           )
    this.Gsri_Mcr.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.GENCOMUN_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.GENCOMUN_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.GENCOMUN_IDX,i_nConn)
      *
      * delete GENCOMUN
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'CUSERIAL',this.w_CUSERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.GENCOMUN_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.GENCOMUN_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,3,.t.)
          .link_1_4('Full')
        .DoRTCalc(5,12,.t.)
          .link_1_13('Full')
        .DoRTCalc(14,33,.t.)
        if .o_CUCODINT<>.w_CUCODINT
            .w_CUIMPTRA = '1'
        endif
        .DoRTCalc(35,37,.t.)
          .link_2_2('Full')
        .DoRTCalc(39,42,.t.)
        if .o_CuRapFir<>.w_CuRapFir
            .w_CUCFISOT = iif(.w_CuRapFir='S',.w_Rfcodfis,Space(16))
        endif
        if .o_CuRapFir<>.w_CuRapFir
            .w_CUCODFED = Space(11)
        endif
        if .o_CuRapFir<>.w_CuRapFir
            .w_CUCODCAR = iif(.w_CuRapFir='N','1',.w_Rfcodcar)
        endif
        if .o_CuRapFir<>.w_CuRapFir
            .w_CUCOGSOT = iif(.w_CuRapFir='N',space(20),Left(.w_Rfcognom,24))
        endif
        if .o_CuRapFir<>.w_CuRapFir
            .w_CUNOMSOT = iif(.w_CuRapFir='N',space(20),Left(.w_Rf__Nome,20))
        endif
        if .o_CUDATINI<>.w_CUDATINI.or. .o_CUNOMFIL<>.w_CUNOMFIL.or. .o_CUCODFIS<>.w_CUCODFIS
          .Calculate_ZJFMMVWDUN()
        endif
        .DoRTCalc(48,58,.t.)
        if .o_Cutipcas<>.w_Cutipcas.or. .o_Cucersin<>.w_Cucersin.or. .o_CuAgrCer<>.w_CuAgrCer
            .w_CUSOGEST = 'N'
        endif
        if .o_Cutipcas<>.w_Cutipcas.or. .o_Cucersin<>.w_Cucersin
            .w_CUAGRCER = 'N'
        endif
        .DoRTCalc(61,66,.t.)
        if .o_Cutipcas<>.w_Cutipcas
            .w_CUCERSIN = iif(.w_Cutipcas $ 'SA','S','N')
        endif
        if .o_Cutipcas<>.w_Cutipcas.or. .o_Cucersin<>.w_Cucersin
          .Calculate_QHCWEBEERF()
        endif
        .DoRTCalc(68,68,.t.)
            .w_TIPCER = THIS.pTipCer
        .oPgFrm.Page1.oPag.oObj_1_56.Calculate()
        .DoRTCalc(70,74,.t.)
        if .o_CU__ANNO<>.w_CU__ANNO
            .w_EVEECC_2016 = '0'
        endif
        if .o_CU__ANNO<>.w_CU__ANNO
            .w_EVEECC_2017 = '0'
        endif
        if .o_CU__ANNO<>.w_CU__ANNO.or. .o_EVEECC_2016<>.w_EVEECC_2016.or. .o_EVEECC_2017<>.w_EVEECC_2017
          .Calculate_GEKLRYYVGZ()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_codazi<>.w_codazi
           cp_AskTableProg(this,i_nConn,"CEUNI","i_codazi,w_Cuserial")
          .op_Cuserial = .w_Cuserial
        endif
        .op_codazi = .w_codazi
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_56.Calculate()
    endwith
  return

  proc Calculate_ZJFMMVWDUN()
    with this
          * --- Inizializzo nome file
          .w_CUNOMFIL = iif(not empty(.w_CUNOMFIL), .w_CUNOMFIL, left(.w_DirName+Alltrim(.w_Cucodfis)+'_'+Substr(.w_Cuserial,2,9)+'.CUR'+space(254),254))
    endwith
  endproc
  proc Calculate_QHCWEBEERF()
    with this
          * --- Chiusura cursore 
          Gsri_Bcu(this;
              ,'C';
             )
    endwith
  endproc
  proc Calculate_GEKLRYYVGZ()
    with this
          * --- Valorizzo campo CUEVEECC
          .w_CUEVEECC = iif(.w_CU__ANNO<=2016,.w_EVEECC_2016,iif(.w_CU__ANNO>=2017,.w_EVEECC_2017,'0'))
    endwith
  endproc
  proc Calculate_LFLBLFKCYE()
    with this
          * --- Valorizzo eventi eccezionali
          .w_EVEECC_2016 = .w_CUEVEECC
          .w_EVEECC_2017 = .w_CUEVEECC
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCUCOGNOM_1_23.enabled = this.oPgFrm.Page1.oPag.oCUCOGNOM_1_23.mCond()
    this.oPgFrm.Page1.oPag.oCU__NOME_1_24.enabled = this.oPgFrm.Page1.oPag.oCU__NOME_1_24.mCond()
    this.oPgFrm.Page1.oPag.oCURAGSOC_1_25.enabled = this.oPgFrm.Page1.oPag.oCURAGSOC_1_25.mCond()
    this.oPgFrm.Page2.oPag.oCUCFISOT_2_9.enabled = this.oPgFrm.Page2.oPag.oCUCFISOT_2_9.mCond()
    this.oPgFrm.Page2.oPag.oCUCODFED_2_11.enabled = this.oPgFrm.Page2.oPag.oCUCODFED_2_11.mCond()
    this.oPgFrm.Page2.oPag.oCUCODCAR_2_12.enabled = this.oPgFrm.Page2.oPag.oCUCODCAR_2_12.mCond()
    this.oPgFrm.Page2.oPag.oCUCOGSOT_2_14.enabled = this.oPgFrm.Page2.oPag.oCUCOGSOT_2_14.mCond()
    this.oPgFrm.Page2.oPag.oCUNOMSOT_2_15.enabled = this.oPgFrm.Page2.oPag.oCUNOMSOT_2_15.mCond()
    this.oPgFrm.Page4.oPag.oCU__ANNO_4_10.enabled = this.oPgFrm.Page4.oPag.oCU__ANNO_4_10.mCond()
    this.oPgFrm.Page4.oPag.oCUSOGEST_4_12.enabled = this.oPgFrm.Page4.oPag.oCUSOGEST_4_12.mCond()
    this.oPgFrm.Page4.oPag.oCUAGRCER_4_13.enabled = this.oPgFrm.Page4.oPag.oCUAGRCER_4_13.mCond()
    this.oPgFrm.Page4.oPag.oCUTIPCAS_4_27.enabled = this.oPgFrm.Page4.oPag.oCUTIPCAS_4_27.mCond()
    this.oPgFrm.Page4.oPag.oCUCERSIN_4_28.enabled = this.oPgFrm.Page4.oPag.oCUCERSIN_4_28.mCond()
    this.oPgFrm.Page4.oPag.oCUCERANN_4_33.enabled = this.oPgFrm.Page4.oPag.oCUCERANN_4_33.mCond()
    this.oPgFrm.Page4.oPag.oCUCERSOS_4_34.enabled = this.oPgFrm.Page4.oPag.oCUCERSOS_4_34.mCond()
    this.oPgFrm.Page4.oPag.oEVEECC_2017_4_40.enabled = this.oPgFrm.Page4.oPag.oEVEECC_2017_4_40.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_22.enabled = this.oPgFrm.Page4.oPag.oBtn_4_22.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_24.enabled = this.oPgFrm.Page4.oPag.oBtn_4_24.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_31.enabled = this.oPgFrm.Page4.oPag.oBtn_4_31.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_36.enabled = this.oPgFrm.Page4.oPag.oBtn_4_36.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_38.enabled = this.oPgFrm.Page4.oPag.oBtn_4_38.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_43.enabled = this.oPgFrm.Page4.oPag.oBtn_4_43.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page3.oPag.oStr_3_11.visible=!this.oPgFrm.Page3.oPag.oStr_3_11.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_6.visible=!this.oPgFrm.Page4.oPag.oStr_4_6.mHide()
    this.oPgFrm.Page4.oPag.oCUDATINI_4_7.visible=!this.oPgFrm.Page4.oPag.oCUDATINI_4_7.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_8.visible=!this.oPgFrm.Page4.oPag.oStr_4_8.mHide()
    this.oPgFrm.Page4.oPag.oCUDATFIN_4_9.visible=!this.oPgFrm.Page4.oPag.oCUDATFIN_4_9.mHide()
    this.oPgFrm.Page4.oPag.oCU__ANNO_4_10.visible=!this.oPgFrm.Page4.oPag.oCU__ANNO_4_10.mHide()
    this.oPgFrm.Page4.oPag.oCUDOMFIS_4_11.visible=!this.oPgFrm.Page4.oPag.oCUDOMFIS_4_11.mHide()
    this.oPgFrm.Page4.oPag.oCUSOGEST_4_12.visible=!this.oPgFrm.Page4.oPag.oCUSOGEST_4_12.mHide()
    this.oPgFrm.Page4.oPag.oCUAGRCER_4_13.visible=!this.oPgFrm.Page4.oPag.oCUAGRCER_4_13.mHide()
    this.oPgFrm.Page4.oPag.oBtn_4_22.visible=!this.oPgFrm.Page4.oPag.oBtn_4_22.mHide()
    this.oPgFrm.Page4.oPag.oLinkPC_4_23.visible=!this.oPgFrm.Page4.oPag.oLinkPC_4_23.mHide()
    this.oPgFrm.Page4.oPag.oBtn_4_24.visible=!this.oPgFrm.Page4.oPag.oBtn_4_24.mHide()
    this.oPgFrm.Page4.oPag.oCUTIPCAS_4_27.visible=!this.oPgFrm.Page4.oPag.oCUTIPCAS_4_27.mHide()
    this.oPgFrm.Page4.oPag.oCUCERSIN_4_28.visible=!this.oPgFrm.Page4.oPag.oCUCERSIN_4_28.mHide()
    this.oPgFrm.Page4.oPag.oBtn_4_29.visible=!this.oPgFrm.Page4.oPag.oBtn_4_29.mHide()
    this.oPgFrm.Page4.oPag.oBtn_4_31.visible=!this.oPgFrm.Page4.oPag.oBtn_4_31.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_32.visible=!this.oPgFrm.Page4.oPag.oStr_4_32.mHide()
    this.oPgFrm.Page4.oPag.oCUCERANN_4_33.visible=!this.oPgFrm.Page4.oPag.oCUCERANN_4_33.mHide()
    this.oPgFrm.Page4.oPag.oCUCERSOS_4_34.visible=!this.oPgFrm.Page4.oPag.oCUCERSOS_4_34.mHide()
    this.oPgFrm.Page4.oPag.oLinkPC_4_35.visible=!this.oPgFrm.Page4.oPag.oLinkPC_4_35.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_17.visible=!this.oPgFrm.Page3.oPag.oStr_3_17.mHide()
    this.oPgFrm.Page4.oPag.oBtn_4_36.visible=!this.oPgFrm.Page4.oPag.oBtn_4_36.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_37.visible=!this.oPgFrm.Page4.oPag.oStr_4_37.mHide()
    this.oPgFrm.Page4.oPag.oBtn_4_38.visible=!this.oPgFrm.Page4.oPag.oBtn_4_38.mHide()
    this.oPgFrm.Page4.oPag.oEVEECC_2016_4_39.visible=!this.oPgFrm.Page4.oPag.oEVEECC_2016_4_39.mHide()
    this.oPgFrm.Page4.oPag.oEVEECC_2017_4_40.visible=!this.oPgFrm.Page4.oPag.oEVEECC_2017_4_40.mHide()
    this.oPgFrm.Page4.oPag.oBtn_4_43.visible=!this.oPgFrm.Page4.oPag.oBtn_4_43.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("New record")
          .Calculate_ZJFMMVWDUN()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Edit Aborted") or lower(cEvent)==lower("Blank")
          .Calculate_QHCWEBEERF()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_56.Event(cEvent)
        if lower(cEvent)==lower("Blank")
          .Calculate_GEKLRYYVGZ()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Load")
          .Calculate_LFLBLFKCYE()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZCOFAZI,AZTELFAX,AZ_EMAIL,AZPERAZI,AZLOCAZI,AZPROAZI,AZCAPAZI,AZINDAZI";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_CODAZI)
            select AZCODAZI,AZCOFAZI,AZTELFAX,AZ_EMAIL,AZPERAZI,AZLOCAZI,AZPROAZI,AZCAPAZI,AZINDAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.AZCODAZI,space(5))
      this.w_AZCOFAZI = NVL(_Link_.AZCOFAZI,space(16))
      this.w_AZTELFAX = NVL(_Link_.AZTELFAX,space(18))
      this.w_AZ_EMAIL = NVL(_Link_.AZ_EMAIL,space(50))
      this.w_AZPERAZI = NVL(_Link_.AZPERAZI,space(1))
      this.w_AZLOCAZI = NVL(_Link_.AZLOCAZI,space(30))
      this.w_AZPROAZI = NVL(_Link_.AZPROAZI,space(2))
      this.w_AZCAPAZI = NVL(_Link_.AZCAPAZI,space(9))
      this.w_AZINDAZI = NVL(_Link_.AZINDAZI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_AZCOFAZI = space(16)
      this.w_AZTELFAX = space(18)
      this.w_AZ_EMAIL = space(50)
      this.w_AZPERAZI = space(1)
      this.w_AZLOCAZI = space(30)
      this.w_AZPROAZI = space(2)
      this.w_AZCAPAZI = space(9)
      this.w_AZINDAZI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODAZI2
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TITOLARI_IDX,3]
    i_lTable = "TITOLARI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TITOLARI_IDX,2], .t., this.TITOLARI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TITOLARI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TTCODAZI,TTCOGTIT,TTNOMTIT,TTLOCTIT,TTPROTIT,TTCAPTIT,TTINDIRI,TTTELEFO";
                   +" from "+i_cTable+" "+i_lTable+" where TTCODAZI="+cp_ToStrODBC(this.w_CODAZI2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TTCODAZI',this.w_CODAZI2)
            select TTCODAZI,TTCOGTIT,TTNOMTIT,TTLOCTIT,TTPROTIT,TTCAPTIT,TTINDIRI,TTTELEFO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI2 = NVL(_Link_.TTCODAZI,space(5))
      this.w_COGTIT1 = NVL(_Link_.TTCOGTIT,space(25))
      this.w_NOMTIT1 = NVL(_Link_.TTNOMTIT,space(25))
      this.w_TTLOCTIT = NVL(_Link_.TTLOCTIT,space(30))
      this.w_TTPROTIT = NVL(_Link_.TTPROTIT,space(2))
      this.w_TTCAPTIT = NVL(_Link_.TTCAPTIT,space(9))
      this.w_TTINDIRI = NVL(_Link_.TTINDIRI,space(35))
      this.w_TTTELEFO = NVL(_Link_.TTTELEFO,space(18))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI2 = space(5)
      endif
      this.w_COGTIT1 = space(25)
      this.w_NOMTIT1 = space(25)
      this.w_TTLOCTIT = space(30)
      this.w_TTPROTIT = space(2)
      this.w_TTCAPTIT = space(9)
      this.w_TTINDIRI = space(35)
      this.w_TTTELEFO = space(18)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TITOLARI_IDX,2])+'\'+cp_ToStr(_Link_.TTCODAZI,1)
      cp_ShowWarn(i_cKey,this.TITOLARI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RAPFIRM
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DAT_RAPP_IDX,3]
    i_lTable = "DAT_RAPP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DAT_RAPP_IDX,2], .t., this.DAT_RAPP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DAT_RAPP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RAPFIRM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RAPFIRM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RFCODAZI,RFCODFIS,RFCOGNOM,RF__NOME,RFCODCAR";
                   +" from "+i_cTable+" "+i_lTable+" where RFCODAZI="+cp_ToStrODBC(this.w_RAPFIRM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RFCODAZI',this.w_RAPFIRM)
            select RFCODAZI,RFCODFIS,RFCOGNOM,RF__NOME,RFCODCAR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RAPFIRM = NVL(_Link_.RFCODAZI,space(10))
      this.w_RFCODFIS = NVL(_Link_.RFCODFIS,space(16))
      this.w_RFCOGNOM = NVL(_Link_.RFCOGNOM,space(40))
      this.w_RF__NOME = NVL(_Link_.RF__NOME,space(40))
      this.w_RFCODCAR = NVL(_Link_.RFCODCAR,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_RAPFIRM = space(10)
      endif
      this.w_RFCODFIS = space(16)
      this.w_RFCOGNOM = space(40)
      this.w_RF__NOME = space(40)
      this.w_RFCODCAR = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DAT_RAPP_IDX,2])+'\'+cp_ToStr(_Link_.RFCODAZI,1)
      cp_ShowWarn(i_cKey,this.DAT_RAPP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RAPFIRM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCUDATCER_1_2.value==this.w_CUDATCER)
      this.oPgFrm.Page1.oPag.oCUDATCER_1_2.value=this.w_CUDATCER
    endif
    if not(this.oPgFrm.Page1.oPag.oCUDESAGG_1_3.value==this.w_CUDESAGG)
      this.oPgFrm.Page1.oPag.oCUDESAGG_1_3.value=this.w_CUDESAGG
    endif
    if not(this.oPgFrm.Page1.oPag.oCUCODFIS_1_21.value==this.w_CUCODFIS)
      this.oPgFrm.Page1.oPag.oCUCODFIS_1_21.value=this.w_CUCODFIS
    endif
    if not(this.oPgFrm.Page1.oPag.oCUCOGNOM_1_23.value==this.w_CUCOGNOM)
      this.oPgFrm.Page1.oPag.oCUCOGNOM_1_23.value=this.w_CUCOGNOM
    endif
    if not(this.oPgFrm.Page1.oPag.oCU__NOME_1_24.value==this.w_CU__NOME)
      this.oPgFrm.Page1.oPag.oCU__NOME_1_24.value=this.w_CU__NOME
    endif
    if not(this.oPgFrm.Page1.oPag.oCURAGSOC_1_25.value==this.w_CURAGSOC)
      this.oPgFrm.Page1.oPag.oCURAGSOC_1_25.value=this.w_CURAGSOC
    endif
    if not(this.oPgFrm.Page1.oPag.oCUCOMUNE_1_26.value==this.w_CUCOMUNE)
      this.oPgFrm.Page1.oPag.oCUCOMUNE_1_26.value=this.w_CUCOMUNE
    endif
    if not(this.oPgFrm.Page1.oPag.oCUPRORES_1_27.value==this.w_CUPRORES)
      this.oPgFrm.Page1.oPag.oCUPRORES_1_27.value=this.w_CUPRORES
    endif
    if not(this.oPgFrm.Page1.oPag.oCU___CAP_1_28.value==this.w_CU___CAP)
      this.oPgFrm.Page1.oPag.oCU___CAP_1_28.value=this.w_CU___CAP
    endif
    if not(this.oPgFrm.Page1.oPag.oCUINDIRI_1_29.value==this.w_CUINDIRI)
      this.oPgFrm.Page1.oPag.oCUINDIRI_1_29.value=this.w_CUINDIRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCUNUMFAX_1_30.value==this.w_CUNUMFAX)
      this.oPgFrm.Page1.oPag.oCUNUMFAX_1_30.value=this.w_CUNUMFAX
    endif
    if not(this.oPgFrm.Page1.oPag.oCU__MAIL_1_31.value==this.w_CU__MAIL)
      this.oPgFrm.Page1.oPag.oCU__MAIL_1_31.value=this.w_CU__MAIL
    endif
    if not(this.oPgFrm.Page3.oPag.oCUDATFIR_3_3.value==this.w_CUDATFIR)
      this.oPgFrm.Page3.oPag.oCUDATFIR_3_3.value=this.w_CUDATFIR
    endif
    if not(this.oPgFrm.Page3.oPag.oCUCODINT_3_4.value==this.w_CUCODINT)
      this.oPgFrm.Page3.oPag.oCUCODINT_3_4.value=this.w_CUCODINT
    endif
    if not(this.oPgFrm.Page3.oPag.oCUIMPTRA_3_5.RadioValue()==this.w_CUIMPTRA)
      this.oPgFrm.Page3.oPag.oCUIMPTRA_3_5.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oCUDATIMP_3_7.value==this.w_CUDATIMP)
      this.oPgFrm.Page3.oPag.oCUDATIMP_3_7.value=this.w_CUDATIMP
    endif
    if not(this.oPgFrm.Page3.oPag.oCUFIRINT_3_10.RadioValue()==this.w_CUFIRINT)
      this.oPgFrm.Page3.oPag.oCUFIRINT_3_10.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCURAPFIR_2_1.RadioValue()==this.w_CURAPFIR)
      this.oPgFrm.Page2.oPag.oCURAPFIR_2_1.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCUCFISOT_2_9.value==this.w_CUCFISOT)
      this.oPgFrm.Page2.oPag.oCUCFISOT_2_9.value=this.w_CUCFISOT
    endif
    if not(this.oPgFrm.Page2.oPag.oCUCODFED_2_11.value==this.w_CUCODFED)
      this.oPgFrm.Page2.oPag.oCUCODFED_2_11.value=this.w_CUCODFED
    endif
    if not(this.oPgFrm.Page2.oPag.oCUCODCAR_2_12.RadioValue()==this.w_CUCODCAR)
      this.oPgFrm.Page2.oPag.oCUCODCAR_2_12.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCUCOGSOT_2_14.value==this.w_CUCOGSOT)
      this.oPgFrm.Page2.oPag.oCUCOGSOT_2_14.value=this.w_CUCOGSOT
    endif
    if not(this.oPgFrm.Page2.oPag.oCUNOMSOT_2_15.value==this.w_CUNOMSOT)
      this.oPgFrm.Page2.oPag.oCUNOMSOT_2_15.value=this.w_CUNOMSOT
    endif
    if not(this.oPgFrm.Page1.oPag.oCUCODATT_1_44.value==this.w_CUCODATT)
      this.oPgFrm.Page1.oPag.oCUCODATT_1_44.value=this.w_CUCODATT
    endif
    if not(this.oPgFrm.Page1.oPag.oCUCODSED_1_46.value==this.w_CUCODSED)
      this.oPgFrm.Page1.oPag.oCUCODSED_1_46.value=this.w_CUCODSED
    endif
    if not(this.oPgFrm.Page3.oPag.oCUNUMCER_3_12.value==this.w_CUNUMCER)
      this.oPgFrm.Page3.oPag.oCUNUMCER_3_12.value=this.w_CUNUMCER
    endif
    if not(this.oPgFrm.Page3.oPag.oCUFIRDIC_3_13.RadioValue()==this.w_CUFIRDIC)
      this.oPgFrm.Page3.oPag.oCUFIRDIC_3_13.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oCUDATINI_4_7.value==this.w_CUDATINI)
      this.oPgFrm.Page4.oPag.oCUDATINI_4_7.value=this.w_CUDATINI
    endif
    if not(this.oPgFrm.Page4.oPag.oCUDATFIN_4_9.value==this.w_CUDATFIN)
      this.oPgFrm.Page4.oPag.oCUDATFIN_4_9.value=this.w_CUDATFIN
    endif
    if not(this.oPgFrm.Page4.oPag.oCU__ANNO_4_10.value==this.w_CU__ANNO)
      this.oPgFrm.Page4.oPag.oCU__ANNO_4_10.value=this.w_CU__ANNO
    endif
    if not(this.oPgFrm.Page4.oPag.oCUDOMFIS_4_11.RadioValue()==this.w_CUDOMFIS)
      this.oPgFrm.Page4.oPag.oCUDOMFIS_4_11.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oCUSOGEST_4_12.RadioValue()==this.w_CUSOGEST)
      this.oPgFrm.Page4.oPag.oCUSOGEST_4_12.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oCUAGRCER_4_13.RadioValue()==this.w_CUAGRCER)
      this.oPgFrm.Page4.oPag.oCUAGRCER_4_13.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oCUNOMFIL_4_16.value==this.w_CUNOMFIL)
      this.oPgFrm.Page4.oPag.oCUNOMFIL_4_16.value=this.w_CUNOMFIL
    endif
    if not(this.oPgFrm.Page4.oPag.oCUTIPFOR_4_20.RadioValue()==this.w_CUTIPFOR)
      this.oPgFrm.Page4.oPag.oCUTIPFOR_4_20.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oCUTIPCAS_4_27.RadioValue()==this.w_CUTIPCAS)
      this.oPgFrm.Page4.oPag.oCUTIPCAS_4_27.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oCUCERSIN_4_28.RadioValue()==this.w_CUCERSIN)
      this.oPgFrm.Page4.oPag.oCUCERSIN_4_28.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oCUCERANN_4_33.RadioValue()==this.w_CUCERANN)
      this.oPgFrm.Page4.oPag.oCUCERANN_4_33.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oCUCERSOS_4_34.RadioValue()==this.w_CUCERSOS)
      this.oPgFrm.Page4.oPag.oCUCERSOS_4_34.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oEVEECC_2016_4_39.RadioValue()==this.w_EVEECC_2016)
      this.oPgFrm.Page4.oPag.oEVEECC_2016_4_39.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oEVEECC_2017_4_40.RadioValue()==this.w_EVEECC_2017)
      this.oPgFrm.Page4.oPag.oEVEECC_2017_4_40.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'GENCOMUN')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CUDATCER))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCUDATCER_1_2.SetFocus()
            i_bnoObbl = !empty(.w_CUDATCER)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_CUCODFIS)) or not(iif(.w_AZPERAZI = 'S',chkcfp(alltrim(.w_CUCODFIS),'CF'),chkcfp(alltrim(.w_CUCODFIS),'PI'))))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCUCODFIS_1_21.SetFocus()
            i_bnoObbl = !empty(.w_CUCODFIS)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CUCOGNOM))  and (.w_AZPERAZI='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCUCOGNOM_1_23.SetFocus()
            i_bnoObbl = !empty(.w_CUCOGNOM)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CU__NOME))  and (.w_AZPERAZI='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCU__NOME_1_24.SetFocus()
            i_bnoObbl = !empty(.w_CU__NOME)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CURAGSOC))  and (.w_AZPERAZI<>'S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCURAGSOC_1_25.SetFocus()
            i_bnoObbl = !empty(.w_CURAGSOC)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CUCOMUNE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCUCOMUNE_1_26.SetFocus()
            i_bnoObbl = !empty(.w_CUCOMUNE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CUPRORES))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCUPRORES_1_27.SetFocus()
            i_bnoObbl = !empty(.w_CUPRORES)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(at(' ',alltrim(.w_Cunumfax))=0 and at('/',alltrim(.w_Cunumfax))=0 and at('-',alltrim(.w_Cunumfax))=0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCUNUMFAX_1_30.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Non inserire caratteri separatori tra prefisso e numero")
          case   (empty(.w_CUDATFIR))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oCUDATFIR_3_3.SetFocus()
            i_bnoObbl = !empty(.w_CUDATFIR)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(iif(not empty(.w_Cucodint),chkcfp(alltrim(.w_Cucodint),'CF'),.T.))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oCUCODINT_3_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_CUCFISOT)) or not(iif(not empty(.w_Cucfisot),chkcfp(alltrim(.w_Cucfisot),'CF'),.T.)))  and (.w_CuRapFir='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCUCFISOT_2_9.SetFocus()
            i_bnoObbl = !empty(.w_CUCFISOT)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(iif(Not Empty(.w_Cucodfed),Chkcfp(.w_CUCODFED,"PI", "", "", ""),.t.))  and (.w_CuRapFir='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCUCODFED_2_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CUCODCAR))  and (.w_CuRapFir='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCUCODCAR_2_12.SetFocus()
            i_bnoObbl = !empty(.w_CUCODCAR)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CUCOGSOT))  and (.w_CuRapFir='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCUCOGSOT_2_14.SetFocus()
            i_bnoObbl = !empty(.w_CUCOGSOT)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CUNOMSOT))  and (.w_CuRapFir='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCUNOMSOT_2_15.SetFocus()
            i_bnoObbl = !empty(.w_CUNOMSOT)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_CUDATINI)) or not(.w_Cudatfin>=.w_Cudatini And .w_Cudatini > Date(2013,12,31)))  and not(.w_CUTIPCER<>'CUR15')
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oCUDATINI_4_7.SetFocus()
            i_bnoObbl = !empty(.w_CUDATINI)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � maggiore della data finale o fuori periodo")
          case   ((empty(.w_CUDATFIN)) or not(.w_Cudatfin>=.w_Cudatini And .w_Cudatfin<Date(2015,01,01)))  and not(.w_CUTIPCER<>'CUR15')
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oCUDATFIN_4_9.SetFocus()
            i_bnoObbl = !empty(.w_CUDATFIN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � maggiore della data finale o fuori periodo")
          case   ((empty(.w_CU__ANNO)) or not(.w_CU__ANNO>=2015))  and not(.w_CUTIPCER='CUR15')  and (.cFunction='Load')
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oCU__ANNO_4_10.SetFocus()
            i_bnoObbl = !empty(.w_CU__ANNO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_EVEECC_2016))  and not(.w_CUTIPCER<>'CUR16' Or .w_CU__ANNO<2016 Or .w_CU__ANNO>=2017)
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oEVEECC_2016_4_39.SetFocus()
            i_bnoObbl = !empty(.w_EVEECC_2016)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_EVEECC_2017))  and not(.w_CUTIPCER<>'CUR16' Or .w_CU__ANNO<2017)  and (.cFunction<>'Query' )
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oEVEECC_2017_4_40.SetFocus()
            i_bnoObbl = !empty(.w_EVEECC_2017)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      *i_bRes = i_bRes .and. .Gsri_Mcu.CheckForm()
      if i_bres
        i_bres=  .Gsri_Mcu.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=4
        endif
      endif
      *i_bRes = i_bRes .and. .Gsri_Mcr.CheckForm()
      if i_bres
        i_bres=  .Gsri_Mcr.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=4
        endif
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CUCODFIS = this.w_CUCODFIS
    this.o_CUCODINT = this.w_CUCODINT
    this.o_CURAPFIR = this.w_CURAPFIR
    this.o_CUCODCAR = this.w_CUCODCAR
    this.o_CUDATINI = this.w_CUDATINI
    this.o_CU__ANNO = this.w_CU__ANNO
    this.o_CUAGRCER = this.w_CUAGRCER
    this.o_CUNOMFIL = this.w_CUNOMFIL
    this.o_CUTIPFOR = this.w_CUTIPFOR
    this.o_CUTIPCAS = this.w_CUTIPCAS
    this.o_CUCERSIN = this.w_CUCERSIN
    this.o_EVEECC_2016 = this.w_EVEECC_2016
    this.o_EVEECC_2017 = this.w_EVEECC_2017
    * --- Gsri_Mcu : Depends On
    this.Gsri_Mcu.SaveDependsOn()
    * --- Gsri_Mcr : Depends On
    this.Gsri_Mcr.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgsri_acuPag1 as StdContainer
  Width  = 786
  height = 429
  stdWidth  = 786
  stdheight = 429
  resizeXpos=469
  resizeYpos=203
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCUDATCER_1_2 as StdField with uid="TWFLVODDQH",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CUDATCER", cQueryName = "CUDATCER",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di generazione della certificazione",;
    HelpContextID = 33268600,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=176, Top=17

  add object oCUDESAGG_1_3 as StdField with uid="ZJAXFDWJCR",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CUDESAGG", cQueryName = "CUDESAGG",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Campo che accoglie le note aggiuntive",;
    HelpContextID = 267363181,;
   bGlobalFont=.t.,;
    Height=21, Width=601, Left=176, Top=51, InputMask=replicate('X',254)

  add object oCUCODFIS_1_21 as StdField with uid="DFQWHMDFGZ",rtseq=21,rtrep=.f.,;
    cFormVar = "w_CUCODFIS", cQueryName = "CUCODFIS",;
    bObbl = .t. , nPag = 1, value=space(16), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale del sostituto d'imposta",;
    HelpContextID = 200699015,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=238, Top=110, cSayPict='REPLICATE("!",16)', cGetPict='REPLICATE("!",16)', InputMask=replicate('X',16)

  func oCUCODFIS_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (iif(.w_AZPERAZI = 'S',chkcfp(alltrim(.w_CUCODFIS),'CF'),chkcfp(alltrim(.w_CUCODFIS),'PI')))
    endwith
    return bRes
  endfunc

  add object oCUCOGNOM_1_23 as StdField with uid="USTYABUOUZ",rtseq=23,rtrep=.f.,;
    cFormVar = "w_CUCOGNOM", cQueryName = "CUCOGNOM",;
    bObbl = .t. , nPag = 1, value=space(24), bMultilanguage =  .f.,;
    ToolTipText = "Cognome",;
    HelpContextID = 63335565,;
   bGlobalFont=.t.,;
    Height=21, Width=209, Left=177, Top=151, cSayPict="REPL('!',24)", cGetPict="REPL('!',24)", InputMask=replicate('X',24)

  func oCUCOGNOM_1_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZPERAZI='S')
    endwith
   endif
  endfunc

  add object oCU__NOME_1_24 as StdField with uid="LKFBHLXRZV",rtseq=24,rtrep=.f.,;
    cFormVar = "w_CU__NOME", cQueryName = "CU__NOME",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Nome",;
    HelpContextID = 38055061,;
   bGlobalFont=.t.,;
    Height=21, Width=174, Left=533, Top=151, cSayPict="REPL('!',20)", cGetPict="REPL('!',20)", InputMask=replicate('X',20)

  func oCU__NOME_1_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZPERAZI='S')
    endwith
   endif
  endfunc

  add object oCURAGSOC_1_25 as StdField with uid="QITJWPXNAA",rtseq=25,rtrep=.f.,;
    cFormVar = "w_CURAGSOC", cQueryName = "CURAGSOC",;
    bObbl = .t. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Denominazione o ragione sociale",;
    HelpContextID = 19694441,;
   bGlobalFont=.t.,;
    Height=21, Width=461, Left=177, Top=195, cSayPict="REPL('!',60)", cGetPict="REPL('!',60)", InputMask=replicate('X',60)

  func oCURAGSOC_1_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZPERAZI<>'S')
    endwith
   endif
  endfunc

  add object oCUCOMUNE_1_26 as StdField with uid="VUHYGNVUUM",rtseq=26,rtrep=.f.,;
    cFormVar = "w_CUCOMUNE", cQueryName = "CUCOMUNE",;
    bObbl = .t. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Comune di residenza",;
    HelpContextID = 208039061,;
   bGlobalFont=.t.,;
    Height=21, Width=237, Left=180, Top=242, InputMask=replicate('X',30), bHasZoom = .t. 

  proc oCUCOMUNE_1_26.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C","",".w_Cucomune",".w_Cuprores")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oCUPRORES_1_27 as StdField with uid="XQKERCASOH",rtseq=27,rtrep=.f.,;
    cFormVar = "w_CUPRORES", cQueryName = "CUPRORES",;
    bObbl = .t. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Provincia di residenza",;
    HelpContextID = 12411769,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=533, Top=242, InputMask=replicate('X',2), bHasZoom = .t. 

  proc oCUPRORES_1_27.mZoom
      with this.Parent.oContained
        Gsar_Bxc(this.Parent.oContained,"P","","",".w_Cuprores")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oCU___CAP_1_28 as StdField with uid="PHQOJJSEHE",rtseq=28,rtrep=.f.,;
    cFormVar = "w_CU___CAP", cQueryName = "CU___CAP",;
    bObbl = .f. , nPag = 1, value=space(9), bMultilanguage =  .f.,;
    ToolTipText = "Codice avviamento postale",;
    HelpContextID = 46879606,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=645, Top=242, cSayPict="'999999999'", cGetPict="'999999999'", InputMask=replicate('X',9)

  add object oCUINDIRI_1_29 as StdField with uid="NFHHHYIQUS",rtseq=29,rtrep=.f.,;
    cFormVar = "w_CUINDIRI", cQueryName = "CUINDIRI",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo di residenza",;
    HelpContextID = 118027119,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=178, Top=292, InputMask=replicate('X',35)

  add object oCUNUMFAX_1_30 as StdField with uid="WSQUUGFPOX",rtseq=30,rtrep=.f.,;
    cFormVar = "w_CUNUMFAX", cQueryName = "CUNUMFAX",;
    bObbl = .f. , nPag = 1, value=space(12), bMultilanguage =  .f.,;
    sErrorMsg = "Non inserire caratteri separatori tra prefisso e numero",;
    ToolTipText = "Numero di telefono o fax",;
    HelpContextID = 77611902,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=645, Top=292, cSayPict='"999999999999"', cGetPict='"999999999999"', InputMask=replicate('X',12)

  func oCUNUMFAX_1_30.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (at(' ',alltrim(.w_Cunumfax))=0 and at('/',alltrim(.w_Cunumfax))=0 and at('-',alltrim(.w_Cunumfax))=0)
    endwith
    return bRes
  endfunc

  add object oCU__MAIL_1_31 as StdField with uid="MTZLJHHSMZ",rtseq=31,rtrep=.f.,;
    cFormVar = "w_CU__MAIL", cQueryName = "CU__MAIL",;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo mail",;
    HelpContextID = 5549198,;
   bGlobalFont=.t.,;
    Height=21, Width=566, Left=180, Top=343, cSayPict="replicate('!', 100)", cGetPict="replicate('!', 100)", InputMask=replicate('X',100)

  add object oCUCODATT_1_44 as StdField with uid="RNKMVQTHKN",rtseq=49,rtrep=.f.,;
    cFormVar = "w_CUCODATT", cQueryName = "CUCODATT",;
    bObbl = .f. , nPag = 1, value=space(6), bMultilanguage =  .f.,;
    ToolTipText = "Codice attivit�",;
    HelpContextID = 252285818,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=180, Top=395, cSayPict='repl("!",6)', cGetPict='repl("!",6)', InputMask=replicate('X',6), bHasZoom = .t. 

  proc oCUCODATT_1_44.mZoom
    vx_exec("gsri8sft.vzm",this.Parent.oContained)
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oCUCODSED_1_46 as StdField with uid="RHCNQPWXWS",rtseq=50,rtrep=.f.,;
    cFormVar = "w_CUCODSED", cQueryName = "CUCODSED",nZero=3,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice sede",;
    HelpContextID = 17404778,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=533, Top=395, cSayPict='"999"', cGetPict='"999"', InputMask=replicate('X',3)


  add object oObj_1_56 as cp_runprogram with uid="NBFRWAJSRD",left=0, top=446, width=172,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSRI_BHC(w_HASEVCOP)",;
    cEvent = "HasEvent",;
    nPag=1;
    , ToolTipText = "Richiamato dall'area manuale declare (hascpevent)";
    , HelpContextID = 68292838

  add object oStr_1_32 as StdString with uid="DSZQYEUOKT",Visible=.t., Left=7, Top=82,;
    Alignment=0, Width=395, Height=19,;
    Caption="Dati del soggetto cui si riferisce la comunicazione"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_33 as StdString with uid="MSUZFRVMFD",Visible=.t., Left=9, Top=112,;
    Alignment=1, Width=226, Height=18,;
    Caption="Cod. fiscale del sostituto d'imposta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="OSGCYPTITY",Visible=.t., Left=96, Top=153,;
    Alignment=1, Width=79, Height=18,;
    Caption="Cognome:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="LGSUOYWZDY",Visible=.t., Left=465, Top=153,;
    Alignment=1, Width=62, Height=18,;
    Caption="Nome:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="BOGQGLFCGC",Visible=.t., Left=26, Top=196,;
    Alignment=1, Width=149, Height=18,;
    Caption="Denominazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="DIRMDFWZQS",Visible=.t., Left=10, Top=346,;
    Alignment=1, Width=165, Height=18,;
    Caption="Indirizzo di posta elettronica:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="BSANYZJWZM",Visible=.t., Left=540, Top=295,;
    Alignment=1, Width=103, Height=18,;
    Caption="Telefono o fax:"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="VHNTQYRJGX",Visible=.t., Left=26, Top=245,;
    Alignment=1, Width=149, Height=18,;
    Caption="Comune di residenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="WCLUHWMOQF",Visible=.t., Left=422, Top=245,;
    Alignment=1, Width=105, Height=18,;
    Caption="Provincia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="OXMPNGFMEZ",Visible=.t., Left=591, Top=245,;
    Alignment=1, Width=52, Height=18,;
    Caption="CAP:"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="GAYOZAUEFU",Visible=.t., Left=44, Top=295,;
    Alignment=1, Width=131, Height=18,;
    Caption="Indirizzo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="YKJNGKACOZ",Visible=.t., Left=76, Top=396,;
    Alignment=1, Width=99, Height=18,;
    Caption="Codice attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="QSNOUCRXIM",Visible=.t., Left=428, Top=396,;
    Alignment=1, Width=99, Height=18,;
    Caption="Codice sede:"  ;
  , bGlobalFont=.t.

  add object oStr_1_50 as StdString with uid="JOEJGOSTIM",Visible=.t., Left=44, Top=18,;
    Alignment=1, Width=129, Height=18,;
    Caption="Data comunicazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_51 as StdString with uid="KTZEYIUZXG",Visible=.t., Left=55, Top=52,;
    Alignment=1, Width=118, Height=18,;
    Caption="Note aggiuntive:"  ;
  , bGlobalFont=.t.

  add object oBox_1_47 as StdBox with uid="UWPWFZSCNE",left=-1, top=103, width=494,height=2
enddefine
define class tgsri_acuPag2 as StdContainer
  Width  = 786
  height = 429
  stdWidth  = 786
  stdheight = 429
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCURAPFIR_2_1 as StdCheck with uid="MGUJGMBSME",rtseq=37,rtrep=.f.,left=602, top=26, caption="Rappresentante firmatario",;
    ToolTipText = "Se attivo, abilita l'inserimento dei dati relativi al rappresentante firmatario",;
    HelpContextID = 188972168,;
    cFormVar="w_CURAPFIR", bObbl = .f. , nPag = 2;
    , Tabstop=.F.;
   , bGlobalFont=.t.


  func oCURAPFIR_2_1.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCURAPFIR_2_1.GetRadio()
    this.Parent.oContained.w_CURAPFIR = this.RadioValue()
    return .t.
  endfunc

  func oCURAPFIR_2_1.SetRadio()
    this.Parent.oContained.w_CURAPFIR=trim(this.Parent.oContained.w_CURAPFIR)
    this.value = ;
      iif(this.Parent.oContained.w_CURAPFIR=='S',1,;
      0)
  endfunc

  add object oCUCFISOT_2_9 as StdField with uid="ZBQYPAUWAS",rtseq=43,rtrep=.f.,;
    cFormVar = "w_CUCFISOT", cQueryName = "CUCFISOT",;
    bObbl = .t. , nPag = 2, value=space(16), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale del rappresentante",;
    HelpContextID = 22057850,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=138, Top=92, cSayPict='REPLICATE("!",16)', cGetPict='REPLICATE("!",16)', InputMask=replicate('X',16)

  func oCUCFISOT_2_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CuRapFir='S')
    endwith
   endif
  endfunc

  func oCUCFISOT_2_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (iif(not empty(.w_Cucfisot),chkcfp(alltrim(.w_Cucfisot),'CF'),.T.))
    endwith
    return bRes
  endfunc

  add object oCUCODFED_2_11 as StdField with uid="CECQWLBBDY",rtseq=44,rtrep=.f.,;
    cFormVar = "w_CUCODFED", cQueryName = "CUCODFED",;
    bObbl = .f. , nPag = 2, value=space(11), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale societ� dichiarante",;
    HelpContextID = 67736426,;
   bGlobalFont=.t.,;
    Height=21, Width=90, Left=559, Top=92, cSayPict='REPLICATE("!",11)', cGetPict='REPLICATE("!",11)', InputMask=replicate('X',11)

  func oCUCODFED_2_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CuRapFir='S')
    endwith
   endif
  endfunc

  func oCUCODFED_2_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (iif(Not Empty(.w_Cucodfed),Chkcfp(.w_CUCODFED,"PI", "", "", ""),.t.))
    endwith
    return bRes
  endfunc


  add object oCUCODCAR_2_12 as StdCombo with uid="PMBLFYANVD",rtseq=45,rtrep=.f.,left=138,top=150,width=286,height=22;
    , ToolTipText = "Codice carica";
    , HelpContextID = 17404792;
    , cFormVar="w_CUCODCAR",RowSource=""+""+l_descri1+","+""+l_descri2+","+""+l_descri3+","+""+l_descri4+","+""+l_descri5+","+""+l_descri6+","+""+l_descri7+","+""+l_descri8+","+""+l_descri9+","+""+l_descri10+","+""+l_descri11+","+""+l_descri12+","+""+l_descri13+","+""+l_descri14+","+""+l_descri15+"", bObbl = .t. , nPag = 2;
  , bGlobalFont=.t.


  func oCUCODCAR_2_12.RadioValue()
    return(iif(this.value =1,'1',;
    iif(this.value =2,'2',;
    iif(this.value =3,'3',;
    iif(this.value =4,'4',;
    iif(this.value =5,'5',;
    iif(this.value =6,'6',;
    iif(this.value =7,'7',;
    iif(this.value =8,'8',;
    iif(this.value =9,'9',;
    iif(this.value =10,'10',;
    iif(this.value =11,'11',;
    iif(this.value =12,'12',;
    iif(this.value =13,'13',;
    iif(this.value =14,'14',;
    iif(this.value =15,'15',;
    '  '))))))))))))))))
  endfunc
  func oCUCODCAR_2_12.GetRadio()
    this.Parent.oContained.w_CUCODCAR = this.RadioValue()
    return .t.
  endfunc

  func oCUCODCAR_2_12.SetRadio()
    this.Parent.oContained.w_CUCODCAR=trim(this.Parent.oContained.w_CUCODCAR)
    this.value = ;
      iif(this.Parent.oContained.w_CUCODCAR=='1',1,;
      iif(this.Parent.oContained.w_CUCODCAR=='2',2,;
      iif(this.Parent.oContained.w_CUCODCAR=='3',3,;
      iif(this.Parent.oContained.w_CUCODCAR=='4',4,;
      iif(this.Parent.oContained.w_CUCODCAR=='5',5,;
      iif(this.Parent.oContained.w_CUCODCAR=='6',6,;
      iif(this.Parent.oContained.w_CUCODCAR=='7',7,;
      iif(this.Parent.oContained.w_CUCODCAR=='8',8,;
      iif(this.Parent.oContained.w_CUCODCAR=='9',9,;
      iif(this.Parent.oContained.w_CUCODCAR=='10',10,;
      iif(this.Parent.oContained.w_CUCODCAR=='11',11,;
      iif(this.Parent.oContained.w_CUCODCAR=='12',12,;
      iif(this.Parent.oContained.w_CUCODCAR=='13',13,;
      iif(this.Parent.oContained.w_CUCODCAR=='14',14,;
      iif(this.Parent.oContained.w_CUCODCAR=='15',15,;
      0)))))))))))))))
  endfunc

  func oCUCODCAR_2_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CuRapFir='S')
    endwith
   endif
  endfunc

  add object oCUCOGSOT_2_14 as StdField with uid="UJGVIFVFSY",rtseq=46,rtrep=.f.,;
    cFormVar = "w_CUCOGSOT", cQueryName = "CUCOGSOT",;
    bObbl = .t. , nPag = 2, value=space(24), bMultilanguage =  .f.,;
    ToolTipText = "Cognome del rappresentante",;
    HelpContextID = 20550522,;
   bGlobalFont=.t.,;
    Height=21, Width=209, Left=138, Top=206, cSayPict="REPL('!',24)", cGetPict="REPL('!',24)", InputMask=replicate('X',24)

  func oCUCOGSOT_2_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CuRapFir='S')
    endwith
   endif
  endfunc

  add object oCUNOMSOT_2_15 as StdField with uid="YTVQXAGYUL",rtseq=47,rtrep=.f.,;
    cFormVar = "w_CUNOMSOT", cQueryName = "CUNOMSOT",;
    bObbl = .t. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Nome del rappresentante",;
    HelpContextID = 26887034,;
   bGlobalFont=.t.,;
    Height=21, Width=174, Left=559, Top=206, cSayPict="REPL('!',20)", cGetPict="REPL('!',20)", InputMask=replicate('X',20)

  func oCUNOMSOT_2_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CuRapFir='S')
    endwith
   endif
  endfunc

  add object oStr_2_7 as StdString with uid="VUXSIFVRZX",Visible=.t., Left=7, Top=30,;
    Alignment=0, Width=392, Height=19,;
    Caption="Dati relativi al rappresentante firmatario della comunicazione"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_8 as StdString with uid="EXBWILFTOA",Visible=.t., Left=8, Top=59,;
    Alignment=0, Width=534, Height=18,;
    Caption="(Soggetto che effettua la comunicazione, se diverso dal soggetto cui si riferisce la comunicazione)"  ;
  , bGlobalFont=.t.

  add object oStr_2_10 as StdString with uid="CLTNFQGUIT",Visible=.t., Left=10, Top=94,;
    Alignment=1, Width=123, Height=18,;
    Caption="Codice fiscale:"  ;
  , bGlobalFont=.t.

  add object oStr_2_13 as StdString with uid="HDLLWZBNWD",Visible=.t., Left=15, Top=154,;
    Alignment=1, Width=118, Height=18,;
    Caption="Codice carica:"  ;
  , bGlobalFont=.t.

  add object oStr_2_17 as StdString with uid="TENSGSVNUQ",Visible=.t., Left=51, Top=208,;
    Alignment=1, Width=82, Height=18,;
    Caption="Cognome:"  ;
  , bGlobalFont=.t.

  add object oStr_2_18 as StdString with uid="TIMIFEPNLS",Visible=.t., Left=471, Top=208,;
    Alignment=1, Width=82, Height=18,;
    Caption="Nome:"  ;
  , bGlobalFont=.t.

  add object oStr_2_19 as StdString with uid="HFZNTWSUVL",Visible=.t., Left=283, Top=94,;
    Alignment=1, Width=270, Height=18,;
    Caption="Codice fiscale societ� o ente dichiarante:"  ;
  , bGlobalFont=.t.

  add object oBox_2_20 as StdBox with uid="KMJWVYLVMI",left=4, top=52, width=782,height=2
enddefine
define class tgsri_acuPag3 as StdContainer
  Width  = 786
  height = 429
  stdWidth  = 786
  stdheight = 429
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCUDATFIR_3_3 as StdField with uid="LNQWZABJHQ",rtseq=32,rtrep=.f.,;
    cFormVar = "w_CUDATFIR", cQueryName = "CUDATFIR",;
    bObbl = .t. , nPag = 3, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data firma sostituto d'imposta",;
    HelpContextID = 184835208,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=482, Top=109

  add object oCUCODINT_3_4 as StdField with uid="VHBKITNWRO",rtseq=33,rtrep=.f.,;
    cFormVar = "w_CUCODINT", cQueryName = "CUCODINT",;
    bObbl = .f. , nPag = 3, value=space(16), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale dell'incaricato",;
    HelpContextID = 150367366,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=229, Top=184, cSayPict='REPLICATE("!",16)', cGetPict='REPLICATE("!",16)', InputMask=replicate('X',16)

  func oCUCODINT_3_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (iif(not empty(.w_Cucodint),chkcfp(alltrim(.w_Cucodint),'CF'),.T.))
    endwith
    return bRes
  endfunc


  add object oCUIMPTRA_3_5 as StdCombo with uid="JKQBQAAXZG",rtseq=34,rtrep=.f.,left=229,top=237,width=279,height=22;
    , ToolTipText = "Impegno a trasmettere in via telematica la comunicazione";
    , HelpContextID = 46658407;
    , cFormVar="w_CUIMPTRA",RowSource=""+"1 - Dichiarazione predisposta dal sostituto,"+"2 - Dichiarazione predisposta da chi effettua l'invio", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oCUIMPTRA_3_5.RadioValue()
    return(iif(this.value =1,'1',;
    iif(this.value =2,'2',;
    space(1))))
  endfunc
  func oCUIMPTRA_3_5.GetRadio()
    this.Parent.oContained.w_CUIMPTRA = this.RadioValue()
    return .t.
  endfunc

  func oCUIMPTRA_3_5.SetRadio()
    this.Parent.oContained.w_CUIMPTRA=trim(this.Parent.oContained.w_CUIMPTRA)
    this.value = ;
      iif(this.Parent.oContained.w_CUIMPTRA=='1',1,;
      iif(this.Parent.oContained.w_CUIMPTRA=='2',2,;
      0))
  endfunc

  add object oCUDATIMP_3_7 as StdField with uid="SYRFGCWSSA",rtseq=35,rtrep=.f.,;
    cFormVar = "w_CUDATIMP", cQueryName = "CUDATIMP",;
    bObbl = .f. , nPag = 3, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data dell'impegno",;
    HelpContextID = 134503562,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=229, Top=302

  add object oCUFIRINT_3_10 as StdCheck with uid="EOBSFOCMRE",rtseq=36,rtrep=.f.,left=603, top=312, caption="Firma dell'incaricato",;
    ToolTipText = "Firma dell'incaricato",;
    HelpContextID = 136068230,;
    cFormVar="w_CUFIRINT", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oCUFIRINT_3_10.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oCUFIRINT_3_10.GetRadio()
    this.Parent.oContained.w_CUFIRINT = this.RadioValue()
    return .t.
  endfunc

  func oCUFIRINT_3_10.SetRadio()
    this.Parent.oContained.w_CUFIRINT=trim(this.Parent.oContained.w_CUFIRINT)
    this.value = ;
      iif(this.Parent.oContained.w_CUFIRINT=='1',1,;
      0)
  endfunc

  add object oCUNUMCER_3_12 as StdField with uid="TWOVEAMZAE",rtseq=51,rtrep=.f.,;
    cFormVar = "w_CUNUMCER", cQueryName = "CUNUMCER",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero comunicazioni relative a certificazioni",;
    HelpContextID = 27280248,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=482, Top=74, cSayPict='"9999999999"', cGetPict='"9999999999"'

  add object oCUFIRDIC_3_13 as StdCheck with uid="AFDBERVVYG",rtseq=52,rtrep=.f.,left=572, top=117, caption="Firma del sostituto d'imposta", enabled=.f.,;
    ToolTipText = "Firma del sostituto d'imposta",;
    HelpContextID = 219954327,;
    cFormVar="w_CUFIRDIC", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oCUFIRDIC_3_13.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oCUFIRDIC_3_13.GetRadio()
    this.Parent.oContained.w_CUFIRDIC = this.RadioValue()
    return .t.
  endfunc

  func oCUFIRDIC_3_13.SetRadio()
    this.Parent.oContained.w_CUFIRDIC=trim(this.Parent.oContained.w_CUFIRDIC)
    this.value = ;
      iif(this.Parent.oContained.w_CUFIRDIC=='1',1,;
      0)
  endfunc

  add object oStr_3_2 as StdString with uid="QKZXRMROZN",Visible=.t., Left=10, Top=134,;
    Alignment=0, Width=217, Height=18,;
    Caption="Impegno alla trasmissione telematica"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_6 as StdString with uid="NECDVAUAQY",Visible=.t., Left=8, Top=186,;
    Alignment=1, Width=220, Height=18,;
    Caption="Codice fiscale dell'incaricato:"  ;
  , bGlobalFont=.t.

  add object oStr_3_8 as StdString with uid="AOYTOWESTU",Visible=.t., Left=101, Top=303,;
    Alignment=1, Width=127, Height=18,;
    Caption="Data dell'impegno:"  ;
  , bGlobalFont=.t.

  add object oStr_3_9 as StdString with uid="RCGCOENWMJ",Visible=.t., Left=9, Top=239,;
    Alignment=1, Width=219, Height=18,;
    Caption="Impegno a trasmettere in via telematica:"  ;
  , bGlobalFont=.t.

  add object oStr_3_11 as StdString with uid="IFJAGUWMCJ",Visible=.t., Left=11, Top=78,;
    Alignment=1, Width=463, Height=18,;
    Caption="Numero comunicazioni relative a certificazioni lavoro autonomo e provvigioni:"  ;
  , bGlobalFont=.t.

  func oStr_3_11.mHide()
    with this.Parent.oContained
      return (.w_CUTIPCER<>'CUR15')
    endwith
  endfunc

  add object oStr_3_14 as StdString with uid="JKCJHHVZLW",Visible=.t., Left=10, Top=27,;
    Alignment=0, Width=217, Height=18,;
    Caption="Firma della comunicazione"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_16 as StdString with uid="BCCZUOICFT",Visible=.t., Left=283, Top=109,;
    Alignment=1, Width=191, Height=18,;
    Caption="Data firma sostituto d'imposta:"  ;
  , bGlobalFont=.t.

  add object oStr_3_17 as StdString with uid="SNXFAUWFTT",Visible=.t., Left=207, Top=78,;
    Alignment=1, Width=267, Height=18,;
    Caption="Numero comunicazioni relative a certificazioni:"  ;
  , bGlobalFont=.t.

  func oStr_3_17.mHide()
    with this.Parent.oContained
      return (.w_CUTIPCER='CUR15')
    endwith
  endfunc

  add object oBox_3_1 as StdBox with uid="XQVCPWENHJ",left=1, top=151, width=776,height=2

  add object oBox_3_15 as StdBox with uid="UZBSRPWUBL",left=1, top=49, width=776,height=2
enddefine
define class tgsri_acuPag4 as StdContainer
  Width  = 786
  height = 429
  stdWidth  = 786
  stdheight = 429
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCUDATINI_4_7 as StdField with uid="XJBVRMEWHV",rtseq=55,rtrep=.f.,;
    cFormVar = "w_CUDATINI", cQueryName = "CUDATINI",;
    bObbl = .t. , nPag = 4, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � maggiore della data finale o fuori periodo",;
    ToolTipText = "Data di inizio generazione",;
    HelpContextID = 134503569,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=224, Top=120

  func oCUDATINI_4_7.mHide()
    with this.Parent.oContained
      return (.w_CUTIPCER<>'CUR15')
    endwith
  endfunc

  func oCUDATINI_4_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_Cudatfin>=.w_Cudatini And .w_Cudatini > Date(2013,12,31))
    endwith
    return bRes
  endfunc

  add object oCUDATFIN_4_9 as StdField with uid="TVWLRAJGVN",rtseq=56,rtrep=.f.,;
    cFormVar = "w_CUDATFIN", cQueryName = "CUDATFIN",;
    bObbl = .t. , nPag = 4, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � maggiore della data finale o fuori periodo",;
    ToolTipText = "Data di fine generazione",;
    HelpContextID = 184835212,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=441, Top=120

  func oCUDATFIN_4_9.mHide()
    with this.Parent.oContained
      return (.w_CUTIPCER<>'CUR15')
    endwith
  endfunc

  func oCUDATFIN_4_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_Cudatfin>=.w_Cudatini And .w_Cudatfin<Date(2015,01,01))
    endwith
    return bRes
  endfunc

  add object oCU__ANNO_4_10 as StdField with uid="HYNPMMDCOL",rtseq=57,rtrep=.f.,;
    cFormVar = "w_CU__ANNO", cQueryName = "CU__ANNO",;
    bObbl = .t. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Anno di riferimento",;
    HelpContextID = 68463755,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=224, Top=120, cSayPict='"9999"', cGetPict='"9999"'

  func oCU__ANNO_4_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  func oCU__ANNO_4_10.mHide()
    with this.Parent.oContained
      return (.w_CUTIPCER='CUR15')
    endwith
  endfunc

  func oCU__ANNO_4_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_CU__ANNO>=2015)
    endwith
    return bRes
  endfunc

  add object oCUDOMFIS_4_11 as StdCheck with uid="YCWEFCSJMP",rtseq=58,rtrep=.f.,left=441, top=119, caption="Non valorizza domicilio fiscale",;
    ToolTipText = "Se attivato, non verr� valorizzato il domicilio fiscale se causale prestazione diversa da N",;
    HelpContextID = 191257735,;
    cFormVar="w_CUDOMFIS", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oCUDOMFIS_4_11.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCUDOMFIS_4_11.GetRadio()
    this.Parent.oContained.w_CUDOMFIS = this.RadioValue()
    return .t.
  endfunc

  func oCUDOMFIS_4_11.SetRadio()
    this.Parent.oContained.w_CUDOMFIS=trim(this.Parent.oContained.w_CUDOMFIS)
    this.value = ;
      iif(this.Parent.oContained.w_CUDOMFIS=='S',1,;
      0)
  endfunc

  func oCUDOMFIS_4_11.mHide()
    with this.Parent.oContained
      return (.w_CUTIPCER='CUR15')
    endwith
  endfunc

  add object oCUSOGEST_4_12 as StdCheck with uid="QGLQGIQSVI",rtseq=59,rtrep=.f.,left=224, top=163, caption="Solo soggetti esteri",;
    ToolTipText = "Se attivato, considera esclusivamente soggetti esteri",;
    HelpContextID = 54170490,;
    cFormVar="w_CUSOGEST", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oCUSOGEST_4_12.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCUSOGEST_4_12.GetRadio()
    this.Parent.oContained.w_CUSOGEST = this.RadioValue()
    return .t.
  endfunc

  func oCUSOGEST_4_12.SetRadio()
    this.Parent.oContained.w_CUSOGEST=trim(this.Parent.oContained.w_CUSOGEST)
    this.value = ;
      iif(this.Parent.oContained.w_CUSOGEST=='S',1,;
      0)
  endfunc

  func oCUSOGEST_4_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Cutipcas='O' And .w_Cucersin<>'S' And .w_CuAgrCer<>'S')
    endwith
   endif
  endfunc

  func oCUSOGEST_4_12.mHide()
    with this.Parent.oContained
      return (.w_Cutipcer<>'CUR15')
    endwith
  endfunc

  add object oCUAGRCER_4_13 as StdCheck with uid="GLRZBSMWLG",rtseq=60,rtrep=.f.,left=441, top=163, caption="Aggrega certificazioni opzionale",;
    ToolTipText = "A parit� di altre condizioni verranno aggregate certificazioni con somme non soggette a certificazioni prive di somme non soggette",;
    HelpContextID = 31552376,;
    cFormVar="w_CUAGRCER", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oCUAGRCER_4_13.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCUAGRCER_4_13.GetRadio()
    this.Parent.oContained.w_CUAGRCER = this.RadioValue()
    return .t.
  endfunc

  func oCUAGRCER_4_13.SetRadio()
    this.Parent.oContained.w_CUAGRCER=trim(this.Parent.oContained.w_CUAGRCER)
    this.value = ;
      iif(this.Parent.oContained.w_CUAGRCER=='S',1,;
      0)
  endfunc

  func oCUAGRCER_4_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Cutipcas='O' And .w_Cucersin<>'S' And .cFunction='Load')
    endwith
   endif
  endfunc

  func oCUAGRCER_4_13.mHide()
    with this.Parent.oContained
      return (.w_Cutipcer<>'CUR15')
    endwith
  endfunc

  add object oCUNOMFIL_4_16 as StdField with uid="VPKTICWKHO",rtseq=62,rtrep=.f.,;
    cFormVar = "w_CUNOMFIL", cQueryName = "CUNOMFIL",;
    bObbl = .f. , nPag = 4, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Nome file e percorso per inoltro telematico",;
    HelpContextID = 191216782,;
   bGlobalFont=.t.,;
    Height=21, Width=636, Left=98, Top=333, InputMask=replicate('X',254)


  add object oBtn_4_19 as StdButton with uid="TQEMIMWBDV",left=746, top=333, width=21,height=20,;
    caption="...", nPag=4;
    , ToolTipText = "Seleziona la cartella dove si vuole salvare il file";
    , HelpContextID = 109503786;
  , bGlobalFont=.t.

    proc oBtn_4_19.Click()
      with this.Parent.oContained
        do SfogliaDir with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oCUTIPFOR_4_20 as StdCombo with uid="TMIIFPZQPN",rtseq=63,rtrep=.f.,left=224,top=28,width=279,height=22;
    , height = 21;
    , ToolTipText = "Tipo fornitore";
    , HelpContextID = 188439688;
    , cFormVar="w_CUTIPFOR",RowSource=""+"01: Soggetti che inviano la propria dichiarazione,"+"10: Intermediari", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oCUTIPFOR_4_20.RadioValue()
    return(iif(this.value =1,'01',;
    iif(this.value =2,'10',;
    space(2))))
  endfunc
  func oCUTIPFOR_4_20.GetRadio()
    this.Parent.oContained.w_CUTIPFOR = this.RadioValue()
    return .t.
  endfunc

  func oCUTIPFOR_4_20.SetRadio()
    this.Parent.oContained.w_CUTIPFOR=trim(this.Parent.oContained.w_CUTIPFOR)
    this.value = ;
      iif(this.Parent.oContained.w_CUTIPFOR=='01',1,;
      iif(this.Parent.oContained.w_CUTIPFOR=='10',2,;
      0))
  endfunc


  add object oBtn_4_22 as StdButton with uid="AYLATODQCL",left=719, top=382, width=48,height=45,;
    CpPicture="bmp\SAVE.bmp", caption="", nPag=4;
    , ToolTipText = "Genera file";
    , HelpContextID = 249676566;
    , caption='\<Genera';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_22.Click()
      with this.Parent.oContained
        Gsri_Bcu(this.Parent.oContained,"A")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_4_22.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not empty(.w_Cunomfil))
      endwith
    endif
  endfunc

  func oBtn_4_22.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_Cutipcer<>'CUR15')
     endwith
    endif
  endfunc


  add object oLinkPC_4_23 as stdDynamicChildContainer with uid="ZNFBGGEZZK",left=7, top=378, width=212, height=49, bOnScreen=.t.;
    , tabstop=.f.

  func oLinkPC_4_23.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (1=1)
     endwith
    endif
  endfunc


  add object oBtn_4_24 as StdButton with uid="PJPVILDQVM",left=667, top=382, width=48,height=45,;
    CpPicture="bmp\STAMPA.BMP", caption="", nPag=4;
    , ToolTipText = "Stampa frontespizio e riepilogo";
    , HelpContextID = 249676566;
    , caption='M\<odello';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_24.Click()
      with this.Parent.oContained
        Gsri_Bcu(this.Parent.oContained,"M")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_4_24.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction<> 'Load')
      endwith
    endif
  endfunc

  func oBtn_4_24.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (Empty (.w_Cuserial) Or .w_Cutipcer<>'CUR15')
     endwith
    endif
  endfunc


  add object oCUTIPCAS_4_27 as StdCombo with uid="QJCHZNLGQC",rtseq=66,rtrep=.f.,left=224,top=212,width=183,height=22;
    , ToolTipText = "Tipo di comunicazione";
    , HelpContextID = 29664121;
    , cFormVar="w_CUTIPCAS",RowSource=""+"Ordinaria,"+"Sostitutiva,"+"Annullamento", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oCUTIPCAS_4_27.RadioValue()
    return(iif(this.value =1,'O',;
    iif(this.value =2,'S',;
    iif(this.value =3,'A',;
    space(1)))))
  endfunc
  func oCUTIPCAS_4_27.GetRadio()
    this.Parent.oContained.w_CUTIPCAS = this.RadioValue()
    return .t.
  endfunc

  func oCUTIPCAS_4_27.SetRadio()
    this.Parent.oContained.w_CUTIPCAS=trim(this.Parent.oContained.w_CUTIPCAS)
    this.value = ;
      iif(this.Parent.oContained.w_CUTIPCAS=='O',1,;
      iif(this.Parent.oContained.w_CUTIPCAS=='S',2,;
      iif(this.Parent.oContained.w_CUTIPCAS=='A',3,;
      0)))
  endfunc

  func oCUTIPCAS_4_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  func oCUTIPCAS_4_27.mHide()
    with this.Parent.oContained
      return (.w_Cutipcer<>'CUR15')
    endwith
  endfunc

  add object oCUCERSIN_4_28 as StdCheck with uid="GLHKTIVQJA",rtseq=67,rtrep=.f.,left=441, top=216, caption="Certificazione singola",;
    ToolTipText = "Se attivo, consente di generare un file per singola certificazione",;
    HelpContextID = 237005964,;
    cFormVar="w_CUCERSIN", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oCUCERSIN_4_28.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCUCERSIN_4_28.GetRadio()
    this.Parent.oContained.w_CUCERSIN = this.RadioValue()
    return .t.
  endfunc

  func oCUCERSIN_4_28.SetRadio()
    this.Parent.oContained.w_CUCERSIN=trim(this.Parent.oContained.w_CUCERSIN)
    this.value = ;
      iif(this.Parent.oContained.w_CUCERSIN=='S',1,;
      0)
  endfunc

  func oCUCERSIN_4_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Cutipcas='O' And .cFunction='Load')
    endwith
   endif
  endfunc

  func oCUCERSIN_4_28.mHide()
    with this.Parent.oContained
      return (.w_Cutipcer<>'CUR15')
    endwith
  endfunc


  add object oBtn_4_29 as StdButton with uid="WYIIVEVOIK",left=636, top=212, width=21,height=20,;
    caption="...", nPag=4;
    , ToolTipText = "Seleziona la certificazione che si intende generare/annullare/sostituire";
    , HelpContextID = 109503786;
  , bGlobalFont=.t.

    proc oBtn_4_29.Click()
      with this.Parent.oContained
        Gsri_Bcu(this.Parent.oContained,"P")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_4_29.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return ((.w_Cutipcas='O' And .w_Cucersin<>'S') Or .cFunction<>'Load' Or .w_Cutipcer<>'CUR15')
     endwith
    endif
  endfunc


  add object oBtn_4_31 as StdButton with uid="AYNXZIEVQV",left=719, top=382, width=48,height=45,;
    CpPicture="bmp\SAVE.bmp", caption="", nPag=4;
    , ToolTipText = "Premere per generare file telematico ";
    , HelpContextID = 249676566;
    ,  tabstop = .f., caption='\<Genera';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_31.Click()
      with this.Parent.oContained
        do Gsut_Bcr with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_4_31.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not empty(.w_Cunomfil) And isInoltel() And not g_DEMO AND BBVERSION>=146)
      endwith
    endif
  endfunc

  func oBtn_4_31.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_Cutipcer<>'CUR16' Or .w_CU__ANNO>=2016)
     endwith
    endif
  endfunc

  add object oCUCERANN_4_33 as StdCheck with uid="RAZLZDRKNF",rtseq=70,rtrep=.f.,left=226, top=216, caption="Annullamento",;
    ToolTipText = "Fornitura relativa all'invio di certificazioni da annullare",;
    HelpContextID = 2124940,;
    cFormVar="w_CUCERANN", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oCUCERANN_4_33.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCUCERANN_4_33.GetRadio()
    this.Parent.oContained.w_CUCERANN = this.RadioValue()
    return .t.
  endfunc

  func oCUCERANN_4_33.SetRadio()
    this.Parent.oContained.w_CUCERANN=trim(this.Parent.oContained.w_CUCERANN)
    this.value = ;
      iif(this.Parent.oContained.w_CUCERANN=='S',1,;
      0)
  endfunc

  func oCUCERANN_4_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  func oCUCERANN_4_33.mHide()
    with this.Parent.oContained
      return (.w_CUTIPCER='CUR15')
    endwith
  endfunc

  add object oCUCERSOS_4_34 as StdCheck with uid="VJNHOOWUKH",rtseq=71,rtrep=.f.,left=441, top=216, caption="Sostituzione",;
    ToolTipText = "Fornitura relativa all'invio di certificazioni da sostituire",;
    HelpContextID = 31429497,;
    cFormVar="w_CUCERSOS", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oCUCERSOS_4_34.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCUCERSOS_4_34.GetRadio()
    this.Parent.oContained.w_CUCERSOS = this.RadioValue()
    return .t.
  endfunc

  func oCUCERSOS_4_34.SetRadio()
    this.Parent.oContained.w_CUCERSOS=trim(this.Parent.oContained.w_CUCERSOS)
    this.value = ;
      iif(this.Parent.oContained.w_CUCERSOS=='S',1,;
      0)
  endfunc

  func oCUCERSOS_4_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  func oCUCERSOS_4_34.mHide()
    with this.Parent.oContained
      return (.w_CUTIPCER='CUR15')
    endwith
  endfunc


  add object oLinkPC_4_35 as stdDynamicChildContainer with uid="VHPTGNVSJC",left=223, top=374, width=260, height=54, bOnScreen=.t.;
    , tabstop=.f.

  func oLinkPC_4_35.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (1=1)
     endwith
    endif
  endfunc


  add object oBtn_4_36 as StdButton with uid="ILUFYLYWTX",left=667, top=382, width=48,height=45,;
    CpPicture="bmp\STAMPA.BMP", caption="", nPag=4;
    , ToolTipText = "Stampa frontespizio e riepilogo";
    , HelpContextID = 249676566;
    , caption='M\<odello';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_36.Click()
      with this.Parent.oContained
        Gsri_Bcs(this.Parent.oContained,"O")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_4_36.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction<> 'Load')
      endwith
    endif
  endfunc

  func oBtn_4_36.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (Empty (.w_Cuserial) Or .w_Cutipcer<>'CUR16' )
     endwith
    endif
  endfunc


  add object oBtn_4_38 as StdButton with uid="BBWTBVQXFD",left=719, top=382, width=48,height=45,;
    CpPicture="bmp\SAVE.bmp", caption="", nPag=4;
    , ToolTipText = "Premere per generare file telematico ";
    , HelpContextID = 249676566;
    ,  tabstop = .f., caption='\<Genera';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_38.Click()
      with this.Parent.oContained
        do Gsbb1Bcr with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_4_38.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not empty(.w_Cunomfil) And isInoltel() And not g_DEMO AND BBVERSION>=278)
      endwith
    endif
  endfunc

  func oBtn_4_38.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_Cutipcer<>'CUR16' Or .w_CU__ANNO<>2016)
     endwith
    endif
  endfunc


  add object oEVEECC_2016_4_39 as StdCombo with uid="WJJWKVJLDV",rtseq=75,rtrep=.f.,left=224,top=259,width=543,height=22;
    , ToolTipText = "Eventi eccezionali";
    , HelpContextID = 252491656;
    , cFormVar="w_EVEECC_2016",RowSource=""+"0 - Nessuno,"+"1 - Contribuenti vittime di richieste estorsive ex. Art.20 Co.2 legge n. 44/99,"+"3 - Contribuenti residenti al 12/2/2011 nel comune di Lampedusa e Linosa (migranti Nord Africa),"+"6 - Contribuenti colpiti da altri eventi eccezionali (Certificazione unica e 770)", bObbl = .t. , nPag = 4;
  , bGlobalFont=.t.


  func oEVEECC_2016_4_39.RadioValue()
    return(iif(this.value =1,'0',;
    iif(this.value =2,'1',;
    iif(this.value =3,'3',;
    iif(this.value =4,'6',;
    space(1))))))
  endfunc
  func oEVEECC_2016_4_39.GetRadio()
    this.Parent.oContained.w_EVEECC_2016 = this.RadioValue()
    return .t.
  endfunc

  func oEVEECC_2016_4_39.SetRadio()
    this.Parent.oContained.w_EVEECC_2016=trim(this.Parent.oContained.w_EVEECC_2016)
    this.value = ;
      iif(this.Parent.oContained.w_EVEECC_2016=='0',1,;
      iif(this.Parent.oContained.w_EVEECC_2016=='1',2,;
      iif(this.Parent.oContained.w_EVEECC_2016=='3',3,;
      iif(this.Parent.oContained.w_EVEECC_2016=='6',4,;
      0))))
  endfunc

  func oEVEECC_2016_4_39.mHide()
    with this.Parent.oContained
      return (.w_CUTIPCER<>'CUR16' Or .w_CU__ANNO<2016 Or .w_CU__ANNO>=2017)
    endwith
  endfunc


  add object oEVEECC_2017_4_40 as StdCombo with uid="GPQREOUZEG",rtseq=76,rtrep=.f.,left=224,top=259,width=543,height=22;
    , ToolTipText = "Eventi eccezionali";
    , HelpContextID = 252487560;
    , cFormVar="w_EVEECC_2017",RowSource=""+"0 - Nessuno,"+"1 - Contribuenti vittime di richieste estorsive ex. Art.20 Co.2 legge n. 44/99,"+"3 - Contribuenti residenti al 12/2/2011 nel comune di Lampedusa e Linosa (migranti Nord Africa),"+"8 - Contribuenti colpiti da altri eventi eccezionali (Certificazione unica e 770)", bObbl = .t. , nPag = 4;
  , bGlobalFont=.t.


  func oEVEECC_2017_4_40.RadioValue()
    return(iif(this.value =1,'0',;
    iif(this.value =2,'1',;
    iif(this.value =3,'3',;
    iif(this.value =4,'8',;
    space(1))))))
  endfunc
  func oEVEECC_2017_4_40.GetRadio()
    this.Parent.oContained.w_EVEECC_2017 = this.RadioValue()
    return .t.
  endfunc

  func oEVEECC_2017_4_40.SetRadio()
    this.Parent.oContained.w_EVEECC_2017=trim(this.Parent.oContained.w_EVEECC_2017)
    this.value = ;
      iif(this.Parent.oContained.w_EVEECC_2017=='0',1,;
      iif(this.Parent.oContained.w_EVEECC_2017=='1',2,;
      iif(this.Parent.oContained.w_EVEECC_2017=='3',3,;
      iif(this.Parent.oContained.w_EVEECC_2017=='8',4,;
      0))))
  endfunc

  func oEVEECC_2017_4_40.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' )
    endwith
   endif
  endfunc

  func oEVEECC_2017_4_40.mHide()
    with this.Parent.oContained
      return (.w_CUTIPCER<>'CUR16' Or .w_CU__ANNO<2017)
    endwith
  endfunc


  add object oBtn_4_43 as StdButton with uid="EUNEWRALBP",left=719, top=382, width=48,height=45,;
    CpPicture="bmp\SAVE.bmp", caption="", nPag=4;
    , ToolTipText = "Premere per generare file telematico ";
    , HelpContextID = 249676566;
    ,  tabstop = .f., caption='\<Genera';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_43.Click()
      with this.Parent.oContained
        do Gsbb2Bcr with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_4_43.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not empty(.w_Cunomfil) And isInoltel() And not g_DEMO AND BBVERSION>=359)
      endwith
    endif
  endfunc

  func oBtn_4_43.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_Cutipcer<>'CUR16' Or .w_CU__ANNO<2017)
     endwith
    endif
  endfunc

  add object oStr_4_5 as StdString with uid="UTEVLEHEXF",Visible=.t., Left=7, Top=82,;
    Alignment=0, Width=217, Height=18,;
    Caption="Parametri di selezione"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_6 as StdString with uid="IKQMNTEOIJ",Visible=.t., Left=153, Top=123,;
    Alignment=1, Width=70, Height=18,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  func oStr_4_6.mHide()
    with this.Parent.oContained
      return (.w_CUTIPCER<>'CUR15')
    endwith
  endfunc

  add object oStr_4_8 as StdString with uid="QAILFHVDUV",Visible=.t., Left=379, Top=123,;
    Alignment=1, Width=58, Height=18,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  func oStr_4_8.mHide()
    with this.Parent.oContained
      return (.w_CUTIPCER<>'CUR15')
    endwith
  endfunc

  add object oStr_4_17 as StdString with uid="LTPVJFCSHF",Visible=.t., Left=7, Top=296,;
    Alignment=0, Width=217, Height=18,;
    Caption="File telematico"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_18 as StdString with uid="SSBWQANJWI",Visible=.t., Left=11, Top=334,;
    Alignment=1, Width=81, Height=18,;
    Caption="Nome file:"  ;
  , bGlobalFont=.t.

  add object oStr_4_21 as StdString with uid="CQLAGOZQUG",Visible=.t., Left=127, Top=29,;
    Alignment=1, Width=96, Height=18,;
    Caption="Tipo fornitore:"  ;
  , bGlobalFont=.t.

  add object oStr_4_26 as StdString with uid="ADHGQRRRGH",Visible=.t., Left=7, Top=184,;
    Alignment=0, Width=217, Height=18,;
    Caption="Tipo di comunicazione"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_32 as StdString with uid="SJCUFHTJAM",Visible=.t., Left=157, Top=123,;
    Alignment=1, Width=66, Height=18,;
    Caption="Anno:"  ;
  , bGlobalFont=.t.

  func oStr_4_32.mHide()
    with this.Parent.oContained
      return (.w_CUTIPCER='CUR15')
    endwith
  endfunc

  add object oStr_4_37 as StdString with uid="YGPFZZMPPJ",Visible=.t., Left=105, Top=260,;
    Alignment=1, Width=118, Height=18,;
    Caption="Eventi eccezionali:"  ;
  , bGlobalFont=.t.

  func oStr_4_37.mHide()
    with this.Parent.oContained
      return (.w_CUTIPCER='CUR15' Or .w_CU__ANNO<2016)
    endwith
  endfunc

  add object oBox_4_4 as StdBox with uid="VQUFPWYELS",left=-4, top=99, width=776,height=2

  add object oBox_4_14 as StdBox with uid="UFRJQQJEBL",left=-4, top=314, width=776,height=2

  add object oBox_4_25 as StdBox with uid="JPMDGSDDII",left=1, top=201, width=776,height=2
  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gsri_mcu",lower(this.oContained.Gsri_Mcu.class))=0
        this.oContained.Gsri_Mcu.createrealchild()
      endif
      if type('this.oContained')='O' and at("gsri_mcr",lower(this.oContained.Gsri_Mcr.class))=0
        this.oContained.Gsri_Mcr.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsri_acu','GENCOMUN','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".CUSERIAL=GENCOMUN.CUSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsri_acu
proc SfogliaDir (parent)
local PathName, oField
  PathName = Cp_GetDir()
  if !empty(PathName)
    parent.w_DirName = PathName
    parent.w_CUNOMFIL=left(parent.w_DirName+Alltrim(parent.w_Cucodfis)+'_'+Substr(parent.w_Cuserial,2,9)+'.CUR'+space(254),254)
  endif
endproc
* --- Fine Area Manuale
