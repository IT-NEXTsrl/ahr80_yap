* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_bvs                                                        *
*              Valorizzazione stato certificazione                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2016-02-09                                                      *
* Last revis.: 2016-02-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsri_bvs",oParentObject)
return(i_retval)

define class tgsri_bvs as StdBatch
  * --- Local variables
  * --- WorkFile variables
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Valorizzazione stato certificazione
    this.oParentObject.w_Stato = " "
    * --- Select from GSRI11KDE
    do vq_exec with 'GSRI11KDE',this,'_Curs_GSRI11KDE','',.f.,.t.
    if used('_Curs_GSRI11KDE')
      select _Curs_GSRI11KDE
      locate for 1=1
      do while not(eof())
      this.oParentObject.w_Stato = _Curs_Gsri11kde.Stato
        select _Curs_GSRI11KDE
        continue
      enddo
      use
    endif
    * --- Siccome la query Gsri11Kde non considera eventuali figli di tipo respinto,
    *     se il campo Stato risulta vuoto controllo se per caso � stato comunicato
    this.oParentObject.w_Stato = iif(this.oParentObject.w_Stato=" " And Not Empty(this.oParentObject.w_Gdserial),"C",this.oParentObject.w_Stato)
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  proc CloseCursors()
    if used('_Curs_GSRI11KDE')
      use in _Curs_GSRI11KDE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
