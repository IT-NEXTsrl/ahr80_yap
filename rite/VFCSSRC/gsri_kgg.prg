* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_kgg                                                        *
*              Giroconto ritenute                                              *
*                                                                              *
*      Author: ZUCCHETTI TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [146] [VRS_59]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-01-02                                                      *
* Last revis.: 2008-09-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsri_kgg",oParentObject))

* --- Class definition
define class tgsri_kgg as StdForm
  Top    = 16
  Left   = 41

  * --- Standard Properties
  Width  = 602
  Height = 392
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-09-26"
  HelpContextID=32110185
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=38

  * --- Constant Properties
  _IDX = 0
  CONTROPA_IDX = 0
  CAU_CONT_IDX = 0
  CONTI_IDX = 0
  TAB_RITE_IDX = 0
  cPrg = "gsri_kgg"
  cComment = "Giroconto ritenute"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_TIPORI = space(1)
  w_TIPCON = space(1)
  o_TIPCON = space(1)
  w_CODAZI = space(5)
  w_CODAZI1 = space(5)
  w_FLGSTO = space(1)
  o_FLGSTO = space(1)
  w_INIDATA = ctod('  /  /  ')
  w_FINDATA = ctod('  /  /  ')
  w_DATAREG = space(1)
  w_REGIDATA = ctod('  /  /  ')
  w_NUMODIE = 0
  w_SERIEOR = space(10)
  w_DATAODIE = ctod('  /  /  ')
  w_DESCRIZI = space(50)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_FLPART = space(1)
  w_VPDATREG = ctod('  /  /  ')
  w_EVICRE = space(15)
  w_COIRVE = space(15)
  w_COPRVE = space(15)
  w_CAUGIR = space(5)
  w_CONTRS = space(15)
  w_DESCRIR = space(40)
  w_DESCRPV = space(40)
  w_CONRIT = space(15)
  w_DESCRITE = space(40)
  w_CORIPR = space(15)
  w_DESCRIPR = space(40)
  w_GIROCO = space(5)
  w_CAUSAL = space(5)
  w_DESCRI = space(35)
  w_DESCRV = space(35)
  w_DESCRI1 = space(40)
  w_DESCRIT1 = space(40)
  w_DESSAL = space(35)
  w_FLGST1 = space(1)
  o_FLGST1 = space(1)
  w_TIPSAL = space(1)
  w_FLSAL = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsri_kggPag1","gsri_kgg",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oINIDATA_1_6
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- gsri_kgg
    WITH THIS.PARENT
     DO CASE
             CASE  OPARENTOBJECT= 'A'
                   .cComment = ah_msgformat("Giroconto ritenute operate")
             CASE  OPARENTOBJECT = 'V'
                   .cComment = ah_msgformat("Giroconto ritenute subite")
       ENDCASE
    ENDWITH
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='CONTROPA'
    this.cWorkTables[2]='CAU_CONT'
    this.cWorkTables[3]='CONTI'
    this.cWorkTables[4]='TAB_RITE'
    return(this.OpenAllTables(4))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do GSRI_BGG with this
      endwith
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIPORI=space(1)
      .w_TIPCON=space(1)
      .w_CODAZI=space(5)
      .w_CODAZI1=space(5)
      .w_FLGSTO=space(1)
      .w_INIDATA=ctod("  /  /  ")
      .w_FINDATA=ctod("  /  /  ")
      .w_DATAREG=space(1)
      .w_REGIDATA=ctod("  /  /  ")
      .w_NUMODIE=0
      .w_SERIEOR=space(10)
      .w_DATAODIE=ctod("  /  /  ")
      .w_DESCRIZI=space(50)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_FLPART=space(1)
      .w_VPDATREG=ctod("  /  /  ")
      .w_EVICRE=space(15)
      .w_COIRVE=space(15)
      .w_COPRVE=space(15)
      .w_CAUGIR=space(5)
      .w_CONTRS=space(15)
      .w_DESCRIR=space(40)
      .w_DESCRPV=space(40)
      .w_CONRIT=space(15)
      .w_DESCRITE=space(40)
      .w_CORIPR=space(15)
      .w_DESCRIPR=space(40)
      .w_GIROCO=space(5)
      .w_CAUSAL=space(5)
      .w_DESCRI=space(35)
      .w_DESCRV=space(35)
      .w_DESCRI1=space(40)
      .w_DESCRIT1=space(40)
      .w_DESSAL=space(35)
      .w_FLGST1=space(1)
      .w_TIPSAL=space(1)
      .w_FLSAL=space(1)
        .w_TIPORI = this.oParentObject
        .w_TIPCON = 'G'
        .w_CODAZI = i_codazi
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_CODAZI))
          .link_1_3('Full')
        endif
        .w_CODAZI1 = I_CODAZI
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_CODAZI1))
          .link_1_4('Full')
        endif
          .DoRTCalc(5,5,.f.)
        .w_INIDATA = .w_INIDATA
        .w_FINDATA = .w_FINDATA
        .w_DATAREG = IIF((.w_FLGSTO='S' AND .w_TIPORI='A')  OR ( .w_FLGST1='S' AND .w_TIPORI='V') ,'O','P')
        .w_REGIDATA = i_datsys
          .DoRTCalc(10,11,.f.)
        .w_DATAODIE = i_datsys
          .DoRTCalc(13,13,.f.)
        .w_OBTEST = i_datsys
      .oPgFrm.Page1.oPag.oObj_1_26.Calculate()
          .DoRTCalc(15,16,.f.)
        .w_VPDATREG = i_DATSYS
        .w_EVICRE = .w_EVICRE
        .DoRTCalc(18,18,.f.)
        if not(empty(.w_EVICRE))
          .link_1_32('Full')
        endif
        .w_COIRVE = .w_COIRVE
        .DoRTCalc(19,19,.f.)
        if not(empty(.w_COIRVE))
          .link_1_33('Full')
        endif
        .w_COPRVE = .w_COPRVE
        .DoRTCalc(20,20,.f.)
        if not(empty(.w_COPRVE))
          .link_1_34('Full')
        endif
        .w_CAUGIR = .w_CAUGIR
        .DoRTCalc(21,21,.f.)
        if not(empty(.w_CAUGIR))
          .link_1_35('Full')
        endif
        .w_CONTRS = .w_CONTRS
        .DoRTCalc(22,22,.f.)
        if not(empty(.w_CONTRS))
          .link_1_36('Full')
        endif
          .DoRTCalc(23,24,.f.)
        .w_CONRIT = .w_CONRIT
        .DoRTCalc(25,25,.f.)
        if not(empty(.w_CONRIT))
          .link_1_39('Full')
        endif
          .DoRTCalc(26,26,.f.)
        .w_CORIPR = .w_CORIPR
        .DoRTCalc(27,27,.f.)
        if not(empty(.w_CORIPR))
          .link_1_41('Full')
        endif
          .DoRTCalc(28,28,.f.)
        .w_GIROCO = .w_GIROCO
        .DoRTCalc(29,29,.f.)
        if not(empty(.w_GIROCO))
          .link_1_43('Full')
        endif
        .w_CAUSAL = .w_CAUSAL
        .DoRTCalc(30,30,.f.)
        if not(empty(.w_CAUSAL))
          .link_1_44('Full')
        endif
    endwith
    this.DoRTCalc(31,38,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_20.enabled = this.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_21.enabled = this.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
            .w_TIPORI = this.oParentObject
        .DoRTCalc(2,2,.t.)
        if .o_TIPCON<>.w_TIPCON
          .link_1_3('Full')
        endif
          .link_1_4('Full')
        .DoRTCalc(5,7,.t.)
        if .o_FLGSTO<>.w_FLGSTO
            .w_DATAREG = IIF((.w_FLGSTO='S' AND .w_TIPORI='A')  OR ( .w_FLGST1='S' AND .w_TIPORI='V') ,'O','P')
        endif
        .oPgFrm.Page1.oPag.oObj_1_26.Calculate()
        .DoRTCalc(9,28,.t.)
        if .o_FLGST1<>.w_FLGST1
            .w_GIROCO = .w_GIROCO
          .link_1_43('Full')
        endif
        if .o_FLGST1<>.w_FLGST1
            .w_CAUSAL = .w_CAUSAL
          .link_1_44('Full')
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(31,38,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_26.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oDATAREG_1_9.enabled = this.oPgFrm.Page1.oPag.oDATAREG_1_9.mCond()
    this.oPgFrm.Page1.oPag.oCAUGIR_1_35.enabled = this.oPgFrm.Page1.oPag.oCAUGIR_1_35.mCond()
    this.oPgFrm.Page1.oPag.oCONTRS_1_36.enabled = this.oPgFrm.Page1.oPag.oCONTRS_1_36.mCond()
    this.oPgFrm.Page1.oPag.oCONRIT_1_39.enabled = this.oPgFrm.Page1.oPag.oCONRIT_1_39.mCond()
    this.oPgFrm.Page1.oPag.oCORIPR_1_41.enabled = this.oPgFrm.Page1.oPag.oCORIPR_1_41.mCond()
    this.oPgFrm.Page1.oPag.oCAUSAL_1_44.enabled = this.oPgFrm.Page1.oPag.oCAUSAL_1_44.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_8.visible=!this.oPgFrm.Page1.oPag.oStr_1_8.mHide()
    this.oPgFrm.Page1.oPag.oREGIDATA_1_10.visible=!this.oPgFrm.Page1.oPag.oREGIDATA_1_10.mHide()
    this.oPgFrm.Page1.oPag.oNUMODIE_1_11.visible=!this.oPgFrm.Page1.oPag.oNUMODIE_1_11.mHide()
    this.oPgFrm.Page1.oPag.oSERIEOR_1_12.visible=!this.oPgFrm.Page1.oPag.oSERIEOR_1_12.mHide()
    this.oPgFrm.Page1.oPag.oDATAODIE_1_13.visible=!this.oPgFrm.Page1.oPag.oDATAODIE_1_13.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_15.visible=!this.oPgFrm.Page1.oPag.oStr_1_15.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_16.visible=!this.oPgFrm.Page1.oPag.oStr_1_16.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_17.visible=!this.oPgFrm.Page1.oPag.oStr_1_17.mHide()
    this.oPgFrm.Page1.oPag.oEVICRE_1_32.visible=!this.oPgFrm.Page1.oPag.oEVICRE_1_32.mHide()
    this.oPgFrm.Page1.oPag.oCOIRVE_1_33.visible=!this.oPgFrm.Page1.oPag.oCOIRVE_1_33.mHide()
    this.oPgFrm.Page1.oPag.oCOPRVE_1_34.visible=!this.oPgFrm.Page1.oPag.oCOPRVE_1_34.mHide()
    this.oPgFrm.Page1.oPag.oCAUGIR_1_35.visible=!this.oPgFrm.Page1.oPag.oCAUGIR_1_35.mHide()
    this.oPgFrm.Page1.oPag.oCONTRS_1_36.visible=!this.oPgFrm.Page1.oPag.oCONTRS_1_36.mHide()
    this.oPgFrm.Page1.oPag.oDESCRIR_1_37.visible=!this.oPgFrm.Page1.oPag.oDESCRIR_1_37.mHide()
    this.oPgFrm.Page1.oPag.oDESCRPV_1_38.visible=!this.oPgFrm.Page1.oPag.oDESCRPV_1_38.mHide()
    this.oPgFrm.Page1.oPag.oCONRIT_1_39.visible=!this.oPgFrm.Page1.oPag.oCONRIT_1_39.mHide()
    this.oPgFrm.Page1.oPag.oDESCRITE_1_40.visible=!this.oPgFrm.Page1.oPag.oDESCRITE_1_40.mHide()
    this.oPgFrm.Page1.oPag.oCORIPR_1_41.visible=!this.oPgFrm.Page1.oPag.oCORIPR_1_41.mHide()
    this.oPgFrm.Page1.oPag.oDESCRIPR_1_42.visible=!this.oPgFrm.Page1.oPag.oDESCRIPR_1_42.mHide()
    this.oPgFrm.Page1.oPag.oGIROCO_1_43.visible=!this.oPgFrm.Page1.oPag.oGIROCO_1_43.mHide()
    this.oPgFrm.Page1.oPag.oCAUSAL_1_44.visible=!this.oPgFrm.Page1.oPag.oCAUSAL_1_44.mHide()
    this.oPgFrm.Page1.oPag.oDESCRI_1_45.visible=!this.oPgFrm.Page1.oPag.oDESCRI_1_45.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_46.visible=!this.oPgFrm.Page1.oPag.oStr_1_46.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_47.visible=!this.oPgFrm.Page1.oPag.oStr_1_47.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_48.visible=!this.oPgFrm.Page1.oPag.oStr_1_48.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_49.visible=!this.oPgFrm.Page1.oPag.oStr_1_49.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_50.visible=!this.oPgFrm.Page1.oPag.oStr_1_50.mHide()
    this.oPgFrm.Page1.oPag.oDESCRV_1_51.visible=!this.oPgFrm.Page1.oPag.oDESCRV_1_51.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_52.visible=!this.oPgFrm.Page1.oPag.oStr_1_52.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_53.visible=!this.oPgFrm.Page1.oPag.oStr_1_53.mHide()
    this.oPgFrm.Page1.oPag.oDESCRI1_1_54.visible=!this.oPgFrm.Page1.oPag.oDESCRI1_1_54.mHide()
    this.oPgFrm.Page1.oPag.oDESCRIT1_1_55.visible=!this.oPgFrm.Page1.oPag.oDESCRIT1_1_55.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_56.visible=!this.oPgFrm.Page1.oPag.oStr_1_56.mHide()
    this.oPgFrm.Page1.oPag.oDESSAL_1_57.visible=!this.oPgFrm.Page1.oPag.oDESSAL_1_57.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_58.visible=!this.oPgFrm.Page1.oPag.oStr_1_58.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_26.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTROPA_IDX,3]
    i_lTable = "CONTROPA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2], .t., this.CONTROPA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COCODAZI,COGIRRIT,COCOIRVE,COCOPRVE,COCONRIT,COCORIPR,COCONTRS,COCAUSLD,COCAUGIR,COEVICRE";
                   +" from "+i_cTable+" "+i_lTable+" where COCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COCODAZI',this.w_CODAZI)
            select COCODAZI,COGIRRIT,COCOIRVE,COCOPRVE,COCONRIT,COCORIPR,COCONTRS,COCAUSLD,COCAUGIR,COEVICRE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.COCODAZI,space(5))
      this.w_GIROCO = NVL(_Link_.COGIRRIT,space(5))
      this.w_COIRVE = NVL(_Link_.COCOIRVE,space(15))
      this.w_COPRVE = NVL(_Link_.COCOPRVE,space(15))
      this.w_CONRIT = NVL(_Link_.COCONRIT,space(15))
      this.w_CORIPR = NVL(_Link_.COCORIPR,space(15))
      this.w_CONTRS = NVL(_Link_.COCONTRS,space(15))
      this.w_CAUSAL = NVL(_Link_.COCAUSLD,space(5))
      this.w_CAUGIR = NVL(_Link_.COCAUGIR,space(5))
      this.w_EVICRE = NVL(_Link_.COEVICRE,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_GIROCO = space(5)
      this.w_COIRVE = space(15)
      this.w_COPRVE = space(15)
      this.w_CONRIT = space(15)
      this.w_CORIPR = space(15)
      this.w_CONTRS = space(15)
      this.w_CAUSAL = space(5)
      this.w_CAUGIR = space(5)
      this.w_EVICRE = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])+'\'+cp_ToStr(_Link_.COCODAZI,1)
      cp_ShowWarn(i_cKey,this.CONTROPA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODAZI1
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TAB_RITE_IDX,3]
    i_lTable = "TAB_RITE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TAB_RITE_IDX,2], .t., this.TAB_RITE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TAB_RITE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRTIPRIT,TRCODAZI,TRFLGSTO,TRFLGST1";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODAZI="+cp_ToStrODBC(this.w_CODAZI1);
                   +" and TRTIPRIT="+cp_ToStrODBC(this.w_TIPORI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRTIPRIT',this.w_TIPORI;
                       ,'TRCODAZI',this.w_CODAZI1)
            select TRTIPRIT,TRCODAZI,TRFLGSTO,TRFLGST1;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI1 = NVL(_Link_.TRCODAZI,space(5))
      this.w_FLGSTO = NVL(_Link_.TRFLGSTO,space(1))
      this.w_FLGST1 = NVL(_Link_.TRFLGST1,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI1 = space(5)
      endif
      this.w_FLGSTO = space(1)
      this.w_FLGST1 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TAB_RITE_IDX,2])+'\'+cp_ToStr(_Link_.TRTIPRIT,1)+'\'+cp_ToStr(_Link_.TRCODAZI,1)
      cp_ShowWarn(i_cKey,this.TAB_RITE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=EVICRE
  func Link_1_32(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_EVICRE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_API',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_EVICRE)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_EVICRE))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_EVICRE)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_EVICRE)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_EVICRE)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_EVICRE) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oEVICRE_1_32'),i_cWhere,'GSAR_API',"Conti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Valore non ammesso o obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_EVICRE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_EVICRE);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_EVICRE)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_EVICRE = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCRI1 = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_EVICRE = space(15)
      endif
      this.w_DESCRI1 = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Valore non ammesso o obsoleto")
        endif
        this.w_EVICRE = space(15)
        this.w_DESCRI1 = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_EVICRE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COIRVE
  func Link_1_33(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COIRVE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_API',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_COIRVE)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_COIRVE))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COIRVE)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_COIRVE)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_COIRVE)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_COIRVE) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCOIRVE_1_33'),i_cWhere,'GSAR_API',"Conti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Valore non ammesso o obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COIRVE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_COIRVE);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_COIRVE)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COIRVE = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCRIR = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_COIRVE = space(15)
      endif
      this.w_DESCRIR = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Valore non ammesso o obsoleto")
        endif
        this.w_COIRVE = space(15)
        this.w_DESCRIR = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COIRVE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COPRVE
  func Link_1_34(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COPRVE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_API',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_COPRVE)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_COPRVE))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COPRVE)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COPRVE) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCOPRVE_1_34'),i_cWhere,'GSAR_API',"Conti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Il conto selezionato � obsoleto oppure inesistente")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COPRVE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_COPRVE);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_COPRVE)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COPRVE = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCRPV = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_COPRVE = space(15)
      endif
      this.w_DESCRPV = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il conto selezionato � obsoleto oppure inesistente")
        endif
        this.w_COPRVE = space(15)
        this.w_DESCRPV = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COPRVE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CAUGIR
  func Link_1_35(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAUGIR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_CAUGIR)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_CAUGIR))
          select CCCODICE,CCDESCRI,CCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CAUGIR)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CAUGIR) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oCAUGIR_1_35'),i_cWhere,'',"Causali contabili",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAUGIR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_CAUGIR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_CAUGIR)
            select CCCODICE,CCDESCRI,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAUGIR = NVL(_Link_.CCCODICE,space(5))
      this.w_DESCRV = NVL(_Link_.CCDESCRI,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CAUGIR = space(5)
      endif
      this.w_DESCRV = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale selezionata inesistente oppure obsoleta")
        endif
        this.w_CAUGIR = space(5)
        this.w_DESCRV = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAUGIR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CONTRS
  func Link_1_36(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CONTRS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_API',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CONTRS)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_CONTRS))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CONTRS)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CONTRS) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCONTRS_1_36'),i_cWhere,'GSAR_API',"Conti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Il conto selezionato � obsoleto oppure inesistente")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CONTRS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CONTRS);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_CONTRS)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CONTRS = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCRIT1 = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CONTRS = space(15)
      endif
      this.w_DESCRIT1 = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il conto selezionato � obsoleto oppure inesistente")
        endif
        this.w_CONTRS = space(15)
        this.w_DESCRIT1 = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CONTRS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CONRIT
  func Link_1_39(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CONRIT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_API',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CONRIT)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_CONRIT))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CONRIT)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CONRIT) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCONRIT_1_39'),i_cWhere,'GSAR_API',"Conti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Il conto selezionato � obsoleto oppure inesistente")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CONRIT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CONRIT);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_CONRIT)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CONRIT = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCRITE = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CONRIT = space(15)
      endif
      this.w_DESCRITE = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il conto selezionato � obsoleto oppure inesistente")
        endif
        this.w_CONRIT = space(15)
        this.w_DESCRITE = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CONRIT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CORIPR
  func Link_1_41(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CORIPR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_API',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CORIPR)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_CORIPR))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CORIPR)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CORIPR) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCORIPR_1_41'),i_cWhere,'GSAR_API',"Conti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Il conto selezionato � obsoleto oppure inesistente")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CORIPR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CORIPR);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_CORIPR)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CORIPR = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCRIPR = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CORIPR = space(15)
      endif
      this.w_DESCRIPR = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il conto selezionato � obsoleto oppure inesistente")
        endif
        this.w_CORIPR = space(15)
        this.w_DESCRIPR = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CORIPR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GIROCO
  func Link_1_43(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GIROCO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_GIROCO)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_GIROCO))
          select CCCODICE,CCDESCRI,CCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GIROCO)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GIROCO) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oGIROCO_1_43'),i_cWhere,'',"Causali contabili",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GIROCO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_GIROCO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_GIROCO)
            select CCCODICE,CCDESCRI,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GIROCO = NVL(_Link_.CCCODICE,space(5))
      this.w_DESCRI = NVL(_Link_.CCDESCRI,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_GIROCO = space(5)
      endif
      this.w_DESCRI = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale selezionata inesistente oppure obsoleta")
        endif
        this.w_GIROCO = space(5)
        this.w_DESCRI = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GIROCO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CAUSAL
  func Link_1_44(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAUSAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_CAUSAL)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO,CCTIPREG,CCFLPART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_CAUSAL))
          select CCCODICE,CCDESCRI,CCDTOBSO,CCTIPREG,CCFLPART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CAUSAL)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CAUSAL) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oCAUSAL_1_44'),i_cWhere,'',"Causali contabili",'CAUCOSAL.CAU_CONT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO,CCTIPREG,CCFLPART";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCDTOBSO,CCTIPREG,CCFLPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAUSAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO,CCTIPREG,CCFLPART";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_CAUSAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_CAUSAL)
            select CCCODICE,CCDESCRI,CCDTOBSO,CCTIPREG,CCFLPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAUSAL = NVL(_Link_.CCCODICE,space(5))
      this.w_DESSAL = NVL(_Link_.CCDESCRI,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
      this.w_TIPSAL = NVL(_Link_.CCTIPREG,space(1))
      this.w_FLSAL = NVL(_Link_.CCFLPART,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CAUSAL = space(5)
      endif
      this.w_DESSAL = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_TIPSAL = space(1)
      this.w_FLSAL = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  AND .w_TIPSAL='N' AND .w_FLSAL='S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("La causale selezionata � inesistente,non salda partite o obsoleta")
        endif
        this.w_CAUSAL = space(5)
        this.w_DESSAL = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_TIPSAL = space(1)
        this.w_FLSAL = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAUSAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oINIDATA_1_6.value==this.w_INIDATA)
      this.oPgFrm.Page1.oPag.oINIDATA_1_6.value=this.w_INIDATA
    endif
    if not(this.oPgFrm.Page1.oPag.oFINDATA_1_7.value==this.w_FINDATA)
      this.oPgFrm.Page1.oPag.oFINDATA_1_7.value=this.w_FINDATA
    endif
    if not(this.oPgFrm.Page1.oPag.oDATAREG_1_9.RadioValue()==this.w_DATAREG)
      this.oPgFrm.Page1.oPag.oDATAREG_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oREGIDATA_1_10.value==this.w_REGIDATA)
      this.oPgFrm.Page1.oPag.oREGIDATA_1_10.value=this.w_REGIDATA
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMODIE_1_11.value==this.w_NUMODIE)
      this.oPgFrm.Page1.oPag.oNUMODIE_1_11.value=this.w_NUMODIE
    endif
    if not(this.oPgFrm.Page1.oPag.oSERIEOR_1_12.value==this.w_SERIEOR)
      this.oPgFrm.Page1.oPag.oSERIEOR_1_12.value=this.w_SERIEOR
    endif
    if not(this.oPgFrm.Page1.oPag.oDATAODIE_1_13.value==this.w_DATAODIE)
      this.oPgFrm.Page1.oPag.oDATAODIE_1_13.value=this.w_DATAODIE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRIZI_1_14.value==this.w_DESCRIZI)
      this.oPgFrm.Page1.oPag.oDESCRIZI_1_14.value=this.w_DESCRIZI
    endif
    if not(this.oPgFrm.Page1.oPag.oEVICRE_1_32.value==this.w_EVICRE)
      this.oPgFrm.Page1.oPag.oEVICRE_1_32.value=this.w_EVICRE
    endif
    if not(this.oPgFrm.Page1.oPag.oCOIRVE_1_33.value==this.w_COIRVE)
      this.oPgFrm.Page1.oPag.oCOIRVE_1_33.value=this.w_COIRVE
    endif
    if not(this.oPgFrm.Page1.oPag.oCOPRVE_1_34.value==this.w_COPRVE)
      this.oPgFrm.Page1.oPag.oCOPRVE_1_34.value=this.w_COPRVE
    endif
    if not(this.oPgFrm.Page1.oPag.oCAUGIR_1_35.value==this.w_CAUGIR)
      this.oPgFrm.Page1.oPag.oCAUGIR_1_35.value=this.w_CAUGIR
    endif
    if not(this.oPgFrm.Page1.oPag.oCONTRS_1_36.value==this.w_CONTRS)
      this.oPgFrm.Page1.oPag.oCONTRS_1_36.value=this.w_CONTRS
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRIR_1_37.value==this.w_DESCRIR)
      this.oPgFrm.Page1.oPag.oDESCRIR_1_37.value=this.w_DESCRIR
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRPV_1_38.value==this.w_DESCRPV)
      this.oPgFrm.Page1.oPag.oDESCRPV_1_38.value=this.w_DESCRPV
    endif
    if not(this.oPgFrm.Page1.oPag.oCONRIT_1_39.value==this.w_CONRIT)
      this.oPgFrm.Page1.oPag.oCONRIT_1_39.value=this.w_CONRIT
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRITE_1_40.value==this.w_DESCRITE)
      this.oPgFrm.Page1.oPag.oDESCRITE_1_40.value=this.w_DESCRITE
    endif
    if not(this.oPgFrm.Page1.oPag.oCORIPR_1_41.value==this.w_CORIPR)
      this.oPgFrm.Page1.oPag.oCORIPR_1_41.value=this.w_CORIPR
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRIPR_1_42.value==this.w_DESCRIPR)
      this.oPgFrm.Page1.oPag.oDESCRIPR_1_42.value=this.w_DESCRIPR
    endif
    if not(this.oPgFrm.Page1.oPag.oGIROCO_1_43.value==this.w_GIROCO)
      this.oPgFrm.Page1.oPag.oGIROCO_1_43.value=this.w_GIROCO
    endif
    if not(this.oPgFrm.Page1.oPag.oCAUSAL_1_44.value==this.w_CAUSAL)
      this.oPgFrm.Page1.oPag.oCAUSAL_1_44.value=this.w_CAUSAL
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_45.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_45.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRV_1_51.value==this.w_DESCRV)
      this.oPgFrm.Page1.oPag.oDESCRV_1_51.value=this.w_DESCRV
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI1_1_54.value==this.w_DESCRI1)
      this.oPgFrm.Page1.oPag.oDESCRI1_1_54.value=this.w_DESCRI1
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRIT1_1_55.value==this.w_DESCRIT1)
      this.oPgFrm.Page1.oPag.oDESCRIT1_1_55.value=this.w_DESCRIT1
    endif
    if not(this.oPgFrm.Page1.oPag.oDESSAL_1_57.value==this.w_DESSAL)
      this.oPgFrm.Page1.oPag.oDESSAL_1_57.value=this.w_DESSAL
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_INIDATA)) or not(.w_FINDATA>=.w_INIDATA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oINIDATA_1_6.SetFocus()
            i_bnoObbl = !empty(.w_INIDATA)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_FINDATA)) or not(.w_FINDATA>=.w_INIDATA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFINDATA_1_7.SetFocus()
            i_bnoObbl = !empty(.w_FINDATA)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_REGIDATA))  and not(.w_DATAREG='P')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oREGIDATA_1_10.SetFocus()
            i_bnoObbl = !empty(.w_REGIDATA)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_EVICRE)) or not(EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO))  and not(.w_TIPORI='A')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oEVICRE_1_32.SetFocus()
            i_bnoObbl = !empty(.w_EVICRE)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valore non ammesso o obsoleto")
          case   ((empty(.w_COIRVE)) or not(EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO))  and not(.w_TIPORI='V')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOIRVE_1_33.SetFocus()
            i_bnoObbl = !empty(.w_COIRVE)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valore non ammesso o obsoleto")
          case   ((empty(.w_COPRVE)) or not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(.w_TIPORI='V')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOPRVE_1_34.SetFocus()
            i_bnoObbl = !empty(.w_COPRVE)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il conto selezionato � obsoleto oppure inesistente")
          case   ((empty(.w_CAUGIR)) or not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(.w_TIPORI='A')  and (.w_FLGST1='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAUGIR_1_35.SetFocus()
            i_bnoObbl = !empty(.w_CAUGIR)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale selezionata inesistente oppure obsoleta")
          case   ((empty(.w_CONTRS)) or not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(.w_TIPORI='A')  and (.w_FLGST1='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCONTRS_1_36.SetFocus()
            i_bnoObbl = !empty(.w_CONTRS)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il conto selezionato � obsoleto oppure inesistente")
          case   ((empty(.w_CONRIT)) or not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(.w_TIPORI='V')  and (.w_FLGSTO='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCONRIT_1_39.SetFocus()
            i_bnoObbl = !empty(.w_CONRIT)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il conto selezionato � obsoleto oppure inesistente")
          case   ((empty(.w_CORIPR)) or not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(.w_TIPORI='V')  and (.w_FLGSTO='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCORIPR_1_41.SetFocus()
            i_bnoObbl = !empty(.w_CORIPR)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il conto selezionato � obsoleto oppure inesistente")
          case   ((empty(.w_GIROCO)) or not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(.w_TIPORI='V')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGIROCO_1_43.SetFocus()
            i_bnoObbl = !empty(.w_GIROCO)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale selezionata inesistente oppure obsoleta")
          case   ((empty(.w_CAUSAL)) or not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  AND .w_TIPSAL='N' AND .w_FLSAL='S'))  and not(.w_TIPORI='A')  and (.w_FLGST1<>'S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAUSAL_1_44.SetFocus()
            i_bnoObbl = !empty(.w_CAUSAL)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La causale selezionata � inesistente,non salda partite o obsoleta")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TIPCON = this.w_TIPCON
    this.o_FLGSTO = this.w_FLGSTO
    this.o_FLGST1 = this.w_FLGST1
    return

enddefine

* --- Define pages as container
define class tgsri_kggPag1 as StdContainer
  Width  = 598
  height = 392
  stdWidth  = 598
  stdheight = 392
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oINIDATA_1_6 as StdField with uid="CKZXVWVUBM",rtseq=6,rtrep=.f.,;
    cFormVar = "w_INIDATA", cQueryName = "INIDATA",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data registrazione di inizio selezione",;
    HelpContextID = 107932806,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=183, Top=44

  func oINIDATA_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_FINDATA>=.w_INIDATA)
    endwith
    return bRes
  endfunc

  add object oFINDATA_1_7 as StdField with uid="RFUDMZELTX",rtseq=7,rtrep=.f.,;
    cFormVar = "w_FINDATA", cQueryName = "FINDATA",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data registrazione di fine selezione",;
    HelpContextID = 107951958,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=457, Top=44

  func oFINDATA_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_FINDATA>=.w_INIDATA)
    endwith
    return bRes
  endfunc


  add object oDATAREG_1_9 as StdCombo with uid="XWKNJXTCSI",rtseq=8,rtrep=.f.,left=184,top=85,width=118,height=21;
    , ToolTipText = "Sintetica: unico movimento; dettagliata: un movimento per ogni ritenuta";
    , HelpContextID = 126054602;
    , cFormVar="w_DATAREG",RowSource=""+"Sintetica,"+"Dettagliata", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDATAREG_1_9.RadioValue()
    return(iif(this.value =1,'O',;
    iif(this.value =2,'P',;
    space(1))))
  endfunc
  func oDATAREG_1_9.GetRadio()
    this.Parent.oContained.w_DATAREG = this.RadioValue()
    return .t.
  endfunc

  func oDATAREG_1_9.SetRadio()
    this.Parent.oContained.w_DATAREG=trim(this.Parent.oContained.w_DATAREG)
    this.value = ;
      iif(this.Parent.oContained.w_DATAREG=='O',1,;
      iif(this.Parent.oContained.w_DATAREG=='P',2,;
      0))
  endfunc

  func oDATAREG_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((.w_FLGSTO='S' AND .w_TIPORI='A')  OR ( .w_FLGST1='S' AND .w_TIPORI='V'))
    endwith
   endif
  endfunc

  add object oREGIDATA_1_10 as StdField with uid="VLKRCVXEEH",rtseq=9,rtrep=.f.,;
    cFormVar = "w_REGIDATA", cQueryName = "REGIDATA",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data registrazione del movimento",;
    HelpContextID = 61064279,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=461, Top=85

  func oREGIDATA_1_10.mHide()
    with this.Parent.oContained
      return (.w_DATAREG='P')
    endwith
  endfunc

  add object oNUMODIE_1_11 as StdField with uid="KFHBGVZBXM",rtseq=10,rtrep=.f.,;
    cFormVar = "w_NUMODIE", cQueryName = "NUMODIE",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero del documento",;
    HelpContextID = 195703766,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=183, Top=123, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  func oNUMODIE_1_11.mHide()
    with this.Parent.oContained
      return (.w_DATAREG='P')
    endwith
  endfunc

  add object oSERIEOR_1_12 as StdField with uid="OXCTCSMHTF",rtseq=11,rtrep=.f.,;
    cFormVar = "w_SERIEOR", cQueryName = "SERIEOR",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie del documento",;
    HelpContextID = 28603430,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=312, Top=123, cSayPict='"!!!!!!!!!!"', cGetPict='"!!!!!!!!!!"', InputMask=replicate('X',10)

  func oSERIEOR_1_12.mHide()
    with this.Parent.oContained
      return (.w_DATAREG='P')
    endwith
  endfunc

  add object oDATAODIE_1_13 as StdField with uid="PLRKLGOAVS",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DATAODIE", cQueryName = "DATAODIE",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data documento",;
    HelpContextID = 145977477,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=461, Top=123

  func oDATAODIE_1_13.mHide()
    with this.Parent.oContained
      return (.w_Datareg='P')
    endwith
  endfunc

  add object oDESCRIZI_1_14 as StdField with uid="OBXXKZYCCI",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DESCRIZI", cQueryName = "DESCRIZI",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione del movimento di giroconto da generare",;
    HelpContextID = 58817665,;
   bGlobalFont=.t.,;
    Height=21, Width=352, Left=183, Top=157, InputMask=replicate('X',50)


  add object oBtn_1_20 as StdButton with uid="IZQIBPSTVD",left=491, top=341, width=48,height=45,;
    CpPicture="BMP\GENERA.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per generare giroconto ritenute";
    , HelpContextID = 111301478;
    , caption='\<Genera';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_20.Click()
      with this.Parent.oContained
        do GSRI_BGG with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_21 as StdButton with uid="GGCFXHPQLH",left=542, top=341, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 24792762;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_21.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_26 as cp_runprogram with uid="CGGJNJHEFF",left=6, top=402, width=182,height=22,;
    caption='GSRI_BDT(NEW)',;
   bGlobalFont=.t.,;
    prg='GSRI_BDT("New")',;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 155198010

  add object oEVICRE_1_32 as StdField with uid="LGOZMEYXHZ",rtseq=18,rtrep=.f.,;
    cFormVar = "w_EVICRE", cQueryName = "EVICRE",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Valore non ammesso o obsoleto",;
    ToolTipText = "Conto di evidenza credito v/erario per ritenute subite",;
    HelpContextID = 142472262,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=183, Top=198, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_API", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_EVICRE"

  func oEVICRE_1_32.mHide()
    with this.Parent.oContained
      return (.w_TIPORI='A')
    endwith
  endfunc

  func oEVICRE_1_32.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_32('Part',this)
    endwith
    return bRes
  endfunc

  proc oEVICRE_1_32.ecpDrop(oSource)
    this.Parent.oContained.link_1_32('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oEVICRE_1_32.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oEVICRE_1_32'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_API',"Conti",'',this.parent.oContained
  endproc
  proc oEVICRE_1_32.mZoomOnZoom
    local i_obj
    i_obj=GSAR_API()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_EVICRE
     i_obj.ecpSave()
  endproc

  add object oCOIRVE_1_33 as StdField with uid="JTIYWAEYMF",rtseq=19,rtrep=.f.,;
    cFormVar = "w_COIRVE", cQueryName = "COIRVE",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Valore non ammesso o obsoleto",;
    ToolTipText = "Erario c\ritenute IRPEF da versare",;
    HelpContextID = 147647782,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=183, Top=198, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_API", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_COIRVE"

  func oCOIRVE_1_33.mHide()
    with this.Parent.oContained
      return (.w_TIPORI='V')
    endwith
  endfunc

  func oCOIRVE_1_33.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_33('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOIRVE_1_33.ecpDrop(oSource)
    this.Parent.oContained.link_1_33('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOIRVE_1_33.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCOIRVE_1_33'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_API',"Conti",'',this.parent.oContained
  endproc
  proc oCOIRVE_1_33.mZoomOnZoom
    local i_obj
    i_obj=GSAR_API()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_COIRVE
     i_obj.ecpSave()
  endproc

  add object oCOPRVE_1_34 as StdField with uid="YBGCIKKLLH",rtseq=20,rtrep=.f.,;
    cFormVar = "w_COPRVE", cQueryName = "COPRVE",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il conto selezionato � obsoleto oppure inesistente",;
    ToolTipText = "Erario c\ritenute previdenziali da versare",;
    HelpContextID = 147676454,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=183, Top=227, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_API", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_COPRVE"

  func oCOPRVE_1_34.mHide()
    with this.Parent.oContained
      return (.w_TIPORI='V')
    endwith
  endfunc

  func oCOPRVE_1_34.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_34('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOPRVE_1_34.ecpDrop(oSource)
    this.Parent.oContained.link_1_34('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOPRVE_1_34.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCOPRVE_1_34'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_API',"Conti",'',this.parent.oContained
  endproc
  proc oCOPRVE_1_34.mZoomOnZoom
    local i_obj
    i_obj=GSAR_API()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_COPRVE
     i_obj.ecpSave()
  endproc

  add object oCAUGIR_1_35 as StdField with uid="ODTKIIXOJD",rtseq=21,rtrep=.f.,;
    cFormVar = "w_CAUGIR", cQueryName = "CAUGIR",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale selezionata inesistente oppure obsoleta",;
    ToolTipText = "Causale di giroconto utilizzata in caso di gestione storno immediato",;
    HelpContextID = 83009318,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=183, Top=258, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", oKey_1_1="CCCODICE", oKey_1_2="this.w_CAUGIR"

  func oCAUGIR_1_35.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLGST1='S')
    endwith
   endif
  endfunc

  func oCAUGIR_1_35.mHide()
    with this.Parent.oContained
      return (.w_TIPORI='A')
    endwith
  endfunc

  func oCAUGIR_1_35.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_35('Part',this)
    endwith
    return bRes
  endfunc

  proc oCAUGIR_1_35.ecpDrop(oSource)
    this.Parent.oContained.link_1_35('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCAUGIR_1_35.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oCAUGIR_1_35'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Causali contabili",'',this.parent.oContained
  endproc

  add object oCONTRS_1_36 as StdField with uid="HAKWESWXYZ",rtseq=22,rtrep=.f.,;
    cFormVar = "w_CONTRS", cQueryName = "CONTRS",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il conto selezionato � obsoleto oppure inesistente",;
    ToolTipText = "Conto di transito (da utilizzarsi solo per flag storno immediato attivo)",;
    HelpContextID = 110050598,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=183, Top=227, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_API", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CONTRS"

  func oCONTRS_1_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLGST1='S')
    endwith
   endif
  endfunc

  func oCONTRS_1_36.mHide()
    with this.Parent.oContained
      return (.w_TIPORI='A')
    endwith
  endfunc

  func oCONTRS_1_36.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_36('Part',this)
    endwith
    return bRes
  endfunc

  proc oCONTRS_1_36.ecpDrop(oSource)
    this.Parent.oContained.link_1_36('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCONTRS_1_36.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCONTRS_1_36'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_API',"Conti",'',this.parent.oContained
  endproc
  proc oCONTRS_1_36.mZoomOnZoom
    local i_obj
    i_obj=GSAR_API()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CONTRS
     i_obj.ecpSave()
  endproc

  add object oDESCRIR_1_37 as StdField with uid="MOCFDQILHK",rtseq=23,rtrep=.f.,;
    cFormVar = "w_DESCRIR", cQueryName = "DESCRIR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 209617718,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=302, Top=198, InputMask=replicate('X',40)

  func oDESCRIR_1_37.mHide()
    with this.Parent.oContained
      return (.w_TIPORI='V')
    endwith
  endfunc

  add object oDESCRPV_1_38 as StdField with uid="AVCVKDEYFI",rtseq=24,rtrep=.f.,;
    cFormVar = "w_DESCRPV", cQueryName = "DESCRPV",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 209812682,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=302, Top=227, InputMask=replicate('X',40)

  func oDESCRPV_1_38.mHide()
    with this.Parent.oContained
      return (.w_TIPORI='V')
    endwith
  endfunc

  add object oCONRIT_1_39 as StdField with uid="LUOPTMCIAY",rtseq=25,rtrep=.f.,;
    cFormVar = "w_CONRIT", cQueryName = "CONRIT",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il conto selezionato � obsoleto oppure inesistente",;
    ToolTipText = "Erario c\ritenute",;
    HelpContextID = 117259558,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=183, Top=257, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_API", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CONRIT"

  func oCONRIT_1_39.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLGSTO='S')
    endwith
   endif
  endfunc

  func oCONRIT_1_39.mHide()
    with this.Parent.oContained
      return (.w_TIPORI='V')
    endwith
  endfunc

  func oCONRIT_1_39.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_39('Part',this)
    endwith
    return bRes
  endfunc

  proc oCONRIT_1_39.ecpDrop(oSource)
    this.Parent.oContained.link_1_39('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCONRIT_1_39.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCONRIT_1_39'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_API',"Conti",'',this.parent.oContained
  endproc
  proc oCONRIT_1_39.mZoomOnZoom
    local i_obj
    i_obj=GSAR_API()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CONRIT
     i_obj.ecpSave()
  endproc

  add object oDESCRITE_1_40 as StdField with uid="DMCAUCLWQA",rtseq=26,rtrep=.f.,;
    cFormVar = "w_DESCRITE", cQueryName = "DESCRITE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 209617787,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=302, Top=257, InputMask=replicate('X',40)

  func oDESCRITE_1_40.mHide()
    with this.Parent.oContained
      return (.w_TIPORI='V')
    endwith
  endfunc

  add object oCORIPR_1_41 as StdField with uid="CHVIGYOMLH",rtseq=27,rtrep=.f.,;
    cFormVar = "w_CORIPR", cQueryName = "CORIPR",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il conto selezionato � obsoleto oppure inesistente",;
    ToolTipText = "Erario c\ritenute previdenziali",;
    HelpContextID = 90471718,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=183, Top=286, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_API", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CORIPR"

  func oCORIPR_1_41.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLGSTO='S')
    endwith
   endif
  endfunc

  func oCORIPR_1_41.mHide()
    with this.Parent.oContained
      return (.w_TIPORI='V')
    endwith
  endfunc

  func oCORIPR_1_41.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_41('Part',this)
    endwith
    return bRes
  endfunc

  proc oCORIPR_1_41.ecpDrop(oSource)
    this.Parent.oContained.link_1_41('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCORIPR_1_41.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCORIPR_1_41'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_API',"Conti",'',this.parent.oContained
  endproc
  proc oCORIPR_1_41.mZoomOnZoom
    local i_obj
    i_obj=GSAR_API()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CORIPR
     i_obj.ecpSave()
  endproc

  add object oDESCRIPR_1_42 as StdField with uid="LATFLJQGRN",rtseq=28,rtrep=.f.,;
    cFormVar = "w_DESCRIPR", cQueryName = "DESCRIPR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 209617800,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=302, Top=287, InputMask=replicate('X',40)

  func oDESCRIPR_1_42.mHide()
    with this.Parent.oContained
      return (.w_TIPORI='V')
    endwith
  endfunc

  add object oGIROCO_1_43 as StdField with uid="SFQIAXRSBJ",rtseq=29,rtrep=.f.,;
    cFormVar = "w_GIROCO", cQueryName = "GIROCO",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale selezionata inesistente oppure obsoleta",;
    ToolTipText = "Causale di giroconto utilizzata nel caso di gestione storno immediato",;
    HelpContextID = 26900326,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=183, Top=315, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", oKey_1_1="CCCODICE", oKey_1_2="this.w_GIROCO"

  func oGIROCO_1_43.mHide()
    with this.Parent.oContained
      return (.w_TIPORI='V')
    endwith
  endfunc

  func oGIROCO_1_43.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_43('Part',this)
    endwith
    return bRes
  endfunc

  proc oGIROCO_1_43.ecpDrop(oSource)
    this.Parent.oContained.link_1_43('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGIROCO_1_43.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oGIROCO_1_43'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Causali contabili",'',this.parent.oContained
  endproc

  add object oCAUSAL_1_44 as StdField with uid="VEEUUIHKGC",rtseq=30,rtrep=.f.,;
    cFormVar = "w_CAUSAL", cQueryName = "CAUSAL",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "La causale selezionata � inesistente,non salda partite o obsoleta",;
    ToolTipText = "Causale di saldaconto utilizzata in caso di gestione storno differito",;
    HelpContextID = 243179302,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=183, Top=286, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", oKey_1_1="CCCODICE", oKey_1_2="this.w_CAUSAL"

  func oCAUSAL_1_44.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLGST1<>'S')
    endwith
   endif
  endfunc

  func oCAUSAL_1_44.mHide()
    with this.Parent.oContained
      return (.w_TIPORI='A')
    endwith
  endfunc

  func oCAUSAL_1_44.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_44('Part',this)
    endwith
    return bRes
  endfunc

  proc oCAUSAL_1_44.ecpDrop(oSource)
    this.Parent.oContained.link_1_44('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCAUSAL_1_44.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oCAUSAL_1_44'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Causali contabili",'CAUCOSAL.CAU_CONT_VZM',this.parent.oContained
  endproc

  add object oDESCRI_1_45 as StdField with uid="ZJQOZHMIMQ",rtseq=31,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 209617718,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=250, Top=315, InputMask=replicate('X',35)

  func oDESCRI_1_45.mHide()
    with this.Parent.oContained
      return (.w_TIPORI='V')
    endwith
  endfunc

  add object oDESCRV_1_51 as StdField with uid="POXUXAHBXM",rtseq=32,rtrep=.f.,;
    cFormVar = "w_DESCRV", cQueryName = "DESCRV",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 159286070,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=249, Top=258, InputMask=replicate('X',35)

  func oDESCRV_1_51.mHide()
    with this.Parent.oContained
      return (.w_TIPORI='A')
    endwith
  endfunc

  add object oDESCRI1_1_54 as StdField with uid="XEXJGHXKWZ",rtseq=33,rtrep=.f.,;
    cFormVar = "w_DESCRI1", cQueryName = "DESCRI1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 209617718,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=302, Top=198, InputMask=replicate('X',40)

  func oDESCRI1_1_54.mHide()
    with this.Parent.oContained
      return (.w_TIPORI='A')
    endwith
  endfunc

  add object oDESCRIT1_1_55 as StdField with uid="SICPIGFMKM",rtseq=34,rtrep=.f.,;
    cFormVar = "w_DESCRIT1", cQueryName = "DESCRIT1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 209617767,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=302, Top=227, InputMask=replicate('X',40)

  func oDESCRIT1_1_55.mHide()
    with this.Parent.oContained
      return (.w_TIPORI='A')
    endwith
  endfunc

  add object oDESSAL_1_57 as StdField with uid="JMIIHRYUTQ",rtseq=35,rtrep=.f.,;
    cFormVar = "w_DESSAL", cQueryName = "DESSAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 243172150,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=249, Top=287, InputMask=replicate('X',35)

  func oDESSAL_1_57.mHide()
    with this.Parent.oContained
      return (.w_TIPORI='A')
    endwith
  endfunc

  add object oStr_1_8 as StdString with uid="DCZLQWTSFJ",Visible=.t., Left=336, Top=86,;
    Alignment=1, Width=120, Height=15,;
    Caption="Data registrazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_8.mHide()
    with this.Parent.oContained
      return (.w_DATAREG='P')
    endwith
  endfunc

  add object oStr_1_15 as StdString with uid="IROCGVZCFI",Visible=.t., Left=89, Top=123,;
    Alignment=1, Width=88, Height=15,;
    Caption="Doc.N.:"  ;
  , bGlobalFont=.t.

  func oStr_1_15.mHide()
    with this.Parent.oContained
      return (.w_DATAREG='P')
    endwith
  endfunc

  add object oStr_1_16 as StdString with uid="YEYXEAXEYF",Visible=.t., Left=300, Top=125,;
    Alignment=2, Width=11, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  func oStr_1_16.mHide()
    with this.Parent.oContained
      return (.w_DATAREG='P')
    endwith
  endfunc

  add object oStr_1_17 as StdString with uid="PAZOJHBLHL",Visible=.t., Left=403, Top=121,;
    Alignment=1, Width=53, Height=18,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  func oStr_1_17.mHide()
    with this.Parent.oContained
      return (.w_DATAREG='P')
    endwith
  endfunc

  add object oStr_1_23 as StdString with uid="DPDKVVOWAM",Visible=.t., Left=21, Top=10,;
    Alignment=0, Width=233, Height=15,;
    Caption="Generazione giroconto ritenute"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_24 as StdString with uid="SIKDKRGXXR",Visible=.t., Left=11, Top=44,;
    Alignment=1, Width=166, Height=15,;
    Caption="Ritenute generate dal:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="OMVIHLBGRK",Visible=.t., Left=415, Top=42,;
    Alignment=1, Width=41, Height=18,;
    Caption="Al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="PUZJNGUVXD",Visible=.t., Left=21, Top=157,;
    Alignment=1, Width=156, Height=15,;
    Caption="Descrizione giroconto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="OJGKKPJHOR",Visible=.t., Left=21, Top=85,;
    Alignment=1, Width=156, Height=15,;
    Caption="Tipo registrazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="DJOQZHKZTS",Visible=.t., Left=11, Top=201,;
    Alignment=1, Width=166, Height=15,;
    Caption="Ritenute IRPEF da versare:"  ;
  , bGlobalFont=.t.

  func oStr_1_46.mHide()
    with this.Parent.oContained
      return (.w_TIPORI='V')
    endwith
  endfunc

  add object oStr_1_47 as StdString with uid="YGNURHCSEK",Visible=.t., Left=11, Top=230,;
    Alignment=1, Width=166, Height=15,;
    Caption="Ritenute prev. da versare:"  ;
  , bGlobalFont=.t.

  func oStr_1_47.mHide()
    with this.Parent.oContained
      return (.w_TIPORI='V')
    endwith
  endfunc

  add object oStr_1_48 as StdString with uid="HQOTQQTCDI",Visible=.t., Left=11, Top=317,;
    Alignment=1, Width=166, Height=18,;
    Caption="Causale giroconto:"  ;
  , bGlobalFont=.t.

  func oStr_1_48.mHide()
    with this.Parent.oContained
      return (.w_TIPORI='V')
    endwith
  endfunc

  add object oStr_1_49 as StdString with uid="FJLTTIPRDJ",Visible=.t., Left=11, Top=261,;
    Alignment=1, Width=166, Height=15,;
    Caption="Erario c\ritenute:"  ;
  , bGlobalFont=.t.

  func oStr_1_49.mHide()
    with this.Parent.oContained
      return (.w_TIPORI='V')
    endwith
  endfunc

  add object oStr_1_50 as StdString with uid="OVQFOZFMRI",Visible=.t., Left=11, Top=289,;
    Alignment=1, Width=166, Height=18,;
    Caption="Rit. previdenziali:"  ;
  , bGlobalFont=.t.

  func oStr_1_50.mHide()
    with this.Parent.oContained
      return (.w_TIPORI='V')
    endwith
  endfunc

  add object oStr_1_52 as StdString with uid="QZBRTIXTWD",Visible=.t., Left=11, Top=230,;
    Alignment=1, Width=166, Height=18,;
    Caption="Conto di transito rit.:"  ;
  , bGlobalFont=.t.

  func oStr_1_52.mHide()
    with this.Parent.oContained
      return (.w_TIPORI='A')
    endwith
  endfunc

  add object oStr_1_53 as StdString with uid="AYUIUBHPHY",Visible=.t., Left=11, Top=201,;
    Alignment=1, Width=166, Height=15,;
    Caption="Credito v/erario:"  ;
  , bGlobalFont=.t.

  func oStr_1_53.mHide()
    with this.Parent.oContained
      return (.w_TIPORI='A')
    endwith
  endfunc

  add object oStr_1_56 as StdString with uid="WFAIBEWNWY",Visible=.t., Left=11, Top=260,;
    Alignment=1, Width=166, Height=15,;
    Caption="Causale Giroconto:"  ;
  , bGlobalFont=.t.

  func oStr_1_56.mHide()
    with this.Parent.oContained
      return (.w_TIPORI='A')
    endwith
  endfunc

  add object oStr_1_58 as StdString with uid="MHBUQUEVBZ",Visible=.t., Left=11, Top=289,;
    Alignment=1, Width=166, Height=18,;
    Caption="Causale saldaconto:"  ;
  , bGlobalFont=.t.

  func oStr_1_58.mHide()
    with this.Parent.oContained
      return (.w_TIPORI='A')
    endwith
  endfunc

  add object oBox_1_22 as StdBox with uid="RUTRWUKSZG",left=8, top=75, width=572,height=2

  add object oBox_1_27 as StdBox with uid="BGERMTPRCR",left=8, top=184, width=572,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsri_kgg','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
