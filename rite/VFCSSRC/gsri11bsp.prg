* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri11bsp                                                       *
*              STAMPA PDF  770/2014                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-07-19                                                      *
* Last revis.: 2014-07-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsri11bsp",oParentObject)
return(i_retval)

define class tgsri11bsp as StdBatch
  * --- Local variables
  w_TOTRITACC = 0
  w_EMAIL = space(100)
  w_PHONE = space(12)
  w_QUADRO = 0
  w_TRASMATT = 0
  w_DATFIN = ctod("  /  /  ")
  w_DATINI = ctod("  /  /  ")
  w_DataAppo = space(8)
  w_ANNO = space(4)
  w_CODVAL = space(3)
  w_SOSPENDI = .f.
  w_DECIMI = space(1)
  w_VALUTA = space(1)
  w_PERCIN = space(15)
  w_PERCFIN = space(15)
  w_STAMPAST = space(1)
  w_STAMPAH = space(1)
  w_STAMPAJ = space(1)
  w_PRIMOAVV = .f.
  w_TIPOPERAZ = space(1)
  w_REGAGE = space(1)
  w_CALCOLI = 0
  w_MESS = space(254)
  w_DATIPRES = .f.
  w_RESFDF = 0
  w_MRCODCON = space(15)
  w_CODSNS = space(1)
  w_PERCIPCF = space(16)
  w_CAUPRE = space(1)
  w_PROVIN = space(2)
  w_ANDESCRI = space(40)
  w_LOCALEST = space(30)
  w_INDIREST = space(35)
  w_NAZIOEST = space(3)
  w_NACODEST = space(5)
  w_CFPEREST = space(20)
  w_OLDPERCF = space(20)
  w_OLDPER = space(15)
  w_ERROR = .f.
  w_PERCESTE = space(1)
  w_EREDE = space(1)
  w_EVENECC = space(1)
  w_BLOCK = .f.
  w_ATTENTION = .f.
  * --- WorkFile variables
  AZIENDA_idx=0
  CONTI_idx=0
  DES_DIVE_idx=0
  NAZIONI_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch per l'elaborazione dei dati relativi al frontespizio del Mod. 770/2011, per la stampa su formato PDF
    * --- Creo il cursore __TMP__ contenente i dati presenti nel frontespizio, tali dati saranno elaborati dalla CP_FFDF
    * --- Sez. I
    * --- Sez. II
    * --- Sez. IV
    * --- Sez. Firma / Imp. trasmis.
    * --- Sez. Domicilio per la notificazione degli atti
    * --- Replicata per essere visibile nelle vqr utilizzate in GSRI_BSC e per non modiifcare ilnome del parametro
    this.w_PRIMOAVV = .T.
    this.w_TIPOPERAZ = this.oParentObject.w_TIPOPERAZ
    this.w_ANNO = this.oParentObject.w_ANNO
    this.w_VALUTA = this.oParentObject.w_VALUTA
    this.w_DECIMI = this.oParentObject.w_DECIMI
    this.w_DATFIN = this.oParentObject.w_DATFIN
    this.w_DATINI = this.oParentObject.w_DATINI
    this.w_PERCIN = this.oParentObject.w_PERCIN
    this.w_PERCFIN = this.oParentObject.w_PERCFIN
    this.w_CODVAL = this.oParentObject.w_CODVAL
    this.w_SOSPENDI = .F.
    this.w_STAMPAST = "S"
    this.w_STAMPAH = "S"
    this.w_STAMPAJ = "S"
    DECLARE ARRFDF (1,2)
    ARRFDF (1,1) = "RFFISSED"
    ARRFDF (1,2) = 11
    this.w_PHONE = iif(empty(this.oParentObject.w_TELEFONO),this.oParentObject.w_SETELEFONO,this.oParentObject.w_TELEFONO)
    CREATE CURSOR __TMP__ (COGDEN C(60), NOME C(20), CODFIS C(16), CODFISC C(16), CORRET C(1),; 
 INTEGRA C(1), EVEECC C(1),CODEATT C(6), TELNUM C(12), FAXPREF C(4), FAXNUM C(8), EMAIL C(100), ; 
 PFCOMUNE C(40), PFPROV C(2),PFDATNAS1 C(2), PFDATNAS2 C(2), PFDATNAS3 C(4), PFMAS C(1), PFFEM C(1), ; 
 PFCOMRES C(40), PFPRORES C(2),PFVIARES C(35), PFCAPRES C(5), PFDATVAR1 C(2), PFDATVAR2 C(2),; 
 PFDATVAR3 C(4), ASSEDLEG1 C(2), ASSEDLEG2 C(4),ASCOMUNE C(40), ASPROV C(3), ASVIA C(35),; 
 ASCAP C(24), ASDOMFIS1 C(2), ASDOMFIS2 C(4), ASCOMFIS C(40), ASPROFIS C(2), ; 
 ASVIAFIS C(35), ASCAPFIS C(5), STATO C(1), NATGIU C(2), SITUAZ C(1), CFDICAPP C(11), RFCODFIS C(16),; 
 CODCAR C(2),RFCOG C(24), RFNOM C(20), RFSEXM C(1), RFSEXF C(1), RFDATNAS1 C(2), RFDATNAS2 C(2), ; 
 RFDATNAS3 C(4), RFCOMNAS C(40),RFPRONAS C(2), RFCOMUNE C(24), RFPROV C(3), RFCAP C(24), RFVIA C(35), ; 
 RFNUM C(12), NUMCOM N(8) ,ST C(1), SX C(1),ST4 C(1),SX4 C(1),S71 C(1),S74 C(1),RFCFINS2 C(16), ; 
 NUMCOM2 N(8) ,CFINTER C(16),NUMCAF C(5), IMPTRAS C(1), DATIMP1 C(2), DATIMP2 C(2),DATIMP3 C(4), ; 
 CFRESCAF C(16), ART35 C(1),NUMCOM4 N(8),DATCAR1 C(2),DATCAR2 C(2),DATCAR3 C(4), ; 
 DATAFAL1 C(2),DATAFAL2 C(2),DATAFAL3 C(4),RECODCOM C(4),SECODCOM C(4), ; 
 SERCODCOM C(4),CFDELCAF C(11),CFPROCAF C(16),CFRESPCAF C(16), ; 
 FOCODSOS C(16),INVAVTEL C(1),RICAVTEL C(1),; 
 FLCONCF C(1),SS1 C(1),SS2 C(1),SS4 C(1),CORINTPA C(1),SV1 C(1),SV4 C(1),RFFISSED C(11),SY C(1),SY2 C(1),SY4 C(1),; 
 SOGG1 C(1),SOGG2 C(1),SOGG3 C(1),SOGG4 C(1),SOGG5 C(1),SITPART C(2),; 
 CODFIS_1 C(16),CODFIS_2 C(16),CODFIS_3 C(16),CODFIS_4 C(16),CODFIS_5 C(16),; 
 NOTRASMIS C(1),ATTESTA C(1))
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZ_EMAIL"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZ_EMAIL;
        from (i_cTable) where;
            AZCODAZI = i_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_EMAIL = NVL(cp_ToDate(_read_.AZ_EMAIL),cp_NullValue(_read_.AZ_EMAIL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Memorizzo i dati nel cursore __TMP__
    INSERT INTO __TMP__ VALUES(iif (empty(this.oParentObject.w_FODENOMINA),upper(this.oParentObject.w_FOCOGNOME),; 
 upper(this.oParentObject.w_FODENOMINA)),upper(this.oParentObject.w_FONOME),this.oParentObject.w_FOCODFIS,this.oParentObject.w_FOCODFIS, ; 
 iif(this.oParentObject.w_FLAGCOR="1","X",""),iif(this.oParentObject.w_FLAGINT="1","X",""),; 
 iif(this.oParentObject.w_CODEVECC="0"," ",this.oParentObject.w_CODEVECC),iif(empty(this.oParentObject.w_SECODATT),this.oParentObject.w_CODATT,this.oParentObject.w_SECODATT), ; 
 this.w_PHONE, space(4),space(8),upper(this.w_EMAIL),upper(this.oParentObject.w_COMUNE),upper(this.oParentObject.w_SIGLA), ; 
 iif(empty(this.oParentObject.w_DATANASC),"",right("0"+alltrim(str(day(this.oParentObject.w_DATANASC))),2)),; 
 iif(empty(this.oParentObject.w_DATANASC),"",right("0"+alltrim(str(month(this.oParentObject.w_DATANASC))),2)), ; 
 iif(empty(this.oParentObject.w_DATANASC),"",alltrim(str(year(this.oParentObject.w_DATANASC)))),; 
 iif(this.oParentObject.w_SESSO="M","X",""),iif(this.oParentObject.w_SESSO="F","X",""),upper(this.oParentObject.w_RECOMUNE),upper(this.oParentObject.w_RESIGLA),upper(this.oParentObject.w_INDIRIZ),; 
 this.oParentObject.w_CAP,iif(empty(this.oParentObject.w_VARRES),"",right("0"+alltrim(str(day(this.oParentObject.w_VARRES))),2)), ; 
 iif(empty(this.oParentObject.w_VARRES),"",right("0"+alltrim(str(month(this.oParentObject.w_VARRES))),2)), ; 
 iif(empty(this.oParentObject.w_VARRES),"",alltrim(str(year(this.oParentObject.w_VARRES)))),; 
 iif(empty(this.oParentObject.w_SEVARSED),"",right("0"+alltrim(str(month(this.oParentObject.w_SEVARSED))),2)), ; 
 iif(empty(this.oParentObject.w_SEVARSED),"",alltrim(str(year(this.oParentObject.w_SEVARSED)))), upper(this.oParentObject.w_SECOMUNE),; 
 upper(this.oParentObject.w_SESIGLA),upper(this.oParentObject.w_SEINDIRI2),this.oParentObject.w_SECAP, ; 
 iif(empty(this.oParentObject.w_SEVARDOM),"",right("0"+alltrim(str(month(this.oParentObject.w_SEVARDOM))),2)),; 
 iif(empty(this.oParentObject.w_SEVARDOM),"",alltrim(str(year(this.oParentObject.w_SEVARDOM)))),upper(this.oParentObject.w_SERCOMUN), ; 
 upper(this.oParentObject.w_SERSIGLA),upper(this.oParentObject.w_SERINDIR),this.oParentObject.w_SERCAP, this.oParentObject.w_STATO,; 
 iif(empty(this.oParentObject.w_NATGIU),"  ",right("00"+alltrim(this.oParentObject.w_NATGIU),2)),this.oParentObject.w_SITUAZ,this.oParentObject.w_CODFISDA,this.oParentObject.w_RFCODFIS,; 
 iif(empty(this.oParentObject.w_RFCODCAR),"  ",right("00"+alltrim(this.oParentObject.w_RFCODCAR),2)), ; 
 iif(!empty(this.oParentObject.w_RFDENOMI),upper(this.oParentObject.w_RFDENOMI),upper(this.oParentObject.w_RFCOGNOME)),iif(!empty(this.oParentObject.w_RFDENOMI),"",; 
 upper(this.oParentObject.w_RFNOME)), iif(this.oParentObject.w_RFSESSO="M","X",""),iif(this.oParentObject.w_RFSESSO="F","X",""),; 
 iif(empty(this.oParentObject.w_RFDATANASC),"",right("0"+alltrim(str(day(this.oParentObject.w_RFDATANASC))),2)), ; 
 iif(empty(this.oParentObject.w_RFDATANASC),"",right("0"+alltrim(str(month(this.oParentObject.w_RFDATANASC))),2)),; 
 iif(empty(this.oParentObject.w_RFDATANASC),"",alltrim(str(year(this.oParentObject.w_RFDATANASC)))), ; 
 upper(this.oParentObject.w_RFCOMNAS),upper(this.oParentObject.w_RFSIGNAS),upper(this.oParentObject.w_RFCOMUNE),upper(this.oParentObject.w_RFSIGLA), this.oParentObject.w_RFCAP,; 
 upper(this.oParentObject.w_RFINDIRIZ),this.oParentObject.w_RFTELEFONO,iif(this.oParentObject.w_SEZ1="1",this.oParentObject.w_NUMCERTIF2,0), ; 
 iif(this.oParentObject.w_qST1="1","X",""),iif(this.oParentObject.w_qSX1="1","X",""),iif(this.oParentObject.w_qST14="1","X",""),iif(this.oParentObject.w_qSX14="1","X",""),; 
 iif(this.oParentObject.w_q771="1" ,"X",""),iif(this.oParentObject.w_q774="1","X",""),this.oParentObject.w_RFCFINS2,iif(this.oParentObject.w_SEZ2="1",this.oParentObject.w_NUMCERTIF2,0),this.oParentObject.w_CODFISIN, ; 
 iif(this.oParentObject.w_NUMCAF=0,"",str(this.oParentObject.w_NUMCAF,5)),this.oParentObject.w_IMPTRTEL, ; 
 iif(empty(this.oParentObject.w_RFDATIMP),"",right("0"+alltrim(str(day(this.oParentObject.w_RFDATIMP))),2)),; 
 iif(empty(this.oParentObject.w_RFDATIMP),"",right("0"+alltrim(str(month(this.oParentObject.w_RFDATIMP))),2)), ; 
 iif(empty(this.oParentObject.w_RFDATIMP),"",alltrim(str(year(this.oParentObject.w_RFDATIMP)))),this.oParentObject.w_CFRESCAF,"",; 
 iif(this.oParentObject.w_SEZ4="1",this.oParentObject.w_NUMCERTIF2,0), ; 
 iif(empty(this.oParentObject.w_RFDATCAR),"",right("0"+alltrim(str(day(this.oParentObject.w_RFDATCAR))),2)),iif(empty(this.oParentObject.w_RFDATCAR),"",; 
 right("0"+alltrim(str(month(this.oParentObject.w_RFDATCAR))),2)), ; 
 iif(empty(this.oParentObject.w_RFDATCAR),"",alltrim(str(year(this.oParentObject.w_RFDATCAR)))),; 
 iif(empty(this.oParentObject.w_RFDATFAL),"",right("0"+alltrim(str(day(this.oParentObject.w_RFDATFAL))),2)),iif(empty(this.oParentObject.w_RFDATFAL),"",; 
 right("0"+alltrim(str(month(this.oParentObject.w_RFDATFAL))),2)), ; 
 iif(empty(this.oParentObject.w_RFDATFAL),"",alltrim(str(year(this.oParentObject.w_RFDATFAL)))),this.oParentObject.w_RECODCOM,this.oParentObject.w_SECODCOM,; 
 this.oParentObject.w_SERCODCOM,iif(this.oParentObject.w_SELCAF="1",this.oParentObject.w_CFDELCAF,""),iif(this.oParentObject.w_SELCAF="2",this.oParentObject.w_CFRESCAF,""),; 
 iif(this.oParentObject.w_SELCAF="1",this.oParentObject.w_CFRESCAF,""), ; 
 this.oParentObject.w_FOCODSOS,iif(this.oParentObject.w_INVAVTEL="1","X",""),iif(this.oParentObject.w_RICAVTEL="1","X","")," ",; 
 iif(this.oParentObject.w_qSS1="1","X",""),iif(this.oParentObject.w_qSS12="1","X",""),iif(this.oParentObject.w_qSS14="1","X",""), ; 
 iif(this.oParentObject.w_FLAGCIPAR="1","X",""),iif(this.oParentObject.w_qSV1="1","X",""),iif(this.oParentObject.w_qSV14="1","X",""),this.oParentObject.w_RFFISSED,iif(this.oParentObject.w_qSY1="1","X",""),iif(this.oParentObject.w_qSY12="1","X",""),; 
 iif(this.oParentObject.w_qSY14="1","X",""),; 
 iif(this.oParentObject.w_SOGG1="0"," ",this.oParentObject.w_SOGG1),iif(this.oParentObject.w_SOGG2="0"," ",this.oParentObject.w_SOGG2),iif(this.oParentObject.w_SOGG3="0"," ",this.oParentObject.w_SOGG3),; 
 iif(this.oParentObject.w_SOGG4="0"," ",this.oParentObject.w_SOGG4),iif(this.oParentObject.w_SOGG5="0"," ",this.oParentObject.w_SOGG5),; 
 right("00"+alltrim(this.oParentObject.w_CODSITPAR),2),this.oParentObject.w_CODFIS_1,this.oParentObject.w_CODFIS_2,this.oParentObject.w_CODFIS_3,this.oParentObject.w_CODFIS_4,this.oParentObject.w_CODFIS_5,; 
 iif(this.oParentObject.w_NOTRASMIS="0"," ",this.oParentObject.w_NOTRASMIS),iif(this.oParentObject.w_ATTESTA="1","X","") )
    * --- Resta da definire il codice soggetto e impego a presentare in via telematica la dichiarazione
    result1=cp_ffdf(" ","770Frontespizio14.pdf",this," ",@ARRFDF)
    CP_CHPRN(tempadhoc()+"\770Frontespizio141.FDF")
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Chiedo se si vuole stampare anche i Quadri St,  H  e J del modello 770
    do GSRI_KSQ with this
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Lancio i batches standard che creano i cursori
    if this.w_STAMPAST = "S"
      * --- Stampo in formato PDF i prospetti del quadro ST
      do GSRI_BST with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if used("RITE3") 
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    if this.w_STAMPAH = "S"
      * --- Stampo in formato PDF i prospetti del quadro H
      do GSRI_BSC with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if used("RITE1") 
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    if this.w_STAMPAJ = "S"
      * --- Stampo in formato PDF i prospetti del quadro H
      do GSRI_BSC with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if used("RITE1") 
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    * --- Cancello i cursori
    if used("RITE1")
      select RITE1
      use
    endif
    if used("RITE3")
      select RITE3
      use
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Ritardo l'operazione successiva
    if type("g_PDFDELAY")="N"
      wait "" timeout g_PDFDELAY
    else
      wait "" timeout 5
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampa prospetto ST in PDF
    DECLARE ARRFDF (2,2)
    ARRFDF (1,1) = "CODFIS"
    ARRFDF (1,2) = 16
    ARRFDF (2,1) = "NUMMOD"
    ARRFDF (2,2) = 2
    this.w_DATIPRES = .F.
    * --- Creo il cursore dei dati
    L_CAMPI="CREATE CURSOR __TMP__ (CODFIS C(16),CODFISOS C(16), NUMMOD C(2), EVEECC C(1),"
    this.w_QUADRO = 0
    do while this.w_QUADRO < 13
      this.w_QUADRO = this.w_QUADRO+1
      L_CAMPI=L_CAMPI+"DI"+ALLTRIM(STR(this.w_QUADRO))+"STM01 C(2), DI"+ALLTRIM(STR(this.w_QUADRO))+"STA01 C(4), "
      L_CAMPI=L_CAMPI+" DI"+ALLTRIM(STR(this.w_QUADRO))+"ST002 C(30), "
      L_CAMPI=L_CAMPI+"DI"+ALLTRIM(STR(this.w_QUADRO))+"ST004 C(30), DI"+ALLTRIM(STR(this.w_QUADRO))+"ST006 C(30), "
      L_CAMPI=L_CAMPI+"DI"+ALLTRIM(STR(this.w_QUADRO))+"ST007 C(30), DI"+ALLTRIM(STR(this.w_QUADRO))+"ST008 C(30), "
      L_CAMPI=L_CAMPI+"DI"+ALLTRIM(STR(this.w_QUADRO))+"ST009 C(1), DI"+ALLTRIM(STR(this.w_QUADRO))+"ST010 C(254), "
      L_CAMPI=L_CAMPI+"DI"+ALLTRIM(STR(this.w_QUADRO))+"ST011 C(16), DI"+ALLTRIM(STR(this.w_QUADRO))+"ST012 C(16), "
      L_CAMPI=L_CAMPI+"DI"+ALLTRIM(STR(this.w_QUADRO))+"STG14 C(2), "
      L_CAMPI=L_CAMPI+"DI"+ALLTRIM(STR(this.w_QUADRO))+"STM14 C(2), DI"+ALLTRIM(STR(this.w_QUADRO))+"STA14 C(4), "
    enddo
    L_CAMPI=substr(L_CAMPI,1,len(L_CAMPI)-2)+")"
    &L_CAMPI
    select RITE3
    this.w_QUADRO = 0
    this.w_TRASMATT = 1
    L_CAMPI="INSERT INTO __TMP__ (CODFIS, CODFISOS, NUMMOD, EVEECC, "
    L_VALORI=' VALUES ("'+this.oParentObject.w_FOCODFIS+'","'+this.oParentObject.w_FOCODSOS+'", "01","'+iif(! empty(this.oParentObject.w_FOCODSOS),this.oParentObject.w_CODEVECC," ")+'","'
    do while !eof()
      select RITE3
      this.w_REGAGE = NVL(VPREGAGE,"")
      * --- Se c'� il flag regime agevolato il quadro ST non deve essere stampato
      if this.w_REGAGE<>"S"
        this.w_QUADRO = this.w_QUADRO+1
        L_CAMPI=L_CAMPI+"DI"+ALLTRIM(STR(this.w_QUADRO))+"STM01, DI"+ALLTRIM(STR(this.w_QUADRO))+"STA01, "
        L_CAMPI= L_CAMPI+ "DI"+ALLTRIM(STR(this.w_QUADRO))+"ST002, "
        L_CAMPI=L_CAMPI+"DI"+ALLTRIM(STR(this.w_QUADRO))+"ST004, DI"+ALLTRIM(STR(this.w_QUADRO))+"ST006, "
        L_CAMPI=L_CAMPI+"DI"+ALLTRIM(STR(this.w_QUADRO))+"ST007,  DI"+ALLTRIM(STR(this.w_QUADRO))+"ST008, "
        L_CAMPI=L_CAMPI+"DI"+ALLTRIM(STR(this.w_QUADRO))+"ST009, DI"+ALLTRIM(STR(this.w_QUADRO))+"ST010, "
        L_CAMPI=L_CAMPI+"DI"+ALLTRIM(STR(this.w_QUADRO))+"ST011, DI"+ALLTRIM(STR(this.w_QUADRO))+"ST012, "
        L_CAMPI=L_CAMPI+ "DI"+ALLTRIM(STR(this.w_QUADRO))+"STG14, "
        L_CAMPI=L_CAMPI+"DI"+ALLTRIM(STR(this.w_QUADRO))+"STM14, DI"+ALLTRIM(STR(this.w_QUADRO))+"STA14, "
        if this.w_quadro=1
          L_VALORI=L_VALORI+right("00"+alltrim(str(MESE)),2)+'",'
          L_VALORI=L_VALORI+'"'+str(ANNO,4,0)+'"'
        else
          L_VALORI=L_VALORI+'"'+right("00"+alltrim(str(MESE)),2)+'",'
          L_VALORI=L_VALORI+'"'+str(ANNO,4,0)+'"'
        endif
        this.w_CALCOLI = SOMTOTA-VPRITECC-VPRITCOM
        if VPCAUPRE # "X" and VPCAUPRE # "Y" and (SOMTOTA < 0 or VPRITECC < 0 or VPRITCOM < 0 or this.w_CALCOLI < 0 or VPIMPINT < 0)
          this.w_MESS = "Attenzione: presenza di importi negativi nel prospetto st."+CHR(13)+"Impossibile generare il file"
          ah_ErrorMsg(this.w_MESS,"stop","")
          if used("__TMP__")
            Select __TMP__ 
 Use
          endif
          i_retcode = 'stop'
          return
        endif
        L_VALORI=L_VALORI+", "+'"'+IIF(SOMTOTA<=0," ",ALLTRIM(STR(SOMTOTA,18,4)))+'"'+", "+'"'+IIF(VPRITECC<=0 or "K"$VP__NOTE," ",ALLTRIM(STR(VPRITECC,18,4)))+'"'+", "+'"'+IIF(VPRITCOM<=0 or "K"$VP__NOTE," ",ALLTRIM(STR(VPRITCOM,18,4)))+'"'
        L_VALORI = L_VALORI +"," +'"'+IIF(VPIMPVER<=0," ",ALLTRIM(STR(VPIMPVER,18,4)))+'"'+","+'"'+IIF(VPIMPINT<=0," ",ALLTRIM(STR(VPIMPINT,18,4)))+'"'
        if VPRAVOPE = "S"
          L_VALORI=L_VALORI+", "+'"'+"X"+'"'
        else
          L_VALORI=L_VALORI+", "+'" "'
        endif
        L_VALORI = L_VALORI+","+'"'+ALLTRIM(VP__NOTE)+'","'+ALLTRIM(MRCODTRI)+'"'
        * --- Tesoreria
        if VPTIPVER = "T"
          L_VALORI=L_VALORI+", "+'"'+"X"+'"'
        else
          L_VALORI=L_VALORI+", "+'" "'
        endif
        * --- Campo 14
        if ! empty(nvl(VPDATVER,""))
          L_VALORI=L_VALORI+", "+'"'+right("00"+alltrim(str(day(VPDATVER))),2)+'"'
          L_VALORI=L_VALORI+", "+'"'+right("00"+alltrim(str(month(VPDATVER))),2)+'"'
          L_VALORI=L_VALORI+", "+'"'+str(year(VPDATVER),4,0)+'", '
        else
          L_VALORI=L_VALORI+", "+'"  "'
          L_VALORI=L_VALORI+", "+'"  "'
          L_VALORI=L_VALORI+", "+'"  "'+", "
        endif
        * --- Incremento il numero di Rigo
        if this.w_QUADRO >=12
          * --- Incremento il numero di Modello
          L_MACRO=substr(L_CAMPI,1,len(L_CAMPI)-2)+")"+substr(L_VALORI,1,len(L_VALORI)-2)+")"
          &L_MACRO
          this.w_QUADRO = 0
          this.w_TRASMATT = this.w_TRASMATT+1
          L_CAMPI="INSERT INTO __TMP__ (CODFIS, CODFISOS, NUMMOD, EVEECC, "
          L_VALORI=' VALUES ("'+this.oParentObject.w_FOCODFIS+'", "'+this.oParentObject.w_FOCODSOS+'", "'+RIGHT("00"+ALLTRIM(STR(this.w_TRASMATT,2,0)),2)+'","'+iif(! empty(this.oParentObject.w_FOCODSOS),this.oParentObject.w_CODEVECC," ")+'","'
          this.w_DATIPRES = .T.
        endif
        skip in RITE3
      else
        skip in RITE3
      endif
    enddo
    if this.w_QUADRO>0
      L_MACRO=substr(L_CAMPI,1,len(L_CAMPI)-2)+")"+substr(L_VALORI,1,len(L_VALORI)-2)+")"
      &L_MACRO
      this.w_DATIPRES = .T.
    endif
    this.w_RESFDF = cp_ffdf(" ","Quadro_ST_14.pdf",this," ",@ARRFDF)
    * --- Se non mi ritorna un numero significa che ho incontrato un errore
    do case
      case Not Left( this.w_RESFDF , 1) $ "0123456789"
        * --- Errore durante creazione di un FDF..
        this.w_MESS = this.w_RESFDF
        ah_ErrorMsg(this.w_MESS,"stop","")
      case Left( this.w_RESFDF , 1) = "0"
        * --- Nessun FDF creato...
        this.w_MESS = "Nessun file PDF da generare."
        ah_ErrorMsg(this.w_MESS,"stop","")
      otherwise
        this.w_QUADRO = 1
        do while this.w_QUADRO<=Val( this.w_RESFDF )
          CP_CHPRN(tempadhoc()+"\Quadro_ST_14"+ALLTRIM(STR(this.w_QUADRO))+".FDF",.NULL.,2)
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_QUADRO = this.w_QUADRO+1
        enddo
    endcase
    if used("__TMP__")
      Select __TMP__ 
 Use
    endif
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampa  in formato PDF del quadro H
    this.w_ERROR = .F.
    this.w_OLDPERCF = "*"
    this.w_OLDPER = ""
    this.Pag6()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Se i controlli non restituiscono nessun errore proseguo
    *     con l'elaborazione
    if ! this.w_BLOCK
      DECLARE ARRFDF (2,2)
      ARRFDF (1,1) = "CODFIS"
      ARRFDF (1,2) = 16
      ARRFDF (2,1) = "NUMMOD"
      ARRFDF (2,2) = 8
      this.w_TRASMATT = 1
      * --- Creo il cursore dei dati
      L_CAMPI="CREATE CURSOR __TMP__ (CODFIS C(16), CODFISOS C(16), NUMMOD C(8), AU001 C(16), AU002 C(50), AU003 C(20), "
      L_CAMPI = L_CAMPI + "AU004 C(1), AU005G C(2), AU005M C(2), AU005A C(4), AU006 C(50), AU007 C(2), AU008 C(50), AU009 C(2),  AU010 C(2), "
      L_CAMPI = L_CAMPI + "AU011 C(4),AU012 C(100), AU013 C(1), AU014 C(1), AU015 C(100), AU016 C(30), AU017 C(35), AU018 C(4), AU019 C(2),AU020 C(4), "
      L_CAMPI = L_CAMPI +"AU022 C(30), AU023 C(30), AU024 C(1),AU025 C(30), AU026 C(30),AU027 C(30),AU038 C(30), AU039 C(30), "
      L_CAMPI = L_CAMPI + "AU040 C(30), AU041 C(30),AU042 C(30), AU043 C(30))"
      &L_CAMPI
      Select Rite1 
 Go Top
      this.w_OLDPERCF = NVL(ANCODFIS,"")
      this.w_OLDPER = NVL(MRCODCON,"")
      do while !eof()
        * --- Record H
        L_CAMPI="INSERT INTO  __TMP__ (CODFIS , CODFISOS, NUMMOD , AU001 , AU002 , AU003 , "
        L_CAMPI = L_CAMPI + "AU004 , AU005G , AU005M , AU005A , AU006 , AU007 , AU008 , AU009 , AU010, "
        L_CAMPI = L_CAMPI + "AU011 , AU012 , AU013 , AU014 , AU015 , AU016, AU017 ,AU018, AU019 ,AU020, "
        L_CAMPI = L_CAMPI +"AU022 , AU023 , AU024 , AU025 , AU026, AU027,AU038, AU039, AU040,AU041,AU042,AU043)"
        select RITE1
        this.w_PERCESTE = NVL(ANFLGEST,"N")
        this.w_CFPEREST = NVL(ANCOFISC," ")
        this.w_CAUPRE = UPPER(NVL(TRCAUPRE," "))
        this.w_MRCODCON = NVL(MRCODCON,"")
        this.w_CODSNS = NVL(MRCODSNS," ")
        if this.w_OLDPERCF<>NVL(ANCODFIS,"") OR this.w_OLDPER<>MRCODCON
          this.w_TRASMATT = this.w_TRASMATT + 1
          this.w_OLDPERCF = NVL(ANCODFIS,"")
          this.w_OLDPER = NVL(MRCODCON,"")
        endif
        L_VALORI=' VALUES ("'+this.oParentObject.w_FOCODFIS+'", "'+this.oParentObject.w_FOCODSOS+'", "'+RIGHT(REPL("0",8)+ALLTRIM(STR(this.w_TRASMATT)),8)+'",'
        if this.w_PERCESTE="S"
          L_VALORI = L_VALORI +'"                ",'
        else
          L_VALORI = L_VALORI + '"'+NVL(ANCODFIS,"")+'",'
        endif
        * --- Posizione 30 record posizionale
        this.w_PERCIPCF = ANCODFIS
        L_VALORI = L_VALORI +' "'+iif(ANPERFIS="S",upper(NVL(ANCOGNOM,"")),upper(NVL(ANDESCRI,"")))+'",'
        L_VALORI = L_VALORI + '"'+iif(ANPERFIS="S",upper(NVL(AN__NOME,"")),"")+'",'
        L_VALORI = L_VALORI + '"'+iif(ANPERFIS="S",upper(NVL(AN_SESSO,"")),"")+'",'
        L_VALORI = L_VALORI + '"'+iif(ANPERFIS="S",ALLTRIM(NVL(STR(DAY(ANDATNAS))," ")),"  ")+'",'
        L_VALORI = L_VALORI + '"'+iif(ANPERFIS="S",ALLTRIM(NVL(STR(MONTH(ANDATNAS))," ")),"  ")+'",'
        L_VALORI = L_VALORI + '"'+iif(ANPERFIS="S",ALLTRIM(NVL(STR(YEAR(ANDATNAS))," ")),"  ")+'",'
        L_VALORI = L_VALORI + '"'+iif(ANPERFIS="S",upper(NVL(ANLOCNAS,"")),"")+'",'
        L_VALORI = L_VALORI + '"'+iif(ANPERFIS="S",upper(NVL(ANPRONAS,"")),"")+'",'
        * --- Riservato ai percipienti esteri
        if this.w_PERCESTE = "S"
          * --- Inserisco valori vuoti per AU008,AU009,AU010 , AU011 e AU012
          L_VALORI = L_VALORI + '"        ",'
          L_VALORI = L_VALORI + '"        ",'
          L_VALORI = L_VALORI + '"        ",'
          L_VALORI = L_VALORI + '"        ",'
          L_VALORI = L_VALORI + '"        ",'
          * --- Campi 13
          if NVL(AN_EREDE,"N")="S"
            L_VALORI=L_VALORI+'"'+"X"+'"'+","
          else
            L_VALORI=L_VALORI+'" "'+","
          endif
          L_VALORI = L_VALORI + '"'+iif(NVL(ANEVEECC,"0")#"0", ANEVEECC," ")+'",'
          * --- Gestisco i campi riservati al percipiente estero
          * --- Codice Fiscale estero
          if Empty(this.w_CFPEREST)
            L_VALORI = L_VALORI + '"'+UPPER(NVL(ANCODFIS,""))+'",'
          else
            L_VALORI = L_VALORI + '"'+upper(this.w_CFPEREST)+'",'
          endif
          * --- Localit� di residenza estera
          L_VALORI = L_VALORI + '"'+UPPER(NVL(ANLOCALI,""))+'",'
          * --- Via e numero civico
          L_VALORI = L_VALORI + '"'+UPPER(NVL(ANINDIRI,""))+'",'
          * --- Codice stato estero
          L_VALORI = L_VALORI + '"'+RIGHT(repl("0",3)+UPPER(ALLTRIM(NVL(NACODEST,""))),3)+'",'
        else
          L_VALORI = L_VALORI + '"'+upper(NVL(ANLOCALI,""))+'",'
          L_VALORI = L_VALORI + '"'+upper(ANPROVIN)+'",'
          * --- Campo AU010
          L_VALORI = L_VALORI + '"'+upper(iif(nvl(TIPOPREST," ")="S",NVL(PERCODREG,"")," "))+'",'
          * --- Campo AU011
          L_VALORI = L_VALORI + '"'+upper(iif(nvl(TIPOPREST," ")="S",NVL(ANCODCOM,"")," "))+'",'
          * --- Campo AU012
          L_VALORI = L_VALORI + '"'+upper(NVL(ANINDIRI,""))+'",'
          * --- Campo AU013
          if NVL(AN_EREDE,"N")="S"
            L_VALORI=L_VALORI+'"'+"X"+'"'+","
          else
            L_VALORI=L_VALORI+'" "'+","
          endif
          * --- Campo AU014
          L_VALORI = L_VALORI + '"'+iif(NVL(ANEVEECC,"0")#"0", ANEVEECC," ")+'",'
          * --- Inserisco valori vuoti da AU015  a  AU018
          L_VALORI = L_VALORI + '"        ",'
          L_VALORI = L_VALORI + '"        ",'
          L_VALORI = L_VALORI + '"        ",'
          L_VALORI = L_VALORI + '"        ",'
        endif
        * --- Dati relativi alle somme erogate
        L_VALORI = L_VALORI + '"'+upper(NVL(TRCAUPRE,""))+'",'
        L_VALORI = L_VALORI + '"'+IIF (upper(NVL(TRCAUPRE,"")) $ "GHI" , alltrim(str(year(this.w_DATFIN))) ,"")+'",'
        L_VALORI = L_VALORI +'"'+IIF(NVL(SOMTOTA,0)<=0," ",ALLTRIM(STR(SOMTOTA,18,4)))+'"'+","
        if this.w_PERCESTE = "S" and ANFLSGRE="S"
          L_VALORI = L_VALORI +'"'+IIF(NVL(NONSOGG,0)<=0," ",ALLTRIM(STR(NONSOGG,18,4)))+'"'+","
          L_VALORI = L_VALORI + '"'+NVL(MRCODSNS,"")+'",'
          * --- Inserisco il valore 0 per il campo AU025
          L_VALORI=L_VALORI+'" "'+","
        else
          * --- Inserisco il valore 0 per il campo AU023
          L_VALORI=L_VALORI+'" "'+","
          L_VALORI = L_VALORI + '"'+NVL(MRCODSNS,"")+'",'
          L_VALORI = L_VALORI +'"'+IIF(NVL(NONSOGG,0)<=0," ",ALLTRIM(STR(NONSOGG,18,4)))+'"'+","
        endif
        L_VALORI = L_VALORI +'"'+IIF(NVL(IMPONI,0)<=0," ",ALLTRIM(STR(IMPONI,18,4)))+'"'+","
        L_VALORI = L_VALORI +'"'+IIF(NVL(MRTOTIM1,0)<=0," ",ALLTRIM(STR(MRTOTIM1,18,4)))+'"'+","
        if this.w_CAUPRE $ "M-V-C"
          L_VALORI = L_VALORI +'"'+IIF(NVL(MRTOTIM2,0) -NVL(MRIMPPER,0)<=0," ",ALLTRIM(STR(NVL(MRTOTIM2,0) -NVL(MRIMPPER,0),18,4)))+'"'+","
          L_VALORI = L_VALORI +'"'+IIF(NVL(MRIMPPER,0)<=0," ",ALLTRIM(STR(MRIMPPER,18,4)))+'"'+","
        else
          L_VALORI=L_VALORI+'" "'+","
          L_VALORI=L_VALORI+'" "'+","
        endif
        Select RITE1
        * --- Campi AU040 e AU041
        L_VALORI = L_VALORI +'"'+ IIF(NVL(MRSPERIM,0)<=0," ",ALLTRIM(STR(MRSPERIM,18,4)))+'"'+","
        if RITERIMB < 0 and TRCAUPRE $ "X-Y"
          L_VALORI = L_VALORI +'"'+ALLTRIM(STR(ABS(RITERIMB),18,4))+'"'+","
        else
          L_VALORI=L_VALORI+'" "'+","
        endif
        * --- Campi AU042 e AU043
        L_VALORI = L_VALORI +'"'+IIF(NVL(IMPREFALL,0)<=0," ",ALLTRIM(STR(IMPREFALL,18,4)))+'"'+","
        L_VALORI = L_VALORI +'"'+IIF(NVL(IMPOSFALL,0)<=0," ",ALLTRIM(STR(IMPOSFALL,18,4)))+'"'+")"
        * --- Inserisco il record
        L_MACRO = L_CAMPI+L_VALORI
        &L_MACRO
        Select RITE1
        skip in RITE1
      enddo
      * --- Stampa quadro H attuale
      Select RITE1
      this.w_RESFDF = cp_ffdf(" ","Quadro_H_14.pdf",this," ",@ARRFDF)
      * --- Se non mi ritorna un numero significa che ho incontrato un errore
      do case
        case Not Left( this.w_RESFDF , 1) $ "0123456789"
          * --- Errore durante creazione di un FDF..
          this.w_MESS = this.w_RESFDF
          ah_ErrorMsg(this.w_MESS,"stop","")
        case Left( this.w_RESFDF , 1) = "0"
          * --- Nessun FDF creato...
          this.w_MESS = "Nessun file PDF da generare."
          ah_ErrorMsg(this.w_MESS,"stop","")
        otherwise
          this.w_QUADRO = 1
          do while this.w_QUADRO<=Val( this.w_RESFDF )
            CP_CHPRN(tempadhoc()+"\Quadro_H_14"+ALLTRIM(STR(this.w_QUADRO))+".FDF",.NULL.,2)
            this.Pag2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_QUADRO = this.w_QUADRO+1
          enddo
      endcase
    endif
  endproc


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    Create cursor __TMP__ (CODFIS C(16), CODFISOS C(16), NUMMOD C(8),TOTRITAC C(30),CODFISCALE C(16))
    DECLARE ARRFDF (2,2)
    ARRFDF (1,1) = "CODFIS"
    ARRFDF (1,2) = 16
    ARRFDF (2,1) = "NUMMOD"
    ARRFDF (2,2) = 8
    select RITE1
    * --- Calcolo la somma delle ritenute a titolo di acconto
    SUM(MRTOTIM1) TO this.w_TOTRITACC
    Insert into __TMP__ values (this.oParentObject.w_FOCODFIS, this.oParentObject.w_FOCODSOS, "01", ALLTRIM(STR(this.w_TOTRITACC,18,4)),this.oParentObject.w_FOCODFIS)
    result = cp_ffdf(" ","Quadro_SS_14.pdf",this," ",@ARRFDF)
    CP_CHPRN(tempadhoc()+"\Quadro_SS_141.FDF",.NULL.,2)
    if used("__TMP__")
      select __TMP__ 
 use
    endif
  endproc


  procedure Pag6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Creo cursore messaggistica di errore (Mancanza campi obbligatori)
    create cursor LISTAPERC (CODCON C(15),DENOMI C(40),FLGEST C(1),CODFIS C(1),COGDEN C(1),NOME C(1),; 
 SESSO C(1),DATNAS C(1),COMNAS C(1),COMUN C(1),CODREG C(1),CODCOM C(1),EVENECC C(1))
    Select MAX(NVL(ANCODFIS," ")) as ANCODFIS,MAX(NVL(ANFLGEST," ")) AS ANFLGEST,MAX(ANPERFIS) AS ANPERFIS,; 
 MAX(NVL(ANCOGNOM," ")) AS ANCOGNOM,MAX(NVL(ANDESCRI," ")) AS ANDESCRI,; 
 MAX(NVL(ANLOCALI," ")) AS ANLOCALI,MAX(NVL(AN__NOME," ")) AS AN__NOME,; 
 MAX(NVL(AN_SESSO," ")) AS AN_SESSO,MAX(CP_TODATE(ANDATNAS)) AS ANDATNAS,; 
 MAX(NVL(ANLOCNAS," ")) AS ANLOCNAS,MAX(NVL(ANCOFISC," ")) AS ANCOFISC,; 
 MAX(NVL(ANEVEECC,"0")) AS ANEVEECC,MRCODCON,; 
 MAX(NVL(PERCODREG," ")) AS PERCODREG,NVL(TRCAUPRE," ") AS TRCAUPRE,MAX(ANCODCOM) AS ANCODCOM; 
 FROM RITE1 INTO CURSOR CONTROLLO GROUP BY MRCODCON,TRCAUPRE ORDER BY MRCODCON,TRCAUPRE
    this.w_BLOCK = .f.
    this.w_ATTENTION = .f.
    Select Controllo 
 Go Top 
 Scan
    this.w_MRCODCON = NVL(MRCODCON,"")
    this.w_ANDESCRI = NVL(ANDESCRI,"")
    this.w_PERCESTE = ANFLGEST
    this.w_CFPEREST = ANCOFISC
    if EMPTY(ANCODFIS) and this.w_PERCESTE<>"S"
      this.w_BLOCK = .t.
      INSERT INTO LISTAPERC VALUES (this.w_MRCODCON,this.w_ANDESCRI,this.w_PERCESTE,"1","0","0","0","0","0","0","0","0","0")
      select Controllo
    endif
    if iif(ANPERFIS="S",EMPTY(ANCOGNOM),EMPTY(ANDESCRI) )
      this.w_BLOCK = .t.
      INSERT INTO LISTAPERC VALUES (this.w_MRCODCON,this.w_ANDESCRI,this.w_PERCESTE,"0","1","0","0","0","0","0","0","0","0")
      select Controllo
    endif
    if EMPTY(ANLOCALI) and this.w_PERCESTE<>"S"
      this.w_BLOCK = .t.
      INSERT INTO LISTAPERC VALUES (this.w_MRCODCON,this.w_ANDESCRI,this.w_PERCESTE,"0","0","0","0","0","0","1","0","0","0")
      select Controllo
    endif
    if EMPTY(AN__NOME) and ANPERFIS="S"
      this.w_BLOCK = .t.
      INSERT INTO LISTAPERC VALUES (this.w_MRCODCON,this.w_ANDESCRI,this.w_PERCESTE,"0","0","1","0","0","0","0","0","0","0")
      select Controllo
    endif
    if EMPTY(AN_SESSO) and ANPERFIS="S"
      this.w_BLOCK = .t.
      INSERT INTO LISTAPERC VALUES (this.w_MRCODCON,this.w_ANDESCRI,this.w_PERCESTE,"0","0","0","1","0","0","0","0","0","0")
      select Controllo
    endif
    if EMPTY(ANDATNAS) and ANPERFIS="S"
      this.w_BLOCK = .t.
      INSERT INTO LISTAPERC VALUES (this.w_MRCODCON,this.w_ANDESCRI,this.w_PERCESTE,"0","0","0","0","1","0","0","0","0","0")
      select Controllo
    endif
    if EMPTY(ANLOCNAS) and ANPERFIS="S"
      this.w_BLOCK = .t.
      INSERT INTO LISTAPERC VALUES (this.w_MRCODCON,this.w_ANDESCRI,this.w_PERCESTE,"0","0","0","0","0","1","0","0","0","0")
      select Controllo
    endif
    if this.w_PERCESTE = "S" and empty(this.w_CFPEREST)
      this.w_ATTENTION = .t.
      INSERT INTO LISTAPERC VALUES (this.w_MRCODCON,this.w_ANDESCRI,this.w_PERCESTE,"1","0","0","0","0","0","0","0","0","0")
      select Controllo
    endif
    if EMPTY(PERCODREG) and TRCAUPRE= "N" and this.w_PERCESTE<>"S"
      this.w_ATTENTION = .t.
      INSERT INTO LISTAPERC VALUES (this.w_MRCODCON,this.w_ANDESCRI,this.w_PERCESTE,"0","0","0","0","0","0","0","1","0","0")
      select Controllo
    endif
    if EMPTY(ANCODCOM) and TRCAUPRE= "N" and this.w_PERCESTE<>"S"
      this.w_ATTENTION = .t.
      INSERT INTO LISTAPERC VALUES (this.w_MRCODCON,this.w_ANDESCRI,this.w_PERCESTE,"0","0","0","0","0","0","0","0","1","0")
      select Controllo
    endif
    if ANEVEECC<>"0" AND ANEVEECC<>"1" AND ANEVEECC<>"2" AND ANEVEECC<>"3" AND ANEVEECC<>"4" 
      this.w_BLOCK = .t.
      INSERT INTO LISTAPERC VALUES (this.w_MRCODCON,this.w_ANDESCRI,this.w_PERCESTE,"0","0","0","0","0","0","0","0","0","1")
      select Controllo
    endif
    Endscan
    if this.w_BLOCK OR this.w_ATTENTION
      if this.w_BLOCK
        this.w_MESS = "Dati anagrafici percipienti mancanti"+chr(13)+"Impossibile stampare i quadri H."+chr(13)+"Vuoi visualizzare la stampa di verifica?"
      else
        this.w_MESS = "Dati anagrafici percipienti mancanti"+chr(13)+"Vuoi visualizzare la stampa di verifica?"
      endif
      if ah_YesNo(this.w_MESS)
        Select CODCON,MAX(DENOMI) AS DENOMI,MAX(FLGEST) AS FLGEST,MAX(CODFIS) as CODFIS,; 
 MAX(COGDEN) as COGDEN,MAX(NOME) as NOME,MAX(SESSO) as SESSO, MAX(DATNAS) as DATNAS,; 
 MAX(COMNAS) as COMNAS,MAX(COMUN) as COMUN, MAX(CODREG) as CODREG,; 
 MAX(CODCOM) AS CODCOM,MAX(EVENECC) AS EVENECC; 
 from LISTAPERC into cursor __TMP__ group by CODCON order by CODCON
        CP_CHPRN("..\RITE\EXE\QUERY\GSRI3BFT.FRX")
      endif
    endif
    if used("__TMP__")
      select __TMP__ 
 use
    endif
    if used("LISTAPERC")
      select LISTAPERC 
 use
    endif
    if used("CONTROLLO")
      select CONTROLLO 
 use
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='DES_DIVE'
    this.cWorkTables[4]='NAZIONI'
    return(this.OpenAllTables(4))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
