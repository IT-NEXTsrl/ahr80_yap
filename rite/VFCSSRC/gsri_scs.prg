* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_scs                                                        *
*              Elenco certificazioni                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2015-02-05                                                      *
* Last revis.: 2015-03-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsri_scs",oParentObject))

* --- Class definition
define class tgsri_scs as StdForm
  Top    = 9
  Left   = 10

  * --- Standard Properties
  Width  = 654
  Height = 517
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-03-09"
  HelpContextID=90830441
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=39

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsri_scs"
  cComment = "Elenco certificazioni"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_TIPOZOOM = space(1)
  w_SERIALE_CERT = space(10)
  w_CDSERIAL = space(10)
  w_NUMERORIGA = 0
  w_XCHK = 0
  w_CUTIPCAS = space(1)
  w_CODFIS = space(16)
  w_COGNOM = space(60)
  w_NOME = space(50)
  w_DATNAS = ctod('  /  /  ')
  w_COMNAS = space(30)
  w_PRONAS = space(2)
  w_SESSO = space(1)
  w_CATPAR = space(2)
  w_EVEECC = space(1)
  w_COMFIS = space(30)
  w_PROFIS = space(2)
  w_COMUNE = space(4)
  w_CAUPRE = space(2)
  w_AMMCOR = 0
  w_SNSOGG = 0
  w_CODICE = space(1)
  w_IMPONI = 0
  w_RITACC = 0
  w_SOGERO = 0
  w_PERCIP = 0
  w_SPERIM = 0
  w_RITRIM = 0
  w_CUDATCER = ctod('  /  /  ')
  w_CODICEFIS = space(16)
  w_TIPO_COMU = space(1)
  w_SNSOGG1 = 0
  w_PROMOD = 0
  w_PROMOD1 = 0
  w_PROMOD2 = 0
  w_SNSOGG2 = 0
  w_CODICE1 = space(1)
  w_CODICE2 = space(1)
  w_FPERCI = space(15)
  w_AnuPerc = .NULL.
  w_ElePerc = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsri_scsPag1","gsri_scs",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_AnuPerc = this.oPgFrm.Pages(1).oPag.AnuPerc
    this.w_ElePerc = this.oPgFrm.Pages(1).oPag.ElePerc
    DoDefault()
    proc Destroy()
      this.w_AnuPerc = .NULL.
      this.w_ElePerc = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIPOZOOM=space(1)
      .w_SERIALE_CERT=space(10)
      .w_CDSERIAL=space(10)
      .w_NUMERORIGA=0
      .w_XCHK=0
      .w_CUTIPCAS=space(1)
      .w_CODFIS=space(16)
      .w_COGNOM=space(60)
      .w_NOME=space(50)
      .w_DATNAS=ctod("  /  /  ")
      .w_COMNAS=space(30)
      .w_PRONAS=space(2)
      .w_SESSO=space(1)
      .w_CATPAR=space(2)
      .w_EVEECC=space(1)
      .w_COMFIS=space(30)
      .w_PROFIS=space(2)
      .w_COMUNE=space(4)
      .w_CAUPRE=space(2)
      .w_AMMCOR=0
      .w_SNSOGG=0
      .w_CODICE=space(1)
      .w_IMPONI=0
      .w_RITACC=0
      .w_SOGERO=0
      .w_PERCIP=0
      .w_SPERIM=0
      .w_RITRIM=0
      .w_CUDATCER=ctod("  /  /  ")
      .w_CODICEFIS=space(16)
      .w_TIPO_COMU=space(1)
      .w_SNSOGG1=0
      .w_PROMOD=0
      .w_PROMOD1=0
      .w_PROMOD2=0
      .w_SNSOGG2=0
      .w_CODICE1=space(1)
      .w_CODICE2=space(1)
      .w_FPERCI=space(15)
      .w_SERIALE_CERT=oParentObject.w_SERIALE_CERT
      .w_CUDATCER=oParentObject.w_CUDATCER
      .w_CODICEFIS=oParentObject.w_CODICEFIS
      .w_TIPO_COMU=oParentObject.w_TIPO_COMU
        .w_TIPOZOOM = 'E'
      .oPgFrm.Page1.oPag.AnuPerc.Calculate(.w_Seriale_Cert)
          .DoRTCalc(2,2,.f.)
        .w_CDSERIAL = iif(.w_Cutipcas='A',Alltrim(Nvl(.w_AnuPerc.getVar('Cdserial'),' ')),Alltrim(Nvl(.w_ElePerc.getVar('Cdserial'),' ')))
        .w_NUMERORIGA = iif(.w_Cutipcas='A',Nvl(.w_AnuPerc.getVar('Cprownum'),0),Nvl(.w_ElePerc.getVar('Cprownum'),0))
        .w_XCHK = iif(.w_Cutipcas='A',Nvl(.w_AnuPerc.getVar('XCHK'),0),Nvl(.w_ElePerc.getVar('XCHK'),0))
        .w_CUTIPCAS = This.oParentObject.oParentObject.w_Cutipcas
        .w_CODFIS = Alltrim(Nvl(.w_ElePerc.getVar('Cdcodfis'),' '))
        .w_COGNOM = Alltrim(Nvl(.w_ElePerc.getVar('Cdcognom'),' '))
        .w_NOME = Alltrim(Nvl(.w_ElePerc.getVar('Cd__Nome'),' '))
        .w_DATNAS = Nvl(.w_ElePerc.getVar('Cddatnas'),Cp_CharToDate('  -  -    '))
        .w_COMNAS = Alltrim(Nvl(.w_ElePerc.getVar('CdComnas'),' '))
        .w_PRONAS = Alltrim(Nvl(.w_ElePerc.getVar('CdPronas'),' '))
        .w_SESSO = Alltrim(Nvl(.w_ElePerc.getVar('Cd_Sesso'),' '))
        .w_CATPAR = Alltrim(Nvl(.w_ElePerc.getVar('Cdcatpar'),' '))
        .w_EVEECC = Alltrim(Nvl(.w_ElePerc.getVar('Cdeveecc'),'0'))
        .w_COMFIS = Alltrim(Nvl(.w_ElePerc.getVar('Cdcomfis'),' '))
        .w_PROFIS = Alltrim(Nvl(.w_ElePerc.getVar('Cdprofis'),'  '))
        .w_COMUNE = Alltrim(Nvl(.w_ElePerc.getVar('Cdcomune'),'  '))
        .w_CAUPRE = Alltrim(Nvl(.w_ElePerc.getVar('Cdcaupre'),'  '))
        .w_AMMCOR = Nvl(.w_ElePerc.getVar('Cdammcor'),0)
        .w_SNSOGG = Nvl(.w_ElePerc.getVar('Cdsnsogg'),0)
        .w_CODICE = Alltrim(Nvl(.w_ElePerc.getVar('Cdcodice'),' '))
        .w_IMPONI = Nvl(.w_ElePerc.getVar('Cdimponi'),0)
        .w_RITACC = Nvl(.w_ElePerc.getVar('Cdritacc'),0)
        .w_SOGERO = Nvl(.w_ElePerc.getVar('Cdsogero'),0)
        .w_PERCIP = Nvl(.w_ElePerc.getVar('Cdpercip'),0)
        .w_SPERIM = Nvl(.w_ElePerc.getVar('Cdsperim'),0)
        .w_RITRIM = Nvl(.w_ElePerc.getVar('Cdritrim'),0)
      .oPgFrm.Page1.oPag.ElePerc.Calculate(.w_Seriale_Cert)
          .DoRTCalc(29,31,.f.)
        .w_SNSOGG1 = Nvl(.w_ElePerc.getVar('Snsogg1'),0)
        .w_PROMOD = Nvl(.w_ElePerc.getVar('Cdpromod'),0)
        .w_PROMOD1 = Nvl(.w_ElePerc.getVar('Promod1'),0)
        .w_PROMOD2 = Nvl(.w_ElePerc.getVar('Promod2'),0)
        .w_SNSOGG2 = Nvl(.w_ElePerc.getVar('Snsogg2'),0)
        .w_CODICE1 = Alltrim(Nvl(.w_ElePerc.getVar('Codice1'),' '))
        .w_CODICE2 = Alltrim(Nvl(.w_ElePerc.getVar('Codice2'),' '))
        .w_FPERCI = Alltrim(Nvl(.w_ElePerc.getVar('Cdfperci'),'  '))
      .oPgFrm.Page1.oPag.oObj_1_48.Calculate('','',RGB(255,255,0))
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_4.enabled = this.oPgFrm.Page1.oPag.oBtn_1_4.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_5.enabled = this.oPgFrm.Page1.oPag.oBtn_1_5.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsri_scs
    if used ('ElencoCertificazioni')
       use in ElencoCertificazioni
    Endif
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_SERIALE_CERT=.w_SERIALE_CERT
      .oParentObject.w_CUDATCER=.w_CUDATCER
      .oParentObject.w_CODICEFIS=.w_CODICEFIS
      .oParentObject.w_TIPO_COMU=.w_TIPO_COMU
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.AnuPerc.Calculate(.w_Seriale_Cert)
        .DoRTCalc(1,2,.t.)
            .w_CDSERIAL = iif(.w_Cutipcas='A',Alltrim(Nvl(.w_AnuPerc.getVar('Cdserial'),' ')),Alltrim(Nvl(.w_ElePerc.getVar('Cdserial'),' ')))
            .w_NUMERORIGA = iif(.w_Cutipcas='A',Nvl(.w_AnuPerc.getVar('Cprownum'),0),Nvl(.w_ElePerc.getVar('Cprownum'),0))
            .w_XCHK = iif(.w_Cutipcas='A',Nvl(.w_AnuPerc.getVar('XCHK'),0),Nvl(.w_ElePerc.getVar('XCHK'),0))
        .DoRTCalc(6,6,.t.)
            .w_CODFIS = Alltrim(Nvl(.w_ElePerc.getVar('Cdcodfis'),' '))
            .w_COGNOM = Alltrim(Nvl(.w_ElePerc.getVar('Cdcognom'),' '))
            .w_NOME = Alltrim(Nvl(.w_ElePerc.getVar('Cd__Nome'),' '))
            .w_DATNAS = Nvl(.w_ElePerc.getVar('Cddatnas'),Cp_CharToDate('  -  -    '))
            .w_COMNAS = Alltrim(Nvl(.w_ElePerc.getVar('CdComnas'),' '))
            .w_PRONAS = Alltrim(Nvl(.w_ElePerc.getVar('CdPronas'),' '))
            .w_SESSO = Alltrim(Nvl(.w_ElePerc.getVar('Cd_Sesso'),' '))
            .w_CATPAR = Alltrim(Nvl(.w_ElePerc.getVar('Cdcatpar'),' '))
            .w_EVEECC = Alltrim(Nvl(.w_ElePerc.getVar('Cdeveecc'),'0'))
            .w_COMFIS = Alltrim(Nvl(.w_ElePerc.getVar('Cdcomfis'),' '))
            .w_PROFIS = Alltrim(Nvl(.w_ElePerc.getVar('Cdprofis'),'  '))
            .w_COMUNE = Alltrim(Nvl(.w_ElePerc.getVar('Cdcomune'),'  '))
            .w_CAUPRE = Alltrim(Nvl(.w_ElePerc.getVar('Cdcaupre'),'  '))
            .w_AMMCOR = Nvl(.w_ElePerc.getVar('Cdammcor'),0)
            .w_SNSOGG = Nvl(.w_ElePerc.getVar('Cdsnsogg'),0)
            .w_CODICE = Alltrim(Nvl(.w_ElePerc.getVar('Cdcodice'),' '))
            .w_IMPONI = Nvl(.w_ElePerc.getVar('Cdimponi'),0)
            .w_RITACC = Nvl(.w_ElePerc.getVar('Cdritacc'),0)
            .w_SOGERO = Nvl(.w_ElePerc.getVar('Cdsogero'),0)
            .w_PERCIP = Nvl(.w_ElePerc.getVar('Cdpercip'),0)
            .w_SPERIM = Nvl(.w_ElePerc.getVar('Cdsperim'),0)
            .w_RITRIM = Nvl(.w_ElePerc.getVar('Cdritrim'),0)
        .oPgFrm.Page1.oPag.ElePerc.Calculate(.w_Seriale_Cert)
        .DoRTCalc(29,31,.t.)
            .w_SNSOGG1 = Nvl(.w_ElePerc.getVar('Snsogg1'),0)
            .w_PROMOD = Nvl(.w_ElePerc.getVar('Cdpromod'),0)
            .w_PROMOD1 = Nvl(.w_ElePerc.getVar('Promod1'),0)
            .w_PROMOD2 = Nvl(.w_ElePerc.getVar('Promod2'),0)
            .w_SNSOGG2 = Nvl(.w_ElePerc.getVar('Snsogg2'),0)
            .w_CODICE1 = Alltrim(Nvl(.w_ElePerc.getVar('Codice1'),' '))
            .w_CODICE2 = Alltrim(Nvl(.w_ElePerc.getVar('Codice2'),' '))
            .w_FPERCI = Alltrim(Nvl(.w_ElePerc.getVar('Cdfperci'),'  '))
        .oPgFrm.Page1.oPag.oObj_1_48.Calculate('','',RGB(255,255,0))
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.AnuPerc.Calculate(.w_Seriale_Cert)
        .oPgFrm.Page1.oPag.ElePerc.Calculate(.w_Seriale_Cert)
        .oPgFrm.Page1.oPag.oObj_1_48.Calculate('','',RGB(255,255,0))
    endwith
  return

  proc Calculate_VCFQLXIRJI()
    with this
          * --- Blank
          Gsri_Bcu(this;
              ,'B';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.AnuPerc.Event(cEvent)
      .oPgFrm.Page1.oPag.ElePerc.Event(cEvent)
        if lower(cEvent)==lower("Blank")
          .Calculate_VCFQLXIRJI()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_48.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCUDATCER_1_33.value==this.w_CUDATCER)
      this.oPgFrm.Page1.oPag.oCUDATCER_1_33.value=this.w_CUDATCER
    endif
    if not(this.oPgFrm.Page1.oPag.oCODICEFIS_1_35.value==this.w_CODICEFIS)
      this.oPgFrm.Page1.oPag.oCODICEFIS_1_35.value=this.w_CODICEFIS
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPO_COMU_1_37.RadioValue()==this.w_TIPO_COMU)
      this.oPgFrm.Page1.oPag.oTIPO_COMU_1_37.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- gsri_scs
      if This.w_Cutipcas='A'
         Select * from (this.w_AnuPerc.ccursor) where xchk=1 into cursor ElencoCertificazioni
      else
         Select * from (this.w_ElePerc.ccursor) where xchk=1 into cursor ElencoCertificazioni
      endif
      If Reccount('ElencoCertificazioni')>1 And This.oParentObject.oParentObject.w_Cucersin='S'
         Ah_ErrorMsg("Se attivo il flag certificazione singola%0� possibile selezionare un'unica certificazione")
         Use in ElencoCertificazioni
         i_bRes=.f.
      Else
         if Reccount('ElencoCertificazioni')=1 
           Select ElencoCertificazioni
           Go Top
           if (Empty(ElencoCertificazioni.Cdprotec) Or ElencoCertificazioni.Cdprodoc=0) And This.w_Cutipcas $ 'AS'
             Ah_ErrorMsg("Per la certificazione selezionata%0non � stato impostato l'identificativo dell'invio")
             use in ElencoCertificazioni
             i_bRes=.f.
           Endif
         Endif
      Endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsri_scsPag1 as StdContainer
  Width  = 650
  height = 517
  stdWidth  = 650
  stdheight = 517
  resizeXpos=366
  resizeYpos=238
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object AnuPerc as cp_szoombox with uid="GOGGMGFANQ",left=5, top=107, width=633,height=326,;
    caption='Object',;
   bGlobalFont=.t.,;
    bOptions=.t.,cZoomFile="GSRI_SCS",cTable="GENDCOMU",bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.t.,cMenuFile="GSRI_SCS",cZoomOnZoom="",;
    nPag=1;
    , HelpContextID = 87167206


  add object oBtn_1_4 as StdButton with uid="JNTOFUXNOR",left=538, top=466, width=48,height=45,;
    CpPicture="BMP\ok.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per aggiornare";
    , HelpContextID = 115478;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_4.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_5 as StdButton with uid="BRAIRZJTXI",left=589, top=466, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 115478;
    , caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_5.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oCUDATCER_1_33 as StdField with uid="YFKCTMWLQQ",rtseq=29,rtrep=.f.,;
    cFormVar = "w_CUDATCER", cQueryName = "CUDATCER",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 52142968,;
   bGlobalFont=.t.,;
    Height=21, Width=74, Left=241, Top=9

  add object oCODICEFIS_1_35 as StdField with uid="XXBZESXVBV",rtseq=30,rtrep=.f.,;
    cFormVar = "w_CODICEFIS", cQueryName = "CODICEFIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(16), bMultilanguage =  .f.,;
    HelpContextID = 68395679,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=241, Top=42, InputMask=replicate('X',16)


  add object oTIPO_COMU_1_37 as StdCombo with uid="DACBENUJOV",rtseq=31,rtrep=.f.,left=241,top=74,width=165,height=22, enabled=.f.;
    , HelpContextID = 203792941;
    , cFormVar="w_TIPO_COMU",RowSource=""+"Certificazione singola,"+"Sostitutiva,"+"Annullamento,"+"Ordinaria", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPO_COMU_1_37.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'S',;
    iif(this.value =3,'A',;
    iif(this.value =4,'O',;
    space(1))))))
  endfunc
  func oTIPO_COMU_1_37.GetRadio()
    this.Parent.oContained.w_TIPO_COMU = this.RadioValue()
    return .t.
  endfunc

  func oTIPO_COMU_1_37.SetRadio()
    this.Parent.oContained.w_TIPO_COMU=trim(this.Parent.oContained.w_TIPO_COMU)
    this.value = ;
      iif(this.Parent.oContained.w_TIPO_COMU=='C',1,;
      iif(this.Parent.oContained.w_TIPO_COMU=='S',2,;
      iif(this.Parent.oContained.w_TIPO_COMU=='A',3,;
      iif(this.Parent.oContained.w_TIPO_COMU=='O',4,;
      0))))
  endfunc


  add object ElePerc as cp_szoombox with uid="FQNBQGOSQQ",left=5, top=107, width=633,height=326,;
    caption='Object',;
   bGlobalFont=.t.,;
    bOptions=.t.,cZoomFile="GSRI1SCS",cTable="GENDCOMU",bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.t.,cMenuFile="GSRI_SCS",cZoomOnZoom="",;
    nPag=1;
    , HelpContextID = 87167206


  add object oObj_1_48 as cp_calclbl with uid="HNHEPYUDQR",left=9, top=490, width=22,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",;
    nPag=1;
    , HelpContextID = 87167206

  add object oStr_1_32 as StdString with uid="JRPQIQGAIZ",Visible=.t., Left=87, Top=11,;
    Alignment=1, Width=151, Height=18,;
    Caption="Data comunicazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="WIVKAWFOXE",Visible=.t., Left=10, Top=44,;
    Alignment=1, Width=228, Height=18,;
    Caption="Cod. fiscale del sostituto d'imposta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="RLSSHHHQBW",Visible=.t., Left=82, Top=78,;
    Alignment=1, Width=156, Height=18,;
    Caption="Tipo di comunicazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_49 as StdString with uid="EVLPRVXNKV",Visible=.t., Left=38, Top=492,;
    Alignment=0, Width=299, Height=18,;
    Caption="Certificazione presente in un'altra C.U."  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsri_scs','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
