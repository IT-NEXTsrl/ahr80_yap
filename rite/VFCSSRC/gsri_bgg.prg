* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_bgg                                                        *
*              Batch generazione giroconto                                     *
*                                                                              *
*      Author: Zucchetti TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][98][VRS_164]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-01-03                                                      *
* Last revis.: 2015-11-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsri_bgg",oParentObject)
return(i_retval)

define class tgsri_bgg as StdBatch
  * --- Local variables
  w_TIPCON = space(1)
  w_CONTO = space(15)
  w_CONVER = space(15)
  w_CODCAU = space(5)
  w_MRSERIAL = space(10)
  w_ERRORE = .f.
  w_CHIUSO = space(1)
  w_PNSERIAL = space(10)
  w_PNNUMRER = 0
  w_CPROWNUM = 0
  w_CPROWORD = 0
  w_PNCOMPET = space(4)
  w_PNCODESE = space(4)
  w_PNCODUTE = 0
  w_PNDATREG = ctod("  /  /  ")
  w_PNCODCAU = space(5)
  w_PNNUMDOC = 0
  w_PNALFDOC = space(10)
  w_PNDATDOC = ctod("  /  /  ")
  w_PNVALNAZ = space(3)
  w_PNCODVAL = space(3)
  w_PNCAOVAL = 0
  w_PNFLPROV = space(1)
  w_PNCODCLF = space(15)
  w_PNTIPCLF = space(1)
  w_PNDESSUP = space(50)
  w_FLSTAT = space(1)
  w_CAONAZ = 0
  w_DECTOP = 0
  w_APPO = space(10)
  w_APPVAL = 0
  w_PNIMPDAR = 0
  w_PNIMPAVE = 0
  w_TIPO = space(5)
  w_PNFLSALD = space(1)
  w_SEZIONE = space(1)
  w_PNCODCON = space(15)
  w_MESS = space(50)
  w_MAXDAT = ctod("  /  /  ")
  w_STALIG = ctod("  /  /  ")
  w_DATBLO = ctod("  /  /  ")
  w_CODESE = space(4)
  w_MESS1 = space(50)
  w_APPO1 = space(10)
  w_APPO2 = space(10)
  w_ESEINI = space(4)
  w_ESEFIN = space(4)
  w_ESERCI = space(4)
  w_VALUTA = space(3)
  w_CAMBIO = 0
  w_SERIALE = space(10)
  w_IMPORTO = 0
  w_CHIAVE = space(10)
  w_SCRIVO = .f.
  w_FLGSTO = space(1)
  w_IMPORTO2 = 0
  w_PTALFDOC = space(10)
  w_PTBANAPP = space(10)
  w_PTBANNOS = space(15)
  w_PTCAOVAL = 0
  w_PTCAOAPE = 0
  w_PTCODVAL = space(3)
  w_PTDATAPE = ctod("  /  /  ")
  w_PTDATDOC = ctod("  /  /  ")
  w_PTDATSCA = ctod("  /  /  ")
  w_PTMODPAG = space(10)
  w_PTNUMCOR = space(25)
  w_PTNUMDOC = 0
  w_PTNUMPAR = space(31)
  w_PTCODAGE = space(5)
  w_MRRIFCON = space(10)
  w_MRRIFORD = 0
  w_MRRIFNUM = 0
  w_NODOC = 0
  w_TOTREG = 0
  w_SEGNO = space(1)
  w_PTTOTIMP = 0
  w_PTSERRIF = space(10)
  w_PTORDRIF = 0
  w_PTNUMRIF = 0
  w_PNPRG = space(8)
  w_PARMSG1 = space(10)
  w_PARMSG2 = space(10)
  w_oERRORLOG = .NULL.
  w_CCDESRIG = space(254)
  w_CCDESSUP = space(254)
  w_PNDESRIG = space(50)
  * --- WorkFile variables
  CAU_CONT_idx=0
  VALUTE_idx=0
  PNT_MAST_idx=0
  PNT_DETT_idx=0
  SALDICON_idx=0
  AZIENDA_idx=0
  MOV_RITE_idx=0
  TAB_RITE_idx=0
  PAR_TITE_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch lanciato dalla generazione giroconto ritenute (GSRI_KGG)
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Crea il File delle Messaggistiche di Errore
    CREATE CURSOR MessErr (MSG C(120))
    this.w_oERRORLOG=createobject("AH_ERRORLOG")
    this.w_NODOC = 0
    this.w_TOTREG = 0
    * --- Controllo se ho impostato nell'archivio Tabella Ritenute il flag Storno Immediato
    if this.oParentObject.w_TIPORI="A"
      * --- Read from TAB_RITE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.TAB_RITE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TAB_RITE_idx,2],.t.,this.TAB_RITE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "TRFLGSTO"+;
          " from "+i_cTable+" TAB_RITE where ";
              +"TRCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
              +" and TRTIPRIT = "+cp_ToStrODBC(this.oParentObject.w_TIPORI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          TRFLGSTO;
          from (i_cTable) where;
              TRCODAZI = this.oParentObject.w_CODAZI;
              and TRTIPRIT = this.oParentObject.w_TIPORI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_FLGSTO = NVL(cp_ToDate(_read_.TRFLGSTO),cp_NullValue(_read_.TRFLGSTO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_TIPCON = "F"
      this.w_CONTO = this.oParentObject.w_CONRIT
      this.w_CONVER = this.oParentObject.w_COIRVE
      this.w_CODCAU = this.oParentObject.w_GIROCO
    else
      * --- Read from TAB_RITE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.TAB_RITE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TAB_RITE_idx,2],.t.,this.TAB_RITE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "TRFLGST1"+;
          " from "+i_cTable+" TAB_RITE where ";
              +"TRCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
              +" and TRTIPRIT = "+cp_ToStrODBC(this.oParentObject.w_TIPORI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          TRFLGST1;
          from (i_cTable) where;
              TRCODAZI = this.oParentObject.w_CODAZI;
              and TRTIPRIT = this.oParentObject.w_TIPORI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_FLGSTO = NVL(cp_ToDate(_read_.TRFLGST1),cp_NullValue(_read_.TRFLGST1))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_TIPCON = "C"
      this.w_CONTO = this.oParentObject.w_EVICRE
      this.w_CONVER = this.oParentObject.w_CONTRS
      this.w_CODCAU = iif(this.w_FLGSTO="S",this.oParentObject.w_CAUGIR,this.oParentObject.w_CAUSAL)
    endif
    * --- Verifico che nella maschera di elaborazione siano stati inseriti correttamente i valori.
    this.Page_4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Verifico quali sono i movimenti ritenute che hanno data di registrazione
    *     all'interno dei filtri impostati nella maschera e che hanno il campo Mrgengir vuoto.
    *     Effettuo il secondo controllo, per evitare che non venga rigenerato il giroconto, anche per movimenti gi� presenti in primanota.
    if this.w_FLGSTO="S"
      vq_exec("..\RITE\EXE\QUERY\GSRI_BGG.VQR",this,"RITENUTE")
    else
      vq_exec("..\RITE\EXE\QUERY\GSRI_BGG_N.VQR",this,"RITENUTE")
    endif
    if Reccount("RITENUTE")>0
      if this.w_FLGSTO="S"
        * --- Passo il risultato della query in un cursore di appoggio per verificare se il 
        *     movimento deve essere generato.
        Select * FROM RITENUTE INTO CURSOR ERRORE WHERE MRFLGSTO="N" ORDER BY MRDATREG,MRSERIAL
        Select ERRORE
        Go Top 
        this.w_MRSERIAL = space(10)
        Scan
        * --- Segnalo l'errore
        if this.w_MRSERIAL<>ERRORE.MRSERIAL
          this.w_APPO2 = "Verificare movimento ritenute numero: %1 del %2"
          this.w_PARMSG1 = ALLTRIM(STR(NVL(MRNUMREG,0)))
          this.w_PARMSG2 = DTOC(CP_TODATE(MRDATREG))
          this.w_oERRORLOG.AddMsgLog(this.w_APPO2, this.w_PARMSG1, this.w_PARMSG2)     
          this.w_APPO2 = "Flag storno ritenute presente sul movimento ritenute differente da quello impostato nella tabella ritenute"
          this.w_oERRORLOG.AddMsgLog(this.w_APPO2)     
          this.w_MRSERIAL = ERRORE.MRSERIAL
        endif
        this.w_NODOC = this.w_NODOC + 1
        Endscan
        if USED("ERRORE")
          SELECT ERRORE
          USE
        endif
        * --- Elimino i movimenti ritenute che hanno il flag ad 'N'
        SELECT RITENUTE
        Delete from Ritenute where Mrflgsto="N"
        * --- Passo il risultato della query in un cursore di appoggio per verificare se il 
        *     movimento deve essere generato.
        Select * FROM RITENUTE INTO CURSOR ERRMOV WHERE MRFLGSTO="S" ORDER BY MRDATREG,MRSERIAL
      else
        * --- Passo il risultato della query in un cursore di appoggio per verificare se il 
        *     movimento deve essere generato.
        Select * FROM RITENUTE INTO CURSOR ERRORE WHERE MRFLGSTO="S" ORDER BY MRDATREG,MRSERIAL
        * --- Elimino i movimenti ritenute che hanno il flag ad 'S'
        Select ERRORE
        Go Top 
        this.w_MRSERIAL = space(10)
        Scan 
        if this.w_MRSERIAL<>ERRORE.MRSERIAL
          * --- Segnalo l'errore
          this.w_PARMSG1 = ALLTRIM(STR(NVL(MRNUMREG,0)))
          this.w_PARMSG2 = DTOC(CP_TODATE(MRDATREG))
          this.w_APPO2 = "Verificare movimento ritenute numero: %1 del %2"
          this.w_oERRORLOG.AddMsgLog(this.w_APPO2, this.w_PARMSG1,this.w_PARMSG2)     
          this.w_APPO2 = "Flag storno ritenute presente sul movimento ritenute differente da quello impostato nella tabella ritenute"
          this.w_oERRORLOG.AddMsgLog(this.w_APPO2)     
          this.w_MRSERIAL = ERRORE.MRSERIAL
        endif
        this.w_NODOC = this.w_NODOC + 1
        Endscan
        if USED("ERRORE")
          SELECT ERRORE
          USE
        endif
        SELECT RITENUTE
        Delete from Ritenute where Mrflgsto="S"
        * --- Passo il risultato della query in un cursore di appoggio per verificare se il 
        *     movimento deve essere generato.
        Select * FROM RITENUTE INTO CURSOR ERRMOV WHERE MRFLGSTO="N" ORDER BY MRDATREG,MRSERIAL
      endif
      * --- Verifico se sono presenti movimenti ritenute caricati manualmente.
      *     Per tutti questi movimenti non deve essere generato il giroconto e devono essere
      *     stampati gli estremi dei movimenti non generati
      Select ERRMOV
      Go Top 
      this.w_MRSERIAL = space(10)
      Scan for empty(nvl(mrrifcon,space(10)))
      * --- Segnalo l'errore
      if this.w_MRSERIAL<>ERRMOV.MRSERIAL
        this.w_PARMSG1 = ALLTRIM(STR(NVL(MRNUMREG,0)))
        this.w_PARMSG2 = DTOC(CP_TODATE(MRDATREG))
        this.w_APPO2 = "Verificare movimento ritenute numero: %1 del %2"
        this.w_oERRORLOG.AddMsgLog(this.w_APPO2, this.w_PARMSG1,this.w_PARMSG2)     
        this.w_APPO2 = "Movimento creato manualmente"
        this.w_oERRORLOG.AddMsgLog(this.w_APPO2)     
        this.w_MRSERIAL = ERRMOV.MRSERIAL
      endif
      this.w_NODOC = this.w_NODOC + 1
      Endscan
      if USED("ERRMOV")
        SELECT ERRMOV
        USE
      endif
      SELECT RITENUTE
      * --- Elimino le registrazioni che hanno il campo MRFLGSTO uguale a 'N'
      Delete from Ritenute where empty(nvl(mrrifcon,space(10)))
      * --- Controllo che sia presente almeno un record prima di lanciare l'elaborazione.
      *     Non posso effettuare una Reccount del cursore Ritenute perch� mi verrebbero
      *     considerati anche i record cancellati.
      SELECT ("RITENUTE")
      count for not deleted() to RECNC
      SELECT ("RITENUTE")
      GO TOP
      if RECNC>0
        * --- Elaboro il cursore di generazione a seconda se ho selezionato a data odierna oppure a data pagamento.
        this.Page_5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        ah_Msg("Inizio fase di contabilizzazione...")
        * --- Fase di Blocco
        *     Blocco la Prima Nota con data = Max (data reg.)
        *     Calcolo il Max Data reg.
        this.w_MAXDAT = IIF(this.oParentObject.w_DATAREG="O", this.oParentObject.w_REGIDATA, this.oParentObject.w_FINDATA)
        * --- Try
        local bErr_02DE89C0
        bErr_02DE89C0=bTrsErr
        this.Try_02DE89C0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          ah_ErrorMsg(i_errmsg,,"")
          i_retcode = 'stop'
          return
        endif
        bTrsErr=bTrsErr or bErr_02DE89C0
        * --- End
        * --- Controllo se ho selezionato 'Data odierna' oppure 'Data pagamento'.
        *     Se data odierna viene generata un'unica registrazione per tutte le ritenute selezionate;
        *     altrimenti tante registrazioni quanto i pagamenti.
        if this.oParentObject.w_DATAREG="O"
          * --- Genero un unico movimento giroconto.
          this.w_PNCOMPET = CALCESER(this.oParentObject.w_REGIDATA)
          this.w_PNCODESE = this.w_PNCOMPET
          this.w_PNCODUTE = IIF(g_UNIUTE $ "UE", i_CODUTE, 0)
          this.w_PNDATREG = this.oParentObject.w_REGIDATA
          this.w_PNPRG = IIF(g_UNIUTE $ "GE", DTOS(this.w_PNDATREG), SPACE(8))
          this.w_PNCODCAU = this.w_CODCAU
          this.w_PNNUMDOC = this.oParentObject.w_NUMODIE
          this.w_PNALFDOC = this.oParentObject.w_SERIEOR
          this.w_PNDATDOC = this.oParentObject.w_DATAODIE
          this.w_PNVALNAZ = g_PERVAL
          this.w_PNCODVAL = g_PERVAL
          this.w_PNCAOVAL = g_CAOVAL
          this.w_PNFLPROV = "N"
          this.w_PNCODCLF = SPACE(15)
          this.w_PNTIPCLF = SPACE(1)
          this.w_CAONAZ = g_CAOVAL
          this.w_DECTOP = g_PERPVL
          this.w_PNSERIAL = SPACE(10)
          this.w_PNNUMRER = 0
          this.Page_9()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Inizio la scrittura della testata del movimento di primanota.
          * --- Calcolo il progressivo del seriale ed inoltre il numero registrazione.
          * --- Try
          local bErr_02DE8960
          bErr_02DE8960=bTrsErr
          this.Try_02DE8960()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- rollback
            bTrsErr=.t.
            cp_EndTrs(.t.)
            this.w_MESS1 = "Errore nella scrittura"
          endif
          bTrsErr=bTrsErr or bErr_02DE8960
          * --- End
        else
          * --- Ho selezionato a data pagamento
          this.Page_7()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        * --- Verifico se l'esercizio risulta chiuso, in questo caso lancio direttamente la stampa
        *     degli errori. Posso basarmi su questa variabile dato che i movimenti da generare devono
        *     appartenere allo stesso esercizio.
        if this.w_ERRORE=.F.
          if this.w_TOTREG=0 
            this.w_APPO1 = "Per l'intervallo selezionato non esistono movimenti da generare"
            ah_ERRORMSG(this.w_APPO1,48)
          else
            this.w_APPO = "Operazione completata%0N. %1 movimenti di primanota generati"
            ah_ERRORMSG (this.w_APPO,64,"", ALLTRIM(STR(this.w_TOTREG)))
          endif
        endif
        this.w_oERRORLOG.PrintLog(This,"Errori riscontrati")     
        * --- Alla Fine Rimuovo il Blocco Primanota
        * --- Try
        local bErr_02DED3D0
        bErr_02DED3D0=bTrsErr
        this.Try_02DED3D0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          ah_ErrorMsg("Impossibile rimuovere blocco primanota. Rimuoverlo dai dati azienda","!","")
        endif
        bTrsErr=bTrsErr or bErr_02DED3D0
        * --- End
      else
        this.w_APPO1 = "Per l'intervallo selezionato non esistono movimenti da generare"
        ah_ERRORMSG(this.w_APPO1,48)
        this.w_oERRORLOG.PrintLog(This,"Errori riscontrati")     
      endif
    else
      this.w_APPO1 = "Per l'intervallo selezionato non esistono movimenti da generare"
      ah_ErrorMsg(this.w_APPO1,"!","")
    endif
    this.Page_6()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc
  proc Try_02DE89C0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Leggo la data dei blocco e la data stampa del libro giornale
    this.w_STALIG = cp_CharToDate("  -  -  ")
    this.w_DATBLO = cp_CharToDate("  -  -  ")
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZDATBLO,AZSTALIG"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZDATBLO,AZSTALIG;
        from (i_cTable) where;
            AZCODAZI = this.oParentObject.w_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DATBLO = NVL(cp_ToDate(_read_.AZDATBLO),cp_NullValue(_read_.AZDATBLO))
      this.w_STALIG = NVL(cp_ToDate(_read_.AZSTALIG),cp_NullValue(_read_.AZSTALIG))
      use
    else
      * --- Error: sql sentence error.
      i_Error = 'AZIENDA'
      return
    endif
    select (i_nOldArea)
    do case
      case this.w_MAXDAT<=this.w_STALIG AND NOT EMPTY(this.w_STALIG)
        * --- Raise
        i_Error="Data di contabilizzazione inferiore a data ultima stampa L.G."
        return
      case EMPTY(this.w_DATBLO)
        * --- Inserisce <Blocco> per Primanota
        * --- Write into AZIENDA
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.AZIENDA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.AZIENDA_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"AZDATBLO ="+cp_NullLink(cp_ToStrODBC(this.w_MAXDAT),'AZIENDA','AZDATBLO');
              +i_ccchkf ;
          +" where ";
              +"AZCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
                 )
        else
          update (i_cTable) set;
              AZDATBLO = this.w_MAXDAT;
              &i_ccchkf. ;
           where;
              AZCODAZI = this.oParentObject.w_CODAZI;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      case Not EMPTY(this.w_DATBLO)
        * --- Un altro utente ha impostato il blocco - controllo concorrenza
        * --- Raise
        i_Error="Prima nota bloccata - verificare semaforo bollati in dati azienda"
        return
    endcase
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_02DE8960()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    i_Conn=i_TableProp[this.PNT_MAST_IDX, 3]
    cp_NextTableProg(this, i_Conn, "SEPNT", "i_codazi,w_PNSERIAL")
    cp_NextTableProg(this, i_Conn, "PRPNT", "i_codazi,w_PNCODESE,w_PNCODUTE,w_PNPRG,w_PNNUMRER")
    ah_msg("Scrivo reg. n.: %1 del %2",.t.,.f.,.f., ALLTRIM(STR(this.w_PNNUMRER)), DTOC(this.w_PNDATREG))
    * --- Scrivo la testata del movimento giroconto.
    * --- Insert into PNT_MAST
    i_nConn=i_TableProp[this.PNT_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PNT_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PNSERIAL"+",PNCODESE"+",PNCODUTE"+",PNNUMRER"+",PNDATREG"+",PNCODCAU"+",PNCOMPET"+",PNTIPREG"+",PNFLIVDF"+",PNNUMREG"+",PNTIPDOC"+",PNPRD"+",PNPRP"+",PNALFDOC"+",PNNUMDOC"+",PNDATDOC"+",PNANNDOC"+",PNANNPRO"+",PNALFPRO"+",PNNUMPRO"+",PNVALNAZ"+",PNCODVAL"+",PNCAOVAL"+",PNDESSUP"+",PNTIPCLF"+",PNCODCLF"+",UTCC"+",UTCV"+",UTDC"+",UTDV"+",PNCOMIVA"+",PNFLLIBG"+",PNFLREGI"+",PNFLPROV"+",PNRIFDOC"+",PNRIFCES"+",PNFLGSTO"+",PNPRG"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PNT_MAST','PNSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODESE),'PNT_MAST','PNCODESE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODUTE),'PNT_MAST','PNCODUTE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMRER),'PNT_MAST','PNNUMRER');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATREG),'PNT_MAST','PNDATREG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCAU),'PNT_MAST','PNCODCAU');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCOMPET),'PNT_MAST','PNCOMPET');
      +","+cp_NullLink(cp_ToStrODBC("N"),'PNT_MAST','PNTIPREG');
      +","+cp_NullLink(cp_ToStrODBC(" "),'PNT_MAST','PNFLIVDF');
      +","+cp_NullLink(cp_ToStrODBC(0),'PNT_MAST','PNNUMREG');
      +","+cp_NullLink(cp_ToStrODBC("  "),'PNT_MAST','PNTIPDOC');
      +","+cp_NullLink(cp_ToStrODBC("NN"),'PNT_MAST','PNPRD');
      +","+cp_NullLink(cp_ToStrODBC("NN"),'PNT_MAST','PNPRP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNALFDOC),'PNT_MAST','PNALFDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMDOC),'PNT_MAST','PNNUMDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATDOC),'PNT_MAST','PNDATDOC');
      +","+cp_NullLink(cp_ToStrODBC("    "),'PNT_MAST','PNANNDOC');
      +","+cp_NullLink(cp_ToStrODBC("    "),'PNT_MAST','PNANNPRO');
      +","+cp_NullLink(cp_ToStrODBC("  "),'PNT_MAST','PNALFPRO');
      +","+cp_NullLink(cp_ToStrODBC(0),'PNT_MAST','PNNUMPRO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNVALNAZ),'PNT_MAST','PNVALNAZ');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODVAL),'PNT_MAST','PNCODVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCAOVAL),'PNT_MAST','PNCAOVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNDESSUP),'PNT_MAST','PNDESSUP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNTIPCLF),'PNT_MAST','PNTIPCLF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCLF),'PNT_MAST','PNCODCLF');
      +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'PNT_MAST','UTCC');
      +","+cp_NullLink(cp_ToStrODBC(0),'PNT_MAST','UTCV');
      +","+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'PNT_MAST','UTDC');
      +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -    ")),'PNT_MAST','UTDV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATREG),'PNT_MAST','PNCOMIVA');
      +","+cp_NullLink(cp_ToStrODBC(" "),'PNT_MAST','PNFLLIBG');
      +","+cp_NullLink(cp_ToStrODBC(" "),'PNT_MAST','PNFLREGI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLPROV),'PNT_MAST','PNFLPROV');
      +","+cp_NullLink(cp_ToStrODBC(SPACE(10)),'PNT_MAST','PNRIFDOC');
      +","+cp_NullLink(cp_ToStrODBC(" "),'PNT_MAST','PNRIFCES');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FLGSTO),'PNT_MAST','PNFLGSTO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNPRG),'PNT_MAST','PNPRG');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PNSERIAL',this.w_PNSERIAL,'PNCODESE',this.w_PNCODESE,'PNCODUTE',this.w_PNCODUTE,'PNNUMRER',this.w_PNNUMRER,'PNDATREG',this.w_PNDATREG,'PNCODCAU',this.w_PNCODCAU,'PNCOMPET',this.w_PNCOMPET,'PNTIPREG',"N",'PNFLIVDF'," ",'PNNUMREG',0,'PNTIPDOC',"  ",'PNPRD',"NN")
      insert into (i_cTable) (PNSERIAL,PNCODESE,PNCODUTE,PNNUMRER,PNDATREG,PNCODCAU,PNCOMPET,PNTIPREG,PNFLIVDF,PNNUMREG,PNTIPDOC,PNPRD,PNPRP,PNALFDOC,PNNUMDOC,PNDATDOC,PNANNDOC,PNANNPRO,PNALFPRO,PNNUMPRO,PNVALNAZ,PNCODVAL,PNCAOVAL,PNDESSUP,PNTIPCLF,PNCODCLF,UTCC,UTCV,UTDC,UTDV,PNCOMIVA,PNFLLIBG,PNFLREGI,PNFLPROV,PNRIFDOC,PNRIFCES,PNFLGSTO,PNPRG &i_ccchkf. );
         values (;
           this.w_PNSERIAL;
           ,this.w_PNCODESE;
           ,this.w_PNCODUTE;
           ,this.w_PNNUMRER;
           ,this.w_PNDATREG;
           ,this.w_PNCODCAU;
           ,this.w_PNCOMPET;
           ,"N";
           ," ";
           ,0;
           ,"  ";
           ,"NN";
           ,"NN";
           ,this.w_PNALFDOC;
           ,this.w_PNNUMDOC;
           ,this.w_PNDATDOC;
           ,"    ";
           ,"    ";
           ,"  ";
           ,0;
           ,this.w_PNVALNAZ;
           ,this.w_PNCODVAL;
           ,this.w_PNCAOVAL;
           ,this.w_PNDESSUP;
           ,this.w_PNTIPCLF;
           ,this.w_PNCODCLF;
           ,i_CODUTE;
           ,0;
           ,SetInfoDate( g_CALUTD );
           ,cp_CharToDate("  -  -    ");
           ,this.w_PNDATREG;
           ," ";
           ," ";
           ,this.w_PNFLPROV;
           ,SPACE(10);
           ," ";
           ,this.w_FLGSTO;
           ,this.w_PNPRG;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Lancio in esecuzione pagina 3 due volte.La prima per scrivere il conto in avere la seconda per scrivere il relativo conto dare.
    * --- Scrivo il dettaglio di primanota.
    if Reccount("GENERA")>0
      Select Genera
      Go Top
      Scan 
      this.w_SEZIONE = "A"
      * --- Serve per sapere se scrivo il conto in Dare oppure in Avere.
      * --- Inizio a scrivere i conti in AVERE.
      this.Page_3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_SEZIONE = "D"
      * --- Inizio a scrivere i conti in Dare.
      this.Page_3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      Endscan
      * --- Aggiorno i movimenti ritenute con il serial del movimento generato.
      * --- Utilizzo il cursore ritenute perch� su questo cursore non ho effettuato nessun raggruppamento.
      Select Ritenute
      Go top
      Scan
      this.w_SERIALE = RITENUTE.MRSERIAL
      * --- Write into MOV_RITE
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.MOV_RITE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MOV_RITE_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.MOV_RITE_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MRGENGIR ="+cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'MOV_RITE','MRGENGIR');
            +i_ccchkf ;
        +" where ";
            +"MRSERIAL = "+cp_ToStrODBC(this.w_SERIALE);
               )
      else
        update (i_cTable) set;
            MRGENGIR = this.w_PNSERIAL;
            &i_ccchkf. ;
         where;
            MRSERIAL = this.w_SERIALE;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      Endscan
      * --- commit
      cp_EndTrs(.t.)
      this.w_MESS1 = "Operazione terminata con successo"
      this.w_TOTREG = this.w_TOTREG + 1
    else
      this.w_APPO2 = "Nessun movimento generato. Importi non valorizzati"
      this.w_NODOC = 1
      this.w_oERRORLOG.AddMsgLog(this.w_APPO2)     
      * --- Raise
      i_Error=this.w_APPO2
      return
    endif
    return
  proc Try_02DED3D0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into AZIENDA
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.AZIENDA_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"AZDATBLO ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'AZIENDA','AZDATBLO');
          +i_ccchkf ;
      +" where ";
          +"AZCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
             )
    else
      update (i_cTable) set;
          AZDATBLO = cp_CharToDate("  -  -  ");
          &i_ccchkf. ;
       where;
          AZCODAZI = this.oParentObject.w_CODAZI;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Dichiarazione variabili.
    * --- Variabili globali
    * --- Variabili locali
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Scrive Dettaglio P.N. e Saldi
    this.w_CPROWNUM = this.w_CPROWNUM + 1
    this.w_CPROWORD = this.w_CPROWORD + 10
    this.w_PNFLSALD = IIF(this.w_PNFLPROV="S", " ", "+")
    this.w_APPVAL = GENERA.IMPORTO
    * --- Controllo se sto scrivendo il conto in Dare oppure in Avere
    this.w_TIPO = Genera.TIPO
    if this.w_SEZIONE= IIF(this.oParentObject.w_TIPORI="A","A","D")
      if this.w_APPVAL>0
        this.w_PNIMPDAR = 0
        this.w_PNIMPAVE = this.w_APPVAL
        * --- Devo verificare se il conto da assocciare � di tipo INPS oppure di tipo IRPEF
        if this.w_TIPO="I"
          this.w_PNCODCON = this.w_CONVER
        else
          this.w_PNCODCON = this.oParentObject.w_COPRVE
        endif
      else
        this.w_PNIMPDAR = ABS(this.w_APPVAL)
        this.w_PNIMPAVE = 0
        * --- Devo verificare se il conto da assocciare � di tipo INPS oppure di tipo IRPEF
        if this.w_TIPO="I"
          this.w_PNCODCON = this.w_CONVER
        else
          this.w_PNCODCON = this.oParentObject.w_COPRVE
        endif
      endif
    else
      if this.w_APPVAL>0
        this.w_PNIMPDAR = this.w_APPVAL
        this.w_PNIMPAVE = 0
      else
        this.w_PNIMPDAR = 0
        this.w_PNIMPAVE = ABS(this.w_APPVAL)
      endif
      * --- Devo verificare se il conto da assocciare � di tipo INPS oppure di tipo IRPEF
      if this.w_TIPO="I"
        this.w_PNCODCON = this.w_CONTO
      else
        this.w_PNCODCON = this.oParentObject.w_CORIPR
      endif
    endif
    * --- Effettuo la scrittura all'interno del dettaglio di primanota
    * --- Insert into PNT_DETT
    i_nConn=i_TableProp[this.PNT_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PNT_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PNSERIAL"+",CPROWNUM"+",CPROWORD"+",PNTIPCON"+",PNCODCON"+",PNIMPDAR"+",PNIMPAVE"+",PNFLPART"+",PNFLZERO"+",PNDESRIG"+",PNCAURIG"+",PNCODPAG"+",PNFLSALD"+",PNFLSALI"+",PNFLSALF"+",PNLIBGIO"+",PNINICOM"+",PNFINCOM"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PNT_DETT','PNSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'PNT_DETT','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'PNT_DETT','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC("G"),'PNT_DETT','PNTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCON),'PNT_DETT','PNCODCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNIMPDAR),'PNT_DETT','PNIMPDAR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNIMPAVE),'PNT_DETT','PNIMPAVE');
      +","+cp_NullLink(cp_ToStrODBC("N"),'PNT_DETT','PNFLPART');
      +","+cp_NullLink(cp_ToStrODBC(" "),'PNT_DETT','PNFLZERO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNDESRIG),'PNT_DETT','PNDESRIG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCAU),'PNT_DETT','PNCAURIG');
      +","+cp_NullLink(cp_ToStrODBC(SPACE(5)),'PNT_DETT','PNCODPAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLSALD),'PNT_DETT','PNFLSALD');
      +","+cp_NullLink(cp_ToStrODBC(" "),'PNT_DETT','PNFLSALI');
      +","+cp_NullLink(cp_ToStrODBC(" "),'PNT_DETT','PNFLSALF');
      +","+cp_NullLink(cp_ToStrODBC(0),'PNT_DETT','PNLIBGIO');
      +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'PNT_DETT','PNINICOM');
      +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'PNT_DETT','PNFINCOM');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PNSERIAL',this.w_PNSERIAL,'CPROWNUM',this.w_CPROWNUM,'CPROWORD',this.w_CPROWORD,'PNTIPCON',"G",'PNCODCON',this.w_PNCODCON,'PNIMPDAR',this.w_PNIMPDAR,'PNIMPAVE',this.w_PNIMPAVE,'PNFLPART',"N",'PNFLZERO'," ",'PNDESRIG',this.w_PNDESRIG,'PNCAURIG',this.w_PNCODCAU,'PNCODPAG',SPACE(5))
      insert into (i_cTable) (PNSERIAL,CPROWNUM,CPROWORD,PNTIPCON,PNCODCON,PNIMPDAR,PNIMPAVE,PNFLPART,PNFLZERO,PNDESRIG,PNCAURIG,PNCODPAG,PNFLSALD,PNFLSALI,PNFLSALF,PNLIBGIO,PNINICOM,PNFINCOM &i_ccchkf. );
         values (;
           this.w_PNSERIAL;
           ,this.w_CPROWNUM;
           ,this.w_CPROWORD;
           ,"G";
           ,this.w_PNCODCON;
           ,this.w_PNIMPDAR;
           ,this.w_PNIMPAVE;
           ,"N";
           ," ";
           ,this.w_PNDESRIG;
           ,this.w_PNCODCAU;
           ,SPACE(5);
           ,this.w_PNFLSALD;
           ," ";
           ," ";
           ,0;
           ,cp_CharToDate("  -  -  ");
           ,cp_CharToDate("  -  -  ");
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    if this.w_PNFLPROV<>"S"
      * --- Aggiorna Saldo
      * --- Try
      local bErr_04260198
      bErr_04260198=bTrsErr
      this.Try_04260198()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_04260198
      * --- End
      * --- Write into SALDICON
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.SALDICON_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICON_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"SLDARPER =SLDARPER+ "+cp_ToStrODBC(this.w_PNIMPDAR);
        +",SLAVEPER =SLAVEPER+ "+cp_ToStrODBC(this.w_PNIMPAVE);
            +i_ccchkf ;
        +" where ";
            +"SLTIPCON = "+cp_ToStrODBC("G");
            +" and SLCODICE = "+cp_ToStrODBC(this.w_PNCODCON);
            +" and SLCODESE = "+cp_ToStrODBC(this.w_PNCODESE);
               )
      else
        update (i_cTable) set;
            SLDARPER = SLDARPER + this.w_PNIMPDAR;
            ,SLAVEPER = SLAVEPER + this.w_PNIMPAVE;
            &i_ccchkf. ;
         where;
            SLTIPCON = "G";
            and SLCODICE = this.w_PNCODCON;
            and SLCODESE = this.w_PNCODESE;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
  endproc
  proc Try_04260198()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDICON
    i_nConn=i_TableProp[this.SALDICON_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDICON_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SLTIPCON"+",SLCODICE"+",SLCODESE"+",SLDARPRO"+",SLAVEPRO"+",SLDARPER"+",SLAVEPER"+",SLDARINI"+",SLAVEINI"+",SLDARFIN"+",SLAVEFIN"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC("G"),'SALDICON','SLTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCON),'SALDICON','SLCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODESE),'SALDICON','SLCODESE');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARPRO');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEPRO');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARPER');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEPER');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARINI');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEINI');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARFIN');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEFIN');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SLTIPCON',"G",'SLCODICE',this.w_PNCODCON,'SLCODESE',this.w_PNCODESE,'SLDARPRO',0,'SLAVEPRO',0,'SLDARPER',0,'SLAVEPER',0,'SLDARINI',0,'SLAVEINI',0,'SLDARFIN',0,'SLAVEFIN',0)
      insert into (i_cTable) (SLTIPCON,SLCODICE,SLCODESE,SLDARPRO,SLAVEPRO,SLDARPER,SLAVEPER,SLDARINI,SLAVEINI,SLDARFIN,SLAVEFIN &i_ccchkf. );
         values (;
           "G";
           ,this.w_PNCODCON;
           ,this.w_PNCODESE;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controllo che le date di selezione siano all'interno dello stesso esercizio.
    this.w_ESEINI = CALCESER(this.oParentObject.w_INIDATA,SPACE(4))
    this.w_ESEFIN = CALCESER(this.oParentObject.w_FINDATA,SPACE(4))
    if this.w_ESEINI<>this.w_ESEFIN
      this.w_MESS = "Le date di inizio e fine selezione appartengono ad esercizi differenti%0Impossibile effettuare la generazione"
      ah_ErrorMsg(this.w_MESS,"!","")
      i_retcode = 'stop'
      return
    endif
    * --- Se creo un unico moviemnto di primanota devo verificare che la data sia all'interno
    *     dell'esercizio in cui effettuo la registrazione.
    if this.oParentObject.w_DATAREG="O"
      this.w_ESERCI = CALCESER(this.oParentObject.w_REGIDATA,SPACE(4))
      if this.w_ESERCI<>g_CODESE
        this.w_MESS = "Attenzione:Data registrazione esterna all'esercizio corrente"
        ah_ErrorMsg(this.w_MESS,"!","")
      endif
    endif
    * --- Verifico che siano stati inseriti i conti sui quali effettuare l'operazione di giroconto.
    if EMPTY(this.w_CONVER) and ((this.oParentObject.w_TIPORI="V" AND this.w_FLGSTO="S") or this.oParentObject.w_TIPORI="A")
      this.w_MESS = IIF(this.oParentObject.w_TIPORI="A","Conto ritenute IRPEF da versare non specificato","Conto di transito non specificato")
      ah_ErrorMsg(this.w_MESS,"!","")
      i_retcode = 'stop'
      return
    endif
    if EMPTY(this.oParentObject.w_COPRVE) and this.oParentObject.w_TIPORI="A"
      this.w_MESS = "Conto ritenute previdenziali da versare non specificato"
      ah_ErrorMsg(this.w_MESS,"!","")
      i_retcode = 'stop'
      return
    endif
    if EMPTY(this.w_CONTO) AND ((this.w_FLGSTO="S" AND this.oParentObject.w_TIPORI="A") OR this.oParentObject.w_TIPORI="V")
      this.w_MESS = IIF(this.oParentObject.w_TIPORI="A","Conto erario c\ritenute non specificato","Conto crediti v/erario non specificato")
      ah_ErrorMsg(this.w_MESS,"!","")
      i_retcode = 'stop'
      return
    endif
    if this.w_FLGSTO="S"
      if EMPTY(this.oParentObject.w_CORIPR) and this.oParentObject.w_TIPORI="A"
        this.w_MESS = "Conto ritenute previdenziali non specificato"
        ah_ErrorMsg(this.w_MESS,"!","")
        i_retcode = 'stop'
        return
      endif
    endif
    if EMPTY(this.w_CODCAU) 
      if this.oParentObject.w_TIPORI="V" AND this.w_FLGSTO<>"S"
        this.w_MESS = "Causale saldaconto non specificata"
      else
        this.w_MESS = "Causale giroconto non specificata"
      endif
      ah_ErrorMsg(this.w_MESS,"!","")
      i_retcode = 'stop'
      return
    endif
  endproc


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elaboro il cursore a seconda se seleziono a data pagamento oppure a data odierna.
    *     Se ho selezionato a data odierna devo prima convertire tutto nella valuta dell'esercizio e poi successivamente
    *     ragruppare per business unit e tipologia versamento.
    *     Rendo scrivibile il cursore Ritenute
    =Wrcursor("Ritenute")
    Select Ritenute
    Go top
    Scan for Ritenute.Mrcodval<>g_perval
    this.w_CAMBIO = NVL(RITENUTE.MRCAOVAL,g_CAOVAL)
    this.w_IMPORTO = cp_ROUND(VAL2MON(RITENUTE.IMPORTO,this.w_CAMBIO,g_CAOVAL,this.oParentObject.w_REGIDATA,g_PERVAL), g_PERPVL)
    Replace IMPORTO with this.w_IMPORTO
    Endscan
    if this.oParentObject.w_DATAREG="O"
      * --- Raggruppo il cursore per codice business unit e tipologia versamento.
      Select Sum(Importo) as Importo,Max(Mrserial) as Mrserial,Tipo as Tipo from Ritenute; 
 group by tipo having Sum(Importo)<>0 into cursor GENERA 
    else
      Select Importo as Importo,Mrserial as Mrserial,Tipo as Tipo ,; 
 Mrnumreg as Mrnumreg,Mrdatreg as Mrdatreg,Mrnumdoc as Mrnumdoc,Mrserdoc as Mrserdoc,Mrdatdoc as Mrdatdoc,; 
 Mrtipcon as Mrtipcon,Mrcodcon as Mrcodcon,Mrrifcon as Mrrifcon,Mrriford as Mrriford,Mrrifnum as Mrrifnum from Ritenute; 
 into cursor GENERA order by Mrdatreg,Mrserial,Tipo
    endif
  endproc


  procedure Page_6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if USED("RITENUTE")
      SELECT Ritenute
      USE
    endif
    if USED("GENERA")
      SELECT GENERA
      USE
    endif
    if USED("PARTITE")
      SELECT PARTITE
      USE
    endif
    if USED("MESSERR")
      SELECT MESSERR
      USE
    endif
    if USED("__TMP__")
      SELECT __TMP__
      USE
    endif
    i_retcode = 'stop'
    return
  endproc


  procedure Page_7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Selezionando a data pagamento verranno generati tanti movimenti di giroconto quanti sono
    *     i movimenti di pagamento con date di registrazione differenti.
    Select Genera
    Go Top
    this.w_CHIAVE = "XXXXXXXXXX"
    SCAN FOR NOT EMPTY(NVL(MRSERIAL," ")) AND NOT EMPTY(NVL(MRDATREG, cp_CharToDate("  -  -  ")))
    this.w_SCRIVO = .F.
    * --- Verifico se ho lo stesso serial e quindi non devo riscrivere la testata della registrazione di primanota.
    if this.w_CHIAVE<>Genera.MRSERIAL
      * --- Scrivo un nuovo documento
      this.w_CHIAVE = Genera.Mrserial
      this.w_PNSERIAL = SPACE(10)
      this.w_PNNUMRER = 0
      this.w_CPROWNUM = 0
      this.w_CPROWORD = 0
      this.w_SCRIVO = .T.
      this.w_ERRORE = .F.
      this.w_PNCODUTE = IIF(g_UNIUTE $ "UE", i_CODUTE, 0)
      this.w_PNDATREG = GENERA.MRDATREG
      this.w_PNCOMPET = CALCESER(this.w_PNDATREG)
      this.w_PNCODESE = this.w_PNCOMPET
      this.w_PNPRG = IIF(g_UNIUTE $ "GE", DTOS(this.w_PNDATREG), SPACE(8))
      * --- Devo verificare se l'esercizio in corso risulta chiuso, in questo caso non devo
      *     permettere la creazione del movimento di primanota.
      this.w_PNNUMDOC = GENERA.MRNUMDOC
      this.w_PNALFDOC = GENERA.MRSERDOC
      this.w_PNDATDOC = GENERA.MRDATDOC
      this.w_PNVALNAZ = g_PERVAL
      this.w_PNCODVAL = g_PERVAL
      this.w_PNCAOVAL = g_CAOVAL
      this.w_PNFLPROV = "N"
      this.w_PNCODCLF = SPACE(15)
      this.w_PNTIPCLF = SPACE(1)
      this.w_CAONAZ = g_CAOVAL
      this.w_DECTOP = g_PERPVL
      this.Page_9()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.w_ERRORE=.F.
      * --- Inizio la scrittura della testata del movimento di primanota.
      *     Calcolo il progressivo del seriale ed inoltre il numero registrazione.
      * --- Try
      local bErr_042BBBA8
      bErr_042BBBA8=bTrsErr
      this.Try_042BBBA8()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        this.w_MESS1 = "Errore nella scrittura"
      endif
      bTrsErr=bTrsErr or bErr_042BBBA8
      * --- End
    endif
    Endscan
  endproc
  proc Try_042BBBA8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Controllo se devo scrivere la testata del documento
    if this.w_SCRIVO
      * --- begin transaction
      cp_BeginTrs()
      i_Conn=i_TableProp[this.PNT_MAST_IDX, 3]
      cp_NextTableProg(this, i_Conn, "SEPNT", "i_codazi,w_PNSERIAL")
      cp_NextTableProg(this, i_Conn, "PRPNT", "i_codazi,w_PNCODESE,w_PNCODUTE,w_PNPRG,w_PNNUMRER")
      ah_msg("Scrivo reg. n.: %1 del %2",.t.,.f.,.f., ALLTRIM(STR(this.w_PNNUMRER)), DTOC(this.w_PNDATREG))
      * --- Scrivo la testata del movimento giroconto.
      * --- Insert into PNT_MAST
      i_nConn=i_TableProp[this.PNT_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PNT_MAST_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"PNSERIAL"+",PNCODESE"+",PNCODUTE"+",PNNUMRER"+",PNDATREG"+",PNCODCAU"+",PNCOMPET"+",PNTIPREG"+",PNFLIVDF"+",PNNUMREG"+",PNTIPDOC"+",PNPRD"+",PNPRP"+",PNALFDOC"+",PNNUMDOC"+",PNDATDOC"+",PNANNDOC"+",PNANNPRO"+",PNALFPRO"+",PNNUMPRO"+",PNVALNAZ"+",PNCODVAL"+",PNCAOVAL"+",PNDESSUP"+",PNTIPCLF"+",PNCODCLF"+",UTCC"+",UTCV"+",UTDC"+",UTDV"+",PNCOMIVA"+",PNFLLIBG"+",PNFLREGI"+",PNFLPROV"+",PNRIFDOC"+",PNRIFCES"+",PNFLGSTO"+",PNPRG"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PNT_MAST','PNSERIAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODESE),'PNT_MAST','PNCODESE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODUTE),'PNT_MAST','PNCODUTE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMRER),'PNT_MAST','PNNUMRER');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATREG),'PNT_MAST','PNDATREG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCAU),'PNT_MAST','PNCODCAU');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNCOMPET),'PNT_MAST','PNCOMPET');
        +","+cp_NullLink(cp_ToStrODBC("N"),'PNT_MAST','PNTIPREG');
        +","+cp_NullLink(cp_ToStrODBC(" "),'PNT_MAST','PNFLIVDF');
        +","+cp_NullLink(cp_ToStrODBC(0),'PNT_MAST','PNNUMREG');
        +","+cp_NullLink(cp_ToStrODBC("  "),'PNT_MAST','PNTIPDOC');
        +","+cp_NullLink(cp_ToStrODBC("NN"),'PNT_MAST','PNPRD');
        +","+cp_NullLink(cp_ToStrODBC("NN"),'PNT_MAST','PNPRP');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNALFDOC),'PNT_MAST','PNALFDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMDOC),'PNT_MAST','PNNUMDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATDOC),'PNT_MAST','PNDATDOC');
        +","+cp_NullLink(cp_ToStrODBC("    "),'PNT_MAST','PNANNDOC');
        +","+cp_NullLink(cp_ToStrODBC("    "),'PNT_MAST','PNANNPRO');
        +","+cp_NullLink(cp_ToStrODBC("  "),'PNT_MAST','PNALFPRO');
        +","+cp_NullLink(cp_ToStrODBC(0),'PNT_MAST','PNNUMPRO');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNVALNAZ),'PNT_MAST','PNVALNAZ');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODVAL),'PNT_MAST','PNCODVAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNCAOVAL),'PNT_MAST','PNCAOVAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNDESSUP),'PNT_MAST','PNDESSUP');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNTIPCLF),'PNT_MAST','PNTIPCLF');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCLF),'PNT_MAST','PNCODCLF');
        +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'PNT_MAST','UTCC');
        +","+cp_NullLink(cp_ToStrODBC(0),'PNT_MAST','UTCV');
        +","+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'PNT_MAST','UTDC');
        +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -    ")),'PNT_MAST','UTDV');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATREG),'PNT_MAST','PNCOMIVA');
        +","+cp_NullLink(cp_ToStrODBC(" "),'PNT_MAST','PNFLLIBG');
        +","+cp_NullLink(cp_ToStrODBC(" "),'PNT_MAST','PNFLREGI');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLPROV),'PNT_MAST','PNFLPROV');
        +","+cp_NullLink(cp_ToStrODBC(SPACE(10)),'PNT_MAST','PNRIFDOC');
        +","+cp_NullLink(cp_ToStrODBC(" "),'PNT_MAST','PNRIFCES');
        +","+cp_NullLink(cp_ToStrODBC(this.w_FLGSTO),'PNT_MAST','PNFLGSTO');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNPRG),'PNT_MAST','PNPRG');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'PNSERIAL',this.w_PNSERIAL,'PNCODESE',this.w_PNCODESE,'PNCODUTE',this.w_PNCODUTE,'PNNUMRER',this.w_PNNUMRER,'PNDATREG',this.w_PNDATREG,'PNCODCAU',this.w_PNCODCAU,'PNCOMPET',this.w_PNCOMPET,'PNTIPREG',"N",'PNFLIVDF'," ",'PNNUMREG',0,'PNTIPDOC',"  ",'PNPRD',"NN")
        insert into (i_cTable) (PNSERIAL,PNCODESE,PNCODUTE,PNNUMRER,PNDATREG,PNCODCAU,PNCOMPET,PNTIPREG,PNFLIVDF,PNNUMREG,PNTIPDOC,PNPRD,PNPRP,PNALFDOC,PNNUMDOC,PNDATDOC,PNANNDOC,PNANNPRO,PNALFPRO,PNNUMPRO,PNVALNAZ,PNCODVAL,PNCAOVAL,PNDESSUP,PNTIPCLF,PNCODCLF,UTCC,UTCV,UTDC,UTDV,PNCOMIVA,PNFLLIBG,PNFLREGI,PNFLPROV,PNRIFDOC,PNRIFCES,PNFLGSTO,PNPRG &i_ccchkf. );
           values (;
             this.w_PNSERIAL;
             ,this.w_PNCODESE;
             ,this.w_PNCODUTE;
             ,this.w_PNNUMRER;
             ,this.w_PNDATREG;
             ,this.w_PNCODCAU;
             ,this.w_PNCOMPET;
             ,"N";
             ," ";
             ,0;
             ,"  ";
             ,"NN";
             ,"NN";
             ,this.w_PNALFDOC;
             ,this.w_PNNUMDOC;
             ,this.w_PNDATDOC;
             ,"    ";
             ,"    ";
             ,"  ";
             ,0;
             ,this.w_PNVALNAZ;
             ,this.w_PNCODVAL;
             ,this.w_PNCAOVAL;
             ,this.w_PNDESSUP;
             ,this.w_PNTIPCLF;
             ,this.w_PNCODCLF;
             ,i_CODUTE;
             ,0;
             ,SetInfoDate( g_CALUTD );
             ,cp_CharToDate("  -  -    ");
             ,this.w_PNDATREG;
             ," ";
             ," ";
             ,this.w_PNFLPROV;
             ,SPACE(10);
             ," ";
             ,this.w_FLGSTO;
             ,this.w_PNPRG;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      this.w_TOTREG = this.w_TOTREG + 1
    endif
    * --- Devo controllare l'importo della riga dato che posso generare Giroconti 
    *     Ritenute anche per Note di Credito
    this.w_APPVAL = GENERA.IMPORTO
    if this.w_APPVAL>0
      this.w_SEZIONE = "A"
      * --- Inizio a scrivere i conti in AVERE.
      this.Page_3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_SEZIONE = "D"
      * --- Inizio a scrivere i conti in Dare.
      if this.w_FLGSTO="S"
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      else
        * --- Inizio a scrivere i conti in Dare.
        this.Page_8()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    else
      * --- Inizio a scrivere i conti in Avere
      this.w_SEZIONE = "A"
      if this.w_FLGSTO="S"
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_SEZIONE = "D"
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      else
        * --- Inizio a scrivere i conti in Dare.
        this.Page_8()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_SEZIONE = "A"
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    * --- Aggiorno i movimenti ritenute con il serial del movimento generato.
    * --- Devo verificare se ho modificato il serial del movimento
    if this.w_SCRIVO
      * --- Write into MOV_RITE
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.MOV_RITE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MOV_RITE_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.MOV_RITE_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MRGENGIR ="+cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'MOV_RITE','MRGENGIR');
            +i_ccchkf ;
        +" where ";
            +"MRSERIAL = "+cp_ToStrODBC(this.w_CHIAVE);
               )
      else
        update (i_cTable) set;
            MRGENGIR = this.w_PNSERIAL;
            &i_ccchkf. ;
         where;
            MRSERIAL = this.w_CHIAVE;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      if Isalt()
        * --- Aggiorno stato incasso
        gsal_baf(this,this.w_PNSERIAL,"P",Cp_chartodate("  -  -    "),Cp_chartodate("  -  -    "),"A")
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    this.w_MESS1 = "Operazione terminata con successo"
    return


  procedure Page_8
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Scrive Dettaglio P.N. e Saldi
    *     Devo verificare se � lo stesso fornitore
    this.w_CPROWNUM = this.w_CPROWNUM + 1
    this.w_CPROWORD = this.w_CPROWORD + 10
    this.w_MRRIFCON = GENERA.MRRIFCON
    this.w_MRRIFORD = GENERA.MRRIFORD
    this.w_MRRIFNUM = GENERA.MRRIFNUM
    this.w_PNFLSALD = IIF(this.w_PNFLPROV="S", " ", "+")
    this.w_PNCODCON = GENERA.MRCODCON
    if (this.w_APPVAL>0 AND this.oParentObject.w_TIPORI="A") OR (this.w_APPVAL<0 AND this.oParentObject.w_TIPORI="V")
      this.w_PNIMPDAR = ABS(GENERA.IMPORTO)
      this.w_PNIMPAVE = 0
      this.w_SEGNO = "D"
      this.w_PTTOTIMP = Abs(this.w_PNIMPDAR)
    else
      this.w_PNIMPAVE = ABS(GENERA.IMPORTO)
      this.w_PNIMPDAR = 0
      this.w_SEGNO = "A"
      this.w_PTTOTIMP = Abs(this.w_PNIMPAVE)
    endif
    * --- Effettuo la scrittura all'interno del dettaglio di primanota
    * --- Insert into PNT_DETT
    i_nConn=i_TableProp[this.PNT_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PNT_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PNSERIAL"+",CPROWNUM"+",CPROWORD"+",PNTIPCON"+",PNCODCON"+",PNIMPDAR"+",PNIMPAVE"+",PNFLPART"+",PNFLZERO"+",PNDESRIG"+",PNCAURIG"+",PNCODPAG"+",PNFLSALD"+",PNFLSALI"+",PNFLSALF"+",PNLIBGIO"+",PNINICOM"+",PNFINCOM"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PNT_DETT','PNSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'PNT_DETT','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'PNT_DETT','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TIPCON),'PNT_DETT','PNTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCON),'PNT_DETT','PNCODCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNIMPDAR),'PNT_DETT','PNIMPDAR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNIMPAVE),'PNT_DETT','PNIMPAVE');
      +","+cp_NullLink(cp_ToStrODBC("S"),'PNT_DETT','PNFLPART');
      +","+cp_NullLink(cp_ToStrODBC(" "),'PNT_DETT','PNFLZERO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNDESRIG),'PNT_DETT','PNDESRIG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCAU),'PNT_DETT','PNCAURIG');
      +","+cp_NullLink(cp_ToStrODBC(SPACE(5)),'PNT_DETT','PNCODPAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLSALD),'PNT_DETT','PNFLSALD');
      +","+cp_NullLink(cp_ToStrODBC(" "),'PNT_DETT','PNFLSALI');
      +","+cp_NullLink(cp_ToStrODBC(" "),'PNT_DETT','PNFLSALF');
      +","+cp_NullLink(cp_ToStrODBC(0),'PNT_DETT','PNLIBGIO');
      +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'PNT_DETT','PNINICOM');
      +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'PNT_DETT','PNFINCOM');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PNSERIAL',this.w_PNSERIAL,'CPROWNUM',this.w_CPROWNUM,'CPROWORD',this.w_CPROWORD,'PNTIPCON',this.w_TIPCON,'PNCODCON',this.w_PNCODCON,'PNIMPDAR',this.w_PNIMPDAR,'PNIMPAVE',this.w_PNIMPAVE,'PNFLPART',"S",'PNFLZERO'," ",'PNDESRIG',this.w_PNDESRIG,'PNCAURIG',this.w_PNCODCAU,'PNCODPAG',SPACE(5))
      insert into (i_cTable) (PNSERIAL,CPROWNUM,CPROWORD,PNTIPCON,PNCODCON,PNIMPDAR,PNIMPAVE,PNFLPART,PNFLZERO,PNDESRIG,PNCAURIG,PNCODPAG,PNFLSALD,PNFLSALI,PNFLSALF,PNLIBGIO,PNINICOM,PNFINCOM &i_ccchkf. );
         values (;
           this.w_PNSERIAL;
           ,this.w_CPROWNUM;
           ,this.w_CPROWORD;
           ,this.w_TIPCON;
           ,this.w_PNCODCON;
           ,this.w_PNIMPDAR;
           ,this.w_PNIMPAVE;
           ,"S";
           ," ";
           ,this.w_PNDESRIG;
           ,this.w_PNCODCAU;
           ,SPACE(5);
           ,this.w_PNFLSALD;
           ," ";
           ," ";
           ,0;
           ,cp_CharToDate("  -  -  ");
           ,cp_CharToDate("  -  -  ");
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Devo risalire alla partita sospesa creata al momento del ricevimento della fattura.
    *     Devo effettuare questa operazione per poterla chiudere
    if this.w_PNFLPROV<>"S"
      * --- Aggiorna Saldo
      * --- Try
      local bErr_043116A0
      bErr_043116A0=bTrsErr
      this.Try_043116A0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_043116A0
      * --- End
      * --- Write into SALDICON
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.SALDICON_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICON_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"SLDARPER =SLDARPER+ "+cp_ToStrODBC(this.w_PNIMPDAR);
        +",SLAVEPER =SLAVEPER+ "+cp_ToStrODBC(this.w_PNIMPAVE);
            +i_ccchkf ;
        +" where ";
            +"SLTIPCON = "+cp_ToStrODBC(this.w_TIPCON);
            +" and SLCODICE = "+cp_ToStrODBC(this.w_PNCODCON);
            +" and SLCODESE = "+cp_ToStrODBC(this.w_PNCODESE);
               )
      else
        update (i_cTable) set;
            SLDARPER = SLDARPER + this.w_PNIMPDAR;
            ,SLAVEPER = SLAVEPER + this.w_PNIMPAVE;
            &i_ccchkf. ;
         where;
            SLTIPCON = this.w_TIPCON;
            and SLCODICE = this.w_PNCODCON;
            and SLCODESE = this.w_PNCODESE;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    vq_exec("..\RITE\EXE\QUERY\GSRI2BGG.VQR",this,"PARTITE")
    Select PARTITE
    Go Top
    this.w_PTALFDOC = NVL(PARTITE.PTALFDOC,Space(10))
    this.w_PTBANAPP = NVL(PARTITE.PTBANAPP," ")
    this.w_PTBANNOS = NVL(PARTITE.PTBANAPP," ")
    this.w_PTCAOVAL = NVL(PARTITE.PTCAOVAL,0)
    this.w_PTCAOAPE = NVL(PARTITE.PTCAOAPE,0)
    this.w_PTCODVAL = NVL(PARTITE.PTCODVAL," ")
    this.w_PTDATAPE = CP_TODATE(PARTITE.PTDATAPE)
    this.w_PTDATDOC = CP_TODATE(PARTITE.PTDATDOC)
    this.w_PTDATSCA = PARTITE.PTDATSCA
    this.w_PTMODPAG = PARTITE.PTMODPAG
    this.w_PTNUMCOR = NVL(PARTITE.PTNUMCOR," ")
    this.w_PTNUMDOC = NVL(PARTITE.PTNUMDOC,0)
    this.w_PTNUMPAR = PARTITE.PTNUMPAR
    this.w_PTSERRIF = PARTITE.PTSERRIF
    this.w_PTORDRIF = PARTITE.PTORDRIF
    this.w_PTNUMRIF = PARTITE.PTNUMRIF
    this.w_PTCODAGE = PARTITE.PTCODAGE
    if this.w_PTCODVAL<>g_PERVAL
      this.w_PTTOTIMP = cp_ROUND(MON2VAL(this.w_PTTOTIMP,this.w_PTCAOVAL,g_CAOVAL,this.oParentObject.w_REGIDATA,g_PERVAL), g_PERPVL)
    endif
    * --- Scrivo la nuova riga con il flag sospeso
    * --- Insert into PAR_TITE
    i_nConn=i_TableProp[this.PAR_TITE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_TITE_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CPROWNUM"+",PT_SEGNO"+",PTALFDOC"+",PTBANAPP"+",PTBANNOS"+",PTCAOAPE"+",PTCAOVAL"+",PTCODCON"+",PTCODVAL"+",PTDATAPE"+",PTDATDOC"+",PTDATSCA"+",PTFLCRSA"+",PTFLSOSP"+",PTMODPAG"+",PTNUMCOR"+",PTNUMDOC"+",PTNUMPAR"+",PTROWORD"+",PTSERIAL"+",PTTIPCON"+",PTTOTABB"+",PTTOTIMP"+",PTSERRIF"+",PTORDRIF"+",PTNUMRIF"+",PTDATREG"+",PTCODAGE"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(1),'PAR_TITE','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SEGNO),'PAR_TITE','PT_SEGNO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTALFDOC),'PAR_TITE','PTALFDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANAPP),'PAR_TITE','PTBANAPP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANNOS),'PAR_TITE','PTBANNOS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTCAOAPE),'PAR_TITE','PTCAOAPE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTCAOVAL),'PAR_TITE','PTCAOVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCON),'PAR_TITE','PTCODCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTCODVAL),'PAR_TITE','PTCODVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATAPE),'PAR_TITE','PTDATAPE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATDOC),'PAR_TITE','PTDATDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATSCA),'PAR_TITE','PTDATSCA');
      +","+cp_NullLink(cp_ToStrODBC("S"),'PAR_TITE','PTFLCRSA');
      +","+cp_NullLink(cp_ToStrODBC("S"),'PAR_TITE','PTFLSOSP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTMODPAG),'PAR_TITE','PTMODPAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMCOR),'PAR_TITE','PTNUMCOR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMDOC),'PAR_TITE','PTNUMDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMPAR),'PAR_TITE','PTNUMPAR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'PAR_TITE','PTROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PAR_TITE','PTSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TIPCON),'PAR_TITE','PTTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(0),'PAR_TITE','PTTOTABB');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTTOTIMP),'PAR_TITE','PTTOTIMP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTSERRIF),'PAR_TITE','PTSERRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTORDRIF),'PAR_TITE','PTORDRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMRIF),'PAR_TITE','PTNUMRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATREG),'PAR_TITE','PTDATREG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTCODAGE),'PAR_TITE','PTCODAGE');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CPROWNUM',1,'PT_SEGNO',this.w_SEGNO,'PTALFDOC',this.w_PTALFDOC,'PTBANAPP',this.w_PTBANAPP,'PTBANNOS',this.w_PTBANNOS,'PTCAOAPE',this.w_PTCAOAPE,'PTCAOVAL',this.w_PTCAOVAL,'PTCODCON',this.w_PNCODCON,'PTCODVAL',this.w_PTCODVAL,'PTDATAPE',this.w_PTDATAPE,'PTDATDOC',this.w_PTDATDOC,'PTDATSCA',this.w_PTDATSCA)
      insert into (i_cTable) (CPROWNUM,PT_SEGNO,PTALFDOC,PTBANAPP,PTBANNOS,PTCAOAPE,PTCAOVAL,PTCODCON,PTCODVAL,PTDATAPE,PTDATDOC,PTDATSCA,PTFLCRSA,PTFLSOSP,PTMODPAG,PTNUMCOR,PTNUMDOC,PTNUMPAR,PTROWORD,PTSERIAL,PTTIPCON,PTTOTABB,PTTOTIMP,PTSERRIF,PTORDRIF,PTNUMRIF,PTDATREG,PTCODAGE &i_ccchkf. );
         values (;
           1;
           ,this.w_SEGNO;
           ,this.w_PTALFDOC;
           ,this.w_PTBANAPP;
           ,this.w_PTBANNOS;
           ,this.w_PTCAOAPE;
           ,this.w_PTCAOVAL;
           ,this.w_PNCODCON;
           ,this.w_PTCODVAL;
           ,this.w_PTDATAPE;
           ,this.w_PTDATDOC;
           ,this.w_PTDATSCA;
           ,"S";
           ,"S";
           ,this.w_PTMODPAG;
           ,this.w_PTNUMCOR;
           ,this.w_PTNUMDOC;
           ,this.w_PTNUMPAR;
           ,this.w_CPROWNUM;
           ,this.w_PNSERIAL;
           ,this.w_TIPCON;
           ,0;
           ,this.w_PTTOTIMP;
           ,this.w_PTSERRIF;
           ,this.w_PTORDRIF;
           ,this.w_PTNUMRIF;
           ,this.w_PNDATREG;
           ,this.w_PTCODAGE;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    Select Genera
  endproc
  proc Try_043116A0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDICON
    i_nConn=i_TableProp[this.SALDICON_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDICON_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SLTIPCON"+",SLCODICE"+",SLCODESE"+",SLDARPRO"+",SLAVEPRO"+",SLDARPER"+",SLAVEPER"+",SLDARINI"+",SLAVEINI"+",SLDARFIN"+",SLAVEFIN"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_TIPCON),'SALDICON','SLTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCON),'SALDICON','SLCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODESE),'SALDICON','SLCODESE');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARPRO');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEPRO');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARPER');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEPER');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARINI');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEINI');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARFIN');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEFIN');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SLTIPCON',this.w_TIPCON,'SLCODICE',this.w_PNCODCON,'SLCODESE',this.w_PNCODESE,'SLDARPRO',0,'SLAVEPRO',0,'SLDARPER',0,'SLAVEPER',0,'SLDARINI',0,'SLAVEINI',0,'SLDARFIN',0,'SLAVEFIN',0)
      insert into (i_cTable) (SLTIPCON,SLCODICE,SLCODESE,SLDARPRO,SLAVEPRO,SLDARPER,SLAVEPER,SLDARINI,SLAVEINI,SLDARFIN,SLAVEFIN &i_ccchkf. );
         values (;
           this.w_TIPCON;
           ,this.w_PNCODCON;
           ,this.w_PNCODESE;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Page_9
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_PNCODCAU = this.w_CODCAU
    this.w_PNDESSUP = this.oParentObject.w_DESCRIZI
    * --- Read from CAU_CONT
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CAU_CONT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAU_CONT_idx,2],.t.,this.CAU_CONT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CCDESSUP,CCDESRIG"+;
        " from "+i_cTable+" CAU_CONT where ";
            +"CCCODICE = "+cp_ToStrODBC(this.w_PNCODCAU);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CCDESSUP,CCDESRIG;
        from (i_cTable) where;
            CCCODICE = this.w_PNCODCAU;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CCDESSUP = NVL(cp_ToDate(_read_.CCDESSUP),cp_NullValue(_read_.CCDESSUP))
      this.w_CCDESRIG = NVL(cp_ToDate(_read_.CCDESRIG),cp_NullValue(_read_.CCDESRIG))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if Empty(this.w_PNDESSUP) and (Not Empty(this.w_CCDESSUP) or Not Empty(this.w_CCDESRIG))
      * --- Array elenco parametri per descrizioni di riga e testata
       
 DIMENSION ARPARAM[12,2] 
 ARPARAM[1,1]="NUMDOC" 
 ARPARAM[1,2]=ALLTRIM(STR(this.w_PNNUMDOC)) 
 ARPARAM[2,1]="ALFDOC" 
 ARPARAM[2,2]=this.w_PNALFDOC 
 ARPARAM[3,1]="DATDOC" 
 ARPARAM[3,2]=DTOC(this.w_PNDATDOC) 
 ARPARAM[4,1]="ALFPRO" 
 ARPARAM[4,2]="" 
 ARPARAM[5,1]="DATREG" 
 ARPARAM[5,2]=DTOC(this.w_PNDATREG) 
 ARPARAM[6,1]="CODCON" 
 ARPARAM[6,2]="" 
 ARPARAM[7,1]="ELEDOC" 
 ARPARAM[7,2]="" 
 ARPARAM[8,1]="CODAGE" 
 ARPARAM[8,2]="" 
 ARPARAM[9,1]="NUMPRO" 
 ARPARAM[9,2]="" 
 ARPARAM[10,1]="NUMPAR" 
 ARPARAM[10,2]="" 
 ARPARAM[11,1]="DATSCA" 
 ARPARAM[11,2]="" 
 ARPARAM[12,1]="TIPCON" 
 ARPARAM[12,2]=""
      this.w_PNDESSUP = CALDESPA(this.w_CCDESSUP,@ARPARAM)
    endif
    if Empty(this.w_PNDESRIG) and Not Empty(this.w_CCDESRIG)
      this.w_PNDESRIG = CALDESPA(this.w_CCDESRIG,@ARPARAM)
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,9)]
    this.cWorkTables[1]='CAU_CONT'
    this.cWorkTables[2]='VALUTE'
    this.cWorkTables[3]='PNT_MAST'
    this.cWorkTables[4]='PNT_DETT'
    this.cWorkTables[5]='SALDICON'
    this.cWorkTables[6]='AZIENDA'
    this.cWorkTables[7]='MOV_RITE'
    this.cWorkTables[8]='TAB_RITE'
    this.cWorkTables[9]='PAR_TITE'
    return(this.OpenAllTables(9))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
