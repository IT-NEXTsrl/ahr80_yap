* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_bst                                                        *
*              QUADRO 770/ST                                                   *
*                                                                              *
*      Author: Zucchetti spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][98]                                                      *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-05-19                                                      *
* Last revis.: 2013-07-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsri_bst",oParentObject)
return(i_retval)

define class tgsri_bst as StdBatch
  * --- Local variables
  w_CODTRISST = space(5)
  w_MESSEXIT = space(100)
  w_SERIAL = space(10)
  w_NUMEREG = 0
  w_CODESE = space(4)
  w_DATAREG = ctod("  /  /  ")
  w_OLDSERIAL = space(10)
  w_CAUPRE = space(1)
  w_AZCOFAZI = space(16)
  w_770MESE = 0
  w_770CODCO = space(3)
  w_770VP__N = space(20)
  w_770ANNO = 0
  w_770DATVE = ctod("  /  /  ")
  w_770VPEVE = space(1)
  w_770SOMTO = 0
  w_770ABI = space(5)
  w_770VPART = 0
  w_770IMPVE = 0
  w_770CAB = space(5)
  w_770VPCOD = 0
  w_770IMPIN = 0
  w_770CODTR = space(5)
  w_770VPSTATUS = space(1)
  w_770VALVE = space(3)
  w_770VPRITE = 0
  w_AVVISO = .f.
  w_770TIPVE = space(1)
  w_770VPRITC = 0
  w_770MODVE = space(1)
  w_MESS = space(50)
  w_APPO = space(100)
  w_MESSAGE = space(100)
  w_VPREGAGE = space(1)
  w_770RAVVEDIM = space(1)
  w_PRIMOAVVISO = .f.
  * --- WorkFile variables
  AZIENDA_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questo batch serve per convertire i movimenti ritenute in base alla maschera
    * --- di stampa del quadro 'QUADROST'
    * --- Variabili utilizzate come filtro sul cursore RITENUTE
    * --- Variabili di appoggio per il cursore 'QUADROST'
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZCOFAZI"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZCOFAZI;
        from (i_cTable) where;
            AZCODAZI = i_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_AZCOFAZI = NVL(cp_ToDate(_read_.AZCOFAZI),cp_NullValue(_read_.AZCOFAZI))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Variabile per impedire la visualizzazione del messaggio di avviso quando sono presenti
    *     ritenute abbinate in distinta con condizioni diverse.
    this.w_PRIMOAVVISO = .F.
    this.w_MESSEXIT = "Sono state abbinate ritenute con condizioni diverse alla solita distinta di versamento%0Impossibile compilare dati quadro ST"
    * --- Controllo che le data di selezione siano all'interno dello stesso anno
    if STR(YEAR(this.oParentObject.w_DATFIN),4,0)<>STR(YEAR(this.oParentObject.w_DATINI),4,0)
      this.w_MESS = "L'intervallo di date deve essere interno ad un anno solare%0"
      ah_ERRORMSG(this.w_MESS,48)
      i_retcode = 'stop'
      return
    endif
    vq_exec("..\RITE\EXE\QUERY\GSRI5SSC.VQR",this,"DISTINTE")
    Select DISTINTE
    if RECCOUNT() > 0 and (TYPE("this.oparentObject.OparentObject.w_QST1")="C" and (this.oparentObject.OparentObject.w_QST1="1" or this.oparentObject.OparentObject.w_QST14="1") or lower(this.oParentObject.Class)="tgsri4sst")
      Select DISTINTE
      Go Top
      SCAN
      this.w_NUMEREG = NVL(VPNUMREG,000000)
      this.w_CODESE = NVL(VPCODESE,"")
      this.w_DATAREG = NVL(VPDATREG,Cp_CharToDate(" - - "))
      if NOT (this.w_PRIMOAVVISO)
        create cursor ELEDISTINTE (NUMEREG N(6), CODESE C(4), DATREG D)
        ah_ERRORMSG(this.w_MESSEXIT,48)
        this.w_PRIMOAVVISO = .T.
      endif
      insert into ELEDISTINTE (NUMEREG,CODESE,DATREG) values (this.w_NUMEREG,this.w_CODESE,this.w_DATAREG)
      Select DISTINTE
      ENDSCAN
      if lower(this.oParentObject.Class) = "tgsri3bft" or lower(this.oParentObject.Class) = "tgsri4bft" or lower(this.oParentObject.Class) = "tgsri5bft" or like("tgsut?bft",lower(this.oParentObject.Class))
        this.oParentObject.w_SOSPENDI = .T.
        Select * FROM ELEDISTINTE Order By NUMEREG into Cursor __TMP__
        CP_CHPRN("..\RITE\EXE\QUERY\GSRI4SST.FRX","",this.oParentObject)
      endif
      if lower(this.oParentObject.Class) = "tgsri4sst" OR lower(this.oParentObject.Class) = "tgsri2sst" OR lower(this.oParentObject.Class) = "tgsri3sst"
        * --- Lancio il report di stampa per l'anno 2002/2003
        Select * FROM ELEDISTINTE Order By NUMEREG into Cursor __TMP__
        CP_CHPRN("..\RITE\EXE\QUERY\GSRI4SST.FRX","",this.oParentObject)
      endif
      if used("ELEDISTINTE")
        select ("ELEDISTINTE")
        USE
      endif
    endif
    if used("DISTINTE")
      select ("DISTINTE")
      USE
    endif
    if NOT (this.w_PRIMOAVVISO)
      this.w_AVVISO = .T.
      this.w_MESSAGE = "Esistono distinte provvisorie, tali distinte non verranno considerate"
      * --- Lancio la query che va a leggere nei movimenti ritenute
      vq_exec("..\RITE\EXE\QUERY\GSRI3SSC.VQR",this,"RITE")
      * --- Controllo se esiste almeno un record
      if RECCOUNT("RITE")>0
        * --- Per lo stesso periodo di riferimento (mese ed anno) e lo stesso tributo possono
        *     esistere pi� versamenti.
        *     Il totale delle ritenute effettuate, il totale importo versato, e gli interessi devono essere
        *     sommati mentre gli altri dati devono riguardare l'ultimo versamento effettuato.(Data Versamento pi� prossima alla fine del mese)
        * --- Creo un cursore denominato 'QUADROST' che conterr� i dati di stampa.
        CREATE CURSOR QUADROST (MESE N(2,0), ANNO N(4,0) , SOMTOTA N(18,4), VPIMPVER N(18,4), VPIMPINT N(18,4), VPVALVER C(3),;
         VPTIPVER C(1),VPMODVER C(1),VPCODCON C(3), VPDATVER D(8),VPCODABI C(5),VPCODCAB C(5),MRCODTRI C(5),VPRITECC N(18,4),;
        VPRITCOM N(18,4),VP__NOTE C(20),VPEVEECC C(1),VPARTICO N(3,0),VPCODREG N(2,0),VPREGAGE C(1),VPCAUPRE C(1),VPRAVOPE C(1))
        * --- Seleziono il cursore contenente i versamenti
        Select RITE
        GO TOP
        this.w_CODTRISST = NVL(MRCODTRI,SPACE(5))
        this.w_OLDSERIAL = NVL(VPSERIAL,SPACE(10))
        this.w_CAUPRE = NVL(MRCAURIT," ")
        SCAN FOR NOT EMPTY(NVL(MRCODTRI,SPACE(5)))
        if VPSTATUS="D"
          * --- Controllo se sono variati il codice tributo oppure il mese oppure l'anno
          if this.w_CODTRISST<>MRCODTRI or this.w_OLDSERIAL<>VPSERIAL
            * --- Effettuo la scrittura all'interno del cursore 'QUADROST'
            if this.w_770VPSTATUS="D"
              INSERT INTO QUADROST (MESE,ANNO,SOMTOTA,VPIMPVER,VPIMPINT,VPVALVER,;
              VPTIPVER,VPMODVER,VPCODCON,VPDATVER,VPCODABI,VPCODCAB,MRCODTRI,VPRITECC,;
              VPRITCOM,VP__NOTE,VPEVEECC,VPARTICO,VPCODREG,VPREGAGE,VPCAUPRE,VPRAVOPE);
              VALUES (this.w_770MESE,this.w_770ANNO,this.w_770SOMTO,this.w_770IMPVE,this.w_770IMPIN,this.w_770VALVE,;
              this.w_770TIPVE,this.w_770MODVE,this.w_770CODCO,this.w_770DATVE,this.w_770ABI,this.w_770CAB,this.w_770CODTR,;
              this.w_770VPRITE,this.w_770VPRITC,this.w_770VP__N,this.w_770VPEVE,this.w_770VPART,this.w_770VPCOD,this.w_VPREGAGE,this.w_CAUPRE,this.w_770RAVVEDIM)
            endif
            * --- Riazzero i Totalizzatori
            this.w_770SOMTO = 0
            this.w_770IMPVE = 0
            this.w_770IMPIN = 0
            this.w_770VPART = NVL(VPARTICO,0)
            this.w_770VPCOD = NVL(VPCODREG,0)
            this.w_770CODTR = NVL(MRCODTRI,"")
            this.w_770VP__N = NVL(VP__NOTE,"")
            do case
              case YEAR(this.oParentObject.w_DATINI) = 2004 OR YEAR(this.oParentObject.w_DATINI) = 2005
                this.w_770MESE = iif("A"$this.w_770VP__N OR "B"$this.w_770VP__N OR "M"$this.w_770VP__N OR "P"$this.w_770VP__N OR "N"$this.w_770VP__N,12,NVL(MESE,0))
                this.w_770ANNO = iif("A"$this.w_770VP__N OR "B"$this.w_770VP__N OR "M"$this.w_770VP__N OR "P"$this.w_770VP__N OR "N"$this.w_770VP__N,YEAR(this.oParentObject.w_DATFIN),NVL(ANNO,0))
              case YEAR(this.oParentObject.w_DATINI) = 2006 OR YEAR(this.oParentObject.w_DATINI) = 2007
                this.w_770MESE = iif("A"$this.w_770VP__N OR "B"$this.w_770VP__N OR "M"$this.w_770VP__N OR "D"$this.w_770VP__N OR "E"$this.w_770VP__N OR ALLTRIM(this.w_770CODTR)="1713",12,NVL(MESE,0))
                this.w_770ANNO = iif("A"$this.w_770VP__N OR "B"$this.w_770VP__N OR "M"$this.w_770VP__N OR "D"$this.w_770VP__N OR "E"$this.w_770VP__N OR ALLTRIM(this.w_770CODTR)="1713",YEAR(this.oParentObject.w_DATFIN),NVL(ANNO,0))
              otherwise
                this.w_770MESE = iif("A"$this.w_770VP__N OR "B"$this.w_770VP__N OR "D"$this.w_770VP__N OR "E"$this.w_770VP__N OR ALLTRIM(this.w_770CODTR)="1713",12,NVL(MESE,0))
                this.w_770ANNO = iif("A"$this.w_770VP__N OR "B"$this.w_770VP__N OR "D"$this.w_770VP__N OR "E"$this.w_770VP__N OR ALLTRIM(this.w_770CODTR)="1713",YEAR(this.oParentObject.w_DATFIN),NVL(ANNO,0))
            endcase
            this.w_770VALVE = NVL(VPVALVER,"")
            this.w_770TIPVE = NVL(VPTIPVER,"")
            this.w_770MODVE = NVL(VPMODVER,"")
            this.w_770CODCO = NVL(VPCODCON,"")
            this.w_770DATVE = CP_TODATE(VPDATVER)
            this.w_770ABI = NVL(VPCODABI,"")
            this.w_770CAB = NVL(VPCODCAB,"")
            this.w_770VPEVE = NVL(VPEVEECC,"")
            this.w_770VPSTATUS = NVL(VPSTATUS,"")
            this.w_VPREGAGE = NVL(VPREGAGE,"")
            this.w_770RAVVEDIM = NVL(VPRAVOPE,"")
            * --- Controllo se la valuta del movimento ritenuta � uguale alla valuta scelta sulla maschera di stampa.
            if MRCODVAL<>this.oParentObject.w_CODVAL
              this.w_770SOMTO = cp_ROUND(VALCAM(NVL(SOMTOTA,0), MRCODVAL, this.oParentObject.w_CODVAL, i_DATSYS, 0), this.oParentObject.w_DECIMI)
              this.w_770IMPVE = cp_ROUND(VALCAM(NVL(VPIMPVER,0), VPVALVER, this.oParentObject.w_CODVAL, i_DATSYS, 0), this.oParentObject.w_DECIMI)
            else
              this.w_770SOMTO = SOMTOTA
              this.w_770IMPVE = VPIMPVER
            endif
            * --- Controllo se sono presenti pi� righe di un unico versamento
            * --- Controllo se la valuta del versamento IRPEF � uguale alla valuta scelta sulla maschera di stampa.
            if VPSERIAL<>this.w_SERIAL
              if VPVALVER<>this.oParentObject.w_CODVAL
                this.w_770VPRITE = cp_ROUND(VALCAM(NVL(VPRITECC,0), VPVALVER, this.oParentObject.w_CODVAL, i_DATSYS, 0), this.oParentObject.w_DECIMI)
                this.w_770VPRITC = cp_ROUND(VALCAM(NVL(VPRITCOM,0), VPVALVER, this.oParentObject.w_CODVAL, i_DATSYS, 0), this.oParentObject.w_DECIMI)
                this.w_770IMPIN = cp_ROUND(VALCAM(NVL(VPIMPINT,0), VPVALVER, this.oParentObject.w_CODVAL, i_DATSYS, 0), this.oParentObject.w_DECIMI)
              else
                this.w_770IMPIN = VPIMPINT
                this.w_770VPRITE = NVL(VPRITECC,"")
                this.w_770VPRITC = NVL(VPRITCOM,"")
              endif
            endif
          else
            * --- Preparo le variabili da inserire all'interno del cursore 'QUADROST'
            this.w_770CODTR = NVL(MRCODTRI,"")
            this.w_770VP__N = NVL(VP__NOTE,"")
            do case
              case YEAR(this.oParentObject.w_DATINI) = 2004 OR YEAR(this.oParentObject.w_DATINI) = 2005
                this.w_770MESE = iif("A"$this.w_770VP__N OR "B"$this.w_770VP__N OR "M"$this.w_770VP__N OR "P"$this.w_770VP__N OR "N"$this.w_770VP__N,12,NVL(MESE,0))
                this.w_770ANNO = iif("A"$this.w_770VP__N OR "B"$this.w_770VP__N OR "M"$this.w_770VP__N OR "P"$this.w_770VP__N OR "N"$this.w_770VP__N,VAL(this.oParentObject.w_ANNO),NVL(ANNO,0))
              case YEAR(this.oParentObject.w_DATINI) = 2006 OR YEAR(this.oParentObject.w_DATINI) = 2007
                this.w_770MESE = iif("A"$this.w_770VP__N OR "B"$this.w_770VP__N OR "M"$this.w_770VP__N OR "D"$this.w_770VP__N OR "E"$this.w_770VP__N OR ALLTRIM(this.w_770CODTR)="1713",12,NVL(MESE,0))
                this.w_770ANNO = iif("A"$this.w_770VP__N OR "B"$this.w_770VP__N OR "M"$this.w_770VP__N OR "D"$this.w_770VP__N OR "E"$this.w_770VP__N OR ALLTRIM(this.w_770CODTR)="1713",VAL(this.oParentObject.w_ANNO),NVL(ANNO,0))
              otherwise
                this.w_770MESE = iif("A"$this.w_770VP__N OR "B"$this.w_770VP__N OR "D"$this.w_770VP__N OR "E"$this.w_770VP__N OR ALLTRIM(this.w_770CODTR)="1713",12,NVL(MESE,0))
                this.w_770ANNO = iif("A"$this.w_770VP__N OR "B"$this.w_770VP__N OR "D"$this.w_770VP__N OR "E"$this.w_770VP__N OR ALLTRIM(this.w_770CODTR)="1713",YEAR(this.oParentObject.w_DATFIN),NVL(ANNO,0))
            endcase
            this.w_770VALVE = NVL(VPVALVER,"")
            this.w_770TIPVE = NVL(VPTIPVER,"")
            this.w_770MODVE = NVL(VPMODVER,"")
            this.w_770CODCO = NVL(VPCODCON,"")
            this.w_770DATVE = CP_TODATE(VPDATVER)
            this.w_770ABI = NVL(VPCODABI,"")
            this.w_770CAB = NVL(VPCODCAB,"")
            this.w_770VPEVE = NVL(VPEVEECC,"")
            this.w_770VPART = NVL(VPARTICO,0)
            this.w_770VPCOD = NVL(VPCODREG,0)
            this.w_770VPSTATUS = NVL(VPSTATUS,"")
            this.w_VPREGAGE = NVL(VPREGAGE,"")
            this.w_770RAVVEDIM = NVL(VPRAVOPE,"")
            if MRCODVAL<>this.oParentObject.w_CODVAL
              this.w_770SOMTO = this.w_770SOMTO+cp_ROUND(VALCAM(NVL(SOMTOTA,0), MRCODVAL, this.oParentObject.w_CODVAL, i_DATSYS, 0), this.oParentObject.w_DECIMI)
              this.w_770IMPVE = cp_ROUND(VALCAM(NVL(VPIMPVER,0), VPVALVER, this.oParentObject.w_CODVAL, i_DATSYS, 0), this.oParentObject.w_DECIMI)
            else
              this.w_770SOMTO = (this.w_770SOMTO+SOMTOTA)
              this.w_770IMPVE = VPIMPVER
            endif
            if VPSERIAL<>this.w_SERIAL
              * --- Controllo se la valuta del versamento IRPEF � uguale alla valuta scelta sulla maschera di stampa.
              if VPVALVER<>this.oParentObject.w_CODVAL
                this.w_770IMPIN = cp_ROUND(VALCAM(NVL(VPIMPINT,0), VPVALVER, this.oParentObject.w_CODVAL, i_DATSYS, 0), this.oParentObject.w_DECIMI)
                this.w_770VPRITE = cp_ROUND(VALCAM(NVL(VPRITECC,0), VPVALVER, this.oParentObject.w_CODVAL, i_DATSYS, 0), this.oParentObject.w_DECIMI)
                this.w_770VPRITC = cp_ROUND(VALCAM(NVL(VPRITCOM,0), VPVALVER, this.oParentObject.w_CODVAL, i_DATSYS, 0), this.oParentObject.w_DECIMI)
              else
                this.w_770IMPIN = VPIMPINT
                this.w_770VPRITE = NVL(VPRITECC,"")
                this.w_770VPRITC = NVL(VPRITCOM,"")
              endif
            endif
          endif
        else
          if this.w_AVVISO
            ah_ERRORMSG(this.w_MESSAGE,48)
          endif
          this.w_AVVISO = .F.
        endif
        this.w_CODTRISST = NVL(MRCODTRI,SPACE(5))
        this.w_SERIAL = NVL(VPSERIAL,"")
        this.w_CAUPRE = NVL(MRCAURIT," ")
        this.w_OLDSERIAL = NVL(VPSERIAL,SPACE(10))
        ENDSCAN
        * --- Scrivo l'ultimo record di stampa
        * --- Effettuo la scrittura all'interno del cursore 'QUADROST'
        if this.w_770VPSTATUS="D"
          INSERT INTO QUADROST (MESE,ANNO,SOMTOTA,VPIMPVER,VPIMPINT,VPVALVER,;
          VPTIPVER,VPMODVER,VPCODCON,VPDATVER,VPCODABI,VPCODCAB,MRCODTRI,VPRITECC,;
          VPRITCOM,VP__NOTE,VPEVEECC,VPARTICO,VPCODREG,VPREGAGE,VPCAUPRE,VPRAVOPE);
          VALUES (this.w_770MESE,this.w_770ANNO,this.w_770SOMTO,this.w_770IMPVE,this.w_770IMPIN,this.w_770VALVE,;
          this.w_770TIPVE,this.w_770MODVE,this.w_770CODCO,this.w_770DATVE,this.w_770ABI,this.w_770CAB,this.w_770CODTR,;
          this.w_770VPRITE,this.w_770VPRITC,this.w_770VP__N,this.w_770VPEVE,this.w_770VPART,this.w_770VPCOD,this.w_VPREGAGE,this.w_CAUPRE,this.w_770RAVVEDIM)
        else
          if this.w_AVVISO
            ah_ERRORMSG(this.w_MESSAGE,48)
          endif
        endif
        * --- Riazzero i Totalizzatori
        this.w_770SOMTO = 0
        this.w_770IMPVE = 0
        this.w_770IMPIN = 0
        Select * FROM QUADROST Order By MESE,ANNO,MRCODTRI into Cursor __TMP__
        * --- Generazione File Telematico - Modello 770
        if lower(this.oParentObject.Class) = "tgsri5bft" or like("tgsut?bft",lower(this.oParentObject.Class)) or like("tgsri?bsp",lower(this.oParentObject.Class)) or like("tgsri??bsp",lower(this.oParentObject.Class))
          select * from __tmp__ into cursor RITE3
        endif
        * --- Passo le seguenti variabili al report
        L_CODVAL=this.oParentObject.w_CODVAL
        L_DATINI=this.oParentObject.w_DATINI
        L_VALUTA=this.oParentObject.w_VALUTA
        L_DECIMI=this.oParentObject.w_DECIMI
        L_DATFIN=this.oParentObject.w_DATFIN
        L_AZCOFAZI=this.w_AZCOFAZI
        * --- Generazione File Telematico
        if lower(this.oParentObject.Class) = "tgsri_sst"
           
 WRCURSOR("__TMP__") 
 Delete from __TMP__ where VPREGAGE = "S" 
 Select __TMP__ 
 Go Top
          * --- Lancio il report di stampa per l'anno 2001
          CP_CHPRN("..\RITE\EXE\QUERY\GSRI3SSC.FRX","",this.oParentObject)
        endif
        if lower(this.oParentObject.Class) = "tgsri1sst" or lower(this.oParentObject.Class) = "tgsri2sst"
          Select __TMP__
          locate for SOMTOTA < 0 or VPRITECC < 0 or VPRITCOM < 0 or SOMTOTA-VPRITECC-VPRITCOM < 0 or VPIMPINT < 0
          if found()
            AH_ERRORMSG("Attenzione: presenza di importi negativi nel prospetto ST",48)
          endif
          * --- Lancio il report di stampa per l'anno 2002
          if this.w_PRIMOAVVISO
            Select * FROM ELEDISTINTE Order By NUMEREG into Cursor __TMP__
            CP_CHPRN("..\RITE\EXE\QUERY\GSRI4SST.FRX","",this.oParentObject)
          else
            CP_CHPRN("..\RITE\EXE\QUERY\GSRI3SST.FRX","",this.oParentObject)
          endif
        endif
        if lower(this.oParentObject.Class) = "tgsri3sst" or lower(this.oParentObject.Class) = "tgsri4sst" 
          Select __TMP__
          locate for VPCAUPRE # "X" and VPCAUPRE # "Y" and (SOMTOTA < 0 or VPRITECC < 0 or VPRITCOM < 0 or SOMTOTA-VPRITECC-VPRITCOM < 0 or VPIMPINT < 0)
          if found()
            AH_ERRORMSG("Attenzione: presenza di importi negativi nel prospetto ST",48)
          endif
          * --- Lancio il report di stampa per l'anno 2003
          if this.w_PRIMOAVVISO
            Select * FROM ELEDISTINTE Order By NUMEREG into Cursor __TMP__
            CP_CHPRN("..\RITE\EXE\QUERY\GSRI4SST.FRX","",this.oParentObject)
          else
             
 WRCURSOR("__TMP__") 
 Delete from __TMP__ where VPREGAGE = "S" 
 Select __TMP__ 
 Go Top
            do case
              case YEAR(this.oParentObject.w_DATINI) = 2004
                CP_CHPRN("..\RITE\EXE\QUERY\GSRI5SST.FRX","",this.oParentObject)
              case YEAR(this.oParentObject.w_DATINI) = 2005
                CP_CHPRN("..\RITE\EXE\QUERY\GSRI6SST.FRX","",this.oParentObject)
              case YEAR(this.oParentObject.w_DATINI) = 2006
                CP_CHPRN("..\RITE\EXE\QUERY\GSRI7SST.FRX","",this.oParentObject)
              case YEAR(this.oParentObject.w_DATINI) = 2007
                CP_CHPRN("..\RITE\EXE\QUERY\GSRI8SST.FRX","",this.oParentObject)
              case YEAR(this.oParentObject.w_DATINI) >= 2008 AND YEAR(this.oParentObject.w_DATINI) < 2012
                CP_CHPRN("..\RITE\EXE\QUERY\GSRI9SST.FRX","",this.oParentObject)
              case YEAR(this.oParentObject.w_DATINI) >= 2012
                CP_CHPRN("..\RITE\EXE\QUERY\GSRI10SST.FRX","",this.oParentObject)
            endcase
          endif
        endif
      else
        this.w_APPO = "Per l'intervallo selezionato non esistono movimenti da stampare"
        ah_ERRORMSG(this.w_APPO,48)
      endif
    endif
    if used("RITE")
      select ("RITE")
      USE
    endif
    if used("QUADROST")
      select ("QUADROST")
      USE
    endif
    if used("__TMP__")
      select ("__TMP__")
      USE
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='AZIENDA'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
