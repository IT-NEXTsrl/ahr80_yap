* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_kde                                                        *
*              Manutenzione dati certificazione unica                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2015-12-28                                                      *
* Last revis.: 2018-01-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsri_kde",oParentObject))

* --- Class definition
define class tgsri_kde as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 794
  Height = 532+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2018-01-22"
  HelpContextID=82441833
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=39

  * --- Constant Properties
  _IDX = 0
  CONTI_IDX = 0
  cPrg = "gsri_kde"
  cComment = "Manutenzione dati certificazione unica"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CHTIPCON = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_CODICEFIS = space(16)
  w_SERIALE_CERT = space(10)
  w_CUTIPCER = space(5)
  w_CONFERMA = .F.
  w_CH__ANNO = 0
  w_CHCERDEF = space(1)
  w_CHDAVERI = space(1)
  w_CHSCLGEN = space(1)
  w_CHPEREST = space(1)
  w_CHSCHUMA = space(1)
  w_CHTIPOPE = space(1)
  w_CHTIPDAT = space(1)
  w_CHCAUPRE = space(2)
  w_CHTIPREC = space(1)
  w_CHCODCON = space(15)
  w_CHCODCONF = space(15)
  w_CHCODFIS = space(16)
  w_CHCOFISC = space(25)
  w_CHDATCOM = space(1)
  o_CHDATCOM = space(1)
  w_CHDESCRI = space(60)
  w_CHDESCRIF = space(60)
  w_CUDATCER = ctod('  /  /  ')
  w_TIPO_COMU = space(1)
  w_CHSERIAL = space(10)
  w_CHPROTEC = space(17)
  w_CHPRODOC = 0
  w_XCHK = 0
  w_GEN = space(1)
  w_ANRITENU = space(1)
  w_ANRITENUF = space(1)
  w_TIPREC = space(1)
  w_CHDATEST = space(10)
  w_TIPOPE = space(1)
  w_DATRETT = space(10)
  w_VAR_WORK = space(10)
  w_NOMEFILE = space(254)
  w_CERTMOD = space(1)
  w_ZoomAg = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsri_kdePag1","gsri_kde",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Filtri")
      .Pages(2).addobject("oPag","tgsri_kdePag2","gsri_kde",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Visualizzazione")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCH__ANNO_1_7
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomAg = this.oPgFrm.Pages(2).oPag.ZoomAg
    DoDefault()
    proc Destroy()
      this.w_ZoomAg = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='CONTI'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CHTIPCON=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_CODICEFIS=space(16)
      .w_SERIALE_CERT=space(10)
      .w_CUTIPCER=space(5)
      .w_CONFERMA=.f.
      .w_CH__ANNO=0
      .w_CHCERDEF=space(1)
      .w_CHDAVERI=space(1)
      .w_CHSCLGEN=space(1)
      .w_CHPEREST=space(1)
      .w_CHSCHUMA=space(1)
      .w_CHTIPOPE=space(1)
      .w_CHTIPDAT=space(1)
      .w_CHCAUPRE=space(2)
      .w_CHTIPREC=space(1)
      .w_CHCODCON=space(15)
      .w_CHCODCONF=space(15)
      .w_CHCODFIS=space(16)
      .w_CHCOFISC=space(25)
      .w_CHDATCOM=space(1)
      .w_CHDESCRI=space(60)
      .w_CHDESCRIF=space(60)
      .w_CUDATCER=ctod("  /  /  ")
      .w_TIPO_COMU=space(1)
      .w_CHSERIAL=space(10)
      .w_CHPROTEC=space(17)
      .w_CHPRODOC=0
      .w_XCHK=0
      .w_GEN=space(1)
      .w_ANRITENU=space(1)
      .w_ANRITENUF=space(1)
      .w_TIPREC=space(1)
      .w_CHDATEST=space(10)
      .w_TIPOPE=space(1)
      .w_DATRETT=space(10)
      .w_VAR_WORK=space(10)
      .w_NOMEFILE=space(254)
      .w_CERTMOD=space(1)
        .w_CHTIPCON = 'F'
        .w_OBTEST = CTOD('01-01-1900')
        .w_CODICEFIS = Space(16)
        .w_SERIALE_CERT = Space(10)
        .w_CUTIPCER = 'CUR16'
          .DoRTCalc(6,6,.f.)
        .w_CH__ANNO = year(i_DATSYS)-1
        .w_CHCERDEF = 'T'
        .w_CHDAVERI = 'T'
        .w_CHSCLGEN = 'T'
        .w_CHPEREST = 'T'
        .w_CHSCHUMA = 'T'
        .w_CHTIPOPE = 'T'
        .w_CHTIPDAT = 'T'
        .w_CHCAUPRE = 'TT'
        .w_CHTIPREC = 'T'
        .DoRTCalc(17,17,.f.)
        if not(empty(.w_CHCODCON))
          .link_1_17('Full')
        endif
        .DoRTCalc(18,18,.f.)
        if not(empty(.w_CHCODCONF))
          .link_1_18('Full')
        endif
          .DoRTCalc(19,20,.f.)
        .w_CHDATCOM = 'T'
          .DoRTCalc(22,23,.f.)
        .w_CUDATCER = {}
        .w_TIPO_COMU = Space(1)
      .oPgFrm.Page2.oPag.ZoomAg.Calculate()
        .w_CHSERIAL = NVL(.w_ZoomAg.getVar('CHSERIAL'), ' ')
        .w_CHPROTEC = NVL(.w_ZoomAg.getVar('CHPROTEC'), ' ')
        .w_CHPRODOC = NVL(.w_ZoomAg.getVar('CHPRODOC'),0)
        .w_XCHK = Nvl(.w_ZoomAg.getVar('XCHK'),0)
        .w_GEN = Nvl(.w_ZoomAg.getVar('GEN'),'N')
          .DoRTCalc(31,32,.f.)
        .w_TIPREC = NVL(.w_ZoomAg.getVar('TIPREC'), ' ')
        .w_CHDATEST = NVL(.w_ZoomAg.getVar('Chdatest'), ' ')
        .w_TIPOPE = NVL(.w_ZoomAg.getVar('TIPOPE'), ' ')
        .w_DATRETT = NVL(.w_ZoomAg.getVar('DATRETT'), ' ')
          .DoRTCalc(37,38,.f.)
        .w_CERTMOD = NVL(.w_ZoomAg.getVar('CERTMOD'), ' ')
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_22.enabled = this.oPgFrm.Page1.oPag.oBtn_1_22.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_42.enabled = this.oPgFrm.Page1.oPag.oBtn_1_42.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_43.enabled = this.oPgFrm.Page1.oPag.oBtn_1_43.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
        if .o_CHDATCOM<>.w_CHDATCOM
            .w_CODICEFIS = Space(16)
        endif
        if .o_CHDATCOM<>.w_CHDATCOM
            .w_SERIALE_CERT = Space(10)
        endif
        .DoRTCalc(5,23,.t.)
        if .o_CHDATCOM<>.w_CHDATCOM
            .w_CUDATCER = {}
        endif
        if .o_CHDATCOM<>.w_CHDATCOM
            .w_TIPO_COMU = Space(1)
        endif
        .oPgFrm.Page2.oPag.ZoomAg.Calculate()
            .w_CHSERIAL = NVL(.w_ZoomAg.getVar('CHSERIAL'), ' ')
            .w_CHPROTEC = NVL(.w_ZoomAg.getVar('CHPROTEC'), ' ')
            .w_CHPRODOC = NVL(.w_ZoomAg.getVar('CHPRODOC'),0)
            .w_XCHK = Nvl(.w_ZoomAg.getVar('XCHK'),0)
            .w_GEN = Nvl(.w_ZoomAg.getVar('GEN'),'N')
        .DoRTCalc(31,32,.t.)
            .w_TIPREC = NVL(.w_ZoomAg.getVar('TIPREC'), ' ')
            .w_CHDATEST = NVL(.w_ZoomAg.getVar('Chdatest'), ' ')
            .w_TIPOPE = NVL(.w_ZoomAg.getVar('TIPOPE'), ' ')
            .w_DATRETT = NVL(.w_ZoomAg.getVar('DATRETT'), ' ')
        .DoRTCalc(37,38,.t.)
            .w_CERTMOD = NVL(.w_ZoomAg.getVar('CERTMOD'), ' ')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page2.oPag.ZoomAg.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oBtn_1_22.visible=!this.oPgFrm.Page1.oPag.oBtn_1_22.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_36.visible=!this.oPgFrm.Page1.oPag.oStr_1_36.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_38.visible=!this.oPgFrm.Page1.oPag.oStr_1_38.mHide()
    this.oPgFrm.Page1.oPag.oCUDATCER_1_39.visible=!this.oPgFrm.Page1.oPag.oCUDATCER_1_39.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_40.visible=!this.oPgFrm.Page1.oPag.oStr_1_40.mHide()
    this.oPgFrm.Page1.oPag.oTIPO_COMU_1_41.visible=!this.oPgFrm.Page1.oPag.oTIPO_COMU_1_41.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page2.oPag.ZoomAg.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CHCODCON
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CHCODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFR',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CHCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_CHTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_CHTIPCON;
                     ,'ANCODICE',trim(this.w_CHCODCON))
          select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CHCODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CHCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_CHTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CHCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_CHTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CHCODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCHCODCON_1_17'),i_cWhere,'GSAR_AFR',"Elenco fornitori",'GSRI_ADH.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CHTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_CHTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CHCODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CHCODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_CHTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_CHTIPCON;
                       ,'ANCODICE',this.w_CHCODCON)
            select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CHCODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_CHDESCRI = NVL(_Link_.ANDESCRI,space(60))
      this.w_ANRITENU = NVL(_Link_.ANRITENU,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CHCODCON = space(15)
      endif
      this.w_CHDESCRI = space(60)
      this.w_ANRITENU = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((Empty(.w_Chcodconf)) Or (.w_Chcodcon<=.w_Chcodconf)) And .w_Anritenu $ 'CS'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CHCODCON = space(15)
        this.w_CHDESCRI = space(60)
        this.w_ANRITENU = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CHCODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CHCODCONF
  func Link_1_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CHCODCONF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFR',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CHCODCONF)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_CHTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_CHTIPCON;
                     ,'ANCODICE',trim(this.w_CHCODCONF))
          select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CHCODCONF)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CHCODCONF)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_CHTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CHCODCONF)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_CHTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CHCODCONF) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCHCODCONF_1_18'),i_cWhere,'GSAR_AFR',"Elenco fornitori",'GSRI_ADH.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CHTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_CHTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CHCODCONF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CHCODCONF);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_CHTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_CHTIPCON;
                       ,'ANCODICE',this.w_CHCODCONF)
            select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CHCODCONF = NVL(_Link_.ANCODICE,space(15))
      this.w_CHDESCRIF = NVL(_Link_.ANDESCRI,space(60))
      this.w_ANRITENUF = NVL(_Link_.ANRITENU,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CHCODCONF = space(15)
      endif
      this.w_CHDESCRIF = space(60)
      this.w_ANRITENUF = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((.w_Chcodconf>=.w_Chcodcon) Or Empty(.w_Chcodcon)) AND .w_Anritenuf $ 'CS'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CHCODCONF = space(15)
        this.w_CHDESCRIF = space(60)
        this.w_ANRITENUF = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CHCODCONF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCH__ANNO_1_7.value==this.w_CH__ANNO)
      this.oPgFrm.Page1.oPag.oCH__ANNO_1_7.value=this.w_CH__ANNO
    endif
    if not(this.oPgFrm.Page1.oPag.oCHCERDEF_1_8.RadioValue()==this.w_CHCERDEF)
      this.oPgFrm.Page1.oPag.oCHCERDEF_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCHDAVERI_1_9.RadioValue()==this.w_CHDAVERI)
      this.oPgFrm.Page1.oPag.oCHDAVERI_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCHSCLGEN_1_10.RadioValue()==this.w_CHSCLGEN)
      this.oPgFrm.Page1.oPag.oCHSCLGEN_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCHPEREST_1_11.RadioValue()==this.w_CHPEREST)
      this.oPgFrm.Page1.oPag.oCHPEREST_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCHSCHUMA_1_12.RadioValue()==this.w_CHSCHUMA)
      this.oPgFrm.Page1.oPag.oCHSCHUMA_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCHTIPOPE_1_13.RadioValue()==this.w_CHTIPOPE)
      this.oPgFrm.Page1.oPag.oCHTIPOPE_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCHTIPDAT_1_14.RadioValue()==this.w_CHTIPDAT)
      this.oPgFrm.Page1.oPag.oCHTIPDAT_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCHCAUPRE_1_15.RadioValue()==this.w_CHCAUPRE)
      this.oPgFrm.Page1.oPag.oCHCAUPRE_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCHTIPREC_1_16.RadioValue()==this.w_CHTIPREC)
      this.oPgFrm.Page1.oPag.oCHTIPREC_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCHCODCON_1_17.value==this.w_CHCODCON)
      this.oPgFrm.Page1.oPag.oCHCODCON_1_17.value=this.w_CHCODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oCHCODCONF_1_18.value==this.w_CHCODCONF)
      this.oPgFrm.Page1.oPag.oCHCODCONF_1_18.value=this.w_CHCODCONF
    endif
    if not(this.oPgFrm.Page1.oPag.oCHCODFIS_1_19.value==this.w_CHCODFIS)
      this.oPgFrm.Page1.oPag.oCHCODFIS_1_19.value=this.w_CHCODFIS
    endif
    if not(this.oPgFrm.Page1.oPag.oCHCOFISC_1_20.value==this.w_CHCOFISC)
      this.oPgFrm.Page1.oPag.oCHCOFISC_1_20.value=this.w_CHCOFISC
    endif
    if not(this.oPgFrm.Page1.oPag.oCHDATCOM_1_21.RadioValue()==this.w_CHDATCOM)
      this.oPgFrm.Page1.oPag.oCHDATCOM_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCHDESCRI_1_30.value==this.w_CHDESCRI)
      this.oPgFrm.Page1.oPag.oCHDESCRI_1_30.value=this.w_CHDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCHDESCRIF_1_37.value==this.w_CHDESCRIF)
      this.oPgFrm.Page1.oPag.oCHDESCRIF_1_37.value=this.w_CHDESCRIF
    endif
    if not(this.oPgFrm.Page1.oPag.oCUDATCER_1_39.value==this.w_CUDATCER)
      this.oPgFrm.Page1.oPag.oCUDATCER_1_39.value=this.w_CUDATCER
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPO_COMU_1_41.RadioValue()==this.w_TIPO_COMU)
      this.oPgFrm.Page1.oPag.oTIPO_COMU_1_41.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_CH__ANNO)) or not(.w_CH__ANNO>=2015))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCH__ANNO_1_7.SetFocus()
            i_bnoObbl = !empty(.w_CH__ANNO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(((Empty(.w_Chcodconf)) Or (.w_Chcodcon<=.w_Chcodconf)) And .w_Anritenu $ 'CS')  and not(empty(.w_CHCODCON))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCHCODCON_1_17.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(((.w_Chcodconf>=.w_Chcodcon) Or Empty(.w_Chcodcon)) AND .w_Anritenuf $ 'CS')  and not(empty(.w_CHCODCONF))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCHCODCONF_1_18.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CHDATCOM = this.w_CHDATCOM
    return

enddefine

* --- Define pages as container
define class tgsri_kdePag1 as StdContainer
  Width  = 790
  height = 532
  stdWidth  = 790
  stdheight = 532
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCH__ANNO_1_7 as StdField with uid="JBAYHUASCK",rtseq=7,rtrep=.f.,;
    cFormVar = "w_CH__ANNO", cQueryName = "CH__ANNO",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Anno di riferimento",;
    HelpContextID = 41204107,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=175, Top=30, cSayPict='"9999"', cGetPict='"9999"'

  func oCH__ANNO_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_CH__ANNO>=2015)
    endwith
    return bRes
  endfunc


  add object oCHCERDEF_1_8 as StdCombo with uid="CGNAQPOCUZ",rtseq=8,rtrep=.f.,left=462,top=29,width=73,height=22;
    , ToolTipText = "Certificazioni definitive";
    , HelpContextID = 75466348;
    , cFormVar="w_CHCERDEF",RowSource=""+"Si,"+"No,"+"Tutte", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCHCERDEF_1_8.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oCHCERDEF_1_8.GetRadio()
    this.Parent.oContained.w_CHCERDEF = this.RadioValue()
    return .t.
  endfunc

  func oCHCERDEF_1_8.SetRadio()
    this.Parent.oContained.w_CHCERDEF=trim(this.Parent.oContained.w_CHCERDEF)
    this.value = ;
      iif(this.Parent.oContained.w_CHCERDEF=='S',1,;
      iif(this.Parent.oContained.w_CHCERDEF=='N',2,;
      iif(this.Parent.oContained.w_CHCERDEF=='T',3,;
      0)))
  endfunc


  add object oCHDAVERI_1_9 as StdCombo with uid="HHHJPCDAST",rtseq=9,rtrep=.f.,left=175,top=68,width=73,height=22;
    , ToolTipText = "Certificazioni da verificare";
    , HelpContextID = 96179823;
    , cFormVar="w_CHDAVERI",RowSource=""+"Si,"+"No,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCHDAVERI_1_9.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oCHDAVERI_1_9.GetRadio()
    this.Parent.oContained.w_CHDAVERI = this.RadioValue()
    return .t.
  endfunc

  func oCHDAVERI_1_9.SetRadio()
    this.Parent.oContained.w_CHDAVERI=trim(this.Parent.oContained.w_CHDAVERI)
    this.value = ;
      iif(this.Parent.oContained.w_CHDAVERI=='S',1,;
      iif(this.Parent.oContained.w_CHDAVERI=='N',2,;
      iif(this.Parent.oContained.w_CHDAVERI=='T',3,;
      0)))
  endfunc


  add object oCHSCLGEN_1_10 as StdCombo with uid="BQLKCHSUNV",rtseq=10,rtrep=.f.,left=462,top=68,width=73,height=22;
    , ToolTipText = "Escludi da generazione";
    , HelpContextID = 119441012;
    , cFormVar="w_CHSCLGEN",RowSource=""+"Si,"+"No,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCHSCLGEN_1_10.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oCHSCLGEN_1_10.GetRadio()
    this.Parent.oContained.w_CHSCLGEN = this.RadioValue()
    return .t.
  endfunc

  func oCHSCLGEN_1_10.SetRadio()
    this.Parent.oContained.w_CHSCLGEN=trim(this.Parent.oContained.w_CHSCLGEN)
    this.value = ;
      iif(this.Parent.oContained.w_CHSCLGEN=='S',1,;
      iif(this.Parent.oContained.w_CHSCLGEN=='N',2,;
      iif(this.Parent.oContained.w_CHSCLGEN=='T',3,;
      0)))
  endfunc


  add object oCHPEREST_1_11 as StdCombo with uid="MTWIVZVSQN",rtseq=11,rtrep=.f.,left=175,top=106,width=73,height=22;
    , ToolTipText = "Percipiente estero";
    , HelpContextID = 92296826;
    , cFormVar="w_CHPEREST",RowSource=""+"Si,"+"No,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCHPEREST_1_11.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oCHPEREST_1_11.GetRadio()
    this.Parent.oContained.w_CHPEREST = this.RadioValue()
    return .t.
  endfunc

  func oCHPEREST_1_11.SetRadio()
    this.Parent.oContained.w_CHPEREST=trim(this.Parent.oContained.w_CHPEREST)
    this.value = ;
      iif(this.Parent.oContained.w_CHPEREST=='S',1,;
      iif(this.Parent.oContained.w_CHPEREST=='N',2,;
      iif(this.Parent.oContained.w_CHPEREST=='T',3,;
      0)))
  endfunc


  add object oCHSCHUMA_1_12 as StdCombo with uid="VSQCVAVIYY",rtseq=12,rtrep=.f.,left=462,top=106,width=73,height=22;
    , ToolTipText = "Non residenti Schumacker";
    , HelpContextID = 186743193;
    , cFormVar="w_CHSCHUMA",RowSource=""+"Si,"+"No,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCHSCHUMA_1_12.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oCHSCHUMA_1_12.GetRadio()
    this.Parent.oContained.w_CHSCHUMA = this.RadioValue()
    return .t.
  endfunc

  func oCHSCHUMA_1_12.SetRadio()
    this.Parent.oContained.w_CHSCHUMA=trim(this.Parent.oContained.w_CHSCHUMA)
    this.value = ;
      iif(this.Parent.oContained.w_CHSCHUMA=='S',1,;
      iif(this.Parent.oContained.w_CHSCHUMA=='N',2,;
      iif(this.Parent.oContained.w_CHSCHUMA=='T',3,;
      0)))
  endfunc


  add object oCHTIPOPE_1_13 as StdCombo with uid="AHURCQYSSJ",rtseq=13,rtrep=.f.,left=175,top=144,width=106,height=22;
    , ToolTipText = "Tipo operazione";
    , HelpContextID = 258250347;
    , cFormVar="w_CHTIPOPE",RowSource=""+"Ordinario,"+"Sostitutiva,"+"Annullamento,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCHTIPOPE_1_13.RadioValue()
    return(iif(this.value =1,'O',;
    iif(this.value =2,'S',;
    iif(this.value =3,'A',;
    iif(this.value =4,'T',;
    space(1))))))
  endfunc
  func oCHTIPOPE_1_13.GetRadio()
    this.Parent.oContained.w_CHTIPOPE = this.RadioValue()
    return .t.
  endfunc

  func oCHTIPOPE_1_13.SetRadio()
    this.Parent.oContained.w_CHTIPOPE=trim(this.Parent.oContained.w_CHTIPOPE)
    this.value = ;
      iif(this.Parent.oContained.w_CHTIPOPE=='O',1,;
      iif(this.Parent.oContained.w_CHTIPOPE=='S',2,;
      iif(this.Parent.oContained.w_CHTIPOPE=='A',3,;
      iif(this.Parent.oContained.w_CHTIPOPE=='T',4,;
      0))))
  endfunc


  add object oCHTIPDAT_1_14 as StdCombo with uid="CUNNNAHVLJ",rtseq=14,rtrep=.f.,left=462,top=144,width=106,height=22;
    , ToolTipText = "Tipo inserimento";
    , HelpContextID = 73700986;
    , cFormVar="w_CHTIPDAT",RowSource=""+"Automatico,"+"Manuale,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCHTIPDAT_1_14.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'M',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oCHTIPDAT_1_14.GetRadio()
    this.Parent.oContained.w_CHTIPDAT = this.RadioValue()
    return .t.
  endfunc

  func oCHTIPDAT_1_14.SetRadio()
    this.Parent.oContained.w_CHTIPDAT=trim(this.Parent.oContained.w_CHTIPDAT)
    this.value = ;
      iif(this.Parent.oContained.w_CHTIPDAT=='A',1,;
      iif(this.Parent.oContained.w_CHTIPDAT=='M',2,;
      iif(this.Parent.oContained.w_CHTIPDAT=='T',3,;
      0)))
  endfunc


  add object oCHCAUPRE_1_15 as StdCombo with uid="MERGWDMNOJ",rtseq=15,rtrep=.f.,left=175,top=182,width=73,height=22;
    , ToolTipText = "Causale prestazione";
    , HelpContextID = 11241067;
    , cFormVar="w_CHCAUPRE",RowSource=""+"Tipo A,"+"Tipo B,"+"Tipo C,"+"Tipo D,"+"Tipo E,"+"Tipo F,"+"Tipo G,"+"Tipo H,"+"Tipo I,"+"Tipo J,"+"Tipo K,"+"Tipo L,"+"Tipo L1,"+"Tipo M,"+"Tipo M1,"+"Tipo M2,"+"Tipo N,"+"Tipo O,"+"Tipo O1,"+"Tipo P,"+"Tipo Q,"+"Tipo R,"+"Tipo S,"+"Tipo T,"+"Tipo U,"+"Tipo V,"+"Tipo V1,"+"Tipo V2,"+"Tipo W,"+"Tipo X,"+"Tipo Y,"+"Tipo Z,"+"Tipo ZO,"+"Tutte", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCHCAUPRE_1_15.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'B',;
    iif(this.value =3,'C',;
    iif(this.value =4,'D',;
    iif(this.value =5,'E',;
    iif(this.value =6,'F',;
    iif(this.value =7,'G',;
    iif(this.value =8,'H',;
    iif(this.value =9,'I',;
    iif(this.value =10,'J',;
    iif(this.value =11,'K',;
    iif(this.value =12,'L',;
    iif(this.value =13,'L1',;
    iif(this.value =14,'M',;
    iif(this.value =15,'M1',;
    iif(this.value =16,'M2',;
    iif(this.value =17,'N',;
    iif(this.value =18,'O',;
    iif(this.value =19,'O1',;
    iif(this.value =20,'P',;
    iif(this.value =21,'Q',;
    iif(this.value =22,'R',;
    iif(this.value =23,'S',;
    iif(this.value =24,'T',;
    iif(this.value =25,'U',;
    iif(this.value =26,'V',;
    iif(this.value =27,'V1',;
    iif(this.value =28,'V2',;
    iif(this.value =29,'W',;
    iif(this.value =30,'X',;
    iif(this.value =31,'Y',;
    iif(this.value =32,'Z',;
    iif(this.value =33,'ZO',;
    iif(this.value =34,'TT',;
    space(2))))))))))))))))))))))))))))))))))))
  endfunc
  func oCHCAUPRE_1_15.GetRadio()
    this.Parent.oContained.w_CHCAUPRE = this.RadioValue()
    return .t.
  endfunc

  func oCHCAUPRE_1_15.SetRadio()
    this.Parent.oContained.w_CHCAUPRE=trim(this.Parent.oContained.w_CHCAUPRE)
    this.value = ;
      iif(this.Parent.oContained.w_CHCAUPRE=='A',1,;
      iif(this.Parent.oContained.w_CHCAUPRE=='B',2,;
      iif(this.Parent.oContained.w_CHCAUPRE=='C',3,;
      iif(this.Parent.oContained.w_CHCAUPRE=='D',4,;
      iif(this.Parent.oContained.w_CHCAUPRE=='E',5,;
      iif(this.Parent.oContained.w_CHCAUPRE=='F',6,;
      iif(this.Parent.oContained.w_CHCAUPRE=='G',7,;
      iif(this.Parent.oContained.w_CHCAUPRE=='H',8,;
      iif(this.Parent.oContained.w_CHCAUPRE=='I',9,;
      iif(this.Parent.oContained.w_CHCAUPRE=='J',10,;
      iif(this.Parent.oContained.w_CHCAUPRE=='K',11,;
      iif(this.Parent.oContained.w_CHCAUPRE=='L',12,;
      iif(this.Parent.oContained.w_CHCAUPRE=='L1',13,;
      iif(this.Parent.oContained.w_CHCAUPRE=='M',14,;
      iif(this.Parent.oContained.w_CHCAUPRE=='M1',15,;
      iif(this.Parent.oContained.w_CHCAUPRE=='M2',16,;
      iif(this.Parent.oContained.w_CHCAUPRE=='N',17,;
      iif(this.Parent.oContained.w_CHCAUPRE=='O',18,;
      iif(this.Parent.oContained.w_CHCAUPRE=='O1',19,;
      iif(this.Parent.oContained.w_CHCAUPRE=='P',20,;
      iif(this.Parent.oContained.w_CHCAUPRE=='Q',21,;
      iif(this.Parent.oContained.w_CHCAUPRE=='R',22,;
      iif(this.Parent.oContained.w_CHCAUPRE=='S',23,;
      iif(this.Parent.oContained.w_CHCAUPRE=='T',24,;
      iif(this.Parent.oContained.w_CHCAUPRE=='U',25,;
      iif(this.Parent.oContained.w_CHCAUPRE=='V',26,;
      iif(this.Parent.oContained.w_CHCAUPRE=='V1',27,;
      iif(this.Parent.oContained.w_CHCAUPRE=='V2',28,;
      iif(this.Parent.oContained.w_CHCAUPRE=='W',29,;
      iif(this.Parent.oContained.w_CHCAUPRE=='X',30,;
      iif(this.Parent.oContained.w_CHCAUPRE=='Y',31,;
      iif(this.Parent.oContained.w_CHCAUPRE=='Z',32,;
      iif(this.Parent.oContained.w_CHCAUPRE=='ZO',33,;
      iif(this.Parent.oContained.w_CHCAUPRE=='TT',34,;
      0))))))))))))))))))))))))))))))))))
  endfunc


  add object oCHTIPREC_1_16 as StdCombo with uid="FJRTFQGRPN",rtseq=16,rtrep=.f.,left=462,top=182,width=73,height=21;
    , ToolTipText = "Stato certificazione";
    , HelpContextID = 40146537;
    , cFormVar="w_CHTIPREC",RowSource=""+"Respinta,"+"Accolta,"+"Tutte", bObbl = .f. , nPag = 1;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oCHTIPREC_1_16.RadioValue()
    return(iif(this.value =1,'Q',;
    iif(this.value =2,'R',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oCHTIPREC_1_16.GetRadio()
    this.Parent.oContained.w_CHTIPREC = this.RadioValue()
    return .t.
  endfunc

  func oCHTIPREC_1_16.SetRadio()
    this.Parent.oContained.w_CHTIPREC=trim(this.Parent.oContained.w_CHTIPREC)
    this.value = ;
      iif(this.Parent.oContained.w_CHTIPREC=='Q',1,;
      iif(this.Parent.oContained.w_CHTIPREC=='R',2,;
      iif(this.Parent.oContained.w_CHTIPREC=='T',3,;
      0)))
  endfunc

  add object oCHCODCON_1_17 as StdField with uid="UCJURWGOQP",rtseq=17,rtrep=.f.,;
    cFormVar = "w_CHCODCON", cQueryName = "CHCODCON",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice conto iniziale",;
    HelpContextID = 223771020,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=175, Top=220, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_AFR", oKey_1_1="ANTIPCON", oKey_1_2="this.w_CHTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CHCODCON"

  func oCHCODCON_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oCHCODCON_1_17.ecpDrop(oSource)
    this.Parent.oContained.link_1_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCHCODCON_1_17.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_CHTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_CHTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCHCODCON_1_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFR',"Elenco fornitori",'GSRI_ADH.CONTI_VZM',this.parent.oContained
  endproc
  proc oCHCODCON_1_17.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_CHTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CHCODCON
     i_obj.ecpSave()
  endproc

  add object oCHCODCONF_1_18 as StdField with uid="UPBQAAIJKT",rtseq=18,rtrep=.f.,;
    cFormVar = "w_CHCODCONF", cQueryName = "CHCODCONF",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice conto iniziale",;
    HelpContextID = 223769900,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=175, Top=258, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_AFR", oKey_1_1="ANTIPCON", oKey_1_2="this.w_CHTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CHCODCONF"

  func oCHCODCONF_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oCHCODCONF_1_18.ecpDrop(oSource)
    this.Parent.oContained.link_1_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCHCODCONF_1_18.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_CHTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_CHTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCHCODCONF_1_18'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFR',"Elenco fornitori",'GSRI_ADH.CONTI_VZM',this.parent.oContained
  endproc
  proc oCHCODCONF_1_18.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_CHTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CHCODCONF
     i_obj.ecpSave()
  endproc

  add object oCHCODFIS_1_19 as StdField with uid="FPPNBONOQK",rtseq=19,rtrep=.f.,;
    cFormVar = "w_CHCODFIS", cQueryName = "CHCODFIS",;
    bObbl = .f. , nPag = 1, value=space(16), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale",;
    HelpContextID = 173439367,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=176, Top=296, InputMask=replicate('X',16)

  add object oCHCOFISC_1_20 as StdField with uid="AQPQZSDDKJ",rtseq=20,rtrep=.f.,;
    cFormVar = "w_CHCOFISC", cQueryName = "CHCOFISC",;
    bObbl = .f. , nPag = 1, value=space(25), bMultilanguage =  .f.,;
    ToolTipText = "Codice identificativo fiscale",;
    HelpContextID = 147424873,;
   bGlobalFont=.t.,;
    Height=21, Width=188, Left=175, Top=334, InputMask=replicate('X',25)


  add object oCHDATCOM_1_21 as StdCombo with uid="VQDYBREBOU",rtseq=21,rtrep=.f.,left=175,top=372,width=73,height=22;
    , ToolTipText = "Dati comunicati";
    , HelpContextID = 207907213;
    , cFormVar="w_CHDATCOM",RowSource=""+"Si,"+"No,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCHDATCOM_1_21.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oCHDATCOM_1_21.GetRadio()
    this.Parent.oContained.w_CHDATCOM = this.RadioValue()
    return .t.
  endfunc

  func oCHDATCOM_1_21.SetRadio()
    this.Parent.oContained.w_CHDATCOM=trim(this.Parent.oContained.w_CHDATCOM)
    this.value = ;
      iif(this.Parent.oContained.w_CHDATCOM=='S',1,;
      iif(this.Parent.oContained.w_CHDATCOM=='N',2,;
      iif(this.Parent.oContained.w_CHDATCOM=='T',3,;
      0)))
  endfunc


  add object oBtn_1_22 as StdButton with uid="PEMMEUSHQF",left=175, top=425, width=15,height=19,;
    caption="...", nPag=1;
    , HelpContextID = 82240810;
  , bGlobalFont=.t.

    proc oBtn_1_22.Click()
      do GSRI3SCS with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_22.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_CHDATCOM<>'S')
     endwith
    endif
  endfunc

  add object oCHDESCRI_1_30 as StdField with uid="MGGNLWGUXP",rtseq=22,rtrep=.f.,;
    cFormVar = "w_CHDESCRI", cQueryName = "CHDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 59741807,;
   bGlobalFont=.t.,;
    Height=21, Width=356, Left=343, Top=220, InputMask=replicate('X',60)

  add object oCHDESCRIF_1_37 as StdField with uid="NAYTKHBCLX",rtseq=23,rtrep=.f.,;
    cFormVar = "w_CHDESCRIF", cQueryName = "CHDESCRIF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 59742927,;
   bGlobalFont=.t.,;
    Height=21, Width=356, Left=343, Top=258, InputMask=replicate('X',60)

  add object oCUDATCER_1_39 as StdField with uid="YFKCTMWLQQ",rtseq=24,rtrep=.f.,;
    cFormVar = "w_CUDATCER", cQueryName = "CUDATCER",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 60531576,;
   bGlobalFont=.t.,;
    Height=21, Width=74, Left=350, Top=422

  func oCUDATCER_1_39.mHide()
    with this.Parent.oContained
      return (.w_CHDATCOM<>'S')
    endwith
  endfunc


  add object oTIPO_COMU_1_41 as StdCombo with uid="DACBENUJOV",rtseq=25,rtrep=.f.,left=588,top=419,width=195,height=22, enabled=.f.;
    , HelpContextID = 195404333;
    , cFormVar="w_TIPO_COMU",RowSource=""+"Annullamento/Sostituzione,"+"Sostitutuzione,"+"Annullamento,"+"Ordinaria", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPO_COMU_1_41.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'S',;
    iif(this.value =3,'A',;
    iif(this.value =4,'O',;
    space(1))))))
  endfunc
  func oTIPO_COMU_1_41.GetRadio()
    this.Parent.oContained.w_TIPO_COMU = this.RadioValue()
    return .t.
  endfunc

  func oTIPO_COMU_1_41.SetRadio()
    this.Parent.oContained.w_TIPO_COMU=trim(this.Parent.oContained.w_TIPO_COMU)
    this.value = ;
      iif(this.Parent.oContained.w_TIPO_COMU=='T',1,;
      iif(this.Parent.oContained.w_TIPO_COMU=='S',2,;
      iif(this.Parent.oContained.w_TIPO_COMU=='A',3,;
      iif(this.Parent.oContained.w_TIPO_COMU=='O',4,;
      0))))
  endfunc

  func oTIPO_COMU_1_41.mHide()
    with this.Parent.oContained
      return (.w_CHDATCOM<>'S')
    endwith
  endfunc


  add object oBtn_1_42 as StdButton with uid="ZXYGHBDURE",left=677, top=481, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare i dati";
    , HelpContextID = 203389930;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_42.Click()
      =cp_StandardFunction(this,"PgDn")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_43 as StdButton with uid="KUWCMEGMLG",left=729, top=481, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 8504086;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_43.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_23 as StdString with uid="AMWXDKXOMM",Visible=.t., Left=11, Top=68,;
    Alignment=1, Width=159, Height=18,;
    Caption="Certificazioni da verificare:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="JELVCYJCSJ",Visible=.t., Left=302, Top=68,;
    Alignment=1, Width=154, Height=18,;
    Caption="Escludi da generazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="QLVIRWGQKI",Visible=.t., Left=58, Top=107,;
    Alignment=1, Width=112, Height=18,;
    Caption="Percipiente estero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="DLPTMSKZVP",Visible=.t., Left=307, Top=107,;
    Alignment=1, Width=149, Height=18,;
    Caption="Non residenti Schumacker:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="QDVKRQIEET",Visible=.t., Left=29, Top=183,;
    Alignment=1, Width=141, Height=18,;
    Caption="Causale prestazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="OQVFFADKVX",Visible=.t., Left=338, Top=145,;
    Alignment=1, Width=118, Height=18,;
    Caption="Tipo inserimento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="HROKKLNQMS",Visible=.t., Left=57, Top=374,;
    Alignment=1, Width=113, Height=18,;
    Caption="Dati comunicati:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="BNSTAFZAAA",Visible=.t., Left=103, Top=221,;
    Alignment=1, Width=67, Height=18,;
    Caption="Da fornitore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="QTJDMGJXFC",Visible=.t., Left=113, Top=261,;
    Alignment=1, Width=57, Height=18,;
    Caption="A fornitore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="IYWQINFNOK",Visible=.t., Left=89, Top=297,;
    Alignment=1, Width=81, Height=18,;
    Caption="Codice fiscale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="KJZJGEHLMF",Visible=.t., Left=63, Top=145,;
    Alignment=1, Width=107, Height=18,;
    Caption="Tipo operazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="RDIQAAKAKE",Visible=.t., Left=124, Top=33,;
    Alignment=1, Width=46, Height=18,;
    Caption="Anno:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="TNZRAHIUYO",Visible=.t., Left=50, Top=426,;
    Alignment=1, Width=113, Height=18,;
    Caption="Elenco generazioni"  ;
  , bGlobalFont=.t.

  func oStr_1_36.mHide()
    with this.Parent.oContained
      return (.w_CHDATCOM<>'S')
    endwith
  endfunc

  add object oStr_1_38 as StdString with uid="JRPQIQGAIZ",Visible=.t., Left=204, Top=424,;
    Alignment=1, Width=138, Height=18,;
    Caption="Data comunicazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_38.mHide()
    with this.Parent.oContained
      return (.w_CHDATCOM<>'S')
    endwith
  endfunc

  add object oStr_1_40 as StdString with uid="RLSSHHHQBW",Visible=.t., Left=435, Top=423,;
    Alignment=1, Width=149, Height=18,;
    Caption="Tipo di comunicazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_40.mHide()
    with this.Parent.oContained
      return (.w_CHDATCOM<>'S')
    endwith
  endfunc

  add object oStr_1_47 as StdString with uid="ZJJEJDZFYC",Visible=.t., Left=-3, Top=335,;
    Alignment=1, Width=171, Height=18,;
    Caption="Codice identificativo fiscale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_48 as StdString with uid="XYOVVCLTTJ",Visible=.t., Left=351, Top=183,;
    Alignment=1, Width=105, Height=18,;
    Caption="Stato certificazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_50 as StdString with uid="EVYEBQFHSQ",Visible=.t., Left=283, Top=33,;
    Alignment=1, Width=173, Height=18,;
    Caption="Certificazioni definitive:"  ;
  , bGlobalFont=.t.
enddefine
define class tgsri_kdePag2 as StdContainer
  Width  = 790
  height = 532
  stdWidth  = 790
  stdheight = 532
  resizeXpos=342
  resizeYpos=249
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object ZoomAg as cp_szoombox with uid="WUTNYJHGAP",left=1, top=4, width=783,height=502,;
    caption='ZoomAg',;
   bGlobalFont=.t.,;
    cMenuFile="GSRI_KDE",cTable="CUDTETRH",bOptions=.t.,bAdvOptions=.t.,bReadOnly=.t.,bQueryOnLoad=.f.,cZoomFile="GSRI_KDE",bRetriveAllRows=.f.,cZoomOnZoom="",;
    cEvent = "ActivatePage 2,Aggiorna",;
    nPag=2;
    , HelpContextID = 110784150

  add object oStr_2_11 as StdString with uid="RZYRRIRGOQ",Visible=.t., Left=7, Top=514,;
    Alignment=0, Width=126, Height=17,;
    Caption="Percipiente estero"    , ForeColor=RGB(0,0,255);
  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_12 as StdString with uid="UETCDYPSPY",Visible=.t., Left=167, Top=514,;
    Alignment=0, Width=204, Height=17,;
    Caption="Origine di una nuova certificazione"    , BackStyle=1, BackColor=RGB(255,127,39);
  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsri_kde','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
