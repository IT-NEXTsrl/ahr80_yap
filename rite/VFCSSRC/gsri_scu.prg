* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_scu                                                        *
*              Selezione percipienti                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2015-02-05                                                      *
* Last revis.: 2015-03-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsri_scu",oParentObject))

* --- Class definition
define class tgsri_scu as StdForm
  Top    = 9
  Left   = 10

  * --- Standard Properties
  Width  = 794
  Height = 562
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-03-09"
  HelpContextID=90830441
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=8

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsri_scu"
  cComment = "Selezione percipienti"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CUTIPCAS = space(1)
  w_TIPOZOOM = space(1)
  w_STAMPAFRONT = space(1)
  w_FIRMA_DICHIARAZIONE = space(250)
  w_FIRMA_INTERMEDIARIO = space(250)
  w_FIRMA_IMPOSTA = space(250)
  w_SERIALE = space(10)
  w_STAMPADETTAGLIO = space(1)
  w_ElePerc = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsri_scuPag1","gsri_scu",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSTAMPAFRONT_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ElePerc = this.oPgFrm.Pages(1).oPag.ElePerc
    DoDefault()
    proc Destroy()
      this.w_ElePerc = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CUTIPCAS=space(1)
      .w_TIPOZOOM=space(1)
      .w_STAMPAFRONT=space(1)
      .w_FIRMA_DICHIARAZIONE=space(250)
      .w_FIRMA_INTERMEDIARIO=space(250)
      .w_FIRMA_IMPOSTA=space(250)
      .w_SERIALE=space(10)
      .w_STAMPADETTAGLIO=space(1)
      .w_TIPOZOOM=oParentObject.w_TIPOZOOM
      .w_STAMPAFRONT=oParentObject.w_STAMPAFRONT
      .w_FIRMA_DICHIARAZIONE=oParentObject.w_FIRMA_DICHIARAZIONE
      .w_FIRMA_INTERMEDIARIO=oParentObject.w_FIRMA_INTERMEDIARIO
      .w_FIRMA_IMPOSTA=oParentObject.w_FIRMA_IMPOSTA
      .w_STAMPADETTAGLIO=oParentObject.w_STAMPADETTAGLIO
        .w_CUTIPCAS = This.oParentObject.oParentObject.w_Cutipcas
          .DoRTCalc(2,2,.f.)
        .w_STAMPAFRONT = .w_STAMPAFRONT
      .oPgFrm.Page1.oPag.ElePerc.Calculate()
          .DoRTCalc(4,6,.f.)
        .w_SERIALE = This.oParentObject.oParentObject.w_Cuserial
        .w_STAMPADETTAGLIO = 'S'
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- gsri_scu
    Select * from (this.w_ElePerc.ccursor) where xchk=1 into cursor ElencoPercipienti
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_TIPOZOOM=.w_TIPOZOOM
      .oParentObject.w_STAMPAFRONT=.w_STAMPAFRONT
      .oParentObject.w_FIRMA_DICHIARAZIONE=.w_FIRMA_DICHIARAZIONE
      .oParentObject.w_FIRMA_INTERMEDIARIO=.w_FIRMA_INTERMEDIARIO
      .oParentObject.w_FIRMA_IMPOSTA=.w_FIRMA_IMPOSTA
      .oParentObject.w_STAMPADETTAGLIO=.w_STAMPADETTAGLIO
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.ElePerc.Calculate()
        .DoRTCalc(1,7,.t.)
            .w_STAMPADETTAGLIO = 'S'
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ElePerc.Calculate()
    endwith
  return

  proc Calculate_ZDFLOLROZK()
    with this
          * --- Seleziona record percipienti
          Gsri_Bcu(this;
              ,'S';
             )
    endwith
  endproc
  proc Calculate_YNKSLDBNSX()
    with this
          * --- Assegno il valore 'N' alle variabili w_STAMPAFRONT
          .oParentobject.w_STAMPAFRONT = 'N'
          .oParentobject.w_STAMPADETTAGLIO = 'N'
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oSTAMPAFRONT_1_3.enabled = this.oPgFrm.Page1.oPag.oSTAMPAFRONT_1_3.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ElePerc.Event(cEvent)
        if lower(cEvent)==lower("Blank")
          .Calculate_ZDFLOLROZK()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Edit Aborted")
          .Calculate_YNKSLDBNSX()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSTAMPAFRONT_1_3.RadioValue()==this.w_STAMPAFRONT)
      this.oPgFrm.Page1.oPag.oSTAMPAFRONT_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFIRMA_DICHIARAZIONE_1_4.value==this.w_FIRMA_DICHIARAZIONE)
      this.oPgFrm.Page1.oPag.oFIRMA_DICHIARAZIONE_1_4.value=this.w_FIRMA_DICHIARAZIONE
    endif
    if not(this.oPgFrm.Page1.oPag.oFIRMA_INTERMEDIARIO_1_5.value==this.w_FIRMA_INTERMEDIARIO)
      this.oPgFrm.Page1.oPag.oFIRMA_INTERMEDIARIO_1_5.value=this.w_FIRMA_INTERMEDIARIO
    endif
    if not(this.oPgFrm.Page1.oPag.oFIRMA_IMPOSTA_1_6.value==this.w_FIRMA_IMPOSTA)
      this.oPgFrm.Page1.oPag.oFIRMA_IMPOSTA_1_6.value=this.w_FIRMA_IMPOSTA
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsri_scuPag1 as StdContainer
  Width  = 790
  height = 562
  stdWidth  = 790
  stdheight = 562
  resizeXpos=366
  resizeYpos=238
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSTAMPAFRONT_1_3 as StdCheck with uid="BJLXRETCBZ",rtseq=3,rtrep=.f.,left=9, top=9, caption="Stampa frontespizio",;
    ToolTipText = "Se attivo, viene stampato il frontespizio della certificazione in formato PDF",;
    HelpContextID = 15533672,;
    cFormVar="w_STAMPAFRONT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSTAMPAFRONT_1_3.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oSTAMPAFRONT_1_3.GetRadio()
    this.Parent.oContained.w_STAMPAFRONT = this.RadioValue()
    return .t.
  endfunc

  func oSTAMPAFRONT_1_3.SetRadio()
    this.Parent.oContained.w_STAMPAFRONT=trim(this.Parent.oContained.w_STAMPAFRONT)
    this.value = ;
      iif(this.Parent.oContained.w_STAMPAFRONT=='S',1,;
      0)
  endfunc

  func oSTAMPAFRONT_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CUTIPCAS='O')
    endwith
   endif
  endfunc

  add object oFIRMA_DICHIARAZIONE_1_4 as StdField with uid="SORZZBTLBS",rtseq=4,rtrep=.f.,;
    cFormVar = "w_FIRMA_DICHIARAZIONE", cQueryName = "FIRMA_DICHIARAZIONE",;
    bObbl = .f. , nPag = 1, value=space(250), bMultilanguage =  .f.,;
    ToolTipText = "Firma della comunicazione",;
    HelpContextID = 73594632,;
   bGlobalFont=.t.,;
    Height=21, Width=566, Left=211, Top=39, InputMask=replicate('X',250)

  add object oFIRMA_INTERMEDIARIO_1_5 as StdField with uid="FVTGJEEOOZ",rtseq=5,rtrep=.f.,;
    cFormVar = "w_FIRMA_INTERMEDIARIO", cQueryName = "FIRMA_INTERMEDIARIO",;
    bObbl = .f. , nPag = 1, value=space(250), bMultilanguage =  .f.,;
    ToolTipText = "Firma dell'intermediario",;
    HelpContextID = 157278139,;
   bGlobalFont=.t.,;
    Height=21, Width=566, Left=211, Top=69, InputMask=replicate('X',250)

  add object oFIRMA_IMPOSTA_1_6 as StdField with uid="RPNVGVRXEK",rtseq=6,rtrep=.f.,;
    cFormVar = "w_FIRMA_IMPOSTA", cQueryName = "FIRMA_IMPOSTA",;
    bObbl = .f. , nPag = 1, value=space(250), bMultilanguage =  .f.,;
    ToolTipText = "Firma del sostituto d'imposta",;
    HelpContextID = 228459613,;
   bGlobalFont=.t.,;
    Height=21, Width=566, Left=211, Top=99, InputMask=replicate('X',250)


  add object ElePerc as cp_szoombox with uid="CCHYKRKZAC",left=3, top=148, width=781,height=354,;
    caption='Object',;
   bGlobalFont=.t.,;
    bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.t.,cMenuFile="",cZoomOnZoom="",cTable="GENDCOMU",cZoomFile="Gsri_Scu",bOptions=.t.,;
    cEvent = "Blank",;
    nPag=1;
    , HelpContextID = 87167206


  add object oBtn_1_10 as StdButton with uid="QFDCFAAMDM",left=10, top=511, width=48,height=45,;
    CpPicture="bmp\Check.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per selezionare i percipienti";
    , HelpContextID = 115478;
    , caption='\<Seleziona';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_10.Click()
      with this.Parent.oContained
        Gsri_Bcu(this.Parent.oContained,"S")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_11 as StdButton with uid="FVGQBXKFJS",left=66, top=511, width=48,height=45,;
    CpPicture="bmp\UnCheck.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per deselezionare i percipienti";
    , HelpContextID = 115478;
    ,  caption='\<Deselez.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_11.Click()
      with this.Parent.oContained
        Gsri_Bcu(this.Parent.oContained,"D")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_12 as StdButton with uid="KYPYELXDUC",left=122, top=511, width=48,height=45,;
    CpPicture="bmp\InvCheck.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per invertire la selezione dei percipienti";
    , HelpContextID = 115478;
    , caption='\<Inv. Sel.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_12.Click()
      with this.Parent.oContained
        Gsri_Bcu(this.Parent.oContained,"I")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_14 as StdButton with uid="NHMAPMFHBS",left=678, top=511, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per stampare i percipienti selezionati";
    , HelpContextID = 115478;
    , caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_14.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_15 as StdButton with uid="BRAIRZJTXI",left=731, top=511, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 115478;
    , caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_15.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_8 as StdString with uid="REGOGOKIXF",Visible=.t., Left=5, Top=125,;
    Alignment=0, Width=148, Height=18,;
    Caption="Elenco percipienti"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_18 as StdString with uid="NPMUGKBSUY",Visible=.t., Left=44, Top=41,;
    Alignment=1, Width=164, Height=18,;
    Caption="Firma della comunicazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="KLBNRNWNFY",Visible=.t., Left=65, Top=71,;
    Alignment=1, Width=143, Height=18,;
    Caption="Firma dell'intermediario:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="VTWTEZSYWR",Visible=.t., Left=36, Top=100,;
    Alignment=1, Width=172, Height=18,;
    Caption="Firma del sostituto d'imposta:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsri_scu','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
