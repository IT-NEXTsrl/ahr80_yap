* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_bav                                                        *
*              Legge ritenute della distinta                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][98][VRS_2]                                               *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-05-15                                                      *
* Last revis.: 2006-01-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsri_bav",oParentObject)
return(i_retval)

define class tgsri_bav as StdBatch
  * --- Local variables
  w_ZOOM = space(10)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Esegue Lettura Movimenti Ritenute in Distinta (Lanciato da GSRI_AVP e GSRI_AVN) durante l'evento Load
    * --- Carico lo Zoom solo se non sono in Caricamento
    if this.oParentObject.cFunction="Load"
      this.oParentObject.w_FILPAR = "zzz111kkk1"
      This.bUpdateParentObject=.f.
    endif
    * --- Inizializza le Depends per non Ricalcolare i Campi dopo la Load...
    This.OparentObject.SaveDependsOn()
    this.w_ZOOM = this.oParentObject.w_CalcZoom
    * --- Consente la Cancellazione scadenze dalla Distinta solo se no Definitiva
    * --- Lancia lo Zoom
    This.OparentObject.NotifyEvent("Legge")
    * --- Setta Proprieta' Campi del Cursore
    FOR I=1 TO This.w_Zoom.grd.ColumnCount
    NC = ALLTRIM(STR(I))
    if NOT "XCHK" $ UPPER(This.w_Zoom.grd.Column&NC..ControlSource) OR this.oParentObject.w_VPSTATUS="D"
      This.w_Zoom.grd.Column&NC..Enabled=.f.
    endif
    ENDFOR
    this.oParentObject.w_FILPAR = SPACE(10)
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
