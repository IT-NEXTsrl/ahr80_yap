* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_bci                                                        *
*              Creazione immagini per modulo CUD                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2016-01-19                                                      *
* Last revis.: 2018-09-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,PcurDati,w_CUD2016_Temp,w_TipoGest
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsri_bci",oParentObject,m.PcurDati,m.w_CUD2016_Temp,m.w_TipoGest)
return(i_retval)

define class tgsri_bci as StdBatch
  * --- Local variables
  PcurDati = space(10)
  w_CUD2016_Temp = space(0)
  w_TipoGest = space(3)
  w_NuovoFile = space(10)
  w_FILEORIGINE = space(0)
  w_TOTCAMPI = 0
  w_CICLACAMPI = 0
  w_VALORE = space(0)
  w_ERRORE = space(0)
  w_PAGINA = 0
  w_TIPOREC = space(0)
  * --- WorkFile variables
  CONTI_idx=0
  runtime_filters = 2

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    SELECT (this.PcurDati)
    this.w_TOTCAMPI = FCOUNT(this.PcurDati)
    * --- Crea il cursore contenente 1 riga per ognin campo
    CREATE CURSOR CampiValori (CAMPO char(40) , TESTO M)
    * --- Ciclo su tutti i campi del record corrente per creare il cursore con le coordinate grafiche di ogni campo e il testo  da scrivere
    *     La funzione AH_drawstring  utilizza il cursore per creare la nuova immagine
    * --- Prepara le immagini per la stampa sintetica
    SELECT (this.PcurDati) 
 GO TOP 
 SCAN
    this.w_TIPOREC = TIPOREC
    * --- Per ogni record bisogna creare unimmagine nella Temp 
    * --- Il nome/percorso dell immagine verr� salvato nel campo IMMAGINE del cursore dati  (Stampa Sintetica) 
    this.w_NuovoFile = alltrim(this.w_CUD2016_Temp)+alltrim(SYS(2015))+".PNG"
    if TYPE ("g_FORCEJPG") ="C"
      this.w_NuovoFile = FORCEEXT (this.w_NuovoFile, g_FORCEJPG )
    endif
    SELECT CampiValori 
 ZAP 
 SELECT (this.PcurDati)
    this.w_CICLACAMPI = 1
    do while this.w_CICLACAMPI <= this.w_TOTCAMPI
      nomecampo=FIELD ( this.W_CICLACAMPI )
      * --- Vengono stampati solo campi di tipo carattere o numerico
      if TYPE (nomecampo) $ "CN"
        if Not Empty (Nvl(&Nomecampo," "))
          this.w_VALORE = IIF ( TYPE (nomecampo) = "N" , TRAN( &nomecampo ,"@Z "+V_PV[80]) , &nomecampo )
          INSERT INTO CampiValori values ( ALLTRIM (nomecampo ), ALLTRIM (this.w_VALORE))
        endif
      endif
      this.w_CICLACAMPI = this.w_CICLACAMPI + 1
    enddo
    * --- Crea il curosre contenente il testo da inserire nell immagine e le posizioni/formato del testo
    do case
      case this.w_TipoGest=="770"
        SELECT * from CampiValori Inner join CUR770 on CampiValori.Campo=CUR770.Campo where alltrim(Modello)=alltrim(this.PcurDati) AND alltrim(Tiporec) = alltrim(this.w_Tiporec) into cursor Datimodulo
      case this.w_TipoGest=="770_2017"
        SELECT * from CampiValori Inner join CUR7702017 on CampiValori.Campo=CUR7702017.Campo where alltrim(Modello)=alltrim(this.PcurDati) AND alltrim(Tiporec) = alltrim(this.w_Tiporec) into cursor Datimodulo
      case this.w_TipoGest=="770_2018"
        SELECT * from CampiValori Inner join CUR7702018 on CampiValori.Campo=CUR7702018.Campo where alltrim(Modello)=alltrim(this.PcurDati) AND alltrim(Tiporec) = alltrim(this.w_Tiporec) into cursor Datimodulo
      case this.w_TipoGest=="Cud"
        SELECT * from CampiValori Inner Join CUD2016 on CampiValori.Campo=CUD2016.Campo where alltrim(Modello)=alltrim(this.PcurDati) AND alltrim(Tiporec) = alltrim(this.w_Tiporec) Into Cursor Datimodulo
      case this.w_TipoGest=="Cud_2017"
        SELECT * from CampiValori Inner Join CUD2017 on CampiValori.Campo=CUD2017.Campo where alltrim(Modello)=alltrim(this.PcurDati) AND alltrim(Tiporec) = alltrim(this.w_Tiporec) Into Cursor Datimodulo
      case this.w_TipoGest=="Cud_2018"
        SELECT * from CampiValori Inner Join CUD2018 on CampiValori.Campo=CUD2018.Campo where alltrim(Modello)=alltrim(this.PcurDati) AND alltrim(Tiporec) = alltrim(this.w_Tiporec) Into Cursor Datimodulo
    endcase
    * --- Determina da quale immagine bisogna partire per creare il file temporaneo
    SELECT DATIMODULO 
 Go Top
    if this.w_TipoGest="770"
      this.w_FILEORIGINE = ALLTRIM(ADDBS(ALLTRIM(Sys(5)+Sys(2003))))+"BMP\770\" + ALLTRIM(immagine)
    else
      this.w_FILEORIGINE = ALLTRIM(ADDBS(ALLTRIM(Sys(5)+Sys(2003))))+"BMP\CUD\" + ALLTRIM(immagine)
    endif
    * --- Su XP le librerie GDI non gestiscono le immagii PNG quindi si utilizzano immagini JPG
    if TYPE ("g_FORCEJPG")="C"
      this.w_FILEORIGINE = FORCEEXT (this.w_FILEORIGINE , g_FORCEJPG)
    endif
    * --- Crea l immagine temporanea scrivendo sull immagine originale il testo conntenuto nel cursore DATIMODULO
    this.w_ERRORE = ah_drawstring (this.w_FILEORIGINE , "DATIMODULO", this.w_NuovoFile)
    if NOT EMPTY (this.w_ERRORE)
      ah_errormsg ("Errore nella creazione del file temporaneo %1 (File di origine %2 )  %0%3",,, this.w_NuovoFile ,this.w_FILEORIGINE,this.w_ERRORE)
    endif
    * --- Dopo la creazione dell immagine si puo distruggere il cursore usato per creare l immagine
    if USED ("DATIMODULO")
      SELECT DATIMODULO 
 USE
    endif
    * --- --Scrive il nome dell immagine temporanea nel cursore che andr� in stampa
    Select (this.PcurDati) 
 REPLACE IMMAGINE with this.w_NuovoFile
    ENDSCAN
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if USED ("CUD2016")
      SELECT CUD2016 
 USE
    endif
    if USED ("CampiValori")
      SELECT CampiValori 
 USE
    endif
    if USED ("DATIMODULO")
      SELECT DATIMODULO 
 USE
    endif
    if USED ("CUR770")
      SELECT CUR770 
 USE
    endif
    if USED ("CUD2017")
      SELECT CUD2017 
 USE
    endif
    if USED ("CUR7702017")
      SELECT CUR7702017 
 USE
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,PcurDati,w_CUD2016_Temp,w_TipoGest)
    this.PcurDati=PcurDati
    this.w_CUD2016_Temp=w_CUD2016_Temp
    this.w_TipoGest=w_TipoGest
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='CONTI'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="PcurDati,w_CUD2016_Temp,w_TipoGest"
endproc
