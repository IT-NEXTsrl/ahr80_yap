* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_scr                                                        *
*              Stampa certificazioni                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_54]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-05-15                                                      *
* Last revis.: 2011-04-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsri_scr",oParentObject))

* --- Class definition
define class tgsri_scr as StdForm
  Top    = 17
  Left   = 38

  * --- Standard Properties
  Width  = 536
  Height = 215
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-04-26"
  HelpContextID=90830441
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=18

  * --- Constant Properties
  _IDX = 0
  CONTI_IDX = 0
  VALUTE_IDX = 0
  cPrg = "gsri_scr"
  cComment = "Stampa certificazioni"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_ANNO = space(4)
  w_DATINI = ctod('  /  /  ')
  o_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  o_DATFIN = ctod('  /  /  ')
  w_TIPCON = space(1)
  w_PERCIN = space(15)
  w_DESCINI = space(40)
  w_PERCFIN = space(15)
  w_DESCFIN = space(40)
  w_CODVAL = space(3)
  w_decimi = 0
  w_RITE = space(1)
  w_TIPCLF = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_CAMBIO = 0
  w_STAMPA = space(1)
  w_DESCTRIB = space(1)
  w_NOSTANOTE = space(1)
  w_SIMVAL = space(5)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsri_scrPag1","gsri_scr",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDATINI_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='VALUTE'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_ANNO=space(4)
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_TIPCON=space(1)
      .w_PERCIN=space(15)
      .w_DESCINI=space(40)
      .w_PERCFIN=space(15)
      .w_DESCFIN=space(40)
      .w_CODVAL=space(3)
      .w_decimi=0
      .w_RITE=space(1)
      .w_TIPCLF=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_CAMBIO=0
      .w_STAMPA=space(1)
      .w_DESCTRIB=space(1)
      .w_NOSTANOTE=space(1)
      .w_SIMVAL=space(5)
        .w_ANNO = STR(((YEAR(i_DATSYS))-1),4,0)
        .w_DATINI = cp_CharToDate("01-01-"+.w_ANNO)
        .w_DATFIN = cp_CharToDate("31-12-"+ALLTRIM(STR(YEAR(.w_DATINI))))
        .w_TIPCON = 'F'
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_PERCIN))
          .link_1_5('Full')
        endif
        .DoRTCalc(6,7,.f.)
        if not(empty(.w_PERCFIN))
          .link_1_7('Full')
        endif
          .DoRTCalc(8,8,.f.)
        .w_CODVAL = g_PERVAL
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_CODVAL))
          .link_1_9('Full')
        endif
          .DoRTCalc(10,12,.f.)
        .w_OBTEST = i_INIDAT
        .w_CAMBIO = GETCAM(.w_CODVAL, .w_DATFIN)
        .w_STAMPA = 'S'
        .w_DESCTRIB = 'S'
        .w_NOSTANOTE = 'N'
    endwith
    this.DoRTCalc(18,18,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_24.enabled = this.oPgFrm.Page1.oPag.oBtn_1_24.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_25.enabled = this.oPgFrm.Page1.oPag.oBtn_1_25.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
        if .o_DATINI<>.w_DATINI
            .w_DATFIN = cp_CharToDate("31-12-"+ALLTRIM(STR(YEAR(.w_DATINI))))
        endif
        .DoRTCalc(4,8,.t.)
            .w_CODVAL = g_PERVAL
          .link_1_9('Full')
        .DoRTCalc(10,13,.t.)
        if .o_DATFIN<>.w_DATFIN
            .w_CAMBIO = GETCAM(.w_CODVAL, .w_DATFIN)
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(15,18,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PERCIN
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PERCIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFR',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_PERCIN)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU,ANTIPCLF";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_PERCIN))
          select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU,ANTIPCLF;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PERCIN)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_PERCIN)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU,ANTIPCLF";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_PERCIN)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU,ANTIPCLF;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PERCIN) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oPERCIN_1_5'),i_cWhere,'GSAR_AFR',"Elenco fornitori/collaboratori",'GSRI_AMR.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU,ANTIPCLF";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU,ANTIPCLF;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice fornitore inesistente o obsoleto o non soggetto a ritenute o no agente")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU,ANTIPCLF";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU,ANTIPCLF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PERCIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU,ANTIPCLF";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_PERCIN);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_PERCIN)
            select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU,ANTIPCLF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PERCIN = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCINI = NVL(_Link_.ANDESCRI,space(40))
      this.w_RITE = NVL(_Link_.ANRITENU,space(1))
      this.w_TIPCLF = NVL(_Link_.ANTIPCLF,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PERCIN = space(15)
      endif
      this.w_DESCINI = space(40)
      this.w_RITE = space(1)
      this.w_TIPCLF = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((EMPTY(.w_PERCFIN)) OR (.w_PERCIN<=.w_PERCFIN)) AND (.w_RITE $ 'CS' OR .w_TIPCLF='A')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice fornitore inesistente o obsoleto o non soggetto a ritenute o no agente")
        endif
        this.w_PERCIN = space(15)
        this.w_DESCINI = space(40)
        this.w_RITE = space(1)
        this.w_TIPCLF = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PERCIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PERCFIN
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PERCFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFR',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_PERCFIN)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU,ANTIPCLF";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_PERCFIN))
          select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU,ANTIPCLF;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PERCFIN)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_PERCFIN)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU,ANTIPCLF";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_PERCFIN)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU,ANTIPCLF;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PERCFIN) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oPERCFIN_1_7'),i_cWhere,'GSAR_AFR',"Elenco fornitori/collaboratori",'GSRI_AMR.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU,ANTIPCLF";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU,ANTIPCLF;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice fornitore inesistente o obsoleto o non soggetto a ritenute o no agente")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU,ANTIPCLF";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU,ANTIPCLF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PERCFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU,ANTIPCLF";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_PERCFIN);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_PERCFIN)
            select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU,ANTIPCLF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PERCFIN = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCFIN = NVL(_Link_.ANDESCRI,space(40))
      this.w_RITE = NVL(_Link_.ANRITENU,space(1))
      this.w_TIPCLF = NVL(_Link_.ANTIPCLF,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PERCFIN = space(15)
      endif
      this.w_DESCFIN = space(40)
      this.w_RITE = space(1)
      this.w_TIPCLF = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((.w_PERCFIN>=.w_PERCIN) OR (EMPTY(.w_PERCIN))) AND (.w_RITE $ 'CS' OR .w_TIPCLF='A')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice fornitore inesistente o obsoleto o non soggetto a ritenute o no agente")
        endif
        this.w_PERCFIN = space(15)
        this.w_DESCFIN = space(40)
        this.w_RITE = space(1)
        this.w_TIPCLF = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PERCFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODVAL
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT,VASIMVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_CODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_CODVAL)
            select VACODVAL,VADECTOT,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_decimi = NVL(_Link_.VADECTOT,0)
      this.w_SIMVAL = NVL(_Link_.VASIMVAL,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_CODVAL = space(3)
      endif
      this.w_decimi = 0
      this.w_SIMVAL = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_2.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_2.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_3.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_3.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oPERCIN_1_5.value==this.w_PERCIN)
      this.oPgFrm.Page1.oPag.oPERCIN_1_5.value=this.w_PERCIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCINI_1_6.value==this.w_DESCINI)
      this.oPgFrm.Page1.oPag.oDESCINI_1_6.value=this.w_DESCINI
    endif
    if not(this.oPgFrm.Page1.oPag.oPERCFIN_1_7.value==this.w_PERCFIN)
      this.oPgFrm.Page1.oPag.oPERCFIN_1_7.value=this.w_PERCFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCFIN_1_8.value==this.w_DESCFIN)
      this.oPgFrm.Page1.oPag.oDESCFIN_1_8.value=this.w_DESCFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oSTAMPA_1_20.RadioValue()==this.w_STAMPA)
      this.oPgFrm.Page1.oPag.oSTAMPA_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCTRIB_1_21.RadioValue()==this.w_DESCTRIB)
      this.oPgFrm.Page1.oPag.oDESCTRIB_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNOSTANOTE_1_22.RadioValue()==this.w_NOSTANOTE)
      this.oPgFrm.Page1.oPag.oNOSTANOTE_1_22.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_DATINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATINI_1_2.SetFocus()
            i_bnoObbl = !empty(.w_DATINI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFIN_1_3.SetFocus()
            i_bnoObbl = !empty(.w_DATFIN)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(((EMPTY(.w_PERCFIN)) OR (.w_PERCIN<=.w_PERCFIN)) AND (.w_RITE $ 'CS' OR .w_TIPCLF='A'))  and not(empty(.w_PERCIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPERCIN_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice fornitore inesistente o obsoleto o non soggetto a ritenute o no agente")
          case   not(((.w_PERCFIN>=.w_PERCIN) OR (EMPTY(.w_PERCIN))) AND (.w_RITE $ 'CS' OR .w_TIPCLF='A'))  and not(empty(.w_PERCFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPERCFIN_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice fornitore inesistente o obsoleto o non soggetto a ritenute o no agente")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DATINI = this.w_DATINI
    this.o_DATFIN = this.w_DATFIN
    return

enddefine

* --- Define pages as container
define class tgsri_scrPag1 as StdContainer
  Width  = 532
  height = 215
  stdWidth  = 532
  stdheight = 215
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDATINI_1_2 as StdField with uid="AJPLUSMNTN",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data iniziale di stampa",;
    HelpContextID = 121336010,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=103, Top=13

  add object oDATFIN_1_3 as StdField with uid="THVHQBHVTE",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data finale di stampa",;
    HelpContextID = 42889418,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=103, Top=42

  add object oPERCIN_1_5 as StdField with uid="EJINRRSXZZ",rtseq=5,rtrep=.f.,;
    cFormVar = "w_PERCIN", cQueryName = "PERCIN",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice fornitore inesistente o obsoleto o non soggetto a ritenute o no agente",;
    ToolTipText = "Percipiente iniziale selezionato",;
    HelpContextID = 43093002,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=103, Top=71, cSayPict="p_FOR", cGetPict="p_FOR", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_AFR", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_PERCIN"

  func oPERCIN_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oPERCIN_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPERCIN_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oPERCIN_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFR',"Elenco fornitori/collaboratori",'GSRI_AMR.CONTI_VZM',this.parent.oContained
  endproc
  proc oPERCIN_1_5.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_PERCIN
     i_obj.ecpSave()
  endproc

  add object oDESCINI_1_6 as StdField with uid="NRSHMRNVFH",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DESCINI", cQueryName = "DESCINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 225346358,;
   bGlobalFont=.t.,;
    Height=21, Width=268, Left=229, Top=71, InputMask=replicate('X',40)

  add object oPERCFIN_1_7 as StdField with uid="YNERFBBRMX",rtseq=7,rtrep=.f.,;
    cFormVar = "w_PERCFIN", cQueryName = "PERCFIN",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice fornitore inesistente o obsoleto o non soggetto a ritenute o no agente",;
    ToolTipText = "Percipiente finale selezionato",;
    HelpContextID = 130124810,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=103, Top=100, cSayPict="p_FOR", cGetPict="p_FOR", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_AFR", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_PERCFIN"

  func oPERCFIN_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oPERCFIN_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPERCFIN_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oPERCFIN_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFR',"Elenco fornitori/collaboratori",'GSRI_AMR.CONTI_VZM',this.parent.oContained
  endproc
  proc oPERCFIN_1_7.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_PERCFIN
     i_obj.ecpSave()
  endproc

  add object oDESCFIN_1_8 as StdField with uid="FKPPKWMZNW",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DESCFIN", cQueryName = "DESCFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 130120906,;
   bGlobalFont=.t.,;
    Height=21, Width=268, Left=229, Top=100, InputMask=replicate('X',40)


  add object oSTAMPA_1_20 as StdCombo with uid="TVZSYPRRTP",rtseq=15,rtrep=.f.,left=103,top=131,width=142,height=21;
    , ToolTipText = "Tipo di stampa selezionata";
    , HelpContextID = 253267162;
    , cFormVar="w_STAMPA",RowSource=""+"Sintetica,"+"Dettagliata uso interno,"+"Dettagliata uso esterno", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSTAMPA_1_20.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    iif(this.value =3,'U',;
    space(1)))))
  endfunc
  func oSTAMPA_1_20.GetRadio()
    this.Parent.oContained.w_STAMPA = this.RadioValue()
    return .t.
  endfunc

  func oSTAMPA_1_20.SetRadio()
    this.Parent.oContained.w_STAMPA=trim(this.Parent.oContained.w_STAMPA)
    this.value = ;
      iif(this.Parent.oContained.w_STAMPA=='S',1,;
      iif(this.Parent.oContained.w_STAMPA=='D',2,;
      iif(this.Parent.oContained.w_STAMPA=='U',3,;
      0)))
  endfunc

  add object oDESCTRIB_1_21 as StdCheck with uid="QHJLASXRSO",rtseq=16,rtrep=.f.,left=250, top=131, caption="Stampa descrizione codice tributo",;
    ToolTipText = "Se attivo stampa descrizione codice tributo",;
    HelpContextID = 35554168,;
    cFormVar="w_DESCTRIB", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDESCTRIB_1_21.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oDESCTRIB_1_21.GetRadio()
    this.Parent.oContained.w_DESCTRIB = this.RadioValue()
    return .t.
  endfunc

  func oDESCTRIB_1_21.SetRadio()
    this.Parent.oContained.w_DESCTRIB=trim(this.Parent.oContained.w_DESCTRIB)
    this.value = ;
      iif(this.Parent.oContained.w_DESCTRIB=='S',1,;
      0)
  endfunc

  add object oNOSTANOTE_1_22 as StdCheck with uid="JROIAVWAIP",rtseq=17,rtrep=.f.,left=103, top=159, caption="No stampa note",;
    ToolTipText = "Se attivo non riporta le note nella stampa",;
    HelpContextID = 50359686,;
    cFormVar="w_NOSTANOTE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oNOSTANOTE_1_22.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oNOSTANOTE_1_22.GetRadio()
    this.Parent.oContained.w_NOSTANOTE = this.RadioValue()
    return .t.
  endfunc

  func oNOSTANOTE_1_22.SetRadio()
    this.Parent.oContained.w_NOSTANOTE=trim(this.Parent.oContained.w_NOSTANOTE)
    this.value = ;
      iif(this.Parent.oContained.w_NOSTANOTE=='S',1,;
      0)
  endfunc


  add object oBtn_1_24 as StdButton with uid="ZRSHEBJNTU",left=424, top=164, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire la stampa";
    , HelpContextID = 115478;
    , caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_24.Click()
      with this.Parent.oContained
        do GSRI_BCR with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_25 as StdButton with uid="WVEPMTDFUM",left=476, top=164, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 115478;
    , caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_25.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_14 as StdString with uid="YSDIKDREPG",Visible=.t., Left=6, Top=71,;
    Alignment=1, Width=94, Height=15,;
    Caption="Da percipiente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="EDEPJJZBOC",Visible=.t., Left=6, Top=100,;
    Alignment=1, Width=94, Height=15,;
    Caption="A percipiente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="PKSWZAXSWG",Visible=.t., Left=6, Top=13,;
    Alignment=1, Width=94, Height=15,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="ZJKKORFSOZ",Visible=.t., Left=6, Top=42,;
    Alignment=1, Width=94, Height=15,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="LLTKTLGBRZ",Visible=.t., Left=6, Top=131,;
    Alignment=1, Width=94, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsri_scr','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
