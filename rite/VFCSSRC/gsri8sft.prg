* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri8sft                                                        *
*              Generazione file telematico 770/2008                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-06-11                                                      *
* Last revis.: 2009-12-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- gsri8sft
g_LoadFuncButton=.F.
* --- Fine Area Manuale
return(createobject("tgsri8sft",oParentObject))

* --- Class definition
define class tgsri8sft as StdForm
  Top    = 4
  Left   = 23

  * --- Standard Properties
  Width  = 751
  Height = 562+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-12-02"
  HelpContextID=43054697
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=167

  * --- Constant Properties
  _IDX = 0
  VALUTE_IDX = 0
  CONTI_IDX = 0
  AZIENDA_IDX = 0
  ESERCIZI_IDX = 0
  SEDIAZIE_IDX = 0
  TITOLARI_IDX = 0
  DAT_RAPP_IDX = 0
  cPrg = "gsri8sft"
  cComment = "Generazione file telematico 770/2008"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODAZI = space(5)
  o_CODAZI = space(5)
  w_PERAZI = space(1)
  o_PERAZI = space(1)
  w_ROWNUM = 0
  w_PERFIS = space(5)
  w_TIPSL = space(2)
  w_SEDILEG = space(5)
  w_TIPFS = space(2)
  w_SEDIFIS = space(5)
  w_CodFisAzi = space(16)
  w_PERAZI2 = space(1)
  w_FOCOGNOME = space(24)
  o_FOCOGNOME = space(24)
  w_FONOME = space(20)
  o_FONOME = space(20)
  w_FODENOMINA = space(60)
  o_FODENOMINA = space(60)
  w_FOCODFIS = space(16)
  o_FOCODFIS = space(16)
  w_FOCODSOS = space(16)
  w_FLAGEURO = space(1)
  w_FLAGEVE = space(2)
  o_FLAGEVE = space(2)
  w_CODEVECC = space(1)
  w_FLAGCONF = space(1)
  w_Forza1 = .F.
  o_Forza1 = .F.
  w_FLAGCOR = space(1)
  o_FLAGCOR = space(1)
  w_FLAGINT = space(1)
  o_FLAGINT = space(1)
  w_TIPOPERAZ = space(1)
  w_AIDATINV = ctod('  /  /  ')
  w_DATANASC = ctod('  /  /  ')
  o_DATANASC = ctod('  /  /  ')
  w_COMUNE = space(40)
  o_COMUNE = space(40)
  w_SIGLA = space(2)
  o_SIGLA = space(2)
  w_SESSO = space(1)
  o_SESSO = space(1)
  w_VARRES = ctod('  /  /  ')
  w_RECOMUNE = space(40)
  o_RECOMUNE = space(40)
  w_RESIGLA = space(2)
  o_RESIGLA = space(2)
  w_RECODCOM = space(4)
  w_INDIRIZ = space(35)
  o_INDIRIZ = space(35)
  w_CAP = space(5)
  o_CAP = space(5)
  w_CODATT = space(6)
  w_TELEFONO = space(12)
  w_SEVARSED = ctod('  /  /  ')
  w_SECOMUNE = space(40)
  o_SECOMUNE = space(40)
  w_SESIGLA = space(2)
  o_SESIGLA = space(2)
  w_SEINDIRI2 = space(35)
  o_SEINDIRI2 = space(35)
  w_SECAP = space(5)
  o_SECAP = space(5)
  w_SECODCOM = space(4)
  w_SEVARDOM = ctod('  /  /  ')
  w_SERCOMUN = space(40)
  o_SERCOMUN = space(40)
  w_SERSIGLA = space(2)
  o_SERSIGLA = space(2)
  w_SERCODCOM = space(4)
  w_SERCAP = space(5)
  o_SERCAP = space(5)
  w_SERINDIR = space(35)
  o_SERINDIR = space(35)
  w_SECODATT = space(6)
  w_SETELEFONO = space(12)
  w_NATGIU = space(2)
  w_STATO = space(1)
  w_SITUAZ = space(1)
  w_CODFISDA = space(11)
  w_ANNO = space(4)
  w_ESERCIZIO = space(4)
  w_VALUTAESE = space(3)
  w_VALUTA = space(1)
  w_CODVAL = space(3)
  w_decimi = 0
  w_TIPCON = space(1)
  w_RITE = space(1)
  w_TIPCLF = space(1)
  w_COGNOME = space(24)
  w_NOME = space(20)
  o_NOME = space(20)
  w_DENOMINA = space(60)
  w_ONLUS = space(1)
  w_SETATT = space(2)
  w_Forza2 = .F.
  o_Forza2 = .F.
  w_RAPFIRM = space(5)
  o_RAPFIRM = space(5)
  w_RFCODFIS = space(16)
  w_RFCODCAR = space(2)
  o_RFCODCAR = space(2)
  w_RFDENOMI = space(60)
  o_RFDENOMI = space(60)
  w_RFCOGNOME = space(24)
  o_RFCOGNOME = space(24)
  w_RFNOME = space(20)
  w_RFSESSO = space(1)
  w_RFDATANASC = ctod('  /  /  ')
  w_RFCOMNAS = space(40)
  w_RFSIGNAS = space(2)
  w_RFCOMUNE = space(40)
  w_RFSIGLA = space(2)
  w_RFCAP = space(5)
  w_RFINDIRIZ = space(35)
  w_RFTELEFONO = space(12)
  w_RFDATCAR = ctod('  /  /  ')
  w_RFDATFAL = ctod('  /  /  ')
  w_SEZ1 = space(1)
  o_SEZ1 = space(1)
  w_qSS1 = space(1)
  w_qST1 = space(1)
  w_qSX1 = space(1)
  w_q771 = space(1)
  w_SEZ2 = space(1)
  o_SEZ2 = space(1)
  w_qSS12 = space(1)
  w_SEZ3 = space(1)
  w_qSS13 = space(1)
  w_qST13 = space(1)
  w_qSX13 = space(1)
  w_q773 = space(1)
  w_SEZ4 = space(1)
  o_SEZ4 = space(1)
  w_qSS14 = space(1)
  w_qST14 = space(1)
  w_qSX14 = space(1)
  w_q774 = space(1)
  w_RFCFINS2 = space(13)
  w_NUMCERTIF2 = 0
  w_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  o_DATFIN = ctod('  /  /  ')
  w_DirName = space(200)
  w_FileName = space(30)
  w_IDESTA = space(17)
  w_PROIFT = space(6)
  w_TIPFORN = space(2)
  o_TIPFORN = space(2)
  w_FODATANASC = ctod('  /  /  ')
  w_FOSESSO = space(1)
  w_FOCOMUNE = space(40)
  w_FOSIGLA = space(2)
  w_FORECOMUNE = space(40)
  w_FORESIGLA = space(2)
  w_FOCAP = space(5)
  w_FOINDIRIZ = space(35)
  w_FOSECOMUNE = space(40)
  w_FOSESIGLA = space(2)
  w_FOSECAP = space(5)
  w_FOSEINDIRI2 = space(35)
  w_FOSERCOMUN = space(40)
  w_FOSERSIGLA = space(2)
  w_FOSERCAP = space(5)
  w_FOSERINDIR = space(35)
  w_SELCAF = space(10)
  o_SELCAF = space(10)
  w_CFRESCAF = space(16)
  w_CFDELCAF = space(11)
  w_FLCONFCF = space(1)
  w_FLAGFIR = space(1)
  w_CODAZI = space(5)
  w_PERCIN = space(15)
  w_DESCINI = space(40)
  w_PERCFIN = space(15)
  w_DESCFIN = space(40)
  w_OBTEST = ctod('  /  /  ')
  w_FIRMDICH = space(1)
  w_FIRMPRES = space(1)
  w_CFINCCON = space(16)
  o_CFINCCON = space(16)
  w_INVAVTEL = space(1)
  o_INVAVTEL = space(1)
  w_CODSOGG = space(1)
  w_NUMCAF = 0
  w_RFDATIMP = ctod('  /  /  ')
  w_CODFISIN = space(16)
  o_CODFISIN = space(16)
  w_IMPTRTEL = space(1)
  o_IMPTRTEL = space(1)
  w_FIRMINT = space(1)
  w_RICAVTEL = space(1)
  w_CFDOMNOT = space(16)
  w_UFDOMNOT = space(60)
  w_CODOMNOT = space(24)
  w_NODOMNOT = space(20)
  w_CMDOMNOT = space(40)
  w_PRDOMNOT = space(2)
  w_CCDOMNOT = space(4)
  w_CPDOMNOT = space(5)
  w_VPDOMNOT = space(15)
  w_INDOMNOT = space(35)
  w_NCDOMNOT = space(10)
  w_FRDOMNOT = space(35)
  w_SEDOMNOT = space(24)
  o_SEDOMNOT = space(24)
  w_CEDOMNOT = space(3)
  w_SFDOMNOT = space(24)
  w_LRDOMNOT = space(24)
  w_IEDOMNOT = space(35)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=5, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsri8sftPag1","gsri8sft",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pers.fis.\Altri sogg")
      .Pages(2).addobject("oPag","tgsri8sftPag2","gsri8sft",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Rapp.firmatario")
      .Pages(3).addobject("oPag","tgsri8sftPag3","gsri8sft",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Firma\Imp. trasmis.")
      .Pages(4).addobject("oPag","tgsri8sftPag4","gsri8sft",4)
      .Pages(4).oPag.Visible=.t.
      .Pages(4).Caption=cp_Translate("Fornitore file")
      .Pages(5).addobject("oPag","tgsri8sftPag5","gsri8sft",5)
      .Pages(5).oPag.Visible=.t.
      .Pages(5).Caption=cp_Translate("File telematico")
      .Pages(5).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oFOCOGNOME_1_11
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[7]
    this.cWorkTables[1]='VALUTE'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='AZIENDA'
    this.cWorkTables[4]='ESERCIZI'
    this.cWorkTables[5]='SEDIAZIE'
    this.cWorkTables[6]='TITOLARI'
    this.cWorkTables[7]='DAT_RAPP'
    return(this.OpenAllTables(7))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gsri8sft
    *this.opgfrm.tabstyle = 1
    this.opgfrm.page1.caption = ah_msgformat('Persone fisiche\Altri soggetti')
    this.opgfrm.page2.caption = ah_msgformat('Rappresentante firmatario')
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI=space(5)
      .w_PERAZI=space(1)
      .w_ROWNUM=0
      .w_PERFIS=space(5)
      .w_TIPSL=space(2)
      .w_SEDILEG=space(5)
      .w_TIPFS=space(2)
      .w_SEDIFIS=space(5)
      .w_CodFisAzi=space(16)
      .w_PERAZI2=space(1)
      .w_FOCOGNOME=space(24)
      .w_FONOME=space(20)
      .w_FODENOMINA=space(60)
      .w_FOCODFIS=space(16)
      .w_FOCODSOS=space(16)
      .w_FLAGEURO=space(1)
      .w_FLAGEVE=space(2)
      .w_CODEVECC=space(1)
      .w_FLAGCONF=space(1)
      .w_Forza1=.f.
      .w_FLAGCOR=space(1)
      .w_FLAGINT=space(1)
      .w_TIPOPERAZ=space(1)
      .w_AIDATINV=ctod("  /  /  ")
      .w_DATANASC=ctod("  /  /  ")
      .w_COMUNE=space(40)
      .w_SIGLA=space(2)
      .w_SESSO=space(1)
      .w_VARRES=ctod("  /  /  ")
      .w_RECOMUNE=space(40)
      .w_RESIGLA=space(2)
      .w_RECODCOM=space(4)
      .w_INDIRIZ=space(35)
      .w_CAP=space(5)
      .w_CODATT=space(6)
      .w_TELEFONO=space(12)
      .w_SEVARSED=ctod("  /  /  ")
      .w_SECOMUNE=space(40)
      .w_SESIGLA=space(2)
      .w_SEINDIRI2=space(35)
      .w_SECAP=space(5)
      .w_SECODCOM=space(4)
      .w_SEVARDOM=ctod("  /  /  ")
      .w_SERCOMUN=space(40)
      .w_SERSIGLA=space(2)
      .w_SERCODCOM=space(4)
      .w_SERCAP=space(5)
      .w_SERINDIR=space(35)
      .w_SECODATT=space(6)
      .w_SETELEFONO=space(12)
      .w_NATGIU=space(2)
      .w_STATO=space(1)
      .w_SITUAZ=space(1)
      .w_CODFISDA=space(11)
      .w_ANNO=space(4)
      .w_ESERCIZIO=space(4)
      .w_VALUTAESE=space(3)
      .w_VALUTA=space(1)
      .w_CODVAL=space(3)
      .w_decimi=0
      .w_TIPCON=space(1)
      .w_RITE=space(1)
      .w_TIPCLF=space(1)
      .w_COGNOME=space(24)
      .w_NOME=space(20)
      .w_DENOMINA=space(60)
      .w_ONLUS=space(1)
      .w_SETATT=space(2)
      .w_Forza2=.f.
      .w_RAPFIRM=space(5)
      .w_RFCODFIS=space(16)
      .w_RFCODCAR=space(2)
      .w_RFDENOMI=space(60)
      .w_RFCOGNOME=space(24)
      .w_RFNOME=space(20)
      .w_RFSESSO=space(1)
      .w_RFDATANASC=ctod("  /  /  ")
      .w_RFCOMNAS=space(40)
      .w_RFSIGNAS=space(2)
      .w_RFCOMUNE=space(40)
      .w_RFSIGLA=space(2)
      .w_RFCAP=space(5)
      .w_RFINDIRIZ=space(35)
      .w_RFTELEFONO=space(12)
      .w_RFDATCAR=ctod("  /  /  ")
      .w_RFDATFAL=ctod("  /  /  ")
      .w_SEZ1=space(1)
      .w_qSS1=space(1)
      .w_qST1=space(1)
      .w_qSX1=space(1)
      .w_q771=space(1)
      .w_SEZ2=space(1)
      .w_qSS12=space(1)
      .w_SEZ3=space(1)
      .w_qSS13=space(1)
      .w_qST13=space(1)
      .w_qSX13=space(1)
      .w_q773=space(1)
      .w_SEZ4=space(1)
      .w_qSS14=space(1)
      .w_qST14=space(1)
      .w_qSX14=space(1)
      .w_q774=space(1)
      .w_RFCFINS2=space(13)
      .w_NUMCERTIF2=0
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_DirName=space(200)
      .w_FileName=space(30)
      .w_IDESTA=space(17)
      .w_PROIFT=space(6)
      .w_TIPFORN=space(2)
      .w_FODATANASC=ctod("  /  /  ")
      .w_FOSESSO=space(1)
      .w_FOCOMUNE=space(40)
      .w_FOSIGLA=space(2)
      .w_FORECOMUNE=space(40)
      .w_FORESIGLA=space(2)
      .w_FOCAP=space(5)
      .w_FOINDIRIZ=space(35)
      .w_FOSECOMUNE=space(40)
      .w_FOSESIGLA=space(2)
      .w_FOSECAP=space(5)
      .w_FOSEINDIRI2=space(35)
      .w_FOSERCOMUN=space(40)
      .w_FOSERSIGLA=space(2)
      .w_FOSERCAP=space(5)
      .w_FOSERINDIR=space(35)
      .w_SELCAF=space(10)
      .w_CFRESCAF=space(16)
      .w_CFDELCAF=space(11)
      .w_FLCONFCF=space(1)
      .w_FLAGFIR=space(1)
      .w_CODAZI=space(5)
      .w_PERCIN=space(15)
      .w_DESCINI=space(40)
      .w_PERCFIN=space(15)
      .w_DESCFIN=space(40)
      .w_OBTEST=ctod("  /  /  ")
      .w_FIRMDICH=space(1)
      .w_FIRMPRES=space(1)
      .w_CFINCCON=space(16)
      .w_INVAVTEL=space(1)
      .w_CODSOGG=space(1)
      .w_NUMCAF=0
      .w_RFDATIMP=ctod("  /  /  ")
      .w_CODFISIN=space(16)
      .w_IMPTRTEL=space(1)
      .w_FIRMINT=space(1)
      .w_RICAVTEL=space(1)
      .w_CFDOMNOT=space(16)
      .w_UFDOMNOT=space(60)
      .w_CODOMNOT=space(24)
      .w_NODOMNOT=space(20)
      .w_CMDOMNOT=space(40)
      .w_PRDOMNOT=space(2)
      .w_CCDOMNOT=space(4)
      .w_CPDOMNOT=space(5)
      .w_VPDOMNOT=space(15)
      .w_INDOMNOT=space(35)
      .w_NCDOMNOT=space(10)
      .w_FRDOMNOT=space(35)
      .w_SEDOMNOT=space(24)
      .w_CEDOMNOT=space(3)
      .w_SFDOMNOT=space(24)
      .w_LRDOMNOT=space(24)
      .w_IEDOMNOT=space(35)
        .w_CODAZI = i_codazi
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODAZI))
          .link_1_1('Full')
        endif
          .DoRTCalc(2,2,.f.)
        .w_ROWNUM = 1
        .w_PERFIS = iif(.w_PERAZI='S',i_CODAZI,' ')
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_PERFIS))
          .link_1_4('Full')
        endif
        .w_TIPSL = 'SL'
        .w_SEDILEG = iif(.w_PERAZI<>'S',i_CODAZI,' ')
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_SEDILEG))
          .link_1_6('Full')
        endif
        .w_TIPFS = 'DF'
        .w_SEDIFIS = iif(.w_PERAZI<>'S',i_CODAZI,' ')
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_SEDIFIS))
          .link_1_8('Full')
        endif
          .DoRTCalc(9,9,.f.)
        .w_PERAZI2 = IsPerFis(.w_FOCOGNOME,.w_FONOME,.w_FODENOMINA)
        .w_FOCOGNOME = iif(.w_Perazi='S',left(.w_FOCOGNOME+SPACE(24),24),space(24))
        .w_FONOME = iif(.w_Perazi='S',left(.w_FONOME+SPACE(20),20),space(20))
        .w_FODENOMINA = iif(.w_Perazi<>'S',left(.w_FODENOMINA+space(60),60),space(60))
        .w_FOCODFIS = iif(empty(.w_FOCODFIS),.w_CodFisAzi,.w_FOCODFIS)
          .DoRTCalc(15,15,.f.)
        .w_FLAGEURO = '1'
        .w_FLAGEVE = '00'
        .w_CODEVECC = iif(.w_FLAGEVE='01',.w_CODEVECC,'0')
        .w_FLAGCONF = '0'
        .w_Forza1 = iif(.w_Perazi<>'S',.w_Forza1,.f.)
        .w_FLAGCOR = iif(.w_FLAGINT='1','0',.w_FLAGCOR)
        .w_FLAGINT = iif(.w_FLAGCOR='1','0',.w_FLAGINT)
        .w_TIPOPERAZ = ' '
        .w_AIDATINV = i_DATSYS
        .w_DATANASC = iif(.w_Perazi='S' or .w_Forza1,.w_DATANASC,CTOD('  -  -  '))
        .w_COMUNE = iif(.w_Perazi='S' or .w_Forza1,left(.w_COMUNE+space(40),40),space(40))
        .w_SIGLA = iif(.w_Perazi='S' or .w_Forza1,.w_SIGLA,'  ')
        .w_SESSO = iif(.w_Perazi='S' or .w_Forza1,.w_SESSO,' ')
        .w_VARRES = iif(.w_Perazi='S' or .w_Forza1,.w_VARRES,CTOD('  -  -  '))
        .w_RECOMUNE = iif(.w_Perazi='S' or .w_Forza1,left(.w_RECOMUNE+space(40),40),space(40))
        .w_RESIGLA = iif(.w_Perazi='S' or .w_Forza1,.w_RESIGLA,'  ')
        .w_RECODCOM = iif(.w_Perazi='S' or .w_Forza1,.w_RECODCOM,Space(4))
        .w_INDIRIZ = iif(.w_Perazi='S' or .w_Forza1,left(.w_INDIRIZ+space(35),35),space(35))
        .w_CAP = iif(.w_Perazi='S' or .w_Forza1,.w_CAP,space(5))
        .w_CODATT = iif(.w_Perazi='S' or .w_Forza1,IIF(EMPTY(.w_CODATT),IIF(g_ATTIVI='S', space(6), LEFT(Alltrim(CALATTSO(g_CATAZI,Alltrim(STR(year(i_DATSYS))),12)),6)),.w_CODATT),SPACE(5))
        .w_TELEFONO = iif(.w_Perazi='S' or .w_Forza1,left(.w_TELEFONO,12),space(12))
        .w_SEVARSED = iif(.w_Perazi<>'S' or .w_Forza1,.w_SEVARSED,CTOD('  -  -  '))
        .w_SECOMUNE = iif(.w_Perazi<>'S' or .w_Forza1,left(.w_SECOMUNE+space(40),40),space(40))
        .w_SESIGLA = iif(.w_Perazi<>'S' or .w_Forza1,.w_SESIGLA,'  ')
        .w_SEINDIRI2 = iif(.w_Perazi<>'S' or .w_Forza1,left(.w_SEINDIRI2+space(35),35),space(35))
        .w_SECAP = iif(.w_Perazi<>'S' or .w_Forza1,.w_SECAP,space(5))
        .w_SECODCOM = iif(.w_Perazi<>'S' or .w_Forza1,.w_SECODCOM,Space(4))
        .w_SEVARDOM = iif(.w_Perazi<>'S' or .w_Forza1,.w_SEVARDOM,CTOD('  -  -  '))
        .w_SERCOMUN = iif(.w_Perazi<>'S' or .w_Forza1,left(.w_SERCOMUN+space(40),40),space(40))
        .w_SERSIGLA = iif(.w_Perazi<>'S' or .w_Forza1,.w_SERSIGLA,'  ')
        .w_SERCODCOM = iif(.w_Perazi<>'S' or .w_Forza1,.w_SERCODCOM,Space(4))
        .w_SERCAP = iif(.w_Perazi<>'S' or .w_Forza1,.w_SERCAP,space(5))
        .w_SERINDIR = iif(.w_Perazi<>'S' or .w_Forza1,left(.w_SERINDIR+space(35),35),space(35))
        .w_SECODATT = iif(.w_Perazi<>'S' or .w_Forza1,IIF(EMPTY(.w_SECODATT),IIF(g_ATTIVI='S', space(6), LEFT(Alltrim(CALATTSO(g_CATAZI,Alltrim(STR(year(i_DATSYS))),12)),6)),.w_SECODATT),space(5))
        .w_SETELEFONO = iif(.w_Perazi<>'S' or .w_Forza1,left(.w_SETELEFONO,12),space(12))
        .w_NATGIU = iif(.w_Perazi<>'S' or .w_Forza1,NATGIU(),'  ')
        .w_STATO = iif(.w_Perazi<>'S' or .w_Forza1,.w_STATO,' ')
        .w_SITUAZ = iif(.w_Perazi<>'S' or .w_Forza1,.w_SITUAZ,' ')
        .w_CODFISDA = iif(.w_Perazi<>'S' or .w_Forza1,.w_CODFISDA,space(11))
        .w_ANNO = STR(YEAR(i_DATSYS),4,0)
        .w_ESERCIZIO = calceser(.w_DATFIN)
        .DoRTCalc(56,56,.f.)
        if not(empty(.w_ESERCIZIO))
          .link_1_56('Full')
        endif
          .DoRTCalc(57,57,.f.)
        .w_VALUTA = IIF(.w_VALUTAESE=g_CODLIR,'L','E')
        .w_CODVAL = iif(.w_flageuro='0',g_codlir,g_codeur)
        .DoRTCalc(59,59,.f.)
        if not(empty(.w_CODVAL))
          .link_1_59('Full')
        endif
          .DoRTCalc(60,60,.f.)
        .w_TIPCON = 'F'
          .DoRTCalc(62,66,.f.)
        .w_ONLUS = iif(.w_Perazi<>'S' or .w_Forza1,.w_ONLUS,' ')
        .w_SETATT = iif(.w_Perazi<>'S' or .w_Forza1,.w_SETATT,'  ')
        .w_Forza2 = iif(.w_Perazi<>'S',.w_Forza2,.f.)
        .w_RAPFIRM = i_CODAZI
        .DoRTCalc(70,70,.f.)
        if not(empty(.w_RAPFIRM))
          .link_2_2('Full')
        endif
        .w_RFCODFIS = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFCODFIS,space(16))
        .w_RFCODCAR = iif(.w_Perazi<>'S' or .w_Forza2,'1','  ')
        .w_RFDENOMI = iif(empty(.w_RFCOGNOME) and (.w_Perazi<>'S' or .w_Forza2),.w_RFDENOMI,space(60))
        .w_RFCOGNOME = iif(empty(.w_RFDENOMI) and (.w_Perazi<>'S' or .w_Forza2) ,.w_RFCOGNOME,space(24))
        .w_RFNOME = iif(empty(.w_RFDENOMI) and (.w_Perazi<>'S' or .w_Forza2),.w_RFNOME,space(20))
        .w_RFSESSO = iif(empty(.w_RFDENOMI) and (.w_Perazi<>'S' or .w_Forza2),.w_RFSESSO,' ')
        .w_RFDATANASC = iif(empty(.w_RFDENOMI) and (.w_Perazi<>'S' or .w_Forza2),.w_RFDATANASC,CTOD('  -  -  '))
        .w_RFCOMNAS = iif(empty(.w_RFDENOMI) and (.w_Perazi<>'S' or .w_Forza2),.w_RFCOMNAS,space(40))
        .w_RFSIGNAS = iif(empty(.w_RFDENOMI) and (.w_Perazi<>'S' or .w_Forza2),.w_RFSIGNAS,'  ')
        .w_RFCOMUNE = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFCOMUNE,space(40))
        .w_RFSIGLA = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFSIGLA,'  ')
        .w_RFCAP = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFCAP,space(5))
        .w_RFINDIRIZ = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFINDIRIZ,space(35))
        .w_RFTELEFONO = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFTELEFONO,space(12))
        .w_RFDATCAR = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFDATCAR,CTOD('  -  -  '))
        .w_RFDATFAL = iif(inlist(.w_RFCODCAR,'3','4'),.w_RFDATFAL,ctod('  -  -    '))
        .w_SEZ1 = iif(.w_SEZ2='1' or .w_SEZ4='1','0','1')
        .w_qSS1 = iif(.w_SEZ1='1',.w_qSS1,'0')
        .w_qST1 = iif(.w_SEZ1='1','1','0')
        .w_qSX1 = iif(.w_SEZ1='1',.w_qSX1,'0')
        .w_q771 = '0'
        .w_SEZ2 = iif(.w_SEZ1='1' or .w_SEZ4='1','0','1')
        .w_qSS12 = iif(.w_SEZ2='1',.w_qSS12,'0')
        .w_SEZ3 = '0'
        .w_qSS13 = '0'
        .w_qST13 = '0'
        .w_qSX13 = '0'
        .w_q773 = '0'
        .w_SEZ4 = iif(.w_SEZ1='1' or .w_SEZ2='1','0','1')
        .w_qSS14 = iif(.w_SEZ4='1',.w_qSS14,'0')
        .w_qST14 = iif(.w_SEZ4='1','1','0')
        .w_qSX14 = iif(.w_SEZ4='1',.w_qSX14,'0')
        .w_q774 = '0'
        .w_RFCFINS2 = iif(.w_SEZ4='1',.w_RFCFINS2,space(13))
        .w_NUMCERTIF2 = 0
        .w_DATINI = CTOD("01-01-2007")
        .w_DATFIN = CTOD("31-12-2007")
        .w_DirName = sys(5)+sys(2003)+'\'
        .w_FileName = alltrim(.w_FOCODFIS)+'_77S08.77S'
        .w_IDESTA = ' '
        .w_PROIFT = ' '
        .w_TIPFORN = '01'
        .w_FODATANASC = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI='S',.w_DATANASC,CTOD('  -  -  '))
        .w_FOSESSO = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI='S',.w_SESSO,' ')
        .w_FOCOMUNE = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI='S',.w_COMUNE,space(40))
        .w_FOSIGLA = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI='S',.w_SIGLA,'  ')
        .w_FORECOMUNE = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI='S',.w_RECOMUNE,space(40))
        .w_FORESIGLA = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI='S',.w_RESIGLA,'  ')
        .w_FOCAP = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI='S',.w_CAP,space(5))
        .w_FOINDIRIZ = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI='S',.w_INDIRIZ,space(35))
        .w_FOSECOMUNE = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI<>'S',.w_SECOMUNE,space(40))
        .w_FOSESIGLA = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI<>'S',.w_SESIGLA,'  ')
        .w_FOSECAP = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI<>'S',.w_SECAP,space(5))
        .w_FOSEINDIRI2 = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI<>'S',.w_SEINDIRI2,space(35))
        .w_FOSERCOMUN = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI<>'S',.w_SERCOMUN,space(40))
        .w_FOSERSIGLA = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI<>'S',.w_SERSIGLA,'  ')
        .w_FOSERCAP = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI<>'S',.w_SERCAP,space(5))
        .w_FOSERINDIR = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI<>'S',.w_SERINDIR,space(35))
        .w_SELCAF = '1'
        .w_CFRESCAF = SPACE(16)
        .w_CFDELCAF = iif(.w_SELCAF <> '1',SPACE(11),.w_CFDELCAF)
        .w_FLCONFCF = '0'
        .w_FLAGFIR = '0'
        .w_CODAZI = i_codazi
        .DoRTCalc(135,135,.f.)
        if not(empty(.w_PERCIN))
          .link_5_24('Full')
        endif
        .DoRTCalc(136,137,.f.)
        if not(empty(.w_PERCFIN))
          .link_5_26('Full')
        endif
          .DoRTCalc(138,138,.f.)
        .w_OBTEST = .w_DATINI
        .w_FIRMDICH = iif(.w_SEZ1='1',.w_FIRMDICH,'0')
        .w_FIRMPRES = iif(.w_SEZ1='1',.w_FIRMPRES,'0')
          .DoRTCalc(142,142,.f.)
        .w_INVAVTEL = iif(.w_IMPTRTEL <> '0',.w_INVAVTEL,'0')
        .w_CODSOGG = iif(! empty(.w_CFINCCON),.w_CODSOGG,'0')
        .w_NUMCAF = 0
        .w_RFDATIMP = CTOD('  -  -  ')
        .w_CODFISIN = space(16)
        .w_IMPTRTEL = '0'
        .w_FIRMINT = '0'
        .w_RICAVTEL = iif(.w_INVAVTEL='1',.w_RICAVTEL,'0')
        .w_CFDOMNOT = SPACE(16)
        .w_UFDOMNOT = space(60)
        .w_CODOMNOT = space(24)
        .w_NODOMNOT = space(20)
        .w_CMDOMNOT = iif(! empty(.w_SEDOMNOT),space(40),.w_CMDOMNOT)
        .w_PRDOMNOT = iif(! empty(.w_SEDOMNOT),space(2),.w_PRDOMNOT)
        .w_CCDOMNOT = iif(! empty(.w_SEDOMNOT),space(4),.w_CCDOMNOT)
        .w_CPDOMNOT = iif(! empty(.w_SEDOMNOT),space(5),.w_CPDOMNOT)
        .w_VPDOMNOT = iif(! empty(.w_SEDOMNOT),space(15),.w_VPDOMNOT)
        .w_INDOMNOT = iif(! empty(.w_SEDOMNOT),space(35),.w_INDOMNOT)
        .w_NCDOMNOT = iif(! empty(.w_SEDOMNOT),space(10),.w_NCDOMNOT)
        .w_FRDOMNOT = iif(! empty(.w_SEDOMNOT),space(35),.w_FRDOMNOT)
        .w_SEDOMNOT = space(24)
        .w_CEDOMNOT = iif(empty(.w_SEDOMNOT),space(3),.w_CEDOMNOT)
        .w_SFDOMNOT = iif(empty(.w_SEDOMNOT),space(24),.w_SFDOMNOT)
        .w_LRDOMNOT = iif(empty(.w_SEDOMNOT),space(24),.w_LRDOMNOT)
        .w_IEDOMNOT = iif(empty(.w_SEDOMNOT),space(35),.w_IEDOMNOT)
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page5.oPag.oBtn_5_4.enabled = this.oPgFrm.Page5.oPag.oBtn_5_4.mCond()
    this.oPgFrm.Page5.oPag.oBtn_5_20.enabled = this.oPgFrm.Page5.oPag.oBtn_5_20.mCond()
    this.oPgFrm.Page5.oPag.oBtn_5_21.enabled = this.oPgFrm.Page5.oPag.oBtn_5_21.mCond()
    this.oPgFrm.Page5.oPag.oBtn_5_22.enabled = this.oPgFrm.Page5.oPag.oBtn_5_22.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        if .o_CODAZI<>.w_CODAZI
          .link_1_1('Full')
        endif
        .DoRTCalc(2,3,.t.)
        if .o_PERAZI<>.w_PERAZI
            .w_PERFIS = iif(.w_PERAZI='S',i_CODAZI,' ')
          .link_1_4('Full')
        endif
        .DoRTCalc(5,5,.t.)
        if .o_PERAZI<>.w_PERAZI
            .w_SEDILEG = iif(.w_PERAZI<>'S',i_CODAZI,' ')
          .link_1_6('Full')
        endif
        .DoRTCalc(7,7,.t.)
        if .o_PERAZI<>.w_PERAZI
            .w_SEDIFIS = iif(.w_PERAZI<>'S',i_CODAZI,' ')
          .link_1_8('Full')
        endif
        .DoRTCalc(9,9,.t.)
            .w_PERAZI2 = IsPerFis(.w_FOCOGNOME,.w_FONOME,.w_FODENOMINA)
        if .o_PERAZI<>.w_PERAZI
            .w_FOCOGNOME = iif(.w_Perazi='S',left(.w_FOCOGNOME+SPACE(24),24),space(24))
        endif
        if .o_PERAZI<>.w_PERAZI
            .w_FONOME = iif(.w_Perazi='S',left(.w_FONOME+SPACE(20),20),space(20))
        endif
        if .o_PERAZI<>.w_PERAZI
            .w_FODENOMINA = iif(.w_Perazi<>'S',left(.w_FODENOMINA+space(60),60),space(60))
        endif
        if .o_FOCODFIS<>.w_FOCODFIS
            .w_FOCODFIS = iif(empty(.w_FOCODFIS),.w_CodFisAzi,.w_FOCODFIS)
        endif
        .DoRTCalc(15,17,.t.)
        if .o_FLAGEVE<>.w_FLAGEVE
            .w_CODEVECC = iif(.w_FLAGEVE='01',.w_CODEVECC,'0')
        endif
        .DoRTCalc(19,19,.t.)
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA
            .w_Forza1 = iif(.w_Perazi<>'S',.w_Forza1,.f.)
        endif
        if .o_FLAGINT<>.w_FLAGINT
            .w_FLAGCOR = iif(.w_FLAGINT='1','0',.w_FLAGCOR)
        endif
        if .o_FLAGCOR<>.w_FLAGCOR
            .w_FLAGINT = iif(.w_FLAGCOR='1','0',.w_FLAGINT)
        endif
        if .o_FLAGCOR<>.w_FLAGCOR.or. .o_FLAGINT<>.w_FLAGINT
            .w_TIPOPERAZ = ' '
        endif
        .DoRTCalc(24,24,.t.)
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_DATANASC = iif(.w_Perazi='S' or .w_Forza1,.w_DATANASC,CTOD('  -  -  '))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_COMUNE = iif(.w_Perazi='S' or .w_Forza1,left(.w_COMUNE+space(40),40),space(40))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_SIGLA = iif(.w_Perazi='S' or .w_Forza1,.w_SIGLA,'  ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_NOME<>.w_NOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_SESSO = iif(.w_Perazi='S' or .w_Forza1,.w_SESSO,' ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_VARRES = iif(.w_Perazi='S' or .w_Forza1,.w_VARRES,CTOD('  -  -  '))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_RECOMUNE = iif(.w_Perazi='S' or .w_Forza1,left(.w_RECOMUNE+space(40),40),space(40))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_RESIGLA = iif(.w_Perazi='S' or .w_Forza1,.w_RESIGLA,'  ')
        endif
        if .o_Forza1<>.w_Forza1
            .w_RECODCOM = iif(.w_Perazi='S' or .w_Forza1,.w_RECODCOM,Space(4))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_INDIRIZ = iif(.w_Perazi='S' or .w_Forza1,left(.w_INDIRIZ+space(35),35),space(35))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_CAP = iif(.w_Perazi='S' or .w_Forza1,.w_CAP,space(5))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_CODATT = iif(.w_Perazi='S' or .w_Forza1,IIF(EMPTY(.w_CODATT),IIF(g_ATTIVI='S', space(6), LEFT(Alltrim(CALATTSO(g_CATAZI,Alltrim(STR(year(i_DATSYS))),12)),6)),.w_CODATT),SPACE(5))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_TELEFONO = iif(.w_Perazi='S' or .w_Forza1,left(.w_TELEFONO,12),space(12))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_SEVARSED = iif(.w_Perazi<>'S' or .w_Forza1,.w_SEVARSED,CTOD('  -  -  '))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_SECOMUNE = iif(.w_Perazi<>'S' or .w_Forza1,left(.w_SECOMUNE+space(40),40),space(40))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_SESIGLA = iif(.w_Perazi<>'S' or .w_Forza1,.w_SESIGLA,'  ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA
            .w_SEINDIRI2 = iif(.w_Perazi<>'S' or .w_Forza1,left(.w_SEINDIRI2+space(35),35),space(35))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_SECAP = iif(.w_Perazi<>'S' or .w_Forza1,.w_SECAP,space(5))
        endif
        if .o_Forza1<>.w_Forza1
            .w_SECODCOM = iif(.w_Perazi<>'S' or .w_Forza1,.w_SECODCOM,Space(4))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_SEVARDOM = iif(.w_Perazi<>'S' or .w_Forza1,.w_SEVARDOM,CTOD('  -  -  '))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_SERCOMUN = iif(.w_Perazi<>'S' or .w_Forza1,left(.w_SERCOMUN+space(40),40),space(40))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_SERSIGLA = iif(.w_Perazi<>'S' or .w_Forza1,.w_SERSIGLA,'  ')
        endif
        if .o_Forza1<>.w_Forza1
            .w_SERCODCOM = iif(.w_Perazi<>'S' or .w_Forza1,.w_SERCODCOM,Space(4))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_SERCAP = iif(.w_Perazi<>'S' or .w_Forza1,.w_SERCAP,space(5))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_SERINDIR = iif(.w_Perazi<>'S' or .w_Forza1,left(.w_SERINDIR+space(35),35),space(35))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_SECODATT = iif(.w_Perazi<>'S' or .w_Forza1,IIF(EMPTY(.w_SECODATT),IIF(g_ATTIVI='S', space(6), LEFT(Alltrim(CALATTSO(g_CATAZI,Alltrim(STR(year(i_DATSYS))),12)),6)),.w_SECODATT),space(5))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_SETELEFONO = iif(.w_Perazi<>'S' or .w_Forza1,left(.w_SETELEFONO,12),space(12))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_NATGIU = iif(.w_Perazi<>'S' or .w_Forza1,NATGIU(),'  ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_STATO = iif(.w_Perazi<>'S' or .w_Forza1,.w_STATO,' ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_SITUAZ = iif(.w_Perazi<>'S' or .w_Forza1,.w_SITUAZ,' ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_CODFISDA = iif(.w_Perazi<>'S' or .w_Forza1,.w_CODFISDA,space(11))
        endif
        .DoRTCalc(55,55,.t.)
        if .o_DATFIN<>.w_DATFIN
            .w_ESERCIZIO = calceser(.w_DATFIN)
          .link_1_56('Full')
        endif
        .DoRTCalc(57,57,.t.)
            .w_VALUTA = IIF(.w_VALUTAESE=g_CODLIR,'L','E')
            .w_CODVAL = iif(.w_flageuro='0',g_codlir,g_codeur)
          .link_1_59('Full')
        .DoRTCalc(60,66,.t.)
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_ONLUS = iif(.w_Perazi<>'S' or .w_Forza1,.w_ONLUS,' ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza1<>.w_Forza1
            .w_SETATT = iif(.w_Perazi<>'S' or .w_Forza1,.w_SETATT,'  ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA
            .w_Forza2 = iif(.w_Perazi<>'S',.w_Forza2,.f.)
        endif
        if .o_RAPFIRM<>.w_RAPFIRM
          .link_2_2('Full')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza2<>.w_Forza2
            .w_RFCODFIS = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFCODFIS,space(16))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza2<>.w_Forza2
            .w_RFCODCAR = iif(.w_Perazi<>'S' or .w_Forza2,'1','  ')
        endif
        if .o_RFCOGNOME<>.w_RFCOGNOME.or. .o_Forza2<>.w_Forza2
            .w_RFDENOMI = iif(empty(.w_RFCOGNOME) and (.w_Perazi<>'S' or .w_Forza2),.w_RFDENOMI,space(60))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza2<>.w_Forza2.or. .o_RFDENOMI<>.w_RFDENOMI
            .w_RFCOGNOME = iif(empty(.w_RFDENOMI) and (.w_Perazi<>'S' or .w_Forza2) ,.w_RFCOGNOME,space(24))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza2<>.w_Forza2.or. .o_RFDENOMI<>.w_RFDENOMI
            .w_RFNOME = iif(empty(.w_RFDENOMI) and (.w_Perazi<>'S' or .w_Forza2),.w_RFNOME,space(20))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza2<>.w_Forza2.or. .o_RFDENOMI<>.w_RFDENOMI
            .w_RFSESSO = iif(empty(.w_RFDENOMI) and (.w_Perazi<>'S' or .w_Forza2),.w_RFSESSO,' ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza2<>.w_Forza2.or. .o_RFDENOMI<>.w_RFDENOMI
            .w_RFDATANASC = iif(empty(.w_RFDENOMI) and (.w_Perazi<>'S' or .w_Forza2),.w_RFDATANASC,CTOD('  -  -  '))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza2<>.w_Forza2.or. .o_RFDENOMI<>.w_RFDENOMI
            .w_RFCOMNAS = iif(empty(.w_RFDENOMI) and (.w_Perazi<>'S' or .w_Forza2),.w_RFCOMNAS,space(40))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza2<>.w_Forza2.or. .o_RFDENOMI<>.w_RFDENOMI
            .w_RFSIGNAS = iif(empty(.w_RFDENOMI) and (.w_Perazi<>'S' or .w_Forza2),.w_RFSIGNAS,'  ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza2<>.w_Forza2.or. .o_RFDENOMI<>.w_RFDENOMI
            .w_RFCOMUNE = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFCOMUNE,space(40))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza2<>.w_Forza2.or. .o_RFDENOMI<>.w_RFDENOMI
            .w_RFSIGLA = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFSIGLA,'  ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza2<>.w_Forza2.or. .o_RFDENOMI<>.w_RFDENOMI
            .w_RFCAP = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFCAP,space(5))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza2<>.w_Forza2.or. .o_RFDENOMI<>.w_RFDENOMI
            .w_RFINDIRIZ = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFINDIRIZ,space(35))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_Forza2<>.w_Forza2.or. .o_RFDENOMI<>.w_RFDENOMI
            .w_RFTELEFONO = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFTELEFONO,space(12))
        endif
        if .o_Forza2<>.w_Forza2
            .w_RFDATCAR = iif(.w_Perazi<>'S' or .w_Forza2,.w_RFDATCAR,CTOD('  -  -  '))
        endif
        if .o_Forza2<>.w_Forza2.or. .o_RFCODCAR<>.w_RFCODCAR
            .w_RFDATFAL = iif(inlist(.w_RFCODCAR,'3','4'),.w_RFDATFAL,ctod('  -  -    '))
        endif
        if .o_SEZ2<>.w_SEZ2.or. .o_SEZ4<>.w_SEZ4
            .w_SEZ1 = iif(.w_SEZ2='1' or .w_SEZ4='1','0','1')
        endif
        if .o_SEZ1<>.w_SEZ1.or. .o_SEZ2<>.w_SEZ2.or. .o_SEZ4<>.w_SEZ4
            .w_qSS1 = iif(.w_SEZ1='1',.w_qSS1,'0')
        endif
        if .o_SEZ1<>.w_SEZ1.or. .o_SEZ2<>.w_SEZ2.or. .o_SEZ4<>.w_SEZ4
            .w_qST1 = iif(.w_SEZ1='1','1','0')
        endif
        if .o_SEZ1<>.w_SEZ1.or. .o_SEZ2<>.w_SEZ2.or. .o_SEZ4<>.w_SEZ4
            .w_qSX1 = iif(.w_SEZ1='1',.w_qSX1,'0')
        endif
        if .o_SEZ1<>.w_SEZ1.or. .o_SEZ2<>.w_SEZ2.or. .o_SEZ4<>.w_SEZ4
            .w_q771 = '0'
        endif
        if .o_SEZ1<>.w_SEZ1.or. .o_SEZ4<>.w_SEZ4
            .w_SEZ2 = iif(.w_SEZ1='1' or .w_SEZ4='1','0','1')
        endif
        if .o_SEZ1<>.w_SEZ1.or. .o_SEZ2<>.w_SEZ2.or. .o_SEZ4<>.w_SEZ4
            .w_qSS12 = iif(.w_SEZ2='1',.w_qSS12,'0')
        endif
        .DoRTCalc(94,97,.t.)
        if .o_SEZ1<>.w_SEZ1.or. .o_SEZ2<>.w_SEZ2.or. .o_SEZ4<>.w_SEZ4
            .w_q773 = '0'
        endif
        if .o_SEZ1<>.w_SEZ1.or. .o_SEZ2<>.w_SEZ2
            .w_SEZ4 = iif(.w_SEZ1='1' or .w_SEZ2='1','0','1')
        endif
        if .o_SEZ1<>.w_SEZ1.or. .o_SEZ2<>.w_SEZ2.or. .o_SEZ4<>.w_SEZ4
            .w_qSS14 = iif(.w_SEZ4='1',.w_qSS14,'0')
        endif
        if .o_SEZ2<>.w_SEZ2.or. .o_SEZ1<>.w_SEZ1.or. .o_SEZ4<>.w_SEZ4
            .w_qST14 = iif(.w_SEZ4='1','1','0')
        endif
        if .o_SEZ2<>.w_SEZ2.or. .o_SEZ1<>.w_SEZ1.or. .o_SEZ4<>.w_SEZ4
            .w_qSX14 = iif(.w_SEZ4='1',.w_qSX14,'0')
        endif
        if .o_SEZ1<>.w_SEZ1.or. .o_SEZ2<>.w_SEZ2.or. .o_SEZ4<>.w_SEZ4
            .w_q774 = '0'
        endif
        if .o_SEZ2<>.w_SEZ2.or. .o_SEZ1<>.w_SEZ1
            .w_RFCFINS2 = iif(.w_SEZ4='1',.w_RFCFINS2,space(13))
        endif
        .DoRTCalc(105,109,.t.)
        if .o_FLAGCOR<>.w_FLAGCOR.or. .o_FLAGINT<>.w_FLAGINT
            .w_IDESTA = ' '
        endif
        if .o_FLAGCOR<>.w_FLAGCOR.or. .o_FLAGINT<>.w_FLAGINT
            .w_PROIFT = ' '
        endif
        .DoRTCalc(112,112,.t.)
        if .o_TIPFORN<>.w_TIPFORN.or. .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_DATANASC<>.w_DATANASC
            .w_FODATANASC = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI='S',.w_DATANASC,CTOD('  -  -  '))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_TIPFORN<>.w_TIPFORN.or. .o_SESSO<>.w_SESSO
            .w_FOSESSO = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI='S',.w_SESSO,' ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_TIPFORN<>.w_TIPFORN.or. .o_COMUNE<>.w_COMUNE
            .w_FOCOMUNE = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI='S',.w_COMUNE,space(40))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_TIPFORN<>.w_TIPFORN.or. .o_SIGLA<>.w_SIGLA
            .w_FOSIGLA = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI='S',.w_SIGLA,'  ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_TIPFORN<>.w_TIPFORN.or. .o_RECOMUNE<>.w_RECOMUNE
            .w_FORECOMUNE = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI='S',.w_RECOMUNE,space(40))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_TIPFORN<>.w_TIPFORN.or. .o_RESIGLA<>.w_RESIGLA
            .w_FORESIGLA = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI='S',.w_RESIGLA,'  ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_TIPFORN<>.w_TIPFORN.or. .o_CAP<>.w_CAP
            .w_FOCAP = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI='S',.w_CAP,space(5))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_TIPFORN<>.w_TIPFORN.or. .o_INDIRIZ<>.w_INDIRIZ
            .w_FOINDIRIZ = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI='S',.w_INDIRIZ,space(35))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_TIPFORN<>.w_TIPFORN.or. .o_SECOMUNE<>.w_SECOMUNE
            .w_FOSECOMUNE = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI<>'S',.w_SECOMUNE,space(40))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_TIPFORN<>.w_TIPFORN.or. .o_SESIGLA<>.w_SESIGLA
            .w_FOSESIGLA = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI<>'S',.w_SESIGLA,'  ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_TIPFORN<>.w_TIPFORN.or. .o_SECAP<>.w_SECAP
            .w_FOSECAP = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI<>'S',.w_SECAP,space(5))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_TIPFORN<>.w_TIPFORN.or. .o_SEINDIRI2<>.w_SEINDIRI2
            .w_FOSEINDIRI2 = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI<>'S',.w_SEINDIRI2,space(35))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_TIPFORN<>.w_TIPFORN.or. .o_SERCOMUN<>.w_SERCOMUN
            .w_FOSERCOMUN = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI<>'S',.w_SERCOMUN,space(40))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_TIPFORN<>.w_TIPFORN.or. .o_SERSIGLA<>.w_SERSIGLA
            .w_FOSERSIGLA = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI<>'S',.w_SERSIGLA,'  ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_TIPFORN<>.w_TIPFORN.or. .o_SERCAP<>.w_SERCAP
            .w_FOSERCAP = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI<>'S',.w_SERCAP,space(5))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_TIPFORN<>.w_TIPFORN.or. .o_SERINDIR<>.w_SERINDIR
            .w_FOSERINDIR = iif(inlist(.w_TIPFORN,'01','02') and .w_PERAZI<>'S',.w_SERINDIR,space(35))
        endif
        .DoRTCalc(129,130,.t.)
        if .o_SELCAF<>.w_SELCAF
            .w_CFDELCAF = iif(.w_SELCAF <> '1',SPACE(11),.w_CFDELCAF)
        endif
        .DoRTCalc(132,133,.t.)
            .w_CODAZI = i_codazi
          .link_5_24('Full')
        .DoRTCalc(136,136,.t.)
          .link_5_26('Full')
        .DoRTCalc(138,139,.t.)
        if .o_SEZ1<>.w_SEZ1.or. .o_SEZ2<>.w_SEZ2
            .w_FIRMDICH = iif(.w_SEZ1='1',.w_FIRMDICH,'0')
        endif
        if .o_SEZ1<>.w_SEZ1.or. .o_SEZ2<>.w_SEZ2
            .w_FIRMPRES = iif(.w_SEZ1='1',.w_FIRMPRES,'0')
        endif
        .DoRTCalc(142,142,.t.)
        if .o_IMPTRTEL<>.w_IMPTRTEL
            .w_INVAVTEL = iif(.w_IMPTRTEL <> '0',.w_INVAVTEL,'0')
        endif
        if .o_CFINCCON<>.w_CFINCCON
            .w_CODSOGG = iif(! empty(.w_CFINCCON),.w_CODSOGG,'0')
        endif
        .DoRTCalc(145,146,.t.)
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_TIPFORN<>.w_TIPFORN.or. .o_FOCODFIS<>.w_FOCODFIS
            .w_CODFISIN = space(16)
        endif
        .DoRTCalc(148,149,.t.)
        if .o_INVAVTEL<>.w_INVAVTEL
            .w_RICAVTEL = iif(.w_INVAVTEL='1',.w_RICAVTEL,'0')
        endif
        .DoRTCalc(151,154,.t.)
        if .o_SEDOMNOT<>.w_SEDOMNOT
            .w_CMDOMNOT = iif(! empty(.w_SEDOMNOT),space(40),.w_CMDOMNOT)
        endif
        if .o_SEDOMNOT<>.w_SEDOMNOT
            .w_PRDOMNOT = iif(! empty(.w_SEDOMNOT),space(2),.w_PRDOMNOT)
        endif
        if .o_SEDOMNOT<>.w_SEDOMNOT
            .w_CCDOMNOT = iif(! empty(.w_SEDOMNOT),space(4),.w_CCDOMNOT)
        endif
        if .o_SEDOMNOT<>.w_SEDOMNOT
            .w_CPDOMNOT = iif(! empty(.w_SEDOMNOT),space(5),.w_CPDOMNOT)
        endif
        if .o_SEDOMNOT<>.w_SEDOMNOT
            .w_VPDOMNOT = iif(! empty(.w_SEDOMNOT),space(15),.w_VPDOMNOT)
        endif
        if .o_SEDOMNOT<>.w_SEDOMNOT
            .w_INDOMNOT = iif(! empty(.w_SEDOMNOT),space(35),.w_INDOMNOT)
        endif
        if .o_SEDOMNOT<>.w_SEDOMNOT
            .w_NCDOMNOT = iif(! empty(.w_SEDOMNOT),space(10),.w_NCDOMNOT)
        endif
        if .o_SEDOMNOT<>.w_SEDOMNOT
            .w_FRDOMNOT = iif(! empty(.w_SEDOMNOT),space(35),.w_FRDOMNOT)
        endif
        .DoRTCalc(163,163,.t.)
        if .o_SEDOMNOT<>.w_SEDOMNOT
            .w_CEDOMNOT = iif(empty(.w_SEDOMNOT),space(3),.w_CEDOMNOT)
        endif
        if .o_SEDOMNOT<>.w_SEDOMNOT
            .w_SFDOMNOT = iif(empty(.w_SEDOMNOT),space(24),.w_SFDOMNOT)
        endif
        if .o_SEDOMNOT<>.w_SEDOMNOT
            .w_LRDOMNOT = iif(empty(.w_SEDOMNOT),space(24),.w_LRDOMNOT)
        endif
        if .o_SEDOMNOT<>.w_SEDOMNOT
            .w_IEDOMNOT = iif(empty(.w_SEDOMNOT),space(35),.w_IEDOMNOT)
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oFOCOGNOME_1_11.enabled = this.oPgFrm.Page1.oPag.oFOCOGNOME_1_11.mCond()
    this.oPgFrm.Page1.oPag.oFONOME_1_12.enabled = this.oPgFrm.Page1.oPag.oFONOME_1_12.mCond()
    this.oPgFrm.Page1.oPag.oFODENOMINA_1_13.enabled = this.oPgFrm.Page1.oPag.oFODENOMINA_1_13.mCond()
    this.oPgFrm.Page1.oPag.oCODEVECC_1_18.enabled = this.oPgFrm.Page1.oPag.oCODEVECC_1_18.mCond()
    this.oPgFrm.Page1.oPag.oTIPOPERAZ_1_23.enabled = this.oPgFrm.Page1.oPag.oTIPOPERAZ_1_23.mCond()
    this.oPgFrm.Page1.oPag.oDATANASC_1_25.enabled = this.oPgFrm.Page1.oPag.oDATANASC_1_25.mCond()
    this.oPgFrm.Page1.oPag.oCOMUNE_1_26.enabled = this.oPgFrm.Page1.oPag.oCOMUNE_1_26.mCond()
    this.oPgFrm.Page1.oPag.oSIGLA_1_27.enabled = this.oPgFrm.Page1.oPag.oSIGLA_1_27.mCond()
    this.oPgFrm.Page1.oPag.oSESSO_1_28.enabled = this.oPgFrm.Page1.oPag.oSESSO_1_28.mCond()
    this.oPgFrm.Page1.oPag.oVARRES_1_29.enabled = this.oPgFrm.Page1.oPag.oVARRES_1_29.mCond()
    this.oPgFrm.Page1.oPag.oRECOMUNE_1_30.enabled = this.oPgFrm.Page1.oPag.oRECOMUNE_1_30.mCond()
    this.oPgFrm.Page1.oPag.oRESIGLA_1_31.enabled = this.oPgFrm.Page1.oPag.oRESIGLA_1_31.mCond()
    this.oPgFrm.Page1.oPag.oRECODCOM_1_32.enabled = this.oPgFrm.Page1.oPag.oRECODCOM_1_32.mCond()
    this.oPgFrm.Page1.oPag.oINDIRIZ_1_33.enabled = this.oPgFrm.Page1.oPag.oINDIRIZ_1_33.mCond()
    this.oPgFrm.Page1.oPag.oCAP_1_34.enabled = this.oPgFrm.Page1.oPag.oCAP_1_34.mCond()
    this.oPgFrm.Page1.oPag.oCODATT_1_35.enabled = this.oPgFrm.Page1.oPag.oCODATT_1_35.mCond()
    this.oPgFrm.Page1.oPag.oTELEFONO_1_36.enabled = this.oPgFrm.Page1.oPag.oTELEFONO_1_36.mCond()
    this.oPgFrm.Page1.oPag.oSEVARSED_1_37.enabled = this.oPgFrm.Page1.oPag.oSEVARSED_1_37.mCond()
    this.oPgFrm.Page1.oPag.oSECOMUNE_1_38.enabled = this.oPgFrm.Page1.oPag.oSECOMUNE_1_38.mCond()
    this.oPgFrm.Page1.oPag.oSESIGLA_1_39.enabled = this.oPgFrm.Page1.oPag.oSESIGLA_1_39.mCond()
    this.oPgFrm.Page1.oPag.oSEINDIRI2_1_40.enabled = this.oPgFrm.Page1.oPag.oSEINDIRI2_1_40.mCond()
    this.oPgFrm.Page1.oPag.oSECAP_1_41.enabled = this.oPgFrm.Page1.oPag.oSECAP_1_41.mCond()
    this.oPgFrm.Page1.oPag.oSECODCOM_1_42.enabled = this.oPgFrm.Page1.oPag.oSECODCOM_1_42.mCond()
    this.oPgFrm.Page1.oPag.oSEVARDOM_1_43.enabled = this.oPgFrm.Page1.oPag.oSEVARDOM_1_43.mCond()
    this.oPgFrm.Page1.oPag.oSERCOMUN_1_44.enabled = this.oPgFrm.Page1.oPag.oSERCOMUN_1_44.mCond()
    this.oPgFrm.Page1.oPag.oSERSIGLA_1_45.enabled = this.oPgFrm.Page1.oPag.oSERSIGLA_1_45.mCond()
    this.oPgFrm.Page1.oPag.oSERCODCOM_1_46.enabled = this.oPgFrm.Page1.oPag.oSERCODCOM_1_46.mCond()
    this.oPgFrm.Page1.oPag.oSERCAP_1_47.enabled = this.oPgFrm.Page1.oPag.oSERCAP_1_47.mCond()
    this.oPgFrm.Page1.oPag.oSERINDIR_1_48.enabled = this.oPgFrm.Page1.oPag.oSERINDIR_1_48.mCond()
    this.oPgFrm.Page1.oPag.oSECODATT_1_49.enabled = this.oPgFrm.Page1.oPag.oSECODATT_1_49.mCond()
    this.oPgFrm.Page1.oPag.oSETELEFONO_1_50.enabled = this.oPgFrm.Page1.oPag.oSETELEFONO_1_50.mCond()
    this.oPgFrm.Page1.oPag.oNATGIU_1_51.enabled = this.oPgFrm.Page1.oPag.oNATGIU_1_51.mCond()
    this.oPgFrm.Page1.oPag.oSTATO_1_52.enabled = this.oPgFrm.Page1.oPag.oSTATO_1_52.mCond()
    this.oPgFrm.Page1.oPag.oSITUAZ_1_53.enabled = this.oPgFrm.Page1.oPag.oSITUAZ_1_53.mCond()
    this.oPgFrm.Page1.oPag.oCODFISDA_1_54.enabled = this.oPgFrm.Page1.oPag.oCODFISDA_1_54.mCond()
    this.oPgFrm.Page2.oPag.oRFCODFIS_2_3.enabled = this.oPgFrm.Page2.oPag.oRFCODFIS_2_3.mCond()
    this.oPgFrm.Page2.oPag.oRFCODCAR_2_4.enabled = this.oPgFrm.Page2.oPag.oRFCODCAR_2_4.mCond()
    this.oPgFrm.Page2.oPag.oRFDENOMI_2_5.enabled = this.oPgFrm.Page2.oPag.oRFDENOMI_2_5.mCond()
    this.oPgFrm.Page2.oPag.oRFCOGNOME_2_6.enabled = this.oPgFrm.Page2.oPag.oRFCOGNOME_2_6.mCond()
    this.oPgFrm.Page2.oPag.oRFNOME_2_7.enabled = this.oPgFrm.Page2.oPag.oRFNOME_2_7.mCond()
    this.oPgFrm.Page2.oPag.oRFSESSO_2_8.enabled = this.oPgFrm.Page2.oPag.oRFSESSO_2_8.mCond()
    this.oPgFrm.Page2.oPag.oRFDATANASC_2_9.enabled = this.oPgFrm.Page2.oPag.oRFDATANASC_2_9.mCond()
    this.oPgFrm.Page2.oPag.oRFCOMNAS_2_10.enabled = this.oPgFrm.Page2.oPag.oRFCOMNAS_2_10.mCond()
    this.oPgFrm.Page2.oPag.oRFSIGNAS_2_11.enabled = this.oPgFrm.Page2.oPag.oRFSIGNAS_2_11.mCond()
    this.oPgFrm.Page2.oPag.oRFCOMUNE_2_12.enabled = this.oPgFrm.Page2.oPag.oRFCOMUNE_2_12.mCond()
    this.oPgFrm.Page2.oPag.oRFSIGLA_2_13.enabled = this.oPgFrm.Page2.oPag.oRFSIGLA_2_13.mCond()
    this.oPgFrm.Page2.oPag.oRFCAP_2_14.enabled = this.oPgFrm.Page2.oPag.oRFCAP_2_14.mCond()
    this.oPgFrm.Page2.oPag.oRFINDIRIZ_2_15.enabled = this.oPgFrm.Page2.oPag.oRFINDIRIZ_2_15.mCond()
    this.oPgFrm.Page2.oPag.oRFTELEFONO_2_16.enabled = this.oPgFrm.Page2.oPag.oRFTELEFONO_2_16.mCond()
    this.oPgFrm.Page2.oPag.oRFDATCAR_2_17.enabled = this.oPgFrm.Page2.oPag.oRFDATCAR_2_17.mCond()
    this.oPgFrm.Page2.oPag.oRFDATFAL_2_18.enabled = this.oPgFrm.Page2.oPag.oRFDATFAL_2_18.mCond()
    this.oPgFrm.Page2.oPag.oqSS1_2_20.enabled = this.oPgFrm.Page2.oPag.oqSS1_2_20.mCond()
    this.oPgFrm.Page2.oPag.oqST1_2_21.enabled = this.oPgFrm.Page2.oPag.oqST1_2_21.mCond()
    this.oPgFrm.Page2.oPag.oqSX1_2_22.enabled = this.oPgFrm.Page2.oPag.oqSX1_2_22.mCond()
    this.oPgFrm.Page2.oPag.oq771_2_23.enabled = this.oPgFrm.Page2.oPag.oq771_2_23.mCond()
    this.oPgFrm.Page2.oPag.oqSS12_2_25.enabled = this.oPgFrm.Page2.oPag.oqSS12_2_25.mCond()
    this.oPgFrm.Page2.oPag.oSEZ3_2_26.enabled = this.oPgFrm.Page2.oPag.oSEZ3_2_26.mCond()
    this.oPgFrm.Page2.oPag.oqSS13_2_27.enabled = this.oPgFrm.Page2.oPag.oqSS13_2_27.mCond()
    this.oPgFrm.Page2.oPag.oqST13_2_28.enabled = this.oPgFrm.Page2.oPag.oqST13_2_28.mCond()
    this.oPgFrm.Page2.oPag.oqSX13_2_29.enabled = this.oPgFrm.Page2.oPag.oqSX13_2_29.mCond()
    this.oPgFrm.Page2.oPag.oq773_2_30.enabled = this.oPgFrm.Page2.oPag.oq773_2_30.mCond()
    this.oPgFrm.Page2.oPag.oqSS14_2_32.enabled = this.oPgFrm.Page2.oPag.oqSS14_2_32.mCond()
    this.oPgFrm.Page2.oPag.oqST14_2_33.enabled = this.oPgFrm.Page2.oPag.oqST14_2_33.mCond()
    this.oPgFrm.Page2.oPag.oqSX14_2_34.enabled = this.oPgFrm.Page2.oPag.oqSX14_2_34.mCond()
    this.oPgFrm.Page2.oPag.oq774_2_35.enabled = this.oPgFrm.Page2.oPag.oq774_2_35.mCond()
    this.oPgFrm.Page2.oPag.oRFCFINS2_2_36.enabled = this.oPgFrm.Page2.oPag.oRFCFINS2_2_36.mCond()
    this.oPgFrm.Page5.oPag.oIDESTA_5_6.enabled = this.oPgFrm.Page5.oPag.oIDESTA_5_6.mCond()
    this.oPgFrm.Page5.oPag.oPROIFT_5_7.enabled = this.oPgFrm.Page5.oPag.oPROIFT_5_7.mCond()
    this.oPgFrm.Page4.oPag.oFODATANASC_4_2.enabled = this.oPgFrm.Page4.oPag.oFODATANASC_4_2.mCond()
    this.oPgFrm.Page4.oPag.oFOSESSO_4_3.enabled = this.oPgFrm.Page4.oPag.oFOSESSO_4_3.mCond()
    this.oPgFrm.Page4.oPag.oFOCOMUNE_4_4.enabled = this.oPgFrm.Page4.oPag.oFOCOMUNE_4_4.mCond()
    this.oPgFrm.Page4.oPag.oFOSIGLA_4_5.enabled = this.oPgFrm.Page4.oPag.oFOSIGLA_4_5.mCond()
    this.oPgFrm.Page4.oPag.oFORECOMUNE_4_6.enabled = this.oPgFrm.Page4.oPag.oFORECOMUNE_4_6.mCond()
    this.oPgFrm.Page4.oPag.oFORESIGLA_4_7.enabled = this.oPgFrm.Page4.oPag.oFORESIGLA_4_7.mCond()
    this.oPgFrm.Page4.oPag.oFOCAP_4_8.enabled = this.oPgFrm.Page4.oPag.oFOCAP_4_8.mCond()
    this.oPgFrm.Page4.oPag.oFOINDIRIZ_4_9.enabled = this.oPgFrm.Page4.oPag.oFOINDIRIZ_4_9.mCond()
    this.oPgFrm.Page4.oPag.oFOSECOMUNE_4_11.enabled = this.oPgFrm.Page4.oPag.oFOSECOMUNE_4_11.mCond()
    this.oPgFrm.Page4.oPag.oFOSESIGLA_4_12.enabled = this.oPgFrm.Page4.oPag.oFOSESIGLA_4_12.mCond()
    this.oPgFrm.Page4.oPag.oFOSECAP_4_13.enabled = this.oPgFrm.Page4.oPag.oFOSECAP_4_13.mCond()
    this.oPgFrm.Page4.oPag.oFOSEINDIRI2_4_14.enabled = this.oPgFrm.Page4.oPag.oFOSEINDIRI2_4_14.mCond()
    this.oPgFrm.Page4.oPag.oFOSERCOMUN_4_15.enabled = this.oPgFrm.Page4.oPag.oFOSERCOMUN_4_15.mCond()
    this.oPgFrm.Page4.oPag.oFOSERSIGLA_4_16.enabled = this.oPgFrm.Page4.oPag.oFOSERSIGLA_4_16.mCond()
    this.oPgFrm.Page4.oPag.oFOSERCAP_4_17.enabled = this.oPgFrm.Page4.oPag.oFOSERCAP_4_17.mCond()
    this.oPgFrm.Page4.oPag.oFOSERINDIR_4_18.enabled = this.oPgFrm.Page4.oPag.oFOSERINDIR_4_18.mCond()
    this.oPgFrm.Page3.oPag.oINVAVTEL_3_4.enabled = this.oPgFrm.Page3.oPag.oINVAVTEL_3_4.mCond()
    this.oPgFrm.Page3.oPag.oCODSOGG_3_5.enabled = this.oPgFrm.Page3.oPag.oCODSOGG_3_5.mCond()
    this.oPgFrm.Page3.oPag.oRICAVTEL_3_11.enabled = this.oPgFrm.Page3.oPag.oRICAVTEL_3_11.mCond()
    this.oPgFrm.Page3.oPag.oCMDOMNOT_3_33.enabled = this.oPgFrm.Page3.oPag.oCMDOMNOT_3_33.mCond()
    this.oPgFrm.Page3.oPag.oPRDOMNOT_3_35.enabled = this.oPgFrm.Page3.oPag.oPRDOMNOT_3_35.mCond()
    this.oPgFrm.Page3.oPag.oCCDOMNOT_3_37.enabled = this.oPgFrm.Page3.oPag.oCCDOMNOT_3_37.mCond()
    this.oPgFrm.Page3.oPag.oCPDOMNOT_3_39.enabled = this.oPgFrm.Page3.oPag.oCPDOMNOT_3_39.mCond()
    this.oPgFrm.Page3.oPag.oVPDOMNOT_3_41.enabled = this.oPgFrm.Page3.oPag.oVPDOMNOT_3_41.mCond()
    this.oPgFrm.Page3.oPag.oINDOMNOT_3_43.enabled = this.oPgFrm.Page3.oPag.oINDOMNOT_3_43.mCond()
    this.oPgFrm.Page3.oPag.oNCDOMNOT_3_45.enabled = this.oPgFrm.Page3.oPag.oNCDOMNOT_3_45.mCond()
    this.oPgFrm.Page3.oPag.oFRDOMNOT_3_47.enabled = this.oPgFrm.Page3.oPag.oFRDOMNOT_3_47.mCond()
    this.oPgFrm.Page3.oPag.oCEDOMNOT_3_51.enabled = this.oPgFrm.Page3.oPag.oCEDOMNOT_3_51.mCond()
    this.oPgFrm.Page3.oPag.oSFDOMNOT_3_53.enabled = this.oPgFrm.Page3.oPag.oSFDOMNOT_3_53.mCond()
    this.oPgFrm.Page3.oPag.oLRDOMNOT_3_55.enabled = this.oPgFrm.Page3.oPag.oLRDOMNOT_3_55.mCond()
    this.oPgFrm.Page3.oPag.oIEDOMNOT_3_57.enabled = this.oPgFrm.Page3.oPag.oIEDOMNOT_3_57.mCond()
    this.oPgFrm.Page5.oPag.oBtn_5_21.enabled = this.oPgFrm.Page5.oPag.oBtn_5_21.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page2.oPag.oRFDATFAL_2_18.visible=!this.oPgFrm.Page2.oPag.oRFDATFAL_2_18.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_65.visible=!this.oPgFrm.Page2.oPag.oStr_2_65.mHide()
    this.oPgFrm.Page4.oPag.oCFDELCAF_4_21.visible=!this.oPgFrm.Page4.oPag.oCFDELCAF_4_21.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_67.visible=!this.oPgFrm.Page2.oPag.oStr_2_67.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_47.visible=!this.oPgFrm.Page4.oPag.oStr_4_47.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsri8sft
    *Disabilito etichetta relativa alla sezione III
    this.opgfrm.pages(2).opag.oStr_2_59.enabled=.f.
    this.opgfrm.pages(2).opag.oStr_2_60.enabled=.f.
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsri8sft
    if cevent ='Done'
      g_LoadFuncButton = .T.
    endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZCOFAZI,AZRAGAZI,AZPERAZI,AZIVACAR,AZIVACOF";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_CODAZI)
            select AZCODAZI,AZCOFAZI,AZRAGAZI,AZPERAZI,AZIVACAR,AZIVACOF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.AZCODAZI,space(5))
      this.w_CodFisAzi = NVL(_Link_.AZCOFAZI,space(16))
      this.w_FODENOMINA = NVL(_Link_.AZRAGAZI,space(60))
      this.w_PERAZI = NVL(_Link_.AZPERAZI,space(1))
      this.w_RFCODCAR = NVL(_Link_.AZIVACAR,space(2))
      this.w_RFCODFIS = NVL(_Link_.AZIVACOF,space(16))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_CodFisAzi = space(16)
      this.w_FODENOMINA = space(60)
      this.w_PERAZI = space(1)
      this.w_RFCODCAR = space(2)
      this.w_RFCODFIS = space(16)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PERFIS
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TITOLARI_IDX,3]
    i_lTable = "TITOLARI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TITOLARI_IDX,2], .t., this.TITOLARI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TITOLARI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PERFIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PERFIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TTCODAZI,TTDATNAS,TT_SESSO,TTLUONAS,TTPRONAS,TTLOCTIT,TTPROTIT,TTINDIRI,TTCAPTIT,TTTELEFO,TTCOGTIT,TTNOMTIT";
                   +" from "+i_cTable+" "+i_lTable+" where TTCODAZI="+cp_ToStrODBC(this.w_PERFIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TTCODAZI',this.w_PERFIS)
            select TTCODAZI,TTDATNAS,TT_SESSO,TTLUONAS,TTPRONAS,TTLOCTIT,TTPROTIT,TTINDIRI,TTCAPTIT,TTTELEFO,TTCOGTIT,TTNOMTIT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PERFIS = NVL(_Link_.TTCODAZI,space(5))
      this.w_DATANASC = NVL(cp_ToDate(_Link_.TTDATNAS),ctod("  /  /  "))
      this.w_SESSO = NVL(_Link_.TT_SESSO,space(1))
      this.w_COMUNE = NVL(_Link_.TTLUONAS,space(40))
      this.w_SIGLA = NVL(_Link_.TTPRONAS,space(2))
      this.w_RECOMUNE = NVL(_Link_.TTLOCTIT,space(40))
      this.w_RESIGLA = NVL(_Link_.TTPROTIT,space(2))
      this.w_INDIRIZ = NVL(_Link_.TTINDIRI,space(35))
      this.w_CAP = NVL(_Link_.TTCAPTIT,space(5))
      this.w_TELEFONO = NVL(_Link_.TTTELEFO,space(12))
      this.w_FOCOGNOME = NVL(_Link_.TTCOGTIT,space(24))
      this.w_FONOME = NVL(_Link_.TTNOMTIT,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_PERFIS = space(5)
      endif
      this.w_DATANASC = ctod("  /  /  ")
      this.w_SESSO = space(1)
      this.w_COMUNE = space(40)
      this.w_SIGLA = space(2)
      this.w_RECOMUNE = space(40)
      this.w_RESIGLA = space(2)
      this.w_INDIRIZ = space(35)
      this.w_CAP = space(5)
      this.w_TELEFONO = space(12)
      this.w_FOCOGNOME = space(24)
      this.w_FONOME = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TITOLARI_IDX,2])+'\'+cp_ToStr(_Link_.TTCODAZI,1)
      cp_ShowWarn(i_cKey,this.TITOLARI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PERFIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SEDILEG
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SEDIAZIE_IDX,3]
    i_lTable = "SEDIAZIE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2], .t., this.SEDIAZIE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SEDILEG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SEDILEG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SETIPRIF,SECODAZI,SELOCALI,SEPROVIN,SEINDIRI,SE___CAP";
                   +" from "+i_cTable+" "+i_lTable+" where SECODAZI="+cp_ToStrODBC(this.w_SEDILEG);
                   +" and SETIPRIF="+cp_ToStrODBC(this.w_TIPSL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SETIPRIF',this.w_TIPSL;
                       ,'SECODAZI',this.w_SEDILEG)
            select SETIPRIF,SECODAZI,SELOCALI,SEPROVIN,SEINDIRI,SE___CAP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SEDILEG = NVL(_Link_.SECODAZI,space(5))
      this.w_SECOMUNE = NVL(_Link_.SELOCALI,space(40))
      this.w_SESIGLA = NVL(_Link_.SEPROVIN,space(2))
      this.w_SEINDIRI2 = NVL(_Link_.SEINDIRI,space(35))
      this.w_SECAP = NVL(_Link_.SE___CAP,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_SEDILEG = space(5)
      endif
      this.w_SECOMUNE = space(40)
      this.w_SESIGLA = space(2)
      this.w_SEINDIRI2 = space(35)
      this.w_SECAP = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2])+'\'+cp_ToStr(_Link_.SETIPRIF,1)+'\'+cp_ToStr(_Link_.SECODAZI,1)
      cp_ShowWarn(i_cKey,this.SEDIAZIE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SEDILEG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SEDIFIS
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SEDIAZIE_IDX,3]
    i_lTable = "SEDIAZIE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2], .t., this.SEDIAZIE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SEDIFIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SEDIFIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SETIPRIF,SECODAZI,SELOCALI,SEPROVIN,SEINDIRI,SE___CAP,SETELEFO";
                   +" from "+i_cTable+" "+i_lTable+" where SECODAZI="+cp_ToStrODBC(this.w_SEDIFIS);
                   +" and SETIPRIF="+cp_ToStrODBC(this.w_TIPFS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SETIPRIF',this.w_TIPFS;
                       ,'SECODAZI',this.w_SEDIFIS)
            select SETIPRIF,SECODAZI,SELOCALI,SEPROVIN,SEINDIRI,SE___CAP,SETELEFO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SEDIFIS = NVL(_Link_.SECODAZI,space(5))
      this.w_SERCOMUN = NVL(_Link_.SELOCALI,space(40))
      this.w_SERSIGLA = NVL(_Link_.SEPROVIN,space(2))
      this.w_SERINDIR = NVL(_Link_.SEINDIRI,space(35))
      this.w_SERCAP = NVL(_Link_.SE___CAP,space(5))
      this.w_SETELEFONO = NVL(_Link_.SETELEFO,space(12))
    else
      if i_cCtrl<>'Load'
        this.w_SEDIFIS = space(5)
      endif
      this.w_SERCOMUN = space(40)
      this.w_SERSIGLA = space(2)
      this.w_SERINDIR = space(35)
      this.w_SERCAP = space(5)
      this.w_SETELEFONO = space(12)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2])+'\'+cp_ToStr(_Link_.SETIPRIF,1)+'\'+cp_ToStr(_Link_.SECODAZI,1)
      cp_ShowWarn(i_cKey,this.SEDIAZIE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SEDIFIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ESERCIZIO
  func Link_1_56(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ESERCIZIO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ESERCIZIO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_ESERCIZIO);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_ESERCIZIO)
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ESERCIZIO = NVL(_Link_.ESCODESE,space(4))
      this.w_VALUTAESE = NVL(_Link_.ESVALNAZ,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_ESERCIZIO = space(4)
      endif
      this.w_VALUTAESE = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ESERCIZIO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODVAL
  func Link_1_59(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_CODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_CODVAL)
            select VACODVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_decimi = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_CODVAL = space(3)
      endif
      this.w_decimi = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RAPFIRM
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DAT_RAPP_IDX,3]
    i_lTable = "DAT_RAPP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DAT_RAPP_IDX,2], .t., this.DAT_RAPP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DAT_RAPP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RAPFIRM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RAPFIRM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RFCODAZI,RFCODFIS,RFCODCAR,RFDENOMI,RFCOGNOM,RF__NOME,RF_SESSO,RFDATNAS,RFCOMNAS,RFPRONAS,RFCOMRES,RFPRORES,RFCAPRES,RFINDRES,RFTELEFO,RFDATCAR";
                   +" from "+i_cTable+" "+i_lTable+" where RFCODAZI="+cp_ToStrODBC(this.w_RAPFIRM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RFCODAZI',this.w_RAPFIRM)
            select RFCODAZI,RFCODFIS,RFCODCAR,RFDENOMI,RFCOGNOM,RF__NOME,RF_SESSO,RFDATNAS,RFCOMNAS,RFPRONAS,RFCOMRES,RFPRORES,RFCAPRES,RFINDRES,RFTELEFO,RFDATCAR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RAPFIRM = NVL(_Link_.RFCODAZI,space(5))
      this.w_RFCODFIS = NVL(_Link_.RFCODFIS,space(16))
      this.w_RFCODCAR = NVL(_Link_.RFCODCAR,space(2))
      this.w_RFDENOMI = NVL(_Link_.RFDENOMI,space(60))
      this.w_RFCOGNOME = NVL(_Link_.RFCOGNOM,space(24))
      this.w_RFNOME = NVL(_Link_.RF__NOME,space(20))
      this.w_RFSESSO = NVL(_Link_.RF_SESSO,space(1))
      this.w_RFDATANASC = NVL(cp_ToDate(_Link_.RFDATNAS),ctod("  /  /  "))
      this.w_RFCOMNAS = NVL(_Link_.RFCOMNAS,space(40))
      this.w_RFSIGNAS = NVL(_Link_.RFPRONAS,space(2))
      this.w_RFCOMUNE = NVL(_Link_.RFCOMRES,space(40))
      this.w_RFSIGLA = NVL(_Link_.RFPRORES,space(2))
      this.w_RFCAP = NVL(_Link_.RFCAPRES,space(5))
      this.w_RFINDIRIZ = NVL(_Link_.RFINDRES,space(35))
      this.w_RFTELEFONO = NVL(_Link_.RFTELEFO,space(12))
      this.w_RFDATCAR = NVL(cp_ToDate(_Link_.RFDATCAR),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_RAPFIRM = space(5)
      endif
      this.w_RFCODFIS = space(16)
      this.w_RFCODCAR = space(2)
      this.w_RFDENOMI = space(60)
      this.w_RFCOGNOME = space(24)
      this.w_RFNOME = space(20)
      this.w_RFSESSO = space(1)
      this.w_RFDATANASC = ctod("  /  /  ")
      this.w_RFCOMNAS = space(40)
      this.w_RFSIGNAS = space(2)
      this.w_RFCOMUNE = space(40)
      this.w_RFSIGLA = space(2)
      this.w_RFCAP = space(5)
      this.w_RFINDIRIZ = space(35)
      this.w_RFTELEFONO = space(12)
      this.w_RFDATCAR = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DAT_RAPP_IDX,2])+'\'+cp_ToStr(_Link_.RFCODAZI,1)
      cp_ShowWarn(i_cKey,this.DAT_RAPP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RAPFIRM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PERCIN
  func Link_5_24(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PERCIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PERCIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU,ANTIPCLF";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_PERCIN);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_PERCIN)
            select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU,ANTIPCLF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PERCIN = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCINI = NVL(_Link_.ANDESCRI,space(40))
      this.w_RITE = NVL(_Link_.ANRITENU,space(1))
      this.w_TIPCLF = NVL(_Link_.ANTIPCLF,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PERCIN = space(15)
      endif
      this.w_DESCINI = space(40)
      this.w_RITE = space(1)
      this.w_TIPCLF = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((EMPTY(.w_PERCFIN)) OR (.w_PERCIN<=.w_PERCFIN)) AND (.w_RITE $ 'CS' AND .w_TIPCLF<>'A')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice fornitore inesistente o obsoleto o non soggetto a ritenute o agente")
        endif
        this.w_PERCIN = space(15)
        this.w_DESCINI = space(40)
        this.w_RITE = space(1)
        this.w_TIPCLF = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PERCIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PERCFIN
  func Link_5_26(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PERCFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PERCFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU,ANTIPCLF";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_PERCFIN);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_PERCFIN)
            select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU,ANTIPCLF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PERCFIN = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCFIN = NVL(_Link_.ANDESCRI,space(40))
      this.w_RITE = NVL(_Link_.ANRITENU,space(1))
      this.w_TIPCLF = NVL(_Link_.ANTIPCLF,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PERCFIN = space(15)
      endif
      this.w_DESCFIN = space(40)
      this.w_RITE = space(1)
      this.w_TIPCLF = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((.w_PERCFIN>=.w_PERCIN) OR (EMPTY(.w_PERCIN))) AND (.w_RITE $ 'CS' AND .w_TIPCLF<>'A')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice fornitore inesistente o obsoleto o non soggetto a ritenute o agente")
        endif
        this.w_PERCFIN = space(15)
        this.w_DESCFIN = space(40)
        this.w_RITE = space(1)
        this.w_TIPCLF = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PERCFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oFOCOGNOME_1_11.value==this.w_FOCOGNOME)
      this.oPgFrm.Page1.oPag.oFOCOGNOME_1_11.value=this.w_FOCOGNOME
    endif
    if not(this.oPgFrm.Page1.oPag.oFONOME_1_12.value==this.w_FONOME)
      this.oPgFrm.Page1.oPag.oFONOME_1_12.value=this.w_FONOME
    endif
    if not(this.oPgFrm.Page1.oPag.oFODENOMINA_1_13.value==this.w_FODENOMINA)
      this.oPgFrm.Page1.oPag.oFODENOMINA_1_13.value=this.w_FODENOMINA
    endif
    if not(this.oPgFrm.Page1.oPag.oFOCODFIS_1_14.value==this.w_FOCODFIS)
      this.oPgFrm.Page1.oPag.oFOCODFIS_1_14.value=this.w_FOCODFIS
    endif
    if not(this.oPgFrm.Page1.oPag.oFOCODSOS_1_15.value==this.w_FOCODSOS)
      this.oPgFrm.Page1.oPag.oFOCODSOS_1_15.value=this.w_FOCODSOS
    endif
    if not(this.oPgFrm.Page1.oPag.oFLAGEVE_1_17.RadioValue()==this.w_FLAGEVE)
      this.oPgFrm.Page1.oPag.oFLAGEVE_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODEVECC_1_18.RadioValue()==this.w_CODEVECC)
      this.oPgFrm.Page1.oPag.oCODEVECC_1_18.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLAGCONF_1_19.RadioValue()==this.w_FLAGCONF)
      this.oPgFrm.Page1.oPag.oFLAGCONF_1_19.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oForza1_1_20.RadioValue()==this.w_Forza1)
      this.oPgFrm.Page1.oPag.oForza1_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLAGCOR_1_21.RadioValue()==this.w_FLAGCOR)
      this.oPgFrm.Page1.oPag.oFLAGCOR_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLAGINT_1_22.RadioValue()==this.w_FLAGINT)
      this.oPgFrm.Page1.oPag.oFLAGINT_1_22.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOPERAZ_1_23.RadioValue()==this.w_TIPOPERAZ)
      this.oPgFrm.Page1.oPag.oTIPOPERAZ_1_23.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDATANASC_1_25.value==this.w_DATANASC)
      this.oPgFrm.Page1.oPag.oDATANASC_1_25.value=this.w_DATANASC
    endif
    if not(this.oPgFrm.Page1.oPag.oCOMUNE_1_26.value==this.w_COMUNE)
      this.oPgFrm.Page1.oPag.oCOMUNE_1_26.value=this.w_COMUNE
    endif
    if not(this.oPgFrm.Page1.oPag.oSIGLA_1_27.value==this.w_SIGLA)
      this.oPgFrm.Page1.oPag.oSIGLA_1_27.value=this.w_SIGLA
    endif
    if not(this.oPgFrm.Page1.oPag.oSESSO_1_28.RadioValue()==this.w_SESSO)
      this.oPgFrm.Page1.oPag.oSESSO_1_28.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oVARRES_1_29.value==this.w_VARRES)
      this.oPgFrm.Page1.oPag.oVARRES_1_29.value=this.w_VARRES
    endif
    if not(this.oPgFrm.Page1.oPag.oRECOMUNE_1_30.value==this.w_RECOMUNE)
      this.oPgFrm.Page1.oPag.oRECOMUNE_1_30.value=this.w_RECOMUNE
    endif
    if not(this.oPgFrm.Page1.oPag.oRESIGLA_1_31.value==this.w_RESIGLA)
      this.oPgFrm.Page1.oPag.oRESIGLA_1_31.value=this.w_RESIGLA
    endif
    if not(this.oPgFrm.Page1.oPag.oRECODCOM_1_32.value==this.w_RECODCOM)
      this.oPgFrm.Page1.oPag.oRECODCOM_1_32.value=this.w_RECODCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oINDIRIZ_1_33.value==this.w_INDIRIZ)
      this.oPgFrm.Page1.oPag.oINDIRIZ_1_33.value=this.w_INDIRIZ
    endif
    if not(this.oPgFrm.Page1.oPag.oCAP_1_34.value==this.w_CAP)
      this.oPgFrm.Page1.oPag.oCAP_1_34.value=this.w_CAP
    endif
    if not(this.oPgFrm.Page1.oPag.oCODATT_1_35.value==this.w_CODATT)
      this.oPgFrm.Page1.oPag.oCODATT_1_35.value=this.w_CODATT
    endif
    if not(this.oPgFrm.Page1.oPag.oTELEFONO_1_36.value==this.w_TELEFONO)
      this.oPgFrm.Page1.oPag.oTELEFONO_1_36.value=this.w_TELEFONO
    endif
    if not(this.oPgFrm.Page1.oPag.oSEVARSED_1_37.value==this.w_SEVARSED)
      this.oPgFrm.Page1.oPag.oSEVARSED_1_37.value=this.w_SEVARSED
    endif
    if not(this.oPgFrm.Page1.oPag.oSECOMUNE_1_38.value==this.w_SECOMUNE)
      this.oPgFrm.Page1.oPag.oSECOMUNE_1_38.value=this.w_SECOMUNE
    endif
    if not(this.oPgFrm.Page1.oPag.oSESIGLA_1_39.value==this.w_SESIGLA)
      this.oPgFrm.Page1.oPag.oSESIGLA_1_39.value=this.w_SESIGLA
    endif
    if not(this.oPgFrm.Page1.oPag.oSEINDIRI2_1_40.value==this.w_SEINDIRI2)
      this.oPgFrm.Page1.oPag.oSEINDIRI2_1_40.value=this.w_SEINDIRI2
    endif
    if not(this.oPgFrm.Page1.oPag.oSECAP_1_41.value==this.w_SECAP)
      this.oPgFrm.Page1.oPag.oSECAP_1_41.value=this.w_SECAP
    endif
    if not(this.oPgFrm.Page1.oPag.oSECODCOM_1_42.value==this.w_SECODCOM)
      this.oPgFrm.Page1.oPag.oSECODCOM_1_42.value=this.w_SECODCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oSEVARDOM_1_43.value==this.w_SEVARDOM)
      this.oPgFrm.Page1.oPag.oSEVARDOM_1_43.value=this.w_SEVARDOM
    endif
    if not(this.oPgFrm.Page1.oPag.oSERCOMUN_1_44.value==this.w_SERCOMUN)
      this.oPgFrm.Page1.oPag.oSERCOMUN_1_44.value=this.w_SERCOMUN
    endif
    if not(this.oPgFrm.Page1.oPag.oSERSIGLA_1_45.value==this.w_SERSIGLA)
      this.oPgFrm.Page1.oPag.oSERSIGLA_1_45.value=this.w_SERSIGLA
    endif
    if not(this.oPgFrm.Page1.oPag.oSERCODCOM_1_46.value==this.w_SERCODCOM)
      this.oPgFrm.Page1.oPag.oSERCODCOM_1_46.value=this.w_SERCODCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oSERCAP_1_47.value==this.w_SERCAP)
      this.oPgFrm.Page1.oPag.oSERCAP_1_47.value=this.w_SERCAP
    endif
    if not(this.oPgFrm.Page1.oPag.oSERINDIR_1_48.value==this.w_SERINDIR)
      this.oPgFrm.Page1.oPag.oSERINDIR_1_48.value=this.w_SERINDIR
    endif
    if not(this.oPgFrm.Page1.oPag.oSECODATT_1_49.value==this.w_SECODATT)
      this.oPgFrm.Page1.oPag.oSECODATT_1_49.value=this.w_SECODATT
    endif
    if not(this.oPgFrm.Page1.oPag.oSETELEFONO_1_50.value==this.w_SETELEFONO)
      this.oPgFrm.Page1.oPag.oSETELEFONO_1_50.value=this.w_SETELEFONO
    endif
    if not(this.oPgFrm.Page1.oPag.oNATGIU_1_51.value==this.w_NATGIU)
      this.oPgFrm.Page1.oPag.oNATGIU_1_51.value=this.w_NATGIU
    endif
    if not(this.oPgFrm.Page1.oPag.oSTATO_1_52.RadioValue()==this.w_STATO)
      this.oPgFrm.Page1.oPag.oSTATO_1_52.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSITUAZ_1_53.RadioValue()==this.w_SITUAZ)
      this.oPgFrm.Page1.oPag.oSITUAZ_1_53.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODFISDA_1_54.value==this.w_CODFISDA)
      this.oPgFrm.Page1.oPag.oCODFISDA_1_54.value=this.w_CODFISDA
    endif
    if not(this.oPgFrm.Page2.oPag.oForza2_2_1.RadioValue()==this.w_Forza2)
      this.oPgFrm.Page2.oPag.oForza2_2_1.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oRFCODFIS_2_3.value==this.w_RFCODFIS)
      this.oPgFrm.Page2.oPag.oRFCODFIS_2_3.value=this.w_RFCODFIS
    endif
    if not(this.oPgFrm.Page2.oPag.oRFCODCAR_2_4.RadioValue()==this.w_RFCODCAR)
      this.oPgFrm.Page2.oPag.oRFCODCAR_2_4.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oRFDENOMI_2_5.value==this.w_RFDENOMI)
      this.oPgFrm.Page2.oPag.oRFDENOMI_2_5.value=this.w_RFDENOMI
    endif
    if not(this.oPgFrm.Page2.oPag.oRFCOGNOME_2_6.value==this.w_RFCOGNOME)
      this.oPgFrm.Page2.oPag.oRFCOGNOME_2_6.value=this.w_RFCOGNOME
    endif
    if not(this.oPgFrm.Page2.oPag.oRFNOME_2_7.value==this.w_RFNOME)
      this.oPgFrm.Page2.oPag.oRFNOME_2_7.value=this.w_RFNOME
    endif
    if not(this.oPgFrm.Page2.oPag.oRFSESSO_2_8.RadioValue()==this.w_RFSESSO)
      this.oPgFrm.Page2.oPag.oRFSESSO_2_8.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oRFDATANASC_2_9.value==this.w_RFDATANASC)
      this.oPgFrm.Page2.oPag.oRFDATANASC_2_9.value=this.w_RFDATANASC
    endif
    if not(this.oPgFrm.Page2.oPag.oRFCOMNAS_2_10.value==this.w_RFCOMNAS)
      this.oPgFrm.Page2.oPag.oRFCOMNAS_2_10.value=this.w_RFCOMNAS
    endif
    if not(this.oPgFrm.Page2.oPag.oRFSIGNAS_2_11.value==this.w_RFSIGNAS)
      this.oPgFrm.Page2.oPag.oRFSIGNAS_2_11.value=this.w_RFSIGNAS
    endif
    if not(this.oPgFrm.Page2.oPag.oRFCOMUNE_2_12.value==this.w_RFCOMUNE)
      this.oPgFrm.Page2.oPag.oRFCOMUNE_2_12.value=this.w_RFCOMUNE
    endif
    if not(this.oPgFrm.Page2.oPag.oRFSIGLA_2_13.value==this.w_RFSIGLA)
      this.oPgFrm.Page2.oPag.oRFSIGLA_2_13.value=this.w_RFSIGLA
    endif
    if not(this.oPgFrm.Page2.oPag.oRFCAP_2_14.value==this.w_RFCAP)
      this.oPgFrm.Page2.oPag.oRFCAP_2_14.value=this.w_RFCAP
    endif
    if not(this.oPgFrm.Page2.oPag.oRFINDIRIZ_2_15.value==this.w_RFINDIRIZ)
      this.oPgFrm.Page2.oPag.oRFINDIRIZ_2_15.value=this.w_RFINDIRIZ
    endif
    if not(this.oPgFrm.Page2.oPag.oRFTELEFONO_2_16.value==this.w_RFTELEFONO)
      this.oPgFrm.Page2.oPag.oRFTELEFONO_2_16.value=this.w_RFTELEFONO
    endif
    if not(this.oPgFrm.Page2.oPag.oRFDATCAR_2_17.value==this.w_RFDATCAR)
      this.oPgFrm.Page2.oPag.oRFDATCAR_2_17.value=this.w_RFDATCAR
    endif
    if not(this.oPgFrm.Page2.oPag.oRFDATFAL_2_18.value==this.w_RFDATFAL)
      this.oPgFrm.Page2.oPag.oRFDATFAL_2_18.value=this.w_RFDATFAL
    endif
    if not(this.oPgFrm.Page2.oPag.oSEZ1_2_19.RadioValue()==this.w_SEZ1)
      this.oPgFrm.Page2.oPag.oSEZ1_2_19.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oqSS1_2_20.RadioValue()==this.w_qSS1)
      this.oPgFrm.Page2.oPag.oqSS1_2_20.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oqST1_2_21.RadioValue()==this.w_qST1)
      this.oPgFrm.Page2.oPag.oqST1_2_21.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oqSX1_2_22.RadioValue()==this.w_qSX1)
      this.oPgFrm.Page2.oPag.oqSX1_2_22.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oq771_2_23.RadioValue()==this.w_q771)
      this.oPgFrm.Page2.oPag.oq771_2_23.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oSEZ2_2_24.RadioValue()==this.w_SEZ2)
      this.oPgFrm.Page2.oPag.oSEZ2_2_24.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oqSS12_2_25.RadioValue()==this.w_qSS12)
      this.oPgFrm.Page2.oPag.oqSS12_2_25.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oSEZ3_2_26.RadioValue()==this.w_SEZ3)
      this.oPgFrm.Page2.oPag.oSEZ3_2_26.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oqSS13_2_27.RadioValue()==this.w_qSS13)
      this.oPgFrm.Page2.oPag.oqSS13_2_27.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oqST13_2_28.RadioValue()==this.w_qST13)
      this.oPgFrm.Page2.oPag.oqST13_2_28.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oqSX13_2_29.RadioValue()==this.w_qSX13)
      this.oPgFrm.Page2.oPag.oqSX13_2_29.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oq773_2_30.RadioValue()==this.w_q773)
      this.oPgFrm.Page2.oPag.oq773_2_30.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oSEZ4_2_31.RadioValue()==this.w_SEZ4)
      this.oPgFrm.Page2.oPag.oSEZ4_2_31.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oqSS14_2_32.RadioValue()==this.w_qSS14)
      this.oPgFrm.Page2.oPag.oqSS14_2_32.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oqST14_2_33.RadioValue()==this.w_qST14)
      this.oPgFrm.Page2.oPag.oqST14_2_33.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oqSX14_2_34.RadioValue()==this.w_qSX14)
      this.oPgFrm.Page2.oPag.oqSX14_2_34.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oq774_2_35.RadioValue()==this.w_q774)
      this.oPgFrm.Page2.oPag.oq774_2_35.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oRFCFINS2_2_36.value==this.w_RFCFINS2)
      this.oPgFrm.Page2.oPag.oRFCFINS2_2_36.value=this.w_RFCFINS2
    endif
    if not(this.oPgFrm.Page5.oPag.oDATINI_5_1.value==this.w_DATINI)
      this.oPgFrm.Page5.oPag.oDATINI_5_1.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page5.oPag.oDATFIN_5_2.value==this.w_DATFIN)
      this.oPgFrm.Page5.oPag.oDATFIN_5_2.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page5.oPag.oDirName_5_3.value==this.w_DirName)
      this.oPgFrm.Page5.oPag.oDirName_5_3.value=this.w_DirName
    endif
    if not(this.oPgFrm.Page5.oPag.oFileName_5_5.value==this.w_FileName)
      this.oPgFrm.Page5.oPag.oFileName_5_5.value=this.w_FileName
    endif
    if not(this.oPgFrm.Page5.oPag.oIDESTA_5_6.value==this.w_IDESTA)
      this.oPgFrm.Page5.oPag.oIDESTA_5_6.value=this.w_IDESTA
    endif
    if not(this.oPgFrm.Page5.oPag.oPROIFT_5_7.value==this.w_PROIFT)
      this.oPgFrm.Page5.oPag.oPROIFT_5_7.value=this.w_PROIFT
    endif
    if not(this.oPgFrm.Page4.oPag.oTIPFORN_4_1.RadioValue()==this.w_TIPFORN)
      this.oPgFrm.Page4.oPag.oTIPFORN_4_1.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oFODATANASC_4_2.value==this.w_FODATANASC)
      this.oPgFrm.Page4.oPag.oFODATANASC_4_2.value=this.w_FODATANASC
    endif
    if not(this.oPgFrm.Page4.oPag.oFOSESSO_4_3.RadioValue()==this.w_FOSESSO)
      this.oPgFrm.Page4.oPag.oFOSESSO_4_3.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oFOCOMUNE_4_4.value==this.w_FOCOMUNE)
      this.oPgFrm.Page4.oPag.oFOCOMUNE_4_4.value=this.w_FOCOMUNE
    endif
    if not(this.oPgFrm.Page4.oPag.oFOSIGLA_4_5.value==this.w_FOSIGLA)
      this.oPgFrm.Page4.oPag.oFOSIGLA_4_5.value=this.w_FOSIGLA
    endif
    if not(this.oPgFrm.Page4.oPag.oFORECOMUNE_4_6.value==this.w_FORECOMUNE)
      this.oPgFrm.Page4.oPag.oFORECOMUNE_4_6.value=this.w_FORECOMUNE
    endif
    if not(this.oPgFrm.Page4.oPag.oFORESIGLA_4_7.value==this.w_FORESIGLA)
      this.oPgFrm.Page4.oPag.oFORESIGLA_4_7.value=this.w_FORESIGLA
    endif
    if not(this.oPgFrm.Page4.oPag.oFOCAP_4_8.value==this.w_FOCAP)
      this.oPgFrm.Page4.oPag.oFOCAP_4_8.value=this.w_FOCAP
    endif
    if not(this.oPgFrm.Page4.oPag.oFOINDIRIZ_4_9.value==this.w_FOINDIRIZ)
      this.oPgFrm.Page4.oPag.oFOINDIRIZ_4_9.value=this.w_FOINDIRIZ
    endif
    if not(this.oPgFrm.Page4.oPag.oFOSECOMUNE_4_11.value==this.w_FOSECOMUNE)
      this.oPgFrm.Page4.oPag.oFOSECOMUNE_4_11.value=this.w_FOSECOMUNE
    endif
    if not(this.oPgFrm.Page4.oPag.oFOSESIGLA_4_12.value==this.w_FOSESIGLA)
      this.oPgFrm.Page4.oPag.oFOSESIGLA_4_12.value=this.w_FOSESIGLA
    endif
    if not(this.oPgFrm.Page4.oPag.oFOSECAP_4_13.value==this.w_FOSECAP)
      this.oPgFrm.Page4.oPag.oFOSECAP_4_13.value=this.w_FOSECAP
    endif
    if not(this.oPgFrm.Page4.oPag.oFOSEINDIRI2_4_14.value==this.w_FOSEINDIRI2)
      this.oPgFrm.Page4.oPag.oFOSEINDIRI2_4_14.value=this.w_FOSEINDIRI2
    endif
    if not(this.oPgFrm.Page4.oPag.oFOSERCOMUN_4_15.value==this.w_FOSERCOMUN)
      this.oPgFrm.Page4.oPag.oFOSERCOMUN_4_15.value=this.w_FOSERCOMUN
    endif
    if not(this.oPgFrm.Page4.oPag.oFOSERSIGLA_4_16.value==this.w_FOSERSIGLA)
      this.oPgFrm.Page4.oPag.oFOSERSIGLA_4_16.value=this.w_FOSERSIGLA
    endif
    if not(this.oPgFrm.Page4.oPag.oFOSERCAP_4_17.value==this.w_FOSERCAP)
      this.oPgFrm.Page4.oPag.oFOSERCAP_4_17.value=this.w_FOSERCAP
    endif
    if not(this.oPgFrm.Page4.oPag.oFOSERINDIR_4_18.value==this.w_FOSERINDIR)
      this.oPgFrm.Page4.oPag.oFOSERINDIR_4_18.value=this.w_FOSERINDIR
    endif
    if not(this.oPgFrm.Page4.oPag.oSELCAF_4_19.RadioValue()==this.w_SELCAF)
      this.oPgFrm.Page4.oPag.oSELCAF_4_19.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oCFRESCAF_4_20.value==this.w_CFRESCAF)
      this.oPgFrm.Page4.oPag.oCFRESCAF_4_20.value=this.w_CFRESCAF
    endif
    if not(this.oPgFrm.Page4.oPag.oCFDELCAF_4_21.value==this.w_CFDELCAF)
      this.oPgFrm.Page4.oPag.oCFDELCAF_4_21.value=this.w_CFDELCAF
    endif
    if not(this.oPgFrm.Page4.oPag.oFLCONFCF_4_22.RadioValue()==this.w_FLCONFCF)
      this.oPgFrm.Page4.oPag.oFLCONFCF_4_22.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oFLAGFIR_4_23.RadioValue()==this.w_FLAGFIR)
      this.oPgFrm.Page4.oPag.oFLAGFIR_4_23.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oFIRMDICH_3_1.RadioValue()==this.w_FIRMDICH)
      this.oPgFrm.Page3.oPag.oFIRMDICH_3_1.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oFIRMPRES_3_2.RadioValue()==this.w_FIRMPRES)
      this.oPgFrm.Page3.oPag.oFIRMPRES_3_2.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oCFINCCON_3_3.value==this.w_CFINCCON)
      this.oPgFrm.Page3.oPag.oCFINCCON_3_3.value=this.w_CFINCCON
    endif
    if not(this.oPgFrm.Page3.oPag.oINVAVTEL_3_4.RadioValue()==this.w_INVAVTEL)
      this.oPgFrm.Page3.oPag.oINVAVTEL_3_4.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oCODSOGG_3_5.RadioValue()==this.w_CODSOGG)
      this.oPgFrm.Page3.oPag.oCODSOGG_3_5.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oNUMCAF_3_6.value==this.w_NUMCAF)
      this.oPgFrm.Page3.oPag.oNUMCAF_3_6.value=this.w_NUMCAF
    endif
    if not(this.oPgFrm.Page3.oPag.oRFDATIMP_3_7.value==this.w_RFDATIMP)
      this.oPgFrm.Page3.oPag.oRFDATIMP_3_7.value=this.w_RFDATIMP
    endif
    if not(this.oPgFrm.Page3.oPag.oCODFISIN_3_8.value==this.w_CODFISIN)
      this.oPgFrm.Page3.oPag.oCODFISIN_3_8.value=this.w_CODFISIN
    endif
    if not(this.oPgFrm.Page3.oPag.oIMPTRTEL_3_9.RadioValue()==this.w_IMPTRTEL)
      this.oPgFrm.Page3.oPag.oIMPTRTEL_3_9.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oFIRMINT_3_10.RadioValue()==this.w_FIRMINT)
      this.oPgFrm.Page3.oPag.oFIRMINT_3_10.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oRICAVTEL_3_11.RadioValue()==this.w_RICAVTEL)
      this.oPgFrm.Page3.oPag.oRICAVTEL_3_11.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oCFDOMNOT_3_25.value==this.w_CFDOMNOT)
      this.oPgFrm.Page3.oPag.oCFDOMNOT_3_25.value=this.w_CFDOMNOT
    endif
    if not(this.oPgFrm.Page3.oPag.oUFDOMNOT_3_27.value==this.w_UFDOMNOT)
      this.oPgFrm.Page3.oPag.oUFDOMNOT_3_27.value=this.w_UFDOMNOT
    endif
    if not(this.oPgFrm.Page3.oPag.oCODOMNOT_3_28.value==this.w_CODOMNOT)
      this.oPgFrm.Page3.oPag.oCODOMNOT_3_28.value=this.w_CODOMNOT
    endif
    if not(this.oPgFrm.Page3.oPag.oNODOMNOT_3_31.value==this.w_NODOMNOT)
      this.oPgFrm.Page3.oPag.oNODOMNOT_3_31.value=this.w_NODOMNOT
    endif
    if not(this.oPgFrm.Page3.oPag.oCMDOMNOT_3_33.value==this.w_CMDOMNOT)
      this.oPgFrm.Page3.oPag.oCMDOMNOT_3_33.value=this.w_CMDOMNOT
    endif
    if not(this.oPgFrm.Page3.oPag.oPRDOMNOT_3_35.value==this.w_PRDOMNOT)
      this.oPgFrm.Page3.oPag.oPRDOMNOT_3_35.value=this.w_PRDOMNOT
    endif
    if not(this.oPgFrm.Page3.oPag.oCCDOMNOT_3_37.value==this.w_CCDOMNOT)
      this.oPgFrm.Page3.oPag.oCCDOMNOT_3_37.value=this.w_CCDOMNOT
    endif
    if not(this.oPgFrm.Page3.oPag.oCPDOMNOT_3_39.value==this.w_CPDOMNOT)
      this.oPgFrm.Page3.oPag.oCPDOMNOT_3_39.value=this.w_CPDOMNOT
    endif
    if not(this.oPgFrm.Page3.oPag.oVPDOMNOT_3_41.value==this.w_VPDOMNOT)
      this.oPgFrm.Page3.oPag.oVPDOMNOT_3_41.value=this.w_VPDOMNOT
    endif
    if not(this.oPgFrm.Page3.oPag.oINDOMNOT_3_43.value==this.w_INDOMNOT)
      this.oPgFrm.Page3.oPag.oINDOMNOT_3_43.value=this.w_INDOMNOT
    endif
    if not(this.oPgFrm.Page3.oPag.oNCDOMNOT_3_45.value==this.w_NCDOMNOT)
      this.oPgFrm.Page3.oPag.oNCDOMNOT_3_45.value=this.w_NCDOMNOT
    endif
    if not(this.oPgFrm.Page3.oPag.oFRDOMNOT_3_47.value==this.w_FRDOMNOT)
      this.oPgFrm.Page3.oPag.oFRDOMNOT_3_47.value=this.w_FRDOMNOT
    endif
    if not(this.oPgFrm.Page3.oPag.oSEDOMNOT_3_49.value==this.w_SEDOMNOT)
      this.oPgFrm.Page3.oPag.oSEDOMNOT_3_49.value=this.w_SEDOMNOT
    endif
    if not(this.oPgFrm.Page3.oPag.oCEDOMNOT_3_51.value==this.w_CEDOMNOT)
      this.oPgFrm.Page3.oPag.oCEDOMNOT_3_51.value=this.w_CEDOMNOT
    endif
    if not(this.oPgFrm.Page3.oPag.oSFDOMNOT_3_53.value==this.w_SFDOMNOT)
      this.oPgFrm.Page3.oPag.oSFDOMNOT_3_53.value=this.w_SFDOMNOT
    endif
    if not(this.oPgFrm.Page3.oPag.oLRDOMNOT_3_55.value==this.w_LRDOMNOT)
      this.oPgFrm.Page3.oPag.oLRDOMNOT_3_55.value=this.w_LRDOMNOT
    endif
    if not(this.oPgFrm.Page3.oPag.oIEDOMNOT_3_57.value==this.w_IEDOMNOT)
      this.oPgFrm.Page3.oPag.oIEDOMNOT_3_57.value=this.w_IEDOMNOT
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_FOCOGNOME))  and (empty(.w_FODENOMINA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFOCOGNOME_1_11.SetFocus()
            i_bnoObbl = !empty(.w_FOCOGNOME)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_FONOME))  and (empty(.w_FODENOMINA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFONOME_1_12.SetFocus()
            i_bnoObbl = !empty(.w_FONOME)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_FODENOMINA)) or not(not empty(.w_FODENOMINA)))  and (empty(.w_FOCOGNOME) and empty(.w_FONOME))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFODENOMINA_1_13.SetFocus()
            i_bnoObbl = !empty(.w_FODENOMINA)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_FOCODFIS)) or not(iif(.w_PERAZI = 'S',chkcfp(alltrim(.w_FOCODFIS),'CF'),chkcfp(alltrim(.w_FOCODFIS),'PI'))))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFOCODFIS_1_14.SetFocus()
            i_bnoObbl = !empty(.w_FOCODFIS)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Lunghezza codice fiscale o partita IVA non corretta")
          case   not(.w_FOCODFIS <> .w_FOCODSOS and iif(!empty(.w_FOCODSOS),iif(len(alltrim(.w_FOCODSOS))=16,chkcfp(alltrim(.w_FOCODSOS),'CF'),chkcfp(alltrim(.w_FOCODSOS),'PI')),.t.))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFOCODSOS_1_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice fiscale del sostituto d'imposta deve essere diverso dal codice fiscale del dichiarante")
          case   not((.w_FLAGCOR='1' or .w_FLAGINT='1') and not empty(.w_TIPOPERAZ))  and ((.w_FLAGCOR = '1') OR (.w_FLAGINT = '1'))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTIPOPERAZ_1_23.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire il tipo dell'operazione")
          case   (empty(.w_DATANASC))  and (.w_Perazi='S' or .w_Forza1)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATANASC_1_25.SetFocus()
            i_bnoObbl = !empty(.w_DATANASC)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_COMUNE))  and (.w_Perazi='S' or .w_Forza1)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOMUNE_1_26.SetFocus()
            i_bnoObbl = !empty(.w_COMUNE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_SESSO))  and (.w_Perazi='S' or .w_Forza1)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSESSO_1_28.SetFocus()
            i_bnoObbl = !empty(.w_SESSO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_RECOMUNE))  and (.w_Perazi='S' or .w_Forza1)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRECOMUNE_1_30.SetFocus()
            i_bnoObbl = !empty(.w_RECOMUNE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(! empty(.w_RESIGLA))  and (.w_Perazi='S' or .w_Forza1)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRESIGLA_1_31.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire la sigla della provincia di residenza anagrafica o di domicilio fiscale")
          case   not(! empty(.w_CAP))  and (.w_Perazi='S' or .w_Forza1)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAP_1_34.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire il C.A.P. della residenza anagrafica o del domicilio fiscale del fornitore")
          case   not(at(' ',alltrim(.w_TELEFONO))=0 and at('/',alltrim(.w_TELEFONO))=0 and at('-',alltrim(.w_TELEFONO))=0)  and (.w_Perazi='S' or .w_Forza1)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTELEFONO_1_36.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Non inserire caratteri separatori tra prefisso e numero")
          case   (empty(.w_SECOMUNE))  and (.w_Perazi<>'S' or .w_Forza1)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSECOMUNE_1_38.SetFocus()
            i_bnoObbl = !empty(.w_SECOMUNE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(! empty(.w_SECAP))  and (.w_Perazi<>'S' or .w_Forza1)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSECAP_1_41.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire il C.A.P. della sede legale del fornitore")
          case   not(at(' ',alltrim(.w_SETELEFONO))=0 and at('/',alltrim(.w_SETELEFONO))=0 and at('-',alltrim(.w_SETELEFONO))=0)  and (.w_Perazi<>'S' or .w_Forza1)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSETELEFONO_1_50.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Non inserire caratteri separatori tra prefisso e numero")
          case   not((val(.w_NATGIU)>=1 and val(.w_NATGIU)<=44) OR (val(.w_NATGIU)>=50 and val(.w_NATGIU)<=58))  and (.w_Perazi<>'S' or .w_Forza1)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNATGIU_1_51.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valore non ammesso per natura giuridica")
          case   not(iif(not empty(.w_CODFISDA),chkcfp(alltrim(.w_CODFISDA),'PI'),.T.))  and (.w_Perazi<>'S' or .w_Forza1)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODFISDA_1_54.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(iif(not empty(.w_RFCODFIS),chkcfp(alltrim(.w_RFCODFIS),'CF'),.T.))  and (.w_PERAZI <> 'S' or (.w_Forza2 and .w_PERAZI='S'))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oRFCODFIS_2_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_RFCODCAR))  and (.w_PERAZI <> 'S' or .w_Forza2)
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oRFCODCAR_2_4.SetFocus()
            i_bnoObbl = !empty(.w_RFCODCAR)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(at(' ',alltrim(.w_RFTELEFONO))=0 and at('/',alltrim(.w_RFTELEFONO))=0 and at('-',alltrim(.w_RFTELEFONO))=0)  and (.w_Perazi<>'S' or .w_Forza2)
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oRFTELEFONO_2_16.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Non inserire caratteri separatori tra prefisso e numero")
          case   not(iif(not empty(.w_RFCFINS2),chkcfp(alltrim(.w_RFCFINS2),'CF'),iif(.w_SEZ4='1',.F.,.T.) ))  and (.w_SEZ4='1')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oRFCFINS2_2_36.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire il codice fiscale dell'intermediario della sezione IV")
          case   ((empty(.w_DATINI)) or not((.w_DATFIN>=.w_DATINI) AND (.w_DATINI > CTOD("31-12-06"))))
            .oPgFrm.ActivePage = 5
            .oPgFrm.Page5.oPag.oDATINI_5_1.SetFocus()
            i_bnoObbl = !empty(.w_DATINI)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � maggiore della data finale o fuori periodo")
          case   ((empty(.w_DATFIN)) or not((.w_DATFIN>=.w_DATINI) AND (.w_DATFIN < CTOD("01-01-08"))))
            .oPgFrm.ActivePage = 5
            .oPgFrm.Page5.oPag.oDATFIN_5_2.SetFocus()
            i_bnoObbl = !empty(.w_DATFIN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � maggiore della data finale o fuori periodo")
          case   (empty(.w_IDESTA))  and (.w_FLAGCOR = '1' or .w_FLAGINT = '1')
            .oPgFrm.ActivePage = 5
            .oPgFrm.Page5.oPag.oIDESTA_5_6.SetFocus()
            i_bnoObbl = !empty(.w_IDESTA)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_PROIFT))  and (.w_FLAGCOR = '1' or .w_FLAGINT = '1')
            .oPgFrm.ActivePage = 5
            .oPgFrm.Page5.oPag.oPROIFT_5_7.SetFocus()
            i_bnoObbl = !empty(.w_PROIFT)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((.w_Perazi='S' and  .w_TIPFORN<>'06' ) or .w_Perazi<>'S')
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oTIPFORN_4_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Selezione non consentita con tipo fornitore persona fisica")
          case   not(iif(not empty(.w_CFRESCAF),chkcfp(alltrim(.w_CFRESCAF),'CF'),.T.))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oCFRESCAF_4_20.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(iif(not empty(.w_CFDELCAF),chkcfp(alltrim(.w_CFDELCAF),'PI'),.T.))  and not(.w_SELCAF <> '1')
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oCFDELCAF_4_21.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(((EMPTY(.w_PERCFIN)) OR (.w_PERCIN<=.w_PERCFIN)) AND (.w_RITE $ 'CS' AND .w_TIPCLF<>'A'))  and not(empty(.w_PERCIN))
            .oPgFrm.ActivePage = 5
            .oPgFrm.Page5.oPag.oPERCIN_5_24.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice fornitore inesistente o obsoleto o non soggetto a ritenute o agente")
          case   not(((.w_PERCFIN>=.w_PERCIN) OR (EMPTY(.w_PERCIN))) AND (.w_RITE $ 'CS' AND .w_TIPCLF<>'A'))  and not(empty(.w_PERCFIN))
            .oPgFrm.ActivePage = 5
            .oPgFrm.Page5.oPag.oPERCFIN_5_26.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice fornitore inesistente o obsoleto o non soggetto a ritenute o agente")
          case   not(iif(not empty(.w_CFINCCON),chkcfp(alltrim(.w_CFINCCON),'CF'),.T.))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oCFINCCON_3_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(iif(not empty(.w_CODFISIN),chkcfp(alltrim(.w_CODFISIN),'CF'),.T.))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oCODFISIN_3_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(iif(not empty(.w_CFDOMNOT),chkcfp(alltrim(.w_CFDOMNOT),'CF'),.T.))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oCFDOMNOT_3_25.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODAZI = this.w_CODAZI
    this.o_PERAZI = this.w_PERAZI
    this.o_FOCOGNOME = this.w_FOCOGNOME
    this.o_FONOME = this.w_FONOME
    this.o_FODENOMINA = this.w_FODENOMINA
    this.o_FOCODFIS = this.w_FOCODFIS
    this.o_FLAGEVE = this.w_FLAGEVE
    this.o_Forza1 = this.w_Forza1
    this.o_FLAGCOR = this.w_FLAGCOR
    this.o_FLAGINT = this.w_FLAGINT
    this.o_DATANASC = this.w_DATANASC
    this.o_COMUNE = this.w_COMUNE
    this.o_SIGLA = this.w_SIGLA
    this.o_SESSO = this.w_SESSO
    this.o_RECOMUNE = this.w_RECOMUNE
    this.o_RESIGLA = this.w_RESIGLA
    this.o_INDIRIZ = this.w_INDIRIZ
    this.o_CAP = this.w_CAP
    this.o_SECOMUNE = this.w_SECOMUNE
    this.o_SESIGLA = this.w_SESIGLA
    this.o_SEINDIRI2 = this.w_SEINDIRI2
    this.o_SECAP = this.w_SECAP
    this.o_SERCOMUN = this.w_SERCOMUN
    this.o_SERSIGLA = this.w_SERSIGLA
    this.o_SERCAP = this.w_SERCAP
    this.o_SERINDIR = this.w_SERINDIR
    this.o_NOME = this.w_NOME
    this.o_Forza2 = this.w_Forza2
    this.o_RAPFIRM = this.w_RAPFIRM
    this.o_RFCODCAR = this.w_RFCODCAR
    this.o_RFDENOMI = this.w_RFDENOMI
    this.o_RFCOGNOME = this.w_RFCOGNOME
    this.o_SEZ1 = this.w_SEZ1
    this.o_SEZ2 = this.w_SEZ2
    this.o_SEZ4 = this.w_SEZ4
    this.o_DATFIN = this.w_DATFIN
    this.o_TIPFORN = this.w_TIPFORN
    this.o_SELCAF = this.w_SELCAF
    this.o_CFINCCON = this.w_CFINCCON
    this.o_INVAVTEL = this.w_INVAVTEL
    this.o_CODFISIN = this.w_CODFISIN
    this.o_IMPTRTEL = this.w_IMPTRTEL
    this.o_SEDOMNOT = this.w_SEDOMNOT
    return

enddefine

* --- Define pages as container
define class tgsri8sftPag1 as StdContainer
  Width  = 747
  height = 562
  stdWidth  = 747
  stdheight = 562
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oFOCOGNOME_1_11 as StdField with uid="SZIDZXIFTU",rtseq=11,rtrep=.f.,;
    cFormVar = "w_FOCOGNOME", cQueryName = "FOCOGNOME",;
    bObbl = .t. , nPag = 1, value=space(24), bMultilanguage =  .f.,;
    ToolTipText = "Cognome del fornitore",;
    HelpContextID = 265121293,;
   bGlobalFont=.t.,;
    Height=21, Width=177, Left=128, Top=10, cSayPict="repl('!',24)", cGetPict="repl('!',24)", InputMask=replicate('X',24)

  func oFOCOGNOME_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_FODENOMINA))
    endwith
   endif
  endfunc

  add object oFONOME_1_12 as StdField with uid="NPXPQLRYZB",rtseq=12,rtrep=.f.,;
    cFormVar = "w_FONOME", cQueryName = "FONOME",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Nome del fornitore",;
    HelpContextID = 141345450,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=574, Top=10, cSayPict="repl('!',20)", cGetPict="repl('!',20)", InputMask=replicate('X',20)

  func oFONOME_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_FODENOMINA))
    endwith
   endif
  endfunc

  add object oFODENOMINA_1_13 as StdField with uid="WNCFJWGGXA",rtseq=13,rtrep=.f.,;
    cFormVar = "w_FODENOMINA", cQueryName = "FODENOMINA",;
    bObbl = .t. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Denominazione del fornitore",;
    HelpContextID = 241638529,;
   bGlobalFont=.t.,;
    Height=21, Width=592, Left=128, Top=34, cSayPict="repl('!',60)", cGetPict="repl('!',60)", InputMask=replicate('X',60)

  func oFODENOMINA_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_FOCOGNOME) and empty(.w_FONOME))
    endwith
   endif
  endfunc

  func oFODENOMINA_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (not empty(.w_FODENOMINA))
    endwith
    return bRes
  endfunc

  add object oFOCODFIS_1_14 as StdField with uid="NQSHPZVXNW",rtseq=14,rtrep=.f.,;
    cFormVar = "w_FOCODFIS", cQueryName = "FOCODFIS",;
    bObbl = .t. , nPag = 1, value=space(16), bMultilanguage =  .f.,;
    sErrorMsg = "Lunghezza codice fiscale o partita IVA non corretta",;
    HelpContextID = 134050391,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=128, Top=58, cSayPict="repl('!',16)", cGetPict="repl('!',16)", InputMask=replicate('X',16)

  func oFOCODFIS_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (iif(.w_PERAZI = 'S',chkcfp(alltrim(.w_FOCODFIS),'CF'),chkcfp(alltrim(.w_FOCODFIS),'PI')))
    endwith
    return bRes
  endfunc

  add object oFOCODSOS_1_15 as StdField with uid="MWHDNMTQGN",rtseq=15,rtrep=.f.,;
    cFormVar = "w_FOCODSOS", cQueryName = "FOCODSOS",;
    bObbl = .f. , nPag = 1, value=space(16), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice fiscale del sostituto d'imposta deve essere diverso dal codice fiscale del dichiarante",;
    ToolTipText = "Codice fiscale del sostituto d'imposta, da compilare solo in caso di operazioni straodinarie e successioni",;
    HelpContextID = 184382039,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=574, Top=58, cSayPict="repl('!',16)", cGetPict="repl('!',16)", InputMask=replicate('X',16), tabstop=.F.

  func oFOCODSOS_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_FOCODFIS <> .w_FOCODSOS and iif(!empty(.w_FOCODSOS),iif(len(alltrim(.w_FOCODSOS))=16,chkcfp(alltrim(.w_FOCODSOS),'CF'),chkcfp(alltrim(.w_FOCODSOS),'PI')),.t.))
    endwith
    return bRes
  endfunc

  add object oFLAGEVE_1_17 as StdCheck with uid="WYTEXMRWTY",rtseq=17,rtrep=.f.,left=65, top=110, caption="Eventi eccezionali",;
    HelpContextID = 134900310,;
    cFormVar="w_FLAGEVE", bObbl = .f. , nPag = 1;
    , tabstop =.f.;
   , bGlobalFont=.t.


  func oFLAGEVE_1_17.RadioValue()
    return(iif(this.value =1,'01',;
    '00'))
  endfunc
  func oFLAGEVE_1_17.GetRadio()
    this.Parent.oContained.w_FLAGEVE = this.RadioValue()
    return .t.
  endfunc

  func oFLAGEVE_1_17.SetRadio()
    this.Parent.oContained.w_FLAGEVE=trim(this.Parent.oContained.w_FLAGEVE)
    this.value = ;
      iif(this.Parent.oContained.w_FLAGEVE=='01',1,;
      0)
  endfunc


  add object oCODEVECC_1_18 as StdCombo with uid="BYLGKOJBJM",rtseq=18,rtrep=.f.,left=65,top=167,width=448,height=21;
    , ToolTipText = "Eventi eccezionali";
    , HelpContextID = 135830889;
    , cFormVar="w_CODEVECC",RowSource=""+"0 - Nessuno,"+"1 - Contribuenti vittime di richieste estorsive ex. Art.20 Co.2 legge n. 44/99,"+"3 - Contribuenti residenti al 31/10/02 nelle province CB e FG (sisma),"+"4 - Altri eventi eccezionali", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCODEVECC_1_18.RadioValue()
    return(iif(this.value =1,'0',;
    iif(this.value =2,'1',;
    iif(this.value =3,'3',;
    iif(this.value =4,'4',;
    space(1))))))
  endfunc
  func oCODEVECC_1_18.GetRadio()
    this.Parent.oContained.w_CODEVECC = this.RadioValue()
    return .t.
  endfunc

  func oCODEVECC_1_18.SetRadio()
    this.Parent.oContained.w_CODEVECC=trim(this.Parent.oContained.w_CODEVECC)
    this.value = ;
      iif(this.Parent.oContained.w_CODEVECC=='0',1,;
      iif(this.Parent.oContained.w_CODEVECC=='1',2,;
      iif(this.Parent.oContained.w_CODEVECC=='3',3,;
      iif(this.Parent.oContained.w_CODEVECC=='4',4,;
      0))))
  endfunc

  func oCODEVECC_1_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLAGEVE='01')
    endwith
   endif
  endfunc

  add object oFLAGCONF_1_19 as StdCheck with uid="EXJMLGCXGO",rtseq=19,rtrep=.f.,left=337, top=110, caption="Conferma",;
    HelpContextID = 253072740,;
    cFormVar="w_FLAGCONF", bObbl = .f. , nPag = 1;
    , tabstop =.f.;
   , bGlobalFont=.t.


  func oFLAGCONF_1_19.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oFLAGCONF_1_19.GetRadio()
    this.Parent.oContained.w_FLAGCONF = this.RadioValue()
    return .t.
  endfunc

  func oFLAGCONF_1_19.SetRadio()
    this.Parent.oContained.w_FLAGCONF=trim(this.Parent.oContained.w_FLAGCONF)
    this.value = ;
      iif(this.Parent.oContained.w_FLAGCONF=='1',1,;
      0)
  endfunc

  add object oForza1_1_20 as StdCheck with uid="WYTWDOWGTW",rtseq=20,rtrep=.f.,left=337, top=136, caption="Forza editing",;
    HelpContextID = 184509098,;
    cFormVar="w_Forza1", bObbl = .f. , nPag = 1;
    , tabstop = .f.;
   , bGlobalFont=.t.


  func oForza1_1_20.RadioValue()
    return(iif(this.value =1,.t.,;
    .f.))
  endfunc
  func oForza1_1_20.GetRadio()
    this.Parent.oContained.w_Forza1 = this.RadioValue()
    return .t.
  endfunc

  func oForza1_1_20.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_Forza1==.t.,1,;
      0)
  endfunc

  add object oFLAGCOR_1_21 as StdCheck with uid="KAIBPTXLER",rtseq=21,rtrep=.f.,left=497, top=110, caption="Correttiva",;
    HelpContextID = 15362646,;
    cFormVar="w_FLAGCOR", bObbl = .f. , nPag = 1;
    , tabstop =.f.;
   , bGlobalFont=.t.


  func oFLAGCOR_1_21.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oFLAGCOR_1_21.GetRadio()
    this.Parent.oContained.w_FLAGCOR = this.RadioValue()
    return .t.
  endfunc

  func oFLAGCOR_1_21.SetRadio()
    this.Parent.oContained.w_FLAGCOR=trim(this.Parent.oContained.w_FLAGCOR)
    this.value = ;
      iif(this.Parent.oContained.w_FLAGCOR=='1',1,;
      0)
  endfunc

  add object oFLAGINT_1_22 as StdCheck with uid="SSJZDRXGKM",rtseq=22,rtrep=.f.,left=497, top=136, caption="Integrativa",;
    HelpContextID = 4876886,;
    cFormVar="w_FLAGINT", bObbl = .f. , nPag = 1;
    , tabstop =.f.;
   , bGlobalFont=.t.


  func oFLAGINT_1_22.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oFLAGINT_1_22.GetRadio()
    this.Parent.oContained.w_FLAGINT = this.RadioValue()
    return .t.
  endfunc

  func oFLAGINT_1_22.SetRadio()
    this.Parent.oContained.w_FLAGINT=trim(this.Parent.oContained.w_FLAGINT)
    this.value = ;
      iif(this.Parent.oContained.w_FLAGINT=='1',1,;
      0)
  endfunc


  add object oTIPOPERAZ_1_23 as StdCombo with uid="FVZJLSCZCJ",value=4,rtseq=23,rtrep=.f.,left=604,top=136,width=114,height=21;
    , tabstop =.f.;
    , ToolTipText = "Tipo operazioni possibili in correzione o integrazione";
    , HelpContextID = 130244119;
    , cFormVar="w_TIPOPERAZ",RowSource=""+"Inserimento,"+"Aggiornamento,"+"Cancellazione,"+"Nessuna", bObbl = .f. , nPag = 1;
    , sErrorMsg = "Inserire il tipo dell'operazione";
  , bGlobalFont=.t.


  func oTIPOPERAZ_1_23.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'A',;
    iif(this.value =3,'C',;
    iif(this.value =4,' ',;
    ' ')))))
  endfunc
  func oTIPOPERAZ_1_23.GetRadio()
    this.Parent.oContained.w_TIPOPERAZ = this.RadioValue()
    return .t.
  endfunc

  func oTIPOPERAZ_1_23.SetRadio()
    this.Parent.oContained.w_TIPOPERAZ=trim(this.Parent.oContained.w_TIPOPERAZ)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOPERAZ=='I',1,;
      iif(this.Parent.oContained.w_TIPOPERAZ=='A',2,;
      iif(this.Parent.oContained.w_TIPOPERAZ=='C',3,;
      iif(this.Parent.oContained.w_TIPOPERAZ=='',4,;
      0))))
  endfunc

  func oTIPOPERAZ_1_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((.w_FLAGCOR = '1') OR (.w_FLAGINT = '1'))
    endwith
   endif
  endfunc

  func oTIPOPERAZ_1_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_FLAGCOR='1' or .w_FLAGINT='1') and not empty(.w_TIPOPERAZ))
    endwith
    return bRes
  endfunc

  add object oDATANASC_1_25 as StdField with uid="WGYOTWHLZZ",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DATANASC", cQueryName = "DATANASC",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di nascita del fornitore",;
    HelpContextID = 60133241,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=36, Top=227

  func oDATANASC_1_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' or .w_Forza1)
    endwith
   endif
  endfunc

  add object oCOMUNE_1_26 as StdField with uid="QDXVFBYYJG",rtseq=26,rtrep=.f.,;
    cFormVar = "w_COMUNE", cQueryName = "COMUNE",;
    bObbl = .t. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune (o stato estero) di nascita del fornitore",;
    HelpContextID = 139907802,;
   bGlobalFont=.t.,;
    Height=21, Width=319, Left=142, Top=227, cSayPict='repl("!",40)', cGetPict='repl("!",40)', InputMask=replicate('X',40)

  func oCOMUNE_1_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' or .w_Forza1)
    endwith
   endif
  endfunc

  add object oSIGLA_1_27 as StdField with uid="AQSAXFLTQI",rtseq=27,rtrep=.f.,;
    cFormVar = "w_SIGLA", cQueryName = "SIGLA",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia di nascita del fornitore",;
    HelpContextID = 238041050,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=469, Top=227, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2)

  func oSIGLA_1_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' or .w_Forza1)
    endwith
   endif
  endfunc


  add object oSESSO_1_28 as StdCombo with uid="NCCDEVHYQI",rtseq=28,rtrep=.f.,left=544,top=228,width=92,height=21;
    , height = 21;
    , ToolTipText = "Sesso del fornitore";
    , HelpContextID = 222854106;
    , cFormVar="w_SESSO",RowSource=""+"Maschio,"+"Femmina", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oSESSO_1_28.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oSESSO_1_28.GetRadio()
    this.Parent.oContained.w_SESSO = this.RadioValue()
    return .t.
  endfunc

  func oSESSO_1_28.SetRadio()
    this.Parent.oContained.w_SESSO=trim(this.Parent.oContained.w_SESSO)
    this.value = ;
      iif(this.Parent.oContained.w_SESSO=='M',1,;
      iif(this.Parent.oContained.w_SESSO=='F',2,;
      0))
  endfunc

  func oSESSO_1_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' or .w_Forza1)
    endwith
   endif
  endfunc

  add object oVARRES_1_29 as StdField with uid="LCJAHFQKSB",rtseq=29,rtrep=.f.,;
    cFormVar = "w_VARRES", cQueryName = "VARRES",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data della variazione",;
    HelpContextID = 85356630,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=36, Top=266

  func oVARRES_1_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' or .w_Forza1)
    endwith
   endif
  endfunc

  add object oRECOMUNE_1_30 as StdField with uid="OZIDXVLWTW",rtseq=30,rtrep=.f.,;
    cFormVar = "w_RECOMUNE", cQueryName = "RECOMUNE",;
    bObbl = .t. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune di residenza anagrafica o di domicilio fiscale del fornitore",;
    HelpContextID = 141392805,;
   bGlobalFont=.t.,;
    Height=21, Width=319, Left=142, Top=266, cSayPict='repl("!",40)', cGetPict='repl("!",40)', InputMask=replicate('X',40)

  func oRECOMUNE_1_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' or .w_Forza1)
    endwith
   endif
  endfunc

  add object oRESIGLA_1_31 as StdField with uid="SUWJKWJLTK",rtseq=31,rtrep=.f.,;
    cFormVar = "w_RESIGLA", cQueryName = "RESIGLA",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Inserire la sigla della provincia di residenza anagrafica o di domicilio fiscale",;
    ToolTipText = "Sigla della provincia di residenza anagrafica o di domicilio fiscale",;
    HelpContextID = 237863958,;
   bGlobalFont=.t.,;
    Height=21, Width=32, Left=469, Top=266, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2)

  func oRESIGLA_1_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' or .w_Forza1)
    endwith
   endif
  endfunc

  func oRESIGLA_1_31.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (! empty(.w_RESIGLA))
    endwith
    return bRes
  endfunc

  add object oRECODCOM_1_32 as StdField with uid="NRKWZXCBJQ",rtseq=32,rtrep=.f.,;
    cFormVar = "w_RECODCOM", cQueryName = "RECODCOM",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Codice comune",;
    HelpContextID = 184384413,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=544, Top=266, cSayPict="repl('!',4)", cGetPict="repl('!',4)", InputMask=replicate('X',4)

  func oRECODCOM_1_32.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' or .w_Forza1)
    endwith
   endif
  endfunc

  add object oINDIRIZ_1_33 as StdField with uid="ZFPHCRDCWK",rtseq=33,rtrep=.f.,;
    cFormVar = "w_INDIRIZ", cQueryName = "INDIRIZ",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo ( frazione, via e numero civico) di residenza anagrafica o domicilio",;
    HelpContextID = 69428090,;
   bGlobalFont=.t.,;
    Height=21, Width=319, Left=142, Top=306, cSayPict='repl("!",35)', cGetPict='repl("!",35)', InputMask=replicate('X',35)

  func oINDIRIZ_1_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' or .w_Forza1)
    endwith
   endif
  endfunc

  add object oCAP_1_34 as StdField with uid="MDJULVDKRQ",rtseq=34,rtrep=.f.,;
    cFormVar = "w_CAP", cQueryName = "CAP",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Inserire il C.A.P. della residenza anagrafica o del domicilio fiscale del fornitore",;
    ToolTipText = "C.A.P. della residenza anagrafica o del domicilio fiscale del fornitore",;
    HelpContextID = 42709210,;
   bGlobalFont=.t.,;
    Height=21, Width=56, Left=469, Top=306, cSayPict="'99999'", cGetPict="'99999'", InputMask=replicate('X',5)

  func oCAP_1_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' or .w_Forza1)
    endwith
   endif
  endfunc

  func oCAP_1_34.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (! empty(.w_CAP))
    endwith
    return bRes
  endfunc

  add object oCODATT_1_35 as StdField with uid="UFNDOTUYER",rtseq=35,rtrep=.f.,;
    cFormVar = "w_CODATT", cQueryName = "CODATT",;
    bObbl = .f. , nPag = 1, value=space(6), bMultilanguage =  .f.,;
    ToolTipText = "Codice attivit�",;
    HelpContextID = 116694310,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=544, Top=306, cSayPict='repl("!",6)', cGetPict='repl("!",6)', InputMask=replicate('X',6), bHasZoom = .t. 

  func oCODATT_1_35.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' or .w_Forza1)
    endwith
   endif
  endfunc

  proc oCODATT_1_35.mZoom
    vx_exec("gsri8sft.vzm",this.Parent.oContained)
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oTELEFONO_1_36 as StdField with uid="QVUZNFWEXA",rtseq=36,rtrep=.f.,;
    cFormVar = "w_TELEFONO", cQueryName = "TELEFONO",;
    bObbl = .f. , nPag = 1, value=space(12), bMultilanguage =  .f.,;
    sErrorMsg = "Non inserire caratteri separatori tra prefisso e numero",;
    ToolTipText = "Numero di telefono, inserire solo numeri",;
    HelpContextID = 250014587,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=623, Top=306, cSayPict="repl('!',12)", cGetPict="repl('!',12)", InputMask=replicate('X',12)

  func oTELEFONO_1_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' or .w_Forza1)
    endwith
   endif
  endfunc

  func oTELEFONO_1_36.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (at(' ',alltrim(.w_TELEFONO))=0 and at('/',alltrim(.w_TELEFONO))=0 and at('-',alltrim(.w_TELEFONO))=0)
    endwith
    return bRes
  endfunc

  add object oSEVARSED_1_37 as StdField with uid="MKYAOPUZVB",rtseq=37,rtrep=.f.,;
    cFormVar = "w_SEVARSED", cQueryName = "SEVARSED",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data variazione sede legale",;
    HelpContextID = 97891434,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=36, Top=370

  func oSEVARSED_1_37.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza1)
    endwith
   endif
  endfunc

  add object oSECOMUNE_1_38 as StdField with uid="NFZSOTAWKE",rtseq=38,rtrep=.f.,;
    cFormVar = "w_SECOMUNE", cQueryName = "SECOMUNE",;
    bObbl = .t. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune della sede legale del fornitore",;
    HelpContextID = 141392789,;
   bGlobalFont=.t.,;
    Height=21, Width=319, Left=142, Top=370, cSayPict='repl("!",40)', cGetPict='repl("!",40)', InputMask=replicate('X',40)

  func oSECOMUNE_1_38.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza1)
    endwith
   endif
  endfunc

  add object oSESIGLA_1_39 as StdField with uid="OOQMIUMTFG",rtseq=39,rtrep=.f.,;
    cFormVar = "w_SESIGLA", cQueryName = "SESIGLA",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia della sede legale del fornitore",;
    HelpContextID = 237863974,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=469, Top=370, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2)

  func oSESIGLA_1_39.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza1)
    endwith
   endif
  endfunc

  add object oSEINDIRI2_1_40 as StdField with uid="OXQZLOKZQT",rtseq=40,rtrep=.f.,;
    cFormVar = "w_SEINDIRI2", cQueryName = "SEINDIRI2",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo della sede legale del fornitore",;
    HelpContextID = 184674191,;
   bGlobalFont=.t.,;
    Height=21, Width=319, Left=142, Top=408, cSayPict='repl("!",35)', cGetPict='repl("!",35)', InputMask=replicate('X',35)

  func oSEINDIRI2_1_40.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza1)
    endwith
   endif
  endfunc

  add object oSECAP_1_41 as StdField with uid="AVSZVWBCSZ",rtseq=41,rtrep=.f.,;
    cFormVar = "w_SECAP", cQueryName = "SECAP",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Inserire il C.A.P. della sede legale del fornitore",;
    ToolTipText = "C.A.P. della sede legale del fornitore",;
    HelpContextID = 223050714,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=469, Top=408, cSayPict="'99999'", cGetPict="'99999'", InputMask=replicate('X',5)

  func oSECAP_1_41.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza1)
    endwith
   endif
  endfunc

  func oSECAP_1_41.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (! empty(.w_SECAP))
    endwith
    return bRes
  endfunc

  add object oSECODCOM_1_42 as StdField with uid="PSHALDQRCE",rtseq=42,rtrep=.f.,;
    cFormVar = "w_SECODCOM", cQueryName = "SECODCOM",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Codice comune",;
    HelpContextID = 184384397,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=555, Top=408, cSayPict="repl('!',4)", cGetPict="repl('!',4)", InputMask=replicate('X',4)

  func oSECODCOM_1_42.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza1)
    endwith
   endif
  endfunc

  add object oSEVARDOM_1_43 as StdField with uid="CYDZEIXVFX",rtseq=43,rtrep=.f.,;
    cFormVar = "w_SEVARDOM", cQueryName = "SEVARDOM",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data variazione domicilio fiscale",;
    HelpContextID = 153766797,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=36, Top=450

  func oSEVARDOM_1_43.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza1)
    endwith
   endif
  endfunc

  add object oSERCOMUN_1_44 as StdField with uid="KMAFMNRNPP",rtseq=44,rtrep=.f.,;
    cFormVar = "w_SERCOMUN", cQueryName = "SERCOMUN",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune di domicilio fiscale del fornitore",;
    HelpContextID = 262632564,;
   bGlobalFont=.t.,;
    Height=21, Width=319, Left=142, Top=450, cSayPict='repl("!",40)', cGetPict='repl("!",40)', InputMask=replicate('X',40)

  func oSERCOMUN_1_44.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza1)
    endwith
   endif
  endfunc

  add object oSERSIGLA_1_45 as StdField with uid="TGOHNSUWWG",rtseq=45,rtrep=.f.,;
    cFormVar = "w_SERSIGLA", cQueryName = "SERSIGLA",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia di domicilio fiscale del fornitore",;
    HelpContextID = 111709081,;
   bGlobalFont=.t.,;
    Height=21, Width=32, Left=469, Top=450, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2)

  func oSERSIGLA_1_45.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza1)
    endwith
   endif
  endfunc

  add object oSERCODCOM_1_46 as StdField with uid="SCNIBQOVNV",rtseq=46,rtrep=.f.,;
    cFormVar = "w_SERCODCOM", cQueryName = "SERCODCOM",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Codice comune",;
    HelpContextID = 111638853,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=559, Top=450, cSayPict="repl('!',4)", cGetPict="repl('!',4)", InputMask=replicate('X',4)

  func oSERCODCOM_1_46.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza1)
    endwith
   endif
  endfunc

  add object oSERCAP_1_47 as StdField with uid="KHVCIKTTRJ",rtseq=47,rtrep=.f.,;
    cFormVar = "w_SERCAP", cQueryName = "SERCAP",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "C.A.P. del domicilio fiscale del fornitore",;
    HelpContextID = 29848614,;
   bGlobalFont=.t.,;
    Height=21, Width=56, Left=655, Top=450, cSayPict="'99999'", cGetPict="'99999'", InputMask=replicate('X',5)

  func oSERCAP_1_47.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza1)
    endwith
   endif
  endfunc

  add object oSERINDIR_1_48 as StdField with uid="ZEWEROQCEA",rtseq=48,rtrep=.f.,;
    cFormVar = "w_SERINDIR", cQueryName = "SERINDIR",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo ( frazione, via e numero civico) di domicilio fiscale del fornitore",;
    HelpContextID = 157453192,;
   bGlobalFont=.t.,;
    Height=21, Width=319, Left=142, Top=493, cSayPict='repl("!",35)', cGetPict='repl("!",35)', InputMask=replicate('X',35)

  func oSERINDIR_1_48.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza1)
    endwith
   endif
  endfunc

  add object oSECODATT_1_49 as StdField with uid="RQBOLJQSDR",rtseq=49,rtrep=.f.,;
    cFormVar = "w_SECODATT", cQueryName = "SECODATT",;
    bObbl = .f. , nPag = 1, value=space(6), bMultilanguage =  .f.,;
    ToolTipText = "Codice attivit�",;
    HelpContextID = 50496634,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=469, Top=493, cSayPict='repl("!",6)', cGetPict='repl("!",6)', InputMask=replicate('X',6), bHasZoom = .t. 

  func oSECODATT_1_49.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza1)
    endwith
   endif
  endfunc

  proc oSECODATT_1_49.mZoom
    vx_exec("gsri8sft1.vzm",this.Parent.oContained)
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oSETELEFONO_1_50 as StdField with uid="TDMFKZTMEM",rtseq=50,rtrep=.f.,;
    cFormVar = "w_SETELEFONO", cQueryName = "SETELEFONO",;
    bObbl = .f. , nPag = 1, value=space(12), bMultilanguage =  .f.,;
    sErrorMsg = "Non inserire caratteri separatori tra prefisso e numero",;
    ToolTipText = "Numero di telefono, inserire solo numeri",;
    HelpContextID = 125429845,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=559, Top=493, cSayPict="repl('!',12)", cGetPict="repl('!',12)", InputMask=replicate('X',12)

  func oSETELEFONO_1_50.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza1)
    endwith
   endif
  endfunc

  func oSETELEFONO_1_50.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (at(' ',alltrim(.w_SETELEFONO))=0 and at('/',alltrim(.w_SETELEFONO))=0 and at('-',alltrim(.w_SETELEFONO))=0)
    endwith
    return bRes
  endfunc

  add object oNATGIU_1_51 as StdField with uid="GGFCANZHTS",rtseq=51,rtrep=.f.,;
    cFormVar = "w_NATGIU", cQueryName = "NATGIU",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Valore non ammesso per natura giuridica",;
    ToolTipText = "Tipologia natura giuridica",;
    HelpContextID = 122392534,;
    FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=41, Left=655, Top=493, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2)

  func oNATGIU_1_51.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza1)
    endwith
   endif
  endfunc

  func oNATGIU_1_51.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((val(.w_NATGIU)>=1 and val(.w_NATGIU)<=44) OR (val(.w_NATGIU)>=50 and val(.w_NATGIU)<=58))
    endwith
    return bRes
  endfunc


  add object oSTATO_1_52 as StdCombo with uid="HVJLCFQSUG",rtseq=52,rtrep=.f.,left=142,top=536,width=135,height=21;
    , height = 21;
    , ToolTipText = "Stato della societ� o ente.";
    , HelpContextID = 222858458;
    , cFormVar="w_STATO",RowSource=""+"1 - Soggetto in normale attivit�,"+"2 - Soggetto in liquidazione per cessazione attivit�,"+"3 - Soggetto in fallimento o in liquidazione coatta amministrativa,"+"4 - Soggetto estinto", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSTATO_1_52.RadioValue()
    return(iif(this.value =1,'1',;
    iif(this.value =2,'2',;
    iif(this.value =3,'3',;
    iif(this.value =4,'4',;
    space(1))))))
  endfunc
  func oSTATO_1_52.GetRadio()
    this.Parent.oContained.w_STATO = this.RadioValue()
    return .t.
  endfunc

  func oSTATO_1_52.SetRadio()
    this.Parent.oContained.w_STATO=trim(this.Parent.oContained.w_STATO)
    this.value = ;
      iif(this.Parent.oContained.w_STATO=='1',1,;
      iif(this.Parent.oContained.w_STATO=='2',2,;
      iif(this.Parent.oContained.w_STATO=='3',3,;
      iif(this.Parent.oContained.w_STATO=='4',4,;
      0))))
  endfunc

  func oSTATO_1_52.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza1)
    endwith
   endif
  endfunc


  add object oSITUAZ_1_53 as StdCombo with uid="QWWEIZUDRN",value=1,rtseq=53,rtrep=.f.,left=278,top=536,width=187,height=21;
    , height = 21;
    , ToolTipText = "Situazione della societ� o ente";
    , HelpContextID = 198809638;
    , cFormVar="w_SITUAZ",RowSource=""+"Non selezionata,"+"1 - Periodo imposta inizio liquidazione,"+"2 - Periodo imposta successivo a periodo liquidazione o fallimento,"+"3 - Periodo imposta termine liquidazione,"+"5 - Periodo imposta avvenuta trasformazione,"+"6 - Periodo normale di imposta", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSITUAZ_1_53.RadioValue()
    return(iif(this.value =1,' ',;
    iif(this.value =2,'1',;
    iif(this.value =3,'2',;
    iif(this.value =4,'3',;
    iif(this.value =5,'5',;
    iif(this.value =6,'6',;
    space(1))))))))
  endfunc
  func oSITUAZ_1_53.GetRadio()
    this.Parent.oContained.w_SITUAZ = this.RadioValue()
    return .t.
  endfunc

  func oSITUAZ_1_53.SetRadio()
    this.Parent.oContained.w_SITUAZ=trim(this.Parent.oContained.w_SITUAZ)
    this.value = ;
      iif(this.Parent.oContained.w_SITUAZ=='',1,;
      iif(this.Parent.oContained.w_SITUAZ=='1',2,;
      iif(this.Parent.oContained.w_SITUAZ=='2',3,;
      iif(this.Parent.oContained.w_SITUAZ=='3',4,;
      iif(this.Parent.oContained.w_SITUAZ=='5',5,;
      iif(this.Parent.oContained.w_SITUAZ=='6',6,;
      0))))))
  endfunc

  func oSITUAZ_1_53.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza1)
    endwith
   endif
  endfunc

  add object oCODFISDA_1_54 as StdField with uid="XSNKRKYERI",rtseq=54,rtrep=.f.,;
    cFormVar = "w_CODFISDA", cQueryName = "CODFISDA",;
    bObbl = .f. , nPag = 1, value=space(11), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale del dicastero di appartenenza",;
    HelpContextID = 88710503,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=469, Top=536, cSayPict='repl("!",11)', cGetPict='repl("!",11)', InputMask=replicate('X',11)

  func oCODFISDA_1_54.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza1)
    endwith
   endif
  endfunc

  func oCODFISDA_1_54.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (iif(not empty(.w_CODFISDA),chkcfp(alltrim(.w_CODFISDA),'PI'),.T.))
    endwith
    return bRes
  endfunc

  add object oStr_1_64 as StdString with uid="PZCKHMGKWT",Visible=.t., Left=27, Top=11,;
    Alignment=1, Width=98, Height=15,;
    Caption="Cognome:"  ;
  , bGlobalFont=.t.

  add object oStr_1_65 as StdString with uid="MIJLOSOMKI",Visible=.t., Left=534, Top=10,;
    Alignment=1, Width=37, Height=15,;
    Caption="Nome:"  ;
  , bGlobalFont=.t.

  add object oStr_1_67 as StdString with uid="TZKQRFHOUY",Visible=.t., Left=34, Top=86,;
    Alignment=0, Width=204, Height=15,;
    Caption="Tipo di dichiarazione"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_68 as StdString with uid="UCIEAMMITE",Visible=.t., Left=27, Top=35,;
    Alignment=1, Width=98, Height=15,;
    Caption="Denominazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_69 as StdString with uid="HLIVBEVOMR",Visible=.t., Left=27, Top=60,;
    Alignment=1, Width=98, Height=15,;
    Caption="Codice fiscale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_74 as StdString with uid="EFRWRPPCTM",Visible=.t., Left=36, Top=191,;
    Alignment=0, Width=204, Height=15,;
    Caption="Persona fisica"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_75 as StdString with uid="HRDBHHFBYE",Visible=.t., Left=142, Top=251,;
    Alignment=0, Width=223, Height=13,;
    Caption="Comune di residenza o domicilio fiscale"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_76 as StdString with uid="ZDKIULOHFL",Visible=.t., Left=36, Top=211,;
    Alignment=0, Width=103, Height=13,;
    Caption="Data di nascita"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_77 as StdString with uid="CTCAOZXFVH",Visible=.t., Left=142, Top=211,;
    Alignment=0, Width=223, Height=13,;
    Caption="Comune di nascita"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_78 as StdString with uid="VETKYUGDVO",Visible=.t., Left=469, Top=211,;
    Alignment=0, Width=48, Height=13,;
    Caption="Provincia"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_79 as StdString with uid="FWEPYXGQZJ",Visible=.t., Left=36, Top=251,;
    Alignment=0, Width=87, Height=13,;
    Caption="Data variazione"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_80 as StdString with uid="HVQAZPYBNO",Visible=.t., Left=469, Top=251,;
    Alignment=0, Width=48, Height=13,;
    Caption="Provincia"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_81 as StdString with uid="ATTHQLRPRN",Visible=.t., Left=142, Top=290,;
    Alignment=0, Width=216, Height=13,;
    Caption="Indirizzo, frazione, via e numero civico"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_82 as StdString with uid="KSQHILFMHS",Visible=.t., Left=544, Top=211,;
    Alignment=0, Width=48, Height=13,;
    Caption="Sesso"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_83 as StdString with uid="IVLKBJFSJW",Visible=.t., Left=469, Top=290,;
    Alignment=0, Width=56, Height=13,;
    Caption="CAP"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_84 as StdString with uid="YZOMBVBHCP",Visible=.t., Left=544, Top=290,;
    Alignment=0, Width=77, Height=13,;
    Caption="Cod.attivit�"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_85 as StdString with uid="AIZMGLVLMZ",Visible=.t., Left=623, Top=290,;
    Alignment=0, Width=48, Height=13,;
    Caption="Telefono"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_89 as StdString with uid="DEKDZLFCML",Visible=.t., Left=36, Top=333,;
    Alignment=0, Width=228, Height=15,;
    Caption="Altri soggetti"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_90 as StdString with uid="EBKJKEHJKK",Visible=.t., Left=36, Top=354,;
    Alignment=0, Width=80, Height=13,;
    Caption="Sede legale"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_91 as StdString with uid="RZVEDQJFMM",Visible=.t., Left=142, Top=354,;
    Alignment=0, Width=76, Height=13,;
    Caption="Comune"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_92 as StdString with uid="UNXIIROKSZ",Visible=.t., Left=654, Top=475,;
    Alignment=0, Width=69, Height=13,;
    Caption="Nat. giuridica"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_93 as StdString with uid="UQKLITXGSW",Visible=.t., Left=469, Top=354,;
    Alignment=0, Width=48, Height=13,;
    Caption="Provincia"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_94 as StdString with uid="EXNBDAOCWO",Visible=.t., Left=142, Top=394,;
    Alignment=0, Width=216, Height=13,;
    Caption="Indirizzo, frazione, via e numero civico"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_95 as StdString with uid="WLXGABIKNW",Visible=.t., Left=469, Top=394,;
    Alignment=0, Width=56, Height=13,;
    Caption="CAP"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_96 as StdString with uid="RMDNMKOJQJ",Visible=.t., Left=36, Top=434,;
    Alignment=0, Width=76, Height=13,;
    Caption="Dom. fiscale"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_97 as StdString with uid="ZVHEBJFODD",Visible=.t., Left=142, Top=434,;
    Alignment=0, Width=76, Height=13,;
    Caption="Comune"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_98 as StdString with uid="VJNNNJGRSB",Visible=.t., Left=469, Top=434,;
    Alignment=0, Width=48, Height=13,;
    Caption="Provincia"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_99 as StdString with uid="XMGFDNXRGY",Visible=.t., Left=142, Top=475,;
    Alignment=0, Width=216, Height=13,;
    Caption="Indirizzo, frazione, via e numero civico"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_100 as StdString with uid="CNBFEAFDFD",Visible=.t., Left=655, Top=434,;
    Alignment=0, Width=56, Height=13,;
    Caption="CAP"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_101 as StdString with uid="DYEPGPKKPJ",Visible=.t., Left=469, Top=475,;
    Alignment=0, Width=84, Height=13,;
    Caption="Codice attivit�"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_102 as StdString with uid="ZUHOUEPINF",Visible=.t., Left=559, Top=475,;
    Alignment=0, Width=47, Height=13,;
    Caption="Telefono"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_103 as StdString with uid="TVXFVOYTZB",Visible=.t., Left=142, Top=519,;
    Alignment=0, Width=48, Height=13,;
    Caption="Stato"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_104 as StdString with uid="GPUYEQERVJ",Visible=.t., Left=278, Top=519,;
    Alignment=0, Width=64, Height=13,;
    Caption="Situazione"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_105 as StdString with uid="ARBZPVKLZS",Visible=.t., Left=469, Top=518,;
    Alignment=0, Width=248, Height=13,;
    Caption="Codice fiscale del dicastero di appartenenza"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_106 as StdString with uid="BBSCZKFXTA",Visible=.t., Left=604, Top=118,;
    Alignment=0, Width=101, Height=13,;
    Caption="Tipo operazione"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_108 as StdString with uid="HPMMFXHZSB",Visible=.t., Left=544, Top=251,;
    Alignment=0, Width=91, Height=13,;
    Caption="Cod. comune"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_109 as StdString with uid="DLHBLVEZGX",Visible=.t., Left=559, Top=394,;
    Alignment=0, Width=91, Height=13,;
    Caption="Cod. comune"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_110 as StdString with uid="SNVZONYPPB",Visible=.t., Left=559, Top=434,;
    Alignment=0, Width=91, Height=13,;
    Caption="Cod. comune"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_111 as StdString with uid="ZHPUEMRERJ",Visible=.t., Left=356, Top=60,;
    Alignment=1, Width=215, Height=15,;
    Caption="Codice fiscale sostituto d'imposta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_112 as StdString with uid="JTEPEFQRBC",Visible=.t., Left=65, Top=149,;
    Alignment=0, Width=154, Height=14,;
    Caption="Eventi eccezionali"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_1_66 as StdBox with uid="YPBURJVQFL",left=32, top=103, width=681,height=2

  add object oBox_1_73 as StdBox with uid="TFBVYLMEDX",left=32, top=206, width=681,height=2

  add object oBox_1_88 as StdBox with uid="HJLYLCJQUK",left=32, top=348, width=681,height=2
enddefine
define class tgsri8sftPag2 as StdContainer
  Width  = 747
  height = 562
  stdWidth  = 747
  stdheight = 562
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oForza2_2_1 as StdCheck with uid="ANPGYPQFYE",rtseq=69,rtrep=.f.,left=563, top=5, caption="Forza editing",;
    HelpContextID = 167731882,;
    cFormVar="w_Forza2", bObbl = .f. , nPag = 2;
    , TABSTOP=.F.;
   , bGlobalFont=.t.


  func oForza2_2_1.RadioValue()
    return(iif(this.value =1,.t.,;
    .f.))
  endfunc
  func oForza2_2_1.GetRadio()
    this.Parent.oContained.w_Forza2 = this.RadioValue()
    return .t.
  endfunc

  func oForza2_2_1.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_Forza2==.t.,1,;
      0)
  endfunc

  add object oRFCODFIS_2_3 as StdField with uid="MTXMRCWFQH",rtseq=71,rtrep=.f.,;
    cFormVar = "w_RFCODFIS", cQueryName = "RFCODFIS",;
    bObbl = .f. , nPag = 2, value=space(16), bMultilanguage =  .f.,;
    HelpContextID = 134052503,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=33, Top=54, cSayPict='repl("!",16)', cGetPict='repl("!",16)', InputMask=replicate('X',16)

  func oRFCODFIS_2_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PERAZI <> 'S' or (.w_Forza2 and .w_PERAZI='S'))
    endwith
   endif
  endfunc

  func oRFCODFIS_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (iif(not empty(.w_RFCODFIS),chkcfp(alltrim(.w_RFCODFIS),'CF'),.T.))
    endwith
    return bRes
  endfunc


  add object oRFCODCAR_2_4 as StdCombo with uid="YYMESQRZRO",rtseq=72,rtrep=.f.,left=222,top=54,width=286,height=21;
    , ToolTipText = "Codice carica";
    , HelpContextID = 84051304;
    , cFormVar="w_RFCODCAR",RowSource=""+"1) Rappr. legale o negoziale,"+"2) Rappr. di minore - curatore eredit�,"+"3) Curatore fallimentare,"+"4) Commissario liquidatore,"+"5) Commissario giudiziale,"+"6) Rappr. fiscale di soggetto non residente,"+"7) Erede del dichiarante,"+"8) Liquidatore volontario,"+"9) Sogg. dich. IVA operaz. straord.,"+"11) Tutore,"+"12) Liquid. volont. ditta indiv.,"+"13) Ammin. condominio,"+"14) Sogg. per conto P.A.,"+"15) Comm. liquid. P.A.", bObbl = .t. , nPag = 2;
  , bGlobalFont=.t.


  func oRFCODCAR_2_4.RadioValue()
    return(iif(this.value =1,'1',;
    iif(this.value =2,'2',;
    iif(this.value =3,'3',;
    iif(this.value =4,'4',;
    iif(this.value =5,'5',;
    iif(this.value =6,'6',;
    iif(this.value =7,'7',;
    iif(this.value =8,'8',;
    iif(this.value =9,'9',;
    iif(this.value =10,'11',;
    iif(this.value =11,'12',;
    iif(this.value =12,'13',;
    iif(this.value =13,'14',;
    iif(this.value =14,'15',;
    '  ')))))))))))))))
  endfunc
  func oRFCODCAR_2_4.GetRadio()
    this.Parent.oContained.w_RFCODCAR = this.RadioValue()
    return .t.
  endfunc

  func oRFCODCAR_2_4.SetRadio()
    this.Parent.oContained.w_RFCODCAR=trim(this.Parent.oContained.w_RFCODCAR)
    this.value = ;
      iif(this.Parent.oContained.w_RFCODCAR=='1',1,;
      iif(this.Parent.oContained.w_RFCODCAR=='2',2,;
      iif(this.Parent.oContained.w_RFCODCAR=='3',3,;
      iif(this.Parent.oContained.w_RFCODCAR=='4',4,;
      iif(this.Parent.oContained.w_RFCODCAR=='5',5,;
      iif(this.Parent.oContained.w_RFCODCAR=='6',6,;
      iif(this.Parent.oContained.w_RFCODCAR=='7',7,;
      iif(this.Parent.oContained.w_RFCODCAR=='8',8,;
      iif(this.Parent.oContained.w_RFCODCAR=='9',9,;
      iif(this.Parent.oContained.w_RFCODCAR=='11',10,;
      iif(this.Parent.oContained.w_RFCODCAR=='12',11,;
      iif(this.Parent.oContained.w_RFCODCAR=='13',12,;
      iif(this.Parent.oContained.w_RFCODCAR=='14',13,;
      iif(this.Parent.oContained.w_RFCODCAR=='15',14,;
      0))))))))))))))
  endfunc

  func oRFCODCAR_2_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PERAZI <> 'S' or .w_Forza2)
    endwith
   endif
  endfunc

  add object oRFDENOMI_2_5 as StdField with uid="VRBLXOZTDU",rtseq=73,rtrep=.f.,;
    cFormVar = "w_RFDENOMI", cQueryName = "RFDENOMI",;
    bObbl = .f. , nPag = 2, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Ragione sociale del rappresentante",;
    HelpContextID = 241658529,;
   bGlobalFont=.t.,;
    Height=21, Width=197, Left=514, Top=54, cSayPict='repl("!",60)', cGetPict='repl("!",60)', InputMask=replicate('X',60)

  func oRFDENOMI_2_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_RFCOGNOME) and (.w_PERAZI<>'S' or (.w_PERAZI='S' and .w_Forza2)))
    endwith
   endif
  endfunc

  add object oRFCOGNOME_2_6 as StdField with uid="TUTTYIGKSZ",rtseq=74,rtrep=.f.,;
    cFormVar = "w_RFCOGNOME", cQueryName = "RFCOGNOME",;
    bObbl = .f. , nPag = 2, value=space(24), bMultilanguage =  .f.,;
    ToolTipText = "Cognome del rappresentante",;
    HelpContextID = 265123405,;
   bGlobalFont=.t.,;
    Height=21, Width=177, Left=33, Top=97, cSayPict='repl("!",24)', cGetPict='repl("!",24)', InputMask=replicate('X',24)

  func oRFCOGNOME_2_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_RFDENOMI) and (.w_Perazi<>'S' or .w_Forza2))
    endwith
   endif
  endfunc

  add object oRFNOME_2_7 as StdField with uid="RKLEAKPRVC",rtseq=75,rtrep=.f.,;
    cFormVar = "w_RFNOME", cQueryName = "RFNOME",;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Nome del rappresentante",;
    HelpContextID = 141347562,;
   bGlobalFont=.t.,;
    Height=21, Width=160, Left=253, Top=97, cSayPict='repl("!",20)', cGetPict='repl("!",20)', InputMask=replicate('X',20)

  func oRFNOME_2_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_RFDENOMI) and (.w_Perazi<>'S' or .w_Forza2))
    endwith
   endif
  endfunc


  add object oRFSESSO_2_8 as StdCombo with uid="WGXEPZGXVZ",value=1,rtseq=76,rtrep=.f.,left=434,top=97,width=114,height=21;
    , height = 21;
    , ToolTipText = "Sesso del rappresentante";
    , HelpContextID = 169245418;
    , cFormVar="w_RFSESSO",RowSource=""+"Non selezionato,"+"Maschio,"+"Femmina", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oRFSESSO_2_8.RadioValue()
    return(iif(this.value =1,' ',;
    iif(this.value =2,'M',;
    iif(this.value =3,'F',;
    space(1)))))
  endfunc
  func oRFSESSO_2_8.GetRadio()
    this.Parent.oContained.w_RFSESSO = this.RadioValue()
    return .t.
  endfunc

  func oRFSESSO_2_8.SetRadio()
    this.Parent.oContained.w_RFSESSO=trim(this.Parent.oContained.w_RFSESSO)
    this.value = ;
      iif(this.Parent.oContained.w_RFSESSO=='',1,;
      iif(this.Parent.oContained.w_RFSESSO=='M',2,;
      iif(this.Parent.oContained.w_RFSESSO=='F',3,;
      0)))
  endfunc

  func oRFSESSO_2_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_RFDENOMI) and (.w_Perazi<>'S' or .w_Forza2))
    endwith
   endif
  endfunc

  add object oRFDATANASC_2_9 as StdField with uid="AUOENXXQZR",rtseq=77,rtrep=.f.,;
    cFormVar = "w_RFDATANASC", cQueryName = "RFDATANASC",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di nascita del rappresentante",;
    HelpContextID = 202056313,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=33, Top=142

  func oRFDATANASC_2_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_RFDENOMI) and (.w_Perazi<>'S' or .w_Forza2))
    endwith
   endif
  endfunc

  add object oRFCOMNAS_2_10 as StdField with uid="RPGGCLVJHX",rtseq=78,rtrep=.f.,;
    cFormVar = "w_RFCOMNAS", cQueryName = "RFCOMNAS",;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune (o stato estero) di nascita del rappresentante",;
    HelpContextID = 9602409,;
   bGlobalFont=.t.,;
    Height=21, Width=260, Left=152, Top=142, cSayPict='repl("!",40)', cGetPict='repl("!",40)', InputMask=replicate('X',40)

  func oRFCOMNAS_2_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_RFDENOMI) and (.w_Perazi<>'S' or .w_Forza2))
    endwith
   endif
  endfunc

  add object oRFSIGNAS_2_11 as StdField with uid="DAVHPWZQVP",rtseq=79,rtrep=.f.,;
    cFormVar = "w_RFSIGNAS", cQueryName = "RFSIGNAS",;
    bObbl = .f. , nPag = 2, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia di nascita del rappresentante",;
    HelpContextID = 2983273,;
   bGlobalFont=.t.,;
    Height=21, Width=32, Left=432, Top=142, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2)

  func oRFSIGNAS_2_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_RFDENOMI) and (.w_Perazi<>'S' or .w_Forza2))
    endwith
   endif
  endfunc

  add object oRFCOMUNE_2_12 as StdField with uid="DDNELJFMGC",rtseq=80,rtrep=.f.,;
    cFormVar = "w_RFCOMUNE", cQueryName = "RFCOMUNE",;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune di residenza anagrafica o di domicilio fiscale del rappresentante",;
    HelpContextID = 141392549,;
   bGlobalFont=.t.,;
    Height=21, Width=296, Left=33, Top=183, cSayPict='repl("!",40)', cGetPict='repl("!",40)', InputMask=replicate('X',40)

  func oRFCOMUNE_2_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza2)
    endwith
   endif
  endfunc

  add object oRFSIGLA_2_13 as StdField with uid="MPTADLFURD",rtseq=81,rtrep=.f.,;
    cFormVar = "w_RFSIGLA", cQueryName = "RFSIGLA",;
    bObbl = .f. , nPag = 2, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia di residenza anagrafica o di domicilio fiscale",;
    HelpContextID = 237864214,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=379, Top=183, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2)

  func oRFSIGLA_2_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza2)
    endwith
   endif
  endfunc

  add object oRFCAP_2_14 as StdField with uid="VMMUJSTIQO",rtseq=82,rtrep=.f.,;
    cFormVar = "w_RFCAP", cQueryName = "RFCAP",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "C.A.P. della residenza anagrafica o del domicilio fiscale del rappresentante",;
    HelpContextID = 223050474,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=432, Top=183, cSayPict="'99999'", cGetPict="'99999'", InputMask=replicate('X',5)

  func oRFCAP_2_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza2)
    endwith
   endif
  endfunc

  add object oRFINDIRIZ_2_15 as StdField with uid="HCOWWPHROD",rtseq=83,rtrep=.f.,;
    cFormVar = "w_RFINDIRIZ", cQueryName = "RFINDIRIZ",;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo ( frazione, via e numero civico) di residenza anagrafica o domicilio",;
    HelpContextID = 184675071,;
   bGlobalFont=.t.,;
    Height=21, Width=296, Left=33, Top=222, cSayPict='repl("!",35)', cGetPict='repl("!",35)', InputMask=replicate('X',35)

  func oRFINDIRIZ_2_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza2)
    endwith
   endif
  endfunc

  add object oRFTELEFONO_2_16 as StdField with uid="MMGWOVEOZI",rtseq=84,rtrep=.f.,;
    cFormVar = "w_RFTELEFONO", cQueryName = "RFTELEFONO",;
    bObbl = .f. , nPag = 2, value=space(12), bMultilanguage =  .f.,;
    sErrorMsg = "Non inserire caratteri separatori tra prefisso e numero",;
    HelpContextID = 125430085,;
   bGlobalFont=.t.,;
    Height=21, Width=104, Left=379, Top=222, cSayPict="repl('!',12)", cGetPict="repl('!',12)", InputMask=replicate('X',12)

  func oRFTELEFONO_2_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_Forza2)
    endwith
   endif
  endfunc

  func oRFTELEFONO_2_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (at(' ',alltrim(.w_RFTELEFONO))=0 and at('/',alltrim(.w_RFTELEFONO))=0 and at('-',alltrim(.w_RFTELEFONO))=0)
    endwith
    return bRes
  endfunc

  add object oRFDATCAR_2_17 as StdField with uid="VJTTUCHDUN",rtseq=85,rtrep=.f.,;
    cFormVar = "w_RFDATCAR", cQueryName = "RFDATCAR",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data decorrenza carica",;
    HelpContextID = 99915112,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=35, Top=267

  func oRFDATCAR_2_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PERAZI <> 'S' or .w_Forza2)
    endwith
   endif
  endfunc

  add object oRFDATFAL_2_18 as StdField with uid="YBFSONZHOQ",rtseq=86,rtrep=.f.,;
    cFormVar = "w_RFDATFAL", cQueryName = "RFDATFAL",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di apertura fallimento",;
    HelpContextID = 150246754,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=240, Top=267

  func oRFDATFAL_2_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PERAZI <> 'S' or .w_Forza2)
    endwith
   endif
  endfunc

  func oRFDATFAL_2_18.mHide()
    with this.Parent.oContained
      return (! inlist(.w_RFCODCAR,'3','4'))
    endwith
  endfunc

  add object oSEZ1_2_19 as StdCheck with uid="IATICRAGRO",rtseq=87,rtrep=.f.,left=27, top=337, caption="Sezione I - trasmissione integrale modello 770 semplificato",;
    HelpContextID = 39455706,;
    cFormVar="w_SEZ1", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oSEZ1_2_19.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oSEZ1_2_19.GetRadio()
    this.Parent.oContained.w_SEZ1 = this.RadioValue()
    return .t.
  endfunc

  func oSEZ1_2_19.SetRadio()
    this.Parent.oContained.w_SEZ1=trim(this.Parent.oContained.w_SEZ1)
    this.value = ;
      iif(this.Parent.oContained.w_SEZ1=='1',1,;
      0)
  endfunc

  add object oqSS1_2_20 as StdCheck with uid="FDPHJSPFOT",rtseq=88,rtrep=.f.,left=597, top=340, caption="SS",;
    HelpContextID = 39480314,;
    cFormVar="w_qSS1", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oqSS1_2_20.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oqSS1_2_20.GetRadio()
    this.Parent.oContained.w_qSS1 = this.RadioValue()
    return .t.
  endfunc

  func oqSS1_2_20.SetRadio()
    this.Parent.oContained.w_qSS1=trim(this.Parent.oContained.w_qSS1)
    this.value = ;
      iif(this.Parent.oContained.w_qSS1=='1',1,;
      0)
  endfunc

  func oqSS1_2_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SEZ1='1')
    endwith
   endif
  endfunc

  add object oqST1_2_21 as StdCheck with uid="BGCLFWVZAL",rtseq=89,rtrep=.f.,left=646, top=340, caption="ST",;
    HelpContextID = 39476218,;
    cFormVar="w_qST1", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oqST1_2_21.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oqST1_2_21.GetRadio()
    this.Parent.oContained.w_qST1 = this.RadioValue()
    return .t.
  endfunc

  func oqST1_2_21.SetRadio()
    this.Parent.oContained.w_qST1=trim(this.Parent.oContained.w_qST1)
    this.value = ;
      iif(this.Parent.oContained.w_qST1=='1',1,;
      0)
  endfunc

  func oqST1_2_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SEZ1='1')
    endwith
   endif
  endfunc

  add object oqSX1_2_22 as StdCheck with uid="ZXSRWVYXVB",rtseq=90,rtrep=.f.,left=696, top=340, caption="SX",;
    HelpContextID = 39459834,;
    cFormVar="w_qSX1", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oqSX1_2_22.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oqSX1_2_22.GetRadio()
    this.Parent.oContained.w_qSX1 = this.RadioValue()
    return .t.
  endfunc

  func oqSX1_2_22.SetRadio()
    this.Parent.oContained.w_qSX1=trim(this.Parent.oContained.w_qSX1)
    this.value = ;
      iif(this.Parent.oContained.w_qSX1=='1',1,;
      0)
  endfunc

  func oqSX1_2_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SEZ1='1')
    endwith
   endif
  endfunc

  add object oq771_2_23 as StdCheck with uid="KKOIUQJLMI",rtseq=91,rtrep=.f.,left=597, top=364, caption="770 ordinario 2008",;
    HelpContextID = 39602170,;
    cFormVar="w_q771", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oq771_2_23.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oq771_2_23.GetRadio()
    this.Parent.oContained.w_q771 = this.RadioValue()
    return .t.
  endfunc

  func oq771_2_23.SetRadio()
    this.Parent.oContained.w_q771=trim(this.Parent.oContained.w_q771)
    this.value = ;
      iif(this.Parent.oContained.w_q771=='1',1,;
      0)
  endfunc

  func oq771_2_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SEZ1='1')
    endwith
   endif
  endfunc

  add object oSEZ2_2_24 as StdCheck with uid="SEIYBKNVNP",rtseq=92,rtrep=.f.,left=27, top=388, caption="Sezione II - trasmissione modello 770 semplificato in due parti",;
    HelpContextID = 39390170,;
    cFormVar="w_SEZ2", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oSEZ2_2_24.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oSEZ2_2_24.GetRadio()
    this.Parent.oContained.w_SEZ2 = this.RadioValue()
    return .t.
  endfunc

  func oSEZ2_2_24.SetRadio()
    this.Parent.oContained.w_SEZ2=trim(this.Parent.oContained.w_SEZ2)
    this.value = ;
      iif(this.Parent.oContained.w_SEZ2=='1',1,;
      0)
  endfunc

  add object oqSS12_2_25 as StdCheck with uid="ZLYMXAXWCP",rtseq=93,rtrep=.f.,left=597, top=391, caption="SS",;
    HelpContextID = 255486970,;
    cFormVar="w_qSS12", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oqSS12_2_25.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oqSS12_2_25.GetRadio()
    this.Parent.oContained.w_qSS12 = this.RadioValue()
    return .t.
  endfunc

  func oqSS12_2_25.SetRadio()
    this.Parent.oContained.w_qSS12=trim(this.Parent.oContained.w_qSS12)
    this.value = ;
      iif(this.Parent.oContained.w_qSS12=='1',1,;
      0)
  endfunc

  func oqSS12_2_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SEZ2='1')
    endwith
   endif
  endfunc

  add object oSEZ3_2_26 as StdCheck with uid="JCOMOCOMMG",rtseq=94,rtrep=.f.,left=27, top=424, caption="Sezione III - trasmissione modello 770 semplificato per le sole",;
    HelpContextID = 39324634,;
    cFormVar="w_SEZ3", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oSEZ3_2_26.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oSEZ3_2_26.GetRadio()
    this.Parent.oContained.w_SEZ3 = this.RadioValue()
    return .t.
  endfunc

  func oSEZ3_2_26.SetRadio()
    this.Parent.oContained.w_SEZ3=trim(this.Parent.oContained.w_SEZ3)
    this.value = ;
      iif(this.Parent.oContained.w_SEZ3=='1',1,;
      0)
  endfunc

  func oSEZ3_2_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.F.)
    endwith
   endif
  endfunc

  add object oqSS13_2_27 as StdCheck with uid="DUXDQHEYEX",rtseq=95,rtrep=.f.,left=597, top=427, caption="SS",;
    HelpContextID = 254438394,;
    cFormVar="w_qSS13", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oqSS13_2_27.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oqSS13_2_27.GetRadio()
    this.Parent.oContained.w_qSS13 = this.RadioValue()
    return .t.
  endfunc

  func oqSS13_2_27.SetRadio()
    this.Parent.oContained.w_qSS13=trim(this.Parent.oContained.w_qSS13)
    this.value = ;
      iif(this.Parent.oContained.w_qSS13=='1',1,;
      0)
  endfunc

  func oqSS13_2_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.F.)
    endwith
   endif
  endfunc

  add object oqST13_2_28 as StdCheck with uid="DQLFPXPMQP",rtseq=96,rtrep=.f.,left=646, top=427, caption="ST",;
    HelpContextID = 254434298,;
    cFormVar="w_qST13", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oqST13_2_28.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oqST13_2_28.GetRadio()
    this.Parent.oContained.w_qST13 = this.RadioValue()
    return .t.
  endfunc

  func oqST13_2_28.SetRadio()
    this.Parent.oContained.w_qST13=trim(this.Parent.oContained.w_qST13)
    this.value = ;
      iif(this.Parent.oContained.w_qST13=='1',1,;
      0)
  endfunc

  func oqST13_2_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.F.)
    endwith
   endif
  endfunc

  add object oqSX13_2_29 as StdCheck with uid="CTCKRHAZLH",rtseq=97,rtrep=.f.,left=696, top=427, caption="SX",;
    HelpContextID = 254417914,;
    cFormVar="w_qSX13", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oqSX13_2_29.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oqSX13_2_29.GetRadio()
    this.Parent.oContained.w_qSX13 = this.RadioValue()
    return .t.
  endfunc

  func oqSX13_2_29.SetRadio()
    this.Parent.oContained.w_qSX13=trim(this.Parent.oContained.w_qSX13)
    this.value = ;
      iif(this.Parent.oContained.w_qSX13=='1',1,;
      0)
  endfunc

  func oqSX13_2_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.F.)
    endwith
   endif
  endfunc

  add object oq773_2_30 as StdCheck with uid="RFGQSXMJHK",rtseq=98,rtrep=.f.,left=597, top=456, caption="770 ordinario 2008",;
    HelpContextID = 39471098,;
    cFormVar="w_q773", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oq773_2_30.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oq773_2_30.GetRadio()
    this.Parent.oContained.w_q773 = this.RadioValue()
    return .t.
  endfunc

  func oq773_2_30.SetRadio()
    this.Parent.oContained.w_q773=trim(this.Parent.oContained.w_q773)
    this.value = ;
      iif(this.Parent.oContained.w_q773=='1',1,;
      0)
  endfunc

  func oq773_2_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.f.)
    endwith
   endif
  endfunc

  add object oSEZ4_2_31 as StdCheck with uid="GYUOHRLOVF",rtseq=99,rtrep=.f.,left=27, top=482, caption="Sezione IV - trasmissione modello 770 semplificato per le sole",;
    HelpContextID = 39259098,;
    cFormVar="w_SEZ4", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oSEZ4_2_31.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oSEZ4_2_31.GetRadio()
    this.Parent.oContained.w_SEZ4 = this.RadioValue()
    return .t.
  endfunc

  func oSEZ4_2_31.SetRadio()
    this.Parent.oContained.w_SEZ4=trim(this.Parent.oContained.w_SEZ4)
    this.value = ;
      iif(this.Parent.oContained.w_SEZ4=='1',1,;
      0)
  endfunc

  add object oqSS14_2_32 as StdCheck with uid="NVZNOPVLBE",rtseq=100,rtrep=.f.,left=597, top=485, caption="SS",;
    HelpContextID = 253389818,;
    cFormVar="w_qSS14", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oqSS14_2_32.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oqSS14_2_32.GetRadio()
    this.Parent.oContained.w_qSS14 = this.RadioValue()
    return .t.
  endfunc

  func oqSS14_2_32.SetRadio()
    this.Parent.oContained.w_qSS14=trim(this.Parent.oContained.w_qSS14)
    this.value = ;
      iif(this.Parent.oContained.w_qSS14=='1',1,;
      0)
  endfunc

  func oqSS14_2_32.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SEZ4='1')
    endwith
   endif
  endfunc

  add object oqST14_2_33 as StdCheck with uid="PJKXDUVTCT",rtseq=101,rtrep=.f.,left=646, top=485, caption="ST",;
    HelpContextID = 253385722,;
    cFormVar="w_qST14", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oqST14_2_33.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oqST14_2_33.GetRadio()
    this.Parent.oContained.w_qST14 = this.RadioValue()
    return .t.
  endfunc

  func oqST14_2_33.SetRadio()
    this.Parent.oContained.w_qST14=trim(this.Parent.oContained.w_qST14)
    this.value = ;
      iif(this.Parent.oContained.w_qST14=='1',1,;
      0)
  endfunc

  func oqST14_2_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SEZ4='1')
    endwith
   endif
  endfunc

  add object oqSX14_2_34 as StdCheck with uid="INCRNISXXR",rtseq=102,rtrep=.f.,left=696, top=485, caption="SX",;
    HelpContextID = 253369338,;
    cFormVar="w_qSX14", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oqSX14_2_34.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oqSX14_2_34.GetRadio()
    this.Parent.oContained.w_qSX14 = this.RadioValue()
    return .t.
  endfunc

  func oqSX14_2_34.SetRadio()
    this.Parent.oContained.w_qSX14=trim(this.Parent.oContained.w_qSX14)
    this.value = ;
      iif(this.Parent.oContained.w_qSX14=='1',1,;
      0)
  endfunc

  func oqSX14_2_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SEZ4='1')
    endwith
   endif
  endfunc

  add object oq774_2_35 as StdCheck with uid="UWZUSBDPCN",rtseq=103,rtrep=.f.,left=597, top=512, caption="770 ordinario 2008",;
    HelpContextID = 39405562,;
    cFormVar="w_q774", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oq774_2_35.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oq774_2_35.GetRadio()
    this.Parent.oContained.w_q774 = this.RadioValue()
    return .t.
  endfunc

  func oq774_2_35.SetRadio()
    this.Parent.oContained.w_q774=trim(this.Parent.oContained.w_q774)
    this.value = ;
      iif(this.Parent.oContained.w_q774=='1',1,;
      0)
  endfunc

  func oq774_2_35.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SEZ4='1')
    endwith
   endif
  endfunc

  add object oRFCFINS2_2_36 as StdField with uid="ZFMCVGTPTY",rtseq=104,rtrep=.f.,;
    cFormVar = "w_RFCFINS2", cQueryName = "RFCFINS2",;
    bObbl = .f. , nPag = 2, value=space(13), bMultilanguage =  .f.,;
    sErrorMsg = "Inserire il codice fiscale dell'intermediario della sezione IV",;
    ToolTipText = "Codice fiscale dell'intermediario della sezione IV",;
    HelpContextID = 4818248,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=542, Top=539, cSayPict="repl('!',16)", cGetPict="repl('!',16)", InputMask=replicate('X',13)

  func oRFCFINS2_2_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SEZ4='1')
    endwith
   endif
  endfunc

  func oRFCFINS2_2_36.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (iif(not empty(.w_RFCFINS2),chkcfp(alltrim(.w_RFCFINS2),'CF'),iif(.w_SEZ4='1',.F.,.T.) ))
    endwith
    return bRes
  endfunc

  add object oStr_2_37 as StdString with uid="XHNHYLLXLG",Visible=.t., Left=438, Top=342,;
    Alignment=1, Width=152, Height=18,;
    Caption="Quadri della dichiarazione:"  ;
  , bGlobalFont=.t.

  add object oStr_2_39 as StdString with uid="FTEQCYTUWC",Visible=.t., Left=22, Top=307,;
    Alignment=0, Width=221, Height=18,;
    Caption="Redazione della dichiarazione"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_41 as StdString with uid="FHMRICXWEG",Visible=.t., Left=30, Top=14,;
    Alignment=0, Width=429, Height=15,;
    Caption="Dati relativi al rappresentante firmatario della dichiarazione"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_42 as StdString with uid="RULZHJZXRW",Visible=.t., Left=33, Top=37,;
    Alignment=0, Width=112, Height=13,;
    Caption="Codice fiscale"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_43 as StdString with uid="USKIUPDQMV",Visible=.t., Left=222, Top=37,;
    Alignment=0, Width=84, Height=13,;
    Caption="Codice carica"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_44 as StdString with uid="GPZTYMBFPO",Visible=.t., Left=152, Top=124,;
    Alignment=0, Width=104, Height=13,;
    Caption="Comune di nascita"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_45 as StdString with uid="RCZFFIGQZJ",Visible=.t., Left=33, Top=80,;
    Alignment=0, Width=76, Height=13,;
    Caption="Cognome"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_46 as StdString with uid="BXGOIXINBX",Visible=.t., Left=253, Top=80,;
    Alignment=0, Width=76, Height=13,;
    Caption="Nome"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_47 as StdString with uid="XMDUKEDFTN",Visible=.t., Left=434, Top=79,;
    Alignment=0, Width=76, Height=13,;
    Caption="Sesso"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_48 as StdString with uid="VNNLUPWWEO",Visible=.t., Left=33, Top=124,;
    Alignment=0, Width=103, Height=13,;
    Caption="Data di nascita"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_49 as StdString with uid="LCBMVTEUCO",Visible=.t., Left=432, Top=124,;
    Alignment=0, Width=48, Height=13,;
    Caption="Provincia"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_50 as StdString with uid="ECENBGSUXY",Visible=.t., Left=33, Top=167,;
    Alignment=0, Width=120, Height=13,;
    Caption="Comune di residenza"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_51 as StdString with uid="WXIGENELJC",Visible=.t., Left=379, Top=167,;
    Alignment=0, Width=48, Height=13,;
    Caption="Provincia"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_52 as StdString with uid="FSUMAEAACK",Visible=.t., Left=432, Top=167,;
    Alignment=0, Width=56, Height=13,;
    Caption="CAP"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_53 as StdString with uid="XDOCDGJPIJ",Visible=.t., Left=33, Top=208,;
    Alignment=0, Width=48, Height=13,;
    Caption="Indirizzo"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_54 as StdString with uid="XJBBNGRGLQ",Visible=.t., Left=379, Top=208,;
    Alignment=0, Width=48, Height=13,;
    Caption="Telefono"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_55 as StdString with uid="TXZKOFBIQL",Visible=.t., Left=438, Top=393,;
    Alignment=1, Width=153, Height=18,;
    Caption="Quadri della dichiarazione:"  ;
  , bGlobalFont=.t.

  add object oStr_2_57 as StdString with uid="OXYILKHLHG",Visible=.t., Left=43, Top=541,;
    Alignment=1, Width=495, Height=18,;
    Caption="Codice fiscale del soggetto che presenta la restante parte della dichiarazione:"  ;
  , bGlobalFont=.t.

  add object oStr_2_59 as StdString with uid="FEVBZISFXP",Visible=.t., Left=438, Top=429,;
    Alignment=1, Width=155, Height=18,;
    Caption="Quadri della dichiarazione:"  ;
  , bGlobalFont=.t.

  add object oStr_2_60 as StdString with uid="TUOIRRRLKC",Visible=.t., Left=111, Top=452,;
    Alignment=0, Width=212, Height=18,;
    Caption="comunicazioni dati lavoro dipendente"  ;
  , bGlobalFont=.t.

  add object oStr_2_61 as StdString with uid="MUSADUKPGF",Visible=.t., Left=438, Top=487,;
    Alignment=1, Width=153, Height=18,;
    Caption="Quadri della dichiarazione:"  ;
  , bGlobalFont=.t.

  add object oStr_2_63 as StdString with uid="DWKAXCLKMG",Visible=.t., Left=112, Top=507,;
    Alignment=0, Width=288, Height=18,;
    Caption="comunicazioni dati certificazioni lavoro autonomo"  ;
  , bGlobalFont=.t.

  add object oStr_2_64 as StdString with uid="CWRNAVQEGI",Visible=.t., Left=514, Top=37,;
    Alignment=0, Width=77, Height=13,;
    Caption="Denominazione"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_65 as StdString with uid="PJKPYIOJZC",Visible=.t., Left=0, Top=583,;
    Alignment=0, Width=1002, Height=18,;
    Caption="Se si modifica la sequenza degli elementi del form o si varia il numero (inserimento o cancellazione item), si deve modificare l'area manuale: notify event init."  ;
  , bGlobalFont=.t.

  func oStr_2_65.mHide()
    with this.Parent.oContained
      return (.T.)
    endwith
  endfunc

  add object oStr_2_66 as StdString with uid="GURHHHOVKL",Visible=.t., Left=35, Top=248,;
    Alignment=0, Width=116, Height=17,;
    Caption="Data decorrenza carica"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_67 as StdString with uid="PMSDRIMIWM",Visible=.t., Left=240, Top=248,;
    Alignment=0, Width=142, Height=17,;
    Caption="Data apertura fallimento"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_2_67.mHide()
    with this.Parent.oContained
      return (! inlist(.w_RFCODCAR,'3','4'))
    endwith
  endfunc

  add object oBox_2_38 as StdBox with uid="FZZKXTTQTF",left=22, top=324, width=722,height=2

  add object oBox_2_40 as StdBox with uid="LEBGSAUSDO",left=26, top=29, width=683,height=2

  add object oBox_2_56 as StdBox with uid="LDVQJNYPWN",left=22, top=357, width=722,height=2

  add object oBox_2_58 as StdBox with uid="YNIXMVKZXY",left=22, top=392, width=722,height=2

  add object oBox_2_62 as StdBox with uid="JYXRYDOTBH",left=22, top=450, width=722,height=2
enddefine
define class tgsri8sftPag3 as StdContainer
  Width  = 747
  height = 562
  stdWidth  = 747
  stdheight = 562
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oFIRMDICH_3_1 as StdCheck with uid="ZFJVFMHAUQ",rtseq=140,rtrep=.f.,left=34, top=23, caption="Firma dichiarante",;
    ToolTipText = "Firma del dichiarante",;
    HelpContextID = 184645534,;
    cFormVar="w_FIRMDICH", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oFIRMDICH_3_1.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oFIRMDICH_3_1.GetRadio()
    this.Parent.oContained.w_FIRMDICH = this.RadioValue()
    return .t.
  endfunc

  func oFIRMDICH_3_1.SetRadio()
    this.Parent.oContained.w_FIRMDICH=trim(this.Parent.oContained.w_FIRMDICH)
    this.value = ;
      iif(this.Parent.oContained.w_FIRMDICH=='1',1,;
      0)
  endfunc

  add object oFIRMPRES_3_2 as StdCheck with uid="XOLWNXUWLK",rtseq=141,rtrep=.f.,left=34, top=45, caption="Firma incaricato del controllo contabile",;
    ToolTipText = "Firma dell'incaricato del controllo contabile",;
    HelpContextID = 79787945,;
    cFormVar="w_FIRMPRES", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oFIRMPRES_3_2.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oFIRMPRES_3_2.GetRadio()
    this.Parent.oContained.w_FIRMPRES = this.RadioValue()
    return .t.
  endfunc

  func oFIRMPRES_3_2.SetRadio()
    this.Parent.oContained.w_FIRMPRES=trim(this.Parent.oContained.w_FIRMPRES)
    this.value = ;
      iif(this.Parent.oContained.w_FIRMPRES=='1',1,;
      0)
  endfunc

  add object oCFINCCON_3_3 as StdField with uid="CABYVRIXJS",rtseq=142,rtrep=.f.,;
    cFormVar = "w_CFINCCON", cQueryName = "CFINCCON",;
    bObbl = .f. , nPag = 3, value=space(16), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale dell'incaricato del controllo contabile",;
    HelpContextID = 185473932,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=34, Top=89, cSayPict='repl("!",16)', cGetPict='repl("!",16)', InputMask=replicate('X',16)

  func oCFINCCON_3_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (iif(not empty(.w_CFINCCON),chkcfp(alltrim(.w_CFINCCON),'CF'),.T.))
    endwith
    return bRes
  endfunc

  add object oINVAVTEL_3_4 as StdCheck with uid="BRAWFSFUUQ",rtseq=143,rtrep=.f.,left=370, top=23, caption="Invio avviso telematico all'intermediario",;
    ToolTipText = "Se attivo, il contribuente invia richiesta all'intermediario di verificare i controlli effettuati sulla dichiarazione",;
    HelpContextID = 118865106,;
    cFormVar="w_INVAVTEL", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oINVAVTEL_3_4.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oINVAVTEL_3_4.GetRadio()
    this.Parent.oContained.w_INVAVTEL = this.RadioValue()
    return .t.
  endfunc

  func oINVAVTEL_3_4.SetRadio()
    this.Parent.oContained.w_INVAVTEL=trim(this.Parent.oContained.w_INVAVTEL)
    this.value = ;
      iif(this.Parent.oContained.w_INVAVTEL=='1',1,;
      0)
  endfunc

  func oINVAVTEL_3_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_IMPTRTEL <> '0')
    endwith
   endif
  endfunc


  add object oCODSOGG_3_5 as StdCombo with uid="OGOPZFOIFE",rtseq=144,rtrep=.f.,left=370,top=89,width=330,height=21;
    , HelpContextID = 162962726;
    , cFormVar="w_CODSOGG",RowSource=""+"1 - Controllo effettuato da un revisore contabile,"+"2 - Controllo effettuato dal presidente della societ� di rev.,"+"3 - Controllo effettuato dal presidente del collegio sindacale,"+"0 - Nessuno", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oCODSOGG_3_5.RadioValue()
    return(iif(this.value =1,'1',;
    iif(this.value =2,'2',;
    iif(this.value =3,'3',;
    iif(this.value =4,'0',;
    space(1))))))
  endfunc
  func oCODSOGG_3_5.GetRadio()
    this.Parent.oContained.w_CODSOGG = this.RadioValue()
    return .t.
  endfunc

  func oCODSOGG_3_5.SetRadio()
    this.Parent.oContained.w_CODSOGG=trim(this.Parent.oContained.w_CODSOGG)
    this.value = ;
      iif(this.Parent.oContained.w_CODSOGG=='1',1,;
      iif(this.Parent.oContained.w_CODSOGG=='2',2,;
      iif(this.Parent.oContained.w_CODSOGG=='3',3,;
      iif(this.Parent.oContained.w_CODSOGG=='0',4,;
      0))))
  endfunc

  func oCODSOGG_3_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (! empty(.w_CFINCCON))
    endwith
   endif
  endfunc

  add object oNUMCAF_3_6 as StdField with uid="KVCYFTJAQL",rtseq=145,rtrep=.f.,;
    cFormVar = "w_NUMCAF", cQueryName = "NUMCAF",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero iscrizione all'albo del C.A.F.",;
    HelpContextID = 137940010,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=34, Top=154, cSayPict='"99999"', cGetPict='"99999"'

  add object oRFDATIMP_3_7 as StdField with uid="UVYTNIYLIS",rtseq=146,rtrep=.f.,;
    cFormVar = "w_RFDATIMP", cQueryName = "RFDATIMP",;
    bObbl = .f. , nPag = 3, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data impegno trasmissione fornitura",;
    HelpContextID = 67857050,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=34, Top=197

  add object oCODFISIN_3_8 as StdField with uid="OIWLMRKWII",rtseq=147,rtrep=.f.,;
    cFormVar = "w_CODFISIN", cQueryName = "CODFISIN",;
    bObbl = .f. , nPag = 3, value=space(16), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale dell'intermediario che effettua la trasmissione",;
    HelpContextID = 179724940,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=34, Top=242, cSayPict="repl('!',16)", cGetPict="repl('!',16)", InputMask=replicate('X',16)

  func oCODFISIN_3_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (iif(not empty(.w_CODFISIN),chkcfp(alltrim(.w_CODFISIN),'CF'),.T.))
    endwith
    return bRes
  endfunc


  add object oIMPTRTEL_3_9 as StdCombo with uid="UWMCFDUCPC",rtseq=148,rtrep=.f.,left=370,top=153,width=327,height=21;
    , ToolTipText = "Impegno trasmissione telematica della dichiarazione";
    , HelpContextID = 115891154;
    , cFormVar="w_IMPTRTEL",RowSource=""+"1 - Dichiarazione predisposta dal contribuente,"+"2 - Dichiarazione predisposta da chi effettua l'invio,"+"0 - Nessuno", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oIMPTRTEL_3_9.RadioValue()
    return(iif(this.value =1,'1',;
    iif(this.value =2,'2',;
    iif(this.value =3,'0',;
    space(1)))))
  endfunc
  func oIMPTRTEL_3_9.GetRadio()
    this.Parent.oContained.w_IMPTRTEL = this.RadioValue()
    return .t.
  endfunc

  func oIMPTRTEL_3_9.SetRadio()
    this.Parent.oContained.w_IMPTRTEL=trim(this.Parent.oContained.w_IMPTRTEL)
    this.value = ;
      iif(this.Parent.oContained.w_IMPTRTEL=='1',1,;
      iif(this.Parent.oContained.w_IMPTRTEL=='2',2,;
      iif(this.Parent.oContained.w_IMPTRTEL=='0',3,;
      0)))
  endfunc

  add object oFIRMINT_3_10 as StdCheck with uid="PJOQBGGUFU",rtseq=149,rtrep=.f.,left=370, top=197, caption="Firma intermediario",;
    ToolTipText = "Firma intermediario",;
    HelpContextID = 5338966,;
    cFormVar="w_FIRMINT", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oFIRMINT_3_10.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oFIRMINT_3_10.GetRadio()
    this.Parent.oContained.w_FIRMINT = this.RadioValue()
    return .t.
  endfunc

  func oFIRMINT_3_10.SetRadio()
    this.Parent.oContained.w_FIRMINT=trim(this.Parent.oContained.w_FIRMINT)
    this.value = ;
      iif(this.Parent.oContained.w_FIRMINT=='1',1,;
      0)
  endfunc

  add object oRICAVTEL_3_11 as StdCheck with uid="GQSRGLMKLR",rtseq=150,rtrep=.f.,left=370, top=224, caption="Ricezione avviso telematico",;
    ToolTipText = "Se attivo, l'intermediario accetta di effettuare verifiche per conto del contribuente sulla sua dichiarazione",;
    HelpContextID = 118786146,;
    cFormVar="w_RICAVTEL", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oRICAVTEL_3_11.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oRICAVTEL_3_11.GetRadio()
    this.Parent.oContained.w_RICAVTEL = this.RadioValue()
    return .t.
  endfunc

  func oRICAVTEL_3_11.SetRadio()
    this.Parent.oContained.w_RICAVTEL=trim(this.Parent.oContained.w_RICAVTEL)
    this.value = ;
      iif(this.Parent.oContained.w_RICAVTEL=='1',1,;
      0)
  endfunc

  func oRICAVTEL_3_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_INVAVTEL='1')
    endwith
   endif
  endfunc

  add object oCFDOMNOT_3_25 as StdField with uid="TVFLDVYQST",rtseq=151,rtrep=.f.,;
    cFormVar = "w_CFDOMNOT", cQueryName = "CFDOMNOT",;
    bObbl = .f. , nPag = 3, value=space(16), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale domicilio notificazione",;
    HelpContextID = 258829190,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=34, Top=307, cSayPict="repl('!',16)", cGetPict="repl('!',16)", InputMask=replicate('X',16)

  func oCFDOMNOT_3_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (iif(not empty(.w_CFDOMNOT),chkcfp(alltrim(.w_CFDOMNOT),'CF'),.T.))
    endwith
    return bRes
  endfunc

  add object oUFDOMNOT_3_27 as StdField with uid="UCCYYODLGV",rtseq=152,rtrep=.f.,;
    cFormVar = "w_UFDOMNOT", cQueryName = "UFDOMNOT",;
    bObbl = .f. , nPag = 3, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Ufficio o ragione sociale",;
    HelpContextID = 258828902,;
   bGlobalFont=.t.,;
    Height=21, Width=332, Left=264, Top=307, cSayPict='repl("!",60)', cGetPict='repl("!",60)', InputMask=replicate('X',60)

  add object oCODOMNOT_3_28 as StdField with uid="SMLCYFWQFT",rtseq=153,rtrep=.f.,;
    cFormVar = "w_CODOMNOT", cQueryName = "CODOMNOT",;
    bObbl = .f. , nPag = 3, value=space(24), bMultilanguage =  .f.,;
    ToolTipText = "Cognome",;
    HelpContextID = 258826886,;
   bGlobalFont=.t.,;
    Height=21, Width=181, Left=36, Top=352, cSayPict='repl("!",24)', cGetPict='repl("!",24)', InputMask=replicate('X',24)

  add object oNODOMNOT_3_31 as StdField with uid="AQAEGIQHVF",rtseq=154,rtrep=.f.,;
    cFormVar = "w_NODOMNOT", cQueryName = "NODOMNOT",;
    bObbl = .f. , nPag = 3, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Nome",;
    HelpContextID = 258826710,;
   bGlobalFont=.t.,;
    Height=21, Width=181, Left=264, Top=352, cSayPict='repl("!",20)', cGetPict='repl("!",20)', InputMask=replicate('X',20)

  add object oCMDOMNOT_3_33 as StdField with uid="WENACEUWEI",rtseq=155,rtrep=.f.,;
    cFormVar = "w_CMDOMNOT", cQueryName = "CMDOMNOT",;
    bObbl = .f. , nPag = 3, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune",;
    HelpContextID = 258827398,;
   bGlobalFont=.t.,;
    Height=21, Width=307, Left=36, Top=394, cSayPict='repl("!",40)', cGetPict='repl("!",40)', InputMask=replicate('X',40)

  func oCMDOMNOT_3_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_SEDOMNOT))
    endwith
   endif
  endfunc

  add object oPRDOMNOT_3_35 as StdField with uid="ZUQBZAKIKH",rtseq=156,rtrep=.f.,;
    cFormVar = "w_PRDOMNOT", cQueryName = "PRDOMNOT",;
    bObbl = .f. , nPag = 3, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Provincia",;
    HelpContextID = 258825910,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=444, Top=394, cSayPict='repl("!",2)', cGetPict='repl("!",2)', InputMask=replicate('X',2)

  func oPRDOMNOT_3_35.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_SEDOMNOT))
    endwith
   endif
  endfunc

  add object oCCDOMNOT_3_37 as StdField with uid="PRVNNYDANU",rtseq=157,rtrep=.f.,;
    cFormVar = "w_CCDOMNOT", cQueryName = "CCDOMNOT",;
    bObbl = .f. , nPag = 3, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Codice comune",;
    HelpContextID = 258829958,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=534, Top=394, cSayPict='repl("!",4)', cGetPict='repl("!",4)', InputMask=replicate('X',4)

  func oCCDOMNOT_3_37.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_SEDOMNOT))
    endwith
   endif
  endfunc

  add object oCPDOMNOT_3_39 as StdField with uid="UCGYWPBBRM",rtseq=158,rtrep=.f.,;
    cFormVar = "w_CPDOMNOT", cQueryName = "CPDOMNOT",;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "C.A.P.",;
    HelpContextID = 258826630,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=635, Top=394, cSayPict='repl("!",5)', cGetPict='repl("!",5)', InputMask=replicate('X',5)

  func oCPDOMNOT_3_39.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_SEDOMNOT))
    endwith
   endif
  endfunc

  add object oVPDOMNOT_3_41 as StdField with uid="HCUPIJJQOR",rtseq=159,rtrep=.f.,;
    cFormVar = "w_VPDOMNOT", cQueryName = "VPDOMNOT",;
    bObbl = .f. , nPag = 3, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Tipologia (Via, piazza, ecc..)",;
    HelpContextID = 258826326,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=36, Top=439, cSayPict='repl("!",15)', cGetPict='repl("!",15)', InputMask=replicate('X',15)

  func oVPDOMNOT_3_41.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_SEDOMNOT))
    endwith
   endif
  endfunc

  add object oINDOMNOT_3_43 as StdField with uid="OJCNUKIHUB",rtseq=160,rtrep=.f.,;
    cFormVar = "w_INDOMNOT", cQueryName = "INDOMNOT",;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo",;
    HelpContextID = 258827046,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=177, Top=439, cSayPict='repl("!",35)', cGetPict='repl("!",35)', InputMask=replicate('X',35)

  func oINDOMNOT_3_43.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_SEDOMNOT))
    endwith
   endif
  endfunc

  add object oNCDOMNOT_3_45 as StdField with uid="UPQZYAJQIJ",rtseq=161,rtrep=.f.,;
    cFormVar = "w_NCDOMNOT", cQueryName = "NCDOMNOT",;
    bObbl = .f. , nPag = 3, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Numero civico",;
    HelpContextID = 258829782,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=444, Top=439, cSayPict="repl('9',10)", cGetPict="repl('9',10)", InputMask=replicate('X',10)

  func oNCDOMNOT_3_45.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_SEDOMNOT))
    endwith
   endif
  endfunc

  add object oFRDOMNOT_3_47 as StdField with uid="YRHNZULXDI",rtseq=162,rtrep=.f.,;
    cFormVar = "w_FRDOMNOT", cQueryName = "FRDOMNOT",;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Frazione",;
    HelpContextID = 258826070,;
   bGlobalFont=.t.,;
    Height=21, Width=177, Left=534, Top=439, cSayPict='repl("!",35)', cGetPict='repl("!",35)', InputMask=replicate('X',35)

  func oFRDOMNOT_3_47.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_SEDOMNOT))
    endwith
   endif
  endfunc

  add object oSEDOMNOT_3_49 as StdField with uid="AIZBPMFKHR",rtseq=163,rtrep=.f.,;
    cFormVar = "w_SEDOMNOT", cQueryName = "SEDOMNOT",;
    bObbl = .f. , nPag = 3, value=space(24), bMultilanguage =  .f.,;
    ToolTipText = "Stato estero",;
    HelpContextID = 258829190,;
   bGlobalFont=.t.,;
    Height=21, Width=181, Left=36, Top=482, cSayPict='repl("!",24)', cGetPict='repl("!",24)', InputMask=replicate('X',24)

  add object oCEDOMNOT_3_51 as StdField with uid="XEVCXPFOUY",rtseq=164,rtrep=.f.,;
    cFormVar = "w_CEDOMNOT", cQueryName = "CEDOMNOT",;
    bObbl = .f. , nPag = 3, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice stato estero",;
    HelpContextID = 258829446,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=224, Top=482, cSayPict='repl("!",3)', cGetPict='repl("!",3)', InputMask=replicate('X',3)

  func oCEDOMNOT_3_51.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (! empty(.w_SEDOMNOT))
    endwith
   endif
  endfunc

  add object oSFDOMNOT_3_53 as StdField with uid="RXTSJGDQKK",rtseq=165,rtrep=.f.,;
    cFormVar = "w_SFDOMNOT", cQueryName = "SFDOMNOT",;
    bObbl = .f. , nPag = 3, value=space(24), bMultilanguage =  .f.,;
    ToolTipText = "Stato federato, provincia, contea",;
    HelpContextID = 258828934,;
   bGlobalFont=.t.,;
    Height=21, Width=195, Left=380, Top=484, cSayPict='repl("!",24)', cGetPict='repl("!",24)', InputMask=replicate('X',24)

  func oSFDOMNOT_3_53.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (! empty(.w_SEDOMNOT))
    endwith
   endif
  endfunc

  add object oLRDOMNOT_3_55 as StdField with uid="YNYYXTWCWO",rtseq=166,rtrep=.f.,;
    cFormVar = "w_LRDOMNOT", cQueryName = "LRDOMNOT",;
    bObbl = .f. , nPag = 3, value=space(24), bMultilanguage =  .f.,;
    ToolTipText = "Localit� di residenza",;
    HelpContextID = 258825974,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=36, Top=523, cSayPict='repl("!",24)', cGetPict='repl("!",24)', InputMask=replicate('X',24)

  func oLRDOMNOT_3_55.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (! empty(.w_SEDOMNOT))
    endwith
   endif
  endfunc

  add object oIEDOMNOT_3_57 as StdField with uid="NYGJYJFLAI",rtseq=167,rtrep=.f.,;
    cFormVar = "w_IEDOMNOT", cQueryName = "IEDOMNOT",;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo estero",;
    HelpContextID = 258829350,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=380, Top=523, cSayPict='repl("!",35)', cGetPict='repl("!",35)', InputMask=replicate('X',35)

  func oIEDOMNOT_3_57.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (! empty(.w_SEDOMNOT))
    endwith
   endif
  endfunc

  add object oStr_3_12 as StdString with uid="LQRAAIJAJN",Visible=.t., Left=34, Top=180,;
    Alignment=0, Width=194, Height=13,;
    Caption="Data dell'impegno a trasmettere"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_13 as StdString with uid="ELNVKYREGV",Visible=.t., Left=26, Top=116,;
    Alignment=0, Width=314, Height=15,;
    Caption="Impegno alla presentazione telematica"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_15 as StdString with uid="EMANNTQNBZ",Visible=.t., Left=34, Top=135,;
    Alignment=0, Width=250, Height=13,;
    Caption="Numero di iscrizione all'albo del C.A.F."  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_16 as StdString with uid="BFGGPKVGZN",Visible=.t., Left=26, Top=5,;
    Alignment=0, Width=150, Height=15,;
    Caption="Firma della dichiarazione"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_18 as StdString with uid="CSGBUBGTCE",Visible=.t., Left=34, Top=69,;
    Alignment=0, Width=306, Height=17,;
    Caption="Codice fiscale dell'incaricato del controllo contabile"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_19 as StdString with uid="AJIXNHKAAD",Visible=.t., Left=34, Top=222,;
    Alignment=0, Width=170, Height=17,;
    Caption="Cod. fis. intermediario"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_20 as StdString with uid="NMLWFHTSQA",Visible=.t., Left=370, Top=135,;
    Alignment=0, Width=297, Height=13,;
    Caption="Impegno a trasmettere in via telematica la dichiarazione"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_21 as StdString with uid="JHWTBRLPBO",Visible=.t., Left=370, Top=69,;
    Alignment=0, Width=266, Height=17,;
    Caption="Soggetto che effettua il controllo contabile"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_22 as StdString with uid="EMBIIFQUGR",Visible=.t., Left=26, Top=267,;
    Alignment=0, Width=330, Height=15,;
    Caption="Domicilio per la notificazione degli atti"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_24 as StdString with uid="PEYDIWZLYW",Visible=.t., Left=34, Top=287,;
    Alignment=0, Width=114, Height=16,;
    Caption="Codice fiscale"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_26 as StdString with uid="YYOXOZASLU",Visible=.t., Left=34, Top=333,;
    Alignment=0, Width=114, Height=17,;
    Caption="Cognome (o ufficio)"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_29 as StdString with uid="SSWUCRFBLX",Visible=.t., Left=264, Top=287,;
    Alignment=0, Width=145, Height=16,;
    Caption="Ufficio o ragione sociale"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_30 as StdString with uid="IPLAPUMPAR",Visible=.t., Left=264, Top=333,;
    Alignment=0, Width=114, Height=17,;
    Caption="Nome"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_32 as StdString with uid="DSEPOLTRHY",Visible=.t., Left=36, Top=376,;
    Alignment=0, Width=114, Height=17,;
    Caption="Comune"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_34 as StdString with uid="SWUYJBGWBS",Visible=.t., Left=444, Top=376,;
    Alignment=0, Width=73, Height=17,;
    Caption="Provincia"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_36 as StdString with uid="NPFUARVPHW",Visible=.t., Left=534, Top=376,;
    Alignment=0, Width=91, Height=17,;
    Caption="Cod. comune"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_38 as StdString with uid="SDCOZRDJUW",Visible=.t., Left=635, Top=376,;
    Alignment=0, Width=77, Height=17,;
    Caption="Cap"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_40 as StdString with uid="EFHPOFEGBQ",Visible=.t., Left=36, Top=419,;
    Alignment=0, Width=114, Height=17,;
    Caption="Via, piazza, ecc."  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_42 as StdString with uid="PCSKEAJMOD",Visible=.t., Left=177, Top=419,;
    Alignment=0, Width=114, Height=17,;
    Caption="Indirizzo"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_44 as StdString with uid="CUPENILMME",Visible=.t., Left=444, Top=419,;
    Alignment=0, Width=74, Height=17,;
    Caption="N� civico"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_46 as StdString with uid="SMFFNOYWBN",Visible=.t., Left=534, Top=419,;
    Alignment=0, Width=113, Height=17,;
    Caption="Frazione"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_48 as StdString with uid="YPXNYDSWBI",Visible=.t., Left=36, Top=463,;
    Alignment=0, Width=114, Height=17,;
    Caption="Stato estero"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_50 as StdString with uid="VNBGVATZQX",Visible=.t., Left=224, Top=463,;
    Alignment=0, Width=141, Height=17,;
    Caption="Cod. stato estero"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_52 as StdString with uid="PKZDMDHUSW",Visible=.t., Left=380, Top=463,;
    Alignment=0, Width=171, Height=17,;
    Caption="Stato federato, provincia, contea"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_54 as StdString with uid="DAVYANSVOQ",Visible=.t., Left=36, Top=507,;
    Alignment=0, Width=120, Height=14,;
    Caption="Localit� di residenza"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_56 as StdString with uid="TIRLPBRTUX",Visible=.t., Left=380, Top=507,;
    Alignment=0, Width=114, Height=15,;
    Caption="Indirizzo estero"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_3_14 as StdBox with uid="DTCNDAKETU",left=26, top=131, width=672,height=2

  add object oBox_3_17 as StdBox with uid="KJABZYZADQ",left=26, top=21, width=672,height=1

  add object oBox_3_23 as StdBox with uid="IKSZIQYOTM",left=26, top=299, width=672,height=2
enddefine
define class tgsri8sftPag4 as StdContainer
  Width  = 747
  height = 562
  stdWidth  = 747
  stdheight = 562
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oTIPFORN_4_1 as StdCombo with uid="TMIIFPZQPN",rtseq=112,rtrep=.f.,left=119,top=14,width=300,height=21;
    , height = 21;
    , ToolTipText = "Tipo fornitore";
    , HelpContextID = 190162890;
    , cFormVar="w_TIPFORN",RowSource=""+"01: Soggetti che inviano le dichiarazioni,"+"06: Amministrazione dello stato,"+"07: Ente poste,"+"08: Banche convenzionate,"+"10: C.A.F. dip. e pens C.A.F. imp art3 C 2 altri int.", bObbl = .f. , nPag = 4;
    , sErrorMsg = "Selezione non consentita con tipo fornitore persona fisica";
  , bGlobalFont=.t.


  func oTIPFORN_4_1.RadioValue()
    return(iif(this.value =1,'01',;
    iif(this.value =2,'06',;
    iif(this.value =3,'07',;
    iif(this.value =4,'08',;
    iif(this.value =5,'10',;
    space(2)))))))
  endfunc
  func oTIPFORN_4_1.GetRadio()
    this.Parent.oContained.w_TIPFORN = this.RadioValue()
    return .t.
  endfunc

  func oTIPFORN_4_1.SetRadio()
    this.Parent.oContained.w_TIPFORN=trim(this.Parent.oContained.w_TIPFORN)
    this.value = ;
      iif(this.Parent.oContained.w_TIPFORN=='01',1,;
      iif(this.Parent.oContained.w_TIPFORN=='06',2,;
      iif(this.Parent.oContained.w_TIPFORN=='07',3,;
      iif(this.Parent.oContained.w_TIPFORN=='08',4,;
      iif(this.Parent.oContained.w_TIPFORN=='10',5,;
      0)))))
  endfunc

  func oTIPFORN_4_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_Perazi='S' and  .w_TIPFORN<>'06' ) or .w_Perazi<>'S')
    endwith
    return bRes
  endfunc

  add object oFODATANASC_4_2 as StdField with uid="DDIBJMNRLH",rtseq=113,rtrep=.f.,;
    cFormVar = "w_FODATANASC", cQueryName = "FODATANASC",;
    bObbl = .f. , nPag = 4, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di nascita del fornitore",;
    HelpContextID = 202054201,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=64, Top=81

  func oFODATANASC_4_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' or .w_TIPFORN$('0305060910'))
    endwith
   endif
  endfunc


  add object oFOSESSO_4_3 as StdCombo with uid="EZVQYTQPNM",value=1,rtseq=114,rtrep=.f.,left=177,top=81,width=96,height=21;
    , height = 21;
    , ToolTipText = "Sesso del fornitore";
    , HelpContextID = 169243306;
    , cFormVar="w_FOSESSO",RowSource=""+"Non selezionato,"+"Maschio,"+"Femmina", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oFOSESSO_4_3.RadioValue()
    return(iif(this.value =1,' ',;
    iif(this.value =2,'M',;
    iif(this.value =3,'F',;
    space(1)))))
  endfunc
  func oFOSESSO_4_3.GetRadio()
    this.Parent.oContained.w_FOSESSO = this.RadioValue()
    return .t.
  endfunc

  func oFOSESSO_4_3.SetRadio()
    this.Parent.oContained.w_FOSESSO=trim(this.Parent.oContained.w_FOSESSO)
    this.value = ;
      iif(this.Parent.oContained.w_FOSESSO=='',1,;
      iif(this.Parent.oContained.w_FOSESSO=='M',2,;
      iif(this.Parent.oContained.w_FOSESSO=='F',3,;
      0)))
  endfunc

  func oFOSESSO_4_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S'  or .w_TIPFORN$('0305060910'))
    endwith
   endif
  endfunc

  add object oFOCOMUNE_4_4 as StdField with uid="QAAPEJDMEJ",rtseq=115,rtrep=.f.,;
    cFormVar = "w_FOCOMUNE", cQueryName = "FOCOMUNE",;
    bObbl = .f. , nPag = 4, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune (o stato estero) di nascita del fornitore",;
    HelpContextID = 141390437,;
   bGlobalFont=.t.,;
    Height=21, Width=320, Left=286, Top=81, cSayPict="repl('!',40)", cGetPict="repl('!',40)", InputMask=replicate('X',40)

  func oFOCOMUNE_4_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' or .w_TIPFORN$('0305060910'))
    endwith
   endif
  endfunc

  add object oFOSIGLA_4_5 as StdField with uid="PWEJHDBSSF",rtseq=116,rtrep=.f.,;
    cFormVar = "w_FOSIGLA", cQueryName = "FOSIGLA",;
    bObbl = .f. , nPag = 4, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia di nascita del fornitore",;
    HelpContextID = 237866326,;
   bGlobalFont=.t.,;
    Height=21, Width=32, Left=614, Top=81, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2)

  func oFOSIGLA_4_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' or .w_TIPFORN$('0305060910'))
    endwith
   endif
  endfunc

  add object oFORECOMUNE_4_6 as StdField with uid="RGOQWOEVVH",rtseq=117,rtrep=.f.,;
    cFormVar = "w_FORECOMUNE", cQueryName = "FORECOMUNE",;
    bObbl = .f. , nPag = 4, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune di residenza anagrafica o di domicilio fiscale del fornitore",;
    HelpContextID = 253114485,;
   bGlobalFont=.t.,;
    Height=21, Width=360, Left=64, Top=124, cSayPict="repl('!',40)", cGetPict="repl('!',40)", InputMask=replicate('X',40)

  func oFORECOMUNE_4_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' or .w_TIPFORN$('0305060910'))
    endwith
   endif
  endfunc

  add object oFORESIGLA_4_7 as StdField with uid="MJCFSUINVZ",rtseq=118,rtrep=.f.,;
    cFormVar = "w_FORESIGLA", cQueryName = "FORESIGLA",;
    bObbl = .f. , nPag = 4, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia di residenza anagrafica o di domicilio fiscale",;
    HelpContextID = 199852466,;
   bGlobalFont=.t.,;
    Height=21, Width=32, Left=441, Top=124, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2)

  func oFORESIGLA_4_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' or .w_TIPFORN$('0305060910'))
    endwith
   endif
  endfunc

  add object oFOCAP_4_8 as StdField with uid="TTYPCQBXFI",rtseq=119,rtrep=.f.,;
    cFormVar = "w_FOCAP", cQueryName = "FOCAP",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "C.A.P. della residenza anagrafica o del domicilio fiscale del fornitore",;
    HelpContextID = 223048362,;
   bGlobalFont=.t.,;
    Height=21, Width=56, Left=505, Top=124, cSayPict="'99999'", cGetPict="'99999'", InputMask=replicate('X',5)

  func oFOCAP_4_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' or .w_TIPFORN$('0305060910'))
    endwith
   endif
  endfunc

  add object oFOINDIRIZ_4_9 as StdField with uid="ZFPYJFYYCZ",rtseq=120,rtrep=.f.,;
    cFormVar = "w_FOINDIRIZ", cQueryName = "FOINDIRIZ",;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo ( frazione, via e numero civico) di residenza anagrafica o domicilio",;
    HelpContextID = 184677183,;
   bGlobalFont=.t.,;
    Height=21, Width=261, Left=64, Top=168, cSayPict="repl('!',35)", cGetPict="repl('!',35)", InputMask=replicate('X',35)

  func oFOINDIRIZ_4_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S' or .w_TIPFORN$('0305060910'))
    endwith
   endif
  endfunc

  add object oFOSECOMUNE_4_11 as StdField with uid="FTDECCKCEA",rtseq=121,rtrep=.f.,;
    cFormVar = "w_FOSECOMUNE", cQueryName = "FOSECOMUNE",;
    bObbl = .f. , nPag = 4, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune della sede legale del fornitore",;
    HelpContextID = 253110389,;
   bGlobalFont=.t.,;
    Height=21, Width=360, Left=64, Top=240, cSayPict="repl('!',40)", cGetPict="repl('!',40)", InputMask=replicate('X',40)

  func oFOSECOMUNE_4_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_TIPFORN$('0305060910'))
    endwith
   endif
  endfunc

  add object oFOSESIGLA_4_12 as StdField with uid="QBWKROZPRH",rtseq=122,rtrep=.f.,;
    cFormVar = "w_FOSESIGLA", cQueryName = "FOSESIGLA",;
    bObbl = .f. , nPag = 4, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia della sede legale del fornitore",;
    HelpContextID = 199856562,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=441, Top=240, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2)

  func oFOSESIGLA_4_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_TIPFORN$('0305060910'))
    endwith
   endif
  endfunc

  add object oFOSECAP_4_13 as StdField with uid="UHFFPYICUM",rtseq=123,rtrep=.f.,;
    cFormVar = "w_FOSECAP", cQueryName = "FOSECAP",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "C.A.P. della sede legale del fornitore",;
    HelpContextID = 219574954,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=505, Top=240, cSayPict="'99999'", cGetPict="'99999'", InputMask=replicate('X',5)

  func oFOSECAP_4_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_TIPFORN$('0305060910'))
    endwith
   endif
  endfunc

  add object oFOSEINDIRI2_4_14 as StdField with uid="UCUBGSOZHZ",rtseq=124,rtrep=.f.,;
    cFormVar = "w_FOSEINDIRI2", cQueryName = "FOSEINDIRI2",;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo della sede legale del fornitore",;
    HelpContextID = 5045183,;
   bGlobalFont=.t.,;
    Height=21, Width=261, Left=64, Top=282, cSayPict="repl('!',35)", cGetPict="repl('!',35)", InputMask=replicate('X',35)

  func oFOSEINDIRI2_4_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_TIPFORN$('0305060910'))
    endwith
   endif
  endfunc

  add object oFOSERCOMUN_4_15 as StdField with uid="VRBXTIUTMI",rtseq=125,rtrep=.f.,;
    cFormVar = "w_FOSERCOMUN", cQueryName = "FOSERCOMUN",;
    bObbl = .f. , nPag = 4, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune di domicilio fiscale del fornitore",;
    HelpContextID = 170270477,;
   bGlobalFont=.t.,;
    Height=21, Width=360, Left=64, Top=323, cSayPict="repl('!',40)", cGetPict="repl('!',40)", InputMask=replicate('X',40)

  func oFOSERCOMUN_4_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_TIPFORN$('0305060910'))
    endwith
   endif
  endfunc

  add object oFOSERSIGLA_4_16 as StdField with uid="RALWYRLTJY",rtseq=126,rtrep=.f.,;
    cFormVar = "w_FOSERSIGLA", cQueryName = "FOSERSIGLA",;
    bObbl = .f. , nPag = 4, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia di domicilio fiscale del fornitore",;
    HelpContextID = 170273955,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=442, Top=323, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2)

  func oFOSERSIGLA_4_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_TIPFORN$('0305060910'))
    endwith
   endif
  endfunc

  add object oFOSERCAP_4_17 as StdField with uid="UZATTOELQF",rtseq=127,rtrep=.f.,;
    cFormVar = "w_FOSERCAP", cQueryName = "FOSERCAP",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "C.A.P. del domicilio fiscale del fornitore",;
    HelpContextID = 98143654,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=505, Top=323, cSayPict="'99999'", cGetPict="'99999'", InputMask=replicate('X',5)

  func oFOSERCAP_4_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_TIPFORN$('0305060910'))
    endwith
   endif
  endfunc

  add object oFOSERINDIR_4_18 as StdField with uid="OCERQWQXAX",rtseq=128,rtrep=.f.,;
    cFormVar = "w_FOSERINDIR", cQueryName = "FOSERINDIR",;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo ( frazione, via e numero civico) di domicilio fiscale del fornitore",;
    HelpContextID = 69606358,;
   bGlobalFont=.t.,;
    Height=21, Width=261, Left=64, Top=364, cSayPict="repl('!',35)", cGetPict="repl('!',35)", InputMask=replicate('X',35)

  func oFOSERINDIR_4_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S' or .w_TIPFORN$('0305060910'))
    endwith
   endif
  endfunc


  add object oSELCAF_4_19 as StdCombo with uid="LYTTOZMRSR",rtseq=129,rtrep=.f.,left=31,top=434,width=118,height=21;
    , ToolTipText = "Selezione CAF o Professionista";
    , HelpContextID = 137948122;
    , cFormVar="w_SELCAF",RowSource=""+"CAF,"+"Professionista", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oSELCAF_4_19.RadioValue()
    return(iif(this.value =1,'1',;
    iif(this.value =2,'2',;
    space(10))))
  endfunc
  func oSELCAF_4_19.GetRadio()
    this.Parent.oContained.w_SELCAF = this.RadioValue()
    return .t.
  endfunc

  func oSELCAF_4_19.SetRadio()
    this.Parent.oContained.w_SELCAF=trim(this.Parent.oContained.w_SELCAF)
    this.value = ;
      iif(this.Parent.oContained.w_SELCAF=='1',1,;
      iif(this.Parent.oContained.w_SELCAF=='2',2,;
      0))
  endfunc

  add object oCFRESCAF_4_20 as StdField with uid="TRSLJXUKSK",rtseq=130,rtrep=.f.,;
    cFormVar = "w_CFRESCAF", cQueryName = "CFRESCAF",;
    bObbl = .f. , nPag = 4, value=space(16), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale",;
    HelpContextID = 99185772,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=234, Top=434, cSayPict="repl('!',16)", cGetPict="repl('!',16)", InputMask=replicate('X',16)

  func oCFRESCAF_4_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (iif(not empty(.w_CFRESCAF),chkcfp(alltrim(.w_CFRESCAF),'CF'),.T.))
    endwith
    return bRes
  endfunc

  add object oCFDELCAF_4_21 as StdField with uid="RRZPSOFPPW",rtseq=131,rtrep=.f.,;
    cFormVar = "w_CFDELCAF", cQueryName = "CFDELCAF",;
    bObbl = .f. , nPag = 4, value=space(11), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale",;
    HelpContextID = 91788396,;
   bGlobalFont=.t.,;
    Height=21, Width=104, Left=31, Top=480, cSayPict="repl('!',11)", cGetPict="repl('!',11)", InputMask=replicate('X',11)

  func oCFDELCAF_4_21.mHide()
    with this.Parent.oContained
      return (.w_SELCAF <> '1')
    endwith
  endfunc

  func oCFDELCAF_4_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (iif(not empty(.w_CFDELCAF),chkcfp(alltrim(.w_CFDELCAF),'PI'),.T.))
    endwith
    return bRes
  endfunc

  add object oFLCONFCF_4_22 as StdCheck with uid="HCOGZMJVPF",rtseq=132,rtrep=.f.,left=234, top=466, caption="Codice fiscale non registrato in anagrafe",;
    ToolTipText = "Se attivo � possibile procedere alla trasmissione telematica della dichiarazione",;
    HelpContextID = 144870044,;
    cFormVar="w_FLCONFCF", bObbl = .f. , nPag = 4;
    , tabstop =.f.;
   , bGlobalFont=.t.


  func oFLCONFCF_4_22.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oFLCONFCF_4_22.GetRadio()
    this.Parent.oContained.w_FLCONFCF = this.RadioValue()
    return .t.
  endfunc

  func oFLCONFCF_4_22.SetRadio()
    this.Parent.oContained.w_FLCONFCF=trim(this.Parent.oContained.w_FLCONFCF)
    this.value = ;
      iif(this.Parent.oContained.w_FLCONFCF=='1',1,;
      0)
  endfunc

  add object oFLAGFIR_4_23 as StdCheck with uid="GMLRFHNQTY",rtseq=133,rtrep=.f.,left=234, top=485, caption="Flag firma del riquadro visto di conformit�",;
    ToolTipText = "Flag firma riquadro 'visto conformit�'",;
    HelpContextID = 186280534,;
    cFormVar="w_FLAGFIR", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oFLAGFIR_4_23.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oFLAGFIR_4_23.GetRadio()
    this.Parent.oContained.w_FLAGFIR = this.RadioValue()
    return .t.
  endfunc

  func oFLAGFIR_4_23.SetRadio()
    this.Parent.oContained.w_FLAGFIR=trim(this.Parent.oContained.w_FLAGFIR)
    this.value = ;
      iif(this.Parent.oContained.w_FLAGFIR=='1',1,;
      0)
  endfunc

  add object oStr_4_25 as StdString with uid="DCPHCCFCXL",Visible=.t., Left=25, Top=44,;
    Alignment=0, Width=204, Height=15,;
    Caption="Persona fisica"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_26 as StdString with uid="YGYYBXXOBR",Visible=.t., Left=25, Top=199,;
    Alignment=0, Width=228, Height=15,;
    Caption="Altri soggetti"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_27 as StdString with uid="CCNGTZXXUE",Visible=.t., Left=64, Top=107,;
    Alignment=0, Width=306, Height=13,;
    Caption="Comune di residenza o domicilio fiscale"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_28 as StdString with uid="AKNIDKFOUL",Visible=.t., Left=64, Top=64,;
    Alignment=0, Width=103, Height=13,;
    Caption="Data di nascita"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_29 as StdString with uid="XXYSRZPODK",Visible=.t., Left=286, Top=64,;
    Alignment=0, Width=223, Height=13,;
    Caption="Comune di nascita"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_30 as StdString with uid="GTEVNWAGWC",Visible=.t., Left=614, Top=64,;
    Alignment=0, Width=48, Height=13,;
    Caption="Provincia"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_31 as StdString with uid="AVMJOZOAMZ",Visible=.t., Left=64, Top=222,;
    Alignment=0, Width=223, Height=13,;
    Caption="Comune della sede legale"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_32 as StdString with uid="ARKFUFVBKV",Visible=.t., Left=441, Top=107,;
    Alignment=0, Width=48, Height=13,;
    Caption="Provincia"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_33 as StdString with uid="OWDEPTUCER",Visible=.t., Left=64, Top=150,;
    Alignment=0, Width=250, Height=13,;
    Caption="Indirizzo, frazione, via e numero civico"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_34 as StdString with uid="ASIOLJGQNU",Visible=.t., Left=177, Top=64,;
    Alignment=0, Width=48, Height=13,;
    Caption="Sesso"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_35 as StdString with uid="RFSIWFUVPA",Visible=.t., Left=505, Top=107,;
    Alignment=0, Width=56, Height=13,;
    Caption="CAP"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_36 as StdString with uid="MDEDFCKUHY",Visible=.t., Left=16, Top=14,;
    Alignment=1, Width=98, Height=15,;
    Caption="Tipo fornitore:"  ;
  , bGlobalFont=.t.

  add object oStr_4_37 as StdString with uid="HNBYHSXXZT",Visible=.t., Left=441, Top=222,;
    Alignment=0, Width=48, Height=13,;
    Caption="Provincia"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_38 as StdString with uid="GQYCDLXSEK",Visible=.t., Left=64, Top=347,;
    Alignment=0, Width=149, Height=13,;
    Caption="Indirizzo di domicilio fiscale"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_39 as StdString with uid="XRGWLJIBWO",Visible=.t., Left=441, Top=308,;
    Alignment=0, Width=48, Height=13,;
    Caption="Provincia"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_40 as StdString with uid="BCAUNHALPM",Visible=.t., Left=505, Top=222,;
    Alignment=0, Width=56, Height=13,;
    Caption="CAP"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_41 as StdString with uid="OXXFNCZRJN",Visible=.t., Left=505, Top=308,;
    Alignment=0, Width=56, Height=13,;
    Caption="CAP"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_42 as StdString with uid="ZFKUQNCSTW",Visible=.t., Left=64, Top=266,;
    Alignment=0, Width=223, Height=13,;
    Caption="Indirizzo della sede legale"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_43 as StdString with uid="TNQFMKZSKA",Visible=.t., Left=64, Top=308,;
    Alignment=0, Width=223, Height=13,;
    Caption="Comune di domicilio fiscale"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_44 as StdString with uid="CSLGHNVXQQ",Visible=.t., Left=25, Top=392,;
    Alignment=0, Width=228, Height=15,;
    Caption="Visto di conformit�"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_46 as StdString with uid="PFUNCMOJXV",Visible=.t., Left=234, Top=416,;
    Alignment=0, Width=368, Height=13,;
    Caption="Codice fiscale del responsabile del C.A.F. o professionista"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_47 as StdString with uid="XSXIBZURHT",Visible=.t., Left=31, Top=463,;
    Alignment=0, Width=192, Height=13,;
    Caption="Codice fiscale del C.A.F."  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_4_47.mHide()
    with this.Parent.oContained
      return (.w_SELCAF <> '1')
    endwith
  endfunc

  add object oStr_4_48 as StdString with uid="NDTJKEXSFR",Visible=.t., Left=31, Top=416,;
    Alignment=0, Width=168, Height=13,;
    Caption="Tipologia"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_49 as StdString with uid="AHIYNFTIDM",Visible=.t., Left=31, Top=521,;
    Alignment=0, Width=451, Height=18,;
    Caption="Si rilascia il visto di conformit� ai sensi art. 35 del D.lgs.  n. 241/1997"  ;
  , bGlobalFont=.t.

  add object oBox_4_10 as StdBox with uid="BTOQXYGKKW",left=21, top=58, width=672,height=2

  add object oBox_4_24 as StdBox with uid="QVBCKAWCXU",left=21, top=215, width=672,height=2

  add object oBox_4_45 as StdBox with uid="FWAQWRIPNX",left=21, top=407, width=672,height=2
enddefine
define class tgsri8sftPag5 as StdContainer
  Width  = 747
  height = 562
  stdWidth  = 747
  stdheight = 562
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDATINI_5_1 as StdField with uid="JPXZKEKVIP",rtseq=106,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .t. , nPag = 5, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � maggiore della data finale o fuori periodo",;
    ToolTipText = "Data iniziale di stampa",;
    HelpContextID = 73560266,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=142, Top=39

  func oDATINI_5_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_DATFIN>=.w_DATINI) AND (.w_DATINI > CTOD("31-12-06")))
    endwith
    return bRes
  endfunc

  add object oDATFIN_5_2 as StdField with uid="FNNEPIJJDP",rtseq=107,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .t. , nPag = 5, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � maggiore della data finale o fuori periodo",;
    ToolTipText = "Data finale di stampa",;
    HelpContextID = 4886326,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=410, Top=39

  func oDATFIN_5_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_DATFIN>=.w_DATINI) AND (.w_DATFIN < CTOD("01-01-08")))
    endwith
    return bRes
  endfunc

  add object oDirName_5_3 as StdField with uid="EJPBDNOIEF",rtseq=108,rtrep=.f.,;
    cFormVar = "w_DirName", cQueryName = "DirName",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(200), bMultilanguage =  .f.,;
    HelpContextID = 13932342,;
   bGlobalFont=.t.,;
    Height=21, Width=446, Left=123, Top=149, InputMask=replicate('X',200)


  add object oBtn_5_4 as StdButton with uid="QIOPRPPDCF",left=571, top=149, width=18,height=21,;
    caption="...", nPag=5;
    , HelpContextID = 42853674;
    , tabstop = .f.;
  , bGlobalFont=.t.

    proc oBtn_5_4.Click()
      with this.Parent.oContained
        do SfogliaDir with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oFileName_5_5 as StdField with uid="OOIIRZMLFQ",rtseq=109,rtrep=.f.,;
    cFormVar = "w_FileName", cQueryName = "FileName",;
    bObbl = .f. , nPag = 5, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 205834309,;
   bGlobalFont=.t.,;
    Height=21, Width=212, Left=123, Top=175, InputMask=replicate('X',30)

  add object oIDESTA_5_6 as StdField with uid="QWRFCHFTQX",rtseq=110,rtrep=.f.,;
    cFormVar = "w_IDESTA", cQueryName = "IDESTA",;
    bObbl = .t. , nPag = 5, value=space(17), bMultilanguage =  .f.,;
    HelpContextID = 200891770,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=410, Top=292, InputMask=replicate('X',17), inputmask='99999999999999999'

  func oIDESTA_5_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLAGCOR = '1' or .w_FLAGINT = '1')
    endwith
   endif
  endfunc

  add object oPROIFT_5_7 as StdField with uid="COXZWAWVRC",rtseq=111,rtrep=.f.,;
    cFormVar = "w_PROIFT", cQueryName = "PROIFT",;
    bObbl = .t. , nPag = 5, value=space(6), bMultilanguage =  .f.,;
    HelpContextID = 102584566,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=410, Top=319, InputMask=replicate('X',6), inputmask='999999'

  func oPROIFT_5_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLAGCOR = '1' or .w_FLAGINT = '1')
    endwith
   endif
  endfunc


  add object oBtn_5_20 as StdButton with uid="UJKRZMFCSW",left=537, top=498, width=48,height=45,;
    CpPicture="bmp\STAMPA.BMP", caption="", nPag=5;
    , ToolTipText = "Premere per eseguire la stampa";
    , HelpContextID = 47891222;
    , caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_5_20.Click()
      with this.Parent.oContained
        do gsri5bsp with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_5_21 as StdButton with uid="QFDCFAAMDM",left=593, top=498, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=5;
    , ToolTipText = "Premere per generare file telematico";
    , HelpContextID = 47891222;
    , tabstop = .f., caption='\<Genera';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_5_21.Click()
      with this.Parent.oContained
        do GSUT6BFT with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_5_21.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return ( isInoltel())
      endwith
    endif
  endfunc


  add object oBtn_5_22 as StdButton with uid="JRQIJZOZZF",left=649, top=498, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=5;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 47891222;
    , tabstop = .f., caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_5_22.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_5_8 as StdString with uid="LCHEPLEODA",Visible=.t., Left=69, Top=39,;
    Alignment=1, Width=70, Height=15,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_5_9 as StdString with uid="GUHHFLLZER",Visible=.t., Left=343, Top=39,;
    Alignment=1, Width=63, Height=15,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_5_10 as StdString with uid="QDPZXPECZB",Visible=.t., Left=37, Top=149,;
    Alignment=1, Width=82, Height=15,;
    Caption="Directory:"  ;
  , bGlobalFont=.t.

  add object oStr_5_11 as StdString with uid="FKJYZDRMHF",Visible=.t., Left=37, Top=175,;
    Alignment=1, Width=82, Height=15,;
    Caption="Nome file:"  ;
  , bGlobalFont=.t.

  add object oStr_5_12 as StdString with uid="TKFFKWNXKB",Visible=.t., Left=26, Top=17,;
    Alignment=0, Width=163, Height=15,;
    Caption="Parametri di selezione"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_5_15 as StdString with uid="KLZWVWGXPN",Visible=.t., Left=26, Top=116,;
    Alignment=0, Width=204, Height=15,;
    Caption="File telematico"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_5_17 as StdString with uid="XKVUVPRKKQ",Visible=.t., Left=26, Top=260,;
    Alignment=0, Width=411, Height=15,;
    Caption="Protocollo telematico della dichiarazione da sostituire"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_5_18 as StdString with uid="ETMYQQHNMJ",Visible=.t., Left=91, Top=292,;
    Alignment=1, Width=315, Height=15,;
    Caption="Identificativo assegnato dal servizio telematico all'invio:"  ;
  , bGlobalFont=.t.

  add object oStr_5_19 as StdString with uid="ZMKICUMICZ",Visible=.t., Left=70, Top=319,;
    Alignment=1, Width=336, Height=15,;
    Caption="Progressivo dichiarazione all'interno del file inviato:"  ;
  , bGlobalFont=.t.

  add object oBox_5_13 as StdBox with uid="EWLUHYXJCA",left=22, top=32, width=676,height=2

  add object oBox_5_14 as StdBox with uid="ALLGPWEGAO",left=22, top=131, width=676,height=2

  add object oBox_5_16 as StdBox with uid="FUJGWUZXRO",left=22, top=275, width=676,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsri8sft','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsri8sft
proc SfogliaDir (parent)
local PathName, oField
  PathName = Cp_GetDir()
  if !empty(PathName)
    parent.w_DirName = PathName
  endif
endproc

func IsPerFis(Cognome,Nome,Denominazione)
do case
case empty(Cognome) and empty(Nome) and empty(Denominazione)
  return 'N'
case !empty(Cognome) or !empty(Nome)
  return 'S'
case !empty(Denominazione)
  return 'N'
endcase
* --- Fine Area Manuale
