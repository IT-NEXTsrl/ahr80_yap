* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_ked                                                        *
*              Estrazione dati                                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2015-05-11                                                      *
* Last revis.: 2017-02-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- gsri_ked
* ---- Parametro Gestione
 * ---- S: 770
 * ---- C: Certificazione unica
ptipEst=oparentobject
* --- Fine Area Manuale
return(createobject("tgsri_ked",oParentObject))

* --- Class definition
define class tgsri_ked as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 575
  Height = 559
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2017-02-07"
  HelpContextID=65664617
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=26

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsri_ked"
  cComment = "Estrazione dati"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_TIPEST = space(1)
  w_RECORDH = space(1)
  o_RECORDH = space(1)
  w_RECORDCU = space(1)
  w_TIPOPERAZ = space(1)
  w_TIPOPERAZ = space(1)
  w_ELIRECORDH = space(1)
  o_ELIRECORDH = space(1)
  w_ELIRECORDCUD = space(1)
  w_ELIRECORDHM = space(1)
  w_RECORDST = space(1)
  o_RECORDST = space(1)
  w_ELIRECORDST = space(1)
  w_RECORDST = space(1)
  w_ELIRECORDST = space(1)
  w_ESTR_RES = space(1)
  w_ESTRNRES = space(1)
  w_ESTRSRES = space(1)
  w_ANNO = 0
  o_ANNO = 0
  w_DATINI = ctod('  /  /  ')
  o_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  w_DATAINIANNO = ctod('  /  /  ')
  w_DATAFINEANNO = ctod('  /  /  ')
  w_PERCIN = space(15)
  w_PERCFIN = space(15)
  w_OBTEST = ctod('  /  /  ')
  w_Msg = space(0)
  w_CURAPFAL = ctod('  /  /  ')
  w_MESS_ERR = space(10)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsri_kedPag1","gsri_ked",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oRECORDH_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- gsri_ked
    * --- Imposta il Titolo della Finestra del Documento in Base al modello
    WITH THIS.PARENT
              pTipEst=oparentobject
              DO CASE
                 CASE pTipEst == 'C'
                       .cComment =CP_TRANSLATE('Estrazione dati certificazione unica')
                 CASE pTipEst == 'S'
                       .cComment = CP_TRANSLATE('Estrazione dati')
             ENDCASE
    ENDWITH
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIPEST=space(1)
      .w_RECORDH=space(1)
      .w_RECORDCU=space(1)
      .w_TIPOPERAZ=space(1)
      .w_TIPOPERAZ=space(1)
      .w_ELIRECORDH=space(1)
      .w_ELIRECORDCUD=space(1)
      .w_ELIRECORDHM=space(1)
      .w_RECORDST=space(1)
      .w_ELIRECORDST=space(1)
      .w_RECORDST=space(1)
      .w_ELIRECORDST=space(1)
      .w_ESTR_RES=space(1)
      .w_ESTRNRES=space(1)
      .w_ESTRSRES=space(1)
      .w_ANNO=0
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_DATAINIANNO=ctod("  /  /  ")
      .w_DATAFINEANNO=ctod("  /  /  ")
      .w_PERCIN=space(15)
      .w_PERCFIN=space(15)
      .w_OBTEST=ctod("  /  /  ")
      .w_Msg=space(0)
      .w_CURAPFAL=ctod("  /  /  ")
      .w_MESS_ERR=space(10)
        .w_TIPEST = pTipEst
        .w_RECORDH = iif(.w_TipEst='S' And .w_Anno=2014,'S',' ')
        .w_RECORDCU = iif(.w_TipEst='C','S',' ')
        .w_TIPOPERAZ = 'O'
        .w_TIPOPERAZ = 'O'
        .w_ELIRECORDH = iif(.w_RecordH='S','S',' ')
        .w_ELIRECORDCUD = ' '
        .w_ELIRECORDHM = ' '
        .w_RECORDST = IIF(.w_TipEst='S','S','N')
        .w_ELIRECORDST = ' '
        .w_RECORDST = IIF(.w_TipEst='S','S','N')
        .w_ELIRECORDST = ' '
        .w_ESTR_RES = 'S'
        .w_ESTRNRES = 'S'
        .w_ESTRSRES = 'S'
        .w_ANNO = iif(.w_TipEst='S',year(i_DATSYS)-1,iif(year(i_DATSYS)-1<2016,2016,year(i_DATSYS)-1))
        .w_DATINI = CTOD("01-01-"+alltrim(str(.w_ANNO)))
        .w_DATFIN = CTOD("31-12-"+alltrim(str(.w_ANNO)))
        .w_DATAINIANNO = CTOD("01-01-"+alltrim(Str(.w_ANNO)))
        .w_DATAFINEANNO = CTOD("31-12-"+alltrim(str(.w_ANNO)))
        .w_PERCIN = Space(15)
        .w_PERCFIN = Space(15)
        .w_OBTEST = .w_DATINI
    endwith
    this.DoRTCalc(24,26,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_27.enabled = this.oPgFrm.Page1.oPag.oBtn_1_27.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_28.enabled = this.oPgFrm.Page1.oPag.oBtn_1_28.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
            .w_TIPEST = pTipEst
        if .o_Anno<>.w_Anno
            .w_RECORDH = iif(.w_TipEst='S' And .w_Anno=2014,'S',' ')
        endif
        .DoRTCalc(3,3,.t.)
        if .o_ANNO<>.w_ANNO
            .w_TIPOPERAZ = 'O'
        endif
        .DoRTCalc(5,5,.t.)
        if .o_RecordH<>.w_RecordH
            .w_ELIRECORDH = iif(.w_RecordH='S','S',' ')
        endif
        .DoRTCalc(7,7,.t.)
        if .o_ElirecordH<>.w_ElirecordH.or. .o_RecordH<>.w_RecordH
            .w_ELIRECORDHM = ' '
        endif
        .DoRTCalc(9,9,.t.)
        if .o_RecordST<>.w_RecordST
            .w_ELIRECORDST = ' '
        endif
        .DoRTCalc(11,11,.t.)
        if .o_RecordST<>.w_RecordST
            .w_ELIRECORDST = ' '
        endif
        .DoRTCalc(13,16,.t.)
        if .o_ANNO<>.w_ANNO
            .w_DATINI = CTOD("01-01-"+alltrim(str(.w_ANNO)))
        endif
        if .o_ANNO<>.w_ANNO
            .w_DATFIN = CTOD("31-12-"+alltrim(str(.w_ANNO)))
        endif
        if .o_ANNO<>.w_ANNO
            .w_DATAINIANNO = CTOD("01-01-"+alltrim(Str(.w_ANNO)))
        endif
        if .o_ANNO<>.w_ANNO
            .w_DATAFINEANNO = CTOD("31-12-"+alltrim(str(.w_ANNO)))
        endif
        .DoRTCalc(21,22,.t.)
        if .o_DATINI<>.w_DATINI
            .w_OBTEST = .w_DATINI
        endif
        if .o_ELIRECORDH<>.w_ELIRECORDH
          .Calculate_OAMNFRKMFX()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(24,26,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_OAMNFRKMFX()
    with this
          * --- Controllo check Elimina dati record H
          Gsri_bed(this;
              ,'C';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oELIRECORDH_1_7.enabled = this.oPgFrm.Page1.oPag.oELIRECORDH_1_7.mCond()
    this.oPgFrm.Page1.oPag.oELIRECORDHM_1_9.enabled = this.oPgFrm.Page1.oPag.oELIRECORDHM_1_9.mCond()
    this.oPgFrm.Page1.oPag.oELIRECORDST_1_11.enabled = this.oPgFrm.Page1.oPag.oELIRECORDST_1_11.mCond()
    this.oPgFrm.Page1.oPag.oELIRECORDST_1_13.enabled = this.oPgFrm.Page1.oPag.oELIRECORDST_1_13.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_27.enabled = this.oPgFrm.Page1.oPag.oBtn_1_27.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oRECORDH_1_3.visible=!this.oPgFrm.Page1.oPag.oRECORDH_1_3.mHide()
    this.oPgFrm.Page1.oPag.oRECORDCU_1_4.visible=!this.oPgFrm.Page1.oPag.oRECORDCU_1_4.mHide()
    this.oPgFrm.Page1.oPag.oTIPOPERAZ_1_5.visible=!this.oPgFrm.Page1.oPag.oTIPOPERAZ_1_5.mHide()
    this.oPgFrm.Page1.oPag.oTIPOPERAZ_1_6.visible=!this.oPgFrm.Page1.oPag.oTIPOPERAZ_1_6.mHide()
    this.oPgFrm.Page1.oPag.oELIRECORDH_1_7.visible=!this.oPgFrm.Page1.oPag.oELIRECORDH_1_7.mHide()
    this.oPgFrm.Page1.oPag.oELIRECORDCUD_1_8.visible=!this.oPgFrm.Page1.oPag.oELIRECORDCUD_1_8.mHide()
    this.oPgFrm.Page1.oPag.oELIRECORDHM_1_9.visible=!this.oPgFrm.Page1.oPag.oELIRECORDHM_1_9.mHide()
    this.oPgFrm.Page1.oPag.oRECORDST_1_10.visible=!this.oPgFrm.Page1.oPag.oRECORDST_1_10.mHide()
    this.oPgFrm.Page1.oPag.oELIRECORDST_1_11.visible=!this.oPgFrm.Page1.oPag.oELIRECORDST_1_11.mHide()
    this.oPgFrm.Page1.oPag.oRECORDST_1_12.visible=!this.oPgFrm.Page1.oPag.oRECORDST_1_12.mHide()
    this.oPgFrm.Page1.oPag.oELIRECORDST_1_13.visible=!this.oPgFrm.Page1.oPag.oELIRECORDST_1_13.mHide()
    this.oPgFrm.Page1.oPag.oESTR_RES_1_14.visible=!this.oPgFrm.Page1.oPag.oESTR_RES_1_14.mHide()
    this.oPgFrm.Page1.oPag.oESTRNRES_1_15.visible=!this.oPgFrm.Page1.oPag.oESTRNRES_1_15.mHide()
    this.oPgFrm.Page1.oPag.oESTRSRES_1_16.visible=!this.oPgFrm.Page1.oPag.oESTRSRES_1_16.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_33.visible=!this.oPgFrm.Page1.oPag.oStr_1_33.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_35.visible=!this.oPgFrm.Page1.oPag.oStr_1_35.mHide()
    this.oPgFrm.Page1.oPag.oCURAPFAL_1_36.visible=!this.oPgFrm.Page1.oPag.oCURAPFAL_1_36.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oRECORDH_1_3.RadioValue()==this.w_RECORDH)
      this.oPgFrm.Page1.oPag.oRECORDH_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oRECORDCU_1_4.RadioValue()==this.w_RECORDCU)
      this.oPgFrm.Page1.oPag.oRECORDCU_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOPERAZ_1_5.RadioValue()==this.w_TIPOPERAZ)
      this.oPgFrm.Page1.oPag.oTIPOPERAZ_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOPERAZ_1_6.RadioValue()==this.w_TIPOPERAZ)
      this.oPgFrm.Page1.oPag.oTIPOPERAZ_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oELIRECORDH_1_7.RadioValue()==this.w_ELIRECORDH)
      this.oPgFrm.Page1.oPag.oELIRECORDH_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oELIRECORDCUD_1_8.RadioValue()==this.w_ELIRECORDCUD)
      this.oPgFrm.Page1.oPag.oELIRECORDCUD_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oELIRECORDHM_1_9.RadioValue()==this.w_ELIRECORDHM)
      this.oPgFrm.Page1.oPag.oELIRECORDHM_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oRECORDST_1_10.RadioValue()==this.w_RECORDST)
      this.oPgFrm.Page1.oPag.oRECORDST_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oELIRECORDST_1_11.RadioValue()==this.w_ELIRECORDST)
      this.oPgFrm.Page1.oPag.oELIRECORDST_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oRECORDST_1_12.RadioValue()==this.w_RECORDST)
      this.oPgFrm.Page1.oPag.oRECORDST_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oELIRECORDST_1_13.RadioValue()==this.w_ELIRECORDST)
      this.oPgFrm.Page1.oPag.oELIRECORDST_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oESTR_RES_1_14.RadioValue()==this.w_ESTR_RES)
      this.oPgFrm.Page1.oPag.oESTR_RES_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oESTRNRES_1_15.RadioValue()==this.w_ESTRNRES)
      this.oPgFrm.Page1.oPag.oESTRNRES_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oESTRSRES_1_16.RadioValue()==this.w_ESTRSRES)
      this.oPgFrm.Page1.oPag.oESTRSRES_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oANNO_1_19.value==this.w_ANNO)
      this.oPgFrm.Page1.oPag.oANNO_1_19.value=this.w_ANNO
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_21.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_21.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_22.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_22.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oMsg_1_32.value==this.w_Msg)
      this.oPgFrm.Page1.oPag.oMsg_1_32.value=this.w_Msg
    endif
    if not(this.oPgFrm.Page1.oPag.oCURAPFAL_1_36.value==this.w_CURAPFAL)
      this.oPgFrm.Page1.oPag.oCURAPFAL_1_36.value=this.w_CURAPFAL
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_ANNO)) or not((.w_TipEst='S' And .w_ANNO>=2014) Or .w_TipEst='C'))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oANNO_1_19.SetFocus()
            i_bnoObbl = !empty(.w_ANNO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_DATINI)) or not((.w_DATFIN>=.w_DATINI) AND (.w_DATINI>=.w_DATAINIANNO AND .w_DATINI<=.w_DATAFINEANNO)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATINI_1_21.SetFocus()
            i_bnoObbl = !empty(.w_DATINI)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � maggiore della data finale o fuori periodo")
          case   ((empty(.w_DATFIN)) or not((.w_DATFIN>=.w_DATINI) AND (.w_DATFIN>=.w_DATAINIANNO AND .w_DATFIN<=.w_DATAFINEANNO)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFIN_1_22.SetFocus()
            i_bnoObbl = !empty(.w_DATFIN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � maggiore della data finale o fuori periodo")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_RECORDH = this.w_RECORDH
    this.o_ELIRECORDH = this.w_ELIRECORDH
    this.o_RECORDST = this.w_RECORDST
    this.o_ANNO = this.w_ANNO
    this.o_DATINI = this.w_DATINI
    return

enddefine

* --- Define pages as container
define class tgsri_kedPag1 as StdContainer
  Width  = 571
  height = 559
  stdWidth  = 571
  stdheight = 559
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oRECORDH_1_3 as StdCheck with uid="YIMQMHGJER",rtseq=2,rtrep=.f.,left=136, top=20, caption="Record H",;
    ToolTipText = "Se attivo, estrae tutti i record H",;
    HelpContextID = 92898326,;
    cFormVar="w_RECORDH", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oRECORDH_1_3.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oRECORDH_1_3.GetRadio()
    this.Parent.oContained.w_RECORDH = this.RadioValue()
    return .t.
  endfunc

  func oRECORDH_1_3.SetRadio()
    this.Parent.oContained.w_RECORDH=trim(this.Parent.oContained.w_RECORDH)
    this.value = ;
      iif(this.Parent.oContained.w_RECORDH=='S',1,;
      0)
  endfunc

  func oRECORDH_1_3.mHide()
    with this.Parent.oContained
      return (.w_TipEst='C' Or .w_Anno>2014)
    endwith
  endfunc

  add object oRECORDCU_1_4 as StdCheck with uid="AUSIJJFBGN",rtseq=3,rtrep=.f.,left=136, top=20, caption="Certificazioni uniche", enabled=.f.,;
    ToolTipText = "Estrae tutte le certificazioni",;
    HelpContextID = 92898411,;
    cFormVar="w_RECORDCU", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oRECORDCU_1_4.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oRECORDCU_1_4.GetRadio()
    this.Parent.oContained.w_RECORDCU = this.RadioValue()
    return .t.
  endfunc

  func oRECORDCU_1_4.SetRadio()
    this.Parent.oContained.w_RECORDCU=trim(this.Parent.oContained.w_RECORDCU)
    this.value = ;
      iif(this.Parent.oContained.w_RECORDCU=='S',1,;
      0)
  endfunc

  func oRECORDCU_1_4.mHide()
    with this.Parent.oContained
      return (.w_TipEst='S')
    endwith
  endfunc


  add object oTIPOPERAZ_1_5 as StdCombo with uid="FVZJLSCZCJ",rtseq=4,rtrep=.f.,left=447,top=17,width=114,height=20;
    , tabstop =.f.;
    , ToolTipText = "Indica il tipo di operazione nelle dichiarazioni parziali del modello 770";
    , HelpContextID = 107634199;
    , cFormVar="w_TIPOPERAZ",RowSource=""+"Ordinario,"+"Aggiornamento,"+"Inserimento,"+"Cancellazione", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPOPERAZ_1_5.RadioValue()
    return(iif(this.value =1,'O',;
    iif(this.value =2,'A',;
    iif(this.value =3,'I',;
    iif(this.value =4,'C',;
    space(1))))))
  endfunc
  func oTIPOPERAZ_1_5.GetRadio()
    this.Parent.oContained.w_TIPOPERAZ = this.RadioValue()
    return .t.
  endfunc

  func oTIPOPERAZ_1_5.SetRadio()
    this.Parent.oContained.w_TIPOPERAZ=trim(this.Parent.oContained.w_TIPOPERAZ)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOPERAZ=='O',1,;
      iif(this.Parent.oContained.w_TIPOPERAZ=='A',2,;
      iif(this.Parent.oContained.w_TIPOPERAZ=='I',3,;
      iif(this.Parent.oContained.w_TIPOPERAZ=='C',4,;
      0))))
  endfunc

  func oTIPOPERAZ_1_5.mHide()
    with this.Parent.oContained
      return (.w_TipEst='C' Or .w_Anno>2014)
    endwith
  endfunc


  add object oTIPOPERAZ_1_6 as StdCombo with uid="FWVXSGETAY",rtseq=5,rtrep=.f.,left=447,top=17,width=114,height=20;
    , tabstop =.f.;
    , ToolTipText = "Indica il tipo di certificazione da estrarre leggendo la tipologia operazione sui movimenti ritenuta ";
    , HelpContextID = 107634199;
    , cFormVar="w_TIPOPERAZ",RowSource=""+"Ordinario,"+"Sostitutiva,"+"Annullamento", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPOPERAZ_1_6.RadioValue()
    return(iif(this.value =1,'O',;
    iif(this.value =2,'S',;
    iif(this.value =3,'A',;
    space(1)))))
  endfunc
  func oTIPOPERAZ_1_6.GetRadio()
    this.Parent.oContained.w_TIPOPERAZ = this.RadioValue()
    return .t.
  endfunc

  func oTIPOPERAZ_1_6.SetRadio()
    this.Parent.oContained.w_TIPOPERAZ=trim(this.Parent.oContained.w_TIPOPERAZ)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOPERAZ=='O',1,;
      iif(this.Parent.oContained.w_TIPOPERAZ=='S',2,;
      iif(this.Parent.oContained.w_TIPOPERAZ=='A',3,;
      0)))
  endfunc

  func oTIPOPERAZ_1_6.mHide()
    with this.Parent.oContained
      return (.w_TipEst='S')
    endwith
  endfunc

  add object oELIRECORDH_1_7 as StdCheck with uid="JBBHYUXKQL",rtseq=6,rtrep=.f.,left=136, top=46, caption="Elimina dati record H",;
    ToolTipText = "Elimina dati record H",;
    HelpContextID = 205703464,;
    cFormVar="w_ELIRECORDH", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oELIRECORDH_1_7.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oELIRECORDH_1_7.GetRadio()
    this.Parent.oContained.w_ELIRECORDH = this.RadioValue()
    return .t.
  endfunc

  func oELIRECORDH_1_7.SetRadio()
    this.Parent.oContained.w_ELIRECORDH=trim(this.Parent.oContained.w_ELIRECORDH)
    this.value = ;
      iif(this.Parent.oContained.w_ELIRECORDH=='S',1,;
      0)
  endfunc

  func oELIRECORDH_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_RecordH='S')
    endwith
   endif
  endfunc

  func oELIRECORDH_1_7.mHide()
    with this.Parent.oContained
      return (.w_TipEst='C' Or .w_Anno>2014)
    endwith
  endfunc

  add object oELIRECORDCUD_1_8 as StdCheck with uid="WAJFJLXOXZ",rtseq=7,rtrep=.f.,left=136, top=46, caption="Elimina anche certificazioni definitive",;
    ToolTipText = "Se attivo, la procedura in fase di estrazione elimina anche le certificazioni definitive",;
    HelpContextID = 200900136,;
    cFormVar="w_ELIRECORDCUD", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oELIRECORDCUD_1_8.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oELIRECORDCUD_1_8.GetRadio()
    this.Parent.oContained.w_ELIRECORDCUD = this.RadioValue()
    return .t.
  endfunc

  func oELIRECORDCUD_1_8.SetRadio()
    this.Parent.oContained.w_ELIRECORDCUD=trim(this.Parent.oContained.w_ELIRECORDCUD)
    this.value = ;
      iif(this.Parent.oContained.w_ELIRECORDCUD=='S',1,;
      0)
  endfunc

  func oELIRECORDCUD_1_8.mHide()
    with this.Parent.oContained
      return (.w_TipEst='S')
    endwith
  endfunc

  add object oELIRECORDHM_1_9 as StdCheck with uid="NXPSMGHBLD",rtseq=8,rtrep=.f.,left=136, top=75, caption="Elimina anche dati caricati manualmente",;
    ToolTipText = "Elimina anche dati caricati manualmente",;
    HelpContextID = 205388072,;
    cFormVar="w_ELIRECORDHM", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oELIRECORDHM_1_9.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oELIRECORDHM_1_9.GetRadio()
    this.Parent.oContained.w_ELIRECORDHM = this.RadioValue()
    return .t.
  endfunc

  func oELIRECORDHM_1_9.SetRadio()
    this.Parent.oContained.w_ELIRECORDHM=trim(this.Parent.oContained.w_ELIRECORDHM)
    this.value = ;
      iif(this.Parent.oContained.w_ELIRECORDHM=='S',1,;
      0)
  endfunc

  func oELIRECORDHM_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ElirecordH='S')
    endwith
   endif
  endfunc

  func oELIRECORDHM_1_9.mHide()
    with this.Parent.oContained
      return (.w_TipEst='C' Or .w_Anno>2014)
    endwith
  endfunc

  add object oRECORDST_1_10 as StdCheck with uid="KQNFHWSXPN",rtseq=9,rtrep=.f.,left=136, top=104, caption="Record ST",;
    ToolTipText = "Se attivo, estrae tutti i record ST",;
    HelpContextID = 92898410,;
    cFormVar="w_RECORDST", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oRECORDST_1_10.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oRECORDST_1_10.GetRadio()
    this.Parent.oContained.w_RECORDST = this.RadioValue()
    return .t.
  endfunc

  func oRECORDST_1_10.SetRadio()
    this.Parent.oContained.w_RECORDST=trim(this.Parent.oContained.w_RECORDST)
    this.value = ;
      iif(this.Parent.oContained.w_RECORDST=='S',1,;
      0)
  endfunc

  func oRECORDST_1_10.mHide()
    with this.Parent.oContained
      return (.w_TipEst='C' Or .w_Anno>2014)
    endwith
  endfunc

  add object oELIRECORDST_1_11 as StdCheck with uid="OINDTAZHQS",rtseq=10,rtrep=.f.,left=136, top=133, caption="Elimina anche dati caricati manualmente",;
    ToolTipText = "Elimina anche dati caricati manualmente",;
    HelpContextID = 205356584,;
    cFormVar="w_ELIRECORDST", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oELIRECORDST_1_11.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oELIRECORDST_1_11.GetRadio()
    this.Parent.oContained.w_ELIRECORDST = this.RadioValue()
    return .t.
  endfunc

  func oELIRECORDST_1_11.SetRadio()
    this.Parent.oContained.w_ELIRECORDST=trim(this.Parent.oContained.w_ELIRECORDST)
    this.value = ;
      iif(this.Parent.oContained.w_ELIRECORDST=='S',1,;
      0)
  endfunc

  func oELIRECORDST_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_RecordST='S')
    endwith
   endif
  endfunc

  func oELIRECORDST_1_11.mHide()
    with this.Parent.oContained
      return (.w_TipEst='C' Or .w_Anno>2014)
    endwith
  endfunc

  add object oRECORDST_1_12 as StdCheck with uid="DYRELWCPRL",rtseq=11,rtrep=.f.,left=136, top=20, caption="Record ST",;
    ToolTipText = "Se attivo, estrae tutti i record ST",;
    HelpContextID = 92898410,;
    cFormVar="w_RECORDST", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oRECORDST_1_12.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oRECORDST_1_12.GetRadio()
    this.Parent.oContained.w_RECORDST = this.RadioValue()
    return .t.
  endfunc

  func oRECORDST_1_12.SetRadio()
    this.Parent.oContained.w_RECORDST=trim(this.Parent.oContained.w_RECORDST)
    this.value = ;
      iif(this.Parent.oContained.w_RECORDST=='S',1,;
      0)
  endfunc

  func oRECORDST_1_12.mHide()
    with this.Parent.oContained
      return (.w_TipEst='C' Or .w_Anno=2014)
    endwith
  endfunc

  add object oELIRECORDST_1_13 as StdCheck with uid="LQLOPIUQTV",rtseq=12,rtrep=.f.,left=136, top=46, caption="Elimina anche dati caricati manualmente",;
    ToolTipText = "Elimina anche dati caricati manualmente",;
    HelpContextID = 205356584,;
    cFormVar="w_ELIRECORDST", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oELIRECORDST_1_13.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oELIRECORDST_1_13.GetRadio()
    this.Parent.oContained.w_ELIRECORDST = this.RadioValue()
    return .t.
  endfunc

  func oELIRECORDST_1_13.SetRadio()
    this.Parent.oContained.w_ELIRECORDST=trim(this.Parent.oContained.w_ELIRECORDST)
    this.value = ;
      iif(this.Parent.oContained.w_ELIRECORDST=='S',1,;
      0)
  endfunc

  func oELIRECORDST_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_RecordST='S')
    endwith
   endif
  endfunc

  func oELIRECORDST_1_13.mHide()
    with this.Parent.oContained
      return (.w_TipEst='C' Or .w_Anno=2014)
    endwith
  endfunc

  add object oESTR_RES_1_14 as StdCheck with uid="GGQRYDZYGL",rtseq=13,rtrep=.f.,left=136, top=75, caption="Residenti",;
    ToolTipText = "Se attivo, estrae le certificazioni uniche di soli soggetti non esteri",;
    HelpContextID = 73245081,;
    cFormVar="w_ESTR_RES", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oESTR_RES_1_14.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oESTR_RES_1_14.GetRadio()
    this.Parent.oContained.w_ESTR_RES = this.RadioValue()
    return .t.
  endfunc

  func oESTR_RES_1_14.SetRadio()
    this.Parent.oContained.w_ESTR_RES=trim(this.Parent.oContained.w_ESTR_RES)
    this.value = ;
      iif(this.Parent.oContained.w_ESTR_RES=='S',1,;
      0)
  endfunc

  func oESTR_RES_1_14.mHide()
    with this.Parent.oContained
      return (.w_TipEst='S')
    endwith
  endfunc

  add object oESTRNRES_1_15 as StdCheck with uid="QOZJRGARAT",rtseq=14,rtrep=.f.,left=136, top=104, caption="Non residenti",;
    ToolTipText = "Se attivo, estrae le certificazioni uniche di soli soggetti esteri non residenti",;
    HelpContextID = 55419289,;
    cFormVar="w_ESTRNRES", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oESTRNRES_1_15.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oESTRNRES_1_15.GetRadio()
    this.Parent.oContained.w_ESTRNRES = this.RadioValue()
    return .t.
  endfunc

  func oESTRNRES_1_15.SetRadio()
    this.Parent.oContained.w_ESTRNRES=trim(this.Parent.oContained.w_ESTRNRES)
    this.value = ;
      iif(this.Parent.oContained.w_ESTRNRES=='S',1,;
      0)
  endfunc

  func oESTRNRES_1_15.mHide()
    with this.Parent.oContained
      return (.w_TipEst='S')
    endwith
  endfunc

  add object oESTRSRES_1_16 as StdCheck with uid="QABIOOMGHF",rtseq=15,rtrep=.f.,left=136, top=133, caption="Non residenti Schumacker",;
    ToolTipText = "Se attivo, estrae le certificazioni uniche di soli soggetti non residenti Schumacker",;
    HelpContextID = 60662169,;
    cFormVar="w_ESTRSRES", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oESTRSRES_1_16.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oESTRSRES_1_16.GetRadio()
    this.Parent.oContained.w_ESTRSRES = this.RadioValue()
    return .t.
  endfunc

  func oESTRSRES_1_16.SetRadio()
    this.Parent.oContained.w_ESTRSRES=trim(this.Parent.oContained.w_ESTRSRES)
    this.value = ;
      iif(this.Parent.oContained.w_ESTRSRES=='S',1,;
      0)
  endfunc

  func oESTRSRES_1_16.mHide()
    with this.Parent.oContained
      return (.w_TipEst='S')
    endwith
  endfunc

  add object oANNO_1_19 as StdField with uid="SZFWRROVJC",rtseq=16,rtrep=.f.,;
    cFormVar = "w_ANNO", cQueryName = "ANNO",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Anno di estrazione",;
    HelpContextID = 60146682,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=136, Top=201, cSayPict='"9999"', cGetPict='"9999"'

  func oANNO_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_TipEst='S' And .w_ANNO>=2014) Or .w_TipEst='C')
    endwith
    return bRes
  endfunc

  add object oDATINI_1_21 as StdField with uid="JPXZKEKVIP",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � maggiore della data finale o fuori periodo",;
    ToolTipText = "Da data iniziale ",;
    HelpContextID = 96170186,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=275, Top=201

  func oDATINI_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_DATFIN>=.w_DATINI) AND (.w_DATINI>=.w_DATAINIANNO AND .w_DATINI<=.w_DATAFINEANNO))
    endwith
    return bRes
  endfunc

  add object oDATFIN_1_22 as StdField with uid="FNNEPIJJDP",rtseq=18,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � maggiore della data finale o fuori periodo",;
    ToolTipText = "A data finale ",;
    HelpContextID = 17723594,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=447, Top=201

  func oDATFIN_1_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_DATFIN>=.w_DATINI) AND (.w_DATFIN>=.w_DATAINIANNO AND .w_DATFIN<=.w_DATAFINEANNO))
    endwith
    return bRes
  endfunc


  add object oBtn_1_27 as StdButton with uid="ZMIZZNTUDJ",left=462, top=506, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per estrarre i dati";
    , HelpContextID = 10509546;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_27.Click()
      with this.Parent.oContained
        GSRI_BED(this.Parent.oContained,"E")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_27.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty(.w_Datini) Or Not Empty(.w_Datfin))
      endwith
    endif
  endfunc


  add object oBtn_1_28 as StdButton with uid="JWIBHYOYEX",left=516, top=506, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 58347194;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_28.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oMsg_1_32 as StdMemo with uid="ATBBCHAJSX",rtseq=24,rtrep=.f.,;
    cFormVar = "w_Msg", cQueryName = "Msg",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 65211962,;
    FontName = "Courier New", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=255, Width=563, Left=2, Top=242, tabstop = .f., readonly = .t.

  add object oCURAPFAL_1_36 as StdField with uid="ICFDKLFUVG",rtseq=25,rtrep=.f.,;
    cFormVar = "w_CURAPFAL", cQueryName = "CURAPFAL",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Accoglie la data di apertura del fallimento",;
    HelpContextID = 123503474,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=447, Top=73, tabstop=.F.

  func oCURAPFAL_1_36.mHide()
    with this.Parent.oContained
      return (.w_TipEst='S')
    endwith
  endfunc

  add object oStr_1_2 as StdString with uid="XCMNAKLBAL",Visible=.t., Left=4, Top=20,;
    Alignment=1, Width=125, Height=18,;
    Caption="Estrazione dati:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="YIBISOAYVK",Visible=.t., Left=7, Top=171,;
    Alignment=0, Width=162, Height=19,;
    Caption="Periodo di estrazione"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_20 as StdString with uid="TXOFEDZUGY",Visible=.t., Left=13, Top=204,;
    Alignment=1, Width=119, Height=18,;
    Caption="Anno:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="LCHEPLEODA",Visible=.t., Left=207, Top=204,;
    Alignment=1, Width=64, Height=15,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="GUHHFLLZER",Visible=.t., Left=387, Top=204,;
    Alignment=1, Width=56, Height=15,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="JOBLVCQYKB",Visible=.t., Left=337, Top=20,;
    Alignment=1, Width=106, Height=18,;
    Caption="Tipo operazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_33.mHide()
    with this.Parent.oContained
      return (.w_TipEst='S' And .w_Anno>2014)
    endwith
  endfunc

  add object oStr_1_35 as StdString with uid="KNFQRRWQPP",Visible=.t., Left=292, Top=75,;
    Alignment=1, Width=151, Height=18,;
    Caption="Data apertura fallimento:"  ;
  , bGlobalFont=.t.

  func oStr_1_35.mHide()
    with this.Parent.oContained
      return (.w_TipEst='S')
    endwith
  endfunc

  add object oBox_1_18 as StdBox with uid="EPZSCHKGYW",left=0, top=188, width=566,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsri_ked','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
