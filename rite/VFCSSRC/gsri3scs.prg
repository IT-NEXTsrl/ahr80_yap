* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri3scs                                                        *
*              Selezione comunicazioni generate                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2015-02-05                                                      *
* Last revis.: 2016-02-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsri3scs",oParentObject))

* --- Class definition
define class tgsri3scs as StdForm
  Top    = 9
  Left   = 10

  * --- Standard Properties
  Width  = 792
  Height = 339
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-02-04"
  HelpContextID=93714025
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=7

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsri3scs"
  cComment = "Selezione comunicazioni generate"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CUDATCER = ctod('  /  /  ')
  w_CODICEFIS = space(16)
  w_TIPO_COMU = space(1)
  w_SERIALE_CERT = space(10)
  w_CONFERMA = .F.
  w_CUTIPCER = space(5)
  w_NOMEFILE = space(254)
  w_ComGen = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsri3scsPag1","gsri3scs",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ComGen = this.oPgFrm.Pages(1).oPag.ComGen
    DoDefault()
    proc Destroy()
      this.w_ComGen = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CUDATCER=ctod("  /  /  ")
      .w_CODICEFIS=space(16)
      .w_TIPO_COMU=space(1)
      .w_SERIALE_CERT=space(10)
      .w_CONFERMA=.f.
      .w_CUTIPCER=space(5)
      .w_NOMEFILE=space(254)
      .w_CUDATCER=oParentObject.w_CUDATCER
      .w_CODICEFIS=oParentObject.w_CODICEFIS
      .w_TIPO_COMU=oParentObject.w_TIPO_COMU
      .w_SERIALE_CERT=oParentObject.w_SERIALE_CERT
      .w_CONFERMA=oParentObject.w_CONFERMA
      .w_CUTIPCER=oParentObject.w_CUTIPCER
      .w_NOMEFILE=oParentObject.w_NOMEFILE
      .oPgFrm.Page1.oPag.ComGen.Calculate()
        .w_CUDATCER = Nvl(Cp_todate(.w_ComGen.getVar('Cudatcer')),Cp_CharToDate('  -  -    '))
        .w_CODICEFIS = Alltrim(Nvl(.w_ComGen.getVar('Cucodfis'),' '))
        .w_TIPO_COMU = Alltrim(Nvl(.w_ComGen.getVar('Tipcas'),' '))
      .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
        .w_SERIALE_CERT = Alltrim(Nvl(.w_ComGen.getVar('Cuserial'),' '))
        .w_CONFERMA = .t.
          .DoRTCalc(6,6,.f.)
        .w_NOMEFILE = Alltrim(Nvl(.w_ComGen.getVar('Cunomfil'),' '))
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_CUDATCER=.w_CUDATCER
      .oParentObject.w_CODICEFIS=.w_CODICEFIS
      .oParentObject.w_TIPO_COMU=.w_TIPO_COMU
      .oParentObject.w_SERIALE_CERT=.w_SERIALE_CERT
      .oParentObject.w_CONFERMA=.w_CONFERMA
      .oParentObject.w_CUTIPCER=.w_CUTIPCER
      .oParentObject.w_NOMEFILE=.w_NOMEFILE
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.ComGen.Calculate()
            .w_CUDATCER = Nvl(Cp_todate(.w_ComGen.getVar('Cudatcer')),Cp_CharToDate('  -  -    '))
            .w_CODICEFIS = Alltrim(Nvl(.w_ComGen.getVar('Cucodfis'),' '))
            .w_TIPO_COMU = Alltrim(Nvl(.w_ComGen.getVar('Tipcas'),' '))
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
            .w_SERIALE_CERT = Alltrim(Nvl(.w_ComGen.getVar('Cuserial'),' '))
            .w_CONFERMA = .t.
        .DoRTCalc(6,6,.t.)
            .w_NOMEFILE = Alltrim(Nvl(.w_ComGen.getVar('Cunomfil'),' '))
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ComGen.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ComGen.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_5.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsri3scsPag1 as StdContainer
  Width  = 788
  height = 339
  stdWidth  = 788
  stdheight = 339
  resizeXpos=366
  resizeYpos=238
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object ComGen as cp_zoombox with uid="GOGGMGFANQ",left=5, top=8, width=777,height=278,;
    caption='Object',;
   bGlobalFont=.t.,;
    cZoomOnZoom="",bRetriveAllRows=.f.,cZoomFile="GSRI3SCS",bOptions=.t.,cTable="GENDCOMU",bReadOnly=.t.,bQueryOnLoad=.t.,cMenuFile="",bAdvOptions=.f.,;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 84283622


  add object oObj_1_5 as cp_runprogram with uid="BEJRALCUOP",left=5, top=345, width=186,height=32,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="Gsri_Bcu('X')",;
    cEvent = "w_comgen selected",;
    nPag=1;
    , HelpContextID = 84283622
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsri3scs','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
