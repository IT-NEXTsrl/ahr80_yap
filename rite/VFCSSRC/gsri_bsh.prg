* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_bsh                                                        *
*              QUADRO 770/SC                                                   *
*                                                                              *
*      Author: TAMSoftware & CODELAB                                           *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][98][VRS_61]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-05-19                                                      *
* Last revis.: 2015-07-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsri_bsh",oParentObject)
return(i_retval)

define class tgsri_bsh as StdBatch
  * --- Local variables
  w_AZCOFAZI = space(16)
  * --- WorkFile variables
  AZIENDA_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questo batch serve per convertire i movimenti ritenute in base alla maschera
    * --- di stampa del quadro 770/SC
    * --- La variabile Tipo mi serve per sapere se la routine � lanciata
    *     dalla generazione oppure dalla stampa
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZCOFAZI"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZCOFAZI;
        from (i_cTable) where;
            AZCODAZI = i_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_AZCOFAZI = NVL(cp_ToDate(_read_.AZCOFAZI),cp_NullValue(_read_.AZCOFAZI))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    vq_exec("..\RITE\EXE\QUERY\GSRI_BSH.VQR",this,"RITE")
    SELECT RHSERIAL,MAX(RHCODCON) AS RHCODCON,MAX(RHCODFIS) AS RHCODFIS,; 
 MAX(RHPERFIS) AS RHPERFIS,MAX(RHPEREST) AS RHPEREST,; 
 MAX(RHCOGNOM) AS RHCOGNOM,MAX(RHDESCRI) AS RHDESCRI,MAX(RH__NOME) AS RH__NOME, ; 
 MAX(RH_SESSO) AS RH_SESSO,MAX(RHDATNAS) AS RHDATNAS,MAX(RHCOMEST) AS RHCOMEST,; 
 MAX(RHPRONAS) AS RHPRONAS,MAX(RHCATPAR) AS RHCATPAR,MAX(RHEVEECC) AS RHEVEECC,; 
 MAX(IIF(this.oParentObject.w_DOMFIS<>"S" OR RHCAUPRE="N",RHCOMUNE,SPACE(30))) AS RHCOMUNE,; 
 MAX(IIF(this.oParentObject.w_DOMFIS<>"S" OR RHCAUPRE="N",RHPROVIN,SPACE(2))) AS RHPROVIN,MAX(RHCODCOM) as RHCODCOM,; 
 MAX(RHCOFISC) AS RHCOFISC,MAX(RHCITEST) AS RHCITEST,MAX(RHINDEST) AS RHINDEST,; 
 MAX(RHSTAEST) AS RHSTAEST,MAX(RHCAUPRE) AS RHCAUPRE,; 
 SUM(RHAUX004) AS RHAUX004,SUM(RHAUX005) AS RHAUX005,; 
 SUM(RHAUX008) AS RHAUX008,SUM(RHAUX009) AS RHAUX009 ,SUM(RHAUX020) as RHAUX020,; 
 SUM(RHAUX021) as RHAUX021,SUM(RHAUX022) as RHAUX022,SUM(RHAUX023) AS RHAUX023,; 
 SUM(IMPOSFALL) as IMPOSFALL,SUM(IMPREFALL) as IMPREFALL,; 
 MAX(ANFLSGRE) ANFLSGRE,RHAUX006,MAX(RHDAVERI) AS RHDAVERI , MAX(GDPROMOD) as GDPROMOD ; 
 FROM RITE GROUP BY RHSERIAL,RHAUX006; 
 ORDER BY RHCODCON ,RHCAUPRE,RHSERIAL,RHAUX006 into cursor __tmp__
    Use in Select("Rite")
    if this.oParentObject.w_TIPO $ "GV"
      select * from __tmp__ into cursor RITE1
      Use In __Tmp__
    else
      L_AZCOFAZI=this.w_AZCOFAZI
      L_DECIMI=g_PERPVL
      L_ANNO=this.oParentObject.w_ANNO
      if This.oParentObject.w_SOLODAVER="S"
        Select * from __TMP__ into cursor __TMP__ where Nvl(Rhdaveri,"N")="S" order by Rhcodcon,Rhcaupre,Rhserial,Rhaux006
      endif
      if Reccount("__TMP__")>0
        do case
          case this.oParentObject.w_Quadro="H"
            CP_CHPRN("..\RITE\EXE\QUERY\GSRI_BSH.FRX","",this.oParentObject)
          case this.oParentObject.w_Quadro="J"
            CP_CHPRN("..\RITE\EXE\QUERY\GSRI_SRS.FRX","",this.oParentObject)
        endcase
      else
        ah_ErrorMsg("Non esistono record da stampare")
      endif
      if used("__TMP__")
        select ("__TMP__")
        use
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='AZIENDA'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
