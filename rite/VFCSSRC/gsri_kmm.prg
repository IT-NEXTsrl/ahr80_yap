* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_kmm                                                        *
*              Aggiornamento intestatari/movimenti ritenute                    *
*                                                                              *
*      Author: Zucchetti S.p.A.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-05-22                                                      *
* Last revis.: 2018-05-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsri_kmm",oParentObject))

* --- Class definition
define class tgsri_kmm as StdForm
  Top    = 26
  Left   = 19

  * --- Standard Properties
  Width  = 785
  Height = 548
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2018-05-09"
  HelpContextID=68553111
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=24

  * --- Constant Properties
  _IDX = 0
  CONTI_IDX = 0
  cPrg = "gsri_kmm"
  cComment = "Aggiornamento intestatari/movimenti ritenute"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_TIPOAGGIORNAMENTO = space(1)
  o_TIPOAGGIORNAMENTO = space(1)
  w_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  w_TIPCON = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_PERCIN = space(15)
  w_PERCFIN = space(15)
  w_CODSNSOGG = space(1)
  w_CAUPRE = space(2)
  w_SELEZ = space(1)
  w_TIPOPERAZ = space(1)
  w_DESCINI = space(40)
  w_DESCFIN = space(40)
  w_RITEINI = space(1)
  w_RITEFIN = space(1)
  w_CODSNS = space(1)
  w_CODSNS = space(1)
  w_MOVIMENTI = space(1)
  o_MOVIMENTI = space(1)
  w_MRSERIAL_S = space(10)
  w_MRSERIAL = space(10)
  w_ANCODICE = space(15)
  w_EVEECC = space(1)
  w_MOV_CAUPRE = space(2)
  w_MRSERIAL_C = space(10)
  w_MOVRITE = .NULL.
  w_MOVSRITE = .NULL.
  w_SOMNSOGG = .NULL.
  w_MOVCRITE = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsri_kmmPag1","gsri_kmm",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTIPOAGGIORNAMENTO_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_MOVRITE = this.oPgFrm.Pages(1).oPag.MOVRITE
    this.w_MOVSRITE = this.oPgFrm.Pages(1).oPag.MOVSRITE
    this.w_SOMNSOGG = this.oPgFrm.Pages(1).oPag.SOMNSOGG
    this.w_MOVCRITE = this.oPgFrm.Pages(1).oPag.MOVCRITE
    DoDefault()
    proc Destroy()
      this.w_MOVRITE = .NULL.
      this.w_MOVSRITE = .NULL.
      this.w_SOMNSOGG = .NULL.
      this.w_MOVCRITE = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='CONTI'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIPOAGGIORNAMENTO=space(1)
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_TIPCON=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_PERCIN=space(15)
      .w_PERCFIN=space(15)
      .w_CODSNSOGG=space(1)
      .w_CAUPRE=space(2)
      .w_SELEZ=space(1)
      .w_TIPOPERAZ=space(1)
      .w_DESCINI=space(40)
      .w_DESCFIN=space(40)
      .w_RITEINI=space(1)
      .w_RITEFIN=space(1)
      .w_CODSNS=space(1)
      .w_CODSNS=space(1)
      .w_MOVIMENTI=space(1)
      .w_MRSERIAL_S=space(10)
      .w_MRSERIAL=space(10)
      .w_ANCODICE=space(15)
      .w_EVEECC=space(1)
      .w_MOV_CAUPRE=space(2)
      .w_MRSERIAL_C=space(10)
        .w_TIPOAGGIORNAMENTO = 'T'
        .w_DATINI = DATE(2017,01,01)
        .w_DATFIN = DATE(2017,12,31)
        .w_TIPCON = 'F'
        .w_OBTEST = i_INIDAT
        .w_PERCIN = SPACE(15)
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_PERCIN))
          .link_1_6('Full')
        endif
        .w_PERCFIN = SPACE(15)
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_PERCFIN))
          .link_1_7('Full')
        endif
        .w_CODSNSOGG = 'T'
        .w_CAUPRE = 'TT'
      .oPgFrm.Page1.oPag.MOVRITE.Calculate()
        .w_SELEZ = 'D'
        .w_TIPOPERAZ = ' '
      .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
      .oPgFrm.Page1.oPag.MOVSRITE.Calculate()
      .oPgFrm.Page1.oPag.SOMNSOGG.Calculate()
          .DoRTCalc(12,15,.f.)
        .w_CODSNS = '7'
        .w_CODSNS = '7'
        .w_MOVIMENTI = 'N'
        .w_MRSERIAL_S = .w_MOVSRITE.getVar('Mrserial')
        .w_MRSERIAL = .w_MOVRITE.getVar('Mrserial')
        .w_ANCODICE = .w_SOMNSOGG.getVar('Ancodice')
        .w_EVEECC = 'N'
      .oPgFrm.Page1.oPag.MOVCRITE.Calculate()
        .w_MOV_CAUPRE = 'A'
        .w_MRSERIAL_C = .w_MOVCRITE.getVar('Mrserial')
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,5,.t.)
        if .o_TIPOAGGIORNAMENTO<>.w_TIPOAGGIORNAMENTO
            .w_PERCIN = SPACE(15)
          .link_1_6('Full')
        endif
        if .o_TIPOAGGIORNAMENTO<>.w_TIPOAGGIORNAMENTO
            .w_PERCFIN = SPACE(15)
          .link_1_7('Full')
        endif
        .oPgFrm.Page1.oPag.MOVRITE.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
        .oPgFrm.Page1.oPag.MOVSRITE.Calculate()
        .oPgFrm.Page1.oPag.SOMNSOGG.Calculate()
        if .o_TIPOAGGIORNAMENTO<>.w_TIPOAGGIORNAMENTO.or. .o_MOVIMENTI<>.w_MOVIMENTI
          .Calculate_GWNDNOCQZH()
        endif
        .DoRTCalc(8,17,.t.)
        if .o_TIPOAGGIORNAMENTO<>.w_TIPOAGGIORNAMENTO
            .w_MOVIMENTI = 'N'
        endif
            .w_MRSERIAL_S = .w_MOVSRITE.getVar('Mrserial')
            .w_MRSERIAL = .w_MOVRITE.getVar('Mrserial')
            .w_ANCODICE = .w_SOMNSOGG.getVar('Ancodice')
        if .o_TIPOAGGIORNAMENTO<>.w_TIPOAGGIORNAMENTO
          .Calculate_OCJGCZIYKE()
        endif
        .oPgFrm.Page1.oPag.MOVCRITE.Calculate()
        .DoRTCalc(22,23,.t.)
            .w_MRSERIAL_C = .w_MOVCRITE.getVar('Mrserial')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.MOVRITE.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
        .oPgFrm.Page1.oPag.MOVSRITE.Calculate()
        .oPgFrm.Page1.oPag.SOMNSOGG.Calculate()
        .oPgFrm.Page1.oPag.MOVCRITE.Calculate()
    endwith
  return

  proc Calculate_GWNDNOCQZH()
    with this
          * --- Blank
          Gsri_bmm(this;
              ,'B';
             )
    endwith
  endproc
  proc Calculate_OCJGCZIYKE()
    with this
          * --- Aggiornamento campo somme non soggette ed eventi eccezionali
          .w_CODSNS = '7'
          .w_EVEECC = 'N'
          .w_MOV_CAUPRE = 'A'
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oDATINI_1_2.visible=!this.oPgFrm.Page1.oPag.oDATINI_1_2.mHide()
    this.oPgFrm.Page1.oPag.oDATFIN_1_3.visible=!this.oPgFrm.Page1.oPag.oDATFIN_1_3.mHide()
    this.oPgFrm.Page1.oPag.oPERCIN_1_6.visible=!this.oPgFrm.Page1.oPag.oPERCIN_1_6.mHide()
    this.oPgFrm.Page1.oPag.oPERCFIN_1_7.visible=!this.oPgFrm.Page1.oPag.oPERCFIN_1_7.mHide()
    this.oPgFrm.Page1.oPag.oCODSNSOGG_1_8.visible=!this.oPgFrm.Page1.oPag.oCODSNSOGG_1_8.mHide()
    this.oPgFrm.Page1.oPag.oCAUPRE_1_9.visible=!this.oPgFrm.Page1.oPag.oCAUPRE_1_9.mHide()
    this.oPgFrm.Page1.oPag.oTIPOPERAZ_1_13.visible=!this.oPgFrm.Page1.oPag.oTIPOPERAZ_1_13.mHide()
    this.oPgFrm.Page1.oPag.oDESCINI_1_18.visible=!this.oPgFrm.Page1.oPag.oDESCINI_1_18.mHide()
    this.oPgFrm.Page1.oPag.oDESCFIN_1_19.visible=!this.oPgFrm.Page1.oPag.oDESCFIN_1_19.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_20.visible=!this.oPgFrm.Page1.oPag.oStr_1_20.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_21.visible=!this.oPgFrm.Page1.oPag.oStr_1_21.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_27.visible=!this.oPgFrm.Page1.oPag.oStr_1_27.mHide()
    this.oPgFrm.Page1.oPag.oCODSNS_1_28.visible=!this.oPgFrm.Page1.oPag.oCODSNS_1_28.mHide()
    this.oPgFrm.Page1.oPag.oCODSNS_1_29.visible=!this.oPgFrm.Page1.oPag.oCODSNS_1_29.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_30.visible=!this.oPgFrm.Page1.oPag.oStr_1_30.mHide()
    this.oPgFrm.Page1.oPag.oMOVIMENTI_1_31.visible=!this.oPgFrm.Page1.oPag.oMOVIMENTI_1_31.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_35.visible=!this.oPgFrm.Page1.oPag.oStr_1_35.mHide()
    this.oPgFrm.Page1.oPag.oEVEECC_1_36.visible=!this.oPgFrm.Page1.oPag.oEVEECC_1_36.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_37.visible=!this.oPgFrm.Page1.oPag.oStr_1_37.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_38.visible=!this.oPgFrm.Page1.oPag.oStr_1_38.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_40.visible=!this.oPgFrm.Page1.oPag.oStr_1_40.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_41.visible=!this.oPgFrm.Page1.oPag.oStr_1_41.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_42.visible=!this.oPgFrm.Page1.oPag.oStr_1_42.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_44.visible=!this.oPgFrm.Page1.oPag.oStr_1_44.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_45.visible=!this.oPgFrm.Page1.oPag.oStr_1_45.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_46.visible=!this.oPgFrm.Page1.oPag.oStr_1_46.mHide()
    this.oPgFrm.Page1.oPag.oMOV_CAUPRE_1_47.visible=!this.oPgFrm.Page1.oPag.oMOV_CAUPRE_1_47.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.MOVRITE.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_16.Event(cEvent)
      .oPgFrm.Page1.oPag.MOVSRITE.Event(cEvent)
      .oPgFrm.Page1.oPag.SOMNSOGG.Event(cEvent)
        if lower(cEvent)==lower("Blank")
          .Calculate_GWNDNOCQZH()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.MOVCRITE.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PERCIN
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PERCIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFR',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_PERCIN)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_PERCIN))
          select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PERCIN)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_PERCIN)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_PERCIN)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PERCIN) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oPERCIN_1_6'),i_cWhere,'GSAR_AFR',"ELENCO FORNITORI/COLLABORATORI",'GSRI_AMR.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice fornitore inesistente o non soggetto a ritenute")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PERCIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_PERCIN);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_PERCIN)
            select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PERCIN = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCINI = NVL(_Link_.ANDESCRI,space(40))
      this.w_RITEINI = NVL(_Link_.ANRITENU,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PERCIN = space(15)
      endif
      this.w_DESCINI = space(40)
      this.w_RITEINI = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((EMPTY(.w_PERCFIN)) OR (.w_PERCIN<=.w_PERCFIN)) AND .w_RITEINI $ 'CS'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice fornitore inesistente o non soggetto a ritenute")
        endif
        this.w_PERCIN = space(15)
        this.w_DESCINI = space(40)
        this.w_RITEINI = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PERCIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PERCFIN
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PERCFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFR',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_PERCFIN)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_PERCFIN))
          select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PERCFIN)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_PERCFIN)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_PERCFIN)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PERCFIN) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oPERCFIN_1_7'),i_cWhere,'GSAR_AFR',"ELENCO FORNITORI/COLLABORATORI",'GSRI_AMR.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice fornitore inesistente o non soggetto a ritenute")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PERCFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_PERCFIN);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_PERCFIN)
            select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PERCFIN = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCFIN = NVL(_Link_.ANDESCRI,space(40))
      this.w_RITEFIN = NVL(_Link_.ANRITENU,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PERCFIN = space(15)
      endif
      this.w_DESCFIN = space(40)
      this.w_RITEFIN = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((.w_PERCFIN>=.w_PERCIN) OR (EMPTY(.w_PERCIN))) AND .w_RITEFIN $ 'CS'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice fornitore inesistente o non soggetto a ritenute")
        endif
        this.w_PERCFIN = space(15)
        this.w_DESCFIN = space(40)
        this.w_RITEFIN = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PERCFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTIPOAGGIORNAMENTO_1_1.RadioValue()==this.w_TIPOAGGIORNAMENTO)
      this.oPgFrm.Page1.oPag.oTIPOAGGIORNAMENTO_1_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_2.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_2.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_3.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_3.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oPERCIN_1_6.value==this.w_PERCIN)
      this.oPgFrm.Page1.oPag.oPERCIN_1_6.value=this.w_PERCIN
    endif
    if not(this.oPgFrm.Page1.oPag.oPERCFIN_1_7.value==this.w_PERCFIN)
      this.oPgFrm.Page1.oPag.oPERCFIN_1_7.value=this.w_PERCFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCODSNSOGG_1_8.RadioValue()==this.w_CODSNSOGG)
      this.oPgFrm.Page1.oPag.oCODSNSOGG_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCAUPRE_1_9.RadioValue()==this.w_CAUPRE)
      this.oPgFrm.Page1.oPag.oCAUPRE_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEZ_1_12.RadioValue()==this.w_SELEZ)
      this.oPgFrm.Page1.oPag.oSELEZ_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOPERAZ_1_13.RadioValue()==this.w_TIPOPERAZ)
      this.oPgFrm.Page1.oPag.oTIPOPERAZ_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCINI_1_18.value==this.w_DESCINI)
      this.oPgFrm.Page1.oPag.oDESCINI_1_18.value=this.w_DESCINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCFIN_1_19.value==this.w_DESCFIN)
      this.oPgFrm.Page1.oPag.oDESCFIN_1_19.value=this.w_DESCFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCODSNS_1_28.RadioValue()==this.w_CODSNS)
      this.oPgFrm.Page1.oPag.oCODSNS_1_28.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODSNS_1_29.RadioValue()==this.w_CODSNS)
      this.oPgFrm.Page1.oPag.oCODSNS_1_29.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMOVIMENTI_1_31.RadioValue()==this.w_MOVIMENTI)
      this.oPgFrm.Page1.oPag.oMOVIMENTI_1_31.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oEVEECC_1_36.RadioValue()==this.w_EVEECC)
      this.oPgFrm.Page1.oPag.oEVEECC_1_36.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMOV_CAUPRE_1_47.RadioValue()==this.w_MOV_CAUPRE)
      this.oPgFrm.Page1.oPag.oMOV_CAUPRE_1_47.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_DATINI)) or not((.w_DATFIN>=.w_DATINI) AND (.w_DATINI > DATE(2010,12,31))))  and not(.w_TIPOAGGIORNAMENTO='I')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATINI_1_2.SetFocus()
            i_bnoObbl = !empty(.w_DATINI)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � maggiore della data finale o fuori periodo")
          case   ((empty(.w_DATFIN)) or not((.w_DATFIN>=.w_DATINI)))  and not(.w_TIPOAGGIORNAMENTO='I')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFIN_1_3.SetFocus()
            i_bnoObbl = !empty(.w_DATFIN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � maggiore della data finale o fuori periodo")
          case   not(((EMPTY(.w_PERCFIN)) OR (.w_PERCIN<=.w_PERCFIN)) AND .w_RITEINI $ 'CS')  and not(.w_TIPOAGGIORNAMENTO='T')  and not(empty(.w_PERCIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPERCIN_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice fornitore inesistente o non soggetto a ritenute")
          case   not(((.w_PERCFIN>=.w_PERCIN) OR (EMPTY(.w_PERCIN))) AND .w_RITEFIN $ 'CS')  and not(.w_TIPOAGGIORNAMENTO='T')  and not(empty(.w_PERCFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPERCFIN_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice fornitore inesistente o non soggetto a ritenute")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TIPOAGGIORNAMENTO = this.w_TIPOAGGIORNAMENTO
    this.o_MOVIMENTI = this.w_MOVIMENTI
    return

enddefine

* --- Define pages as container
define class tgsri_kmmPag1 as StdContainer
  Width  = 781
  height = 548
  stdWidth  = 781
  stdheight = 548
  resizeXpos=304
  resizeYpos=232
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oTIPOAGGIORNAMENTO_1_1 as StdCombo with uid="QZXNZNBXQX",rtseq=1,rtrep=.f.,left=169,top=10,width=167,height=22;
    , ToolTipText = "Identifica la tipologia di aggiornamento";
    , HelpContextID = 160470195;
    , cFormVar="w_TIPOAGGIORNAMENTO",RowSource=""+"Intestatari,"+"Codice somme non soggette,"+"Tipo operazione,"+"Causale prestazione", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPOAGGIORNAMENTO_1_1.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'S',;
    iif(this.value =3,'T',;
    iif(this.value =4,'P',;
    space(1))))))
  endfunc
  func oTIPOAGGIORNAMENTO_1_1.GetRadio()
    this.Parent.oContained.w_TIPOAGGIORNAMENTO = this.RadioValue()
    return .t.
  endfunc

  func oTIPOAGGIORNAMENTO_1_1.SetRadio()
    this.Parent.oContained.w_TIPOAGGIORNAMENTO=trim(this.Parent.oContained.w_TIPOAGGIORNAMENTO)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOAGGIORNAMENTO=='I',1,;
      iif(this.Parent.oContained.w_TIPOAGGIORNAMENTO=='S',2,;
      iif(this.Parent.oContained.w_TIPOAGGIORNAMENTO=='T',3,;
      iif(this.Parent.oContained.w_TIPOAGGIORNAMENTO=='P',4,;
      0))))
  endfunc

  add object oDATINI_1_2 as StdField with uid="JPXZKEKVIP",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � maggiore della data finale o fuori periodo",;
    ToolTipText = "Data iniziale di selezione movimenti",;
    HelpContextID = 230387914,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=428, Top=12

  func oDATINI_1_2.mHide()
    with this.Parent.oContained
      return (.w_TIPOAGGIORNAMENTO='I')
    endwith
  endfunc

  func oDATINI_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_DATFIN>=.w_DATINI) AND (.w_DATINI > DATE(2010,12,31)))
    endwith
    return bRes
  endfunc

  add object oDATFIN_1_3 as StdField with uid="FNNEPIJJDP",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � maggiore della data finale o fuori periodo",;
    ToolTipText = "Data finale di selezione movimenti",;
    HelpContextID = 151941322,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=600, Top=12

  func oDATFIN_1_3.mHide()
    with this.Parent.oContained
      return (.w_TIPOAGGIORNAMENTO='I')
    endwith
  endfunc

  func oDATFIN_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_DATFIN>=.w_DATINI))
    endwith
    return bRes
  endfunc

  add object oPERCIN_1_6 as StdField with uid="FSEQQLEDJJ",rtseq=6,rtrep=.f.,;
    cFormVar = "w_PERCIN", cQueryName = "PERCIN",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice fornitore inesistente o non soggetto a ritenute",;
    ToolTipText = "Percipiente iniziale selezionato",;
    HelpContextID = 152144906,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=169, Top=42, cSayPict="p_FOR", cGetPict="p_FOR", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_AFR", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_PERCIN"

  func oPERCIN_1_6.mHide()
    with this.Parent.oContained
      return (.w_TIPOAGGIORNAMENTO='T')
    endwith
  endfunc

  proc oPERCIN_1_6.mAfter
    with this.Parent.oContained
      .w_PERCIN=CALCZER(.w_PERCIN,'CONTI')
    endwith
  endproc

  func oPERCIN_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oPERCIN_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPERCIN_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oPERCIN_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFR',"ELENCO FORNITORI/COLLABORATORI",'GSRI_AMR.CONTI_VZM',this.parent.oContained
  endproc
  proc oPERCIN_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_PERCIN
     i_obj.ecpSave()
  endproc

  add object oPERCFIN_1_7 as StdField with uid="UTNRCHZBCD",rtseq=7,rtrep=.f.,;
    cFormVar = "w_PERCFIN", cQueryName = "PERCFIN",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice fornitore inesistente o non soggetto a ritenute",;
    ToolTipText = "Percipiente finale selezionato",;
    HelpContextID = 29258742,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=169, Top=71, cSayPict="p_FOR", cGetPict="p_FOR", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_AFR", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_PERCFIN"

  func oPERCFIN_1_7.mHide()
    with this.Parent.oContained
      return (.w_TIPOAGGIORNAMENTO='T')
    endwith
  endfunc

  proc oPERCFIN_1_7.mAfter
    with this.Parent.oContained
      .w_PERCFIN=CALCZER(.w_PERCFIN,'CONTI')
    endwith
  endproc

  func oPERCFIN_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oPERCFIN_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPERCFIN_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oPERCFIN_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFR',"ELENCO FORNITORI/COLLABORATORI",'GSRI_AMR.CONTI_VZM',this.parent.oContained
  endproc
  proc oPERCFIN_1_7.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_PERCFIN
     i_obj.ecpSave()
  endproc


  add object oCODSNSOGG_1_8 as StdCombo with uid="TWEEYVCYFL",rtseq=8,rtrep=.f.,left=224,top=100,width=412,height=22;
    , ToolTipText = "Codice che identifica la tipologia di somme non soggette";
    , HelpContextID = 62021155;
    , cFormVar="w_CODSNSOGG",RowSource=""+"1 - Compensi docenti/ricercatori Legge n. 2 del 28 gennaio 2009,"+"2 - Lavoratori con beneficio fiscale ex art. 3 Legge 30 dicembre 2010/238,"+"5 - Compensi soggetti art.16 del D.Igs. n. 147 del 2015,"+"6 - Assegni di servizio civile,"+"7 - Erogazione di altri redditi non soggetti a ritenuta,"+"Erogazione di altri redditi non soggetti a ritenuta (Certificazione unica 2016),"+"Erogazione di altri redditi non soggetti a ritenuta (Certificazione unica 2017),"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCODSNSOGG_1_8.RadioValue()
    return(iif(this.value =1,'1',;
    iif(this.value =2,'2',;
    iif(this.value =3,'5',;
    iif(this.value =4,'A',;
    iif(this.value =5,'7',;
    iif(this.value =6,'3',;
    iif(this.value =7,'6',;
    iif(this.value =8,'T',;
    space(1))))))))))
  endfunc
  func oCODSNSOGG_1_8.GetRadio()
    this.Parent.oContained.w_CODSNSOGG = this.RadioValue()
    return .t.
  endfunc

  func oCODSNSOGG_1_8.SetRadio()
    this.Parent.oContained.w_CODSNSOGG=trim(this.Parent.oContained.w_CODSNSOGG)
    this.value = ;
      iif(this.Parent.oContained.w_CODSNSOGG=='1',1,;
      iif(this.Parent.oContained.w_CODSNSOGG=='2',2,;
      iif(this.Parent.oContained.w_CODSNSOGG=='5',3,;
      iif(this.Parent.oContained.w_CODSNSOGG=='A',4,;
      iif(this.Parent.oContained.w_CODSNSOGG=='7',5,;
      iif(this.Parent.oContained.w_CODSNSOGG=='3',6,;
      iif(this.Parent.oContained.w_CODSNSOGG=='6',7,;
      iif(this.Parent.oContained.w_CODSNSOGG=='T',8,;
      0))))))))
  endfunc

  func oCODSNSOGG_1_8.mHide()
    with this.Parent.oContained
      return (.w_TIPOAGGIORNAMENTO $'TIP')
    endwith
  endfunc


  add object oCAUPRE_1_9 as StdCombo with uid="MERGWDMNOJ",rtseq=9,rtrep=.f.,left=169,top=100,width=73,height=22;
    , ToolTipText = "Causale prestazione";
    , HelpContextID = 24404186;
    , cFormVar="w_CAUPRE",RowSource=""+"Tipo A,"+"Tipo B,"+"Tipo C,"+"Tipo D,"+"Tipo E,"+"Tipo F,"+"Tipo G,"+"Tipo H,"+"Tipo I,"+"Tipo J,"+"Tipo K,"+"Tipo L,"+"Tipo L1,"+"Tipo M,"+"Tipo M1,"+"Tipo M2,"+"Tipo N,"+"Tipo O,"+"Tipo O1,"+"Tipo P,"+"Tipo Q,"+"Tipo R,"+"Tipo S,"+"Tipo T,"+"Tipo U,"+"Tipo V,"+"Tipo V1,"+"Tipo V2,"+"Tipo W,"+"Tipo X,"+"Tipo Y,"+"Tipo Z,"+"Tipo ZO,"+"Tutte", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCAUPRE_1_9.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'B',;
    iif(this.value =3,'C',;
    iif(this.value =4,'D',;
    iif(this.value =5,'E',;
    iif(this.value =6,'F',;
    iif(this.value =7,'G',;
    iif(this.value =8,'H',;
    iif(this.value =9,'I',;
    iif(this.value =10,'J',;
    iif(this.value =11,'K',;
    iif(this.value =12,'L',;
    iif(this.value =13,'L1',;
    iif(this.value =14,'M',;
    iif(this.value =15,'M1',;
    iif(this.value =16,'M2',;
    iif(this.value =17,'N',;
    iif(this.value =18,'O',;
    iif(this.value =19,'O1',;
    iif(this.value =20,'P',;
    iif(this.value =21,'Q',;
    iif(this.value =22,'R',;
    iif(this.value =23,'S',;
    iif(this.value =24,'T',;
    iif(this.value =25,'U',;
    iif(this.value =26,'V',;
    iif(this.value =27,'V1',;
    iif(this.value =28,'V2',;
    iif(this.value =29,'W',;
    iif(this.value =30,'X',;
    iif(this.value =31,'Y',;
    iif(this.value =32,'Z',;
    iif(this.value =33,'ZO',;
    iif(this.value =34,'TT',;
    space(2))))))))))))))))))))))))))))))))))))
  endfunc
  func oCAUPRE_1_9.GetRadio()
    this.Parent.oContained.w_CAUPRE = this.RadioValue()
    return .t.
  endfunc

  func oCAUPRE_1_9.SetRadio()
    this.Parent.oContained.w_CAUPRE=trim(this.Parent.oContained.w_CAUPRE)
    this.value = ;
      iif(this.Parent.oContained.w_CAUPRE=='A',1,;
      iif(this.Parent.oContained.w_CAUPRE=='B',2,;
      iif(this.Parent.oContained.w_CAUPRE=='C',3,;
      iif(this.Parent.oContained.w_CAUPRE=='D',4,;
      iif(this.Parent.oContained.w_CAUPRE=='E',5,;
      iif(this.Parent.oContained.w_CAUPRE=='F',6,;
      iif(this.Parent.oContained.w_CAUPRE=='G',7,;
      iif(this.Parent.oContained.w_CAUPRE=='H',8,;
      iif(this.Parent.oContained.w_CAUPRE=='I',9,;
      iif(this.Parent.oContained.w_CAUPRE=='J',10,;
      iif(this.Parent.oContained.w_CAUPRE=='K',11,;
      iif(this.Parent.oContained.w_CAUPRE=='L',12,;
      iif(this.Parent.oContained.w_CAUPRE=='L1',13,;
      iif(this.Parent.oContained.w_CAUPRE=='M',14,;
      iif(this.Parent.oContained.w_CAUPRE=='M1',15,;
      iif(this.Parent.oContained.w_CAUPRE=='M2',16,;
      iif(this.Parent.oContained.w_CAUPRE=='N',17,;
      iif(this.Parent.oContained.w_CAUPRE=='O',18,;
      iif(this.Parent.oContained.w_CAUPRE=='O1',19,;
      iif(this.Parent.oContained.w_CAUPRE=='P',20,;
      iif(this.Parent.oContained.w_CAUPRE=='Q',21,;
      iif(this.Parent.oContained.w_CAUPRE=='R',22,;
      iif(this.Parent.oContained.w_CAUPRE=='S',23,;
      iif(this.Parent.oContained.w_CAUPRE=='T',24,;
      iif(this.Parent.oContained.w_CAUPRE=='U',25,;
      iif(this.Parent.oContained.w_CAUPRE=='V',26,;
      iif(this.Parent.oContained.w_CAUPRE=='V1',27,;
      iif(this.Parent.oContained.w_CAUPRE=='V2',28,;
      iif(this.Parent.oContained.w_CAUPRE=='W',29,;
      iif(this.Parent.oContained.w_CAUPRE=='X',30,;
      iif(this.Parent.oContained.w_CAUPRE=='Y',31,;
      iif(this.Parent.oContained.w_CAUPRE=='Z',32,;
      iif(this.Parent.oContained.w_CAUPRE=='ZO',33,;
      iif(this.Parent.oContained.w_CAUPRE=='TT',34,;
      0))))))))))))))))))))))))))))))))))
  endfunc

  func oCAUPRE_1_9.mHide()
    with this.Parent.oContained
      return (.w_TIPOAGGIORNAMENTO <>'P')
    endwith
  endfunc


  add object MOVRITE as cp_szoombox with uid="HTYDWYZWRQ",left=4, top=137, width=763,height=227,;
    caption='Object',;
   bGlobalFont=.t.,;
    cZoomFile="GSRI_KMM",cTable="MOV_RITE",bAdvOptions=.f.,bOptions=.t.,bQueryOnLoad=.t.,cMenuFile="GSRI_KMM",cZoomOnZoom="",bReadOnly=.t.,bNoZoomGridShape=.f.,bQueryOnDblClick=.t.,;
    cEvent = "Blank,AggiornaT",;
    nPag=1;
    , HelpContextID = 21884698


  add object oBtn_1_11 as StdButton with uid="IBQSEBRPEL",left=719, top=88, width=48,height=45,;
    CpPicture="bmp\requery.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per effettuare la ricerca";
    , HelpContextID = 108936426;
    , caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_11.Click()
      with this.Parent.oContained
        Gsri_Bmm(this.Parent.oContained,"A")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oSELEZ_1_12 as StdRadio with uid="TPBBPOENMK",rtseq=10,rtrep=.f.,left=8, top=509, width=132,height=33;
    , cFormVar="w_SELEZ", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZ_1_12.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutti"
      this.Buttons(1).HelpContextID = 167777318
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutti"
      this.Buttons(2).HelpContextID = 167777318
      this.Buttons(2).Top=15
      this.SetAll("Width",130)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELEZ_1_12.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oSELEZ_1_12.GetRadio()
    this.Parent.oContained.w_SELEZ = this.RadioValue()
    return .t.
  endfunc

  func oSELEZ_1_12.SetRadio()
    this.Parent.oContained.w_SELEZ=trim(this.Parent.oContained.w_SELEZ)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZ=='S',1,;
      iif(this.Parent.oContained.w_SELEZ=='D',2,;
      0))
  endfunc


  add object oTIPOPERAZ_1_13 as StdCombo with uid="FVZJLSCZCJ",value=4,rtseq=11,rtrep=.f.,left=183,top=415,width=114,height=22;
    , ToolTipText = "Tipo operazioni da assegnare ai movimenti selezionati";
    , HelpContextID = 26583529;
    , cFormVar="w_TIPOPERAZ",RowSource=""+"Inserimento,"+"Aggiornamento,"+"Cancellazione,"+"Nessuna", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPOPERAZ_1_13.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'A',;
    iif(this.value =3,'C',;
    iif(this.value =4,' ',;
    ' ')))))
  endfunc
  func oTIPOPERAZ_1_13.GetRadio()
    this.Parent.oContained.w_TIPOPERAZ = this.RadioValue()
    return .t.
  endfunc

  func oTIPOPERAZ_1_13.SetRadio()
    this.Parent.oContained.w_TIPOPERAZ=trim(this.Parent.oContained.w_TIPOPERAZ)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOPERAZ=='I',1,;
      iif(this.Parent.oContained.w_TIPOPERAZ=='A',2,;
      iif(this.Parent.oContained.w_TIPOPERAZ=='C',3,;
      iif(this.Parent.oContained.w_TIPOPERAZ=='',4,;
      0))))
  endfunc

  func oTIPOPERAZ_1_13.mHide()
    with this.Parent.oContained
      return (.w_TIPOAGGIORNAMENTO $'ISP')
    endwith
  endfunc


  add object oBtn_1_14 as StdButton with uid="IZQIBPSTVD",left=669, top=495, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per apportare le modifiche";
    , HelpContextID = 108936426;
    , caption='\<Modifica';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_14.Click()
      with this.Parent.oContained
        GSRI_BMM(this.Parent.oContained,"M")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_14.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_MOVIMENTI='N')
      endwith
    endif
  endfunc


  add object oBtn_1_15 as StdButton with uid="GGCFXHPQLH",left=719, top=495, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 108936426;
    , caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_15.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_16 as cp_runprogram with uid="YKGSLIAKHA",left=-15, top=555, width=194,height=24,;
    caption='Selezione/Deselezione',;
   bGlobalFont=.t.,;
    prg="GSRI_BMM('S')",;
    cEvent = "w_SELEZ Changed",;
    nPag=1;
    , HelpContextID = 125760791

  add object oDESCINI_1_18 as StdField with uid="BCVQCCTVSS",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DESCINI", cQueryName = "DESCINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 116294454,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=303, Top=42, InputMask=replicate('X',40)

  func oDESCINI_1_18.mHide()
    with this.Parent.oContained
      return (.w_TIPOAGGIORNAMENTO='T')
    endwith
  endfunc

  add object oDESCFIN_1_19 as StdField with uid="UAOUOSAMTQ",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DESCFIN", cQueryName = "DESCFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 29262646,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=303, Top=71, InputMask=replicate('X',40)

  func oDESCFIN_1_19.mHide()
    with this.Parent.oContained
      return (.w_TIPOAGGIORNAMENTO='T')
    endwith
  endfunc


  add object MOVSRITE as cp_szoombox with uid="BTIFDWBXJM",left=4, top=137, width=763,height=227,;
    caption='Object',;
   bGlobalFont=.t.,;
    cZoomFile="GSRI1KMM",cTable="MOV_RITE",bAdvOptions=.f.,bOptions=.t.,bQueryOnLoad=.t.,cMenuFile="GSRI_KMM",cZoomOnZoom="",bReadOnly=.t.,bNoZoomGridShape=.f.,bQueryOnDblClick=.t.,;
    cEvent = "AggiornaS",;
    nPag=1;
    , HelpContextID = 21884698


  add object SOMNSOGG as cp_szoombox with uid="GFZDYZCNEW",left=4, top=137, width=763,height=227,;
    caption='Object',;
   bGlobalFont=.t.,;
    cZoomFile="GSRI1KMM",cTable="CONTI",bAdvOptions=.f.,bOptions=.t.,bQueryOnLoad=.t.,cMenuFile="GSRI_KMM",cZoomOnZoom="",bReadOnly=.t.,bNoZoomGridShape=.f.,bQueryOnDblClick=.t.,;
    cEvent = "AggiornaI",;
    nPag=1;
    , HelpContextID = 21884698


  add object oCODSNS_1_28 as StdCombo with uid="UMBMOBYFOI",rtseq=16,rtrep=.f.,left=183,top=415,width=412,height=22;
    , ToolTipText = "Codice che identifica la tipologia di somme non soggette";
    , HelpContextID = 62022362;
    , cFormVar="w_CODSNS",RowSource=""+"1 - Compensi docenti/ricercatori Legge n. 2 del 28 gennaio 2009,"+"2 - Lavoratori con beneficio fiscale ex art. 3 Legge 30 dicembre 2010/238,"+"5 - Compensi soggetti art.16 del D.Igs. n. 147 del 2015,"+"6 - Assegni di servizio civile,"+"7 - Erogazione di altri redditi non soggetti a ritenuta", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCODSNS_1_28.RadioValue()
    return(iif(this.value =1,'1',;
    iif(this.value =2,'2',;
    iif(this.value =3,'5',;
    iif(this.value =4,'A',;
    iif(this.value =5,'7',;
    ' '))))))
  endfunc
  func oCODSNS_1_28.GetRadio()
    this.Parent.oContained.w_CODSNS = this.RadioValue()
    return .t.
  endfunc

  func oCODSNS_1_28.SetRadio()
    this.Parent.oContained.w_CODSNS=trim(this.Parent.oContained.w_CODSNS)
    this.value = ;
      iif(this.Parent.oContained.w_CODSNS=='1',1,;
      iif(this.Parent.oContained.w_CODSNS=='2',2,;
      iif(this.Parent.oContained.w_CODSNS=='5',3,;
      iif(this.Parent.oContained.w_CODSNS=='A',4,;
      iif(this.Parent.oContained.w_CODSNS=='7',5,;
      0)))))
  endfunc

  func oCODSNS_1_28.mHide()
    with this.Parent.oContained
      return (.w_TIPOAGGIORNAMENTO <> 'S')
    endwith
  endfunc


  add object oCODSNS_1_29 as StdCombo with uid="NDECYDLWQX",rtseq=17,rtrep=.f.,left=183,top=415,width=412,height=22;
    , ToolTipText = "Codice che identifica la tipologia di somme non soggette";
    , HelpContextID = 62022362;
    , cFormVar="w_CODSNS",RowSource=""+"1 - Compensi docenti/ricercatori Legge n. 2 del 28 gennaio 2009,"+"2 - Lavoratori con beneficio fiscale ex art. 3 Legge 30 dicembre 2010/238,"+"5 - Compensi soggetti art.16 del D.Igs. n. 147 del 2015,"+"6 - Assegni di servizio civile,"+"7 - Erogazione di altri redditi non soggetti a ritenuta,"+"Nessun aggiornamento", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCODSNS_1_29.RadioValue()
    return(iif(this.value =1,'1',;
    iif(this.value =2,'2',;
    iif(this.value =3,'5',;
    iif(this.value =4,'A',;
    iif(this.value =5,'7',;
    iif(this.value =6,'0',;
    space(1))))))))
  endfunc
  func oCODSNS_1_29.GetRadio()
    this.Parent.oContained.w_CODSNS = this.RadioValue()
    return .t.
  endfunc

  func oCODSNS_1_29.SetRadio()
    this.Parent.oContained.w_CODSNS=trim(this.Parent.oContained.w_CODSNS)
    this.value = ;
      iif(this.Parent.oContained.w_CODSNS=='1',1,;
      iif(this.Parent.oContained.w_CODSNS=='2',2,;
      iif(this.Parent.oContained.w_CODSNS=='5',3,;
      iif(this.Parent.oContained.w_CODSNS=='A',4,;
      iif(this.Parent.oContained.w_CODSNS=='7',5,;
      iif(this.Parent.oContained.w_CODSNS=='0',6,;
      0))))))
  endfunc

  func oCODSNS_1_29.mHide()
    with this.Parent.oContained
      return (.w_TIPOAGGIORNAMENTO <> 'I')
    endwith
  endfunc

  add object oMOVIMENTI_1_31 as StdCheck with uid="CBMZKZBQWZ",rtseq=18,rtrep=.f.,left=607, top=43, caption="Visualizza tutti i movimenti ",;
    ToolTipText = "Se attivo, visualizza tutti i movimenti ritenute",;
    HelpContextID = 238338730,;
    cFormVar="w_MOVIMENTI", bObbl = .f. , nPag = 1;
    , tabstop=.F.;
   , bGlobalFont=.t.


  func oMOVIMENTI_1_31.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oMOVIMENTI_1_31.GetRadio()
    this.Parent.oContained.w_MOVIMENTI = this.RadioValue()
    return .t.
  endfunc

  func oMOVIMENTI_1_31.SetRadio()
    this.Parent.oContained.w_MOVIMENTI=trim(this.Parent.oContained.w_MOVIMENTI)
    this.value = ;
      iif(this.Parent.oContained.w_MOVIMENTI=='S',1,;
      0)
  endfunc

  func oMOVIMENTI_1_31.mHide()
    with this.Parent.oContained
      return (.w_TIPOAGGIORNAMENTO $'TI')
    endwith
  endfunc


  add object oEVEECC_1_36 as StdCombo with uid="JABXNCYFUH",rtseq=22,rtrep=.f.,left=183,top=460,width=585,height=22;
    , ToolTipText = "Codice che identifica gli eventi eccezionali";
    , HelpContextID = 74468282;
    , cFormVar="w_EVEECC",RowSource=""+"0 - Nessuno,"+"1 - Contribuenti vittime di richieste estorsive ex. Art.20 Co.2 legge n. 44/99,"+"2 - Soggetti colpiti dagli eventi sismici del 24/08/2016 (Abruzzo Lazio Marche Umbria),"+"3 - Contribuenti residenti al 12/2/2011 nel comune di Lampedusa e Linosa (migranti Nord Africa),"+"4 - Soggetti colpiti dagli eventi sismici dell'ottobre 2016 (Abruzzo Lazio Marche Umbria),"+"5 - Soggetti colpiti dagli eventi sismici di gennaio 2017,"+"8 - Contribuenti colpiti da altri eventi eccezionali (Certificazione unica e 770),"+"Nessun aggiornamento", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oEVEECC_1_36.RadioValue()
    return(iif(this.value =1,'0',;
    iif(this.value =2,'1',;
    iif(this.value =3,'9',;
    iif(this.value =4,'3',;
    iif(this.value =5,'A',;
    iif(this.value =6,'C',;
    iif(this.value =7,'D',;
    iif(this.value =8,'N',;
    '0')))))))))
  endfunc
  func oEVEECC_1_36.GetRadio()
    this.Parent.oContained.w_EVEECC = this.RadioValue()
    return .t.
  endfunc

  func oEVEECC_1_36.SetRadio()
    this.Parent.oContained.w_EVEECC=trim(this.Parent.oContained.w_EVEECC)
    this.value = ;
      iif(this.Parent.oContained.w_EVEECC=='0',1,;
      iif(this.Parent.oContained.w_EVEECC=='1',2,;
      iif(this.Parent.oContained.w_EVEECC=='9',3,;
      iif(this.Parent.oContained.w_EVEECC=='3',4,;
      iif(this.Parent.oContained.w_EVEECC=='A',5,;
      iif(this.Parent.oContained.w_EVEECC=='C',6,;
      iif(this.Parent.oContained.w_EVEECC=='D',7,;
      iif(this.Parent.oContained.w_EVEECC=='N',8,;
      0))))))))
  endfunc

  func oEVEECC_1_36.mHide()
    with this.Parent.oContained
      return (.w_TIPOAGGIORNAMENTO <> 'I')
    endwith
  endfunc


  add object MOVCRITE as cp_szoombox with uid="EUHGIDRJCG",left=4, top=137, width=763,height=227,;
    caption='Object',;
   bGlobalFont=.t.,;
    cZoomFile="GSRI3KMM",cTable="MOV_RITE",bAdvOptions=.f.,bOptions=.t.,bQueryOnLoad=.t.,cMenuFile="GSRI_KMM",cZoomOnZoom="",bReadOnly=.t.,bNoZoomGridShape=.f.,bQueryOnDblClick=.t.,;
    cEvent = "AggiornaP",;
    nPag=1;
    , HelpContextID = 21884698


  add object oMOV_CAUPRE_1_47 as StdCombo with uid="CNZUKOLLNO",rtseq=23,rtrep=.f.,left=183,top=415,width=137,height=22;
    , ToolTipText = "Causale prestazione";
    , HelpContextID = 106231754;
    , cFormVar="w_MOV_CAUPRE",RowSource=""+"Tipo A,"+"Tipo B,"+"Tipo C,"+"Tipo D,"+"Tipo E,"+"Tipo F,"+"Tipo G,"+"Tipo H,"+"Tipo I,"+"Tipo J,"+"Tipo K,"+"Tipo L,"+"Tipo L1,"+"Tipo M,"+"Tipo M1,"+"Tipo M2,"+"Tipo N,"+"Tipo O,"+"Tipo O1,"+"Tipo P,"+"Tipo Q,"+"Tipo R,"+"Tipo S,"+"Tipo T,"+"Tipo U,"+"Tipo V,"+"Tipo V1,"+"Tipo V2,"+"Tipo W,"+"Tipo X,"+"Tipo Y,"+"Tipo Z,"+"Tipo ZO", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oMOV_CAUPRE_1_47.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'B',;
    iif(this.value =3,'C',;
    iif(this.value =4,'D',;
    iif(this.value =5,'E',;
    iif(this.value =6,'F',;
    iif(this.value =7,'G',;
    iif(this.value =8,'H',;
    iif(this.value =9,'I',;
    iif(this.value =10,'J',;
    iif(this.value =11,'K',;
    iif(this.value =12,'L',;
    iif(this.value =13,'L1',;
    iif(this.value =14,'M',;
    iif(this.value =15,'M1',;
    iif(this.value =16,'M2',;
    iif(this.value =17,'N',;
    iif(this.value =18,'O',;
    iif(this.value =19,'O1',;
    iif(this.value =20,'P',;
    iif(this.value =21,'Q',;
    iif(this.value =22,'R',;
    iif(this.value =23,'S',;
    iif(this.value =24,'T',;
    iif(this.value =25,'U',;
    iif(this.value =26,'V',;
    iif(this.value =27,'V1',;
    iif(this.value =28,'V2',;
    iif(this.value =29,'W',;
    iif(this.value =30,'X',;
    iif(this.value =31,'Y',;
    iif(this.value =32,'Z',;
    iif(this.value =33,'ZO',;
    space(2)))))))))))))))))))))))))))))))))))
  endfunc
  func oMOV_CAUPRE_1_47.GetRadio()
    this.Parent.oContained.w_MOV_CAUPRE = this.RadioValue()
    return .t.
  endfunc

  func oMOV_CAUPRE_1_47.SetRadio()
    this.Parent.oContained.w_MOV_CAUPRE=trim(this.Parent.oContained.w_MOV_CAUPRE)
    this.value = ;
      iif(this.Parent.oContained.w_MOV_CAUPRE=='A',1,;
      iif(this.Parent.oContained.w_MOV_CAUPRE=='B',2,;
      iif(this.Parent.oContained.w_MOV_CAUPRE=='C',3,;
      iif(this.Parent.oContained.w_MOV_CAUPRE=='D',4,;
      iif(this.Parent.oContained.w_MOV_CAUPRE=='E',5,;
      iif(this.Parent.oContained.w_MOV_CAUPRE=='F',6,;
      iif(this.Parent.oContained.w_MOV_CAUPRE=='G',7,;
      iif(this.Parent.oContained.w_MOV_CAUPRE=='H',8,;
      iif(this.Parent.oContained.w_MOV_CAUPRE=='I',9,;
      iif(this.Parent.oContained.w_MOV_CAUPRE=='J',10,;
      iif(this.Parent.oContained.w_MOV_CAUPRE=='K',11,;
      iif(this.Parent.oContained.w_MOV_CAUPRE=='L',12,;
      iif(this.Parent.oContained.w_MOV_CAUPRE=='L1',13,;
      iif(this.Parent.oContained.w_MOV_CAUPRE=='M',14,;
      iif(this.Parent.oContained.w_MOV_CAUPRE=='M1',15,;
      iif(this.Parent.oContained.w_MOV_CAUPRE=='M2',16,;
      iif(this.Parent.oContained.w_MOV_CAUPRE=='N',17,;
      iif(this.Parent.oContained.w_MOV_CAUPRE=='O',18,;
      iif(this.Parent.oContained.w_MOV_CAUPRE=='O1',19,;
      iif(this.Parent.oContained.w_MOV_CAUPRE=='P',20,;
      iif(this.Parent.oContained.w_MOV_CAUPRE=='Q',21,;
      iif(this.Parent.oContained.w_MOV_CAUPRE=='R',22,;
      iif(this.Parent.oContained.w_MOV_CAUPRE=='S',23,;
      iif(this.Parent.oContained.w_MOV_CAUPRE=='T',24,;
      iif(this.Parent.oContained.w_MOV_CAUPRE=='U',25,;
      iif(this.Parent.oContained.w_MOV_CAUPRE=='V',26,;
      iif(this.Parent.oContained.w_MOV_CAUPRE=='V1',27,;
      iif(this.Parent.oContained.w_MOV_CAUPRE=='V2',28,;
      iif(this.Parent.oContained.w_MOV_CAUPRE=='W',29,;
      iif(this.Parent.oContained.w_MOV_CAUPRE=='X',30,;
      iif(this.Parent.oContained.w_MOV_CAUPRE=='Y',31,;
      iif(this.Parent.oContained.w_MOV_CAUPRE=='Z',32,;
      iif(this.Parent.oContained.w_MOV_CAUPRE=='ZO',33,;
      0)))))))))))))))))))))))))))))))))
  endfunc

  func oMOV_CAUPRE_1_47.mHide()
    with this.Parent.oContained
      return (.w_TIPOAGGIORNAMENTO <>'P')
    endwith
  endfunc

  add object oStr_1_17 as StdString with uid="ZSMFOCKQOV",Visible=.t., Left=46, Top=14,;
    Alignment=1, Width=117, Height=18,;
    Caption="Aggiornamento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="NDPNWZVVZL",Visible=.t., Left=79, Top=44,;
    Alignment=1, Width=84, Height=15,;
    Caption="Da percipiente:"  ;
  , bGlobalFont=.t.

  func oStr_1_20.mHide()
    with this.Parent.oContained
      return (.w_TIPOAGGIORNAMENTO='T')
    endwith
  endfunc

  add object oStr_1_21 as StdString with uid="LFCJBFVJHQ",Visible=.t., Left=88, Top=72,;
    Alignment=1, Width=75, Height=15,;
    Caption="A percipiente:"  ;
  , bGlobalFont=.t.

  func oStr_1_21.mHide()
    with this.Parent.oContained
      return (.w_TIPOAGGIORNAMENTO='T')
    endwith
  endfunc

  add object oStr_1_27 as StdString with uid="ELHDTAWWKJ",Visible=.t., Left=6, Top=416,;
    Alignment=1, Width=174, Height=18,;
    Caption="Cod. somme non soggette:"  ;
  , bGlobalFont=.t.

  func oStr_1_27.mHide()
    with this.Parent.oContained
      return (.w_TIPOAGGIORNAMENTO $ 'TP')
    endwith
  endfunc

  add object oStr_1_30 as StdString with uid="VLQSAALIHB",Visible=.t., Left=6, Top=104,;
    Alignment=1, Width=214, Height=18,;
    Caption="Cod. somme non soggette percipiente:"  ;
  , bGlobalFont=.t.

  func oStr_1_30.mHide()
    with this.Parent.oContained
      return (.w_TIPOAGGIORNAMENTO $'TIP')
    endwith
  endfunc

  add object oStr_1_35 as StdString with uid="QBGFQMSXUU",Visible=.t., Left=8, Top=375,;
    Alignment=0, Width=196, Height=17,;
    Caption="Movimenti privi di somme non soggette"    , BackStyle=1, BackColor=RGB(255,0,0);
  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_35.mHide()
    with this.Parent.oContained
      return (.w_MOVIMENTI='N' OR .w_TIPOAGGIORNAMENTO='P')
    endwith
  endfunc

  add object oStr_1_37 as StdString with uid="KNMAYEAWHE",Visible=.t., Left=80, Top=460,;
    Alignment=1, Width=100, Height=18,;
    Caption="Eventi eccezionali:"  ;
  , bGlobalFont=.t.

  func oStr_1_37.mHide()
    with this.Parent.oContained
      return (.w_TIPOAGGIORNAMENTO <> 'I')
    endwith
  endfunc

  add object oStr_1_38 as StdString with uid="UYFJGOGOUG",Visible=.t., Left=343, Top=14,;
    Alignment=1, Width=81, Height=18,;
    Caption="Da data reg.:"  ;
  , bGlobalFont=.t.

  func oStr_1_38.mHide()
    with this.Parent.oContained
      return (.w_TIPOAGGIORNAMENTO='I')
    endwith
  endfunc

  add object oStr_1_40 as StdString with uid="JBQYPDJOFP",Visible=.t., Left=523, Top=14,;
    Alignment=1, Width=73, Height=18,;
    Caption="A data reg.:"  ;
  , bGlobalFont=.t.

  func oStr_1_40.mHide()
    with this.Parent.oContained
      return (.w_TIPOAGGIORNAMENTO='I')
    endwith
  endfunc

  add object oStr_1_41 as StdString with uid="KKMMFSBGJA",Visible=.t., Left=107, Top=416,;
    Alignment=1, Width=73, Height=18,;
    Caption="Tipo operaz.:"  ;
  , bGlobalFont=.t.

  func oStr_1_41.mHide()
    with this.Parent.oContained
      return (.w_TIPOAGGIORNAMENTO $'ISP')
    endwith
  endfunc

  add object oStr_1_42 as StdString with uid="EJSSBWSSEH",Visible=.t., Left=20, Top=104,;
    Alignment=1, Width=143, Height=18,;
    Caption="Causale prestazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_42.mHide()
    with this.Parent.oContained
      return (.w_TIPOAGGIORNAMENTO <>'P')
    endwith
  endfunc

  add object oStr_1_44 as StdString with uid="HIJMPEOKWM",Visible=.t., Left=8, Top=394,;
    Alignment=0, Width=196, Height=17,;
    Caption="Movimenti presenti in C.U. generata"    , BackStyle=1, BackColor=RGB(255,0,0);
  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_44.mHide()
    with this.Parent.oContained
      return (.w_MOVIMENTI='N' OR .w_TIPOAGGIORNAMENTO='S')
    endwith
  endfunc

  add object oStr_1_45 as StdString with uid="YSDGSWIFTN",Visible=.t., Left=8, Top=375,;
    Alignment=0, Width=196, Height=17,;
    Caption="Movimenti presenti in C.U."    , BackStyle=1, BackColor=RGB(255,255,0);
  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_45.mHide()
    with this.Parent.oContained
      return (.w_TIPOAGGIORNAMENTO $ 'STI')
    endwith
  endfunc

  add object oStr_1_46 as StdString with uid="BESPTNFKYE",Visible=.t., Left=37, Top=416,;
    Alignment=1, Width=143, Height=18,;
    Caption="Causale prestazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_46.mHide()
    with this.Parent.oContained
      return (.w_TIPOAGGIORNAMENTO <>'P')
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsri_kmm','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
