* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_ksq                                                        *
*              Stampa quadri SS - ST - H                                       *
*                                                                              *
*      Author: Zucchetti S.p.A.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-08-31                                                      *
* Last revis.: 2010-06-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsri_ksq",oParentObject))

* --- Class definition
define class tgsri_ksq as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 404
  Height = 112
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2010-06-17"
  HelpContextID=99219049
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=3

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsri_ksq"
  cComment = "Stampa quadri SS - ST - H "
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_STAMPAST = space(1)
  w_STAMPAH = space(1)
  w_STAMPAJ = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsri_ksqPag1","gsri_ksq",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSTAMPAST_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- gsri_ksq
    thisform.Autocenter= .T.
    thisform.Closable = .F.
    
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_STAMPAST=space(1)
      .w_STAMPAH=space(1)
      .w_STAMPAJ=space(1)
      .w_STAMPAST=oParentObject.w_STAMPAST
      .w_STAMPAH=oParentObject.w_STAMPAH
      .w_STAMPAJ=oParentObject.w_STAMPAJ
        .w_STAMPAST = .w_STAMPAST
        .w_STAMPAH = .w_STAMPAH
        .w_STAMPAJ = .w_STAMPAJ
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_4.enabled = this.oPgFrm.Page1.oPag.oBtn_1_4.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_5.enabled = this.oPgFrm.Page1.oPag.oBtn_1_5.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_STAMPAST=.w_STAMPAST
      .oParentObject.w_STAMPAH=.w_STAMPAH
      .oParentObject.w_STAMPAJ=.w_STAMPAJ
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,3,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_YNKSLDBNSX()
    with this
          * --- Assegno il valore 'N' alle variabili w_STAMPAH e w_STAMPAST
          .oParentobject.w_STAMPAH = 'N'
          .oParentobject.w_STAMPAST = 'N'
          .oParentobject.w_STAMPAJ = 'N'
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Edit Aborted")
          .Calculate_YNKSLDBNSX()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSTAMPAST_1_1.RadioValue()==this.w_STAMPAST)
      this.oPgFrm.Page1.oPag.oSTAMPAST_1_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSTAMPAH_1_2.RadioValue()==this.w_STAMPAH)
      this.oPgFrm.Page1.oPag.oSTAMPAH_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSTAMPAJ_1_3.RadioValue()==this.w_STAMPAJ)
      this.oPgFrm.Page1.oPag.oSTAMPAJ_1_3.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsri_ksqPag1 as StdContainer
  Width  = 400
  height = 112
  stdWidth  = 400
  stdheight = 112
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSTAMPAST_1_1 as StdCheck with uid="BJLXRETCBZ",rtseq=1,rtrep=.f.,left=12, top=36, caption="Stampa quadri ST",;
    ToolTipText = "Se attivo, vengono stampati i quadri ST della dichiarazione in formato PDF",;
    HelpContextID = 6779770,;
    cFormVar="w_STAMPAST", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSTAMPAST_1_1.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oSTAMPAST_1_1.GetRadio()
    this.Parent.oContained.w_STAMPAST = this.RadioValue()
    return .t.
  endfunc

  func oSTAMPAST_1_1.SetRadio()
    this.Parent.oContained.w_STAMPAST=trim(this.Parent.oContained.w_STAMPAST)
    this.value = ;
      iif(this.Parent.oContained.w_STAMPAST=='S',1,;
      0)
  endfunc

  add object oSTAMPAH_1_2 as StdCheck with uid="PVMRAJWLNA",rtseq=2,rtrep=.f.,left=187, top=36, caption="Stampa quadri H",;
    ToolTipText = "Se attivo, vengono stampati i quadri H della dichiarazione in formato PDF",;
    HelpContextID = 6779686,;
    cFormVar="w_STAMPAH", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSTAMPAH_1_2.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oSTAMPAH_1_2.GetRadio()
    this.Parent.oContained.w_STAMPAH = this.RadioValue()
    return .t.
  endfunc

  func oSTAMPAH_1_2.SetRadio()
    this.Parent.oContained.w_STAMPAH=trim(this.Parent.oContained.w_STAMPAH)
    this.value = ;
      iif(this.Parent.oContained.w_STAMPAH=='S',1,;
      0)
  endfunc

  add object oSTAMPAJ_1_3 as StdCheck with uid="YMVZATGDST",rtseq=3,rtrep=.f.,left=12, top=69, caption="Stampa quadri SS",;
    ToolTipText = "Se attivo, vengono stampati i quadri SS della dichiarazione in formato PDF",;
    HelpContextID = 6779686,;
    cFormVar="w_STAMPAJ", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSTAMPAJ_1_3.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oSTAMPAJ_1_3.GetRadio()
    this.Parent.oContained.w_STAMPAJ = this.RadioValue()
    return .t.
  endfunc

  func oSTAMPAJ_1_3.SetRadio()
    this.Parent.oContained.w_STAMPAJ=trim(this.Parent.oContained.w_STAMPAJ)
    this.value = ;
      iif(this.Parent.oContained.w_STAMPAJ=='S',1,;
      0)
  endfunc


  add object oBtn_1_4 as StdButton with uid="QFDCFAAMDM",left=291, top=61, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per stampare i quadri selezionati";
    , HelpContextID = 8273130;
    , caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_4.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_5 as StdButton with uid="BRAIRZJTXI",left=344, top=61, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 8273130;
    , caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_5.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_7 as StdString with uid="DOUVHPOJSN",Visible=.t., Left=7, Top=5,;
    Alignment=0, Width=299, Height=18,;
    Caption="Selezione quadri della dichiarazione da stampare"  ;
  , bGlobalFont=.t.

  add object oBox_1_8 as StdBox with uid="KHJKEHAASK",left=4, top=27, width=390,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsri_ksq','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
