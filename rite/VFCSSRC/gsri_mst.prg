* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_mst                                                        *
*              Dettaglio dati estratti record ST                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2015-06-05                                                      *
* Last revis.: 2018-09-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- gsri_mst
PARAMETERS pTipSt
* --- Fine Area Manuale
return(createobject("tgsri_mst"))

* --- Class definition
define class tgsri_mst as StdTrsForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 841
  Height = 530+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2018-09-20"
  HelpContextID=97121897
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=25

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  DATESDST_IDX = 0
  GEDETTEL_IDX = 0
  cFile = "DATESDST"
  cKeySelect = "STSERIAL"
  cKeyWhere  = "STSERIAL=this.w_STSERIAL"
  cKeyDetail  = "STSERIAL=this.w_STSERIAL"
  cKeyWhereODBC = '"STSERIAL="+cp_ToStrODBC(this.w_STSERIAL)';

  cKeyDetailWhereODBC = '"STSERIAL="+cp_ToStrODBC(this.w_STSERIAL)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"DATESDST.STSERIAL="+cp_ToStrODBC(this.w_STSERIAL)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'DATESDST.ST__MESE,DATESDST.STCODTRI'
  cPrg = "gsri_mst"
  cComment = "Dettaglio dati estratti record ST"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 20
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_STSERIAL = space(10)
  w_ST__ANNO = 0
  o_ST__ANNO = 0
  w_STTIPOPE = space(1)
  o_STTIPOPE = space(1)
  w_ST__MESE = 0
  w_STCODTRI = space(4)
  w_STRITOPE = 0
  o_STRITOPE = 0
  w_STRITECC = 0
  o_STRITECC = 0
  w_STRITCOM = 0
  o_STRITCOM = 0
  w_STIMPVER = 0
  o_STIMPVER = 0
  w_STIMPINT = 0
  o_STIMPINT = 0
  w_STESCGEN = space(1)
  w_ST__NOTE = space(20)
  o_ST__NOTE = space(20)
  w_STDATVER = ctod('  /  /  ')
  w_STRAVOPE = space(1)
  w_STTIPVER = space(1)
  w_STVERSER = space(10)
  w_HASEVCOP = space(50)
  w_HASEVENT = .F.
  w_GDDATEST = space(10)
  w_GDTIPEST = space(1)
  w_RESCHK = 0
  w_GTSERIAL = space(10)
  w_GDSERIAL = space(10)
  w_STDATDUP = space(10)
  w_TIPST = space(4)

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_STSERIAL = this.W_STSERIAL
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsri_mst
  * ---- Parametro Gestione
  pTipST='   '
  
  * --- Per gestione sicurezza procedure con parametri
  func getSecuritycode()
    return(lower('GSRI_MST,'+this.pTipST))
  EndFunc
  
   * --- Gestiti i soli eventi Modifica, cancellazione, inserimento
  func HasCPEvents(i_cOp)
   if this.cFunction='Query'
  	  	If Upper(i_cop)='ECPEDIT' Or Upper(i_cop)='ECPDELETE'
  	  		* esegue controllo se movimento bloccato se da problemi si pu� chiamare direttamente il Batch
          This.w_HASEVCOP=i_cop
  	  		This.NotifyEvent('HasEvent')
  	  		Return ( This.w_HASEVENT )
       Else
  	  		Return(.t.)
  	  	endif
  	Else
  	  	* --- Se l'evento non � modifica/cancellazione/inserimento prosegue
        Return(.t.)
    Endif
   EndFunc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'DATESDST','gsri_mst')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsri_mstPag1","gsri_mst",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Dettaglio record ST")
      .Pages(1).HelpContextID = 224628723
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- gsri_mst
    * --- Imposta il Titolo della Finestra della Gestione in Base al tipo
    WITH THIS.PARENT
       IF Type('pTipSt')='U' Or EMPTY(pTipSt)
          .pTipSt = '    '
       ELSE
          .pTipSt = pTipSt
       ENDIF
       DO CASE
             CASE pTipSt = '2015'
                   .cComment = CP_TRANSLATE('Dettaglio dati estratti ST 2016')
                   .cAutoZoom = 'GSRI_MST'
             CASE pTipSt = '2016'
                   .cComment = CP_TRANSLATE('Dettaglio dati estratti ST 2017')
                   .cAutoZoom = 'GSRI_MST_2017'
             CASE pTipSt = '2017'
                   .cComment = CP_TRANSLATE('Dettaglio dati estratti ST 2018')
                   .cAutoZoom = 'GSRI_MST_2018'
             Otherwise
                   .cComment = CP_TRANSLATE('Dettaglio dati estratti ST')
                   .cAutoZoom = 'DEFAULT'
       ENDCASE
    ENDWIT
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='GEDETTEL'
    this.cWorkTables[2]='DATESDST'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.DATESDST_IDX,5],7]
    this.nPostItConn=i_TableProp[this.DATESDST_IDX,3]
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_STSERIAL = NVL(STSERIAL,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from DATESDST where STSERIAL=KeySet.STSERIAL
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.DATESDST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DATESDST_IDX,2],this.bLoadRecFilter,this.DATESDST_IDX,"gsri_mst")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('DATESDST')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "DATESDST.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' DATESDST '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'STSERIAL',this.w_STSERIAL  )
      select * from (i_cTable) DATESDST where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_HASEVCOP = space(50)
        .w_HASEVENT = .f.
        .w_GDDATEST = .w_Stserial
        .w_GDTIPEST = 'E'
        .w_RESCHK = 0
        .w_GTSERIAL = .w_STSERIAL
        .w_GDSERIAL = space(10)
        .w_TIPST = This.pTipSt
        .w_STSERIAL = NVL(STSERIAL,space(10))
        .op_STSERIAL = .w_STSERIAL
        .w_ST__ANNO = NVL(ST__ANNO,0)
        .w_STTIPOPE = NVL(STTIPOPE,space(1))
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
          .link_1_18('Load')
        .w_STDATDUP = NVL(STDATDUP,space(10))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'DATESDST')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_ST__MESE = NVL(ST__MESE,0)
          .w_STCODTRI = NVL(STCODTRI,space(4))
          .w_STRITOPE = NVL(STRITOPE,0)
          .w_STRITECC = NVL(STRITECC,0)
          .w_STRITCOM = NVL(STRITCOM,0)
          .w_STIMPVER = NVL(STIMPVER,0)
          .w_STIMPINT = NVL(STIMPINT,0)
          .w_STESCGEN = NVL(STESCGEN,space(1))
          .w_ST__NOTE = NVL(ST__NOTE,space(20))
          .w_STDATVER = NVL(cp_ToDate(STDATVER),ctod("  /  /  "))
          .w_STRAVOPE = NVL(STRAVOPE,space(1))
          .w_STTIPVER = NVL(STTIPVER,space(1))
          .w_STVERSER = NVL(STVERSER,space(10))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page1.oPag.oBtn_1_7.enabled = .oPgFrm.Page1.oPag.oBtn_1_7.mCond()
        .oPgFrm.Page1.oPag.oBtn_1_17.enabled = .oPgFrm.Page1.oPag.oBtn_1_17.mCond()
        .oPgFrm.Page1.oPag.oBtn_1_20.enabled = .oPgFrm.Page1.oPag.oBtn_1_20.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_STSERIAL=space(10)
      .w_ST__ANNO=0
      .w_STTIPOPE=space(1)
      .w_ST__MESE=0
      .w_STCODTRI=space(4)
      .w_STRITOPE=0
      .w_STRITECC=0
      .w_STRITCOM=0
      .w_STIMPVER=0
      .w_STIMPINT=0
      .w_STESCGEN=space(1)
      .w_ST__NOTE=space(20)
      .w_STDATVER=ctod("  /  /  ")
      .w_STRAVOPE=space(1)
      .w_STTIPVER=space(1)
      .w_STVERSER=space(10)
      .w_HASEVCOP=space(50)
      .w_HASEVENT=.f.
      .w_GDDATEST=space(10)
      .w_GDTIPEST=space(1)
      .w_RESCHK=0
      .w_GTSERIAL=space(10)
      .w_GDSERIAL=space(10)
      .w_STDATDUP=space(10)
      .w_TIPST=space(4)
      if .cFunction<>"Filter"
        .DoRTCalc(1,2,.f.)
        .w_STTIPOPE = 'O'
        .DoRTCalc(4,5,.f.)
        .w_STRITOPE = Abs(.w_STRITOPE)
        .w_STRITECC = Abs(.w_STRITECC)
        .w_STRITCOM = Abs(.w_STRITCOM)
        .w_STIMPVER = Abs(.w_STIMPVER)
        .w_STIMPINT = Abs(.w_STIMPINT)
        .w_STESCGEN = 'N'
        .w_ST__NOTE = iif(.w_TipST<'2015',Chrtran(.w_ST__NOTE,'FGHIMORYXW',''),iif(.w_TipST='2015',Chrtran(.w_ST__NOTE,'FGHIMOPRYXW',''),iif(.w_TipST='2016',Chrtran(.w_ST__NOTE,'GHIMOPRTUVYXW',''),Chrtran(.w_ST__NOTE,'GHIJMOPRVYXW',''))))
        .DoRTCalc(13,13,.f.)
        .w_STRAVOPE = 'N'
        .w_STTIPVER = 'N'
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
        .DoRTCalc(16,18,.f.)
        .w_GDDATEST = .w_Stserial
        .w_GDTIPEST = 'E'
        .DoRTCalc(21,21,.f.)
        .w_GTSERIAL = .w_STSERIAL
        .DoRTCalc(22,22,.f.)
        if not(empty(.w_GTSERIAL))
         .link_1_18('Full')
        endif
        .DoRTCalc(23,24,.f.)
        .w_TIPST = This.pTipSt
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'DATESDST')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_20.enabled = this.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oST__ANNO_1_2.enabled = i_bVal
      .Page1.oPag.oSTTIPOPE_1_3.enabled = i_bVal
      .Page1.oPag.oST__NOTE_2_9.enabled = i_bVal
      .Page1.oPag.oSTDATVER_2_10.enabled = i_bVal
      .Page1.oPag.oSTRAVOPE_2_11.enabled = i_bVal
      .Page1.oPag.oSTTIPVER_2_12.enabled = i_bVal
      .Page1.oPag.oBtn_1_7.enabled = .Page1.oPag.oBtn_1_7.mCond()
      .Page1.oPag.oBtn_1_17.enabled = .Page1.oPag.oBtn_1_17.mCond()
      .Page1.oPag.oBtn_1_20.enabled = .Page1.oPag.oBtn_1_20.mCond()
      .Page1.oPag.oObj_1_11.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Query"
        .Page1.oPag.oST__ANNO_1_2.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'DATESDST',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DATESDST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DATESDST_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"STSER","i_codazi,w_STSERIAL")
      .op_codazi = .w_codazi
      .op_STSERIAL = .w_STSERIAL
    endwith
    this.SetControlsValue()
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.DATESDST_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_STSERIAL,"STSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ST__ANNO,"ST__ANNO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_STTIPOPE,"STTIPOPE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_STDATDUP,"STDATDUP",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.DATESDST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DATESDST_IDX,2])
    i_lTable = "DATESDST"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.DATESDST_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select distinct "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select distinct &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_ST__MESE N(2);
      ,t_STCODTRI C(4);
      ,t_STRITOPE N(18,4);
      ,t_STRITECC N(18,4);
      ,t_STRITCOM N(18,4);
      ,t_STIMPVER N(18,4);
      ,t_STIMPINT N(18,4);
      ,t_STESCGEN N(3);
      ,t_ST__NOTE C(20);
      ,t_STDATVER D(8);
      ,t_STRAVOPE N(3);
      ,t_STTIPVER N(3);
      ,CPROWNUM N(10);
      ,t_STVERSER C(10);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsri_mstbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oST__MESE_2_1.controlsource=this.cTrsName+'.t_ST__MESE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSTCODTRI_2_2.controlsource=this.cTrsName+'.t_STCODTRI'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSTRITOPE_2_3.controlsource=this.cTrsName+'.t_STRITOPE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSTRITECC_2_4.controlsource=this.cTrsName+'.t_STRITECC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSTRITCOM_2_5.controlsource=this.cTrsName+'.t_STRITCOM'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSTIMPVER_2_6.controlsource=this.cTrsName+'.t_STIMPVER'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSTIMPINT_2_7.controlsource=this.cTrsName+'.t_STIMPINT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSTESCGEN_2_8.controlsource=this.cTrsName+'.t_STESCGEN'
    this.oPgFRm.Page1.oPag.oST__NOTE_2_9.controlsource=this.cTrsName+'.t_ST__NOTE'
    this.oPgFRm.Page1.oPag.oSTDATVER_2_10.controlsource=this.cTrsName+'.t_STDATVER'
    this.oPgFRm.Page1.oPag.oSTRAVOPE_2_11.controlsource=this.cTrsName+'.t_STRAVOPE'
    this.oPgFRm.Page1.oPag.oSTTIPVER_2_12.controlsource=this.cTrsName+'.t_STTIPVER'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(55)
    this.AddVLine(108)
    this.AddVLine(239)
    this.AddVLine(371)
    this.AddVLine(495)
    this.AddVLine(626)
    this.AddVLine(756)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oST__MESE_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.DATESDST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DATESDST_IDX,2])
      with this
          cp_NextTableProg(this,i_nConn,"STSER","i_codazi,w_STSERIAL")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DATESDST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DATESDST_IDX,2])
      *
      * insert into DATESDST
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'DATESDST')
        i_extval=cp_InsertValODBCExtFlds(this,'DATESDST')
        i_cFldBody=" "+;
                  "(STSERIAL,ST__ANNO,STTIPOPE,ST__MESE,STCODTRI"+;
                  ",STRITOPE,STRITECC,STRITCOM,STIMPVER,STIMPINT"+;
                  ",STESCGEN,ST__NOTE,STDATVER,STRAVOPE,STTIPVER"+;
                  ",STVERSER,STDATDUP,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_STSERIAL)+","+cp_ToStrODBC(this.w_ST__ANNO)+","+cp_ToStrODBC(this.w_STTIPOPE)+","+cp_ToStrODBC(this.w_ST__MESE)+","+cp_ToStrODBC(this.w_STCODTRI)+;
             ","+cp_ToStrODBC(this.w_STRITOPE)+","+cp_ToStrODBC(this.w_STRITECC)+","+cp_ToStrODBC(this.w_STRITCOM)+","+cp_ToStrODBC(this.w_STIMPVER)+","+cp_ToStrODBC(this.w_STIMPINT)+;
             ","+cp_ToStrODBC(this.w_STESCGEN)+","+cp_ToStrODBC(this.w_ST__NOTE)+","+cp_ToStrODBC(this.w_STDATVER)+","+cp_ToStrODBC(this.w_STRAVOPE)+","+cp_ToStrODBC(this.w_STTIPVER)+;
             ","+cp_ToStrODBC(this.w_STVERSER)+","+cp_ToStrODBC(this.w_STDATDUP)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'DATESDST')
        i_extval=cp_InsertValVFPExtFlds(this,'DATESDST')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'STSERIAL',this.w_STSERIAL)
        INSERT INTO (i_cTable) (;
                   STSERIAL;
                  ,ST__ANNO;
                  ,STTIPOPE;
                  ,ST__MESE;
                  ,STCODTRI;
                  ,STRITOPE;
                  ,STRITECC;
                  ,STRITCOM;
                  ,STIMPVER;
                  ,STIMPINT;
                  ,STESCGEN;
                  ,ST__NOTE;
                  ,STDATVER;
                  ,STRAVOPE;
                  ,STTIPVER;
                  ,STVERSER;
                  ,STDATDUP;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_STSERIAL;
                  ,this.w_ST__ANNO;
                  ,this.w_STTIPOPE;
                  ,this.w_ST__MESE;
                  ,this.w_STCODTRI;
                  ,this.w_STRITOPE;
                  ,this.w_STRITECC;
                  ,this.w_STRITCOM;
                  ,this.w_STIMPVER;
                  ,this.w_STIMPINT;
                  ,this.w_STESCGEN;
                  ,this.w_ST__NOTE;
                  ,this.w_STDATVER;
                  ,this.w_STRAVOPE;
                  ,this.w_STTIPVER;
                  ,this.w_STVERSER;
                  ,this.w_STDATDUP;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.DATESDST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DATESDST_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_ST__MESE)) And not (empty(t_STCODTRI))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'DATESDST')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 " ST__ANNO="+cp_ToStrODBC(this.w_ST__ANNO)+;
                 ",STTIPOPE="+cp_ToStrODBC(this.w_STTIPOPE)+;
                 ",STDATDUP="+cp_ToStrODBC(this.w_STDATDUP)+;
                 ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'DATESDST')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                  ST__ANNO=this.w_ST__ANNO;
                 ,STTIPOPE=this.w_STTIPOPE;
                 ,STDATDUP=this.w_STDATDUP;
                 ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_ST__MESE)) And not (empty(t_STCODTRI))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update DATESDST
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'DATESDST')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " ST__ANNO="+cp_ToStrODBC(this.w_ST__ANNO)+;
                     ",STTIPOPE="+cp_ToStrODBC(this.w_STTIPOPE)+;
                     ",ST__MESE="+cp_ToStrODBC(this.w_ST__MESE)+;
                     ",STCODTRI="+cp_ToStrODBC(this.w_STCODTRI)+;
                     ",STRITOPE="+cp_ToStrODBC(this.w_STRITOPE)+;
                     ",STRITECC="+cp_ToStrODBC(this.w_STRITECC)+;
                     ",STRITCOM="+cp_ToStrODBC(this.w_STRITCOM)+;
                     ",STIMPVER="+cp_ToStrODBC(this.w_STIMPVER)+;
                     ",STIMPINT="+cp_ToStrODBC(this.w_STIMPINT)+;
                     ",STESCGEN="+cp_ToStrODBC(this.w_STESCGEN)+;
                     ",ST__NOTE="+cp_ToStrODBC(this.w_ST__NOTE)+;
                     ",STDATVER="+cp_ToStrODBC(this.w_STDATVER)+;
                     ",STRAVOPE="+cp_ToStrODBC(this.w_STRAVOPE)+;
                     ",STTIPVER="+cp_ToStrODBC(this.w_STTIPVER)+;
                     ",STVERSER="+cp_ToStrODBC(this.w_STVERSER)+;
                     ",STDATDUP="+cp_ToStrODBC(this.w_STDATDUP)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'DATESDST')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      ST__ANNO=this.w_ST__ANNO;
                     ,STTIPOPE=this.w_STTIPOPE;
                     ,ST__MESE=this.w_ST__MESE;
                     ,STCODTRI=this.w_STCODTRI;
                     ,STRITOPE=this.w_STRITOPE;
                     ,STRITECC=this.w_STRITECC;
                     ,STRITCOM=this.w_STRITCOM;
                     ,STIMPVER=this.w_STIMPVER;
                     ,STIMPINT=this.w_STIMPINT;
                     ,STESCGEN=this.w_STESCGEN;
                     ,ST__NOTE=this.w_ST__NOTE;
                     ,STDATVER=this.w_STDATVER;
                     ,STRAVOPE=this.w_STRAVOPE;
                     ,STTIPVER=this.w_STTIPVER;
                     ,STVERSER=this.w_STVERSER;
                     ,STDATDUP=this.w_STDATDUP;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.DATESDST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DATESDST_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_ST__MESE)) And not (empty(t_STCODTRI))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete DATESDST
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_ST__MESE)) And not (empty(t_STCODTRI))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DATESDST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DATESDST_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,5,.t.)
        if .o_STRITOPE<>.w_STRITOPE
          .w_STRITOPE = Abs(.w_STRITOPE)
        endif
        if .o_STRITECC<>.w_STRITECC
          .w_STRITECC = Abs(.w_STRITECC)
        endif
        if .o_STRITCOM<>.w_STRITCOM
          .w_STRITCOM = Abs(.w_STRITCOM)
        endif
        if .o_STIMPVER<>.w_STIMPVER
          .w_STIMPVER = Abs(.w_STIMPVER)
        endif
        if .o_STIMPINT<>.w_STIMPINT
          .w_STIMPINT = Abs(.w_STIMPINT)
        endif
        .DoRTCalc(11,11,.t.)
        if .o_ST__NOTE<>.w_ST__NOTE
          .w_ST__NOTE = iif(.w_TipST<'2015',Chrtran(.w_ST__NOTE,'FGHIMORYXW',''),iif(.w_TipST='2015',Chrtran(.w_ST__NOTE,'FGHIMOPRYXW',''),iif(.w_TipST='2016',Chrtran(.w_ST__NOTE,'GHIMOPRTUVYXW',''),Chrtran(.w_ST__NOTE,'GHIJMOPRVYXW',''))))
        endif
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
        if .o_ST__ANNO<>.w_ST__ANNO.or. .o_STTIPOPE<>.w_STTIPOPE
          .Calculate_BHRICATIGF()
        endif
        .DoRTCalc(13,21,.t.)
          .link_1_18('Full')
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_codazi<>.w_codazi
           cp_AskTableProg(this,i_nConn,"STSER","i_codazi,w_STSERIAL")
          .op_STSERIAL = .w_STSERIAL
        endif
        .op_codazi = .w_codazi
      endwith
      this.DoRTCalc(23,25,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_STVERSER with this.w_STVERSER
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return
  proc Calculate_BHRICATIGF()
    with this
          * --- St__Anno Changed,w_Sttipope Changed
          Gsri_Bct(this;
              ,'N';
             )
    endwith
  endproc
  proc Calculate_HCBLWZJNWF()
    with this
          * --- Chkfinali
          Gsri_Bct(this;
              ,'L';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oST__ANNO_1_2.enabled = this.oPgFrm.Page1.oPag.oST__ANNO_1_2.mCond()
    this.oPgFrm.Page1.oPag.oSTTIPOPE_1_3.enabled = this.oPgFrm.Page1.oPag.oSTTIPOPE_1_3.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_20.enabled = this.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oSTRITECC_2_4.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oSTRITECC_2_4.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oSTTIPOPE_1_3.visible=!this.oPgFrm.Page1.oPag.oSTTIPOPE_1_3.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_7.visible=!this.oPgFrm.Page1.oPag.oBtn_1_7.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_8.visible=!this.oPgFrm.Page1.oPag.oStr_1_8.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_17.visible=!this.oPgFrm.Page1.oPag.oBtn_1_17.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_20.visible=!this.oPgFrm.Page1.oPag.oBtn_1_20.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oSTTIPVER_2_12.visible=!this.oPgFrm.Page1.oPag.oSTTIPVER_2_12.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_11.Event(cEvent)
        if lower(cEvent)==lower("ChkFinali")
          .Calculate_HCBLWZJNWF()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=GTSERIAL
  func Link_1_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GEDETTEL_IDX,3]
    i_lTable = "GEDETTEL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GEDETTEL_IDX,2], .t., this.GEDETTEL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GEDETTEL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GTSERIAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GTSERIAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GDTIPEST,GDDATEST,GDSERIAL";
                   +" from "+i_cTable+" "+i_lTable+" where GDDATEST="+cp_ToStrODBC(this.w_GTSERIAL);
                   +" and GDTIPEST="+cp_ToStrODBC(this.w_GDTIPEST);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GDTIPEST',this.w_GDTIPEST;
                       ,'GDDATEST',this.w_GTSERIAL)
            select GDTIPEST,GDDATEST,GDSERIAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GTSERIAL = NVL(_Link_.GDDATEST,space(10))
      this.w_GDSERIAL = NVL(_Link_.GDSERIAL,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_GTSERIAL = space(10)
      endif
      this.w_GDSERIAL = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GEDETTEL_IDX,2])+'\'+cp_ToStr(_Link_.GDTIPEST,1)+'\'+cp_ToStr(_Link_.GDDATEST,1)
      cp_ShowWarn(i_cKey,this.GEDETTEL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GTSERIAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oST__ANNO_1_2.value==this.w_ST__ANNO)
      this.oPgFrm.Page1.oPag.oST__ANNO_1_2.value=this.w_ST__ANNO
    endif
    if not(this.oPgFrm.Page1.oPag.oSTTIPOPE_1_3.RadioValue()==this.w_STTIPOPE)
      this.oPgFrm.Page1.oPag.oSTTIPOPE_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oST__NOTE_2_9.value==this.w_ST__NOTE)
      this.oPgFrm.Page1.oPag.oST__NOTE_2_9.value=this.w_ST__NOTE
      replace t_ST__NOTE with this.oPgFrm.Page1.oPag.oST__NOTE_2_9.value
    endif
    if not(this.oPgFrm.Page1.oPag.oSTDATVER_2_10.value==this.w_STDATVER)
      this.oPgFrm.Page1.oPag.oSTDATVER_2_10.value=this.w_STDATVER
      replace t_STDATVER with this.oPgFrm.Page1.oPag.oSTDATVER_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oSTRAVOPE_2_11.RadioValue()==this.w_STRAVOPE)
      this.oPgFrm.Page1.oPag.oSTRAVOPE_2_11.SetRadio()
      replace t_STRAVOPE with this.oPgFrm.Page1.oPag.oSTRAVOPE_2_11.value
    endif
    if not(this.oPgFrm.Page1.oPag.oSTTIPVER_2_12.RadioValue()==this.w_STTIPVER)
      this.oPgFrm.Page1.oPag.oSTTIPVER_2_12.SetRadio()
      replace t_STTIPVER with this.oPgFrm.Page1.oPag.oSTTIPVER_2_12.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oST__MESE_2_1.value==this.w_ST__MESE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oST__MESE_2_1.value=this.w_ST__MESE
      replace t_ST__MESE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oST__MESE_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSTCODTRI_2_2.value==this.w_STCODTRI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSTCODTRI_2_2.value=this.w_STCODTRI
      replace t_STCODTRI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSTCODTRI_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSTRITOPE_2_3.value==this.w_STRITOPE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSTRITOPE_2_3.value=this.w_STRITOPE
      replace t_STRITOPE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSTRITOPE_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSTRITECC_2_4.value==this.w_STRITECC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSTRITECC_2_4.value=this.w_STRITECC
      replace t_STRITECC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSTRITECC_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSTRITCOM_2_5.value==this.w_STRITCOM)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSTRITCOM_2_5.value=this.w_STRITCOM
      replace t_STRITCOM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSTRITCOM_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSTIMPVER_2_6.value==this.w_STIMPVER)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSTIMPVER_2_6.value=this.w_STIMPVER
      replace t_STIMPVER with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSTIMPVER_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSTIMPINT_2_7.value==this.w_STIMPINT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSTIMPINT_2_7.value=this.w_STIMPINT
      replace t_STIMPINT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSTIMPINT_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSTESCGEN_2_8.RadioValue()==this.w_STESCGEN)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSTESCGEN_2_8.SetRadio()
      replace t_STESCGEN with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSTESCGEN_2_8.value
    endif
    cp_SetControlsValueExtFlds(this,'DATESDST')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_ST__ANNO) or not((.w_TipST='2015' And .w_St__Anno=2015) Or (.w_TipST='2014' And .w_St__Anno<2015) Or (.w_TipST='2016' And .w_St__Anno=2016) Or (.w_TipST='2017' And .w_St__Anno=2017)))  and (.cFunction='Load')
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oST__ANNO_1_2.SetFocus()
            i_bnoObbl = !empty(.w_ST__ANNO)
            i_bnoChk = .f.
            i_bRes = .f.
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsri_mst
      * --- Controlli Finali
      if i_bRes=.t. and .cFunction='Load'
         .w_RESCHK=0
           .NotifyEvent('ChkFinali')
           if .w_RESCHK<>0
              i_bRes=.f.
           endif 
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    local i_oldarea
    i_oldarea=select()
    select count(*) as cnt from (this.cTrsName);
       where not(deleted()) and (not(Empty(t_ST__MESE)) And not (empty(t_STCODTRI)));
        into cursor __chk__
    if not(max(0,1)<=cnt)
      do cp_ErrorMsg with MSG_YOU_MUST_INSERT_AT_LEAST_ONE_ROW
      use
      select (i_oldarea)
      return(.f.)
    endif
    use
    select (i_oldarea)
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(.w_ST__MESE>=1 AND .w_ST__MESE<=12) and (not(Empty(.w_ST__MESE)) And not (empty(.w_STCODTRI)))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oST__MESE_2_1
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Mese indicato non valido")
      endcase
      if not(Empty(.w_ST__MESE)) And not (empty(.w_STCODTRI))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_ST__ANNO = this.w_ST__ANNO
    this.o_STTIPOPE = this.w_STTIPOPE
    this.o_STRITOPE = this.w_STRITOPE
    this.o_STRITECC = this.w_STRITECC
    this.o_STRITCOM = this.w_STRITCOM
    this.o_STIMPVER = this.w_STIMPVER
    this.o_STIMPINT = this.w_STIMPINT
    this.o_ST__NOTE = this.w_ST__NOTE
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_ST__MESE)) And not (empty(t_STCODTRI)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_ST__MESE=0
      .w_STCODTRI=space(4)
      .w_STRITOPE=0
      .w_STRITECC=0
      .w_STRITCOM=0
      .w_STIMPVER=0
      .w_STIMPINT=0
      .w_STESCGEN=space(1)
      .w_ST__NOTE=space(20)
      .w_STDATVER=ctod("  /  /  ")
      .w_STRAVOPE=space(1)
      .w_STTIPVER=space(1)
      .w_STVERSER=space(10)
      .DoRTCalc(1,5,.f.)
        .w_STRITOPE = Abs(.w_STRITOPE)
        .w_STRITECC = Abs(.w_STRITECC)
        .w_STRITCOM = Abs(.w_STRITCOM)
        .w_STIMPVER = Abs(.w_STIMPVER)
        .w_STIMPINT = Abs(.w_STIMPINT)
        .w_STESCGEN = 'N'
        .w_ST__NOTE = iif(.w_TipST<'2015',Chrtran(.w_ST__NOTE,'FGHIMORYXW',''),iif(.w_TipST='2015',Chrtran(.w_ST__NOTE,'FGHIMOPRYXW',''),iif(.w_TipST='2016',Chrtran(.w_ST__NOTE,'GHIMOPRTUVYXW',''),Chrtran(.w_ST__NOTE,'GHIJMOPRVYXW',''))))
      .DoRTCalc(13,13,.f.)
        .w_STRAVOPE = 'N'
        .w_STTIPVER = 'N'
    endwith
    this.DoRTCalc(16,25,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_ST__MESE = t_ST__MESE
    this.w_STCODTRI = t_STCODTRI
    this.w_STRITOPE = t_STRITOPE
    this.w_STRITECC = t_STRITECC
    this.w_STRITCOM = t_STRITCOM
    this.w_STIMPVER = t_STIMPVER
    this.w_STIMPINT = t_STIMPINT
    this.w_STESCGEN = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSTESCGEN_2_8.RadioValue(.t.)
    this.w_ST__NOTE = t_ST__NOTE
    this.w_STDATVER = t_STDATVER
    this.w_STRAVOPE = this.oPgFrm.Page1.oPag.oSTRAVOPE_2_11.RadioValue(.t.)
    this.w_STTIPVER = this.oPgFrm.Page1.oPag.oSTTIPVER_2_12.RadioValue(.t.)
    this.w_STVERSER = t_STVERSER
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_ST__MESE with this.w_ST__MESE
    replace t_STCODTRI with this.w_STCODTRI
    replace t_STRITOPE with this.w_STRITOPE
    replace t_STRITECC with this.w_STRITECC
    replace t_STRITCOM with this.w_STRITCOM
    replace t_STIMPVER with this.w_STIMPVER
    replace t_STIMPINT with this.w_STIMPINT
    replace t_STESCGEN with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSTESCGEN_2_8.ToRadio()
    replace t_ST__NOTE with this.w_ST__NOTE
    replace t_STDATVER with this.w_STDATVER
    replace t_STRAVOPE with this.oPgFrm.Page1.oPag.oSTRAVOPE_2_11.ToRadio()
    replace t_STTIPVER with this.oPgFrm.Page1.oPag.oSTTIPVER_2_12.ToRadio()
    replace t_STVERSER with this.w_STVERSER
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsri_mstPag1 as StdContainer
  Width  = 837
  height = 530
  stdWidth  = 837
  stdheight = 530
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oST__ANNO_1_2 as StdField with uid="XYHZUQUAUD",rtseq=2,rtrep=.f.,;
    cFormVar = "w_ST__ANNO", cQueryName = "ST__ANNO",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Anno di riferimento",;
    HelpContextID = 55880843,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=141, Top=13, cSayPict='"9999"', cGetPict='"9999"'

  func oST__ANNO_1_2.mCond()
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
  endfunc

  func oST__ANNO_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_TipST='2015' And .w_St__Anno=2015) Or (.w_TipST='2014' And .w_St__Anno<2015) Or (.w_TipST='2016' And .w_St__Anno=2016) Or (.w_TipST='2017' And .w_St__Anno=2017))
    endwith
    return bRes
  endfunc


  add object oSTTIPOPE_1_3 as StdCombo with uid="ODJPSMOELS",rtseq=3,rtrep=.f.,left=329,top=14,width=168,height=21;
    , HelpContextID = 243573611;
    , cFormVar="w_STTIPOPE",RowSource=""+"Ordinario,"+"Aggiornamento,"+"Inserimento,"+"Cancellazione", bObbl = .f. , nPag = 1;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oSTTIPOPE_1_3.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..STTIPOPE,&i_cF..t_STTIPOPE),this.value)
    return(iif(xVal =1,'O',;
    iif(xVal =2,'A',;
    iif(xVal =3,'I',;
    iif(xVal =4,'C',;
    space(1))))))
  endfunc
  func oSTTIPOPE_1_3.GetRadio()
    this.Parent.oContained.w_STTIPOPE = this.RadioValue()
    return .t.
  endfunc

  func oSTTIPOPE_1_3.ToRadio()
    this.Parent.oContained.w_STTIPOPE=trim(this.Parent.oContained.w_STTIPOPE)
    return(;
      iif(this.Parent.oContained.w_STTIPOPE=='O',1,;
      iif(this.Parent.oContained.w_STTIPOPE=='A',2,;
      iif(this.Parent.oContained.w_STTIPOPE=='I',3,;
      iif(this.Parent.oContained.w_STTIPOPE=='C',4,;
      0)))))
  endfunc

  func oSTTIPOPE_1_3.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oSTTIPOPE_1_3.mCond()
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
  endfunc

  func oSTTIPOPE_1_3.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TipST='2015' Or .w_TipST='2016' Or .w_TipST='2017')
    endwith
    endif
  endfunc


  add object oBtn_1_7 as StdButton with uid="RCCGCSIAEP",left=767, top=473, width=48,height=45,;
    CpPicture="bmp\VERIFICA.bmp", caption="", nPag=1;
    , ToolTipText = "Visualizza versamenti periodici Irpef";
    , HelpContextID = 26483574;
    , Caption='V\<ers. per.',tabstop=.f.;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_7.Click()
      with this.Parent.oContained
        Gsri_Bct(this.Parent.oContained,"A")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_7.mCond()
    with this.Parent.oContained
      return (Not Empty(.w_Stverser))
    endwith
  endfunc

  func oBtn_1_7.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty(.w_Stverser))
    endwith
   endif
  endfunc


  add object oObj_1_11 as cp_runprogram with uid="XTEQWVBIFS",left=0, top=545, width=210,height=27,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSRI_BHE(w_HASEVCOP)",;
    cEvent = "HasEvent",;
    nPag=1;
    , ToolTipText = "Richiamato dall'area manuale declare (hascpevent)";
    , HelpContextID = 80875750


  add object oBtn_1_17 as StdButton with uid="QDFSLCUEJK",left=778, top=5, width=48,height=45,;
    CpPicture="bmp\VERIFICA.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per accedere alla generazione file 770";
    , HelpContextID = 26483574;
    , tabstop=.f., caption='\<File 770';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_17.Click()
      with this.Parent.oContained
        Gsri_Bct(this.Parent.oContained,"G")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_17.mCond()
    with this.Parent.oContained
      return (Not Empty(.w_Gdserial))
    endwith
  endfunc

  func oBtn_1_17.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty(.w_Gdserial))
    endwith
   endif
  endfunc


  add object oBtn_1_20 as StdButton with uid="MGTLLIGFFL",left=726, top=5, width=48,height=45,;
    CpPicture="bmp\Copia.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per duplicare i dati estratti";
    , HelpContextID = 26483574;
    , tabstop=.f., caption='Du\<plica';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_20.Click()
      with this.Parent.oContained
        Gsri_Bct(this.Parent.oContained,"D")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_20.mCond()
    with this.Parent.oContained
      return (Not Empty(.w_Gdserial))
    endwith
  endfunc

  func oBtn_1_20.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty(.w_Gdserial) Or .w_St__Anno<=2014)
    endwith
   endif
  endfunc


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=16, top=58, width=812,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=8,Field1="ST__MESE",Label1="Mese",Field2="STCODTRI",Label2="Tributo",Field3="STRITOPE",Label3="Ritenute operate",Field4="STRITECC",Label4="Importo a scomputo",Field5="STRITCOM",Label5="Crediti d'imposta",Field6="STIMPVER",Label6="Importo versato",Field7="STIMPINT",Label7="Interessi",Field8="STESCGEN",Label8="Escl. da gen.",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 218968954

  add object oStr_1_4 as StdString with uid="PESYXIRQFF",Visible=.t., Left=10, Top=15,;
    Alignment=1, Width=127, Height=18,;
    Caption="Anno di riferimento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="BSOSOTSULR",Visible=.t., Left=-5, Top=481,;
    Alignment=1, Width=134, Height=18,;
    Caption="Note su versamento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="KUIMHLQFFU",Visible=.t., Left=289, Top=481,;
    Alignment=1, Width=115, Height=18,;
    Caption="Data versamento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="QFRJWMQTHW",Visible=.t., Left=225, Top=15,;
    Alignment=1, Width=100, Height=18,;
    Caption="Tipo operazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_8.mHide()
    with this.Parent.oContained
      return (.w_TipST='2015' Or .w_TipST='2016' Or .w_TipST='2017')
    endwith
  endfunc

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=9,top=78,;
    width=808+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*20*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=10,top=79,width=807+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*20*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oST__NOTE_2_9.Refresh()
      this.Parent.oSTDATVER_2_10.Refresh()
      this.Parent.oSTRAVOPE_2_11.Refresh()
      this.Parent.oSTTIPVER_2_12.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oST__NOTE_2_9 as StdTrsField with uid="JNOZKBFILE",rtseq=12,rtrep=.t.,;
    cFormVar="w_ST__NOTE",value=space(20),;
    ToolTipText = "Note versamento (A-B-C-D-E-F-K-L-N-Q-S-T-U-Z) ",;
    HelpContextID = 242963307,;
    cTotal="", bFixedPos=.t., cQueryName = "ST__NOTE",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=132, Top=477, cGetPict=[repl('!',20)], InputMask=replicate('X',20)

  add object oSTDATVER_2_10 as StdTrsField with uid="BZRTCKKFCQ",rtseq=13,rtrep=.t.,;
    cFormVar="w_STDATVER",value=ctod("  /  /  "),;
    ToolTipText = "Data del versamento",;
    HelpContextID = 96183160,;
    cTotal="", bFixedPos=.t., cQueryName = "STDATVER",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=406, Top=477

  add object oSTRAVOPE_2_11 as StdTrsCheck with uid="KCAXALFVLL",rtrep=.t.,;
    cFormVar="w_STRAVOPE",  caption="Ravvedimento",;
    ToolTipText = "Ravvedimento",;
    HelpContextID = 249332587,;
    Left=491, Top=476,;
    cTotal="", cQueryName = "STRAVOPE",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
   , bGlobalFont=.t.


  func oSTRAVOPE_2_11.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..STRAVOPE,&i_cF..t_STRAVOPE),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oSTRAVOPE_2_11.GetRadio()
    this.Parent.oContained.w_STRAVOPE = this.RadioValue()
    return .t.
  endfunc

  func oSTRAVOPE_2_11.ToRadio()
    this.Parent.oContained.w_STRAVOPE=trim(this.Parent.oContained.w_STRAVOPE)
    return(;
      iif(this.Parent.oContained.w_STRAVOPE=='S',1,;
      0))
  endfunc

  func oSTRAVOPE_2_11.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oSTTIPVER_2_12 as StdTrsCheck with uid="QRVLRBVLXF",rtrep=.t.,;
    cFormVar="w_STTIPVER",  caption="Tesoreria",;
    ToolTipText = "Tesoreria",;
    HelpContextID = 92578680,;
    Left=620, Top=476,;
    cTotal="", cQueryName = "STTIPVER",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
   , bGlobalFont=.t.


  func oSTTIPVER_2_12.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..STTIPVER,&i_cF..t_STTIPVER),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oSTTIPVER_2_12.GetRadio()
    this.Parent.oContained.w_STTIPVER = this.RadioValue()
    return .t.
  endfunc

  func oSTTIPVER_2_12.ToRadio()
    this.Parent.oContained.w_STTIPVER=trim(this.Parent.oContained.w_STTIPVER)
    return(;
      iif(this.Parent.oContained.w_STTIPVER=='S',1,;
      0))
  endfunc

  func oSTTIPVER_2_12.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oSTTIPVER_2_12.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TipST='2016' or .w_TipST='2017' )
    endwith
    endif
  endfunc

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsri_mstBodyRow as CPBodyRowCnt
  Width=798
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oST__MESE_2_1 as StdTrsField with uid="ILIVFUFRQY",rtseq=4,rtrep=.t.,;
    cFormVar="w_ST__MESE",value=0,;
    ToolTipText = "Mese di riferimento",;
    HelpContextID = 74142571,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Mese indicato non valido",;
   bGlobalFont=.t.,;
    Height=17, Width=34, Left=-2, Top=0, cSayPict=["99"], cGetPict=["99"]

  func oST__MESE_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_ST__MESE>=1 AND .w_ST__MESE<=12)
    endwith
    return bRes
  endfunc

  add object oSTCODTRI_2_2 as StdTrsField with uid="RMBXGIEMTL",rtseq=5,rtrep=.t.,;
    cFormVar="w_STCODTRI",value=space(4),;
    ToolTipText = "Codice tributo",;
    HelpContextID = 46764911,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=48, Left=37, Top=0, InputMask=replicate('X',4)

  add object oSTRITOPE_2_3 as StdTrsField with uid="SGCFCWEJOU",rtseq=6,rtrep=.t.,;
    cFormVar="w_STRITOPE",value=0,;
    ToolTipText = "Ritenute operate",;
    HelpContextID = 247759723,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=125, Left=90, Top=0, cSayPict=[v_PV(80)], cGetPict=[v_GV(80)]

  add object oSTRITECC_2_4 as StdTrsField with uid="XLYJICVEKK",rtseq=7,rtrep=.t.,;
    cFormVar="w_STRITECC",value=0,;
    ToolTipText = "Importi utilizzati a scomputo (non gestito a partire dal mod.770/2017)",;
    HelpContextID = 79987561,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=125, Left=222, Top=0, cSayPict=[v_PV(80)], cGetPict=[v_GV(80)]

  func oSTRITECC_2_4.mCond()
    with this.Parent.oContained
      return (.w_TipST='2014' Or .w_TipST='2015')
    endwith
  endfunc

  add object oSTRITCOM_2_5 as StdTrsField with uid="MZMGESOVKQ",rtseq=8,rtrep=.t.,;
    cFormVar="w_STRITCOM",value=0,;
    ToolTipText = "Crediti d'imposta utilizzati a scomputo",;
    HelpContextID = 222002317,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=118, Left=354, Top=0, cSayPict=[v_PV(80)], cGetPict=[v_GV(80)]

  add object oSTIMPVER_2_6 as StdTrsField with uid="SXAENXQXGK",rtseq=9,rtrep=.t.,;
    cFormVar="w_STIMPVER",value=0,;
    ToolTipText = "Importo versato",;
    HelpContextID = 92795768,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=125, Left=477, Top=0, cSayPict=[v_PV(80)], cGetPict=[v_GV(80)]

  add object oSTIMPINT_2_7 as StdTrsField with uid="APWNKOUBHJ",rtseq=10,rtrep=.t.,;
    cFormVar="w_STIMPINT",value=0,;
    ToolTipText = "Interessi",;
    HelpContextID = 125308038,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=125, Left=609, Top=0, cSayPict=[v_PV(80)], cGetPict=[v_GV(80)]

  add object oSTESCGEN_2_8 as StdTrsCombo with uid="PUNQMEEBJY",rtrep=.t.,;
    cFormVar="w_STESCGEN", RowSource=""+"Si,"+"No" , ;
    ToolTipText = "Se vale si, riga esclusa dalla generazione del file telematico",;
    HelpContextID = 96318324,;
    Height=22, Width=56, Left=737, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oSTESCGEN_2_8.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..STESCGEN,&i_cF..t_STESCGEN),this.value)
    return(iif(xVal =1,'S',;
    iif(xVal =2,'N',;
    space(1))))
  endfunc
  func oSTESCGEN_2_8.GetRadio()
    this.Parent.oContained.w_STESCGEN = this.RadioValue()
    return .t.
  endfunc

  func oSTESCGEN_2_8.ToRadio()
    this.Parent.oContained.w_STESCGEN=trim(this.Parent.oContained.w_STESCGEN)
    return(;
      iif(this.Parent.oContained.w_STESCGEN=='S',1,;
      iif(this.Parent.oContained.w_STESCGEN=='N',2,;
      0)))
  endfunc

  func oSTESCGEN_2_8.SetRadio()
    this.value=this.ToRadio()
  endfunc
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oST__MESE_2_1.When()
    return(.t.)
  proc oST__MESE_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oST__MESE_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=19
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsri_mst','DATESDST','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".STSERIAL=DATESDST.STSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
