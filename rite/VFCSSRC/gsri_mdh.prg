* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_mdh                                                        *
*              Dettaglio dati estratti record H                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-06-22                                                      *
* Last revis.: 2015-06-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsri_mdh")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsri_mdh")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsri_mdh")
  return

* --- Class definition
define class tgsri_mdh as StdPCForm
  Width  = 800
  Height = 442
  Top    = 10
  Left   = 10
  cComment = "Dettaglio dati estratti record H"
  cPrg = "gsri_mdh"
  HelpContextID=80344681
  add object cnt as tcgsri_mdh
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsri_mdh as PCContext
  w_RHSERIAL = space(10)
  w_RHRIFMOV = space(10)
  w_RHDATREG = space(8)
  w_RHNUMREG = 0
  w_RHCODESE = space(4)
  w_RHESCLGE = space(1)
  w_RHAUX004 = 0
  w_RHAUX005 = 0
  w_RHAUX006 = space(1)
  w_RHAUX008 = 0
  w_RHAUX009 = 0
  w_RHAUX020 = 0
  w_RHAUX021 = 0
  w_RHAUX022 = 0
  w_RHAUX023 = 0
  proc Save(i_oFrom)
    this.w_RHSERIAL = i_oFrom.w_RHSERIAL
    this.w_RHRIFMOV = i_oFrom.w_RHRIFMOV
    this.w_RHDATREG = i_oFrom.w_RHDATREG
    this.w_RHNUMREG = i_oFrom.w_RHNUMREG
    this.w_RHCODESE = i_oFrom.w_RHCODESE
    this.w_RHESCLGE = i_oFrom.w_RHESCLGE
    this.w_RHAUX004 = i_oFrom.w_RHAUX004
    this.w_RHAUX005 = i_oFrom.w_RHAUX005
    this.w_RHAUX006 = i_oFrom.w_RHAUX006
    this.w_RHAUX008 = i_oFrom.w_RHAUX008
    this.w_RHAUX009 = i_oFrom.w_RHAUX009
    this.w_RHAUX020 = i_oFrom.w_RHAUX020
    this.w_RHAUX021 = i_oFrom.w_RHAUX021
    this.w_RHAUX022 = i_oFrom.w_RHAUX022
    this.w_RHAUX023 = i_oFrom.w_RHAUX023
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_RHSERIAL = this.w_RHSERIAL
    i_oTo.w_RHRIFMOV = this.w_RHRIFMOV
    i_oTo.w_RHDATREG = this.w_RHDATREG
    i_oTo.w_RHNUMREG = this.w_RHNUMREG
    i_oTo.w_RHCODESE = this.w_RHCODESE
    i_oTo.w_RHESCLGE = this.w_RHESCLGE
    i_oTo.w_RHAUX004 = this.w_RHAUX004
    i_oTo.w_RHAUX005 = this.w_RHAUX005
    i_oTo.w_RHAUX006 = this.w_RHAUX006
    i_oTo.w_RHAUX008 = this.w_RHAUX008
    i_oTo.w_RHAUX009 = this.w_RHAUX009
    i_oTo.w_RHAUX020 = this.w_RHAUX020
    i_oTo.w_RHAUX021 = this.w_RHAUX021
    i_oTo.w_RHAUX022 = this.w_RHAUX022
    i_oTo.w_RHAUX023 = this.w_RHAUX023
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsri_mdh as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 800
  Height = 442
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-06-25"
  HelpContextID=80344681
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=15

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  DATESDRH_IDX = 0
  cFile = "DATESDRH"
  cKeySelect = "RHSERIAL"
  cKeyWhere  = "RHSERIAL=this.w_RHSERIAL"
  cKeyDetail  = "RHSERIAL=this.w_RHSERIAL"
  cKeyWhereODBC = '"RHSERIAL="+cp_ToStrODBC(this.w_RHSERIAL)';

  cKeyDetailWhereODBC = '"RHSERIAL="+cp_ToStrODBC(this.w_RHSERIAL)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"DATESDRH.RHSERIAL="+cp_ToStrODBC(this.w_RHSERIAL)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'DATESDRH.RHDATREG desc,DATESDRH.RHNUMREG desc,DATESDRH.CPROWNUM'
  cPrg = "gsri_mdh"
  cComment = "Dettaglio dati estratti record H"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 20
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_RHSERIAL = space(10)
  w_RHRIFMOV = space(10)
  w_RHDATREG = ctod('  /  /  ')
  o_RHDATREG = ctod('  /  /  ')
  w_RHNUMREG = 0
  w_RHCODESE = space(4)
  w_RHESCLGE = space(1)
  w_RHAUX004 = 0
  w_RHAUX005 = 0
  o_RHAUX005 = 0
  w_RHAUX006 = space(1)
  w_RHAUX008 = 0
  w_RHAUX009 = 0
  w_RHAUX020 = 0
  w_RHAUX021 = 0
  w_RHAUX022 = 0
  w_RHAUX023 = 0
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsri_mdhPag1","gsri_mdh",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='DATESDRH'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.DATESDRH_IDX,5],7]
    this.nPostItConn=i_TableProp[this.DATESDRH_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsri_mdh'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from DATESDRH where RHSERIAL=KeySet.RHSERIAL
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.DATESDRH_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DATESDRH_IDX,2],this.bLoadRecFilter,this.DATESDRH_IDX,"gsri_mdh")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('DATESDRH')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "DATESDRH.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' DATESDRH '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'RHSERIAL',this.w_RHSERIAL  )
      select * from (i_cTable) DATESDRH where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_RHSERIAL = NVL(RHSERIAL,space(10))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'DATESDRH')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_RHRIFMOV = NVL(RHRIFMOV,space(10))
          .w_RHDATREG = NVL(cp_ToDate(RHDATREG),ctod("  /  /  "))
          .w_RHNUMREG = NVL(RHNUMREG,0)
          .w_RHCODESE = NVL(RHCODESE,space(4))
          .w_RHESCLGE = NVL(RHESCLGE,space(1))
          .w_RHAUX004 = NVL(RHAUX004,0)
          .w_RHAUX005 = NVL(RHAUX005,0)
          .w_RHAUX006 = NVL(RHAUX006,space(1))
          .w_RHAUX008 = NVL(RHAUX008,0)
          .w_RHAUX009 = NVL(RHAUX009,0)
          .w_RHAUX020 = NVL(RHAUX020,0)
          .w_RHAUX021 = NVL(RHAUX021,0)
          .w_RHAUX022 = NVL(RHAUX022,0)
          .w_RHAUX023 = NVL(RHAUX023,0)
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page1.oPag.oBtn_2_15.enabled = .oPgFrm.Page1.oPag.oBtn_2_15.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_RHSERIAL=space(10)
      .w_RHRIFMOV=space(10)
      .w_RHDATREG=ctod("  /  /  ")
      .w_RHNUMREG=0
      .w_RHCODESE=space(4)
      .w_RHESCLGE=space(1)
      .w_RHAUX004=0
      .w_RHAUX005=0
      .w_RHAUX006=space(1)
      .w_RHAUX008=0
      .w_RHAUX009=0
      .w_RHAUX020=0
      .w_RHAUX021=0
      .w_RHAUX022=0
      .w_RHAUX023=0
      if .cFunction<>"Filter"
        .DoRTCalc(1,4,.f.)
        .w_RHCODESE = iif(Empty(.w_Rhdatreg),Space(4),Str(year(.w_Rhdatreg),4,0))
        .w_RHESCLGE = 'N'
        .DoRTCalc(7,8,.f.)
        .w_RHAUX006 = iif(.w_Rhaux005=0,' ','3')
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'DATESDRH')
    this.DoRTCalc(10,15,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page1.oPag.oBtn_2_15.enabled = this.oPgFrm.Page1.oPag.oBtn_2_15.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oRHAUX004_2_6.enabled = i_bVal
      .Page1.oPag.oRHAUX005_2_7.enabled = i_bVal
      .Page1.oPag.oRHAUX006_2_8.enabled = i_bVal
      .Page1.oPag.oRHAUX008_2_9.enabled = i_bVal
      .Page1.oPag.oRHAUX009_2_10.enabled = i_bVal
      .Page1.oPag.oRHAUX020_2_11.enabled = i_bVal
      .Page1.oPag.oRHAUX021_2_12.enabled = i_bVal
      .Page1.oPag.oRHAUX022_2_13.enabled = i_bVal
      .Page1.oPag.oRHAUX023_2_14.enabled = i_bVal
      .Page1.oPag.oBtn_2_15.enabled = .Page1.oPag.oBtn_2_15.mCond()
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'DATESDRH',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.DATESDRH_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RHSERIAL,"RHSERIAL",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_RHDATREG D(8);
      ,t_RHNUMREG N(6);
      ,t_RHCODESE C(4);
      ,t_RHESCLGE N(3);
      ,t_RHAUX004 N(18,4);
      ,t_RHAUX005 N(18,4);
      ,t_RHAUX006 N(3);
      ,t_RHAUX008 N(18,4);
      ,t_RHAUX009 N(18,4);
      ,t_RHAUX020 N(18,4);
      ,t_RHAUX021 N(18,4);
      ,t_RHAUX022 N(18,4);
      ,t_RHAUX023 N(18,4);
      ,CPROWNUM N(10);
      ,t_RHRIFMOV C(10);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsri_mdhbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oRHDATREG_2_2.controlsource=this.cTrsName+'.t_RHDATREG'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oRHNUMREG_2_3.controlsource=this.cTrsName+'.t_RHNUMREG'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oRHCODESE_2_4.controlsource=this.cTrsName+'.t_RHCODESE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oRHESCLGE_2_5.controlsource=this.cTrsName+'.t_RHESCLGE'
    this.oPgFRm.Page1.oPag.oRHAUX004_2_6.controlsource=this.cTrsName+'.t_RHAUX004'
    this.oPgFRm.Page1.oPag.oRHAUX005_2_7.controlsource=this.cTrsName+'.t_RHAUX005'
    this.oPgFRm.Page1.oPag.oRHAUX006_2_8.controlsource=this.cTrsName+'.t_RHAUX006'
    this.oPgFRm.Page1.oPag.oRHAUX008_2_9.controlsource=this.cTrsName+'.t_RHAUX008'
    this.oPgFRm.Page1.oPag.oRHAUX009_2_10.controlsource=this.cTrsName+'.t_RHAUX009'
    this.oPgFRm.Page1.oPag.oRHAUX020_2_11.controlsource=this.cTrsName+'.t_RHAUX020'
    this.oPgFRm.Page1.oPag.oRHAUX021_2_12.controlsource=this.cTrsName+'.t_RHAUX021'
    this.oPgFRm.Page1.oPag.oRHAUX022_2_13.controlsource=this.cTrsName+'.t_RHAUX022'
    this.oPgFRm.Page1.oPag.oRHAUX023_2_14.controlsource=this.cTrsName+'.t_RHAUX023'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(88)
    this.AddVLine(155)
    this.AddVLine(215)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRHDATREG_2_2
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.DATESDRH_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DATESDRH_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DATESDRH_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DATESDRH_IDX,2])
      *
      * insert into DATESDRH
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'DATESDRH')
        i_extval=cp_InsertValODBCExtFlds(this,'DATESDRH')
        i_cFldBody=" "+;
                  "(RHSERIAL,RHRIFMOV,RHDATREG,RHNUMREG,RHCODESE"+;
                  ",RHESCLGE,RHAUX004,RHAUX005,RHAUX006,RHAUX008"+;
                  ",RHAUX009,RHAUX020,RHAUX021,RHAUX022,RHAUX023,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_RHSERIAL)+","+cp_ToStrODBC(this.w_RHRIFMOV)+","+cp_ToStrODBC(this.w_RHDATREG)+","+cp_ToStrODBC(this.w_RHNUMREG)+","+cp_ToStrODBC(this.w_RHCODESE)+;
             ","+cp_ToStrODBC(this.w_RHESCLGE)+","+cp_ToStrODBC(this.w_RHAUX004)+","+cp_ToStrODBC(this.w_RHAUX005)+","+cp_ToStrODBC(this.w_RHAUX006)+","+cp_ToStrODBC(this.w_RHAUX008)+;
             ","+cp_ToStrODBC(this.w_RHAUX009)+","+cp_ToStrODBC(this.w_RHAUX020)+","+cp_ToStrODBC(this.w_RHAUX021)+","+cp_ToStrODBC(this.w_RHAUX022)+","+cp_ToStrODBC(this.w_RHAUX023)+;
             ","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'DATESDRH')
        i_extval=cp_InsertValVFPExtFlds(this,'DATESDRH')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'RHSERIAL',this.w_RHSERIAL)
        INSERT INTO (i_cTable) (;
                   RHSERIAL;
                  ,RHRIFMOV;
                  ,RHDATREG;
                  ,RHNUMREG;
                  ,RHCODESE;
                  ,RHESCLGE;
                  ,RHAUX004;
                  ,RHAUX005;
                  ,RHAUX006;
                  ,RHAUX008;
                  ,RHAUX009;
                  ,RHAUX020;
                  ,RHAUX021;
                  ,RHAUX022;
                  ,RHAUX023;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_RHSERIAL;
                  ,this.w_RHRIFMOV;
                  ,this.w_RHDATREG;
                  ,this.w_RHNUMREG;
                  ,this.w_RHCODESE;
                  ,this.w_RHESCLGE;
                  ,this.w_RHAUX004;
                  ,this.w_RHAUX005;
                  ,this.w_RHAUX006;
                  ,this.w_RHAUX008;
                  ,this.w_RHAUX009;
                  ,this.w_RHAUX020;
                  ,this.w_RHAUX021;
                  ,this.w_RHAUX022;
                  ,this.w_RHAUX023;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.DATESDRH_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DATESDRH_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (Not Empty(t_RHDATREG)) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'DATESDRH')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'DATESDRH')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (Not Empty(t_RHDATREG)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update DATESDRH
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'DATESDRH')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " RHRIFMOV="+cp_ToStrODBC(this.w_RHRIFMOV)+;
                     ",RHDATREG="+cp_ToStrODBC(this.w_RHDATREG)+;
                     ",RHNUMREG="+cp_ToStrODBC(this.w_RHNUMREG)+;
                     ",RHCODESE="+cp_ToStrODBC(this.w_RHCODESE)+;
                     ",RHESCLGE="+cp_ToStrODBC(this.w_RHESCLGE)+;
                     ",RHAUX004="+cp_ToStrODBC(this.w_RHAUX004)+;
                     ",RHAUX005="+cp_ToStrODBC(this.w_RHAUX005)+;
                     ",RHAUX006="+cp_ToStrODBC(this.w_RHAUX006)+;
                     ",RHAUX008="+cp_ToStrODBC(this.w_RHAUX008)+;
                     ",RHAUX009="+cp_ToStrODBC(this.w_RHAUX009)+;
                     ",RHAUX020="+cp_ToStrODBC(this.w_RHAUX020)+;
                     ",RHAUX021="+cp_ToStrODBC(this.w_RHAUX021)+;
                     ",RHAUX022="+cp_ToStrODBC(this.w_RHAUX022)+;
                     ",RHAUX023="+cp_ToStrODBC(this.w_RHAUX023)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'DATESDRH')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      RHRIFMOV=this.w_RHRIFMOV;
                     ,RHDATREG=this.w_RHDATREG;
                     ,RHNUMREG=this.w_RHNUMREG;
                     ,RHCODESE=this.w_RHCODESE;
                     ,RHESCLGE=this.w_RHESCLGE;
                     ,RHAUX004=this.w_RHAUX004;
                     ,RHAUX005=this.w_RHAUX005;
                     ,RHAUX006=this.w_RHAUX006;
                     ,RHAUX008=this.w_RHAUX008;
                     ,RHAUX009=this.w_RHAUX009;
                     ,RHAUX020=this.w_RHAUX020;
                     ,RHAUX021=this.w_RHAUX021;
                     ,RHAUX022=this.w_RHAUX022;
                     ,RHAUX023=this.w_RHAUX023;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.DATESDRH_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DATESDRH_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (Not Empty(t_RHDATREG)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete DATESDRH
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (Not Empty(t_RHDATREG)) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DATESDRH_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DATESDRH_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,4,.t.)
        if .o_Rhdatreg<>.w_Rhdatreg
          .w_RHCODESE = iif(Empty(.w_Rhdatreg),Space(4),Str(year(.w_Rhdatreg),4,0))
        endif
        .DoRTCalc(6,8,.t.)
        if .o_RHAUX005<>.w_RHAUX005
          .w_RHAUX006 = iif(.w_Rhaux005=0,' ','3')
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(10,15,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_RHRIFMOV with this.w_RHRIFMOV
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBtn_2_15.enabled =this.oPgFrm.Page1.oPag.oBtn_2_15.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_4.visible=!this.oPgFrm.Page1.oPag.oStr_1_4.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oRHAUX006_2_8.visible=!this.oPgFrm.Page1.oPag.oRHAUX006_2_8.mHide()
    this.oPgFrm.Page1.oPag.oBtn_2_15.visible=!this.oPgFrm.Page1.oPag.oBtn_2_15.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oRHAUX004_2_6.value==this.w_RHAUX004)
      this.oPgFrm.Page1.oPag.oRHAUX004_2_6.value=this.w_RHAUX004
      replace t_RHAUX004 with this.oPgFrm.Page1.oPag.oRHAUX004_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oRHAUX005_2_7.value==this.w_RHAUX005)
      this.oPgFrm.Page1.oPag.oRHAUX005_2_7.value=this.w_RHAUX005
      replace t_RHAUX005 with this.oPgFrm.Page1.oPag.oRHAUX005_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oRHAUX006_2_8.RadioValue()==this.w_RHAUX006)
      this.oPgFrm.Page1.oPag.oRHAUX006_2_8.SetRadio()
      replace t_RHAUX006 with this.oPgFrm.Page1.oPag.oRHAUX006_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oRHAUX008_2_9.value==this.w_RHAUX008)
      this.oPgFrm.Page1.oPag.oRHAUX008_2_9.value=this.w_RHAUX008
      replace t_RHAUX008 with this.oPgFrm.Page1.oPag.oRHAUX008_2_9.value
    endif
    if not(this.oPgFrm.Page1.oPag.oRHAUX009_2_10.value==this.w_RHAUX009)
      this.oPgFrm.Page1.oPag.oRHAUX009_2_10.value=this.w_RHAUX009
      replace t_RHAUX009 with this.oPgFrm.Page1.oPag.oRHAUX009_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oRHAUX020_2_11.value==this.w_RHAUX020)
      this.oPgFrm.Page1.oPag.oRHAUX020_2_11.value=this.w_RHAUX020
      replace t_RHAUX020 with this.oPgFrm.Page1.oPag.oRHAUX020_2_11.value
    endif
    if not(this.oPgFrm.Page1.oPag.oRHAUX021_2_12.value==this.w_RHAUX021)
      this.oPgFrm.Page1.oPag.oRHAUX021_2_12.value=this.w_RHAUX021
      replace t_RHAUX021 with this.oPgFrm.Page1.oPag.oRHAUX021_2_12.value
    endif
    if not(this.oPgFrm.Page1.oPag.oRHAUX022_2_13.value==this.w_RHAUX022)
      this.oPgFrm.Page1.oPag.oRHAUX022_2_13.value=this.w_RHAUX022
      replace t_RHAUX022 with this.oPgFrm.Page1.oPag.oRHAUX022_2_13.value
    endif
    if not(this.oPgFrm.Page1.oPag.oRHAUX023_2_14.value==this.w_RHAUX023)
      this.oPgFrm.Page1.oPag.oRHAUX023_2_14.value=this.w_RHAUX023
      replace t_RHAUX023 with this.oPgFrm.Page1.oPag.oRHAUX023_2_14.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRHDATREG_2_2.value==this.w_RHDATREG)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRHDATREG_2_2.value=this.w_RHDATREG
      replace t_RHDATREG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRHDATREG_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRHNUMREG_2_3.value==this.w_RHNUMREG)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRHNUMREG_2_3.value=this.w_RHNUMREG
      replace t_RHNUMREG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRHNUMREG_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRHCODESE_2_4.value==this.w_RHCODESE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRHCODESE_2_4.value=this.w_RHCODESE
      replace t_RHCODESE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRHCODESE_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRHESCLGE_2_5.RadioValue()==this.w_RHESCLGE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRHESCLGE_2_5.SetRadio()
      replace t_RHESCLGE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRHESCLGE_2_5.value
    endif
    cp_SetControlsValueExtFlds(this,'DATESDRH')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if Not Empty(.w_RHDATREG)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_RHDATREG = this.w_RHDATREG
    this.o_RHAUX005 = this.w_RHAUX005
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(Not Empty(t_RHDATREG))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_RHRIFMOV=space(10)
      .w_RHDATREG=ctod("  /  /  ")
      .w_RHNUMREG=0
      .w_RHCODESE=space(4)
      .w_RHESCLGE=space(1)
      .w_RHAUX004=0
      .w_RHAUX005=0
      .w_RHAUX006=space(1)
      .w_RHAUX008=0
      .w_RHAUX009=0
      .w_RHAUX020=0
      .w_RHAUX021=0
      .w_RHAUX022=0
      .w_RHAUX023=0
      .DoRTCalc(1,4,.f.)
        .w_RHCODESE = iif(Empty(.w_Rhdatreg),Space(4),Str(year(.w_Rhdatreg),4,0))
        .w_RHESCLGE = 'N'
      .DoRTCalc(7,8,.f.)
        .w_RHAUX006 = iif(.w_Rhaux005=0,' ','3')
    endwith
    this.DoRTCalc(10,15,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_RHRIFMOV = t_RHRIFMOV
    this.w_RHDATREG = t_RHDATREG
    this.w_RHNUMREG = t_RHNUMREG
    this.w_RHCODESE = t_RHCODESE
    this.w_RHESCLGE = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRHESCLGE_2_5.RadioValue(.t.)
    this.w_RHAUX004 = t_RHAUX004
    this.w_RHAUX005 = t_RHAUX005
    this.w_RHAUX006 = this.oPgFrm.Page1.oPag.oRHAUX006_2_8.RadioValue(.t.)
    this.w_RHAUX008 = t_RHAUX008
    this.w_RHAUX009 = t_RHAUX009
    this.w_RHAUX020 = t_RHAUX020
    this.w_RHAUX021 = t_RHAUX021
    this.w_RHAUX022 = t_RHAUX022
    this.w_RHAUX023 = t_RHAUX023
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_RHRIFMOV with this.w_RHRIFMOV
    replace t_RHDATREG with this.w_RHDATREG
    replace t_RHNUMREG with this.w_RHNUMREG
    replace t_RHCODESE with this.w_RHCODESE
    replace t_RHESCLGE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRHESCLGE_2_5.ToRadio()
    replace t_RHAUX004 with this.w_RHAUX004
    replace t_RHAUX005 with this.w_RHAUX005
    replace t_RHAUX006 with this.oPgFrm.Page1.oPag.oRHAUX006_2_8.ToRadio()
    replace t_RHAUX008 with this.w_RHAUX008
    replace t_RHAUX009 with this.w_RHAUX009
    replace t_RHAUX020 with this.w_RHAUX020
    replace t_RHAUX021 with this.w_RHAUX021
    replace t_RHAUX022 with this.w_RHAUX022
    replace t_RHAUX023 with this.w_RHAUX023
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsri_mdhPag1 as StdContainer
  Width  = 796
  height = 442
  stdWidth  = 796
  stdheight = 442
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=4, top=13, width=286,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=4,Field1="RHDATREG",Label1="Data reg.",Field2="RHNUMREG",Label2="Reg. n.",Field3="RHCODESE",Label3="Esercizio",Field4="RHESCLGE",Label4="Escl. da gen.",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 235746170

  add object oStr_1_2 as StdString with uid="CSDCKEXDVW",Visible=.t., Left=373, Top=6,;
    Alignment=0, Width=178, Height=18,;
    Caption="Ammontare lordo corrisposto"  ;
  , bGlobalFont=.t.

  add object oStr_1_3 as StdString with uid="PKYNJABTMG",Visible=.t., Left=373, Top=48,;
    Alignment=0, Width=320, Height=18,;
    Caption="Somme non soggette"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="GJUYRIFSFR",Visible=.t., Left=373, Top=89,;
    Alignment=0, Width=206, Height=18,;
    Caption="Codice"  ;
  , bGlobalFont=.t.

  func oStr_1_4.mHide()
    with this.Parent.oContained
      return (.w_RHAUX005=0)
    endwith
  endfunc

  add object oStr_1_5 as StdString with uid="LCHPQYZRZQ",Visible=.t., Left=373, Top=133,;
    Alignment=0, Width=107, Height=18,;
    Caption="Imponibile"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="TQNGSVODAV",Visible=.t., Left=373, Top=173,;
    Alignment=0, Width=204, Height=18,;
    Caption="Ritenute a titolo di acconto"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="XXZQZMSONP",Visible=.t., Left=373, Top=215,;
    Alignment=0, Width=320, Height=18,;
    Caption="Contributi previdenziali a carico del soggetto erogante"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="ILHZDOSWGN",Visible=.t., Left=373, Top=256,;
    Alignment=0, Width=320, Height=18,;
    Caption="Contributi previdenziali a carico del percipiente"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="HTLGXFAXXA",Visible=.t., Left=373, Top=297,;
    Alignment=0, Width=142, Height=18,;
    Caption="Spese rimborsate"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="DOGVEWZOMF",Visible=.t., Left=373, Top=338,;
    Alignment=0, Width=284, Height=18,;
    Caption="Ritenute rimborsate"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-6,top=32,;
    width=282+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*20*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-5,top=33,width=281+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*20*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oRHAUX004_2_6.Refresh()
      this.Parent.oRHAUX005_2_7.Refresh()
      this.Parent.oRHAUX006_2_8.Refresh()
      this.Parent.oRHAUX008_2_9.Refresh()
      this.Parent.oRHAUX009_2_10.Refresh()
      this.Parent.oRHAUX020_2_11.Refresh()
      this.Parent.oRHAUX021_2_12.Refresh()
      this.Parent.oRHAUX022_2_13.Refresh()
      this.Parent.oRHAUX023_2_14.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oRHAUX004_2_6 as StdTrsField with uid="MAVFPGAHCL",rtseq=7,rtrep=.t.,;
    cFormVar="w_RHAUX004",value=0,;
    ToolTipText = "Ammontare lordo corrisposto",;
    HelpContextID = 17786698,;
    cTotal="", bFixedPos=.t., cQueryName = "RHAUX004",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=373, Top=25, cSayPict=[v_PV(80)], cGetPict=[v_GV(80)]

  add object oRHAUX005_2_7 as StdTrsField with uid="CKMDAWEWHP",rtseq=8,rtrep=.t.,;
    cFormVar="w_RHAUX005",value=0,;
    ToolTipText = "Somme non soggette",;
    HelpContextID = 17786699,;
    cTotal="", bFixedPos=.t., cQueryName = "RHAUX005",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=373, Top=66, cSayPict=[v_PV(80)], cGetPict=[v_GV(80)]

  add object oRHAUX006_2_8 as StdTrsCombo with uid="YWSPEEKKCZ",rtrep=.t.,;
    cFormVar="w_RHAUX006", RowSource=""+"1 - Compensi docenti/ricercatori Legge n. 2 del 28 gennaio 2009,"+"2 - Lavoratori con beneficio fiscale ex art. 3 Legge 30 dicembre 2010/238,"+"3 - Erogazione di altri redditi non soggetti a ritenuta" , ;
    ToolTipText = "Codice somme non soggette",;
    HelpContextID = 17786700,;
    Height=26, Width=404, Left=373, Top=107,;
    cTotal="", cQueryName = "RHAUX006",;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oRHAUX006_2_8.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..RHAUX006,&i_cF..t_RHAUX006),this.value)
    return(iif(xVal =1,'1',;
    iif(xVal =2,'2',;
    iif(xVal =3,'3',;
    ' '))))
  endfunc
  func oRHAUX006_2_8.GetRadio()
    this.Parent.oContained.w_RHAUX006 = this.RadioValue()
    return .t.
  endfunc

  func oRHAUX006_2_8.ToRadio()
    this.Parent.oContained.w_RHAUX006=trim(this.Parent.oContained.w_RHAUX006)
    return(;
      iif(this.Parent.oContained.w_RHAUX006=='1',1,;
      iif(this.Parent.oContained.w_RHAUX006=='2',2,;
      iif(this.Parent.oContained.w_RHAUX006=='3',3,;
      0))))
  endfunc

  func oRHAUX006_2_8.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oRHAUX006_2_8.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_RHAUX005=0)
    endwith
    endif
  endfunc

  add object oRHAUX008_2_9 as StdTrsField with uid="FWDDCVIJSD",rtseq=10,rtrep=.t.,;
    cFormVar="w_RHAUX008",value=0,;
    ToolTipText = "Imponibile",;
    HelpContextID = 17786702,;
    cTotal="", bFixedPos=.t., cQueryName = "RHAUX008",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=373, Top=151, cSayPict=[v_PV(80)], cGetPict=[v_GV(80)]

  add object oRHAUX009_2_10 as StdTrsField with uid="SLXMQCWFTN",rtseq=11,rtrep=.t.,;
    cFormVar="w_RHAUX009",value=0,;
    ToolTipText = "Ritenute a titolo di acconto",;
    HelpContextID = 17786703,;
    cTotal="", bFixedPos=.t., cQueryName = "RHAUX009",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=373, Top=192, cSayPict=[v_PV(80)], cGetPict=[v_GV(80)]

  add object oRHAUX020_2_11 as StdTrsField with uid="MOYIUSGWJU",rtseq=12,rtrep=.t.,;
    cFormVar="w_RHAUX020",value=0,;
    ToolTipText = "Contributi previdenziali a carico del soggetto erogante",;
    HelpContextID = 17786694,;
    cTotal="", bFixedPos=.t., cQueryName = "RHAUX020",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=373, Top=233, cSayPict=[v_PV(80)], cGetPict=[v_GV(80)]

  add object oRHAUX021_2_12 as StdTrsField with uid="IWGEADRIDN",rtseq=13,rtrep=.t.,;
    cFormVar="w_RHAUX021",value=0,;
    ToolTipText = "Contributi previdenziali a carico del percipiente",;
    HelpContextID = 17786695,;
    cTotal="", bFixedPos=.t., cQueryName = "RHAUX021",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=373, Top=274, cSayPict=[v_PV(80)], cGetPict=[v_GV(80)]

  add object oRHAUX022_2_13 as StdTrsField with uid="EPEWIQMITO",rtseq=14,rtrep=.t.,;
    cFormVar="w_RHAUX022",value=0,;
    ToolTipText = "Spese rimborsate",;
    HelpContextID = 17786696,;
    cTotal="", bFixedPos=.t., cQueryName = "RHAUX022",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=373, Top=315, cSayPict=[v_PV(80)], cGetPict=[v_GV(80)]

  add object oRHAUX023_2_14 as StdTrsField with uid="COGKTSSVFY",rtseq=15,rtrep=.t.,;
    cFormVar="w_RHAUX023",value=0,;
    ToolTipText = "Ritenute rimborsate",;
    HelpContextID = 17786697,;
    cTotal="", bFixedPos=.t., cQueryName = "RHAUX023",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=373, Top=356, cSayPict=[v_PV(80)], cGetPict=[v_GV(80)]

  add object oBtn_2_15 as StdButton with uid="RCCGCSIAEP",width=48,height=45,;
   left=308, top=34,;
    CpPicture="bmp\VERIFICA.bmp", caption="", nPag=2;
    , ToolTipText = "Visualizza movimenti ritenute";
    , HelpContextID = 9706358;
    , Caption='M\<ov.Rite',tabstop=.f.;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_15.Click()
      with this.Parent.oContained
        Gsri_Bdh(this.Parent.oContained,"M")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_15.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_RHRIFMOV))
    endwith
  endfunc

  func oBtn_2_15.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_RHRIFMOV))
    endwith
   endif
  endfunc

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsri_mdhBodyRow as CPBodyRowCnt
  Width=272
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oRHDATREG_2_2 as StdTrsField with uid="XUVPODZBXR",rtseq=3,rtrep=.t.,;
    cFormVar="w_RHDATREG",value=ctod("  /  /  "),;
    ToolTipText = "Data registrazione",;
    HelpContextID = 222587043,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=83, Left=-2, Top=0

  add object oRHNUMREG_2_3 as StdTrsField with uid="IQIUBETHSG",rtseq=4,rtrep=.t.,;
    cFormVar="w_RHNUMREG",value=0,;
    ToolTipText = "Numero registrazione",;
    HelpContextID = 228575395,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=62, Left=84, Top=0, cSayPict=["999999"], cGetPict=["999999"]

  add object oRHCODESE_2_4 as StdTrsField with uid="UWMUJGOKCH",rtseq=5,rtrep=.t.,;
    cFormVar="w_RHCODESE",value=space(4),;
    ToolTipText = "Codice esercizio di riferimento",;
    HelpContextID = 80316251,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=55, Left=152, Top=0, InputMask=replicate('X',4)

  add object oRHESCLGE_2_5 as StdTrsCombo with uid="PUNQMEEBJY",rtrep=.t.,;
    cFormVar="w_RHESCLGE", RowSource=""+"Si,"+"No" , ;
    ToolTipText = "Escluso da generazione",;
    HelpContextID = 71456933,;
    Height=22, Width=56, Left=211, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oRHESCLGE_2_5.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..RHESCLGE,&i_cF..t_RHESCLGE),this.value)
    return(iif(xVal =1,'S',;
    iif(xVal =2,'N',;
    space(1))))
  endfunc
  func oRHESCLGE_2_5.GetRadio()
    this.Parent.oContained.w_RHESCLGE = this.RadioValue()
    return .t.
  endfunc

  func oRHESCLGE_2_5.ToRadio()
    this.Parent.oContained.w_RHESCLGE=trim(this.Parent.oContained.w_RHESCLGE)
    return(;
      iif(this.Parent.oContained.w_RHESCLGE=='S',1,;
      iif(this.Parent.oContained.w_RHESCLGE=='N',2,;
      0)))
  endfunc

  func oRHESCLGE_2_5.SetRadio()
    this.value=this.ToRadio()
  endfunc
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oRHDATREG_2_2.When()
    return(.t.)
  proc oRHDATREG_2_2.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oRHDATREG_2_2.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=19
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsri_mdh','DATESDRH','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".RHSERIAL=DATESDRH.RHSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
