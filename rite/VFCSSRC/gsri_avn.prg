* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_avn                                                        *
*              Versamenti periodici previdenziali                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [146] [VRS_124]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-05-15                                                      *
* Last revis.: 2010-06-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsri_avn"))

* --- Class definition
define class tgsri_avn as StdForm
  Top    = 11
  Left   = 8

  * --- Standard Properties
  Width  = 684
  Height = 387+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2010-06-18"
  HelpContextID=209062295
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=60

  * --- Constant Properties
  VEN_RITE_IDX = 0
  COC_MAST_IDX = 0
  ESERCIZI_IDX = 0
  TRI_BUTI_IDX = 0
  VALUTE_IDX = 0
  BAN_CHE_IDX = 0
  cFile = "VEN_RITE"
  cKeySelect = "VPSERIAL"
  cKeyWhere  = "VPSERIAL=this.w_VPSERIAL"
  cKeyWhereODBC = '"VPSERIAL="+cp_ToStrODBC(this.w_VPSERIAL)';

  cKeyWhereODBCqualified = '"VEN_RITE.VPSERIAL="+cp_ToStrODBC(this.w_VPSERIAL)';

  cPrg = "gsri_avn"
  cComment = "Versamenti periodici previdenziali"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_VPSERIAL = space(10)
  w_CODAZI = space(5)
  w_VPCODESE = space(4)
  o_VPCODESE = space(4)
  w_VPNUMREG = 0
  w_VALNAZ = space(3)
  w_DECTOT = 0
  w_VPDATREG = ctod('  /  /  ')
  o_VPDATREG = ctod('  /  /  ')
  w_VPDESCRI = space(40)
  w_INIDATA = ctod('  /  /  ')
  w_FINDATA = ctod('  /  /  ')
  w_VPDATINI = ctod('  /  /  ')
  w_VPDATFIN = ctod('  /  /  ')
  w_VPCODTRI = space(5)
  w_DESTRI = space(35)
  w_VPCODTRI = space(5)
  w_VPSTATUS = space(1)
  o_VPSTATUS = space(1)
  w_VPNUMCOR = space(15)
  w_DESCOR = space(35)
  w_VPCODBAN = space(10)
  w_DESBAN = space(50)
  w_VPNUMVER = 0
  w_VPDATVER = ctod('  /  /  ')
  w_VPCODABI = space(5)
  w_VPCODCAB = space(5)
  w_VPCODCON = space(3)
  w_VPIMPVER = 0
  w_VPIMPINT = 0
  w_VPVALVER = space(3)
  o_VPVALVER = space(3)
  w_CALCPICT = 0
  w_FILPAR = space(10)
  w_OBTEST = ctod('  /  /  ')
  w_DESVAL = space(35)
  w_DECTOP = 0
  w_CALCPIC2 = 0
  w_TOTAL3 = 0
  w_VALUTA = space(3)
  w_DESTRI = space(35)
  w_FLACON = space(1)
  w_GIAVER = space(1)
  w_OREP = space(50)
  w_OQRY = space(50)
  w_DATA1 = ctod('  /  /  ')
  w_DATA2 = ctod('  /  /  ')
  w_CODTRI = space(5)
  w_NUMCOR = space(15)
  w_STATO = space(1)
  w_NREGIS = 0
  w_NDISTI = space(10)
  w_FORNITORE = space(15)
  w_divisa = space(3)
  w_decimi = 0
  w_BOT = space(10)
  w_SELEZI = space(1)
  w_FLSELE = 0
  w_TOTAL1 = 0
  w_TOTAL2 = 0
  w_TOTAL3 = 0
  w_SIMVAL = space(5)
  w_VPVERF24 = space(10)
  w_CAMBIO = 0

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_VPSERIAL = this.W_VPSERIAL
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_VPCODESE = this.W_VPCODESE
  op_VPNUMREG = this.W_VPNUMREG
  w_CalcZoom = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'VEN_RITE','gsri_avn')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsri_avnPag1","gsri_avn",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Distinta")
      .Pages(1).HelpContextID = 25274263
      .Pages(2).addobject("oPag","tgsri_avnPag2","gsri_avn",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Dettaglio")
      .Pages(2).HelpContextID = 100550001
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
    proc Init()
      this.w_CalcZoom = this.oPgFrm.Pages(2).oPag.CalcZoom
      DoDefault()
    proc Destroy()
      this.w_CalcZoom = .NULL.
      DoDefault()

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='COC_MAST'
    this.cWorkTables[2]='ESERCIZI'
    this.cWorkTables[3]='TRI_BUTI'
    this.cWorkTables[4]='VALUTE'
    this.cWorkTables[5]='BAN_CHE'
    this.cWorkTables[6]='VEN_RITE'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(6))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.VEN_RITE_IDX,5],7]
    this.nPostItConn=i_TableProp[this.VEN_RITE_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_VPSERIAL = NVL(VPSERIAL,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_14_joined
    link_1_14_joined=.f.
    local link_1_16_joined
    link_1_16_joined=.f.
    local link_1_18_joined
    link_1_18_joined=.f.
    local link_1_20_joined
    link_1_20_joined=.f.
    local link_1_29_joined
    link_1_29_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from VEN_RITE where VPSERIAL=KeySet.VPSERIAL
    *
    i_nConn = i_TableProp[this.VEN_RITE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.VEN_RITE_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('VEN_RITE')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "VEN_RITE.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' VEN_RITE '
      link_1_14_joined=this.AddJoinedLink_1_14(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_16_joined=this.AddJoinedLink_1_16(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_18_joined=this.AddJoinedLink_1_18(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_20_joined=this.AddJoinedLink_1_20(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_29_joined=this.AddJoinedLink_1_29(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'VPSERIAL',this.w_VPSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_VALNAZ = space(3)
        .w_DECTOT = 0
        .w_INIDATA = ctod("  /  /  ")
        .w_FINDATA = ctod("  /  /  ")
        .w_DESTRI = space(35)
        .w_DESCOR = space(35)
        .w_DESBAN = space(50)
        .w_OBTEST = i_datsys
        .w_DESVAL = space(35)
        .w_DECTOP = 0
        .w_TOTAL3 = 0
        .w_DESTRI = space(35)
        .w_FLACON = space(1)
        .w_GIAVER = '3'
        .w_OREP = space(50)
        .w_OQRY = space(50)
        .w_DATA1 = ctod("  /  /  ")
        .w_DATA2 = ctod("  /  /  ")
        .w_CODTRI = space(5)
        .w_NUMCOR = space(15)
        .w_FORNITORE = space(15)
        .w_BOT = space(10)
        .w_SELEZI = 'D'
        .w_FLSELE = 0
        .w_TOTAL1 = 0
        .w_TOTAL2 = 0
        .w_TOTAL3 = 0
        .w_SIMVAL = space(5)
        .w_VPSERIAL = NVL(VPSERIAL,space(10))
        .op_VPSERIAL = .w_VPSERIAL
        .w_CODAZI = i_CODAZI
        .w_VPCODESE = NVL(VPCODESE,space(4))
        .op_VPCODESE = .w_VPCODESE
          .link_1_3('Load')
        .w_VPNUMREG = NVL(VPNUMREG,0)
        .op_VPNUMREG = .w_VPNUMREG
          .link_1_5('Load')
        .w_VPDATREG = NVL(cp_ToDate(VPDATREG),ctod("  /  /  "))
        .w_VPDESCRI = NVL(VPDESCRI,space(40))
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
        .w_VPDATINI = NVL(cp_ToDate(VPDATINI),ctod("  /  /  "))
        .w_VPDATFIN = NVL(cp_ToDate(VPDATFIN),ctod("  /  /  "))
        .w_VPCODTRI = NVL(VPCODTRI,space(5))
          if link_1_14_joined
            this.w_VPCODTRI = NVL(TRCODTRI114,NVL(this.w_VPCODTRI,space(5)))
            this.w_DESTRI = NVL(TRDESTRI114,space(35))
          else
          .link_1_14('Load')
          endif
        .w_VPCODTRI = NVL(VPCODTRI,space(5))
          if link_1_16_joined
            this.w_VPCODTRI = NVL(TRCODTRI116,NVL(this.w_VPCODTRI,space(5)))
            this.w_DESTRI = NVL(TRDESTRI116,space(35))
            this.w_FLACON = NVL(TRFLACON116,space(1))
          else
          .link_1_16('Load')
          endif
        .w_VPSTATUS = NVL(VPSTATUS,space(1))
        .w_VPNUMCOR = NVL(VPNUMCOR,space(15))
          if link_1_18_joined
            this.w_VPNUMCOR = NVL(BACODBAN118,NVL(this.w_VPNUMCOR,space(15)))
            this.w_DESCOR = NVL(BADESCRI118,space(35))
            this.w_VPCODABI = NVL(BACODABI118,space(5))
            this.w_VPCODCAB = NVL(BACODCAB118,space(5))
          else
          .link_1_18('Load')
          endif
        .w_VPCODBAN = NVL(VPCODBAN,space(10))
          if link_1_20_joined
            this.w_VPCODBAN = NVL(BACODBAN120,NVL(this.w_VPCODBAN,space(10)))
            this.w_VPCODABI = NVL(BACODABI120,space(5))
            this.w_VPCODCAB = NVL(BACODCAB120,space(5))
            this.w_DESBAN = NVL(BADESBAN120,space(50))
          else
          .link_1_20('Load')
          endif
        .w_VPNUMVER = NVL(VPNUMVER,0)
        .w_VPDATVER = NVL(cp_ToDate(VPDATVER),ctod("  /  /  "))
        .w_VPCODABI = NVL(VPCODABI,space(5))
        .w_VPCODCAB = NVL(VPCODCAB,space(5))
        .w_VPCODCON = NVL(VPCODCON,space(3))
        .w_VPIMPVER = NVL(VPIMPVER,0)
        .w_VPIMPINT = NVL(VPIMPINT,0)
        .w_VPVALVER = NVL(VPVALVER,space(3))
          if link_1_29_joined
            this.w_VPVALVER = NVL(VACODVAL129,NVL(this.w_VPVALVER,space(3)))
            this.w_DECTOP = NVL(VADECTOT129,0)
            this.w_SIMVAL = NVL(VASIMVAL129,space(5))
          else
          .link_1_29('Load')
          endif
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .w_FILPAR = SPACE(10)
        .w_CALCPIC2 = DEFPIP(.w_DECTOP)
        .w_VALUTA = .w_VPVALVER
        .w_STATO = .w_VPSTATUS
        .w_NREGIS = .w_VPNUMREG
        .w_NDISTI = .w_VPSERIAL
        .w_divisa = .w_VPVALVER
        .w_decimi = .w_DECTOP
        .oPgFrm.Page2.oPag.CalcZoom.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_6.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_7.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_9.Calculate()
        .w_VPVERF24 = NVL(VPVERF24,space(10))
        .w_CAMBIO = GETCAM(.w_VPVALVER, .w_VPDATFIN)
        .oPgFrm.Page1.oPag.oObj_1_80.Calculate()
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'VEN_RITE')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_41.enabled = this.oPgFrm.Page1.oPag.oBtn_1_41.mCond()
      this.oPgFrm.Page2.oPag.oBtn_2_3.enabled = this.oPgFrm.Page2.oPag.oBtn_2_3.mCond()
      this.oPgFrm.Page2.oPag.oBtn_2_4.enabled = this.oPgFrm.Page2.oPag.oBtn_2_4.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_VPSERIAL = space(10)
      .w_CODAZI = space(5)
      .w_VPCODESE = space(4)
      .w_VPNUMREG = 0
      .w_VALNAZ = space(3)
      .w_DECTOT = 0
      .w_VPDATREG = ctod("  /  /  ")
      .w_VPDESCRI = space(40)
      .w_INIDATA = ctod("  /  /  ")
      .w_FINDATA = ctod("  /  /  ")
      .w_VPDATINI = ctod("  /  /  ")
      .w_VPDATFIN = ctod("  /  /  ")
      .w_VPCODTRI = space(5)
      .w_DESTRI = space(35)
      .w_VPCODTRI = space(5)
      .w_VPSTATUS = space(1)
      .w_VPNUMCOR = space(15)
      .w_DESCOR = space(35)
      .w_VPCODBAN = space(10)
      .w_DESBAN = space(50)
      .w_VPNUMVER = 0
      .w_VPDATVER = ctod("  /  /  ")
      .w_VPCODABI = space(5)
      .w_VPCODCAB = space(5)
      .w_VPCODCON = space(3)
      .w_VPIMPVER = 0
      .w_VPIMPINT = 0
      .w_VPVALVER = space(3)
      .w_CALCPICT = 0
      .w_FILPAR = space(10)
      .w_OBTEST = ctod("  /  /  ")
      .w_DESVAL = space(35)
      .w_DECTOP = 0
      .w_CALCPIC2 = 0
      .w_TOTAL3 = 0
      .w_VALUTA = space(3)
      .w_DESTRI = space(35)
      .w_FLACON = space(1)
      .w_GIAVER = space(1)
      .w_OREP = space(50)
      .w_OQRY = space(50)
      .w_DATA1 = ctod("  /  /  ")
      .w_DATA2 = ctod("  /  /  ")
      .w_CODTRI = space(5)
      .w_NUMCOR = space(15)
      .w_STATO = space(1)
      .w_NREGIS = 0
      .w_NDISTI = space(10)
      .w_FORNITORE = space(15)
      .w_divisa = space(3)
      .w_decimi = 0
      .w_BOT = space(10)
      .w_SELEZI = space(1)
      .w_FLSELE = 0
      .w_TOTAL1 = 0
      .w_TOTAL2 = 0
      .w_TOTAL3 = 0
      .w_SIMVAL = space(5)
      .w_VPVERF24 = space(10)
      .w_CAMBIO = 0
      if .cFunction<>"Filter"
          .DoRTCalc(1,1,.f.)
        .w_CODAZI = i_CODAZI
        .w_VPCODESE = g_CODESE
        .DoRTCalc(3,3,.f.)
          if not(empty(.w_VPCODESE))
          .link_1_3('Full')
          endif
        .DoRTCalc(4,5,.f.)
          if not(empty(.w_VALNAZ))
          .link_1_5('Full')
          endif
          .DoRTCalc(6,6,.f.)
        .w_VPDATREG = i_datsys
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
          .DoRTCalc(8,10,.f.)
        .w_VPDATINI = .w_INIDATA
        .w_VPDATFIN = .w_FINDATA
        .DoRTCalc(13,13,.f.)
          if not(empty(.w_VPCODTRI))
          .link_1_14('Full')
          endif
        .DoRTCalc(14,15,.f.)
          if not(empty(.w_VPCODTRI))
          .link_1_16('Full')
          endif
        .w_VPSTATUS = 'P'
        .DoRTCalc(17,17,.f.)
          if not(empty(.w_VPNUMCOR))
          .link_1_18('Full')
          endif
        .DoRTCalc(18,19,.f.)
          if not(empty(.w_VPCODBAN))
          .link_1_20('Full')
          endif
          .DoRTCalc(20,21,.f.)
        .w_VPDATVER = IIF(.w_VPSTATUS='D', .w_VPDATREG, cp_CharToDate('  -  -  '))
          .DoRTCalc(23,27,.f.)
        .w_VPVALVER = G_PERVAL
        .DoRTCalc(28,28,.f.)
          if not(empty(.w_VPVALVER))
          .link_1_29('Full')
          endif
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .w_FILPAR = SPACE(10)
        .w_OBTEST = i_datsys
          .DoRTCalc(32,33,.f.)
        .w_CALCPIC2 = DEFPIP(.w_DECTOP)
        .w_TOTAL3 = 0
        .w_VALUTA = .w_VPVALVER
          .DoRTCalc(37,38,.f.)
        .w_GIAVER = '3'
          .DoRTCalc(40,45,.f.)
        .w_STATO = .w_VPSTATUS
        .w_NREGIS = .w_VPNUMREG
        .w_NDISTI = .w_VPSERIAL
          .DoRTCalc(49,49,.f.)
        .w_divisa = .w_VPVALVER
        .w_decimi = .w_DECTOP
        .oPgFrm.Page2.oPag.CalcZoom.Calculate()
          .DoRTCalc(52,52,.f.)
        .w_SELEZI = 'D'
        .w_FLSELE = 0
        .oPgFrm.Page2.oPag.oObj_2_6.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_7.Calculate()
        .w_TOTAL1 = 0
        .oPgFrm.Page2.oPag.oObj_2_9.Calculate()
        .w_TOTAL2 = 0
        .w_TOTAL3 = 0
          .DoRTCalc(58,59,.f.)
        .w_CAMBIO = GETCAM(.w_VPVALVER, .w_VPDATFIN)
        .oPgFrm.Page1.oPag.oObj_1_80.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'VEN_RITE')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_41.enabled = this.oPgFrm.Page1.oPag.oBtn_1_41.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_3.enabled = this.oPgFrm.Page2.oPag.oBtn_2_3.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_4.enabled = this.oPgFrm.Page2.oPag.oBtn_2_4.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.VEN_RITE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.VEN_RITE_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"SEVERINP","i_codazi,w_VPSERIAL")
      cp_AskTableProg(this,i_nConn,"PRVERINP","i_codazi,w_VPCODESE,w_VPNUMREG")
      .op_codazi = .w_codazi
      .op_VPSERIAL = .w_VPSERIAL
      .op_codazi = .w_codazi
      .op_VPCODESE = .w_VPCODESE
      .op_VPNUMREG = .w_VPNUMREG
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oVPCODESE_1_3.enabled = i_bVal
      .Page1.oPag.oVPNUMREG_1_4.enabled = i_bVal
      .Page1.oPag.oVPDATREG_1_7.enabled = i_bVal
      .Page1.oPag.oVPDESCRI_1_8.enabled = i_bVal
      .Page1.oPag.oVPDATINI_1_12.enabled = i_bVal
      .Page1.oPag.oVPDATFIN_1_13.enabled = i_bVal
      .Page1.oPag.oVPCODTRI_1_16.enabled = i_bVal
      .Page1.oPag.oVPSTATUS_1_17.enabled = i_bVal
      .Page1.oPag.oVPNUMCOR_1_18.enabled = i_bVal
      .Page1.oPag.oVPCODBAN_1_20.enabled = i_bVal
      .Page1.oPag.oVPNUMVER_1_22.enabled = i_bVal
      .Page1.oPag.oVPDATVER_1_23.enabled = i_bVal
      .Page1.oPag.oVPCODABI_1_24.enabled = i_bVal
      .Page1.oPag.oVPCODCAB_1_25.enabled = i_bVal
      .Page1.oPag.oVPCODCON_1_26.enabled = i_bVal
      .Page1.oPag.oVPIMPVER_1_27.enabled = i_bVal
      .Page1.oPag.oVPIMPINT_1_28.enabled = i_bVal
      .Page1.oPag.oVPVALVER_1_29.enabled = i_bVal
      .Page2.oPag.oSELEZI_2_2.enabled_(i_bVal)
      .Page1.oPag.oBtn_1_41.enabled = .Page1.oPag.oBtn_1_41.mCond()
      .Page1.oPag.oBtn_1_42.enabled = i_bVal
      .Page2.oPag.oBtn_2_3.enabled = .Page2.oPag.oBtn_2_3.mCond()
      .Page2.oPag.oBtn_2_4.enabled = .Page2.oPag.oBtn_2_4.mCond()
      .Page1.oPag.oObj_1_11.enabled = i_bVal
      .Page2.oPag.CalcZoom.enabled = i_bVal
      .Page2.oPag.oObj_2_6.enabled = i_bVal
      .Page2.oPag.oObj_2_7.enabled = i_bVal
      .Page2.oPag.oObj_2_9.enabled = i_bVal
      .Page1.oPag.oObj_1_80.enabled = i_bVal
      if i_cOp = "Query"
        .Page1.oPag.oVPNUMREG_1_4.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'VEN_RITE',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.VEN_RITE_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VPSERIAL,"VPSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VPCODESE,"VPCODESE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VPNUMREG,"VPNUMREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VPDATREG,"VPDATREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VPDESCRI,"VPDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VPDATINI,"VPDATINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VPDATFIN,"VPDATFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VPCODTRI,"VPCODTRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VPCODTRI,"VPCODTRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VPSTATUS,"VPSTATUS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VPNUMCOR,"VPNUMCOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VPCODBAN,"VPCODBAN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VPNUMVER,"VPNUMVER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VPDATVER,"VPDATVER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VPCODABI,"VPCODABI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VPCODCAB,"VPCODCAB",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VPCODCON,"VPCODCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VPIMPVER,"VPIMPVER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VPIMPINT,"VPIMPINT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VPVALVER,"VPVALVER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VPVERF24,"VPVERF24",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.VEN_RITE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.VEN_RITE_IDX,2])
    i_lTable = "VEN_RITE"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.VEN_RITE_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSRI_SIN with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.VEN_RITE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.VEN_RITE_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.VEN_RITE_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"SEVERINP","i_codazi,w_VPSERIAL")
          cp_NextTableProg(this,i_nConn,"PRVERINP","i_codazi,w_VPCODESE,w_VPNUMREG")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into VEN_RITE
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'VEN_RITE')
        i_extval=cp_InsertValODBCExtFlds(this,'VEN_RITE')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(VPSERIAL,VPCODESE,VPNUMREG,VPDATREG,VPDESCRI"+;
                  ",VPDATINI,VPDATFIN,VPCODTRI,VPSTATUS,VPNUMCOR"+;
                  ",VPCODBAN,VPNUMVER,VPDATVER,VPCODABI,VPCODCAB"+;
                  ",VPCODCON,VPIMPVER,VPIMPINT,VPVALVER,VPVERF24 "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_VPSERIAL)+;
                  ","+cp_ToStrODBCNull(this.w_VPCODESE)+;
                  ","+cp_ToStrODBC(this.w_VPNUMREG)+;
                  ","+cp_ToStrODBC(this.w_VPDATREG)+;
                  ","+cp_ToStrODBC(this.w_VPDESCRI)+;
                  ","+cp_ToStrODBC(this.w_VPDATINI)+;
                  ","+cp_ToStrODBC(this.w_VPDATFIN)+;
                  ","+cp_ToStrODBCNull(this.w_VPCODTRI)+;
                  ","+cp_ToStrODBC(this.w_VPSTATUS)+;
                  ","+cp_ToStrODBCNull(this.w_VPNUMCOR)+;
                  ","+cp_ToStrODBCNull(this.w_VPCODBAN)+;
                  ","+cp_ToStrODBC(this.w_VPNUMVER)+;
                  ","+cp_ToStrODBC(this.w_VPDATVER)+;
                  ","+cp_ToStrODBC(this.w_VPCODABI)+;
                  ","+cp_ToStrODBC(this.w_VPCODCAB)+;
                  ","+cp_ToStrODBC(this.w_VPCODCON)+;
                  ","+cp_ToStrODBC(this.w_VPIMPVER)+;
                  ","+cp_ToStrODBC(this.w_VPIMPINT)+;
                  ","+cp_ToStrODBCNull(this.w_VPVALVER)+;
                  ","+cp_ToStrODBC(this.w_VPVERF24)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'VEN_RITE')
        i_extval=cp_InsertValVFPExtFlds(this,'VEN_RITE')
        cp_CheckDeletedKey(i_cTable,0,'VPSERIAL',this.w_VPSERIAL)
        INSERT INTO (i_cTable);
              (VPSERIAL,VPCODESE,VPNUMREG,VPDATREG,VPDESCRI,VPDATINI,VPDATFIN,VPCODTRI,VPSTATUS,VPNUMCOR,VPCODBAN,VPNUMVER,VPDATVER,VPCODABI,VPCODCAB,VPCODCON,VPIMPVER,VPIMPINT,VPVALVER,VPVERF24  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_VPSERIAL;
                  ,this.w_VPCODESE;
                  ,this.w_VPNUMREG;
                  ,this.w_VPDATREG;
                  ,this.w_VPDESCRI;
                  ,this.w_VPDATINI;
                  ,this.w_VPDATFIN;
                  ,this.w_VPCODTRI;
                  ,this.w_VPSTATUS;
                  ,this.w_VPNUMCOR;
                  ,this.w_VPCODBAN;
                  ,this.w_VPNUMVER;
                  ,this.w_VPDATVER;
                  ,this.w_VPCODABI;
                  ,this.w_VPCODCAB;
                  ,this.w_VPCODCON;
                  ,this.w_VPIMPVER;
                  ,this.w_VPIMPINT;
                  ,this.w_VPVALVER;
                  ,this.w_VPVERF24;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.VEN_RITE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.VEN_RITE_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.VEN_RITE_IDX,i_nConn)
      *
      * update VEN_RITE
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'VEN_RITE')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " VPCODESE="+cp_ToStrODBCNull(this.w_VPCODESE)+;
             ",VPNUMREG="+cp_ToStrODBC(this.w_VPNUMREG)+;
             ",VPDATREG="+cp_ToStrODBC(this.w_VPDATREG)+;
             ",VPDESCRI="+cp_ToStrODBC(this.w_VPDESCRI)+;
             ",VPDATINI="+cp_ToStrODBC(this.w_VPDATINI)+;
             ",VPDATFIN="+cp_ToStrODBC(this.w_VPDATFIN)+;
             ",VPCODTRI="+cp_ToStrODBCNull(this.w_VPCODTRI)+;
             ",VPSTATUS="+cp_ToStrODBC(this.w_VPSTATUS)+;
             ",VPNUMCOR="+cp_ToStrODBCNull(this.w_VPNUMCOR)+;
             ",VPCODBAN="+cp_ToStrODBCNull(this.w_VPCODBAN)+;
             ",VPNUMVER="+cp_ToStrODBC(this.w_VPNUMVER)+;
             ",VPDATVER="+cp_ToStrODBC(this.w_VPDATVER)+;
             ",VPCODABI="+cp_ToStrODBC(this.w_VPCODABI)+;
             ",VPCODCAB="+cp_ToStrODBC(this.w_VPCODCAB)+;
             ",VPCODCON="+cp_ToStrODBC(this.w_VPCODCON)+;
             ",VPIMPVER="+cp_ToStrODBC(this.w_VPIMPVER)+;
             ",VPIMPINT="+cp_ToStrODBC(this.w_VPIMPINT)+;
             ",VPVALVER="+cp_ToStrODBCNull(this.w_VPVALVER)+;
             ",VPVERF24="+cp_ToStrODBC(this.w_VPVERF24)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'VEN_RITE')
        i_cWhere = cp_PKFox(i_cTable  ,'VPSERIAL',this.w_VPSERIAL  )
        UPDATE (i_cTable) SET;
              VPCODESE=this.w_VPCODESE;
             ,VPNUMREG=this.w_VPNUMREG;
             ,VPDATREG=this.w_VPDATREG;
             ,VPDESCRI=this.w_VPDESCRI;
             ,VPDATINI=this.w_VPDATINI;
             ,VPDATFIN=this.w_VPDATFIN;
             ,VPCODTRI=this.w_VPCODTRI;
             ,VPSTATUS=this.w_VPSTATUS;
             ,VPNUMCOR=this.w_VPNUMCOR;
             ,VPCODBAN=this.w_VPCODBAN;
             ,VPNUMVER=this.w_VPNUMVER;
             ,VPDATVER=this.w_VPDATVER;
             ,VPCODABI=this.w_VPCODABI;
             ,VPCODCAB=this.w_VPCODCAB;
             ,VPCODCON=this.w_VPCODCON;
             ,VPIMPVER=this.w_VPIMPVER;
             ,VPIMPINT=this.w_VPIMPINT;
             ,VPVALVER=this.w_VPVALVER;
             ,VPVERF24=this.w_VPVERF24;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.VEN_RITE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.VEN_RITE_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.VEN_RITE_IDX,i_nConn)
      *
      * delete VEN_RITE
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'VPSERIAL',this.w_VPSERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.VEN_RITE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.VEN_RITE_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
            .w_CODAZI = i_CODAZI
        .DoRTCalc(3,4,.t.)
        if .o_VPCODESE<>.w_VPCODESE
          .link_1_5('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
        .DoRTCalc(6,12,.t.)
          .link_1_14('Full')
        .DoRTCalc(14,21,.t.)
        if .o_VPSTATUS<>.w_VPSTATUS.or. .o_VPDATREG<>.w_VPDATREG
            .w_VPDATVER = IIF(.w_VPSTATUS='D', .w_VPDATREG, cp_CharToDate('  -  -  '))
        endif
        .DoRTCalc(23,28,.t.)
        if .o_VPCODESE<>.w_VPCODESE
            .w_CALCPICT = DEFPIC(.w_DECTOT)
        endif
            .w_FILPAR = SPACE(10)
        .DoRTCalc(31,33,.t.)
        if .o_VPVALVER<>.w_VPVALVER
            .w_CALCPIC2 = DEFPIP(.w_DECTOP)
        endif
        .DoRTCalc(35,35,.t.)
            .w_VALUTA = .w_VPVALVER
        .DoRTCalc(37,45,.t.)
            .w_STATO = .w_VPSTATUS
            .w_NREGIS = .w_VPNUMREG
            .w_NDISTI = .w_VPSERIAL
        .DoRTCalc(49,49,.t.)
            .w_divisa = .w_VPVALVER
            .w_decimi = .w_DECTOP
        .oPgFrm.Page2.oPag.CalcZoom.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_6.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_7.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_9.Calculate()
        .DoRTCalc(52,59,.t.)
            .w_CAMBIO = GETCAM(.w_VPVALVER, .w_VPDATFIN)
        .oPgFrm.Page1.oPag.oObj_1_80.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_codazi<>.w_codazi
           cp_AskTableProg(this,i_nConn,"SEVERINP","i_codazi,w_VPSERIAL")
          .op_VPSERIAL = .w_VPSERIAL
        endif
        if .op_codazi<>.w_codazi .or. .op_VPCODESE<>.w_VPCODESE
           cp_AskTableProg(this,i_nConn,"PRVERINP","i_codazi,w_VPCODESE,w_VPNUMREG")
          .op_VPNUMREG = .w_VPNUMREG
        endif
        .op_codazi = .w_codazi
        .op_codazi = .w_codazi
        .op_VPCODESE = .w_VPCODESE
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
        .oPgFrm.Page2.oPag.CalcZoom.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_6.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_7.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_9.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_80.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oVPNUMCOR_1_18.enabled = this.oPgFrm.Page1.oPag.oVPNUMCOR_1_18.mCond()
    this.oPgFrm.Page1.oPag.oVPCODBAN_1_20.enabled = this.oPgFrm.Page1.oPag.oVPCODBAN_1_20.mCond()
    this.oPgFrm.Page1.oPag.oVPNUMVER_1_22.enabled = this.oPgFrm.Page1.oPag.oVPNUMVER_1_22.mCond()
    this.oPgFrm.Page1.oPag.oVPDATVER_1_23.enabled = this.oPgFrm.Page1.oPag.oVPDATVER_1_23.mCond()
    this.oPgFrm.Page1.oPag.oVPCODABI_1_24.enabled = this.oPgFrm.Page1.oPag.oVPCODABI_1_24.mCond()
    this.oPgFrm.Page1.oPag.oVPCODCAB_1_25.enabled = this.oPgFrm.Page1.oPag.oVPCODCAB_1_25.mCond()
    this.oPgFrm.Page1.oPag.oVPCODCON_1_26.enabled = this.oPgFrm.Page1.oPag.oVPCODCON_1_26.mCond()
    this.oPgFrm.Page2.oPag.oSELEZI_2_2.enabled_(this.oPgFrm.Page2.oPag.oSELEZI_2_2.mCond())
    this.oPgFrm.Page1.oPag.oBtn_1_42.enabled = this.oPgFrm.Page1.oPag.oBtn_1_42.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_3.enabled = this.oPgFrm.Page2.oPag.oBtn_2_3.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_4.enabled = this.oPgFrm.Page2.oPag.oBtn_2_4.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_11.Event(cEvent)
      .oPgFrm.Page2.oPag.CalcZoom.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_6.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_7.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_9.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_80.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=VPCODESE
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VPCODESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_VPCODESE)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_CODAZI;
                     ,'ESCODESE',trim(this.w_VPCODESE))
          select ESCODAZI,ESCODESE,ESVALNAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_VPCODESE)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_VPCODESE) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oVPCODESE_1_3'),i_cWhere,'',"Esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VPCODESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_VPCODESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_VPCODESE)
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VPCODESE = NVL(_Link_.ESCODESE,space(4))
      this.w_VALNAZ = NVL(_Link_.ESVALNAZ,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_VPCODESE = space(4)
      endif
      this.w_VALNAZ = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VPCODESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VALNAZ
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALNAZ) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALNAZ)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALNAZ);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALNAZ)
            select VACODVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALNAZ = NVL(_Link_.VACODVAL,space(3))
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_VALNAZ = space(3)
      endif
      this.w_DECTOT = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALNAZ Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VPCODTRI
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TRI_BUTI_IDX,3]
    i_lTable = "TRI_BUTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2], .t., this.TRI_BUTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VPCODTRI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VPCODTRI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODTRI,TRDESTRI";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODTRI="+cp_ToStrODBC(this.w_VPCODTRI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODTRI',this.w_VPCODTRI)
            select TRCODTRI,TRDESTRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VPCODTRI = NVL(_Link_.TRCODTRI,space(5))
      this.w_DESTRI = NVL(_Link_.TRDESTRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_VPCODTRI = space(5)
      endif
      this.w_DESTRI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2])+'\'+cp_ToStr(_Link_.TRCODTRI,1)
      cp_ShowWarn(i_cKey,this.TRI_BUTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VPCODTRI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_14(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TRI_BUTI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_14.TRCODTRI as TRCODTRI114"+ ",link_1_14.TRDESTRI as TRDESTRI114"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_14 on VEN_RITE.VPCODTRI=link_1_14.TRCODTRI"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_14"
          i_cKey=i_cKey+'+" and VEN_RITE.VPCODTRI=link_1_14.TRCODTRI(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=VPCODTRI
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TRI_BUTI_IDX,3]
    i_lTable = "TRI_BUTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2], .t., this.TRI_BUTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VPCODTRI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATB',True,'TRI_BUTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODTRI like "+cp_ToStrODBC(trim(this.w_VPCODTRI)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODTRI,TRDESTRI,TRFLACON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODTRI","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODTRI',trim(this.w_VPCODTRI))
          select TRCODTRI,TRDESTRI,TRFLACON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODTRI into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_VPCODTRI)==trim(_Link_.TRCODTRI) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_VPCODTRI) and !this.bDontReportError
            deferred_cp_zoom('TRI_BUTI','*','TRCODTRI',cp_AbsName(oSource.parent,'oVPCODTRI_1_16'),i_cWhere,'GSAR_ATB',"Tributi",'GSRI2AMR.TRI_BUTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODTRI,TRDESTRI,TRFLACON";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODTRI="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODTRI',oSource.xKey(1))
            select TRCODTRI,TRDESTRI,TRFLACON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VPCODTRI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODTRI,TRDESTRI,TRFLACON";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODTRI="+cp_ToStrODBC(this.w_VPCODTRI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODTRI',this.w_VPCODTRI)
            select TRCODTRI,TRDESTRI,TRFLACON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VPCODTRI = NVL(_Link_.TRCODTRI,space(5))
      this.w_DESTRI = NVL(_Link_.TRDESTRI,space(35))
      this.w_FLACON = NVL(_Link_.TRFLACON,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_VPCODTRI = space(5)
      endif
      this.w_DESTRI = space(35)
      this.w_FLACON = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FLACON<>'R'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_VPCODTRI = space(5)
        this.w_DESTRI = space(35)
        this.w_FLACON = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2])+'\'+cp_ToStr(_Link_.TRCODTRI,1)
      cp_ShowWarn(i_cKey,this.TRI_BUTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VPCODTRI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_16(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TRI_BUTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_16.TRCODTRI as TRCODTRI116"+ ",link_1_16.TRDESTRI as TRDESTRI116"+ ",link_1_16.TRFLACON as TRFLACON116"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_16 on VEN_RITE.VPCODTRI=link_1_16.TRCODTRI"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_16"
          i_cKey=i_cKey+'+" and VEN_RITE.VPCODTRI=link_1_16.TRCODTRI(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=VPNUMCOR
  func Link_1_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_lTable = "COC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2], .t., this.COC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VPNUMCOR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'COC_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BACODBAN like "+cp_ToStrODBC(trim(this.w_VPNUMCOR)+"%");

          i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BACODABI,BACODCAB";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BACODBAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BACODBAN',trim(this.w_VPNUMCOR))
          select BACODBAN,BADESCRI,BACODABI,BACODCAB;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BACODBAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_VPNUMCOR)==trim(_Link_.BACODBAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_VPNUMCOR) and !this.bDontReportError
            deferred_cp_zoom('COC_MAST','*','BACODBAN',cp_AbsName(oSource.parent,'oVPNUMCOR_1_18'),i_cWhere,'',"Elenco conti banche",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BACODABI,BACODCAB";
                     +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',oSource.xKey(1))
            select BACODBAN,BADESCRI,BACODABI,BACODCAB;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VPNUMCOR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BACODABI,BACODCAB";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_VPNUMCOR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_VPNUMCOR)
            select BACODBAN,BADESCRI,BACODABI,BACODCAB;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VPNUMCOR = NVL(_Link_.BACODBAN,space(15))
      this.w_DESCOR = NVL(_Link_.BADESCRI,space(35))
      this.w_VPCODABI = NVL(_Link_.BACODABI,space(5))
      this.w_VPCODCAB = NVL(_Link_.BACODCAB,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_VPNUMCOR = space(15)
      endif
      this.w_DESCOR = space(35)
      this.w_VPCODABI = space(5)
      this.w_VPCODCAB = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.COC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VPNUMCOR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_18(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.COC_MAST_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_18.BACODBAN as BACODBAN118"+ ",link_1_18.BADESCRI as BADESCRI118"+ ",link_1_18.BACODABI as BACODABI118"+ ",link_1_18.BACODCAB as BACODCAB118"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_18 on VEN_RITE.VPNUMCOR=link_1_18.BACODBAN"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_18"
          i_cKey=i_cKey+'+" and VEN_RITE.VPNUMCOR=link_1_18.BACODBAN(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=VPCODBAN
  func Link_1_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.BAN_CHE_IDX,3]
    i_lTable = "BAN_CHE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2], .t., this.BAN_CHE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VPCODBAN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'BAN_CHE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BACODBAN like "+cp_ToStrODBC(trim(this.w_VPCODBAN)+"%");

          i_ret=cp_SQL(i_nConn,"select BACODBAN,BACODABI,BACODCAB,BADESBAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BACODBAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BACODBAN',trim(this.w_VPCODBAN))
          select BACODBAN,BACODABI,BACODCAB,BADESBAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BACODBAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_VPCODBAN)==trim(_Link_.BACODBAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_VPCODBAN) and !this.bDontReportError
            deferred_cp_zoom('BAN_CHE','*','BACODBAN',cp_AbsName(oSource.parent,'oVPCODBAN_1_20'),i_cWhere,'',"Elenco banche",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BACODABI,BACODCAB,BADESBAN";
                     +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',oSource.xKey(1))
            select BACODBAN,BACODABI,BACODCAB,BADESBAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VPCODBAN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BACODABI,BACODCAB,BADESBAN";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_VPCODBAN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_VPCODBAN)
            select BACODBAN,BACODABI,BACODCAB,BADESBAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VPCODBAN = NVL(_Link_.BACODBAN,space(10))
      this.w_VPCODABI = NVL(_Link_.BACODABI,space(5))
      this.w_VPCODCAB = NVL(_Link_.BACODCAB,space(5))
      this.w_DESBAN = NVL(_Link_.BADESBAN,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_VPCODBAN = space(10)
      endif
      this.w_VPCODABI = space(5)
      this.w_VPCODCAB = space(5)
      this.w_DESBAN = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.BAN_CHE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VPCODBAN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_20(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.BAN_CHE_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_20.BACODBAN as BACODBAN120"+ ",link_1_20.BACODABI as BACODABI120"+ ",link_1_20.BACODCAB as BACODCAB120"+ ",link_1_20.BADESBAN as BADESBAN120"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_20 on VEN_RITE.VPCODBAN=link_1_20.BACODBAN"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_20"
          i_cKey=i_cKey+'+" and VEN_RITE.VPCODBAN=link_1_20.BACODBAN(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=VPVALVER
  func Link_1_29(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VPVALVER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_VPVALVER)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT,VASIMVAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_VPVALVER))
          select VACODVAL,VADECTOT,VASIMVAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_VPVALVER)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_VPVALVER) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oVPVALVER_1_29'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT,VASIMVAL";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADECTOT,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VPVALVER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT,VASIMVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VPVALVER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VPVALVER)
            select VACODVAL,VADECTOT,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VPVALVER = NVL(_Link_.VACODVAL,space(3))
      this.w_DECTOP = NVL(_Link_.VADECTOT,0)
      this.w_SIMVAL = NVL(_Link_.VASIMVAL,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_VPVALVER = space(3)
      endif
      this.w_DECTOP = 0
      this.w_SIMVAL = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VPVALVER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_29(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VALUTE_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_29.VACODVAL as VACODVAL129"+ ",link_1_29.VADECTOT as VADECTOT129"+ ",link_1_29.VASIMVAL as VASIMVAL129"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_29 on VEN_RITE.VPVALVER=link_1_29.VACODVAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_29"
          i_cKey=i_cKey+'+" and VEN_RITE.VPVALVER=link_1_29.VACODVAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oVPCODESE_1_3.value==this.w_VPCODESE)
      this.oPgFrm.Page1.oPag.oVPCODESE_1_3.value=this.w_VPCODESE
    endif
    if not(this.oPgFrm.Page1.oPag.oVPNUMREG_1_4.value==this.w_VPNUMREG)
      this.oPgFrm.Page1.oPag.oVPNUMREG_1_4.value=this.w_VPNUMREG
    endif
    if not(this.oPgFrm.Page1.oPag.oVPDATREG_1_7.value==this.w_VPDATREG)
      this.oPgFrm.Page1.oPag.oVPDATREG_1_7.value=this.w_VPDATREG
    endif
    if not(this.oPgFrm.Page1.oPag.oVPDESCRI_1_8.value==this.w_VPDESCRI)
      this.oPgFrm.Page1.oPag.oVPDESCRI_1_8.value=this.w_VPDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oVPDATINI_1_12.value==this.w_VPDATINI)
      this.oPgFrm.Page1.oPag.oVPDATINI_1_12.value=this.w_VPDATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oVPDATFIN_1_13.value==this.w_VPDATFIN)
      this.oPgFrm.Page1.oPag.oVPDATFIN_1_13.value=this.w_VPDATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oVPCODTRI_1_16.value==this.w_VPCODTRI)
      this.oPgFrm.Page1.oPag.oVPCODTRI_1_16.value=this.w_VPCODTRI
    endif
    if not(this.oPgFrm.Page1.oPag.oVPSTATUS_1_17.RadioValue()==this.w_VPSTATUS)
      this.oPgFrm.Page1.oPag.oVPSTATUS_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oVPNUMCOR_1_18.value==this.w_VPNUMCOR)
      this.oPgFrm.Page1.oPag.oVPNUMCOR_1_18.value=this.w_VPNUMCOR
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCOR_1_19.value==this.w_DESCOR)
      this.oPgFrm.Page1.oPag.oDESCOR_1_19.value=this.w_DESCOR
    endif
    if not(this.oPgFrm.Page1.oPag.oVPCODBAN_1_20.value==this.w_VPCODBAN)
      this.oPgFrm.Page1.oPag.oVPCODBAN_1_20.value=this.w_VPCODBAN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESBAN_1_21.value==this.w_DESBAN)
      this.oPgFrm.Page1.oPag.oDESBAN_1_21.value=this.w_DESBAN
    endif
    if not(this.oPgFrm.Page1.oPag.oVPNUMVER_1_22.value==this.w_VPNUMVER)
      this.oPgFrm.Page1.oPag.oVPNUMVER_1_22.value=this.w_VPNUMVER
    endif
    if not(this.oPgFrm.Page1.oPag.oVPDATVER_1_23.value==this.w_VPDATVER)
      this.oPgFrm.Page1.oPag.oVPDATVER_1_23.value=this.w_VPDATVER
    endif
    if not(this.oPgFrm.Page1.oPag.oVPCODABI_1_24.value==this.w_VPCODABI)
      this.oPgFrm.Page1.oPag.oVPCODABI_1_24.value=this.w_VPCODABI
    endif
    if not(this.oPgFrm.Page1.oPag.oVPCODCAB_1_25.value==this.w_VPCODCAB)
      this.oPgFrm.Page1.oPag.oVPCODCAB_1_25.value=this.w_VPCODCAB
    endif
    if not(this.oPgFrm.Page1.oPag.oVPCODCON_1_26.value==this.w_VPCODCON)
      this.oPgFrm.Page1.oPag.oVPCODCON_1_26.value=this.w_VPCODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oVPIMPVER_1_27.value==this.w_VPIMPVER)
      this.oPgFrm.Page1.oPag.oVPIMPVER_1_27.value=this.w_VPIMPVER
    endif
    if not(this.oPgFrm.Page1.oPag.oVPIMPINT_1_28.value==this.w_VPIMPINT)
      this.oPgFrm.Page1.oPag.oVPIMPINT_1_28.value=this.w_VPIMPINT
    endif
    if not(this.oPgFrm.Page1.oPag.oVPVALVER_1_29.value==this.w_VPVALVER)
      this.oPgFrm.Page1.oPag.oVPVALVER_1_29.value=this.w_VPVALVER
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTAL3_1_58.value==this.w_TOTAL3)
      this.oPgFrm.Page1.oPag.oTOTAL3_1_58.value=this.w_TOTAL3
    endif
    if not(this.oPgFrm.Page1.oPag.oDESTRI_1_61.value==this.w_DESTRI)
      this.oPgFrm.Page1.oPag.oDESTRI_1_61.value=this.w_DESTRI
    endif
    if not(this.oPgFrm.Page2.oPag.oSELEZI_2_2.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page2.oPag.oSELEZI_2_2.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTOTAL1_2_8.value==this.w_TOTAL1)
      this.oPgFrm.Page2.oPag.oTOTAL1_2_8.value=this.w_TOTAL1
    endif
    if not(this.oPgFrm.Page2.oPag.oTOTAL2_2_10.value==this.w_TOTAL2)
      this.oPgFrm.Page2.oPag.oTOTAL2_2_10.value=this.w_TOTAL2
    endif
    if not(this.oPgFrm.Page2.oPag.oTOTAL3_2_11.value==this.w_TOTAL3)
      this.oPgFrm.Page2.oPag.oTOTAL3_2_11.value=this.w_TOTAL3
    endif
    if not(this.oPgFrm.Page1.oPag.oSIMVAL_1_77.value==this.w_SIMVAL)
      this.oPgFrm.Page1.oPag.oSIMVAL_1_77.value=this.w_SIMVAL
    endif
    cp_SetControlsValueExtFlds(this,'VEN_RITE')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_VPCODESE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oVPCODESE_1_3.SetFocus()
            i_bnoObbl = !empty(.w_VPCODESE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_VPNUMREG))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oVPNUMREG_1_4.SetFocus()
            i_bnoObbl = !empty(.w_VPNUMREG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_VPDATREG))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oVPDATREG_1_7.SetFocus()
            i_bnoObbl = !empty(.w_VPDATREG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_VPDATINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oVPDATINI_1_12.SetFocus()
            i_bnoObbl = !empty(.w_VPDATINI)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Le date sono incongruenti (devono avere stesso mese e anno)")
          case   ((empty(.w_VPDATFIN)) or not(.w_VPDATFIN>=.w_VPDATINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oVPDATFIN_1_13.SetFocus()
            i_bnoObbl = !empty(.w_VPDATFIN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Le date sono incongruenti (devono avere stesso mese e anno)")
          case   not(.w_FLACON<>'R')  and not(empty(.w_VPCODTRI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oVPCODTRI_1_16.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_VPVALVER))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oVPVALVER_1_29.SetFocus()
            i_bnoObbl = !empty(.w_VPVALVER)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_VPCODESE = this.w_VPCODESE
    this.o_VPDATREG = this.w_VPDATREG
    this.o_VPSTATUS = this.w_VPSTATUS
    this.o_VPVALVER = this.w_VPVALVER
    return

enddefine

* --- Define pages as container
define class tgsri_avnPag1 as StdContainer
  Width  = 680
  height = 387
  stdWidth  = 680
  stdheight = 387
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oVPCODESE_1_3 as StdField with uid="RSRVXNLZIH",rtseq=3,rtrep=.f.,;
    cFormVar = "w_VPCODESE", cQueryName = "VPCODESE",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Codice esercizio di riferimento",;
    HelpContextID = 167145573,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=188, Top=12, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_VPCODESE"

  func oVPCODESE_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oVPCODESE_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oVPCODESE_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CODAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_CODAZI)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oVPCODESE_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Esercizi",'',this.parent.oContained
  endproc

  add object oVPNUMREG_1_4 as StdField with uid="SFUXQCLENE",rtseq=4,rtrep=.f.,;
    cFormVar = "w_VPNUMREG", cQueryName = "VPNUMREG",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero registrazione",;
    HelpContextID = 60833693,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=117, Top=12, cSayPict='"999999"', cGetPict='"999999"'

  add object oVPDATREG_1_7 as StdField with uid="PZAJKQPLIF",rtseq=7,rtrep=.f.,;
    cFormVar = "w_VPDATREG", cQueryName = "VPDATREG",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di registrazione",;
    HelpContextID = 66822045,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=294, Top=12

  add object oVPDESCRI_1_8 as StdField with uid="JCTMOMINEW",rtseq=8,rtrep=.f.,;
    cFormVar = "w_VPDESCRI", cQueryName = "VPDESCRI",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Note descritive del versamento",;
    HelpContextID = 185622625,;
   bGlobalFont=.t.,;
    Height=21, Width=328, Left=117, Top=47, InputMask=replicate('X',40)


  add object oObj_1_11 as cp_runprogram with uid="MXKTDBGOQJ",left=4, top=405, width=296,height=20,;
    caption='GSRI_BDT(New)',;
   bGlobalFont=.t.,;
    prg="GSRI_BDT('New')",;
    cEvent = "New record,w_VPDATREG Changed",;
    nPag=1;
    , HelpContextID = 138272198

  add object oVPDATINI_1_12 as StdField with uid="HOMUSDRAZR",rtseq=11,rtrep=.f.,;
    cFormVar = "w_VPDATINI", cQueryName = "VPDATINI",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Le date sono incongruenti (devono avere stesso mese e anno)",;
    ToolTipText = "Data di inizio selezione registrazioni di ritenute",;
    HelpContextID = 84172897,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=117, Top=82

  add object oVPDATFIN_1_13 as StdField with uid="FZFDRMFKLH",rtseq=12,rtrep=.f.,;
    cFormVar = "w_VPDATFIN", cQueryName = "VPDATFIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Le date sono incongruenti (devono avere stesso mese e anno)",;
    ToolTipText = "Data di fine selezione registrazioni di ritenute",;
    HelpContextID = 133930916,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=227, Top=82

  func oVPDATFIN_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_VPDATFIN>=.w_VPDATINI)
    endwith
    return bRes
  endfunc

  add object oVPCODTRI_1_16 as StdField with uid="YLGYOXHIKM",rtseq=15,rtrep=.f.,;
    cFormVar = "w_VPCODTRI", cQueryName = "VPCODTRI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice tributo",;
    HelpContextID = 183922785,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=117, Top=116, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TRI_BUTI", cZoomOnZoom="GSAR_ATB", oKey_1_1="TRCODTRI", oKey_1_2="this.w_VPCODTRI"

  func oVPCODTRI_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oVPCODTRI_1_16.ecpDrop(oSource)
    this.Parent.oContained.link_1_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oVPCODTRI_1_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TRI_BUTI','*','TRCODTRI',cp_AbsName(this.parent,'oVPCODTRI_1_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATB',"Tributi",'GSRI2AMR.TRI_BUTI_VZM',this.parent.oContained
  endproc
  proc oVPCODTRI_1_16.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODTRI=this.parent.oContained.w_VPCODTRI
     i_obj.ecpSave()
  endproc


  add object oVPSTATUS_1_17 as StdCombo with uid="NHVQBBZWJX",rtseq=16,rtrep=.f.,left=557,top=12,width=112,height=21;
    , tabstop=.f.;
    , ToolTipText = "Status della registrazione";
    , HelpContextID = 81760169;
    , cFormVar="w_VPSTATUS",RowSource=""+"Provvisorio,"+"Confermato", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oVPSTATUS_1_17.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oVPSTATUS_1_17.GetRadio()
    this.Parent.oContained.w_VPSTATUS = this.RadioValue()
    return .t.
  endfunc

  func oVPSTATUS_1_17.SetRadio()
    this.Parent.oContained.w_VPSTATUS=trim(this.Parent.oContained.w_VPSTATUS)
    this.value = ;
      iif(this.Parent.oContained.w_VPSTATUS=='P',1,;
      iif(this.Parent.oContained.w_VPSTATUS=='D',2,;
      0))
  endfunc

  add object oVPNUMCOR_1_18 as StdField with uid="OHFSLTLQJQ",rtseq=17,rtrep=.f.,;
    cFormVar = "w_VPNUMCOR", cQueryName = "VPNUMCOR",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice del conto banca di pagamento dell'imposta",;
    HelpContextID = 190824536,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=117, Top=186, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="COC_MAST", oKey_1_1="BACODBAN", oKey_1_2="this.w_VPNUMCOR"

  func oVPNUMCOR_1_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VPSTATUS='D')
    endwith
   endif
  endfunc

  func oVPNUMCOR_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oVPNUMCOR_1_18.ecpDrop(oSource)
    this.Parent.oContained.link_1_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oVPNUMCOR_1_18.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COC_MAST','*','BACODBAN',cp_AbsName(this.parent,'oVPNUMCOR_1_18'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco conti banche",'',this.parent.oContained
  endproc

  add object oDESCOR_1_19 as StdField with uid="GDCXNXYQMZ",rtseq=18,rtrep=.f.,;
    cFormVar = "w_DESCOR", cQueryName = "DESCOR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 206666954,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=249, Top=186, InputMask=replicate('X',35)

  add object oVPCODBAN_1_20 as StdField with uid="CZSSUMGZZN",rtseq=19,rtrep=.f.,;
    cFormVar = "w_VPCODBAN", cQueryName = "VPCODBAN",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice della banca di pagamento dell'imposta in assenza del conto di tesoreria",;
    HelpContextID = 217477212,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=117, Top=216, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="BAN_CHE", oKey_1_1="BACODBAN", oKey_1_2="this.w_VPCODBAN"

  func oVPCODBAN_1_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_VPNUMCOR) AND .w_VPSTATUS='D')
    endwith
   endif
  endfunc

  func oVPCODBAN_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_20('Part',this)
    endwith
    return bRes
  endfunc

  proc oVPCODBAN_1_20.ecpDrop(oSource)
    this.Parent.oContained.link_1_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oVPCODBAN_1_20.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'BAN_CHE','*','BACODBAN',cp_AbsName(this.parent,'oVPCODBAN_1_20'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco banche",'',this.parent.oContained
  endproc

  add object oDESBAN_1_21 as StdField with uid="APVFAZUMIL",rtseq=20,rtrep=.f.,;
    cFormVar = "w_DESBAN", cQueryName = "DESBAN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 20085962,;
   bGlobalFont=.t.,;
    Height=21, Width=359, Left=219, Top=216, InputMask=replicate('X',50)

  add object oVPNUMVER_1_22 as StdField with uid="NXAZZTVVXY",rtseq=21,rtrep=.f.,;
    cFormVar = "w_VPNUMVER", cQueryName = "VPNUMVER",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero del versamento sul conto corrente",;
    HelpContextID = 127942568,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=117, Top=246, cSayPict='"999999"', cGetPict='"999999"'

  func oVPNUMVER_1_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VPSTATUS='D')
    endwith
   endif
  endfunc

  add object oVPDATVER_1_23 as StdField with uid="ZXAEJKMAIM",rtseq=22,rtrep=.f.,;
    cFormVar = "w_VPDATVER", cQueryName = "VPDATVER",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di avvenuto versamento sul C/C",;
    HelpContextID = 133930920,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=239, Top=246

  func oVPDATVER_1_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VPSTATUS='D')
    endwith
   endif
  endfunc

  add object oVPCODABI_1_24 as StdField with uid="ASPRSVLIAZ",rtseq=23,rtrep=.f.,;
    cFormVar = "w_VPCODABI", cQueryName = "VPCODABI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice ABI della banca",;
    HelpContextID = 234254433,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=117, Top=276, cSayPict="'99999'", cGetPict="'99999'", InputMask=replicate('X',5)

  func oVPCODABI_1_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VPSTATUS='D')
    endwith
   endif
  endfunc

  add object oVPCODCAB_1_25 as StdField with uid="LTUTDQFHVV",rtseq=24,rtrep=.f.,;
    cFormVar = "w_VPCODCAB", cQueryName = "VPCODCAB",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice CAB della filiale",;
    HelpContextID = 200700008,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=239, Top=275, cSayPict="'99999'", cGetPict="'99999'", InputMask=replicate('X',5)

  func oVPCODCAB_1_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VPSTATUS='D')
    endwith
   endif
  endfunc

  add object oVPCODCON_1_26 as StdField with uid="NNLIOFLRQY",rtseq=25,rtrep=.f.,;
    cFormVar = "w_VPCODCON", cQueryName = "VPCODCON",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice della concessione",;
    HelpContextID = 200699996,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=400, Top=275, InputMask=replicate('X',3)

  func oVPCODCON_1_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VPSTATUS='D')
    endwith
   endif
  endfunc

  add object oVPIMPVER_1_27 as StdField with uid="EVOEBFJPVO",rtseq=26,rtrep=.f.,;
    cFormVar = "w_VPIMPVER", cQueryName = "VPIMPVER",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo effettivamente versato",;
    HelpContextID = 130543528,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=117, Top=306, cSayPict="v_PV(38+VVP)", cGetPict="v_GV(38+VVP)"

  add object oVPIMPINT_1_28 as StdField with uid="GQNEEHJAJS",rtseq=27,rtrep=.f.,;
    cFormVar = "w_VPIMPINT", cQueryName = "VPIMPINT",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Interessi versati",;
    HelpContextID = 87560278,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=316, Top=306, cSayPict="v_PV(38+VVP)", cGetPict="v_GV(38+VVP)"

  add object oVPVALVER_1_29 as StdField with uid="VEYKJTCMLD",rtseq=28,rtrep=.f.,;
    cFormVar = "w_VPVALVER", cQueryName = "VPVALVER",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Valuta di versamento",;
    HelpContextID = 125616040,;
    FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=48, Left=553, Top=306, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", oKey_1_1="VACODVAL", oKey_1_2="this.w_VPVALVER"

  func oVPVALVER_1_29.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_29('Part',this)
    endwith
    return bRes
  endfunc

  proc oVPVALVER_1_29.ecpDrop(oSource)
    this.Parent.oContained.link_1_29('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oVPVALVER_1_29.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oVPVALVER_1_29'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc


  add object oBtn_1_41 as StdButton with uid="PLEEICKVVR",left=628, top=339, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per abbandonare le modifiche";
    , HelpContextID = 216379718;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_41.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_42 as StdButton with uid="XVOKLUAWFH",left=578, top=339, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per memorizzare i dati inseriti";
    , HelpContextID = 209091046;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_42.Click()
      with this.Parent.oContained
        GSRI_BDC(this.Parent.oContained,.w_VPSERIAL,this.name)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_42.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction='Load')
      endwith
    endif
  endfunc

  add object oTOTAL3_1_58 as StdField with uid="WBQWNAMNKP",rtseq=35,rtrep=.f.,;
    cFormVar = "w_TOTAL3", cQueryName = "TOTAL3",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 193159626,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=553, Top=275, cSayPict="v_PV[38+VVP]", cGetPict="v_GV[38+VVP]"

  add object oDESTRI_1_61 as StdField with uid="EHTKXOMEPS",rtseq=37,rtrep=.f.,;
    cFormVar = "w_DESTRI", cQueryName = "DESTRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 84966602,;
   bGlobalFont=.t.,;
    Height=21, Width=261, Left=184, Top=116, InputMask=replicate('X',35)

  add object oSIMVAL_1_77 as StdField with uid="LCIIOWVXUG",rtseq=58,rtrep=.f.,;
    cFormVar = "w_SIMVAL", cQueryName = "SIMVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 52352986,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=604, Top=306, InputMask=replicate('X',5)


  add object oObj_1_80 as cp_runprogram with uid="LVXXNTROEL",left=4, top=429, width=296,height=20,;
    caption='GSRI_BDK(INPS)',;
   bGlobalFont=.t.,;
    prg="GSRI_BDK('INPS')",;
    cEvent = "Delete start",;
    nPag=1;
    , HelpContextID = 54111537

  add object oStr_1_30 as StdString with uid="UDAHRZXYMO",Visible=.t., Left=4, Top=12,;
    Alignment=1, Width=111, Height=15,;
    Caption="Registrazione n.:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_31 as StdString with uid="ISOMMWHTEF",Visible=.t., Left=180, Top=12,;
    Alignment=0, Width=11, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="UOLJGNEJOA",Visible=.t., Left=248, Top=12,;
    Alignment=1, Width=42, Height=15,;
    Caption="Del:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_33 as StdString with uid="UQIVDKWBOP",Visible=.t., Left=9, Top=48,;
    Alignment=1, Width=106, Height=15,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="NUWHSPLNTL",Visible=.t., Left=444, Top=12,;
    Alignment=1, Width=109, Height=15,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="AGMVHALUVB",Visible=.t., Left=191, Top=246,;
    Alignment=1, Width=45, Height=15,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="ADAOLBNGJZ",Visible=.t., Left=10, Top=275,;
    Alignment=1, Width=105, Height=15,;
    Caption="Codice ABI:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="XWCCORILNS",Visible=.t., Left=185, Top=275,;
    Alignment=1, Width=50, Height=15,;
    Caption="CAB:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="KKTXKQQJXY",Visible=.t., Left=308, Top=275,;
    Alignment=1, Width=90, Height=15,;
    Caption="Concessione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="WSIPIDYHIS",Visible=.t., Left=9, Top=82,;
    Alignment=1, Width=106, Height=15,;
    Caption="Ritenute dal:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="HNGGZUVKEX",Visible=.t., Left=202, Top=82,;
    Alignment=1, Width=23, Height=15,;
    Caption="Al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="ENHOIJGEPZ",Visible=.t., Left=6, Top=153,;
    Alignment=0, Width=654, Height=15,;
    Caption="Estremi del versamento"  ;
  , bGlobalFont=.t.

  add object oStr_1_47 as StdString with uid="YOVOURBYLA",Visible=.t., Left=10, Top=246,;
    Alignment=1, Width=105, Height=15,;
    Caption="N.versamento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_48 as StdString with uid="JFGUYOINUD",Visible=.t., Left=10, Top=186,;
    Alignment=1, Width=105, Height=15,;
    Caption="Conto tesoreria:"  ;
  , bGlobalFont=.t.

  add object oStr_1_49 as StdString with uid="NTVNRKWNLW",Visible=.t., Left=10, Top=216,;
    Alignment=1, Width=105, Height=15,;
    Caption="Banca:"  ;
  , bGlobalFont=.t.

  add object oStr_1_51 as StdString with uid="HZEXCQWVAE",Visible=.t., Left=3, Top=306,;
    Alignment=1, Width=112, Height=15,;
    Caption="Importo versato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_52 as StdString with uid="TDTBINZTQW",Visible=.t., Left=241, Top=306,;
    Alignment=1, Width=74, Height=15,;
    Caption="Interessi:"  ;
  , bGlobalFont=.t.

  add object oStr_1_53 as StdString with uid="OHYPKAFXWK",Visible=.t., Left=522, Top=306,;
    Alignment=1, Width=28, Height=15,;
    Caption="In:"  ;
  , bGlobalFont=.t.

  add object oStr_1_57 as StdString with uid="QXWWIYETFA",Visible=.t., Left=445, Top=275,;
    Alignment=1, Width=105, Height=15,;
    Caption="Totale ritenute:"  ;
  , bGlobalFont=.t.

  add object oStr_1_60 as StdString with uid="CEOOAKRIGZ",Visible=.t., Left=9, Top=117,;
    Alignment=1, Width=106, Height=15,;
    Caption="Codice tributo:"  ;
  , bGlobalFont=.t.

  add object oBox_1_45 as StdBox with uid="TYVWNIHGQL",left=1, top=170, width=676,height=1
enddefine
define class tgsri_avnPag2 as StdContainer
  Width  = 680
  height = 387
  stdWidth  = 680
  stdheight = 387
  resizeXpos=488
  resizeYpos=165
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object CalcZoom as cp_szoombox with uid="RTKDZCTTNG",left=2, top=4, width=680,height=298,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable='MOV_RITE',cZoomFile='GSRI_KVN',bOptions=.f.,bQueryOnLoad=.f.,;
    cEvent = "Legge",;
    nPag=2;
    , HelpContextID = 149810970

  add object oSELEZI_2_2 as StdRadio with uid="LWHVTXURSL",rtseq=53,rtrep=.f.,left=5, top=326, width=153,height=32;
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=2;
  , bGlobalFont=.t.

    proc oSELEZI_2_2.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutto"
      this.Buttons(1).HelpContextID = 77589466
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutto"
      this.Buttons(2).HelpContextID = 77589466
      this.Buttons(2).Top=15
      this.SetAll("Width",151)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELEZI_2_2.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oSELEZI_2_2.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_2_2.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=='S',1,;
      iif(this.Parent.oContained.w_SELEZI=='D',2,;
      0))
  endfunc

  func oSELEZI_2_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_VPSERIAL) AND .cFunction<>'Load')
    endwith
   endif
  endfunc


  add object oBtn_2_3 as StdButton with uid="OPDGYVETXJ",left=207, top=341, width=48,height=45,;
    CpPicture="BMP\ELIMINA.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per eliminare i movimenti ritenute selezionati dalla distinta";
    , HelpContextID = 243660218;
    , caption='\<Elimina';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_3.Click()
      with this.Parent.oContained
        GSRI_BDD(this.Parent.oContained,"INPS")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_3.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_VPSTATUS<>'D' AND .cFunction<>'Load' AND NOT EMPTY(.w_VPSERIAL))
      endwith
    endif
  endfunc


  add object oBtn_2_4 as StdButton with uid="YXBLHYHQUF",left=259, top=341, width=48,height=45,;
    CpPicture="BMP\ABBINA.BMP", caption="", nPag=2;
    , ToolTipText = "Abbina movimenti ritenute";
    , HelpContextID = 188379130;
    , caption='\<Abbina';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_4.Click()
      do GSRI_KAN with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_4.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction<>'Load' AND NOT EMPTY(.w_VPSERIAL))
      endwith
    endif
  endfunc


  add object oObj_2_6 as cp_runprogram with uid="ZHJPPSXJQH",left=192, top=397, width=220,height=19,;
    caption='GSRI_BAV',;
   bGlobalFont=.t.,;
    prg="GSRI_BAV",;
    cEvent = "Load,Ricarica,New record,w_VPSTATUS Changed",;
    nPag=2;
    , HelpContextID = 189496900


  add object oObj_2_7 as cp_runprogram with uid="AXADFGXSMB",left=416, top=397, width=180,height=19,;
    caption='GSRI_BDS',;
   bGlobalFont=.t.,;
    prg="GSRI_BDS",;
    cEvent = "w_SELEZI Changed",;
    nPag=2;
    , HelpContextID = 189496903

  add object oTOTAL1_2_8 as StdField with uid="SRFGQAXUTA",rtseq=55,rtrep=.f.,;
    cFormVar = "w_TOTAL1", cQueryName = "TOTAL1",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    HelpContextID = 226714058,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=207, Top=307, cSayPict="v_PV[38+VVP]", cGetPict="v_GV[38+VVP]"


  add object oObj_2_9 as cp_runprogram with uid="NTOZRDJMNL",left=-4, top=425, width=228,height=20,;
    caption='GSRI_BIN',;
   bGlobalFont=.t.,;
    prg="GSRI_BIN",;
    cEvent = "Legge,w_VPVALVER Changed",;
    nPag=2;
    , HelpContextID = 78938548

  add object oTOTAL2_2_10 as StdField with uid="IFZSAQPKCY",rtseq=56,rtrep=.f.,;
    cFormVar = "w_TOTAL2", cQueryName = "TOTAL2",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    HelpContextID = 209936842,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=329, Top=307, cSayPict="v_PV[38+VVP]", cGetPict="v_GV[38+VVP]"

  add object oTOTAL3_2_11 as StdField with uid="UMGTFIQCCD",rtseq=57,rtrep=.f.,;
    cFormVar = "w_TOTAL3", cQueryName = "TOTAL3",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    HelpContextID = 193159626,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=503, Top=306, cSayPict="v_PV[38+VVP]", cGetPict="v_GV[38+VVP]"

  add object oStr_2_12 as StdString with uid="YWTBSDKJAR",Visible=.t., Left=140, Top=307,;
    Alignment=1, Width=64, Height=15,;
    Caption="Totali:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsri_avn','VEN_RITE','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".VPSERIAL=VEN_RITE.VPSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
