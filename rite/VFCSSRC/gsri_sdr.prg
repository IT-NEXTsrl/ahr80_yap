* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_sdr                                                        *
*              Stampa dati ritenute                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [146] [VRS_55]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-03-01                                                      *
* Last revis.: 2008-09-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsri_sdr",oParentObject))

* --- Class definition
define class tgsri_sdr as StdForm
  Top    = 13
  Left   = 85

  * --- Standard Properties
  Width  = 564
  Height = 258
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-09-25"
  HelpContextID=74053225
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=23

  * --- Constant Properties
  _IDX = 0
  TIP_DOCU_IDX = 0
  CONTI_IDX = 0
  VALUTE_IDX = 0
  AGENTI_IDX = 0
  MAGAZZIN_IDX = 0
  PAG_AMEN_IDX = 0
  ART_ICOL_IDX = 0
  BUSIUNIT_IDX = 0
  ESERCIZI_IDX = 0
  cpusers_IDX = 0
  CAU_CONT_IDX = 0
  TRI_BUTI_IDX = 0
  cPrg = "gsri_sdr"
  cComment = "Stampa dati ritenute"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_azienda = space(5)
  w_PNCOMPET = space(5)
  w_DADATREG = ctod('  /  /  ')
  w_A_DATREG = ctod('  /  /  ')
  w_PNCODUTE = 0
  o_PNCODUTE = 0
  w_PNFLPROV = space(1)
  w_DACODCLF = space(15)
  w_RITENU = space(1)
  w_A_CODCLF = space(15)
  w_RITENU1 = space(1)
  w_PNCODCAU = space(5)
  w_TRIB1 = space(5)
  w_TRIB2 = space(5)
  w_ccff = space(1)
  w_forni1 = space(40)
  w_forni2 = space(40)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_DESUTE = space(20)
  w_DESCAU = space(30)
  w_des1 = space(35)
  w_des2 = space(35)
  w_GESRIT = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsri_sdrPag1","gsri_sdr",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPNCOMPET_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[12]
    this.cWorkTables[1]='TIP_DOCU'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='VALUTE'
    this.cWorkTables[4]='AGENTI'
    this.cWorkTables[5]='MAGAZZIN'
    this.cWorkTables[6]='PAG_AMEN'
    this.cWorkTables[7]='ART_ICOL'
    this.cWorkTables[8]='BUSIUNIT'
    this.cWorkTables[9]='ESERCIZI'
    this.cWorkTables[10]='cpusers'
    this.cWorkTables[11]='CAU_CONT'
    this.cWorkTables[12]='TRI_BUTI'
    return(this.OpenAllTables(12))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_azienda=space(5)
      .w_PNCOMPET=space(5)
      .w_DADATREG=ctod("  /  /  ")
      .w_A_DATREG=ctod("  /  /  ")
      .w_PNCODUTE=0
      .w_PNFLPROV=space(1)
      .w_DACODCLF=space(15)
      .w_RITENU=space(1)
      .w_A_CODCLF=space(15)
      .w_RITENU1=space(1)
      .w_PNCODCAU=space(5)
      .w_TRIB1=space(5)
      .w_TRIB2=space(5)
      .w_ccff=space(1)
      .w_forni1=space(40)
      .w_forni2=space(40)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_DESUTE=space(20)
      .w_DESCAU=space(30)
      .w_des1=space(35)
      .w_des2=space(35)
      .w_GESRIT=space(1)
        .w_azienda = i_codazi
        .w_PNCOMPET = g_CODESE
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_PNCOMPET))
          .link_1_2('Full')
        endif
          .DoRTCalc(3,4,.f.)
        .w_PNCODUTE = iif(g_uniute<>'S',I_CODUTE,0)
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_PNCODUTE))
          .link_1_5('Full')
        endif
        .w_PNFLPROV = 'S'
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_DACODCLF))
          .link_1_7('Full')
        endif
        .DoRTCalc(8,9,.f.)
        if not(empty(.w_A_CODCLF))
          .link_1_9('Full')
        endif
        .DoRTCalc(10,11,.f.)
        if not(empty(.w_PNCODCAU))
          .link_1_11('Full')
        endif
        .w_TRIB1 = SPACE(5)
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_TRIB1))
          .link_1_12('Full')
        endif
        .w_TRIB2 = SPACE(5)
        .DoRTCalc(13,13,.f.)
        if not(empty(.w_TRIB2))
          .link_1_13('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_14.Calculate()
        .w_ccff = 'F'
          .DoRTCalc(15,16,.f.)
        .w_OBTEST = i_INIDAT
          .DoRTCalc(18,18,.f.)
        .w_DESUTE = IIF(.w_PNCODUTE<>0,.w_DESUTE,'Tutti')
    endwith
    this.DoRTCalc(20,23,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate()
        .DoRTCalc(1,18,.t.)
        if .o_PNCODUTE<>.w_PNCODUTE
            .w_DESUTE = IIF(.w_PNCODUTE<>0,.w_DESUTE,'Tutti')
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(20,23,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oPNCODUTE_1_5.enabled = this.oPgFrm.Page1.oPag.oPNCODUTE_1_5.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_14.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PNCOMPET
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PNCOMPET) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_PNCOMPET)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_AZIENDA;
                     ,'ESCODESE',trim(this.w_PNCOMPET))
          select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PNCOMPET)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PNCOMPET) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oPNCOMPET_1_2'),i_cWhere,'',"Esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_AZIENDA<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice esercizio inesistente")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PNCOMPET)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_PNCOMPET);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_AZIENDA;
                       ,'ESCODESE',this.w_PNCOMPET)
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PNCOMPET = NVL(_Link_.ESCODESE,space(5))
      this.w_DADATREG = NVL(cp_ToDate(_Link_.ESINIESE),ctod("  /  /  "))
      this.w_A_DATREG = NVL(cp_ToDate(_Link_.ESFINESE),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_PNCOMPET = space(5)
      endif
      this.w_DADATREG = ctod("  /  /  ")
      this.w_A_DATREG = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PNCOMPET Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PNCODUTE
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.cpusers_IDX,3]
    i_lTable = "cpusers"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2], .t., this.cpusers_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PNCODUTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'cpusers')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_PNCODUTE);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_PNCODUTE)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_PNCODUTE) and !this.bDontReportError
            deferred_cp_zoom('cpusers','*','CODE',cp_AbsName(oSource.parent,'oPNCODUTE_1_5'),i_cWhere,'',"Utenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PNCODUTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_PNCODUTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_PNCODUTE)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PNCODUTE = NVL(_Link_.CODE,0)
      this.w_DESUTE = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_PNCODUTE = 0
      endif
      this.w_DESUTE = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.cpusers_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PNCODUTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DACODCLF
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DACODCLF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_DACODCLF)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_ccff);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDTOBSO,ANDESCRI,ANRITENU";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_ccff;
                     ,'ANCODICE',trim(this.w_DACODCLF))
          select ANTIPCON,ANCODICE,ANDTOBSO,ANDESCRI,ANRITENU;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DACODCLF)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DACODCLF) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oDACODCLF_1_7'),i_cWhere,'',"Fornitori",'GSRI_AMR.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ccff<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDTOBSO,ANDESCRI,ANRITENU";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDTOBSO,ANDESCRI,ANRITENU;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Il percipiente iniziale � maggiore di quello finale, oppure non soggetto a ritenuta")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDTOBSO,ANDESCRI,ANRITENU";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_ccff);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDTOBSO,ANDESCRI,ANRITENU;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DACODCLF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDTOBSO,ANDESCRI,ANRITENU";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_DACODCLF);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_ccff);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_ccff;
                       ,'ANCODICE',this.w_DACODCLF)
            select ANTIPCON,ANCODICE,ANDTOBSO,ANDESCRI,ANRITENU;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DACODCLF = NVL(_Link_.ANCODICE,space(15))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
      this.w_forni1 = NVL(_Link_.ANDESCRI,space(40))
      this.w_RITENU = NVL(_Link_.ANRITENU,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_DACODCLF = space(15)
      endif
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_forni1 = space(40)
      this.w_RITENU = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((empty(.w_A_CODCLF)) or (.w_DACODCLF<=.w_A_CODCLF)) AND .w_RITENU$'CS'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il percipiente iniziale � maggiore di quello finale, oppure non soggetto a ritenuta")
        endif
        this.w_DACODCLF = space(15)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_forni1 = space(40)
        this.w_RITENU = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DACODCLF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=A_CODCLF
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_A_CODCLF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_A_CODCLF)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_ccff);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDTOBSO,ANDESCRI,ANRITENU";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_ccff;
                     ,'ANCODICE',trim(this.w_A_CODCLF))
          select ANTIPCON,ANCODICE,ANDTOBSO,ANDESCRI,ANRITENU;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_A_CODCLF)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_A_CODCLF) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oA_CODCLF_1_9'),i_cWhere,'',"Fornitori",'GSRI_AMR.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ccff<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDTOBSO,ANDESCRI,ANRITENU";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDTOBSO,ANDESCRI,ANRITENU;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Il percipiente iniziale � maggiore di quello finale, oppure non soggetto a ritenuta")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDTOBSO,ANDESCRI,ANRITENU";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_ccff);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDTOBSO,ANDESCRI,ANRITENU;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_A_CODCLF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDTOBSO,ANDESCRI,ANRITENU";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_A_CODCLF);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_ccff);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_ccff;
                       ,'ANCODICE',this.w_A_CODCLF)
            select ANTIPCON,ANCODICE,ANDTOBSO,ANDESCRI,ANRITENU;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_A_CODCLF = NVL(_Link_.ANCODICE,space(15))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
      this.w_forni2 = NVL(_Link_.ANDESCRI,space(40))
      this.w_RITENU1 = NVL(_Link_.ANRITENU,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_A_CODCLF = space(15)
      endif
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_forni2 = space(40)
      this.w_RITENU1 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((.w_A_CODCLF>=.w_DACODCLF) or empty(.w_DACODCLF))  AND .w_RITENU1$'CS'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il percipiente iniziale � maggiore di quello finale, oppure non soggetto a ritenuta")
        endif
        this.w_A_CODCLF = space(15)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_forni2 = space(40)
        this.w_RITENU1 = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_A_CODCLF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PNCODCAU
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PNCODCAU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_PNCODCAU)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO,CCGESRIT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_PNCODCAU))
          select CCCODICE,CCDESCRI,CCDTOBSO,CCGESRIT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PNCODCAU)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PNCODCAU) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oPNCODCAU_1_11'),i_cWhere,'',"Causali contabili",'GSRI_SDR.CAU_CONT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO,CCGESRIT";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCDTOBSO,CCGESRIT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PNCODCAU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO,CCGESRIT";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_PNCODCAU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_PNCODCAU)
            select CCCODICE,CCDESCRI,CCDTOBSO,CCGESRIT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PNCODCAU = NVL(_Link_.CCCODICE,space(5))
      this.w_DESCAU = NVL(_Link_.CCDESCRI,space(30))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
      this.w_GESRIT = NVL(_Link_.CCGESRIT,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PNCODCAU = space(5)
      endif
      this.w_DESCAU = space(30)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_GESRIT = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO < .w_OBTEST) AND .w_GESRIT='S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale inesistente, oppure obsoleta, oppure non gestisce le ritenute")
        endif
        this.w_PNCODCAU = space(5)
        this.w_DESCAU = space(30)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_GESRIT = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PNCODCAU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TRIB1
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TRI_BUTI_IDX,3]
    i_lTable = "TRI_BUTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2], .t., this.TRI_BUTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TRIB1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'TRI_BUTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODTRI like "+cp_ToStrODBC(trim(this.w_TRIB1)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODTRI,TRDESTRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODTRI","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODTRI',trim(this.w_TRIB1))
          select TRCODTRI,TRDESTRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODTRI into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TRIB1)==trim(_Link_.TRCODTRI) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TRIB1) and !this.bDontReportError
            deferred_cp_zoom('TRI_BUTI','*','TRCODTRI',cp_AbsName(oSource.parent,'oTRIB1_1_12'),i_cWhere,'',"Tabella tributi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODTRI,TRDESTRI";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODTRI="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODTRI',oSource.xKey(1))
            select TRCODTRI,TRDESTRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TRIB1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODTRI,TRDESTRI";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODTRI="+cp_ToStrODBC(this.w_TRIB1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODTRI',this.w_TRIB1)
            select TRCODTRI,TRDESTRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TRIB1 = NVL(_Link_.TRCODTRI,space(5))
      this.w_des1 = NVL(_Link_.TRDESTRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_TRIB1 = space(5)
      endif
      this.w_des1 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY (.w_TRIB2) or .w_TRIB1<=.w_TRIB2
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice tributo iniziale � maggiore di quello finale")
        endif
        this.w_TRIB1 = space(5)
        this.w_des1 = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2])+'\'+cp_ToStr(_Link_.TRCODTRI,1)
      cp_ShowWarn(i_cKey,this.TRI_BUTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TRIB1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TRIB2
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TRI_BUTI_IDX,3]
    i_lTable = "TRI_BUTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2], .t., this.TRI_BUTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TRIB2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'TRI_BUTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODTRI like "+cp_ToStrODBC(trim(this.w_TRIB2)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODTRI,TRDESTRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODTRI","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODTRI',trim(this.w_TRIB2))
          select TRCODTRI,TRDESTRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODTRI into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TRIB2)==trim(_Link_.TRCODTRI) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TRIB2) and !this.bDontReportError
            deferred_cp_zoom('TRI_BUTI','*','TRCODTRI',cp_AbsName(oSource.parent,'oTRIB2_1_13'),i_cWhere,'',"Tabella tributi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODTRI,TRDESTRI";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODTRI="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODTRI',oSource.xKey(1))
            select TRCODTRI,TRDESTRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TRIB2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODTRI,TRDESTRI";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODTRI="+cp_ToStrODBC(this.w_TRIB2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODTRI',this.w_TRIB2)
            select TRCODTRI,TRDESTRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TRIB2 = NVL(_Link_.TRCODTRI,space(5))
      this.w_des2 = NVL(_Link_.TRDESTRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_TRIB2 = space(5)
      endif
      this.w_des2 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_TRIB1) or .w_TRIB2>=.w_TRIB1
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice tributo iniziale � maggiore di quello finale")
        endif
        this.w_TRIB2 = space(5)
        this.w_des2 = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2])+'\'+cp_ToStr(_Link_.TRCODTRI,1)
      cp_ShowWarn(i_cKey,this.TRI_BUTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TRIB2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oPNCOMPET_1_2.value==this.w_PNCOMPET)
      this.oPgFrm.Page1.oPag.oPNCOMPET_1_2.value=this.w_PNCOMPET
    endif
    if not(this.oPgFrm.Page1.oPag.oDADATREG_1_3.value==this.w_DADATREG)
      this.oPgFrm.Page1.oPag.oDADATREG_1_3.value=this.w_DADATREG
    endif
    if not(this.oPgFrm.Page1.oPag.oA_DATREG_1_4.value==this.w_A_DATREG)
      this.oPgFrm.Page1.oPag.oA_DATREG_1_4.value=this.w_A_DATREG
    endif
    if not(this.oPgFrm.Page1.oPag.oPNCODUTE_1_5.value==this.w_PNCODUTE)
      this.oPgFrm.Page1.oPag.oPNCODUTE_1_5.value=this.w_PNCODUTE
    endif
    if not(this.oPgFrm.Page1.oPag.oPNFLPROV_1_6.RadioValue()==this.w_PNFLPROV)
      this.oPgFrm.Page1.oPag.oPNFLPROV_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDACODCLF_1_7.value==this.w_DACODCLF)
      this.oPgFrm.Page1.oPag.oDACODCLF_1_7.value=this.w_DACODCLF
    endif
    if not(this.oPgFrm.Page1.oPag.oA_CODCLF_1_9.value==this.w_A_CODCLF)
      this.oPgFrm.Page1.oPag.oA_CODCLF_1_9.value=this.w_A_CODCLF
    endif
    if not(this.oPgFrm.Page1.oPag.oPNCODCAU_1_11.value==this.w_PNCODCAU)
      this.oPgFrm.Page1.oPag.oPNCODCAU_1_11.value=this.w_PNCODCAU
    endif
    if not(this.oPgFrm.Page1.oPag.oTRIB1_1_12.value==this.w_TRIB1)
      this.oPgFrm.Page1.oPag.oTRIB1_1_12.value=this.w_TRIB1
    endif
    if not(this.oPgFrm.Page1.oPag.oTRIB2_1_13.value==this.w_TRIB2)
      this.oPgFrm.Page1.oPag.oTRIB2_1_13.value=this.w_TRIB2
    endif
    if not(this.oPgFrm.Page1.oPag.oforni1_1_18.value==this.w_forni1)
      this.oPgFrm.Page1.oPag.oforni1_1_18.value=this.w_forni1
    endif
    if not(this.oPgFrm.Page1.oPag.oforni2_1_19.value==this.w_forni2)
      this.oPgFrm.Page1.oPag.oforni2_1_19.value=this.w_forni2
    endif
    if not(this.oPgFrm.Page1.oPag.oDESUTE_1_28.value==this.w_DESUTE)
      this.oPgFrm.Page1.oPag.oDESUTE_1_28.value=this.w_DESUTE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAU_1_29.value==this.w_DESCAU)
      this.oPgFrm.Page1.oPag.oDESCAU_1_29.value=this.w_DESCAU
    endif
    if not(this.oPgFrm.Page1.oPag.odes1_1_32.value==this.w_des1)
      this.oPgFrm.Page1.oPag.odes1_1_32.value=this.w_des1
    endif
    if not(this.oPgFrm.Page1.oPag.odes2_1_33.value==this.w_des2)
      this.oPgFrm.Page1.oPag.odes2_1_33.value=this.w_des2
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(empty(.w_A_DATREG) or .w_DADATREG<=.w_A_DATREG)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDADATREG_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data finale anteriore a data iniziale")
          case   not(empty(.w_DADATREG) or .w_A_DATREG>=.w_DADATREG)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oA_DATREG_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data finale anteriore a data iniziale")
          case   not(((empty(.w_A_CODCLF)) or (.w_DACODCLF<=.w_A_CODCLF)) AND .w_RITENU$'CS')  and not(empty(.w_DACODCLF))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDACODCLF_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il percipiente iniziale � maggiore di quello finale, oppure non soggetto a ritenuta")
          case   not(((.w_A_CODCLF>=.w_DACODCLF) or empty(.w_DACODCLF))  AND .w_RITENU1$'CS')  and not(empty(.w_A_CODCLF))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oA_CODCLF_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il percipiente iniziale � maggiore di quello finale, oppure non soggetto a ritenuta")
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO < .w_OBTEST) AND .w_GESRIT='S')  and not(empty(.w_PNCODCAU))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPNCODCAU_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale inesistente, oppure obsoleta, oppure non gestisce le ritenute")
          case   not(EMPTY (.w_TRIB2) or .w_TRIB1<=.w_TRIB2)  and not(empty(.w_TRIB1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTRIB1_1_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice tributo iniziale � maggiore di quello finale")
          case   not(EMPTY(.w_TRIB1) or .w_TRIB2>=.w_TRIB1)  and not(empty(.w_TRIB2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTRIB2_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice tributo iniziale � maggiore di quello finale")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PNCODUTE = this.w_PNCODUTE
    return

enddefine

* --- Define pages as container
define class tgsri_sdrPag1 as StdContainer
  Width  = 560
  height = 258
  stdWidth  = 560
  stdheight = 258
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPNCOMPET_1_2 as StdField with uid="KQKSGMJZJI",rtseq=2,rtrep=.f.,;
    cFormVar = "w_PNCOMPET", cQueryName = "PNCOMPET",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice esercizio inesistente",;
    ToolTipText = "Esercizio di competenza",;
    HelpContextID = 12160330,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=112, Top=8, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_AZIENDA", oKey_2_1="ESCODESE", oKey_2_2="this.w_PNCOMPET"

  func oPNCOMPET_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oPNCOMPET_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPNCOMPET_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_AZIENDA)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_AZIENDA)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oPNCOMPET_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Esercizi",'',this.parent.oContained
  endproc

  add object oDADATREG_1_3 as StdField with uid="GHXZHKRPDN",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DADATREG", cQueryName = "DADATREG",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data finale anteriore a data iniziale",;
    ToolTipText = "Data di inizio selezione: filtra su data registrazione",;
    HelpContextID = 52137853,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=292, Top=8

  func oDADATREG_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_A_DATREG) or .w_DADATREG<=.w_A_DATREG)
    endwith
    return bRes
  endfunc

  add object oA_DATREG_1_4 as StdField with uid="UYUBZNDAND",rtseq=4,rtrep=.f.,;
    cFormVar = "w_A_DATREG", cQueryName = "A_DATREG",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data finale anteriore a data iniziale",;
    ToolTipText = "Data di fine selezione: filtra su data registrazione",;
    HelpContextID = 52145485,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=474, Top=8

  func oA_DATREG_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_DADATREG) or .w_A_DATREG>=.w_DADATREG)
    endwith
    return bRes
  endfunc

  add object oPNCODUTE_1_5 as StdField with uid="QYJLFGSJGR",rtseq=5,rtrep=.f.,;
    cFormVar = "w_PNCODUTE", cQueryName = "PNCODUTE",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Utente selezionato (0 tutti)",;
    HelpContextID = 86609211,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=112, Top=33, cSayPict='"9999"', cGetPict='"9999"', bHasZoom = .t. , cLinkFile="cpusers", oKey_1_1="CODE", oKey_1_2="this.w_PNCODUTE"

  func oPNCODUTE_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_UNIUTE<>'S')
    endwith
   endif
  endfunc

  func oPNCODUTE_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oPNCODUTE_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPNCODUTE_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'cpusers','*','CODE',cp_AbsName(this.parent,'oPNCODUTE_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Utenti",'',this.parent.oContained
  endproc

  add object oPNFLPROV_1_6 as StdCheck with uid="VHAFCRFQCC",rtseq=6,rtrep=.f.,left=363, top=30, caption="Considera reg. provvisorie",;
    ToolTipText = "Considera anche registrazioni provvisorie",;
    HelpContextID = 219759284,;
    cFormVar="w_PNFLPROV", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPNFLPROV_1_6.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPNFLPROV_1_6.GetRadio()
    this.Parent.oContained.w_PNFLPROV = this.RadioValue()
    return .t.
  endfunc

  func oPNFLPROV_1_6.SetRadio()
    this.Parent.oContained.w_PNFLPROV=trim(this.Parent.oContained.w_PNFLPROV)
    this.value = ;
      iif(this.Parent.oContained.w_PNFLPROV=='S',1,;
      0)
  endfunc

  add object oDACODCLF_1_7 as StdField with uid="DJRQNZLCER",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DACODCLF", cQueryName = "DACODCLF",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il percipiente iniziale � maggiore di quello finale, oppure non soggetto a ritenuta",;
    ToolTipText = "Percipiente di inizio selezione",;
    HelpContextID = 215384196,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=112, Top=58, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", oKey_1_1="ANTIPCON", oKey_1_2="this.w_ccff", oKey_2_1="ANCODICE", oKey_2_2="this.w_DACODCLF"

  func oDACODCLF_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oDACODCLF_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDACODCLF_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_ccff)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_ccff)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oDACODCLF_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Fornitori",'GSRI_AMR.CONTI_VZM',this.parent.oContained
  endproc

  add object oA_CODCLF_1_9 as StdField with uid="WCMMHKKRJP",rtseq=9,rtrep=.f.,;
    cFormVar = "w_A_CODCLF", cQueryName = "A_CODCLF",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il percipiente iniziale � maggiore di quello finale, oppure non soggetto a ritenuta",;
    ToolTipText = "Percipiente di fine selezione",;
    HelpContextID = 215376564,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=112, Top=84, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", oKey_1_1="ANTIPCON", oKey_1_2="this.w_ccff", oKey_2_1="ANCODICE", oKey_2_2="this.w_A_CODCLF"

  func oA_CODCLF_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oA_CODCLF_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oA_CODCLF_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_ccff)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_ccff)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oA_CODCLF_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Fornitori",'GSRI_AMR.CONTI_VZM',this.parent.oContained
  endproc

  add object oPNCODCAU_1_11 as StdField with uid="AGIPXIQXKA",rtseq=11,rtrep=.f.,;
    cFormVar = "w_PNCODCAU", cQueryName = "PNCODCAU",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale inesistente, oppure obsoleta, oppure non gestisce le ritenute",;
    ToolTipText = "Causali contabili",;
    HelpContextID = 215380661,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=112, Top=110, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", oKey_1_1="CCCODICE", oKey_1_2="this.w_PNCODCAU"

  func oPNCODCAU_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oPNCODCAU_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPNCODCAU_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oPNCODCAU_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Causali contabili",'GSRI_SDR.CAU_CONT_VZM',this.parent.oContained
  endproc

  add object oTRIB1_1_12 as StdField with uid="OISXLATHZQ",rtseq=12,rtrep=.f.,;
    cFormVar = "w_TRIB1", cQueryName = "TRIB1",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice tributo iniziale � maggiore di quello finale",;
    ToolTipText = "Codice tributo di inizio selezione",;
    HelpContextID = 18026186,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=112, Top=134, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TRI_BUTI", oKey_1_1="TRCODTRI", oKey_1_2="this.w_TRIB1"

  func oTRIB1_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oTRIB1_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTRIB1_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TRI_BUTI','*','TRCODTRI',cp_AbsName(this.parent,'oTRIB1_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Tabella tributi",'',this.parent.oContained
  endproc

  add object oTRIB2_1_13 as StdField with uid="LWSNNAOMDV",rtseq=13,rtrep=.f.,;
    cFormVar = "w_TRIB2", cQueryName = "TRIB2",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice tributo iniziale � maggiore di quello finale",;
    ToolTipText = "Codice tributo di fine selezione",;
    HelpContextID = 16977610,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=112, Top=158, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TRI_BUTI", oKey_1_1="TRCODTRI", oKey_1_2="this.w_TRIB2"

  func oTRIB2_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oTRIB2_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTRIB2_1_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TRI_BUTI','*','TRCODTRI',cp_AbsName(this.parent,'oTRIB2_1_13'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Tabella tributi",'',this.parent.oContained
  endproc


  add object oObj_1_14 as cp_outputCombo with uid="ZLERXIVPWT",left=112, top=182, width=434,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 103944422


  add object oBtn_1_15 as StdButton with uid="JQMPIDSGPR",left=455, top=208, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire la stampa";
    , HelpContextID = 200699098;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_15.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_15.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oBtn_1_16 as StdButton with uid="ZQHWCISIST",left=505, top=208, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 66735802;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_16.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oforni1_1_18 as StdField with uid="HETIPNUHNX",rtseq=15,rtrep=.f.,;
    cFormVar = "w_forni1", cQueryName = "forni1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 207904938,;
   bGlobalFont=.t.,;
    Height=21, Width=304, Left=229, Top=58, InputMask=replicate('X',40)

  add object oforni2_1_19 as StdField with uid="RBJEQFHISD",rtseq=16,rtrep=.f.,;
    cFormVar = "w_forni2", cQueryName = "forni2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 191127722,;
   bGlobalFont=.t.,;
    Height=21, Width=304, Left=229, Top=84, InputMask=replicate('X',40)

  add object oDESUTE_1_28 as StdField with uid="LSFTKVNZBB",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DESUTE", cQueryName = "DESUTE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 164592842,;
   bGlobalFont=.t.,;
    Height=21, Width=199, Left=155, Top=33, InputMask=replicate('X',20)

  add object oDESCAU_1_29 as StdField with uid="WJUJGQKPAB",rtseq=20,rtrep=.f.,;
    cFormVar = "w_DESCAU", cQueryName = "DESCAU",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 185695434,;
   bGlobalFont=.t.,;
    Height=21, Width=304, Left=169, Top=110, InputMask=replicate('X',30)

  add object odes1_1_32 as StdField with uid="TGRLDJXMLX",rtseq=21,rtrep=.f.,;
    cFormVar = "w_des1", cQueryName = "des1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 70343370,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=169, Top=134, InputMask=replicate('X',35)

  add object odes2_1_33 as StdField with uid="GZLSODIWXS",rtseq=22,rtrep=.f.,;
    cFormVar = "w_des2", cQueryName = "des2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 70277834,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=169, Top=158, InputMask=replicate('X',35)

  add object oStr_1_22 as StdString with uid="XDDFYQAPJU",Visible=.t., Left=7, Top=58,;
    Alignment=1, Width=100, Height=15,;
    Caption="Da percipiente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="SZHKOAZVPG",Visible=.t., Left=7, Top=84,;
    Alignment=1, Width=100, Height=15,;
    Caption="A percipiente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="ZOSLADKNPV",Visible=.t., Left=180, Top=8,;
    Alignment=1, Width=109, Height=18,;
    Caption="Da data reg.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="KRLMYGJKGY",Visible=.t., Left=387, Top=8,;
    Alignment=1, Width=82, Height=18,;
    Caption="A data reg.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="RBFVOTFEME",Visible=.t., Left=7, Top=8,;
    Alignment=1, Width=100, Height=18,;
    Caption="Competenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="MORFUDFTPZ",Visible=.t., Left=7, Top=33,;
    Alignment=1, Width=100, Height=15,;
    Caption="Utente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="MLQGOJUIOQ",Visible=.t., Left=7, Top=110,;
    Alignment=1, Width=100, Height=18,;
    Caption="Causale cont.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="OTNOGPJTPI",Visible=.t., Left=7, Top=157,;
    Alignment=1, Width=100, Height=15,;
    Caption="A tributo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="RWPUSEDSNW",Visible=.t., Left=7, Top=134,;
    Alignment=1, Width=100, Height=15,;
    Caption="Da tributo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="LLTKTLGBRZ",Visible=.t., Left=7, Top=182,;
    Alignment=1, Width=100, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsri_sdr','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
