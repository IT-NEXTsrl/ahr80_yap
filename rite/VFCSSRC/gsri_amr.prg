* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_amr                                                        *
*              Movimenti di ritenute                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_87]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-05-15                                                      *
* Last revis.: 2018-01-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- gsri_amr
PARAMETERS pTipRit
* --- Fine Area Manuale
return(createobject("tgsri_amr"))

* --- Class definition
define class tgsri_amr as StdForm
  Top    = 1
  Left   = 9

  * --- Standard Properties
  Width  = 743
  Height = 476+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2018-01-22"
  HelpContextID=210368105
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=71

  * --- Constant Properties
  MOV_RITE_IDX = 0
  CONTI_IDX = 0
  TRI_BUTI_IDX = 0
  ESERCIZI_IDX = 0
  VALUTE_IDX = 0
  VEP_RITE_IDX = 0
  VEN_RITE_IDX = 0
  TAB_RITE_IDX = 0
  DATIRITE_IDX = 0
  cFile = "MOV_RITE"
  cKeySelect = "MRSERIAL"
  cKeyWhere  = "MRSERIAL=this.w_MRSERIAL"
  cKeyWhereODBC = '"MRSERIAL="+cp_ToStrODBC(this.w_MRSERIAL)';

  cKeyWhereODBCqualified = '"MOV_RITE.MRSERIAL="+cp_ToStrODBC(this.w_MRSERIAL)';

  cPrg = "gsri_amr"
  cComment = "Movimenti di ritenute"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_TIPRIT = space(1)
  w_MRSERIAL = space(10)
  w_CODAZI1 = space(5)
  w_CODAZI = space(5)
  w_MRTIPCON = space(1)
  o_MRTIPCON = space(1)
  w_MRNUMREG = 0
  w_MRCODESE = space(4)
  o_MRCODESE = space(4)
  w_MRDATREG = ctod('  /  /  ')
  o_MRDATREG = ctod('  /  /  ')
  w_MRCODVAL = space(3)
  o_MRCODVAL = space(3)
  w_MRCAOVAL = 0
  w_MRNUMDOC = 0
  w_MRSERDOC = space(10)
  w_MRDATDOC = ctod('  /  /  ')
  o_MRDATDOC = ctod('  /  /  ')
  w_MRVALNAZ = space(3)
  w_DECTOT = 0
  w_CAOVAL = 0
  w_CALCPIC = 0
  w_FLGRIT = space(10)
  w_MRCODCON = space(15)
  o_MRCODCON = space(15)
  w_MRTIPPAR = space(1)
  w_MRDATCER = ctod('  /  /  ')
  w_MRSTACER = space(1)
  w_MRCODTRI = space(5)
  o_MRCODTRI = space(5)
  w_CAUPRE2 = space(2)
  o_CAUPRE2 = space(2)
  w_MRCAURIT = space(2)
  w_MRDATPAG = ctod('  /  /  ')
  w_MRNONSO1 = 0
  o_MRNONSO1 = 0
  w_MRIMPRI1 = 0
  o_MRIMPRI1 = 0
  w_MRPERRI1 = 0
  o_MRPERRI1 = 0
  w_NUMDI1 = 0
  w_ESEDI1 = space(4)
  w_DATDI1 = ctod('  /  /  ')
  w_MRTOTIM1 = 0
  w_FLRITE = space(1)
  o_FLRITE = space(1)
  w_RITINPS = 0
  w_MRNONSO2 = 0
  w_MRCODTR2 = space(5)
  w_DESTR2 = space(35)
  w_MRIMPRI2 = 0
  o_MRIMPRI2 = 0
  w_MRPERRI2 = 0
  o_MRPERRI2 = 0
  w_COINPS = 0
  w_MRTOTIM2 = 0
  w_MRPERPER = 0
  o_MRPERPER = 0
  w_NUMDI2 = 0
  w_ESEDI2 = space(4)
  w_DATDI2 = ctod('  /  /  ')
  w_MRIMPPER = 0
  w_MRSPERIM = 0
  w_MRRIFDI1 = space(10)
  w_DESFOR = space(40)
  w_DTOBSO = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  w_FLFOAG = space(1)
  w_MRRIFDI2 = space(10)
  w_MRRIFCON = space(10)
  w_MRRIFORD = 0
  w_MRRIFNUM = 0
  w_FLACO1 = space(1)
  w_FLACO2 = space(1)
  w_SIMVAL = space(5)
  w_FLGSTO = space(1)
  w_FLGST1 = space(1)
  w_MRFLGSTO = space(1)
  w_DESTR1 = space(60)
  w_MRGENGIR = space(10)
  w_MRCODSNS = space(1)
  w_CODSNS = space(1)
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_MRSERIAL = this.W_MRSERIAL
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_MRCODESE = this.W_MRCODESE
  op_MRNUMREG = this.W_MRNUMREG

  * --- Children pointers
  GSRI_MDP = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsri_amr
  * ---- Parametro Gestione
   * ---- A: ritenute operate
   * ---- V: ritenute subite
  pTipRit=' '
  
  * --- Per gestione sicurezza procedure con parametri
  func getSecuritycode()
    return(lower('GSRI_AMR,'+this.pTipRit))
  EndFunc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'MOV_RITE','gsri_amr')
    stdPageFrame::Init()
    *set procedure to GSRI_MDP additive
    with this
      .Pages(1).addobject("oPag","tgsri_amrPag1","gsri_amr",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Movimenti di ritenute")
      .Pages(1).HelpContextID = 84801138
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure GSRI_MDP
    * --- Area Manuale = Init Page Frame
    * --- gsri_amr
    * --- Imposta il Titolo della Finestra della Gestione in Base al tipo
    WITH THIS.PARENT
       IF Type('pTipRIT')='U' Or EMPTY(pTipRIT)
          .pTipRIT = 'A'
       ELSE
          .pTiprit = pTipRIT
       ENDIF
       DO CASE
             CASE pTipRit = 'A'
                   .cComment = CP_TRANSLATE('Movimenti ritenute operate')
                   .cAutoZoom = 'GSRIAAMR'
             CASE pTipRit = 'V'
                   .cComment = CP_TRANSLATE('Movimenti ritenute subite')
                   .cAutoZoom = 'GSRIVAMR'
       ENDCASE
    ENDWIT
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[9]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='TRI_BUTI'
    this.cWorkTables[3]='ESERCIZI'
    this.cWorkTables[4]='VALUTE'
    this.cWorkTables[5]='VEP_RITE'
    this.cWorkTables[6]='VEN_RITE'
    this.cWorkTables[7]='TAB_RITE'
    this.cWorkTables[8]='DATIRITE'
    this.cWorkTables[9]='MOV_RITE'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(9))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.MOV_RITE_IDX,5],7]
    this.nPostItConn=i_TableProp[this.MOV_RITE_IDX,3]
  return

  function CreateChildren()
    this.GSRI_MDP = CREATEOBJECT('stdLazyChild',this,'GSRI_MDP')
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSRI_MDP)
      this.GSRI_MDP.DestroyChildrenChain()
      this.GSRI_MDP=.NULL.
    endif
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSRI_MDP.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSRI_MDP.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSRI_MDP.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSRI_MDP.SetKey(;
            .w_MRSERIAL,"DPSERIAL";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSRI_MDP.ChangeRow(this.cRowID+'      1',1;
             ,.w_MRSERIAL,"DPSERIAL";
             )
    endwith
    return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_MRSERIAL = NVL(MRSERIAL,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_9_joined
    link_1_9_joined=.f.
    local link_1_19_joined
    link_1_19_joined=.f.
    local link_1_23_joined
    link_1_23_joined=.f.
    local link_1_37_joined
    link_1_37_joined=.f.
    local link_1_55_joined
    link_1_55_joined=.f.
    local link_1_70_joined
    link_1_70_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from MOV_RITE where MRSERIAL=KeySet.MRSERIAL
    *
    i_nConn = i_TableProp[this.MOV_RITE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOV_RITE_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('MOV_RITE')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "MOV_RITE.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' MOV_RITE '
      link_1_9_joined=this.AddJoinedLink_1_9(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_19_joined=this.AddJoinedLink_1_19(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_23_joined=this.AddJoinedLink_1_23(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_37_joined=this.AddJoinedLink_1_37(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_55_joined=this.AddJoinedLink_1_55(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_70_joined=this.AddJoinedLink_1_70(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'MRSERIAL',this.w_MRSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DECTOT = 0
        .w_CAOVAL = GETCAM(.w_MRCODVAL, IIF(EMPTY(.w_MRDATDOC),.w_MRDATREG,.w_MRDATDOC), 7)
        .w_FLGRIT = space(10)
        .w_CAUPRE2 = space(2)
        .w_NUMDI1 = 0
        .w_ESEDI1 = space(4)
        .w_DATDI1 = ctod("  /  /  ")
        .w_FLRITE = space(1)
        .w_RITINPS = 0
        .w_DESTR2 = space(35)
        .w_COINPS = 0
        .w_NUMDI2 = 0
        .w_ESEDI2 = space(4)
        .w_DATDI2 = ctod("  /  /  ")
        .w_DESFOR = space(40)
        .w_DTOBSO = ctod("  /  /  ")
        .w_OBTEST = i_datsys
        .w_FLFOAG = space(1)
        .w_FLACO1 = space(1)
        .w_FLACO2 = space(1)
        .w_SIMVAL = space(5)
        .w_FLGSTO = space(1)
        .w_FLGST1 = space(1)
        .w_DESTR1 = space(60)
        .w_CODSNS = space(1)
        .w_TIPRIT = THIS.pTipRit
        .w_MRSERIAL = NVL(MRSERIAL,space(10))
        .op_MRSERIAL = .w_MRSERIAL
        .w_CODAZI1 = i_CODAZI
          .link_1_3('Load')
        .w_CODAZI = i_CODAZI
        .w_MRTIPCON = NVL(MRTIPCON,space(1))
        .w_MRNUMREG = NVL(MRNUMREG,0)
        .op_MRNUMREG = .w_MRNUMREG
        .w_MRCODESE = NVL(MRCODESE,space(4))
        .op_MRCODESE = .w_MRCODESE
          * evitabile
          *.link_1_7('Load')
        .w_MRDATREG = NVL(cp_ToDate(MRDATREG),ctod("  /  /  "))
        .w_MRCODVAL = NVL(MRCODVAL,space(3))
          if link_1_9_joined
            this.w_MRCODVAL = NVL(VACODVAL109,NVL(this.w_MRCODVAL,space(3)))
            this.w_DECTOT = NVL(VADECTOT109,0)
            this.w_SIMVAL = NVL(VASIMVAL109,space(5))
          else
          .link_1_9('Load')
          endif
        .w_MRCAOVAL = NVL(MRCAOVAL,0)
        .w_MRNUMDOC = NVL(MRNUMDOC,0)
        .w_MRSERDOC = NVL(MRSERDOC,space(10))
        .w_MRDATDOC = NVL(cp_ToDate(MRDATDOC),ctod("  /  /  "))
        .w_MRVALNAZ = NVL(MRVALNAZ,space(3))
        .w_CALCPIC = DEFPIC(.w_DECTOT)
        .w_MRCODCON = NVL(MRCODCON,space(15))
          if link_1_19_joined
            this.w_MRCODCON = NVL(ANCODICE119,NVL(this.w_MRCODCON,space(15)))
            this.w_DESFOR = NVL(ANDESCRI119,space(40))
            this.w_DTOBSO = NVL(cp_ToDate(ANDTOBSO119),ctod("  /  /  "))
            this.w_FLRITE = NVL(ANRITENU119,space(1))
            this.w_FLFOAG = NVL(ANTIPCLF119,space(1))
            this.w_RITINPS = NVL(ANRIINPS119,0)
            this.w_FLGRIT = NVL(ANFLRITE119,space(10))
            this.w_COINPS = NVL(ANCOINPS119,0)
            this.w_CODSNS = NVL(ANCODSNS119,space(1))
          else
          .link_1_19('Load')
          endif
        .w_MRTIPPAR = NVL(MRTIPPAR,space(1))
        .w_MRDATCER = NVL(cp_ToDate(MRDATCER),ctod("  /  /  "))
        .w_MRSTACER = NVL(MRSTACER,space(1))
        .w_MRCODTRI = NVL(MRCODTRI,space(5))
          if link_1_23_joined
            this.w_MRCODTRI = NVL(TRCODTRI123,NVL(this.w_MRCODTRI,space(5)))
            this.w_DESTR1 = NVL(TRDESTRI123,space(60))
            this.w_FLACO1 = NVL(TRFLACON123,space(1))
            this.w_CAUPRE2 = NVL(TRCAUPRE2123,space(2))
            this.w_MRCAURIT = NVL(TRCAUPRE2123,space(2))
          else
          .link_1_23('Load')
          endif
        .w_MRCAURIT = NVL(MRCAURIT,space(2))
        .w_MRDATPAG = NVL(cp_ToDate(MRDATPAG),ctod("  /  /  "))
        .w_MRNONSO1 = NVL(MRNONSO1,0)
        .w_MRIMPRI1 = NVL(MRIMPRI1,0)
        .w_MRPERRI1 = NVL(MRPERRI1,0)
        .w_MRTOTIM1 = NVL(MRTOTIM1,0)
        .w_MRNONSO2 = NVL(MRNONSO2,0)
        .w_MRCODTR2 = NVL(MRCODTR2,space(5))
          if link_1_37_joined
            this.w_MRCODTR2 = NVL(TRCODTRI137,NVL(this.w_MRCODTR2,space(5)))
            this.w_DESTR2 = NVL(TRDESTRI137,space(35))
            this.w_FLACO2 = NVL(TRFLACON137,space(1))
          else
          .link_1_37('Load')
          endif
        .w_MRIMPRI2 = NVL(MRIMPRI2,0)
        .w_MRPERRI2 = NVL(MRPERRI2,0)
        .w_MRTOTIM2 = NVL(MRTOTIM2,0)
        .w_MRPERPER = NVL(MRPERPER,0)
        .w_MRIMPPER = NVL(MRIMPPER,0)
        .w_MRSPERIM = NVL(MRSPERIM,0)
        .w_MRRIFDI1 = NVL(MRRIFDI1,space(10))
          if link_1_55_joined
            this.w_MRRIFDI1 = NVL(VPSERIAL155,NVL(this.w_MRRIFDI1,space(10)))
            this.w_NUMDI1 = NVL(VPNUMREG155,0)
            this.w_ESEDI1 = NVL(VPCODESE155,space(4))
            this.w_DATDI1 = NVL(cp_ToDate(VPDATREG155),ctod("  /  /  "))
          else
          .link_1_55('Load')
          endif
        .w_MRRIFDI2 = NVL(MRRIFDI2,space(10))
          if link_1_70_joined
            this.w_MRRIFDI2 = NVL(VPSERIAL170,NVL(this.w_MRRIFDI2,space(10)))
            this.w_NUMDI2 = NVL(VPNUMREG170,0)
            this.w_ESEDI2 = NVL(VPCODESE170,space(4))
            this.w_DATDI2 = NVL(cp_ToDate(VPDATREG170),ctod("  /  /  "))
          else
          .link_1_70('Load')
          endif
        .w_MRRIFCON = NVL(MRRIFCON,space(10))
        .w_MRRIFORD = NVL(MRRIFORD,0)
        .w_MRRIFNUM = NVL(MRRIFNUM,0)
        .oPgFrm.Page1.oPag.oObj_1_78.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_83.Calculate()
        .w_MRFLGSTO = NVL(MRFLGSTO,space(1))
        .w_MRGENGIR = NVL(MRGENGIR,space(10))
        .w_MRCODSNS = NVL(MRCODSNS,space(1))
        .oPgFrm.Page1.oPag.oObj_1_111.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_112.Calculate()
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'MOV_RITE')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_77.enabled = this.oPgFrm.Page1.oPag.oBtn_1_77.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIPRIT = space(1)
      .w_MRSERIAL = space(10)
      .w_CODAZI1 = space(5)
      .w_CODAZI = space(5)
      .w_MRTIPCON = space(1)
      .w_MRNUMREG = 0
      .w_MRCODESE = space(4)
      .w_MRDATREG = ctod("  /  /  ")
      .w_MRCODVAL = space(3)
      .w_MRCAOVAL = 0
      .w_MRNUMDOC = 0
      .w_MRSERDOC = space(10)
      .w_MRDATDOC = ctod("  /  /  ")
      .w_MRVALNAZ = space(3)
      .w_DECTOT = 0
      .w_CAOVAL = 0
      .w_CALCPIC = 0
      .w_FLGRIT = space(10)
      .w_MRCODCON = space(15)
      .w_MRTIPPAR = space(1)
      .w_MRDATCER = ctod("  /  /  ")
      .w_MRSTACER = space(1)
      .w_MRCODTRI = space(5)
      .w_CAUPRE2 = space(2)
      .w_MRCAURIT = space(2)
      .w_MRDATPAG = ctod("  /  /  ")
      .w_MRNONSO1 = 0
      .w_MRIMPRI1 = 0
      .w_MRPERRI1 = 0
      .w_NUMDI1 = 0
      .w_ESEDI1 = space(4)
      .w_DATDI1 = ctod("  /  /  ")
      .w_MRTOTIM1 = 0
      .w_FLRITE = space(1)
      .w_RITINPS = 0
      .w_MRNONSO2 = 0
      .w_MRCODTR2 = space(5)
      .w_DESTR2 = space(35)
      .w_MRIMPRI2 = 0
      .w_MRPERRI2 = 0
      .w_COINPS = 0
      .w_MRTOTIM2 = 0
      .w_MRPERPER = 0
      .w_NUMDI2 = 0
      .w_ESEDI2 = space(4)
      .w_DATDI2 = ctod("  /  /  ")
      .w_MRIMPPER = 0
      .w_MRSPERIM = 0
      .w_MRRIFDI1 = space(10)
      .w_DESFOR = space(40)
      .w_DTOBSO = ctod("  /  /  ")
      .w_OBTEST = ctod("  /  /  ")
      .w_FLFOAG = space(1)
      .w_MRRIFDI2 = space(10)
      .w_MRRIFCON = space(10)
      .w_MRRIFORD = 0
      .w_MRRIFNUM = 0
      .w_FLACO1 = space(1)
      .w_FLACO2 = space(1)
      .w_SIMVAL = space(5)
      .w_FLGSTO = space(1)
      .w_FLGST1 = space(1)
      .w_MRFLGSTO = space(1)
      .w_DESTR1 = space(60)
      .w_MRGENGIR = space(10)
      .w_MRCODSNS = space(1)
      .w_CODSNS = space(1)
      .w_UTCC = 0
      .w_UTCV = 0
      .w_UTDC = ctot("")
      .w_UTDV = ctot("")
      if .cFunction<>"Filter"
        .w_TIPRIT = THIS.pTipRit
          .DoRTCalc(2,2,.f.)
        .w_CODAZI1 = i_CODAZI
        .DoRTCalc(3,3,.f.)
          if not(empty(.w_CODAZI1))
          .link_1_3('Full')
          endif
        .w_CODAZI = i_CODAZI
        .w_MRTIPCON = IIF(THIS.pTipRit='A','F','C')
          .DoRTCalc(6,6,.f.)
        .w_MRCODESE = g_CODESE
        .DoRTCalc(7,7,.f.)
          if not(empty(.w_MRCODESE))
          .link_1_7('Full')
          endif
        .w_MRDATREG = i_datsys
        .DoRTCalc(9,9,.f.)
          if not(empty(.w_MRCODVAL))
          .link_1_9('Full')
          endif
        .w_MRCAOVAL = GETCAM(.w_MRCODVAL, IIF(EMPTY(.w_MRDATDOC),.w_MRDATREG,.w_MRDATDOC), 7)
          .DoRTCalc(11,15,.f.)
        .w_CAOVAL = GETCAM(.w_MRCODVAL, IIF(EMPTY(.w_MRDATDOC),.w_MRDATREG,.w_MRDATDOC), 7)
        .w_CALCPIC = DEFPIC(.w_DECTOT)
        .DoRTCalc(18,19,.f.)
          if not(empty(.w_MRCODCON))
          .link_1_19('Full')
          endif
        .w_MRTIPPAR = ' '
          .DoRTCalc(21,21,.f.)
        .w_MRSTACER = 'N'
        .DoRTCalc(23,23,.f.)
          if not(empty(.w_MRCODTRI))
          .link_1_23('Full')
          endif
          .DoRTCalc(24,24,.f.)
        .w_MRCAURIT = .w_CAUPRE2
          .DoRTCalc(26,32,.f.)
        .w_MRTOTIM1 = cp_ROUND((.w_MRIMPRI1*.w_MRPERRI1)/100, .w_DECTOT)
        .DoRTCalc(34,37,.f.)
          if not(empty(.w_MRCODTR2))
          .link_1_37('Full')
          endif
          .DoRTCalc(38,39,.f.)
        .w_MRPERRI2 = iif(.w_FLRITE='C',.w_RITINPS,0)
          .DoRTCalc(41,41,.f.)
        .w_MRTOTIM2 = cp_ROUND((.w_MRIMPRI2*.w_MRPERRI2)/100, .w_DECTOT)
        .w_MRPERPER = iif(.w_FLRITE='C',.w_COINPS,0)
          .DoRTCalc(44,46,.f.)
        .w_MRIMPPER = cp_ROUND((.w_MRIMPRI2*.w_MRPERPER)/100, .w_DECTOT)
        .DoRTCalc(48,49,.f.)
          if not(empty(.w_MRRIFDI1))
          .link_1_55('Full')
          endif
          .DoRTCalc(50,51,.f.)
        .w_OBTEST = i_datsys
        .DoRTCalc(53,54,.f.)
          if not(empty(.w_MRRIFDI2))
          .link_1_70('Full')
          endif
        .oPgFrm.Page1.oPag.oObj_1_78.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_83.Calculate()
          .DoRTCalc(55,62,.f.)
        .w_MRFLGSTO = iif(.w_TIPRIT='A',.w_FLGSTO,.w_FLGST1)
          .DoRTCalc(64,65,.f.)
        .w_MRCODSNS = IIF(.w_MRTIPCON='C' OR .w_MRNONSO1=0,' ',IIF(NOT EMPTY(.w_MRCODSNS),.w_MRCODSNS,IIF(EMPTY(.w_CODSNS),'7',.w_CODSNS)))
        .oPgFrm.Page1.oPag.oObj_1_111.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_112.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'MOV_RITE')
    this.DoRTCalc(67,71,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_77.enabled = this.oPgFrm.Page1.oPag.oBtn_1_77.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.MOV_RITE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOV_RITE_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"SEMOVRIT","i_codazi,w_MRSERIAL")
      cp_AskTableProg(this,i_nConn,"PRMOVRIT","i_codazi,w_MRCODESE,w_MRNUMREG")
      .op_codazi = .w_codazi
      .op_MRSERIAL = .w_MRSERIAL
      .op_codazi = .w_codazi
      .op_MRCODESE = .w_MRCODESE
      .op_MRNUMREG = .w_MRNUMREG
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oMRNUMREG_1_6.enabled = i_bVal
      .Page1.oPag.oMRCODESE_1_7.enabled = i_bVal
      .Page1.oPag.oMRDATREG_1_8.enabled = i_bVal
      .Page1.oPag.oMRCODVAL_1_9.enabled = i_bVal
      .Page1.oPag.oMRCAOVAL_1_10.enabled = i_bVal
      .Page1.oPag.oMRNUMDOC_1_11.enabled = i_bVal
      .Page1.oPag.oMRSERDOC_1_12.enabled = i_bVal
      .Page1.oPag.oMRDATDOC_1_13.enabled = i_bVal
      .Page1.oPag.oMRCODCON_1_19.enabled = i_bVal
      .Page1.oPag.oMRTIPPAR_1_20.enabled = i_bVal
      .Page1.oPag.oMRDATCER_1_21.enabled = i_bVal
      .Page1.oPag.oMRSTACER_1_22.enabled = i_bVal
      .Page1.oPag.oMRCODTRI_1_23.enabled = i_bVal
      .Page1.oPag.oMRCAURIT_1_25.enabled = i_bVal
      .Page1.oPag.oMRDATPAG_1_26.enabled = i_bVal
      .Page1.oPag.oMRNONSO1_1_27.enabled = i_bVal
      .Page1.oPag.oMRIMPRI1_1_28.enabled = i_bVal
      .Page1.oPag.oMRPERRI1_1_29.enabled = i_bVal
      .Page1.oPag.oMRTOTIM1_1_33.enabled = i_bVal
      .Page1.oPag.oMRNONSO2_1_36.enabled = i_bVal
      .Page1.oPag.oMRCODTR2_1_37.enabled = i_bVal
      .Page1.oPag.oMRIMPRI2_1_39.enabled = i_bVal
      .Page1.oPag.oMRPERRI2_1_40.enabled = i_bVal
      .Page1.oPag.oMRTOTIM2_1_42.enabled = i_bVal
      .Page1.oPag.oMRPERPER_1_43.enabled = i_bVal
      .Page1.oPag.oMRIMPPER_1_47.enabled = i_bVal
      .Page1.oPag.oMRSPERIM_1_48.enabled = i_bVal
      .Page1.oPag.oMRCODSNS_1_108.enabled = i_bVal
      .Page1.oPag.oBtn_1_77.enabled = .Page1.oPag.oBtn_1_77.mCond()
      .Page1.oPag.oObj_1_78.enabled = i_bVal
      .Page1.oPag.oObj_1_83.enabled = i_bVal
      .Page1.oPag.oObj_1_111.enabled = i_bVal
      .Page1.oPag.oObj_1_112.enabled = i_bVal
    endwith
    this.GSRI_MDP.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'MOV_RITE',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSRI_MDP.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.MOV_RITE_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MRSERIAL,"MRSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MRTIPCON,"MRTIPCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MRNUMREG,"MRNUMREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MRCODESE,"MRCODESE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MRDATREG,"MRDATREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MRCODVAL,"MRCODVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MRCAOVAL,"MRCAOVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MRNUMDOC,"MRNUMDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MRSERDOC,"MRSERDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MRDATDOC,"MRDATDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MRVALNAZ,"MRVALNAZ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MRCODCON,"MRCODCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MRTIPPAR,"MRTIPPAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MRDATCER,"MRDATCER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MRSTACER,"MRSTACER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MRCODTRI,"MRCODTRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MRCAURIT,"MRCAURIT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MRDATPAG,"MRDATPAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MRNONSO1,"MRNONSO1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MRIMPRI1,"MRIMPRI1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MRPERRI1,"MRPERRI1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MRTOTIM1,"MRTOTIM1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MRNONSO2,"MRNONSO2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MRCODTR2,"MRCODTR2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MRIMPRI2,"MRIMPRI2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MRPERRI2,"MRPERRI2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MRTOTIM2,"MRTOTIM2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MRPERPER,"MRPERPER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MRIMPPER,"MRIMPPER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MRSPERIM,"MRSPERIM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MRRIFDI1,"MRRIFDI1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MRRIFDI2,"MRRIFDI2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MRRIFCON,"MRRIFCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MRRIFORD,"MRRIFORD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MRRIFNUM,"MRRIFNUM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MRFLGSTO,"MRFLGSTO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MRGENGIR,"MRGENGIR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MRCODSNS,"MRCODSNS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.MOV_RITE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOV_RITE_IDX,2])
    i_lTable = "MOV_RITE"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.MOV_RITE_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      with this
        GSRI_BDM(this,"S")
      endwith
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.MOV_RITE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MOV_RITE_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.MOV_RITE_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"SEMOVRIT","i_codazi,w_MRSERIAL")
          cp_NextTableProg(this,i_nConn,"PRMOVRIT","i_codazi,w_MRCODESE,w_MRNUMREG")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into MOV_RITE
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'MOV_RITE')
        i_extval=cp_InsertValODBCExtFlds(this,'MOV_RITE')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(MRSERIAL,MRTIPCON,MRNUMREG,MRCODESE,MRDATREG"+;
                  ",MRCODVAL,MRCAOVAL,MRNUMDOC,MRSERDOC,MRDATDOC"+;
                  ",MRVALNAZ,MRCODCON,MRTIPPAR,MRDATCER,MRSTACER"+;
                  ",MRCODTRI,MRCAURIT,MRDATPAG,MRNONSO1,MRIMPRI1"+;
                  ",MRPERRI1,MRTOTIM1,MRNONSO2,MRCODTR2,MRIMPRI2"+;
                  ",MRPERRI2,MRTOTIM2,MRPERPER,MRIMPPER,MRSPERIM"+;
                  ",MRRIFDI1,MRRIFDI2,MRRIFCON,MRRIFORD,MRRIFNUM"+;
                  ",MRFLGSTO,MRGENGIR,MRCODSNS,UTCC,UTCV"+;
                  ",UTDC,UTDV "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_MRSERIAL)+;
                  ","+cp_ToStrODBC(this.w_MRTIPCON)+;
                  ","+cp_ToStrODBC(this.w_MRNUMREG)+;
                  ","+cp_ToStrODBCNull(this.w_MRCODESE)+;
                  ","+cp_ToStrODBC(this.w_MRDATREG)+;
                  ","+cp_ToStrODBCNull(this.w_MRCODVAL)+;
                  ","+cp_ToStrODBC(this.w_MRCAOVAL)+;
                  ","+cp_ToStrODBC(this.w_MRNUMDOC)+;
                  ","+cp_ToStrODBC(this.w_MRSERDOC)+;
                  ","+cp_ToStrODBC(this.w_MRDATDOC)+;
                  ","+cp_ToStrODBC(this.w_MRVALNAZ)+;
                  ","+cp_ToStrODBCNull(this.w_MRCODCON)+;
                  ","+cp_ToStrODBC(this.w_MRTIPPAR)+;
                  ","+cp_ToStrODBC(this.w_MRDATCER)+;
                  ","+cp_ToStrODBC(this.w_MRSTACER)+;
                  ","+cp_ToStrODBCNull(this.w_MRCODTRI)+;
                  ","+cp_ToStrODBC(this.w_MRCAURIT)+;
                  ","+cp_ToStrODBC(this.w_MRDATPAG)+;
                  ","+cp_ToStrODBC(this.w_MRNONSO1)+;
                  ","+cp_ToStrODBC(this.w_MRIMPRI1)+;
                  ","+cp_ToStrODBC(this.w_MRPERRI1)+;
                  ","+cp_ToStrODBC(this.w_MRTOTIM1)+;
                  ","+cp_ToStrODBC(this.w_MRNONSO2)+;
                  ","+cp_ToStrODBCNull(this.w_MRCODTR2)+;
                  ","+cp_ToStrODBC(this.w_MRIMPRI2)+;
                  ","+cp_ToStrODBC(this.w_MRPERRI2)+;
                  ","+cp_ToStrODBC(this.w_MRTOTIM2)+;
                  ","+cp_ToStrODBC(this.w_MRPERPER)+;
                  ","+cp_ToStrODBC(this.w_MRIMPPER)+;
                  ","+cp_ToStrODBC(this.w_MRSPERIM)+;
                  ","+cp_ToStrODBCNull(this.w_MRRIFDI1)+;
                  ","+cp_ToStrODBCNull(this.w_MRRIFDI2)+;
                  ","+cp_ToStrODBC(this.w_MRRIFCON)+;
                  ","+cp_ToStrODBC(this.w_MRRIFORD)+;
                  ","+cp_ToStrODBC(this.w_MRRIFNUM)+;
                  ","+cp_ToStrODBC(this.w_MRFLGSTO)+;
                  ","+cp_ToStrODBC(this.w_MRGENGIR)+;
                  ","+cp_ToStrODBC(this.w_MRCODSNS)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'MOV_RITE')
        i_extval=cp_InsertValVFPExtFlds(this,'MOV_RITE')
        cp_CheckDeletedKey(i_cTable,0,'MRSERIAL',this.w_MRSERIAL)
        INSERT INTO (i_cTable);
              (MRSERIAL,MRTIPCON,MRNUMREG,MRCODESE,MRDATREG,MRCODVAL,MRCAOVAL,MRNUMDOC,MRSERDOC,MRDATDOC,MRVALNAZ,MRCODCON,MRTIPPAR,MRDATCER,MRSTACER,MRCODTRI,MRCAURIT,MRDATPAG,MRNONSO1,MRIMPRI1,MRPERRI1,MRTOTIM1,MRNONSO2,MRCODTR2,MRIMPRI2,MRPERRI2,MRTOTIM2,MRPERPER,MRIMPPER,MRSPERIM,MRRIFDI1,MRRIFDI2,MRRIFCON,MRRIFORD,MRRIFNUM,MRFLGSTO,MRGENGIR,MRCODSNS,UTCC,UTCV,UTDC,UTDV  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_MRSERIAL;
                  ,this.w_MRTIPCON;
                  ,this.w_MRNUMREG;
                  ,this.w_MRCODESE;
                  ,this.w_MRDATREG;
                  ,this.w_MRCODVAL;
                  ,this.w_MRCAOVAL;
                  ,this.w_MRNUMDOC;
                  ,this.w_MRSERDOC;
                  ,this.w_MRDATDOC;
                  ,this.w_MRVALNAZ;
                  ,this.w_MRCODCON;
                  ,this.w_MRTIPPAR;
                  ,this.w_MRDATCER;
                  ,this.w_MRSTACER;
                  ,this.w_MRCODTRI;
                  ,this.w_MRCAURIT;
                  ,this.w_MRDATPAG;
                  ,this.w_MRNONSO1;
                  ,this.w_MRIMPRI1;
                  ,this.w_MRPERRI1;
                  ,this.w_MRTOTIM1;
                  ,this.w_MRNONSO2;
                  ,this.w_MRCODTR2;
                  ,this.w_MRIMPRI2;
                  ,this.w_MRPERRI2;
                  ,this.w_MRTOTIM2;
                  ,this.w_MRPERPER;
                  ,this.w_MRIMPPER;
                  ,this.w_MRSPERIM;
                  ,this.w_MRRIFDI1;
                  ,this.w_MRRIFDI2;
                  ,this.w_MRRIFCON;
                  ,this.w_MRRIFORD;
                  ,this.w_MRRIFNUM;
                  ,this.w_MRFLGSTO;
                  ,this.w_MRGENGIR;
                  ,this.w_MRCODSNS;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.MOV_RITE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MOV_RITE_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.MOV_RITE_IDX,i_nConn)
      *
      * update MOV_RITE
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'MOV_RITE')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " MRTIPCON="+cp_ToStrODBC(this.w_MRTIPCON)+;
             ",MRNUMREG="+cp_ToStrODBC(this.w_MRNUMREG)+;
             ",MRCODESE="+cp_ToStrODBCNull(this.w_MRCODESE)+;
             ",MRDATREG="+cp_ToStrODBC(this.w_MRDATREG)+;
             ",MRCODVAL="+cp_ToStrODBCNull(this.w_MRCODVAL)+;
             ",MRCAOVAL="+cp_ToStrODBC(this.w_MRCAOVAL)+;
             ",MRNUMDOC="+cp_ToStrODBC(this.w_MRNUMDOC)+;
             ",MRSERDOC="+cp_ToStrODBC(this.w_MRSERDOC)+;
             ",MRDATDOC="+cp_ToStrODBC(this.w_MRDATDOC)+;
             ",MRVALNAZ="+cp_ToStrODBC(this.w_MRVALNAZ)+;
             ",MRCODCON="+cp_ToStrODBCNull(this.w_MRCODCON)+;
             ",MRTIPPAR="+cp_ToStrODBC(this.w_MRTIPPAR)+;
             ",MRDATCER="+cp_ToStrODBC(this.w_MRDATCER)+;
             ",MRSTACER="+cp_ToStrODBC(this.w_MRSTACER)+;
             ",MRCODTRI="+cp_ToStrODBCNull(this.w_MRCODTRI)+;
             ",MRCAURIT="+cp_ToStrODBC(this.w_MRCAURIT)+;
             ",MRDATPAG="+cp_ToStrODBC(this.w_MRDATPAG)+;
             ",MRNONSO1="+cp_ToStrODBC(this.w_MRNONSO1)+;
             ",MRIMPRI1="+cp_ToStrODBC(this.w_MRIMPRI1)+;
             ",MRPERRI1="+cp_ToStrODBC(this.w_MRPERRI1)+;
             ",MRTOTIM1="+cp_ToStrODBC(this.w_MRTOTIM1)+;
             ",MRNONSO2="+cp_ToStrODBC(this.w_MRNONSO2)+;
             ",MRCODTR2="+cp_ToStrODBCNull(this.w_MRCODTR2)+;
             ",MRIMPRI2="+cp_ToStrODBC(this.w_MRIMPRI2)+;
             ",MRPERRI2="+cp_ToStrODBC(this.w_MRPERRI2)+;
             ",MRTOTIM2="+cp_ToStrODBC(this.w_MRTOTIM2)+;
             ",MRPERPER="+cp_ToStrODBC(this.w_MRPERPER)+;
             ",MRIMPPER="+cp_ToStrODBC(this.w_MRIMPPER)+;
             ",MRSPERIM="+cp_ToStrODBC(this.w_MRSPERIM)+;
             ",MRRIFDI1="+cp_ToStrODBCNull(this.w_MRRIFDI1)+;
             ",MRRIFDI2="+cp_ToStrODBCNull(this.w_MRRIFDI2)+;
             ",MRRIFCON="+cp_ToStrODBC(this.w_MRRIFCON)+;
             ",MRRIFORD="+cp_ToStrODBC(this.w_MRRIFORD)+;
             ",MRRIFNUM="+cp_ToStrODBC(this.w_MRRIFNUM)+;
             ",MRFLGSTO="+cp_ToStrODBC(this.w_MRFLGSTO)+;
             ",MRGENGIR="+cp_ToStrODBC(this.w_MRGENGIR)+;
             ",MRCODSNS="+cp_ToStrODBC(this.w_MRCODSNS)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'MOV_RITE')
        i_cWhere = cp_PKFox(i_cTable  ,'MRSERIAL',this.w_MRSERIAL  )
        UPDATE (i_cTable) SET;
              MRTIPCON=this.w_MRTIPCON;
             ,MRNUMREG=this.w_MRNUMREG;
             ,MRCODESE=this.w_MRCODESE;
             ,MRDATREG=this.w_MRDATREG;
             ,MRCODVAL=this.w_MRCODVAL;
             ,MRCAOVAL=this.w_MRCAOVAL;
             ,MRNUMDOC=this.w_MRNUMDOC;
             ,MRSERDOC=this.w_MRSERDOC;
             ,MRDATDOC=this.w_MRDATDOC;
             ,MRVALNAZ=this.w_MRVALNAZ;
             ,MRCODCON=this.w_MRCODCON;
             ,MRTIPPAR=this.w_MRTIPPAR;
             ,MRDATCER=this.w_MRDATCER;
             ,MRSTACER=this.w_MRSTACER;
             ,MRCODTRI=this.w_MRCODTRI;
             ,MRCAURIT=this.w_MRCAURIT;
             ,MRDATPAG=this.w_MRDATPAG;
             ,MRNONSO1=this.w_MRNONSO1;
             ,MRIMPRI1=this.w_MRIMPRI1;
             ,MRPERRI1=this.w_MRPERRI1;
             ,MRTOTIM1=this.w_MRTOTIM1;
             ,MRNONSO2=this.w_MRNONSO2;
             ,MRCODTR2=this.w_MRCODTR2;
             ,MRIMPRI2=this.w_MRIMPRI2;
             ,MRPERRI2=this.w_MRPERRI2;
             ,MRTOTIM2=this.w_MRTOTIM2;
             ,MRPERPER=this.w_MRPERPER;
             ,MRIMPPER=this.w_MRIMPPER;
             ,MRSPERIM=this.w_MRSPERIM;
             ,MRRIFDI1=this.w_MRRIFDI1;
             ,MRRIFDI2=this.w_MRRIFDI2;
             ,MRRIFCON=this.w_MRRIFCON;
             ,MRRIFORD=this.w_MRRIFORD;
             ,MRRIFNUM=this.w_MRRIFNUM;
             ,MRFLGSTO=this.w_MRFLGSTO;
             ,MRGENGIR=this.w_MRGENGIR;
             ,MRCODSNS=this.w_MRCODSNS;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSRI_MDP : Saving
      this.GSRI_MDP.ChangeRow(this.cRowID+'      1',0;
             ,this.w_MRSERIAL,"DPSERIAL";
             )
      this.GSRI_MDP.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSRI_MDP : Deleting
    this.GSRI_MDP.ChangeRow(this.cRowID+'      1',0;
           ,this.w_MRSERIAL,"DPSERIAL";
           )
    this.GSRI_MDP.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.MOV_RITE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MOV_RITE_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.MOV_RITE_IDX,i_nConn)
      *
      * delete MOV_RITE
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'MRSERIAL',this.w_MRSERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.MOV_RITE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOV_RITE_IDX,2])
    if i_bUpd
      with this
            .w_TIPRIT = THIS.pTipRit
        .DoRTCalc(2,2,.t.)
            .w_CODAZI1 = i_CODAZI
          .link_1_3('Full')
            .w_CODAZI = i_CODAZI
            .w_MRTIPCON = IIF(THIS.pTipRit='A','F','C')
        .DoRTCalc(6,8,.t.)
        if .o_MRCODESE<>.w_MRCODESE
          .link_1_9('Full')
        endif
        if .o_MRCODVAL<>.w_MRCODVAL.or. .o_MRDATREG<>.w_MRDATREG.or. .o_MRDATDOC<>.w_MRDATDOC
            .w_MRCAOVAL = GETCAM(.w_MRCODVAL, IIF(EMPTY(.w_MRDATDOC),.w_MRDATREG,.w_MRDATDOC), 7)
        endif
        .DoRTCalc(11,16,.t.)
        if .o_MRCODESE<>.w_MRCODESE.or. .o_MRCODVAL<>.w_MRCODVAL
            .w_CALCPIC = DEFPIC(.w_DECTOT)
        endif
        .DoRTCalc(18,24,.t.)
        if .o_MRCODTRI<>.w_MRCODTRI.or. .o_CAUPRE2<>.w_CAUPRE2
            .w_MRCAURIT = .w_CAUPRE2
        endif
        .DoRTCalc(26,32,.t.)
        if .o_MRIMPRI1<>.w_MRIMPRI1.or. .o_MRPERRI1<>.w_MRPERRI1
            .w_MRTOTIM1 = cp_ROUND((.w_MRIMPRI1*.w_MRPERRI1)/100, .w_DECTOT)
        endif
        .DoRTCalc(34,39,.t.)
        if .o_FLRITE<>.w_FLRITE
            .w_MRPERRI2 = iif(.w_FLRITE='C',.w_RITINPS,0)
        endif
        .DoRTCalc(41,41,.t.)
        if .o_MRIMPRI2<>.w_MRIMPRI2.or. .o_MRPERRI2<>.w_MRPERRI2
            .w_MRTOTIM2 = cp_ROUND((.w_MRIMPRI2*.w_MRPERRI2)/100, .w_DECTOT)
        endif
        if .o_FLRITE<>.w_FLRITE
            .w_MRPERPER = iif(.w_FLRITE='C',.w_COINPS,0)
        endif
        .DoRTCalc(44,46,.t.)
        if .o_MRPERPER<>.w_MRPERPER.or. .o_MRIMPRI2<>.w_MRIMPRI2
            .w_MRIMPPER = cp_ROUND((.w_MRIMPRI2*.w_MRPERPER)/100, .w_DECTOT)
        endif
        .DoRTCalc(48,48,.t.)
          .link_1_55('Full')
        .DoRTCalc(50,53,.t.)
          .link_1_70('Full')
        .oPgFrm.Page1.oPag.oObj_1_78.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_83.Calculate()
        .DoRTCalc(55,62,.t.)
            .w_MRFLGSTO = iif(.w_TIPRIT='A',.w_FLGSTO,.w_FLGST1)
        .DoRTCalc(64,65,.t.)
        if .o_MRTIPCON<>.w_MRTIPCON.or. .o_MRNONSO1<>.w_MRNONSO1.or. .o_MRCODCON<>.w_MRCODCON
            .w_MRCODSNS = IIF(.w_MRTIPCON='C' OR .w_MRNONSO1=0,' ',IIF(NOT EMPTY(.w_MRCODSNS),.w_MRCODSNS,IIF(EMPTY(.w_CODSNS),'7',.w_CODSNS)))
        endif
        .oPgFrm.Page1.oPag.oObj_1_111.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_112.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_codazi<>.w_codazi
           cp_AskTableProg(this,i_nConn,"SEMOVRIT","i_codazi,w_MRSERIAL")
          .op_MRSERIAL = .w_MRSERIAL
        endif
        if .op_codazi<>.w_codazi .or. .op_MRCODESE<>.w_MRCODESE
           cp_AskTableProg(this,i_nConn,"PRMOVRIT","i_codazi,w_MRCODESE,w_MRNUMREG")
          .op_MRNUMREG = .w_MRNUMREG
        endif
        .op_codazi = .w_codazi
        .op_codazi = .w_codazi
        .op_MRCODESE = .w_MRCODESE
      endwith
      this.DoRTCalc(67,71,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_78.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_83.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_111.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_112.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oMRCAOVAL_1_10.enabled = this.oPgFrm.Page1.oPag.oMRCAOVAL_1_10.mCond()
    this.oPgFrm.Page1.oPag.oMRCAURIT_1_25.enabled = this.oPgFrm.Page1.oPag.oMRCAURIT_1_25.mCond()
    this.oPgFrm.Page1.oPag.oMRNONSO2_1_36.enabled = this.oPgFrm.Page1.oPag.oMRNONSO2_1_36.mCond()
    this.oPgFrm.Page1.oPag.oMRCODTR2_1_37.enabled = this.oPgFrm.Page1.oPag.oMRCODTR2_1_37.mCond()
    this.oPgFrm.Page1.oPag.oMRIMPRI2_1_39.enabled = this.oPgFrm.Page1.oPag.oMRIMPRI2_1_39.mCond()
    this.oPgFrm.Page1.oPag.oMRPERRI2_1_40.enabled = this.oPgFrm.Page1.oPag.oMRPERRI2_1_40.mCond()
    this.oPgFrm.Page1.oPag.oMRTOTIM2_1_42.enabled = this.oPgFrm.Page1.oPag.oMRTOTIM2_1_42.mCond()
    this.oPgFrm.Page1.oPag.oMRPERPER_1_43.enabled = this.oPgFrm.Page1.oPag.oMRPERPER_1_43.mCond()
    this.oPgFrm.Page1.oPag.oMRIMPPER_1_47.enabled = this.oPgFrm.Page1.oPag.oMRIMPPER_1_47.mCond()
    this.oPgFrm.Page1.oPag.oMRSPERIM_1_48.enabled = this.oPgFrm.Page1.oPag.oMRSPERIM_1_48.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oMRCAOVAL_1_10.visible=!this.oPgFrm.Page1.oPag.oMRCAOVAL_1_10.mHide()
    this.oPgFrm.Page1.oPag.oMRDATCER_1_21.visible=!this.oPgFrm.Page1.oPag.oMRDATCER_1_21.mHide()
    this.oPgFrm.Page1.oPag.oMRSTACER_1_22.visible=!this.oPgFrm.Page1.oPag.oMRSTACER_1_22.mHide()
    this.oPgFrm.Page1.oPag.oMRDATPAG_1_26.visible=!this.oPgFrm.Page1.oPag.oMRDATPAG_1_26.mHide()
    this.oPgFrm.Page1.oPag.oNUMDI1_1_30.visible=!this.oPgFrm.Page1.oPag.oNUMDI1_1_30.mHide()
    this.oPgFrm.Page1.oPag.oESEDI1_1_31.visible=!this.oPgFrm.Page1.oPag.oESEDI1_1_31.mHide()
    this.oPgFrm.Page1.oPag.oDATDI1_1_32.visible=!this.oPgFrm.Page1.oPag.oDATDI1_1_32.mHide()
    this.oPgFrm.Page1.oPag.oNUMDI2_1_44.visible=!this.oPgFrm.Page1.oPag.oNUMDI2_1_44.mHide()
    this.oPgFrm.Page1.oPag.oESEDI2_1_45.visible=!this.oPgFrm.Page1.oPag.oESEDI2_1_45.mHide()
    this.oPgFrm.Page1.oPag.oDATDI2_1_46.visible=!this.oPgFrm.Page1.oPag.oDATDI2_1_46.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_57.visible=!this.oPgFrm.Page1.oPag.oStr_1_57.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_71.visible=!this.oPgFrm.Page1.oPag.oStr_1_71.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_72.visible=!this.oPgFrm.Page1.oPag.oStr_1_72.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_73.visible=!this.oPgFrm.Page1.oPag.oStr_1_73.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_77.visible=!this.oPgFrm.Page1.oPag.oBtn_1_77.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_95.visible=!this.oPgFrm.Page1.oPag.oStr_1_95.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_96.visible=!this.oPgFrm.Page1.oPag.oStr_1_96.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_97.visible=!this.oPgFrm.Page1.oPag.oStr_1_97.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_101.visible=!this.oPgFrm.Page1.oPag.oStr_1_101.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_102.visible=!this.oPgFrm.Page1.oPag.oStr_1_102.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_105.visible=!this.oPgFrm.Page1.oPag.oStr_1_105.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_106.visible=!this.oPgFrm.Page1.oPag.oStr_1_106.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_107.visible=!this.oPgFrm.Page1.oPag.oStr_1_107.mHide()
    this.oPgFrm.Page1.oPag.oMRCODSNS_1_108.visible=!this.oPgFrm.Page1.oPag.oMRCODSNS_1_108.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_109.visible=!this.oPgFrm.Page1.oPag.oStr_1_109.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_78.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_83.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_111.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_112.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI1
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TAB_RITE_IDX,3]
    i_lTable = "TAB_RITE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TAB_RITE_IDX,2], .t., this.TAB_RITE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TAB_RITE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRTIPRIT,TRCODAZI,TRFLGSTO,TRFLGST1";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODAZI="+cp_ToStrODBC(this.w_CODAZI1);
                   +" and TRTIPRIT="+cp_ToStrODBC(this.w_TIPRIT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRTIPRIT',this.w_TIPRIT;
                       ,'TRCODAZI',this.w_CODAZI1)
            select TRTIPRIT,TRCODAZI,TRFLGSTO,TRFLGST1;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI1 = NVL(_Link_.TRCODAZI,space(5))
      this.w_FLGSTO = NVL(_Link_.TRFLGSTO,space(1))
      this.w_FLGST1 = NVL(_Link_.TRFLGST1,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI1 = space(5)
      endif
      this.w_FLGSTO = space(1)
      this.w_FLGST1 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TAB_RITE_IDX,2])+'\'+cp_ToStr(_Link_.TRTIPRIT,1)+'\'+cp_ToStr(_Link_.TRCODAZI,1)
      cp_ShowWarn(i_cKey,this.TAB_RITE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MRCODESE
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MRCODESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_MRCODESE)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_CODAZI;
                     ,'ESCODESE',trim(this.w_MRCODESE))
          select ESCODAZI,ESCODESE,ESVALNAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MRCODESE)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MRCODESE) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oMRCODESE_1_7'),i_cWhere,'',"Esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MRCODESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_MRCODESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_MRCODESE)
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MRCODESE = NVL(_Link_.ESCODESE,space(4))
      this.w_MRVALNAZ = NVL(_Link_.ESVALNAZ,space(3))
      this.w_MRCODVAL = NVL(_Link_.ESVALNAZ,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_MRCODESE = space(4)
      endif
      this.w_MRVALNAZ = space(3)
      this.w_MRCODVAL = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MRCODESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MRCODVAL
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MRCODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_MRCODVAL)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT,VASIMVAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_MRCODVAL))
          select VACODVAL,VADECTOT,VASIMVAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MRCODVAL)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MRCODVAL) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oMRCODVAL_1_9'),i_cWhere,'',"Valute",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT,VASIMVAL";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADECTOT,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MRCODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT,VASIMVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_MRCODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_MRCODVAL)
            select VACODVAL,VADECTOT,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MRCODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
      this.w_SIMVAL = NVL(_Link_.VASIMVAL,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MRCODVAL = space(3)
      endif
      this.w_DECTOT = 0
      this.w_SIMVAL = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MRCODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_9(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VALUTE_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_9.VACODVAL as VACODVAL109"+ ",link_1_9.VADECTOT as VADECTOT109"+ ",link_1_9.VASIMVAL as VASIMVAL109"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_9 on MOV_RITE.MRCODVAL=link_1_9.VACODVAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_9"
          i_cKey=i_cKey+'+" and MOV_RITE.MRCODVAL=link_1_9.VACODVAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MRCODCON
  func Link_1_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MRCODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_MRCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_MRTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANRITENU,ANTIPCLF,ANRIINPS,ANFLRITE,ANCOINPS,ANCODSNS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_MRTIPCON;
                     ,'ANCODICE',trim(this.w_MRCODCON))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANRITENU,ANTIPCLF,ANRIINPS,ANFLRITE,ANCOINPS,ANCODSNS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MRCODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MRCODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oMRCODCON_1_19'),i_cWhere,'GSAR_BZC',"Elenco fornitori/clienti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_MRTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANRITENU,ANTIPCLF,ANRIINPS,ANFLRITE,ANCOINPS,ANCODSNS";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANRITENU,ANTIPCLF,ANRIINPS,ANFLRITE,ANCOINPS,ANCODSNS;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice fornitore\cliente inesistente o obsoleto o non soggetto a ritenute o no agente")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANRITENU,ANTIPCLF,ANRIINPS,ANFLRITE,ANCOINPS,ANCODSNS";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_MRTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANRITENU,ANTIPCLF,ANRIINPS,ANFLRITE,ANCOINPS,ANCODSNS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MRCODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANRITENU,ANTIPCLF,ANRIINPS,ANFLRITE,ANCOINPS,ANCODSNS";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_MRCODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_MRTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_MRTIPCON;
                       ,'ANCODICE',this.w_MRCODCON)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANRITENU,ANTIPCLF,ANRIINPS,ANFLRITE,ANCOINPS,ANCODSNS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MRCODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DESFOR = NVL(_Link_.ANDESCRI,space(40))
      this.w_DTOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
      this.w_FLRITE = NVL(_Link_.ANRITENU,space(1))
      this.w_FLFOAG = NVL(_Link_.ANTIPCLF,space(1))
      this.w_RITINPS = NVL(_Link_.ANRIINPS,0)
      this.w_FLGRIT = NVL(_Link_.ANFLRITE,space(10))
      this.w_COINPS = NVL(_Link_.ANCOINPS,0)
      this.w_CODSNS = NVL(_Link_.ANCODSNS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MRCODCON = space(15)
      endif
      this.w_DESFOR = space(40)
      this.w_DTOBSO = ctod("  /  /  ")
      this.w_FLRITE = space(1)
      this.w_FLFOAG = space(1)
      this.w_RITINPS = 0
      this.w_FLGRIT = space(10)
      this.w_COINPS = 0
      this.w_CODSNS = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_DTOBSO>.w_MRDATREG OR EMPTY(.w_DTOBSO)) AND ((.w_FLRITE $ 'CS' OR .w_FLFOAG='A' AND .w_MRTIPCON='F') OR (.w_FLGRIT = 'S' AND .w_MRTIPCON='C'))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice fornitore\cliente inesistente o obsoleto o non soggetto a ritenute o no agente")
        endif
        this.w_MRCODCON = space(15)
        this.w_DESFOR = space(40)
        this.w_DTOBSO = ctod("  /  /  ")
        this.w_FLRITE = space(1)
        this.w_FLFOAG = space(1)
        this.w_RITINPS = 0
        this.w_FLGRIT = space(10)
        this.w_COINPS = 0
        this.w_CODSNS = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MRCODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_19(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 9 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+9<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_19.ANCODICE as ANCODICE119"+ ",link_1_19.ANDESCRI as ANDESCRI119"+ ",link_1_19.ANDTOBSO as ANDTOBSO119"+ ",link_1_19.ANRITENU as ANRITENU119"+ ",link_1_19.ANTIPCLF as ANTIPCLF119"+ ",link_1_19.ANRIINPS as ANRIINPS119"+ ",link_1_19.ANFLRITE as ANFLRITE119"+ ",link_1_19.ANCOINPS as ANCOINPS119"+ ",link_1_19.ANCODSNS as ANCODSNS119"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_19 on MOV_RITE.MRCODCON=link_1_19.ANCODICE"+" and MOV_RITE.MRTIPCON=link_1_19.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+9
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_19"
          i_cKey=i_cKey+'+" and MOV_RITE.MRCODCON=link_1_19.ANCODICE(+)"'+'+" and MOV_RITE.MRTIPCON=link_1_19.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+9
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MRCODTRI
  func Link_1_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TRI_BUTI_IDX,3]
    i_lTable = "TRI_BUTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2], .t., this.TRI_BUTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MRCODTRI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATB',True,'TRI_BUTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODTRI like "+cp_ToStrODBC(trim(this.w_MRCODTRI)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODTRI,TRDESTRI,TRFLACON,TRCAUPRE2";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODTRI","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODTRI',trim(this.w_MRCODTRI))
          select TRCODTRI,TRDESTRI,TRFLACON,TRCAUPRE2;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODTRI into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MRCODTRI)==trim(_Link_.TRCODTRI) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MRCODTRI) and !this.bDontReportError
            deferred_cp_zoom('TRI_BUTI','*','TRCODTRI',cp_AbsName(oSource.parent,'oMRCODTRI_1_23'),i_cWhere,'GSAR_ATB',"Codici tributi",'GSRI1AMR.TRI_BUTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODTRI,TRDESTRI,TRFLACON,TRCAUPRE2";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODTRI="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODTRI',oSource.xKey(1))
            select TRCODTRI,TRDESTRI,TRFLACON,TRCAUPRE2;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MRCODTRI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODTRI,TRDESTRI,TRFLACON,TRCAUPRE2";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODTRI="+cp_ToStrODBC(this.w_MRCODTRI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODTRI',this.w_MRCODTRI)
            select TRCODTRI,TRDESTRI,TRFLACON,TRCAUPRE2;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MRCODTRI = NVL(_Link_.TRCODTRI,space(5))
      this.w_DESTR1 = NVL(_Link_.TRDESTRI,space(60))
      this.w_FLACO1 = NVL(_Link_.TRFLACON,space(1))
      this.w_CAUPRE2 = NVL(_Link_.TRCAUPRE2,space(2))
      this.w_MRCAURIT = NVL(_Link_.TRCAUPRE2,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_MRCODTRI = space(5)
      endif
      this.w_DESTR1 = space(60)
      this.w_FLACO1 = space(1)
      this.w_CAUPRE2 = space(2)
      this.w_MRCAURIT = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FLACO1='R'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_MRCODTRI = space(5)
        this.w_DESTR1 = space(60)
        this.w_FLACO1 = space(1)
        this.w_CAUPRE2 = space(2)
        this.w_MRCAURIT = space(2)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2])+'\'+cp_ToStr(_Link_.TRCODTRI,1)
      cp_ShowWarn(i_cKey,this.TRI_BUTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MRCODTRI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_23(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TRI_BUTI_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_23.TRCODTRI as TRCODTRI123"+ ",link_1_23.TRDESTRI as TRDESTRI123"+ ",link_1_23.TRFLACON as TRFLACON123"+ ",link_1_23.TRCAUPRE2 as TRCAUPRE2123"+ ",link_1_23.TRCAUPRE2 as TRCAUPRE2123"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_23 on MOV_RITE.MRCODTRI=link_1_23.TRCODTRI"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_23"
          i_cKey=i_cKey+'+" and MOV_RITE.MRCODTRI=link_1_23.TRCODTRI(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MRCODTR2
  func Link_1_37(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TRI_BUTI_IDX,3]
    i_lTable = "TRI_BUTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2], .t., this.TRI_BUTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MRCODTR2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATB',True,'TRI_BUTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODTRI like "+cp_ToStrODBC(trim(this.w_MRCODTR2)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODTRI,TRDESTRI,TRFLACON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODTRI","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODTRI',trim(this.w_MRCODTR2))
          select TRCODTRI,TRDESTRI,TRFLACON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODTRI into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MRCODTR2)==trim(_Link_.TRCODTRI) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MRCODTR2) and !this.bDontReportError
            deferred_cp_zoom('TRI_BUTI','*','TRCODTRI',cp_AbsName(oSource.parent,'oMRCODTR2_1_37'),i_cWhere,'GSAR_ATB',"Codici tributi",'GSRI2AMR.TRI_BUTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODTRI,TRDESTRI,TRFLACON";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODTRI="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODTRI',oSource.xKey(1))
            select TRCODTRI,TRDESTRI,TRFLACON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MRCODTR2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODTRI,TRDESTRI,TRFLACON";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODTRI="+cp_ToStrODBC(this.w_MRCODTR2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODTRI',this.w_MRCODTR2)
            select TRCODTRI,TRDESTRI,TRFLACON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MRCODTR2 = NVL(_Link_.TRCODTRI,space(5))
      this.w_DESTR2 = NVL(_Link_.TRDESTRI,space(35))
      this.w_FLACO2 = NVL(_Link_.TRFLACON,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MRCODTR2 = space(5)
      endif
      this.w_DESTR2 = space(35)
      this.w_FLACO2 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FLACO2<>'R'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_MRCODTR2 = space(5)
        this.w_DESTR2 = space(35)
        this.w_FLACO2 = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2])+'\'+cp_ToStr(_Link_.TRCODTRI,1)
      cp_ShowWarn(i_cKey,this.TRI_BUTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MRCODTR2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_37(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TRI_BUTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_37.TRCODTRI as TRCODTRI137"+ ",link_1_37.TRDESTRI as TRDESTRI137"+ ",link_1_37.TRFLACON as TRFLACON137"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_37 on MOV_RITE.MRCODTR2=link_1_37.TRCODTRI"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_37"
          i_cKey=i_cKey+'+" and MOV_RITE.MRCODTR2=link_1_37.TRCODTRI(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MRRIFDI1
  func Link_1_55(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VEP_RITE_IDX,3]
    i_lTable = "VEP_RITE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VEP_RITE_IDX,2], .t., this.VEP_RITE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VEP_RITE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MRRIFDI1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MRRIFDI1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VPSERIAL,VPNUMREG,VPCODESE,VPDATREG";
                   +" from "+i_cTable+" "+i_lTable+" where VPSERIAL="+cp_ToStrODBC(this.w_MRRIFDI1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VPSERIAL',this.w_MRRIFDI1)
            select VPSERIAL,VPNUMREG,VPCODESE,VPDATREG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MRRIFDI1 = NVL(_Link_.VPSERIAL,space(10))
      this.w_NUMDI1 = NVL(_Link_.VPNUMREG,0)
      this.w_ESEDI1 = NVL(_Link_.VPCODESE,space(4))
      this.w_DATDI1 = NVL(cp_ToDate(_Link_.VPDATREG),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_MRRIFDI1 = space(10)
      endif
      this.w_NUMDI1 = 0
      this.w_ESEDI1 = space(4)
      this.w_DATDI1 = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VEP_RITE_IDX,2])+'\'+cp_ToStr(_Link_.VPSERIAL,1)
      cp_ShowWarn(i_cKey,this.VEP_RITE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MRRIFDI1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_55(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VEP_RITE_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VEP_RITE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_55.VPSERIAL as VPSERIAL155"+ ",link_1_55.VPNUMREG as VPNUMREG155"+ ",link_1_55.VPCODESE as VPCODESE155"+ ",link_1_55.VPDATREG as VPDATREG155"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_55 on MOV_RITE.MRRIFDI1=link_1_55.VPSERIAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_55"
          i_cKey=i_cKey+'+" and MOV_RITE.MRRIFDI1=link_1_55.VPSERIAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MRRIFDI2
  func Link_1_70(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VEN_RITE_IDX,3]
    i_lTable = "VEN_RITE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VEN_RITE_IDX,2], .t., this.VEN_RITE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VEN_RITE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MRRIFDI2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MRRIFDI2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VPSERIAL,VPNUMREG,VPCODESE,VPDATREG";
                   +" from "+i_cTable+" "+i_lTable+" where VPSERIAL="+cp_ToStrODBC(this.w_MRRIFDI2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VPSERIAL',this.w_MRRIFDI2)
            select VPSERIAL,VPNUMREG,VPCODESE,VPDATREG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MRRIFDI2 = NVL(_Link_.VPSERIAL,space(10))
      this.w_NUMDI2 = NVL(_Link_.VPNUMREG,0)
      this.w_ESEDI2 = NVL(_Link_.VPCODESE,space(4))
      this.w_DATDI2 = NVL(cp_ToDate(_Link_.VPDATREG),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_MRRIFDI2 = space(10)
      endif
      this.w_NUMDI2 = 0
      this.w_ESEDI2 = space(4)
      this.w_DATDI2 = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VEN_RITE_IDX,2])+'\'+cp_ToStr(_Link_.VPSERIAL,1)
      cp_ShowWarn(i_cKey,this.VEN_RITE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MRRIFDI2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_70(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VEN_RITE_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VEN_RITE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_70.VPSERIAL as VPSERIAL170"+ ",link_1_70.VPNUMREG as VPNUMREG170"+ ",link_1_70.VPCODESE as VPCODESE170"+ ",link_1_70.VPDATREG as VPDATREG170"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_70 on MOV_RITE.MRRIFDI2=link_1_70.VPSERIAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_70"
          i_cKey=i_cKey+'+" and MOV_RITE.MRRIFDI2=link_1_70.VPSERIAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oMRNUMREG_1_6.value==this.w_MRNUMREG)
      this.oPgFrm.Page1.oPag.oMRNUMREG_1_6.value=this.w_MRNUMREG
    endif
    if not(this.oPgFrm.Page1.oPag.oMRCODESE_1_7.value==this.w_MRCODESE)
      this.oPgFrm.Page1.oPag.oMRCODESE_1_7.value=this.w_MRCODESE
    endif
    if not(this.oPgFrm.Page1.oPag.oMRDATREG_1_8.value==this.w_MRDATREG)
      this.oPgFrm.Page1.oPag.oMRDATREG_1_8.value=this.w_MRDATREG
    endif
    if not(this.oPgFrm.Page1.oPag.oMRCODVAL_1_9.value==this.w_MRCODVAL)
      this.oPgFrm.Page1.oPag.oMRCODVAL_1_9.value=this.w_MRCODVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oMRCAOVAL_1_10.value==this.w_MRCAOVAL)
      this.oPgFrm.Page1.oPag.oMRCAOVAL_1_10.value=this.w_MRCAOVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oMRNUMDOC_1_11.value==this.w_MRNUMDOC)
      this.oPgFrm.Page1.oPag.oMRNUMDOC_1_11.value=this.w_MRNUMDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oMRSERDOC_1_12.value==this.w_MRSERDOC)
      this.oPgFrm.Page1.oPag.oMRSERDOC_1_12.value=this.w_MRSERDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oMRDATDOC_1_13.value==this.w_MRDATDOC)
      this.oPgFrm.Page1.oPag.oMRDATDOC_1_13.value=this.w_MRDATDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oMRCODCON_1_19.value==this.w_MRCODCON)
      this.oPgFrm.Page1.oPag.oMRCODCON_1_19.value=this.w_MRCODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oMRTIPPAR_1_20.RadioValue()==this.w_MRTIPPAR)
      this.oPgFrm.Page1.oPag.oMRTIPPAR_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMRDATCER_1_21.value==this.w_MRDATCER)
      this.oPgFrm.Page1.oPag.oMRDATCER_1_21.value=this.w_MRDATCER
    endif
    if not(this.oPgFrm.Page1.oPag.oMRSTACER_1_22.RadioValue()==this.w_MRSTACER)
      this.oPgFrm.Page1.oPag.oMRSTACER_1_22.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMRCODTRI_1_23.value==this.w_MRCODTRI)
      this.oPgFrm.Page1.oPag.oMRCODTRI_1_23.value=this.w_MRCODTRI
    endif
    if not(this.oPgFrm.Page1.oPag.oMRCAURIT_1_25.RadioValue()==this.w_MRCAURIT)
      this.oPgFrm.Page1.oPag.oMRCAURIT_1_25.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMRDATPAG_1_26.value==this.w_MRDATPAG)
      this.oPgFrm.Page1.oPag.oMRDATPAG_1_26.value=this.w_MRDATPAG
    endif
    if not(this.oPgFrm.Page1.oPag.oMRNONSO1_1_27.value==this.w_MRNONSO1)
      this.oPgFrm.Page1.oPag.oMRNONSO1_1_27.value=this.w_MRNONSO1
    endif
    if not(this.oPgFrm.Page1.oPag.oMRIMPRI1_1_28.value==this.w_MRIMPRI1)
      this.oPgFrm.Page1.oPag.oMRIMPRI1_1_28.value=this.w_MRIMPRI1
    endif
    if not(this.oPgFrm.Page1.oPag.oMRPERRI1_1_29.value==this.w_MRPERRI1)
      this.oPgFrm.Page1.oPag.oMRPERRI1_1_29.value=this.w_MRPERRI1
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMDI1_1_30.value==this.w_NUMDI1)
      this.oPgFrm.Page1.oPag.oNUMDI1_1_30.value=this.w_NUMDI1
    endif
    if not(this.oPgFrm.Page1.oPag.oESEDI1_1_31.value==this.w_ESEDI1)
      this.oPgFrm.Page1.oPag.oESEDI1_1_31.value=this.w_ESEDI1
    endif
    if not(this.oPgFrm.Page1.oPag.oDATDI1_1_32.value==this.w_DATDI1)
      this.oPgFrm.Page1.oPag.oDATDI1_1_32.value=this.w_DATDI1
    endif
    if not(this.oPgFrm.Page1.oPag.oMRTOTIM1_1_33.value==this.w_MRTOTIM1)
      this.oPgFrm.Page1.oPag.oMRTOTIM1_1_33.value=this.w_MRTOTIM1
    endif
    if not(this.oPgFrm.Page1.oPag.oMRNONSO2_1_36.value==this.w_MRNONSO2)
      this.oPgFrm.Page1.oPag.oMRNONSO2_1_36.value=this.w_MRNONSO2
    endif
    if not(this.oPgFrm.Page1.oPag.oMRCODTR2_1_37.value==this.w_MRCODTR2)
      this.oPgFrm.Page1.oPag.oMRCODTR2_1_37.value=this.w_MRCODTR2
    endif
    if not(this.oPgFrm.Page1.oPag.oDESTR2_1_38.value==this.w_DESTR2)
      this.oPgFrm.Page1.oPag.oDESTR2_1_38.value=this.w_DESTR2
    endif
    if not(this.oPgFrm.Page1.oPag.oMRIMPRI2_1_39.value==this.w_MRIMPRI2)
      this.oPgFrm.Page1.oPag.oMRIMPRI2_1_39.value=this.w_MRIMPRI2
    endif
    if not(this.oPgFrm.Page1.oPag.oMRPERRI2_1_40.value==this.w_MRPERRI2)
      this.oPgFrm.Page1.oPag.oMRPERRI2_1_40.value=this.w_MRPERRI2
    endif
    if not(this.oPgFrm.Page1.oPag.oMRTOTIM2_1_42.value==this.w_MRTOTIM2)
      this.oPgFrm.Page1.oPag.oMRTOTIM2_1_42.value=this.w_MRTOTIM2
    endif
    if not(this.oPgFrm.Page1.oPag.oMRPERPER_1_43.value==this.w_MRPERPER)
      this.oPgFrm.Page1.oPag.oMRPERPER_1_43.value=this.w_MRPERPER
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMDI2_1_44.value==this.w_NUMDI2)
      this.oPgFrm.Page1.oPag.oNUMDI2_1_44.value=this.w_NUMDI2
    endif
    if not(this.oPgFrm.Page1.oPag.oESEDI2_1_45.value==this.w_ESEDI2)
      this.oPgFrm.Page1.oPag.oESEDI2_1_45.value=this.w_ESEDI2
    endif
    if not(this.oPgFrm.Page1.oPag.oDATDI2_1_46.value==this.w_DATDI2)
      this.oPgFrm.Page1.oPag.oDATDI2_1_46.value=this.w_DATDI2
    endif
    if not(this.oPgFrm.Page1.oPag.oMRIMPPER_1_47.value==this.w_MRIMPPER)
      this.oPgFrm.Page1.oPag.oMRIMPPER_1_47.value=this.w_MRIMPPER
    endif
    if not(this.oPgFrm.Page1.oPag.oMRSPERIM_1_48.value==this.w_MRSPERIM)
      this.oPgFrm.Page1.oPag.oMRSPERIM_1_48.value=this.w_MRSPERIM
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFOR_1_58.value==this.w_DESFOR)
      this.oPgFrm.Page1.oPag.oDESFOR_1_58.value=this.w_DESFOR
    endif
    if not(this.oPgFrm.Page1.oPag.oSIMVAL_1_85.value==this.w_SIMVAL)
      this.oPgFrm.Page1.oPag.oSIMVAL_1_85.value=this.w_SIMVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oDESTR1_1_89.value==this.w_DESTR1)
      this.oPgFrm.Page1.oPag.oDESTR1_1_89.value=this.w_DESTR1
    endif
    if not(this.oPgFrm.Page1.oPag.oMRCODSNS_1_108.RadioValue()==this.w_MRCODSNS)
      this.oPgFrm.Page1.oPag.oMRCODSNS_1_108.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'MOV_RITE')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_MRNUMREG))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMRNUMREG_1_6.SetFocus()
            i_bnoObbl = !empty(.w_MRNUMREG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_MRCODESE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMRCODESE_1_7.SetFocus()
            i_bnoObbl = !empty(.w_MRCODESE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_MRDATREG))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMRDATREG_1_8.SetFocus()
            i_bnoObbl = !empty(.w_MRDATREG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_MRCAOVAL))  and not(.w_CAOVAL<>0 OR EMPTY(.w_MRCODVAL))  and (.w_CAOVAL=0 AND NOT EMPTY(.w_MRCODVAL))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMRCAOVAL_1_10.SetFocus()
            i_bnoObbl = !empty(.w_MRCAOVAL)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_MRDATDOC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMRDATDOC_1_13.SetFocus()
            i_bnoObbl = !empty(.w_MRDATDOC)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_MRCODCON)) or not((.w_DTOBSO>.w_MRDATREG OR EMPTY(.w_DTOBSO)) AND ((.w_FLRITE $ 'CS' OR .w_FLFOAG='A' AND .w_MRTIPCON='F') OR (.w_FLGRIT = 'S' AND .w_MRTIPCON='C'))))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMRCODCON_1_19.SetFocus()
            i_bnoObbl = !empty(.w_MRCODCON)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice fornitore\cliente inesistente o obsoleto o non soggetto a ritenute o no agente")
          case   not(.w_FLACO1='R')  and not(empty(.w_MRCODTRI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMRCODTRI_1_23.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_FLACO2<>'R')  and (.w_FLRITE='C')  and not(empty(.w_MRCODTR2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMRCODTR2_1_37.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      *i_bRes = i_bRes .and. .GSRI_MDP.CheckForm()
      if i_bres
        i_bres=  .GSRI_MDP.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_MRTIPCON = this.w_MRTIPCON
    this.o_MRCODESE = this.w_MRCODESE
    this.o_MRDATREG = this.w_MRDATREG
    this.o_MRCODVAL = this.w_MRCODVAL
    this.o_MRDATDOC = this.w_MRDATDOC
    this.o_MRCODCON = this.w_MRCODCON
    this.o_MRCODTRI = this.w_MRCODTRI
    this.o_CAUPRE2 = this.w_CAUPRE2
    this.o_MRNONSO1 = this.w_MRNONSO1
    this.o_MRIMPRI1 = this.w_MRIMPRI1
    this.o_MRPERRI1 = this.w_MRPERRI1
    this.o_FLRITE = this.w_FLRITE
    this.o_MRIMPRI2 = this.w_MRIMPRI2
    this.o_MRPERRI2 = this.w_MRPERRI2
    this.o_MRPERPER = this.w_MRPERPER
    * --- GSRI_MDP : Depends On
    this.GSRI_MDP.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgsri_amrPag1 as StdContainer
  Width  = 881
  height = 476
  stdWidth  = 881
  stdheight = 476
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMRNUMREG_1_6 as StdField with uid="OYLGEXJHGI",rtseq=6,rtrep=.f.,;
    cFormVar = "w_MRNUMREG", cQueryName = "MRNUMREG",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero registrazione",;
    HelpContextID = 178274573,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=125, Top=11, cSayPict='"999999"', cGetPict='"999999"'

  add object oMRCODESE_1_7 as StdField with uid="WJTIUNAYHL",rtseq=7,rtrep=.f.,;
    cFormVar = "w_MRCODESE", cQueryName = "MRCODESE",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Codice esercizio di riferimento",;
    HelpContextID = 218730763,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=195, Top=11, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_MRCODESE"

  func oMRCODESE_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oMRCODESE_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMRCODESE_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CODAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_CODAZI)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oMRCODESE_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Esercizi",'',this.parent.oContained
  endproc

  add object oMRDATREG_1_8 as StdField with uid="ACUVVYYYRE",rtseq=8,rtrep=.f.,;
    cFormVar = "w_MRDATREG", cQueryName = "MRDATREG",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di registrazione",;
    HelpContextID = 184262925,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=295, Top=11

  add object oMRCODVAL_1_9 as StdField with uid="RJYQKONVAW",rtseq=9,rtrep=.f.,;
    cFormVar = "w_MRCODVAL", cQueryName = "MRCODVAL",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice valuta",;
    HelpContextID = 32927470,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=437, Top=11, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", oKey_1_1="VACODVAL", oKey_1_2="this.w_MRCODVAL"

  func oMRCODVAL_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oMRCODVAL_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMRCODVAL_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oMRCODVAL_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Valute",'',this.parent.oContained
  endproc

  add object oMRCAOVAL_1_10 as StdField with uid="GJREABROUK",rtseq=10,rtrep=.f.,;
    cFormVar = "w_MRCAOVAL", cQueryName = "MRCAOVAL",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Cambio della registrazione",;
    HelpContextID = 22310638,;
   bGlobalFont=.t.,;
    Height=21, Width=104, Left=622, Top=11, cSayPict='"99999.999999"', cGetPict='"99999.999999"', tabstop=.f.

  func oMRCAOVAL_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CAOVAL=0 AND NOT EMPTY(.w_MRCODVAL))
    endwith
   endif
  endfunc

  func oMRCAOVAL_1_10.mHide()
    with this.Parent.oContained
      return (.w_CAOVAL<>0 OR EMPTY(.w_MRCODVAL))
    endwith
  endfunc

  add object oMRNUMDOC_1_11 as StdField with uid="YXRAUMXEEG",rtseq=11,rtrep=.f.,;
    cFormVar = "w_MRNUMDOC", cQueryName = "MRNUMDOC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero del documento associato",;
    HelpContextID = 56606455,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=125, Top=45, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  add object oMRSERDOC_1_12 as StdField with uid="HHLQWTFBVX",rtseq=12,rtrep=.f.,;
    cFormVar = "w_MRSERDOC", cQueryName = "MRSERDOC",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie del documento",;
    HelpContextID = 52391671,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=264, Top=45, cSayPict='"!!!!!!!!!!"', cGetPict='"!!!!!!!!!!"', InputMask=replicate('X',10)

  add object oMRDATDOC_1_13 as StdField with uid="VWINTAWBNM",rtseq=13,rtrep=.f.,;
    cFormVar = "w_MRDATDOC", cQueryName = "MRDATDOC",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data del documento associato",;
    HelpContextID = 50618103,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=434, Top=45

  add object oMRCODCON_1_19 as StdField with uid="ADSWWRKOLP",rtseq=19,rtrep=.f.,;
    cFormVar = "w_MRCODCON", cQueryName = "MRCODCON",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice fornitore\cliente inesistente o obsoleto o non soggetto a ritenute o no agente",;
    ToolTipText = "Codice del fornitore / cliente",;
    HelpContextID = 83259116,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=125, Top=75, cSayPict="p_FOR", cGetPict="p_FOR", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_MRTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_MRCODCON"

  func oMRCODCON_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oMRCODCON_1_19.ecpDrop(oSource)
    this.Parent.oContained.link_1_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMRCODCON_1_19.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_MRTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_MRTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oMRCODCON_1_19'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Elenco fornitori/clienti",'',this.parent.oContained
  endproc
  proc oMRCODCON_1_19.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_MRTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_MRCODCON
     i_obj.ecpSave()
  endproc


  add object oMRTIPPAR_1_20 as StdCombo with uid="YSHBZASZZZ",value=1,rtseq=20,rtrep=.f.,left=125,top=102,width=134,height=21;
    , ToolTipText = "Indica il tipo di operazione nelle dichiarazioni parziali del modello 770";
    , HelpContextID = 121331432;
    , cFormVar="w_MRTIPPAR",RowSource=""+"Nessuno,"+"Aggiornamento,"+"Inserimento,"+"Cancellazione", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oMRTIPPAR_1_20.RadioValue()
    return(iif(this.value =1,' ',;
    iif(this.value =2,'A',;
    iif(this.value =3,'I',;
    iif(this.value =4,'C',;
    space(1))))))
  endfunc
  func oMRTIPPAR_1_20.GetRadio()
    this.Parent.oContained.w_MRTIPPAR = this.RadioValue()
    return .t.
  endfunc

  func oMRTIPPAR_1_20.SetRadio()
    this.Parent.oContained.w_MRTIPPAR=trim(this.Parent.oContained.w_MRTIPPAR)
    this.value = ;
      iif(this.Parent.oContained.w_MRTIPPAR=='',1,;
      iif(this.Parent.oContained.w_MRTIPPAR=='A',2,;
      iif(this.Parent.oContained.w_MRTIPPAR=='I',3,;
      iif(this.Parent.oContained.w_MRTIPPAR=='C',4,;
      0))))
  endfunc

  add object oMRDATCER_1_21 as StdField with uid="VEXSFCYYUT",rtseq=21,rtrep=.f.,;
    cFormVar = "w_MRDATCER", cQueryName = "MRDATCER",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di certificazione",;
    HelpContextID = 201040152,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=346, Top=102

  func oMRDATCER_1_21.mHide()
    with this.Parent.oContained
      return (.w_MRTIPCON='F')
    endwith
  endfunc


  add object oMRSTACER_1_22 as StdCombo with uid="KSAKXXFQSJ",rtseq=22,rtrep=.f.,left=594,top=102,width=122,height=21;
    , ToolTipText = "Stato delle certificazioni pervenute\non pervenute";
    , HelpContextID = 182423832;
    , cFormVar="w_MRSTACER",RowSource=""+"Pervenuta,"+"Non pervenuta", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oMRSTACER_1_22.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'N',;
    space(1))))
  endfunc
  func oMRSTACER_1_22.GetRadio()
    this.Parent.oContained.w_MRSTACER = this.RadioValue()
    return .t.
  endfunc

  func oMRSTACER_1_22.SetRadio()
    this.Parent.oContained.w_MRSTACER=trim(this.Parent.oContained.w_MRSTACER)
    this.value = ;
      iif(this.Parent.oContained.w_MRSTACER=='P',1,;
      iif(this.Parent.oContained.w_MRSTACER=='N',2,;
      0))
  endfunc

  func oMRSTACER_1_22.mHide()
    with this.Parent.oContained
      return (.w_MRTIPCON='F')
    endwith
  endfunc

  add object oMRCODTRI_1_23 as StdField with uid="GUOUSHMEAQ",rtseq=23,rtrep=.f.,;
    cFormVar = "w_MRCODTRI", cQueryName = "MRCODTRI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice tributo",;
    HelpContextID = 201953551,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=140, Top=152, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TRI_BUTI", cZoomOnZoom="GSAR_ATB", oKey_1_1="TRCODTRI", oKey_1_2="this.w_MRCODTRI"

  func oMRCODTRI_1_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_23('Part',this)
    endwith
    return bRes
  endfunc

  proc oMRCODTRI_1_23.ecpDrop(oSource)
    this.Parent.oContained.link_1_23('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMRCODTRI_1_23.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TRI_BUTI','*','TRCODTRI',cp_AbsName(this.parent,'oMRCODTRI_1_23'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATB',"Codici tributi",'GSRI1AMR.TRI_BUTI_VZM',this.parent.oContained
  endproc
  proc oMRCODTRI_1_23.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODTRI=this.parent.oContained.w_MRCODTRI
     i_obj.ecpSave()
  endproc


  add object oMRCAURIT_1_25 as StdCombo with uid="YCCBNABVZR",rtseq=25,rtrep=.f.,left=140,top=180,width=76,height=21;
    , ToolTipText = "Causale prestazione mod. 770 telematico";
    , HelpContextID = 185307418;
    , cFormVar="w_MRCAURIT",RowSource=""+"Tipo A,"+"Tipo B,"+"Tipo C,"+"Tipo D,"+"Tipo E,"+"Tipo F,"+"Tipo G,"+"Tipo H,"+"Tipo I,"+"Tipo J,"+"Tipo K,"+"Tipo L,"+"Tipo L1,"+"Tipo M,"+"Tipo M1,"+"Tipo M2,"+"Tipo N,"+"Tipo O,"+"Tipo O1,"+"Tipo P,"+"Tipo Q,"+"Tipo R,"+"Tipo S,"+"Tipo T,"+"Tipo U,"+"Tipo V,"+"Tipo V1,"+"Tipo V2,"+"Tipo W,"+"Tipo X,"+"Tipo Y,"+"Tipo Z,"+"Tipo ZO", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oMRCAURIT_1_25.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'B',;
    iif(this.value =3,'C',;
    iif(this.value =4,'D',;
    iif(this.value =5,'E',;
    iif(this.value =6,'F',;
    iif(this.value =7,'G',;
    iif(this.value =8,'H',;
    iif(this.value =9,'I',;
    iif(this.value =10,'J',;
    iif(this.value =11,'K',;
    iif(this.value =12,'L',;
    iif(this.value =13,'L1',;
    iif(this.value =14,'M',;
    iif(this.value =15,'M1',;
    iif(this.value =16,'M2',;
    iif(this.value =17,'N',;
    iif(this.value =18,'O',;
    iif(this.value =19,'O1',;
    iif(this.value =20,'P',;
    iif(this.value =21,'Q',;
    iif(this.value =22,'R',;
    iif(this.value =23,'S',;
    iif(this.value =24,'T',;
    iif(this.value =25,'U',;
    iif(this.value =26,'V',;
    iif(this.value =27,'V1',;
    iif(this.value =28,'V2',;
    iif(this.value =29,'W',;
    iif(this.value =30,'X',;
    iif(this.value =31,'Y',;
    iif(this.value =32,'Z',;
    iif(this.value =33,'ZO',;
    space(2)))))))))))))))))))))))))))))))))))
  endfunc
  func oMRCAURIT_1_25.GetRadio()
    this.Parent.oContained.w_MRCAURIT = this.RadioValue()
    return .t.
  endfunc

  func oMRCAURIT_1_25.SetRadio()
    this.Parent.oContained.w_MRCAURIT=trim(this.Parent.oContained.w_MRCAURIT)
    this.value = ;
      iif(this.Parent.oContained.w_MRCAURIT=='A',1,;
      iif(this.Parent.oContained.w_MRCAURIT=='B',2,;
      iif(this.Parent.oContained.w_MRCAURIT=='C',3,;
      iif(this.Parent.oContained.w_MRCAURIT=='D',4,;
      iif(this.Parent.oContained.w_MRCAURIT=='E',5,;
      iif(this.Parent.oContained.w_MRCAURIT=='F',6,;
      iif(this.Parent.oContained.w_MRCAURIT=='G',7,;
      iif(this.Parent.oContained.w_MRCAURIT=='H',8,;
      iif(this.Parent.oContained.w_MRCAURIT=='I',9,;
      iif(this.Parent.oContained.w_MRCAURIT=='J',10,;
      iif(this.Parent.oContained.w_MRCAURIT=='K',11,;
      iif(this.Parent.oContained.w_MRCAURIT=='L',12,;
      iif(this.Parent.oContained.w_MRCAURIT=='L1',13,;
      iif(this.Parent.oContained.w_MRCAURIT=='M',14,;
      iif(this.Parent.oContained.w_MRCAURIT=='M1',15,;
      iif(this.Parent.oContained.w_MRCAURIT=='M2',16,;
      iif(this.Parent.oContained.w_MRCAURIT=='N',17,;
      iif(this.Parent.oContained.w_MRCAURIT=='O',18,;
      iif(this.Parent.oContained.w_MRCAURIT=='O1',19,;
      iif(this.Parent.oContained.w_MRCAURIT=='P',20,;
      iif(this.Parent.oContained.w_MRCAURIT=='Q',21,;
      iif(this.Parent.oContained.w_MRCAURIT=='R',22,;
      iif(this.Parent.oContained.w_MRCAURIT=='S',23,;
      iif(this.Parent.oContained.w_MRCAURIT=='T',24,;
      iif(this.Parent.oContained.w_MRCAURIT=='U',25,;
      iif(this.Parent.oContained.w_MRCAURIT=='V',26,;
      iif(this.Parent.oContained.w_MRCAURIT=='V1',27,;
      iif(this.Parent.oContained.w_MRCAURIT=='V2',28,;
      iif(this.Parent.oContained.w_MRCAURIT=='W',29,;
      iif(this.Parent.oContained.w_MRCAURIT=='X',30,;
      iif(this.Parent.oContained.w_MRCAURIT=='Y',31,;
      iif(this.Parent.oContained.w_MRCAURIT=='Z',32,;
      iif(this.Parent.oContained.w_MRCAURIT=='ZO',33,;
      0)))))))))))))))))))))))))))))))))
  endfunc

  func oMRCAURIT_1_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((.w_FLRITE$'CS'  or .w_FLGRIT='S') And .cFunction<>'Query')
    endwith
   endif
  endfunc

  add object oMRDATPAG_1_26 as StdField with uid="TXZHUMNKVN",rtseq=26,rtrep=.f.,;
    cFormVar = "w_MRDATPAG", cQueryName = "MRDATPAG",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di pagamento della ritenuta",;
    HelpContextID = 117726963,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=619, Top=180

  func oMRDATPAG_1_26.mHide()
    with this.Parent.oContained
      return (.w_MRTIPCON='F')
    endwith
  endfunc

  add object oMRNONSO1_1_27 as StdField with uid="NOVYFAHWKP",rtseq=27,rtrep=.f.,;
    cFormVar = "w_MRNONSO1", cQueryName = "MRNONSO1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale somme non soggette",;
    HelpContextID = 72728329,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=140, Top=208, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  add object oMRIMPRI1_1_28 as StdField with uid="PFPFSMDUCY",rtseq=28,rtrep=.f.,;
    cFormVar = "w_MRIMPRI1", cQueryName = "MRIMPRI1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Imponibile ritenuta",;
    HelpContextID = 180875511,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=382, Top=208, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  add object oMRPERRI1_1_29 as StdField with uid="OJKLRIVDHE",rtseq=29,rtrep=.f.,;
    cFormVar = "w_MRPERRI1", cQueryName = "MRPERRI1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Percentuale di ritenuta IRPEF",;
    HelpContextID = 182477047,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=619, Top=208, cSayPict='"999.99"', cGetPict='"999.99"'

  add object oNUMDI1_1_30 as StdField with uid="SSKTTXPCYX",rtseq=30,rtrep=.f.,;
    cFormVar = "w_NUMDI1", cQueryName = "NUMDI1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero di registrazione della distinta di versamento",;
    HelpContextID = 112249898,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=140, Top=276

  func oNUMDI1_1_30.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_MRRIFDI1))
    endwith
  endfunc

  add object oESEDI1_1_31 as StdField with uid="ROZIEYQSKV",rtseq=31,rtrep=.f.,;
    cFormVar = "w_ESEDI1", cQueryName = "ESEDI1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Esercizio di riferimento della distinta di versamento",;
    HelpContextID = 112283322,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=219, Top=276, InputMask=replicate('X',4)

  func oESEDI1_1_31.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_MRRIFDI1))
    endwith
  endfunc

  add object oDATDI1_1_32 as StdField with uid="EGNOJIJBWA",rtseq=32,rtrep=.f.,;
    cFormVar = "w_DATDI1", cQueryName = "DATDI1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di registrazione della distinta di versamento",;
    HelpContextID = 112226506,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=324, Top=276

  func oDATDI1_1_32.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_MRRIFDI1))
    endwith
  endfunc

  add object oMRTOTIM1_1_33 as StdField with uid="SGAKRPVIAW",rtseq=33,rtrep=.f.,;
    cFormVar = "w_MRTOTIM1", cQueryName = "MRTOTIM1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale importo ritenuta",;
    HelpContextID = 234184457,;
   bGlobalFont=.t.,;
    Height=21, Width=115, Left=619, Top=240, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  add object oMRNONSO2_1_36 as StdField with uid="CFOZMMVUWQ",rtseq=36,rtrep=.f.,;
    cFormVar = "w_MRNONSO2", cQueryName = "MRNONSO2",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale somme non soggette",;
    HelpContextID = 72728328,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=125, Top=367, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oMRNONSO2_1_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLRITE='C')
    endwith
   endif
  endfunc

  add object oMRCODTR2_1_37 as StdField with uid="FVKSRMMMXQ",rtseq=37,rtrep=.f.,;
    cFormVar = "w_MRCODTR2", cQueryName = "MRCODTR2",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice tributo",;
    HelpContextID = 201953528,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=125, Top=340, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TRI_BUTI", cZoomOnZoom="GSAR_ATB", oKey_1_1="TRCODTRI", oKey_1_2="this.w_MRCODTR2"

  func oMRCODTR2_1_37.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLRITE='C')
    endwith
   endif
  endfunc

  func oMRCODTR2_1_37.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_37('Part',this)
    endwith
    return bRes
  endfunc

  proc oMRCODTR2_1_37.ecpDrop(oSource)
    this.Parent.oContained.link_1_37('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMRCODTR2_1_37.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TRI_BUTI','*','TRCODTRI',cp_AbsName(this.parent,'oMRCODTR2_1_37'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATB',"Codici tributi",'GSRI2AMR.TRI_BUTI_VZM',this.parent.oContained
  endproc
  proc oMRCODTR2_1_37.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODTRI=this.parent.oContained.w_MRCODTR2
     i_obj.ecpSave()
  endproc

  add object oDESTR2_1_38 as StdField with uid="KMYPGSOVWJ",rtseq=38,rtrep=.f.,;
    cFormVar = "w_DESTR2", cQueryName = "DESTR2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 84966602,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=189, Top=340, InputMask=replicate('X',35)

  add object oMRIMPRI2_1_39 as StdField with uid="PHPTORSWIT",rtseq=39,rtrep=.f.,;
    cFormVar = "w_MRIMPRI2", cQueryName = "MRIMPRI2",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Imponibile ritenuta",;
    HelpContextID = 180875512,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=382, Top=368, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oMRIMPRI2_1_39.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLRITE='C')
    endwith
   endif
  endfunc

  add object oMRPERRI2_1_40 as StdField with uid="NXAZRFQGOX",rtseq=40,rtrep=.f.,;
    cFormVar = "w_MRPERRI2", cQueryName = "MRPERRI2",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Percentuale ritenuta complessiva",;
    HelpContextID = 182477048,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=619, Top=368, cSayPict='"999.99"', cGetPict='"999.99"'

  func oMRPERRI2_1_40.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLRITE='C')
    endwith
   endif
  endfunc

  add object oMRTOTIM2_1_42 as StdField with uid="GOVOCADAVS",rtseq=42,rtrep=.f.,;
    cFormVar = "w_MRTOTIM2", cQueryName = "MRTOTIM2",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale importo ritenuta",;
    HelpContextID = 234184456,;
   bGlobalFont=.t.,;
    Height=21, Width=115, Left=619, Top=396, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oMRTOTIM2_1_42.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLRITE='C')
    endwith
   endif
  endfunc

  add object oMRPERPER_1_43 as StdField with uid="OWDZXDCKRF",rtseq=43,rtrep=.f.,;
    cFormVar = "w_MRPERPER", cQueryName = "MRPERPER",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Percentuale ritenuta a carico del percipiente",;
    HelpContextID = 148922648,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=619, Top=424, cSayPict='"999.99"', cGetPict='"999.99"'

  func oMRPERPER_1_43.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLRITE='C')
    endwith
   endif
  endfunc

  add object oNUMDI2_1_44 as StdField with uid="MYNOZJEPCD",rtseq=44,rtrep=.f.,;
    cFormVar = "w_NUMDI2", cQueryName = "NUMDI2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero di registrazione della distinta di versamento",;
    HelpContextID = 95472682,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=125, Top=395

  func oNUMDI2_1_44.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_MRRIFDI2))
    endwith
  endfunc

  add object oESEDI2_1_45 as StdField with uid="NLPSRGWWDZ",rtseq=45,rtrep=.f.,;
    cFormVar = "w_ESEDI2", cQueryName = "ESEDI2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Esercizio di riferimento della distinta di versamento",;
    HelpContextID = 95506106,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=202, Top=395, InputMask=replicate('X',4)

  func oESEDI2_1_45.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_MRRIFDI2))
    endwith
  endfunc

  add object oDATDI2_1_46 as StdField with uid="WYQQOCHILJ",rtseq=46,rtrep=.f.,;
    cFormVar = "w_DATDI2", cQueryName = "DATDI2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di registrazione della distinta di versamento",;
    HelpContextID = 95449290,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=291, Top=395

  func oDATDI2_1_46.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_MRRIFDI2))
    endwith
  endfunc

  add object oMRIMPPER_1_47 as StdField with uid="XNDBBIIIYK",rtseq=47,rtrep=.f.,;
    cFormVar = "w_MRIMPPER", cQueryName = "MRIMPPER",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo ritenuta a carico del percipiente",;
    HelpContextID = 147321112,;
   bGlobalFont=.t.,;
    Height=21, Width=115, Left=619, Top=451, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oMRIMPPER_1_47.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLRITE='C')
    endwith
   endif
  endfunc

  add object oMRSPERIM_1_48 as StdField with uid="VMNUUHAJZO",rtseq=48,rtrep=.f.,;
    cFormVar = "w_MRSPERIM", cQueryName = "MRSPERIM",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 169578771,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=125, Top=423, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oMRSPERIM_1_48.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLRITE='C')
    endwith
   endif
  endfunc

  add object oDESFOR_1_58 as StdField with uid="MUVNGEANHF",rtseq=50,rtrep=.f.,;
    cFormVar = "w_DESFOR", cQueryName = "DESFOR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 89029834,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=264, Top=75, InputMask=replicate('X',40)


  add object oBtn_1_77 as StdButton with uid="DOISBSYLYW",left=668, top=41, width=48,height=45,;
    CpPicture="BMP\TESO.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per accedere alla registrazione contabile associata";
    , HelpContextID = 134604934;
    , tabstop=.f., caption='\<Primanota';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_77.Click()
      with this.Parent.oContained
        GSAR_BZP(this.Parent.oContained,.w_MRRIFCON)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_77.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (empty(.w_MRRIFCON))
     endwith
    endif
  endfunc


  add object oObj_1_78 as cp_runprogram with uid="AKRNVLWZFJ",left=1, top=578, width=256,height=20,;
    caption='GSRI_BDM(D)',;
   bGlobalFont=.t.,;
    prg="GSRI_BDM('D')",;
    cEvent = "Delete start,Edit Started",;
    nPag=1;
    , HelpContextID = 196565043


  add object oObj_1_83 as cp_runprogram with uid="HOWARXDSIY",left=1, top=554, width=256,height=20,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="gsri_bpr",;
    cEvent = "w_MRCODTRI Changed",;
    nPag=1;
    , HelpContextID = 236064998


  add object oLinkPC_1_84 as StdButton with uid="SXHZZTLXCE",left=797, top=381, width=84,height=25,;
    caption="LinkPC", nPag=1,visible=.f.;
    , HelpContextID = 68659274;
  , bGlobalFont=.t.

    proc oLinkPC_1_84.Click()
      this.Parent.oContained.GSRI_MDP.LinkPCClick()
    endproc

  add object oSIMVAL_1_85 as StdField with uid="SVVHWMNCLT",rtseq=60,rtrep=.f.,;
    cFormVar = "w_SIMVAL", cQueryName = "SIMVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 203347930,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=486, Top=11, InputMask=replicate('X',5)

  add object oDESTR1_1_89 as StdField with uid="RRATTZGNZB",rtseq=64,rtrep=.f.,;
    cFormVar = "w_DESTR1", cQueryName = "DESTR1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 101743818,;
   bGlobalFont=.t.,;
    Height=21, Width=433, Left=206, Top=152, InputMask=replicate('X',60)


  add object oMRCODSNS_1_108 as StdCombo with uid="AEVXIRIVMN",rtseq=66,rtrep=.f.,left=140,top=240,width=378,height=22;
    , tabstop=.f.;
    , ToolTipText = "Codice che identifica la tipologia di somme non soggette";
    , HelpContextID = 83259111;
    , cFormVar="w_MRCODSNS",RowSource=""+"1 - Compensi docenti/ricercatori Legge n. 2 del 28 gennaio 2009,"+"2 - Lavoratori con beneficio fiscale ex art. 3 Legge 30 dicembre 2010/238,"+"5 - Compensi soggetti art.16 del D.Igs. n. 147 del 2015,"+"6 - Assegni di servizio civile,"+"7 - Erogazione di altri redditi non soggetti a ritenuta,"+"Erogazione di altri redditi non soggetti a ritenuta (Certificazione unica 2016),"+"Erogazione di altri redditi non soggetti a ritenuta (Certificazione unica 2017)", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oMRCODSNS_1_108.RadioValue()
    return(iif(this.value =1,'1',;
    iif(this.value =2,'2',;
    iif(this.value =3,'5',;
    iif(this.value =4,'A',;
    iif(this.value =5,'7',;
    iif(this.value =6,'3',;
    iif(this.value =7,'6',;
    ' '))))))))
  endfunc
  func oMRCODSNS_1_108.GetRadio()
    this.Parent.oContained.w_MRCODSNS = this.RadioValue()
    return .t.
  endfunc

  func oMRCODSNS_1_108.SetRadio()
    this.Parent.oContained.w_MRCODSNS=trim(this.Parent.oContained.w_MRCODSNS)
    this.value = ;
      iif(this.Parent.oContained.w_MRCODSNS=='1',1,;
      iif(this.Parent.oContained.w_MRCODSNS=='2',2,;
      iif(this.Parent.oContained.w_MRCODSNS=='5',3,;
      iif(this.Parent.oContained.w_MRCODSNS=='A',4,;
      iif(this.Parent.oContained.w_MRCODSNS=='7',5,;
      iif(this.Parent.oContained.w_MRCODSNS=='3',6,;
      iif(this.Parent.oContained.w_MRCODSNS=='6',7,;
      0)))))))
  endfunc

  func oMRCODSNS_1_108.mHide()
    with this.Parent.oContained
      return (.w_MRTIPCON='C' OR .w_MRNONSO1=0)
    endwith
  endfunc


  add object oObj_1_111 as cp_runprogram with uid="PULMAKYWHB",left=272, top=554, width=256,height=20,;
    caption='GSRI_BDM(R)',;
   bGlobalFont=.t.,;
    prg="GSRI_BDM('R')",;
    cEvent = "w_MRDATREG Changed",;
    nPag=1;
    , HelpContextID = 196568627


  add object oObj_1_112 as cp_runprogram with uid="HZXKCXKPMM",left=272, top=574, width=256,height=20,;
    caption='GSRI_BDM(T)',;
   bGlobalFont=.t.,;
    prg="GSRI_BDM('T')",;
    cEvent = "w_MRCODTRI Changed",;
    nPag=1;
    , HelpContextID = 196569139

  add object oStr_1_49 as StdString with uid="ZDQVNCUGCN",Visible=.t., Left=1, Top=11,;
    Alignment=1, Width=120, Height=15,;
    Caption="Registrazione n.:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_50 as StdString with uid="DDOKZOLCSM",Visible=.t., Left=183, Top=12,;
    Alignment=2, Width=11, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_51 as StdString with uid="NMNDIOXLLT",Visible=.t., Left=253, Top=11,;
    Alignment=1, Width=37, Height=15,;
    Caption="Del:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_52 as StdString with uid="CSHSZARGGX",Visible=.t., Left=1, Top=45,;
    Alignment=1, Width=120, Height=15,;
    Caption="Documento n.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_53 as StdString with uid="BBANQUMUBO",Visible=.t., Left=255, Top=44,;
    Alignment=2, Width=11, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_54 as StdString with uid="ZIKOBVHOZJ",Visible=.t., Left=395, Top=45,;
    Alignment=1, Width=37, Height=15,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_56 as StdString with uid="BAMUHZWPXZ",Visible=.t., Left=380, Top=11,;
    Alignment=1, Width=55, Height=15,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_57 as StdString with uid="KIVIWVENVY",Visible=.t., Left=537, Top=11,;
    Alignment=1, Width=83, Height=15,;
    Caption="Cambio:"  ;
  , bGlobalFont=.t.

  func oStr_1_57.mHide()
    with this.Parent.oContained
      return (.w_CAOVAL<>0 OR EMPTY(.w_MRCODVAL))
    endwith
  endfunc

  add object oStr_1_62 as StdString with uid="EPLRVBDOCQ",Visible=.t., Left=12, Top=125,;
    Alignment=0, Width=638, Height=15,;
    Caption="Ritenute IRPEF"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_63 as StdString with uid="AQECQPVMKM",Visible=.t., Left=12, Top=309,;
    Alignment=0, Width=654, Height=15,;
    Caption="Ritenute contributi previdenziali"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_64 as StdString with uid="NNRKUGUYEL",Visible=.t., Left=4, Top=367,;
    Alignment=1, Width=117, Height=18,;
    Caption="Somme non sogg.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_65 as StdString with uid="LAETYTYLTY",Visible=.t., Left=252, Top=368,;
    Alignment=1, Width=127, Height=15,;
    Caption="Imponibile ritenuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_66 as StdString with uid="JPTLOSLCZH",Visible=.t., Left=529, Top=368,;
    Alignment=1, Width=84, Height=15,;
    Caption="% Ritenuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_67 as StdString with uid="XZKAKFYYRV",Visible=.t., Left=538, Top=396,;
    Alignment=1, Width=75, Height=15,;
    Caption="Importo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_71 as StdString with uid="RSCJXOLEPU",Visible=.t., Left=7, Top=395,;
    Alignment=1, Width=114, Height=15,;
    Caption="Numero distinta:"  ;
  , bGlobalFont=.t.

  func oStr_1_71.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_MRRIFDI2))
    endwith
  endfunc

  add object oStr_1_72 as StdString with uid="NSTOWGHIPR",Visible=.t., Left=189, Top=395,;
    Alignment=0, Width=7, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  func oStr_1_72.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_MRRIFDI2))
    endwith
  endfunc

  add object oStr_1_73 as StdString with uid="VMNMALOEDH",Visible=.t., Left=253, Top=395,;
    Alignment=1, Width=35, Height=15,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  func oStr_1_73.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_MRRIFDI2))
    endwith
  endfunc

  add object oStr_1_79 as StdString with uid="HCTYYKVQAT",Visible=.t., Left=37, Top=340,;
    Alignment=1, Width=84, Height=15,;
    Caption="Cod.tributo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_82 as StdString with uid="JCAEDRRBEQ",Visible=.t., Left=477, Top=451,;
    Alignment=1, Width=136, Height=15,;
    Caption="Importo percipiente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_90 as StdString with uid="AVCKVJPMLD",Visible=.t., Left=18, Top=209,;
    Alignment=1, Width=118, Height=18,;
    Caption="Somme non sogg.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_91 as StdString with uid="BKJXFJREEH",Visible=.t., Left=18, Top=152,;
    Alignment=1, Width=118, Height=15,;
    Caption="Cod.tributo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_92 as StdString with uid="NYQLOGUSVI",Visible=.t., Left=252, Top=209,;
    Alignment=1, Width=127, Height=15,;
    Caption="Imponibile ritenuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_93 as StdString with uid="PNZWZQDPZJ",Visible=.t., Left=538, Top=209,;
    Alignment=1, Width=75, Height=18,;
    Caption="% Ritenuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_94 as StdString with uid="SVERJXSGWY",Visible=.t., Left=538, Top=240,;
    Alignment=1, Width=75, Height=15,;
    Caption="Importo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_95 as StdString with uid="GVLTTMFJFS",Visible=.t., Left=18, Top=276,;
    Alignment=1, Width=118, Height=15,;
    Caption="Numero distinta:"  ;
  , bGlobalFont=.t.

  func oStr_1_95.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_MRRIFDI1))
    endwith
  endfunc

  add object oStr_1_96 as StdString with uid="GKMGLAXZBN",Visible=.t., Left=207, Top=276,;
    Alignment=2, Width=11, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  func oStr_1_96.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_MRRIFDI1))
    endwith
  endfunc

  add object oStr_1_97 as StdString with uid="UAXOTBJHFX",Visible=.t., Left=292, Top=276,;
    Alignment=1, Width=30, Height=15,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  func oStr_1_97.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_MRRIFDI1))
    endwith
  endfunc

  add object oStr_1_98 as StdString with uid="GCVAMVIXVW",Visible=.t., Left=18, Top=180,;
    Alignment=1, Width=118, Height=18,;
    Caption="Causale prestazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_100 as StdString with uid="PNEKQNKYYM",Visible=.t., Left=8, Top=423,;
    Alignment=1, Width=113, Height=15,;
    Caption="Spese rimborsate:"  ;
  , bGlobalFont=.t.

  add object oStr_1_101 as StdString with uid="AQUEIZAIYI",Visible=.t., Left=1, Top=77,;
    Alignment=1, Width=120, Height=18,;
    Caption="Fornitore/percip.:"  ;
  , bGlobalFont=.t.

  func oStr_1_101.mHide()
    with this.Parent.oContained
      return (.w_MRTIPCON='C')
    endwith
  endfunc

  add object oStr_1_102 as StdString with uid="FMWEBFSOFJ",Visible=.t., Left=1, Top=77,;
    Alignment=1, Width=120, Height=18,;
    Caption="Cliente:"  ;
  , bGlobalFont=.t.

  func oStr_1_102.mHide()
    with this.Parent.oContained
      return (.w_MRTIPCON='F')
    endwith
  endfunc

  add object oStr_1_103 as StdString with uid="UXMLIDZWHS",Visible=.t., Left=14, Top=102,;
    Alignment=1, Width=107, Height=18,;
    Caption="Tipo operaz.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_104 as StdString with uid="KKZIJIYFRO",Visible=.t., Left=486, Top=424,;
    Alignment=1, Width=127, Height=18,;
    Caption="% Ritenuta percipiente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_105 as StdString with uid="FWFKXSIQMH",Visible=.t., Left=264, Top=105,;
    Alignment=1, Width=79, Height=18,;
    Caption="Data di certif.:"  ;
  , bGlobalFont=.t.

  func oStr_1_105.mHide()
    with this.Parent.oContained
      return (.w_MRTIPCON='F')
    endwith
  endfunc

  add object oStr_1_106 as StdString with uid="CFBYCXXFJI",Visible=.t., Left=489, Top=105,;
    Alignment=1, Width=102, Height=18,;
    Caption="Stato della certif. :"  ;
  , bGlobalFont=.t.

  func oStr_1_106.mHide()
    with this.Parent.oContained
      return (.w_MRTIPCON='F')
    endwith
  endfunc

  add object oStr_1_107 as StdString with uid="DTGGCRMAOL",Visible=.t., Left=522, Top=180,;
    Alignment=1, Width=91, Height=18,;
    Caption="Data di pag. rit.:"  ;
  , bGlobalFont=.t.

  func oStr_1_107.mHide()
    with this.Parent.oContained
      return (.w_MRTIPCON='F')
    endwith
  endfunc

  add object oStr_1_109 as StdString with uid="GPWMFPGVFG",Visible=.t., Left=3, Top=242,;
    Alignment=1, Width=133, Height=18,;
    Caption="Cod. somme non sogg.:"  ;
  , bGlobalFont=.t.

  func oStr_1_109.mHide()
    with this.Parent.oContained
      return (.w_MRTIPCON='C' OR .w_MRNONSO1=0)
    endwith
  endfunc

  add object oBox_1_68 as StdBox with uid="YDICHFIDVI",left=7, top=143, width=717,height=1

  add object oBox_1_69 as StdBox with uid="SSORPSKKRL",left=7, top=327, width=717,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsri_amr','MOV_RITE','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".MRSERIAL=MOV_RITE.MRSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
