* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_bsc                                                        *
*              QUADRO 770/SC                                                   *
*                                                                              *
*      Author: TAMSoftware & CODELAB                                           *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][98][VRS_61]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-05-19                                                      *
* Last revis.: 2014-07-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsri_bsc",oParentObject)
return(i_retval)

define class tgsri_bsc as StdBatch
  * --- Local variables
  w_MESS = space(50)
  w_DATAFALL = ctod("  /  /  ")
  w_TIPOPERAZ = space(1)
  w_OBTEST = ctod("  /  /  ")
  w_AZCOFAZI = space(16)
  w_CODSNS = space(1)
  w_MRCODCON = space(15)
  w_CAUPRE = space(1)
  w_NRECORD = 0
  * --- WorkFile variables
  AZIENDA_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questo batch serve per convertire i movimenti ritenute in base alla maschera
    * --- di stampa del quadro 770/SC
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZCOFAZI"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZCOFAZI;
        from (i_cTable) where;
            AZCODAZI = i_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_AZCOFAZI = NVL(cp_ToDate(_read_.AZCOFAZI),cp_NullValue(_read_.AZCOFAZI))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Utilizzata nella query Gsri4bsc per la lettura delle destinazioni diverse
    this.w_OBTEST = this.oParentObject.w_DATFIN
    this.w_TIPOPERAZ = IIF (TYPE ("this.oParentObject.w_TIPOPERAZ")="C" , this.oParentObject.w_TIPOPERAZ , "" ) 
    * --- Controllo che le data di selezione siano all'interno dello stesso anno
    if STR(YEAR(this.oParentObject.w_DATFIN),4,0)<>STR(YEAR(this.oParentObject.w_DATINI),4,0)
      this.w_MESS = "L'intervallo di date deve essere interno ad un anno solare"
      ah_ErrorMsg(this.w_MESS,"!","")
      i_retcode = 'stop'
      return
    endif
    * --- Lancio la query che va a leggere nei movimenti ritenute
    * --- sdoppio la query perch� il batch viene lanciato sia per il prospetto st sia per la stampa altri percipienti
    if TYPE("THIS.OPARENTOBJECT")= "O" AND UPPER(this.oParentObject.Class)="TGSRI_SSC"
      vq_exec("..\RITE\EXE\QUERY\GSRI_SSC.VQR",this,"RITE")
    else
      this.w_DATAFALL = iif(type("this.oParentObject.oParentObject.w_RFDATFAL")<>"U",this.oParentObject.oParentObject.w_RFDATFAL,ctod("  -  -    "))
      if empty(this.w_DATAFALL)
        * --- Verifico se la routine � stata lanciata dalla maschera di stampa GSRI3SSH
        *     In questo caso valorizzo la data di fallimetno se presente
        this.w_DATAFALL = iif(type("this.oParentObject.w_RFDATFAL")<>"U",this.oParentObject.w_RFDATFAL,ctod("  -  -    "))
      endif
      vq_exec("..\RITE\EXE\QUERY\GSRI_BSC.VQR",this,"RITE")
    endif
    * --- Nel caso in cui stia stampando i dati del 2012 oppure
    *     successivi, non devo effettuare la Int ma la Round.
    *     Rendo scrivibile il cursore RITE
    WRCURSOR("RITE")
    SCAN FOR MRCODVAL<>this.oParentObject.w_CODVAL
    if year(this.oParentObject.w_DATFIN)>=2012
      REPLACE SOMTOTA WITH cp_ROUND(VALCAM(NVL(SOMTOTA,0), MRCODVAL, this.oParentObject.w_CODVAL, i_DATSYS, 0),this.oParentObject.w_DECIMI)
      REPLACE IMPONI WITH cp_ROUND(VALCAM(NVL(IMPONI,0), MRCODVAL, this.oParentObject.w_CODVAL, i_DATSYS, 0),this.oParentObject.w_DECIMI)
      REPLACE NONSOGG WITH cp_ROUND(VALCAM(NVL(NONSOGG,0), MRCODVAL, this.oParentObject.w_CODVAL, i_DATSYS, 0),this.oParentObject.w_DECIMI)
      REPLACE MRTOTIM1 WITH cp_ROUND(VALCAM(NVL(MRTOTIM1,0), MRCODVAL, this.oParentObject.w_CODVAL, i_DATSYS, 0),this.oParentObject.w_DECIMI)
      REPLACE MRTOTIM2 WITH cp_ROUND(VALCAM(NVL(MRTOTIM2,0), MRCODVAL, this.oParentObject.w_CODVAL, i_DATSYS, 0),this.oParentObject.w_DECIMI)
      REPLACE MRIMPPER WITH cp_ROUND(VALCAM(NVL(MRIMPPER,0), MRCODVAL, this.oParentObject.w_CODVAL, i_DATSYS, 0),this.oParentObject.w_DECIMI)
      REPLACE MRIMPRI2 WITH cp_ROUND(VALCAM(NVL(MRIMPRI2,0), MRCODVAL, this.oParentObject.w_CODVAL, i_DATSYS, 0),this.oParentObject.w_DECIMI)
      REPLACE MRSPERIM WITH cp_ROUND(VALCAM(NVL(MRSPERIM,0), MRCODVAL, this.oParentObject.w_CODVAL, i_DATSYS, 0),this.oParentObject.w_DECIMI)
      REPLACE RITERIMB WITH cp_ROUND(VALCAM(NVL(RITERIMB,0), MRCODVAL, this.oParentObject.w_CODVAL, i_DATSYS, 0),this.oParentObject.w_DECIMI)
      * --- Gestioni importi prima e post fallimento
      REPLACE IMPREFALL WITH cp_ROUND(VALCAM(NVL(IMPREFALL,0), MRCODVAL, this.oParentObject.w_CODVAL, i_DATSYS, 0),this.oParentObject.w_DECIMI)
      REPLACE IMPOSFALL WITH cp_ROUND(VALCAM(NVL(IMPOSFALL,0), MRCODVAL, this.oParentObject.w_CODVAL, i_DATSYS, 0),this.oParentObject.w_DECIMI)
    else
      REPLACE SOMTOTA WITH INT(VALCAM(NVL(SOMTOTA,0), MRCODVAL, this.oParentObject.w_CODVAL, i_DATSYS, 0))
      REPLACE IMPONI WITH INT(VALCAM(NVL(IMPONI,0), MRCODVAL, this.oParentObject.w_CODVAL, i_DATSYS, 0))
      REPLACE NONSOGG WITH INT(VALCAM(NVL(NONSOGG,0), MRCODVAL, this.oParentObject.w_CODVAL, i_DATSYS, 0))
      REPLACE MRTOTIM1 WITH INT(VALCAM(NVL(MRTOTIM1,0), MRCODVAL, this.oParentObject.w_CODVAL, i_DATSYS, 0))
      REPLACE MRTOTIM2 WITH INT(VALCAM(NVL(MRTOTIM2,0), MRCODVAL, this.oParentObject.w_CODVAL, i_DATSYS, 0))
      REPLACE MRIMPPER WITH INT(VALCAM(NVL(MRIMPPER,0), MRCODVAL, this.oParentObject.w_CODVAL, i_DATSYS, 0))
      REPLACE MRIMPRI2 WITH INT(VALCAM(NVL(MRIMPRI2,0), MRCODVAL, this.oParentObject.w_CODVAL, i_DATSYS, 0))
      REPLACE MRSPERIM WITH INT(VALCAM(NVL(MRSPERIM,0), MRCODVAL, this.oParentObject.w_CODVAL, i_DATSYS, 0))
      REPLACE RITERIMB WITH INT(VALCAM(NVL(RITERIMB,0), MRCODVAL, this.oParentObject.w_CODVAL, i_DATSYS, 0))
      * --- Gestioni importi prima e post fallimento
      REPLACE IMPREFALL WITH INT(VALCAM(NVL(IMPREFALL,0), MRCODVAL, this.oParentObject.w_CODVAL, i_DATSYS, 0))
      REPLACE IMPOSFALL WITH INT(VALCAM(NVL(IMPOSFALL,0), MRCODVAL, this.oParentObject.w_CODVAL, i_DATSYS, 0))
    endif
    ENDSCAN
    * --- Seleziono il cursore RITE e sommo i vari campi nella valuta selezionata in maschera
    if TYPE("THIS.OPARENTOBJECT")= "O" AND UPPER(this.oParentObject.Class)="TGSRI_SSC"
      SELECT MRCODCON,ANCODFIS,ANPERFIS,ANCOGNOM,AN__NOME, ANDESCRI, AN_SESSO,ANDATNAS,; 
 ANLOCNAS,ANPRONAS, ANLOCALI, ANPROVIN, ANINDIRI, ANNAZION, NADESNAZ,NACODEST,ANFLGEST,; 
 MRCODTRI, SUM(SOMTOTA) AS SOMTOTA, SUM(IMPONI) AS IMPONI, SUM(NONSOGG) AS NONSOGG,; 
 SUM(MRTOTIM1) as MRTOTIM1 ,SUM(MRTOTIM2) as MRTOTIM2,SUM(MRIMPPER) as MRIMPPER,; 
 MRCODVAL,TRCAUPRE,SUM(MRIMPRI2) as MRIMPRI2,; 
 MIN(VPCODREG) as VPCODREG, MIN(ANFLSGRE) as ANFLSGRE,SUM(MRSPERIM) AS MRSPERIM,; 
 SUM(RITERIMB) AS RITERIMB, ; 
 SUM(IMPOSFALL) as IMPOSFALL,SUM(IMPREFALL) as IMPREFALL, MAX(ANCODREG) as PERCODREG,MAX(ANCODCOM) AS ANCODCOM ; 
 FROM RITE GROUP BY MRCODCON, MRCODTRI,TRCAUPRE; 
 ORDER BY MRCODCON ,MRCODTRI, TRCAUPRE into cursor __tmp__
    else
      SELECT MRCODCON,MAX(ANFLGEST) AS ANFLGEST,MAX(ANCODFIS) ANCODFIS,; 
 MAX(ANCOGNOM) AS ANCOGNOM,MAX(AN__NOME) AS AN__NOME, MAX(ANDESCRI) AS ANDESCRI,; 
 MAX(AN_SESSO) AS AN_SESSO,MAX(ANDATNAS) ANDATNAS,MAX(ANLOCNAS) AS ANLOCNAS,; 
 MAX(ANPRONAS) AS ANPRONAS,MAX(AN_EREDE) AS AN_EREDE,MAX(ANEVEECC) AS ANEVEECC,; 
 MAX(ANCOFISC) AS ANCOFISC,MAX(ANNAZION) AS ANNAZION,MAX(ANLOCALI) AS ANLOCALI,; 
 MAX(ANINDIRI) AS ANINDIRI,MAX(ANPROVIN) AS ANPROVIN,; 
 TRCAUPRE,MAX(ANCODREG) as PERCODREG,SUM(SOMTOTA) AS SOMTOTA,MIN(ANFLSGRE) AS ANFLSGRE,; 
 SUM(NONSOGG) AS NONSOGG,SUM(IMPONI) AS IMPONI,SUM(MRTOTIM1) as MRTOTIM1,; 
 SUM(MRTOTIM2) as MRTOTIM2,SUM(MRIMPPER) as MRIMPPER,SUM(MRSPERIM) AS MRSPERIM,; 
 SUM(RITERIMB) AS RITERIMB,SUM(IMPOSFALL) as IMPOSFALL,SUM(IMPREFALL) as IMPREFALL,; 
 MAX(NACODEST) AS NACODEST,MAX(ANPERFIS) AS ANPERFIS, MRCODSNS,; 
 MAX(TIPOPREST) AS TIPOPREST, MAX(ANCODCOM) AS ANCODCOM; 
 FROM RITE GROUP BY MRCODCON, TRCAUPRE,MRCODSNS; 
 ORDER BY MRCODCON ,TRCAUPRE,MRCODSNS into cursor __tmp__
    endif
    * --- Generazione File Telematico - Modello 770 
    if lower(this.oParentObject.Class) = "tgsri5bft" or lower(this.oParentObject.Class) = "tgsri4bft" or like("tgsut?bft",lower(this.oParentObject.Class)) or like("tgsri?bsp",lower(this.oParentObject.Class)) or like("tgsri??bsp",lower(this.oParentObject.Class))
      select * from __tmp__ into cursor RITE1
    endif
    L_ANNO=this.oParentObject.w_ANNO
    L_PERCFIN=this.oParentObject.w_PERCFIN
    L_CODVAL=this.oParentObject.w_CODVAL
    L_DATFIN=this.oParentObject.w_DATFIN
    L_DECIMI=this.oParentObject.w_DECIMI
    L_PERCIN=this.oParentObject.w_PERCIN
    L_DATINI=this.oParentObject.w_DATINI
    L_AZCOFAZI=this.w_AZCOFAZI
    * --- Generazione File Telematico - Modello 770
    if lower(this.oParentObject.Class) = "tgsri_ssc"
      * --- Lancio il report di stampa per il modello 770/2001
      CP_CHPRN("..\RITE\EXE\QUERY\GSRI_SSC.FRX","",this.oParentObject)
    endif
    if lower(this.oParentObject.Class) = "tgsri_ssh" Or lower(this.oParentObject.Class) = "tgsri1ssh" Or lower(this.oParentObject.Class) = "tgsri2ssh"
      * --- Lancio il report di stampa per il modello 770/2003
      CP_CHPRN("..\RITE\EXE\QUERY\GSRI_SSH.FRX","",this.oParentObject)
    endif
    if lower(this.oParentObject.Class) = "tgsri3ssh" OR lower(this.oParentObject.Class) = "tgsri_sss"
      * --- Lancio il report di stampa per il modello 770/2005  -  770/2006  -  770/2007 -  770/2008 - 770/2010
      * --- Aggiungo i campi per i dati relativi ai contributi previdenziali
      =wrcursor("__TMP__")
      do case
        case year(this.oParentObject.w_DATFIN)<=2005
           
 ALTER TABLE __TMP__ ADD COLUMN AU31 N(20,5) NULL 
 ALTER TABLE __TMP__ ADD COLUMN AU32 N(20,5) NULL 
 ALTER TABLE __TMP__ ADD COLUMN AU33 N(20,5) NULL 
 ALTER TABLE __TMP__ ADD COLUMN AU34 N(20,5) NULL
        case year(this.oParentObject.w_DATFIN)>=2006
           
 ALTER TABLE __TMP__ ADD COLUMN AU33 N(20,5) NULL 
 ALTER TABLE __TMP__ ADD COLUMN AU34 N(20,5) NULL 
 ALTER TABLE __TMP__ ADD COLUMN AU35 N(20,5) NULL 
 ALTER TABLE __TMP__ ADD COLUMN AU36 N(20,5) NULL 
 ALTER TABLE __TMP__ ADD COLUMN AU37 N(20,5) NULL 
 ALTER TABLE __TMP__ ADD COLUMN AU38 N(20,5) NULL
      endcase
      select __TMP__ 
 Go Top
      Scan
      this.w_MRCODCON = NVL(MRCODCON,"")
      this.w_CAUPRE = UPPER(NVL(TRCAUPRE," "))
      this.w_CODSNS = NVL(MRCODSNS," ")
      do case
        case year(this.oParentObject.w_DATFIN)<=2005
          this.w_NRECORD = RECNO()
          if this.w_CAUPRE $ "M-V-C"
            select MRCODCON,SUM(MRTOTIM2) as MRTOTIM2,SUM(MRIMPPER) as MRIMPPER,TRCAUPRE ; 
 from __TMP__ where TRCAUPRE=this.w_CAUPRE and MRCODCON=this.w_MRCODCON group by MRCODCON,TRCAUPRE into cursor TOTPRE
            update __TMP__ set AU31=NVL(TOTPRE.MRTOTIM2,0) -NVL(TOTPRE.MRIMPPER,0),AU32=NVL(TOTPRE.MRIMPPER,0); 
 where __TMP__.MRCODCON=this.w_MRCODCON and __TMP__.TRCAUPRE=this.w_CAUPRE
            * --- Elimino il cursore dei totali per causale prestazione
            if used("TOTPRE")
              select TOTPRE
              use
            endif
            Select __TMP__
          endif
          update __TMP__ set AU33=NVL(MRSPERIM,0) where __TMP__.MRCODCON=this.w_MRCODCON and __TMP__.TRCAUPRE=this.w_CAUPRE
          Goto This.w_NRECORD
          if NVL(RITERIMB,0) < 0 and this.w_CAUPRE $ "X-Y"
            update __TMP__ set AU34=ABS(NVL(RITERIMB,0)) where __TMP__.MRCODCON=this.w_MRCODCON and __TMP__.TRCAUPRE=this.w_CAUPRE
          endif
          Goto This.w_NRECORD
        case year(this.oParentObject.w_DATFIN)>=2006
          this.w_NRECORD = RECNO()
          if this.w_CAUPRE $ "M-V-C"
            update __TMP__ set AU33=NVL(__TMP__.MRTOTIM2,0) -NVL(__TMP__.MRIMPPER,0),AU34=NVL(__TMP__.MRIMPPER,0); 
 where __TMP__.MRCODCON=this.w_MRCODCON and __TMP__.TRCAUPRE=this.w_CAUPRE AND __TMP__.MRCODSNS=this.w_CODSNS
            Select __TMP__
          endif
          update __TMP__ set AU35=NVL(MRSPERIM,0) where __TMP__.MRCODCON=this.w_MRCODCON and __TMP__.TRCAUPRE=this.w_CAUPRE AND __TMP__.MRCODSNS=this.w_CODSNS
          Goto This.w_NRECORD
          if NVL(RITERIMB,0) < 0 and this.w_CAUPRE $ "X-Y"
            update __TMP__ set AU36=ABS(NVL(RITERIMB,0)) where __TMP__.MRCODCON=this.w_MRCODCON and __TMP__.TRCAUPRE=this.w_CAUPRE AND __TMP__.MRCODSNS=this.w_CODSNS
          endif
          if ! EMPTY(this.w_DATAFALL)
            update __TMP__ set AU37=ABS(NVL(IMPREFALL,0)) where __TMP__.MRCODCON=this.w_MRCODCON and __TMP__.TRCAUPRE=this.w_CAUPRE AND __TMP__.MRCODSNS=this.w_CODSNS
            update __TMP__ set AU38=ABS(NVL(IMPOSFALL,0)) where __TMP__.MRCODCON=this.w_MRCODCON and __TMP__.TRCAUPRE=this.w_CAUPRE AND __TMP__.MRCODSNS=this.w_CODSNS
          endif
          Goto This.w_NRECORD
      endcase
      Endscan
      do case
        case year(this.oParentObject.w_DATFIN)<=2005
          * --- Differenziare il report di stampa per gestire i nuovi campi ..33 e ..34
          CP_CHPRN("..\RITE\EXE\QUERY\GSRI1SSH.FRX","",this.oParentObject)
        case year(this.oParentObject.w_DATFIN)>=2006 AND year(this.oParentObject.w_DATFIN)<=2011
          if lower(this.oParentObject.Class) = "tgsri_sss"
            * --- Stampa prospetto SS
            CP_CHPRN("..\RITE\EXE\QUERY\GSRI_SSS.FRX","",this.oParentObject)
          else
            CP_CHPRN("..\RITE\EXE\QUERY\GSRI2SSH.FRX","",this.oParentObject)
          endif
        case year(this.oParentObject.w_DATFIN)>=2012 
          if lower(this.oParentObject.Class) = "tgsri_sss"
            CP_CHPRN("..\RITE\EXE\QUERY\GSRI1SSS.FRX","",this.oParentObject)
          else
            CP_CHPRN("..\RITE\EXE\QUERY\GSRI3SSH.FRX","",this.oParentObject)
          endif
      endcase
    endif
    if used("RITE")
      select ("RITE")
      USE
    endif
    if used("__tmp__")
      select ("__tmp__")
      USE
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='AZIENDA'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
