* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_mpr                                                        *
*              Dati previdenziali                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2016-01-13                                                      *
* Last revis.: 2017-02-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsri_mpr")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsri_mpr")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsri_mpr")
  return

* --- Class definition
define class tgsri_mpr as StdPCForm
  Width  = 795
  Height = 303
  Top    = 10
  Left   = 10
  cComment = "Dati previdenziali"
  cPrg = "gsri_mpr"
  HelpContextID=147453545
  add object cnt as tcgsri_mpr
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsri_mpr as PCContext
  w_DPSERIAL = space(10)
  w_DPROGMOD = 0
  w_DPAU0029 = space(11)
  w_DPAU0030 = space(60)
  w_DPAU0031 = space(2)
  w_DPAU0032 = space(40)
  w_DPAU0033 = space(2)
  w_DPAU0036 = space(1)
  w_DPAU0034 = 0
  w_DPAU0035 = 0
  w_DPAU0037 = 0
  w_DPAU0038 = 0
  w_DPAU0039 = 0
  w_CH__ANNO = 0
  proc Save(i_oFrom)
    this.w_DPSERIAL = i_oFrom.w_DPSERIAL
    this.w_DPROGMOD = i_oFrom.w_DPROGMOD
    this.w_DPAU0029 = i_oFrom.w_DPAU0029
    this.w_DPAU0030 = i_oFrom.w_DPAU0030
    this.w_DPAU0031 = i_oFrom.w_DPAU0031
    this.w_DPAU0032 = i_oFrom.w_DPAU0032
    this.w_DPAU0033 = i_oFrom.w_DPAU0033
    this.w_DPAU0036 = i_oFrom.w_DPAU0036
    this.w_DPAU0034 = i_oFrom.w_DPAU0034
    this.w_DPAU0035 = i_oFrom.w_DPAU0035
    this.w_DPAU0037 = i_oFrom.w_DPAU0037
    this.w_DPAU0038 = i_oFrom.w_DPAU0038
    this.w_DPAU0039 = i_oFrom.w_DPAU0039
    this.w_CH__ANNO = i_oFrom.w_CH__ANNO
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_DPSERIAL = this.w_DPSERIAL
    i_oTo.w_DPROGMOD = this.w_DPROGMOD
    i_oTo.w_DPAU0029 = this.w_DPAU0029
    i_oTo.w_DPAU0030 = this.w_DPAU0030
    i_oTo.w_DPAU0031 = this.w_DPAU0031
    i_oTo.w_DPAU0032 = this.w_DPAU0032
    i_oTo.w_DPAU0033 = this.w_DPAU0033
    i_oTo.w_DPAU0036 = this.w_DPAU0036
    i_oTo.w_DPAU0034 = this.w_DPAU0034
    i_oTo.w_DPAU0035 = this.w_DPAU0035
    i_oTo.w_DPAU0037 = this.w_DPAU0037
    i_oTo.w_DPAU0038 = this.w_DPAU0038
    i_oTo.w_DPAU0039 = this.w_DPAU0039
    i_oTo.w_CH__ANNO = this.w_CH__ANNO
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsri_mpr as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 795
  Height = 303
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2017-02-07"
  HelpContextID=147453545
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=14

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  CUDDPTDH_IDX = 0
  cFile = "CUDDPTDH"
  cKeySelect = "DPSERIAL"
  cKeyWhere  = "DPSERIAL=this.w_DPSERIAL"
  cKeyDetail  = "DPSERIAL=this.w_DPSERIAL and DPROGMOD=this.w_DPROGMOD"
  cKeyWhereODBC = '"DPSERIAL="+cp_ToStrODBC(this.w_DPSERIAL)';

  cKeyDetailWhereODBC = '"DPSERIAL="+cp_ToStrODBC(this.w_DPSERIAL)';
      +'+" and DPROGMOD="+cp_ToStrODBC(this.w_DPROGMOD)';

  cKeyWhereODBCqualified = '"CUDDPTDH.DPSERIAL="+cp_ToStrODBC(this.w_DPSERIAL)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cPrg = "gsri_mpr"
  cComment = "Dati previdenziali"
  i_nRowNum = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_DPSERIAL = space(10)
  w_DPROGMOD = 0
  w_DPAU0029 = space(11)
  w_DPAU0030 = space(60)
  w_DPAU0031 = space(2)
  w_DPAU0032 = space(40)
  w_DPAU0033 = space(2)
  w_DPAU0036 = space(1)
  w_DPAU0034 = 0
  w_DPAU0035 = 0
  w_DPAU0037 = 0
  w_DPAU0038 = 0
  w_DPAU0039 = 0
  w_CH__ANNO = 0
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsri_mprPag1","gsri_mpr",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='CUDDPTDH'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CUDDPTDH_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CUDDPTDH_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsri_mpr'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from CUDDPTDH where DPSERIAL=KeySet.DPSERIAL
    *                            and DPROGMOD=KeySet.DPROGMOD
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.CUDDPTDH_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CUDDPTDH_IDX,2],this.bLoadRecFilter,this.CUDDPTDH_IDX,"gsri_mpr")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CUDDPTDH')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CUDDPTDH.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' CUDDPTDH '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'DPSERIAL',this.w_DPSERIAL  )
      select * from (i_cTable) CUDDPTDH where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_CH__ANNO = 0
        .w_DPSERIAL = NVL(DPSERIAL,space(10))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'CUDDPTDH')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_DPROGMOD = NVL(DPROGMOD,0)
          .w_DPAU0029 = NVL(DPAU0029,space(11))
          .w_DPAU0030 = NVL(DPAU0030,space(60))
          .w_DPAU0031 = NVL(DPAU0031,space(2))
          .w_DPAU0032 = NVL(DPAU0032,space(40))
          .w_DPAU0033 = NVL(DPAU0033,space(2))
          .w_DPAU0036 = NVL(DPAU0036,space(1))
          .w_DPAU0034 = NVL(DPAU0034,0)
          .w_DPAU0035 = NVL(DPAU0035,0)
          .w_DPAU0037 = NVL(DPAU0037,0)
          .w_DPAU0038 = NVL(DPAU0038,0)
          .w_DPAU0039 = NVL(DPAU0039,0)
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace DPROGMOD with .w_DPROGMOD
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_DPSERIAL=space(10)
      .w_DPROGMOD=0
      .w_DPAU0029=space(11)
      .w_DPAU0030=space(60)
      .w_DPAU0031=space(2)
      .w_DPAU0032=space(40)
      .w_DPAU0033=space(2)
      .w_DPAU0036=space(1)
      .w_DPAU0034=0
      .w_DPAU0035=0
      .w_DPAU0037=0
      .w_DPAU0038=0
      .w_DPAU0039=0
      .w_CH__ANNO=0
      if .cFunction<>"Filter"
        .DoRTCalc(1,7,.f.)
        .w_DPAU0036 = 'N'
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'CUDDPTDH')
    this.DoRTCalc(9,14,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oDPAU0029_2_2.enabled = i_bVal
      .Page1.oPag.oDPAU0030_2_3.enabled = i_bVal
      .Page1.oPag.oDPAU0031_2_4.enabled = i_bVal
      .Page1.oPag.oDPAU0032_2_5.enabled = i_bVal
      .Page1.oPag.oDPAU0033_2_6.enabled = i_bVal
      .Page1.oPag.oDPAU0036_2_7.enabled = i_bVal
      .Page1.oPag.oDPAU0034_2_8.enabled = i_bVal
      .Page1.oPag.oDPAU0035_2_9.enabled = i_bVal
      .Page1.oPag.oDPAU0037_2_10.enabled = i_bVal
      .Page1.oPag.oDPAU0038_2_11.enabled = i_bVal
      .Page1.oPag.oDPAU0039_2_12.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'CUDDPTDH',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CUDDPTDH_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPSERIAL,"DPSERIAL",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_DPROGMOD N(8);
      ,t_DPAU0029 C(11);
      ,t_DPAU0030 C(60);
      ,t_DPAU0031 C(2);
      ,t_DPAU0032 C(40);
      ,t_DPAU0033 C(2);
      ,t_DPAU0036 N(3);
      ,t_DPAU0034 N(18,4);
      ,t_DPAU0035 N(18,4);
      ,t_DPAU0037 N(18,4);
      ,t_DPAU0038 N(18,4);
      ,t_DPAU0039 N(18,4);
      ,DPROGMOD N(8);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsri_mprbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDPROGMOD_2_1.controlsource=this.cTrsName+'.t_DPROGMOD'
    this.oPgFRm.Page1.oPag.oDPAU0029_2_2.controlsource=this.cTrsName+'.t_DPAU0029'
    this.oPgFRm.Page1.oPag.oDPAU0030_2_3.controlsource=this.cTrsName+'.t_DPAU0030'
    this.oPgFRm.Page1.oPag.oDPAU0031_2_4.controlsource=this.cTrsName+'.t_DPAU0031'
    this.oPgFRm.Page1.oPag.oDPAU0032_2_5.controlsource=this.cTrsName+'.t_DPAU0032'
    this.oPgFRm.Page1.oPag.oDPAU0033_2_6.controlsource=this.cTrsName+'.t_DPAU0033'
    this.oPgFRm.Page1.oPag.oDPAU0036_2_7.controlsource=this.cTrsName+'.t_DPAU0036'
    this.oPgFRm.Page1.oPag.oDPAU0034_2_8.controlsource=this.cTrsName+'.t_DPAU0034'
    this.oPgFRm.Page1.oPag.oDPAU0035_2_9.controlsource=this.cTrsName+'.t_DPAU0035'
    this.oPgFRm.Page1.oPag.oDPAU0037_2_10.controlsource=this.cTrsName+'.t_DPAU0037'
    this.oPgFRm.Page1.oPag.oDPAU0038_2_11.controlsource=this.cTrsName+'.t_DPAU0038'
    this.oPgFRm.Page1.oPag.oDPAU0039_2_12.controlsource=this.cTrsName+'.t_DPAU0039'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDPROGMOD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.CUDDPTDH_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CUDDPTDH_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CUDDPTDH_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CUDDPTDH_IDX,2])
      *
      * insert into CUDDPTDH
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CUDDPTDH')
        i_extval=cp_InsertValODBCExtFlds(this,'CUDDPTDH')
        i_cFldBody=" "+;
                  "(DPSERIAL,DPROGMOD,DPAU0029,DPAU0030,DPAU0031"+;
                  ",DPAU0032,DPAU0033,DPAU0036,DPAU0034,DPAU0035"+;
                  ",DPAU0037,DPAU0038,DPAU0039,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_DPSERIAL)+","+cp_ToStrODBC(this.w_DPROGMOD)+","+cp_ToStrODBC(this.w_DPAU0029)+","+cp_ToStrODBC(this.w_DPAU0030)+","+cp_ToStrODBC(this.w_DPAU0031)+;
             ","+cp_ToStrODBC(this.w_DPAU0032)+","+cp_ToStrODBC(this.w_DPAU0033)+","+cp_ToStrODBC(this.w_DPAU0036)+","+cp_ToStrODBC(this.w_DPAU0034)+","+cp_ToStrODBC(this.w_DPAU0035)+;
             ","+cp_ToStrODBC(this.w_DPAU0037)+","+cp_ToStrODBC(this.w_DPAU0038)+","+cp_ToStrODBC(this.w_DPAU0039)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CUDDPTDH')
        i_extval=cp_InsertValVFPExtFlds(this,'CUDDPTDH')
        cp_CheckDeletedKey(i_cTable,0,'DPSERIAL',this.w_DPSERIAL,'DPROGMOD',this.w_DPROGMOD)
        INSERT INTO (i_cTable) (;
                   DPSERIAL;
                  ,DPROGMOD;
                  ,DPAU0029;
                  ,DPAU0030;
                  ,DPAU0031;
                  ,DPAU0032;
                  ,DPAU0033;
                  ,DPAU0036;
                  ,DPAU0034;
                  ,DPAU0035;
                  ,DPAU0037;
                  ,DPAU0038;
                  ,DPAU0039;
                  ,CPCCCHK &i_extfld.) VALUES (;
                  this.w_DPSERIAL;
                  ,this.w_DPROGMOD;
                  ,this.w_DPAU0029;
                  ,this.w_DPAU0030;
                  ,this.w_DPAU0031;
                  ,this.w_DPAU0032;
                  ,this.w_DPAU0033;
                  ,this.w_DPAU0036;
                  ,this.w_DPAU0034;
                  ,this.w_DPAU0035;
                  ,this.w_DPAU0037;
                  ,this.w_DPAU0038;
                  ,this.w_DPAU0039;
                  ,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.CUDDPTDH_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CUDDPTDH_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_DPROGMOD))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'CUDDPTDH')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and DPROGMOD="+cp_ToStrODBC(&i_TN.->DPROGMOD)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'CUDDPTDH')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and DPROGMOD=&i_TN.->DPROGMOD;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_DPROGMOD))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and DPROGMOD="+cp_ToStrODBC(&i_TN.->DPROGMOD)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and DPROGMOD=&i_TN.->DPROGMOD;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = 0
              replace DPROGMOD with this.w_DPROGMOD
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update CUDDPTDH
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'CUDDPTDH')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " DPAU0029="+cp_ToStrODBC(this.w_DPAU0029)+;
                     ",DPAU0030="+cp_ToStrODBC(this.w_DPAU0030)+;
                     ",DPAU0031="+cp_ToStrODBC(this.w_DPAU0031)+;
                     ",DPAU0032="+cp_ToStrODBC(this.w_DPAU0032)+;
                     ",DPAU0033="+cp_ToStrODBC(this.w_DPAU0033)+;
                     ",DPAU0036="+cp_ToStrODBC(this.w_DPAU0036)+;
                     ",DPAU0034="+cp_ToStrODBC(this.w_DPAU0034)+;
                     ",DPAU0035="+cp_ToStrODBC(this.w_DPAU0035)+;
                     ",DPAU0037="+cp_ToStrODBC(this.w_DPAU0037)+;
                     ",DPAU0038="+cp_ToStrODBC(this.w_DPAU0038)+;
                     ",DPAU0039="+cp_ToStrODBC(this.w_DPAU0039)+;
                     ",DPROGMOD="+cp_ToStrODBC(this.w_DPROGMOD)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+;
                             " and DPROGMOD="+cp_ToStrODBC(DPROGMOD)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'CUDDPTDH')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      DPAU0029=this.w_DPAU0029;
                     ,DPAU0030=this.w_DPAU0030;
                     ,DPAU0031=this.w_DPAU0031;
                     ,DPAU0032=this.w_DPAU0032;
                     ,DPAU0033=this.w_DPAU0033;
                     ,DPAU0036=this.w_DPAU0036;
                     ,DPAU0034=this.w_DPAU0034;
                     ,DPAU0035=this.w_DPAU0035;
                     ,DPAU0037=this.w_DPAU0037;
                     ,DPAU0038=this.w_DPAU0038;
                     ,DPAU0039=this.w_DPAU0039;
                     ,DPROGMOD=this.w_DPROGMOD;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                                      and DPROGMOD=&i_TN.->DPROGMOD;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.CUDDPTDH_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CUDDPTDH_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_DPROGMOD))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete CUDDPTDH
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and DPROGMOD="+cp_ToStrODBC(&i_TN.->DPROGMOD)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and DPROGMOD=&i_TN.->DPROGMOD;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_DPROGMOD))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CUDDPTDH_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CUDDPTDH_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,14,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_4.visible=!this.oPgFrm.Page1.oPag.oStr_1_4.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oDPAU0031_2_4.visible=!this.oPgFrm.Page1.oPag.oDPAU0031_2_4.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oDPAU0029_2_2.value==this.w_DPAU0029)
      this.oPgFrm.Page1.oPag.oDPAU0029_2_2.value=this.w_DPAU0029
      replace t_DPAU0029 with this.oPgFrm.Page1.oPag.oDPAU0029_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDPAU0030_2_3.value==this.w_DPAU0030)
      this.oPgFrm.Page1.oPag.oDPAU0030_2_3.value=this.w_DPAU0030
      replace t_DPAU0030 with this.oPgFrm.Page1.oPag.oDPAU0030_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDPAU0031_2_4.value==this.w_DPAU0031)
      this.oPgFrm.Page1.oPag.oDPAU0031_2_4.value=this.w_DPAU0031
      replace t_DPAU0031 with this.oPgFrm.Page1.oPag.oDPAU0031_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDPAU0032_2_5.value==this.w_DPAU0032)
      this.oPgFrm.Page1.oPag.oDPAU0032_2_5.value=this.w_DPAU0032
      replace t_DPAU0032 with this.oPgFrm.Page1.oPag.oDPAU0032_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDPAU0033_2_6.value==this.w_DPAU0033)
      this.oPgFrm.Page1.oPag.oDPAU0033_2_6.value=this.w_DPAU0033
      replace t_DPAU0033 with this.oPgFrm.Page1.oPag.oDPAU0033_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDPAU0036_2_7.RadioValue()==this.w_DPAU0036)
      this.oPgFrm.Page1.oPag.oDPAU0036_2_7.SetRadio()
      replace t_DPAU0036 with this.oPgFrm.Page1.oPag.oDPAU0036_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDPAU0034_2_8.value==this.w_DPAU0034)
      this.oPgFrm.Page1.oPag.oDPAU0034_2_8.value=this.w_DPAU0034
      replace t_DPAU0034 with this.oPgFrm.Page1.oPag.oDPAU0034_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDPAU0035_2_9.value==this.w_DPAU0035)
      this.oPgFrm.Page1.oPag.oDPAU0035_2_9.value=this.w_DPAU0035
      replace t_DPAU0035 with this.oPgFrm.Page1.oPag.oDPAU0035_2_9.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDPAU0037_2_10.value==this.w_DPAU0037)
      this.oPgFrm.Page1.oPag.oDPAU0037_2_10.value=this.w_DPAU0037
      replace t_DPAU0037 with this.oPgFrm.Page1.oPag.oDPAU0037_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDPAU0038_2_11.value==this.w_DPAU0038)
      this.oPgFrm.Page1.oPag.oDPAU0038_2_11.value=this.w_DPAU0038
      replace t_DPAU0038 with this.oPgFrm.Page1.oPag.oDPAU0038_2_11.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDPAU0039_2_12.value==this.w_DPAU0039)
      this.oPgFrm.Page1.oPag.oDPAU0039_2_12.value=this.w_DPAU0039
      replace t_DPAU0039 with this.oPgFrm.Page1.oPag.oDPAU0039_2_12.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDPROGMOD_2_1.value==this.w_DPROGMOD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDPROGMOD_2_1.value=this.w_DPROGMOD
      replace t_DPROGMOD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDPROGMOD_2_1.value
    endif
    cp_SetControlsValueExtFlds(this,'CUDDPTDH')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(iif(Not Empty(.w_Dpau0029),Chkcfp(.w_Dpau0029,"PI", "", "", ""),.t.)) and (not(Empty(.w_DPROGMOD)))
          .oNewFocus=.oPgFrm.Page1.oPag.oDPAU0029_2_2
          i_bRes = .f.
          i_bnoChk = .f.
      endcase
      if not(Empty(.w_DPROGMOD))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_DPROGMOD)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_DPROGMOD=0
      .w_DPAU0029=space(11)
      .w_DPAU0030=space(60)
      .w_DPAU0031=space(2)
      .w_DPAU0032=space(40)
      .w_DPAU0033=space(2)
      .w_DPAU0036=space(1)
      .w_DPAU0034=0
      .w_DPAU0035=0
      .w_DPAU0037=0
      .w_DPAU0038=0
      .w_DPAU0039=0
      .DoRTCalc(1,7,.f.)
        .w_DPAU0036 = 'N'
    endwith
    this.DoRTCalc(9,14,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_DPROGMOD = t_DPROGMOD
    this.w_DPAU0029 = t_DPAU0029
    this.w_DPAU0030 = t_DPAU0030
    this.w_DPAU0031 = t_DPAU0031
    this.w_DPAU0032 = t_DPAU0032
    this.w_DPAU0033 = t_DPAU0033
    this.w_DPAU0036 = this.oPgFrm.Page1.oPag.oDPAU0036_2_7.RadioValue(.t.)
    this.w_DPAU0034 = t_DPAU0034
    this.w_DPAU0035 = t_DPAU0035
    this.w_DPAU0037 = t_DPAU0037
    this.w_DPAU0038 = t_DPAU0038
    this.w_DPAU0039 = t_DPAU0039
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_DPROGMOD with this.w_DPROGMOD
    replace t_DPAU0029 with this.w_DPAU0029
    replace t_DPAU0030 with this.w_DPAU0030
    replace t_DPAU0031 with this.w_DPAU0031
    replace t_DPAU0032 with this.w_DPAU0032
    replace t_DPAU0033 with this.w_DPAU0033
    replace t_DPAU0036 with this.oPgFrm.Page1.oPag.oDPAU0036_2_7.ToRadio()
    replace t_DPAU0034 with this.w_DPAU0034
    replace t_DPAU0035 with this.w_DPAU0035
    replace t_DPAU0037 with this.w_DPAU0037
    replace t_DPAU0038 with this.w_DPAU0038
    replace t_DPAU0039 with this.w_DPAU0039
    if i_srv='A'
      replace DPROGMOD with this.w_DPROGMOD
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsri_mprPag1 as StdContainer
  Width  = 791
  height = 303
  stdWidth  = 791
  stdheight = 303
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=5, top=9, width=79,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=1,Field1="DPROGMOD",Label1="Mod. N.",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 168637306

  add object oStr_1_2 as StdString with uid="PIGETOKDCB",Visible=.t., Left=111, Top=7,;
    Alignment=0, Width=180, Height=18,;
    Caption="Codice fiscale ente previdenziale"  ;
  , bGlobalFont=.t.

  add object oStr_1_3 as StdString with uid="BQXMJHUUYB",Visible=.t., Left=111, Top=56,;
    Alignment=0, Width=245, Height=18,;
    Caption="Denominazione ente previdenziale"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="BBPXAMEUXB",Visible=.t., Left=111, Top=106,;
    Alignment=0, Width=206, Height=18,;
    Caption="Codice ente previdenziale"  ;
  , bGlobalFont=.t.

  func oStr_1_4.mHide()
    with this.Parent.oContained
      return (.w_CH__ANNO>=2016)
    endwith
  endfunc

  add object oStr_1_5 as StdString with uid="ESYWUNJKDP",Visible=.t., Left=111, Top=158,;
    Alignment=0, Width=149, Height=18,;
    Caption="Codice azienda"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="TVYESOBQJC",Visible=.t., Left=113, Top=208,;
    Alignment=0, Width=146, Height=18,;
    Caption="Categoria"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="SSZDJDADVZ",Visible=.t., Left=413, Top=7,;
    Alignment=0, Width=293, Height=18,;
    Caption="Contributi previdenziali a carico del soggetto erogante"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="ASOFVNFWIB",Visible=.t., Left=413, Top=56,;
    Alignment=0, Width=293, Height=18,;
    Caption="Contributi previdenziali a carico del percipiente"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="UPVOHSMYZN",Visible=.t., Left=413, Top=106,;
    Alignment=0, Width=152, Height=18,;
    Caption="Importi altri contributi"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="AYXQMHRWST",Visible=.t., Left=413, Top=158,;
    Alignment=0, Width=156, Height=18,;
    Caption="Contributi dovuti"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="MOFBUYHZZF",Visible=.t., Left=413, Top=208,;
    Alignment=0, Width=154, Height=18,;
    Caption="Contributi versati"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-5,top=28,;
    width=75+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-4,top=29,width=74+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oDPAU0029_2_2.Refresh()
      this.Parent.oDPAU0030_2_3.Refresh()
      this.Parent.oDPAU0031_2_4.Refresh()
      this.Parent.oDPAU0032_2_5.Refresh()
      this.Parent.oDPAU0033_2_6.Refresh()
      this.Parent.oDPAU0036_2_7.Refresh()
      this.Parent.oDPAU0034_2_8.Refresh()
      this.Parent.oDPAU0035_2_9.Refresh()
      this.Parent.oDPAU0037_2_10.Refresh()
      this.Parent.oDPAU0038_2_11.Refresh()
      this.Parent.oDPAU0039_2_12.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oDPAU0029_2_2 as StdTrsField with uid="VIAJHVQFUS",rtseq=3,rtrep=.t.,;
    cFormVar="w_DPAU0029",value=space(11),;
    ToolTipText = "Codice fiscale ente previdenziale",;
    HelpContextID = 91263377,;
    cTotal="", bFixedPos=.t., cQueryName = "DPAU0029",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=90, Left=111, Top=30, cSayPict=[REPLICATE("!",11)], cGetPict=[REPLICATE("!",11)], InputMask=replicate('X',11)

  func oDPAU0029_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (iif(Not Empty(.w_Dpau0029),Chkcfp(.w_Dpau0029,"PI", "", "", ""),.t.))
    endwith
    return bRes
  endfunc

  add object oDPAU0030_2_3 as StdTrsField with uid="RCXXOVJFUW",rtseq=4,rtrep=.t.,;
    cFormVar="w_DPAU0030",value=space(60),;
    ToolTipText = "Denominazione ente previdenziale",;
    HelpContextID = 91263386,;
    cTotal="", bFixedPos=.t., cQueryName = "DPAU0030",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=111, Top=79, InputMask=replicate('X',60)

  add object oDPAU0031_2_4 as StdTrsField with uid="YCPXMUSHQF",rtseq=5,rtrep=.t.,;
    cFormVar="w_DPAU0031",value=space(2),;
    ToolTipText = "Codice ente previdenziale",;
    HelpContextID = 91263385,;
    cTotal="", bFixedPos=.t., cQueryName = "DPAU0031",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=111, Top=129, InputMask=replicate('X',2)

  func oDPAU0031_2_4.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CH__ANNO>=2016)
    endwith
    endif
  endfunc

  add object oDPAU0032_2_5 as StdTrsField with uid="HWXNBNYPKT",rtseq=6,rtrep=.t.,;
    cFormVar="w_DPAU0032",value=space(40),;
    ToolTipText = "Codice azienda",;
    HelpContextID = 91263384,;
    cTotal="", bFixedPos=.t., cQueryName = "DPAU0032",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=111, Top=183, InputMask=replicate('X',40)

  add object oDPAU0033_2_6 as StdTrsField with uid="JANHJCOUFU",rtseq=7,rtrep=.t.,;
    cFormVar="w_DPAU0033",value=space(2),;
    ToolTipText = "Categoria",;
    HelpContextID = 91263383,;
    cTotal="", bFixedPos=.t., cQueryName = "DPAU0033",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=111, Top=230, InputMask=replicate('X',2)

  add object oDPAU0036_2_7 as StdTrsCheck with uid="QVQENQAZIR",rtrep=.t.,;
    cFormVar="w_DPAU0036",  caption="Altri contributi",;
    HelpContextID = 91263380,;
    Left=111, Top=269,;
    cTotal="", cQueryName = "DPAU0036",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
   , bGlobalFont=.t.


  func oDPAU0036_2_7.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..DPAU0036,&i_cF..t_DPAU0036),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oDPAU0036_2_7.GetRadio()
    this.Parent.oContained.w_DPAU0036 = this.RadioValue()
    return .t.
  endfunc

  func oDPAU0036_2_7.ToRadio()
    this.Parent.oContained.w_DPAU0036=trim(this.Parent.oContained.w_DPAU0036)
    return(;
      iif(this.Parent.oContained.w_DPAU0036=='S',1,;
      0))
  endfunc

  func oDPAU0036_2_7.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oDPAU0034_2_8 as StdTrsField with uid="YMPBGVADTP",rtseq=9,rtrep=.t.,;
    cFormVar="w_DPAU0034",value=0,;
    ToolTipText = "Contributi previdenziali a carico del soggetto erogante",;
    HelpContextID = 91263382,;
    cTotal="", bFixedPos=.t., cQueryName = "DPAU0034",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=413, Top=30, cSayPict=[v_PV(80)], cGetPict=[v_GV(80)]

  add object oDPAU0035_2_9 as StdTrsField with uid="CZXOKUDHQY",rtseq=10,rtrep=.t.,;
    cFormVar="w_DPAU0035",value=0,;
    ToolTipText = "Contributi previdenziali a carico del percipiente",;
    HelpContextID = 91263381,;
    cTotal="", bFixedPos=.t., cQueryName = "DPAU0035",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=413, Top=79, cSayPict=[v_PV(80)], cGetPict=[v_GV(80)]

  add object oDPAU0037_2_10 as StdTrsField with uid="RQKWGEFADY",rtseq=11,rtrep=.t.,;
    cFormVar="w_DPAU0037",value=0,;
    ToolTipText = "Importi altri contributi",;
    HelpContextID = 91263379,;
    cTotal="", bFixedPos=.t., cQueryName = "DPAU0037",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=413, Top=129, cSayPict=[v_PV(80)], cGetPict=[v_GV(80)]

  add object oDPAU0038_2_11 as StdTrsField with uid="GTMEBYDYYE",rtseq=12,rtrep=.t.,;
    cFormVar="w_DPAU0038",value=0,;
    ToolTipText = "Contributi dovuti",;
    HelpContextID = 91263378,;
    cTotal="", bFixedPos=.t., cQueryName = "DPAU0038",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=413, Top=183, cSayPict=[v_PV(80)], cGetPict=[v_GV(80)]

  add object oDPAU0039_2_12 as StdTrsField with uid="XBTQTDXTGR",rtseq=13,rtrep=.t.,;
    cFormVar="w_DPAU0039",value=0,;
    ToolTipText = "Contributi versati",;
    HelpContextID = 91263377,;
    cTotal="", bFixedPos=.t., cQueryName = "DPAU0039",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=413, Top=230, cSayPict=[v_PV(80)], cGetPict=[v_GV(80)]

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsri_mprBodyRow as CPBodyRowCnt
  Width=65
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oDPROGMOD_2_1 as StdTrsField with uid="QZOFFSDDJU",rtseq=2,rtrep=.t.,;
    cFormVar="w_DPROGMOD",value=0,isprimarykey=.t.,;
    HelpContextID = 117801350,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=62, Left=-2, Top=0, cSayPict=["99999999"], cGetPict=["99999999"]
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oDPROGMOD_2_1.When()
    return(.t.)
  proc oDPROGMOD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oDPROGMOD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsri_mpr','CUDDPTDH','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".DPSERIAL=CUDDPTDH.DPSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
