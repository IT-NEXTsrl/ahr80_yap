* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_kvi                                                        *
*              Visualizza distinte                                             *
*                                                                              *
*      Author: ZUCCHETTI TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [146] [VRS_61]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-01-05                                                      *
* Last revis.: 2008-09-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsri_kvi",oParentObject))

* --- Class definition
define class tgsri_kvi as StdForm
  Top    = -1
  Left   = 53

  * --- Standard Properties
  Width  = 546
  Height = 453
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-09-25"
  HelpContextID=219548055
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=10

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsri_kvi"
  cComment = "Visualizza distinte"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_MESE = space(2)
  w_ANNO = space(4)
  w_DECTOT = 0
  w_CALCPICT = 0
  w_SELEZI = space(1)
  w_ELABORA = .F.
  w_FLSELE = 0
  w_VALVER = space(3)
  w_MEMO = space(0)
  w_PARAMET = space(2)
  w_ZoomGri1 = .NULL.
  w_ZoomGrid = .NULL.
  w_ZoomGri2 = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsri_kviPag1","gsri_kvi",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSELEZI_1_8
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomGri1 = this.oPgFrm.Pages(1).oPag.ZoomGri1
    this.w_ZoomGrid = this.oPgFrm.Pages(1).oPag.ZoomGrid
    this.w_ZoomGri2 = this.oPgFrm.Pages(1).oPag.ZoomGri2
    DoDefault()
    proc Destroy()
      this.w_ZoomGri1 = .NULL.
      this.w_ZoomGrid = .NULL.
      this.w_ZoomGri2 = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_MESE=space(2)
      .w_ANNO=space(4)
      .w_DECTOT=0
      .w_CALCPICT=0
      .w_SELEZI=space(1)
      .w_ELABORA=.f.
      .w_FLSELE=0
      .w_VALVER=space(3)
      .w_MEMO=space(0)
      .w_PARAMET=space(2)
      .w_MESE=oParentObject.w_MESE
      .w_ANNO=oParentObject.w_ANNO
      .w_DECTOT=oParentObject.w_DECTOT
      .w_ELABORA=oParentObject.w_ELABORA
      .w_VALVER=oParentObject.w_VALVER
      .w_MEMO=oParentObject.w_MEMO
      .w_PARAMET=oParentObject.w_PARAMET
      .oPgFrm.Page1.oPag.oObj_1_1.Calculate()
      .oPgFrm.Page1.oPag.ZoomGri1.Calculate()
          .DoRTCalc(1,3,.f.)
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .w_SELEZI = 'S'
          .DoRTCalc(6,6,.f.)
        .w_FLSELE = 0
      .oPgFrm.Page1.oPag.oObj_1_13.Calculate()
      .oPgFrm.Page1.oPag.ZoomGrid.Calculate()
      .oPgFrm.Page1.oPag.ZoomGri2.Calculate()
    endwith
    this.DoRTCalc(8,10,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_MESE=.w_MESE
      .oParentObject.w_ANNO=.w_ANNO
      .oParentObject.w_DECTOT=.w_DECTOT
      .oParentObject.w_ELABORA=.w_ELABORA
      .oParentObject.w_VALVER=.w_VALVER
      .oParentObject.w_MEMO=.w_MEMO
      .oParentObject.w_PARAMET=.w_PARAMET
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_1.Calculate()
        .oPgFrm.Page1.oPag.ZoomGri1.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate()
        .oPgFrm.Page1.oPag.ZoomGrid.Calculate()
        .oPgFrm.Page1.oPag.ZoomGri2.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,10,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_1.Calculate()
        .oPgFrm.Page1.oPag.ZoomGri1.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate()
        .oPgFrm.Page1.oPag.ZoomGrid.Calculate()
        .oPgFrm.Page1.oPag.ZoomGri2.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oBtn_1_11.visible=!this.oPgFrm.Page1.oPag.oBtn_1_11.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_1.Event(cEvent)
      .oPgFrm.Page1.oPag.ZoomGri1.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_13.Event(cEvent)
      .oPgFrm.Page1.oPag.ZoomGrid.Event(cEvent)
      .oPgFrm.Page1.oPag.ZoomGri2.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oMESE_1_4.value==this.w_MESE)
      this.oPgFrm.Page1.oPag.oMESE_1_4.value=this.w_MESE
    endif
    if not(this.oPgFrm.Page1.oPag.oANNO_1_5.value==this.w_ANNO)
      this.oPgFrm.Page1.oPag.oANNO_1_5.value=this.w_ANNO
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEZI_1_8.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page1.oPag.oSELEZI_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMEMO_1_17.value==this.w_MEMO)
      this.oPgFrm.Page1.oPag.oMEMO_1_17.value=this.w_MEMO
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsri_kviPag1 as StdContainer
  Width  = 542
  height = 453
  stdWidth  = 542
  stdheight = 453
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oObj_1_1 as cp_runprogram with uid="MDTYEXXBSH",left=4, top=476, width=139,height=20,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSRI_BV1('V')",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 139325210


  add object ZoomGri1 as cp_szoombox with uid="WEHQMNBVVX",left=2, top=39, width=534,height=210,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable='VEN_RITE',cZoomFile='GSRI1KVI',bOptions=.f.,bQueryOnLoad=.f.,bReadOnly=.f.,bQueryOnDblClick=.F.,;
    cEvent = "Blank",;
    nPag=1;
    , HelpContextID = 139325210

  add object oMESE_1_4 as StdField with uid="RFUDMZELTX",rtseq=1,rtrep=.f.,;
    cFormVar = "w_MESE", cQueryName = "MESE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Mese di riferimento",;
    HelpContextID = 224428998,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=162, Top=10, InputMask=replicate('X',2)

  add object oANNO_1_5 as StdField with uid="DCZLQWTSFJ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_ANNO", cQueryName = "ANNO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Anno di riferimento",;
    HelpContextID = 225065990,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=188, Top=10, InputMask=replicate('X',4)

  add object oSELEZI_1_8 as StdRadio with uid="KFHBGVZBXM",rtseq=5,rtrep=.f.,left=6, top=250, width=158,height=32;
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZI_1_8.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutto"
      this.Buttons(1).HelpContextID = 201331750
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutto"
      this.Buttons(2).HelpContextID = 201331750
      this.Buttons(2).Top=15
      this.SetAll("Width",156)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELEZI_1_8.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oSELEZI_1_8.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_1_8.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=='S',1,;
      iif(this.Parent.oContained.w_SELEZI=='D',2,;
      0))
  endfunc


  add object oBtn_1_11 as StdButton with uid="OBXXKZYCCI",left=437, top=255, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 219576806;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_11.Click()
      with this.Parent.oContained
        GSRI_BV1(this.Parent.oContained,"A")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_11.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_ELABORA=.F.)
     endwith
    endif
  endfunc


  add object oBtn_1_12 as StdButton with uid="JTIYWAEYMF",left=488, top=255, width=48,height=45,;
    CpPicture="bmp\esc.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 226865478;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_12.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_13 as cp_runprogram with uid="MOCFDQILHK",left=267, top=475, width=111,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSRI_BV1('S')",;
    cEvent = "w_SELEZI Changed",;
    nPag=1;
    , HelpContextID = 139325210

  add object oMEMO_1_17 as StdMemo with uid="DMCAUCLWQA",rtseq=9,rtrep=.f.,;
    cFormVar = "w_MEMO", cQueryName = "MEMO",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Elenco delle distinte non importabili nel modello F24",;
    HelpContextID = 225059782,;
   bGlobalFont=.t.,;
    Height=125, Width=532, Left=2, Top=327, ReadOnly=.t.


  add object ZoomGrid as cp_szoombox with uid="LATFLJQGRN",left=2, top=39, width=534,height=210,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable='VEP_RITE',cZoomFile='GSRI_KVI',bOptions=.f.,bQueryOnLoad=.f.,bReadOnly=.f.,bQueryOnDblClick=.F.,;
    cEvent = "Blank",;
    nPag=1;
    , HelpContextID = 139325210


  add object ZoomGri2 as cp_szoombox with uid="SFQIAXRSBJ",left=2, top=39, width=534,height=210,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable='VEN_RITE',cZoomFile='GSRI2KVI',bOptions=.f.,bQueryOnLoad=.f.,bReadOnly=.f.,bQueryOnDblClick=.F.,;
    cEvent = "Blank",;
    nPag=1;
    , HelpContextID = 139325210

  add object oStr_1_3 as StdString with uid="CKZXVWVUBM",Visible=.t., Left=12, Top=11,;
    Alignment=1, Width=148, Height=15,;
    Caption="Periodo di riferimento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="LUOPTMCIAY",Visible=.t., Left=4, Top=306,;
    Alignment=0, Width=210, Height=15,;
    Caption="Messaggistica di errore"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_1_15 as StdBox with uid="AVCVKDEYFI",left=1, top=302, width=541,height=0
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsri_kvi','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
