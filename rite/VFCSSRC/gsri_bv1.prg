* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_bv1                                                        *
*              ZOOM VERSAMENTI PER F24                                         *
*                                                                              *
*      Author: ZUCCHETTI TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][98][VRS_175]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-01-08                                                      *
* Last revis.: 2007-12-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParame
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsri_bv1",oParentObject,m.pParame)
return(i_retval)

define class tgsri_bv1 as StdBatch
  * --- Local variables
  pParame = space(1)
  w_CURSORE = space(10)
  w_Zoom = .NULL.
  w_Zoom1 = .NULL.
  w_Zoom2 = .NULL.
  w_CODF24 = space(5)
  w_MESS = space(100)
  w_CONT = 0
  w_TRIBUTO = space(5)
  w_CAUCONTR = space(5)
  w_CAUPREV = space(5)
  w_IMPORTO = 0
  w_OK = .f.
  w_GENERA = space(10)
  w_GENINPS = space(10)
  w_GENPREV = space(10)
  w_SEDE = space(5)
  w_MESIRIF = space(2)
  w_ANNIRIF = space(4)
  w_MESFRIF = space(2)
  w_ANNFRIF = space(4)
  w_CODTRI1 = space(5)
  w_IMPORTO1 = 0
  w_CODTRI2 = space(5)
  w_IMPORTO2 = 0
  w_CODTRI3 = space(5)
  w_IMPORTO3 = 0
  w_CODTRI4 = space(5)
  w_IMPORTO4 = 0
  w_CODTRI5 = space(5)
  w_IMPORTO5 = 0
  w_CODTRI6 = space(5)
  w_IMPORTO6 = 0
  w_CODTRI7 = space(5)
  w_IMPORTO7 = 0
  w_CODTRI8 = space(5)
  w_IMPORTO8 = 0
  w_IMPORTO9 = 0
  w_CODTRI9 = space(5)
  w_SEDE1 = space(5)
  w_CAUCONT1 = space(5)
  w_IMPINPS1 = 0
  w_SEDE2 = space(5)
  w_CAUCONT2 = space(5)
  w_IMPINPS2 = 0
  w_SEDE3 = space(5)
  w_CAUCONT3 = space(5)
  w_IMPINPS3 = 0
  w_SEDE4 = space(5)
  w_CAUCONT4 = space(5)
  w_IMPINPS4 = 0
  w_CAUPREV1 = space(5)
  w_IMPOPRE1 = 0
  w_CAUPREV2 = space(5)
  w_IMPOPRE2 = 0
  w_DATAPRE = ctod("  /  /  ")
  w_PUNERA = .NULL.
  w_DECIM = 0
  * --- WorkFile variables
  VEP_RITE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch lanciato dalla maschera Gscg_Kvi all'apertura della stessa.
    * --- Parametro utilizzato per verificare se sono in aggiornamento(A) oppure in caricamento
    * --- dello zoom (V) oppure se utilizzato per selezionare o deselezionare tutto(S)
    * --- Serve per poter sapere se ho almeno una riga da inserire nello zoom
    * --- Assegno il nome del cursore
    * --- Serve per poter sapere se devo lanciare lo zoom dei versamenti I.R.PE.F. oppure I.N.P.S.
    * --- Variabile utilizzata per la messaggistica di errore.
    * --- Serve per poter sapere se ho superato il limite massimo di tributi presenti nel modello F24
    * --- Serve per sapere se ho trovato il codice tributo oppure devo effettuare un inserimento nuovo.
    * --- In questa variabile leggo il nome del cursore dichiarato nell'anagrafica Gscg_Amf
    * --- Mese di inizio riferimento
    * --- Anno di inizio riferimento.
    * --- Mese di fine riferimento
    * --- Anno di fine riferimento.
    * --- Variabili lette dall'anagrafica Gscg_Aef (Modello F24 Sezione Erario)
    * --- Variabili lette dall'anagrafica Gscg_Amf (Modello F24 Sezione I.N.P.S.)
    * --- Variabili lette dall'anagrafica Gscg_Amf (Modello F24 Sezione Altri Enti)
    * --- Puntatore anagrafica Erario
    this.w_IMPORTO = 0
    this.w_CONT = 0
    this.w_Zoom = this.oParentObject.w_ZoomGrid
    this.w_Zoom1 = this.oParentObject.w_ZoomGri1
    this.w_Zoom2 = this.oParentObject.w_ZoomGri2
    * --- Se figlio non instanziato lo instanzio
    if Upper( This.OparentObject.OparentObject.OparentObject.Gscg_avf.class)="STDDYNAMICCHILD"
      This.OparentObject.OparentObject.OparentObject.oPgFrm.Pages[7].opag.uienable(.T.)
    endif
    * --- Data di Presentazione del Modello
    this.w_DATAPRE = This.OparentObject.OparentObject.OparentObject.Gscg_avf.w_VFDTPRES
    do case
      case this.w_DATAPRE >= Cp_CharToDate("01-01-2002") and this.w_DATAPRE <= Cp_CharToDate("31-12-2002")
        * --- Se figlio non instanziato lo instanzio
        if Upper( This.OparentObject.OparentObject.OparentObject.Gscg_aff.class)="STDDYNAMICCHILD"
          This.OparentObject.OparentObject.OparentObject.oPgFrm.Pages[2].opag.uienable(.T.)
        endif
        this.w_PUNERA = This.OparentObject.OparentObject.OparentObject.Gscg_aff
        * --- Valuta Euro:gli impori sono arrotondati a due decimali
        this.w_DECIM = 2
      case this.w_DATAPRE >= Cp_CharToDate("01-01-2003") and this.w_DATAPRE < Cp_CharToDate("29-10-2007")
        * --- Se figlio non instanziato lo instanzio
        if Upper( This.OparentObject.OparentObject.OparentObject.Gscg_afq.class)="STDDYNAMICCHILD"
          This.OparentObject.OparentObject.OparentObject.oPgFrm.Pages[2].opag.uienable(.T.)
        endif
        this.w_PUNERA = This.OparentObject.OparentObject.OparentObject.Gscg_afq
        * --- Valuta Euro:gli impori sono arrotondati a due decimali
        this.w_DECIM = 2
      case this.w_DATAPRE >= Cp_CharToDate("29-10-2007")
        * --- Se figlio non instanziato lo instanzio
        if Upper( This.OparentObject.OparentObject.OparentObject.Gscg_afr.class)="STDDYNAMICCHILD"
          This.OparentObject.OparentObject.OparentObject.oPgFrm.Pages[2].opag.uienable(.T.)
        endif
        this.w_PUNERA = This.OparentObject.OparentObject.OparentObject.Gscg_afr
        * --- Valuta Euro:gli impori sono arrotondati a due decimali
        this.w_DECIM = 2
      otherwise
        * --- Se figlio non instanziato lo instanzio
        if Upper( This.OparentObject.OparentObject.OparentObject.Gscg_aef.class)="STDDYNAMICCHILD"
          This.OparentObject.OparentObject.OparentObject.oPgFrm.Pages[2].opag.uienable(.T.)
        endif
        this.w_PUNERA = This.OparentObject.OparentObject.OparentObject.Gscg_aef
        * --- Se la valuta � Euro gli impori sono arrotondati a zero decimali
        this.w_DECIM = 0
    endcase
    do case
      case this.oParentObject.w_Paramet="IR"
        * --- Rendo visibile lo zoom dei versamenti I.R.PE.F.
        this.w_Zoom.visible = .T.
        this.w_Zoom1.visible = .F.
        this.w_Zoom2.visible = .F.
      case this.oParentObject.w_Paramet="IN"
        * --- Rendo visibile lo zoom dei versamenti I.N.P.S.
        this.w_Zoom.visible = .F.
        this.w_Zoom1.visible = .T.
        this.w_Zoom2.visible = .F.
      case this.oParentObject.w_Paramet="PR"
        * --- Rendo visibile lo zoom dei versamenti previdenziali
        this.w_Zoom.visible = .F.
        this.w_Zoom1.visible = .F.
        this.w_Zoom2.visible = .T.
    endcase
    do case
      case this.pParame="V" And this.oParentObject.w_Elabora=.T.
        * --- Controllo se devo lanciare lo zoom dei versamenti I.R.PE.F. oppure I.N.P.S. oppure Previdenziale
        do case
          case this.oParentObject.w_Paramet="IR"
            * --- Leggo il nome del cursore dal batch Gscg_Bvi
            this.w_CURSORE = this.oparentObject.OparentObject.w_Pcursor
            * --- Cancello il contenuto dello Zoom
            Select (this.w_ZOOM.cCursor)
            Zap
            * --- Inserisco il risultato del cursore nello zoom
            Select (this.w_CURSORE)
            Go Top
            Scan
            Scatter Memvar
            Select (this.w_ZOOM.cCursor)
            Append blank
            Gather memvar
            Endscan
            select (this.w_Zoom.cCursor)
            go top
            Scan
            REPLACE XCHK with 1
            Endscan
            select (this.w_Zoom.cCursor)
            go top
            this.w_zoom.grd.refresh
          case this.oParentObject.w_Paramet="IN"
            this.w_CURSORE = this.oparentObject.OparentObject.w_Pcursor
            * --- Leggo il nome del cursore dal batch Gscg_Bvi
            * --- Cancello il contenuto dello Zoom
            Select (this.w_ZOOM1.cCursor)
            Zap
            * --- Inserisco il risultato del cursore nello zoom
            Select (this.w_CURSORE)
            Go Top
            Scan
            Scatter Memvar
            Select (this.w_ZOOM1.cCursor)
            Append blank
            Gather memvar
            Endscan
            Select (this.w_ZOOM1.cCursor)
            go top
            Scan
            REPLACE XCHK with 1
            Endscan
            Select (this.w_ZOOM1.cCursor)
            go top
            this.w_zoom1.grd.refresh
          case this.oParentObject.w_Paramet="PR"
            this.w_CURSORE = this.oparentObject.OparentObject.w_Pcursor
            * --- Cancello il contenuto dello Zoom
            Select (this.w_ZOOM2.cCursor)
            Zap
            * --- Inserisco il risultato del cursore nello zoom
            Select (this.w_CURSORE)
            Go Top
            Scan
            Scatter Memvar
            Select (this.w_ZOOM2.cCursor)
            Append blank
            Gather memvar
            Endscan
            select (this.w_Zoom2.cCursor)
            go top
            Scan
            REPLACE XCHK with 1
            Endscan
            select (this.w_Zoom2.cCursor)
            go top
            this.w_zoom2.grd.refresh
        endcase
        if used (this.w_Cursore)
          select (this.w_Cursore)
          use
        endif
      case this.pParame="S"
        do case
          case this.oParentObject.w_Paramet="IR"
            NC = this.w_Zoom.cCursor
          case this.oParentObject.w_Paramet="IN"
            NC = this.w_Zoom1.cCursor
          case this.oParentObject.w_Paramet="PR"
            NC = this.w_Zoom2.cCursor
        endcase
        if this.oParentObject.w_SELEZI="S"
          UPDATE &NC SET XCHK = 1
          this.oParentObject.w_FLSELE = 1
        else
          UPDATE &NC SET XCHK = 0
          this.oParentObject.w_FLSELE = 0
        endif
      case this.pParame="A"
        this.w_SEDE = this.OparentObject.OparentObject.w_Sede
        * --- Controllo il parametro impostato.Se I.R.PE.F. oppure I.N.P.S oppure Previdenziali
        do case
          case this.oParentObject.w_Paramet="IR"
            NC = this.w_Zoom.cCursor
          case this.oParentObject.w_Paramet="IN"
            NC = this.w_Zoom1.cCursor
          case this.oParentObject.w_Paramet="PR"
            NC = this.w_Zoom2.cCursor
        endcase
        * --- Prendo in considerazione solo i versamenti scelti nella maschera di selezione.
        Select * from (NC) into cursor GENERA where XCHK=1
        if RECCOUNT("GENERA")>0
          do case
            case this.oParentObject.w_Paramet="IR"
              this.Pag2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              * --- Assegno alla variabile Genera il nome del cursore definito sull'anagrafica.
              this.w_GENERA = this.oparentObject.OparentObject.Oparentobject.w_Genera
              Select * from Genera into cursor (this.w_Genera)
              * --- La variabile Importa serve per poter sapere se esiste almeno un record che sar�
              * --- scritto sull'anagrafica.
              This.OparentObject.OparentObject.OparentObject.w_Importa=.T.
            case this.oParentObject.w_Paramet="IN"
              this.Pag5()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              * --- Assegno alla variabile Geninps il nome del cursore definito sull'anagrafica.
              this.w_GENINPS = this.oparentObject.OparentObject.Oparentobject.w_Geninps
              * --- La variabile Impinps serve per poter sapere se esiste almeno un record che sar�
              * --- scritto sull'anagrafica.
              Select * from Genera into cursor (this.w_Geninps)
              This.OparentObject.OparentObject.OparentObject.w_Impinps=.T.
            case this.oParentObject.w_Paramet="PR"
              this.Pag6()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              * --- Assegno alla variabile Genprev il nome del cursore definito sull'anagrafica.
              this.w_GENPREV = this.oparentObject.OparentObject.Oparentobject.w_Genprev
              * --- La variabile Impinps serve per poter sapere se esiste almeno un record che sar�
              * --- scritto sull'anagrafica.
              Select * from Genera into cursor (this.w_Genprev)
              This.OparentObject.OparentObject.OparentObject.w_Impprev=.T.
          endcase
        endif
        ah_ErrorMsg("Elaborazione terminata","i","")
        this.w_PUNERA.mcalc(.T.)
        this.w_PUNERA.setcontrolsvalue()
        This.OparentObject.OparentObject.OparentObject.mcalc(.T.)
        This.OparentObject.OparentObject.OparentObject.setcontrolsvalue()
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiornamento (Modello F24 Sezione Erario)
    * --- Leggo i valori dall'anagrafica corrispondente al Modello F24 sezione Erario.
    this.w_CODTRI1 = this.w_PUNERA.w_EFTRIER1
    this.w_CODTRI2 = this.w_PUNERA.w_EFTRIER2
    this.w_CODTRI3 = this.w_PUNERA.w_EFTRIER3
    this.w_CODTRI4 = this.w_PUNERA.w_EFTRIER4
    this.w_CODTRI5 = this.w_PUNERA.w_EFTRIER5
    this.w_CODTRI6 = this.w_PUNERA.w_EFTRIER6
    this.w_IMPORTO1 = this.w_PUNERA.w_EFIMDER1
    this.w_IMPORTO2 = this.w_PUNERA.w_EFIMDER2
    this.w_IMPORTO3 = this.w_PUNERA.w_EFIMDER3
    this.w_IMPORTO4 = this.w_PUNERA.w_EFIMDER4
    this.w_IMPORTO5 = this.w_PUNERA.w_EFIMDER5
    this.w_IMPORTO6 = this.w_PUNERA.w_EFIMDER6
    if this.w_DATAPRE <= cp_CharToDate("31-12-2002")
      this.w_CODTRI7 = this.w_PUNERA.w_EFTRIER7
      this.w_CODTRI8 = this.w_PUNERA.w_EFTRIER8
      this.w_CODTRI9 = this.w_PUNERA.w_EFTRIER9
      this.w_IMPORTO7 = this.w_PUNERA.w_EFIMDER7
      this.w_IMPORTO8 = this.w_PUNERA.w_EFIMDER8
      this.w_IMPORTO9 = this.w_PUNERA.w_EFIMDER9
    else
      this.w_CODTRI7 = "w"
      this.w_CODTRI8 = "w"
      this.w_CODTRI9 = "w"
      this.w_IMPORTO7 = 0
      this.w_IMPORTO8 = 0
      this.w_IMPORTO9 = 0
    endif
    * --- Raggruppo il cursore per codice F24
    Select Trcotrif as Trcotrif,Sum(Importo) as Importo From Genera into cursor Aggiorna Group By Trcotrif
    * --- Ciclo il cursore Aggiorna per poter verificare se il tributo � gi� stato inserito nel
    * --- modello F24 oppure � un codice nuovo.
    * --- Devo verificare che il numero di tributi che inserisco non superi il limite
    * --- massimo di tributi inseribili nel modello F24
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Effettuo l'aggiornamento del modello F24 Sezione Erario.
    Select Aggiorna
    Go Top
    Scan
    this.w_TRIBUTO = Aggiorna.Trcotrif
    this.w_OK = .F.
    * --- Devo verificare se la valuta � lire oppure Euro.Effettuo questa operazione per dividere
    * --- nel caso di lire per 1000 riportando 0 decimali.Nel caso di Euro non riporto nessun decimale.
    if g_Codlir=this.oParentObject.w_Valver
      this.w_IMPORTO = cp_Round((Aggiorna.Importo/1000),0)
    else
      this.w_IMPORTO = cp_Round(Aggiorna.Importo,this.w_DECIM)
    endif
    * --- Verifico se il tributo inserito � presente in un record caricato manualmente.
    do case
      case this.w_Tributo=this.w_Codtri1
        this.w_IMPORTO = this.w_Importo+this.w_Importo1
        this.w_PUNERA.w_EFIMDER1=this.w_Importo
        this.w_OK = .T.
      case this.w_Tributo=this.w_Codtri2
        this.w_IMPORTO = this.w_Importo+this.w_Importo2
        this.w_PUNERA.w_EFIMDER2=this.w_Importo
        this.w_OK = .T.
      case this.w_Tributo=this.w_Codtri3
        this.w_IMPORTO = this.w_Importo+this.w_Importo3
        this.w_PUNERA.w_EFIMDER3=this.w_Importo
        this.w_OK = .T.
      case this.w_Tributo=this.w_Codtri4
        this.w_IMPORTO = this.w_Importo+this.w_Importo4
        this.w_PUNERA.w_EFIMDER4=this.w_Importo
        this.w_OK = .T.
      case this.w_Tributo=this.w_Codtri5
        this.w_IMPORTO = this.w_Importo+this.w_Importo5
        this.w_PUNERA.w_EFIMDER5=this.w_Importo
        this.w_OK = .T.
      case this.w_Tributo=this.w_Codtri6
        this.w_IMPORTO = this.w_Importo+this.w_Importo6
        this.w_PUNERA.w_EFIMDER6=this.w_Importo
        this.w_OK = .T.
      case this.w_Tributo=this.w_Codtri7
        this.w_IMPORTO = this.w_Importo+this.w_Importo7
        this.w_PUNERA.w_EFIMDER7=this.w_Importo
        this.w_OK = .T.
      case this.w_Tributo=this.w_Codtri8
        this.w_IMPORTO = this.w_Importo+this.w_Importo8
        this.w_PUNERA.w_EFIMDER8=this.w_Importo
        this.w_OK = .T.
      case this.w_Tributo=this.w_Codtri9
        this.w_IMPORTO = this.w_Importo+this.w_Importo9
        this.w_PUNERA.w_EFIMDER9=this.w_Importo
        this.w_OK = .T.
    endcase
    * --- Se la variabile OK vale .T. vuol dire che ho aggiornato una riga gi� esistente altrimenti devo
    * --- creare una nuova riga nella prima posizione libera.
    if this.w_OK=.F.
      * --- Cerco la prima posizione libera per effettuare un inserimento.
      do case
        case Empty(this.w_Codtri1) 
          this.w_PUNERA.w_EFTRIER1=this.w_Tributo
          this.w_PUNERA.w_EFIMDER1=this.w_Importo
          this.w_PUNERA.w_EFANNER1=this.oParentObject.w_Anno
          this.w_CODTRI1 = this.w_Tributo
        case Empty(this.w_Codtri2)
          this.w_PUNERA.w_EFTRIER2=this.w_Tributo
          this.w_PUNERA.w_EFIMDER2=this.w_Importo
          this.w_PUNERA.w_EFANNER2=this.oParentObject.w_Anno
          this.w_CODTRI2 = this.w_Tributo
        case Empty(this.w_Codtri3)
          this.w_PUNERA.w_EFTRIER3=this.w_Tributo
          this.w_PUNERA.w_EFIMDER3=this.w_Importo
          this.w_PUNERA.w_EFANNER3=this.oParentObject.w_Anno
          this.w_CODTRI3 = this.w_Tributo
        case Empty(this.w_Codtri4)
          this.w_PUNERA.w_EFTRIER4=this.w_Tributo
          this.w_PUNERA.w_EFIMDER4=this.w_Importo
          this.w_PUNERA.w_EFANNER4=this.oParentObject.w_Anno
          this.w_CODTRI4 = this.w_Tributo
        case Empty(this.w_Codtri5)
          this.w_PUNERA.w_EFTRIER5=this.w_Tributo
          this.w_PUNERA.w_EFIMDER5=this.w_Importo
          this.w_PUNERA.w_EFANNER5=this.oParentObject.w_Anno
          this.w_CODTRI5 = this.w_Tributo
        case Empty(this.w_Codtri6)
          this.w_PUNERA.w_EFTRIER6=this.w_Tributo
          this.w_PUNERA.w_EFIMDER6=this.w_Importo
          this.w_PUNERA.w_EFANNER6=this.oParentObject.w_Anno
          this.w_CODTRI6 = this.w_Tributo
        case Empty(this.w_Codtri7)
          this.w_PUNERA.w_EFTRIER7=this.w_Tributo
          this.w_PUNERA.w_EFIMDER7=this.w_Importo
          this.w_PUNERA.w_EFANNER7=this.oParentObject.w_Anno
          this.w_CODTRI7 = this.w_Tributo
        case Empty(this.w_Codtri8)
          this.w_PUNERA.w_EFTRIER8=this.w_Tributo
          this.w_PUNERA.w_EFIMDER8=this.w_Importo
          this.w_PUNERA.w_EFANNER8=this.oParentObject.w_Anno
          this.w_CODTRI8 = this.w_Tributo
        case Empty(this.w_Codtri9)
          this.w_PUNERA.w_EFTRIER9=this.w_Tributo
          this.w_PUNERA.w_EFIMDER9=this.w_Importo
          this.w_PUNERA.w_EFANNER9=this.oParentObject.w_Anno
          this.w_CODTRI9 = this.w_Tributo
      endcase
    endif
    Endscan
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Devo verificare di non inserire pi� tributi di quelli possibili. Se supero il numero
    * --- massimo consentito devo bloccare l'inserimento ed inserire una messaggistica di errore.
    this.w_CONT = 0
    do case
      case this.oParentObject.w_Paramet="IR"
        * --- Con il seguente controllo verifico quanti sono i codici tributo occupati.
        if Not Empty(this.w_Codtri1)
          this.w_CONT = this.w_CONT+1
        endif
        if Not Empty(this.w_Codtri2)
          this.w_CONT = this.w_CONT+1
        endif
        if Not Empty(this.w_Codtri3)
          this.w_CONT = this.w_CONT+1
        endif
        if Not Empty(this.w_Codtri4)
          this.w_CONT = this.w_CONT+1
        endif
        if Not Empty(this.w_Codtri5)
          this.w_CONT = this.w_CONT+1
        endif
        if Not Empty(this.w_Codtri6)
          this.w_CONT = this.w_CONT+1
        endif
        if Not Empty(this.w_Codtri7) and this.w_Codtri7<>"w"
          this.w_CONT = this.w_CONT+1
        endif
        if Not Empty(this.w_Codtri8) and this.w_Codtri8<>"w"
          this.w_CONT = this.w_CONT+1
        endif
        if Not Empty(this.w_Codtri9) and this.w_Codtri9<>"w"
          this.w_CONT = this.w_CONT+1
        endif
        * --- Ciclo il cursore Aggiorna per vedere se i codici letti dai versamenti Irpef siano
        * --- gi� presenti nel modello F24, oppure siano da inserire in questo caso devo
        * --- verificare di non superare il limite massimo.
        Select Aggiorna
        Go Top
        Scan
        this.w_TRIBUTO = Aggiorna.Trcotrif
        this.w_OK = .F.
        do case
          case this.w_Tributo=this.w_Codtri1
            this.w_OK = .T.
          case this.w_Tributo=this.w_Codtri2
            this.w_OK = .T.
          case this.w_Tributo=this.w_Codtri3
            this.w_OK = .T.
          case this.w_Tributo=this.w_Codtri4
            this.w_OK = .T.
          case this.w_Tributo=this.w_Codtri5
            this.w_OK = .T.
          case this.w_Tributo=this.w_Codtri6
            this.w_OK = .T.
          case this.w_Tributo=this.w_Codtri7
            this.w_OK = .T.
          case this.w_Tributo=this.w_Codtri8
            this.w_OK = .T.
          case this.w_Tributo=this.w_Codtri9
            this.w_OK = .T.
        endcase
        * --- Controllo se non ho trovato il codice tributo.
        if this.w_OK=.F.
          this.w_CONT = this.w_CONT+1
        endif
        Endscan
        * --- Limite massimo di codici tributo da inserire 9
        if this.w_CONT>6
          this.w_MESS = "Attenzione: occorre compilare pi� modelli%0Impostare il codice tributo manualmente"
          ah_ErrorMsg(this.w_Mess,"!","")
          this.Pag4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      case this.oParentObject.w_Paramet="IN"
        * --- Con il seguente controllo verifico quanti sono le sedi valorizzate.
        if Not Empty(this.w_Sede1)
          this.w_CONT = this.w_CONT+1
        endif
        if Not Empty(this.w_Sede2)
          this.w_CONT = this.w_CONT+1
        endif
        if Not Empty(this.w_Sede3)
          this.w_CONT = this.w_CONT+1
        endif
        if Not Empty(this.w_Sede4)
          this.w_CONT = this.w_CONT+1
        endif
        * --- Ciclo il cursore Aggiorna per vedere se i codici letti dai versamenti Irpef siano
        * --- gi� presenti nel modello F24, oppure siano da inserire in questo caso devo
        * --- verificare di non superare il limite massimo.
        Select Aggiorna
        Go Top
        Scan
        this.w_CAUCONTR = Aggiorna.Trcaucon
        this.w_SEDE = Aggiorna.Sede
        this.w_OK = .F.
        do case
          case this.w_Caucontr=this.w_Caucont1 and this.w_Sede=this.w_Sede1
            this.w_OK = .T.
          case this.w_Caucontr=this.w_Caucont2 and this.w_Sede=this.w_Sede2
            this.w_OK = .T.
          case this.w_Caucontr=this.w_Caucont3 and this.w_Sede=this.w_Sede3
            this.w_OK = .T.
          case this.w_Caucontr=this.w_Caucont4 and this.w_Sede=this.w_Sede4
            this.w_OK = .T.
        endcase
        * --- Controllo se non ho trovato un riga con stesso codice sede e stessa causale contributo.
        if this.w_OK=.F.
          this.w_CONT = this.w_CONT+1
        endif
        Endscan
        * --- Limite massimo di codici sedi da inserire 4
        if this.w_CONT>4
          this.w_MESS = "Attenzione: occorre compilare pi� modelli%0Impostare il codice sede manualmente"
          ah_ErrorMsg(this.w_Mess,"!","")
          this.Pag4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      case this.oParentObject.w_Paramet="PR"
        * --- Con il seguente controllo verifico quanti sono le causali contributo occupate.
        if Not Empty(this.w_Cauprev1)
          this.w_CONT = this.w_CONT+1
        endif
        if Not Empty(this.w_Cauprev2)
          this.w_CONT = this.w_CONT+1
        endif
        * --- Ciclo il cursore Aggiorna per vedere se i codici letti dai versamenti Previdenziali siano
        * --- gi� presenti nel modello F24, oppure siano da inserire in questo caso devo
        * --- verificare di non superare il limite massimo.
        Select Aggiorna
        Go Top
        Scan
        this.w_CAUPREV = Aggiorna.Trcaupre
        this.w_OK = .F.
        do case
          case this.w_Cauprev=this.w_Cauprev1
            this.w_OK = .T.
          case this.w_Cauprev=this.w_Cauprev2
            this.w_OK = .T.
        endcase
        * --- Controllo se non ho trovato il codice tributo.
        if this.w_OK=.F.
          this.w_CONT = this.w_CONT+1
        endif
        Endscan
        * --- Limite massimo causali contributo da inserire 2 (F24 2003)
        if this.w_CONT>2
          this.w_MESS = "Attenzione: occorre compilare pi� modelli%0Impostare la causale contributo manualmente"
          ah_ErrorMsg(this.w_Mess,"!","")
          this.Pag4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
    endcase
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if used ("ERRORE")
      select ERRORE
      use
    endif
    if used ("GENERA")
      select GENERA
      use
    endif
    if used ("AGGIORNA")
      select AGGIORNA
      use
    endif
    if used ("INTERME")
      select INTERME
      use
    endif
    This.OparentObject.EcpSave()
    i_retcode = 'stop'
    return
  endproc


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiornamento (Modello F24 Sezione I.N.P.S.)
    * --- Leggo i valori dall'anagrafica corrispondente al Modello F24 sezione I.N.P.S.
    this.w_SEDE1 = This.OparentObject.OparentObject.OparentObject.w_MFCDSED1
    this.w_SEDE2 = This.OparentObject.OparentObject.OparentObject.w_MFCDSED2
    this.w_SEDE3 = This.OparentObject.OparentObject.OparentObject.w_MFCDSED3
    this.w_SEDE4 = This.OparentObject.OparentObject.OparentObject.w_MFCDSED4
    this.w_CAUCONT1 = This.OparentObject.OparentObject.OparentObject.w_MFCCONT1
    this.w_CAUCONT2 = This.OparentObject.OparentObject.OparentObject.w_MFCCONT2
    this.w_CAUCONT3 = This.OparentObject.OparentObject.OparentObject.w_MFCCONT3
    this.w_CAUCONT4 = This.OparentObject.OparentObject.OparentObject.w_MFCCONT4
    this.w_IMPINPS1 = This.OparentObject.OparentObject.OparentObject.w_MFIMPSD1
    this.w_IMPINPS2 = This.OparentObject.OparentObject.OparentObject.w_MFIMPSD2
    this.w_IMPINPS3 = This.OparentObject.OparentObject.OparentObject.w_MFIMPSD3
    this.w_IMPINPS4 = This.OparentObject.OparentObject.OparentObject.w_MFIMPSD4
    * --- Inserisco nel cursore Genera il valore della sede letta dalla tabella ritenute.
    Select *,this.w_Sede as Sede from Genera into cursor Interme
    * --- Raggruppo il cursore per causale contributo e sede
    Select Trcaucon as Trcaucon,Sede as Sede,Sum(Importo) as Importo From Interme;
    into cursor Aggiorna Group By Sede,Trcaucon
    * --- Ciclo il cursore Aggiorna per poter verificare se il esiste gi� un record
    * --- nel modello F24 con stessa codice sede e stessa causale.
    * --- Devo verificare che il numero di tributi che inserisco non superi il limite
    * --- massimo di tributi inseribili nel modello F24
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Effettuo l'aggiornamento del modello F24 Sezione I.N.P.S.
    Select Aggiorna
    Go Top
    Scan
    this.w_CAUCONTR = Aggiorna.Trcaucon
    this.w_SEDE = Aggiorna.Sede
    this.w_OK = .F.
    if g_Codlir=this.oParentObject.w_Valver
      this.w_IMPORTO = cp_Round((Aggiorna.Importo/1000),0)
    else
      this.w_IMPORTO = cp_Round(Aggiorna.Importo,this.w_DECIM)
    endif
    * --- Se esiste una riga con stessa sede e stessa causale contributo aggiorno l'importo.
    do case
      case this.w_Caucontr=this.w_Caucont1 and this.w_Sede=this.w_Sede1
        this.w_IMPORTO = this.w_Importo+this.w_Impinps1
        This.OparentObject.OparentObject.OparentObject.w_MFIMPSD1=this.w_Importo
        this.w_OK = .T.
      case this.w_Caucontr=this.w_Caucont2 and this.w_Sede=this.w_Sede2
        this.w_IMPORTO = this.w_Importo+this.w_Impinps2
        This.OparentObject.OparentObject.OparentObject.w_MFIMPSD2=this.w_Importo
        this.w_OK = .T.
      case this.w_Caucontr=this.w_Caucont3 and this.w_Sede=this.w_Sede3
        this.w_IMPORTO = this.w_Importo+this.w_Impinps3
        This.OparentObject.OparentObject.OparentObject.w_MFIMPSD3=this.w_Importo
        this.w_OK = .T.
      case this.w_Caucontr=this.w_Caucont4 and this.w_Sede=this.w_Sede4
        this.w_IMPORTO = this.w_Importo+this.w_Impinps4
        This.OparentObject.OparentObject.OparentObject.w_MFIMPSD4=this.w_Importo
        this.w_OK = .T.
    endcase
    * --- Se la variabile OK vale .T. vuol dire che ho aggiornato una riga gi� esistente altrimenti devo
    * --- creare una nuova riga nella prima posizione libera.
    if this.w_OK=.F.
      * --- Per il perido di inizio e fine riferimento inserisco il perido impostato sull'anagrafica.
      this.w_MESIRIF = this.oParentObject.w_Mese
      this.w_ANNIRIF = this.oParentObject.w_Anno
      this.w_MESFRIF = this.oParentObject.w_Mese
      this.w_ANNFRIF = this.oParentObject.w_Anno
      * --- Per determinati tipi di causale contributo devo impostare come inizio e fine riferimento 0
      * --- Data di inizio riferimento
      if alltrim(this.w_Caucontr)$ "ABR-ABS-AC-ACON-DOM2-DSOS-EMI-FRI-LAZ-BAS-BLAA-BLAS-BOL-BPCF-CAL-CAL-CAM-CBS-CC-COC-COS-COSI-DCON-DLAA-DLAS-DMC"
        this.w_MESIRIF = "0"
        this.w_ANNIRIF = "0"
        this.w_MESFRIF = "0"
        this.w_ANNFRIF = "0"
      endif
      if alltrim(this.w_Caucontr)$ "LIG-LOM-MAR-MOL-PCAS-PIE-POC-POS-POSI-PUG-SAR-SIC-TOS-TRE-UMB-VAL-VEN-VMCF-VMCS"
        this.w_MESIRIF = "0"
        this.w_ANNIRIF = "0"
        this.w_MESFRIF = "0"
        this.w_ANNFRIF = "0"
      endif
      * --- Data di fine riferimento
      if alltrim(this.w_Caucontr)$ "AGRU-C10-CXX-DM10-DPC-EBCM-EBTU-EMCO-EMDM-EMLA-FIPP-LAA-LAAP-PCF-PCFP-PCFS-PESC"
        this.w_MESFRIF = "0"
        this.w_ANNFRIF = "0"
      endif
      do case
        case Empty(this.w_Caucont1) and Empty(this.w_Sede1)
          This.OparentObject.OparentObject.OparentObject.w_Mfcdsed1=this.w_Sede
          This.OparentObject.OparentObject.OparentObject.w_Mfccont1=this.w_Caucontr
          This.OparentObject.OparentObject.OparentObject.w_Mfdames1=this.w_Mesirif
          This.OparentObject.OparentObject.OparentObject.w_Mfdaann1=this.w_Annirif
          This.OparentObject.OparentObject.OparentObject.w_Mf_ames1=this.w_Mesfrif
          This.OparentObject.OparentObject.OparentObject.w_Mfan1=this.w_Annfrif
          This.OparentObject.OparentObject.OparentObject.w_Mfimpsd1=this.w_Importo
          this.w_SEDE1 = this.w_Sede
          this.w_CAUCONT1 = this.w_Caucontr
        case Empty(this.w_Caucont2) and Empty(this.w_Sede2)
          This.OparentObject.OparentObject.OparentObject.w_Mfcdsed2=this.w_Sede
          This.OparentObject.OparentObject.OparentObject.w_Mfccont2=this.w_Caucontr
          This.OparentObject.OparentObject.OparentObject.w_Mfdames2=this.w_Mesirif
          This.OparentObject.OparentObject.OparentObject.w_Mfdaann2=this.w_Annirif
          This.OparentObject.OparentObject.OparentObject.w_Mf_ames2=this.w_Mesfrif
          This.OparentObject.OparentObject.OparentObject.w_Mfan2=this.w_Annfrif
          This.OparentObject.OparentObject.OparentObject.w_Mfimpsd2=this.w_Importo
          this.w_SEDE2 = this.w_Sede
          this.w_CAUCONT2 = this.w_Caucontr
        case Empty(this.w_Caucont3) and Empty(this.w_Sede3)
          This.OparentObject.OparentObject.OparentObject.w_Mfcdsed3=this.w_Sede
          This.OparentObject.OparentObject.OparentObject.w_Mfccont3=this.w_Caucontr
          This.OparentObject.OparentObject.OparentObject.w_Mfdames3=this.w_Mesirif
          This.OparentObject.OparentObject.OparentObject.w_Mfdaann3=this.w_Annirif
          This.OparentObject.OparentObject.OparentObject.w_Mf_ames3=this.w_Mesfrif
          This.OparentObject.OparentObject.OparentObject.w_Mfan3=this.w_Annfrif
          This.OparentObject.OparentObject.OparentObject.w_Mfimpsd3=this.w_Importo
          this.w_SEDE3 = this.w_Sede
          this.w_CAUCONT3 = this.w_Caucontr
        case Empty(this.w_Caucont4) and Empty(this.w_Sede4)
          This.OparentObject.OparentObject.OparentObject.w_Mfcdsed4=this.w_Sede
          This.OparentObject.OparentObject.OparentObject.w_Mfccont4=this.w_Caucontr
          This.OparentObject.OparentObject.OparentObject.w_Mfdames4=this.w_Mesirif
          This.OparentObject.OparentObject.OparentObject.w_Mfdaann4=this.w_Annirif
          This.OparentObject.OparentObject.OparentObject.w_Mf_ames4=this.w_Mesfrif
          This.OparentObject.OparentObject.OparentObject.w_Mfan4=this.w_Annfrif
          This.OparentObject.OparentObject.OparentObject.w_Mfimpsd4=this.w_Importo
          this.w_SEDE4 = this.w_Sede
          this.w_CAUCONT4 = this.w_Caucontr
      endcase
    endif
    Endscan
  endproc


  procedure Pag6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiornamento (Modello F24 Sezione Altri Enti)
    * --- Leggo i valori dall'anagrafica corrispondente al Modello F24 sezione altri Enti.
    this.w_CAUPREV1 = This.OparentObject.OparentObject.OparentObject.w_MFCCOAE1
    this.w_CAUPREV2 = This.OparentObject.OparentObject.OparentObject.w_MFCCOAE2
    this.w_IMPOPRE1 = This.OparentObject.OparentObject.OparentObject.w_MFIMDAE1
    this.w_IMPOPRE2 = This.OparentObject.OparentObject.OparentObject.w_MFIMDAE2
    * --- Raggruppo il cursore per causale contributo
    Select Trcaupre as Trcaupre,Sum(Importo) as Importo From Genera;
    into cursor Aggiorna Group By Trcaupre
    * --- Ciclo il cursore Aggiorna per poter verificare se il esiste gi� un record
    * --- nel modello F24 con stessa causale.
    * --- Devo verificare che il numero di tributi che inserisco non superi il limite
    * --- massimo di tributi inseribili nel modello F24
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Effettuo l'aggiornamento del modello F24 Sezione Altri Enti
    Select Aggiorna
    Go Top
    Scan
    this.w_CAUPREV = Aggiorna.Trcaupre
    this.w_OK = .F.
    if g_Codlir=this.oParentObject.w_Valver
      this.w_IMPORTO = cp_Round((Aggiorna.Importo/1000),0)
    else
      this.w_IMPORTO = cp_Round(Aggiorna.Importo,this.w_DECIM)
    endif
    * --- Se esiste una riga con stessa causale contributo aggiorno l'importo.
    do case
      case this.w_Cauprev=this.w_Cauprev1
        this.w_IMPORTO = this.w_Importo+this.w_Impopre1
        This.OparentObject.OparentObject.OparentObject.w_MFIMDAE1=this.w_Importo
        this.w_OK = .T.
      case this.w_Cauprev=this.w_Cauprev2
        this.w_IMPORTO = this.w_Importo+this.w_Impopre2
        This.OparentObject.OparentObject.OparentObject.w_MFIMDAE2=this.w_Importo
        this.w_OK = .T.
    endcase
    * --- Se la variabile OK vale .T. vuol dire che ho aggiornato una riga gi� esistente altrimenti devo
    * --- creare una nuova riga nella prima posizione libera.
    if this.w_OK=.F.
      * --- Per il perido di inizio e fine riferimento inserisco il perido impostato sull'anagrafica.
      this.w_MESIRIF = this.oParentObject.w_Mese
      this.w_ANNIRIF = this.oParentObject.w_Anno
      this.w_MESFRIF = this.oParentObject.w_Mese
      this.w_ANNFRIF = this.oParentObject.w_Anno
      * --- Per determinati tipi di causale contributo devo impostare come inizio e fine riferimento 0
      * --- Data di inizio riferimento
      if alltrim(this.w_Cauprev)$ "RS-RC-PR-"
        this.w_MESIRIF = "0"
        this.w_ANNIRIF = "0"
        this.w_MESFRIF = "0"
        this.w_ANNFRIF = "0"
      endif
      * --- Data di fine riferimento
      if alltrim(this.w_Cauprev)$ "CCSP-CCLS"
        this.w_MESFRIF = "0"
        this.w_ANNFRIF = "0"
      endif
      do case
        case Empty(this.w_Cauprev1) 
          This.OparentObject.OparentObject.OparentObject.w_Mfccoae1=this.w_Cauprev
          This.OparentObject.OparentObject.OparentObject.w_Mfmsine1=right(("00"+alltrim(this.w_Mesirif)),2)
          This.OparentObject.OparentObject.OparentObject.w_Mfanine1=this.w_Annirif
          This.OparentObject.OparentObject.OparentObject.w_Mfmsfie1=right(("00"+alltrim(this.w_Mesfrif)),2)
          This.OparentObject.OparentObject.OparentObject.w_Mfanf1=this.w_Annfrif
          This.OparentObject.OparentObject.OparentObject.w_Mfimdae1=this.w_Importo
          this.w_CAUPREV1 = this.w_Cauprev
        case Empty(this.w_Cauprev2)
          This.OparentObject.OparentObject.OparentObject.w_Mfccoae2=this.w_Cauprev
          This.OparentObject.OparentObject.OparentObject.w_Mfmsine2=right(("00"+alltrim(this.w_Mesirif)),2)
          This.OparentObject.OparentObject.OparentObject.w_Mfanine2=this.w_Annirif
          This.OparentObject.OparentObject.OparentObject.w_Mfmsfie2=right(("00"+alltrim(this.w_Mesfrif)),2)
          This.OparentObject.OparentObject.OparentObject.w_Mfanf2=this.w_Annfrif
          This.OparentObject.OparentObject.OparentObject.w_Mfimdae2=this.w_Importo
          this.w_CAUPREV2 = this.w_Cauprev
      endcase
    endif
    Endscan
  endproc


  proc Init(oParentObject,pParame)
    this.pParame=pParame
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='VEP_RITE'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParame"
endproc
