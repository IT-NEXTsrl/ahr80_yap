* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_bcu                                                        *
*              Generazione comunicazione unica                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2015-01-21                                                      *
* Last revis.: 2016-02-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParame
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsri_bcu",oParentObject,m.pParame)
return(i_retval)

define class tgsri_bcu as StdBatch
  * --- Local variables
  pParame = space(1)
  w_CFAzi = space(16)
  w_Separa = space(2)
  w_RecordA = space(0)
  w_RecordB = space(0)
  w_RecordD = space(0)
  w_RecordH = space(0)
  w_RecordZ = space(0)
  w_Nomfil = space(254)
  w_Mrcodcon = space(15)
  w_Percipcf = space(16)
  w_ProgrCert = 0
  w_Anflsgre = space(1)
  w_IdenRigoAppo = space(8)
  w_NomeCampoTr = space(16)
  w_NomeCampo = space(100)
  w_Cfperest = space(16)
  w_Gsri_Mcu = .NULL.
  w_TipoRecord = space(1)
  w_Modello = space(20)
  w_Resfdf = 0
  w_Quadro = 0
  w_Sogest = space(1)
  w_Codcon_Appoggio = space(15)
  w_Caupre_Appoggio = space(1)
  w_ProgrModulo = 0
  w_ContaRecordH = 0
  w_ScritturaRecord = space(0)
  w_LunghezzaRecord = 0
  w_Cdprocer = 0
  w_StampaFront = space(1)
  w_StampaDettaglio = space(1)
  w_TipoZoom = space(1)
  w_Firma_Dichiarazione = space(250)
  w_Firma_Intermediario = space(250)
  w_Firma_Imposta = space(250)
  w_AnnoCer = space(4)
  w_ZOOM = .NULL.
  w_NFLTATTR = 0
  w_Zoom = .NULL.
  w_Conferma = .f.
  w_Cdprotec = space(17)
  w_Cdprodoc = 0
  w_Posi = 0
  w_Conferma = .f.
  w_Cdcodfis = space(16)
  w_Cdprotec = space(17)
  w_Cdprodoc = 0
  w_Cdperfis = space(1)
  w_Cdcognom = space(60)
  w_Cd__Nome = space(50)
  w_Cd_Sesso = space(1)
  w_Cddatnas = ctod("  /  /  ")
  w_Cdcomnas = space(30)
  w_Cdpronas = space(2)
  w_Cdcatpar = space(2)
  w_Cdeveecc = space(1)
  w_Cdcomfis = space(30)
  w_Cdprofis = space(2)
  w_Cdcomune = space(4)
  w_Cdcaupre = space(2)
  w_Cdammcor = 0
  w_Cdsnsogg = 0
  w_Cdcodice = space(1)
  w_Cdimponi = 0
  w_Cdritacc = 0
  w_Cdsogero = 0
  w_Cdpercip = 0
  w_Cdsperim = 0
  w_Cdritrim = 0
  w_Tipcas = space(1)
  w_Cdpromod = 0
  w_Cdpromod1 = 0
  w_Cdpromod2 = 0
  w_CdCodice1 = space(1)
  w_CdCodice2 = space(1)
  w_CdSnsogg1 = 0
  w_CdSnsogg2 = 0
  w_Cdfperci = space(15)
  w_Attmod1 = space(1)
  w_Attmod2 = space(1)
  w_ZOOM = .NULL.
  w_ZOOM1 = .NULL.
  w_Cudatcer = ctod("  /  /  ")
  w_CodiceFis = space(16)
  w_Tipo_Comu = space(1)
  w_Seriale_Cert = space(10)
  w_Conferma = .f.
  w_Cutipcer = space(5)
  w_NomeFile = space(254)
  w_Percin = space(15)
  w_Percfin = space(15)
  w_Tipoperaz = space(1)
  w_Datafall = ctod("  /  /  ")
  w_Datini = ctod("  /  /  ")
  w_Datfin = ctod("  /  /  ")
  w_Codval = space(3)
  w_Obtest = ctod("  /  /  ")
  w_Block = .f.
  w_Attention = .f.
  w_Mrcodcon = space(15)
  w_Andescri = space(40)
  w_Perceste = space(1)
  w_Mess = space(254)
  w_Procer = 0
  w_NumeroRecord = 0
  * --- WorkFile variables
  runtime_filters = 2

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Generazione comunicazione unica
    * --- Codice Fiscale dell'Azienda Corrente
    this.w_Separa = chr(13)+chr(10)
    this.w_LunghezzaRecord = 1000000
    do case
      case this.pParame="A"
        this.w_CfAzi = left(alltrim(this.oParentObject.w_Cucodfis)+space(16),16)
        this.w_Nomfil = this.oParentObject.w_Cunomfil
        this.w_Gsri_Mcu = This.oParentObject.Gsri_Mcu
        * --- Controlla i dati della maschera
        this.oParentObject.bUpdated=.t.
        if not this.oParentObject.checkform()
          i_retcode = 'stop'
          return
        endif
        * --- Genera file
        if file(this.oParentObject.w_CUNOMFIL) 
          * --- Il controllo se il file � esistente viene fatto solo nel caso che non
          *     sia stato attivato il chek "Solo soggetti esteri"
          if this.oParentObject.w_Cusogest<>"S"
            if NOT ah_YesNo("Il file esiste gi�. Si vuole sovrascrivere?")
              i_retcode = 'stop'
              return
            else
              Strtofile("",this.w_NomFil)
            endif
          endif
        endif
        * --- Verifica codice carica non ammesso per la generazione del file telematico
        if this.oParentObject.w_Curapfir="S" And this.oParentObject.w_Cucodcar="10"
          ah_ErrorMsg("Il codice carica '10' per il rappresentante firmatario non � ammesso nella generazione del file telematico")
          i_retcode = 'stop'
          return
        endif
        if this.oParentObject.w_Cufirint<>"0" or Not Empty(this.oParentObject.w_Cudatimp) or Not Empty(this.oParentObject.w_Cucodint)
          if Empty(this.oParentObject.w_Cudatimp) Or Empty(this.oParentObject.w_Cucodint) or this.oParentObject.w_Cufirint="0"
            ah_ErrorMsg("Mancano i dati relativi all'impegno della trasmissione")
            i_retcode = 'stop'
            return
          endif
        endif
        * --- Nel caso sia attivo il flag di certificazione singola i dati devono essere 
        *     letti dal cursore ElencoCertificazioni in caso di caricamento mentre in caso
        *     di variazione vengono letti direttamente dalla tabella Gendcomu
        if this.oParentObject.w_Cucersin="S"
          * --- Variabile utilizzata per valorizzare l'anno nel record H
          this.w_Datfin = this.oParentObject.w_Cudatfin
          if .cFunction="Edit"
            Vq_Exec("..\Rite\Exe\Query\Gsri4acu", this, "Rite1")
            if Reccount("Rite1")=0
              ah_ErrorMsg("Non esistono certificazioni da presentare")
              Use In Rite1
              i_retcode = 'stop'
              return
            endif
          else
            if Used("ElencoCertificazioni")
              Curtotab("ElencoCertificazioni","ElePerc")
              Use In ElencoCertificazioni
              * --- Passo il risultato del cursore ElencoPercipienti al cursore temporaneo
              Vq_Exec("..\Rite\Exe\Query\Gsri2acu", this, "Rite1")
              * --- Nel caso di certificazione singola oppure sostitutiva devo verificare se
              *     esistono eventuali record aggiuntivi
              if this.oParentObject.w_Cutipcas $ "SO"
                Vq_Exec("..\Rite\Exe\Query\Gsri5acu", this, "Rite_Sns")
                if Reccount("Rite_Sns")>0
                  Select * from Rite1 ; 
 union all; 
 Select * from Rite_Sns; 
 order By Mrcodcon ,Trcaupre,Ordina,Mrcodsns into cursor Rite
                  Use In Rite1
                  Use In Rite_Sns
                  Select Mrcodcon,Max(Anflgest) As Anflgest,Max(Ancodfis) Ancodfis,; 
 Max(Ancognom) As Ancognom,Max(An__Nome) As An__Nome, Max(Andescri) As Andescri,; 
 Max(An_sesso) As An_sesso,Max(Andatnas) Andatnas,Max(Anlocnas) As Anlocnas,; 
 Max(Anpronas) As Anpronas,Max(Aneveecc) As Aneveecc,; 
 Max(Ancofisc) As Ancofisc,Max(Anlocali) As Anlocali,; 
 Max(Anindiri) As Anindiri,Max(Anprovin) As Anprovin,; 
 Trcaupre,Max(Ancodcom) As Ancodcom,; 
 Max(Somtota) As Somtota,Min(Anflsgre) As Anflsgre,; 
 Sum(Nonsogg) as Nonsogg,Max(Imponi) As Imponi,Max(Mrtotim1) As Mrtotim1,; 
 Max(Mrtotim2) As Mrtotim2,Max(Mrimpper) As Mrimpper,Max(Mrsperim) As Mrsperim,; 
 Min(Riterimb) As Riterimb,Max(Nacodest) As Nacodest,Max(Anperfis) As Anperfis,; 
 Mrcodsns,Max(Ancatpar) as Ancatpar,Max(Cauprest) as Cauprest,; 
 Max(Cdprotec) as Cdprotec,Max(Cdprodoc) as Cdprodoc ,; 
 Max(Cdsercer) as Cdsercer,Min(Ordina) as TipoOrdinamento,Max(Cdprocer) as Cdprocer; 
 From Rite Group By Mrcodcon, Trcaupre,Mrcodsns; 
 Order by Mrcodcon,Trcaupre,TipoOrdinamento,Mrcodsns into cursor Rite1
                  Use In Rite
                endif
                this.Page_3()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
                if this.w_Block
                  Use In Rite1
                  i_retcode = 'stop'
                  return
                endif
              endif
              * --- Drop temporary table ElePerc
              i_nIdx=cp_GetTableDefIdx('ElePerc')
              if i_nIdx<>0
                cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
                cp_RemoveTableDef('ElePerc')
              endif
            else
              do case
                case this.oParentObject.w_Cutipcas="A"
                  ah_ErrorMsg("Non � stata selezionata nessuna certificazione da annullare")
                case this.oParentObject.w_Cutipcas="S"
                  ah_ErrorMsg("Non � stata selezionata nessuna certificazione da sostituire")
                case this.oParentObject.w_Cutipcas="O"
                  ah_ErrorMsg("Non � stata selezionata nessuna certificazione da generare")
              endcase
              i_retcode = 'stop'
              return
            endif
          endif
        else
          * --- Creo un cursore con l'elenco dei record H che dovranno essere riportati
          *     nella generazione
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        * --- Calcolo il numero di fornitori presenti nel cursore Rite1
        if this.oParentObject.w_CuagrCer="S" Or (this.oParentObject.w_Cutipcas $ "SO" And this.oParentObject.w_Cucersin="S")
          Select distinct Mrcodcon,Trcaupre from Rite1 into cursor Temp
        else
          Select distinct Mrcodcon,Trcaupre,Mrcodsns from Rite1 into cursor Temp
        endif
        Select Temp
        this.oParentObject.w_Cunumcer = iif(this.oParentObject.w_Cutipcas="A",0,Reccount())
        Use In Temp
        this.w_Codcon_Appoggio = Space(15)
        this.w_Caupre_Appoggio = Space(1)
        this.w_ProgrModulo = 0
        this.w_ContaRecordH = 0
        this.w_ScritturaRecord = ""
        * --- Riga di testata
        this.Page_4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.Page_5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Ciclo il cursore Rite1 per valorizzare sia i record  D che i record H
        this.Page_11()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        Select Rite1
        Go Top
        this.w_ProgrCert = 0
        Scan
        this.w_Mrcodcon = Rite1.Mrcodcon
        this.w_Perceste = Anflgest
        this.w_Anflsgre = NVL(Anflsgre,"")
        this.w_Cfperest = NVL(Ancofisc," ")
        if NVL(this.w_Perceste,"")="S" And this.w_Anflsgre="S"
          this.w_Percipcf = space(16)
        else
          this.w_Percipcf = IIF(Not Empty(Nvl(Ancofisc," ")),left(Ancofisc,16),Nvl(Ancodfis,""))
        endif
        * --- Creo il record D solo quando viene cambiato il codice fornitore
        if (Mrcodcon<>this.w_Codcon_Appoggio Or Trcaupre<>this.w_Caupre_Appoggio) Or (this.oParentObject.w_CuagrCer="N" And this.oParentObject.w_Cucersin<>"S")
          this.w_ProgrCert = this.w_ProgrCert+1
          this.w_ProgrModulo = 0
          this.w_TipoRecord = "D"
          this.Page_6()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_Codcon_Appoggio = Mrcodcon
          this.w_Caupre_Appoggio = Trcaupre
        endif
        * --- Nel caso di annullamento non scrivo il record H
        if this.oParentObject.w_Cutipcas $ "OS"
          this.w_ProgrModulo = this.w_ProgrModulo+1
          this.w_ContaRecordH = this.w_ContaRecordH+1
          this.w_TipoRecord = "H"
          this.Page_8()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        this.Page_12()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        Select Rite1
        Endscan
        if LEN (this.w_ScritturaRecord) > 0
          Strtofile(this.w_ScritturaRecord ,this.w_NomFil,.t.)
          this.w_ScritturaRecord = ""
        endif
        * --- Scrittura Record Z
        this.Page_10()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if this.oParentObject.w_Cucersin="S"
          this.oParentObject.w_Cudesagg = iif(Not Empty(this.oParentObject.w_Cudesagg),this.oParentObject.w_Cudesagg,Ah_MsgFormat("C.F. Percipiente:%1 Progr. Certificazione:%2",Upper(this.w_Percipcf),Padl(Int(this.w_CdProcer), 5, "0")))
        endif
        this.w_Nomfil = this.oParentObject.w_Cunomfil
        this.w_Sogest = this.oParentObject.w_Cusogest
        this.oParentObject.oFirstControl.SetFocus() 
 this.oParentObject.bUpdated=.t. 
 this.oParentObject.EcpSave()
        if this.w_Sogest<>"S"
          Ah_ErrorMsg("Scrittura del file %1 terminata con successo",,,Alltrim(this.w_Nomfil))
        else
          Ah_ErrorMsg("Elaborazione terminata con successo")
        endif
        Use In Rite1
      case this.pParame="M"
        * --- Lancio una maschera per dare l'oppurtunit� all'utente finale di stampare
        *     solo determinati percipienti
        this.w_StampaFront = "S"
        this.w_StampaDettaglio = "N"
        this.w_TipoZoom = "S"
        do Gsri_Scu with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if this.w_StampaDettaglio="S"
          if Reccount("ElencoPercipienti")>0
            Curtotab("ElencoPercipienti","ElePerc")
            Use In ElencoPercipienti
          else
            this.w_StampaDettaglio = "N"
          endif
        endif
        this.Page_13()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pParame="S" or this.pParame="D" or this.pParame="I"
        this.w_ZOOM = iif(this.oParentObject.w_TipoZoom="S",This.oParentObject.w_ElePerc,iif(this.oParentObject.w_Cutipcas="A",This.oParentObject.w_AnuPerc,This.oParentObject.w_ElePerc))
        Select (this.w_ZOOM.cCursor)
        this.w_NFLTATTR = RECNO()
        UPDATE (this.w_ZOOM.cCursor) SET XCHK=ICASE(this.pParame=="S",1,this.pParame=="D",0,IIF(XCHK=1,0,1))
        Select (this.w_ZOOM.cCursor)
        if this.w_NFLTATTR<RECCOUNT()
          GO this.w_NFLTATTR
        endif
      case this.pParame="Z"
        this.w_Conferma = .F.
        do Gsri1Scs with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if this.w_Conferma
          this.w_ZOOM = This.oParentObject.w_AnuPerc
          Select (this.w_Zoom.cCursor)
          this.w_Posi = Recno()
          Update (this.w_Zoom.cCursor) Set Cdprotec=this.w_Cdprotec,Cdprodoc=this.w_Cdprodoc where Cdserial=This.oParentObject.w_Cdserial and Cprownum=This.oParentObject.w_NumeroRiga
          Select (this.w_Zoom.cCursor)
          if this.w_Posi<Reccount()
            Go this.w_Posi
          endif
        endif
      case this.pParame="C"
        if Used("ElencoCertificazioni")
          Use in ElencoCertificazioni
        endif
      case this.pParame="V"
        this.w_Conferma = .F.
        this.w_Cdcodfis = This.oParentObject.w_Codfis
        this.w_Cdcognom = This.oParentObject.w_Cognom
        this.w_Cd__Nome = This.oParentObject.w_Nome
        this.w_Cdperfis = iif(Not Empty(this.w_Cd__Nome),"S","N")
        this.w_Cd_Sesso = This.oParentObject.w_Sesso
        this.w_Cddatnas = Cp_Todate(This.oParentObject.w_Datnas)
        this.w_Cdcomnas = This.oParentObject.w_Comnas
        this.w_Cdpronas = This.oParentObject.w_Pronas
        this.w_Cdcatpar = This.oParentObject.w_Catpar
        this.w_Cdeveecc = This.oParentObject.w_Eveecc
        this.w_Cdcomfis = This.oParentObject.w_Comfis
        this.w_Cdprofis = This.oParentObject.w_Profis
        this.w_Cdcomune = This.oParentObject.w_Comune
        this.w_Cdcaupre = This.oParentObject.w_Caupre 
        this.w_Cdammcor = This.oParentObject.w_Ammcor
        this.w_Cdsnsogg = This.oParentObject.w_Snsogg
        this.w_Cdcodice = This.oParentObject.w_Codice
        this.w_Cdimponi = This.oParentObject.w_Imponi
        this.w_Cdritacc = This.oParentObject.w_Ritacc
        this.w_Cdsogero = This.oParentObject.w_Sogero
        this.w_Cdpercip = This.oParentObject.w_Percip
        this.w_Cdsperim = This.oParentObject.w_Sperim
        this.w_Cdritrim = This.oParentObject.w_Ritrim
        this.w_Tipcas = This.oParentObject.w_Cutipcas
        this.w_Cdpromod = This.oParentObject.w_Promod
        this.w_Cdpromod1 = This.oParentObject.w_Promod1
        this.w_Cdpromod2 = This.oParentObject.w_Promod2
        this.w_CdCodice1 = This.oParentObject.w_Codice1
        this.w_CdCodice2 = This.oParentObject.w_Codice2
        this.w_CdSnsogg1 = This.oParentObject.w_SnsOgg1
        this.w_CdSnsogg2 = This.oParentObject.w_SnsOgg2
        this.w_Attmod1 = iif(this.w_CdSnsogg1<>0,"S","N")
        this.w_Attmod2 = iif(this.w_CdSnsogg2<>0,"S","N")
        this.w_Cdfperci = This.oParentObject.w_Fperci
        do Gsri2Scs with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if this.w_Conferma
          this.w_ZOOM = This.oParentObject.w_ElePerc
          Select (this.w_Zoom.cCursor)
          this.w_Posi = Recno()
          Update (this.w_Zoom.cCursor) Set Cdprotec=this.w_Cdprotec,Cdprodoc=this.w_Cdprodoc,; 
 Cdcodfis=this.w_Cdcodfis,Cdcognom=this.w_Cdcognom,; 
 Cd__Nome=this.w_Cd__Nome,; 
 Cd_Sesso=this.w_Cd_Sesso,Cddatnas=this.w_Cddatnas,; 
 Cdcomnas=this.w_Cdcomnas,Cdpronas=this.w_Cdpronas,; 
 Cdcatpar=this.w_Cdcatpar,Cdeveecc=this.w_Cdeveecc,; 
 Cdcomfis=this.w_Cdcomfis,Cdprofis=this.w_Cdprofis,; 
 Cdcomune=this.w_Cdcomune,Cdcaupre=this.w_Cdcaupre,; 
 Cdammcor=this.w_Cdammcor,Cdsnsogg=this.w_Cdsnsogg,; 
 Cdcodice=this.w_Cdcodice,Cdimponi=this.w_Cdimponi,; 
 Cdritacc=this.w_Cdritacc,Cdsogero=this.w_Cdsogero,; 
 Cdpercip=this.w_Cdpercip,Cdsperim=this.w_Cdsperim,; 
 Codice1=this.w_CdCodice1,Codice2=this.w_CdCodice2,; 
 SnsOgg1=this.w_CdSnsOgg1,SnsOgg2=this.w_CdSnsOgg2,; 
 Cdritrim=this.w_Cdritrim where Cdserial=This.oParentObject.w_Cdserial and Cprownum=This.oParentObject.w_NumeroRiga
          Select (this.w_Zoom.cCursor)
          if this.w_Posi<Reccount()
            Go this.w_Posi
          endif
        endif
      case this.pParame="X"
        this.oparentObject.ecpsave()
        i_retcode = 'stop'
        return
      case this.pParame="B"
        this.w_ZOOM = This.oParentObject.w_AnuPerc
        this.w_ZOOM1 = This.oParentObject.w_ElePerc
        this.w_ZOOM.VISIBLE = .F.
        this.w_ZOOM1.VISIBLE = .F.
        if this.oParentObject.w_Cutipcas="A"
          this.w_ZOOM.VISIBLE = .T.
        else
          this.w_ZOOM1.VISIBLE = .T.
        endif
      case this.pParame="P"
        this.w_Cutipcer = "CUR15"
        this.w_Conferma = .f.
        do Gsri3Scs with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if this.w_Conferma
          do Gsri_Scs with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
    endcase
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- L'elaborazione che viene effettuata in questa pagina � una copia pi�
    *     semplice di quella che avviene nella routine Gsri_bsc
    this.w_Percin = Space(15)
    this.w_Percfin = Space(15)
    this.w_Tipoperaz = " "
    this.w_Datafall = Cp_CharToDate("  -  -    ")
    this.w_Datini = this.oParentObject.w_Cudatini
    this.w_Datfin = this.oParentObject.w_Cudatfin
    this.w_Codval = g_Codeur
    this.w_Obtest = this.w_Datfin
    vq_exec("..\RITE\EXE\QUERY\GSRI_BSC.VQR",this,"RITE")
    * --- Se attivato il flag "Solo soggetti esteri" devono essere stampati
    *     solo i forntori che hanno il flag percipiente estero attivato
    if this.oParentObject.w_Cusogest="S"
      Select * from Rite Where Nvl(Anflgest,"N")="S" into cursor Rite
    else
      Select * from Rite Where Nvl(Anflgest,"N")<>"S" into cursor Rite
    endif
    * --- Nel caso molto remoto esistano movimenti ritenute in altra valuta effettuo
    *     la conversione 
    Wrcursor("RITE")
    Scan For Mrcodval<>this.w_Codval
    Replace Somtota With Cp_round(Valcam(Nvl(Somtota,0), Mrdcoval, this.w_Codval, i_Datsys, 0),g_Perpvl)
    Replace Imponi With Cp_round(Valcam(Nvl(Imponi,0), Mrdcoval, this.w_Codval, i_Datsys, 0),g_Perpvl)
    Replace Nonsogg With Cp_round(Valcam(Nvl(Nonsogg,0), Mrdcoval, this.w_Codval, i_Datsys, 0),g_Perpvl)
    Replace Mrtotim1 With Cp_round(Valcam(Nvl(Mrtotim1,0), Mrdcoval, this.w_Codval, i_Datsys, 0),g_Perpvl)
    Replace Mrtotim2 With Cp_round(Valcam(Nvl(Mrtotim2,0), Mrdcoval, this.w_Codval, i_Datsys, 0),g_Perpvl)
    Replace Mrimpper With Cp_round(Valcam(Nvl(Mrimpper,0), Mrdcoval, this.w_Codval, i_Datsys, 0),g_Perpvl)
    Replace Mrimpri2 With Cp_round(Valcam(Nvl(Mrimpri2,0), Mrdcoval, this.w_Codval, i_Datsys, 0),g_Perpvl)
    Replace Mrsperim With Cp_round(Valcam(Nvl(Mrsperim,0), Mrdcoval, this.w_Codval, i_Datsys, 0),g_Perpvl)
    Replace Riterimb With Cp_round(Valcam(Nvl(Riterimb,0), Mrdcoval, this.w_Codval, i_Datsys, 0),g_Perpvl)
    Endscan
    * --- Seleziono il cursore RITE e sommo i vari campi nella valuta selezionata in mAschera
    Select Mrcodcon,Max(Anflgest) As Anflgest,Max(Ancodfis) Ancodfis,; 
 Max(Ancognom) As Ancognom,Max(An__Nome) As An__Nome, Max(Andescri) As Andescri,; 
 Max(An_sesso) As An_sesso,Max(Andatnas) Andatnas,Max(Anlocnas) As Anlocnas,; 
 Max(AnpronAs) As AnpronAs,Max(Aneveecc) As Aneveecc,; 
 Max(Ancofisc) As Ancofisc,Max(Anlocali) As Anlocali,; 
 Max(Anindiri) As Anindiri,Max(Anprovin) As Anprovin,; 
 Trcaupre,Max(Ancodcom) As Ancodcom,; 
 Sum(Somtota) As Somtota,Min(Anflsgre) As Anflsgre,; 
 Sum(Nonsogg) As Nonsogg,Sum(Imponi) As Imponi,Sum(Mrtotim1) As Mrtotim1,; 
 Sum(Mrtotim2) As Mrtotim2,Sum(Mrimpper) As Mrimpper,Sum(Mrsperim) As Mrsperim,; 
 Sum(Riterimb) As Riterimb,Max(Nacodest) As Nacodest,Max(Anperfis) As Anperfis,; 
 Mrcodsns,Max(Ancatpar) as Ancatpar,Max(Cauprest) as Cauprest,; 
 Space(17) as Cdprotec,999999*0 as Cdprodoc; 
 From Rite Group By Mrcodcon, Trcaupre,Mrcodsns; 
 Order By Mrcodcon ,Trcaupre,Mrcodsns into cursor Rite1
    Use In Rite
    * --- Verifico se � stato attivato il flag "Aggrega certificazioni opzionale", in questo
    *     caso le somme non soggette potrebbero essere raggruppate
    if this.oParentObject.w_CuAgrCer="S"
      * --- Raggruppo il cursore Rite1 per avere il numero di somme non soggette
      *     che sono presenti per lo stesso fornitore e la stessa causale prestazione
      Select Mrcodcon,Max(Anflgest) As Anflgest,Max(Ancodfis) Ancodfis,; 
 Max(Ancognom) As Ancognom,Max(An__Nome) As An__Nome, Max(Andescri) As Andescri,; 
 Max(An_sesso) As An_sesso,Max(Andatnas) Andatnas,Max(Anlocnas) As Anlocnas,; 
 Max(Anpronas) As Anpronas,Max(Aneveecc) As Aneveecc,; 
 Max(Ancofisc) As Ancofisc,Max(Anlocali) As Anlocali,; 
 Max(Anindiri) As Anindiri,Max(Anprovin) As Anprovin,; 
 Trcaupre,Max(Ancodcom) As Ancodcom,; 
 Sum(Somtota) As Somtota,Min(Anflsgre) As Anflsgre,; 
 Max(Nonsogg)*0 as Nonsogg,Sum(Imponi) As Imponi,Sum(Mrtotim1) As Mrtotim1,; 
 Sum(Mrtotim2) As Mrtotim2,Sum(Mrimpper) As Mrimpper,Sum(Mrsperim) As Mrsperim,; 
 Sum(Riterimb) As Riterimb,Max(Nacodest) As Nacodest,Max(Anperfis) As Anperfis,; 
 Min(iif(Empty(Nvl(Mrcodsns," ")),"9",Mrcodsns)) as Mrcodsns,Max(Ancatpar) as Ancatpar,Max(Cauprest) as Cauprest,; 
 Space(17) as Cdprotec,999999*0 as Cdprodoc From Rite1 Group By Mrcodcon, Trcaupre; 
 Order by Mrcodcon,Trcaupre into cursor Rite_Appoggio
      Select Mrcodcon,Max(Anflgest) As Anflgest,Max(Ancodfis) Ancodfis,; 
 Max(Ancognom) As Ancognom,Max(An__Nome) As An__Nome, Max(Andescri) As Andescri,; 
 Max(An_sesso) As An_sesso,Max(Andatnas) Andatnas,Max(Anlocnas) As Anlocnas,; 
 Max(Anpronas) As Anpronas,Max(Aneveecc) As Aneveecc,; 
 Max(Ancofisc) As Ancofisc,Max(Anlocali) As Anlocali,; 
 Max(Anindiri) As Anindiri,Max(Anprovin) As Anprovin,; 
 Trcaupre,Max(Ancodcom) As Ancodcom,; 
 Max(Somtota)*0 As Somtota,Min(Anflsgre) As Anflsgre,; 
 Sum(Nonsogg) As Nonsogg,Max(Imponi)*0 As Imponi,Max(Mrtotim1)*0 As Mrtotim1,; 
 Max(Mrtotim2)*0 As Mrtotim2,Max(Mrimpper)*0 As Mrimpper,Max(Mrsperim)*0 As Mrsperim,; 
 Max(Riterimb)*0 As Riterimb,Max(Nacodest) As Nacodest,Max(Anperfis) As Anperfis,; 
 Mrcodsns as Mrcodsns,Max(Ancatpar) as Ancatpar,Max(Cauprest) as Cauprest,; 
 Space(17) as Cdprotec,999999*0 as Cdprodoc From Rite1 where Nvl(Mrcodsns," ") $ "123"; 
 Group By Mrcodcon, Trcaupre,Mrcodsns Order by Mrcodcon,Trcaupre,Mrcodsns into cursor Rite_Sns
      Select * from Rite_Appoggio ; 
 union all; 
 Select * from Rite_Sns; 
 order By Mrcodcon ,Trcaupre,Mrcodsns into cursor Rite
      Select Mrcodcon,Max(Anflgest) As Anflgest,Max(Ancodfis) Ancodfis,; 
 Max(Ancognom) As Ancognom,Max(An__Nome) As An__Nome, Max(Andescri) As Andescri,; 
 Max(An_sesso) As An_sesso,Max(Andatnas) Andatnas,Max(Anlocnas) As Anlocnas,; 
 Max(Anpronas) As Anpronas,Max(Aneveecc) As Aneveecc,; 
 Max(Ancofisc) As Ancofisc,Max(Anlocali) As Anlocali,; 
 Max(Anindiri) As Anindiri,Max(Anprovin) As Anprovin,; 
 Trcaupre,Max(Ancodcom) As Ancodcom,; 
 Max(Somtota) As Somtota,Min(Anflsgre) As Anflsgre,; 
 Max(Nonsogg) as Nonsogg,Max(Imponi) As Imponi,Max(Mrtotim1) As Mrtotim1,; 
 Max(Mrtotim2) As Mrtotim2,Max(Mrimpper) As Mrimpper,Max(Mrsperim) As Mrsperim,; 
 Max(Riterimb) As Riterimb,Max(Nacodest) As Nacodest,Max(Anperfis) As Anperfis,; 
 Mrcodsns,Max(Ancatpar) as Ancatpar,Max(Cauprest) as Cauprest,; 
 Max(Cdprotec) as Cdprotec,Max(Cdprodoc) as Cdprodoc From Rite Group By Mrcodcon, Trcaupre,Mrcodsns; 
 Order by Mrcodcon,Trcaupre,Mrcodsns into cursor Rite1
      Use In Rite_Appoggio
      Use In Rite_Sns
      Use In Rite
    endif
    * --- Controllo se esiste almeno un record da inserire nel quadro H
    if Reccount("Rite1")>0
      * --- Verifico congruenza record H
      this.Page_3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if this.w_Block
        Use In Rite1
        i_retcode = 'stop'
        return
      endif
    else
      ah_ErrorMsg("Non esistono certificazioni da presentare")
      Use In Rite1
      i_retcode = 'stop'
      return
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Creo cursore messaggistica di errore (Mancanza campi obbligatori)
    Create cursor Listaperc (Codcon C(15),Denomi C(40),Flgest C(1),Codfis C(1),Cogden C(1),Nome C(1),; 
 Sesso C(1),Datnas C(1),Comnas C(1),Comun C(1),Provincia C(1),Codcom C(1),Evenecc C(1),Cauprest C(1))
    this.w_Block = .f.
    this.w_Attention = .f.
    Select Rite1 
 Go Top 
 Scan
    this.w_Mrcodcon = Nvl(Mrcodcon," ")
    this.w_Andescri = NVL(Andescri," ")
    this.w_Perceste = Anflgest
    if Empty(Ancodfis) and this.w_Perceste<>"S"
      this.w_Block = .t.
      Insert Into Listaperc Values (this.w_Mrcodcon,this.w_Andescri,this.w_Perceste,"1","0","0","0","0","0","0","0","0","0","0")
      select Rite1
    endif
    if iif(Anperfis="S",Empty(Ancognom),Empty(Andescri) )
      this.w_Block = .t.
      Insert Into Listaperc Values (this.w_Mrcodcon,this.w_Andescri,this.w_Perceste,"0","1","0","0","0","0","0","0","0","0","0")
      select Rite1
    endif
    if Anperfis="S"
      if Empty(An__Nome)
        this.w_Block = .t.
        Insert Into Listaperc Values (this.w_Mrcodcon,this.w_Andescri,this.w_Perceste,"0","0","1","0","0","0","0","0","0","0","0")
        select Rite1
      endif
      if Empty(An_Sesso)
        this.w_Block = .t.
        Insert Into Listaperc Values (this.w_Mrcodcon,this.w_Andescri,this.w_Perceste,"0","0","0","1","0","0","0","0","0","0","0")
        select Rite1
      endif
      if Empty(Cp_Todate(Andatnas))
        this.w_Block = .t.
        Insert Into Listaperc Values (this.w_Mrcodcon,this.w_Andescri,this.w_Perceste,"0","0","0","0","1","0","0","0","0","0","0")
        select Rite1
      endif
      if Empty(Anlocnas)
        this.w_Block = .t.
        Insert Into Listaperc Values (this.w_Mrcodcon,this.w_Andescri,this.w_Perceste,"0","0","0","0","0","1","0","0","0","0","0")
        select Rite1
      endif
    endif
    if Aneveecc<>"0" AND Aneveecc<>"1" AND Aneveecc<>"3" AND Aneveecc<>"8" 
      this.w_Block = .t.
      Insert Into Listaperc Values (this.w_Mrcodcon,this.w_Andescri,this.w_Perceste,"0","0","0","0","0","0","0","0","0","1","0")
      select Rite1
    endif
    if Cauprest="S" 
      this.w_Block = .t.
      Insert Into Listaperc Values (this.w_Mrcodcon,this.w_Andescri,this.w_Perceste,"0","0","0","0","0","0","0","0","0","0","1")
      select Rite1
    endif
    if Trcaupre="N" And this.w_Perceste<>"S"
      if Empty(Ancodcom)
        this.w_Block = .t.
        Insert Into Listaperc Values (this.w_Mrcodcon,this.w_Andescri,this.w_Perceste,"0","0","0","0","0","0","0","0","1","0","0")
        select Rite1
      endif
      if Empty(Anlocali) 
        this.w_Attention = .t.
        Insert Into Listaperc Values (this.w_Mrcodcon,this.w_Andescri,this.w_Perceste,"0","0","0","0","0","0","1","0","0","0","0")
        select Rite1
      endif
      if Empty(Anprovin) 
        this.w_Attention = .t.
        Insert Into Listaperc Values (this.w_Mrcodcon,this.w_Andescri,this.w_Perceste,"0","0","0","0","0","0","0","1","0","0","0")
        select Rite1
      endif
    endif
    Endscan
    if this.w_Block Or this.w_Attention
      if this.w_Block
        this.w_Mess = "Dati anagrafici percipienti mancanti"+chr(13)+"Impossibile generare il file"+chr(13)+"Vuoi visualizzare la stampa di verifica?"
      else
        this.w_Mess = "Dati anagrafici percipienti mancanti"+chr(13)+"Vuoi visualizzare la stampa di verifica?"
      endif
      if ah_YesNo(this.w_Mess)
        Select Codcon,MAX(Denomi) AS Denomi,Max(Flgest) As Flgest,Max(Codfis) as Codfis,; 
 Max(Cogden) as Cogden,Max(Nome) as Nome,Max(Sesso) as Sesso, Max(Datnas) as Datnas,; 
 Max(Comnas) as Comnas,Max(Comun) as Comun, Max(Provincia) as Provincia,; 
 Max(Codcom) as Codcom,Max(Evenecc) as Evenecc,Max(Cauprest) as Cauprest; 
 from Listaperc into cursor __TMP__ group by Codcon order by Codcon
        CP_CHPRN("..\RITE\EXE\QUERY\GSRI_ACU.FRX")
      endif
    endif
    Use In Listaperc
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Scrittura Record A
    this.w_RecordA = "A"+Space(14)+"CUR15"+this.oParentObject.w_Cutipfor
    if empty(this.oParentObject.w_Cucodint)
      if this.oParentObject.w_Curapfir="S" And (this.oParentObject.w_Cucodcar = "2" Or this.oParentObject.w_Cucodcar ="3" Or this.oParentObject.w_Cucodcar ="4" Or this.oParentObject.w_Cucodcar ="5" Or this.oParentObject.w_Cucodcar ="7" OR this.oParentObject.w_Cucodcar ="11")
        * --- Cod. fiscale firmatario
        this.w_RecordA = this.w_RecordA+Upper(this.oParentObject.w_Cucfisot)
      else
        this.w_RecordA = this.w_RecordA+Upper(this.w_CfAzi)
      endif
    else
      * --- Cod. fiscale intermediario
      this.w_RecordA = this.w_RecordA+Upper(this.oParentObject.w_Cucodint)
    endif
    this.w_RecordA = this.w_RecordA+Space(1897-len(this.w_RecordA))+"A"+this.w_Separa
    if this.oParentObject.w_Cusogest<>"S"
      Strtofile(this.w_RecordA,this.w_NomFil,.t.)
    endif
    this.w_RecordA = " "
  endproc


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Record B - Frontespizio
    this.w_RecordB = "B"+iif(Not Empty(this.oParentObject.w_Cucodfsi),Upper(this.oParentObject.w_Cucodfsi),Upper(this.w_CfAzi))+"00000001"+Space(48)+"05006900962     "+Space(1)+iif(this.oParentObject.w_Cutipcas="A","1","0")+iif(this.oParentObject.w_Cutipcas="S","1","0")
    this.w_RecordB = this.w_RecordB+Padr(Left(Upper(this.oParentObject.w_Cucognom), 24), 24)+Padr(Left(Upper(this.oParentObject.w_Cu__Nome), 20), 20)
    this.w_RecordB = this.w_RecordB+Padr(Left(Upper(this.oParentObject.w_Curagsoc), 60), 60)
    this.w_RecordB = this.w_RecordB+Padr(Left(Upper(this.oParentObject.w_Cu__Mail),100), 100)+Padr(left(this.oParentObject.w_Cunumfax,12), 12)+Space(12)
    if this.oParentObject.w_Curapfir="S"
      this.w_RecordB = this.w_RecordB+Padr(Upper(this.oParentObject.w_Cucfisot), 16)+ Padl(this.oParentObject.w_Cucodcar, 2, "0")+Padr(Left(Upper(this.oParentObject.w_Cucogsot), 24), 24) + Padr(Left(Upper(this.oParentObject.w_Cunomsot), 20), 20)
      this.w_RecordB = this.w_RecordB+iif(Not Empty(this.oParentObject.w_Cucodfed),Right(Replicate("0",11)+this.oParentObject.w_Cucodfed,11),Replicate("0",11))
    else
      this.w_RecordB = this.w_RecordB+Space(16)+"00"+Space(24)+Space(20)+Replicate("0",11)
    endif
    this.w_RecordB = this.w_RecordB+Replicate("0",8)+Padl(this.oParentObject.w_Cunumcer, 8, "0")+"0"+this.oParentObject.w_Cufirdic
    this.w_RecordB = this.w_RecordB+Padr(Upper(this.oParentObject.w_Cucodint), 16)+iif(this.oParentObject.w_Cufirint<>"0" or Not Empty(this.oParentObject.w_Cudatimp) or Not Empty(this.oParentObject.w_Cucodint),Left(Alltrim(this.oParentObject.w_Cuimptra)+"0",1),"0")
    this.w_RecordB = this.w_RecordB+iif(Not Empty(this.oParentObject.w_Cudatimp),Right("00"+Alltrim(Str(Day(this.oParentObject.w_Cudatimp))),2)+Right("00"+Alltrim(Str(Month(this.oParentObject.w_Cudatimp))),2)+Right(Str(Year(this.oParentObject.w_Cudatimp),4,0),4),Repl("0",8))
    this.w_RecordB = this.w_RecordB+this.oParentObject.w_Cufirint
    this.w_RecordB = this.w_RecordB+Space(1897-len(this.w_RecordB))+"A"+this.w_Separa
    if this.oParentObject.w_Cusogest<>"S"
      Strtofile(this.w_RecordB,this.w_NomFil,.t.)
    endif
    this.w_RecordB = " "
  endproc


  procedure Page_6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_RecordD = "D"+iif(Not Empty(this.oParentObject.w_Cucodfsi),Upper(this.oParentObject.w_Cucodfsi),Upper(this.w_CfAzi))+"00000001"+Upper(this.w_Percipcf)+Padl(this.w_ProgrCert, 5, "0")
    this.w_RecordD = this.w_RecordD+iif(this.oParentObject.w_Cutipcas="O",Replicate("0",23),Padl(Alltrim(Cdprotec),17,"0") + Padl(Alltrim(Str(Cdprodoc,6)),6,"0"))+Space(14)
    this.w_RecordD = this.w_RecordD+iif(this.oParentObject.w_Cutipcas="O",Space(1),this.oParentObject.w_Cutipcas)+Space(4)+"0"
    this.w_NomeCampo = iif(Not Empty(this.oParentObject.w_Cucodfsi),Upper(this.oParentObject.w_Cucodfsi),Upper(this.w_CfAzi))
    this.w_IdenRigoAppo = "DA001001"
    this.Page_7()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_NomeCampo = iif(Not Empty(this.oParentObject.w_Curagsoc),this.oParentObject.w_Curagsoc,this.oParentObject.w_Cucognom)
    this.w_IdenRigoAppo = "DA001002"
    this.Page_7()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_NomeCampo = this.oParentObject.w_Cu__Nome
    this.w_IdenRigoAppo = "DA001003"
    this.Page_7()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_NomeCampo = this.oParentObject.w_Cucomune
    this.w_IdenRigoAppo = "DA001004"
    this.Page_7()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_NomeCampo = this.oParentObject.w_Cuprores
    this.w_IdenRigoAppo = "DA001005"
    this.Page_7()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_NomeCampo = this.oParentObject.w_Cu___Cap
    this.w_IdenRigoAppo = "DA001006"
    this.Page_7()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_NomeCampo = this.oParentObject.w_Cuindiri
    this.w_IdenRigoAppo = "DA001007"
    this.Page_7()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_NomeCampo = this.oParentObject.w_Cunumfax
    this.w_IdenRigoAppo = "DA001008"
    this.Page_7()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_NomeCampo = this.oParentObject.w_Cu__Mail
    this.w_IdenRigoAppo = "DA001009"
    this.Page_7()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_NomeCampo = this.oParentObject.w_Cucodatt
    this.w_IdenRigoAppo = "DA001010"
    this.Page_7()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_NomeCampo = this.oParentObject.w_Cucodsed
    this.w_IdenRigoAppo = "DA001011"
    this.Page_7()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_NomeCampo = iif(this.w_Perceste<> "S" ,Nvl(Ancodfis," "),Space(16))
    this.w_IdenRigoAppo = "DA002001"
    this.Page_7()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_NomeCampo = iif(Anperfis="S",Nvl(Ancognom," "),Nvl(Andescri," "))
    this.w_IdenRigoAppo = "DA002002"
    this.Page_7()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_NomeCampo = iif(Anperfis="S",Nvl(An__Nome," ")," ")
    this.w_IdenRigoAppo = "DA002003"
    this.Page_7()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_NomeCampo = iif(Anperfis="S",Nvl(An_Sesso," ")," ")
    this.w_IdenRigoAppo = "DA002004"
    this.Page_7()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_NomeCampo = iif(Anperfis="S" And Not Empty(Andatnas),Right(space(16)+Right("00"+Alltrim(Str(Day(Andatnas))),2)+Right("00" +Alltrim(Str(Month (Andatnas))),2)+Right("0000"+alltrim(Str(Year(Andatnas))),4),16)," ")
    this.w_IdenRigoAppo = "DA002005"
    this.Page_7()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_NomeCampo = iif(Anperfis="S",Nvl(Anlocnas," ")," ")
    this.w_IdenRigoAppo = "DA002006"
    this.Page_7()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_NomeCampo = iif(Anperfis="S",Nvl(Anpronas," ")," ")
    this.w_IdenRigoAppo = "DA002007"
    this.Page_7()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_NomeCampo = Nvl(Ancatpar,"  ")
    this.w_IdenRigoAppo = "DA002008"
    this.Page_7()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if Nvl(Aneveecc,"0")<> "0" 
      this.w_NomeCampo = Padl(Int(Val(Aneveecc)),16)
      this.w_IdenRigoAppo = "DA002009"
      this.Page_7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if Trcaupre="N" And this.w_Perceste<>"S"
      this.w_NomeCampo = Nvl(Anlocali,"  ")
      this.w_IdenRigoAppo = "DA002020"
      this.Page_7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_NomeCampo = Nvl(Anprovin,"  ")
      this.w_IdenRigoAppo = "DA002021"
      this.Page_7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_NomeCampo = Nvl(Ancodcom,"  ")
      this.w_IdenRigoAppo = "DA002022"
      this.Page_7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.w_Perceste="S"
      this.w_NomeCampo = iif(Not Empty(this.w_Cfperest),this.w_Cfperest,Nvl(Ancodfis," "))
      this.w_IdenRigoAppo = "DA002040"
      this.Page_7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_NomeCampo = Nvl(Anlocali," ")
      this.w_IdenRigoAppo = "DA002041"
      this.Page_7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_NomeCampo = Nvl(Anindiri," ")
      this.w_IdenRigoAppo = "DA002042"
      this.Page_7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_NomeCampo = Space(13)+Right(Repl("0",3)+Alltrim(Nvl(Nacodest," ")),3)
      this.w_IdenRigoAppo = "DA002043"
      this.Page_7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    this.w_NomeCampo = Right(space(16)+Right("00"+Alltrim(Str(Day(this.oParentObject.w_Cudatfir))),2)+Right("00" +Alltrim(Str(Month (this.oParentObject.w_Cudatfir))),2)+Right("0000"+alltrim(Str(Year(this.oParentObject.w_Cudatfir))),4),16)
    this.w_IdenRigoAppo = "DA003001"
    this.Page_7()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_RecordD = this.w_RecordD+"DA003002"+Padl(this.oParentObject.w_Cufirdic,16)
    this.w_RecordD = this.w_RecordD+Space(1897-len(this.w_RecordD))+"A"+this.w_Separa
    if this.oParentObject.w_Cusogest<>"S"
      this.w_ScritturaRecord = this.w_ScritturaRecord + this.w_RecordD
      if Len (this.w_ScritturaRecord) > this.w_LunghezzaRecord
        Strtofile(this.w_ScritturaRecord ,this.w_NomFil,.t.)
        this.w_ScritturaRecord = ""
      endif
    endif
    this.w_RecordD = " "
  endproc


  procedure Page_7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if Not Empty(this.w_NomeCampo)
      this.w_NomeCampoTr = this.w_NomeCampo
      if len(alltrim(this.w_NomeCampoTr)) > 16
        this.w_NomeCampo = this.w_IdenRigoAppo+Left(this.w_NomeCampo,16)
        this.w_NomeCampoTr = Substr(this.w_NomeCampoTr,17)
        do while Not Empty(Alltrim(this.w_NomeCampoTr))
          this.w_NomeCampo = this.w_NomeCampo+this.w_IdenRigoAppo+"+"+left(this.w_NomeCampoTr,15)+Space(15-iif(len(this.w_NomeCampoTr)>15,15,len(this.w_NomeCampoTr)))
          this.w_NomeCampoTr = Substr(this.w_NomeCampoTr,16)
        enddo
      else
        this.w_NomeCampo = this.w_IdenRigoAppo+Padr(this.w_NomeCampo,16)
      endif
      if this.w_TipoRecord="D"
        this.w_RecordD = this.w_RecordD+Upper(this.w_NomeCampo)
      else
        this.w_RecordH = this.w_RecordH+Upper(this.w_NomeCampo)
      endif
    endif
  endproc


  procedure Page_8
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_RecordH = "H"+iif(Not Empty(this.oParentObject.w_Cucodfsi),Upper(this.oParentObject.w_Cucodfsi),Upper(this.w_CfAzi))+Padl(this.w_ProgrModulo, 8, "0")+Upper(this.w_Percipcf)+Padl(this.w_ProgrCert, 5, "0")+Replicate("0",23)+Space(14)+Space(5)+"0"
    if this.w_ProgrModulo=1
      this.w_NomeCampo = Nvl(Trcaupre," ")
      this.w_IdenRigoAppo = "AU001001"
      this.Page_7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_NomeCampo = iif(Alltrim(Nvl(Trcaupre," ")) $ "G-H-I",Padl(Alltrim(Str(Year(this.w_Datfin))),16),Space(16))
      this.w_IdenRigoAppo = "AU001002"
      this.Page_7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_NomeCampo = iif(Nvl(Somtota,0) < 0,0,Nvl(Somtota,0))
      this.Page_9()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_IdenRigoAppo = "AU001004"
      this.Page_7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      this.w_NomeCampo = iif(Alltrim(Nvl(Trcaupre," ")) $ "G-H-I",Padl(Alltrim(Str(Year(this.w_Datfin))),16),Space(16))
      this.w_IdenRigoAppo = "AU001002"
      this.Page_7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.w_Perceste="S" And this.w_Anflsgre="S"
      this.w_NomeCampo = iif(Nvl(Nonsogg,0) < 0,0,Nvl(Nonsogg,0))
      this.Page_9()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_IdenRigoAppo = "AU001005"
      this.Page_7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if Nvl(Mrcodsns," ") $ "123"
        this.w_NomeCampo = Padl(Int(Val(Mrcodsns)),16)
        this.w_IdenRigoAppo = "AU001006"
        this.Page_7()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    else
      if Nvl(Mrcodsns," ") $ "123"
        this.w_NomeCampo = Padl(Int(Val(Mrcodsns)),16)
        this.w_IdenRigoAppo = "AU001006"
        this.Page_7()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      this.w_NomeCampo = iif(Nvl(Nonsogg,0) < 0,0,Nvl(Nonsogg,0))
      this.Page_9()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_IdenRigoAppo = "AU001007"
      this.Page_7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.w_ProgrModulo=1
      this.w_NomeCampo = iif(Nvl(Imponi,0) < 0,0,Nvl(Imponi,0))
      this.Page_9()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_IdenRigoAppo = "AU001008"
      this.Page_7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_NomeCampo = iif(Nvl(Mrtotim1,0) < 0,0,Nvl(Mrtotim1,0))
      this.Page_9()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_IdenRigoAppo = "AU001009"
      this.Page_7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_NomeCampo = iif(Alltrim(Nvl(Trcaupre," ")) $ "C-M-M1-V" And Nvl(Mrtotim2,0)-Nvl(Mrimpper,0)>0 ,Nvl(Mrtotim2,0)-Nvl(Mrimpper,0),0)
      this.Page_9()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_IdenRigoAppo = "AU001020"
      this.Page_7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_NomeCampo = iif(Alltrim(Nvl(Trcaupre," ")) $ "C-M-M1-V" And Nvl(Mrimpper,0)>=0 ,Nvl(Mrimpper,0),0)
      this.Page_9()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_IdenRigoAppo = "AU001021"
      this.Page_7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_NomeCampo = iif(Nvl(Mrsperim,0)>=0 ,Nvl(Mrsperim,0),0)
      this.Page_9()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_IdenRigoAppo = "AU001022"
      this.Page_7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_NomeCampo = iif(Alltrim(Nvl(Trcaupre," ")) $ "X-Y" And Nvl(Riterimb,0)<0 ,Abs(Nvl(Riterimb,0)),0)
      this.Page_9()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_IdenRigoAppo = "AU001023"
      this.Page_7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    this.w_RecordH = this.w_RecordH+Space(1897-len(this.w_RecordH))+"A"+this.w_Separa
    if this.oParentObject.w_Cusogest<>"S"
      this.w_ScritturaRecord = this.w_ScritturaRecord + this.w_RecordH
      if Len (this.w_ScritturaRecord) > this.w_LunghezzaRecord
        Strtofile(this.w_ScritturaRecord,this.w_NomFil,.t.)
        this.w_ScritturaRecord = ""
      endif
    endif
    this.w_RecordH = " "
  endproc


  procedure Page_9
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione formato campi numerici
    if Not Empty(this.w_NomeCampo)
      this.w_NomeCampo = Alltrim(Str(Cp_Round(this.w_NomeCampo,2),16,2))
      this.w_NomeCampo = Right(Space(16)+this.w_NomeCampo,16)
    endif
  endproc


  procedure Page_10
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Scrittura Record Z
    this.w_RecordZ = "Z"+Space(14)+"000000001"+"000000000"+Padl(this.w_ProgrCert, 9, "0")+"000000000"+iif(this.oParentObject.w_Cutipcas="A","000000000",Padl(this.w_ContaRecordH, 9, "0"))
    this.w_RecordZ = this.w_RecordZ+Space(1897-len(this.w_RecordZ))+"A"+this.w_Separa
    if this.oParentObject.w_Cusogest<>"S"
      Strtofile(this.w_RecordZ,this.w_NomFil,.t.)
    endif
    this.w_RecordZ = " "
  endproc


  procedure Page_11
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_Gsri_Mcu.FirstRow()     
    do while Not this.w_Gsri_Mcu.Eof_Trs() And this.w_Gsri_Mcu.FullRow() 
      this.w_Gsri_Mcu.DeleteRow()     
      this.w_Gsri_Mcu.FirstRow()     
    enddo
  endproc


  procedure Page_12
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_Gsri_Mcu.FirstRow()     
    this.w_Gsri_Mcu.AddRow()     
    this.w_Gsri_Mcu.w_Cdserial = this.oParentObject.w_Cuserial
    this.w_Gsri_Mcu.w_Cdfperci = this.w_Mrcodcon
    this.w_Gsri_Mcu.w_Cdprocer = this.w_ProgrCert
    this.w_Gsri_Mcu.w_Cdcodfis = iif(this.w_Perceste<> "S" ,Nvl(Ancodfis," "),Space(16))
    this.w_Gsri_Mcu.w_Cdperest = this.w_Perceste
    this.w_Gsri_Mcu.w_Cdcognom = iif(Anperfis="S",Nvl(Ancognom," "),Nvl(Andescri," "))
    this.w_Gsri_Mcu.w_Cd__Nome = iif(Anperfis="S",Nvl(An__Nome," ")," ")
    this.w_Gsri_Mcu.w_Cd_Sesso = iif(Anperfis="S",Nvl(An_Sesso," ")," ")
    this.w_Gsri_Mcu.w_Cddatnas = iif(Anperfis="S" And Not Empty(Andatnas),Andatnas,Cp_CharToDate("  -  -    "))
    this.w_Gsri_Mcu.w_Cdcomnas = iif(Anperfis="S",Nvl(Anlocnas," ")," ")
    this.w_Gsri_Mcu.w_Cdpronas = iif(Anperfis="S",Nvl(Anpronas," ")," ")
    this.w_Gsri_Mcu.w_Cdcatpar = Nvl(Ancatpar,"  ")
    this.w_Gsri_Mcu.w_Cdeveecc = Aneveecc
    this.w_Gsri_Mcu.w_Cdcomfis = iif(Trcaupre="N" And this.w_Perceste<>"S",Nvl(Anlocali,"  ")," ")
    this.w_Gsri_Mcu.w_Cdprofis = iif(Trcaupre="N" And this.w_Perceste<>"S",Nvl(Anprovin,"  ")," ")
    this.w_Gsri_Mcu.w_Cdcomune = iif(Trcaupre="N" And this.w_Perceste<>"S",Nvl(Ancodcom,"  ")," ")
    this.w_Gsri_Mcu.w_Cdcodfes = iif(this.w_Perceste="S",iif(Not Empty(this.w_Cfperest),this.w_Cfperest,Nvl(Ancodfis," "))," ")
    this.w_Gsri_Mcu.w_Cdlocest = iif(this.w_Perceste="S",Nvl(Anlocali,"  ")," ")
    this.w_Gsri_Mcu.w_Cdindiri = iif(this.w_Perceste="S",Nvl(Anindiri,"  ")," ")
    this.w_Gsri_Mcu.w_Cdcodest = iif(this.w_Perceste="S",Nvl(Nacodest," ")," ")
    if this.w_ProgrModulo=1
      this.w_Gsri_Mcu.w_Cdcaupre = Nvl(Trcaupre," ")
      this.w_Gsri_Mcu.w_Cdammcor = Cp_Round(iif(Nvl(Somtota,0) < 0,0,Nvl(Somtota,0)),2)
      this.w_Gsri_Mcu.w_Cdsogres = this.w_Anflsgre
      this.w_Gsri_Mcu.w_Cdimponi = Cp_Round(iif(Nvl(Imponi,0) < 0,0,Nvl(Imponi,0)),2)
      this.w_Gsri_Mcu.w_Cdritacc = Cp_Round(iif(Nvl(Mrtotim1,0) < 0,0,Nvl(Mrtotim1,0)),2)
      this.w_Gsri_Mcu.w_Cdsogero = Cp_Round(iif(Alltrim(Nvl(Trcaupre," ")) $ "C-M-M1-V" And Nvl(Mrtotim2,0)-Nvl(Mrimpper,0)>0 ,Nvl(Mrtotim2,0)-Nvl(Mrimpper,0),0),2)
      this.w_Gsri_Mcu.w_Cdpercip = Cp_Round(iif(Alltrim(Nvl(Trcaupre," ")) $ "C-M-M1-V" And Nvl(Mrimpper,0)>=0 ,Nvl(Mrimpper,0),0),2)
      this.w_Gsri_Mcu.w_Cdsperim = Cp_Round(iif(Nvl(Mrsperim,0)>=0 ,Nvl(Mrsperim,0),0),2)
      this.w_Gsri_Mcu.w_Cdritrim = Cp_Round(iif(Alltrim(Nvl(Trcaupre," ")) $ "X-Y" And Nvl(Riterimb,0)<0 ,Abs(Nvl(Riterimb,0)),0),2)
    endif
    this.w_Gsri_Mcu.w_Cdcodice = iif(Nvl(Mrcodsns," ") $ "123" And this.w_ProgrModulo<>0,Mrcodsns," ")
    this.w_Gsri_Mcu.w_Cdsnsogg = iif(this.w_ProgrModulo<>0,Cp_Round(iif(Nvl(Nonsogg,0) < 0,0,Nvl(Nonsogg,0)),2),0)
    this.w_Gsri_Mcu.w_Cdprotec = Alltrim(Cdprotec)
    this.w_Gsri_Mcu.w_Cdprodoc = Cdprodoc
    this.w_Gsri_Mcu.w_Cdpromod = iif(this.oParentObject.w_Cutipcas="A",1,this.w_ProgrModulo)
    if this.oParentObject.w_Cucersin="S"
      this.w_Gsri_Mcu.w_Cdsercer = Cdsercer
      this.w_Cdprocer = Nvl(Cdprocer,0)
    endif
    this.w_Gsri_Mcu.SaveRow()     
    this.w_Gsri_Mcu.bUpdated = .t.
  endproc


  procedure Page_13
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    Vq_Exec("..\Rite\Exe\Query\Gsri_Acu", this, "__TMP__")
    DECLARE ARRFDF (3,2)
    ARRFDF (1,1) = "CODICEFISCALE"
    ARRFDF (1,2) = 16
    ARRFDF (2,1) = "CUCODFED"
    ARRFDF (2,2) = 11
    ARRFDF (3,1) = "CUNUMMOD"
    ARRFDF (3,2) = 2
    if this.w_StampaFront="S"
      this.w_Modello = "MODFCUNICA"
      this.Page_14()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.w_StampaDettaglio="S"
      VQ_EXEC("..\Rite\Exe\Query\Gsri1acu", this, "RecordD")
      if (this.oParentObject.w_Cutipcas="O" And this.oParentObject.w_CuAgrCer="N" And this.oParentObject.w_Cucersin<>"S") Or this.oParentObject.w_Cutipcas="A"
        Select * from RecordD into cursor __Tmp__
        * --- Nel caso di "Annullamento" lancio un diverso PDF
        if this.oParentObject.w_Cutipcas="A"
          this.w_Modello = "MODDCUNICA"
        else
          this.w_Modello = "MODDHCUNICA"
        endif
        this.Page_14()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      else
        Select RecordD
        Go Top
        Scan
        this.w_NumeroRecord = Recno()
        Select * from RecordD into cursor __Tmp__ where this.w_NumeroRecord=Recno()
        this.w_Modello = "MODDCUNICA"
        this.Page_14()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        Use in __TMP__
        Select RecordD
        this.w_Procer = RecordD.Cdprocer
        this.w_AnnoCer = RecordD.AnnoCer
        VQ_EXEC("..\Rite\Exe\Query\Gsri1acu_h", this, "__TMP__")
        this.w_Modello = "MODHCUNICA"
        this.Page_14()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        Use in __TMP__
        Select RecordD
        Goto this.w_NumeroRecord
        Endscan
        Use in RecordD
        Use in __TMP__
      endif
    endif
    if this.w_StampaFront<>"S" And this.w_StampaDettaglio<>"S"
      ah_ErrorMsg("Nessuna selezione effettuata")
    endif
    * --- Drop temporary table ElePerc
    i_nIdx=cp_GetTableDefIdx('ElePerc')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('ElePerc')
    endif
    * --- Effettuiamo questo assegnamento perch� altrimenti i tasti funzione
    *     sarebbe disabilitati
    i_Curform=This.oParentObject
  endproc


  procedure Page_14
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    Select __TMP__
    this.w_Resfdf = cp_ffdf(" ",alltrim(this.w_Modello)+".pdf",this," ",@ARRFDF)
    * --- Se non mi ritorna un numero significa che ho incontrato un errore
    do case
      case Not Left( this.w_Resfdf , 1) $ "0123456789"
        * --- Errore durante creazione di un FDF..
        this.w_Mess = this.w_Resfdf
        ah_ErrorMsg(this.w_Mess,)
      case Left( this.w_RESFDF , 1) = "0"
        * --- Nessun FDF creato...
        this.w_MESS = "Nessun file PDF da generare."
        ah_ErrorMsg(this.w_Mess)
      otherwise
        this.w_Quadro = 1
        do while this.w_Quadro<=Val( this.w_Resfdf )
          Cp_Chprn(tempadhoc()+"\"+alltrim(this.w_Modello)+Alltrim(Str(this.w_Quadro))+".Fdf",.Null.,2)
          * --- Ritardo l'operazione successiva
          if type("g_PDFDELAY")="N"
            wait "" timeout g_PDFDELAY
          else
            wait "" timeout 5
          endif
          this.w_Quadro = this.w_Quadro+1
        enddo
    endcase
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pParame)
    this.pParame=pParame
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParame"
endproc
