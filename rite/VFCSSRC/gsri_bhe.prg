* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_bhe                                                        *
*              Controlli preventivi dati record H ed ST                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2015-06-18                                                      *
* Last revis.: 2015-06-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,HASEVCOP
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsri_bhe",oParentObject,m.HASEVCOP)
return(i_retval)

define class tgsri_bhe as StdBatch
  * --- Local variables
  HASEVCOP = space(10)
  w_GTDATGEN = ctod("  /  /  ")
  w_MESS = space(100)
  * --- WorkFile variables
  GEDETTEL_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.bUpdateParentObject = .F.
    this.oParentObject.w_HASEVENT = .T.
    * --- Select from GSRI_BHE
    do vq_exec with 'GSRI_BHE',this,'_Curs_GSRI_BHE','',.f.,.t.
    if used('_Curs_GSRI_BHE')
      select _Curs_GSRI_BHE
      locate for 1=1
      do while not(eof())
      this.w_MESS = "Il dato estratto � gi� presente nel file telematico comunicato il "
      this.oParentObject.w_HASEVENT = .F.
      this.w_GTDATGEN = _Curs_GSRI_BHE.GTDATGEN
      EXIT
        select _Curs_GSRI_BHE
        continue
      enddo
      use
    endif
    this.w_MESS = AH_MsgFormat(this.w_MESS)
    if Not this.oParentObject.w_HASEVENT
      if Upper(this.HASEVCOP)="ECPEDIT"
        ah_ErrorMsg("Impossibile modificare. %0%1%2", 48,"",this.w_MESS,Dtoc(this.w_Gtdatgen))
      else
        ah_ErrorMsg("Impossibile cancellare. %0%1%2", 48,"",this.w_MESS,Dtoc(this.w_Gtdatgen))
      endif
      i_retcode = 'stop'
      return
    else
      * --- In caso di variazione avverto l'utente di procedere all'aggiornamento del record ST
      if Upper(this.HASEVCOP)="ECPEDIT" And this.oParentObject.w_GDTIPEST="H"
        ah_ErrorMsg("Attenzione: in caso di variazione del record H%0occorre procedere con l'aggiornamento manuale del record ST")
      endif
    endif
    if Upper(this.HASEVCOP)="ECPEDIT"
      this.oParentObject.bUpdated = .f.
    endif
  endproc


  proc Init(oParentObject,HASEVCOP)
    this.HASEVCOP=HASEVCOP
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='GEDETTEL'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_GSRI_BHE')
      use in _Curs_GSRI_BHE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="HASEVCOP"
endproc
