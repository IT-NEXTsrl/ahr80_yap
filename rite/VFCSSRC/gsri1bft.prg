* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri1bft                                                        *
*              Gen. file telematico 770/2003                                   *
*                                                                              *
*      Author: Zucchetti spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][98][VRS_831]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-06-11                                                      *
* Last revis.: 2011-02-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsri1bft",oParentObject)
return(i_retval)

define class tgsri1bft as StdBatch
  * --- Local variables
  w_INDIRIZZOEMAIL = space(100)
  FileTelematico = .NULL.
  w_nMod1 = 0
  w_ContaRec = 0
  CauPre = space(1)
  w_NUMCERTIF = 0
  w_CALCOLI = 0
  w_FLAGEUR = space(1)
  w_nMod2 = 0
  w_CFAzi = space(16)
  w_nRECRITE1 = 0
  w_nRECRITE3 = 0
  w_nMod3 = 0
  w_DataAppo = space(8)
  w_nRECRITE2 = 0
  w_OPERAZ = space(1)
  w_SOSPENDI = .f.
  w_OLDPERCF = space(16)
  w_OLDPER = space(15)
  w_PRIMOPERC = .f.
  w_PERCIPCF = space(16)
  w_MESS = space(100)
  w_DATCOMPL = .f.
  w_MRCODCON = space(15)
  w_ANDESCRI = space(40)
  w_ANNO = space(4)
  w_PERCIN = space(15)
  w_CODVAL = space(3)
  w_DATFIN = ctod("  /  /  ")
  w_DECIMI = space(1)
  w_PERCFIN = space(15)
  w_DATINI = ctod("  /  /  ")
  w_VALUTA = space(1)
  w_NumPerSC = 0
  w_NumPerSE = 0
  w_tSOMTOTASC = 0
  w_tSOMTOTASE = 0
  w_tNONSOGGSC = 0
  w_tIMPONISE = 0
  w_tIMPONISC = 0
  w_tMRTOTIM1SE = 0
  w_tMRTOTIM1SC = 0
  * --- WorkFile variables
  AZIENDA_idx=0
  TRI_BUTI_idx=0
  VEP_RITE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Le dichiarazione del caller sono a Pag.8
    * --- Generazione File Telematico 770
    * --- Controllo che le date di selezione siano all'interno dello stesso anno
    if STR(YEAR(this.oParentObject.w_DATFIN),4,0)<>STR(YEAR(this.oParentObject.w_DATINI),4,0)
      this.w_MESS = "L'intervallo di date deve essere interno ad un anno solare%0"
      ah_ERRORMSG(this.w_MESS,48,"")
      i_retcode = 'stop'
      return
    endif
    * --- Eseguo la CheckForm per verificare i campi obbligatori
    this.oParentObject.bUpdated = .t.
    if !this.oParentObject.CheckForm()
      i_retcode = 'stop'
      return
    endif
    * --- Controllo sui valori obbligatori e bloccanti
    if this.oParentObject.w_PERAZI<>"S"
      if not empty(this.oParentObject.w_SESIGLA)
        if empty(this.oParentObject.w_SEINDIRI2)
          ah_errormsg("Manca indirizzo della sede legale",48,"")
          i_retcode = 'stop'
          return
        endif
      endif
      if not empty(this.oParentObject.w_SERSIGLA) or not empty(this.oParentObject.w_SERINDIR) or not empty(this.oParentObject.w_SERCAP)
        if empty(this.oParentObject.w_SERCOMUN)
          ah_errormsg("Manca comune del domicilio fiscale",48,"")
          i_retcode = 'stop'
          return
        endif
      endif
      if not empty(this.oParentObject.w_SERCOMUN) or not empty(this.oParentObject.w_SERINDIR) or not empty(this.oParentObject.w_SERCAP)
        if empty(this.oParentObject.w_SERSIGLA)
          ah_errormsg("Manca provincia del domicilio fiscale","!")
          i_retcode = 'stop'
          return
        endif
      endif
      if not empty(this.oParentObject.w_SERCOMUN) or not empty(this.oParentObject.w_SERSIGLA) or not empty(this.oParentObject.w_SERCAP)
        if empty(this.oParentObject.w_SERINDIR)
          ah_errormsg("Manca indirizzo del domicilio fiscale","!")
          i_retcode = 'stop'
          return
        endif
      endif
    endif
    if this.oParentObject.w_PERAZI<>"S"
      if empty(this.oParentObject.w_RFCODFIS) OR empty(this.oParentObject.w_RFCOGNOME) OR empty(this.oParentObject.w_RFNOME) OR empty(this.oParentObject.w_RFSESSO) OR empty(this.oParentObject.w_RFDATANASC) OR empty(this.oParentObject.w_RFCOMNAS)
        ah_errormsg("Mancano alcuni dati relativi al rappresentante firmatario della dichiarazione")
        i_retcode = 'stop'
        return
      endif
      if (not empty(this.oParentObject.w_RFSIGLA)) AND (this.oParentObject.w_RFSIGLA<>"EE")
        if empty(this.oParentObject.w_RFCOMUNE) OR empty(this.oParentObject.w_RFINDIRIZ)
          ah_errormsg("Mancano alcuni dati relativi alla residenza del rappresentante firmatario")
          i_retcode = 'stop'
          return
        endif
      endif
    endif
    if this.oParentObject.w_PERAZI="S"
      if not empty(this.oParentObject.w_RESIGLA) and (this.oParentObject.w_RESIGLA<>"EE")
        if empty(this.oParentObject.w_INDIRIZ)
          ah_errormsg("Manca l'indirizzo del domicilio fiscale del dichiarante")
          i_retcode = 'stop'
          return
        endif
      endif
    endif
    * --- Controllo dati relativi all'impegno  presentazione telematica
    this.oParentObject.w_IMPTRSOG = iif(empty(this.oParentObject.w_IMPTRSOG),"0",this.oParentObject.w_IMPTRSOG)
    this.oParentObject.w_IMPTRTEL = iif(empty(this.oParentObject.w_IMPTRTEL),"0",this.oParentObject.w_IMPTRTEL)
    if (this.oParentObject.w_IMPTRTEL<>"0") or (this.oParentObject.w_IMPTRSOG<>"0") or (this.oParentObject.w_FIRMINT<>"0") or not empty(this.oParentObject.w_RFDATIMP) ;
      or not empty(this.oParentObject.w_CODFISIN) or this.oParentObject.w_TIPFORN$("0305")
      if empty(this.oParentObject.w_RFDATIMP) OR empty(this.oParentObject.w_CODFISIN) 
        ah_errormsg("Mancano dati relativi al CAF o alla data impegno trasmissione")
        i_retcode = 'stop'
        return
      endif
      if ((this.oParentObject.w_IMPTRTEL="0") and (this.oParentObject.w_IMPTRSOG="0")) or ((this.oParentObject.w_IMPTRTEL="1") and (this.oParentObject.w_IMPTRSOG="1"))
        ah_errormsg("Manca impegno a trasmettere o non sono state selezionate modalit� alternative")
        i_retcode = 'stop'
        return
      endif
    endif
    * --- Controllo formato numerico dei numeri telefonici
    if not empty(this.oParentObject.w_TELEFONO)
      for lettera = 65 to 90
      if chr(lettera) $ this.oParentObject.w_TELEFONO
        ah_errormsg("Numero di telefono relativo alla persona fisica errato")
        i_retcode = 'stop'
        return
      endif
      endfor
    endif
    if not empty(this.oParentObject.w_SETELEFONO)
      for lettera = 65 to 90
      if chr(lettera) $ this.oParentObject.w_SETELEFONO
        ah_errormsg("Numero di telefono relativo ad altri soggetti errato")
        i_retcode = 'stop'
        return
      endif
      endfor
    endif
    if not empty(this.oParentObject.w_RFTELEFONO)
      for lettera = 65 to 90
      if chr(lettera) $ this.oParentObject.w_RFTELEFONO
        ah_errormsg("Numero di telefono relativo al rappresentante firmatario errato")
        i_retcode = 'stop'
        return
      endif
      endfor
    endif
    * --- Fine controllo valori obbligatori e bloccanti
    * --- Questo giro lo faccio per ingannare i Batches standard che lancio successivamente
    this.w_ANNO = this.oParentObject.w_ANNO
    this.w_PERCIN = this.oParentObject.w_PERCIN
    this.w_CODVAL = this.oParentObject.w_CODVAL
    this.w_DATFIN = this.oParentObject.w_DATFIN
    this.w_DECIMI = this.oParentObject.w_DECIMI
    this.w_PERCFIN = this.oParentObject.w_PERCFIN
    this.w_DATINI = this.oParentObject.w_DATINI
    this.w_VALUTA = this.oParentObject.w_VALUTA
    * --- Codice Fiscale dell'Azienda Corrente
    this.w_CFAzi = this.oParentObject.w_FOCODFIS
    this.w_OPERAZ = this.oParentObject.w_TIPOPERAZ
    this.w_FLAGEUR = this.oParentObject.w_FLAGEURO
    this.w_SOSPENDI = .F.
    ah_msg( "Preparazione cursori in corso" )
    * --- Lancio i batches standard che creano i cursori
    do GSRI_BST with this
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    do GSRI_BSC with this
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Ex chiamata a GSRI_BSE
    * --- Controllo se sono state abbinate ritenute con condizioni diverse
    if this.w_SOSPENDI
      i_retcode = 'stop'
      return
    endif
    * --- Conto i modelli totali della dichiarazione
    if used("RITE1")
      select RITE1
      this.w_nMod1 = iif(mod(reccount(),5)=0,int(reccount()/5),int(reccount()/5)+1)
      Select MRCODCON from RITE1 Group By MRCODCON into cursor TEMP
      Select TEMP
      this.w_nRECRITE1 = reccount()
      Select COUNT(*) As RIGHE, COUNT(*) / 5 As REGIST from RITE1 Group By MRCODCON having RIGHE > 5 into cursor TEMP
      if reccount() > 0
        Select TEMP 
 Go Top 
 Scan
        if REGIST - int(REGIST) <>0
          do case
            case Int(REGIST) = 1
              this.w_nRECRITE1 = this.w_nRECRITE1 + 1
            case Int(REGIST) = 2
              this.w_nRECRITE1 = this.w_nRECRITE1 + 2
            case Int(REGIST) = 3
              this.w_nRECRITE1 = this.w_nRECRITE1 + 3
            case Int(REGIST) = 4
              this.w_nRECRITE1 = this.w_nRECRITE1 + 4
            case Int(REGIST) = 5
              this.w_nRECRITE1 = this.w_nRECRITE1 + 5
            case Int(REGIST) = 6
              this.w_nRECRITE1 = this.w_nRECRITE1 + 6
          endcase
        endif
        ENDSCAN
      endif
      if used("TEMP")
        Select TEMP
        use
      endif
    endif
    if used("RITE2")
      select RITE2
      this.w_nMod2 = iif(mod(reccount(),10)=0,int(reccount()/10),int(reccount()/10)+1)
      this.w_nRECRITE2 = reccount()
    endif
    if used("RITE3")
      select RITE3
      this.w_nMod3 = iif(mod(reccount(),13)=0,int(reccount()/13),int(reccount()/13)+1)
      this.w_nRECRITE3 = reccount()
    endif
    * --- Calcolo numero di certificazioni di lavoro autonomo
    this.w_NUMCERTIF = this.w_nRECRITE1
    this.oParentObject.w_NUMCERTIF2 = this.w_NUMCERTIF
    if (this.w_nRECRITE1=0) and (this.w_nRECRITE3=0)
      i_retcode = 'stop'
      return
    endif
    * --- Creo loggetto
    this.FileTelematico = CreateObject("FileTelematico",alltrim(this.oParentObject.w_DirName)+alltrim(this.oParentObject.w_FileName),this)
    * --- Valorizzo il numero totale di modelli
    this.FileTelematico.nModelli = Max(this.w_nMod1,this.w_nMod2,this.w_nMod3)
    * --- Riga di testata
    ah_msg( 'Fase 2: record tipo "A"' )
    this.Pag7()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Record di Tipo "B"
    ah_msg( 'Fase 2: record tipo "B"' )
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    ah_msg( 'Fase 2: record tipo "E" ed "H"' )
    * --- Record di Tipo "E" ed "H"
    create cursor LISTAPERC (CODCON C(15),DENOMI C(40),CODFIS C(1),COGDEN C(1),NOME C(1),SESSO C(1), ; 
 DATNAS C(1),COMNAS C(1),COMUN C(1))
    if used("RITE1") and used("RITE3")
      do while !eof("RITE3") and (this.oParentObject.w_QST1="1" or this.oParentObject.w_QST12="1")
        this.Pag6()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.FileTelematico.ScriviRecordE()     
        * --- Incremento il numero di modelli
        this.FileTelematico.nModelloE = this.FileTelematico.nModelloE + 1
      enddo
      this.w_DATCOMPL = .T.
      do while !eof("RITE1")
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.FileTelematico.ScriviRecordH()     
        * --- Incremento il numero di modelli
        this.FileTelematico.nModelloH = this.FileTelematico.nModelloH + 1
      enddo
      if ! this.w_DATCOMPL
        this.w_MESS = "Dati anagrafici percipienti mancanti%0Impossibile generare il file 770/2003%0Vuoi visualizzare la stampa di verifica?"
        if ah_yesno(this.w_MESS)
          Select CODCON,DENOMI,MAX(CODFIS) as CODFIS,MAX(COGDEN) as COGDEN,MAX(NOME) as NOME, ; 
 MAX(SESSO) as SESSO, MAX(DATNAS) as DATNAS,MAX(COMNAS) as COMNAS,MAX(COMUN) as COMUN ; 
 from LISTAPERC into cursor __TMP__ group by CODCON,DENOMI order by CODCON
          CP_CHPRN("..\RITE\EXE\QUERY\GSRI1BFT.FRX","",this.oParentObject.oParentObject)
          this.FileTelematico.CancellaFile()     
          i_retcode = 'stop'
          return
        else
          this.FileTelematico.CancellaFile()     
          i_retcode = 'stop'
          return
        endif
      endif
    endif
    * --- Riga di chiusura
    ah_msg( 'Fase 2: record tipo "Z"' )
    this.FileTelematico.RecordZ()     
    if used("RITE1")
      select RITE1
      use
    endif
    if used("RITE2")
      select RITE2
      use
    endif
    if used("RITE3")
      select RITE3
      use
    endif
    if used("LISTAPERC")
      select LISTAPERC
      use
    endif
    wait clear
    ah_errormsg("Il file '%1%2' � stato generato",48,"",alltrim(this.oParentObject.w_DirName), alltrim(this.oParentObject.w_FileName))
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Record B - Frontespizio
    this.FileTelematico.RecordB("B")     
    this.FileTelematico.RecordB(this.w_CFAzi)     
    this.FileTelematico.RecordB(right("00000000" + alltrim(str(this.FileTelematico.nModello)),8))     
    this.FileTelematico.RecordB(space(3))     
    this.FileTelematico.RecordB(space(25))     
    this.FileTelematico.RecordB(space(20))     
    this.FileTelematico.RecordB("05006900962     ")     
    * --- Campo 8 - Flag conferma
    this.FileTelematico.RecordB(left(alltrim(this.oParentObject.w_FLAGCONF)+"0",1))     
    * --- Campo 9 - Flag Dichiarazione Corretiva
    this.FileTelematico.RecordB(left(alltrim(this.oParentObject.w_FLAGCOR)+"0",1))     
    * --- Campo 10 - Flag Dichiarazione Integrativa
    this.FileTelematico.RecordB(left(alltrim(this.oParentObject.w_FLAGINT)+"0",1))     
    * --- Campi da 11 a 21 Filler Numerici
    this.FileTelematico.RecordB(repl("0",11))     
    * --- Campo 22 - Flag eventi eccezionali
    this.FileTelematico.RecordB(this.oParentObject.w_FLAGEVE)     
    this.FileTelematico.RecordB(space(7))     
    * --- Dati del frontespizio
    * --- Dati del Contribuente
    this.FileTelematico.RecordB(this.oParentObject.w_FOCOGNOME)     
    this.FileTelematico.RecordB(this.oParentObject.w_FONOME)     
    this.FileTelematico.RecordB(this.oParentObject.w_FODENOMINA)     
    * --- Campi dal 28 al 31 - Filler Numerico
    this.FileTelematico.RecordB(repl("0",14))     
    this.FileTelematico.RecordB(iif(this.oParentObject.w_PERAZI="S",this.oParentObject.w_CODATT,this.oParentObject.w_SECODATT))     
    this.FileTelematico.RecordB(iif(this.oParentObject.w_PERAZI="S",this.oParentObject.w_TELEFONO,this.oParentObject.w_SETELEFONO))     
    * --- fax non gestito
    this.FileTelematico.RecordB(space(12))     
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZ_EMAIL"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZ_EMAIL;
        from (i_cTable) where;
            AZCODAZI = this.oParentObject.w_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_INDIRIZZOEMAIL = NVL(cp_ToDate(_read_.AZ_EMAIL),cp_NullValue(_read_.AZ_EMAIL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.FileTelematico.RecordB(upper(left(this.w_INDIRIZZOEMAIL+space(100),100)))     
    this.FileTelematico.RecordB("0")     
    this.FileTelematico.RecordB(space(9))     
    * --- Persona fisica
    this.FileTelematico.RecordB(this.oParentObject.w_COMUNE)     
    this.FileTelematico.RecordB(this.oParentObject.w_SIGLA)     
    this.w_DataAppo = right("00"+alltrim(str(day(this.oParentObject.w_DATANASC))),2)+right("00"+alltrim(str(month(this.oParentObject.w_DATANASC))),2)+str(year(this.oParentObject.w_DATANASC),4,0)
    this.FileTelematico.RecordB(iif(!empty(this.oParentObject.w_DATANASC),this.w_DataAppo,repl("0",8)))     
    this.FileTelematico.RecordB(this.oParentObject.w_SESSO)     
    this.FileTelematico.RecordB("00000")     
    this.FileTelematico.RecordB(this.oParentObject.w_RECOMUNE)     
    this.FileTelematico.RecordB(this.oParentObject.w_RESIGLA)     
    this.FileTelematico.RecordB(this.oParentObject.w_INDIRIZ)     
    this.FileTelematico.RecordB(iif(empty(this.oParentObject.w_CAP),"00000",Left(this.oParentObject.w_CAP,5)))     
    this.FileTelematico.RecordB(space(4))     
    this.w_DataAppo = right("00"+alltrim(str(day(this.oParentObject.w_VARRES))),2)+right("00"+alltrim(str(month(this.oParentObject.w_VARRES))),2)+str(year(this.oParentObject.w_VARRES),4,0)
    this.FileTelematico.RecordB(iif(!empty(this.oParentObject.w_VARRES),this.w_DataAppo,repl("0",8)))     
    * --- Filler campi 51 - 52
    this.FileTelematico.RecordB("00")     
    * --- Filler campi 53 - 59
    this.FileTelematico.RecordB(space(106))     
    * --- Filler campo  60
    this.FileTelematico.RecordB("000")     
    * --- Filler campi 61 - 63
    this.FileTelematico.RecordB(space(83))     
    * --- Filler campi 64 - 66
    this.FileTelematico.RecordB("000")     
    * --- Filler campo 67
    this.FileTelematico.RecordB(space(9))     
    * --- -----------------------------------------------------------------------------------------------------------------
    *     Altri soggetti
    this.w_DataAppo = right("00"+alltrim(str(month(this.oParentObject.w_SEVARSED))),2)+str(year(this.oParentObject.w_SEVARSED),4,0)
    this.FileTelematico.RecordB(iif(!empty(this.oParentObject.w_SEVARSED),this.w_DataAppo,repl("0",6)))     
    this.FileTelematico.RecordB(this.oParentObject.w_SECOMUNE)     
    this.FileTelematico.RecordB(this.oParentObject.w_SESIGLA)     
    this.FileTelematico.RecordB(this.oParentObject.w_SEINDIRI2)     
    this.FileTelematico.RecordB(iif(empty(this.oParentObject.w_SECAP),"00000",Left(this.oParentObject.w_SECAP,5)))     
    this.w_DataAppo = right("00"+alltrim(str(month(this.oParentObject.w_SEVARDOM))),2)+str(year(this.oParentObject.w_SEVARDOM),4,0)
    this.FileTelematico.RecordB(iif(!empty(this.oParentObject.w_SEVARDOM),this.w_DataAppo,repl("0",6)))     
    this.FileTelematico.RecordB(this.oParentObject.w_SERCOMUN)     
    this.FileTelematico.RecordB(this.oParentObject.w_SERSIGLA)     
    this.FileTelematico.RecordB(this.oParentObject.w_SERINDIR)     
    this.FileTelematico.RecordB(iif(empty(this.oParentObject.w_SERCAP),"00000",Left(this.oParentObject.w_SERCAP,5)))     
    * --- Campi 78 - 81
    this.FileTelematico.RecordB(repl("0",32))     
    this.FileTelematico.RecordB(iif(!empty(this.oParentObject.w_STATO),this.oParentObject.w_STATO,"0"))     
    this.FileTelematico.RecordB(iif(!empty(this.oParentObject.w_NATGIU),RIGHT("00"+ALLTRIM(this.oParentObject.w_NATGIU),2),"00"))     
    this.FileTelematico.RecordB(iif(!empty(this.oParentObject.w_SITUAZ),this.oParentObject.w_SITUAZ,"0"))     
    * --- Campo  85
    this.FileTelematico.RecordB("0")     
    * --- Campo  86
    this.FileTelematico.RecordB(space(24))     
    * --- Campo 87
    this.FileTelematico.RecordB("000")     
    * --- Campo  88 - 89
    this.FileTelematico.RecordB(space(28))     
    * --- Campi  90 - 93
    this.FileTelematico.RecordB(repl("0",19))     
    this.FileTelematico.RecordB(iif(empty(this.oParentObject.w_CODFISDA),"00000000000",this.oParentObject.w_CODFISDA))     
    * --- Campo 94
    this.FileTelematico.RecordB(space(18))     
    * --- Campo 95 - 97
    this.FileTelematico.RecordB(repl("0",30))     
    * --- -------------------------------------------------------------------------------------------------------------------
    *     Dati relativi al rappresentante firmatario della dichiarazione
    *     Riservato a chi presenta la dichiarazione per altri
    this.FileTelematico.RecordB(this.oParentObject.w_RFCODFIS)     
    this.FileTelematico.RecordB(repl("0",12))     
    this.FileTelematico.RecordB(right("00"+alltrim(this.oParentObject.w_RFCODCAR),2))     
    this.FileTelematico.RecordB(repl("0",9))     
    this.FileTelematico.RecordB(this.oParentObject.w_RFCOGNOME)     
    this.FileTelematico.RecordB(this.oParentObject.w_RFNOME)     
    this.FileTelematico.RecordB(Left ( this.oParentObject.w_RFSESSO+Space(1) ,1 ))     
    this.w_DataAppo = right("00"+alltrim(str(day(this.oParentObject.w_RFDATANASC))),2)+right("00"+alltrim(str(month(this.oParentObject.w_RFDATANASC))),2)+str(year(this.oParentObject.w_RFDATANASC),4,0)
    this.FileTelematico.RecordB(iif(!empty(this.oParentObject.w_RFDATANASC),this.w_DataAppo,repl("0",8)))     
    this.FileTelematico.RecordB(this.oParentObject.w_RFCOMNAS)     
    this.FileTelematico.RecordB(this.oParentObject.w_RFSIGNAS)     
    this.FileTelematico.RecordB(this.oParentObject.w_RFCOMUNE)     
    this.FileTelematico.RecordB(this.oParentObject.w_RFSIGLA)     
    this.FileTelematico.RecordB(iif(empty(this.oParentObject.w_RFCAP),"00000",Left(this.oParentObject.w_RFCAP,5)))     
    this.FileTelematico.RecordB(this.oParentObject.w_RFINDIRIZ)     
    this.FileTelematico.RecordB(this.oParentObject.w_RFTELEFONO)     
    * --- Filler campi 115 - 118
    this.FileTelematico.RecordB(repl("0",18))     
    * --- Filler campo 119
    this.FileTelematico.RecordB(space(9))     
    * --- --------------------------------------------------------------------------------------------------------------
    *     Firma della dichiarazione
    * --- Filler campi 120 - 159
    this.FileTelematico.RecordB(repl("0",41))     
    * --- --------------------------------------------------------------------------------------------------------------
    *     Sostituti
    * --- Filler campi 160 - 172
    this.FileTelematico.RecordB(repl("0",13))     
    * --- --------------------------------------------------------------------------------------------------------------
    *     Sezione I Trasmissione modello 770 Semplificato
    this.FileTelematico.RecordB(repl("0", 8))     
    this.FileTelematico.RecordB(right("00000000" + iif(this.oParentObject.w_SEZ1="0","",alltrim(str(this.w_NUMCERTIF))), 8))     
    this.FileTelematico.RecordB(left(alltrim(this.oParentObject.w_qST1)+"0",1))     
    this.FileTelematico.RecordB(left(alltrim(this.oParentObject.w_qSX1)+"0",1))     
    * --- Campo 177
    this.FileTelematico.RecordB(left(alltrim(this.oParentObject.w_FIRMPRES)+"0",1))     
    * --- Campo 178
    this.FileTelematico.RecordB(left(alltrim(this.oParentObject.w_FIRMDICH)+"0",1))     
    * --- --------------------------------------------------------------------------------------------------------------
    *     Sezione II Trasmissione modello 770 Semplificato in due parti
    this.FileTelematico.RecordB(repl("0", 8))     
    this.FileTelematico.RecordB(left(alltrim(this.oParentObject.w_qST12)+"0",1))     
    this.FileTelematico.RecordB(left(alltrim(this.oParentObject.w_qSX12)+"0",1))     
    this.FileTelematico.RecordB(right("00000000" + iif(this.oParentObject.w_SEZ2="0","",alltrim(str(this.w_NUMCERTIF))), 8))     
    * --- Campo 183 - Codice Fiscale Intermediario che presenta la parte restante della dichiarazione
    this.FileTelematico.RecordB(left(alltrim(this.oParentObject.w_RFCFINS2)+space(16),16))     
    * --- Campo 184
    this.FileTelematico.RecordB(left(alltrim(this.oParentObject.w_FIRMPRES2)+"0",1))     
    * --- Campo 185
    this.FileTelematico.RecordB(left(alltrim(this.oParentObject.w_FIRMDICH2)+"0",1))     
    * --- Campo 186 - 200
    this.FileTelematico.RecordB(repl("0",17))     
    * --- ----------------------------------------------------------------------------------------------------------------
    *     Impegno alla presentazione telematica
    this.FileTelematico.RecordB(this.oParentObject.w_CODFISIN)     
    * --- Campo 202
    this.FileTelematico.RecordB(right("00000"+iif(this.oParentObject.w_NUMCAF<>0,ALLTRIM(str(this.oParentObject.w_NUMCAF,5,0)),"00000"),5))     
    * --- Campo 203
    this.FileTelematico.RecordB(left(alltrim(this.oParentObject.w_IMPTRTEL)+"0",1))     
    * --- Campo 204
    this.FileTelematico.RecordB(left(alltrim(this.oParentObject.w_IMPTRSOG)+"0",1))     
    * --- Campo 205
    this.w_DataAppo = right("00"+alltrim(str(day(this.oParentObject.w_RFDATIMP))),2)+right("00"+alltrim(str(month(this.oParentObject.w_RFDATIMP))),2)+right(STR(year(this.oParentObject.w_RFDATIMP),4,0),4)
    this.FileTelematico.RecordB(iif(!empty(this.w_DataAppo) and val(this.w_DataAppo)<>0,this.w_DataAppo,repl("0",8)))     
    * --- Campo 206
    this.FileTelematico.RecordB(this.oParentObject.w_FIRMINT)     
    * --- Campo 207
    this.FileTelematico.RecordB(space(4))     
    * --- ------------------------------------------------------------------------------------------------------------
    *     Visto di conformit�
    * --- Campo 208
    this.FileTelematico.RecordB(this.oParentObject.w_CFRESCAF)     
    this.FileTelematico.RecordB(this.oParentObject.w_VISTOA35)     
    this.FileTelematico.RecordB(this.oParentObject.w_FLAGFIR)     
    * --- Campo Filler 211 - 213
    this.FileTelematico.RecordB(space(37))     
    * --- Campo Filler 214 - 215
    this.FileTelematico.RecordB(repl("0",2))     
    * --- Campo Filler 216
    this.FileTelematico.RecordB(space(5))     
    * --- -----------------------------------------------------------------------------------------------------------------
    *     Dichiarazione Correttiva nei termini e integrativa "parziale"
    this.FileTelematico.RecordB(RIGHT("00000000000000000"+alltrim(STR(this.oParentObject.w_IDESTA)),17))     
    this.FileTelematico.RecordB(RIGHT("000000"+alltrim(STR(this.oParentObject.w_PROIFT)),6))     
    * --- --------------------------------------------------------------------------------------------------------------------------------------------------------
    *     Dati relativi alle dichiarazioni correttive o integrative "parziali" e alle dichiarazioni su pi� invii
    if this.oParentObject.w_FLAGCOR= "1" or this.oParentObject.w_FLAGINT = "1"
      do case
        case this.oParentObject.w_SEZ1 = "1"
          * --- Campo 219
          this.FileTelematico.RecordB(left(alltrim(this.oParentObject.w_qST1)+"0",1))     
          * --- Campo 220
          this.FileTelematico.RecordB(left(alltrim(this.oParentObject.w_qSX1)+"0",1))     
          * --- Campo 221
          this.FileTelematico.RecordB(Repl("0",8))     
          * --- Campo 222
          this.FileTelematico.RecordB(RIGHT("00000000"+alltrim(STR(this.w_NUMCERTIF,8,0)),8))     
        case this.oParentObject.w_SEZ2 = "1"
          * --- Campo 219
          this.FileTelematico.RecordB(left(alltrim(this.oParentObject.w_qST12)+"0",1))     
          * --- Campo 220
          this.FileTelematico.RecordB(left(alltrim(this.oParentObject.w_qSX12)+"0",1))     
          * --- Campo 221
          this.FileTelematico.RecordB(Repl("0",8))     
          * --- Campo 222
          this.FileTelematico.RecordB(RIGHT("00000000"+alltrim(STR(this.w_NUMCERTIF,8,0)),8))     
      endcase
    else
      this.FileTelematico.RecordB(Repl("0",18))     
    endif
    * --- ----------------------------------------------------------------------------------------------------------------------------
    *     Spazio non utilizzato
    * --- Campi 223 - 225
    this.FileTelematico.RecordB(space(271))     
    * --- ----------------------------------------------------------------------------------------------------------------------------
    *     Sezione Riservata alle Banche e Poste
    * --- Campi 226 - 235
    this.FileTelematico.RecordB(repl("0",15))     
    this.FileTelematico.RecordB(" ")     
    this.FileTelematico.RecordB(repl("0",5))     
    this.FileTelematico.RecordB(space(13))     
    * --- ----------------------------------------------------------------------------------------------------------------------------
    *     Ultimi tre caratteri di controllo
    this.FileTelematico.RecordB("A")     
    * --- Scrittura del recordB
    this.FileTelematico.RecordB("",.t.,.t.)     
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_PRIMOPERC = .T.
    this.w_OLDPERCF = "*"
    this.w_OLDPER = ""
    * --- Record H
    select RITE1
    do while !eof()
      select RITE1
      if this.w_OLDPERCF<>ANCODFIS OR this.w_OLDPER<>MRCODCON
        if NOT(this.w_PRIMOPERC)
          * --- Nuovo record per variazione percipiente --- Record 'H'
          * --- Incremento il numero di Modello
          this.FileTelematico.nRigoAU = 1
          this.FileTelematico.nModAU = this.FileTelematico.nModAU + 1
          exit
        endif
        this.w_MRCODCON = NVL(MRCODCON,"")
        this.w_ANDESCRI = NVL(ANDESCRI,"")
        if EMPTY(nvl(ANCODFIS,""))
          this.w_DATCOMPL = .F.
          INSERT INTO LISTAPERC VALUES (this.w_MRCODCON,this.w_ANDESCRI,"1","0","0","0","0","0","0")
          select RITE1
        endif
        if iif(ANPERFIS="S",EMPTY(nvl(ANCOGNOM,"")),EMPTY(nvl(ANDESCRI,"")))
          this.w_DATCOMPL = .F.
          INSERT INTO LISTAPERC VALUES (this.w_MRCODCON,this.w_ANDESCRI,"0","1","0","0","0","0","0")
          select RITE1
        endif
        if EMPTY(nvl(ANLOCALI,""))
          this.w_DATCOMPL = .F.
          INSERT INTO LISTAPERC VALUES (this.w_MRCODCON,this.w_ANDESCRI,"0","0","0","0","0","0","1")
          select RITE1
        endif
        if EMPTY(nvl(AN__NOME,"")) and ANPERFIS="S"
          this.w_DATCOMPL = .F.
          INSERT INTO LISTAPERC VALUES (this.w_MRCODCON,this.w_ANDESCRI,"0","0","1","0","0","0","0")
          select RITE1
        endif
        if EMPTY(nvl(AN_SESSO,"")) and ANPERFIS="S"
          this.w_DATCOMPL = .F.
          INSERT INTO LISTAPERC VALUES (this.w_MRCODCON,this.w_ANDESCRI,"0","0","0","1","0","0","0")
          select RITE1
        endif
        if EMPTY(nvl(ANDATNAS,"")) and ANPERFIS="S"
          this.w_DATCOMPL = .F.
          INSERT INTO LISTAPERC VALUES (this.w_MRCODCON,this.w_ANDESCRI,"0","0","0","0","1","0","0")
          select RITE1
        endif
        if EMPTY(nvl(ANLOCNAS,"")) and ANPERFIS="S"
          this.w_DATCOMPL = .F.
          INSERT INTO LISTAPERC VALUES (this.w_MRCODCON,this.w_ANDESCRI,"","0","0","0","0","1","0")
          select RITE1
        endif
        this.FileTelematico.RecordH("AU","001",ANCODFIS)     
        * --- Posizione 30 record posizionale
        this.w_PERCIPCF = ANCODFIS
        this.FileTelematico.RecordH("AU","002",iif(ANPERFIS="S",upper(NVL(ANCOGNOM,"")),upper(NVL(ANDESCRI,""))))     
        this.FileTelematico.RecordH("AU","003",iif(ANPERFIS="S",upper(NVL(AN__NOME,"")),""))     
        this.FileTelematico.RecordH("AU","004",iif(ANPERFIS="S",upper(NVL(AN_SESSO,"")),""))     
        this.FileTelematico.RecordH("AU","005",iif(ANPERFIS="S",NVL(ANDATNAS,Cp_CharToDate("  -  -  ")),""))     
        this.FileTelematico.RecordH("AU","006",iif(ANPERFIS="S",upper(NVL(ANLOCNAS,"")),""))     
        this.FileTelematico.RecordH("AU","007",iif(ANPERFIS="S",upper(NVL(ANPRONAS,"")),""))     
        this.FileTelematico.RecordH("AU","008",upper(NVL(ANLOCALI,"")))     
        this.FileTelematico.RecordH("AU","009",upper(NVL(ANPROVIN,"")))     
        this.FileTelematico.RecordH("AU","011",upper(NVL(ANINDIRI,"")))     
        this.w_OLDPERCF = NVL(ANCODFIS,"")
        this.w_OLDPER = NVL(MRCODCON,"")
        this.w_PRIMOPERC = .F.
      endif
      * --- Riservato ai percipienti esteri
      * --- Dati relativi alle somme erogate
      this.FileTelematico.RecordH("AU","016",upper(NVL(TRCAUPRE,"")))     
      this.FileTelematico.RecordH("AU","017","            "+alltrim(str(year(this.oParentObject.w_DATFIN))))     
      this.FileTelematico.RecordH("AU","019",NVL(SOMTOTA,0))     
      this.FileTelematico.RecordH("AU","021",NVL(NONSOGG,0))     
      this.FileTelematico.RecordH("AU","022",NVL(IMPONI,0))     
      this.FileTelematico.RecordH("AU","023",NVL(MRTOTIM1,0))     
      * --- Incremento il numero di Rigo
      this.FileTelematico.nRigoAU = this.FileTelematico.nRigoAU + 1
      if this.FileTelematico.nRigoAU > 5
        * --- Incremento il numero di Modello
        this.FileTelematico.nRigoAU = 1
        this.FileTelematico.nModAU = this.FileTelematico.nModAU + 1
        skip in RITE1
        exit
      endif
      skip in RITE1
    enddo
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Record E - Quadro SE
    select RITE2
    do while !eof()
      select RITE2
      this.FileTelematico.RecordE("SE","001",ANCODFIS)     
      this.FileTelematico.RecordE("SE","002",iif(ANPERFIS="S",ANCOGNOM,ANDESCRI))     
      this.FileTelematico.RecordE("SE","003",iif(ANPERFIS="S",AN__NOME,""))     
      this.FileTelematico.RecordE("SE","004",iif(ANPERFIS="S",AN_SESSO,""))     
      this.FileTelematico.RecordE("SE","005",iif(ANPERFIS="S",ANDATNAS,""))     
      this.FileTelematico.RecordE("SE","006",iif(ANPERFIS="S",ANLOCNAS,""))     
      this.FileTelematico.RecordE("SE","007",iif(ANPERFIS="S",ANPRONAS,""))     
      this.FileTelematico.RecordE("SE","008",ANLOCALI)     
      this.FileTelematico.RecordE("SE","009",ANPROVIN)     
      this.FileTelematico.RecordE("SE","010",ANINDIRI)     
      * --- Incremento il numero di Rigo
      this.FileTelematico.nRigoSE = this.FileTelematico.nRigoSE + 1
      if this.FileTelematico.nRigoSE > 10
        * --- Incremento il numero di Modello
        this.FileTelematico.nRigoSE = 2
        this.FileTelematico.nModSE = this.FileTelematico.nModSE + 1
        exit
      endif
      skip in RITE2
    enddo
  endproc


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Record E - Quadro SS
    * --- Quadro SS (SC)
    select count(*) from RITE1 group by ANCODFIS into cursor _quadross_
    this.w_NumPerSC = _tally
    select sum(SOMTOTA) as tSOMTOTA,sum(NONSOGG) as tNONSOGG,sum(IMPONI) as tIMPONI,sum(MRTOTIM1) as tMRTOTIM1 ;
    from RITE1 into cursor _quadross_
    this.w_tSOMTOTASC = _quadross_.tSOMTOTA
    this.w_tNONSOGGSC = _quadross_.tNONSOGG
    this.w_tIMPONISC = _quadross_.tIMPONI
    this.w_tMRTOTIM1SC = _quadross_.tMRTOTIM1
    * --- Quadro SS (SE)
    select count(*) from RITE2 group by ANCODFIS into cursor _quadross_
    this.w_NumPerSE = _tally
    select sum(SOMTOTA) as tSOMTOTA,sum(IMPONI) as tIMPONI,sum(MRTOTIM1) as tMRTOTIM1 from RITE2 into cursor _quadross_
    this.w_tSOMTOTASE = _quadross_.tSOMTOTA
    this.w_tIMPONISE = _quadross_.tIMPONI
    this.w_tMRTOTIM1SE = _quadross_.tMRTOTIM1
    if used("_quadross_")
      select _quadross_
      use
    endif
    this.FileTelematico.RecordE("SS003","001",this.w_NumPerSC,.t.)     
    this.FileTelematico.RecordE("SS003","002",this.w_tSOMTOTASC,.t.)     
    this.FileTelematico.RecordE("SS003","003",this.w_tNONSOGGSC,.t.)     
    this.FileTelematico.RecordE("SS003","005",this.w_tIMPONISC,.t.)     
    this.FileTelematico.RecordE("SS003","006",this.w_tMRTOTIM1SC,.t.)     
    this.FileTelematico.RecordE("SS005","001",this.w_NumPerSE,.t.)     
    this.FileTelematico.RecordE("SS005","002",this.w_tSOMTOTASE,.t.)     
    this.FileTelematico.RecordE("SS005","005",this.w_tIMPONISE,.t.)     
    this.FileTelematico.RecordE("SS005","006",this.w_tMRTOTIM1SE,.t.)     
  endproc


  procedure Pag6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Record E - Quadro ST
    select RITE3
    do while !eof()
      select RITE3
      this.FileTelematico.RecordE("ST","001",iif("A"$VP__NOTE OR "B"$VP__NOTE,"          "+"122002","          "+right("00"+alltrim(str(MESE)),2)+str(ANNO,4,0)))     
      this.w_CALCOLI = SOMTOTA-VPRITECC-VPRITCOM
      if SOMTOTA < 0 or VPRITECC < 0 or VPRITCOM < 0 or this.w_CALCOLI < 0 or VPIMPINT < 0
        this.w_MESS = "Attenzione: presenza di importi negativi nel prospetto ST%0Impossibile generare il file"
        ah_ERRORMSG(this.w_MESS,0)
        this.FileTelematico.CancellaFile()     
        i_retcode = 'stop'
        return
      endif
      this.FileTelematico.RecordE("ST","002",SOMTOTA)     
      this.FileTelematico.RecordE("ST","003",VPRITECC)     
      this.FileTelematico.RecordE("ST","004",VPRITCOM)     
      this.FileTelematico.RecordE("ST","005",this.w_CALCOLI)     
      this.FileTelematico.RecordE("ST","006",VPIMPINT)     
      this.FileTelematico.RecordE("ST","007",VP__NOTE)     
      this.FileTelematico.RecordE("ST","008",VPEVEECC)     
      this.FileTelematico.RecordE("ST","009",MRCODTRI)     
      if VPTIPVER = "T"
        this.FileTelematico.RecordE("ST","010","               1")     
      endif
      this.FileTelematico.RecordE("ST","011",iif(VPCODREG<>0,"              "+right("00"+iif(vpcodreg>9,STR(VPCODREG,2,0),str(vpcodreg,1,0)),2),""))     
      * --- Campo 12
      * --- Incremento il numero di Rigo
      this.FileTelematico.nRigoST = this.FileTelematico.nRigoST + 1
      if this.FileTelematico.nRigoST > 13
        * --- Incremento il numero di Modello
        this.FileTelematico.nRigoST = 2
        this.FileTelematico.nModST = this.FileTelematico.nModST + 1
        skip in RITE3
        exit
      endif
      skip in RITE3
    enddo
  endproc


  procedure Pag7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Record A - Frontespizio
    this.FileTelematico.RecordA("A")     
    this.FileTelematico.RecordA(space(14))     
    this.FileTelematico.RecordA("77S03")     
    this.FileTelematico.RecordA(this.oParentObject.w_TIPFORN)     
    this.FileTelematico.RecordA(iif(empty(this.oParentObject.w_CODFISIN),this.w_CFAzi,this.oParentObject.w_CODFISIN))     
    * --- Dati riservati al fornitore persona fisica
    this.FileTelematico.RecordA(iif(this.oParentObject.w_TIPFORN="01" OR this.oParentObject.w_TIPFORN="02",this.oParentObject.w_FOCOGNOME,SPACE(24)))     
    this.FileTelematico.RecordA(iif(this.oParentObject.w_TIPFORN="01" OR this.oParentObject.w_TIPFORN="02",this.oParentObject.w_FONOME,SPACE(20)))     
    this.FileTelematico.RecordA(iif(empty(this.oParentObject.w_FOSESSO),space(1),this.oParentObject.w_FOSESSO))     
    this.w_DataAppo = right("00"+alltrim(str(day(this.oParentObject.w_FODATANASC))),2)+right("00"+alltrim(str(month(this.oParentObject.w_FODATANASC))),2)+str(year(this.oParentObject.w_FODATANASC),4,0)
    this.FileTelematico.RecordA(iif(!empty(this.oParentObject.w_FODATANASC),this.w_DataAppo,"00000000"))     
    this.FileTelematico.RecordA(this.oParentObject.w_FOCOMUNE)     
    this.FileTelematico.RecordA(this.oParentObject.w_FOSIGLA)     
    this.FileTelematico.RecordA(this.oParentObject.w_FORECOMUNE)     
    this.FileTelematico.RecordA(this.oParentObject.w_FORESIGLA)     
    this.FileTelematico.RecordA(this.oParentObject.w_FOINDIRIZ)     
    this.FileTelematico.RecordA(iif(!empty(this.oParentObject.w_FOCAP),Left(this.oParentObject.w_FOCAP,5),"00000"))     
    * --- Dati riservati al fornitore persona non fisica
    this.FileTelematico.RecordA(iif(this.oParentObject.w_TIPFORN="01" OR this.oParentObject.w_TIPFORN="02",this.oParentObject.w_FODENOMINA,SPACE(60)))     
    this.FileTelematico.RecordA(this.oParentObject.w_FOSECOMUNE)     
    this.FileTelematico.RecordA(this.oParentObject.w_FOSESIGLA)     
    this.FileTelematico.RecordA(this.oParentObject.w_FOSEINDIRI2)     
    this.FileTelematico.RecordA(iif(!empty(this.oParentObject.w_FOSECAP),Left(this.oParentObject.w_FOSECAP,5),"00000"))     
    this.FileTelematico.RecordA(this.oParentObject.w_FOSERCOMUN)     
    this.FileTelematico.RecordA(this.oParentObject.w_FOSERSIGLA)     
    this.FileTelematico.RecordA(this.oParentObject.w_FOSERINDIR)     
    this.FileTelematico.RecordA(iif(!empty(this.oParentObject.w_FOSERCAP),Left(this.oParentObject.w_FOSERCAP,5),"00000"))     
    * --- Campi per ufficio perifierico del C.A.F.
    this.FileTelematico.RecordA(space(40))     
    this.FileTelematico.RecordA(space(2))     
    this.FileTelematico.RecordA(space(35))     
    this.FileTelematico.RecordA("00000")     
    * --- Dichiarazione su pi� invii
    this.FileTelematico.RecordA("0000")     
    this.FileTelematico.RecordA("0000")     
    * --- Chiudo la riga
    this.FileTelematico.RecordA(space(1897-len(this.FileTelematico.RigaA))+"A")     
    this.FileTelematico.RecordA("",.t.,.t.)     
  endproc


  procedure Pag8
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='TRI_BUTI'
    this.cWorkTables[3]='VEP_RITE'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- gsri1bft
  enddefine
  
  func NoPosValue(pQRC,pValore,flagEur)
  local ValorePlus
  do case
  case type('pValore') = 'C'
    if !empty(pValore)
      if len(alltrim(pValore)) > 16 && scrivo il dato su pi� campi
        ValorePlus = pQRC+left(pValore,16)
        pValore = substr(pValore,17)
        do while !empty(alltrim(pValore))
          ValorePlus = ValorePlus + pQRC+'+'+left(pValore,15)+space(15-iif(len(pValore)>15,15,len(pValore)))
          pValore = substr(pValore,17)
        enddo
        return ValorePlus
      else
        return (pQRC+left(pValore+space(16),16))
      endif
    else
      return ''
    endif
  case type('pValore') = 'N'
    if pValore <> 0
      * troncamento delle migliaia
      if (flagEur = '1')
             * troncamento all'unit� di euro
             return (pQRC+right(space(16)+alltrim(str(int(pValore))),16))
        else
             return (pQRC+right(space(16)+alltrim(str(int(pValore/1000))),16))
      endif
    else
      return ''
    endif
  case type('pValore') = 'D'
    if !empty(pValore)
      return (pQRC+right(space(16)+left(dtoc(pValore),2)+substr(dtoc(pValore),4,2)+right(dtoc(pValore),4),16))
    else
      return ''
    endif
  case type('pValore') = 'T'
    if !empty(pValore)
      return (pQRC+right(space(16)+left(dtoc(ttod(pValore)),2)+substr(dtoc(ttod(pValore)),4,2)+right(dtoc(ttod(pValore)),4),16))
    else
      return ''
    endif
  otherwise
    ah_msg( 'Tipo dati non definito: %1',.f.,.f.,.f.,type('pValore'))
    return ''
  endcase
  endfunc
  
  define class FileTelematico as Custom
    oParentObject = .null.
    NomeFile = ''
    RigaA = ''
    RigaB = ''
    RigaE = ''
    PosRigaE = ''
    RigaH = ''
    PosRigaH = ''
    nRigoAU = 1 && Parte da 1
    nRigoSE = 2 && Parte da 2
    nRigoST = 2 && Parte da 2
    nModello = 1 
    nModelloE = 1
    nModelloH = 1
    nModelli = 1
    nModAU = 1
    nModSE = 1
    nModST = 1
    nRecTipoE = 1
    nRecTipoH = 1
  
  proc Init(pNomeFile,oParent)
    strtofile('',pNomeFile)
    this.NomeFile = pNomeFile
    this.oParentObject = oParent
  endproc
  
  proc RecordA(pValore, pACapo,pScrivi)
    this.RigaA =   this.RigaA + upper(pValore)
    if pACapo
        this.RigaA =   this.RigaA + chr(13) + chr(10)
    endif
    if pScrivi
      strtofile(this.RigaA,this.NomeFile,.t.)
      this.RigaA = ''
    endif
  endproc
  
  proc RecordB(pValore, pACapo,pScrivi)
    this.RigaB =   this.RigaB + upper(pValore)
    if pACapo
        this.RigaB =   this.RigaB + chr(13) + chr(10)
    endif
    if pScrivi
      strtofile(this.RigaB,this.NomeFile,.t.)
      this.RigaB = ''
    endif
  endproc
  
  proc RecordEpos &&campi posizionali
    this.PosRigaE = 'E'
    this.PosRigaE = this.PosRigaE + this.oParentObject.w_CFAzi
    this.PosRigaE = this.PosRigaE + right('00000000' + alltrim(str(this.nModelloE)),8) && numero modulo
    this.PosRigaE = this.PosRigaE + space(3)
    this.PosRigaE = this.PosRigaE + right(' '+this.oParentObject.w_OPERAZ,1)
    this.PosRigaE = this.PosRigaE + space(24)
    this.PosRigaE = this.PosRigaE + space(20)  
    this.PosRigaE = this.PosRigaE + '05006900962     ' && PIVA ZUCCHETTI
  endproc
  
  proc RecordE(pQ,pC,pValore,pRigaFissa)
  local nRiga
  if !pRigaFissa
    nRiga = 'this.nRigo'+pQ
    this.RigaE =   this.RigaE + NoPosValue(pQ+right('000'+alltrim(Str(&nRiga)),3)+pC,pValore,oParentObject.w_FLAGEURO)
  else
    this.RigaE =   this.RigaE + NoPosValue(pQ+pC,pValore,this.oParentObject.w_FLAGEURO)
  endif
  endproc
  
  proc ScriviRecordE
    this.RecordEpos()
    strtofile(this.PosRigaE+left(this.RigaE,1800)+     space(iif(int(len(this.RigaE)/1800)=0,1800-len(this.RigaE)+8,8))+     'A'+chr(13)+chr(10),this.NomeFile,.t.)
    do while len(alltrim(this.RigaE)) > 1800
       this.RigaE = substr(this.RigaE,1801)
       strtofile(this.PosRigaE+left(this.RigaE,1800)+space(iif(int(len(this.RigaE)/1800)=0,1800-len(this.RigaE)+8,8))+'A'+chr(13)+chr(10),this.NomeFile,.t.)
       this.nRecTipoE = this.nRecTipoE + 1
    enddo
    this.RigaE = ''
  endproc
  
  proc RecordHpos &&campi posizionali
    this.PosRigaH = 'H'
    this.PosRigaH = this.PosRigaH + this.oParentObject.w_CFAzi
    this.PosRigaH = this.PosRigaH + right('00000000' + alltrim(str(this.nModelloH)),8) && numero modulo
    this.PosRigaH = this.PosRigaH + space(3)
    this.PosRigaH = this.PosRigaH + right(' '+this.oParentObject.w_OPERAZ,1)
    this.PosRigaH = this.PosRigaH + this.oParentObject.w_PERCIPCF &&CF del Percipiente
    this.PosRigaH = this.PosRigaH + space(28)  
    this.PosRigaH = this.PosRigaH + '05006900962     ' && PIVA ZUCCHETTI
  endproc
  
  
  proc RecordH(pQ,pC,pValore,pRigaFissa)
  local nRiga
  if !pRigaFissa
    if val(pc) > 11
        nRiga = 'this.nRigo'+pQ
    else 
        nRiga='1'
    endif    
    this.RigaH =   this.RigaH + NoPosValue(pQ+right('000'+alltrim(Str(&nRiga)),3)+pC,pValore,oParentObject.w_FLAGEURO)
  else
    this.RigaH =   this.RigaH + NoPosValue(pQ+pC,pValore,oParentObject.w_FLAGEURO)
  endif
  endproc
  
  
  proc ScriviRecordH
    this.RecordHpos()
    strtofile(this.PosRigaH+left(this.RigaH,1800)+     space(iif(int(len(this.RigaH)/1800)=0,1800-len(this.RigaH)+8,8))+     'A'+chr(13)+chr(10),this.NomeFile,.t.)
    do while len(alltrim(this.RigaH)) > 1800
       this.RigaH = substr(this.RigaH,1801)
       strtofile(this.PosRigaH+left(this.RigaH,1800)+space(iif(int(len(this.RigaH)/1800)=0,1800-len(this.RigaH)+8,8))+'A'+chr(13)+chr(10),this.NomeFile,.t.)
       this.nRecTipoH = this.nRecTipoH + 1
    enddo
    this.RigaH = ''
  endproc
  
  Proc CancellaFile
    erase(this.NomeFile)
  endproc  
  
  Proc RecordZ
  local Riga
    Riga = 'Z'
    Riga = Riga + space(14)
    Riga = Riga + '000000001'
    Riga = Riga +  right('000000000' + alltrim(str(this.nModelloE-1)),9)
    Riga = Riga + '000000000' && Numero record tipo F
    Riga = Riga + '000000000' && Numero record tipo G
    Riga = Riga +  right('000000000' + alltrim(str(this.nModelloH-1)),9)
    Riga = Riga + space(1837)
    Riga = Riga + 'A'
    Riga = Riga + chr(13) + chr(10)
    strtofile(Riga,this.NomeFile,.t.)
  endproc
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
