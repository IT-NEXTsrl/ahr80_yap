* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_bdd                                                        *
*              Elimina ritenute in distinta I                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][98][VRS_10]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-05-15                                                      *
* Last revis.: 2007-06-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOpz
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsri_bdd",oParentObject,m.pOpz)
return(i_retval)

define class tgsri_bdd as StdBatch
  * --- Local variables
  pOpz = space(10)
  w_ZOOM = space(10)
  w_SERIAL = space(10)
  w_MESS = space(10)
  w_OK = .f.
  * --- WorkFile variables
  MOV_RITE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elimina le Ritenute selezionate in Distinta (Lanciato da GSRI_AVP e GSRI_AVN)
    * --- INPS o IRPEF
    this.w_ZOOM = this.oParentObject.w_CalcZoom
    * --- Consente la Cancellazione Ritenute dalla Distinta se no Definitiva
    this.w_OK = .F.
    if this.oParentObject.w_VPSTATUS<>"D"
      if ah_YesNo("Attenzione: %0i movimenti ritenute %1 selezionati saranno cancellati dalla distinta in modo definitivo%0confermi?","",this.pOpz)
        * --- Try
        local bErr_035E9C90
        bErr_035E9C90=bTrsErr
        this.Try_035E9C90()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          ah_ErrorMsg("Errore durante aggiornamento; operazione annullata",,"")
        endif
        bTrsErr=bTrsErr or bErr_035E9C90
        * --- End
      endif
    else
      ah_ErrorMsg("Operazione non consentita sulle distinte definitive",,"")
    endif
  endproc
  proc Try_035E9C90()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    NC = this.w_ZOOM.cCursor
    SELECT &NC
    GO TOP
    * --- Cicla sui record Selezionati
    SCAN FOR XCHK<>0
    this.w_SERIAL = MRSERIAL
    * --- Try
    local bErr_035E2F40
    bErr_035E2F40=bTrsErr
    this.Try_035E2F40()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_035E2F40
    * --- End
    this.w_OK = .T.
    ENDSCAN
    * --- commit
    cp_EndTrs(.t.)
    if this.w_OK=.T.
      if this.pOpz="IRPEF"
        This.oParentObject.NotifyEvent("Elimina")
      endif
      * --- Lancia lo Zoom
      This.OparentObject.NotifyEvent("Legge")
      ah_ErrorMsg("Aggiornamento completato",,"")
    else
      ah_ErrorMsg("Non ci sono ritenute da togliere",,"")
    endif
    return
  proc Try_035E2F40()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    ah_Msg("Elimino ritenuta %1 del: %2",.T.,.F.,.F.,this.pOpz,DTOC(MRDATDOC))
    if this.pOpz="IRPEF"
      * --- Write into MOV_RITE
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.MOV_RITE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MOV_RITE_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.MOV_RITE_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MRRIFDI1 ="+cp_NullLink(cp_ToStrODBC(SPACE(10)),'MOV_RITE','MRRIFDI1');
            +i_ccchkf ;
        +" where ";
            +"MRSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
               )
      else
        update (i_cTable) set;
            MRRIFDI1 = SPACE(10);
            &i_ccchkf. ;
         where;
            MRSERIAL = this.w_SERIAL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    else
      * --- Write into MOV_RITE
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.MOV_RITE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MOV_RITE_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.MOV_RITE_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MRRIFDI2 ="+cp_NullLink(cp_ToStrODBC(SPACE(10)),'MOV_RITE','MRRIFDI2');
            +i_ccchkf ;
        +" where ";
            +"MRSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
               )
      else
        update (i_cTable) set;
            MRRIFDI2 = SPACE(10);
            &i_ccchkf. ;
         where;
            MRSERIAL = this.w_SERIAL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    return


  proc Init(oParentObject,pOpz)
    this.pOpz=pOpz
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='MOV_RITE'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOpz"
endproc
