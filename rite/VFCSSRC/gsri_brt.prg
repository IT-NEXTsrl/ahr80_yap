* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_brt                                                        *
*              Stampa prospetto ST                                             *
*                                                                              *
*      Author: TAMSoftware & CODELAB                                           *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][98][VRS_61]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-05-19                                                      *
* Last revis.: 2015-06-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsri_brt",oParentObject)
return(i_retval)

define class tgsri_brt as StdBatch
  * --- Local variables
  w_AZCOFAZI = space(16)
  * --- WorkFile variables
  AZIENDA_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questo batch serve per convertire i movimenti ritenute in base alla maschera
    * --- di stampa del quadro 770/SC
    * --- La variabile Tipo mi serve per sapere se la routine � lanciata
    *     dalla generazione oppure dalla stampa
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZCOFAZI"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZCOFAZI;
        from (i_cTable) where;
            AZCODAZI = i_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_AZCOFAZI = NVL(cp_ToDate(_read_.AZCOFAZI),cp_NullValue(_read_.AZCOFAZI))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    vq_exec("..\RITE\EXE\QUERY\GSRI_SRT.VQR",this,"__TMP__")
    if this.oParentObject.w_TIPO $ "GV"
      select * from __tmp__ into cursor RITE3 order by St__Mese,St__Anno,Stcodtri
      Use in Select ("__Tmp__")
    else
      L_AZCOFAZI=this.w_AZCOFAZI
      L_DECIMI=g_PERPVL
      L_ANNO=this.oParentObject.w_ANNO
      if Reccount("__TMP__")>0
        CP_CHPRN("..\RITE\EXE\QUERY\GSRI_SRT.FRX","",this.oParentObject)
      else
        ah_ErrorMsg("Non esistono record da stampare")
      endif
      if used("__TMP__")
        select ("__TMP__")
        use
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='AZIENDA'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
