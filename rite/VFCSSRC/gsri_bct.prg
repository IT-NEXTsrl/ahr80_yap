* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_bct                                                        *
*              Controlli record ST                                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2015-06-22                                                      *
* Last revis.: 2018-09-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParame
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsri_bct",oParentObject,m.pParame)
return(i_retval)

define class tgsri_bct as StdBatch
  * --- Local variables
  pParame = space(1)
  w_GDSERIAL = space(10)
  w_TipoOperazione = space(10)
  w_OBJECT = .NULL.
  w_SERIAL_ST = space(10)
  w_NumrecST = 0
  w_Mess = space(250)
  * --- WorkFile variables
  DATESDST_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controllo che per l'anno ed il tipo operazione non sia gi� presente
    *     un record di tipo ST che non � stato ancora generato
    do case
      case this.pParame $ "LND" 
        * --- Select from Gsri_Bct
        do vq_exec with 'Gsri_Bct',this,'_Curs_Gsri_Bct','',.f.,.t.
        if used('_Curs_Gsri_Bct')
          select _Curs_Gsri_Bct
          locate for 1=1
          do while not(eof())
          this.w_GDSERIAL = _Curs_Gsri_Bct.Stserial
            select _Curs_Gsri_Bct
            continue
          enddo
          use
        endif
        if Not Empty(this.w_Gdserial)
          this.w_TipoOperazione = iif(this.oParentObject.w_Sttipope="O","Ordinario",iif(this.oParentObject.w_Sttipope="A","Aggiornamento",iif(this.oParentObject.w_Sttipope="I","Inserimento","Cancellazione")))
          do case
            case this.pParame="L"
              if this.oParentObject.w_St__Anno>2014
                ah_ErrorMsg("Esiste gi� un record ST non comunicato per l'anno %1%0occorre procedere alla variazione dello stesso.%0Impossibile salvare", 48,"",Alltrim(Str(this.oParentObject.w_St__Anno)))
              else
                ah_ErrorMsg("Esiste gi� un record ST non comunicato%0per l'anno %1 tipo operazione %2;%0occorre procedere alla variazione dello stesso.%0Impossibile salvare", 48,"",Alltrim(Str(this.oParentObject.w_St__Anno)),this.w_TipoOperazione)
              endif
              this.oParentObject.w_RESCHK = -1
            case this.pParame="N"
              if this.oParentObject.w_St__Anno>2014
                ah_ErrorMsg("Esiste gi� un record ST non comunicato per l'anno %1%0occorre procedere alla variazione dello stesso.%0Il salvataggio non sar� consentito", 48,"",Alltrim(Str(this.oParentObject.w_St__Anno)))
              else
                ah_ErrorMsg("Esiste gi� un record ST non comunicato%0per l'anno %1 tipo operazione %2;%0occorre procedere alla variazione dello stesso.%0Il salvataggio non sar� consentito", 48,"",Alltrim(Str(this.oParentObject.w_St__Anno)),this.w_TipoOperazione)
              endif
            case this.pParame="D"
              * --- Controllo di duplicazione
              ah_ErrorMsg("Esiste gi� un record ST non comunicato per l'anno %1%0occorre procedere alla variazione dello stesso.%0La duplicazione non � consentita", 48,"",Alltrim(Str(this.oParentObject.w_St__Anno)))
          endcase
        else
          * --- Solo nel caso in cui l'utente ha premuto il tasto duplica inserisco i dati nella
          *     tabella Datesdst
          if this.pParame="D"
            * --- Try
            local bErr_031FFD78
            bErr_031FFD78=bTrsErr
            this.Try_031FFD78()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- rollback
              bTrsErr=.t.
              cp_EndTrs(.t.)
              ah_ErrorMsg("Errore nella creazione dei dati estratti ST (%1)", "!", "", i_trsmsg)
            endif
            bTrsErr=bTrsErr or bErr_031FFD78
            * --- End
          endif
        endif
      case this.pParame="A"
        this.w_OBJECT = GSRI_AVP()
        if !(this.w_OBJECT.bSec1)
          i_retcode = 'stop'
          return
        endif
        this.w_OBJECT.EcpFilter()     
        * --- Valorizzo i campi chiave
        this.w_OBJECT.w_VPSERIAL = this.oParentObject.w_STVERSER
        * --- Carico il record richiesto
        this.w_OBJECT.EcpSave()     
      case this.pParame="G"
        this.w_OBJECT = IIF(this.oParentObject.w_TIPST="2015",GSRI_AGY("77S16"),IIF(this.oParentObject.w_TIPST="2016",GSRI_AGY("77S17"),IIF(this.oParentObject.w_TIPST="2017",GSRI_AGY("77S18"),GSRI_AGT())))
        if !(this.w_OBJECT.bSec1)
          i_retcode = 'stop'
          return
        endif
        this.w_OBJECT.EcpFilter()     
        * --- Valorizzo i campi chiave
        this.w_OBJECT.w_GTSERIAL = This.oParentObject.w_Gdserial
        * --- Carico il record richiesto
        this.w_OBJECT.EcpSave()     
    endcase
  endproc
  proc Try_031FFD78()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    w_Conn=i_TableProp[this.DATESDST_IDX, 3]
    this.w_SERIAL_ST = space(10)
    cp_NextTableProg(this, w_Conn, "STSER", "i_codazi,w_SERIAL_ST")
    * --- Insert into DATESDST
    i_nConn=i_TableProp[this.DATESDST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DATESDST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\rite\exe\query\gsri1bct",this.DATESDST_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    this.w_NumrecST = i_Rows
    * --- commit
    cp_EndTrs(.t.)
    this.w_Mess = "Operazione completata%0N.%1 dati estratti duplicati"
    ah_ErrorMsg (this.w_Mess,,"", Alltrim(Str(this.w_NumrecST)))
    return


  proc Init(oParentObject,pParame)
    this.pParame=pParame
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='DATESDST'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_Gsri_Bct')
      use in _Curs_Gsri_Bct
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParame"
endproc
