* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_kgr                                                        *
*              Generazione ritenute                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [146] [VRS_33]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-05-16                                                      *
* Last revis.: 2008-09-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsri_kgr",oParentObject))

* --- Class definition
define class tgsri_kgr as StdForm
  Top    = 39
  Left   = 32

  * --- Standard Properties
  Width  = 374
  Height = 150
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-09-25"
  HelpContextID=32110185
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=7

  * --- Constant Properties
  _IDX = 0
  TAB_RITE_IDX = 0
  cPrg = "gsri_kgr"
  cComment = "Generazione ritenute"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_TIPORI = space(1)
  w_CODAZI = space(5)
  w_DATULT = ctod('  /  /  ')
  w_DATINI = ctod('  /  /  ')
  o_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  o_DATFIN = ctod('  /  /  ')
  w_FLDATG = space(1)
  w_DATGEN = ctod('  /  /  ')
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsri_kgrPag1","gsri_kgr",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDATINI_1_4
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- gsri_kgr
    WITH THIS.PARENT
     DO CASE
             CASE OPARENTOBJECT = 'A'
                   .cComment = "Generazione ritenute operate"
             CASE OPARENTOBJECT  = 'V'
                   .cComment = "Generazione ritenute subite"
       ENDCASE
    ENDWITH
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='TAB_RITE'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do GSRI_BGR with this
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIPORI=space(1)
      .w_CODAZI=space(5)
      .w_DATULT=ctod("  /  /  ")
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_FLDATG=space(1)
      .w_DATGEN=ctod("  /  /  ")
        .w_TIPORI = this.oParentObject
        .w_CODAZI = i_CODAZI
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODAZI))
          .link_1_2('Full')
        endif
          .DoRTCalc(3,3,.f.)
        .w_DATINI = .w_DATULT+1
        .w_DATFIN = iif(Not empty(.w_DATINI),MESESKIP(.w_DATINI, 0, 'F'),.w_DATFIN)
        .w_FLDATG = 'U'
        .w_DATGEN = .w_DATFIN
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
            .w_TIPORI = this.oParentObject
          .link_1_2('Full')
        .DoRTCalc(3,4,.t.)
        if .o_DATINI<>.w_DATINI
            .w_DATFIN = iif(Not empty(.w_DATINI),MESESKIP(.w_DATINI, 0, 'F'),.w_DATFIN)
        endif
        .DoRTCalc(6,6,.t.)
        if .o_DATFIN<>.w_DATFIN
            .w_DATGEN = .w_DATFIN
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oDATGEN_1_7.enabled = this.oPgFrm.Page1.oPag.oDATGEN_1_7.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oDATGEN_1_7.visible=!this.oPgFrm.Page1.oPag.oDATGEN_1_7.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TAB_RITE_IDX,3]
    i_lTable = "TAB_RITE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TAB_RITE_IDX,2], .t., this.TAB_RITE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TAB_RITE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRTIPRIT,TRCODAZI,TRDATULT";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   +" and TRTIPRIT="+cp_ToStrODBC(this.w_TIPORI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRTIPRIT',this.w_TIPORI;
                       ,'TRCODAZI',this.w_CODAZI)
            select TRTIPRIT,TRCODAZI,TRDATULT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.TRCODAZI,space(5))
      this.w_DATULT = NVL(cp_ToDate(_Link_.TRDATULT),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_DATULT = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TAB_RITE_IDX,2])+'\'+cp_ToStr(_Link_.TRTIPRIT,1)+'\'+cp_ToStr(_Link_.TRCODAZI,1)
      cp_ShowWarn(i_cKey,this.TAB_RITE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_4.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_4.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_5.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_5.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oFLDATG_1_6.RadioValue()==this.w_FLDATG)
      this.oPgFrm.Page1.oPag.oFLDATG_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDATGEN_1_7.value==this.w_DATGEN)
      this.oPgFrm.Page1.oPag.oDATGEN_1_7.value=this.w_DATGEN
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_DATINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATINI_1_4.SetFocus()
            i_bnoObbl = !empty(.w_DATINI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_DATFIN)) or not(.w_DATFIN>=.w_DATINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFIN_1_5.SetFocus()
            i_bnoObbl = !empty(.w_DATFIN)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_DATGEN)) or not(.w_DATGEN>=.w_DATFIN))  and not(.w_FLDATG<>'U')  and (.w_FLDATG='U')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATGEN_1_7.SetFocus()
            i_bnoObbl = !empty(.w_DATGEN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data scadenza maggiore della data di generazione")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DATINI = this.w_DATINI
    this.o_DATFIN = this.w_DATFIN
    return

enddefine

* --- Define pages as container
define class tgsri_kgrPag1 as StdContainer
  Width  = 370
  height = 150
  stdWidth  = 370
  stdheight = 150
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDATINI_1_4 as StdField with uid="BMUWORMSKX",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di inizio selezione scadenze",;
    HelpContextID = 62615754,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=161, Top=12

  add object oDATFIN_1_5 as StdField with uid="NNMCUZKLCT",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di fine selezione scadenze",;
    HelpContextID = 252604618,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=161, Top=40

  func oDATFIN_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATFIN>=.w_DATINI)
    endwith
    return bRes
  endfunc


  add object oFLDATG_1_6 as StdCombo with uid="PNCPTCYFMF",rtseq=6,rtrep=.f.,left=161,top=74,width=126,height=21;
    , ToolTipText = "Data di registrazione dei movimenti ritenute: unica o uguale alla data registrazione o alla data documento del pagamento";
    , HelpContextID = 90465706;
    , cFormVar="w_FLDATG",RowSource=""+"Unica,"+"Data registrazione,"+"Data documento", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLDATG_1_6.RadioValue()
    return(iif(this.value =1,'U',;
    iif(this.value =2,'R',;
    iif(this.value =3,'D',;
    space(1)))))
  endfunc
  func oFLDATG_1_6.GetRadio()
    this.Parent.oContained.w_FLDATG = this.RadioValue()
    return .t.
  endfunc

  func oFLDATG_1_6.SetRadio()
    this.Parent.oContained.w_FLDATG=trim(this.Parent.oContained.w_FLDATG)
    this.value = ;
      iif(this.Parent.oContained.w_FLDATG=='U',1,;
      iif(this.Parent.oContained.w_FLDATG=='R',2,;
      iif(this.Parent.oContained.w_FLDATG=='D',3,;
      0)))
  endfunc

  add object oDATGEN_1_7 as StdField with uid="QXLIXZYIXE",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DATGEN", cQueryName = "DATGEN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data scadenza maggiore della data di generazione",;
    ToolTipText = "Data di generazione dei movimenti di ritenute",;
    HelpContextID = 256733386,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=289, Top=74

  func oDATGEN_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLDATG='U')
    endwith
   endif
  endfunc

  func oDATGEN_1_7.mHide()
    with this.Parent.oContained
      return (.w_FLDATG<>'U')
    endwith
  endfunc

  func oDATGEN_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATGEN>=.w_DATFIN)
    endwith
    return bRes
  endfunc


  add object oBtn_1_8 as StdButton with uid="MGXPFVZZOE",left=263, top=100, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per inizio elaborazione";
    , HelpContextID = 32081434;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_8.Click()
      with this.Parent.oContained
        do GSRI_BGR with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_9 as StdButton with uid="WONLLGUZCO",left=313, top=100, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 24792762;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_10 as StdString with uid="AUZFUJJXSS",Visible=.t., Left=7, Top=12,;
    Alignment=1, Width=151, Height=15,;
    Caption="Ritenute maturate dal:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="SDQPUDPLLJ",Visible=.t., Left=45, Top=40,;
    Alignment=1, Width=115, Height=15,;
    Caption="Al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="YCYBYHFIKD",Visible=.t., Left=7, Top=76,;
    Alignment=1, Width=151, Height=18,;
    Caption="Data registrazione:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsri_kgr','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
