* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_bir                                                        *
*              Calcola totali IRPEF                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][98][VRS_17]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-05-15                                                      *
* Last revis.: 2007-12-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsri_bir",oParentObject)
return(i_retval)

define class tgsri_bir as StdBatch
  * --- Local variables
  w_APPOGG = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcola Totali IRPEF (da GSRI_AVP)
    this.oParentObject.w_TOTAL1 = 0
    this.oParentObject.w_TOTAL2 = 0
    this.oParentObject.w_TOTAL3 = 0
    this.w_APPOGG = this.oParentObject.w_VPIMPVER
    * --- Select from GSRI_KVP
    do vq_exec with 'GSRI_KVP',this,'_Curs_GSRI_KVP','',.f.,.t.
    if used('_Curs_GSRI_KVP')
      select _Curs_GSRI_KVP
      locate for 1=1
      do while not(eof())
      if this.oParentObject.w_VPVALVER=_Curs_GSRI_KVP.MRCODVAL
        this.oParentObject.w_TOTAL1 = this.oParentObject.w_TOTAL1+MRNONSO1
        this.oParentObject.w_TOTAL2 = this.oParentObject.w_TOTAL2+MRIMPRI1
        this.oParentObject.w_TOTAL3 = this.oParentObject.w_TOTAL3+MRTOTIM1
      else
        this.oParentObject.w_TOTAL1 = cp_Round(this.oParentObject.w_TOTAL1+VAL2MON(MRNONSO1,this.oParentObject.w_CAMBIO,1,I_DATSYS,G_PERVAL),this.oParentObject.w_Dectop)
        this.oParentObject.w_TOTAL2 = cp_Round(this.oParentObject.w_TOTAL2+VAL2MON(MRIMPRI1,this.oParentObject.w_CAMBIO,1,I_DATSYS,G_PERVAL),this.oParentObject.w_Dectop)
        this.oParentObject.w_TOTAL3 = cp_Round(this.oParentObject.w_TOTAL3+VAL2MON(MRTOTIM1,this.oParentObject.w_CAMBIO,1,I_DATSYS,G_PERVAL),this.oParentObject.w_Dectop)
      endif
        select _Curs_GSRI_KVP
        continue
      enddo
      use
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  proc CloseCursors()
    if used('_Curs_GSRI_KVP')
      use in _Curs_GSRI_KVP
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
