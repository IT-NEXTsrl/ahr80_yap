* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_bdh                                                        *
*              Totali Record H                                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2015-05-07                                                      *
* Last revis.: 2016-02-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParame
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsri_bdh",oParentObject,m.pParame)
return(i_retval)

define class tgsri_bdh as StdBatch
  * --- Local variables
  pParame = space(1)
  w_GSRI_MDH = .NULL.
  w_CURS = space(10)
  w_FIELDS = space(250)
  w_WHERE = space(250)
  w_Codice = space(1)
  w_NumRec = 0
  w_OBJECT = .NULL.
  * --- WorkFile variables
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcola totali record H
    do case
      case this.pParame="T"
        this.oParentObject.w_AU001004 = 0
        this.oParentObject.w_AU001005 = 0
        this.oParentObject.w_AU001006 = " "
        this.oParentObject.w_AU001008 = 0
        this.oParentObject.w_AU001009 = 0
        this.oParentObject.w_AU001020 = 0
        this.oParentObject.w_AU001021 = 0
        this.oParentObject.w_AU001022 = 0
        this.oParentObject.w_AU001023 = 0
        this.oParentObject.w_AU002004 = 0
        this.oParentObject.w_AU002005 = 0
        this.oParentObject.w_AU002006 = " "
        this.oParentObject.w_AU002008 = 0
        this.oParentObject.w_AU002009 = 0
        this.oParentObject.w_AU002020 = 0
        this.oParentObject.w_AU002021 = 0
        this.oParentObject.w_AU002022 = 0
        this.oParentObject.w_AU002023 = 0
        this.oParentObject.w_AU003004 = 0
        this.oParentObject.w_AU003005 = 0
        this.oParentObject.w_AU003006 = " "
        this.oParentObject.w_AU003008 = 0
        this.oParentObject.w_AU003009 = 0
        this.oParentObject.w_AU003020 = 0
        this.oParentObject.w_AU003021 = 0
        this.oParentObject.w_AU003022 = 0
        this.oParentObject.w_AU003023 = 0
        this.oParentObject.w_Record2 = .f.
        this.oParentObject.w_Record3 = .f.
        this.w_GSRI_MDH = this.oParentObject.GSRI_MDH
        this.w_CURS = "CodiceSns"
        this.w_FIELDS = "t_Rhaux004 as Rhaux004, t_Rhaux005 as Rhaux005, t_Rhaux006 as Rhaux006,t_Rhaux008 as Rhaux008, t_Rhaux009 as Rhaux009,t_Rhaux020 as Rhaux020,t_Rhaux021 as Rhaux021"
        this.w_FIELDS = this.w_FIELDS+",t_Rhaux022 as Rhaux022,t_Rhaux023 as Rhaux023"
        this.w_WHERE = "Not Empty(t_Rhdatreg) And not Deleted() And t_Rhesclge=2"
        * --- INTERROGA IL TEMPORANEO
        this.w_GSRI_MDH.EXEC_SELECT(this.w_Curs, this.w_Fields, this.w_Where, "", "", "")     
        if Reccount("CodiceSns")>0
          * --- Dato che il campo codice somme non soggette � una combo, nel transitorio
          *     � espresso come numerico, di conseguenza lo converto in un campo carattere
          Select *,iif(Rhaux006=0 Or (Nvl(this.oParentObject.w_Rhperest,"N")="S" And Nvl(this.oParentObject.w_Anflsgre,"N")="S")," ",Str(Rhaux006,1)) as Codice from CodiceSns into cursor CodiceSns
          * --- Il cursore Rite conterr� solo le righe con codice somme non soggette
          *     valorizzato
          Select Sum(Rhaux004) as Rhaux004,Sum(Rhaux005) as Rhaux005,Codice, ; 
 Sum(Rhaux008) as Rhaux008,Sum(Rhaux009) as Rhaux009,Sum(Rhaux020) as Rhaux020,; 
 Sum(Rhaux021) as Rhaux021,Sum(Rhaux022) as Rhaux022,Sum(Rhaux023) as Rhaux023; 
 from CodiceSns where Codice $"123" group by Codice order by Codice into cursor Rite
          if Reccount("Rite")>0
            Select Rite
            Go Top
            this.w_Codice = Codice
            Select Sum(Rhaux004) as Rhaux004,Sum(Rhaux005) as Rhaux005,this.w_Codice as Codice,; 
 Sum(Rhaux008) as Rhaux008,Sum(Rhaux009) as Rhaux009,Sum(Rhaux020) as Rhaux020,; 
 Sum(Rhaux021) as Rhaux021,Sum(Rhaux022) as Rhaux022,Sum(Rhaux023) as Rhaux023; 
 from CodiceSns where Empty(Codice) group by Codice into cursor Rite1
            use in Select ("CodiceSns")
            Select * from Rite ; 
 union all; 
 Select * from Rite1; 
 order By Codice into cursor CodiceSns
            Select Sum(Rhaux004) as Rhaux004,Sum(Rhaux005) as Rhaux005,Codice as Codice,; 
 Sum(Rhaux008) as Rhaux008,Sum(Rhaux009) as Rhaux009,Sum(Rhaux020) as Rhaux020,; 
 Sum(Rhaux021) as Rhaux021,Sum(Rhaux022) as Rhaux022,Sum(Rhaux023) as Rhaux023; 
 from CodiceSns group by Codice into cursor CodiceSns
          else
            Select Sum(Rhaux004) as Rhaux004,Sum(Rhaux005) as Rhaux005,Codice, ; 
 Sum(Rhaux008) as Rhaux008,Sum(Rhaux009) as Rhaux009,Sum(Rhaux020) as Rhaux020,; 
 Sum(Rhaux021) as Rhaux021,Sum(Rhaux022) as Rhaux022,Sum(Rhaux023) as Rhaux023; 
 from CodiceSns group by Codice order by Codice into cursor CodiceSns
          endif
          use in Select ("Rite")
          use in Select ("Rite1")
          Select CodiceSns
          Go Top
          this.w_NumRec = 1
          Scan
          do case
            case this.w_NumRec=1
              this.oParentObject.w_AU001004 = iif(Nvl(Rhaux004,0)<0,0,Rhaux004)
              this.oParentObject.w_AU001005 = iif(Nvl(Rhaux005,0)<0,0,Rhaux005)
              this.oParentObject.w_AU001006 = Codice
              this.oParentObject.w_AU001008 = iif(Nvl(Rhaux008,0) < 0,0,Rhaux008)
              this.oParentObject.w_AU001009 = iif(Nvl(Rhaux009,0) < 0,0,Rhaux009)
              this.oParentObject.w_AU001020 = iif(Alltrim(this.oParentObject.w_Rhcaupre) $ "C-M-M1-V" And Nvl(Rhaux020,0)>0 ,Rhaux020,0)
              this.oParentObject.w_AU001021 = iif(Alltrim(this.oParentObject.w_Rhcaupre) $ "C-M-M1-V" And Nvl(Rhaux021,0)>0,Rhaux021,0)
              this.oParentObject.w_AU001022 = iif(Nvl(Rhaux022,0) < 0,0,Rhaux022)
              this.oParentObject.w_AU001023 = iif(Alltrim(this.oParentObject.w_Rhcaupre) $ "X-Y" And Nvl(Rhaux023,0)>0,Rhaux023,0)
            case this.w_NumRec=2
              this.oParentObject.w_AU002004 = iif(Nvl(Rhaux004,0)<0,0,Rhaux004)
              this.oParentObject.w_AU002005 = iif(Nvl(Rhaux005,0)<0,0,Rhaux005)
              this.oParentObject.w_AU002006 = Codice
              this.oParentObject.w_AU002008 = iif(Nvl(Rhaux008,0) < 0,0,Rhaux008)
              this.oParentObject.w_AU002009 = iif(Nvl(Rhaux009,0) < 0,0,Rhaux009)
              this.oParentObject.w_AU002020 = iif(Alltrim(this.oParentObject.w_Rhcaupre) $ "C-M-M1-V" And Nvl(Rhaux020,0)>0 ,Rhaux020,0)
              this.oParentObject.w_AU002021 = iif(Alltrim(this.oParentObject.w_Rhcaupre) $ "C-M-M1-V" And Nvl(Rhaux021,0)>0,Rhaux021,0)
              this.oParentObject.w_AU002022 = iif(Nvl(Rhaux022,0) < 0,0,Rhaux022)
              this.oParentObject.w_AU002023 = iif(Alltrim(this.oParentObject.w_Rhcaupre) $ "X-Y" And Nvl(Rhaux023,0)>0,Rhaux023,0)
              this.oParentObject.w_Record2 = .t.
            case this.w_NumRec=3
              this.oParentObject.w_AU003004 = iif(Nvl(Rhaux004,0)<0,0,Rhaux004)
              this.oParentObject.w_AU003005 = iif(Nvl(Rhaux005,0)<0,0,Rhaux005)
              this.oParentObject.w_AU003006 = Codice
              this.oParentObject.w_AU003008 = iif(Nvl(Rhaux008,0) < 0,0,Rhaux008)
              this.oParentObject.w_AU003009 = iif(Nvl(Rhaux009,0) < 0,0,Rhaux009)
              this.oParentObject.w_AU003020 = iif(Alltrim(this.oParentObject.w_Rhcaupre) $ "C-M-M1-V" And Nvl(Rhaux020,0)>0 ,Rhaux020,0)
              this.oParentObject.w_AU003021 = iif(Alltrim(this.oParentObject.w_Rhcaupre) $ "C-M-M1-V" And Nvl(Rhaux021,0)>0,Rhaux021,0)
              this.oParentObject.w_AU003022 = iif(Nvl(Rhaux022,0) < 0,0,Rhaux022)
              this.oParentObject.w_AU003023 = iif(Alltrim(this.oParentObject.w_Rhcaupre) $ "X-Y" And Nvl(Rhaux023,0)>0,Rhaux023,0)
              this.oParentObject.w_Record3 = .t.
          endcase
          this.w_NumRec = this.w_NumRec+1
          Endscan
          This.oParentObject.mHideControls()
        else
          * --- Non esistono righe valide da conteggiare
        endif
        use in CodiceSns
      case this.pParame="M"
        this.w_OBJECT = GSRI_AMR("A")
        if !(this.w_OBJECT.bSec1)
          i_retcode = 'stop'
          return
        endif
        this.w_OBJECT.EcpFilter()     
        * --- Valorizzo i campi chiave
        this.w_OBJECT.w_MRSERIAL = This.oParentObject.w_Rhrifmov
        * --- Carico il record richiesto
        this.w_OBJECT.EcpSave()     
      case this.pParame="G" Or this.pParame="U"
        if this.pParame="G"
          this.w_OBJECT = GSRI_AGT()
        else
          this.w_OBJECT = GSRI_ACU("CUR16")
        endif
        if !(this.w_OBJECT.bSec1)
          i_retcode = 'stop'
          return
        endif
        this.w_OBJECT.EcpFilter()     
        * --- Valorizzo i campi chiave
        if this.pParame="G"
          this.w_OBJECT.w_GTSERIAL = This.oParentObject.w_Gdserial
        else
          this.w_OBJECT.w_CUSERIAL = This.oParentObject.w_Gdserial
        endif
        * --- Carico il record richiesto
        this.w_OBJECT.EcpSave()     
      case this.pParame="D"
        * --- Visualizzazione dato estratto
        this.w_OBJECT = GSRI_ACH()
        if !(this.w_OBJECT.bSec1)
          i_retcode = 'stop'
          return
        endif
        this.w_OBJECT.EcpFilter()     
        * --- Valorizzo i campi chiave
        this.w_OBJECT.w_CHSERIAL = This.oParentObject.w_Chdatest
        * --- Carico il record richiesto
        this.w_OBJECT.EcpSave()     
    endcase
  endproc


  proc Init(oParentObject,pParame)
    this.pParame=pParame
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParame"
endproc
