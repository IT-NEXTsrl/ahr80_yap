* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri3bsp                                                        *
*              Stampa PDF 770/2006                                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-07-19                                                      *
* Last revis.: 2013-04-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsri3bsp",oParentObject)
return(i_retval)

define class tgsri3bsp as StdBatch
  * --- Local variables
  w_EMAIL = space(100)
  w_PHONE = space(12)
  w_QUADRO = 0
  w_TRASMATT = 0
  w_DATFIN = ctod("  /  /  ")
  w_DATINI = ctod("  /  /  ")
  w_DataAppo = space(8)
  w_ANNO = space(4)
  w_CODVAL = space(3)
  w_SOSPENDI = .f.
  w_DECIMI = space(1)
  w_VALUTA = space(1)
  w_PERCIN = space(15)
  w_PERCFIN = space(15)
  w_STAMPAST = space(1)
  w_STAMPAH = space(1)
  w_STAMPAJ = space(1)
  w_REGAGE = space(1)
  w_CALCOLI = 0
  w_MESS = space(254)
  w_DATIPRES = .f.
  w_RESFDF = 0
  w_DATCOMPL = .f.
  w_MRCODCON = space(15)
  w_PERCIPCF = space(16)
  w_CAUPRE = space(1)
  w_PROVIN = space(2)
  w_ANDESCRI = space(40)
  w_LOCALEST = space(30)
  w_INDIREST = space(35)
  w_NAZIOEST = space(3)
  w_NACODEST = space(5)
  w_CFPEREST = space(20)
  w_OLDPERCF = space(20)
  w_OLDPER = space(15)
  w_ERROR = .f.
  w_PERCESTE = space(1)
  * --- WorkFile variables
  AZIENDA_idx=0
  CONTI_idx=0
  DES_DIVE_idx=0
  NAZIONI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch per l'elaborazione dei dati relativi al frontespizio del Mod. 770/2006, per la stampa su formato PDF
    * --- Creo il cursore __TMP__ contenente i dati presenti nel frontespizio, tali dati saranno elaborati dalla CP_FFDF
    * --- Sez. I
    * --- Sez. II
    * --- Sez. IV
    * --- Sez. Firma / Imp. trasmis.
    this.w_ANNO = this.oParentObject.w_ANNO
    this.w_VALUTA = this.oParentObject.w_VALUTA
    this.w_DECIMI = this.oParentObject.w_DECIMI
    this.w_DATFIN = this.oParentObject.w_DATFIN
    this.w_DATINI = this.oParentObject.w_DATINI
    this.w_PERCIN = this.oParentObject.w_PERCIN
    this.w_PERCFIN = this.oParentObject.w_PERCFIN
    this.w_CODVAL = this.oParentObject.w_CODVAL
    this.w_SOSPENDI = .F.
    this.w_STAMPAST = "S"
    this.w_STAMPAH = "S"
    DECLARE ARRFDF (1,2)
    ARRFDF (1,1) = "  "
    ARRFDF (1,2) = 1
    this.w_PHONE = iif(empty(this.oParentObject.w_TELEFONO),this.oParentObject.w_SETELEFONO,this.oParentObject.w_TELEFONO)
    CREATE CURSOR __TMP__ (COGDEN C(60), NOME C(20), CODFIS C(16), CODFISC C(16), CORRET C(1), INTEGRA C(1), EVEECC C(1), ; 
 CODEATT C(5), TELNUM C(12), FAXPREF C(4), FAXNUM C(8), EMAIL C(100), PFCOMUNE C(40), PFPROV C(2), ; 
 PFDATNAS1 C(2), PFDATNAS2 C(2), PFDATNAS3 C(4), PFMAS C(1), PFFEM C(1), PFCOMRES C(40), PFPRORES C(2), ; 
 PFVIARES C(35), PFCAPRES C(5), PFDATVAR1 C(2), PFDATVAR2 C(2), PFDATVAR3 C(4), ASSEDLEG1 C(2), ASSEDLEG2 C(4), ; 
 ASCOMUNE C(40), ASPROV C(2), ASVIA C(35), ASCAP C(5), ASDOMFIS1 C(2), ASDOMFIS2 C(4), ASCOMFIS C(40), ASPROFIS C(2), ; 
 ASVIAFIS C(35), ASCAPFIS C(5), STATO C(1), NATGIU C(2), SITUAZ C(1), CFDICAPP C(11), RFCODFIS C(16), CODCAR C(2), ; 
 RFCOG C(24), RFNOM C(20), RFSEXM C(1), RFSEXF C(1), RFDATNAS1 C(2), RFDATNAS2 C(2), RFDATNAS3 C(4), RFCOMNAS C(40), ; 
 RFPRONAS C(2), RFCOMUNE C(40), RFPROV C(2), RFCAP C(5), RFVIA C(35), RFNUM C(12), NUMCOM N(8) , ; 
 ST C(1), SX C(1),ST4 C(1),SX4 C(1),S71 C(1),S74 C(1),RFCFINS2 C(16),NUMCOM2 N(8) ,CFINTER C(16), ; 
 NUMCAF C(5), IMPTRAS C(1), DATIMP1 C(2), DATIMP2 C(2), DATIMP3 C(4),CFRESCAF C(16), ART35 C(1),NUMCOM4 N(8), ; 
 INVAVVTE C(1),CFINCCON C(16),CODSOGET C(1),RICAVVTE C(1))
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZ_EMAIL"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZ_EMAIL;
        from (i_cTable) where;
            AZCODAZI = i_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_EMAIL = NVL(cp_ToDate(_read_.AZ_EMAIL),cp_NullValue(_read_.AZ_EMAIL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Memorizzo i dati nel cursore __TMP__
    INSERT INTO __TMP__ VALUES(iif (empty(this.oParentObject.w_FODENOMINA),upper(this.oParentObject.w_FOCOGNOME),upper(this.oParentObject.w_FODENOMINA)),upper(this.oParentObject.w_FONOME),this.oParentObject.w_FOCODFIS,this.oParentObject.w_FOCODFIS, ; 
 iif(this.oParentObject.w_FLAGCOR="1","X",""),iif(this.oParentObject.w_FLAGINT="1","X",""), iif(this.oParentObject.w_FLAGEVE="01","X",""),iif(empty(this.oParentObject.w_SECODATT),this.oParentObject.w_CODATT,this.oParentObject.w_SECODATT), ; 
 this.w_PHONE, space(4),space(8),upper(this.w_EMAIL),upper(this.oParentObject.w_COMUNE),upper(this.oParentObject.w_SIGLA), ; 
 iif(empty(this.oParentObject.w_DATANASC),"",right("0"+alltrim(str(day(this.oParentObject.w_DATANASC))),2)),iif(empty(this.oParentObject.w_DATANASC),"",right("0"+alltrim(str(month(this.oParentObject.w_DATANASC))),2)), ; 
 iif(empty(this.oParentObject.w_DATANASC),"",alltrim(str(year(this.oParentObject.w_DATANASC)))), iif(this.oParentObject.w_SESSO="M","X",""),iif(this.oParentObject.w_SESSO="F","X",""),upper(this.oParentObject.w_RECOMUNE),upper(this.oParentObject.w_RESIGLA),upper(this.oParentObject.w_INDIRIZ),this.oParentObject.w_CAP, ; 
 iif(empty(this.oParentObject.w_VARRES),"",right("0"+alltrim(str(day(this.oParentObject.w_VARRES))),2)),iif(empty(this.oParentObject.w_VARRES),"",right("0"+alltrim(str(month(this.oParentObject.w_VARRES))),2)), ; 
 iif(empty(this.oParentObject.w_VARRES),"",alltrim(str(year(this.oParentObject.w_VARRES)))), iif(empty(this.oParentObject.w_SEVARSED),"",right("0"+alltrim(str(month(this.oParentObject.w_SEVARSED))),2)), ; 
 iif(empty(this.oParentObject.w_SEVARSED),"",alltrim(str(year(this.oParentObject.w_SEVARSED)))), upper(this.oParentObject.w_SECOMUNE),upper(this.oParentObject.w_SESIGLA),upper(this.oParentObject.w_SEINDIRI2),this.oParentObject.w_SECAP, ; 
 iif(empty(this.oParentObject.w_SEVARDOM),"",right("0"+alltrim(str(month(this.oParentObject.w_SEVARDOM))),2)),iif(empty(this.oParentObject.w_SEVARDOM),"",alltrim(str(year(this.oParentObject.w_SEVARDOM)))),upper(this.oParentObject.w_SERCOMUN), ; 
 upper(this.oParentObject.w_SERSIGLA),upper(this.oParentObject.w_SERINDIR),this.oParentObject.w_SERCAP, this.oParentObject.w_STATO,iif(empty(this.oParentObject.w_NATGIU),"  ",right("00"+alltrim(this.oParentObject.w_NATGIU),2)),this.oParentObject.w_SITUAZ,this.oParentObject.w_CODFISDA,this.oParentObject.w_RFCODFIS,iif(empty(this.oParentObject.w_RFCODCAR),"  ",right("00"+alltrim(this.oParentObject.w_RFCODCAR),2)), ; 
 iif(!empty(this.oParentObject.w_RFDENOMI),upper(this.oParentObject.w_RFDENOMI),upper(this.oParentObject.w_RFCOGNOME)),iif(!empty(this.oParentObject.w_RFDENOMI),"",upper(this.oParentObject.w_RFNOME)), iif(this.oParentObject.w_RFSESSO="M","X",""),iif(this.oParentObject.w_RFSESSO="F","X",""),iif(empty(this.oParentObject.w_RFDATANASC),"",right("0"+alltrim(str(day(this.oParentObject.w_RFDATANASC))),2)), ; 
 iif(empty(this.oParentObject.w_RFDATANASC),"",right("0"+alltrim(str(month(this.oParentObject.w_RFDATANASC))),2)),iif(empty(this.oParentObject.w_RFDATANASC),"",alltrim(str(year(this.oParentObject.w_RFDATANASC)))), ; 
 upper(this.oParentObject.w_RFCOMNAS),upper(this.oParentObject.w_RFSIGNAS),upper(this.oParentObject.w_RFCOMUNE),upper(this.oParentObject.w_RFSIGLA), this.oParentObject.w_RFCAP,upper(this.oParentObject.w_RFINDIRIZ), ; 
 this.oParentObject.w_RFTELEFONO,iif(this.oParentObject.w_SEZ1="1",this.oParentObject.w_NUMCERTIF2,0), ; 
 iif(this.oParentObject.w_qST1="1","X",""),iif(this.oParentObject.w_qSX1="1","X",""),iif(this.oParentObject.w_qST14="1","X",""),iif(this.oParentObject.w_qSX14="1","X",""),iif(this.oParentObject.w_q771="1" ,"X",""),iif(this.oParentObject.w_q774="1","X",""),this.oParentObject.w_RFCFINS2,iif(this.oParentObject.w_SEZ2="1",this.oParentObject.w_NUMCERTIF2,0),this.oParentObject.w_CODFISIN, ; 
 iif(this.oParentObject.w_NUMCAF=0,"",str(this.oParentObject.w_NUMCAF,5)),this.oParentObject.w_IMPTRTEL, ; 
 iif(empty(this.oParentObject.w_RFDATIMP),"",right("0"+alltrim(str(day(this.oParentObject.w_RFDATIMP))),2)),iif(empty(this.oParentObject.w_RFDATIMP),"",right("0"+alltrim(str(month(this.oParentObject.w_RFDATIMP))),2)), ; 
 iif(empty(this.oParentObject.w_RFDATIMP),"",alltrim(str(year(this.oParentObject.w_RFDATIMP)))),this.oParentObject.w_CFRESCAF,iif(this.oParentObject.w_VISTOA35="1","X",""),iif(this.oParentObject.w_SEZ4="1",this.oParentObject.w_NUMCERTIF2,0), ; 
 iif(this.oParentObject.w_FLGINVTI="1","X",""),this.oParentObject.w_CFINCCON,this.oParentObject.w_CODSOGG,iif(this.oParentObject.w_RICAVVTEL="1","X",""))
    * --- Resta da definire il codice soggetto e impego a presentare in via telematica la dichiarazione
    result1=cp_ffdf(" ","770Frontespizio06.pdf",this," ",@ARRFDF)
    CP_CHPRN(SYS(2023)+"\770Frontespizio061.FDF")
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Chiedo se si vuole stampare anche i Quadri St e H del modello 770
    do GSRI_KSQ with this
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Lancio i batches standard che creano i cursori
    if this.w_STAMPAST = "S"
      * --- Stampo in formato PDF i prospetti del quadro ST
      do GSRI_BST with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if used("RITE3") 
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    if this.w_STAMPAH = "S"
      * --- Stampo in formato PDF i prospetti del quadro H
      do GSRI_BSC with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if used("RITE1") 
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    * --- Cancello i cursori
    if used("RITE1")
      select RITE1
      use
    endif
    if used("RITE3")
      select RITE3
      use
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Ritardo l'operazione successiva
    if type("g_PDFDELAY")="N"
      wait "" timeout g_PDFDELAY
    else
      wait "" timeout 5
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampa prospetto ST in PDF
    DECLARE ARRFDF (2,2)
    ARRFDF (1,1) = "CODFIS"
    ARRFDF (1,2) = 16
    ARRFDF (2,1) = "NUMMOD"
    ARRFDF (2,2) = 2
    this.w_DATIPRES = .F.
    * --- Creo il cursore dei dati
    L_CAMPI="CREATE CURSOR __TMP__ (CODFIS C(16), NUMMOD C(2), "
    this.w_QUADRO = 0
    do while this.w_QUADRO < 13
      this.w_QUADRO = this.w_QUADRO+1
      L_CAMPI=L_CAMPI+"DI"+ALLTRIM(STR(this.w_QUADRO))+"STM01 C(2), DI"+ALLTRIM(STR(this.w_QUADRO))+"STA01 C(4), "
      L_CAMPI=L_CAMPI+" DI"+ALLTRIM(STR(this.w_QUADRO))+"ST002 N(18,4), "
      L_CAMPI=L_CAMPI+"DI"+ALLTRIM(STR(this.w_QUADRO))+"ST003 N(18,4), DI"+ALLTRIM(STR(this.w_QUADRO))+"ST004 N(18,4), "
      L_CAMPI=L_CAMPI+"DI"+ALLTRIM(STR(this.w_QUADRO))+"ST005 N(18,4), DI"+ALLTRIM(STR(this.w_QUADRO))+"ST006 N(18,4), "
      L_CAMPI=L_CAMPI+"DI"+ALLTRIM(STR(this.w_QUADRO))+"ST007 C(254), DI"+ALLTRIM(STR(this.w_QUADRO))+"ST008 C(2), "
      L_CAMPI=L_CAMPI+"DI"+ALLTRIM(STR(this.w_QUADRO))+"ST009 C(16), DI"+ALLTRIM(STR(this.w_QUADRO))+"ST010 C(16), "
      L_CAMPI=L_CAMPI+"DI"+ALLTRIM(STR(this.w_QUADRO))+"ST011 C(2), DI"+ALLTRIM(STR(this.w_QUADRO))+"STG12 C(2), "
      L_CAMPI=L_CAMPI+"DI"+ALLTRIM(STR(this.w_QUADRO))+"STM12 C(2), DI"+ALLTRIM(STR(this.w_QUADRO))+"STA12 C(4), "
    enddo
    L_CAMPI=substr(L_CAMPI,1,len(L_CAMPI)-2)+")"
    &L_CAMPI
    select RITE3
    this.w_QUADRO = 0
    this.w_TRASMATT = 1
    L_CAMPI="INSERT INTO __TMP__ (CODFIS, NUMMOD, "
    L_VALORI=' VALUES ("'+this.oParentObject.w_FOCODFIS+'", "01","'
    do while !eof()
      select RITE3
      this.w_REGAGE = NVL(VPREGAGE,"")
      * --- Se c'� il flag regime agevolato il quadro ST non deve essere stampato
      if this.w_REGAGE<>"S"
        this.w_QUADRO = this.w_QUADRO+1
        L_CAMPI=L_CAMPI+"DI"+ALLTRIM(STR(this.w_QUADRO))+"STM01, DI"+ALLTRIM(STR(this.w_QUADRO))+"STA01, "
        L_CAMPI= L_CAMPI+ "DI"+ALLTRIM(STR(this.w_QUADRO))+"ST002, "
        L_CAMPI=L_CAMPI+"DI"+ALLTRIM(STR(this.w_QUADRO))+"ST003, DI"+ALLTRIM(STR(this.w_QUADRO))+"ST004, "
        L_CAMPI=L_CAMPI+"DI"+ALLTRIM(STR(this.w_QUADRO))+"ST005,  DI"+ALLTRIM(STR(this.w_QUADRO))+"ST006, "
        L_CAMPI=L_CAMPI+"DI"+ALLTRIM(STR(this.w_QUADRO))+"ST007, DI"+ALLTRIM(STR(this.w_QUADRO))+"ST008, "
        L_CAMPI=L_CAMPI+"DI"+ALLTRIM(STR(this.w_QUADRO))+"ST009, DI"+ALLTRIM(STR(this.w_QUADRO))+"ST010, "
        L_CAMPI=L_CAMPI+"DI"+ALLTRIM(STR(this.w_QUADRO))+"ST011, DI"+ALLTRIM(STR(this.w_QUADRO))+"STG12, "
        L_CAMPI=L_CAMPI+"DI"+ALLTRIM(STR(this.w_QUADRO))+"STM12, DI"+ALLTRIM(STR(this.w_QUADRO))+"STA12, "
        if this.w_quadro=1
          L_VALORI=L_VALORI+iif("A"$VP__NOTE OR "B"$VP__NOTE OR "D"$VP__NOTE OR "E"$VP__NOTE OR "M"$VP__NOTE OR "N"$VP__NOTE OR "P"$VP__NOTE,"12",right("00"+alltrim(str(MESE)),2))+'",'
          L_VALORI=L_VALORI+'"'+iif("A"$VP__NOTE OR "B"$VP__NOTE OR "D"$VP__NOTE OR "E"$VP__NOTE OR "M"$VP__NOTE OR "N"$VP__NOTE OR "P"$VP__NOTE,"2005",str(ANNO,4,0))+'"'
        else
          L_VALORI=L_VALORI+'"'+iif("A"$VP__NOTE OR "B"$VP__NOTE OR "D"$VP__NOTE OR "E"$VP__NOTE OR "M"$VP__NOTE OR "N"$VP__NOTE OR "P"$VP__NOTE,"12",right("00"+alltrim(str(MESE)),2))+'",'
          L_VALORI=L_VALORI+'"'+iif("A"$VP__NOTE OR "B"$VP__NOTE OR "D"$VP__NOTE OR "E"$VP__NOTE OR "M"$VP__NOTE OR "N"$VP__NOTE OR "P"$VP__NOTE,"2005",str(ANNO,4,0))+'"'
        endif
        this.w_CALCOLI = SOMTOTA-VPRITECC-VPRITCOM
        if VPCAUPRE # "X" and VPCAUPRE # "Y" and (SOMTOTA < 0 or VPRITECC < 0 or VPRITCOM < 0 or this.w_CALCOLI < 0 or VPIMPINT < 0)
          ah_ErrorMsg("Presenza di importi negativi nel prospetto ST. %0Impossibile generare il file","stop","")
          if used("__TMP__")
            Select __TMP__ 
 Use
          endif
          i_retcode = 'stop'
          return
        endif
        L_VALORI=L_VALORI+", "+ALLTRIM(STR(INT(IIF(SOMTOTA<0,0,SOMTOTA))))+", "+ALLTRIM(STR(INT(IIF(VPRITECC<0,0,VPRITECC))))+", "+ALLTRIM(STR(INT(IIF(VPRITCOM<0,0,VPRITCOM))))
        L_VALORI = L_VALORI +"," +ALLTRIM(STR(INT(IIF(VPIMPVER<0,0,VPIMPVER))))+","+ALLTRIM(STR(INT(IIF(VPIMPINT<0,0,VPIMPINT))))
        L_VALORI = L_VALORI+","+'"'+ALLTRIM(VP__NOTE)+'",' +'"'+ALLTRIM(VPEVEECC)+'","'+ALLTRIM(MRCODTRI)+'"'
        if VPTIPVER = "T"
          L_VALORI=L_VALORI+", "+'"'+"X"+'"'
        else
          L_VALORI=L_VALORI+", "+'" "'
        endif
        L_VALORI=L_VALORI+", "+'"'+iif(VPCODREG<>0,right("00"+iif(vpcodreg>9,STR(VPCODREG,2,0),str(vpcodreg,1,0)),2),"")+'"'
        * --- Campo 12
        if ! empty(nvl(VPDATVER,""))
          L_VALORI=L_VALORI+", "+'"'+right("00"+alltrim(str(day(VPDATVER))),2)+'"'
          L_VALORI=L_VALORI+", "+'"'+right("00"+alltrim(str(month(VPDATVER))),2)+'"'
          L_VALORI=L_VALORI+", "+'"'+str(year(VPDATVER),4,0)+'", '
        else
          L_VALORI=L_VALORI+", "+'"  "'
          L_VALORI=L_VALORI+", "+'"  "'
          L_VALORI=L_VALORI+", "+'"  "'+", "
        endif
        * --- Incremento il numero di Rigo
        if this.w_QUADRO >=12
          * --- Incremento il numero di Modello
          L_MACRO=substr(L_CAMPI,1,len(L_CAMPI)-2)+")"+substr(L_VALORI,1,len(L_VALORI)-2)+")"
          &L_MACRO
          this.w_QUADRO = 0
          this.w_TRASMATT = this.w_TRASMATT+1
          L_CAMPI="INSERT INTO __TMP__ (CODFIS, NUMMOD, "
          L_VALORI=' VALUES ("'+this.oParentObject.w_FOCODFIS+'", "'+RIGHT("00"+ALLTRIM(STR(this.w_TRASMATT,2,0)),2)+'","'
          this.w_DATIPRES = .T.
        endif
        skip in RITE3
      else
        skip in RITE3
      endif
    enddo
    if this.w_QUADRO>0
      L_MACRO=substr(L_CAMPI,1,len(L_CAMPI)-2)+")"+substr(L_VALORI,1,len(L_VALORI)-2)+")"
      &L_MACRO
      this.w_DATIPRES = .T.
    endif
    this.w_RESFDF = cp_ffdf(" ","Quadro_ST_06.pdf",this," ",@ARRFDF)
    * --- Se non mi ritorna un numero significa che ho incontrato un errore
    do case
      case Not Left( this.w_RESFDF , 1) $ "0123456789"
        * --- Errore durante creazione di un FDF..
        this.w_MESS = this.w_RESFDF
        ah_ErrorMsg(this.w_MESS,"stop","")
      case Left( this.w_RESFDF , 1) = "0"
        * --- Nessun FDF creato...
        ah_ErrorMsg("Nessun file PDF da generare","stop","")
      otherwise
        this.w_QUADRO = 1
        do while this.w_QUADRO<=Val( this.w_RESFDF )
          CP_CHPRN(SYS(2023)+"\Quadro_ST_06"+ALLTRIM(STR(this.w_QUADRO))+".FDF",.NULL.,2)
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_QUADRO = this.w_QUADRO+1
        enddo
    endcase
    if used("__TMP__")
      Select __TMP__ 
 Use
    endif
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampa  in formato PDF del quadro H
    this.w_ERROR = .F.
    this.w_DATCOMPL = .T.
    this.w_OLDPERCF = "*"
    this.w_OLDPER = ""
    DECLARE ARRFDF (2,2)
    ARRFDF (1,1) = "CODFIS"
    ARRFDF (1,2) = 16
    ARRFDF (2,1) = "NUMMOD"
    ARRFDF (2,2) = 8
    this.w_TRASMATT = 1
    * --- Creo cursore messaggistica di errore (Mancanza campi obbligatori)
    create cursor LISTAPERC (CODCON C(15),DENOMI C(40),CODFIS C(1),COGDEN C(1),NOME C(1),SESSO C(1), ; 
 DATNAS C(1),COMNAS C(1),COMUN C(1))
    * --- Creo il cursore dei dati
    L_CAMPI="CREATE CURSOR __TMP__ (CODFIS C(16), NUMMOD C(8), AU001 C(16), AU002 C(50), AU003 C(20), "
    L_CAMPI = L_CAMPI + "AU004 C(1), AU005G C(2), AU005M C(2), AU005A C(4), AU006 C(50), AU007 C(2), AU008 C(50), AU009 C(2),  "
    L_CAMPI = L_CAMPI + "AU011 C(100), AU012 C(20), AU013 C(50), AU014 C(100), AU015 C(5), AU016 C(2), AU017 C(4), AU019 N(18,4), "
    L_CAMPI = L_CAMPI +"AU020 N(18,4), AU021 N(18,4), AU022 N(18,4), AU023 N(18,4), AU031 N(18,4), AU032 N(18,4), "
    L_CAMPI = L_CAMPI + "AU033 N(18,4), AU034 N(18,4))"
    &L_CAMPI
    Select Rite1 
 Go Top
    do while !eof()
      * --- Record H
      L_CAMPI="INSERT INTO  __TMP__ (CODFIS , NUMMOD , AU001 , AU002 , AU003 , "
      L_CAMPI = L_CAMPI + "AU004 , AU005G , AU005M , AU005A , AU006 , AU007 , AU008 , AU009 ,  "
      L_CAMPI = L_CAMPI + "AU011 , AU012 , AU013 , AU014 , AU015 , AU016, AU017 , AU019 , "
      L_CAMPI = L_CAMPI +"AU020 , AU021 , AU022 , AU023 , AU031 , AU032, AU033, AU034)"
      select RITE1
      L_VALORI=' VALUES ("'+this.oParentObject.w_FOCODFIS+'", "'+RIGHT(REPL("0",8)+ALLTRIM(STR(this.w_TRASMATT)),8)+'",'
      this.w_MRCODCON = NVL(MRCODCON,"")
      this.w_ANDESCRI = NVL(ANDESCRI,"")
      * --- Verifico se percipiente estero
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANFLGEST,ANCOFISC,ANLOCALI,ANINDIRI,ANNAZION"+;
          " from "+i_cTable+" CONTI where ";
              +"ANTIPCON = "+cp_ToStrODBC("F");
              +" and ANCODICE = "+cp_ToStrODBC(this.w_MRCODCON);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANFLGEST,ANCOFISC,ANLOCALI,ANINDIRI,ANNAZION;
          from (i_cTable) where;
              ANTIPCON = "F";
              and ANCODICE = this.w_MRCODCON;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PERCESTE = NVL(cp_ToDate(_read_.ANFLGEST),cp_NullValue(_read_.ANFLGEST))
        this.w_CFPEREST = NVL(cp_ToDate(_read_.ANCOFISC),cp_NullValue(_read_.ANCOFISC))
        this.w_LOCALEST = NVL(cp_ToDate(_read_.ANLOCALI),cp_NullValue(_read_.ANLOCALI))
        this.w_INDIREST = NVL(cp_ToDate(_read_.ANINDIRI),cp_NullValue(_read_.ANINDIRI))
        this.w_NAZIOEST = NVL(cp_ToDate(_read_.ANNAZION),cp_NullValue(_read_.ANNAZION))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if EMPTY(nvl(ANCODFIS,""))
        this.w_DATCOMPL = .F.
        INSERT INTO LISTAPERC VALUES (this.w_MRCODCON,this.w_ANDESCRI,"1","0","0","0","0","0","0")
        select RITE1
      endif
      if iif(ANPERFIS="S",EMPTY(nvl(ANCOGNOM,"")),EMPTY(nvl(ANDESCRI,"")))
        this.w_DATCOMPL = .F.
        INSERT INTO LISTAPERC VALUES (this.w_MRCODCON,this.w_ANDESCRI,"0","1","0","0","0","0","0")
        select RITE1
      endif
      if EMPTY(nvl(ANLOCALI,""))
        this.w_DATCOMPL = .F.
        INSERT INTO LISTAPERC VALUES (this.w_MRCODCON,this.w_ANDESCRI,"0","0","0","0","0","0","1")
        select RITE1
      endif
      if EMPTY(nvl(AN__NOME,"")) and ANPERFIS="S"
        this.w_DATCOMPL = .F.
        INSERT INTO LISTAPERC VALUES (this.w_MRCODCON,this.w_ANDESCRI,"0","0","1","0","0","0","0")
        select RITE1
      endif
      if EMPTY(nvl(AN_SESSO,"")) and ANPERFIS="S"
        this.w_DATCOMPL = .F.
        INSERT INTO LISTAPERC VALUES (this.w_MRCODCON,this.w_ANDESCRI,"0","0","0","1","0","0","0")
        select RITE1
      endif
      if EMPTY(nvl(ANDATNAS,"")) and ANPERFIS="S"
        this.w_DATCOMPL = .F.
        INSERT INTO LISTAPERC VALUES (this.w_MRCODCON,this.w_ANDESCRI,"0","0","0","0","1","0","0")
        select RITE1
      endif
      if EMPTY(nvl(ANLOCNAS,"")) and ANPERFIS="S"
        this.w_DATCOMPL = .F.
        INSERT INTO LISTAPERC VALUES (this.w_MRCODCON,this.w_ANDESCRI,"","0","0","0","0","1","0")
        select RITE1
      endif
      if this.w_PERCESTE="S"
        L_VALORI = L_VALORI +'"                ",'
      else
        L_VALORI = L_VALORI + '"'+NVL(ANCODFIS,"")+'",'
      endif
      * --- Posizione 30 record posizionale
      this.w_PERCIPCF = ANCODFIS
      L_VALORI = L_VALORI +' "'+iif(ANPERFIS="S",upper(NVL(ANCOGNOM,"")),upper(NVL(ANDESCRI,"")))+'",'
      L_VALORI = L_VALORI + '"'+iif(ANPERFIS="S",upper(NVL(AN__NOME,"")),"")+'",'
      L_VALORI = L_VALORI + '"'+iif(ANPERFIS="S",upper(NVL(AN_SESSO,"")),"")+'",'
      L_VALORI = L_VALORI + '"'+iif(ANPERFIS="S",ALLTRIM(NVL(STR(DAY(ANDATNAS))," ")),"  ")+'",'
      L_VALORI = L_VALORI + '"'+iif(ANPERFIS="S",ALLTRIM(NVL(STR(MONTH(ANDATNAS))," ")),"  ")+'",'
      L_VALORI = L_VALORI + '"'+iif(ANPERFIS="S",ALLTRIM(NVL(STR(YEAR(ANDATNAS))," ")),"  ")+'",'
      L_VALORI = L_VALORI + '"'+iif(ANPERFIS="S",upper(NVL(ANLOCNAS,"")),"")+'",'
      L_VALORI = L_VALORI + '"'+iif(ANPERFIS="S",upper(NVL(ANPRONAS,"")),"")+'",'
      this.w_PROVIN = UPPER(NVL(ANPROVIN,""))
      * --- Leggo i dati della residenza
      * --- Select from DES_DIVE
      i_nConn=i_TableProp[this.DES_DIVE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DES_DIVE_idx,2],.t.,this.DES_DIVE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select DDLOCALI,DDPROVIN,DDCODNAZ,DDINDIRI  from "+i_cTable+" DES_DIVE ";
            +" where DDTIPCON='F' and DDCODICE="+cp_ToStrODBC(this.w_MRCODCON)+" and DDTIPRIF = 'RE' ";
             ,"_Curs_DES_DIVE")
      else
        select DDLOCALI,DDPROVIN,DDCODNAZ,DDINDIRI from (i_cTable);
         where DDTIPCON="F" and DDCODICE=this.w_MRCODCON and DDTIPRIF = "RE" ;
          into cursor _Curs_DES_DIVE
      endif
      if used('_Curs_DES_DIVE')
        select _Curs_DES_DIVE
        locate for 1=1
        do while not(eof())
        this.w_LOCALEST = _Curs_DES_DIVE.DDLOCALI
        this.w_INDIREST = _Curs_DES_DIVE.DDINDIRI
        this.w_NAZIOEST = _Curs_DES_DIVE.DDCODNAZ
        this.w_PROVIN = _Curs_DES_DIVE.DDPROVIN
        Exit
          select _Curs_DES_DIVE
          continue
        enddo
        use
      endif
      Select RITE1
      * --- Riservato ai percipienti esteri
      if this.w_PERCESTE = "S"
        * --- Inserisco valori vuoti per AU008,AU009 e AU011
        L_VALORI = L_VALORI + '"        ",'
        L_VALORI = L_VALORI + '"        ",'
        L_VALORI = L_VALORI + '"        ",'
        * --- Gestisco i campi riservati al percipiente estero
        * --- Codice Fiscale estero
        if Empty(this.w_CFPEREST)
          L_VALORI = L_VALORI + '"'+UPPER(NVL(ANCODFIS,""))+'",'
        else
          L_VALORI = L_VALORI + '"'+upper(this.w_CFPEREST)+'",'
        endif
        * --- Localit� di residenza estera
        L_VALORI = L_VALORI + '"'+UPPER(this.w_LOCALEST)+'",'
        * --- Via e numero civico
        L_VALORI = L_VALORI + '"'+UPPER(this.w_INDIREST)+'",'
        * --- Codice stato estero
        * --- Read from NAZIONI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.NAZIONI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.NAZIONI_idx,2],.t.,this.NAZIONI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "NACODEST"+;
            " from "+i_cTable+" NAZIONI where ";
                +"NACODNAZ = "+cp_ToStrODBC(this.w_NAZIOEST);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            NACODEST;
            from (i_cTable) where;
                NACODNAZ = this.w_NAZIOEST;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_NACODEST = NVL(cp_ToDate(_read_.NACODEST),cp_NullValue(_read_.NACODEST))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        L_VALORI = L_VALORI + '"'+RIGHT(repl("0",3)+UPPER(ALLTRIM(this.w_NACODEST)),3)+'",'
      else
        L_VALORI = L_VALORI + '"'+upper(NVL(this.w_LOCALEST,""))+'",'
        L_VALORI = L_VALORI + '"'+upper(this.w_PROVIN)+'",'
        L_VALORI = L_VALORI + '"'+upper(NVL(this.w_INDIREST,""))+'",'
        * --- Inserisco valori vuoti per AU012,AU013,AU014  e AU015
        L_VALORI = L_VALORI + '"        ",'
        L_VALORI = L_VALORI + '"        ",'
        L_VALORI = L_VALORI + '"        ",'
        L_VALORI = L_VALORI + '"        ",'
      endif
      this.w_CAUPRE = UPPER(NVL(TRCAUPRE," "))
      * --- Dati relativi alle somme erogate
      L_VALORI = L_VALORI + '"'+upper(NVL(TRCAUPRE,""))+'",'
      L_VALORI = L_VALORI + '"'+alltrim(str(year(this.w_DATFIN)))+'",'
      L_VALORI = L_VALORI + STR(INT(IIF(NVL(SOMTOTA,0)<0,0,NVL(SOMTOTA,0))))+","
      if this.w_PERCESTE = "S" and ANFLSGRE="S"
        L_VALORI = L_VALORI + STR(INT(IIF(NVL(NONSOGG,0)<0,0,NVL(NONSOGG,0))))+","
        * --- Inserisco il valore 0 pre il campo AU021
        L_VALORI = L_VALORI +"0"+","
      else
        * --- Inserisco il valore 0 pre il campo AU020
        L_VALORI = L_VALORI +"0"+","
        L_VALORI = L_VALORI + STR(INT(IIF(NVL(NONSOGG,0)<0,0,NVL(NONSOGG,0))))+","
      endif
      L_VALORI = L_VALORI + STR(INT(IIF(NVL(IMPONI,0)<0,0,NVL(IMPONI,0))))+","
      L_VALORI = L_VALORI + STR(INT(IIF(NVL(MRTOTIM1,0)<0,0,NVL(MRTOTIM1,0))))+","
      if this.w_CAUPRE $ "M-V-C"
        select MRCODCON,SUM(MRTOTIM2) as MRTOTIM2,SUM(MRIMPPER) as MRIMPPER,TRCAUPRE ; 
 from RITE1 where TRCAUPRE=this.w_CAUPRE and MRCODCON=this.w_MRCODCON group by MRCODCON,TRCAUPRE into cursor TOTPRE
        L_VALORI = L_VALORI + STR(IIF(cp_ROUND(NVL(TOTPRE.MRTOTIM2,0) -NVL(TOTPRE.MRIMPPER,0),0)<0,0,cp_ROUND(NVL(TOTPRE.MRTOTIM2,0) -NVL(TOTPRE.MRIMPPER,0),0)))+","
        L_VALORI = L_VALORI + STR(cp_ROUND(IIF(NVL(TOTPRE.MRIMPPER,0)<0,0,NVL(TOTPRE.MRIMPPER,0)),0))+","
        * --- Elimino il cursore dei totali per causale prestazione
        if used("TOTPRE")
          select TOTPRE
          use
        endif
      else
        L_VALORI = L_VALORI + "0"+ " ,"
        L_VALORI = L_VALORI + "0"+ " ,"
      endif
      Select RITE1
      * --- Campi AU033 e AU034
      L_VALORI = L_VALORI + STR(cp_ROUND(IIF(NVL(MRSPERIM,0)<0,0,NVL(MRSPERIM,0)),0))+","
      if RITERIMB < 0 and TRCAUPRE $ "X-Y"
        L_VALORI = L_VALORI + STR(ABS(cp_ROUND(NVL(RITERIMB,0),0)))+")"
      else
        L_VALORI = L_VALORI + "0)"
      endif
      * --- Inserisco il record
      L_MACRO = L_CAMPI+L_VALORI
      &L_MACRO
      * --- Verifico se sono presenti tutti i campi obbligatori per il quadro H
      if ! this.w_DATCOMPL
        this.w_ERROR = .T.
        * --- Riassegno a true la variabile di controllo su presenza campi obbligatori, in modo da poter continuare la stampa dei restanti quadri H
        this.w_DATCOMPL = .T.
      else
        this.w_TRASMATT = this.w_TRASMATT + 1
      endif
      Select RITE1
      skip in RITE1
    enddo
    * --- Stampa quadro H attuale
    if ! this.w_ERROR
      this.w_RESFDF = cp_ffdf(" ","Quadro_H_06.pdf",this," ",@ARRFDF)
      * --- Se non mi ritorna un numero significa che ho incontrato un errore
      do case
        case Not Left( this.w_RESFDF , 1) $ "0123456789"
          * --- Errore durante creazione di un FDF..
          this.w_MESS = this.w_RESFDF
          ah_ErrorMsg(this.w_MESS,"stop","")
        case Left( this.w_RESFDF , 1) = "0"
          * --- Nessun FDF creato...
          this.w_MESS = "Nessun file PDF da generare."
          ah_ErrorMsg("Nessun file PDF da generare","stop","")
        otherwise
          this.w_QUADRO = 1
          do while this.w_QUADRO<=Val( this.w_RESFDF )
            CP_CHPRN(SYS(2023)+"\Quadro_H_06"+ALLTRIM(STR(this.w_QUADRO))+".FDF",.NULL.,2)
            this.Pag2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_QUADRO = this.w_QUADRO+1
          enddo
      endcase
    else
      if ah_YesNo("Dati anagrafici percipienti mancanti %0Impossibile stampare i quadri H. %0Vuoi visualizzare la stampa di verifica?")
        Select CODCON,DENOMI,MAX(CODFIS) as CODFIS,MAX(COGDEN) as COGDEN,MAX(NOME) as NOME, ; 
 MAX(SESSO) as SESSO, MAX(DATNAS) as DATNAS,MAX(COMNAS) as COMNAS,MAX(COMUN) as COMUN ; 
 from LISTAPERC into cursor __TMP__ group by CODCON,DENOMI order by CODCON
        CP_CHPRN("..\RITE\EXE\QUERY\GSRI1BFT.FRX")
      endif
    endif
    if used("__TMP__")
      select __TMP__ 
 use
    endif
    if used("LISTAPERC")
      select LISTAPERC 
 use
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='DES_DIVE'
    this.cWorkTables[4]='NAZIONI'
    return(this.OpenAllTables(4))

  proc CloseCursors()
    if used('_Curs_DES_DIVE')
      use in _Curs_DES_DIVE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
