* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_bpr                                                        *
*              Trova percentuale ritenuta                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][98][VRS_18]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-09-22                                                      *
* Last revis.: 2009-12-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParam
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsri_bpr",oParentObject,m.pParam)
return(i_retval)

define class tgsri_bpr as StdBatch
  * --- Local variables
  pParam = space(1)
  w_CODAZI = space(5)
  * --- WorkFile variables
  TAB_RITE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch per l'inserimento automatico della percentuale ritenuta al cambiamento del codice tributo da (GSRI_AMR)
    this.w_CODAZI = i_CODAZI
    * --- Read from TAB_RITE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.TAB_RITE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TAB_RITE_idx,2],.t.,this.TAB_RITE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "TRPERRIT"+;
        " from "+i_cTable+" TAB_RITE where ";
            +"TRCODTRI = "+cp_ToStrODBC(this.oParentObject.w_MRCODTRI);
            +" and TRCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
            +" and TRTIPRIT = "+cp_ToStrODBC(this.oParentObject.w_TIPRIT);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        TRPERRIT;
        from (i_cTable) where;
            TRCODTRI = this.oParentObject.w_MRCODTRI;
            and TRCODAZI = this.w_CODAZI;
            and TRTIPRIT = this.oParentObject.w_TIPRIT;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.oParentObject.W_MRPERRI1 = NVL(cp_ToDate(_read_.TRPERRIT),cp_NullValue(_read_.TRPERRIT))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
  endproc


  proc Init(oParentObject,pParam)
    this.pParam=pParam
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='TAB_RITE'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParam"
endproc
