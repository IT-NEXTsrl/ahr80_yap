* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_kdy                                                        *
*              Manutenzione dati estratti record SY                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2015-12-28                                                      *
* Last revis.: 2018-09-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- gsri_kdy
* ---- Parametro Gestione
 * ---- G: Generazione dato estratto record SY
 * ---- M: Manutenzione dato estratto
ptipEst=oparentobject
* --- Fine Area Manuale
return(createobject("tgsri_kdy",oParentObject))

* --- Class definition
define class tgsri_kdy as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 794
  Height = 481+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2018-09-20"
  HelpContextID=82441833
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=22

  * --- Constant Properties
  _IDX = 0
  CONTI_IDX = 0
  cPrg = "gsri_kdy"
  cComment = "Manutenzione dati estratti record SY"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_TIPEST = space(1)
  o_TIPEST = space(1)
  w_SYTIPCON = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_DUPRECSY = space(10)
  w_SY__ANNO = 0
  w_VISPEREST = space(1)
  w_SYDAVERI = space(1)
  w_SYSCLGEN = space(1)
  w_SYCAUPRE = space(2)
  w_SYCODCON = space(15)
  w_SYCODCONF = space(15)
  w_SYCOFISC = space(25)
  w_SYDESCRI = space(60)
  w_SYDESCRIF = space(60)
  w_ANRITENU = space(1)
  w_ANRITENUF = space(1)
  w_ANFLGEST = space(1)
  w_ANCODFIS = space(16)
  w_ANFLGESTF = space(1)
  w_ANCODFISF = space(16)
  w_CHSERIAL = space(10)
  w_SYSERIAL = space(10)
  w_ZoomSY = .NULL.
  w_ZoomCU = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsri_kdyPag1","gsri_kdy",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Filtri")
      .Pages(2).addobject("oPag","tgsri_kdyPag2","gsri_kdy",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Visualizzazione")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDUPRECSY_1_4
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- gsri_kdy
    * --- Imposta il Titolo della Finestra del Documento in Base al modello
    WITH THIS.PARENT
              pTipEst=oparentobject
              DO CASE
                 CASE pTipEst == 'G'
                       .cComment =CP_TRANSLATE('Generazione dati record SY')
                 CASE pTipEst == 'M'
                       .cComment = CP_TRANSLATE('Manutenzione dati record SY')
             ENDCASE
    ENDWITH
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomSY = this.oPgFrm.Pages(2).oPag.ZoomSY
    this.w_ZoomCU = this.oPgFrm.Pages(2).oPag.ZoomCU
    DoDefault()
    proc Destroy()
      this.w_ZoomSY = .NULL.
      this.w_ZoomCU = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='CONTI'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIPEST=space(1)
      .w_SYTIPCON=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_DUPRECSY=space(10)
      .w_SY__ANNO=0
      .w_VISPEREST=space(1)
      .w_SYDAVERI=space(1)
      .w_SYSCLGEN=space(1)
      .w_SYCAUPRE=space(2)
      .w_SYCODCON=space(15)
      .w_SYCODCONF=space(15)
      .w_SYCOFISC=space(25)
      .w_SYDESCRI=space(60)
      .w_SYDESCRIF=space(60)
      .w_ANRITENU=space(1)
      .w_ANRITENUF=space(1)
      .w_ANFLGEST=space(1)
      .w_ANCODFIS=space(16)
      .w_ANFLGESTF=space(1)
      .w_ANCODFISF=space(16)
      .w_CHSERIAL=space(10)
      .w_SYSERIAL=space(10)
        .w_TIPEST = pTipEst
        .w_SYTIPCON = 'F'
        .w_OBTEST = CTOD('01-01-1900')
        .w_DUPRECSY = 'N'
        .w_SY__ANNO = year(i_DATSYS)-1
        .w_VISPEREST = iif(.w_Tipest='M','S','N')
        .w_SYDAVERI = 'T'
        .w_SYSCLGEN = 'T'
        .w_SYCAUPRE = 'TT'
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_SYCODCON))
          .link_1_10('Full')
        endif
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_SYCODCONF))
          .link_1_11('Full')
        endif
      .oPgFrm.Page2.oPag.ZoomSY.Calculate()
      .oPgFrm.Page2.oPag.ZoomCU.Calculate()
          .DoRTCalc(12,20,.f.)
        .w_CHSERIAL = NVL(.w_ZoomCu.getVar('Chserial'), ' ')
        .w_SYSERIAL = NVL(.w_ZoomSy.getVar('Syserial'), ' ')
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_21.enabled = this.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_22.enabled = this.oPgFrm.Page1.oPag.oBtn_1_22.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_5.enabled = this.oPgFrm.Page2.oPag.oBtn_2_5.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_6.enabled = this.oPgFrm.Page2.oPag.oBtn_2_6.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_9.enabled = this.oPgFrm.Page2.oPag.oBtn_2_9.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
            .w_TIPEST = pTipEst
        .DoRTCalc(2,5,.t.)
        if .o_Tipest<>.w_Tipest
            .w_VISPEREST = iif(.w_Tipest='M','S','N')
        endif
        .oPgFrm.Page2.oPag.ZoomSY.Calculate()
        .oPgFrm.Page2.oPag.ZoomCU.Calculate()
        .DoRTCalc(7,20,.t.)
            .w_CHSERIAL = NVL(.w_ZoomCu.getVar('Chserial'), ' ')
            .w_SYSERIAL = NVL(.w_ZoomSy.getVar('Syserial'), ' ')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page2.oPag.ZoomSY.Calculate()
        .oPgFrm.Page2.oPag.ZoomCU.Calculate()
    endwith
  return

  proc Calculate_GWNDNOCQZH()
    with this
          * --- ActivatePage 2
          Gsri_bdy(this;
              ,'B';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oDUPRECSY_1_4.visible=!this.oPgFrm.Page1.oPag.oDUPRECSY_1_4.mHide()
    this.oPgFrm.Page1.oPag.oVISPEREST_1_6.visible=!this.oPgFrm.Page1.oPag.oVISPEREST_1_6.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_13.visible=!this.oPgFrm.Page1.oPag.oStr_1_13.mHide()
    this.oPgFrm.Page2.oPag.oBtn_2_5.visible=!this.oPgFrm.Page2.oPag.oBtn_2_5.mHide()
    this.oPgFrm.Page2.oPag.oBtn_2_9.visible=!this.oPgFrm.Page2.oPag.oBtn_2_9.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_30.visible=!this.oPgFrm.Page1.oPag.oStr_1_30.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_10.visible=!this.oPgFrm.Page2.oPag.oStr_2_10.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_11.visible=!this.oPgFrm.Page2.oPag.oStr_2_11.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_12.visible=!this.oPgFrm.Page2.oPag.oStr_2_12.mHide()
    this.oPgFrm.Page2.oPag.oBtn_2_13.visible=!this.oPgFrm.Page2.oPag.oBtn_2_13.mHide()
    this.oPgFrm.Page2.oPag.oBtn_2_14.visible=!this.oPgFrm.Page2.oPag.oBtn_2_14.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_31.visible=!this.oPgFrm.Page1.oPag.oStr_1_31.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_32.visible=!this.oPgFrm.Page1.oPag.oStr_1_32.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page2.oPag.ZoomSY.Event(cEvent)
      .oPgFrm.Page2.oPag.ZoomCU.Event(cEvent)
        if lower(cEvent)==lower("ActivatePage 2")
          .Calculate_GWNDNOCQZH()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=SYCODCON
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SYCODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFR',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_SYCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_SYTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU,ANFLGEST,ANCODFIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_SYTIPCON;
                     ,'ANCODICE',trim(this.w_SYCODCON))
          select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU,ANFLGEST,ANCODFIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SYCODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_SYCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_SYTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU,ANFLGEST,ANCODFIS";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_SYCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_SYTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU,ANFLGEST,ANCODFIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_SYCODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oSYCODCON_1_10'),i_cWhere,'GSAR_AFR',"Elenco fornitori",'GSRI_ADY.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_SYTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU,ANFLGEST,ANCODFIS";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU,ANFLGEST,ANCODFIS;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU,ANFLGEST,ANCODFIS";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_SYTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU,ANFLGEST,ANCODFIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SYCODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU,ANFLGEST,ANCODFIS";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_SYCODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_SYTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_SYTIPCON;
                       ,'ANCODICE',this.w_SYCODCON)
            select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU,ANFLGEST,ANCODFIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SYCODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_SYDESCRI = NVL(_Link_.ANDESCRI,space(60))
      this.w_ANRITENU = NVL(_Link_.ANRITENU,space(1))
      this.w_ANFLGEST = NVL(_Link_.ANFLGEST,space(1))
      this.w_ANCODFIS = NVL(_Link_.ANCODFIS,space(16))
    else
      if i_cCtrl<>'Load'
        this.w_SYCODCON = space(15)
      endif
      this.w_SYDESCRI = space(60)
      this.w_ANRITENU = space(1)
      this.w_ANFLGEST = space(1)
      this.w_ANCODFIS = space(16)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((Empty(.w_Sycodconf)) Or (.w_Sycodcon<=.w_Sycodconf)) And .w_Anritenu $ 'CS' And .w_Anflgest='S' And (.w_Visperest='S' Or Empty(.w_Ancodfis))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_SYCODCON = space(15)
        this.w_SYDESCRI = space(60)
        this.w_ANRITENU = space(1)
        this.w_ANFLGEST = space(1)
        this.w_ANCODFIS = space(16)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SYCODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SYCODCONF
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SYCODCONF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFR',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_SYCODCONF)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_SYTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU,ANFLGEST,ANCODFIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_SYTIPCON;
                     ,'ANCODICE',trim(this.w_SYCODCONF))
          select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU,ANFLGEST,ANCODFIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SYCODCONF)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_SYCODCONF)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_SYTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU,ANFLGEST,ANCODFIS";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_SYCODCONF)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_SYTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU,ANFLGEST,ANCODFIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_SYCODCONF) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oSYCODCONF_1_11'),i_cWhere,'GSAR_AFR',"Elenco fornitori",'GSRI_ADY.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_SYTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU,ANFLGEST,ANCODFIS";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU,ANFLGEST,ANCODFIS;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU,ANFLGEST,ANCODFIS";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_SYTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU,ANFLGEST,ANCODFIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SYCODCONF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU,ANFLGEST,ANCODFIS";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_SYCODCONF);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_SYTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_SYTIPCON;
                       ,'ANCODICE',this.w_SYCODCONF)
            select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU,ANFLGEST,ANCODFIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SYCODCONF = NVL(_Link_.ANCODICE,space(15))
      this.w_SYDESCRIF = NVL(_Link_.ANDESCRI,space(60))
      this.w_ANRITENUF = NVL(_Link_.ANRITENU,space(1))
      this.w_ANFLGESTF = NVL(_Link_.ANFLGEST,space(1))
      this.w_ANCODFISF = NVL(_Link_.ANCODFIS,space(16))
    else
      if i_cCtrl<>'Load'
        this.w_SYCODCONF = space(15)
      endif
      this.w_SYDESCRIF = space(60)
      this.w_ANRITENUF = space(1)
      this.w_ANFLGESTF = space(1)
      this.w_ANCODFISF = space(16)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((.w_Sycodconf>=.w_Sycodcon) Or Empty(.w_Sycodcon)) AND .w_Anritenuf $ 'CS' And .w_Anflgestf='S' And (.w_Visperest='S' Or Empty(.w_Ancodfisf))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_SYCODCONF = space(15)
        this.w_SYDESCRIF = space(60)
        this.w_ANRITENUF = space(1)
        this.w_ANFLGESTF = space(1)
        this.w_ANCODFISF = space(16)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SYCODCONF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDUPRECSY_1_4.RadioValue()==this.w_DUPRECSY)
      this.oPgFrm.Page1.oPag.oDUPRECSY_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSY__ANNO_1_5.value==this.w_SY__ANNO)
      this.oPgFrm.Page1.oPag.oSY__ANNO_1_5.value=this.w_SY__ANNO
    endif
    if not(this.oPgFrm.Page1.oPag.oVISPEREST_1_6.RadioValue()==this.w_VISPEREST)
      this.oPgFrm.Page1.oPag.oVISPEREST_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSYDAVERI_1_7.RadioValue()==this.w_SYDAVERI)
      this.oPgFrm.Page1.oPag.oSYDAVERI_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSYSCLGEN_1_8.RadioValue()==this.w_SYSCLGEN)
      this.oPgFrm.Page1.oPag.oSYSCLGEN_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSYCAUPRE_1_9.RadioValue()==this.w_SYCAUPRE)
      this.oPgFrm.Page1.oPag.oSYCAUPRE_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSYCODCON_1_10.value==this.w_SYCODCON)
      this.oPgFrm.Page1.oPag.oSYCODCON_1_10.value=this.w_SYCODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oSYCODCONF_1_11.value==this.w_SYCODCONF)
      this.oPgFrm.Page1.oPag.oSYCODCONF_1_11.value=this.w_SYCODCONF
    endif
    if not(this.oPgFrm.Page1.oPag.oSYCOFISC_1_12.value==this.w_SYCOFISC)
      this.oPgFrm.Page1.oPag.oSYCOFISC_1_12.value=this.w_SYCOFISC
    endif
    if not(this.oPgFrm.Page1.oPag.oSYDESCRI_1_16.value==this.w_SYDESCRI)
      this.oPgFrm.Page1.oPag.oSYDESCRI_1_16.value=this.w_SYDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oSYDESCRIF_1_20.value==this.w_SYDESCRIF)
      this.oPgFrm.Page1.oPag.oSYDESCRIF_1_20.value=this.w_SYDESCRIF
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_SY__ANNO)) or not(.w_SY__ANNO>=2015))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSY__ANNO_1_5.SetFocus()
            i_bnoObbl = !empty(.w_SY__ANNO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(((Empty(.w_Sycodconf)) Or (.w_Sycodcon<=.w_Sycodconf)) And .w_Anritenu $ 'CS' And .w_Anflgest='S' And (.w_Visperest='S' Or Empty(.w_Ancodfis)))  and not(empty(.w_SYCODCON))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSYCODCON_1_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(((.w_Sycodconf>=.w_Sycodcon) Or Empty(.w_Sycodcon)) AND .w_Anritenuf $ 'CS' And .w_Anflgestf='S' And (.w_Visperest='S' Or Empty(.w_Ancodfisf)))  and not(empty(.w_SYCODCONF))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSYCODCONF_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TIPEST = this.w_TIPEST
    return

enddefine

* --- Define pages as container
define class tgsri_kdyPag1 as StdContainer
  Width  = 790
  height = 481
  stdWidth  = 790
  stdheight = 481
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDUPRECSY_1_4 as StdCheck with uid="XCHCLOMJHK",rtseq=4,rtrep=.f.,left=175, top=19, caption="Duplica record SY",;
    ToolTipText = "Se attivo, consente di duplicare i dati estratti SY gi� comunicati in un file telematico",;
    HelpContextID = 45966223,;
    cFormVar="w_DUPRECSY", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDUPRECSY_1_4.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oDUPRECSY_1_4.GetRadio()
    this.Parent.oContained.w_DUPRECSY = this.RadioValue()
    return .t.
  endfunc

  func oDUPRECSY_1_4.SetRadio()
    this.Parent.oContained.w_DUPRECSY=trim(this.Parent.oContained.w_DUPRECSY)
    this.value = ;
      iif(this.Parent.oContained.w_DUPRECSY=='S',1,;
      0)
  endfunc

  func oDUPRECSY_1_4.mHide()
    with this.Parent.oContained
      return (.w_Tipest='G')
    endwith
  endfunc

  add object oSY__ANNO_1_5 as StdField with uid="JBAYHUASCK",rtseq=5,rtrep=.f.,;
    cFormVar = "w_SY__ANNO", cQueryName = "SY__ANNO",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Anno di riferimento",;
    HelpContextID = 227235957,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=175, Top=66, cSayPict='"9999"', cGetPict='"9999"'

  func oSY__ANNO_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_SY__ANNO>=2015)
    endwith
    return bRes
  endfunc

  add object oVISPEREST_1_6 as StdCheck with uid="UPOXZBKAWO",rtseq=6,rtrep=.f.,left=491, top=66, caption="Visualizza tutti i percipienti esteri",;
    ToolTipText = "Se attivo, visualizza anche i percipienti esteri con codice fiscale",;
    HelpContextID = 239366679,;
    cFormVar="w_VISPEREST", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oVISPEREST_1_6.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oVISPEREST_1_6.GetRadio()
    this.Parent.oContained.w_VISPEREST = this.RadioValue()
    return .t.
  endfunc

  func oVISPEREST_1_6.SetRadio()
    this.Parent.oContained.w_VISPEREST=trim(this.Parent.oContained.w_VISPEREST)
    this.value = ;
      iif(this.Parent.oContained.w_VISPEREST=='S',1,;
      0)
  endfunc

  func oVISPEREST_1_6.mHide()
    with this.Parent.oContained
      return (.w_Tipest='M')
    endwith
  endfunc


  add object oSYDAVERI_1_7 as StdCombo with uid="HHHJPCDAST",rtseq=7,rtrep=.f.,left=175,top=114,width=73,height=22;
    , ToolTipText = "Dati estratti da verificare";
    , HelpContextID = 96184431;
    , cFormVar="w_SYDAVERI",RowSource=""+"Si,"+"No,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSYDAVERI_1_7.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oSYDAVERI_1_7.GetRadio()
    this.Parent.oContained.w_SYDAVERI = this.RadioValue()
    return .t.
  endfunc

  func oSYDAVERI_1_7.SetRadio()
    this.Parent.oContained.w_SYDAVERI=trim(this.Parent.oContained.w_SYDAVERI)
    this.value = ;
      iif(this.Parent.oContained.w_SYDAVERI=='S',1,;
      iif(this.Parent.oContained.w_SYDAVERI=='N',2,;
      iif(this.Parent.oContained.w_SYDAVERI=='T',3,;
      0)))
  endfunc


  add object oSYSCLGEN_1_8 as StdCombo with uid="BQLKCHSUNV",rtseq=8,rtrep=.f.,left=491,top=114,width=73,height=22;
    , ToolTipText = "Escludi da generazione";
    , HelpContextID = 148989836;
    , cFormVar="w_SYSCLGEN",RowSource=""+"Si,"+"No,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSYSCLGEN_1_8.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oSYSCLGEN_1_8.GetRadio()
    this.Parent.oContained.w_SYSCLGEN = this.RadioValue()
    return .t.
  endfunc

  func oSYSCLGEN_1_8.SetRadio()
    this.Parent.oContained.w_SYSCLGEN=trim(this.Parent.oContained.w_SYSCLGEN)
    this.value = ;
      iif(this.Parent.oContained.w_SYSCLGEN=='S',1,;
      iif(this.Parent.oContained.w_SYSCLGEN=='N',2,;
      iif(this.Parent.oContained.w_SYSCLGEN=='T',3,;
      0)))
  endfunc


  add object oSYCAUPRE_1_9 as StdCombo with uid="MERGWDMNOJ",rtseq=9,rtrep=.f.,left=175,top=178,width=73,height=22;
    , ToolTipText = "Causale prestazione";
    , HelpContextID = 11245675;
    , cFormVar="w_SYCAUPRE",RowSource=""+"Tipo A,"+"Tipo B,"+"Tipo C,"+"Tipo D,"+"Tipo E,"+"Tipo F,"+"Tipo G,"+"Tipo H,"+"Tipo I,"+"Tipo J,"+"Tipo K,"+"Tipo L,"+"Tipo L1,"+"Tipo M,"+"Tipo M1,"+"Tipo M2,"+"Tipo N,"+"Tipo O,"+"Tipo O1,"+"Tipo P,"+"Tipo Q,"+"Tipo R,"+"Tipo S,"+"Tipo T,"+"Tipo U,"+"Tipo V,"+"Tipo V1,"+"Tipo V2,"+"Tipo W,"+"Tipo X,"+"Tipo Y,"+"Tipo Z,"+"Tipo ZO,"+"Tutte", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSYCAUPRE_1_9.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'B',;
    iif(this.value =3,'C',;
    iif(this.value =4,'D',;
    iif(this.value =5,'E',;
    iif(this.value =6,'F',;
    iif(this.value =7,'G',;
    iif(this.value =8,'H',;
    iif(this.value =9,'I',;
    iif(this.value =10,'J',;
    iif(this.value =11,'K',;
    iif(this.value =12,'L',;
    iif(this.value =13,'L1',;
    iif(this.value =14,'M',;
    iif(this.value =15,'M1',;
    iif(this.value =16,'M2',;
    iif(this.value =17,'N',;
    iif(this.value =18,'O',;
    iif(this.value =19,'O1',;
    iif(this.value =20,'P',;
    iif(this.value =21,'Q',;
    iif(this.value =22,'R',;
    iif(this.value =23,'S',;
    iif(this.value =24,'T',;
    iif(this.value =25,'U',;
    iif(this.value =26,'V',;
    iif(this.value =27,'V1',;
    iif(this.value =28,'V2',;
    iif(this.value =29,'W',;
    iif(this.value =30,'X',;
    iif(this.value =31,'Y',;
    iif(this.value =32,'Z',;
    iif(this.value =33,'ZO',;
    iif(this.value =34,'TT',;
    space(2))))))))))))))))))))))))))))))))))))
  endfunc
  func oSYCAUPRE_1_9.GetRadio()
    this.Parent.oContained.w_SYCAUPRE = this.RadioValue()
    return .t.
  endfunc

  func oSYCAUPRE_1_9.SetRadio()
    this.Parent.oContained.w_SYCAUPRE=trim(this.Parent.oContained.w_SYCAUPRE)
    this.value = ;
      iif(this.Parent.oContained.w_SYCAUPRE=='A',1,;
      iif(this.Parent.oContained.w_SYCAUPRE=='B',2,;
      iif(this.Parent.oContained.w_SYCAUPRE=='C',3,;
      iif(this.Parent.oContained.w_SYCAUPRE=='D',4,;
      iif(this.Parent.oContained.w_SYCAUPRE=='E',5,;
      iif(this.Parent.oContained.w_SYCAUPRE=='F',6,;
      iif(this.Parent.oContained.w_SYCAUPRE=='G',7,;
      iif(this.Parent.oContained.w_SYCAUPRE=='H',8,;
      iif(this.Parent.oContained.w_SYCAUPRE=='I',9,;
      iif(this.Parent.oContained.w_SYCAUPRE=='J',10,;
      iif(this.Parent.oContained.w_SYCAUPRE=='K',11,;
      iif(this.Parent.oContained.w_SYCAUPRE=='L',12,;
      iif(this.Parent.oContained.w_SYCAUPRE=='L1',13,;
      iif(this.Parent.oContained.w_SYCAUPRE=='M',14,;
      iif(this.Parent.oContained.w_SYCAUPRE=='M1',15,;
      iif(this.Parent.oContained.w_SYCAUPRE=='M2',16,;
      iif(this.Parent.oContained.w_SYCAUPRE=='N',17,;
      iif(this.Parent.oContained.w_SYCAUPRE=='O',18,;
      iif(this.Parent.oContained.w_SYCAUPRE=='O1',19,;
      iif(this.Parent.oContained.w_SYCAUPRE=='P',20,;
      iif(this.Parent.oContained.w_SYCAUPRE=='Q',21,;
      iif(this.Parent.oContained.w_SYCAUPRE=='R',22,;
      iif(this.Parent.oContained.w_SYCAUPRE=='S',23,;
      iif(this.Parent.oContained.w_SYCAUPRE=='T',24,;
      iif(this.Parent.oContained.w_SYCAUPRE=='U',25,;
      iif(this.Parent.oContained.w_SYCAUPRE=='V',26,;
      iif(this.Parent.oContained.w_SYCAUPRE=='V1',27,;
      iif(this.Parent.oContained.w_SYCAUPRE=='V2',28,;
      iif(this.Parent.oContained.w_SYCAUPRE=='W',29,;
      iif(this.Parent.oContained.w_SYCAUPRE=='X',30,;
      iif(this.Parent.oContained.w_SYCAUPRE=='Y',31,;
      iif(this.Parent.oContained.w_SYCAUPRE=='Z',32,;
      iif(this.Parent.oContained.w_SYCAUPRE=='ZO',33,;
      iif(this.Parent.oContained.w_SYCAUPRE=='TT',34,;
      0))))))))))))))))))))))))))))))))))
  endfunc

  add object oSYCODCON_1_10 as StdField with uid="UCJURWGOQP",rtseq=10,rtrep=.f.,;
    cFormVar = "w_SYCODCON", cQueryName = "SYCODCON",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice conto iniziale",;
    HelpContextID = 44669044,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=175, Top=238, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_AFR", oKey_1_1="ANTIPCON", oKey_1_2="this.w_SYTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_SYCODCON"

  func oSYCODCON_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oSYCODCON_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSYCODCON_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_SYTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_SYTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oSYCODCON_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFR',"Elenco fornitori",'GSRI_ADY.CONTI_VZM',this.parent.oContained
  endproc
  proc oSYCODCON_1_10.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_SYTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_SYCODCON
     i_obj.ecpSave()
  endproc

  add object oSYCODCONF_1_11 as StdField with uid="UPBQAAIJKT",rtseq=11,rtrep=.f.,;
    cFormVar = "w_SYCODCONF", cQueryName = "SYCODCONF",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice conto iniziale",;
    HelpContextID = 44670164,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=175, Top=302, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_AFR", oKey_1_1="ANTIPCON", oKey_1_2="this.w_SYTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_SYCODCONF"

  func oSYCODCONF_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oSYCODCONF_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSYCODCONF_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_SYTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_SYTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oSYCODCONF_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFR',"Elenco fornitori",'GSRI_ADY.CONTI_VZM',this.parent.oContained
  endproc
  proc oSYCODCONF_1_11.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_SYTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_SYCODCONF
     i_obj.ecpSave()
  endproc

  add object oSYCOFISC_1_12 as StdField with uid="AQPQZSDDKJ",rtseq=12,rtrep=.f.,;
    cFormVar = "w_SYCOFISC", cQueryName = "SYCOFISC",;
    bObbl = .f. , nPag = 1, value=space(25), bMultilanguage =  .f.,;
    ToolTipText = "Codice identificativo fiscale",;
    HelpContextID = 147429481,;
   bGlobalFont=.t.,;
    Height=21, Width=188, Left=175, Top=360, InputMask=replicate('X',25)

  add object oSYDESCRI_1_16 as StdField with uid="MGGNLWGUXP",rtseq=13,rtrep=.f.,;
    cFormVar = "w_SYDESCRI", cQueryName = "SYDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 59746415,;
   bGlobalFont=.t.,;
    Height=21, Width=356, Left=372, Top=238, InputMask=replicate('X',60)

  add object oSYDESCRIF_1_20 as StdField with uid="NAYTKHBCLX",rtseq=14,rtrep=.f.,;
    cFormVar = "w_SYDESCRIF", cQueryName = "SYDESCRIF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 59747535,;
   bGlobalFont=.t.,;
    Height=21, Width=356, Left=372, Top=302, InputMask=replicate('X',60)


  add object oBtn_1_21 as StdButton with uid="ZXYGHBDURE",left=677, top=417, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare i dati";
    , HelpContextID = 65045526;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_21.Click()
      =cp_StandardFunction(this,"PgDn")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_22 as StdButton with uid="KUWCMEGMLG",left=729, top=417, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 8504086;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_22.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_13 as StdString with uid="AMWXDKXOMM",Visible=.t., Left=11, Top=117,;
    Alignment=1, Width=159, Height=18,;
    Caption="Certificazioni da verificare:"  ;
  , bGlobalFont=.t.

  func oStr_1_13.mHide()
    with this.Parent.oContained
      return (.w_TipEst='M')
    endwith
  endfunc

  add object oStr_1_14 as StdString with uid="JELVCYJCSJ",Visible=.t., Left=331, Top=117,;
    Alignment=1, Width=154, Height=18,;
    Caption="Escludi da generazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="QDVKRQIEET",Visible=.t., Left=29, Top=180,;
    Alignment=1, Width=141, Height=18,;
    Caption="Causale prestazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="BNSTAFZAAA",Visible=.t., Left=103, Top=241,;
    Alignment=1, Width=67, Height=18,;
    Caption="Da fornitore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="QTJDMGJXFC",Visible=.t., Left=113, Top=304,;
    Alignment=1, Width=57, Height=18,;
    Caption="A fornitore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="RDIQAAKAKE",Visible=.t., Left=124, Top=69,;
    Alignment=1, Width=46, Height=18,;
    Caption="Anno:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="ZJJEJDZFYC",Visible=.t., Left=-3, Top=362,;
    Alignment=1, Width=171, Height=18,;
    Caption="Codice identificativo fiscale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="ELYYYTAXGY",Visible=.t., Left=19, Top=21,;
    Alignment=0, Width=760, Height=18,;
    Caption="La seguente funzione consente di generare dati estratti SY, partendo dalle certificazioni uniche intestate a percipienti esteri"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_30.mHide()
    with this.Parent.oContained
      return (.w_Tipest='M')
    endwith
  endfunc

  add object oStr_1_31 as StdString with uid="BHMFRJEMEI",Visible=.t., Left=19, Top=40,;
    Alignment=0, Width=253, Height=18,;
    Caption="privi di codice fiscale"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_31.mHide()
    with this.Parent.oContained
      return (.w_Tipest='M')
    endwith
  endfunc

  add object oStr_1_32 as StdString with uid="QILPFKYTQO",Visible=.t., Left=11, Top=117,;
    Alignment=1, Width=159, Height=18,;
    Caption="Dato estratto da verificare:"  ;
  , bGlobalFont=.t.

  func oStr_1_32.mHide()
    with this.Parent.oContained
      return (.w_TipEst='G')
    endwith
  endfunc
enddefine
define class tgsri_kdyPag2 as StdContainer
  Width  = 790
  height = 481
  stdWidth  = 790
  stdheight = 481
  resizeXpos=342
  resizeYpos=249
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object ZoomSY as cp_szoombox with uid="WUTNYJHGAP",left=5, top=9, width=783,height=405,;
    caption='ZoomSY',;
   bGlobalFont=.t.,;
    cMenuFile="",cTable="DATESTSY",bOptions=.t.,bAdvOptions=.t.,bReadOnly=.t.,bQueryOnLoad=.f.,cZoomFile="GSRI_KDY",bRetriveAllRows=.f.,cZoomOnZoom="",;
    cEvent = "AggiornaSY",;
    nPag=2;
    , HelpContextID = 163212950


  add object oBtn_2_2 as StdButton with uid="QFDCFAAMDM",left=9, top=429, width=48,height=45,;
    CpPicture="bmp\Check.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per selezionare i percipienti";
    , HelpContextID = 8504086;
    , caption='\<Seleziona';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_2.Click()
      with this.Parent.oContained
        Gsri_Bdy(this.Parent.oContained,"S")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_2_3 as StdButton with uid="FVGQBXKFJS",left=65, top=429, width=48,height=45,;
    CpPicture="bmp\UnCheck.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per deselezionare i percipienti";
    , HelpContextID = 8504086;
    ,  caption='\<Deselez.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_3.Click()
      with this.Parent.oContained
        Gsri_Bdy(this.Parent.oContained,"D")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_2_4 as StdButton with uid="KYPYELXDUC",left=121, top=429, width=48,height=45,;
    CpPicture="bmp\InvCheck.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per invertire la selezione dei percipienti";
    , HelpContextID = 8504086;
    , caption='\<Inv. Sel.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_4.Click()
      with this.Parent.oContained
        Gsri_Bdy(this.Parent.oContained,"I")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_2_5 as StdButton with uid="IZQIBPSTVD",left=689, top=429, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per duplicare i record SY";
    , HelpContextID = 8504086;
    , caption='\<Duplica';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_5.Click()
      with this.Parent.oContained
        GSRI_BDY(this.Parent.oContained,"G")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_5.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_DUPRECSY='N' Or .w_TipEst='G')
     endwith
    endif
  endfunc


  add object oBtn_2_6 as StdButton with uid="GGCFXHPQLH",left=740, top=429, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 8504086;
    , caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_6.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object ZoomCU as cp_szoombox with uid="TVZMWXPUQP",left=5, top=9, width=783,height=405,;
    caption='ZoomCU',;
   bGlobalFont=.t.,;
    cMenuFile="",cTable="CUDTETRH",bOptions=.t.,bAdvOptions=.t.,bReadOnly=.t.,bQueryOnLoad=.f.,cZoomFile="GSRI_KDY",bRetriveAllRows=.f.,cZoomOnZoom="",;
    cEvent = "AggiornaCU",;
    nPag=2;
    , HelpContextID = 79326870


  add object oBtn_2_9 as StdButton with uid="JSKODNKPWM",left=689, top=429, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per generare i dati record SY";
    , HelpContextID = 8504086;
    , caption='\<Genera';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_9.Click()
      with this.Parent.oContained
        GSRI_BDY(this.Parent.oContained,"A")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_9.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_TipEst='M')
     endwith
    endif
  endfunc


  add object oBtn_2_13 as StdButton with uid="ENGJDCXJZO",left=177, top=429, width=48,height=45,;
    CpPicture="bmp\Verifica.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per accedere alla certificazione unica";
    , HelpContextID = 8504086;
    , caption='\<C.U.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_13.Click()
      with this.Parent.oContained
        Gsri_Bdy(this.Parent.oContained,"U")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_13.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_TipEst='M' Or Empty(.w_Chserial))
     endwith
    endif
  endfunc


  add object oBtn_2_14 as StdButton with uid="SAUNDLVLOR",left=177, top=429, width=48,height=45,;
    CpPicture="bmp\Verifica.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per accedere al dato estratto SY";
    , HelpContextID = 8504086;
    , caption='S\<Y';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_14.Click()
      with this.Parent.oContained
        Gsri_Bdy(this.Parent.oContained,"Y")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_14.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_TipEst='G' Or Empty(.w_Syserial))
     endwith
    endif
  endfunc

  add object oStr_2_10 as StdString with uid="RZYRRIRGOQ",Visible=.t., Left=421, Top=429,;
    Alignment=0, Width=248, Height=17,;
    Caption="Record SY gi� generato"    , BackStyle=1, BackColor=RGB(255,0,0);
  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_2_10.mHide()
    with this.Parent.oContained
      return (.w_TipEst='M')
    endwith
  endfunc

  add object oStr_2_11 as StdString with uid="IULAULJOPF",Visible=.t., Left=421, Top=457,;
    Alignment=0, Width=248, Height=17,;
    Caption="Certificazione presente nel file C.U."    , BackStyle=1, BackColor=RGB(0,255,0);
  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_2_11.mHide()
    with this.Parent.oContained
      return (.w_TipEst='M')
    endwith
  endfunc

  add object oStr_2_12 as StdString with uid="UETCDYPSPY",Visible=.t., Left=421, Top=429,;
    Alignment=0, Width=155, Height=17,;
    Caption="Dato estratto gi� duplicato"    , BackStyle=1, BackColor=RGB(255,127,39);
  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_2_12.mHide()
    with this.Parent.oContained
      return (.w_TipEst='G')
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsri_kdy','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
