* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_bdy                                                        *
*              Manutenzione dati estratti record SY                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2016-05-19                                                      *
* Last revis.: 2018-09-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParame
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsri_bdy",oParentObject,m.pParame)
return(i_retval)

define class tgsri_bdy as StdBatch
  * --- Local variables
  pParame = space(1)
  w_ZOOM = .NULL.
  w_NFLTATTR = 0
  w_nRecSY = 0
  w_Syserial = space(10)
  w_Serial = 0
  w_Oldversione = .f.
  w_Numser = 0
  w_Sydatdup = space(10)
  w_Aggser = space(10)
  w_ZoomSY = .NULL.
  w_ZoomCU = .NULL.
  w_nCert = 0
  w_Syserial = space(10)
  w_Oldversione = .f.
  w_Numser = 0
  w_Chdatest = space(10)
  w_Aggser = space(10)
  w_Tipope = space(1)
  w_Genera = space(1)
  w_Object = .NULL.
  w_nRecProv = 0
  w_Gttip770 = space(5)
  * --- WorkFile variables
  TMPDATIESTRATTI_idx=0
  DATESTSY_idx=0
  GEFILTEL_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Manutenzione dati estratti record SY
    this.w_ZOOM = iif(this.pParame="C",Space(10),iif(this.oParentObject.w_Tipest="G",This.oParentObject.w_ZoomCU,This.oParentObject.w_ZoomSY))
    do case
      case this.pParame="S" or this.pParame="D" or this.pParame="I"
        Select (this.w_ZOOM.cCursor)
        this.w_NFLTATTR = RECNO()
        UPDATE (this.w_ZOOM.cCursor) SET XCHK=ICASE(this.pParame=="S",1,this.pParame=="D",0,IIF(XCHK=1,0,1))
        Select (this.w_ZOOM.cCursor)
        if this.w_NFLTATTR<RECCOUNT()
          GO this.w_NFLTATTR
        endif
      case this.pParame="G"
        * --- Verifico che l'utente abbia selezionato almeno un dato estratto da duplicare
        Select Distinct Syserial from (this.w_Zoom.cCursor) Where Xchk=1 into cursor Temp
        Select ("Temp")
        this.w_nRecSY = reccount()
        * --- Creo un cursore temporaneo con l'elenco dei dati estratti da duplicare
        if this.w_nRecSy>0
          Curtotab("Temp","Tmptotrech")
          * --- Create temporary table TMPDATIESTRATTI
          i_nIdx=cp_AddTableDef('TMPDATIESTRATTI') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          vq_exec('..\rite\exe\query\gsri2kdy',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
          this.TMPDATIESTRATTI_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
          AddMsgNL(Space(5)+"Generazione progressivi",this)
          * --- Try
          local bErr_033ABFB0
          bErr_033ABFB0=bTrsErr
          this.Try_033ABFB0()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- rollback
            bTrsErr=.t.
            cp_EndTrs(.t.)
            ah_ErrorMsg("Errore nella creazione dei dati estratti (%1)", "!", "", i_trsmsg)
          endif
          bTrsErr=bTrsErr or bErr_033ABFB0
          * --- End
          Ah_ErrorMsg("Creati %1 dati estratti record SY",,,Alltrim(Str(this.w_nRecSY)))
          This.oParentobject.Notifyevent("AggiornaSY")
        else
          Ah_ErrorMsg("Non � stato selezionato alcun dato estratto da duplicare")
        endif
        * --- Drop temporary table TMPTOTRECH
        i_nIdx=cp_GetTableDefIdx('TMPTOTRECH')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TMPTOTRECH')
        endif
        * --- Drop temporary table TMPDATIESTRATTI
        i_nIdx=cp_GetTableDefIdx('TMPDATIESTRATTI')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TMPDATIESTRATTI')
        endif
        use in Select ("Temp")
      case this.pParame="B"
        this.w_ZoomSY = This.oParentObject.w_ZoomSY
        this.w_ZoomCU = This.oParentObject.w_ZoomCU
        Zap in ( this.w_ZoomSY.cCursor )
        Zap in ( this.w_ZoomCU.cCursor )
        if this.oParentObject.w_Tipest="G"
          this.w_ZoomSY.Visible = .f.
          this.w_ZoomCU.Visible = .t.
          This.oParentObject.notifyevent("AggiornaCU")
        else
          this.w_ZoomSY.Visible = .t.
          this.w_ZoomCU.Visible = .f.
          This.oParentObject.notifyevent("AggiornaSY")
          Select (this.w_Zoom.cCursor)
          Update (this.w_Zoom.cCursor) SET XCHK=iif(This.oParentObject.w_Duprecsy="S",1,0)
        endif
      case this.pParame="A"
        this.w_Tipope = "O"
        * --- Verifico se l'utente ha selezionato certificazioni che hanno generato un
        *     record SY
        Select * from (this.w_Zoom.cCursor) Where Xchk=1 And Nvl(Datestsy,"N")="S" into cursor Temp
        if Reccount("Temp")>0
          if Ah_YesNo("Attenzione: sono state selezionate certificazioni che hanno gi� generato%0un record SY, rispondendo SI verranno rigenerati nuovi record SY.%0Si vuole procedere ?")
            * --- Proseguo con l'elaborazione
          else
            use in Select ("Temp")
            i_retcode = 'stop'
            return
          endif
        endif
        use in Select ("Temp")
        Select Distinct Chserial from (this.w_Zoom.cCursor) Where Xchk=1 into cursor Temp
        Select ("Temp")
        this.w_nCert = reccount()
        if this.w_nCert>0
          * --- Creo un cursore temporaneo con l'elenco delle certificazioni da sostituire/annullare
          Curtotab("Temp","Tmptotrech")
          * --- Create temporary table TMPDATIESTRATTI
          i_nIdx=cp_AddTableDef('TMPDATIESTRATTI') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          vq_exec('..\rite\exe\query\gsri1kde',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
          this.TMPDATIESTRATTI_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
          * --- Try
          local bErr_04CBBEF8
          bErr_04CBBEF8=bTrsErr
          this.Try_04CBBEF8()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            this.w_Genera = "N"
            * --- rollback
            bTrsErr=.t.
            cp_EndTrs(.t.)
            ah_ErrorMsg("Errore nella creazione dei dati estratti (%1)",,,i_trsmsg)
          endif
          bTrsErr=bTrsErr or bErr_04CBBEF8
          * --- End
          * --- Drop temporary table TMPDATIESTRATTI
          i_nIdx=cp_GetTableDefIdx('TMPDATIESTRATTI')
          if i_nIdx<>0
            cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
            cp_RemoveTableDef('TMPDATIESTRATTI')
          endif
          use in Select ("Temp")
          if this.w_Genera="S"
            Ah_ErrorMsg("Creati %1 dati estratti record SY di cui %2 da verificare",,,Alltrim(Str(this.w_nCert)),Alltrim(Str(this.w_nRecProv)))
            This.oParentobject.EcpQuit()
          endif
        else
          use in Select ("Temp")
          Ah_ErrorMsg("Non � stato selezionato alcun record da generare")
        endif
      case this.pParame="C"
        * --- Leggo dalla tabella della genererazione file telematico il flag Gttip770.
        *     Questo campo identifica l'anno di generazione
        this.w_Serial = This.oParentObject.w_Gdserial
        * --- Read from GEFILTEL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.GEFILTEL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.GEFILTEL_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "GTTIP770"+;
            " from "+i_cTable+" GEFILTEL where ";
                +"GTSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            GTTIP770;
            from (i_cTable) where;
                GTSERIAL = this.w_SERIAL;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_GTTIP770 = NVL(cp_ToDate(_read_.GTTIP770),cp_NullValue(_read_.GTTIP770))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_OBJECT = Gsri_Agy(this.w_Gttip770)
        if !(this.w_OBJECT.bSec1)
          i_retcode = 'stop'
          return
        endif
        this.w_OBJECT.EcpFilter()     
        * --- Valorizzo i campi chiave
        this.w_OBJECT.w_Gtserial = This.oParentObject.w_Gdserial
        * --- Carico il record richiesto
        this.w_OBJECT.EcpSave()     
      case this.pParame="U"
        this.w_OBJECT = Gsri_Ach()
        if !(this.w_OBJECT.bSec1)
          i_retcode = 'stop'
          return
        endif
        this.w_OBJECT.EcpFilter()     
        * --- Valorizzo i campi chiave
        this.w_OBJECT.w_Chserial = This.oParentObject.w_Chserial
        * --- Carico il record richiesto
        this.w_OBJECT.EcpSave()     
      case this.pParame="Y"
        this.w_OBJECT = Gsri_Ady()
        if !(this.w_OBJECT.bSec1)
          i_retcode = 'stop'
          return
        endif
        this.w_OBJECT.EcpFilter()     
        * --- Valorizzo i campi chiave
        this.w_OBJECT.w_Syserial = This.oParentObject.w_Syserial
        * --- Carico il record richiesto
        this.w_OBJECT.EcpSave()     
    endcase
  endproc
  proc Try_033ABFB0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.w_Syserial = space(10)
    w_Conn=i_TableProp[this.DATESTSY_IDX, 3] 
 cp_NextTableProg(this, w_Conn, "SYSER", "i_codazi,w_Syserial")
    this.w_Serial = VAL(this.w_Syserial)-1
    this.w_Syserial = cp_StrZeroPad( this.w_Serial+this.w_nRecSY ,10)
    cp_NextTableProg(this, w_Conn, "SYSER", "i_codazi,w_Syserial")
    * --- Verifico la versione del database 
    if upper(CP_DBTYPE)="SQLSERVER"
      sqlexec(w_Conn,"Select convert(char,SERVERPROPERTY('productversion')) As Version", "ChkVersion")
      if USED("ChkVersion") AND VAL(LEFT(ChkVersion.Version,AT(".",ChkVersion.Version)-1)) < 9
        this.w_Oldversione = .t.
      endif
    else
      if TYPE("g_ORACLEVERS")="C" AND g_ORACLEVERS="8"
        this.w_Oldversione = .t.
      endif
    endif
    * --- Genero progressivo seriale
    if this.w_Oldversione
      * --- Select from GSRI3KDY
      do vq_exec with 'GSRI3KDY',this,'_Curs_GSRI3KDY','',.f.,.t.
      if used('_Curs_GSRI3KDY')
        select _Curs_GSRI3KDY
        locate for 1=1
        do while not(eof())
        this.w_Numser = this.w_Numser + 1
        this.w_Sydatdup = _Curs_GSRI3KDY.Sydatdup
        this.w_Aggser = cp_StrZeroPad(this.w_Numser + this.w_Serial, 10)
        * --- Write into TMPDATIESTRATTI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.TMPDATIESTRATTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPDATIESTRATTI_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPDATIESTRATTI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SYSERIAL ="+cp_NullLink(cp_ToStrODBC(this.w_AGGSER),'TMPDATIESTRATTI','SYSERIAL');
              +i_ccchkf ;
          +" where ";
              +"SYDATDUP = "+cp_ToStrODBC(this.w_SYDATDUP);
                 )
        else
          update (i_cTable) set;
              SYSERIAL = this.w_AGGSER;
              &i_ccchkf. ;
           where;
              SYDATDUP = this.w_SYDATDUP;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
          select _Curs_GSRI3KDY
          continue
        enddo
        use
      endif
    else
      * --- Write into TMPDATIESTRATTI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMPDATIESTRATTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPDATIESTRATTI_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="SYDATDUP"
        do vq_exec with 'GSRI4KDY',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPDATIESTRATTI_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="TMPDATIESTRATTI.SYDATDUP = _t2.SYDATDUP";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SYSERIAL = _t2.SYSERIAL";
            +i_ccchkf;
            +" from "+i_cTable+" TMPDATIESTRATTI, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="TMPDATIESTRATTI.SYDATDUP = _t2.SYDATDUP";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI, "+i_cQueryTable+" _t2 set ";
            +"TMPDATIESTRATTI.SYSERIAL = _t2.SYSERIAL";
            +Iif(Empty(i_ccchkf),"",",TMPDATIESTRATTI.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="TMPDATIESTRATTI.SYDATDUP = t2.SYDATDUP";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI set (";
            +"SYSERIAL";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.SYSERIAL";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="TMPDATIESTRATTI.SYDATDUP = _t2.SYDATDUP";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI set ";
            +"SYSERIAL = _t2.SYSERIAL";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".SYDATDUP = "+i_cQueryTable+".SYDATDUP";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SYSERIAL = (select SYSERIAL from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- Insert into DATESTSY
    i_nConn=i_TableProp[this.DATESTSY_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DATESTSY_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"Gsri5kdy",this.DATESTSY_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_04CBBEF8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    AddMsgNL(Space(5)+"Generazione progressivi",this)
    * --- Generazione dati estratti record SY
    this.w_Syserial = space(10)
    w_Conn=i_TableProp[this.DATESTSY_IDX, 3] 
 cp_NextTableProg(this, w_Conn, "SYSER", "i_codazi,w_Syserial")
    this.w_Serial = VAL(this.w_Syserial)-1
    this.w_Syserial = cp_StrZeroPad( this.w_Serial+this.w_nCert ,10)
    cp_NextTableProg(this, w_Conn, "SYSER", "i_codazi,w_Syserial")
    * --- Verifico la versione del database 
    if upper(CP_DBTYPE)="SQLSERVER"
      sqlexec(w_Conn,"Select convert(char,SERVERPROPERTY('productversion')) As Version", "ChkVersion")
      if USED("ChkVersion") AND VAL(LEFT(ChkVersion.Version,AT(".",ChkVersion.Version)-1)) < 9
        this.w_Oldversione = .t.
      endif
    else
      if TYPE("g_ORACLEVERS")="C" AND g_ORACLEVERS="8"
        this.w_Oldversione = .t.
      endif
    endif
    * --- Genero progressivo seriale
    if this.w_Oldversione
      * --- Select from GSRI3KDE
      do vq_exec with 'GSRI3KDE',this,'_Curs_GSRI3KDE','',.f.,.t.
      if used('_Curs_GSRI3KDE')
        select _Curs_GSRI3KDE
        locate for 1=1
        do while not(eof())
        this.w_Numser = this.w_Numser + 1
        this.w_Chdatest = _Curs_GSRI3KDE.Chdatest
        this.w_Aggser = cp_StrZeroPad(this.w_Numser + this.w_Serial, 10)
        * --- Write into TMPDATIESTRATTI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.TMPDATIESTRATTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPDATIESTRATTI_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPDATIESTRATTI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CHSERIAL ="+cp_NullLink(cp_ToStrODBC(this.w_AGGSER),'TMPDATIESTRATTI','CHSERIAL');
              +i_ccchkf ;
          +" where ";
              +"CHDATEST = "+cp_ToStrODBC(this.w_CHDATEST);
                 )
        else
          update (i_cTable) set;
              CHSERIAL = this.w_AGGSER;
              &i_ccchkf. ;
           where;
              CHDATEST = this.w_CHDATEST;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
          select _Curs_GSRI3KDE
          continue
        enddo
        use
      endif
    else
      * --- Write into TMPDATIESTRATTI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMPDATIESTRATTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPDATIESTRATTI_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="CHDATEST"
        do vq_exec with 'GSRI2KDE',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPDATIESTRATTI_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="TMPDATIESTRATTI.CHDATEST = _t2.CHDATEST";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CHSERIAL = _t2.CHSERIAL";
            +i_ccchkf;
            +" from "+i_cTable+" TMPDATIESTRATTI, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="TMPDATIESTRATTI.CHDATEST = _t2.CHDATEST";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI, "+i_cQueryTable+" _t2 set ";
            +"TMPDATIESTRATTI.CHSERIAL = _t2.CHSERIAL";
            +Iif(Empty(i_ccchkf),"",",TMPDATIESTRATTI.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="TMPDATIESTRATTI.CHDATEST = t2.CHDATEST";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI set (";
            +"CHSERIAL";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.CHSERIAL";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="TMPDATIESTRATTI.CHDATEST = _t2.CHDATEST";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI set ";
            +"CHSERIAL = _t2.CHSERIAL";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".CHDATEST = "+i_cQueryTable+".CHDATEST";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CHSERIAL = (select CHSERIAL from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- Insert into DATESTSY
    i_nConn=i_TableProp[this.DATESTSY_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DATESTSY_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"Gsri13kde",this.DATESTSY_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Select from Gsri14kde
    do vq_exec with 'Gsri14kde',this,'_Curs_Gsri14kde','',.f.,.t.
    if used('_Curs_Gsri14kde')
      select _Curs_Gsri14kde
      locate for 1=1
      do while not(eof())
      this.w_nRecProv = Nvl(_Curs_Gsri14Kde.RecProv,0)
        select _Curs_Gsri14kde
        continue
      enddo
      use
    endif
    * --- Calcolo il numero dei dati estratti record SY generati in provvisiorio
    * --- commit
    cp_EndTrs(.t.)
    this.w_Genera = "S"
    return


  proc Init(oParentObject,pParame)
    this.pParame=pParame
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='*TMPDATIESTRATTI'
    this.cWorkTables[2]='DATESTSY'
    this.cWorkTables[3]='GEFILTEL'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_GSRI3KDY')
      use in _Curs_GSRI3KDY
    endif
    if used('_Curs_GSRI3KDE')
      use in _Curs_GSRI3KDE
    endif
    if used('_Curs_Gsri14kde')
      use in _Curs_Gsri14kde
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParame"
endproc
