* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_bhc                                                        *
*              Controlli preventivi dati record H C.U.                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2015-06-18                                                      *
* Last revis.: 2016-06-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,HASEVCOP
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsri_bhc",oParentObject,m.HASEVCOP)
return(i_retval)

define class tgsri_bhc as StdBatch
  * --- Local variables
  HASEVCOP = space(10)
  w_CUDATCER = ctod("  /  /  ")
  w_MESS = space(100)
  w_ANNO = 0
  w_TIPOPERAZ = space(1)
  w_TIPOELA = space(1)
  w_SerialeGenerazione = space(10)
  w_NumRecSt = 0
  * --- WorkFile variables
  GEDETTEL_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.bUpdateParentObject = .F.
    this.oParentObject.w_HASEVENT = .T.
    do case
      case this.oParentObject.w_TIPOCONT="D"
        * --- Select from GSRI_BHC
        do vq_exec with 'GSRI_BHC',this,'_Curs_GSRI_BHC','',.f.,.t.
        if used('_Curs_GSRI_BHC')
          select _Curs_GSRI_BHC
          locate for 1=1
          do while not(eof())
          this.w_MESS = "La certificazione � presente nel file telematico comunicato il "
          this.oParentObject.w_HASEVENT = .F.
          this.w_CUDATCER = _Curs_GSRI_BHC.CUDATCER
          EXIT
            select _Curs_GSRI_BHC
            continue
          enddo
          use
        endif
      case this.oParentObject.w_TIPOCONT="G"
        * --- Select from GSRI1BHC
        do vq_exec with 'GSRI1BHC',this,'_Curs_GSRI1BHC','',.f.,.t.
        if used('_Curs_GSRI1BHC')
          select _Curs_GSRI1BHC
          locate for 1=1
          do while not(eof())
          this.w_MESS = "Esistono certificazioni comunicate e rettificate"
          this.oParentObject.w_HASEVENT = .F.
          EXIT
            select _Curs_GSRI1BHC
            continue
          enddo
          use
        endif
      case this.oParentObject.w_TIPOCONT="I"
        this.w_ANNO = This.oParentObject.w_Gt__Anno
        this.w_TIPOPERAZ = "O"
        this.w_TIPOELA = "C"
        this.w_SerialeGenerazione = This.oParentObject.w_Gtserial
        * --- Il controllo del record ST deve essere fatto solo se esiste almeno un record ST
        *     generato
        * --- Read from GEDETTEL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.GEDETTEL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.GEDETTEL_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "*"+;
            " from "+i_cTable+" GEDETTEL where ";
                +"GDSERIAL = "+cp_ToStrODBC(this.w_SerialeGenerazione);
                +" and GDTIPEST = "+cp_ToStrODBC("E");
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            *;
            from (i_cTable) where;
                GDSERIAL = this.w_SerialeGenerazione;
                and GDTIPEST = "E";
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_NumRecSt = Nvl(i_Rows,0)
        if this.w_NumRecST>0
          * --- Select from GSRI40BED
          do vq_exec with 'GSRI40BED',this,'_Curs_GSRI40BED','',.f.,.t.
          if used('_Curs_GSRI40BED')
            select _Curs_GSRI40BED
            locate for 1=1
            do while not(eof())
            this.w_MESS = "Esiste gi� un record ST non comunicato, occorre procedere alla cancellazione dello stesso"
            this.oParentObject.w_HASEVENT = .F.
            EXIT
              select _Curs_GSRI40BED
              continue
            enddo
            use
          endif
        endif
        if this.oParentObject.w_HasEvent
          * --- Select from GSRI3BHC
          do vq_exec with 'GSRI3BHC',this,'_Curs_GSRI3BHC','',.f.,.t.
          if used('_Curs_GSRI3BHC')
            select _Curs_GSRI3BHC
            locate for 1=1
            do while not(eof())
            this.w_MESS = "Esistono dati estratti generati e rettificati"
            this.oParentObject.w_HASEVENT = .F.
            EXIT
              select _Curs_GSRI3BHC
              continue
            enddo
            use
          endif
        endif
    endcase
    this.w_MESS = AH_MsgFormat(this.w_MESS)
    if Not this.oParentObject.w_HASEVENT
      if Upper(this.HASEVCOP)="ECPEDIT"
        ah_ErrorMsg("Impossibile modificare. %0%1%2", 48,"",this.w_MESS,Dtoc(this.w_Cudatcer))
      else
        if this.oParentObject.w_TIPOCONT="D"
          ah_ErrorMsg("Impossibile cancellare. %0%1%2", 48,"",this.w_MESS,Dtoc(this.w_Cudatcer))
        else
          ah_ErrorMsg("Impossibile cancellare. %0%1", 48,"",this.w_MESS)
        endif
      endif
      i_retcode = 'stop'
      return
    endif
    if Upper(this.HASEVCOP)="ECPEDIT"
      this.oParentObject.bUpdated = .f.
    endif
  endproc


  proc Init(oParentObject,HASEVCOP)
    this.HASEVCOP=HASEVCOP
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='GEDETTEL'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_GSRI_BHC')
      use in _Curs_GSRI_BHC
    endif
    if used('_Curs_GSRI1BHC')
      use in _Curs_GSRI1BHC
    endif
    if used('_Curs_GSRI40BED')
      use in _Curs_GSRI40BED
    endif
    if used('_Curs_GSRI3BHC')
      use in _Curs_GSRI3BHC
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="HASEVCOP"
endproc
