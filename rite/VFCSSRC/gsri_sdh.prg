* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_sdh                                                        *
*              Stampa certificazione unica                                     *
*                                                                              *
*      Author: Zucchetti S.p.A.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_63]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-05-17                                                      *
* Last revis.: 2017-02-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsri_sdh",oParentObject))

* --- Class definition
define class tgsri_sdh as StdForm
  Top    = 8
  Left   = 9

  * --- Standard Properties
  Width  = 788
  Height = 532+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2017-02-07"
  HelpContextID=74053225
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=51

  * --- Constant Properties
  _IDX = 0
  CONTI_IDX = 0
  VALUTE_IDX = 0
  AZIENDA_IDX = 0
  TITOLARI_IDX = 0
  cPrg = "gsri_sdh"
  cComment = "Stampa certificazione unica"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_ANNO = 0
  o_ANNO = 0
  w_TIPCON = space(1)
  w_PERCIN = space(15)
  w_DESCINI = space(40)
  w_RITEINI = space(1)
  w_PERCFIN = space(15)
  w_RITEFIN = space(1)
  w_DESCFIN = space(40)
  w_TIPOPERAZ = space(1)
  w_TIPOSTAMPA = space(1)
  w_SOLOESCL = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_CODAZI = space(5)
  o_CODAZI = space(5)
  w_AZ_EMAIL = space(50)
  w_AZCAPAZI = space(9)
  w_AZINDAZI = space(35)
  w_AZCOFAZI = space(16)
  w_AZTELFAX = space(18)
  w_AZPERAZI = space(1)
  w_AZLOCAZI = space(30)
  w_AZPROAZI = space(2)
  w_CODAZI2 = space(5)
  w_COGTIT1 = space(25)
  w_NOMTIT1 = space(25)
  w_TTLOCTIT = space(30)
  w_TTCAPTIT = space(9)
  w_TTINDIRI = space(35)
  w_TTTELEFO = space(18)
  w_TTPROTIT = space(2)
  w_CUCODFIS = space(16)
  w_CUCOGNOM = space(24)
  w_CU__NOME = space(20)
  w_CURAGSOC = space(60)
  w_CUCOMUNE = space(30)
  w_CUPRORES = space(2)
  w_CU___CAP = space(9)
  w_CUINDIRI = space(35)
  w_CUNUMFAX = space(12)
  w_CU__MAIL = space(100)
  w_CUCODATT = space(6)
  w_CUCODSED = space(3)
  w_CUDATFIR = ctod('  /  /  ')
  w_FIRMA_IMPOSTA = space(250)
  w_AIDATINV = ctod('  /  /  ')
  w_CURAPFAL = ctod('  /  /  ')
  w_TIPO_STAMPA = space(1)
  w_SERIALE = space(10)
  w_DOMFIS = space(1)
  w_CUCODCAR = space(1)
  w_CURAPFIR = space(1)
  w_CU__ANNO = 0
  w_ElePerc = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsri_sdhPag1","gsri_sdh",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Filtri")
      .Pages(2).addobject("oPag","tgsri_sdhPag2","gsri_sdh",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Dati del soggetto a cui si riferisce la comunicazione")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oANNO_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ElePerc = this.oPgFrm.Pages(1).oPag.ElePerc
    DoDefault()
    proc Destroy()
      this.w_ElePerc = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='VALUTE'
    this.cWorkTables[3]='AZIENDA'
    this.cWorkTables[4]='TITOLARI'
    return(this.OpenAllTables(4))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_ANNO=0
      .w_TIPCON=space(1)
      .w_PERCIN=space(15)
      .w_DESCINI=space(40)
      .w_RITEINI=space(1)
      .w_PERCFIN=space(15)
      .w_RITEFIN=space(1)
      .w_DESCFIN=space(40)
      .w_TIPOPERAZ=space(1)
      .w_TIPOSTAMPA=space(1)
      .w_SOLOESCL=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_CODAZI=space(5)
      .w_AZ_EMAIL=space(50)
      .w_AZCAPAZI=space(9)
      .w_AZINDAZI=space(35)
      .w_AZCOFAZI=space(16)
      .w_AZTELFAX=space(18)
      .w_AZPERAZI=space(1)
      .w_AZLOCAZI=space(30)
      .w_AZPROAZI=space(2)
      .w_CODAZI2=space(5)
      .w_COGTIT1=space(25)
      .w_NOMTIT1=space(25)
      .w_TTLOCTIT=space(30)
      .w_TTCAPTIT=space(9)
      .w_TTINDIRI=space(35)
      .w_TTTELEFO=space(18)
      .w_TTPROTIT=space(2)
      .w_CUCODFIS=space(16)
      .w_CUCOGNOM=space(24)
      .w_CU__NOME=space(20)
      .w_CURAGSOC=space(60)
      .w_CUCOMUNE=space(30)
      .w_CUPRORES=space(2)
      .w_CU___CAP=space(9)
      .w_CUINDIRI=space(35)
      .w_CUNUMFAX=space(12)
      .w_CU__MAIL=space(100)
      .w_CUCODATT=space(6)
      .w_CUCODSED=space(3)
      .w_CUDATFIR=ctod("  /  /  ")
      .w_FIRMA_IMPOSTA=space(250)
      .w_AIDATINV=ctod("  /  /  ")
      .w_CURAPFAL=ctod("  /  /  ")
      .w_TIPO_STAMPA=space(1)
      .w_SERIALE=space(10)
      .w_DOMFIS=space(1)
      .w_CUCODCAR=space(1)
      .w_CURAPFIR=space(1)
      .w_CU__ANNO=0
        .w_ANNO = year(i_DATSYS)-1
        .w_TIPCON = 'F'
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_PERCIN))
          .link_1_3('Full')
        endif
        .DoRTCalc(4,6,.f.)
        if not(empty(.w_PERCFIN))
          .link_1_6('Full')
        endif
          .DoRTCalc(7,8,.f.)
        .w_TIPOPERAZ = 'O'
        .w_TIPOSTAMPA = 'S'
        .w_SOLOESCL = 'N'
        .w_OBTEST = i_INIDAT
        .w_CODAZI = i_Codazi
        .DoRTCalc(13,13,.f.)
        if not(empty(.w_CODAZI))
          .link_2_1('Full')
        endif
          .DoRTCalc(14,21,.f.)
        .w_CODAZI2 = i_Codazi
        .DoRTCalc(22,22,.f.)
        if not(empty(.w_CODAZI2))
          .link_2_10('Full')
        endif
          .DoRTCalc(23,29,.f.)
        .w_CUCODFIS = IIF(LEN(ALLTRIM(.w_AZCOFAZI))<=16, LEFT(ALLTRIM(.w_AZCOFAZI),16), RIGHT(ALLTRIM(.w_AZCOFAZI),16))
        .w_CUCOGNOM = Left(iif(.w_Azperazi='S', Upper(.w_Cogtit1), space(24)), 24)
        .w_CU__NOME = Left(iif(.w_Azperazi='S', Upper(.w_Nomtit1), space(20)), 20)
        .w_CURAGSOC = Left(iif(.w_Azperazi<>'S', Upper(g_Ragazi), space(60)), 60)
        .w_CUCOMUNE = Upper(Left(iif(.w_Azperazi='S', .w_Ttloctit, .w_Azlocazi),30))
        .w_CUPRORES = Upper(iif(.w_Azperazi='S', .w_Ttprotit, .w_Azproazi))
        .w_CU___CAP = Upper(Left(iif(.w_Azperazi='S', .w_Ttcaptit, .w_Azcapazi),9))
        .w_CUINDIRI = Upper(Left(iif(.w_Azperazi='S', .w_Ttindiri, .w_Azindazi),35))
        .w_CUNUMFAX = Left(iif(.w_Azperazi='S', .w_Tttelefo, .w_Aztelfax),12)
        .w_CU__MAIL = left(UPPER(.w_Az_email), 100)
        .w_CUCODATT = iif(Empty(.w_Cucodatt),iif(g_Attivi='S', Space(6), Left(Alltrim(Calattso(g_Catazi,Alltrim(Str(Year(i_Datsys))),12)),6)),.w_Cucodatt)
          .DoRTCalc(41,41,.f.)
        .w_CUDATFIR = i_Datsys
          .DoRTCalc(43,43,.f.)
        .w_AIDATINV = i_DATSYS
      .oPgFrm.Page1.oPag.ElePerc.Calculate()
        .w_CURAPFAL = {}
        .w_TIPO_STAMPA = 'S'
        .w_SERIALE = Space(10)
        .w_DOMFIS = 'S'
        .w_CUCODCAR = '1'
        .w_CURAPFIR = 'N'
        .w_CU__ANNO = .w_ANNO
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_21.enabled = this.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,12,.t.)
        if .o_Codazi<>.w_Codazi
          .link_2_1('Full')
        endif
        .DoRTCalc(14,21,.t.)
        if .o_Codazi<>.w_Codazi
          .link_2_10('Full')
        endif
        .oPgFrm.Page1.oPag.ElePerc.Calculate()
        .DoRTCalc(23,50,.t.)
        if .o_ANNO<>.w_ANNO
            .w_CU__ANNO = .w_ANNO
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ElePerc.Calculate()
    endwith
  return

  proc Calculate_ZDFLOLROZK()
    with this
          * --- Seleziona record percipienti
          Gsri_Bcs(this;
              ,'S';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page2.oPag.oCUCOGNOM_2_31.enabled = this.oPgFrm.Page2.oPag.oCUCOGNOM_2_31.mCond()
    this.oPgFrm.Page2.oPag.oCU__NOME_2_32.enabled = this.oPgFrm.Page2.oPag.oCU__NOME_2_32.mCond()
    this.oPgFrm.Page2.oPag.oCURAGSOC_2_33.enabled = this.oPgFrm.Page2.oPag.oCURAGSOC_2_33.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ElePerc.Event(cEvent)
        if lower(cEvent)==lower("Blank")
          .Calculate_ZDFLOLROZK()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PERCIN
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PERCIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFR',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_PERCIN)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_PERCIN))
          select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PERCIN)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_PERCIN)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_PERCIN)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PERCIN) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oPERCIN_1_3'),i_cWhere,'GSAR_AFR',"Elenco fornitori",'GSRI_ADH.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PERCIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_PERCIN);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_PERCIN)
            select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PERCIN = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCINI = NVL(_Link_.ANDESCRI,space(40))
      this.w_RITEINI = NVL(_Link_.ANRITENU,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PERCIN = space(15)
      endif
      this.w_DESCINI = space(40)
      this.w_RITEINI = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(Empty(.w_Percfin) or (.w_Percin<=.w_Percfin)) And .w_Riteini $ 'CS'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PERCIN = space(15)
        this.w_DESCINI = space(40)
        this.w_RITEINI = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PERCIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PERCFIN
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PERCFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFR',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_PERCFIN)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_PERCFIN))
          select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PERCFIN)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_PERCFIN)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_PERCFIN)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PERCFIN) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oPERCFIN_1_6'),i_cWhere,'GSAR_AFR',"Elenco fornitori",'GSRI_ADH.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PERCFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_PERCFIN);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_PERCFIN)
            select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PERCFIN = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCFIN = NVL(_Link_.ANDESCRI,space(40))
      this.w_RITEFIN = NVL(_Link_.ANRITENU,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PERCFIN = space(15)
      endif
      this.w_DESCFIN = space(40)
      this.w_RITEFIN = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((.w_Percfin>=.w_Percin) Or Empty(.w_Percin)) And (.w_Ritefin $ 'CS')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PERCFIN = space(15)
        this.w_DESCFIN = space(40)
        this.w_RITEFIN = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PERCFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODAZI
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZCOFAZI,AZTELFAX,AZ_EMAIL,AZPERAZI,AZLOCAZI,AZPROAZI,AZCAPAZI,AZINDAZI";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_CODAZI)
            select AZCODAZI,AZCOFAZI,AZTELFAX,AZ_EMAIL,AZPERAZI,AZLOCAZI,AZPROAZI,AZCAPAZI,AZINDAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.AZCODAZI,space(5))
      this.w_AZCOFAZI = NVL(_Link_.AZCOFAZI,space(16))
      this.w_AZTELFAX = NVL(_Link_.AZTELFAX,space(18))
      this.w_AZ_EMAIL = NVL(_Link_.AZ_EMAIL,space(50))
      this.w_AZPERAZI = NVL(_Link_.AZPERAZI,space(1))
      this.w_AZLOCAZI = NVL(_Link_.AZLOCAZI,space(30))
      this.w_AZPROAZI = NVL(_Link_.AZPROAZI,space(2))
      this.w_AZCAPAZI = NVL(_Link_.AZCAPAZI,space(9))
      this.w_AZINDAZI = NVL(_Link_.AZINDAZI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_AZCOFAZI = space(16)
      this.w_AZTELFAX = space(18)
      this.w_AZ_EMAIL = space(50)
      this.w_AZPERAZI = space(1)
      this.w_AZLOCAZI = space(30)
      this.w_AZPROAZI = space(2)
      this.w_AZCAPAZI = space(9)
      this.w_AZINDAZI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODAZI2
  func Link_2_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TITOLARI_IDX,3]
    i_lTable = "TITOLARI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TITOLARI_IDX,2], .t., this.TITOLARI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TITOLARI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TTCODAZI,TTCOGTIT,TTNOMTIT,TTLOCTIT,TTPROTIT,TTCAPTIT,TTINDIRI,TTTELEFO";
                   +" from "+i_cTable+" "+i_lTable+" where TTCODAZI="+cp_ToStrODBC(this.w_CODAZI2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TTCODAZI',this.w_CODAZI2)
            select TTCODAZI,TTCOGTIT,TTNOMTIT,TTLOCTIT,TTPROTIT,TTCAPTIT,TTINDIRI,TTTELEFO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI2 = NVL(_Link_.TTCODAZI,space(5))
      this.w_COGTIT1 = NVL(_Link_.TTCOGTIT,space(25))
      this.w_NOMTIT1 = NVL(_Link_.TTNOMTIT,space(25))
      this.w_TTLOCTIT = NVL(_Link_.TTLOCTIT,space(30))
      this.w_TTPROTIT = NVL(_Link_.TTPROTIT,space(2))
      this.w_TTCAPTIT = NVL(_Link_.TTCAPTIT,space(9))
      this.w_TTINDIRI = NVL(_Link_.TTINDIRI,space(35))
      this.w_TTTELEFO = NVL(_Link_.TTTELEFO,space(18))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI2 = space(5)
      endif
      this.w_COGTIT1 = space(25)
      this.w_NOMTIT1 = space(25)
      this.w_TTLOCTIT = space(30)
      this.w_TTPROTIT = space(2)
      this.w_TTCAPTIT = space(9)
      this.w_TTINDIRI = space(35)
      this.w_TTTELEFO = space(18)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TITOLARI_IDX,2])+'\'+cp_ToStr(_Link_.TTCODAZI,1)
      cp_ShowWarn(i_cKey,this.TITOLARI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oANNO_1_1.value==this.w_ANNO)
      this.oPgFrm.Page1.oPag.oANNO_1_1.value=this.w_ANNO
    endif
    if not(this.oPgFrm.Page1.oPag.oPERCIN_1_3.value==this.w_PERCIN)
      this.oPgFrm.Page1.oPag.oPERCIN_1_3.value=this.w_PERCIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCINI_1_4.value==this.w_DESCINI)
      this.oPgFrm.Page1.oPag.oDESCINI_1_4.value=this.w_DESCINI
    endif
    if not(this.oPgFrm.Page1.oPag.oPERCFIN_1_6.value==this.w_PERCFIN)
      this.oPgFrm.Page1.oPag.oPERCFIN_1_6.value=this.w_PERCFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCFIN_1_8.value==this.w_DESCFIN)
      this.oPgFrm.Page1.oPag.oDESCFIN_1_8.value=this.w_DESCFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOPERAZ_1_9.RadioValue()==this.w_TIPOPERAZ)
      this.oPgFrm.Page1.oPag.oTIPOPERAZ_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOSTAMPA_1_10.RadioValue()==this.w_TIPOSTAMPA)
      this.oPgFrm.Page1.oPag.oTIPOSTAMPA_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSOLOESCL_1_11.RadioValue()==this.w_SOLOESCL)
      this.oPgFrm.Page1.oPag.oSOLOESCL_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCUCODFIS_2_30.value==this.w_CUCODFIS)
      this.oPgFrm.Page2.oPag.oCUCODFIS_2_30.value=this.w_CUCODFIS
    endif
    if not(this.oPgFrm.Page2.oPag.oCUCOGNOM_2_31.value==this.w_CUCOGNOM)
      this.oPgFrm.Page2.oPag.oCUCOGNOM_2_31.value=this.w_CUCOGNOM
    endif
    if not(this.oPgFrm.Page2.oPag.oCU__NOME_2_32.value==this.w_CU__NOME)
      this.oPgFrm.Page2.oPag.oCU__NOME_2_32.value=this.w_CU__NOME
    endif
    if not(this.oPgFrm.Page2.oPag.oCURAGSOC_2_33.value==this.w_CURAGSOC)
      this.oPgFrm.Page2.oPag.oCURAGSOC_2_33.value=this.w_CURAGSOC
    endif
    if not(this.oPgFrm.Page2.oPag.oCUCOMUNE_2_34.value==this.w_CUCOMUNE)
      this.oPgFrm.Page2.oPag.oCUCOMUNE_2_34.value=this.w_CUCOMUNE
    endif
    if not(this.oPgFrm.Page2.oPag.oCUPRORES_2_35.value==this.w_CUPRORES)
      this.oPgFrm.Page2.oPag.oCUPRORES_2_35.value=this.w_CUPRORES
    endif
    if not(this.oPgFrm.Page2.oPag.oCU___CAP_2_36.value==this.w_CU___CAP)
      this.oPgFrm.Page2.oPag.oCU___CAP_2_36.value=this.w_CU___CAP
    endif
    if not(this.oPgFrm.Page2.oPag.oCUINDIRI_2_37.value==this.w_CUINDIRI)
      this.oPgFrm.Page2.oPag.oCUINDIRI_2_37.value=this.w_CUINDIRI
    endif
    if not(this.oPgFrm.Page2.oPag.oCUNUMFAX_2_38.value==this.w_CUNUMFAX)
      this.oPgFrm.Page2.oPag.oCUNUMFAX_2_38.value=this.w_CUNUMFAX
    endif
    if not(this.oPgFrm.Page2.oPag.oCU__MAIL_2_39.value==this.w_CU__MAIL)
      this.oPgFrm.Page2.oPag.oCU__MAIL_2_39.value=this.w_CU__MAIL
    endif
    if not(this.oPgFrm.Page2.oPag.oCUCODATT_2_40.value==this.w_CUCODATT)
      this.oPgFrm.Page2.oPag.oCUCODATT_2_40.value=this.w_CUCODATT
    endif
    if not(this.oPgFrm.Page2.oPag.oCUCODSED_2_41.value==this.w_CUCODSED)
      this.oPgFrm.Page2.oPag.oCUCODSED_2_41.value=this.w_CUCODSED
    endif
    if not(this.oPgFrm.Page2.oPag.oCUDATFIR_2_42.value==this.w_CUDATFIR)
      this.oPgFrm.Page2.oPag.oCUDATFIR_2_42.value=this.w_CUDATFIR
    endif
    if not(this.oPgFrm.Page2.oPag.oFIRMA_IMPOSTA_2_43.value==this.w_FIRMA_IMPOSTA)
      this.oPgFrm.Page2.oPag.oFIRMA_IMPOSTA_2_43.value=this.w_FIRMA_IMPOSTA
    endif
    if not(this.oPgFrm.Page1.oPag.oDOMFIS_1_29.RadioValue()==this.w_DOMFIS)
      this.oPgFrm.Page1.oPag.oDOMFIS_1_29.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_ANNO)) or not(.w_ANNO>=2015))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oANNO_1_1.SetFocus()
            i_bnoObbl = !empty(.w_ANNO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((Empty(.w_Percfin) or (.w_Percin<=.w_Percfin)) And .w_Riteini $ 'CS')  and not(empty(.w_PERCIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPERCIN_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(((.w_Percfin>=.w_Percin) Or Empty(.w_Percin)) And (.w_Ritefin $ 'CS'))  and not(empty(.w_PERCFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPERCFIN_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CUCODFIS))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCUCODFIS_2_30.SetFocus()
            i_bnoObbl = !empty(.w_CUCODFIS)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CUCOGNOM))  and (.w_Azperazi='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCUCOGNOM_2_31.SetFocus()
            i_bnoObbl = !empty(.w_CUCOGNOM)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CU__NOME))  and (.w_Azperazi='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCU__NOME_2_32.SetFocus()
            i_bnoObbl = !empty(.w_CU__NOME)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CURAGSOC))  and (.w_Azperazi<>'S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCURAGSOC_2_33.SetFocus()
            i_bnoObbl = !empty(.w_CURAGSOC)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CUCOMUNE))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCUCOMUNE_2_34.SetFocus()
            i_bnoObbl = !empty(.w_CUCOMUNE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CUPRORES))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCUPRORES_2_35.SetFocus()
            i_bnoObbl = !empty(.w_CUPRORES)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(at(' ',alltrim(.w_Cunumfax))=0 and at('/',alltrim(.w_Cunumfax))=0 and at('-',alltrim(.w_Cunumfax))=0)
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCUNUMFAX_2_38.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Non inserire caratteri separatori tra prefisso e numero")
          case   (empty(.w_CUDATFIR))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCUDATFIR_2_42.SetFocus()
            i_bnoObbl = !empty(.w_CUDATFIR)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_ANNO = this.w_ANNO
    this.o_CODAZI = this.w_CODAZI
    return

enddefine

* --- Define pages as container
define class tgsri_sdhPag1 as StdContainer
  Width  = 784
  height = 532
  stdWidth  = 784
  stdheight = 532
  resizeXpos=432
  resizeYpos=291
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oANNO_1_1 as StdField with uid="IFLGTVFEND",rtseq=1,rtrep=.f.,;
    cFormVar = "w_ANNO", cQueryName = "ANNO",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Anno di competenza",;
    HelpContextID = 68535290,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=94, Top=14, cSayPict='"9999"', cGetPict='"9999"'

  func oANNO_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_ANNO>=2015)
    endwith
    return bRes
  endfunc

  add object oPERCIN_1_3 as StdField with uid="PUOHTLUTZX",rtseq=3,rtrep=.f.,;
    cFormVar = "w_PERCIN", cQueryName = "PERCIN",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Percipiente iniziale selezionato",;
    HelpContextID = 242119670,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=94, Top=48, cSayPict="p_FOR", cGetPict="p_FOR", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_AFR", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_PERCIN"

  func oPERCIN_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oPERCIN_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPERCIN_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oPERCIN_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFR',"Elenco fornitori",'GSRI_ADH.CONTI_VZM',this.parent.oContained
  endproc
  proc oPERCIN_1_3.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_PERCIN
     i_obj.ecpSave()
  endproc

  add object oDESCINI_1_4 as StdField with uid="GXGWMDZABW",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DESCINI", cQueryName = "DESCINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 26311882,;
   bGlobalFont=.t.,;
    Height=21, Width=292, Left=212, Top=48, InputMask=replicate('X',40)

  add object oPERCFIN_1_6 as StdField with uid="MRXUNLAJUE",rtseq=6,rtrep=.f.,;
    cFormVar = "w_PERCFIN", cQueryName = "PERCFIN",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Percipiente finale selezionato",;
    HelpContextID = 155087862,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=94, Top=82, cSayPict="p_FOR", cGetPict="p_FOR", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_AFR", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_PERCFIN"

  func oPERCFIN_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oPERCFIN_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPERCFIN_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oPERCFIN_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFR',"Elenco fornitori",'GSRI_ADH.CONTI_VZM',this.parent.oContained
  endproc
  proc oPERCFIN_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_PERCFIN
     i_obj.ecpSave()
  endproc

  add object oDESCFIN_1_8 as StdField with uid="PPTCJEXPMJ",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DESCFIN", cQueryName = "DESCFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 155091766,;
   bGlobalFont=.t.,;
    Height=21, Width=292, Left=212, Top=82, InputMask=replicate('X',40)


  add object oTIPOPERAZ_1_9 as StdCombo with uid="FVZJLSCZCJ",rtseq=9,rtrep=.f.,left=611,top=47,width=111,height=22;
    , ToolTipText = "Tipo operazioni ";
    , HelpContextID = 99245591;
    , cFormVar="w_TIPOPERAZ",RowSource=""+"Ordinario,"+"Sostitutiva,"+"Annullamento", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPOPERAZ_1_9.RadioValue()
    return(iif(this.value =1,'O',;
    iif(this.value =2,'S',;
    iif(this.value =3,'A',;
    space(1)))))
  endfunc
  func oTIPOPERAZ_1_9.GetRadio()
    this.Parent.oContained.w_TIPOPERAZ = this.RadioValue()
    return .t.
  endfunc

  func oTIPOPERAZ_1_9.SetRadio()
    this.Parent.oContained.w_TIPOPERAZ=trim(this.Parent.oContained.w_TIPOPERAZ)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOPERAZ=='O',1,;
      iif(this.Parent.oContained.w_TIPOPERAZ=='S',2,;
      iif(this.Parent.oContained.w_TIPOPERAZ=='A',3,;
      0)))
  endfunc


  add object oTIPOSTAMPA_1_10 as StdCombo with uid="JLGSCEDFQZ",rtseq=10,rtrep=.f.,left=611,top=78,width=111,height=22;
    , HelpContextID = 85630595;
    , cFormVar="w_TIPOSTAMPA",RowSource=""+"Sintetica,"+"Ordinaria", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPOSTAMPA_1_10.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'O',;
    space(1))))
  endfunc
  func oTIPOSTAMPA_1_10.GetRadio()
    this.Parent.oContained.w_TIPOSTAMPA = this.RadioValue()
    return .t.
  endfunc

  func oTIPOSTAMPA_1_10.SetRadio()
    this.Parent.oContained.w_TIPOSTAMPA=trim(this.Parent.oContained.w_TIPOSTAMPA)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOSTAMPA=='S',1,;
      iif(this.Parent.oContained.w_TIPOSTAMPA=='O',2,;
      0))
  endfunc

  add object oSOLOESCL_1_11 as StdCheck with uid="KUNZJZBTZK",rtseq=11,rtrep=.f.,left=212, top=13, caption="Escludi da generazione",;
    ToolTipText = "Se attivo, saranno stampate le sole certificazioni escluse",;
    HelpContextID = 54140530,;
    cFormVar="w_SOLOESCL", bObbl = .f. , nPag = 1;
    , tabstop = .f.;
   , bGlobalFont=.t.


  func oSOLOESCL_1_11.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oSOLOESCL_1_11.GetRadio()
    this.Parent.oContained.w_SOLOESCL = this.RadioValue()
    return .t.
  endfunc

  func oSOLOESCL_1_11.SetRadio()
    this.Parent.oContained.w_SOLOESCL=trim(this.Parent.oContained.w_SOLOESCL)
    this.value = ;
      iif(this.Parent.oContained.w_SOLOESCL=='S',1,;
      0)
  endfunc


  add object oBtn_1_14 as StdButton with uid="MQNDWWYBZI",left=677, top=486, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , HelpContextID = 16892694;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_14.Click()
      with this.Parent.oContained
        Gsri_bcs(this.Parent.oContained,"C")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_14.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty(.w_Cucodfis) And ((.w_Azperazi='S' And Not Empty(.w_Cucognom) And Not Empty(.w_Cu__Nome)) Or (.w_Azperazi<>'S' And Not Empty(.w_Curagsoc))) And Not Empty(.w_Cucomune) And Not Empty(.w_Cuprores))
      endwith
    endif
  endfunc


  add object oBtn_1_15 as StdButton with uid="BRAIRZJTXI",left=732, top=486, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 16892694;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_15.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object ElePerc as cp_szoombox with uid="CCHYKRKZAC",left=2, top=134, width=780,height=345,;
    caption='Object',;
   bGlobalFont=.t.,;
    bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.t.,cMenuFile="",cZoomOnZoom="",cTable="CUDTETRH",cZoomFile="Gsri_Sdh",bOptions=.t.,;
    cEvent = "Blank,Ricerca",;
    nPag=1;
    , HelpContextID = 103944422


  add object oBtn_1_21 as StdButton with uid="HVAVLDIYYL",left=732, top=56, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Riesegue la ricerca con le nuove selezioni";
    , HelpContextID = 206819654;
    , tabstop = .f.,Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_21.Click()
      with this.Parent.oContained
        .NotifyEvent('Ricerca')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_22 as StdButton with uid="QFDCFAAMDM",left=4, top=486, width=48,height=45,;
    CpPicture="bmp\Check.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per selezionare i percipienti";
    , HelpContextID = 16892694;
    , tabstop = .f., caption='\<Seleziona';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_22.Click()
      with this.Parent.oContained
        Gsri_Bcs(this.Parent.oContained,"S")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_23 as StdButton with uid="FVGQBXKFJS",left=60, top=486, width=48,height=45,;
    CpPicture="bmp\UnCheck.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per deselezionare i percipienti";
    , HelpContextID = 16892694;
    ,  tabstop = .f.,caption='\<Deselez.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_23.Click()
      with this.Parent.oContained
        Gsri_Bcs(this.Parent.oContained,"D")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_24 as StdButton with uid="KYPYELXDUC",left=116, top=486, width=48,height=45,;
    CpPicture="bmp\InvCheck.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per invertire la selezione dei percipienti";
    , HelpContextID = 16892694;
    , tabstop = .f., caption='\<Inv. Sel.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_24.Click()
      with this.Parent.oContained
        Gsri_Bcs(this.Parent.oContained,"I")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDOMFIS_1_29 as StdCheck with uid="YAMAKLJUDD",rtseq=48,rtrep=.f.,left=446, top=495, caption="Non valorizza domicilio fiscale",;
    ToolTipText = "Se attivato, non verr� valorizzato il domicilio fiscale se causale prestazione diversa da N",;
    HelpContextID = 57748790,;
    cFormVar="w_DOMFIS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDOMFIS_1_29.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oDOMFIS_1_29.GetRadio()
    this.Parent.oContained.w_DOMFIS = this.RadioValue()
    return .t.
  endfunc

  func oDOMFIS_1_29.SetRadio()
    this.Parent.oContained.w_DOMFIS=trim(this.Parent.oContained.w_DOMFIS)
    this.value = ;
      iif(this.Parent.oContained.w_DOMFIS=='S',1,;
      0)
  endfunc

  add object oStr_1_12 as StdString with uid="EKEJBDHIGM",Visible=.t., Left=6, Top=50,;
    Alignment=1, Width=84, Height=18,;
    Caption="Da fornitore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="AEUJOKCZUT",Visible=.t., Left=15, Top=82,;
    Alignment=1, Width=75, Height=18,;
    Caption="A fornitore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="WGYBMYVTQJ",Visible=.t., Left=516, Top=50,;
    Alignment=1, Width=91, Height=18,;
    Caption="Tipo operazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="PZVASEKDJU",Visible=.t., Left=59, Top=17,;
    Alignment=0, Width=31, Height=18,;
    Caption="Anno:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="REGOGOKIXF",Visible=.t., Left=4, Top=113,;
    Alignment=0, Width=148, Height=18,;
    Caption="Elenco percipienti"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_30 as StdString with uid="UFVUVTJPCI",Visible=.t., Left=520, Top=82,;
    Alignment=1, Width=87, Height=18,;
    Caption="Tipo stampa:"  ;
  , bGlobalFont=.t.
enddefine
define class tgsri_sdhPag2 as StdContainer
  Width  = 784
  height = 532
  stdWidth  = 784
  stdheight = 532
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCUCODFIS_2_30 as StdField with uid="JCMBZFORRT",rtseq=30,rtrep=.f.,;
    cFormVar = "w_CUCODFIS", cQueryName = "CUCODFIS",;
    bObbl = .t. , nPag = 2, value=space(16), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale del sostituto d'imposta",;
    HelpContextID = 165047431,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=221, Top=13, cSayPict='REPLICATE("!",16)', cGetPict='REPLICATE("!",16)', InputMask=replicate('X',16)

  add object oCUCOGNOM_2_31 as StdField with uid="POIZYCIMGW",rtseq=31,rtrep=.f.,;
    cFormVar = "w_CUCOGNOM", cQueryName = "CUCOGNOM",;
    bObbl = .t. , nPag = 2, value=space(24), bMultilanguage =  .f.,;
    ToolTipText = "Cognome",;
    HelpContextID = 240751475,;
   bGlobalFont=.t.,;
    Height=21, Width=216, Left=221, Top=47, cSayPict="REPL('!',24)", cGetPict="REPL('!',24)", InputMask=replicate('X',24)

  func oCUCOGNOM_2_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Azperazi='S')
    endwith
   endif
  endfunc

  add object oCU__NOME_2_32 as StdField with uid="VTRCGFEDOS",rtseq=32,rtrep=.f.,;
    cFormVar = "w_CU__NOME", cQueryName = "CU__NOME",;
    bObbl = .t. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Cognome",;
    HelpContextID = 266031979,;
   bGlobalFont=.t.,;
    Height=21, Width=174, Left=518, Top=47, cSayPict="REPL('!',20)", cGetPict="REPL('!',20)", InputMask=replicate('X',20)

  func oCU__NOME_2_32.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Azperazi='S')
    endwith
   endif
  endfunc

  add object oCURAGSOC_2_33 as StdField with uid="ZCVLCDJGNK",rtseq=33,rtrep=.f.,;
    cFormVar = "w_CURAGSOC", cQueryName = "CURAGSOC",;
    bObbl = .t. , nPag = 2, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Denominazione o ragione sociale",;
    HelpContextID = 55346025,;
   bGlobalFont=.t.,;
    Height=21, Width=461, Left=221, Top=81, cSayPict="REPL('!',60)", cGetPict="REPL('!',60)", InputMask=replicate('X',60)

  func oCURAGSOC_2_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Azperazi<>'S')
    endwith
   endif
  endfunc

  add object oCUCOMUNE_2_34 as StdField with uid="CZKSGJYLFH",rtseq=34,rtrep=.f.,;
    cFormVar = "w_CUCOMUNE", cQueryName = "CUCOMUNE",;
    bObbl = .t. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Comune di residenza",;
    HelpContextID = 96047979,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=221, Top=115, InputMask=replicate('X',30), bHasZoom = .t. 

  proc oCUCOMUNE_2_34.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C","",".w_Cucomune",".w_Cuprores")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oCUPRORES_2_35 as StdField with uid="ODNQALSJHT",rtseq=35,rtrep=.f.,;
    cFormVar = "w_CUPRORES", cQueryName = "CUPRORES",;
    bObbl = .t. , nPag = 2, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Provincia di residenza",;
    HelpContextID = 220372103,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=538, Top=115, InputMask=replicate('X',2), bHasZoom = .t. 

  proc oCUPRORES_2_35.mZoom
      with this.Parent.oContained
        Gsar_Bxc(this.Parent.oContained,"P","","",".w_Cuprores")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oCU___CAP_2_36 as StdField with uid="BVCUJETGMA",rtseq=36,rtrep=.f.,;
    cFormVar = "w_CU___CAP", cQueryName = "CU___CAP",;
    bObbl = .f. , nPag = 2, value=space(9), bMultilanguage =  .f.,;
    ToolTipText = "Codice avviamento postale",;
    HelpContextID = 82531190,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=629, Top=115, cSayPict="'999999999'", cGetPict="'999999999'", InputMask=replicate('X',9)

  add object oCUINDIRI_2_37 as StdField with uid="PWAELSZVXQ",rtseq=37,rtrep=.f.,;
    cFormVar = "w_CUINDIRI", cQueryName = "CUINDIRI",;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo di residenza",;
    HelpContextID = 153678703,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=221, Top=149, InputMask=replicate('X',35)

  add object oCUNUMFAX_2_38 as StdField with uid="LJTGMMGSIL",rtseq=38,rtrep=.f.,;
    cFormVar = "w_CUNUMFAX", cQueryName = "CUNUMFAX",;
    bObbl = .f. , nPag = 2, value=space(12), bMultilanguage =  .f.,;
    sErrorMsg = "Non inserire caratteri separatori tra prefisso e numero",;
    ToolTipText = "Numero di telefono o fax",;
    HelpContextID = 113263486,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=629, Top=149, cSayPict='"999999999999"', cGetPict='"999999999999"', InputMask=replicate('X',12)

  func oCUNUMFAX_2_38.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (at(' ',alltrim(.w_Cunumfax))=0 and at('/',alltrim(.w_Cunumfax))=0 and at('-',alltrim(.w_Cunumfax))=0)
    endwith
    return bRes
  endfunc

  add object oCU__MAIL_2_39 as StdField with uid="DCVMCQXWUV",rtseq=39,rtrep=.f.,;
    cFormVar = "w_CU__MAIL", cQueryName = "CU__MAIL",;
    bObbl = .f. , nPag = 2, value=space(100), bMultilanguage =  .f.,;
    HelpContextID = 238333070,;
   bGlobalFont=.t.,;
    Height=21, Width=531, Left=221, Top=183, InputMask=replicate('X',100)

  add object oCUCODATT_2_40 as StdField with uid="NANLGTYTSL",rtseq=40,rtrep=.f.,;
    cFormVar = "w_CUCODATT", cQueryName = "CUCODATT",;
    bObbl = .f. , nPag = 2, value=space(6), bMultilanguage =  .f.,;
    ToolTipText = "Codice attivit�",;
    HelpContextID = 19501946,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=221, Top=217, cSayPict='repl("!",6)', cGetPict='repl("!",6)', InputMask=replicate('X',6), bHasZoom = .t. 

  proc oCUCODATT_2_40.mZoom
    vx_exec("gsri8sft.vzm",this.Parent.oContained)
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oCUCODSED_2_41 as StdField with uid="BFOZRSTDJY",rtseq=41,rtrep=.f.,;
    cFormVar = "w_CUCODSED", cQueryName = "CUCODSED",nZero=3,;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice sede",;
    HelpContextID = 215379094,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=538, Top=217, cSayPict='"999"', cGetPict='"999"', InputMask=replicate('X',3)

  add object oCUDATFIR_2_42 as StdField with uid="YGKYRAJOMO",rtseq=42,rtrep=.f.,;
    cFormVar = "w_CUDATFIR", cQueryName = "CUDATFIR",;
    bObbl = .t. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 149183624,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=221, Top=251

  add object oFIRMA_IMPOSTA_2_43 as StdField with uid="RPNVGVRXEK",rtseq=43,rtrep=.f.,;
    cFormVar = "w_FIRMA_IMPOSTA", cQueryName = "FIRMA_IMPOSTA",;
    bObbl = .f. , nPag = 2, value=space(250), bMultilanguage =  .f.,;
    ToolTipText = "Firma del sostituto d'imposta",;
    HelpContextID = 211682397,;
   bGlobalFont=.t.,;
    Height=21, Width=531, Left=221, Top=285, InputMask=replicate('X',250)

  add object oStr_2_18 as StdString with uid="IEEVABHEXO",Visible=.t., Left=5, Top=16,;
    Alignment=1, Width=213, Height=18,;
    Caption="Cod. fiscale del sostituto d'imposta:"  ;
  , bGlobalFont=.t.

  add object oStr_2_19 as StdString with uid="RRCIDONQPX",Visible=.t., Left=96, Top=49,;
    Alignment=1, Width=122, Height=18,;
    Caption="Cognome:"  ;
  , bGlobalFont=.t.

  add object oStr_2_20 as StdString with uid="UDZFJLLISQ",Visible=.t., Left=452, Top=49,;
    Alignment=1, Width=60, Height=18,;
    Caption="Nome:"  ;
  , bGlobalFont=.t.

  add object oStr_2_21 as StdString with uid="TNEDCLMTXU",Visible=.t., Left=86, Top=82,;
    Alignment=1, Width=132, Height=18,;
    Caption="Denominazione:"  ;
  , bGlobalFont=.t.

  add object oStr_2_22 as StdString with uid="UIGQFPRNIC",Visible=.t., Left=56, Top=116,;
    Alignment=1, Width=162, Height=18,;
    Caption="Comune di residenza:"  ;
  , bGlobalFont=.t.

  add object oStr_2_23 as StdString with uid="TBSZMJFXQA",Visible=.t., Left=456, Top=116,;
    Alignment=1, Width=76, Height=18,;
    Caption="Provincia:"  ;
  , bGlobalFont=.t.

  add object oStr_2_24 as StdString with uid="QITIDHKRZW",Visible=.t., Left=585, Top=116,;
    Alignment=1, Width=38, Height=18,;
    Caption="CAP:"  ;
  , bGlobalFont=.t.

  add object oStr_2_25 as StdString with uid="JKTFSYKDBT",Visible=.t., Left=134, Top=150,;
    Alignment=1, Width=84, Height=18,;
    Caption="Indirizzo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_26 as StdString with uid="VWIPVDUOVB",Visible=.t., Left=529, Top=150,;
    Alignment=1, Width=94, Height=18,;
    Caption="Telefono o fax:"  ;
  , bGlobalFont=.t.

  add object oStr_2_27 as StdString with uid="UODPZXUYII",Visible=.t., Left=71, Top=218,;
    Alignment=1, Width=147, Height=18,;
    Caption="Codice attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_2_28 as StdString with uid="JDEPLZORYY",Visible=.t., Left=429, Top=218,;
    Alignment=1, Width=103, Height=18,;
    Caption="Codice sede:"  ;
  , bGlobalFont=.t.

  add object oStr_2_29 as StdString with uid="FRMANRMHVC",Visible=.t., Left=47, Top=184,;
    Alignment=1, Width=171, Height=18,;
    Caption="Indirizzo di posta elettronica:"  ;
  , bGlobalFont=.t.

  add object oStr_2_44 as StdString with uid="VTWTEZSYWR",Visible=.t., Left=46, Top=286,;
    Alignment=1, Width=172, Height=18,;
    Caption="Firma del sostituto d'imposta:"  ;
  , bGlobalFont=.t.

  add object oStr_2_45 as StdString with uid="BVGTNNDJYM",Visible=.t., Left=32, Top=253,;
    Alignment=1, Width=186, Height=18,;
    Caption="Data firma sostituto d'imposta:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsri_sdh','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
