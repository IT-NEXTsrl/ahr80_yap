* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_bai                                                        *
*              Abbina movimenti in distinta                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][98][VRS_9]                                               *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-05-15                                                      *
* Last revis.: 2013-04-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsri_bai",oParentObject,m.pOper)
return(i_retval)

define class tgsri_bai as StdBatch
  * --- Local variables
  pOper = space(5)
  w_ZOOM = space(10)
  w_SERIAL = space(10)
  oggetto = .NULL.
  w_MESS = space(10)
  w_OK = .f.
  * --- WorkFile variables
  MOV_RITE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione Abbinamento Movimenti Ritenute in Distinta (Lanciato da GSRI_KAI e GSRI_KAN)
    this.w_ZOOM = this.oParentObject.w_CalcZoom
    do case
      case this.pOper="CALC"
        * --- Setta Proprieta' Campi del Cursore
        This.oParentObject.NotifyEvent("Abbina")
        FOR I=1 TO This.w_Zoom.grd.ColumnCount
        NC = ALLTRIM(STR(I))
        if NOT "XCHK" $ UPPER(This.w_Zoom.grd.Column&NC..ControlSource)
          This.w_Zoom.grd.Column&NC..Enabled=.f.
        endif
        ENDFOR
      case this.pOper="SELE"
        * --- Seleziona/Deseleziona Tutto
        NC = this.w_ZOOM.cCursor
        if this.oParentObject.w_SELEZI="S"
          UPDATE &NC SET XCHK = 1
          this.oParentObject.w_FLSELE = 1
        else
          UPDATE &NC SET XCHK = 0
          this.oParentObject.w_FLSELE = 0
        endif
      case this.pOper="AGGIO"
        if ah_YesNo("I movimenti selezionati saranno abbinati alla distinta.%0Confermi?")
          * --- Abbina Partite
          this.w_OK = .F.
          NC = this.w_ZOOM.cCursor
          SELECT &NC
          GO TOP
          * --- Try
          local bErr_035E8460
          bErr_035E8460=bTrsErr
          this.Try_035E8460()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- rollback
            bTrsErr=.t.
            cp_EndTrs(.t.)
            ah_ErrorMsg("Errore durante aggiornamento! Operazione abbandonata")
          endif
          bTrsErr=bTrsErr or bErr_035E8460
          * --- End
          if this.w_OK=.T.
            if this.oParentObject.w_FLTIPO="IRPEF"
              This.oParentObject.oParentObject.w_PRESEN=.T.
            endif
            * --- Lancia lo Zoom
            This.oParentObject.oParentObject.NotifyEvent("Legge")
            ah_ErrorMsg("Aggiornamento movimenti %1 completato",,"",this.oParentObject.w_FLTIPO)
            * --- Chiudo la Maschera
            This.oParentObject.ecpQuit()
          else
            ah_ErrorMsg("Non ci sono movimenti da abbinare")
          endif
        endif
    endcase
  endproc
  proc Try_035E8460()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Cicla sui record Selezionati
    SCAN FOR XCHK<>0
    this.w_SERIAL = MRSERIAL
    * --- Controllo che non sia vuota la data del documento.
    ah_Msg("Aggiornamento movimento del: %1",.t.,.f.,.f.,DTOC(CP_TODATE(MRDATDOC)))
    if this.oParentObject.w_FLTIPO="IRPEF"
      * --- Write into MOV_RITE
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.MOV_RITE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MOV_RITE_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.MOV_RITE_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MRRIFDI1 ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_VPSERIAL),'MOV_RITE','MRRIFDI1');
            +i_ccchkf ;
        +" where ";
            +"MRSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
               )
      else
        update (i_cTable) set;
            MRRIFDI1 = this.oParentObject.w_VPSERIAL;
            &i_ccchkf. ;
         where;
            MRSERIAL = this.w_SERIAL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    else
      * --- Write into MOV_RITE
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.MOV_RITE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MOV_RITE_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.MOV_RITE_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MRRIFDI2 ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_VPSERIAL),'MOV_RITE','MRRIFDI2');
            +i_ccchkf ;
        +" where ";
            +"MRSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
               )
      else
        update (i_cTable) set;
            MRRIFDI2 = this.oParentObject.w_VPSERIAL;
            &i_ccchkf. ;
         where;
            MRSERIAL = this.w_SERIAL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    this.w_OK = .T.
    ENDSCAN
    * --- commit
    cp_EndTrs(.t.)
    return


  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='MOV_RITE'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
