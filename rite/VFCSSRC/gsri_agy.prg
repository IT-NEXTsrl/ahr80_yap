* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_agy                                                        *
*              Generazione file telematico 770                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2015-01-19                                                      *
* Last revis.: 2018-09-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- gsri_agy
PARAMETERS pTipCer
L_descri1='1) Rappr. legale o negoziale'
L_descri2='2) Rappr. di minore - curatore eredit�'
L_descri3='3) Curatore fallimentare'
L_descri4='4) Commissario liquidatore'
L_descri5='5) Commissario giudiziale'
L_descri6='6) Rappr. fiscale di soggetto non residente'
L_descri7='7) Erede'
L_descri8='8) Liquidatore volontario'
L_descri9='9) Sogg. dich. IVA operaz. straord.'
L_descri10='10) Rappr. fisc. sogg. non resid. D.L.331/1993'
L_descri11='11) Tutore'
L_descri12='12) Liquid. volont. ditta indiv.'
L_descri13='13) Ammin. condominio'
L_descri14='14) Sogg. per conto P.A.'
L_descri15='15) Comm. liquid. P.A.'
* --- Fine Area Manuale
return(createobject("tgsri_agy"))

* --- Class definition
define class tgsri_agy as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 798
  Height = 552+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2018-09-21"
  HelpContextID=42595945
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=148

  * --- Constant Properties
  GEFILTEL_IDX = 0
  AZIENDA_IDX = 0
  TITOLARI_IDX = 0
  SEDIAZIE_IDX = 0
  ENTI_COM_IDX = 0
  DAT_RAPP_IDX = 0
  cFile = "GEFILTEL"
  cKeySelect = "GTSERIAL"
  cKeyWhere  = "GTSERIAL=this.w_GTSERIAL"
  cKeyWhereODBC = '"GTSERIAL="+cp_ToStrODBC(this.w_GTSERIAL)';

  cKeyWhereODBCqualified = '"GEFILTEL.GTSERIAL="+cp_ToStrODBC(this.w_GTSERIAL)';

  cPrg = "gsri_agy"
  cComment = "Generazione file telematico 770"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_GTSERIAL = space(10)
  w_GTDATGEN = ctod('  /  /  ')
  w_GTDESAGG = space(254)
  w_CODAZI = space(5)
  w_AZCOFAZI = space(16)
  w_AZPERAZI = space(1)
  w_GTCODFIS = space(16)
  o_GTCODFIS = space(16)
  w_GTCODSOS = space(16)
  w_CODAZI2 = space(5)
  w_COGTIT1 = space(25)
  w_NOMTIT1 = space(25)
  w_TTTELEFO = space(18)
  w_TTDATNAS = ctod('  /  /  ')
  w_TTLOCTIT = space(30)
  w_TTPROTIT = space(2)
  w_TT_SESSO = space(1)
  w_TTPRONAS = space(2)
  w_TTCAPTIT = space(9)
  w_TTINDIRI = space(35)
  w_TTLUONAS = space(30)
  w_SETELEFONO = space(12)
  w_AZ_EMAIL = space(50)
  w_TIPFS = space(2)
  w_CODAZI3 = space(5)
  w_SENATGIU = space(2)
  w_GTDENOMI = space(60)
  w_GTCOGNOM = space(24)
  w_GT__NOME = space(20)
  w_GT_SESSO = space(1)
  w_GTCOMNAS = space(40)
  w_GTPRONAS = space(2)
  w_GTDATNAS = ctod('  /  /  ')
  w_AIDATINV = ctod('  /  /  ')
  w_GTCODATT = space(6)
  w_GTTELEFO = space(12)
  w_GT__MAIL = space(100)
  w_GT_STATO = space(1)
  w_GTSITUAZ = space(1)
  w_GTNATGIU = space(2)
  w_GTCODDIC = space(11)
  w_GTTIP770 = space(5)
  w_TIPOCONT = space(1)
  w_HASEVCOP = space(50)
  w_HASEVENT = .F.
  w_GTPAEEST = space(3)
  w_GTIDFIES = space(24)
  w_RAPFIRM = space(10)
  w_RFCODFIS = space(16)
  w_RFCOGNOM = space(40)
  w_RF__NOME = space(40)
  w_RFCODCAR = space(2)
  w_RF_SESSO = space(1)
  w_RFDATNAS = ctod('  /  /  ')
  w_RFCOMNAS = space(40)
  w_RFPRONAS = space(2)
  w_RFTELEFO = space(18)
  w_RFDATCAR = ctod('  /  /  ')
  w_GTRAPFIR = space(1)
  o_GTRAPFIR = space(1)
  w_GTRAPFIS = space(16)
  w_GTRAPCAR = space(2)
  o_GTRAPCAR = space(2)
  w_GTCODFED = space(11)
  w_GTRAPCOG = space(24)
  w_GTRAPNOM = space(20)
  w_GTRAPSES = space(1)
  w_GTRAPDNS = ctod('  /  /  ')
  w_GTRAPNAS = space(40)
  w_GTRAPPRO = space(2)
  w_GTRAPEST = space(3)
  o_GTRAPEST = space(3)
  w_GTRAPFED = space(24)
  w_GTRAPRES = space(24)
  w_GTRAPIND = space(35)
  w_GTRAPTEL = space(12)
  w_GTRAPDCA = ctod('  /  /  ')
  w_GTRAPFAL = ctod('  /  /  ')
  w_GTNOTRAS = space(1)
  o_GTNOTRAS = space(1)
  w_GTSEZ1ST = space(1)
  w_GTSEZ1SV = space(1)
  w_GTSEZ1SX = space(1)
  w_GTSEZ1SY = space(1)
  w_GTSEZ1DI = space(1)
  w_GTLAVDIP = space(1)
  w_GTLAVAUT = space(1)
  o_GTLAVAUT = space(1)
  w_GTFISAGG = space(16)
  w_GTTIPINV = space(1)
  o_GTTIPINV = space(1)
  w_GTRIOPDP = space(1)
  w_GTRIOPAU = space(1)
  w_GTTRASNO = space(1)
  o_GTTRASNO = space(1)
  w_GTRIOPSS = space(1)
  w_GTRIOPDI = space(1)
  w_GTRIOPST = space(1)
  w_GTRIOPSV = space(1)
  w_GTRIOPSX = space(1)
  w_GTRIOPSY = space(1)
  o_GTRIOPSY = space(1)
  w_GTCFISAI = space(16)
  w_GTRISEDI = space(1)
  o_GTRISEDI = space(1)
  w_GTRISEAU = space(1)
  o_GTRISEAU = space(1)
  w_GTRISECA = space(1)
  o_GTRISECA = space(1)
  w_GTRISELB = space(1)
  o_GTRISELB = space(1)
  w_GTRISEAR = space(1)
  o_GTRISEAR = space(1)
  w_GTAICFIS = space(16)
  w_GTDIRISE = space(1)
  o_GTDIRISE = space(1)
  w_GTAURISE = space(1)
  o_GTAURISE = space(1)
  w_GTCARISE = space(1)
  o_GTCARISE = space(1)
  w_GTLBRISE = space(1)
  o_GTLBRISE = space(1)
  w_GTARRISE = space(1)
  o_GTARRISE = space(1)
  w_GTFIRDIC = space(1)
  w_GTSITPAR = space(2)
  w_GTATTEST = space(1)
  w_GTSOGGE1 = space(1)
  o_GTSOGGE1 = space(1)
  w_GTFISSG1 = space(16)
  w_GTFIRSG1 = space(1)
  w_GTSOGGE2 = space(1)
  o_GTSOGGE2 = space(1)
  w_GTFISSG2 = space(16)
  w_GTFIRSG2 = space(1)
  w_GTSOGGE3 = space(1)
  o_GTSOGGE3 = space(1)
  w_GTFISSG3 = space(16)
  w_GTFIRSG3 = space(1)
  w_GTSOGGE4 = space(1)
  o_GTSOGGE4 = space(1)
  w_GTFISSG4 = space(16)
  w_GTFIRSG4 = space(1)
  w_GTSOGGE5 = space(1)
  o_GTSOGGE5 = space(1)
  w_GTFISSG5 = space(16)
  w_GTFIRSG5 = space(1)
  w_GTIMPTRA = space(1)
  o_GTIMPTRA = space(1)
  w_GTINVTEL = space(1)
  o_GTINVTEL = space(1)
  w_GTRICAVV = space(1)
  w_GTCODINT = space(16)
  w_GTNUMCAF = 0
  w_GTDATIMP = ctod('  /  /  ')
  w_GTFIRINT = space(1)
  w_GTTIPFOR = space(2)
  w_GTFLGCON = space(1)
  w_GTFLGCOR = space(1)
  o_GTFLGCOR = space(1)
  w_GTFLGINT = space(1)
  o_GTFLGINT = space(1)
  w_EVEECC = space(2)
  o_EVEECC = space(2)
  w_EVEECC_2016 = space(2)
  o_EVEECC_2016 = space(2)
  w_EVEECC_2017 = space(2)
  o_EVEECC_2017 = space(2)
  w_GTEVEECZ = space(2)
  w_GTTIPSOS = space(1)
  w_GTPROTEC = space(17)
  o_GTPROTEC = space(17)
  w_GTPROSEZ = 0
  w_GTSELCAF = space(1)
  o_GTSELCAF = space(1)
  w_GTRESCAF = space(16)
  w_GTDELCAF = space(11)
  w_GTFLGFIR = space(1)
  w_GT__ANNO = 0
  w_GTNOMFIL = space(254)
  o_GTNOMFIL = space(254)
  w_DirName = space(200)

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_Gtserial = this.W_Gtserial

  * --- Children pointers
  Gsri_Mgt = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsri_agy
  * ---- Parametro Gestione
  pTipCer='    '
  
  * --- Per gestione sicurezza procedure con parametri
  func getSecuritycode()
    return(lower('GSRI_AGY,'+this.pTipCer))
  EndFunc
  
  
   * --- Gestiti i soli eventi Modifica, cancellazione, inserimento
  func HasCPEvents(i_cOp)
   if this.cFunction='Query'
  	  	if Upper(i_cop)=='ECPDELETE'
  	  		* esegue controllo se movimento bloccato se da problemi si pu� chiamare direttamente il Batch
          This.w_HASEVCOP=i_cop
  	  		This.NotifyEvent('HasEvent')
  	  		Return ( This.w_HASEVENT )
        Else
  	  		Return(.t.)
  	  	endif
  	Else
  	  	* --- Se l'evento non � modifica/cancellazione/inserimento prosegue
        Return(.t.)
    Endif
   EndFunc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=6, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'GEFILTEL','gsri_agy')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsri_agyPag1","gsri_agy",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Dati frontespizio")
      .Pages(1).HelpContextID = 161447458
      .Pages(2).addobject("oPag","tgsri_agyPag2","gsri_agy",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Rappresentante firmatario")
      .Pages(2).HelpContextID = 9811030
      .Pages(3).addobject("oPag","tgsri_agyPag3","gsri_agy",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Redazione della dichiarazione")
      .Pages(3).HelpContextID = 7150389
      .Pages(4).addobject("oPag","tgsri_agyPag4","gsri_agy",4)
      .Pages(4).oPag.Visible=.t.
      .Pages(4).Caption=cp_Translate("Firma/impegno trasmissione")
      .Pages(4).HelpContextID = 83896724
      .Pages(5).addobject("oPag","tgsri_agyPag5","gsri_agy",5)
      .Pages(5).oPag.Visible=.t.
      .Pages(5).Caption=cp_Translate("File telematico")
      .Pages(5).HelpContextID = 233618194
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- gsri_agy
    Getblackbversion()
    * --- Imposta il Titolo della Finestra della Gestione in Base al tipo
    WITH THIS.PARENT
       IF Type('pTipCer')='U' Or EMPTY(pTipCer)
          .pTipCer = '     '
       ELSE
          .pTipCer = pTipCer
       ENDIF
       DO CASE
             CASE pTipCer = '77S16'
                   .cComment = CP_TRANSLATE('Generazione file telematico 770/2016')
                   .cAutoZoom = 'GSRI_ADY'
             CASE pTipCer = '77S17'
                   .cComment = CP_TRANSLATE('Generazione file telematico 770/2017')
                   .cAutoZoom = 'GSRI_AGY'
             CASE pTipCer = '77S18'
                   .cComment = CP_TRANSLATE('Generazione file telematico 770/2018')
                   .cAutoZoom = 'GSRI_AGY_2018'
             Otherwise
                   .cComment = CP_TRANSLATE('Generazione file telematico')
                   .cAutoZoom = 'DEFAULT'
       ENDCASE
    ENDWIT
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='TITOLARI'
    this.cWorkTables[3]='SEDIAZIE'
    this.cWorkTables[4]='ENTI_COM'
    this.cWorkTables[5]='DAT_RAPP'
    this.cWorkTables[6]='GEFILTEL'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(6))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.GEFILTEL_IDX,5],7]
    this.nPostItConn=i_TableProp[this.GEFILTEL_IDX,3]
  return

  function CreateChildren()
    this.Gsri_Mgt = CREATEOBJECT('stdDynamicChild',this,'Gsri_Mgt',this.oPgFrm.Page5.oPag.oLinkPC_5_34)
    return

  procedure DestroyChildren()
    if !ISNULL(this.Gsri_Mgt)
      this.Gsri_Mgt.DestroyChildrenChain()
      this.Gsri_Mgt=.NULL.
    endif
    this.oPgFrm.Page5.oPag.RemoveObject('oLinkPC_5_34')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.Gsri_Mgt.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.Gsri_Mgt.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.Gsri_Mgt.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.Gsri_Mgt.SetKey(;
            .w_GTSERIAL,"GDSERIAL";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .Gsri_Mgt.ChangeRow(this.cRowID+'      1',1;
             ,.w_GTSERIAL,"GDSERIAL";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.Gsri_Mgt)
        i_f=.Gsri_Mgt.BuildFilter()
        if !(i_f==.Gsri_Mgt.cQueryFilter)
          i_fnidx=.Gsri_Mgt.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.Gsri_Mgt.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.Gsri_Mgt.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.Gsri_Mgt.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.Gsri_Mgt.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_GTSERIAL = NVL(GTSERIAL,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from GEFILTEL where GTSERIAL=KeySet.GTSERIAL
    *
    i_nConn = i_TableProp[this.GEFILTEL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.GEFILTEL_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('GEFILTEL')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "GEFILTEL.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' GEFILTEL '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'GTSERIAL',this.w_GTSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_CODAZI = i_CODAZI
        .w_AZCOFAZI = space(16)
        .w_AZPERAZI = space(1)
        .w_CODAZI2 = i_CODAZI
        .w_COGTIT1 = space(25)
        .w_NOMTIT1 = space(25)
        .w_TTTELEFO = space(18)
        .w_TTDATNAS = ctod("  /  /  ")
        .w_TTLOCTIT = space(30)
        .w_TTPROTIT = space(2)
        .w_TT_SESSO = space(1)
        .w_TTPRONAS = space(2)
        .w_TTCAPTIT = space(9)
        .w_TTINDIRI = space(35)
        .w_TTLUONAS = space(30)
        .w_SETELEFONO = space(12)
        .w_AZ_EMAIL = space(50)
        .w_TIPFS = 'DF' 
        .w_CODAZI3 = i_CODAZI
        .w_SENATGIU = space(2)
        .w_AIDATINV = i_DATSYS
        .w_TIPOCONT = 'I'
        .w_HASEVCOP = space(50)
        .w_HASEVENT = .f.
        .w_RAPFIRM = i_Codazi
        .w_RFCODFIS = space(16)
        .w_RFCOGNOM = space(40)
        .w_RF__NOME = space(40)
        .w_RFCODCAR = space(2)
        .w_RF_SESSO = space(1)
        .w_RFDATNAS = ctod("  /  /  ")
        .w_RFCOMNAS = space(40)
        .w_RFPRONAS = space(2)
        .w_RFTELEFO = space(18)
        .w_RFDATCAR = ctod("  /  /  ")
        .w_EVEECC = '00'
        .w_EVEECC_2016 = '00'
        .w_EVEECC_2017 = '00'
        .w_DirName = sys(5)+sys(2003)+'\'
        .w_GTSERIAL = NVL(GTSERIAL,space(10))
        .op_GTSERIAL = .w_GTSERIAL
        .w_GTDATGEN = NVL(cp_ToDate(GTDATGEN),ctod("  /  /  "))
        .w_GTDESAGG = NVL(GTDESAGG,space(254))
          .link_1_4('Load')
        .w_GTCODFIS = NVL(GTCODFIS,space(16))
        .w_GTCODSOS = NVL(GTCODSOS,space(16))
          .link_1_9('Load')
          .link_1_24('Load')
        .w_GTDENOMI = NVL(GTDENOMI,space(60))
        .w_GTCOGNOM = NVL(GTCOGNOM,space(24))
        .w_GT__NOME = NVL(GT__NOME,space(20))
        .w_GT_SESSO = NVL(GT_SESSO,space(1))
        .w_GTCOMNAS = NVL(GTCOMNAS,space(40))
        .w_GTPRONAS = NVL(GTPRONAS,space(2))
        .w_GTDATNAS = NVL(cp_ToDate(GTDATNAS),ctod("  /  /  "))
        .w_GTCODATT = NVL(GTCODATT,space(6))
        .w_GTTELEFO = NVL(GTTELEFO,space(12))
        .w_GT__MAIL = NVL(GT__MAIL,space(100))
        .w_GT_STATO = NVL(GT_STATO,space(1))
        .w_GTSITUAZ = NVL(GTSITUAZ,space(1))
        .w_GTNATGIU = NVL(GTNATGIU,space(2))
        .w_GTCODDIC = NVL(GTCODDIC,space(11))
        .w_GTTIP770 = NVL(GTTIP770,space(5))
        .oPgFrm.Page1.oPag.oObj_1_62.Calculate()
        .w_GTPAEEST = NVL(GTPAEEST,space(3))
        .w_GTIDFIES = NVL(GTIDFIES,space(24))
          .link_2_2('Load')
        .w_GTRAPFIR = NVL(GTRAPFIR,space(1))
        .w_GTRAPFIS = NVL(GTRAPFIS,space(16))
        .w_GTRAPCAR = NVL(GTRAPCAR,space(2))
        .w_GTCODFED = NVL(GTCODFED,space(11))
        .w_GTRAPCOG = NVL(GTRAPCOG,space(24))
        .w_GTRAPNOM = NVL(GTRAPNOM,space(20))
        .w_GTRAPSES = NVL(GTRAPSES,space(1))
        .w_GTRAPDNS = NVL(cp_ToDate(GTRAPDNS),ctod("  /  /  "))
        .w_GTRAPNAS = NVL(GTRAPNAS,space(40))
        .w_GTRAPPRO = NVL(GTRAPPRO,space(2))
        .w_GTRAPEST = NVL(GTRAPEST,space(3))
        .w_GTRAPFED = NVL(GTRAPFED,space(24))
        .w_GTRAPRES = NVL(GTRAPRES,space(24))
        .w_GTRAPIND = NVL(GTRAPIND,space(35))
        .w_GTRAPTEL = NVL(GTRAPTEL,space(12))
        .w_GTRAPDCA = NVL(cp_ToDate(GTRAPDCA),ctod("  /  /  "))
        .w_GTRAPFAL = NVL(cp_ToDate(GTRAPFAL),ctod("  /  /  "))
        .w_GTNOTRAS = NVL(GTNOTRAS,space(1))
        .w_GTSEZ1ST = NVL(GTSEZ1ST,space(1))
        .w_GTSEZ1SV = NVL(GTSEZ1SV,space(1))
        .w_GTSEZ1SX = NVL(GTSEZ1SX,space(1))
        .w_GTSEZ1SY = NVL(GTSEZ1SY,space(1))
        .w_GTSEZ1DI = NVL(GTSEZ1DI,space(1))
        .w_GTLAVDIP = NVL(GTLAVDIP,space(1))
        .w_GTLAVAUT = NVL(GTLAVAUT,space(1))
        .w_GTFISAGG = NVL(GTFISAGG,space(16))
        .w_GTTIPINV = NVL(GTTIPINV,space(1))
        .w_GTRIOPDP = NVL(GTRIOPDP,space(1))
        .w_GTRIOPAU = NVL(GTRIOPAU,space(1))
        .w_GTTRASNO = NVL(GTTRASNO,space(1))
        .w_GTRIOPSS = NVL(GTRIOPSS,space(1))
        .w_GTRIOPDI = NVL(GTRIOPDI,space(1))
        .w_GTRIOPST = NVL(GTRIOPST,space(1))
        .w_GTRIOPSV = NVL(GTRIOPSV,space(1))
        .w_GTRIOPSX = NVL(GTRIOPSX,space(1))
        .w_GTRIOPSY = NVL(GTRIOPSY,space(1))
        .w_GTCFISAI = NVL(GTCFISAI,space(16))
        .w_GTRISEDI = NVL(GTRISEDI,space(1))
        .w_GTRISEAU = NVL(GTRISEAU,space(1))
        .w_GTRISECA = NVL(GTRISECA,space(1))
        .w_GTRISELB = NVL(GTRISELB,space(1))
        .w_GTRISEAR = NVL(GTRISEAR,space(1))
        .w_GTAICFIS = NVL(GTAICFIS,space(16))
        .w_GTDIRISE = NVL(GTDIRISE,space(1))
        .w_GTAURISE = NVL(GTAURISE,space(1))
        .w_GTCARISE = NVL(GTCARISE,space(1))
        .w_GTLBRISE = NVL(GTLBRISE,space(1))
        .w_GTARRISE = NVL(GTARRISE,space(1))
        .w_GTFIRDIC = NVL(GTFIRDIC,space(1))
        .w_GTSITPAR = NVL(GTSITPAR,space(2))
        .w_GTATTEST = NVL(GTATTEST,space(1))
        .w_GTSOGGE1 = NVL(GTSOGGE1,space(1))
        .w_GTFISSG1 = NVL(GTFISSG1,space(16))
        .w_GTFIRSG1 = NVL(GTFIRSG1,space(1))
        .w_GTSOGGE2 = NVL(GTSOGGE2,space(1))
        .w_GTFISSG2 = NVL(GTFISSG2,space(16))
        .w_GTFIRSG2 = NVL(GTFIRSG2,space(1))
        .w_GTSOGGE3 = NVL(GTSOGGE3,space(1))
        .w_GTFISSG3 = NVL(GTFISSG3,space(16))
        .w_GTFIRSG3 = NVL(GTFIRSG3,space(1))
        .w_GTSOGGE4 = NVL(GTSOGGE4,space(1))
        .w_GTFISSG4 = NVL(GTFISSG4,space(16))
        .w_GTFIRSG4 = NVL(GTFIRSG4,space(1))
        .w_GTSOGGE5 = NVL(GTSOGGE5,space(1))
        .w_GTFISSG5 = NVL(GTFISSG5,space(16))
        .w_GTFIRSG5 = NVL(GTFIRSG5,space(1))
        .w_GTIMPTRA = NVL(GTIMPTRA,space(1))
        .w_GTINVTEL = NVL(GTINVTEL,space(1))
        .w_GTRICAVV = NVL(GTRICAVV,space(1))
        .w_GTCODINT = NVL(GTCODINT,space(16))
        .w_GTNUMCAF = NVL(GTNUMCAF,0)
        .w_GTDATIMP = NVL(cp_ToDate(GTDATIMP),ctod("  /  /  "))
        .w_GTFIRINT = NVL(GTFIRINT,space(1))
        .w_GTTIPFOR = NVL(GTTIPFOR,space(2))
        .w_GTFLGCON = NVL(GTFLGCON,space(1))
        .w_GTFLGCOR = NVL(GTFLGCOR,space(1))
        .w_GTFLGINT = NVL(GTFLGINT,space(1))
        .w_GTEVEECZ = NVL(GTEVEECZ,space(2))
        .w_GTTIPSOS = NVL(GTTIPSOS,space(1))
        .w_GTPROTEC = NVL(GTPROTEC,space(17))
        .w_GTPROSEZ = NVL(GTPROSEZ,0)
        .w_GTSELCAF = NVL(GTSELCAF,space(1))
        .w_GTRESCAF = NVL(GTRESCAF,space(16))
        .w_GTDELCAF = NVL(GTDELCAF,space(11))
        .w_GTFLGFIR = NVL(GTFLGFIR,space(1))
        .w_GT__ANNO = NVL(GT__ANNO,0)
        .w_GTNOMFIL = NVL(GTNOMFIL,space(254))
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'GEFILTEL')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page5.oPag.oBtn_5_35.enabled = this.oPgFrm.Page5.oPag.oBtn_5_35.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_GTSERIAL = space(10)
      .w_GTDATGEN = ctod("  /  /  ")
      .w_GTDESAGG = space(254)
      .w_CODAZI = space(5)
      .w_AZCOFAZI = space(16)
      .w_AZPERAZI = space(1)
      .w_GTCODFIS = space(16)
      .w_GTCODSOS = space(16)
      .w_CODAZI2 = space(5)
      .w_COGTIT1 = space(25)
      .w_NOMTIT1 = space(25)
      .w_TTTELEFO = space(18)
      .w_TTDATNAS = ctod("  /  /  ")
      .w_TTLOCTIT = space(30)
      .w_TTPROTIT = space(2)
      .w_TT_SESSO = space(1)
      .w_TTPRONAS = space(2)
      .w_TTCAPTIT = space(9)
      .w_TTINDIRI = space(35)
      .w_TTLUONAS = space(30)
      .w_SETELEFONO = space(12)
      .w_AZ_EMAIL = space(50)
      .w_TIPFS = space(2)
      .w_CODAZI3 = space(5)
      .w_SENATGIU = space(2)
      .w_GTDENOMI = space(60)
      .w_GTCOGNOM = space(24)
      .w_GT__NOME = space(20)
      .w_GT_SESSO = space(1)
      .w_GTCOMNAS = space(40)
      .w_GTPRONAS = space(2)
      .w_GTDATNAS = ctod("  /  /  ")
      .w_AIDATINV = ctod("  /  /  ")
      .w_GTCODATT = space(6)
      .w_GTTELEFO = space(12)
      .w_GT__MAIL = space(100)
      .w_GT_STATO = space(1)
      .w_GTSITUAZ = space(1)
      .w_GTNATGIU = space(2)
      .w_GTCODDIC = space(11)
      .w_GTTIP770 = space(5)
      .w_TIPOCONT = space(1)
      .w_HASEVCOP = space(50)
      .w_HASEVENT = .f.
      .w_GTPAEEST = space(3)
      .w_GTIDFIES = space(24)
      .w_RAPFIRM = space(10)
      .w_RFCODFIS = space(16)
      .w_RFCOGNOM = space(40)
      .w_RF__NOME = space(40)
      .w_RFCODCAR = space(2)
      .w_RF_SESSO = space(1)
      .w_RFDATNAS = ctod("  /  /  ")
      .w_RFCOMNAS = space(40)
      .w_RFPRONAS = space(2)
      .w_RFTELEFO = space(18)
      .w_RFDATCAR = ctod("  /  /  ")
      .w_GTRAPFIR = space(1)
      .w_GTRAPFIS = space(16)
      .w_GTRAPCAR = space(2)
      .w_GTCODFED = space(11)
      .w_GTRAPCOG = space(24)
      .w_GTRAPNOM = space(20)
      .w_GTRAPSES = space(1)
      .w_GTRAPDNS = ctod("  /  /  ")
      .w_GTRAPNAS = space(40)
      .w_GTRAPPRO = space(2)
      .w_GTRAPEST = space(3)
      .w_GTRAPFED = space(24)
      .w_GTRAPRES = space(24)
      .w_GTRAPIND = space(35)
      .w_GTRAPTEL = space(12)
      .w_GTRAPDCA = ctod("  /  /  ")
      .w_GTRAPFAL = ctod("  /  /  ")
      .w_GTNOTRAS = space(1)
      .w_GTSEZ1ST = space(1)
      .w_GTSEZ1SV = space(1)
      .w_GTSEZ1SX = space(1)
      .w_GTSEZ1SY = space(1)
      .w_GTSEZ1DI = space(1)
      .w_GTLAVDIP = space(1)
      .w_GTLAVAUT = space(1)
      .w_GTFISAGG = space(16)
      .w_GTTIPINV = space(1)
      .w_GTRIOPDP = space(1)
      .w_GTRIOPAU = space(1)
      .w_GTTRASNO = space(1)
      .w_GTRIOPSS = space(1)
      .w_GTRIOPDI = space(1)
      .w_GTRIOPST = space(1)
      .w_GTRIOPSV = space(1)
      .w_GTRIOPSX = space(1)
      .w_GTRIOPSY = space(1)
      .w_GTCFISAI = space(16)
      .w_GTRISEDI = space(1)
      .w_GTRISEAU = space(1)
      .w_GTRISECA = space(1)
      .w_GTRISELB = space(1)
      .w_GTRISEAR = space(1)
      .w_GTAICFIS = space(16)
      .w_GTDIRISE = space(1)
      .w_GTAURISE = space(1)
      .w_GTCARISE = space(1)
      .w_GTLBRISE = space(1)
      .w_GTARRISE = space(1)
      .w_GTFIRDIC = space(1)
      .w_GTSITPAR = space(2)
      .w_GTATTEST = space(1)
      .w_GTSOGGE1 = space(1)
      .w_GTFISSG1 = space(16)
      .w_GTFIRSG1 = space(1)
      .w_GTSOGGE2 = space(1)
      .w_GTFISSG2 = space(16)
      .w_GTFIRSG2 = space(1)
      .w_GTSOGGE3 = space(1)
      .w_GTFISSG3 = space(16)
      .w_GTFIRSG3 = space(1)
      .w_GTSOGGE4 = space(1)
      .w_GTFISSG4 = space(16)
      .w_GTFIRSG4 = space(1)
      .w_GTSOGGE5 = space(1)
      .w_GTFISSG5 = space(16)
      .w_GTFIRSG5 = space(1)
      .w_GTIMPTRA = space(1)
      .w_GTINVTEL = space(1)
      .w_GTRICAVV = space(1)
      .w_GTCODINT = space(16)
      .w_GTNUMCAF = 0
      .w_GTDATIMP = ctod("  /  /  ")
      .w_GTFIRINT = space(1)
      .w_GTTIPFOR = space(2)
      .w_GTFLGCON = space(1)
      .w_GTFLGCOR = space(1)
      .w_GTFLGINT = space(1)
      .w_EVEECC = space(2)
      .w_EVEECC_2016 = space(2)
      .w_EVEECC_2017 = space(2)
      .w_GTEVEECZ = space(2)
      .w_GTTIPSOS = space(1)
      .w_GTPROTEC = space(17)
      .w_GTPROSEZ = 0
      .w_GTSELCAF = space(1)
      .w_GTRESCAF = space(16)
      .w_GTDELCAF = space(11)
      .w_GTFLGFIR = space(1)
      .w_GT__ANNO = 0
      .w_GTNOMFIL = space(254)
      .w_DirName = space(200)
      if .cFunction<>"Filter"
          .DoRTCalc(1,1,.f.)
        .w_GTDATGEN = i_Datsys
          .DoRTCalc(3,3,.f.)
        .w_CODAZI = i_CODAZI
        .DoRTCalc(4,4,.f.)
          if not(empty(.w_CODAZI))
          .link_1_4('Full')
          endif
          .DoRTCalc(5,6,.f.)
        .w_GTCODFIS = IIF(LEN(ALLTRIM(.w_AZCOFAZI))<=16, LEFT(ALLTRIM(.w_AZCOFAZI),16), RIGHT(ALLTRIM(.w_AZCOFAZI),16))
          .DoRTCalc(8,8,.f.)
        .w_CODAZI2 = i_CODAZI
        .DoRTCalc(9,9,.f.)
          if not(empty(.w_CODAZI2))
          .link_1_9('Full')
          endif
          .DoRTCalc(10,22,.f.)
        .w_TIPFS = 'DF' 
        .w_CODAZI3 = i_CODAZI
        .DoRTCalc(24,24,.f.)
          if not(empty(.w_CODAZI3))
          .link_1_24('Full')
          endif
          .DoRTCalc(25,25,.f.)
        .w_GTDENOMI = LEFT(iif(.w_AZPERAZI<>'S', UPPER(g_RAGAZI), space(60)), 60)
        .w_GTCOGNOM = LEFT(iif(.w_AZPERAZI='S',Upper(.w_COGTIT1), space(24)), 24)
        .w_GT__NOME = LEFT(iif(.w_AZPERAZI='S',Upper(.w_NOMTIT1), space(20)), 20)
        .w_GT_SESSO = iif(.w_AZPERAZI='S' and not empty(.w_TT_SESSO), .w_TT_SESSO, 'M')
        .w_GTCOMNAS = left(iif(.w_AZPERAZI='S', UPPER(.w_TTLUONAS), space(40)), 40)
        .w_GTPRONAS = iif(.w_AZPERAZI='S', UPPER(.w_TTPRONAS), space(2))
        .w_GTDATNAS = iif(.w_AZPERAZI='S', .w_TTDATNAS, CTOD("  -  -    "))
        .w_AIDATINV = i_DATSYS
        .w_GTCODATT = iif(Empty(.w_Gtcodatt),iif(g_Attivi='S', Space(6), Left(Alltrim(Calattso(g_Catazi,Alltrim(Str(Year(i_Datsys))),12)),6)),.w_Gtcodatt)
        .w_GTTELEFO = Left(iif(.w_Azperazi='S', .w_Tttelefo, .w_Setelefono),12)
        .w_GT__MAIL = left(UPPER(.w_AZ_EMAIL), 100)
        .w_GT_STATO = iif(.w_Azperazi<>'S', .w_Gt_stato, Space(1))
        .w_GTSITUAZ = iif(.w_Azperazi<>'S', .w_Gtsituaz, Space(1))
        .w_GTNATGIU = Upper(iif(.w_Azperazi<>'S', .w_Senatgiu, Space(2)))
        .w_GTCODDIC = iif(.w_Azperazi<>'S', .w_Gtcoddic, Space(1))
        .w_GTTIP770 = THIS.pTipCer
        .oPgFrm.Page1.oPag.oObj_1_62.Calculate()
        .w_TIPOCONT = 'I'
          .DoRTCalc(43,46,.f.)
        .w_RAPFIRM = i_Codazi
        .DoRTCalc(47,47,.f.)
          if not(empty(.w_RAPFIRM))
          .link_2_2('Full')
          endif
          .DoRTCalc(48,57,.f.)
        .w_GTRAPFIR = iif(.w_Azperazi<>'S','S','N')
        .w_GTRAPFIS = iif(.w_Gtrapfir='S',.w_Rfcodfis,Space(16))
        .w_GTRAPCAR = iif(.w_Gtrapfir='N','1',.w_Rfcodcar)
        .w_GTCODFED = Space(11)
        .w_GTRAPCOG = iif(.w_Gtrapfir='N',space(20),Left(.w_Rfcognom,24))
        .w_GTRAPNOM = iif(.w_GtRapFir='N',space(20),Left(.w_Rf__Nome,20))
        .w_GTRAPSES = iif(.w_GtRapFir='N','M',.w_Rf_Sesso)
        .w_GTRAPDNS = iif(.w_GtRapFir='N',Ctod('  -  -    '),.w_Rfdatnas)
        .w_GTRAPNAS = iif(.w_GtRapFir='N',space(40),Left(.w_Rfcomnas,40))
        .w_GTRAPPRO = iif(.w_GtRapFir='N',space(2),Left(.w_Rfpronas,2))
        .w_GTRAPEST = Space(3)
        .w_GTRAPFED = Space(24)
        .w_GTRAPRES = Space(24)
        .w_GTRAPIND = Space(35)
        .w_GTRAPTEL = iif(.w_GtRapFir='N',space(12),Left(.w_Rftelefo,12))
        .w_GTRAPDCA = iif(.w_GtRapFir='N',Ctod('  -  -    '),.w_Rfdatcar)
        .w_GTRAPFAL = iif(inlist(.w_Gtrapcar,'3','4') And .w_GtRapFir='S',.w_Gtrapfal,ctod('  -  -    '))
        .w_GTNOTRAS = '0'
        .w_GTSEZ1ST = '1'
        .w_GTSEZ1SV = '0'
        .w_GTSEZ1SX = '0'
        .w_GTSEZ1SY = '0'
        .w_GTSEZ1DI = '0'
        .w_GTLAVDIP = '0'
        .w_GTLAVAUT = '0'
          .DoRTCalc(83,83,.f.)
        .w_GTTIPINV = "1"
        .w_GTRIOPDP = '0'
        .w_GTRIOPAU = '1'
        .w_GTTRASNO = '0'
        .w_GTRIOPSS = '0'
        .w_GTRIOPDI = '0'
        .w_GTRIOPST = '1'
        .w_GTRIOPSV = '0'
        .w_GTRIOPSX = '0'
        .w_GTRIOPSY = '0'
        .w_GTCFISAI = Space(16)
        .w_GTRISEDI = '0'
        .w_GTRISEAU = '0'
        .w_GTRISECA = '0'
        .w_GTRISELB = '0'
        .w_GTRISEAR = '0'
        .w_GTAICFIS = Space(16)
        .w_GTDIRISE = '0'
        .w_GTAURISE = '0'
        .w_GTCARISE = '0'
        .w_GTLBRISE = '0'
        .w_GTARRISE = '0'
        .w_GTFIRDIC = '0'
        .w_GTSITPAR = '00'
        .w_GTATTEST = IIF(.w_GTSOGGE1='0' AND .w_GTSOGGE2='0' AND .w_GTSOGGE3='0' AND .w_GTSOGGE4='0' AND .w_GTSOGGE5='0','0',.w_GTATTEST)
        .w_GTSOGGE1 = '0'
        .w_GTFISSG1 = IIF(.w_GTSOGGE1='0',SPACE(16),.w_GTFISSG1)
        .w_GTFIRSG1 = iif(.w_GTSOGGE1$'124','1','0')
        .w_GTSOGGE2 = '0'
        .w_GTFISSG2 = IIF(.w_GTSOGGE2='0',SPACE(16),.w_GTFISSG2)
        .w_GTFIRSG2 = iif(.w_GTSOGGE2$'124','1','0')
        .w_GTSOGGE3 = '0'
        .w_GTFISSG3 = IIF(.w_GTSOGGE3='0',SPACE(16),.w_GTFISSG3)
        .w_GTFIRSG3 = iif(.w_GTSOGGE3$'124','1','0')
        .w_GTSOGGE4 = '0'
        .w_GTFISSG4 = IIF(.w_GTSOGGE4='0',SPACE(16),.w_GTFISSG4)
        .w_GTFIRSG4 = iif(.w_GTSOGGE4$'124','1','0')
        .w_GTSOGGE5 = '0'
        .w_GTFISSG5 = IIF(.w_GTSOGGE5='0',SPACE(16),.w_GTFISSG5)
        .w_GTFIRSG5 = iif(.w_GTSOGGE5$'124','1','0')
        .w_GTIMPTRA = '0'
        .w_GTINVTEL = iif(.w_GTIMPTRA <> '0',.w_GTINVTEL,'0')
        .w_GTRICAVV = iif(.w_GTINVTEL='1',.w_GTRICAVV,'0')
        .w_GTCODINT = Space(16)
        .w_GTNUMCAF = 0
        .w_GTDATIMP = Ctod('  -  -    ')
        .w_GTFIRINT = '0'
        .w_GTTIPFOR = '01'
        .w_GTFLGCON = '0'
        .w_GTFLGCOR = iif(.w_GTFLGINT='1','0',.w_GTFLGCOR)
        .w_GTFLGINT = iif(.w_GTFLGCOR='1','0',.w_GTFLGINT)
        .w_EVEECC = '00'
        .w_EVEECC_2016 = '00'
        .w_EVEECC_2017 = '00'
          .DoRTCalc(138,138,.f.)
        .w_GTTIPSOS = '1'
        .w_GTPROTEC = Space(17)
        .w_GTPROSEZ = 0
        .w_GTSELCAF = '1'
        .w_GTRESCAF = Space(16)
        .w_GTDELCAF = iif(.w_Gtselcaf <> '1',SPACE(11),.w_Gtdelcaf)
        .w_GTFLGFIR = '0'
        .w_GT__ANNO = year(i_DATSYS)-1
          .DoRTCalc(147,147,.f.)
        .w_DirName = sys(5)+sys(2003)+'\'
      endif
    endwith
    cp_BlankRecExtFlds(this,'GEFILTEL')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page5.oPag.oBtn_5_35.enabled = this.oPgFrm.Page5.oPag.oBtn_5_35.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.GEFILTEL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.GEFILTEL_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"GT770","i_codazi,w_Gtserial")
      .op_codazi = .w_codazi
      .op_Gtserial = .w_Gtserial
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oGTDATGEN_1_2.enabled = i_bVal
      .Page1.oPag.oGTDESAGG_1_3.enabled = i_bVal
      .Page1.oPag.oGTCODFIS_1_7.enabled = i_bVal
      .Page1.oPag.oGTCODSOS_1_8.enabled = i_bVal
      .Page1.oPag.oGTDENOMI_1_26.enabled = i_bVal
      .Page1.oPag.oGTCOGNOM_1_27.enabled = i_bVal
      .Page1.oPag.oGT__NOME_1_28.enabled = i_bVal
      .Page1.oPag.oGT_SESSO_1_29.enabled = i_bVal
      .Page1.oPag.oGTCOMNAS_1_30.enabled = i_bVal
      .Page1.oPag.oGTPRONAS_1_31.enabled = i_bVal
      .Page1.oPag.oGTDATNAS_1_32.enabled = i_bVal
      .Page1.oPag.oGTCODATT_1_34.enabled = i_bVal
      .Page1.oPag.oGTTELEFO_1_35.enabled = i_bVal
      .Page1.oPag.oGT__MAIL_1_36.enabled = i_bVal
      .Page1.oPag.oGT_STATO_1_37.enabled = i_bVal
      .Page1.oPag.oGTSITUAZ_1_38.enabled = i_bVal
      .Page1.oPag.oGTNATGIU_1_39.enabled = i_bVal
      .Page1.oPag.oGTCODDIC_1_40.enabled = i_bVal
      .Page1.oPag.oGTPAEEST_1_69.enabled = i_bVal
      .Page1.oPag.oGTIDFIES_1_71.enabled = i_bVal
      .Page2.oPag.oGTRAPFIR_2_13.enabled = i_bVal
      .Page2.oPag.oGTRAPFIS_2_14.enabled = i_bVal
      .Page2.oPag.oGTRAPCAR_2_16.enabled = i_bVal
      .Page2.oPag.oGTCODFED_2_17.enabled = i_bVal
      .Page2.oPag.oGTRAPCOG_2_19.enabled = i_bVal
      .Page2.oPag.oGTRAPNOM_2_20.enabled = i_bVal
      .Page2.oPag.oGTRAPSES_2_25.enabled = i_bVal
      .Page2.oPag.oGTRAPDNS_2_27.enabled = i_bVal
      .Page2.oPag.oGTRAPNAS_2_30.enabled = i_bVal
      .Page2.oPag.oGTRAPPRO_2_31.enabled = i_bVal
      .Page2.oPag.oGTRAPEST_2_34.enabled = i_bVal
      .Page2.oPag.oGTRAPFED_2_36.enabled = i_bVal
      .Page2.oPag.oGTRAPRES_2_37.enabled = i_bVal
      .Page2.oPag.oGTRAPIND_2_39.enabled = i_bVal
      .Page2.oPag.oGTRAPTEL_2_41.enabled = i_bVal
      .Page2.oPag.oGTRAPDCA_2_44.enabled = i_bVal
      .Page2.oPag.oGTRAPFAL_2_45.enabled = i_bVal
      .Page2.oPag.oGTNOTRAS_2_49.enabled = i_bVal
      .Page2.oPag.oGTSEZ1ST_2_50.enabled = i_bVal
      .Page2.oPag.oGTSEZ1SV_2_51.enabled = i_bVal
      .Page2.oPag.oGTSEZ1SX_2_52.enabled = i_bVal
      .Page2.oPag.oGTSEZ1SY_2_53.enabled = i_bVal
      .Page2.oPag.oGTSEZ1DI_2_54.enabled = i_bVal
      .Page2.oPag.oGTLAVDIP_2_55.enabled = i_bVal
      .Page2.oPag.oGTLAVAUT_2_56.enabled = i_bVal
      .Page2.oPag.oGTFISAGG_2_58.enabled = i_bVal
      .Page3.oPag.oGTTIPINV_3_1.enabled = i_bVal
      .Page3.oPag.oGTRIOPDP_3_2.enabled = i_bVal
      .Page3.oPag.oGTRIOPAU_3_3.enabled = i_bVal
      .Page3.oPag.oGTTRASNO_3_4.enabled = i_bVal
      .Page3.oPag.oGTRIOPSS_3_5.enabled = i_bVal
      .Page3.oPag.oGTRIOPDI_3_6.enabled = i_bVal
      .Page3.oPag.oGTRIOPST_3_7.enabled = i_bVal
      .Page3.oPag.oGTRIOPSV_3_8.enabled = i_bVal
      .Page3.oPag.oGTRIOPSX_3_9.enabled = i_bVal
      .Page3.oPag.oGTRIOPSY_3_10.enabled = i_bVal
      .Page3.oPag.oGTCFISAI_3_11.enabled = i_bVal
      .Page3.oPag.oGTRISEDI_3_12.enabled = i_bVal
      .Page3.oPag.oGTRISEAU_3_13.enabled = i_bVal
      .Page3.oPag.oGTRISECA_3_14.enabled = i_bVal
      .Page3.oPag.oGTRISELB_3_15.enabled = i_bVal
      .Page3.oPag.oGTRISEAR_3_16.enabled = i_bVal
      .Page3.oPag.oGTAICFIS_3_17.enabled = i_bVal
      .Page3.oPag.oGTDIRISE_3_18.enabled = i_bVal
      .Page3.oPag.oGTAURISE_3_19.enabled = i_bVal
      .Page3.oPag.oGTCARISE_3_20.enabled = i_bVal
      .Page3.oPag.oGTLBRISE_3_21.enabled = i_bVal
      .Page3.oPag.oGTARRISE_3_22.enabled = i_bVal
      .Page4.oPag.oGTFIRDIC_4_3.enabled = i_bVal
      .Page4.oPag.oGTSITPAR_4_5.enabled = i_bVal
      .Page4.oPag.oGTATTEST_4_6.enabled = i_bVal
      .Page4.oPag.oGTSOGGE1_4_10.enabled = i_bVal
      .Page4.oPag.oGTFISSG1_4_11.enabled = i_bVal
      .Page4.oPag.oGTFIRSG1_4_12.enabled = i_bVal
      .Page4.oPag.oGTSOGGE2_4_13.enabled = i_bVal
      .Page4.oPag.oGTFISSG2_4_14.enabled = i_bVal
      .Page4.oPag.oGTFIRSG2_4_15.enabled = i_bVal
      .Page4.oPag.oGTSOGGE3_4_16.enabled = i_bVal
      .Page4.oPag.oGTFISSG3_4_17.enabled = i_bVal
      .Page4.oPag.oGTFIRSG3_4_18.enabled = i_bVal
      .Page4.oPag.oGTSOGGE4_4_19.enabled = i_bVal
      .Page4.oPag.oGTFISSG4_4_20.enabled = i_bVal
      .Page4.oPag.oGTFIRSG4_4_21.enabled = i_bVal
      .Page4.oPag.oGTSOGGE5_4_22.enabled = i_bVal
      .Page4.oPag.oGTFISSG5_4_23.enabled = i_bVal
      .Page4.oPag.oGTFIRSG5_4_24.enabled = i_bVal
      .Page4.oPag.oGTIMPTRA_4_25.enabled = i_bVal
      .Page4.oPag.oGTINVTEL_4_26.enabled = i_bVal
      .Page4.oPag.oGTRICAVV_4_27.enabled = i_bVal
      .Page4.oPag.oGTCODINT_4_30.enabled = i_bVal
      .Page4.oPag.oGTNUMCAF_4_34.enabled = i_bVal
      .Page4.oPag.oGTDATIMP_4_36.enabled = i_bVal
      .Page4.oPag.oGTFIRINT_4_37.enabled = i_bVal
      .Page5.oPag.oGTTIPFOR_5_2.enabled = i_bVal
      .Page5.oPag.oGTFLGCON_5_3.enabled = i_bVal
      .Page5.oPag.oGTFLGCOR_5_4.enabled = i_bVal
      .Page5.oPag.oGTFLGINT_5_5.enabled = i_bVal
      .Page5.oPag.oEVEECC_5_6.enabled = i_bVal
      .Page5.oPag.oEVEECC_2016_5_7.enabled = i_bVal
      .Page5.oPag.oEVEECC_2017_5_8.enabled = i_bVal
      .Page5.oPag.oGTPROTEC_5_11.enabled = i_bVal
      .Page5.oPag.oGTPROSEZ_5_12.enabled = i_bVal
      .Page5.oPag.oGTSELCAF_5_16.enabled = i_bVal
      .Page5.oPag.oGTRESCAF_5_19.enabled = i_bVal
      .Page5.oPag.oGTDELCAF_5_21.enabled = i_bVal
      .Page5.oPag.oGTFLGFIR_5_22.enabled = i_bVal
      .Page5.oPag.oGT__ANNO_5_24.enabled = i_bVal
      .Page5.oPag.oGTNOMFIL_5_28.enabled = i_bVal
      .Page5.oPag.oBtn_5_31.enabled = i_bVal
      .Page5.oPag.oBtn_5_33.enabled = i_bVal
      .Page5.oPag.oBtn_5_35.enabled = .Page5.oPag.oBtn_5_35.mCond()
      .Page5.oPag.oBtn_5_43.enabled = i_bVal
      .Page5.oPag.oBtn_5_48.enabled = i_bVal
      .Page1.oPag.oObj_1_62.enabled = i_bVal
      if i_cOp = "Query"
        .Page1.oPag.oGTDESAGG_1_3.enabled = .t.
      endif
    endwith
    this.Gsri_Mgt.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'GEFILTEL',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.Gsri_Mgt.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.GEFILTEL_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTSERIAL,"GTSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTDATGEN,"GTDATGEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTDESAGG,"GTDESAGG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTCODFIS,"GTCODFIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTCODSOS,"GTCODSOS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTDENOMI,"GTDENOMI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTCOGNOM,"GTCOGNOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GT__NOME,"GT__NOME",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GT_SESSO,"GT_SESSO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTCOMNAS,"GTCOMNAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTPRONAS,"GTPRONAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTDATNAS,"GTDATNAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTCODATT,"GTCODATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTTELEFO,"GTTELEFO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GT__MAIL,"GT__MAIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GT_STATO,"GT_STATO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTSITUAZ,"GTSITUAZ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTNATGIU,"GTNATGIU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTCODDIC,"GTCODDIC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTTIP770,"GTTIP770",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTPAEEST,"GTPAEEST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTIDFIES,"GTIDFIES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTRAPFIR,"GTRAPFIR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTRAPFIS,"GTRAPFIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTRAPCAR,"GTRAPCAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTCODFED,"GTCODFED",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTRAPCOG,"GTRAPCOG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTRAPNOM,"GTRAPNOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTRAPSES,"GTRAPSES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTRAPDNS,"GTRAPDNS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTRAPNAS,"GTRAPNAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTRAPPRO,"GTRAPPRO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTRAPEST,"GTRAPEST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTRAPFED,"GTRAPFED",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTRAPRES,"GTRAPRES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTRAPIND,"GTRAPIND",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTRAPTEL,"GTRAPTEL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTRAPDCA,"GTRAPDCA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTRAPFAL,"GTRAPFAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTNOTRAS,"GTNOTRAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTSEZ1ST,"GTSEZ1ST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTSEZ1SV,"GTSEZ1SV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTSEZ1SX,"GTSEZ1SX",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTSEZ1SY,"GTSEZ1SY",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTSEZ1DI,"GTSEZ1DI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTLAVDIP,"GTLAVDIP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTLAVAUT,"GTLAVAUT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTFISAGG,"GTFISAGG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTTIPINV,"GTTIPINV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTRIOPDP,"GTRIOPDP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTRIOPAU,"GTRIOPAU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTTRASNO,"GTTRASNO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTRIOPSS,"GTRIOPSS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTRIOPDI,"GTRIOPDI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTRIOPST,"GTRIOPST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTRIOPSV,"GTRIOPSV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTRIOPSX,"GTRIOPSX",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTRIOPSY,"GTRIOPSY",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTCFISAI,"GTCFISAI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTRISEDI,"GTRISEDI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTRISEAU,"GTRISEAU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTRISECA,"GTRISECA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTRISELB,"GTRISELB",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTRISEAR,"GTRISEAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTAICFIS,"GTAICFIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTDIRISE,"GTDIRISE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTAURISE,"GTAURISE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTCARISE,"GTCARISE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTLBRISE,"GTLBRISE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTARRISE,"GTARRISE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTFIRDIC,"GTFIRDIC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTSITPAR,"GTSITPAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTATTEST,"GTATTEST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTSOGGE1,"GTSOGGE1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTFISSG1,"GTFISSG1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTFIRSG1,"GTFIRSG1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTSOGGE2,"GTSOGGE2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTFISSG2,"GTFISSG2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTFIRSG2,"GTFIRSG2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTSOGGE3,"GTSOGGE3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTFISSG3,"GTFISSG3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTFIRSG3,"GTFIRSG3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTSOGGE4,"GTSOGGE4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTFISSG4,"GTFISSG4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTFIRSG4,"GTFIRSG4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTSOGGE5,"GTSOGGE5",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTFISSG5,"GTFISSG5",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTFIRSG5,"GTFIRSG5",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTIMPTRA,"GTIMPTRA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTINVTEL,"GTINVTEL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTRICAVV,"GTRICAVV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTCODINT,"GTCODINT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTNUMCAF,"GTNUMCAF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTDATIMP,"GTDATIMP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTFIRINT,"GTFIRINT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTTIPFOR,"GTTIPFOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTFLGCON,"GTFLGCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTFLGCOR,"GTFLGCOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTFLGINT,"GTFLGINT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTEVEECZ,"GTEVEECZ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTTIPSOS,"GTTIPSOS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTPROTEC,"GTPROTEC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTPROSEZ,"GTPROSEZ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTSELCAF,"GTSELCAF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTRESCAF,"GTRESCAF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTDELCAF,"GTDELCAF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTFLGFIR,"GTFLGFIR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GT__ANNO,"GT__ANNO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GTNOMFIL,"GTNOMFIL",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.GEFILTEL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.GEFILTEL_IDX,2])
    i_lTable = "GEFILTEL"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.GEFILTEL_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.GEFILTEL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.GEFILTEL_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.GEFILTEL_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"GT770","i_codazi,w_Gtserial")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into GEFILTEL
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'GEFILTEL')
        i_extval=cp_InsertValODBCExtFlds(this,'GEFILTEL')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(GTSERIAL,GTDATGEN,GTDESAGG,GTCODFIS,GTCODSOS"+;
                  ",GTDENOMI,GTCOGNOM,GT__NOME,GT_SESSO,GTCOMNAS"+;
                  ",GTPRONAS,GTDATNAS,GTCODATT,GTTELEFO,GT__MAIL"+;
                  ",GT_STATO,GTSITUAZ,GTNATGIU,GTCODDIC,GTTIP770"+;
                  ",GTPAEEST,GTIDFIES,GTRAPFIR,GTRAPFIS,GTRAPCAR"+;
                  ",GTCODFED,GTRAPCOG,GTRAPNOM,GTRAPSES,GTRAPDNS"+;
                  ",GTRAPNAS,GTRAPPRO,GTRAPEST,GTRAPFED,GTRAPRES"+;
                  ",GTRAPIND,GTRAPTEL,GTRAPDCA,GTRAPFAL,GTNOTRAS"+;
                  ",GTSEZ1ST,GTSEZ1SV,GTSEZ1SX,GTSEZ1SY,GTSEZ1DI"+;
                  ",GTLAVDIP,GTLAVAUT,GTFISAGG,GTTIPINV,GTRIOPDP"+;
                  ",GTRIOPAU,GTTRASNO,GTRIOPSS,GTRIOPDI,GTRIOPST"+;
                  ",GTRIOPSV,GTRIOPSX,GTRIOPSY,GTCFISAI,GTRISEDI"+;
                  ",GTRISEAU,GTRISECA,GTRISELB,GTRISEAR,GTAICFIS"+;
                  ",GTDIRISE,GTAURISE,GTCARISE,GTLBRISE,GTARRISE"+;
                  ",GTFIRDIC,GTSITPAR,GTATTEST,GTSOGGE1,GTFISSG1"+;
                  ",GTFIRSG1,GTSOGGE2,GTFISSG2,GTFIRSG2,GTSOGGE3"+;
                  ",GTFISSG3,GTFIRSG3,GTSOGGE4,GTFISSG4,GTFIRSG4"+;
                  ",GTSOGGE5,GTFISSG5,GTFIRSG5,GTIMPTRA,GTINVTEL"+;
                  ",GTRICAVV,GTCODINT,GTNUMCAF,GTDATIMP,GTFIRINT"+;
                  ",GTTIPFOR,GTFLGCON,GTFLGCOR,GTFLGINT,GTEVEECZ"+;
                  ",GTTIPSOS,GTPROTEC,GTPROSEZ,GTSELCAF,GTRESCAF"+;
                  ",GTDELCAF,GTFLGFIR,GT__ANNO,GTNOMFIL "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_GTSERIAL)+;
                  ","+cp_ToStrODBC(this.w_GTDATGEN)+;
                  ","+cp_ToStrODBC(this.w_GTDESAGG)+;
                  ","+cp_ToStrODBC(this.w_GTCODFIS)+;
                  ","+cp_ToStrODBC(this.w_GTCODSOS)+;
                  ","+cp_ToStrODBC(this.w_GTDENOMI)+;
                  ","+cp_ToStrODBC(this.w_GTCOGNOM)+;
                  ","+cp_ToStrODBC(this.w_GT__NOME)+;
                  ","+cp_ToStrODBC(this.w_GT_SESSO)+;
                  ","+cp_ToStrODBC(this.w_GTCOMNAS)+;
                  ","+cp_ToStrODBC(this.w_GTPRONAS)+;
                  ","+cp_ToStrODBC(this.w_GTDATNAS)+;
                  ","+cp_ToStrODBC(this.w_GTCODATT)+;
                  ","+cp_ToStrODBC(this.w_GTTELEFO)+;
                  ","+cp_ToStrODBC(this.w_GT__MAIL)+;
                  ","+cp_ToStrODBC(this.w_GT_STATO)+;
                  ","+cp_ToStrODBC(this.w_GTSITUAZ)+;
                  ","+cp_ToStrODBC(this.w_GTNATGIU)+;
                  ","+cp_ToStrODBC(this.w_GTCODDIC)+;
                  ","+cp_ToStrODBC(this.w_GTTIP770)+;
                  ","+cp_ToStrODBC(this.w_GTPAEEST)+;
                  ","+cp_ToStrODBC(this.w_GTIDFIES)+;
                  ","+cp_ToStrODBC(this.w_GTRAPFIR)+;
                  ","+cp_ToStrODBC(this.w_GTRAPFIS)+;
                  ","+cp_ToStrODBC(this.w_GTRAPCAR)+;
                  ","+cp_ToStrODBC(this.w_GTCODFED)+;
                  ","+cp_ToStrODBC(this.w_GTRAPCOG)+;
                  ","+cp_ToStrODBC(this.w_GTRAPNOM)+;
                  ","+cp_ToStrODBC(this.w_GTRAPSES)+;
                  ","+cp_ToStrODBC(this.w_GTRAPDNS)+;
                  ","+cp_ToStrODBC(this.w_GTRAPNAS)+;
                  ","+cp_ToStrODBC(this.w_GTRAPPRO)+;
                  ","+cp_ToStrODBC(this.w_GTRAPEST)+;
                  ","+cp_ToStrODBC(this.w_GTRAPFED)+;
                  ","+cp_ToStrODBC(this.w_GTRAPRES)+;
                  ","+cp_ToStrODBC(this.w_GTRAPIND)+;
                  ","+cp_ToStrODBC(this.w_GTRAPTEL)+;
                  ","+cp_ToStrODBC(this.w_GTRAPDCA)+;
                  ","+cp_ToStrODBC(this.w_GTRAPFAL)+;
                  ","+cp_ToStrODBC(this.w_GTNOTRAS)+;
                  ","+cp_ToStrODBC(this.w_GTSEZ1ST)+;
                  ","+cp_ToStrODBC(this.w_GTSEZ1SV)+;
                  ","+cp_ToStrODBC(this.w_GTSEZ1SX)+;
                  ","+cp_ToStrODBC(this.w_GTSEZ1SY)+;
                  ","+cp_ToStrODBC(this.w_GTSEZ1DI)+;
                  ","+cp_ToStrODBC(this.w_GTLAVDIP)+;
                  ","+cp_ToStrODBC(this.w_GTLAVAUT)+;
                  ","+cp_ToStrODBC(this.w_GTFISAGG)+;
                  ","+cp_ToStrODBC(this.w_GTTIPINV)+;
                  ","+cp_ToStrODBC(this.w_GTRIOPDP)+;
                  ","+cp_ToStrODBC(this.w_GTRIOPAU)+;
                  ","+cp_ToStrODBC(this.w_GTTRASNO)+;
                  ","+cp_ToStrODBC(this.w_GTRIOPSS)+;
                  ","+cp_ToStrODBC(this.w_GTRIOPDI)+;
                  ","+cp_ToStrODBC(this.w_GTRIOPST)+;
                  ","+cp_ToStrODBC(this.w_GTRIOPSV)+;
                  ","+cp_ToStrODBC(this.w_GTRIOPSX)+;
                  ","+cp_ToStrODBC(this.w_GTRIOPSY)+;
                  ","+cp_ToStrODBC(this.w_GTCFISAI)+;
                  ","+cp_ToStrODBC(this.w_GTRISEDI)+;
                  ","+cp_ToStrODBC(this.w_GTRISEAU)+;
                  ","+cp_ToStrODBC(this.w_GTRISECA)+;
                  ","+cp_ToStrODBC(this.w_GTRISELB)+;
                  ","+cp_ToStrODBC(this.w_GTRISEAR)+;
                  ","+cp_ToStrODBC(this.w_GTAICFIS)+;
                  ","+cp_ToStrODBC(this.w_GTDIRISE)+;
                  ","+cp_ToStrODBC(this.w_GTAURISE)+;
                  ","+cp_ToStrODBC(this.w_GTCARISE)+;
                  ","+cp_ToStrODBC(this.w_GTLBRISE)+;
                  ","+cp_ToStrODBC(this.w_GTARRISE)+;
                  ","+cp_ToStrODBC(this.w_GTFIRDIC)+;
                  ","+cp_ToStrODBC(this.w_GTSITPAR)+;
                  ","+cp_ToStrODBC(this.w_GTATTEST)+;
                  ","+cp_ToStrODBC(this.w_GTSOGGE1)+;
                  ","+cp_ToStrODBC(this.w_GTFISSG1)+;
                  ","+cp_ToStrODBC(this.w_GTFIRSG1)+;
                  ","+cp_ToStrODBC(this.w_GTSOGGE2)+;
                  ","+cp_ToStrODBC(this.w_GTFISSG2)+;
                  ","+cp_ToStrODBC(this.w_GTFIRSG2)+;
                  ","+cp_ToStrODBC(this.w_GTSOGGE3)+;
                  ","+cp_ToStrODBC(this.w_GTFISSG3)+;
                  ","+cp_ToStrODBC(this.w_GTFIRSG3)+;
                  ","+cp_ToStrODBC(this.w_GTSOGGE4)+;
                  ","+cp_ToStrODBC(this.w_GTFISSG4)+;
                  ","+cp_ToStrODBC(this.w_GTFIRSG4)+;
                  ","+cp_ToStrODBC(this.w_GTSOGGE5)+;
                  ","+cp_ToStrODBC(this.w_GTFISSG5)+;
                  ","+cp_ToStrODBC(this.w_GTFIRSG5)+;
                  ","+cp_ToStrODBC(this.w_GTIMPTRA)+;
                  ","+cp_ToStrODBC(this.w_GTINVTEL)+;
                  ","+cp_ToStrODBC(this.w_GTRICAVV)+;
                  ","+cp_ToStrODBC(this.w_GTCODINT)+;
                  ","+cp_ToStrODBC(this.w_GTNUMCAF)+;
                  ","+cp_ToStrODBC(this.w_GTDATIMP)+;
                  ","+cp_ToStrODBC(this.w_GTFIRINT)+;
                  ","+cp_ToStrODBC(this.w_GTTIPFOR)+;
                  ","+cp_ToStrODBC(this.w_GTFLGCON)+;
                  ","+cp_ToStrODBC(this.w_GTFLGCOR)+;
                  ","+cp_ToStrODBC(this.w_GTFLGINT)+;
                  ","+cp_ToStrODBC(this.w_GTEVEECZ)+;
             ""
             i_nnn=i_nnn+;
                  ","+cp_ToStrODBC(this.w_GTTIPSOS)+;
                  ","+cp_ToStrODBC(this.w_GTPROTEC)+;
                  ","+cp_ToStrODBC(this.w_GTPROSEZ)+;
                  ","+cp_ToStrODBC(this.w_GTSELCAF)+;
                  ","+cp_ToStrODBC(this.w_GTRESCAF)+;
                  ","+cp_ToStrODBC(this.w_GTDELCAF)+;
                  ","+cp_ToStrODBC(this.w_GTFLGFIR)+;
                  ","+cp_ToStrODBC(this.w_GT__ANNO)+;
                  ","+cp_ToStrODBC(this.w_GTNOMFIL)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'GEFILTEL')
        i_extval=cp_InsertValVFPExtFlds(this,'GEFILTEL')
        cp_CheckDeletedKey(i_cTable,0,'GTSERIAL',this.w_GTSERIAL)
        INSERT INTO (i_cTable);
              (GTSERIAL,GTDATGEN,GTDESAGG,GTCODFIS,GTCODSOS,GTDENOMI,GTCOGNOM,GT__NOME,GT_SESSO,GTCOMNAS,GTPRONAS,GTDATNAS,GTCODATT,GTTELEFO,GT__MAIL,GT_STATO,GTSITUAZ,GTNATGIU,GTCODDIC,GTTIP770,GTPAEEST,GTIDFIES,GTRAPFIR,GTRAPFIS,GTRAPCAR,GTCODFED,GTRAPCOG,GTRAPNOM,GTRAPSES,GTRAPDNS,GTRAPNAS,GTRAPPRO,GTRAPEST,GTRAPFED,GTRAPRES,GTRAPIND,GTRAPTEL,GTRAPDCA,GTRAPFAL,GTNOTRAS,GTSEZ1ST,GTSEZ1SV,GTSEZ1SX,GTSEZ1SY,GTSEZ1DI,GTLAVDIP,GTLAVAUT,GTFISAGG,GTTIPINV,GTRIOPDP,GTRIOPAU,GTTRASNO,GTRIOPSS,GTRIOPDI,GTRIOPST,GTRIOPSV,GTRIOPSX,GTRIOPSY,GTCFISAI,GTRISEDI,GTRISEAU,GTRISECA,GTRISELB,GTRISEAR,GTAICFIS,GTDIRISE,GTAURISE,GTCARISE,GTLBRISE,GTARRISE,GTFIRDIC,GTSITPAR,GTATTEST,GTSOGGE1,GTFISSG1,GTFIRSG1,GTSOGGE2,GTFISSG2,GTFIRSG2,GTSOGGE3,GTFISSG3,GTFIRSG3,GTSOGGE4,GTFISSG4,GTFIRSG4,GTSOGGE5,GTFISSG5,GTFIRSG5,GTIMPTRA,GTINVTEL,GTRICAVV,GTCODINT,GTNUMCAF,GTDATIMP,GTFIRINT,GTTIPFOR,GTFLGCON,GTFLGCOR,GTFLGINT,GTEVEECZ,GTTIPSOS,GTPROTEC,GTPROSEZ,GTSELCAF,GTRESCAF,GTDELCAF,GTFLGFIR,GT__ANNO,GTNOMFIL  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_GTSERIAL;
                  ,this.w_GTDATGEN;
                  ,this.w_GTDESAGG;
                  ,this.w_GTCODFIS;
                  ,this.w_GTCODSOS;
                  ,this.w_GTDENOMI;
                  ,this.w_GTCOGNOM;
                  ,this.w_GT__NOME;
                  ,this.w_GT_SESSO;
                  ,this.w_GTCOMNAS;
                  ,this.w_GTPRONAS;
                  ,this.w_GTDATNAS;
                  ,this.w_GTCODATT;
                  ,this.w_GTTELEFO;
                  ,this.w_GT__MAIL;
                  ,this.w_GT_STATO;
                  ,this.w_GTSITUAZ;
                  ,this.w_GTNATGIU;
                  ,this.w_GTCODDIC;
                  ,this.w_GTTIP770;
                  ,this.w_GTPAEEST;
                  ,this.w_GTIDFIES;
                  ,this.w_GTRAPFIR;
                  ,this.w_GTRAPFIS;
                  ,this.w_GTRAPCAR;
                  ,this.w_GTCODFED;
                  ,this.w_GTRAPCOG;
                  ,this.w_GTRAPNOM;
                  ,this.w_GTRAPSES;
                  ,this.w_GTRAPDNS;
                  ,this.w_GTRAPNAS;
                  ,this.w_GTRAPPRO;
                  ,this.w_GTRAPEST;
                  ,this.w_GTRAPFED;
                  ,this.w_GTRAPRES;
                  ,this.w_GTRAPIND;
                  ,this.w_GTRAPTEL;
                  ,this.w_GTRAPDCA;
                  ,this.w_GTRAPFAL;
                  ,this.w_GTNOTRAS;
                  ,this.w_GTSEZ1ST;
                  ,this.w_GTSEZ1SV;
                  ,this.w_GTSEZ1SX;
                  ,this.w_GTSEZ1SY;
                  ,this.w_GTSEZ1DI;
                  ,this.w_GTLAVDIP;
                  ,this.w_GTLAVAUT;
                  ,this.w_GTFISAGG;
                  ,this.w_GTTIPINV;
                  ,this.w_GTRIOPDP;
                  ,this.w_GTRIOPAU;
                  ,this.w_GTTRASNO;
                  ,this.w_GTRIOPSS;
                  ,this.w_GTRIOPDI;
                  ,this.w_GTRIOPST;
                  ,this.w_GTRIOPSV;
                  ,this.w_GTRIOPSX;
                  ,this.w_GTRIOPSY;
                  ,this.w_GTCFISAI;
                  ,this.w_GTRISEDI;
                  ,this.w_GTRISEAU;
                  ,this.w_GTRISECA;
                  ,this.w_GTRISELB;
                  ,this.w_GTRISEAR;
                  ,this.w_GTAICFIS;
                  ,this.w_GTDIRISE;
                  ,this.w_GTAURISE;
                  ,this.w_GTCARISE;
                  ,this.w_GTLBRISE;
                  ,this.w_GTARRISE;
                  ,this.w_GTFIRDIC;
                  ,this.w_GTSITPAR;
                  ,this.w_GTATTEST;
                  ,this.w_GTSOGGE1;
                  ,this.w_GTFISSG1;
                  ,this.w_GTFIRSG1;
                  ,this.w_GTSOGGE2;
                  ,this.w_GTFISSG2;
                  ,this.w_GTFIRSG2;
                  ,this.w_GTSOGGE3;
                  ,this.w_GTFISSG3;
                  ,this.w_GTFIRSG3;
                  ,this.w_GTSOGGE4;
                  ,this.w_GTFISSG4;
                  ,this.w_GTFIRSG4;
                  ,this.w_GTSOGGE5;
                  ,this.w_GTFISSG5;
                  ,this.w_GTFIRSG5;
                  ,this.w_GTIMPTRA;
                  ,this.w_GTINVTEL;
                  ,this.w_GTRICAVV;
                  ,this.w_GTCODINT;
                  ,this.w_GTNUMCAF;
                  ,this.w_GTDATIMP;
                  ,this.w_GTFIRINT;
                  ,this.w_GTTIPFOR;
                  ,this.w_GTFLGCON;
                  ,this.w_GTFLGCOR;
                  ,this.w_GTFLGINT;
                  ,this.w_GTEVEECZ;
                  ,this.w_GTTIPSOS;
                  ,this.w_GTPROTEC;
                  ,this.w_GTPROSEZ;
                  ,this.w_GTSELCAF;
                  ,this.w_GTRESCAF;
                  ,this.w_GTDELCAF;
                  ,this.w_GTFLGFIR;
                  ,this.w_GT__ANNO;
                  ,this.w_GTNOMFIL;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.GEFILTEL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.GEFILTEL_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.GEFILTEL_IDX,i_nConn)
      *
      * update GEFILTEL
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'GEFILTEL')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " GTDATGEN="+cp_ToStrODBC(this.w_GTDATGEN)+;
             ",GTDESAGG="+cp_ToStrODBC(this.w_GTDESAGG)+;
             ",GTCODFIS="+cp_ToStrODBC(this.w_GTCODFIS)+;
             ",GTCODSOS="+cp_ToStrODBC(this.w_GTCODSOS)+;
             ",GTDENOMI="+cp_ToStrODBC(this.w_GTDENOMI)+;
             ",GTCOGNOM="+cp_ToStrODBC(this.w_GTCOGNOM)+;
             ",GT__NOME="+cp_ToStrODBC(this.w_GT__NOME)+;
             ",GT_SESSO="+cp_ToStrODBC(this.w_GT_SESSO)+;
             ",GTCOMNAS="+cp_ToStrODBC(this.w_GTCOMNAS)+;
             ",GTPRONAS="+cp_ToStrODBC(this.w_GTPRONAS)+;
             ",GTDATNAS="+cp_ToStrODBC(this.w_GTDATNAS)+;
             ",GTCODATT="+cp_ToStrODBC(this.w_GTCODATT)+;
             ",GTTELEFO="+cp_ToStrODBC(this.w_GTTELEFO)+;
             ",GT__MAIL="+cp_ToStrODBC(this.w_GT__MAIL)+;
             ",GT_STATO="+cp_ToStrODBC(this.w_GT_STATO)+;
             ",GTSITUAZ="+cp_ToStrODBC(this.w_GTSITUAZ)+;
             ",GTNATGIU="+cp_ToStrODBC(this.w_GTNATGIU)+;
             ",GTCODDIC="+cp_ToStrODBC(this.w_GTCODDIC)+;
             ",GTTIP770="+cp_ToStrODBC(this.w_GTTIP770)+;
             ",GTPAEEST="+cp_ToStrODBC(this.w_GTPAEEST)+;
             ",GTIDFIES="+cp_ToStrODBC(this.w_GTIDFIES)+;
             ",GTRAPFIR="+cp_ToStrODBC(this.w_GTRAPFIR)+;
             ",GTRAPFIS="+cp_ToStrODBC(this.w_GTRAPFIS)+;
             ",GTRAPCAR="+cp_ToStrODBC(this.w_GTRAPCAR)+;
             ",GTCODFED="+cp_ToStrODBC(this.w_GTCODFED)+;
             ",GTRAPCOG="+cp_ToStrODBC(this.w_GTRAPCOG)+;
             ",GTRAPNOM="+cp_ToStrODBC(this.w_GTRAPNOM)+;
             ",GTRAPSES="+cp_ToStrODBC(this.w_GTRAPSES)+;
             ",GTRAPDNS="+cp_ToStrODBC(this.w_GTRAPDNS)+;
             ",GTRAPNAS="+cp_ToStrODBC(this.w_GTRAPNAS)+;
             ",GTRAPPRO="+cp_ToStrODBC(this.w_GTRAPPRO)+;
             ",GTRAPEST="+cp_ToStrODBC(this.w_GTRAPEST)+;
             ",GTRAPFED="+cp_ToStrODBC(this.w_GTRAPFED)+;
             ",GTRAPRES="+cp_ToStrODBC(this.w_GTRAPRES)+;
             ",GTRAPIND="+cp_ToStrODBC(this.w_GTRAPIND)+;
             ",GTRAPTEL="+cp_ToStrODBC(this.w_GTRAPTEL)+;
             ",GTRAPDCA="+cp_ToStrODBC(this.w_GTRAPDCA)+;
             ",GTRAPFAL="+cp_ToStrODBC(this.w_GTRAPFAL)+;
             ",GTNOTRAS="+cp_ToStrODBC(this.w_GTNOTRAS)+;
             ",GTSEZ1ST="+cp_ToStrODBC(this.w_GTSEZ1ST)+;
             ",GTSEZ1SV="+cp_ToStrODBC(this.w_GTSEZ1SV)+;
             ",GTSEZ1SX="+cp_ToStrODBC(this.w_GTSEZ1SX)+;
             ",GTSEZ1SY="+cp_ToStrODBC(this.w_GTSEZ1SY)+;
             ",GTSEZ1DI="+cp_ToStrODBC(this.w_GTSEZ1DI)+;
             ",GTLAVDIP="+cp_ToStrODBC(this.w_GTLAVDIP)+;
             ",GTLAVAUT="+cp_ToStrODBC(this.w_GTLAVAUT)+;
             ",GTFISAGG="+cp_ToStrODBC(this.w_GTFISAGG)+;
             ",GTTIPINV="+cp_ToStrODBC(this.w_GTTIPINV)+;
             ",GTRIOPDP="+cp_ToStrODBC(this.w_GTRIOPDP)+;
             ",GTRIOPAU="+cp_ToStrODBC(this.w_GTRIOPAU)+;
             ",GTTRASNO="+cp_ToStrODBC(this.w_GTTRASNO)+;
             ",GTRIOPSS="+cp_ToStrODBC(this.w_GTRIOPSS)+;
             ",GTRIOPDI="+cp_ToStrODBC(this.w_GTRIOPDI)+;
             ",GTRIOPST="+cp_ToStrODBC(this.w_GTRIOPST)+;
             ",GTRIOPSV="+cp_ToStrODBC(this.w_GTRIOPSV)+;
             ",GTRIOPSX="+cp_ToStrODBC(this.w_GTRIOPSX)+;
             ",GTRIOPSY="+cp_ToStrODBC(this.w_GTRIOPSY)+;
             ",GTCFISAI="+cp_ToStrODBC(this.w_GTCFISAI)+;
             ",GTRISEDI="+cp_ToStrODBC(this.w_GTRISEDI)+;
             ",GTRISEAU="+cp_ToStrODBC(this.w_GTRISEAU)+;
             ",GTRISECA="+cp_ToStrODBC(this.w_GTRISECA)+;
             ",GTRISELB="+cp_ToStrODBC(this.w_GTRISELB)+;
             ",GTRISEAR="+cp_ToStrODBC(this.w_GTRISEAR)+;
             ",GTAICFIS="+cp_ToStrODBC(this.w_GTAICFIS)+;
             ",GTDIRISE="+cp_ToStrODBC(this.w_GTDIRISE)+;
             ",GTAURISE="+cp_ToStrODBC(this.w_GTAURISE)+;
             ",GTCARISE="+cp_ToStrODBC(this.w_GTCARISE)+;
             ",GTLBRISE="+cp_ToStrODBC(this.w_GTLBRISE)+;
             ",GTARRISE="+cp_ToStrODBC(this.w_GTARRISE)+;
             ",GTFIRDIC="+cp_ToStrODBC(this.w_GTFIRDIC)+;
             ",GTSITPAR="+cp_ToStrODBC(this.w_GTSITPAR)+;
             ",GTATTEST="+cp_ToStrODBC(this.w_GTATTEST)+;
             ",GTSOGGE1="+cp_ToStrODBC(this.w_GTSOGGE1)+;
             ",GTFISSG1="+cp_ToStrODBC(this.w_GTFISSG1)+;
             ",GTFIRSG1="+cp_ToStrODBC(this.w_GTFIRSG1)+;
             ",GTSOGGE2="+cp_ToStrODBC(this.w_GTSOGGE2)+;
             ",GTFISSG2="+cp_ToStrODBC(this.w_GTFISSG2)+;
             ",GTFIRSG2="+cp_ToStrODBC(this.w_GTFIRSG2)+;
             ",GTSOGGE3="+cp_ToStrODBC(this.w_GTSOGGE3)+;
             ",GTFISSG3="+cp_ToStrODBC(this.w_GTFISSG3)+;
             ",GTFIRSG3="+cp_ToStrODBC(this.w_GTFIRSG3)+;
             ",GTSOGGE4="+cp_ToStrODBC(this.w_GTSOGGE4)+;
             ",GTFISSG4="+cp_ToStrODBC(this.w_GTFISSG4)+;
             ",GTFIRSG4="+cp_ToStrODBC(this.w_GTFIRSG4)+;
             ",GTSOGGE5="+cp_ToStrODBC(this.w_GTSOGGE5)+;
             ",GTFISSG5="+cp_ToStrODBC(this.w_GTFISSG5)+;
             ",GTFIRSG5="+cp_ToStrODBC(this.w_GTFIRSG5)+;
             ",GTIMPTRA="+cp_ToStrODBC(this.w_GTIMPTRA)+;
             ",GTINVTEL="+cp_ToStrODBC(this.w_GTINVTEL)+;
             ",GTRICAVV="+cp_ToStrODBC(this.w_GTRICAVV)+;
             ",GTCODINT="+cp_ToStrODBC(this.w_GTCODINT)+;
             ",GTNUMCAF="+cp_ToStrODBC(this.w_GTNUMCAF)+;
             ",GTDATIMP="+cp_ToStrODBC(this.w_GTDATIMP)+;
             ",GTFIRINT="+cp_ToStrODBC(this.w_GTFIRINT)+;
             ",GTTIPFOR="+cp_ToStrODBC(this.w_GTTIPFOR)+;
             ",GTFLGCON="+cp_ToStrODBC(this.w_GTFLGCON)+;
             ",GTFLGCOR="+cp_ToStrODBC(this.w_GTFLGCOR)+;
             ",GTFLGINT="+cp_ToStrODBC(this.w_GTFLGINT)+;
             ",GTEVEECZ="+cp_ToStrODBC(this.w_GTEVEECZ)+;
             ",GTTIPSOS="+cp_ToStrODBC(this.w_GTTIPSOS)+;
             ""
             i_nnn=i_nnn+;
             ",GTPROTEC="+cp_ToStrODBC(this.w_GTPROTEC)+;
             ",GTPROSEZ="+cp_ToStrODBC(this.w_GTPROSEZ)+;
             ",GTSELCAF="+cp_ToStrODBC(this.w_GTSELCAF)+;
             ",GTRESCAF="+cp_ToStrODBC(this.w_GTRESCAF)+;
             ",GTDELCAF="+cp_ToStrODBC(this.w_GTDELCAF)+;
             ",GTFLGFIR="+cp_ToStrODBC(this.w_GTFLGFIR)+;
             ",GT__ANNO="+cp_ToStrODBC(this.w_GT__ANNO)+;
             ",GTNOMFIL="+cp_ToStrODBC(this.w_GTNOMFIL)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'GEFILTEL')
        i_cWhere = cp_PKFox(i_cTable  ,'GTSERIAL',this.w_GTSERIAL  )
        UPDATE (i_cTable) SET;
              GTDATGEN=this.w_GTDATGEN;
             ,GTDESAGG=this.w_GTDESAGG;
             ,GTCODFIS=this.w_GTCODFIS;
             ,GTCODSOS=this.w_GTCODSOS;
             ,GTDENOMI=this.w_GTDENOMI;
             ,GTCOGNOM=this.w_GTCOGNOM;
             ,GT__NOME=this.w_GT__NOME;
             ,GT_SESSO=this.w_GT_SESSO;
             ,GTCOMNAS=this.w_GTCOMNAS;
             ,GTPRONAS=this.w_GTPRONAS;
             ,GTDATNAS=this.w_GTDATNAS;
             ,GTCODATT=this.w_GTCODATT;
             ,GTTELEFO=this.w_GTTELEFO;
             ,GT__MAIL=this.w_GT__MAIL;
             ,GT_STATO=this.w_GT_STATO;
             ,GTSITUAZ=this.w_GTSITUAZ;
             ,GTNATGIU=this.w_GTNATGIU;
             ,GTCODDIC=this.w_GTCODDIC;
             ,GTTIP770=this.w_GTTIP770;
             ,GTPAEEST=this.w_GTPAEEST;
             ,GTIDFIES=this.w_GTIDFIES;
             ,GTRAPFIR=this.w_GTRAPFIR;
             ,GTRAPFIS=this.w_GTRAPFIS;
             ,GTRAPCAR=this.w_GTRAPCAR;
             ,GTCODFED=this.w_GTCODFED;
             ,GTRAPCOG=this.w_GTRAPCOG;
             ,GTRAPNOM=this.w_GTRAPNOM;
             ,GTRAPSES=this.w_GTRAPSES;
             ,GTRAPDNS=this.w_GTRAPDNS;
             ,GTRAPNAS=this.w_GTRAPNAS;
             ,GTRAPPRO=this.w_GTRAPPRO;
             ,GTRAPEST=this.w_GTRAPEST;
             ,GTRAPFED=this.w_GTRAPFED;
             ,GTRAPRES=this.w_GTRAPRES;
             ,GTRAPIND=this.w_GTRAPIND;
             ,GTRAPTEL=this.w_GTRAPTEL;
             ,GTRAPDCA=this.w_GTRAPDCA;
             ,GTRAPFAL=this.w_GTRAPFAL;
             ,GTNOTRAS=this.w_GTNOTRAS;
             ,GTSEZ1ST=this.w_GTSEZ1ST;
             ,GTSEZ1SV=this.w_GTSEZ1SV;
             ,GTSEZ1SX=this.w_GTSEZ1SX;
             ,GTSEZ1SY=this.w_GTSEZ1SY;
             ,GTSEZ1DI=this.w_GTSEZ1DI;
             ,GTLAVDIP=this.w_GTLAVDIP;
             ,GTLAVAUT=this.w_GTLAVAUT;
             ,GTFISAGG=this.w_GTFISAGG;
             ,GTTIPINV=this.w_GTTIPINV;
             ,GTRIOPDP=this.w_GTRIOPDP;
             ,GTRIOPAU=this.w_GTRIOPAU;
             ,GTTRASNO=this.w_GTTRASNO;
             ,GTRIOPSS=this.w_GTRIOPSS;
             ,GTRIOPDI=this.w_GTRIOPDI;
             ,GTRIOPST=this.w_GTRIOPST;
             ,GTRIOPSV=this.w_GTRIOPSV;
             ,GTRIOPSX=this.w_GTRIOPSX;
             ,GTRIOPSY=this.w_GTRIOPSY;
             ,GTCFISAI=this.w_GTCFISAI;
             ,GTRISEDI=this.w_GTRISEDI;
             ,GTRISEAU=this.w_GTRISEAU;
             ,GTRISECA=this.w_GTRISECA;
             ,GTRISELB=this.w_GTRISELB;
             ,GTRISEAR=this.w_GTRISEAR;
             ,GTAICFIS=this.w_GTAICFIS;
             ,GTDIRISE=this.w_GTDIRISE;
             ,GTAURISE=this.w_GTAURISE;
             ,GTCARISE=this.w_GTCARISE;
             ,GTLBRISE=this.w_GTLBRISE;
             ,GTARRISE=this.w_GTARRISE;
             ,GTFIRDIC=this.w_GTFIRDIC;
             ,GTSITPAR=this.w_GTSITPAR;
             ,GTATTEST=this.w_GTATTEST;
             ,GTSOGGE1=this.w_GTSOGGE1;
             ,GTFISSG1=this.w_GTFISSG1;
             ,GTFIRSG1=this.w_GTFIRSG1;
             ,GTSOGGE2=this.w_GTSOGGE2;
             ,GTFISSG2=this.w_GTFISSG2;
             ,GTFIRSG2=this.w_GTFIRSG2;
             ,GTSOGGE3=this.w_GTSOGGE3;
             ,GTFISSG3=this.w_GTFISSG3;
             ,GTFIRSG3=this.w_GTFIRSG3;
             ,GTSOGGE4=this.w_GTSOGGE4;
             ,GTFISSG4=this.w_GTFISSG4;
             ,GTFIRSG4=this.w_GTFIRSG4;
             ,GTSOGGE5=this.w_GTSOGGE5;
             ,GTFISSG5=this.w_GTFISSG5;
             ,GTFIRSG5=this.w_GTFIRSG5;
             ,GTIMPTRA=this.w_GTIMPTRA;
             ,GTINVTEL=this.w_GTINVTEL;
             ,GTRICAVV=this.w_GTRICAVV;
             ,GTCODINT=this.w_GTCODINT;
             ,GTNUMCAF=this.w_GTNUMCAF;
             ,GTDATIMP=this.w_GTDATIMP;
             ,GTFIRINT=this.w_GTFIRINT;
             ,GTTIPFOR=this.w_GTTIPFOR;
             ,GTFLGCON=this.w_GTFLGCON;
             ,GTFLGCOR=this.w_GTFLGCOR;
             ,GTFLGINT=this.w_GTFLGINT;
             ,GTEVEECZ=this.w_GTEVEECZ;
             ,GTTIPSOS=this.w_GTTIPSOS;
             ,GTPROTEC=this.w_GTPROTEC;
             ,GTPROSEZ=this.w_GTPROSEZ;
             ,GTSELCAF=this.w_GTSELCAF;
             ,GTRESCAF=this.w_GTRESCAF;
             ,GTDELCAF=this.w_GTDELCAF;
             ,GTFLGFIR=this.w_GTFLGFIR;
             ,GT__ANNO=this.w_GT__ANNO;
             ,GTNOMFIL=this.w_GTNOMFIL;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- Gsri_Mgt : Saving
      this.Gsri_Mgt.ChangeRow(this.cRowID+'      1',0;
             ,this.w_GTSERIAL,"GDSERIAL";
             )
      this.Gsri_Mgt.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- Gsri_Mgt : Deleting
    this.Gsri_Mgt.ChangeRow(this.cRowID+'      1',0;
           ,this.w_GTSERIAL,"GDSERIAL";
           )
    this.Gsri_Mgt.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.GEFILTEL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.GEFILTEL_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.GEFILTEL_IDX,i_nConn)
      *
      * delete GEFILTEL
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'GTSERIAL',this.w_GTSERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.GEFILTEL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.GEFILTEL_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,3,.t.)
          .link_1_4('Full')
        .DoRTCalc(5,8,.t.)
          .link_1_9('Full')
        .DoRTCalc(10,23,.t.)
          .link_1_24('Full')
        .oPgFrm.Page1.oPag.oObj_1_62.Calculate()
        .DoRTCalc(25,46,.t.)
          .link_2_2('Full')
        .DoRTCalc(48,58,.t.)
        if .o_GtRapFir<>.w_GtRapFir
            .w_GTRAPFIS = iif(.w_Gtrapfir='S',.w_Rfcodfis,Space(16))
        endif
        if .o_GtRapFir<>.w_GtRapFir
            .w_GTRAPCAR = iif(.w_Gtrapfir='N','1',.w_Rfcodcar)
        endif
        if .o_GtRapFir<>.w_GtRapFir
            .w_GTCODFED = Space(11)
        endif
        if .o_GtRapFir<>.w_GtRapFir
            .w_GTRAPCOG = iif(.w_Gtrapfir='N',space(20),Left(.w_Rfcognom,24))
        endif
        if .o_GtRapFir<>.w_GtRapFir
            .w_GTRAPNOM = iif(.w_GtRapFir='N',space(20),Left(.w_Rf__Nome,20))
        endif
        if .o_GtRapFir<>.w_GtRapFir
            .w_GTRAPSES = iif(.w_GtRapFir='N','M',.w_Rf_Sesso)
        endif
        if .o_GtRapFir<>.w_GtRapFir
            .w_GTRAPDNS = iif(.w_GtRapFir='N',Ctod('  -  -    '),.w_Rfdatnas)
        endif
        if .o_GtRapFir<>.w_GtRapFir
            .w_GTRAPNAS = iif(.w_GtRapFir='N',space(40),Left(.w_Rfcomnas,40))
        endif
        if .o_GtRapFir<>.w_GtRapFir
            .w_GTRAPPRO = iif(.w_GtRapFir='N',space(2),Left(.w_Rfpronas,2))
        endif
        if .o_GtRapFir<>.w_GtRapFir
            .w_GTRAPEST = Space(3)
        endif
        if .o_GtRapFir<>.w_GtRapFir
            .w_GTRAPFED = Space(24)
        endif
        if .o_GtRapFir<>.w_GtRapFir
            .w_GTRAPRES = Space(24)
        endif
        if .o_GtRapFir<>.w_GtRapFir
            .w_GTRAPIND = Space(35)
        endif
        if .o_GtRapFir<>.w_GtRapFir
            .w_GTRAPTEL = iif(.w_GtRapFir='N',space(12),Left(.w_Rftelefo,12))
        endif
        if .o_GtRapFir<>.w_GtRapFir
            .w_GTRAPDCA = iif(.w_GtRapFir='N',Ctod('  -  -    '),.w_Rfdatcar)
        endif
        if .o_Gtrapcar<>.w_Gtrapcar.or. .o_GtRapFir<>.w_GtRapFir
            .w_GTRAPFAL = iif(inlist(.w_Gtrapcar,'3','4') And .w_GtRapFir='S',.w_Gtrapfal,ctod('  -  -    '))
        endif
        if .o_Gtrapest<>.w_Gtrapest
          .Calculate_SWAKCPQGCK()
        endif
        .DoRTCalc(75,80,.t.)
        if .o_GTLAVAUT<>.w_GTLAVAUT
            .w_GTLAVDIP = '0'
        endif
        if .o_GTNOTRAS<>.w_GTNOTRAS
          .Calculate_YKUNMMOPAN()
        endif
        if .o_GTTRASNO<>.w_GTTRASNO
          .Calculate_BGEYYZDQZC()
        endif
        if .o_GTTIPINV<>.w_GTTIPINV.or. .o_GTRISEDI<>.w_GTRISEDI.or. .o_GTRISEAU<>.w_GTRISEAU.or. .o_GTRISECA<>.w_GTRISECA.or. .o_GTRISELB<>.w_GTRISELB.or. .o_GTRISEAR<>.w_GTRISEAR
          .Calculate_SSMHGJEKOV()
        endif
        if .o_GTTIPINV<>.w_GTTIPINV.or. .o_GTDIRISE<>.w_GTDIRISE.or. .o_GTAURISE<>.w_GTAURISE.or. .o_GTCARISE<>.w_GTCARISE.or. .o_GTLBRISE<>.w_GTLBRISE.or. .o_GTARRISE<>.w_GTARRISE
          .Calculate_FRFBRFYLNR()
        endif
        if .o_GTRIOPSY<>.w_GTRIOPSY
          .Calculate_KNJTUNTVIQ()
        endif
        .DoRTCalc(82,107,.t.)
        if .o_GTSOGGE1<>.w_GTSOGGE1.or. .o_GTSOGGE2<>.w_GTSOGGE2.or. .o_GTSOGGE3<>.w_GTSOGGE3.or. .o_GTSOGGE4<>.w_GTSOGGE4.or. .o_GTSOGGE5<>.w_GTSOGGE5
            .w_GTATTEST = IIF(.w_GTSOGGE1='0' AND .w_GTSOGGE2='0' AND .w_GTSOGGE3='0' AND .w_GTSOGGE4='0' AND .w_GTSOGGE5='0','0',.w_GTATTEST)
        endif
        .DoRTCalc(109,109,.t.)
        if .o_GTSOGGE1<>.w_GTSOGGE1
            .w_GTFISSG1 = IIF(.w_GTSOGGE1='0',SPACE(16),.w_GTFISSG1)
        endif
        if .o_GTSOGGE1<>.w_GTSOGGE1
            .w_GTFIRSG1 = iif(.w_GTSOGGE1$'124','1','0')
        endif
        .DoRTCalc(112,112,.t.)
        if .o_GTSOGGE2<>.w_GTSOGGE2
            .w_GTFISSG2 = IIF(.w_GTSOGGE2='0',SPACE(16),.w_GTFISSG2)
        endif
        if .o_GTSOGGE2<>.w_GTSOGGE2
            .w_GTFIRSG2 = iif(.w_GTSOGGE2$'124','1','0')
        endif
        .DoRTCalc(115,115,.t.)
        if .o_GTSOGGE3<>.w_GTSOGGE3
            .w_GTFISSG3 = IIF(.w_GTSOGGE3='0',SPACE(16),.w_GTFISSG3)
        endif
        if .o_GTSOGGE3<>.w_GTSOGGE3
            .w_GTFIRSG3 = iif(.w_GTSOGGE3$'124','1','0')
        endif
        .DoRTCalc(118,118,.t.)
        if .o_GTSOGGE4<>.w_GTSOGGE4
            .w_GTFISSG4 = IIF(.w_GTSOGGE4='0',SPACE(16),.w_GTFISSG4)
        endif
        if .o_GTSOGGE4<>.w_GTSOGGE4
            .w_GTFIRSG4 = iif(.w_GTSOGGE4$'124','1','0')
        endif
        .DoRTCalc(121,121,.t.)
        if .o_GTSOGGE5<>.w_GTSOGGE5
            .w_GTFISSG5 = IIF(.w_GTSOGGE5='0',SPACE(16),.w_GTFISSG5)
        endif
        if .o_GTSOGGE5<>.w_GTSOGGE5
            .w_GTFIRSG5 = iif(.w_GTSOGGE5$'124','1','0')
        endif
        .DoRTCalc(124,124,.t.)
        if .o_GTIMPTRA<>.w_GTIMPTRA
            .w_GTINVTEL = iif(.w_GTIMPTRA <> '0',.w_GTINVTEL,'0')
        endif
        if .o_GTINVTEL<>.w_GTINVTEL
            .w_GTRICAVV = iif(.w_GTINVTEL='1',.w_GTRICAVV,'0')
        endif
        if .o_Gtimptra<>.w_Gtimptra
            .w_GTCODINT = Space(16)
        endif
        if .o_Gtimptra<>.w_Gtimptra
            .w_GTNUMCAF = 0
        endif
        if .o_Gtimptra<>.w_Gtimptra
            .w_GTDATIMP = Ctod('  -  -    ')
        endif
        if .o_Gtimptra<>.w_Gtimptra
            .w_GTFIRINT = '0'
        endif
        if .o_GTNOMFIL<>.w_GTNOMFIL.or. .o_GTCODFIS<>.w_GTCODFIS
          .Calculate_ZJFMMVWDUN()
        endif
        .DoRTCalc(131,132,.t.)
        if .o_GTFLGINT<>.w_GTFLGINT
            .w_GTFLGCOR = iif(.w_GTFLGINT='1','0',.w_GTFLGCOR)
        endif
        if .o_GTFLGCOR<>.w_GTFLGCOR
            .w_GTFLGINT = iif(.w_GTFLGCOR='1','0',.w_GTFLGINT)
        endif
        .DoRTCalc(135,139,.t.)
        if .o_GTFLGCOR<>.w_GTFLGCOR.or. .o_GTFLGINT<>.w_GTFLGINT.or. .o_GTTIPINV<>.w_GTTIPINV
            .w_GTPROTEC = Space(17)
        endif
        if .o_GTPROTEC<>.w_GTPROTEC
            .w_GTPROSEZ = 0
        endif
        .DoRTCalc(142,143,.t.)
        if .o_GTSELCAF<>.w_GTSELCAF
            .w_GTDELCAF = iif(.w_Gtselcaf <> '1',SPACE(11),.w_Gtdelcaf)
        endif
        if .o_EVEECC<>.w_EVEECC.or. .o_EVEECC_2016<>.w_EVEECC_2016.or. .o_EVEECC_2017<>.w_EVEECC_2017
          .Calculate_GEKLRYYVGZ()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_codazi<>.w_codazi
           cp_AskTableProg(this,i_nConn,"GT770","i_codazi,w_Gtserial")
          .op_Gtserial = .w_Gtserial
        endif
        .op_codazi = .w_codazi
      endwith
      this.DoRTCalc(145,148,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_62.Calculate()
    endwith
  return

  proc Calculate_SWAKCPQGCK()
    with this
          * --- Sbianca campi relativi allo stato
          .w_Gtrapfed = Space(24)
          .w_Gtrapres = Space(24)
          .w_Gtrapind = Space(35)
    endwith
  endproc
  proc Calculate_YKUNMMOPAN()
    with this
          * --- Valorizzazione prospetti compilati
          .w_GTSEZ1ST = iif(.w_GTNOTRAS='0','1','0')
          .w_GTSEZ1SV = iif(.w_GTNOTRAS='0',.w_GTSEZ1SV,'0')
          .w_GTSEZ1SX = iif(.w_GTNOTRAS='0' Or .w_GTNOTRAS='2',.w_GTSEZ1SX,'0')
    endwith
  endproc
  proc Calculate_BGEYYZDQZC()
    with this
          * --- Valorizzazione prospetti compilati
          .w_GTRIOPST = iif(.w_GTTRASNO='0','1','0')
          .w_GTRIOPSV = iif(.w_GTTRASNO='0',.w_GTRIOPSV,'0')
          .w_GTRIOPSX = iif(.w_GTTRASNO='0' Or .w_GTTRASNO='2',.w_GTRIOPSX,'0')
    endwith
  endproc
  proc Calculate_SSMHGJEKOV()
    with this
          * --- Valorizzazione gestione separata
          .w_GTCFISAI = iif(.w_GTTIPINV="1" Or (.w_GTRISEDI="0" And .w_GTRISEAU="0" And .w_GTRISECA="0" And .w_GTRISELB="0" And .w_GTRISEAR="0"),Space(16),.w_GTCFISAI)
          .w_GTRISEDI = iif(.w_GTTIPINV="1" ,"0",.w_GTRISEDI)
          .w_GTRISEAU = iif(.w_GTTIPINV="1" ,"0",.w_GTRISEAU)
          .w_GTRISECA = iif(.w_GTTIPINV="1" ,"0",.w_GTRISECA)
          .w_GTRISELB = iif(.w_GTTIPINV="1" ,"0",.w_GTRISELB)
          .w_GTRISEAR = iif(.w_GTTIPINV="1" ,"0",.w_GTRISEAR)
    endwith
  endproc
  proc Calculate_FRFBRFYLNR()
    with this
          * --- Valorizzazione gestione separata
          .w_GTAICFIS = iif(.w_GTTIPINV="1" Or (.w_GTDIRISE="0" And .w_GTAURISE="0" And .w_GTCARISE="0" And .w_GTLBRISE="0" And .w_GTARRISE="0"),Space(16),.w_GTAICFIS)
          .w_GTDIRISE = iif(.w_GTTIPINV="1" ,"0",.w_GTDIRISE)
          .w_GTAURISE = iif(.w_GTTIPINV="1" ,"0",.w_GTAURISE)
          .w_GTCARISE = iif(.w_GTTIPINV="1" ,"0",.w_GTCARISE)
          .w_GTLBRISE = iif(.w_GTTIPINV="1" ,"0",.w_GTLBRISE)
          .w_GTARRISE = iif(.w_GTTIPINV="1" ,"0",.w_GTARRISE)
    endwith
  endproc
  proc Calculate_KNJTUNTVIQ()
    with this
          * --- Valorizzazione quadro SS
          .w_GTRIOPSS = iif(.w_GTRIOPSY='1','1','0')
    endwith
  endproc
  proc Calculate_ZJFMMVWDUN()
    with this
          * --- Inizializzo nome file
          .w_GTNOMFIL = iif(not empty(.w_GTNOMFIL), .w_GTNOMFIL, left(.w_DirName+alltrim(.w_GTCODFIS)+'_'+Substr(.w_Gtserial,2,9)+'.77S'+space(254),254))
    endwith
  endproc
  proc Calculate_GEKLRYYVGZ()
    with this
          * --- Valorizzo campo GTEVEECZ
          .w_GTEVEECZ = iif(.w_GTTIP770='77S16',.w_EVEECC,iif(.w_GTTIP770='77S17',.w_EVEECC_2016,.w_EVEECC_2017))
    endwith
  endproc
  proc Calculate_LFLBLFKCYE()
    with this
          * --- Valorizzo eventi eccezionali
          .w_EVEECC = .w_GTEVEECZ
          .w_EVEECC_2016 = .w_GTEVEECZ
          .w_EVEECC_2017 = .w_GTEVEECZ
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oGTDENOMI_1_26.enabled = this.oPgFrm.Page1.oPag.oGTDENOMI_1_26.mCond()
    this.oPgFrm.Page1.oPag.oGTCOGNOM_1_27.enabled = this.oPgFrm.Page1.oPag.oGTCOGNOM_1_27.mCond()
    this.oPgFrm.Page1.oPag.oGT__NOME_1_28.enabled = this.oPgFrm.Page1.oPag.oGT__NOME_1_28.mCond()
    this.oPgFrm.Page1.oPag.oGT_SESSO_1_29.enabled = this.oPgFrm.Page1.oPag.oGT_SESSO_1_29.mCond()
    this.oPgFrm.Page1.oPag.oGTCOMNAS_1_30.enabled = this.oPgFrm.Page1.oPag.oGTCOMNAS_1_30.mCond()
    this.oPgFrm.Page1.oPag.oGTPRONAS_1_31.enabled = this.oPgFrm.Page1.oPag.oGTPRONAS_1_31.mCond()
    this.oPgFrm.Page1.oPag.oGTDATNAS_1_32.enabled = this.oPgFrm.Page1.oPag.oGTDATNAS_1_32.mCond()
    this.oPgFrm.Page1.oPag.oGT_STATO_1_37.enabled = this.oPgFrm.Page1.oPag.oGT_STATO_1_37.mCond()
    this.oPgFrm.Page1.oPag.oGTSITUAZ_1_38.enabled = this.oPgFrm.Page1.oPag.oGTSITUAZ_1_38.mCond()
    this.oPgFrm.Page1.oPag.oGTNATGIU_1_39.enabled = this.oPgFrm.Page1.oPag.oGTNATGIU_1_39.mCond()
    this.oPgFrm.Page1.oPag.oGTCODDIC_1_40.enabled = this.oPgFrm.Page1.oPag.oGTCODDIC_1_40.mCond()
    this.oPgFrm.Page2.oPag.oGTRAPFIS_2_14.enabled = this.oPgFrm.Page2.oPag.oGTRAPFIS_2_14.mCond()
    this.oPgFrm.Page2.oPag.oGTRAPCAR_2_16.enabled = this.oPgFrm.Page2.oPag.oGTRAPCAR_2_16.mCond()
    this.oPgFrm.Page2.oPag.oGTCODFED_2_17.enabled = this.oPgFrm.Page2.oPag.oGTCODFED_2_17.mCond()
    this.oPgFrm.Page2.oPag.oGTRAPCOG_2_19.enabled = this.oPgFrm.Page2.oPag.oGTRAPCOG_2_19.mCond()
    this.oPgFrm.Page2.oPag.oGTRAPNOM_2_20.enabled = this.oPgFrm.Page2.oPag.oGTRAPNOM_2_20.mCond()
    this.oPgFrm.Page2.oPag.oGTRAPSES_2_25.enabled = this.oPgFrm.Page2.oPag.oGTRAPSES_2_25.mCond()
    this.oPgFrm.Page2.oPag.oGTRAPDNS_2_27.enabled = this.oPgFrm.Page2.oPag.oGTRAPDNS_2_27.mCond()
    this.oPgFrm.Page2.oPag.oGTRAPNAS_2_30.enabled = this.oPgFrm.Page2.oPag.oGTRAPNAS_2_30.mCond()
    this.oPgFrm.Page2.oPag.oGTRAPPRO_2_31.enabled = this.oPgFrm.Page2.oPag.oGTRAPPRO_2_31.mCond()
    this.oPgFrm.Page2.oPag.oGTRAPEST_2_34.enabled = this.oPgFrm.Page2.oPag.oGTRAPEST_2_34.mCond()
    this.oPgFrm.Page2.oPag.oGTRAPFED_2_36.enabled = this.oPgFrm.Page2.oPag.oGTRAPFED_2_36.mCond()
    this.oPgFrm.Page2.oPag.oGTRAPRES_2_37.enabled = this.oPgFrm.Page2.oPag.oGTRAPRES_2_37.mCond()
    this.oPgFrm.Page2.oPag.oGTRAPIND_2_39.enabled = this.oPgFrm.Page2.oPag.oGTRAPIND_2_39.mCond()
    this.oPgFrm.Page2.oPag.oGTRAPTEL_2_41.enabled = this.oPgFrm.Page2.oPag.oGTRAPTEL_2_41.mCond()
    this.oPgFrm.Page2.oPag.oGTRAPDCA_2_44.enabled = this.oPgFrm.Page2.oPag.oGTRAPDCA_2_44.mCond()
    this.oPgFrm.Page2.oPag.oGTRAPFAL_2_45.enabled = this.oPgFrm.Page2.oPag.oGTRAPFAL_2_45.mCond()
    this.oPgFrm.Page2.oPag.oGTNOTRAS_2_49.enabled = this.oPgFrm.Page2.oPag.oGTNOTRAS_2_49.mCond()
    this.oPgFrm.Page2.oPag.oGTSEZ1ST_2_50.enabled = this.oPgFrm.Page2.oPag.oGTSEZ1ST_2_50.mCond()
    this.oPgFrm.Page2.oPag.oGTSEZ1SV_2_51.enabled = this.oPgFrm.Page2.oPag.oGTSEZ1SV_2_51.mCond()
    this.oPgFrm.Page2.oPag.oGTSEZ1SX_2_52.enabled = this.oPgFrm.Page2.oPag.oGTSEZ1SX_2_52.mCond()
    this.oPgFrm.Page2.oPag.oGTSEZ1SY_2_53.enabled = this.oPgFrm.Page2.oPag.oGTSEZ1SY_2_53.mCond()
    this.oPgFrm.Page2.oPag.oGTLAVDIP_2_55.enabled = this.oPgFrm.Page2.oPag.oGTLAVDIP_2_55.mCond()
    this.oPgFrm.Page3.oPag.oGTTRASNO_3_4.enabled = this.oPgFrm.Page3.oPag.oGTTRASNO_3_4.mCond()
    this.oPgFrm.Page3.oPag.oGTRIOPST_3_7.enabled = this.oPgFrm.Page3.oPag.oGTRIOPST_3_7.mCond()
    this.oPgFrm.Page3.oPag.oGTRIOPSV_3_8.enabled = this.oPgFrm.Page3.oPag.oGTRIOPSV_3_8.mCond()
    this.oPgFrm.Page3.oPag.oGTRIOPSX_3_9.enabled = this.oPgFrm.Page3.oPag.oGTRIOPSX_3_9.mCond()
    this.oPgFrm.Page3.oPag.oGTRIOPSY_3_10.enabled = this.oPgFrm.Page3.oPag.oGTRIOPSY_3_10.mCond()
    this.oPgFrm.Page3.oPag.oGTCFISAI_3_11.enabled = this.oPgFrm.Page3.oPag.oGTCFISAI_3_11.mCond()
    this.oPgFrm.Page3.oPag.oGTRISEDI_3_12.enabled = this.oPgFrm.Page3.oPag.oGTRISEDI_3_12.mCond()
    this.oPgFrm.Page3.oPag.oGTRISEAU_3_13.enabled = this.oPgFrm.Page3.oPag.oGTRISEAU_3_13.mCond()
    this.oPgFrm.Page3.oPag.oGTRISECA_3_14.enabled = this.oPgFrm.Page3.oPag.oGTRISECA_3_14.mCond()
    this.oPgFrm.Page3.oPag.oGTRISELB_3_15.enabled = this.oPgFrm.Page3.oPag.oGTRISELB_3_15.mCond()
    this.oPgFrm.Page3.oPag.oGTRISEAR_3_16.enabled = this.oPgFrm.Page3.oPag.oGTRISEAR_3_16.mCond()
    this.oPgFrm.Page3.oPag.oGTAICFIS_3_17.enabled = this.oPgFrm.Page3.oPag.oGTAICFIS_3_17.mCond()
    this.oPgFrm.Page3.oPag.oGTDIRISE_3_18.enabled = this.oPgFrm.Page3.oPag.oGTDIRISE_3_18.mCond()
    this.oPgFrm.Page3.oPag.oGTAURISE_3_19.enabled = this.oPgFrm.Page3.oPag.oGTAURISE_3_19.mCond()
    this.oPgFrm.Page3.oPag.oGTCARISE_3_20.enabled = this.oPgFrm.Page3.oPag.oGTCARISE_3_20.mCond()
    this.oPgFrm.Page3.oPag.oGTLBRISE_3_21.enabled = this.oPgFrm.Page3.oPag.oGTLBRISE_3_21.mCond()
    this.oPgFrm.Page3.oPag.oGTARRISE_3_22.enabled = this.oPgFrm.Page3.oPag.oGTARRISE_3_22.mCond()
    this.oPgFrm.Page4.oPag.oGTATTEST_4_6.enabled = this.oPgFrm.Page4.oPag.oGTATTEST_4_6.mCond()
    this.oPgFrm.Page4.oPag.oGTFISSG1_4_11.enabled = this.oPgFrm.Page4.oPag.oGTFISSG1_4_11.mCond()
    this.oPgFrm.Page4.oPag.oGTFIRSG1_4_12.enabled = this.oPgFrm.Page4.oPag.oGTFIRSG1_4_12.mCond()
    this.oPgFrm.Page4.oPag.oGTFISSG2_4_14.enabled = this.oPgFrm.Page4.oPag.oGTFISSG2_4_14.mCond()
    this.oPgFrm.Page4.oPag.oGTFIRSG2_4_15.enabled = this.oPgFrm.Page4.oPag.oGTFIRSG2_4_15.mCond()
    this.oPgFrm.Page4.oPag.oGTFISSG3_4_17.enabled = this.oPgFrm.Page4.oPag.oGTFISSG3_4_17.mCond()
    this.oPgFrm.Page4.oPag.oGTFIRSG3_4_18.enabled = this.oPgFrm.Page4.oPag.oGTFIRSG3_4_18.mCond()
    this.oPgFrm.Page4.oPag.oGTFISSG4_4_20.enabled = this.oPgFrm.Page4.oPag.oGTFISSG4_4_20.mCond()
    this.oPgFrm.Page4.oPag.oGTFIRSG4_4_21.enabled = this.oPgFrm.Page4.oPag.oGTFIRSG4_4_21.mCond()
    this.oPgFrm.Page4.oPag.oGTFISSG5_4_23.enabled = this.oPgFrm.Page4.oPag.oGTFISSG5_4_23.mCond()
    this.oPgFrm.Page4.oPag.oGTFIRSG5_4_24.enabled = this.oPgFrm.Page4.oPag.oGTFIRSG5_4_24.mCond()
    this.oPgFrm.Page4.oPag.oGTINVTEL_4_26.enabled = this.oPgFrm.Page4.oPag.oGTINVTEL_4_26.mCond()
    this.oPgFrm.Page4.oPag.oGTRICAVV_4_27.enabled = this.oPgFrm.Page4.oPag.oGTRICAVV_4_27.mCond()
    this.oPgFrm.Page4.oPag.oGTCODINT_4_30.enabled = this.oPgFrm.Page4.oPag.oGTCODINT_4_30.mCond()
    this.oPgFrm.Page4.oPag.oGTNUMCAF_4_34.enabled = this.oPgFrm.Page4.oPag.oGTNUMCAF_4_34.mCond()
    this.oPgFrm.Page4.oPag.oGTDATIMP_4_36.enabled = this.oPgFrm.Page4.oPag.oGTDATIMP_4_36.mCond()
    this.oPgFrm.Page4.oPag.oGTFIRINT_4_37.enabled = this.oPgFrm.Page4.oPag.oGTFIRINT_4_37.mCond()
    this.oPgFrm.Page5.oPag.oGTFLGCOR_5_4.enabled = this.oPgFrm.Page5.oPag.oGTFLGCOR_5_4.mCond()
    this.oPgFrm.Page5.oPag.oGTFLGINT_5_5.enabled = this.oPgFrm.Page5.oPag.oGTFLGINT_5_5.mCond()
    this.oPgFrm.Page5.oPag.oGTPROTEC_5_11.enabled = this.oPgFrm.Page5.oPag.oGTPROTEC_5_11.mCond()
    this.oPgFrm.Page5.oPag.oGTPROSEZ_5_12.enabled = this.oPgFrm.Page5.oPag.oGTPROSEZ_5_12.mCond()
    this.oPgFrm.Page5.oPag.oGT__ANNO_5_24.enabled = this.oPgFrm.Page5.oPag.oGT__ANNO_5_24.mCond()
    this.oPgFrm.Page5.oPag.oBtn_5_33.enabled = this.oPgFrm.Page5.oPag.oBtn_5_33.mCond()
    this.oPgFrm.Page5.oPag.oBtn_5_35.enabled = this.oPgFrm.Page5.oPag.oBtn_5_35.mCond()
    this.oPgFrm.Page5.oPag.oBtn_5_43.enabled = this.oPgFrm.Page5.oPag.oBtn_5_43.mCond()
    this.oPgFrm.Page5.oPag.oBtn_5_48.enabled = this.oPgFrm.Page5.oPag.oBtn_5_48.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    local i_show2
    i_show2=not(this.w_GTTIP770<>'77S18')
    this.oPgFrm.Pages(3).enabled=i_show2
    this.oPgFrm.Pages(3).caption=iif(i_show2,cp_translate("Redazione della dichiarazione"),"")
    this.oPgFrm.Pages(3).oPag.visible=this.oPgFrm.Pages(3).enabled
    this.oPgFrm.Page1.oPag.oStr_1_66.visible=!this.oPgFrm.Page1.oPag.oStr_1_66.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_68.visible=!this.oPgFrm.Page1.oPag.oStr_1_68.mHide()
    this.oPgFrm.Page1.oPag.oGTPAEEST_1_69.visible=!this.oPgFrm.Page1.oPag.oGTPAEEST_1_69.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_70.visible=!this.oPgFrm.Page1.oPag.oStr_1_70.mHide()
    this.oPgFrm.Page1.oPag.oGTIDFIES_1_71.visible=!this.oPgFrm.Page1.oPag.oGTIDFIES_1_71.mHide()
    this.oPgFrm.Page2.oPag.oGTRAPFAL_2_45.visible=!this.oPgFrm.Page2.oPag.oGTRAPFAL_2_45.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_46.visible=!this.oPgFrm.Page2.oPag.oStr_2_46.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_48.visible=!this.oPgFrm.Page2.oPag.oStr_2_48.mHide()
    this.oPgFrm.Page2.oPag.oGTNOTRAS_2_49.visible=!this.oPgFrm.Page2.oPag.oGTNOTRAS_2_49.mHide()
    this.oPgFrm.Page2.oPag.oGTSEZ1ST_2_50.visible=!this.oPgFrm.Page2.oPag.oGTSEZ1ST_2_50.mHide()
    this.oPgFrm.Page2.oPag.oGTSEZ1SV_2_51.visible=!this.oPgFrm.Page2.oPag.oGTSEZ1SV_2_51.mHide()
    this.oPgFrm.Page2.oPag.oGTSEZ1SX_2_52.visible=!this.oPgFrm.Page2.oPag.oGTSEZ1SX_2_52.mHide()
    this.oPgFrm.Page2.oPag.oGTSEZ1SY_2_53.visible=!this.oPgFrm.Page2.oPag.oGTSEZ1SY_2_53.mHide()
    this.oPgFrm.Page2.oPag.oGTSEZ1DI_2_54.visible=!this.oPgFrm.Page2.oPag.oGTSEZ1DI_2_54.mHide()
    this.oPgFrm.Page2.oPag.oGTLAVDIP_2_55.visible=!this.oPgFrm.Page2.oPag.oGTLAVDIP_2_55.mHide()
    this.oPgFrm.Page2.oPag.oGTLAVAUT_2_56.visible=!this.oPgFrm.Page2.oPag.oGTLAVAUT_2_56.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_57.visible=!this.oPgFrm.Page2.oPag.oStr_2_57.mHide()
    this.oPgFrm.Page2.oPag.oGTFISAGG_2_58.visible=!this.oPgFrm.Page2.oPag.oGTFISAGG_2_58.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_59.visible=!this.oPgFrm.Page2.oPag.oStr_2_59.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_61.visible=!this.oPgFrm.Page2.oPag.oStr_2_61.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_62.visible=!this.oPgFrm.Page2.oPag.oStr_2_62.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_63.visible=!this.oPgFrm.Page2.oPag.oStr_2_63.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_65.visible=!this.oPgFrm.Page2.oPag.oStr_2_65.mHide()
    this.oPgFrm.Page5.oPag.oEVEECC_5_6.visible=!this.oPgFrm.Page5.oPag.oEVEECC_5_6.mHide()
    this.oPgFrm.Page5.oPag.oEVEECC_2016_5_7.visible=!this.oPgFrm.Page5.oPag.oEVEECC_2016_5_7.mHide()
    this.oPgFrm.Page5.oPag.oEVEECC_2017_5_8.visible=!this.oPgFrm.Page5.oPag.oEVEECC_2017_5_8.mHide()
    this.oPgFrm.Page5.oPag.oGTTIPSOS_5_10.visible=!this.oPgFrm.Page5.oPag.oGTTIPSOS_5_10.mHide()
    this.oPgFrm.Page5.oPag.oGTPROTEC_5_11.visible=!this.oPgFrm.Page5.oPag.oGTPROTEC_5_11.mHide()
    this.oPgFrm.Page5.oPag.oGTPROSEZ_5_12.visible=!this.oPgFrm.Page5.oPag.oGTPROSEZ_5_12.mHide()
    this.oPgFrm.Page5.oPag.oStr_5_20.visible=!this.oPgFrm.Page5.oPag.oStr_5_20.mHide()
    this.oPgFrm.Page5.oPag.oGTDELCAF_5_21.visible=!this.oPgFrm.Page5.oPag.oGTDELCAF_5_21.mHide()
    this.oPgFrm.Page5.oPag.oBtn_5_33.visible=!this.oPgFrm.Page5.oPag.oBtn_5_33.mHide()
    this.oPgFrm.Page5.oPag.oLinkPC_5_34.visible=!this.oPgFrm.Page5.oPag.oLinkPC_5_34.mHide()
    this.oPgFrm.Page5.oPag.oBtn_5_35.visible=!this.oPgFrm.Page5.oPag.oBtn_5_35.mHide()
    this.oPgFrm.Page5.oPag.oStr_5_40.visible=!this.oPgFrm.Page5.oPag.oStr_5_40.mHide()
    this.oPgFrm.Page5.oPag.oBtn_5_43.visible=!this.oPgFrm.Page5.oPag.oBtn_5_43.mHide()
    this.oPgFrm.Page5.oPag.oStr_5_44.visible=!this.oPgFrm.Page5.oPag.oStr_5_44.mHide()
    this.oPgFrm.Page5.oPag.oStr_5_45.visible=!this.oPgFrm.Page5.oPag.oStr_5_45.mHide()
    this.oPgFrm.Page5.oPag.oStr_5_46.visible=!this.oPgFrm.Page5.oPag.oStr_5_46.mHide()
    this.oPgFrm.Page5.oPag.oStr_5_47.visible=!this.oPgFrm.Page5.oPag.oStr_5_47.mHide()
    this.oPgFrm.Page5.oPag.oBtn_5_48.visible=!this.oPgFrm.Page5.oPag.oBtn_5_48.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsri_agy
    This.oPgFrm.Pages(1).oPag.oBox_1_67.visible=iif(This.pTipcer='77S16',.f.,.t.)
    This.oPgFrm.Pages(2).oPag.oBox_2_60.visible=iif(This.pTipcer='77S18',.f.,.t.)
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_62.Event(cEvent)
        if lower(cEvent)==lower("New record")
          .Calculate_ZJFMMVWDUN()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Blank")
          .Calculate_GEKLRYYVGZ()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Load")
          .Calculate_LFLBLFKCYE()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZCOFAZI,AZ_EMAIL,AZPERAZI,AZTELEFO";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_CODAZI)
            select AZCODAZI,AZCOFAZI,AZ_EMAIL,AZPERAZI,AZTELEFO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.AZCODAZI,space(5))
      this.w_AZCOFAZI = NVL(_Link_.AZCOFAZI,space(16))
      this.w_AZ_EMAIL = NVL(_Link_.AZ_EMAIL,space(50))
      this.w_AZPERAZI = NVL(_Link_.AZPERAZI,space(1))
      this.w_SETELEFONO = NVL(_Link_.AZTELEFO,space(12))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_AZCOFAZI = space(16)
      this.w_AZ_EMAIL = space(50)
      this.w_AZPERAZI = space(1)
      this.w_SETELEFONO = space(12)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODAZI2
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TITOLARI_IDX,3]
    i_lTable = "TITOLARI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TITOLARI_IDX,2], .t., this.TITOLARI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TITOLARI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TTCODAZI,TTCOGTIT,TTNOMTIT,TTTELEFO,TTDATNAS,TTLOCTIT,TTPROTIT,TT_SESSO,TTPRONAS,TTLUONAS,TTCAPTIT,TTINDIRI";
                   +" from "+i_cTable+" "+i_lTable+" where TTCODAZI="+cp_ToStrODBC(this.w_CODAZI2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TTCODAZI',this.w_CODAZI2)
            select TTCODAZI,TTCOGTIT,TTNOMTIT,TTTELEFO,TTDATNAS,TTLOCTIT,TTPROTIT,TT_SESSO,TTPRONAS,TTLUONAS,TTCAPTIT,TTINDIRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI2 = NVL(_Link_.TTCODAZI,space(5))
      this.w_COGTIT1 = NVL(_Link_.TTCOGTIT,space(25))
      this.w_NOMTIT1 = NVL(_Link_.TTNOMTIT,space(25))
      this.w_TTTELEFO = NVL(_Link_.TTTELEFO,space(18))
      this.w_TTDATNAS = NVL(cp_ToDate(_Link_.TTDATNAS),ctod("  /  /  "))
      this.w_TTLOCTIT = NVL(_Link_.TTLOCTIT,space(30))
      this.w_TTPROTIT = NVL(_Link_.TTPROTIT,space(2))
      this.w_TT_SESSO = NVL(_Link_.TT_SESSO,space(1))
      this.w_TTPRONAS = NVL(_Link_.TTPRONAS,space(2))
      this.w_TTLUONAS = NVL(_Link_.TTLUONAS,space(30))
      this.w_TTCAPTIT = NVL(_Link_.TTCAPTIT,space(9))
      this.w_TTINDIRI = NVL(_Link_.TTINDIRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI2 = space(5)
      endif
      this.w_COGTIT1 = space(25)
      this.w_NOMTIT1 = space(25)
      this.w_TTTELEFO = space(18)
      this.w_TTDATNAS = ctod("  /  /  ")
      this.w_TTLOCTIT = space(30)
      this.w_TTPROTIT = space(2)
      this.w_TT_SESSO = space(1)
      this.w_TTPRONAS = space(2)
      this.w_TTLUONAS = space(30)
      this.w_TTCAPTIT = space(9)
      this.w_TTINDIRI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TITOLARI_IDX,2])+'\'+cp_ToStr(_Link_.TTCODAZI,1)
      cp_ShowWarn(i_cKey,this.TITOLARI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODAZI3
  func Link_1_24(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SEDIAZIE_IDX,3]
    i_lTable = "SEDIAZIE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2], .t., this.SEDIAZIE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI3) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI3)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SETIPRIF,SECODAZI,SENATGIU";
                   +" from "+i_cTable+" "+i_lTable+" where SECODAZI="+cp_ToStrODBC(this.w_CODAZI3);
                   +" and SETIPRIF="+cp_ToStrODBC(this.w_TIPFS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SETIPRIF',this.w_TIPFS;
                       ,'SECODAZI',this.w_CODAZI3)
            select SETIPRIF,SECODAZI,SENATGIU;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI3 = NVL(_Link_.SECODAZI,space(5))
      this.w_SENATGIU = NVL(_Link_.SENATGIU,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI3 = space(5)
      endif
      this.w_SENATGIU = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2])+'\'+cp_ToStr(_Link_.SETIPRIF,1)+'\'+cp_ToStr(_Link_.SECODAZI,1)
      cp_ShowWarn(i_cKey,this.SEDIAZIE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI3 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RAPFIRM
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DAT_RAPP_IDX,3]
    i_lTable = "DAT_RAPP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DAT_RAPP_IDX,2], .t., this.DAT_RAPP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DAT_RAPP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RAPFIRM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RAPFIRM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RFCODAZI,RFCODFIS,RFCOGNOM,RF__NOME,RFCODCAR,RF_SESSO,RFDATNAS,RFCOMNAS,RFPRONAS,RFTELEFO,RFDATCAR";
                   +" from "+i_cTable+" "+i_lTable+" where RFCODAZI="+cp_ToStrODBC(this.w_RAPFIRM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RFCODAZI',this.w_RAPFIRM)
            select RFCODAZI,RFCODFIS,RFCOGNOM,RF__NOME,RFCODCAR,RF_SESSO,RFDATNAS,RFCOMNAS,RFPRONAS,RFTELEFO,RFDATCAR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RAPFIRM = NVL(_Link_.RFCODAZI,space(10))
      this.w_RFCODFIS = NVL(_Link_.RFCODFIS,space(16))
      this.w_RFCOGNOM = NVL(_Link_.RFCOGNOM,space(40))
      this.w_RF__NOME = NVL(_Link_.RF__NOME,space(40))
      this.w_RFCODCAR = NVL(_Link_.RFCODCAR,space(2))
      this.w_RF_SESSO = NVL(_Link_.RF_SESSO,space(1))
      this.w_RFDATNAS = NVL(cp_ToDate(_Link_.RFDATNAS),ctod("  /  /  "))
      this.w_RFCOMNAS = NVL(_Link_.RFCOMNAS,space(40))
      this.w_RFPRONAS = NVL(_Link_.RFPRONAS,space(2))
      this.w_RFTELEFO = NVL(_Link_.RFTELEFO,space(18))
      this.w_RFDATCAR = NVL(cp_ToDate(_Link_.RFDATCAR),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_RAPFIRM = space(10)
      endif
      this.w_RFCODFIS = space(16)
      this.w_RFCOGNOM = space(40)
      this.w_RF__NOME = space(40)
      this.w_RFCODCAR = space(2)
      this.w_RF_SESSO = space(1)
      this.w_RFDATNAS = ctod("  /  /  ")
      this.w_RFCOMNAS = space(40)
      this.w_RFPRONAS = space(2)
      this.w_RFTELEFO = space(18)
      this.w_RFDATCAR = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DAT_RAPP_IDX,2])+'\'+cp_ToStr(_Link_.RFCODAZI,1)
      cp_ShowWarn(i_cKey,this.DAT_RAPP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RAPFIRM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oGTDATGEN_1_2.value==this.w_GTDATGEN)
      this.oPgFrm.Page1.oPag.oGTDATGEN_1_2.value=this.w_GTDATGEN
    endif
    if not(this.oPgFrm.Page1.oPag.oGTDESAGG_1_3.value==this.w_GTDESAGG)
      this.oPgFrm.Page1.oPag.oGTDESAGG_1_3.value=this.w_GTDESAGG
    endif
    if not(this.oPgFrm.Page1.oPag.oGTCODFIS_1_7.value==this.w_GTCODFIS)
      this.oPgFrm.Page1.oPag.oGTCODFIS_1_7.value=this.w_GTCODFIS
    endif
    if not(this.oPgFrm.Page1.oPag.oGTCODSOS_1_8.value==this.w_GTCODSOS)
      this.oPgFrm.Page1.oPag.oGTCODSOS_1_8.value=this.w_GTCODSOS
    endif
    if not(this.oPgFrm.Page1.oPag.oGTDENOMI_1_26.value==this.w_GTDENOMI)
      this.oPgFrm.Page1.oPag.oGTDENOMI_1_26.value=this.w_GTDENOMI
    endif
    if not(this.oPgFrm.Page1.oPag.oGTCOGNOM_1_27.value==this.w_GTCOGNOM)
      this.oPgFrm.Page1.oPag.oGTCOGNOM_1_27.value=this.w_GTCOGNOM
    endif
    if not(this.oPgFrm.Page1.oPag.oGT__NOME_1_28.value==this.w_GT__NOME)
      this.oPgFrm.Page1.oPag.oGT__NOME_1_28.value=this.w_GT__NOME
    endif
    if not(this.oPgFrm.Page1.oPag.oGT_SESSO_1_29.RadioValue()==this.w_GT_SESSO)
      this.oPgFrm.Page1.oPag.oGT_SESSO_1_29.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGTCOMNAS_1_30.value==this.w_GTCOMNAS)
      this.oPgFrm.Page1.oPag.oGTCOMNAS_1_30.value=this.w_GTCOMNAS
    endif
    if not(this.oPgFrm.Page1.oPag.oGTPRONAS_1_31.value==this.w_GTPRONAS)
      this.oPgFrm.Page1.oPag.oGTPRONAS_1_31.value=this.w_GTPRONAS
    endif
    if not(this.oPgFrm.Page1.oPag.oGTDATNAS_1_32.value==this.w_GTDATNAS)
      this.oPgFrm.Page1.oPag.oGTDATNAS_1_32.value=this.w_GTDATNAS
    endif
    if not(this.oPgFrm.Page1.oPag.oGTCODATT_1_34.value==this.w_GTCODATT)
      this.oPgFrm.Page1.oPag.oGTCODATT_1_34.value=this.w_GTCODATT
    endif
    if not(this.oPgFrm.Page1.oPag.oGTTELEFO_1_35.value==this.w_GTTELEFO)
      this.oPgFrm.Page1.oPag.oGTTELEFO_1_35.value=this.w_GTTELEFO
    endif
    if not(this.oPgFrm.Page1.oPag.oGT__MAIL_1_36.value==this.w_GT__MAIL)
      this.oPgFrm.Page1.oPag.oGT__MAIL_1_36.value=this.w_GT__MAIL
    endif
    if not(this.oPgFrm.Page1.oPag.oGT_STATO_1_37.RadioValue()==this.w_GT_STATO)
      this.oPgFrm.Page1.oPag.oGT_STATO_1_37.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGTSITUAZ_1_38.RadioValue()==this.w_GTSITUAZ)
      this.oPgFrm.Page1.oPag.oGTSITUAZ_1_38.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGTNATGIU_1_39.value==this.w_GTNATGIU)
      this.oPgFrm.Page1.oPag.oGTNATGIU_1_39.value=this.w_GTNATGIU
    endif
    if not(this.oPgFrm.Page1.oPag.oGTCODDIC_1_40.value==this.w_GTCODDIC)
      this.oPgFrm.Page1.oPag.oGTCODDIC_1_40.value=this.w_GTCODDIC
    endif
    if not(this.oPgFrm.Page1.oPag.oGTPAEEST_1_69.value==this.w_GTPAEEST)
      this.oPgFrm.Page1.oPag.oGTPAEEST_1_69.value=this.w_GTPAEEST
    endif
    if not(this.oPgFrm.Page1.oPag.oGTIDFIES_1_71.value==this.w_GTIDFIES)
      this.oPgFrm.Page1.oPag.oGTIDFIES_1_71.value=this.w_GTIDFIES
    endif
    if not(this.oPgFrm.Page2.oPag.oGTRAPFIR_2_13.RadioValue()==this.w_GTRAPFIR)
      this.oPgFrm.Page2.oPag.oGTRAPFIR_2_13.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oGTRAPFIS_2_14.value==this.w_GTRAPFIS)
      this.oPgFrm.Page2.oPag.oGTRAPFIS_2_14.value=this.w_GTRAPFIS
    endif
    if not(this.oPgFrm.Page2.oPag.oGTRAPCAR_2_16.RadioValue()==this.w_GTRAPCAR)
      this.oPgFrm.Page2.oPag.oGTRAPCAR_2_16.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oGTCODFED_2_17.value==this.w_GTCODFED)
      this.oPgFrm.Page2.oPag.oGTCODFED_2_17.value=this.w_GTCODFED
    endif
    if not(this.oPgFrm.Page2.oPag.oGTRAPCOG_2_19.value==this.w_GTRAPCOG)
      this.oPgFrm.Page2.oPag.oGTRAPCOG_2_19.value=this.w_GTRAPCOG
    endif
    if not(this.oPgFrm.Page2.oPag.oGTRAPNOM_2_20.value==this.w_GTRAPNOM)
      this.oPgFrm.Page2.oPag.oGTRAPNOM_2_20.value=this.w_GTRAPNOM
    endif
    if not(this.oPgFrm.Page2.oPag.oGTRAPSES_2_25.RadioValue()==this.w_GTRAPSES)
      this.oPgFrm.Page2.oPag.oGTRAPSES_2_25.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oGTRAPDNS_2_27.value==this.w_GTRAPDNS)
      this.oPgFrm.Page2.oPag.oGTRAPDNS_2_27.value=this.w_GTRAPDNS
    endif
    if not(this.oPgFrm.Page2.oPag.oGTRAPNAS_2_30.value==this.w_GTRAPNAS)
      this.oPgFrm.Page2.oPag.oGTRAPNAS_2_30.value=this.w_GTRAPNAS
    endif
    if not(this.oPgFrm.Page2.oPag.oGTRAPPRO_2_31.value==this.w_GTRAPPRO)
      this.oPgFrm.Page2.oPag.oGTRAPPRO_2_31.value=this.w_GTRAPPRO
    endif
    if not(this.oPgFrm.Page2.oPag.oGTRAPEST_2_34.value==this.w_GTRAPEST)
      this.oPgFrm.Page2.oPag.oGTRAPEST_2_34.value=this.w_GTRAPEST
    endif
    if not(this.oPgFrm.Page2.oPag.oGTRAPFED_2_36.value==this.w_GTRAPFED)
      this.oPgFrm.Page2.oPag.oGTRAPFED_2_36.value=this.w_GTRAPFED
    endif
    if not(this.oPgFrm.Page2.oPag.oGTRAPRES_2_37.value==this.w_GTRAPRES)
      this.oPgFrm.Page2.oPag.oGTRAPRES_2_37.value=this.w_GTRAPRES
    endif
    if not(this.oPgFrm.Page2.oPag.oGTRAPIND_2_39.value==this.w_GTRAPIND)
      this.oPgFrm.Page2.oPag.oGTRAPIND_2_39.value=this.w_GTRAPIND
    endif
    if not(this.oPgFrm.Page2.oPag.oGTRAPTEL_2_41.value==this.w_GTRAPTEL)
      this.oPgFrm.Page2.oPag.oGTRAPTEL_2_41.value=this.w_GTRAPTEL
    endif
    if not(this.oPgFrm.Page2.oPag.oGTRAPDCA_2_44.value==this.w_GTRAPDCA)
      this.oPgFrm.Page2.oPag.oGTRAPDCA_2_44.value=this.w_GTRAPDCA
    endif
    if not(this.oPgFrm.Page2.oPag.oGTRAPFAL_2_45.value==this.w_GTRAPFAL)
      this.oPgFrm.Page2.oPag.oGTRAPFAL_2_45.value=this.w_GTRAPFAL
    endif
    if not(this.oPgFrm.Page2.oPag.oGTNOTRAS_2_49.RadioValue()==this.w_GTNOTRAS)
      this.oPgFrm.Page2.oPag.oGTNOTRAS_2_49.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oGTSEZ1ST_2_50.RadioValue()==this.w_GTSEZ1ST)
      this.oPgFrm.Page2.oPag.oGTSEZ1ST_2_50.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oGTSEZ1SV_2_51.RadioValue()==this.w_GTSEZ1SV)
      this.oPgFrm.Page2.oPag.oGTSEZ1SV_2_51.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oGTSEZ1SX_2_52.RadioValue()==this.w_GTSEZ1SX)
      this.oPgFrm.Page2.oPag.oGTSEZ1SX_2_52.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oGTSEZ1SY_2_53.RadioValue()==this.w_GTSEZ1SY)
      this.oPgFrm.Page2.oPag.oGTSEZ1SY_2_53.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oGTSEZ1DI_2_54.RadioValue()==this.w_GTSEZ1DI)
      this.oPgFrm.Page2.oPag.oGTSEZ1DI_2_54.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oGTLAVDIP_2_55.RadioValue()==this.w_GTLAVDIP)
      this.oPgFrm.Page2.oPag.oGTLAVDIP_2_55.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oGTLAVAUT_2_56.RadioValue()==this.w_GTLAVAUT)
      this.oPgFrm.Page2.oPag.oGTLAVAUT_2_56.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oGTFISAGG_2_58.value==this.w_GTFISAGG)
      this.oPgFrm.Page2.oPag.oGTFISAGG_2_58.value=this.w_GTFISAGG
    endif
    if not(this.oPgFrm.Page3.oPag.oGTTIPINV_3_1.RadioValue()==this.w_GTTIPINV)
      this.oPgFrm.Page3.oPag.oGTTIPINV_3_1.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oGTRIOPDP_3_2.RadioValue()==this.w_GTRIOPDP)
      this.oPgFrm.Page3.oPag.oGTRIOPDP_3_2.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oGTRIOPAU_3_3.RadioValue()==this.w_GTRIOPAU)
      this.oPgFrm.Page3.oPag.oGTRIOPAU_3_3.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oGTTRASNO_3_4.RadioValue()==this.w_GTTRASNO)
      this.oPgFrm.Page3.oPag.oGTTRASNO_3_4.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oGTRIOPSS_3_5.RadioValue()==this.w_GTRIOPSS)
      this.oPgFrm.Page3.oPag.oGTRIOPSS_3_5.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oGTRIOPDI_3_6.RadioValue()==this.w_GTRIOPDI)
      this.oPgFrm.Page3.oPag.oGTRIOPDI_3_6.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oGTRIOPST_3_7.RadioValue()==this.w_GTRIOPST)
      this.oPgFrm.Page3.oPag.oGTRIOPST_3_7.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oGTRIOPSV_3_8.RadioValue()==this.w_GTRIOPSV)
      this.oPgFrm.Page3.oPag.oGTRIOPSV_3_8.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oGTRIOPSX_3_9.RadioValue()==this.w_GTRIOPSX)
      this.oPgFrm.Page3.oPag.oGTRIOPSX_3_9.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oGTRIOPSY_3_10.RadioValue()==this.w_GTRIOPSY)
      this.oPgFrm.Page3.oPag.oGTRIOPSY_3_10.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oGTCFISAI_3_11.value==this.w_GTCFISAI)
      this.oPgFrm.Page3.oPag.oGTCFISAI_3_11.value=this.w_GTCFISAI
    endif
    if not(this.oPgFrm.Page3.oPag.oGTRISEDI_3_12.RadioValue()==this.w_GTRISEDI)
      this.oPgFrm.Page3.oPag.oGTRISEDI_3_12.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oGTRISEAU_3_13.RadioValue()==this.w_GTRISEAU)
      this.oPgFrm.Page3.oPag.oGTRISEAU_3_13.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oGTRISECA_3_14.RadioValue()==this.w_GTRISECA)
      this.oPgFrm.Page3.oPag.oGTRISECA_3_14.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oGTRISELB_3_15.RadioValue()==this.w_GTRISELB)
      this.oPgFrm.Page3.oPag.oGTRISELB_3_15.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oGTRISEAR_3_16.RadioValue()==this.w_GTRISEAR)
      this.oPgFrm.Page3.oPag.oGTRISEAR_3_16.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oGTAICFIS_3_17.value==this.w_GTAICFIS)
      this.oPgFrm.Page3.oPag.oGTAICFIS_3_17.value=this.w_GTAICFIS
    endif
    if not(this.oPgFrm.Page3.oPag.oGTDIRISE_3_18.RadioValue()==this.w_GTDIRISE)
      this.oPgFrm.Page3.oPag.oGTDIRISE_3_18.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oGTAURISE_3_19.RadioValue()==this.w_GTAURISE)
      this.oPgFrm.Page3.oPag.oGTAURISE_3_19.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oGTCARISE_3_20.RadioValue()==this.w_GTCARISE)
      this.oPgFrm.Page3.oPag.oGTCARISE_3_20.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oGTLBRISE_3_21.RadioValue()==this.w_GTLBRISE)
      this.oPgFrm.Page3.oPag.oGTLBRISE_3_21.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oGTARRISE_3_22.RadioValue()==this.w_GTARRISE)
      this.oPgFrm.Page3.oPag.oGTARRISE_3_22.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oGTFIRDIC_4_3.RadioValue()==this.w_GTFIRDIC)
      this.oPgFrm.Page4.oPag.oGTFIRDIC_4_3.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oGTSITPAR_4_5.value==this.w_GTSITPAR)
      this.oPgFrm.Page4.oPag.oGTSITPAR_4_5.value=this.w_GTSITPAR
    endif
    if not(this.oPgFrm.Page4.oPag.oGTATTEST_4_6.RadioValue()==this.w_GTATTEST)
      this.oPgFrm.Page4.oPag.oGTATTEST_4_6.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oGTSOGGE1_4_10.RadioValue()==this.w_GTSOGGE1)
      this.oPgFrm.Page4.oPag.oGTSOGGE1_4_10.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oGTFISSG1_4_11.value==this.w_GTFISSG1)
      this.oPgFrm.Page4.oPag.oGTFISSG1_4_11.value=this.w_GTFISSG1
    endif
    if not(this.oPgFrm.Page4.oPag.oGTFIRSG1_4_12.RadioValue()==this.w_GTFIRSG1)
      this.oPgFrm.Page4.oPag.oGTFIRSG1_4_12.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oGTSOGGE2_4_13.RadioValue()==this.w_GTSOGGE2)
      this.oPgFrm.Page4.oPag.oGTSOGGE2_4_13.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oGTFISSG2_4_14.value==this.w_GTFISSG2)
      this.oPgFrm.Page4.oPag.oGTFISSG2_4_14.value=this.w_GTFISSG2
    endif
    if not(this.oPgFrm.Page4.oPag.oGTFIRSG2_4_15.RadioValue()==this.w_GTFIRSG2)
      this.oPgFrm.Page4.oPag.oGTFIRSG2_4_15.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oGTSOGGE3_4_16.RadioValue()==this.w_GTSOGGE3)
      this.oPgFrm.Page4.oPag.oGTSOGGE3_4_16.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oGTFISSG3_4_17.value==this.w_GTFISSG3)
      this.oPgFrm.Page4.oPag.oGTFISSG3_4_17.value=this.w_GTFISSG3
    endif
    if not(this.oPgFrm.Page4.oPag.oGTFIRSG3_4_18.RadioValue()==this.w_GTFIRSG3)
      this.oPgFrm.Page4.oPag.oGTFIRSG3_4_18.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oGTSOGGE4_4_19.RadioValue()==this.w_GTSOGGE4)
      this.oPgFrm.Page4.oPag.oGTSOGGE4_4_19.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oGTFISSG4_4_20.value==this.w_GTFISSG4)
      this.oPgFrm.Page4.oPag.oGTFISSG4_4_20.value=this.w_GTFISSG4
    endif
    if not(this.oPgFrm.Page4.oPag.oGTFIRSG4_4_21.RadioValue()==this.w_GTFIRSG4)
      this.oPgFrm.Page4.oPag.oGTFIRSG4_4_21.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oGTSOGGE5_4_22.RadioValue()==this.w_GTSOGGE5)
      this.oPgFrm.Page4.oPag.oGTSOGGE5_4_22.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oGTFISSG5_4_23.value==this.w_GTFISSG5)
      this.oPgFrm.Page4.oPag.oGTFISSG5_4_23.value=this.w_GTFISSG5
    endif
    if not(this.oPgFrm.Page4.oPag.oGTFIRSG5_4_24.RadioValue()==this.w_GTFIRSG5)
      this.oPgFrm.Page4.oPag.oGTFIRSG5_4_24.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oGTIMPTRA_4_25.RadioValue()==this.w_GTIMPTRA)
      this.oPgFrm.Page4.oPag.oGTIMPTRA_4_25.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oGTINVTEL_4_26.RadioValue()==this.w_GTINVTEL)
      this.oPgFrm.Page4.oPag.oGTINVTEL_4_26.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oGTRICAVV_4_27.RadioValue()==this.w_GTRICAVV)
      this.oPgFrm.Page4.oPag.oGTRICAVV_4_27.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oGTCODINT_4_30.value==this.w_GTCODINT)
      this.oPgFrm.Page4.oPag.oGTCODINT_4_30.value=this.w_GTCODINT
    endif
    if not(this.oPgFrm.Page4.oPag.oGTNUMCAF_4_34.value==this.w_GTNUMCAF)
      this.oPgFrm.Page4.oPag.oGTNUMCAF_4_34.value=this.w_GTNUMCAF
    endif
    if not(this.oPgFrm.Page4.oPag.oGTDATIMP_4_36.value==this.w_GTDATIMP)
      this.oPgFrm.Page4.oPag.oGTDATIMP_4_36.value=this.w_GTDATIMP
    endif
    if not(this.oPgFrm.Page4.oPag.oGTFIRINT_4_37.RadioValue()==this.w_GTFIRINT)
      this.oPgFrm.Page4.oPag.oGTFIRINT_4_37.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oGTTIPFOR_5_2.RadioValue()==this.w_GTTIPFOR)
      this.oPgFrm.Page5.oPag.oGTTIPFOR_5_2.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oGTFLGCON_5_3.RadioValue()==this.w_GTFLGCON)
      this.oPgFrm.Page5.oPag.oGTFLGCON_5_3.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oGTFLGCOR_5_4.RadioValue()==this.w_GTFLGCOR)
      this.oPgFrm.Page5.oPag.oGTFLGCOR_5_4.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oGTFLGINT_5_5.RadioValue()==this.w_GTFLGINT)
      this.oPgFrm.Page5.oPag.oGTFLGINT_5_5.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oEVEECC_5_6.RadioValue()==this.w_EVEECC)
      this.oPgFrm.Page5.oPag.oEVEECC_5_6.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oEVEECC_2016_5_7.RadioValue()==this.w_EVEECC_2016)
      this.oPgFrm.Page5.oPag.oEVEECC_2016_5_7.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oEVEECC_2017_5_8.RadioValue()==this.w_EVEECC_2017)
      this.oPgFrm.Page5.oPag.oEVEECC_2017_5_8.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oGTTIPSOS_5_10.RadioValue()==this.w_GTTIPSOS)
      this.oPgFrm.Page5.oPag.oGTTIPSOS_5_10.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oGTPROTEC_5_11.value==this.w_GTPROTEC)
      this.oPgFrm.Page5.oPag.oGTPROTEC_5_11.value=this.w_GTPROTEC
    endif
    if not(this.oPgFrm.Page5.oPag.oGTPROSEZ_5_12.value==this.w_GTPROSEZ)
      this.oPgFrm.Page5.oPag.oGTPROSEZ_5_12.value=this.w_GTPROSEZ
    endif
    if not(this.oPgFrm.Page5.oPag.oGTSELCAF_5_16.RadioValue()==this.w_GTSELCAF)
      this.oPgFrm.Page5.oPag.oGTSELCAF_5_16.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oGTRESCAF_5_19.value==this.w_GTRESCAF)
      this.oPgFrm.Page5.oPag.oGTRESCAF_5_19.value=this.w_GTRESCAF
    endif
    if not(this.oPgFrm.Page5.oPag.oGTDELCAF_5_21.value==this.w_GTDELCAF)
      this.oPgFrm.Page5.oPag.oGTDELCAF_5_21.value=this.w_GTDELCAF
    endif
    if not(this.oPgFrm.Page5.oPag.oGTFLGFIR_5_22.RadioValue()==this.w_GTFLGFIR)
      this.oPgFrm.Page5.oPag.oGTFLGFIR_5_22.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oGT__ANNO_5_24.value==this.w_GT__ANNO)
      this.oPgFrm.Page5.oPag.oGT__ANNO_5_24.value=this.w_GT__ANNO
    endif
    if not(this.oPgFrm.Page5.oPag.oGTNOMFIL_5_28.value==this.w_GTNOMFIL)
      this.oPgFrm.Page5.oPag.oGTNOMFIL_5_28.value=this.w_GTNOMFIL
    endif
    cp_SetControlsValueExtFlds(this,'GEFILTEL')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_GTDATGEN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGTDATGEN_1_2.SetFocus()
            i_bnoObbl = !empty(.w_GTDATGEN)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_GTCODFIS)) or not(iif(.w_AZPERAZI = 'S',chkcfp(alltrim(.w_GTCODFIS),'CF'),chkcfp(alltrim(.w_GTCODFIS),'PI'))))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGTCODFIS_1_7.SetFocus()
            i_bnoObbl = !empty(.w_GTCODFIS)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(Empty(.w_Gtcodsos) Or Alltrim(.w_GTCODFIS) <> Alltrim(.w_GTCODSOS) and iif(!empty(.w_GTCODSOS),iif(len(alltrim(.w_GTCODSOS))=16,chkcfp(alltrim(.w_GTCODSOS),'CF'),chkcfp(alltrim(.w_GTCODSOS),'PI')),.t.))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGTCODSOS_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice fiscale del sostituto d'imposta deve essere diverso dal codice fiscale del dichiarante")
          case   (empty(.w_GTDENOMI))  and (.w_AZPERAZI<>'S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGTDENOMI_1_26.SetFocus()
            i_bnoObbl = !empty(.w_GTDENOMI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_GTCOGNOM))  and (.w_AZPERAZI='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGTCOGNOM_1_27.SetFocus()
            i_bnoObbl = !empty(.w_GTCOGNOM)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_GT__NOME))  and (.w_AZPERAZI='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGT__NOME_1_28.SetFocus()
            i_bnoObbl = !empty(.w_GT__NOME)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_GT_SESSO))  and (.w_AZPERAZI='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGT_SESSO_1_29.SetFocus()
            i_bnoObbl = !empty(.w_GT_SESSO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_GTCOMNAS))  and (.w_AZPERAZI='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGTCOMNAS_1_30.SetFocus()
            i_bnoObbl = !empty(.w_GTCOMNAS)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_GTPRONAS))  and (.w_AZPERAZI='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGTPRONAS_1_31.SetFocus()
            i_bnoObbl = !empty(.w_GTPRONAS)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_GTDATNAS))  and (.w_AZPERAZI='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGTDATNAS_1_32.SetFocus()
            i_bnoObbl = !empty(.w_GTDATNAS)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(at(' ',alltrim(.w_Gttelefo))=0 and at('/',alltrim(.w_Gttelefo))=0 and at('-',alltrim(.w_Gttelefo))=0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGTTELEFO_1_35.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Non inserire caratteri separatori tra prefisso e numero")
          case   ((empty(.w_GTNATGIU)) or not(( val(.w_GTNATGIU)>=1 and ( ( (.w_GTTIP770='77S17' OR .w_GTTIP770='77S18')  AND val(.w_GTNATGIU)<=45) OR (.w_GTTIP770<>'77S17' AND .w_GTTIP770<>'77S18' AND val(.w_GTNATGIU)<=44) )  OR (val(.w_GTNATGIU)>=50 and val(.w_GTNATGIU)<=59))))  and (.w_Azperazi<>'S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGTNATGIU_1_39.SetFocus()
            i_bnoObbl = !empty(.w_GTNATGIU)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valore non ammesso per natura giuridica")
          case   not(iif(not empty(.w_Gtcoddic),chkcfp(alltrim(.w_GTCODDIC),'PI'),.T.) )  and (.w_Azperazi<>'S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGTCODDIC_1_40.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_GTRAPFIS)) or not(iif(not empty(.w_Gtrapfis),chkcfp(alltrim(.w_Gtrapfis),'CF'),.T.)))  and (.w_GtRapFir='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oGTRAPFIS_2_14.SetFocus()
            i_bnoObbl = !empty(.w_GTRAPFIS)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_GTRAPCAR))  and (.w_GtRapFir='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oGTRAPCAR_2_16.SetFocus()
            i_bnoObbl = !empty(.w_GTRAPCAR)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(iif(Not Empty(.w_Gtcodfed),Chkcfp(.w_Gtcodfed,"PI", "", "", ""),.t.))  and (.w_GtRapFir='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oGTCODFED_2_17.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_GTRAPCOG))  and (.w_GtRapFir='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oGTRAPCOG_2_19.SetFocus()
            i_bnoObbl = !empty(.w_GTRAPCOG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_GTRAPNOM))  and (.w_GtRapFir='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oGTRAPNOM_2_20.SetFocus()
            i_bnoObbl = !empty(.w_GTRAPNOM)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_GTRAPSES))  and (.w_GtRapFir='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oGTRAPSES_2_25.SetFocus()
            i_bnoObbl = !empty(.w_GTRAPSES)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_GTRAPDNS))  and (.w_GtRapFir='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oGTRAPDNS_2_27.SetFocus()
            i_bnoObbl = !empty(.w_GTRAPDNS)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_GTRAPNAS))  and (.w_GtRapFir='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oGTRAPNAS_2_30.SetFocus()
            i_bnoObbl = !empty(.w_GTRAPNAS)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_GTRAPFED))  and (.w_GtRapFir='S' And Not Empty(.w_Gtrapest))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oGTRAPFED_2_36.SetFocus()
            i_bnoObbl = !empty(.w_GTRAPFED)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_GTRAPIND))  and (.w_GtRapFir='S' And Not Empty(.w_Gtrapest))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oGTRAPIND_2_39.SetFocus()
            i_bnoObbl = !empty(.w_GTRAPIND)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(at(' ',alltrim(.w_Gtraptel))=0 and at('/',alltrim(.w_Gtraptel))=0 and at('-',alltrim(.w_Gtraptel))=0)  and (.w_GtRapFir='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oGTRAPTEL_2_41.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Non inserire caratteri separatori tra prefisso e numero")
          case   not(iif(Not Empty(.w_Gtfisagg),Chkcfp(alltrim(.w_GTFISAGG),'CF'),.t.)                 )  and not(.w_GTTIP770='77S17' OR .w_GTTIP770='77S18')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oGTFISAGG_2_58.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_GTCFISAI)) or not(iif(not empty(.w_Gtcfisai),chkcfp(alltrim(.w_Gtcfisai),'CF'),.T.)))  and (.w_GTTIPINV="2" And (.w_GTRISEDI="1" Or .w_GTRISEAU="1" Or .w_GTRISECA="1" Or .w_GTRISELB="1" Or .w_GTRISEAR="1"))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oGTCFISAI_3_11.SetFocus()
            i_bnoObbl = !empty(.w_GTCFISAI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_GTAICFIS)) or not(iif(not empty(.w_Gtaicfis),chkcfp(alltrim(.w_Gtaicfis),'CF'),.T.) ))  and (.w_GTTIPINV="2" And (.w_GTDIRISE="1" Or .w_GTAURISE="1" Or .w_GTCARISE="1" Or .w_GTLBRISE="1" Or .w_GTARRISE="1"))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oGTAICFIS_3_17.SetFocus()
            i_bnoObbl = !empty(.w_GTAICFIS)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_GTSOGGE1))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oGTSOGGE1_4_10.SetFocus()
            i_bnoObbl = !empty(.w_GTSOGGE1)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_GTFISSG1)) or not(iif(not empty(.w_GTFISSG1),chkcfp(alltrim(.w_GTFISSG1),'CF'),.T.)))  and (.w_GTSOGGE1 $ '1234')
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oGTFISSG1_4_11.SetFocus()
            i_bnoObbl = !empty(.w_GTFISSG1)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_GTSOGGE2))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oGTSOGGE2_4_13.SetFocus()
            i_bnoObbl = !empty(.w_GTSOGGE2)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_GTFISSG2)) or not(iif(not empty(.w_GTFISSG2),chkcfp(alltrim(.w_GTFISSG2),'CF'),.T.)))  and (.w_GTSOGGE2 $ '1234')
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oGTFISSG2_4_14.SetFocus()
            i_bnoObbl = !empty(.w_GTFISSG2)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_GTSOGGE3))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oGTSOGGE3_4_16.SetFocus()
            i_bnoObbl = !empty(.w_GTSOGGE3)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_GTFISSG3)) or not(iif(not empty(.w_GTFISSG3),chkcfp(alltrim(.w_GTFISSG3),'CF'),.T.)))  and (.w_GTSOGGE3 $ '1234')
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oGTFISSG3_4_17.SetFocus()
            i_bnoObbl = !empty(.w_GTFISSG3)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_GTSOGGE4))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oGTSOGGE4_4_19.SetFocus()
            i_bnoObbl = !empty(.w_GTSOGGE4)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_GTFISSG4)) or not(iif(not empty(.w_GTFISSG4),chkcfp(alltrim(.w_GTFISSG4),'CF'),.T.)))  and (.w_GTSOGGE4 $ '1234')
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oGTFISSG4_4_20.SetFocus()
            i_bnoObbl = !empty(.w_GTFISSG4)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_GTSOGGE5))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oGTSOGGE5_4_22.SetFocus()
            i_bnoObbl = !empty(.w_GTSOGGE5)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_GTFISSG5)) or not(iif(not empty(.w_GTFISSG5),chkcfp(alltrim(.w_GTFISSG5),'CF'),.T.)))  and (.w_GTSOGGE5 $ '1234')
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oGTFISSG5_4_23.SetFocus()
            i_bnoObbl = !empty(.w_GTFISSG5)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(iif(not empty(.w_Gtcodint),chkcfp(alltrim(.w_Gtcodint),'CF'),.T.))  and (.w_GTIMPTRA $ '12')
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oGTCODINT_4_30.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((.w_Azperazi='S' and  .w_Gttipfor<>'06' ) or .w_Azperazi<>'S')
            .oPgFrm.ActivePage = 5
            .oPgFrm.Page5.oPag.oGTTIPFOR_5_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Selezione non consentita con tipo fornitore persona fisica")
          case   (empty(.w_GTPROTEC))  and not(.w_GTTIP770<>'77S18')  and ((.w_GTFLGCOR='1' or .w_GTFLGINT='1') And .w_GTTIPINV="2")
            .oPgFrm.ActivePage = 5
            .oPgFrm.Page5.oPag.oGTPROTEC_5_11.SetFocus()
            i_bnoObbl = !empty(.w_GTPROTEC)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_GTPROSEZ))  and not(.w_GTTIP770<>'77S18')  and (Not Empty(.w_Gtprotec))
            .oPgFrm.ActivePage = 5
            .oPgFrm.Page5.oPag.oGTPROSEZ_5_12.SetFocus()
            i_bnoObbl = !empty(.w_GTPROSEZ)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_GTSELCAF))
            .oPgFrm.ActivePage = 5
            .oPgFrm.Page5.oPag.oGTSELCAF_5_16.SetFocus()
            i_bnoObbl = !empty(.w_GTSELCAF)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(iif(not empty(.w_Gtrescaf),chkcfp(alltrim(.w_Gtrescaf),'CF'),.T.))
            .oPgFrm.ActivePage = 5
            .oPgFrm.Page5.oPag.oGTRESCAF_5_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(iif(not empty(.w_Gtdelcaf),chkcfp(alltrim(.w_Gtdelcaf),'PI'),.T.))  and not(.w_Gtselcaf <> '1')
            .oPgFrm.ActivePage = 5
            .oPgFrm.Page5.oPag.oGTDELCAF_5_21.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_GT__ANNO)) or not((.w_GTTIP770='77S16' And .w_GT__ANNO=2015) Or (.w_GTTIP770='77S17' And .w_GT__ANNO=2016) Or (.w_GTTIP770='77S18' And .w_GT__ANNO=2017)))  and (.cFunction='Load')
            .oPgFrm.ActivePage = 5
            .oPgFrm.Page5.oPag.oGT__ANNO_5_24.SetFocus()
            i_bnoObbl = !empty(.w_GT__ANNO)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      *i_bRes = i_bRes .and. .Gsri_Mgt.CheckForm()
      if i_bres
        i_bres=  .Gsri_Mgt.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=5
        endif
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_GTCODFIS = this.w_GTCODFIS
    this.o_GTRAPFIR = this.w_GTRAPFIR
    this.o_GTRAPCAR = this.w_GTRAPCAR
    this.o_GTRAPEST = this.w_GTRAPEST
    this.o_GTNOTRAS = this.w_GTNOTRAS
    this.o_GTLAVAUT = this.w_GTLAVAUT
    this.o_GTTIPINV = this.w_GTTIPINV
    this.o_GTTRASNO = this.w_GTTRASNO
    this.o_GTRIOPSY = this.w_GTRIOPSY
    this.o_GTRISEDI = this.w_GTRISEDI
    this.o_GTRISEAU = this.w_GTRISEAU
    this.o_GTRISECA = this.w_GTRISECA
    this.o_GTRISELB = this.w_GTRISELB
    this.o_GTRISEAR = this.w_GTRISEAR
    this.o_GTDIRISE = this.w_GTDIRISE
    this.o_GTAURISE = this.w_GTAURISE
    this.o_GTCARISE = this.w_GTCARISE
    this.o_GTLBRISE = this.w_GTLBRISE
    this.o_GTARRISE = this.w_GTARRISE
    this.o_GTSOGGE1 = this.w_GTSOGGE1
    this.o_GTSOGGE2 = this.w_GTSOGGE2
    this.o_GTSOGGE3 = this.w_GTSOGGE3
    this.o_GTSOGGE4 = this.w_GTSOGGE4
    this.o_GTSOGGE5 = this.w_GTSOGGE5
    this.o_GTIMPTRA = this.w_GTIMPTRA
    this.o_GTINVTEL = this.w_GTINVTEL
    this.o_GTFLGCOR = this.w_GTFLGCOR
    this.o_GTFLGINT = this.w_GTFLGINT
    this.o_EVEECC = this.w_EVEECC
    this.o_EVEECC_2016 = this.w_EVEECC_2016
    this.o_EVEECC_2017 = this.w_EVEECC_2017
    this.o_GTPROTEC = this.w_GTPROTEC
    this.o_GTSELCAF = this.w_GTSELCAF
    this.o_GTNOMFIL = this.w_GTNOMFIL
    * --- Gsri_Mgt : Depends On
    this.Gsri_Mgt.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgsri_agyPag1 as StdContainer
  Width  = 794
  height = 552
  stdWidth  = 794
  stdheight = 552
  resizeXpos=469
  resizeYpos=203
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oGTDATGEN_1_2 as StdField with uid="TWFLVODDQH",rtseq=2,rtrep=.f.,;
    cFormVar = "w_GTDATGEN", cQueryName = "GTDATGEN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di generazione file telematico",;
    HelpContextID = 100949324,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=165, Top=17

  add object oGTDESAGG_1_3 as StdField with uid="ZJAXFDWJCR",rtseq=3,rtrep=.f.,;
    cFormVar = "w_GTDESAGG", cQueryName = "GTDESAGG",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Campo che accoglie le note aggiuntive",;
    HelpContextID = 202399059,;
   bGlobalFont=.t.,;
    Height=21, Width=601, Left=165, Top=60, InputMask=replicate('X',254)

  add object oGTCODFIS_1_7 as StdField with uid="FYSZBXXOAN",rtseq=7,rtrep=.f.,;
    cFormVar = "w_GTCODFIS", cQueryName = "GTCODFIS",;
    bObbl = .t. , nPag = 1, value=space(16), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale del dichiarante",;
    HelpContextID = 133590343,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=165, Top=101, cSayPict='REPLICATE("!",16)', cGetPict='REPLICATE("!",16)', InputMask=replicate('X',16)

  func oGTCODFIS_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (iif(.w_AZPERAZI = 'S',chkcfp(alltrim(.w_GTCODFIS),'CF'),chkcfp(alltrim(.w_GTCODFIS),'PI')))
    endwith
    return bRes
  endfunc

  add object oGTCODSOS_1_8 as StdField with uid="DFQWHMDFGZ",rtseq=8,rtrep=.f.,;
    cFormVar = "w_GTCODSOS", cQueryName = "GTCODSOS",;
    bObbl = .f. , nPag = 1, value=space(16), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice fiscale del sostituto d'imposta deve essere diverso dal codice fiscale del dichiarante",;
    ToolTipText = "Codice fiscale del sostituto d'imposta, da compilare solo in caso di operazioni straordinarie e successioni",;
    HelpContextID = 84513465,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=546, Top=101, cSayPict='REPLICATE("!",16)', cGetPict='REPLICATE("!",16)', InputMask=replicate('X',16), tabstop=.F.

  func oGTCODSOS_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (Empty(.w_Gtcodsos) Or Alltrim(.w_GTCODFIS) <> Alltrim(.w_GTCODSOS) and iif(!empty(.w_GTCODSOS),iif(len(alltrim(.w_GTCODSOS))=16,chkcfp(alltrim(.w_GTCODSOS),'CF'),chkcfp(alltrim(.w_GTCODSOS),'PI')),.t.))
    endwith
    return bRes
  endfunc

  add object oGTDENOMI_1_26 as StdField with uid="MLRYKOPWFX",rtseq=26,rtrep=.f.,;
    cFormVar = "w_GTDENOMI", cQueryName = "GTDENOMI",;
    bObbl = .t. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Denominazione o ragione sociale",;
    HelpContextID = 27239087,;
   bGlobalFont=.t.,;
    Height=21, Width=461, Left=165, Top=191, cSayPict="REPL('!',60)", cGetPict="REPL('!',60)", InputMask=replicate('X',60)

  func oGTDENOMI_1_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZPERAZI<>'S')
    endwith
   endif
  endfunc

  add object oGTCOGNOM_1_27 as StdField with uid="USTYABUOUZ",rtseq=27,rtrep=.f.,;
    cFormVar = "w_GTCOGNOM", cQueryName = "GTCOGNOM",;
    bObbl = .t. , nPag = 1, value=space(24), bMultilanguage =  .f.,;
    ToolTipText = "Cognome",;
    HelpContextID = 3773107,;
   bGlobalFont=.t.,;
    Height=21, Width=209, Left=165, Top=228, cSayPict="REPL('!',24)", cGetPict="REPL('!',24)", InputMask=replicate('X',24)

  func oGTCOGNOM_1_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZPERAZI='S')
    endwith
   endif
  endfunc

  add object oGT__NOME_1_28 as StdField with uid="LKFBHLXRZV",rtseq=28,rtrep=.f.,;
    cFormVar = "w_GT__NOME", cQueryName = "GT__NOME",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Nome",;
    HelpContextID = 29053611,;
   bGlobalFont=.t.,;
    Height=21, Width=174, Left=435, Top=228, cSayPict="REPL('!',20)", cGetPict="REPL('!',20)", InputMask=replicate('X',20)

  func oGT__NOME_1_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZPERAZI='S')
    endwith
   endif
  endfunc


  add object oGT_SESSO_1_29 as StdCombo with uid="UFMSDHWBVZ",rtseq=29,rtrep=.f.,left=680,top=228,width=91,height=22;
    , ToolTipText = "Sesso del contribuente";
    , HelpContextID = 182496587;
    , cFormVar="w_GT_SESSO",RowSource=""+"Maschio,"+"Femmina", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oGT_SESSO_1_29.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oGT_SESSO_1_29.GetRadio()
    this.Parent.oContained.w_GT_SESSO = this.RadioValue()
    return .t.
  endfunc

  func oGT_SESSO_1_29.SetRadio()
    this.Parent.oContained.w_GT_SESSO=trim(this.Parent.oContained.w_GT_SESSO)
    this.value = ;
      iif(this.Parent.oContained.w_GT_SESSO=='M',1,;
      iif(this.Parent.oContained.w_GT_SESSO=='F',2,;
      0))
  endfunc

  func oGT_SESSO_1_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZPERAZI='S')
    endwith
   endif
  endfunc

  add object oGTCOMNAS_1_30 as StdField with uid="DUFMGZMKGT",rtseq=30,rtrep=.f.,;
    cFormVar = "w_GTCOMNAS", cQueryName = "GTCOMNAS",;
    bObbl = .t. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune di nascita",;
    HelpContextID = 10064569,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=165, Top=265, cSayPict="REPL('!',40)", cGetPict="REPL('!',40)", InputMask=replicate('X',40), bHasZoom = .t. 

  func oGTCOMNAS_1_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZPERAZI='S')
    endwith
   endif
  endfunc

  proc oGTCOMNAS_1_30.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C","",".w_GTCOMNAS",".w_GTPRONAS")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oGTPRONAS_1_31 as StdField with uid="HHSQKKMXTC",rtseq=31,rtrep=.f.,;
    cFormVar = "w_GTPRONAS", cQueryName = "GTPRONAS",;
    bObbl = .t. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Provincia di nascita",;
    HelpContextID = 12411577,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=530, Top=265, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2), bHasZoom = .t. 

  func oGTPRONAS_1_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZPERAZI='S')
    endwith
   endif
  endfunc

  proc oGTPRONAS_1_31.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"P","","",".w_GTPRONAS")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oGTDATNAS_1_32 as StdField with uid="ECDVVXPNAO",rtseq=32,rtrep=.f.,;
    cFormVar = "w_GTDATNAS", cQueryName = "GTDATNAS",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di nascita",;
    HelpContextID = 16491193,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=680, Top=265

  func oGTDATNAS_1_32.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZPERAZI='S')
    endwith
   endif
  endfunc

  add object oGTCODATT_1_34 as StdField with uid="RNKMVQTHKN",rtseq=34,rtrep=.f.,;
    cFormVar = "w_GTCODATT", cQueryName = "GTCODATT",;
    bObbl = .f. , nPag = 1, value=space(6), bMultilanguage =  .f.,;
    ToolTipText = "Codice attivit�",;
    HelpContextID = 217476422,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=165, Top=301, cSayPict='repl("!",6)', cGetPict='repl("!",6)', InputMask=replicate('X',6), bHasZoom = .t. 

  proc oGTCODATT_1_34.mZoom
    vx_exec("gsri8sft.vzm",this.Parent.oContained)
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oGTTELEFO_1_35 as StdField with uid="YTVMLXCIYI",rtseq=35,rtrep=.f.,;
    cFormVar = "w_GTTELEFO", cQueryName = "GTTELEFO",;
    bObbl = .f. , nPag = 1, value=space(12), bMultilanguage =  .f.,;
    sErrorMsg = "Non inserire caratteri separatori tra prefisso e numero",;
    ToolTipText = "Numero di telefono o fax",;
    HelpContextID = 142564683,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=530, Top=301, cSayPict='"999999999999"', cGetPict='"999999999999"', InputMask=replicate('X',12)

  func oGTTELEFO_1_35.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (at(' ',alltrim(.w_Gttelefo))=0 and at('/',alltrim(.w_Gttelefo))=0 and at('-',alltrim(.w_Gttelefo))=0)
    endwith
    return bRes
  endfunc

  add object oGT__MAIL_1_36 as StdField with uid="MTZLJHHSMZ",rtseq=36,rtrep=.f.,;
    cFormVar = "w_GT__MAIL", cQueryName = "GT__MAIL",;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo mail",;
    HelpContextID = 206875982,;
   bGlobalFont=.t.,;
    Height=21, Width=566, Left=165, Top=339, cSayPict="replicate('!', 100)", cGetPict="replicate('!', 100)", InputMask=replicate('X',100)


  add object oGT_STATO_1_37 as StdCombo with uid="OSUGDFVICW",value=5,rtseq=37,rtrep=.f.,left=165,top=376,width=356,height=22;
    , ToolTipText = "Stato della societ� o ente";
    , HelpContextID = 200322379;
    , cFormVar="w_GT_STATO",RowSource=""+"1 - Soggetto in normale attivit�,"+"2 - Soggetto in liquidazione per cessazione attivit�,"+"3 - Soggetto in fallimento o in liquidazione coatta amministrativa,"+"4 - Soggetto estinto,"+"Non selezionato", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oGT_STATO_1_37.RadioValue()
    return(iif(this.value =1,'1',;
    iif(this.value =2,'2',;
    iif(this.value =3,'3',;
    iif(this.value =4,'4',;
    iif(this.value =5,' ',;
    ' '))))))
  endfunc
  func oGT_STATO_1_37.GetRadio()
    this.Parent.oContained.w_GT_STATO = this.RadioValue()
    return .t.
  endfunc

  func oGT_STATO_1_37.SetRadio()
    this.Parent.oContained.w_GT_STATO=trim(this.Parent.oContained.w_GT_STATO)
    this.value = ;
      iif(this.Parent.oContained.w_GT_STATO=='1',1,;
      iif(this.Parent.oContained.w_GT_STATO=='2',2,;
      iif(this.Parent.oContained.w_GT_STATO=='3',3,;
      iif(this.Parent.oContained.w_GT_STATO=='4',4,;
      iif(this.Parent.oContained.w_GT_STATO=='',5,;
      0)))))
  endfunc

  func oGT_STATO_1_37.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Azperazi<>'S')
    endwith
   endif
  endfunc


  add object oGTSITUAZ_1_38 as StdCombo with uid="ZVRLQNVQQV",value=1,rtseq=38,rtrep=.f.,left=165,top=419,width=356,height=22;
    , ToolTipText = "Situazione della societ� o ente";
    , HelpContextID = 134517440;
    , cFormVar="w_GTSITUAZ",RowSource=""+"Non selezionata,"+"1 - Periodo imposta inizio liquidazione,"+"2 - Periodo imposta successivo a periodo liquidazione o fallimento,"+"3 - Periodo imposta termine liquidazione,"+"5 - Periodo imposta avvenuta trasformazione,"+"6 - Periodo normale di imposta", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oGTSITUAZ_1_38.RadioValue()
    return(iif(this.value =1,' ',;
    iif(this.value =2,'1',;
    iif(this.value =3,'2',;
    iif(this.value =4,'3',;
    iif(this.value =5,'5',;
    iif(this.value =6,'6',;
    ' ')))))))
  endfunc
  func oGTSITUAZ_1_38.GetRadio()
    this.Parent.oContained.w_GTSITUAZ = this.RadioValue()
    return .t.
  endfunc

  func oGTSITUAZ_1_38.SetRadio()
    this.Parent.oContained.w_GTSITUAZ=trim(this.Parent.oContained.w_GTSITUAZ)
    this.value = ;
      iif(this.Parent.oContained.w_GTSITUAZ=='',1,;
      iif(this.Parent.oContained.w_GTSITUAZ=='1',2,;
      iif(this.Parent.oContained.w_GTSITUAZ=='2',3,;
      iif(this.Parent.oContained.w_GTSITUAZ=='3',4,;
      iif(this.Parent.oContained.w_GTSITUAZ=='5',5,;
      iif(this.Parent.oContained.w_GTSITUAZ=='6',6,;
      0))))))
  endfunc

  func oGTSITUAZ_1_38.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Azperazi<>'S')
    endwith
   endif
  endfunc

  add object oGTNATGIU_1_39 as StdField with uid="ENXNNRQMZS",rtseq=39,rtrep=.f.,;
    cFormVar = "w_GTNATGIU", cQueryName = "GTNATGIU",;
    bObbl = .t. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Valore non ammesso per natura giuridica",;
    ToolTipText = "Tipologia natura giuridica",;
    HelpContextID = 100908357,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=165, Top=459, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2)

  func oGTNATGIU_1_39.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Azperazi<>'S')
    endwith
   endif
  endfunc

  func oGTNATGIU_1_39.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (( val(.w_GTNATGIU)>=1 and ( ( (.w_GTTIP770='77S17' OR .w_GTTIP770='77S18')  AND val(.w_GTNATGIU)<=45) OR (.w_GTTIP770<>'77S17' AND .w_GTTIP770<>'77S18' AND val(.w_GTNATGIU)<=44) )  OR (val(.w_GTNATGIU)>=50 and val(.w_GTNATGIU)<=59)))
    endwith
    return bRes
  endfunc

  add object oGTCODDIC_1_40 as StdField with uid="POFJPFDSMN",rtseq=40,rtrep=.f.,;
    cFormVar = "w_GTCODDIC", cQueryName = "GTCODDIC",;
    bObbl = .f. , nPag = 1, value=space(11), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale dicastero",;
    HelpContextID = 167144791,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=539, Top=456, cSayPict='repl("!",11)', cGetPict='repl("!",11)', InputMask=replicate('X',11)

  func oGTCODDIC_1_40.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Azperazi<>'S')
    endwith
   endif
  endfunc

  func oGTCODDIC_1_40.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (iif(not empty(.w_Gtcoddic),chkcfp(alltrim(.w_GTCODDIC),'PI'),.T.) )
    endwith
    return bRes
  endfunc


  add object oObj_1_62 as cp_runprogram with uid="NBFRWAJSRD",left=19, top=598, width=172,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSRI_BHC(w_HASEVCOP)",;
    cEvent = "HasEvent",;
    nPag=1;
    , ToolTipText = "Richiamato dall'area manuale declare (hascpevent)";
    , HelpContextID = 135401702

  add object oGTPAEEST_1_69 as StdField with uid="ZZWYBQSKEZ",rtseq=45,rtrep=.f.,;
    cFormVar = "w_GTPAEEST", cQueryName = "GTPAEEST",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice paese estero",;
    HelpContextID = 118252218,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=165, Top=525, cSayPict='"999"', cGetPict='"999"', InputMask=replicate('X',3)

  func oGTPAEEST_1_69.mHide()
    with this.Parent.oContained
      return (.w_GTTIP770<>'77S17' And .w_GTTIP770<>'77S18')
    endwith
  endfunc

  add object oGTIDFIES_1_71 as StdField with uid="DUPQWGWGJA",rtseq=46,rtrep=.f.,;
    cFormVar = "w_GTIDFIES", cQueryName = "GTIDFIES",;
    bObbl = .f. , nPag = 1, value=space(24), bMultilanguage =  .f.,;
    ToolTipText = "Numero di identificazione fiscale estero",;
    HelpContextID = 81857863,;
   bGlobalFont=.t.,;
    Height=21, Width=188, Left=539, Top=527, cSayPict="REPL('!',24)", cGetPict="REPL('!',24)", InputMask=replicate('X',24)

  func oGTIDFIES_1_71.mHide()
    with this.Parent.oContained
      return (.w_GTTIP770<>'77S17' And .w_GTTIP770<>'77S18')
    endwith
  endfunc

  add object oStr_1_41 as StdString with uid="DSZQYEUOKT",Visible=.t., Left=7, Top=159,;
    Alignment=0, Width=395, Height=19,;
    Caption="Dati relativi al sostituto"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_42 as StdString with uid="MSUZFRVMFD",Visible=.t., Left=324, Top=104,;
    Alignment=1, Width=217, Height=18,;
    Caption="Cod. fiscale del sostituto d'imposta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="OSGCYPTITY",Visible=.t., Left=84, Top=230,;
    Alignment=1, Width=79, Height=18,;
    Caption="Cognome:"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="LGSUOYWZDY",Visible=.t., Left=368, Top=230,;
    Alignment=1, Width=62, Height=18,;
    Caption="Nome:"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="DIRMDFWZQS",Visible=.t., Left=33, Top=340,;
    Alignment=1, Width=130, Height=18,;
    Caption="E-mail:"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="YKJNGKACOZ",Visible=.t., Left=64, Top=304,;
    Alignment=1, Width=99, Height=18,;
    Caption="Codice attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_48 as StdString with uid="JOEJGOSTIM",Visible=.t., Left=34, Top=18,;
    Alignment=1, Width=129, Height=18,;
    Caption="Data comunicazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_49 as StdString with uid="KTZEYIUZXG",Visible=.t., Left=45, Top=61,;
    Alignment=1, Width=118, Height=18,;
    Caption="Note aggiuntive:"  ;
  , bGlobalFont=.t.

  add object oStr_1_50 as StdString with uid="PUUEDWCIXC",Visible=.t., Left=5, Top=104,;
    Alignment=1, Width=158, Height=18,;
    Caption="Cod. fiscale dichiarante:"  ;
  , bGlobalFont=.t.

  add object oStr_1_51 as StdString with uid="LOIGJCPWGC",Visible=.t., Left=14, Top=192,;
    Alignment=1, Width=149, Height=18,;
    Caption="Denominazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_52 as StdString with uid="KPXQWREYEY",Visible=.t., Left=423, Top=304,;
    Alignment=1, Width=103, Height=18,;
    Caption="Telefono o fax:"  ;
  , bGlobalFont=.t.

  add object oStr_1_53 as StdString with uid="CNJREOUFHV",Visible=.t., Left=581, Top=267,;
    Alignment=1, Width=96, Height=18,;
    Caption="Data di nascita:"  ;
  , bGlobalFont=.t.

  add object oStr_1_54 as StdString with uid="HEUTCFKDNL",Visible=.t., Left=30, Top=267,;
    Alignment=1, Width=133, Height=18,;
    Caption="Comune di nascita:"  ;
  , bGlobalFont=.t.

  add object oStr_1_55 as StdString with uid="GTAPKTSGGM",Visible=.t., Left=456, Top=267,;
    Alignment=1, Width=70, Height=18,;
    Caption="Provincia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_56 as StdString with uid="JOPERGGNNX",Visible=.t., Left=631, Top=231,;
    Alignment=1, Width=46, Height=18,;
    Caption="Sesso:"  ;
  , bGlobalFont=.t.

  add object oStr_1_57 as StdString with uid="VSDPRDJBEE",Visible=.t., Left=59, Top=461,;
    Alignment=1, Width=104, Height=18,;
    Caption="Natura giuridica:"  ;
  , bGlobalFont=.t.

  add object oStr_1_58 as StdString with uid="LZDXUYAMQO",Visible=.t., Left=119, Top=378,;
    Alignment=1, Width=44, Height=18,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_59 as StdString with uid="MAYFCIUEDN",Visible=.t., Left=93, Top=424,;
    Alignment=1, Width=70, Height=18,;
    Caption="Situazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_60 as StdString with uid="PNLZPVFDQD",Visible=.t., Left=282, Top=459,;
    Alignment=1, Width=252, Height=18,;
    Caption="Codice fiscale del dicastero di appartenenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_66 as StdString with uid="UWWUFTSGGT",Visible=.t., Left=7, Top=491,;
    Alignment=0, Width=395, Height=19,;
    Caption="Dati soggetto estero"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_66.mHide()
    with this.Parent.oContained
      return (.w_GTTIP770<>'77S17' And .w_GTTIP770<>'77S18')
    endwith
  endfunc

  add object oStr_1_68 as StdString with uid="BEPHZAMVMI",Visible=.t., Left=45, Top=527,;
    Alignment=1, Width=118, Height=18,;
    Caption="Codice paese:"  ;
  , bGlobalFont=.t.

  func oStr_1_68.mHide()
    with this.Parent.oContained
      return (.w_GTTIP770<>'77S17' And .w_GTTIP770<>'77S18')
    endwith
  endfunc

  add object oStr_1_70 as StdString with uid="QONMPKPFPO",Visible=.t., Left=353, Top=527,;
    Alignment=1, Width=181, Height=18,;
    Caption="Numero di identificazione fiscale:"  ;
  , bGlobalFont=.t.

  func oStr_1_70.mHide()
    with this.Parent.oContained
      return (.w_GTTIP770<>'77S17' And .w_GTTIP770<>'77S18')
    endwith
  endfunc

  add object oBox_1_47 as StdBox with uid="UWPWFZSCNE",left=-1, top=177, width=785,height=2

  add object oBox_1_67 as StdBox with uid="GEXLVOJGXV",left=0, top=511, width=793,height=2
enddefine
define class tgsri_agyPag2 as StdContainer
  Width  = 794
  height = 552
  stdWidth  = 794
  stdheight = 552
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oGTRAPFIR_2_13 as StdCheck with uid="MGUJGMBSME",rtseq=58,rtrep=.f.,left=592, top=32, caption="Rappresentante firmatario",;
    ToolTipText = "Se attivo, abilita l'inserimento dei dati relativi al rappresentante firmatario",;
    HelpContextID = 121863496,;
    cFormVar="w_GTRAPFIR", bObbl = .f. , nPag = 2;
    , Tabstop=.F.;
   , bGlobalFont=.t.


  func oGTRAPFIR_2_13.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oGTRAPFIR_2_13.GetRadio()
    this.Parent.oContained.w_GTRAPFIR = this.RadioValue()
    return .t.
  endfunc

  func oGTRAPFIR_2_13.SetRadio()
    this.Parent.oContained.w_GTRAPFIR=trim(this.Parent.oContained.w_GTRAPFIR)
    this.value = ;
      iif(this.Parent.oContained.w_GTRAPFIR=='S',1,;
      0)
  endfunc

  add object oGTRAPFIS_2_14 as StdField with uid="ZBQYPAUWAS",rtseq=59,rtrep=.f.,;
    cFormVar = "w_GTRAPFIS", cQueryName = "GTRAPFIS",;
    bObbl = .t. , nPag = 2, value=space(16), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale del rappresentante",;
    HelpContextID = 121863495,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=21, Top=87, cSayPict='REPLICATE("!",16)', cGetPict='REPLICATE("!",16)', InputMask=replicate('X',16)

  func oGTRAPFIS_2_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GtRapFir='S')
    endwith
   endif
  endfunc

  func oGTRAPFIS_2_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (iif(not empty(.w_Gtrapfis),chkcfp(alltrim(.w_Gtrapfis),'CF'),.T.))
    endwith
    return bRes
  endfunc


  add object oGTRAPCAR_2_16 as StdCombo with uid="PMBLFYANVD",rtseq=60,rtrep=.f.,left=238,top=85,width=286,height=22;
    , ToolTipText = "Codice carica del rappresentante";
    , HelpContextID = 96240312;
    , cFormVar="w_GTRAPCAR",RowSource=""+""+l_descri1+","+""+l_descri2+","+""+l_descri3+","+""+l_descri4+","+""+l_descri5+","+""+l_descri6+","+""+l_descri7+","+""+l_descri8+","+""+l_descri9+","+""+l_descri10+","+""+l_descri11+","+""+l_descri12+","+""+l_descri13+","+""+l_descri14+","+""+l_descri15+"", bObbl = .t. , nPag = 2;
  , bGlobalFont=.t.


  func oGTRAPCAR_2_16.RadioValue()
    return(iif(this.value =1,'1',;
    iif(this.value =2,'2',;
    iif(this.value =3,'3',;
    iif(this.value =4,'4',;
    iif(this.value =5,'5',;
    iif(this.value =6,'6',;
    iif(this.value =7,'7',;
    iif(this.value =8,'8',;
    iif(this.value =9,'9',;
    iif(this.value =10,'10',;
    iif(this.value =11,'11',;
    iif(this.value =12,'12',;
    iif(this.value =13,'13',;
    iif(this.value =14,'14',;
    iif(this.value =15,'15',;
    '  '))))))))))))))))
  endfunc
  func oGTRAPCAR_2_16.GetRadio()
    this.Parent.oContained.w_GTRAPCAR = this.RadioValue()
    return .t.
  endfunc

  func oGTRAPCAR_2_16.SetRadio()
    this.Parent.oContained.w_GTRAPCAR=trim(this.Parent.oContained.w_GTRAPCAR)
    this.value = ;
      iif(this.Parent.oContained.w_GTRAPCAR=='1',1,;
      iif(this.Parent.oContained.w_GTRAPCAR=='2',2,;
      iif(this.Parent.oContained.w_GTRAPCAR=='3',3,;
      iif(this.Parent.oContained.w_GTRAPCAR=='4',4,;
      iif(this.Parent.oContained.w_GTRAPCAR=='5',5,;
      iif(this.Parent.oContained.w_GTRAPCAR=='6',6,;
      iif(this.Parent.oContained.w_GTRAPCAR=='7',7,;
      iif(this.Parent.oContained.w_GTRAPCAR=='8',8,;
      iif(this.Parent.oContained.w_GTRAPCAR=='9',9,;
      iif(this.Parent.oContained.w_GTRAPCAR=='10',10,;
      iif(this.Parent.oContained.w_GTRAPCAR=='11',11,;
      iif(this.Parent.oContained.w_GTRAPCAR=='12',12,;
      iif(this.Parent.oContained.w_GTRAPCAR=='13',13,;
      iif(this.Parent.oContained.w_GTRAPCAR=='14',14,;
      iif(this.Parent.oContained.w_GTRAPCAR=='15',15,;
      0)))))))))))))))
  endfunc

  func oGTRAPCAR_2_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GtRapFir='S')
    endwith
   endif
  endfunc

  add object oGTCODFED_2_17 as StdField with uid="CECQWLBBDY",rtseq=61,rtrep=.f.,;
    cFormVar = "w_GTCODFED", cQueryName = "GTCODFED",;
    bObbl = .f. , nPag = 2, value=space(11), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale societ� dichiarante",;
    HelpContextID = 133590358,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=541, Top=87, cSayPict='REPLICATE("!",11)', cGetPict='REPLICATE("!",11)', InputMask=replicate('X',11)

  func oGTCODFED_2_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GtRapFir='S')
    endwith
   endif
  endfunc

  func oGTCODFED_2_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (iif(Not Empty(.w_Gtcodfed),Chkcfp(.w_Gtcodfed,"PI", "", "", ""),.t.))
    endwith
    return bRes
  endfunc

  add object oGTRAPCOG_2_19 as StdField with uid="UJGVIFVFSY",rtseq=62,rtrep=.f.,;
    cFormVar = "w_GTRAPCOG", cQueryName = "GTRAPCOG",;
    bObbl = .t. , nPag = 2, value=space(24), bMultilanguage =  .f.,;
    ToolTipText = "Cognome del rappresentante",;
    HelpContextID = 96240301,;
   bGlobalFont=.t.,;
    Height=21, Width=181, Left=21, Top=135, cSayPict="REPL('!',24)", cGetPict="REPL('!',24)", InputMask=replicate('X',24)

  func oGTRAPCOG_2_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GtRapFir='S')
    endwith
   endif
  endfunc

  add object oGTRAPNOM_2_20 as StdField with uid="YTVQXAGYUL",rtseq=63,rtrep=.f.,;
    cFormVar = "w_GTRAPNOM", cQueryName = "GTRAPNOM",;
    bObbl = .t. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Nome del rappresentante",;
    HelpContextID = 12354227,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=238, Top=135, cSayPict="REPL('!',20)", cGetPict="REPL('!',20)", InputMask=replicate('X',20)

  func oGTRAPNOM_2_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GtRapFir='S')
    endwith
   endif
  endfunc


  add object oGTRAPSES_2_25 as StdCombo with uid="WGVHQBQHKK",rtseq=64,rtrep=.f.,left=541,top=134,width=91,height=22;
    , ToolTipText = "Sesso del rappresentante";
    , HelpContextID = 172195143;
    , cFormVar="w_GTRAPSES",RowSource=""+"Maschio,"+"Femmina", bObbl = .t. , nPag = 2;
  , bGlobalFont=.t.


  func oGTRAPSES_2_25.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oGTRAPSES_2_25.GetRadio()
    this.Parent.oContained.w_GTRAPSES = this.RadioValue()
    return .t.
  endfunc

  func oGTRAPSES_2_25.SetRadio()
    this.Parent.oContained.w_GTRAPSES=trim(this.Parent.oContained.w_GTRAPSES)
    this.value = ;
      iif(this.Parent.oContained.w_GTRAPSES=='M',1,;
      iif(this.Parent.oContained.w_GTRAPSES=='F',2,;
      0))
  endfunc

  func oGTRAPSES_2_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GtRapFir='S')
    endwith
   endif
  endfunc

  add object oGTRAPDNS_2_27 as StdField with uid="SVNJVGWJKE",rtseq=65,rtrep=.f.,;
    cFormVar = "w_GTRAPDNS", cQueryName = "GTRAPDNS",;
    bObbl = .t. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di nascita del rappresentante",;
    HelpContextID = 113017529,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=21, Top=190

  func oGTRAPDNS_2_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GtRapFir='S')
    endwith
   endif
  endfunc

  add object oGTRAPNAS_2_30 as StdField with uid="RMZNYMQUJB",rtseq=66,rtrep=.f.,;
    cFormVar = "w_GTRAPNAS", cQueryName = "GTRAPNAS",;
    bObbl = .t. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune o stato estero di nascita del rappresentante",;
    HelpContextID = 12354233,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=238, Top=192, cSayPict="REPL('!',40)", cGetPict="REPL('!',40)", InputMask=replicate('X',40), bHasZoom = .t. 

  func oGTRAPNAS_2_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GtRapFir='S')
    endwith
   endif
  endfunc

  proc oGTRAPNAS_2_30.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C","",".w_GTRAPNAS",".w_GTRAPPRO")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oGTRAPPRO_2_31 as StdField with uid="YKNWROHEVM",rtseq=67,rtrep=.f.,;
    cFormVar = "w_GTRAPPRO", cQueryName = "GTRAPPRO",;
    bObbl = .f. , nPag = 2, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Provincia di nascita del rappresentante",;
    HelpContextID = 45908661,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=541, Top=192, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2), bHasZoom = .t. 

  func oGTRAPPRO_2_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GtRapFir='S')
    endwith
   endif
  endfunc

  proc oGTRAPPRO_2_31.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"P","","",".w_GTRAPPRO")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oGTRAPEST_2_34 as StdField with uid="BVQWJWHYBT",rtseq=68,rtrep=.f.,;
    cFormVar = "w_GTRAPEST", cQueryName = "GTRAPEST",;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice stato estero",;
    HelpContextID = 129794746,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=21, Top=240, cSayPict='"999"', cGetPict='"999"', InputMask=replicate('X',3)

  func oGTRAPEST_2_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GtRapFir='S')
    endwith
   endif
  endfunc

  add object oGTRAPFED_2_36 as StdField with uid="DTCSRNNJAU",rtseq=69,rtrep=.f.,;
    cFormVar = "w_GTRAPFED", cQueryName = "GTRAPFED",;
    bObbl = .t. , nPag = 2, value=space(24), bMultilanguage =  .f.,;
    ToolTipText = "Stato federato provincia contea",;
    HelpContextID = 121863510,;
   bGlobalFont=.t.,;
    Height=21, Width=195, Left=238, Top=240, cSayPict='repl("!",24)', cGetPict='repl("!",24)', InputMask=replicate('X',24)

  func oGTRAPFED_2_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GtRapFir='S' And Not Empty(.w_Gtrapest))
    endwith
   endif
  endfunc

  add object oGTRAPRES_2_37 as StdField with uid="FSAOQSKNVN",rtseq=70,rtrep=.f.,;
    cFormVar = "w_GTRAPRES", cQueryName = "GTRAPRES",;
    bObbl = .f. , nPag = 2, value=space(24), bMultilanguage =  .f.,;
    ToolTipText = "Localit� di residenza",;
    HelpContextID = 188972359,;
   bGlobalFont=.t.,;
    Height=21, Width=181, Left=541, Top=240, cSayPict='repl("!",24)', cGetPict='repl("!",24)', InputMask=replicate('X',24)

  func oGTRAPRES_2_37.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GtRapFir='S' And Not Empty(.w_Gtrapest))
    endwith
   endif
  endfunc

  add object oGTRAPIND_2_39 as StdField with uid="FEUYQRBDFP",rtseq=71,rtrep=.f.,;
    cFormVar = "w_GTRAPIND", cQueryName = "GTRAPIND",;
    bObbl = .t. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo estero",;
    HelpContextID = 196903594,;
   bGlobalFont=.t.,;
    Height=21, Width=265, Left=21, Top=300, cSayPict='repl("!",35)', cGetPict='repl("!",35)', InputMask=replicate('X',35)

  func oGTRAPIND_2_39.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GtRapFir='S' And Not Empty(.w_Gtrapest))
    endwith
   endif
  endfunc

  add object oGTRAPTEL_2_41 as StdField with uid="NFHWSIZEWL",rtseq=72,rtrep=.f.,;
    cFormVar = "w_GTRAPTEL", cQueryName = "GTRAPTEL",;
    bObbl = .f. , nPag = 2, value=space(12), bMultilanguage =  .f.,;
    sErrorMsg = "Non inserire caratteri separatori tra prefisso e numero",;
    ToolTipText = "Telefono o cellulare del rappresentante",;
    HelpContextID = 155417934,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=541, Top=300, cSayPict='"999999999999"', cGetPict='"999999999999"', InputMask=replicate('X',12)

  func oGTRAPTEL_2_41.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GtRapFir='S')
    endwith
   endif
  endfunc

  func oGTRAPTEL_2_41.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (at(' ',alltrim(.w_Gtraptel))=0 and at('/',alltrim(.w_Gtraptel))=0 and at('-',alltrim(.w_Gtraptel))=0)
    endwith
    return bRes
  endfunc

  add object oGTRAPDCA_2_44 as StdField with uid="EGQRYEUZJV",rtseq=73,rtrep=.f.,;
    cFormVar = "w_GTRAPDCA", cQueryName = "GTRAPDCA",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data decorrenza carica del rappresentante",;
    HelpContextID = 113017511,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=21, Top=369

  func oGTRAPDCA_2_44.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GtRapFir='S')
    endwith
   endif
  endfunc

  add object oGTRAPFAL_2_45 as StdField with uid="HPGEGQQXRE",rtseq=74,rtrep=.f.,;
    cFormVar = "w_GTRAPFAL", cQueryName = "GTRAPFAL",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data apertura fallimento",;
    HelpContextID = 146571954,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=238, Top=369

  func oGTRAPFAL_2_45.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GtRapFir='S')
    endwith
   endif
  endfunc

  func oGTRAPFAL_2_45.mHide()
    with this.Parent.oContained
      return (! inlist(.w_Gtrapcar,'3','4'))
    endwith
  endfunc


  add object oGTNOTRAS_2_49 as StdCombo with uid="YFSAOYJDIN",rtseq=75,rtrep=.f.,left=311,top=447,width=480,height=22;
    , ToolTipText = "Casi di non trasmissione dei quadri ST,SV e SX";
    , HelpContextID = 84558521;
    , cFormVar="w_GTNOTRAS",RowSource=""+"0) Nessuno,"+"1) Amministrazioni dello Stato comprese quelle con ordinamento autonomo,"+"2) Sostituto che non abbia operato ritenute relative al periodo d'imposta 2016", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oGTNOTRAS_2_49.RadioValue()
    return(iif(this.value =1,'0',;
    iif(this.value =2,'1',;
    iif(this.value =3,'2',;
    space(1)))))
  endfunc
  func oGTNOTRAS_2_49.GetRadio()
    this.Parent.oContained.w_GTNOTRAS = this.RadioValue()
    return .t.
  endfunc

  func oGTNOTRAS_2_49.SetRadio()
    this.Parent.oContained.w_GTNOTRAS=trim(this.Parent.oContained.w_GTNOTRAS)
    this.value = ;
      iif(this.Parent.oContained.w_GTNOTRAS=='0',1,;
      iif(this.Parent.oContained.w_GTNOTRAS=='1',2,;
      iif(this.Parent.oContained.w_GTNOTRAS=='2',3,;
      0)))
  endfunc

  func oGTNOTRAS_2_49.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  func oGTNOTRAS_2_49.mHide()
    with this.Parent.oContained
      return (.w_GTTIP770<>'77S17')
    endwith
  endfunc

  add object oGTSEZ1ST_2_50 as StdCheck with uid="ENJNVLTXBM",rtseq=76,rtrep=.f.,left=176, top=479, caption="ST",;
    ToolTipText = "Casella prospetto ST",;
    HelpContextID = 73437882,;
    cFormVar="w_GTSEZ1ST", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oGTSEZ1ST_2_50.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oGTSEZ1ST_2_50.GetRadio()
    this.Parent.oContained.w_GTSEZ1ST = this.RadioValue()
    return .t.
  endfunc

  func oGTSEZ1ST_2_50.SetRadio()
    this.Parent.oContained.w_GTSEZ1ST=trim(this.Parent.oContained.w_GTSEZ1ST)
    this.value = ;
      iif(this.Parent.oContained.w_GTSEZ1ST=='1',1,;
      0)
  endfunc

  func oGTSEZ1ST_2_50.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load' And .w_GTNOTRAS='0')
    endwith
   endif
  endfunc

  func oGTSEZ1ST_2_50.mHide()
    with this.Parent.oContained
      return (.w_GTTIP770='77S18')
    endwith
  endfunc

  add object oGTSEZ1SV_2_51 as StdCheck with uid="QBFGUVQORE",rtseq=77,rtrep=.f.,left=231, top=479, caption="SV",;
    ToolTipText = "Casella prospetto SV",;
    HelpContextID = 73437884,;
    cFormVar="w_GTSEZ1SV", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oGTSEZ1SV_2_51.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oGTSEZ1SV_2_51.GetRadio()
    this.Parent.oContained.w_GTSEZ1SV = this.RadioValue()
    return .t.
  endfunc

  func oGTSEZ1SV_2_51.SetRadio()
    this.Parent.oContained.w_GTSEZ1SV=trim(this.Parent.oContained.w_GTSEZ1SV)
    this.value = ;
      iif(this.Parent.oContained.w_GTSEZ1SV=='1',1,;
      0)
  endfunc

  func oGTSEZ1SV_2_51.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GTNOTRAS='0')
    endwith
   endif
  endfunc

  func oGTSEZ1SV_2_51.mHide()
    with this.Parent.oContained
      return (.w_GTTIP770='77S18')
    endwith
  endfunc

  add object oGTSEZ1SX_2_52 as StdCheck with uid="YUTUHCKENQ",rtseq=78,rtrep=.f.,left=286, top=479, caption="SX",;
    ToolTipText = "Casella prospetto SX",;
    HelpContextID = 73437886,;
    cFormVar="w_GTSEZ1SX", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oGTSEZ1SX_2_52.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oGTSEZ1SX_2_52.GetRadio()
    this.Parent.oContained.w_GTSEZ1SX = this.RadioValue()
    return .t.
  endfunc

  func oGTSEZ1SX_2_52.SetRadio()
    this.Parent.oContained.w_GTSEZ1SX=trim(this.Parent.oContained.w_GTSEZ1SX)
    this.value = ;
      iif(this.Parent.oContained.w_GTSEZ1SX=='1',1,;
      0)
  endfunc

  func oGTSEZ1SX_2_52.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GTNOTRAS='0' Or .w_GTNOTRAS='2')
    endwith
   endif
  endfunc

  func oGTSEZ1SX_2_52.mHide()
    with this.Parent.oContained
      return (.w_GTTIP770='77S18')
    endwith
  endfunc

  add object oGTSEZ1SY_2_53 as StdCheck with uid="MZESQFEYWA",rtseq=79,rtrep=.f.,left=341, top=479, caption="SY",;
    ToolTipText = "Casella prospetto SY",;
    HelpContextID = 73437887,;
    cFormVar="w_GTSEZ1SY", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oGTSEZ1SY_2_53.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oGTSEZ1SY_2_53.GetRadio()
    this.Parent.oContained.w_GTSEZ1SY = this.RadioValue()
    return .t.
  endfunc

  func oGTSEZ1SY_2_53.SetRadio()
    this.Parent.oContained.w_GTSEZ1SY=trim(this.Parent.oContained.w_GTSEZ1SY)
    this.value = ;
      iif(this.Parent.oContained.w_GTSEZ1SY=='1',1,;
      0)
  endfunc

  func oGTSEZ1SY_2_53.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  func oGTSEZ1SY_2_53.mHide()
    with this.Parent.oContained
      return (.w_GTTIP770='77S18')
    endwith
  endfunc

  add object oGTSEZ1DI_2_54 as StdCheck with uid="RRPZLAGJJK",rtseq=80,rtrep=.f.,left=396, top=479, caption="DI",;
    ToolTipText = "Casella prospetto DI",;
    HelpContextID = 73437871,;
    cFormVar="w_GTSEZ1DI", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oGTSEZ1DI_2_54.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oGTSEZ1DI_2_54.GetRadio()
    this.Parent.oContained.w_GTSEZ1DI = this.RadioValue()
    return .t.
  endfunc

  func oGTSEZ1DI_2_54.SetRadio()
    this.Parent.oContained.w_GTSEZ1DI=trim(this.Parent.oContained.w_GTSEZ1DI)
    this.value = ;
      iif(this.Parent.oContained.w_GTSEZ1DI=='1',1,;
      0)
  endfunc

  func oGTSEZ1DI_2_54.mHide()
    with this.Parent.oContained
      return (.w_GTTIP770<>'77S17')
    endwith
  endfunc

  add object oGTLAVDIP_2_55 as StdCheck with uid="ZASCSPDFJZ",rtseq=81,rtrep=.f.,left=176, top=517, caption="Dipendente",;
    ToolTipText = "Casella relativa a certificazioni lavoro dipendente e assimilato",;
    HelpContextID = 149151050,;
    cFormVar="w_GTLAVDIP", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oGTLAVDIP_2_55.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oGTLAVDIP_2_55.GetRadio()
    this.Parent.oContained.w_GTLAVDIP = this.RadioValue()
    return .t.
  endfunc

  func oGTLAVDIP_2_55.SetRadio()
    this.Parent.oContained.w_GTLAVDIP=trim(this.Parent.oContained.w_GTLAVDIP)
    this.value = ;
      iif(this.Parent.oContained.w_GTLAVDIP=='1',1,;
      0)
  endfunc

  func oGTLAVDIP_2_55.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GTLAVAUT='0')
    endwith
   endif
  endfunc

  func oGTLAVDIP_2_55.mHide()
    with this.Parent.oContained
      return (.w_GTTIP770='77S18')
    endwith
  endfunc

  add object oGTLAVAUT_2_56 as StdCheck with uid="LLQESPIFTG",rtseq=82,rtrep=.f.,left=286, top=517, caption="Autonomo",;
    ToolTipText = "Casella relativa a certificazioni lavoro autonomo",;
    HelpContextID = 199482694,;
    cFormVar="w_GTLAVAUT", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oGTLAVAUT_2_56.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oGTLAVAUT_2_56.GetRadio()
    this.Parent.oContained.w_GTLAVAUT = this.RadioValue()
    return .t.
  endfunc

  func oGTLAVAUT_2_56.SetRadio()
    this.Parent.oContained.w_GTLAVAUT=trim(this.Parent.oContained.w_GTLAVAUT)
    this.value = ;
      iif(this.Parent.oContained.w_GTLAVAUT=='1',1,;
      0)
  endfunc

  func oGTLAVAUT_2_56.mHide()
    with this.Parent.oContained
      return (.w_GTTIP770='77S18')
    endwith
  endfunc

  add object oGTFISAGG_2_58 as StdField with uid="HNGWLCODFM",rtseq=83,rtrep=.f.,;
    cFormVar = "w_GTFISAGG", cQueryName = "GTFISAGG",;
    bObbl = .f. , nPag = 2, value=space(16), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale del soggetto che presenta la restante parte della dichiarazione",;
    HelpContextID = 202128723,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=643, Top=509, cSayPict='repl("!",16)', cGetPict='repl("!",16)', InputMask=replicate('X',16)

  func oGTFISAGG_2_58.mHide()
    with this.Parent.oContained
      return (.w_GTTIP770='77S17' OR .w_GTTIP770='77S18')
    endwith
  endfunc

  func oGTFISAGG_2_58.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (iif(Not Empty(.w_Gtfisagg),Chkcfp(alltrim(.w_GTFISAGG),'CF'),.t.)                 )
    endwith
    return bRes
  endfunc

  add object oStr_2_1 as StdString with uid="VUXSIFVRZX",Visible=.t., Left=6, Top=36,;
    Alignment=0, Width=392, Height=19,;
    Caption="Dati relativi al rappresentante firmatario della dichiarazione"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_15 as StdString with uid="CLTNFQGUIT",Visible=.t., Left=21, Top=69,;
    Alignment=0, Width=123, Height=18,;
    Caption="Codice fiscale"  ;
  , bGlobalFont=.t.

  add object oStr_2_18 as StdString with uid="HDLLWZBNWD",Visible=.t., Left=238, Top=69,;
    Alignment=0, Width=118, Height=18,;
    Caption="Codice carica"  ;
  , bGlobalFont=.t.

  add object oStr_2_21 as StdString with uid="TENSGSVNUQ",Visible=.t., Left=21, Top=118,;
    Alignment=0, Width=82, Height=18,;
    Caption="Cognome"  ;
  , bGlobalFont=.t.

  add object oStr_2_22 as StdString with uid="TIMIFEPNLS",Visible=.t., Left=238, Top=118,;
    Alignment=0, Width=82, Height=18,;
    Caption="Nome"  ;
  , bGlobalFont=.t.

  add object oStr_2_23 as StdString with uid="HFZNTWSUVL",Visible=.t., Left=541, Top=69,;
    Alignment=0, Width=243, Height=18,;
    Caption="Codice fiscale societ� o ente dichiarante"  ;
  , bGlobalFont=.t.

  add object oStr_2_26 as StdString with uid="IGPIMVYGMX",Visible=.t., Left=541, Top=118,;
    Alignment=0, Width=39, Height=18,;
    Caption="Sesso"  ;
  , bGlobalFont=.t.

  add object oStr_2_28 as StdString with uid="RLVHZOGGEF",Visible=.t., Left=21, Top=173,;
    Alignment=0, Width=114, Height=18,;
    Caption="Data di nascita"  ;
  , bGlobalFont=.t.

  add object oStr_2_29 as StdString with uid="AMZPLTPUDW",Visible=.t., Left=238, Top=173,;
    Alignment=0, Width=133, Height=18,;
    Caption="Comune di nascita"  ;
  , bGlobalFont=.t.

  add object oStr_2_32 as StdString with uid="HLKUJEIJUZ",Visible=.t., Left=541, Top=173,;
    Alignment=0, Width=70, Height=18,;
    Caption="Provincia"  ;
  , bGlobalFont=.t.

  add object oStr_2_33 as StdString with uid="DAOIGOLXJI",Visible=.t., Left=21, Top=223,;
    Alignment=0, Width=107, Height=18,;
    Caption="Codice stato estero"  ;
  , bGlobalFont=.t.

  add object oStr_2_35 as StdString with uid="AJQBKWBKQH",Visible=.t., Left=238, Top=223,;
    Alignment=0, Width=239, Height=18,;
    Caption="Stato federato, provincia, contea"  ;
  , bGlobalFont=.t.

  add object oStr_2_38 as StdString with uid="QHNWHRMIZF",Visible=.t., Left=541, Top=223,;
    Alignment=0, Width=220, Height=18,;
    Caption="Localit� di residenza"  ;
  , bGlobalFont=.t.

  add object oStr_2_40 as StdString with uid="RJNAUZVOAC",Visible=.t., Left=21, Top=285,;
    Alignment=0, Width=203, Height=18,;
    Caption="Indirizzo estero"  ;
  , bGlobalFont=.t.

  add object oStr_2_42 as StdString with uid="TUPHQFERJK",Visible=.t., Left=541, Top=285,;
    Alignment=0, Width=107, Height=18,;
    Caption="Telefono o cellulare"  ;
  , bGlobalFont=.t.

  add object oStr_2_43 as StdString with uid="HPKUODUFLC",Visible=.t., Left=21, Top=349,;
    Alignment=0, Width=126, Height=18,;
    Caption="Data decorrenza carica"  ;
  , bGlobalFont=.t.

  add object oStr_2_46 as StdString with uid="VBYXFKQFCT",Visible=.t., Left=238, Top=349,;
    Alignment=0, Width=132, Height=18,;
    Caption="Data apertura fallimento"  ;
  , bGlobalFont=.t.

  func oStr_2_46.mHide()
    with this.Parent.oContained
      return (! inlist(.w_GTRAPCAR,'3','4'))
    endwith
  endfunc

  add object oStr_2_48 as StdString with uid="ZTBAJRUHWU",Visible=.t., Left=11, Top=480,;
    Alignment=1, Width=159, Height=18,;
    Caption="Prospetti compilati:"  ;
  , bGlobalFont=.t.

  func oStr_2_48.mHide()
    with this.Parent.oContained
      return (.w_GTTIP770='77S17' OR .w_GTTIP770='77S18')
    endwith
  endfunc

  add object oStr_2_57 as StdString with uid="YDNRUVIFOR",Visible=.t., Left=377, Top=502,;
    Alignment=1, Width=260, Height=18,;
    Caption="Codice fiscale del soggetto che presenta"  ;
  , bGlobalFont=.t.

  func oStr_2_57.mHide()
    with this.Parent.oContained
      return (.w_GTTIP770='77S17' OR .w_GTTIP770='77S18')
    endwith
  endfunc

  add object oStr_2_59 as StdString with uid="RRHKBSDDUP",Visible=.t., Left=10, Top=414,;
    Alignment=0, Width=392, Height=19,;
    Caption="Redazione della dichiarazione"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_2_59.mHide()
    with this.Parent.oContained
      return (.w_GTTIP770='77S18')
    endwith
  endfunc

  add object oStr_2_61 as StdString with uid="KPXSCQBNRM",Visible=.t., Left=11, Top=518,;
    Alignment=1, Width=159, Height=18,;
    Caption="Gestione separata lavoro:"  ;
  , bGlobalFont=.t.

  func oStr_2_61.mHide()
    with this.Parent.oContained
      return (.w_GTTIP770='77S18')
    endwith
  endfunc

  add object oStr_2_62 as StdString with uid="BGHAOIVOVQ",Visible=.t., Left=434, Top=522,;
    Alignment=1, Width=203, Height=18,;
    Caption="la restante parte della dichiarazione:"  ;
  , bGlobalFont=.t.

  func oStr_2_62.mHide()
    with this.Parent.oContained
      return (.w_GTTIP770='77S17' OR .w_GTTIP770='77S18')
    endwith
  endfunc

  add object oStr_2_63 as StdString with uid="LWRWFNQZEU",Visible=.t., Left=21, Top=449,;
    Alignment=1, Width=286, Height=18,;
    Caption="Casi di non trasmissione dei prospetti ST,SV e/o SX:"  ;
  , bGlobalFont=.t.

  func oStr_2_63.mHide()
    with this.Parent.oContained
      return (.w_GTTIP770<>'77S17')
    endwith
  endfunc

  add object oStr_2_65 as StdString with uid="YBZLRQBZQT",Visible=.t., Left=11, Top=480,;
    Alignment=1, Width=159, Height=18,;
    Caption="Quadri compilati:"  ;
  , bGlobalFont=.t.

  func oStr_2_65.mHide()
    with this.Parent.oContained
      return (.w_GTTIP770<>'77S17')
    endwith
  endfunc

  add object oBox_2_24 as StdBox with uid="HFCOEWBAIZ",left=0, top=57, width=785,height=2

  add object oBox_2_60 as StdBox with uid="SKYDCOZIFG",left=4, top=435, width=785,height=2
enddefine
define class tgsri_agyPag3 as StdContainer
  Width  = 794
  height = 552
  stdWidth  = 794
  stdheight = 552
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oGTTIPINV_3_1 as StdCombo with uid="MRFFAVJUPF",rtseq=84,rtrep=.f.,left=210,top=25,width=222,height=21;
    , ToolTipText = "Identifica la tipolgia di invio dei dati relativi ai diversi redditi : pu� valere invio in unico flusso o invio separato dei dati";
    , HelpContextID = 197436092;
    , cFormVar="w_GTTIPINV",RowSource=""+"1 - Invio in un unico flusso,"+"2 - Invio separato", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oGTTIPINV_3_1.RadioValue()
    return(iif(this.value =1,"1",;
    iif(this.value =2,"2",;
    space(1))))
  endfunc
  func oGTTIPINV_3_1.GetRadio()
    this.Parent.oContained.w_GTTIPINV = this.RadioValue()
    return .t.
  endfunc

  func oGTTIPINV_3_1.SetRadio()
    this.Parent.oContained.w_GTTIPINV=trim(this.Parent.oContained.w_GTTIPINV)
    this.value = ;
      iif(this.Parent.oContained.w_GTTIPINV=="1",1,;
      iif(this.Parent.oContained.w_GTTIPINV=="2",2,;
      0))
  endfunc

  add object oGTRIOPDP_3_2 as StdCheck with uid="QIZKYVPQIP",rtseq=85,rtrep=.f.,left=296, top=113, caption="Dipendente",;
    ToolTipText = "Casella relativa a certificazioni lavoro dipendente e assimilato",;
    HelpContextID = 223051082,;
    cFormVar="w_GTRIOPDP", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oGTRIOPDP_3_2.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oGTRIOPDP_3_2.GetRadio()
    this.Parent.oContained.w_GTRIOPDP = this.RadioValue()
    return .t.
  endfunc

  func oGTRIOPDP_3_2.SetRadio()
    this.Parent.oContained.w_GTRIOPDP=trim(this.Parent.oContained.w_GTRIOPDP)
    this.value = ;
      iif(this.Parent.oContained.w_GTRIOPDP=='1',1,;
      0)
  endfunc

  add object oGTRIOPAU_3_3 as StdCheck with uid="LWLGEIRQTD",rtseq=86,rtrep=.f.,left=417, top=113, caption="Autonomo",;
    ToolTipText = "Casella relativa a certificazioni lavoro autonomo",;
    HelpContextID = 45384379,;
    cFormVar="w_GTRIOPAU", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oGTRIOPAU_3_3.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oGTRIOPAU_3_3.GetRadio()
    this.Parent.oContained.w_GTRIOPAU = this.RadioValue()
    return .t.
  endfunc

  func oGTRIOPAU_3_3.SetRadio()
    this.Parent.oContained.w_GTRIOPAU=trim(this.Parent.oContained.w_GTRIOPAU)
    this.value = ;
      iif(this.Parent.oContained.w_GTRIOPAU=='1',1,;
      0)
  endfunc


  add object oGTTRASNO_3_4 as StdCombo with uid="UUPFOYLZAH",rtseq=87,rtrep=.f.,left=294,top=171,width=480,height=21;
    , ToolTipText = "Casi di non trasmissione dei quadri ST,SV e SX";
    , HelpContextID = 81633973;
    , cFormVar="w_GTTRASNO",RowSource=""+"0) Nessuno,"+"1) Amministrazioni dello Stato comprese quelle con ordinamento autonomo,"+"2) Sostituto che non abbia operato ritenute relative al periodo d'imposta 2017", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oGTTRASNO_3_4.RadioValue()
    return(iif(this.value =1,'0',;
    iif(this.value =2,'1',;
    iif(this.value =3,'2',;
    space(1)))))
  endfunc
  func oGTTRASNO_3_4.GetRadio()
    this.Parent.oContained.w_GTTRASNO = this.RadioValue()
    return .t.
  endfunc

  func oGTTRASNO_3_4.SetRadio()
    this.Parent.oContained.w_GTTRASNO=trim(this.Parent.oContained.w_GTTRASNO)
    this.value = ;
      iif(this.Parent.oContained.w_GTTRASNO=='0',1,;
      iif(this.Parent.oContained.w_GTTRASNO=='1',2,;
      iif(this.Parent.oContained.w_GTTRASNO=='2',3,;
      0)))
  endfunc

  func oGTTRASNO_3_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  add object oGTRIOPSS_3_5 as StdCheck with uid="DOFIPZDPME",rtseq=88,rtrep=.f.,left=296, top=226, caption="SS",;
    ToolTipText = "Casella prospetto SS",;
    HelpContextID = 223051079,;
    cFormVar="w_GTRIOPSS", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oGTRIOPSS_3_5.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oGTRIOPSS_3_5.GetRadio()
    this.Parent.oContained.w_GTRIOPSS = this.RadioValue()
    return .t.
  endfunc

  func oGTRIOPSS_3_5.SetRadio()
    this.Parent.oContained.w_GTRIOPSS=trim(this.Parent.oContained.w_GTRIOPSS)
    this.value = ;
      iif(this.Parent.oContained.w_GTRIOPSS=='1',1,;
      0)
  endfunc

  add object oGTRIOPDI_3_6 as StdCheck with uid="DPOGICJKOU",rtseq=89,rtrep=.f.,left=347, top=226, caption="DI",;
    ToolTipText = "Casella prospetto DI",;
    HelpContextID = 223051089,;
    cFormVar="w_GTRIOPDI", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oGTRIOPDI_3_6.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oGTRIOPDI_3_6.GetRadio()
    this.Parent.oContained.w_GTRIOPDI = this.RadioValue()
    return .t.
  endfunc

  func oGTRIOPDI_3_6.SetRadio()
    this.Parent.oContained.w_GTRIOPDI=trim(this.Parent.oContained.w_GTRIOPDI)
    this.value = ;
      iif(this.Parent.oContained.w_GTRIOPDI=='1',1,;
      0)
  endfunc

  add object oGTRIOPST_3_7 as StdCheck with uid="ZSTJAYMUTW",rtseq=90,rtrep=.f.,left=398, top=226, caption="ST",;
    ToolTipText = "Casella prospetto ST",;
    HelpContextID = 223051078,;
    cFormVar="w_GTRIOPST", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oGTRIOPST_3_7.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oGTRIOPST_3_7.GetRadio()
    this.Parent.oContained.w_GTRIOPST = this.RadioValue()
    return .t.
  endfunc

  func oGTRIOPST_3_7.SetRadio()
    this.Parent.oContained.w_GTRIOPST=trim(this.Parent.oContained.w_GTRIOPST)
    this.value = ;
      iif(this.Parent.oContained.w_GTRIOPST=='1',1,;
      0)
  endfunc

  func oGTRIOPST_3_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load' And .w_GTTRASNO='0')
    endwith
   endif
  endfunc

  add object oGTRIOPSV_3_8 as StdCheck with uid="BCTRTUJBGI",rtseq=91,rtrep=.f.,left=449, top=226, caption="SV",;
    ToolTipText = "Casella prospetto SV",;
    HelpContextID = 223051076,;
    cFormVar="w_GTRIOPSV", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oGTRIOPSV_3_8.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oGTRIOPSV_3_8.GetRadio()
    this.Parent.oContained.w_GTRIOPSV = this.RadioValue()
    return .t.
  endfunc

  func oGTRIOPSV_3_8.SetRadio()
    this.Parent.oContained.w_GTRIOPSV=trim(this.Parent.oContained.w_GTRIOPSV)
    this.value = ;
      iif(this.Parent.oContained.w_GTRIOPSV=='1',1,;
      0)
  endfunc

  func oGTRIOPSV_3_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GTTRASNO='0')
    endwith
   endif
  endfunc

  add object oGTRIOPSX_3_9 as StdCheck with uid="DPCMBWYGIC",rtseq=92,rtrep=.f.,left=500, top=226, caption="SX",;
    ToolTipText = "Casella prospetto SX",;
    HelpContextID = 223051074,;
    cFormVar="w_GTRIOPSX", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oGTRIOPSX_3_9.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oGTRIOPSX_3_9.GetRadio()
    this.Parent.oContained.w_GTRIOPSX = this.RadioValue()
    return .t.
  endfunc

  func oGTRIOPSX_3_9.SetRadio()
    this.Parent.oContained.w_GTRIOPSX=trim(this.Parent.oContained.w_GTRIOPSX)
    this.value = ;
      iif(this.Parent.oContained.w_GTRIOPSX=='1',1,;
      0)
  endfunc

  func oGTRIOPSX_3_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GTTRASNO='0' Or .w_GTTRASNO='2')
    endwith
   endif
  endfunc

  add object oGTRIOPSY_3_10 as StdCheck with uid="QRAENGBTPG",rtseq=93,rtrep=.f.,left=551, top=226, caption="SY",;
    ToolTipText = "Casella prospetto SY",;
    HelpContextID = 223051073,;
    cFormVar="w_GTRIOPSY", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oGTRIOPSY_3_10.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oGTRIOPSY_3_10.GetRadio()
    this.Parent.oContained.w_GTRIOPSY = this.RadioValue()
    return .t.
  endfunc

  func oGTRIOPSY_3_10.SetRadio()
    this.Parent.oContained.w_GTRIOPSY=trim(this.Parent.oContained.w_GTRIOPSY)
    this.value = ;
      iif(this.Parent.oContained.w_GTRIOPSY=='1',1,;
      0)
  endfunc

  func oGTRIOPSY_3_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  add object oGTCFISAI_3_11 as StdField with uid="THPAECKFNY",rtseq=94,rtrep=.f.,;
    cFormVar = "w_GTCFISAI", cQueryName = "GTCFISAI",;
    bObbl = .t. , nPag = 3, value=space(16), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale altro incaricato",;
    HelpContextID = 89166511,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=12, Top=325, cSayPict='REPLICATE("!",16)', cGetPict='REPLICATE("!",16)', InputMask=replicate('X',16)

  func oGTCFISAI_3_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GTTIPINV="2" And (.w_GTRISEDI="1" Or .w_GTRISEAU="1" Or .w_GTRISECA="1" Or .w_GTRISELB="1" Or .w_GTRISEAR="1"))
    endwith
   endif
  endfunc

  func oGTCFISAI_3_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (iif(not empty(.w_Gtcfisai),chkcfp(alltrim(.w_Gtcfisai),'CF'),.T.))
    endwith
    return bRes
  endfunc

  add object oGTRISEDI_3_12 as StdCheck with uid="WKDGLDSJVM",rtseq=95,rtrep=.f.,left=161, top=324, caption="Dipendente",;
    ToolTipText = "Casella relativa a certificazioni lavoro dipendente e assimilato",;
    HelpContextID = 134970705,;
    cFormVar="w_GTRISEDI", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oGTRISEDI_3_12.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oGTRISEDI_3_12.GetRadio()
    this.Parent.oContained.w_GTRISEDI = this.RadioValue()
    return .t.
  endfunc

  func oGTRISEDI_3_12.SetRadio()
    this.Parent.oContained.w_GTRISEDI=trim(this.Parent.oContained.w_GTRISEDI)
    this.value = ;
      iif(this.Parent.oContained.w_GTRISEDI=='1',1,;
      0)
  endfunc

  func oGTRISEDI_3_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GTTIPINV="2")
    endwith
   endif
  endfunc

  add object oGTRISEAU_3_13 as StdCheck with uid="LXBHZNYYVI",rtseq=96,rtrep=.f.,left=289, top=324, caption="Autonomo",;
    ToolTipText = "Casella relativa a certificazioni lavoro autonomo",;
    HelpContextID = 133464763,;
    cFormVar="w_GTRISEAU", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oGTRISEAU_3_13.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oGTRISEAU_3_13.GetRadio()
    this.Parent.oContained.w_GTRISEAU = this.RadioValue()
    return .t.
  endfunc

  func oGTRISEAU_3_13.SetRadio()
    this.Parent.oContained.w_GTRISEAU=trim(this.Parent.oContained.w_GTRISEAU)
    this.value = ;
      iif(this.Parent.oContained.w_GTRISEAU=='1',1,;
      0)
  endfunc

  func oGTRISEAU_3_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GTTIPINV="2")
    endwith
   endif
  endfunc

  add object oGTRISECA_3_14 as StdCheck with uid="AVNTYHGMZS",rtseq=97,rtrep=.f.,left=417, top=324, caption="Capitali",;
    ToolTipText = "Casella relativa a redditi di capitale",;
    HelpContextID = 133464743,;
    cFormVar="w_GTRISECA", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oGTRISECA_3_14.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oGTRISECA_3_14.GetRadio()
    this.Parent.oContained.w_GTRISECA = this.RadioValue()
    return .t.
  endfunc

  func oGTRISECA_3_14.SetRadio()
    this.Parent.oContained.w_GTRISECA=trim(this.Parent.oContained.w_GTRISECA)
    this.value = ;
      iif(this.Parent.oContained.w_GTRISECA=='1',1,;
      0)
  endfunc

  func oGTRISECA_3_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GTTIPINV="2")
    endwith
   endif
  endfunc

  add object oGTRISELB_3_15 as StdCheck with uid="DBYYUFKROZ",rtseq=98,rtrep=.f.,left=545, top=324, caption="Locazioni brevi",;
    ToolTipText = "Casella relativa a locazioni brevi",;
    HelpContextID = 133464744,;
    cFormVar="w_GTRISELB", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oGTRISELB_3_15.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oGTRISELB_3_15.GetRadio()
    this.Parent.oContained.w_GTRISELB = this.RadioValue()
    return .t.
  endfunc

  func oGTRISELB_3_15.SetRadio()
    this.Parent.oContained.w_GTRISELB=trim(this.Parent.oContained.w_GTRISELB)
    this.value = ;
      iif(this.Parent.oContained.w_GTRISELB=='1',1,;
      0)
  endfunc

  func oGTRISELB_3_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GTTIPINV="2")
    endwith
   endif
  endfunc

  add object oGTRISEAR_3_16 as StdCheck with uid="GZGAKNIHFZ",rtseq=99,rtrep=.f.,left=673, top=324, caption="Altre ritenute",;
    ToolTipText = "Casella relativa ad altre ritenute",;
    HelpContextID = 133464760,;
    cFormVar="w_GTRISEAR", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oGTRISEAR_3_16.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oGTRISEAR_3_16.GetRadio()
    this.Parent.oContained.w_GTRISEAR = this.RadioValue()
    return .t.
  endfunc

  func oGTRISEAR_3_16.SetRadio()
    this.Parent.oContained.w_GTRISEAR=trim(this.Parent.oContained.w_GTRISEAR)
    this.value = ;
      iif(this.Parent.oContained.w_GTRISEAR=='1',1,;
      0)
  endfunc

  func oGTRISEAR_3_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GTTIPINV="2")
    endwith
   endif
  endfunc

  add object oGTAICFIS_3_17 as StdField with uid="NRUIJZAFZU",rtseq=100,rtrep=.f.,;
    cFormVar = "w_GTAICFIS", cQueryName = "GTAICFIS",;
    bObbl = .t. , nPag = 3, value=space(16), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale altro incaricato",;
    HelpContextID = 135040327,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=12, Top=410, cSayPict='REPLICATE("!",16)', cGetPict='REPLICATE("!",16)', InputMask=replicate('X',16)

  func oGTAICFIS_3_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GTTIPINV="2" And (.w_GTDIRISE="1" Or .w_GTAURISE="1" Or .w_GTCARISE="1" Or .w_GTLBRISE="1" Or .w_GTARRISE="1"))
    endwith
   endif
  endfunc

  func oGTAICFIS_3_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (iif(not empty(.w_Gtaicfis),chkcfp(alltrim(.w_Gtaicfis),'CF'),.T.) )
    endwith
    return bRes
  endfunc

  add object oGTDIRISE_3_18 as StdCheck with uid="ZFCBWGQIHC",rtseq=101,rtrep=.f.,left=161, top=409, caption="Dipendente",;
    ToolTipText = "Casella relativa a certificazioni lavoro dipendente e assimilato",;
    HelpContextID = 199467691,;
    cFormVar="w_GTDIRISE", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oGTDIRISE_3_18.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oGTDIRISE_3_18.GetRadio()
    this.Parent.oContained.w_GTDIRISE = this.RadioValue()
    return .t.
  endfunc

  func oGTDIRISE_3_18.SetRadio()
    this.Parent.oContained.w_GTDIRISE=trim(this.Parent.oContained.w_GTDIRISE)
    this.value = ;
      iif(this.Parent.oContained.w_GTDIRISE=='1',1,;
      0)
  endfunc

  func oGTDIRISE_3_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GTTIPINV="2")
    endwith
   endif
  endfunc

  add object oGTAURISE_3_19 as StdCheck with uid="GHIIBIOGCG",rtseq=102,rtrep=.f.,left=289, top=409, caption="Autonomo",;
    ToolTipText = "Casella relativa a certificazioni lavoro autonomo",;
    HelpContextID = 200241835,;
    cFormVar="w_GTAURISE", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oGTAURISE_3_19.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oGTAURISE_3_19.GetRadio()
    this.Parent.oContained.w_GTAURISE = this.RadioValue()
    return .t.
  endfunc

  func oGTAURISE_3_19.SetRadio()
    this.Parent.oContained.w_GTAURISE=trim(this.Parent.oContained.w_GTAURISE)
    this.value = ;
      iif(this.Parent.oContained.w_GTAURISE=='1',1,;
      0)
  endfunc

  func oGTAURISE_3_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GTTIPINV="2")
    endwith
   endif
  endfunc

  add object oGTCARISE_3_20 as StdCheck with uid="INRMVOTHEK",rtseq=103,rtrep=.f.,left=417, top=409, caption="Capitali",;
    ToolTipText = "Casella relativa a redditi di capitale",;
    HelpContextID = 198939307,;
    cFormVar="w_GTCARISE", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oGTCARISE_3_20.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oGTCARISE_3_20.GetRadio()
    this.Parent.oContained.w_GTCARISE = this.RadioValue()
    return .t.
  endfunc

  func oGTCARISE_3_20.SetRadio()
    this.Parent.oContained.w_GTCARISE=trim(this.Parent.oContained.w_GTCARISE)
    this.value = ;
      iif(this.Parent.oContained.w_GTCARISE=='1',1,;
      0)
  endfunc

  func oGTCARISE_3_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GTTIPINV="2")
    endwith
   endif
  endfunc

  add object oGTLBRISE_3_21 as StdCheck with uid="FBIFIPLFKM",rtseq=104,rtrep=.f.,left=545, top=409, caption="Locazioni brevi",;
    ToolTipText = "Casella relativa a locazioni brevi",;
    HelpContextID = 199041707,;
    cFormVar="w_GTLBRISE", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oGTLBRISE_3_21.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oGTLBRISE_3_21.GetRadio()
    this.Parent.oContained.w_GTLBRISE = this.RadioValue()
    return .t.
  endfunc

  func oGTLBRISE_3_21.SetRadio()
    this.Parent.oContained.w_GTLBRISE=trim(this.Parent.oContained.w_GTLBRISE)
    this.value = ;
      iif(this.Parent.oContained.w_GTLBRISE=='1',1,;
      0)
  endfunc

  func oGTLBRISE_3_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GTTIPINV="2")
    endwith
   endif
  endfunc

  add object oGTARRISE_3_22 as StdCheck with uid="YVPEMEIVIX",rtseq=105,rtrep=.f.,left=673, top=409, caption="Altre ritenute",;
    ToolTipText = "Casella relativa ad altre ritenute",;
    HelpContextID = 200045227,;
    cFormVar="w_GTARRISE", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oGTARRISE_3_22.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oGTARRISE_3_22.GetRadio()
    this.Parent.oContained.w_GTARRISE = this.RadioValue()
    return .t.
  endfunc

  func oGTARRISE_3_22.SetRadio()
    this.Parent.oContained.w_GTARRISE=trim(this.Parent.oContained.w_GTARRISE)
    this.value = ;
      iif(this.Parent.oContained.w_GTARRISE=='1',1,;
      0)
  endfunc

  func oGTARRISE_3_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GTTIPINV="2")
    endwith
   endif
  endfunc

  add object oStr_3_23 as StdString with uid="BEFOVDHFHC",Visible=.t., Left=4, Top=172,;
    Alignment=1, Width=286, Height=18,;
    Caption="Casi di non trasmissione dei prospetti ST,SV e/o SX:"  ;
  , bGlobalFont=.t.

  add object oStr_3_24 as StdString with uid="SKHECHYYMD",Visible=.t., Left=8, Top=28,;
    Alignment=1, Width=198, Height=18,;
    Caption="Tipologia invio:"  ;
  , bGlobalFont=.t.

  add object oStr_3_26 as StdString with uid="WEFUAXIDTZ",Visible=.t., Left=5, Top=70,;
    Alignment=0, Width=346, Height=18,;
    Caption="Quadri compilati e ritenute operate"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_28 as StdString with uid="SYBLIINQYB",Visible=.t., Left=5, Top=265,;
    Alignment=0, Width=159, Height=18,;
    Caption="Gestione separata"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_30 as StdString with uid="SMCVDEIDEM",Visible=.t., Left=12, Top=301,;
    Alignment=0, Width=234, Height=18,;
    Caption="Codice fiscale altro incaricato"  ;
  , bGlobalFont=.t.

  add object oStr_3_31 as StdString with uid="BSQYVOHUKR",Visible=.t., Left=12, Top=386,;
    Alignment=0, Width=234, Height=18,;
    Caption="Codice fiscale altro incaricato"  ;
  , bGlobalFont=.t.

  add object oStr_3_34 as StdString with uid="PXWKBQZXJZ",Visible=.t., Left=131, Top=226,;
    Alignment=1, Width=159, Height=18,;
    Caption="Quadri compilati:"  ;
  , bGlobalFont=.t.

  add object oStr_3_36 as StdString with uid="KXHTVTQCLN",Visible=.t., Left=131, Top=112,;
    Alignment=1, Width=159, Height=18,;
    Caption="Ritenute operate:"  ;
  , bGlobalFont=.t.

  add object oBox_3_27 as StdBox with uid="FRPNSHVOSI",left=5, top=88, width=776,height=2

  add object oBox_3_29 as StdBox with uid="WUZLLUAEBW",left=5, top=283, width=776,height=2
enddefine
define class tgsri_agyPag4 as StdContainer
  Width  = 794
  height = 552
  stdWidth  = 794
  stdheight = 552
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oGTFIRDIC_4_3 as StdCheck with uid="EOBSFOCMRE",rtseq=106,rtrep=.f.,left=16, top=48, caption="Firma dichiarante",;
    ToolTipText = "Firma del dichiarante",;
    HelpContextID = 152845655,;
    cFormVar="w_GTFIRDIC", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oGTFIRDIC_4_3.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oGTFIRDIC_4_3.GetRadio()
    this.Parent.oContained.w_GTFIRDIC = this.RadioValue()
    return .t.
  endfunc

  func oGTFIRDIC_4_3.SetRadio()
    this.Parent.oContained.w_GTFIRDIC=trim(this.Parent.oContained.w_GTFIRDIC)
    this.value = ;
      iif(this.Parent.oContained.w_GTFIRDIC=='1',1,;
      0)
  endfunc

  add object oGTSITPAR_4_5 as StdField with uid="RKITSSQAQN",rtseq=107,rtrep=.f.,;
    cFormVar = "w_GTSITPAR", cQueryName = "GTSITPAR",;
    bObbl = .f. , nPag = 4, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Codice situazioni particolari",;
    HelpContextID = 50631352,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=441, Top=45, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2)

  add object oGTATTEST_4_6 as StdCheck with uid="JMEMELJAWQ",rtseq=108,rtrep=.f.,left=16, top=114, caption="Attestazione",;
    ToolTipText = "Casella attestazione",;
    HelpContextID = 135164602,;
    cFormVar="w_GTATTEST", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oGTATTEST_4_6.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oGTATTEST_4_6.GetRadio()
    this.Parent.oContained.w_GTATTEST = this.RadioValue()
    return .t.
  endfunc

  func oGTATTEST_4_6.SetRadio()
    this.Parent.oContained.w_GTATTEST=trim(this.Parent.oContained.w_GTATTEST)
    this.value = ;
      iif(this.Parent.oContained.w_GTATTEST=='1',1,;
      0)
  endfunc

  func oGTATTEST_4_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GTSOGGE1 $ '1234' Or .w_GTSOGGE2 $ '1234' Or .w_GTSOGGE3 $ '1234' Or .w_GTSOGGE4 $ '1234' Or .w_GTSOGGE5 $ '1234')
    endwith
   endif
  endfunc


  add object oGTSOGGE1_4_10 as StdCombo with uid="EZVNKYSBJI",rtseq=109,rtrep=.f.,left=147,top=113,width=210,height=22;
    , HelpContextID = 113601897;
    , cFormVar="w_GTSOGGE1",RowSource=""+"1 - Revisore contabile,"+"2 - Responsabile della revisione,"+"3 - Societ� di revisione,"+"4 - Collegio sindacale,"+"0 - Nessuno", bObbl = .t. , nPag = 4;
  , bGlobalFont=.t.


  func oGTSOGGE1_4_10.RadioValue()
    return(iif(this.value =1,'1',;
    iif(this.value =2,'2',;
    iif(this.value =3,'3',;
    iif(this.value =4,'4',;
    iif(this.value =5,'0',;
    space(1)))))))
  endfunc
  func oGTSOGGE1_4_10.GetRadio()
    this.Parent.oContained.w_GTSOGGE1 = this.RadioValue()
    return .t.
  endfunc

  func oGTSOGGE1_4_10.SetRadio()
    this.Parent.oContained.w_GTSOGGE1=trim(this.Parent.oContained.w_GTSOGGE1)
    this.value = ;
      iif(this.Parent.oContained.w_GTSOGGE1=='1',1,;
      iif(this.Parent.oContained.w_GTSOGGE1=='2',2,;
      iif(this.Parent.oContained.w_GTSOGGE1=='3',3,;
      iif(this.Parent.oContained.w_GTSOGGE1=='4',4,;
      iif(this.Parent.oContained.w_GTSOGGE1=='0',5,;
      0)))))
  endfunc

  add object oGTFISSG1_4_11 as StdField with uid="ZZVZSLFPUK",rtseq=110,rtrep=.f.,;
    cFormVar = "w_GTFISSG1", cQueryName = "GTFISSG1",;
    bObbl = .t. , nPag = 4, value=space(16), bMultilanguage =  .f.,;
    HelpContextID = 168574313,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=413, Top=114, cSayPict='REPLICATE("!",16)', cGetPict='REPLICATE("!",16)', InputMask=replicate('X',16)

  func oGTFISSG1_4_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GTSOGGE1 $ '1234')
    endwith
   endif
  endfunc

  func oGTFISSG1_4_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (iif(not empty(.w_GTFISSG1),chkcfp(alltrim(.w_GTFISSG1),'CF'),.T.))
    endwith
    return bRes
  endfunc

  add object oGTFIRSG1_4_12 as StdCheck with uid="EGPSGBERXQ",rtseq=111,rtrep=.f.,left=640, top=114, caption="Firma",;
    HelpContextID = 169622889,;
    cFormVar="w_GTFIRSG1", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oGTFIRSG1_4_12.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oGTFIRSG1_4_12.GetRadio()
    this.Parent.oContained.w_GTFIRSG1 = this.RadioValue()
    return .t.
  endfunc

  func oGTFIRSG1_4_12.SetRadio()
    this.Parent.oContained.w_GTFIRSG1=trim(this.Parent.oContained.w_GTFIRSG1)
    this.value = ;
      iif(this.Parent.oContained.w_GTFIRSG1=='1',1,;
      0)
  endfunc

  func oGTFIRSG1_4_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GTSOGGE1='3')
    endwith
   endif
  endfunc


  add object oGTSOGGE2_4_13 as StdCombo with uid="CXHOHAEXWH",rtseq=112,rtrep=.f.,left=147,top=154,width=210,height=22;
    , HelpContextID = 113601896;
    , cFormVar="w_GTSOGGE2",RowSource=""+"1 - Revisore contabile,"+"2 - Responsabile della revisione,"+"3 - Societ� di revisione,"+"4 - Collegio sindacale,"+"0 - Nessuno", bObbl = .t. , nPag = 4;
  , bGlobalFont=.t.


  func oGTSOGGE2_4_13.RadioValue()
    return(iif(this.value =1,'1',;
    iif(this.value =2,'2',;
    iif(this.value =3,'3',;
    iif(this.value =4,'4',;
    iif(this.value =5,'0',;
    space(1)))))))
  endfunc
  func oGTSOGGE2_4_13.GetRadio()
    this.Parent.oContained.w_GTSOGGE2 = this.RadioValue()
    return .t.
  endfunc

  func oGTSOGGE2_4_13.SetRadio()
    this.Parent.oContained.w_GTSOGGE2=trim(this.Parent.oContained.w_GTSOGGE2)
    this.value = ;
      iif(this.Parent.oContained.w_GTSOGGE2=='1',1,;
      iif(this.Parent.oContained.w_GTSOGGE2=='2',2,;
      iif(this.Parent.oContained.w_GTSOGGE2=='3',3,;
      iif(this.Parent.oContained.w_GTSOGGE2=='4',4,;
      iif(this.Parent.oContained.w_GTSOGGE2=='0',5,;
      0)))))
  endfunc

  add object oGTFISSG2_4_14 as StdField with uid="NVJSSMQOBW",rtseq=113,rtrep=.f.,;
    cFormVar = "w_GTFISSG2", cQueryName = "GTFISSG2",;
    bObbl = .t. , nPag = 4, value=space(16), bMultilanguage =  .f.,;
    HelpContextID = 168574312,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=413, Top=155, cSayPict='REPLICATE("!",16)', cGetPict='REPLICATE("!",16)', InputMask=replicate('X',16)

  func oGTFISSG2_4_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GTSOGGE2 $ '1234')
    endwith
   endif
  endfunc

  func oGTFISSG2_4_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (iif(not empty(.w_GTFISSG2),chkcfp(alltrim(.w_GTFISSG2),'CF'),.T.))
    endwith
    return bRes
  endfunc

  add object oGTFIRSG2_4_15 as StdCheck with uid="ABVTIYZZZT",rtseq=114,rtrep=.f.,left=640, top=154, caption="Firma",;
    HelpContextID = 169622888,;
    cFormVar="w_GTFIRSG2", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oGTFIRSG2_4_15.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oGTFIRSG2_4_15.GetRadio()
    this.Parent.oContained.w_GTFIRSG2 = this.RadioValue()
    return .t.
  endfunc

  func oGTFIRSG2_4_15.SetRadio()
    this.Parent.oContained.w_GTFIRSG2=trim(this.Parent.oContained.w_GTFIRSG2)
    this.value = ;
      iif(this.Parent.oContained.w_GTFIRSG2=='1',1,;
      0)
  endfunc

  func oGTFIRSG2_4_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GTSOGGE2='3')
    endwith
   endif
  endfunc


  add object oGTSOGGE3_4_16 as StdCombo with uid="WLMSDPKMOZ",rtseq=115,rtrep=.f.,left=147,top=195,width=210,height=22;
    , HelpContextID = 113601895;
    , cFormVar="w_GTSOGGE3",RowSource=""+"1 - Revisore contabile,"+"2 - Responsabile della revisione,"+"3 - Societ� di revisione,"+"4 - Collegio sindacale,"+"0 - Nessuno", bObbl = .t. , nPag = 4;
  , bGlobalFont=.t.


  func oGTSOGGE3_4_16.RadioValue()
    return(iif(this.value =1,'1',;
    iif(this.value =2,'2',;
    iif(this.value =3,'3',;
    iif(this.value =4,'4',;
    iif(this.value =5,'0',;
    space(1)))))))
  endfunc
  func oGTSOGGE3_4_16.GetRadio()
    this.Parent.oContained.w_GTSOGGE3 = this.RadioValue()
    return .t.
  endfunc

  func oGTSOGGE3_4_16.SetRadio()
    this.Parent.oContained.w_GTSOGGE3=trim(this.Parent.oContained.w_GTSOGGE3)
    this.value = ;
      iif(this.Parent.oContained.w_GTSOGGE3=='1',1,;
      iif(this.Parent.oContained.w_GTSOGGE3=='2',2,;
      iif(this.Parent.oContained.w_GTSOGGE3=='3',3,;
      iif(this.Parent.oContained.w_GTSOGGE3=='4',4,;
      iif(this.Parent.oContained.w_GTSOGGE3=='0',5,;
      0)))))
  endfunc

  add object oGTFISSG3_4_17 as StdField with uid="ZJBUIBXSCC",rtseq=116,rtrep=.f.,;
    cFormVar = "w_GTFISSG3", cQueryName = "GTFISSG3",;
    bObbl = .t. , nPag = 4, value=space(16), bMultilanguage =  .f.,;
    HelpContextID = 168574311,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=413, Top=196, cSayPict='REPLICATE("!",16)', cGetPict='REPLICATE("!",16)', InputMask=replicate('X',16)

  func oGTFISSG3_4_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GTSOGGE3 $ '1234')
    endwith
   endif
  endfunc

  func oGTFISSG3_4_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (iif(not empty(.w_GTFISSG3),chkcfp(alltrim(.w_GTFISSG3),'CF'),.T.))
    endwith
    return bRes
  endfunc

  add object oGTFIRSG3_4_18 as StdCheck with uid="DLPNSEGGLM",rtseq=117,rtrep=.f.,left=640, top=196, caption="Firma",;
    HelpContextID = 169622887,;
    cFormVar="w_GTFIRSG3", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oGTFIRSG3_4_18.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oGTFIRSG3_4_18.GetRadio()
    this.Parent.oContained.w_GTFIRSG3 = this.RadioValue()
    return .t.
  endfunc

  func oGTFIRSG3_4_18.SetRadio()
    this.Parent.oContained.w_GTFIRSG3=trim(this.Parent.oContained.w_GTFIRSG3)
    this.value = ;
      iif(this.Parent.oContained.w_GTFIRSG3=='1',1,;
      0)
  endfunc

  func oGTFIRSG3_4_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GTSOGGE3='3')
    endwith
   endif
  endfunc


  add object oGTSOGGE4_4_19 as StdCombo with uid="TGIOCEALEG",rtseq=118,rtrep=.f.,left=147,top=236,width=210,height=22;
    , HelpContextID = 113601894;
    , cFormVar="w_GTSOGGE4",RowSource=""+"1 - Revisore contabile,"+"2 - Responsabile della revisione,"+"3 - Societ� di revisione,"+"4 - Collegio sindacale,"+"0 - Nessuno", bObbl = .t. , nPag = 4;
  , bGlobalFont=.t.


  func oGTSOGGE4_4_19.RadioValue()
    return(iif(this.value =1,'1',;
    iif(this.value =2,'2',;
    iif(this.value =3,'3',;
    iif(this.value =4,'4',;
    iif(this.value =5,'0',;
    space(1)))))))
  endfunc
  func oGTSOGGE4_4_19.GetRadio()
    this.Parent.oContained.w_GTSOGGE4 = this.RadioValue()
    return .t.
  endfunc

  func oGTSOGGE4_4_19.SetRadio()
    this.Parent.oContained.w_GTSOGGE4=trim(this.Parent.oContained.w_GTSOGGE4)
    this.value = ;
      iif(this.Parent.oContained.w_GTSOGGE4=='1',1,;
      iif(this.Parent.oContained.w_GTSOGGE4=='2',2,;
      iif(this.Parent.oContained.w_GTSOGGE4=='3',3,;
      iif(this.Parent.oContained.w_GTSOGGE4=='4',4,;
      iif(this.Parent.oContained.w_GTSOGGE4=='0',5,;
      0)))))
  endfunc

  add object oGTFISSG4_4_20 as StdField with uid="THORJSFNWM",rtseq=119,rtrep=.f.,;
    cFormVar = "w_GTFISSG4", cQueryName = "GTFISSG4",;
    bObbl = .t. , nPag = 4, value=space(16), bMultilanguage =  .f.,;
    HelpContextID = 168574310,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=413, Top=237, cSayPict='REPLICATE("!",16)', cGetPict='REPLICATE("!",16)', InputMask=replicate('X',16)

  func oGTFISSG4_4_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GTSOGGE4 $ '1234')
    endwith
   endif
  endfunc

  func oGTFISSG4_4_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (iif(not empty(.w_GTFISSG4),chkcfp(alltrim(.w_GTFISSG4),'CF'),.T.))
    endwith
    return bRes
  endfunc

  add object oGTFIRSG4_4_21 as StdCheck with uid="DMLLALZCVQ",rtseq=120,rtrep=.f.,left=640, top=235, caption="Firma",;
    HelpContextID = 169622886,;
    cFormVar="w_GTFIRSG4", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oGTFIRSG4_4_21.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oGTFIRSG4_4_21.GetRadio()
    this.Parent.oContained.w_GTFIRSG4 = this.RadioValue()
    return .t.
  endfunc

  func oGTFIRSG4_4_21.SetRadio()
    this.Parent.oContained.w_GTFIRSG4=trim(this.Parent.oContained.w_GTFIRSG4)
    this.value = ;
      iif(this.Parent.oContained.w_GTFIRSG4=='1',1,;
      0)
  endfunc

  func oGTFIRSG4_4_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GTSOGGE4='3')
    endwith
   endif
  endfunc


  add object oGTSOGGE5_4_22 as StdCombo with uid="MCZSUTOIKT",rtseq=121,rtrep=.f.,left=147,top=277,width=210,height=22;
    , HelpContextID = 113601893;
    , cFormVar="w_GTSOGGE5",RowSource=""+"1 - Revisore contabile,"+"2 - Responsabile della revisione,"+"3 - Societ� di revisione,"+"4 - Collegio sindacale,"+"0 - Nessuno", bObbl = .t. , nPag = 4;
  , bGlobalFont=.t.


  func oGTSOGGE5_4_22.RadioValue()
    return(iif(this.value =1,'1',;
    iif(this.value =2,'2',;
    iif(this.value =3,'3',;
    iif(this.value =4,'4',;
    iif(this.value =5,'0',;
    space(1)))))))
  endfunc
  func oGTSOGGE5_4_22.GetRadio()
    this.Parent.oContained.w_GTSOGGE5 = this.RadioValue()
    return .t.
  endfunc

  func oGTSOGGE5_4_22.SetRadio()
    this.Parent.oContained.w_GTSOGGE5=trim(this.Parent.oContained.w_GTSOGGE5)
    this.value = ;
      iif(this.Parent.oContained.w_GTSOGGE5=='1',1,;
      iif(this.Parent.oContained.w_GTSOGGE5=='2',2,;
      iif(this.Parent.oContained.w_GTSOGGE5=='3',3,;
      iif(this.Parent.oContained.w_GTSOGGE5=='4',4,;
      iif(this.Parent.oContained.w_GTSOGGE5=='0',5,;
      0)))))
  endfunc

  add object oGTFISSG5_4_23 as StdField with uid="OELDBGBAEM",rtseq=122,rtrep=.f.,;
    cFormVar = "w_GTFISSG5", cQueryName = "GTFISSG5",;
    bObbl = .t. , nPag = 4, value=space(16), bMultilanguage =  .f.,;
    HelpContextID = 168574309,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=413, Top=278, cSayPict='REPLICATE("!",16)', cGetPict='REPLICATE("!",16)', InputMask=replicate('X',16)

  func oGTFISSG5_4_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GTSOGGE5 $ '1234')
    endwith
   endif
  endfunc

  func oGTFISSG5_4_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (iif(not empty(.w_GTFISSG5),chkcfp(alltrim(.w_GTFISSG5),'CF'),.T.))
    endwith
    return bRes
  endfunc

  add object oGTFIRSG5_4_24 as StdCheck with uid="WCLBEQIYZH",rtseq=123,rtrep=.f.,left=640, top=277, caption="Firma",;
    HelpContextID = 169622885,;
    cFormVar="w_GTFIRSG5", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oGTFIRSG5_4_24.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oGTFIRSG5_4_24.GetRadio()
    this.Parent.oContained.w_GTFIRSG5 = this.RadioValue()
    return .t.
  endfunc

  func oGTFIRSG5_4_24.SetRadio()
    this.Parent.oContained.w_GTFIRSG5=trim(this.Parent.oContained.w_GTFIRSG5)
    this.value = ;
      iif(this.Parent.oContained.w_GTFIRSG5=='1',1,;
      0)
  endfunc

  func oGTFIRSG5_4_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GTSOGGE5='3')
    endwith
   endif
  endfunc


  add object oGTIMPTRA_4_25 as StdCombo with uid="JKQBQAAXZG",rtseq=124,rtrep=.f.,left=7,top=370,width=279,height=22;
    , ToolTipText = "Impegno trasmissione telematica della dichiarazione";
    , HelpContextID = 113767079;
    , cFormVar="w_GTIMPTRA",RowSource=""+"1 - Dichiarazione predisposta dal contribuente,"+"2 - Dichiarazione predisposta da chi effettua l'invio,"+"0 - Nessuno", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oGTIMPTRA_4_25.RadioValue()
    return(iif(this.value =1,'1',;
    iif(this.value =2,'2',;
    iif(this.value =3,'0',;
    space(1)))))
  endfunc
  func oGTIMPTRA_4_25.GetRadio()
    this.Parent.oContained.w_GTIMPTRA = this.RadioValue()
    return .t.
  endfunc

  func oGTIMPTRA_4_25.SetRadio()
    this.Parent.oContained.w_GTIMPTRA=trim(this.Parent.oContained.w_GTIMPTRA)
    this.value = ;
      iif(this.Parent.oContained.w_GTIMPTRA=='1',1,;
      iif(this.Parent.oContained.w_GTIMPTRA=='2',2,;
      iif(this.Parent.oContained.w_GTIMPTRA=='0',3,;
      0)))
  endfunc

  add object oGTINVTEL_4_26 as StdCheck with uid="AOYHLBHMXO",rtseq=125,rtrep=.f.,left=303, top=370, caption="Invio avviso telematico all'intermediario",;
    ToolTipText = "Se attivo, il contribuente invia richiesta all'intermediario di verificare i controlli effettuati sulla dichiarazione",;
    HelpContextID = 148311374,;
    cFormVar="w_GTINVTEL", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oGTINVTEL_4_26.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oGTINVTEL_4_26.GetRadio()
    this.Parent.oContained.w_GTINVTEL = this.RadioValue()
    return .t.
  endfunc

  func oGTINVTEL_4_26.SetRadio()
    this.Parent.oContained.w_GTINVTEL=trim(this.Parent.oContained.w_GTINVTEL)
    this.value = ;
      iif(this.Parent.oContained.w_GTINVTEL=='1',1,;
      0)
  endfunc

  func oGTINVTEL_4_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GTIMPTRA <> '0')
    endwith
   endif
  endfunc

  add object oGTRICAVV_4_27 as StdCheck with uid="HLGRYECEFC",rtseq=126,rtrep=.f.,left=571, top=370, caption="Ricezione avviso telematico",;
    ToolTipText = "Se attivo, l'intermediario accetta di effettuare verifiche per conto del contribuente sulla sua dichiarazione",;
    HelpContextID = 218856772,;
    cFormVar="w_GTRICAVV", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oGTRICAVV_4_27.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oGTRICAVV_4_27.GetRadio()
    this.Parent.oContained.w_GTRICAVV = this.RadioValue()
    return .t.
  endfunc

  func oGTRICAVV_4_27.SetRadio()
    this.Parent.oContained.w_GTRICAVV=trim(this.Parent.oContained.w_GTRICAVV)
    this.value = ;
      iif(this.Parent.oContained.w_GTRICAVV=='1',1,;
      0)
  endfunc

  func oGTRICAVV_4_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GTINVTEL='1')
    endwith
   endif
  endfunc

  add object oGTCODINT_4_30 as StdField with uid="VHBKITNWRO",rtseq=127,rtrep=.f.,;
    cFormVar = "w_GTCODINT", cQueryName = "GTCODINT",;
    bObbl = .f. , nPag = 4, value=space(16), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale dell'incaricato che effettua la trasmissione",;
    HelpContextID = 185176762,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=7, Top=438, cSayPict='REPLICATE("!",16)', cGetPict='REPLICATE("!",16)', InputMask=replicate('X',16)

  func oGTCODINT_4_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GTIMPTRA $ '12')
    endwith
   endif
  endfunc

  func oGTCODINT_4_30.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (iif(not empty(.w_Gtcodint),chkcfp(alltrim(.w_Gtcodint),'CF'),.T.))
    endwith
    return bRes
  endfunc

  add object oGTNUMCAF_4_34 as StdField with uid="NUMNIWMTQP",rtseq=128,rtrep=.f.,;
    cFormVar = "w_GTNUMCAF", cQueryName = "GTNUMCAF",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 94388908,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=303, Top=438, cSayPict='"99999"', cGetPict='"99999"'

  func oGTNUMCAF_4_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GTIMPTRA $ '12')
    endwith
   endif
  endfunc

  add object oGTDATIMP_4_36 as StdField with uid="SYRFGCWSSA",rtseq=129,rtrep=.f.,;
    cFormVar = "w_GTDATIMP", cQueryName = "GTDATIMP",;
    bObbl = .f. , nPag = 4, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data impegno trasmissione fornitura",;
    HelpContextID = 201040566,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=8, Top=498

  func oGTDATIMP_4_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GTIMPTRA $ '12')
    endwith
   endif
  endfunc

  add object oGTFIRINT_4_37 as StdCheck with uid="ILGGZKTKSB",rtseq=130,rtrep=.f.,left=303, top=502, caption="Firma dell'incaricato",;
    ToolTipText = "Firma dell'incaricato",;
    HelpContextID = 199475898,;
    cFormVar="w_GTFIRINT", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oGTFIRINT_4_37.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oGTFIRINT_4_37.GetRadio()
    this.Parent.oContained.w_GTFIRINT = this.RadioValue()
    return .t.
  endfunc

  func oGTFIRINT_4_37.SetRadio()
    this.Parent.oContained.w_GTFIRINT=trim(this.Parent.oContained.w_GTFIRINT)
    this.value = ;
      iif(this.Parent.oContained.w_GTFIRINT=='1',1,;
      0)
  endfunc

  func oGTFIRINT_4_37.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GTIMPTRA $ '12')
    endwith
   endif
  endfunc

  add object oStr_4_1 as StdString with uid="JKCJHHVZLW",Visible=.t., Left=8, Top=21,;
    Alignment=0, Width=217, Height=18,;
    Caption="Firma della dichiarazione"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_4 as StdString with uid="TUFZQEUEOX",Visible=.t., Left=303, Top=46,;
    Alignment=1, Width=133, Height=18,;
    Caption="Situazioni particolari:"  ;
  , bGlobalFont=.t.

  add object oStr_4_8 as StdString with uid="UDJGLIEBEB",Visible=.t., Left=147, Top=86,;
    Alignment=0, Width=49, Height=18,;
    Caption="Soggetto"  ;
  , bGlobalFont=.t.

  add object oStr_4_9 as StdString with uid="AOSSHYSLXN",Visible=.t., Left=413, Top=87,;
    Alignment=0, Width=78, Height=18,;
    Caption="Codice fiscale"  ;
  , bGlobalFont=.t.

  add object oStr_4_28 as StdString with uid="ILQZWUDAJD",Visible=.t., Left=8, Top=317,;
    Alignment=0, Width=219, Height=18,;
    Caption="Impegno alla presentazione telematica"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_31 as StdString with uid="NECDVAUAQY",Visible=.t., Left=7, Top=419,;
    Alignment=0, Width=220, Height=18,;
    Caption="Codice fiscale dell'incaricato"  ;
  , bGlobalFont=.t.

  add object oStr_4_32 as StdString with uid="AOYTOWESTU",Visible=.t., Left=10, Top=479,;
    Alignment=0, Width=176, Height=18,;
    Caption="Data dell'impegno a trasmettere"  ;
  , bGlobalFont=.t.

  add object oStr_4_33 as StdString with uid="RCGCOENWMJ",Visible=.t., Left=7, Top=352,;
    Alignment=0, Width=219, Height=18,;
    Caption="Impegno a trasmettere in via telematica"  ;
  , bGlobalFont=.t.

  add object oStr_4_35 as StdString with uid="AMDHOXDZUI",Visible=.t., Left=303, Top=419,;
    Alignment=0, Width=209, Height=18,;
    Caption="Numero di iscrizione all'albo del C.A.F."  ;
  , bGlobalFont=.t.

  add object oBox_4_2 as StdBox with uid="UZBSRPWUBL",left=2, top=38, width=776,height=2

  add object oBox_4_7 as StdBox with uid="UIDVIEUFYO",left=2, top=104, width=776,height=2

  add object oBox_4_29 as StdBox with uid="PCRFHLYBEZ",left=2, top=337, width=776,height=2
enddefine
define class tgsri_agyPag5 as StdContainer
  Width  = 794
  height = 552
  stdWidth  = 794
  stdheight = 552
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oGTTIPFOR_5_2 as StdCombo with uid="TMIIFPZQPN",rtseq=131,rtrep=.f.,left=113,top=21,width=279,height=22;
    , ToolTipText = "Tipo fornitore";
    , HelpContextID = 147104440;
    , cFormVar="w_GTTIPFOR",RowSource=""+"01: Soggetti che inviano le dichiarazioni,"+"06: Amministrazione dello stato,"+"10: C.A.F. dip. e pens C.A.F. imp art3 C 2 altri int.", bObbl = .f. , nPag = 5;
    , sErrorMsg = "Selezione non consentita con tipo fornitore persona fisica";
  , bGlobalFont=.t.


  func oGTTIPFOR_5_2.RadioValue()
    return(iif(this.value =1,'01',;
    iif(this.value =2,'06',;
    iif(this.value =3,'10',;
    space(2)))))
  endfunc
  func oGTTIPFOR_5_2.GetRadio()
    this.Parent.oContained.w_GTTIPFOR = this.RadioValue()
    return .t.
  endfunc

  func oGTTIPFOR_5_2.SetRadio()
    this.Parent.oContained.w_GTTIPFOR=trim(this.Parent.oContained.w_GTTIPFOR)
    this.value = ;
      iif(this.Parent.oContained.w_GTTIPFOR=='01',1,;
      iif(this.Parent.oContained.w_GTTIPFOR=='06',2,;
      iif(this.Parent.oContained.w_GTTIPFOR=='10',3,;
      0)))
  endfunc

  func oGTTIPFOR_5_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_Azperazi='S' and  .w_Gttipfor<>'06' ) or .w_Azperazi<>'S')
    endwith
    return bRes
  endfunc

  add object oGTFLGCON_5_3 as StdCheck with uid="LXMPGHQANW",rtseq=132,rtrep=.f.,left=121, top=85, caption="Conferma",;
    ToolTipText = "Se attivo, la generazione � confermata",;
    HelpContextID = 87474868,;
    cFormVar="w_GTFLGCON", bObbl = .f. , nPag = 5;
    , Tabstop=.F.;
   , bGlobalFont=.t.


  func oGTFLGCON_5_3.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oGTFLGCON_5_3.GetRadio()
    this.Parent.oContained.w_GTFLGCON = this.RadioValue()
    return .t.
  endfunc

  func oGTFLGCON_5_3.SetRadio()
    this.Parent.oContained.w_GTFLGCON=trim(this.Parent.oContained.w_GTFLGCON)
    this.value = ;
      iif(this.Parent.oContained.w_GTFLGCON=='1',1,;
      0)
  endfunc

  add object oGTFLGCOR_5_4 as StdCheck with uid="PIBWDELKIT",rtseq=133,rtrep=.f.,left=243, top=85, caption="Correttiva nei termini",;
    ToolTipText = "Se attivo, la dichiarazione � correttiva nei termini",;
    HelpContextID = 87474872,;
    cFormVar="w_GTFLGCOR", bObbl = .f. , nPag = 5;
    , Tabstop=.F.;
   , bGlobalFont=.t.


  func oGTFLGCOR_5_4.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oGTFLGCOR_5_4.GetRadio()
    this.Parent.oContained.w_GTFLGCOR = this.RadioValue()
    return .t.
  endfunc

  func oGTFLGCOR_5_4.SetRadio()
    this.Parent.oContained.w_GTFLGCOR=trim(this.Parent.oContained.w_GTFLGCOR)
    this.value = ;
      iif(this.Parent.oContained.w_GTFLGCOR=='1',1,;
      0)
  endfunc

  func oGTFLGCOR_5_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  add object oGTFLGINT_5_5 as StdCheck with uid="PKZKMPXQFQ",rtseq=134,rtrep=.f.,left=424, top=85, caption="Dichiarazione integrativa",;
    ToolTipText = "Se attivo, la dichiarazione � integrativa nei termini",;
    HelpContextID = 188138170,;
    cFormVar="w_GTFLGINT", bObbl = .f. , nPag = 5;
    , Tabstop=.F.;
   , bGlobalFont=.t.


  func oGTFLGINT_5_5.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oGTFLGINT_5_5.GetRadio()
    this.Parent.oContained.w_GTFLGINT = this.RadioValue()
    return .t.
  endfunc

  func oGTFLGINT_5_5.SetRadio()
    this.Parent.oContained.w_GTFLGINT=trim(this.Parent.oContained.w_GTFLGINT)
    this.value = ;
      iif(this.Parent.oContained.w_GTFLGINT=='1',1,;
      0)
  endfunc

  func oGTFLGINT_5_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc


  add object oEVEECC_5_6 as StdCombo with uid="TZQYETJOUI",rtseq=135,rtrep=.f.,left=125,top=118,width=649,height=22;
    , ToolTipText = "Eventi eccezionali";
    , HelpContextID = 82818118;
    , cFormVar="w_EVEECC",RowSource=""+"0 - Nessuno,"+"1 - Contribuenti vittime di richieste estorsive ex. Art.20 Co.2 legge n. 44/99,"+"2 - Contribuenti residenti dal 4 al 7 febbraio 2015 nei territori della regione Emilia Romagna colpiti da eventi atmosferici,"+"3 - Contribuenti residenti al 12/2/2011 nel comune di Lampedusa e Linosa (migranti Nord Africa),"+"4 - Contribuenti colpiti da altri eventi eccezionali,"+"30 - Soggetti aventi  la sede legale/operativa in zone colpite da eventi calamitosi", bObbl = .f. , nPag = 5;
  , bGlobalFont=.t.


  func oEVEECC_5_6.RadioValue()
    return(iif(this.value =1,'00',;
    iif(this.value =2,'01',;
    iif(this.value =3,'02',;
    iif(this.value =4,'03',;
    iif(this.value =5,'04',;
    iif(this.value =6,'30',;
    space(2))))))))
  endfunc
  func oEVEECC_5_6.GetRadio()
    this.Parent.oContained.w_EVEECC = this.RadioValue()
    return .t.
  endfunc

  func oEVEECC_5_6.SetRadio()
    this.Parent.oContained.w_EVEECC=trim(this.Parent.oContained.w_EVEECC)
    this.value = ;
      iif(this.Parent.oContained.w_EVEECC=='00',1,;
      iif(this.Parent.oContained.w_EVEECC=='01',2,;
      iif(this.Parent.oContained.w_EVEECC=='02',3,;
      iif(this.Parent.oContained.w_EVEECC=='03',4,;
      iif(this.Parent.oContained.w_EVEECC=='04',5,;
      iif(this.Parent.oContained.w_EVEECC=='30',6,;
      0))))))
  endfunc

  func oEVEECC_5_6.mHide()
    with this.Parent.oContained
      return (.w_GTTIP770<>'77S16')
    endwith
  endfunc


  add object oEVEECC_2016_5_7 as StdCombo with uid="IABDVPOJIJ",rtseq=136,rtrep=.f.,left=125,top=118,width=649,height=22;
    , ToolTipText = "Eventi eccezionali";
    , HelpContextID = 83052664;
    , cFormVar="w_EVEECC_2016",RowSource=""+"0 - Nessuno,"+"1 - Contribuenti vittime di richieste estorsive ex. Art.20 Co.2 legge n. 44/99,"+"3 - Contribuenti residenti al 12/2/2011 nel comune di Lampedusa e Linosa (migranti Nord Africa),"+"6 - Contribuenti colpiti da altri eventi eccezionali", bObbl = .f. , nPag = 5;
  , bGlobalFont=.t.


  func oEVEECC_2016_5_7.RadioValue()
    return(iif(this.value =1,'00',;
    iif(this.value =2,'01',;
    iif(this.value =3,'03',;
    iif(this.value =4,'06',;
    space(2))))))
  endfunc
  func oEVEECC_2016_5_7.GetRadio()
    this.Parent.oContained.w_EVEECC_2016 = this.RadioValue()
    return .t.
  endfunc

  func oEVEECC_2016_5_7.SetRadio()
    this.Parent.oContained.w_EVEECC_2016=trim(this.Parent.oContained.w_EVEECC_2016)
    this.value = ;
      iif(this.Parent.oContained.w_EVEECC_2016=='00',1,;
      iif(this.Parent.oContained.w_EVEECC_2016=='01',2,;
      iif(this.Parent.oContained.w_EVEECC_2016=='03',3,;
      iif(this.Parent.oContained.w_EVEECC_2016=='06',4,;
      0))))
  endfunc

  func oEVEECC_2016_5_7.mHide()
    with this.Parent.oContained
      return (.w_GTTIP770<>'77S17')
    endwith
  endfunc


  add object oEVEECC_2017_5_8 as StdCombo with uid="OQANLXEFMW",rtseq=137,rtrep=.f.,left=125,top=118,width=649,height=22;
    , ToolTipText = "Eventi eccezionali";
    , HelpContextID = 83056760;
    , cFormVar="w_EVEECC_2017",RowSource=""+"0 - Nessuno,"+"1 - Contribuenti vittime di richieste estorsive ex. Art.20 Co.2 legge n. 44/99,"+"3 - Contribuenti residenti al 12/2/2011 nel comune di Lampedusa e Linosa (migranti Nord Africa),"+"8 - Contribuenti colpiti da altri eventi eccezionali", bObbl = .f. , nPag = 5;
  , bGlobalFont=.t.


  func oEVEECC_2017_5_8.RadioValue()
    return(iif(this.value =1,'00',;
    iif(this.value =2,'01',;
    iif(this.value =3,'03',;
    iif(this.value =4,'08',;
    space(2))))))
  endfunc
  func oEVEECC_2017_5_8.GetRadio()
    this.Parent.oContained.w_EVEECC_2017 = this.RadioValue()
    return .t.
  endfunc

  func oEVEECC_2017_5_8.SetRadio()
    this.Parent.oContained.w_EVEECC_2017=trim(this.Parent.oContained.w_EVEECC_2017)
    this.value = ;
      iif(this.Parent.oContained.w_EVEECC_2017=='00',1,;
      iif(this.Parent.oContained.w_EVEECC_2017=='01',2,;
      iif(this.Parent.oContained.w_EVEECC_2017=='03',3,;
      iif(this.Parent.oContained.w_EVEECC_2017=='08',4,;
      0))))
  endfunc

  func oEVEECC_2017_5_8.mHide()
    with this.Parent.oContained
      return (.w_GTTIP770<>'77S18')
    endwith
  endfunc


  add object oGTTIPSOS_5_10 as StdCombo with uid="LYMYQUTOAV",rtseq=139,rtrep=.f.,left=125,top=151,width=649,height=22, enabled=.f.;
    , ToolTipText = "Tipologia sostituto";
    , HelpContextID = 96772793;
    , cFormVar="w_GTTIPSOS",RowSource=""+"1 - Sostituto che ha operato solo ritenute da lavoro dipendente e/o da lavoro autonomo,"+"2 - Sostituto che ha operato sia ritenute da lavoro dipendente e/o ritenute da lavoro autonomo che ritenute da reddito di capitale,"+"3 - Sostituto che ha operato solo ritenute da redditi di capitale", bObbl = .f. , nPag = 5;
  , bGlobalFont=.t.


  func oGTTIPSOS_5_10.RadioValue()
    return(iif(this.value =1,'1',;
    iif(this.value =2,'2',;
    iif(this.value =3,'3',;
    space(1)))))
  endfunc
  func oGTTIPSOS_5_10.GetRadio()
    this.Parent.oContained.w_GTTIPSOS = this.RadioValue()
    return .t.
  endfunc

  func oGTTIPSOS_5_10.SetRadio()
    this.Parent.oContained.w_GTTIPSOS=trim(this.Parent.oContained.w_GTTIPSOS)
    this.value = ;
      iif(this.Parent.oContained.w_GTTIPSOS=='1',1,;
      iif(this.Parent.oContained.w_GTTIPSOS=='2',2,;
      iif(this.Parent.oContained.w_GTTIPSOS=='3',3,;
      0)))
  endfunc

  func oGTTIPSOS_5_10.mHide()
    with this.Parent.oContained
      return (.w_GTTIP770<>'77S17')
    endwith
  endfunc

  add object oGTPROTEC_5_11 as StdField with uid="UPEJCCASCK",rtseq=140,rtrep=.f.,;
    cFormVar = "w_GTPROTEC", cQueryName = "GTPROTEC",;
    bObbl = .t. , nPag = 5, value=space(17), bMultilanguage =  .f.,;
    ToolTipText = "Identificativo dell'invio",;
    HelpContextID = 155360599,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=117, Top=193, cSayPict='"99999999999999999"', cGetPict='"99999999999999999"', InputMask=replicate('X',17), Alignment=1

  func oGTPROTEC_5_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((.w_GTFLGCOR='1' or .w_GTFLGINT='1') And .w_GTTIPINV="2")
    endwith
   endif
  endfunc

  func oGTPROTEC_5_11.mHide()
    with this.Parent.oContained
      return (.w_GTTIP770<>'77S18')
    endwith
  endfunc

  add object oGTPROSEZ_5_12 as StdField with uid="VZNYVKEFQK",rtseq=141,rtrep=.f.,;
    cFormVar = "w_GTPROSEZ", cQueryName = "GTPROSEZ",;
    bObbl = .t. , nPag = 5, value=0, bMultilanguage =  .f.,;
    HelpContextID = 172137792,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=273, Top=193, cSayPict='"999999"', cGetPict='"999999"'

  func oGTPROSEZ_5_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not Empty(.w_Gtprotec))
    endwith
   endif
  endfunc

  func oGTPROSEZ_5_12.mHide()
    with this.Parent.oContained
      return (.w_GTTIP770<>'77S18')
    endwith
  endfunc


  add object oGTSELCAF_5_16 as StdCombo with uid="YHBTTGOZYC",rtseq=142,rtrep=.f.,left=128,top=269,width=210,height=22;
    , ToolTipText = "Selezione CAF o Professionista";
    , HelpContextID = 92312236;
    , cFormVar="w_GTSELCAF",RowSource=""+"CAF,"+"Professionista", bObbl = .t. , nPag = 5;
  , bGlobalFont=.t.


  func oGTSELCAF_5_16.RadioValue()
    return(iif(this.value =1,'1',;
    iif(this.value =2,'2',;
    space(1))))
  endfunc
  func oGTSELCAF_5_16.GetRadio()
    this.Parent.oContained.w_GTSELCAF = this.RadioValue()
    return .t.
  endfunc

  func oGTSELCAF_5_16.SetRadio()
    this.Parent.oContained.w_GTSELCAF=trim(this.Parent.oContained.w_GTSELCAF)
    this.value = ;
      iif(this.Parent.oContained.w_GTSELCAF=='1',1,;
      iif(this.Parent.oContained.w_GTSELCAF=='2',2,;
      0))
  endfunc

  add object oGTRESCAF_5_19 as StdField with uid="SKMGILFBZU",rtseq=143,rtrep=.f.,;
    cFormVar = "w_GTRESCAF", cQueryName = "GTRESCAF",;
    bObbl = .f. , nPag = 5, value=space(16), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale",;
    HelpContextID = 99648172,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=408, Top=269, cSayPict='REPLICATE("!",16)', cGetPict='REPLICATE("!",16)', InputMask=replicate('X',16)

  func oGTRESCAF_5_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (iif(not empty(.w_Gtrescaf),chkcfp(alltrim(.w_Gtrescaf),'CF'),.T.))
    endwith
    return bRes
  endfunc

  add object oGTDELCAF_5_21 as StdField with uid="JAMCJBIFRK",rtseq=144,rtrep=.f.,;
    cFormVar = "w_GTDELCAF", cQueryName = "GTDELCAF",;
    bObbl = .f. , nPag = 5, value=space(11), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale del C.A.F.",;
    HelpContextID = 92250796,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=128, Top=322, cSayPict='repl("!",11)', cGetPict='repl("!",11)', InputMask=replicate('X',11)

  func oGTDELCAF_5_21.mHide()
    with this.Parent.oContained
      return (.w_Gtselcaf <> '1')
    endwith
  endfunc

  func oGTDELCAF_5_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (iif(not empty(.w_Gtdelcaf),chkcfp(alltrim(.w_Gtdelcaf),'PI'),.T.))
    endwith
    return bRes
  endfunc

  add object oGTFLGFIR_5_22 as StdCheck with uid="OKVJHQYJJF",rtseq=145,rtrep=.f.,left=408, top=322, caption="Flag firma del riquadro visto di conformit�",;
    ToolTipText = "Flag firma riquadro 'visto conformit�'",;
    HelpContextID = 130628936,;
    cFormVar="w_GTFLGFIR", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oGTFLGFIR_5_22.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oGTFLGFIR_5_22.GetRadio()
    this.Parent.oContained.w_GTFLGFIR = this.RadioValue()
    return .t.
  endfunc

  func oGTFLGFIR_5_22.SetRadio()
    this.Parent.oContained.w_GTFLGFIR=trim(this.Parent.oContained.w_GTFLGFIR)
    this.value = ;
      iif(this.Parent.oContained.w_GTFLGFIR=='1',1,;
      0)
  endfunc

  add object oGT__ANNO_5_24 as StdField with uid="HYNPMMDCOL",rtseq=146,rtrep=.f.,;
    cFormVar = "w_GT__ANNO", cQueryName = "GT__ANNO",;
    bObbl = .t. , nPag = 5, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Anno di riferimento",;
    HelpContextID = 267080373,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=113, Top=404, cSayPict='"9999"', cGetPict='"9999"'

  func oGT__ANNO_5_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  func oGT__ANNO_5_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_GTTIP770='77S16' And .w_GT__ANNO=2015) Or (.w_GTTIP770='77S17' And .w_GT__ANNO=2016) Or (.w_GTTIP770='77S18' And .w_GT__ANNO=2017))
    endwith
    return bRes
  endfunc

  add object oGTNOMFIL_5_28 as StdField with uid="VPKTICWKHO",rtseq=147,rtrep=.f.,;
    cFormVar = "w_GTNOMFIL", cQueryName = "GTNOMFIL",;
    bObbl = .f. , nPag = 5, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Nome file e percorso per inoltro telematico",;
    HelpContextID = 124108110,;
   bGlobalFont=.t.,;
    Height=21, Width=636, Left=113, Top=471, InputMask=replicate('X',254)


  add object oBtn_5_31 as StdButton with uid="TQEMIMWBDV",left=761, top=471, width=21,height=20,;
    caption="...", nPag=5;
    , ToolTipText = "Seleziona la cartella dove si vuole salvare il file";
    , HelpContextID = 42394922;
  , bGlobalFont=.t.

    proc oBtn_5_31.Click()
      with this.Parent.oContained
        do SfogliaDir with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_5_33 as StdButton with uid="AYLATODQCL",left=739, top=505, width=48,height=45,;
    CpPicture="bmp\SAVE.bmp", caption="", nPag=5;
    , ToolTipText = "Premere per generare file telematico ";
    , HelpContextID = 48349974;
    ,  tabstop = .f., caption='\<Genera';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_5_33.Click()
      with this.Parent.oContained
        do GSBB_BFT with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_5_33.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not empty(.w_Gtnomfil) And isInoltel() And not g_DEMO AND BBVERSION>=200)
      endwith
    endif
  endfunc

  func oBtn_5_33.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_GTTIP770='77S17' Or .w_GTTIP770='77S18')
     endwith
    endif
  endfunc


  add object oLinkPC_5_34 as stdDynamicChildContainer with uid="ZNFBGGEZZK",left=4, top=510, width=216, height=39, bOnScreen=.t.;
    , tabstop=.f.

  func oLinkPC_5_34.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (1=1)
     endwith
    endif
  endfunc


  add object oBtn_5_35 as StdButton with uid="PJPVILDQVM",left=687, top=505, width=48,height=45,;
    CpPicture="bmp\STAMPA.BMP", caption="", nPag=5;
    , ToolTipText = "Stampa frontespizio e riepilogo";
    , HelpContextID = 48349974;
    , caption='M\<odello';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_5_35.Click()
      with this.Parent.oContained
        do Gsri_Bsy with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_5_35.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction<> 'Load')
      endwith
    endif
  endfunc

  func oBtn_5_35.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (Empty (.w_Gtserial))
     endwith
    endif
  endfunc


  add object oBtn_5_43 as StdButton with uid="LHMXVPOENY",left=739, top=505, width=48,height=45,;
    CpPicture="bmp\SAVE.bmp", caption="", nPag=5;
    , ToolTipText = "Premere per generare file telematico ";
    , HelpContextID = 48349974;
    ,  tabstop = .f., caption='\<Genera';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_5_43.Click()
      with this.Parent.oContained
        do GSBB1BFT with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_5_43.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not empty(.w_Gtnomfil) And isInoltel() And not g_DEMO AND BBVERSION>=308)
      endwith
    endif
  endfunc

  func oBtn_5_43.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_GTTIP770<>'77S17')
     endwith
    endif
  endfunc


  add object oBtn_5_48 as StdButton with uid="ADCLVMNYIF",left=739, top=505, width=48,height=45,;
    CpPicture="bmp\SAVE.bmp", caption="", nPag=5;
    , ToolTipText = "Premere per generare file telematico ";
    , HelpContextID = 48349974;
    ,  tabstop = .f., caption='\<Genera';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_5_48.Click()
      with this.Parent.oContained
        do GSBB2BFT with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_5_48.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not empty(.w_Gtnomfil) And isInoltel() And not g_DEMO AND BBVERSION>=421)
      endwith
    endif
  endfunc

  func oBtn_5_48.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_GTTIP770<>'77S18')
     endwith
    endif
  endfunc

  add object oStr_5_13 as StdString with uid="MDEDFCKUHY",Visible=.t., Left=20, Top=23,;
    Alignment=1, Width=87, Height=15,;
    Caption="Tipo fornitore:"  ;
  , bGlobalFont=.t.

  add object oStr_5_14 as StdString with uid="SSEVNHZBSS",Visible=.t., Left=8, Top=219,;
    Alignment=0, Width=217, Height=18,;
    Caption="Visto di conformit�"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_5_17 as StdString with uid="JQVYZKCGWO",Visible=.t., Left=126, Top=247,;
    Alignment=0, Width=87, Height=18,;
    Caption="Tipologia"  ;
  , bGlobalFont=.t.

  add object oStr_5_18 as StdString with uid="OYNDIRXSXM",Visible=.t., Left=408, Top=247,;
    Alignment=0, Width=320, Height=18,;
    Caption="Codice fiscale del responsabile del C.A.F. o professionista"  ;
  , bGlobalFont=.t.

  add object oStr_5_20 as StdString with uid="LQMPVMQSEY",Visible=.t., Left=126, Top=303,;
    Alignment=0, Width=141, Height=18,;
    Caption="Codice fiscale del C.A.F."  ;
  , bGlobalFont=.t.

  func oStr_5_20.mHide()
    with this.Parent.oContained
      return (.w_GTSELCAF <> '1')
    endwith
  endfunc

  add object oStr_5_23 as StdString with uid="SBLLSYVEOU",Visible=.t., Left=128, Top=353,;
    Alignment=0, Width=375, Height=18,;
    Caption="Si rilascia il visto di conformit� ai sensi art. 35 del D.lgs.  n. 241/1997"  ;
  , bGlobalFont=.t.

  add object oStr_5_26 as StdString with uid="UTEVLEHEXF",Visible=.t., Left=8, Top=377,;
    Alignment=0, Width=217, Height=18,;
    Caption="Parametri di selezione"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_5_29 as StdString with uid="LTPVJFCSHF",Visible=.t., Left=8, Top=434,;
    Alignment=0, Width=217, Height=18,;
    Caption="File telematico"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_5_30 as StdString with uid="SSBWQANJWI",Visible=.t., Left=26, Top=472,;
    Alignment=1, Width=81, Height=18,;
    Caption="Nome file:"  ;
  , bGlobalFont=.t.

  add object oStr_5_32 as StdString with uid="SJCUFHTJAM",Visible=.t., Left=41, Top=405,;
    Alignment=1, Width=66, Height=18,;
    Caption="Anno:"  ;
  , bGlobalFont=.t.

  add object oStr_5_37 as StdString with uid="HETNZETWGU",Visible=.t., Left=8, Top=56,;
    Alignment=0, Width=395, Height=18,;
    Caption="Tipo di dichiarazione"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_5_39 as StdString with uid="HBKOSEUOHR",Visible=.t., Left=4, Top=119,;
    Alignment=1, Width=110, Height=18,;
    Caption="Eventi eccezionali:"  ;
  , bGlobalFont=.t.

  add object oStr_5_40 as StdString with uid="TYZQPSXRHW",Visible=.t., Left=4, Top=153,;
    Alignment=1, Width=115, Height=18,;
    Caption="Tipologia sostituto:"  ;
  , bGlobalFont=.t.

  func oStr_5_40.mHide()
    with this.Parent.oContained
      return (.w_GTTIP770<>'77S17')
    endwith
  endfunc

  add object oStr_5_44 as StdString with uid="CDULJBZYFZ",Visible=.t., Left=4, Top=155,;
    Alignment=0, Width=285, Height=18,;
    Caption="Protocollo dichiarazione inviata in gestione separata"  ;
  , bGlobalFont=.t.

  func oStr_5_44.mHide()
    with this.Parent.oContained
      return (.w_GTTIP770<>'77S18')
    endwith
  endfunc

  add object oStr_5_45 as StdString with uid="VZURLAIDOZ",Visible=.t., Left=117, Top=176,;
    Alignment=0, Width=145, Height=18,;
    Caption="Identificativo invio"  ;
  , bGlobalFont=.t.

  func oStr_5_45.mHide()
    with this.Parent.oContained
      return (.w_GTTIP770<>'77S18')
    endwith
  endfunc

  add object oStr_5_46 as StdString with uid="XODISBARRH",Visible=.t., Left=273, Top=176,;
    Alignment=0, Width=145, Height=18,;
    Caption="Progressivo"  ;
  , bGlobalFont=.t.

  func oStr_5_46.mHide()
    with this.Parent.oContained
      return (.w_GTTIP770<>'77S18')
    endwith
  endfunc

  add object oStr_5_47 as StdString with uid="DAOKKOAWNW",Visible=.t., Left=258, Top=194,;
    Alignment=0, Width=13, Height=18,;
    Caption="-"  ;
  , bGlobalFont=.t.

  func oStr_5_47.mHide()
    with this.Parent.oContained
      return (.w_GTTIP770<>'77S18')
    endwith
  endfunc

  add object oBox_5_15 as StdBox with uid="AZYMGKTHCS",left=10, top=234, width=776,height=2

  add object oBox_5_25 as StdBox with uid="VQUFPWYELS",left=10, top=393, width=776,height=2

  add object oBox_5_27 as StdBox with uid="UFRJQQJEBL",left=11, top=450, width=776,height=2

  add object oBox_5_36 as StdBox with uid="LKBNEZZUXB",left=2, top=76, width=785,height=2
  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gsri_mgt",lower(this.oContained.Gsri_Mgt.class))=0
        this.oContained.Gsri_Mgt.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsri_agy','GEFILTEL','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".GTSERIAL=GEFILTEL.GTSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsri_agy
proc SfogliaDir (parent)
local PathName, oField
  PathName = Cp_GetDir() 
  if !empty(PathName)
    parent.w_DirName = PathName
    parent.w_GTNOMFIL=left(parent.w_DirName+Alltrim(parent.w_Gtcodfis)+'_'+Substr(parent.w_Gtserial,2,9)+'.77S'+space(254),254)
  endif
endproc
* --- Fine Area Manuale
