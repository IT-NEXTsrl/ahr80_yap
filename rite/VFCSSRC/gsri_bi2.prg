* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_bi2                                                        *
*              Lancia maschera zoom                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][98][VRS_19]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-09-19                                                      *
* Last revis.: 2007-12-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsri_bi2",oParentObject)
return(i_retval)

define class tgsri_bi2 as StdBatch
  * --- Local variables
  w_GIAVER2 = space(10)
  w_STATO2 = space(10)
  w_NREGIS2 = 0
  w_NDISTI2 = space(10)
  w_ESERCI2 = space(4)
  w_DATADI2 = ctod("  /  /  ")
  w_DATFIN = ctod("  /  /  ")
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lanciato dalla maschera  GSRI_SIN
    * --- Assegnamento a variabili locali
    this.w_GIAVER2 = this.oParentObject.w_GIAVER
    this.w_STATO2 = this.oParentObject.w_STATO
    this.w_NREGIS2 = this.oParentObject.w_NREGIS
    this.w_NDISTI2 = this.oParentObject.w_NDISTI
    this.w_ESERCI2 = this.oParentObject.w_ESERCI
    this.w_DATADI2 = this.oParentObject.w_DATADI
    this.w_DATFIN = this.oParentObject.w_DATCAM
    do GSRI_SI2 with this
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Assegnamento a variabili di GSRI_SIN
    this.oParentObject.w_GIAVER = this.w_GIAVER2
    this.oParentObject.w_STATO = this.w_STATO2
    this.oParentObject.w_NREGIS = this.w_NREGIS2
    this.oParentObject.w_NDISTI = this.w_NDISTI2
    this.oParentObject.w_ESERCI = this.w_ESERCI2
    this.oParentObject.w_DATADI = this.w_DATADI2
    this.oParentObject.w_DATCAM = this.w_DATFIN
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
