* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_kgt                                                        *
*              STAMPA QUADRI SS - ST - H                                       *
*                                                                              *
*      Author: Zucchetti S.p.A.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-08-31                                                      *
* Last revis.: 2015-06-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsri_kgt",oParentObject))

* --- Class definition
define class tgsri_kgt as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 795
  Height = 380
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-06-29"
  HelpContextID=32110185
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=20

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsri_kgt"
  cComment = "STAMPA QUADRI SS - ST - H "
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_FIRDIC = space(1)
  w_FIRSG1 = space(1)
  w_FIRSG2 = space(1)
  w_FIRSG3 = space(1)
  w_FIRSG4 = space(1)
  w_FIRSG5 = space(1)
  w_FIRINT = space(1)
  w_STAMPAST = space(1)
  w_STAMPAH = space(1)
  o_STAMPAH = space(1)
  w_STAMPAJ = space(1)
  w_FIRMA_DICHIARAZIONE = space(250)
  w_FIRMA_SOGGETTO1 = space(250)
  w_FIRMA_SOGGETTO2 = space(250)
  w_FIRMA_SOGGETTO3 = space(250)
  w_FIRMA_SOGGETTO4 = space(250)
  w_FIRMA_SOGGETTO5 = space(250)
  w_STAMPAA = space(1)
  w_FIRMA_INTERMEDIARIO = space(250)
  w_FIRMA_CAF = space(250)
  w_FIRCAF = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsri_kgtPag1","gsri_kgt",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSTAMPAST_1_8
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- gsri_kgt
    thisform.Autocenter= .T.
    thisform.Closable = .F.
    
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_FIRDIC=space(1)
      .w_FIRSG1=space(1)
      .w_FIRSG2=space(1)
      .w_FIRSG3=space(1)
      .w_FIRSG4=space(1)
      .w_FIRSG5=space(1)
      .w_FIRINT=space(1)
      .w_STAMPAST=space(1)
      .w_STAMPAH=space(1)
      .w_STAMPAJ=space(1)
      .w_FIRMA_DICHIARAZIONE=space(250)
      .w_FIRMA_SOGGETTO1=space(250)
      .w_FIRMA_SOGGETTO2=space(250)
      .w_FIRMA_SOGGETTO3=space(250)
      .w_FIRMA_SOGGETTO4=space(250)
      .w_FIRMA_SOGGETTO5=space(250)
      .w_STAMPAA=space(1)
      .w_FIRMA_INTERMEDIARIO=space(250)
      .w_FIRMA_CAF=space(250)
      .w_FIRCAF=space(1)
      .w_FIRDIC=oParentObject.w_FIRDIC
      .w_FIRSG1=oParentObject.w_FIRSG1
      .w_FIRSG2=oParentObject.w_FIRSG2
      .w_FIRSG3=oParentObject.w_FIRSG3
      .w_FIRSG4=oParentObject.w_FIRSG4
      .w_FIRSG5=oParentObject.w_FIRSG5
      .w_FIRINT=oParentObject.w_FIRINT
      .w_STAMPAST=oParentObject.w_STAMPAST
      .w_STAMPAH=oParentObject.w_STAMPAH
      .w_STAMPAJ=oParentObject.w_STAMPAJ
      .w_FIRMA_DICHIARAZIONE=oParentObject.w_FIRMA_DICHIARAZIONE
      .w_FIRMA_SOGGETTO1=oParentObject.w_FIRMA_SOGGETTO1
      .w_FIRMA_SOGGETTO2=oParentObject.w_FIRMA_SOGGETTO2
      .w_FIRMA_SOGGETTO3=oParentObject.w_FIRMA_SOGGETTO3
      .w_FIRMA_SOGGETTO4=oParentObject.w_FIRMA_SOGGETTO4
      .w_FIRMA_SOGGETTO5=oParentObject.w_FIRMA_SOGGETTO5
      .w_STAMPAA=oParentObject.w_STAMPAA
      .w_FIRMA_INTERMEDIARIO=oParentObject.w_FIRMA_INTERMEDIARIO
      .w_FIRMA_CAF=oParentObject.w_FIRMA_CAF
      .w_FIRCAF=oParentObject.w_FIRCAF
          .DoRTCalc(1,7,.f.)
        .w_STAMPAST = .w_STAMPAST
        .w_STAMPAH = .w_STAMPAH
    endwith
    this.DoRTCalc(10,20,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_30.enabled = this.oPgFrm.Page1.oPag.oBtn_1_30.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_31.enabled = this.oPgFrm.Page1.oPag.oBtn_1_31.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_FIRDIC=.w_FIRDIC
      .oParentObject.w_FIRSG1=.w_FIRSG1
      .oParentObject.w_FIRSG2=.w_FIRSG2
      .oParentObject.w_FIRSG3=.w_FIRSG3
      .oParentObject.w_FIRSG4=.w_FIRSG4
      .oParentObject.w_FIRSG5=.w_FIRSG5
      .oParentObject.w_FIRINT=.w_FIRINT
      .oParentObject.w_STAMPAST=.w_STAMPAST
      .oParentObject.w_STAMPAH=.w_STAMPAH
      .oParentObject.w_STAMPAJ=.w_STAMPAJ
      .oParentObject.w_FIRMA_DICHIARAZIONE=.w_FIRMA_DICHIARAZIONE
      .oParentObject.w_FIRMA_SOGGETTO1=.w_FIRMA_SOGGETTO1
      .oParentObject.w_FIRMA_SOGGETTO2=.w_FIRMA_SOGGETTO2
      .oParentObject.w_FIRMA_SOGGETTO3=.w_FIRMA_SOGGETTO3
      .oParentObject.w_FIRMA_SOGGETTO4=.w_FIRMA_SOGGETTO4
      .oParentObject.w_FIRMA_SOGGETTO5=.w_FIRMA_SOGGETTO5
      .oParentObject.w_STAMPAA=.w_STAMPAA
      .oParentObject.w_FIRMA_INTERMEDIARIO=.w_FIRMA_INTERMEDIARIO
      .oParentObject.w_FIRMA_CAF=.w_FIRMA_CAF
      .oParentObject.w_FIRCAF=.w_FIRCAF
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,9,.t.)
        if .o_STAMPAH<>.w_STAMPAH
            .w_STAMPAJ = iif(.w_STAMPAH='S',.w_STAMPAJ,'N')
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(11,20,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_YNKSLDBNSX()
    with this
          * --- Assegno il valore 'N' alle variabili w_STAMPAH e w_STAMPAST
          .oParentobject.w_STAMPAH = 'N'
          .oParentobject.w_STAMPAST = 'N'
          .oParentobject.w_STAMPAJ = 'N'
          .oParentobject.w_STAMPAA = 'N'
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oSTAMPAJ_1_10.enabled = this.oPgFrm.Page1.oPag.oSTAMPAJ_1_10.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Edit Aborted")
          .Calculate_YNKSLDBNSX()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSTAMPAST_1_8.RadioValue()==this.w_STAMPAST)
      this.oPgFrm.Page1.oPag.oSTAMPAST_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSTAMPAH_1_9.RadioValue()==this.w_STAMPAH)
      this.oPgFrm.Page1.oPag.oSTAMPAH_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSTAMPAJ_1_10.RadioValue()==this.w_STAMPAJ)
      this.oPgFrm.Page1.oPag.oSTAMPAJ_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFIRMA_DICHIARAZIONE_1_14.value==this.w_FIRMA_DICHIARAZIONE)
      this.oPgFrm.Page1.oPag.oFIRMA_DICHIARAZIONE_1_14.value=this.w_FIRMA_DICHIARAZIONE
    endif
    if not(this.oPgFrm.Page1.oPag.oFIRMA_SOGGETTO1_1_17.value==this.w_FIRMA_SOGGETTO1)
      this.oPgFrm.Page1.oPag.oFIRMA_SOGGETTO1_1_17.value=this.w_FIRMA_SOGGETTO1
    endif
    if not(this.oPgFrm.Page1.oPag.oFIRMA_SOGGETTO2_1_23.value==this.w_FIRMA_SOGGETTO2)
      this.oPgFrm.Page1.oPag.oFIRMA_SOGGETTO2_1_23.value=this.w_FIRMA_SOGGETTO2
    endif
    if not(this.oPgFrm.Page1.oPag.oFIRMA_SOGGETTO3_1_24.value==this.w_FIRMA_SOGGETTO3)
      this.oPgFrm.Page1.oPag.oFIRMA_SOGGETTO3_1_24.value=this.w_FIRMA_SOGGETTO3
    endif
    if not(this.oPgFrm.Page1.oPag.oFIRMA_SOGGETTO4_1_25.value==this.w_FIRMA_SOGGETTO4)
      this.oPgFrm.Page1.oPag.oFIRMA_SOGGETTO4_1_25.value=this.w_FIRMA_SOGGETTO4
    endif
    if not(this.oPgFrm.Page1.oPag.oFIRMA_SOGGETTO5_1_26.value==this.w_FIRMA_SOGGETTO5)
      this.oPgFrm.Page1.oPag.oFIRMA_SOGGETTO5_1_26.value=this.w_FIRMA_SOGGETTO5
    endif
    if not(this.oPgFrm.Page1.oPag.oFIRMA_INTERMEDIARIO_1_28.value==this.w_FIRMA_INTERMEDIARIO)
      this.oPgFrm.Page1.oPag.oFIRMA_INTERMEDIARIO_1_28.value=this.w_FIRMA_INTERMEDIARIO
    endif
    if not(this.oPgFrm.Page1.oPag.oFIRMA_CAF_1_29.value==this.w_FIRMA_CAF)
      this.oPgFrm.Page1.oPag.oFIRMA_CAF_1_29.value=this.w_FIRMA_CAF
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_STAMPAH = this.w_STAMPAH
    return

enddefine

* --- Define pages as container
define class tgsri_kgtPag1 as StdContainer
  Width  = 791
  height = 380
  stdWidth  = 791
  stdheight = 380
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSTAMPAST_1_8 as StdCheck with uid="BJLXRETCBZ",rtseq=8,rtrep=.f.,left=12, top=36, caption="Stampa quadri ST",;
    ToolTipText = "Se attivo, vengono stampati i quadri ST della dichiarazione in formato PDF",;
    HelpContextID = 73888634,;
    cFormVar="w_STAMPAST", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSTAMPAST_1_8.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oSTAMPAST_1_8.GetRadio()
    this.Parent.oContained.w_STAMPAST = this.RadioValue()
    return .t.
  endfunc

  func oSTAMPAST_1_8.SetRadio()
    this.Parent.oContained.w_STAMPAST=trim(this.Parent.oContained.w_STAMPAST)
    this.value = ;
      iif(this.Parent.oContained.w_STAMPAST=='S',1,;
      0)
  endfunc

  add object oSTAMPAH_1_9 as StdCheck with uid="PVMRAJWLNA",rtseq=9,rtrep=.f.,left=187, top=36, caption="Stampa quadri H",;
    ToolTipText = "Se attivo, vengono stampati i quadri H della dichiarazione in formato PDF",;
    HelpContextID = 73888550,;
    cFormVar="w_STAMPAH", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSTAMPAH_1_9.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oSTAMPAH_1_9.GetRadio()
    this.Parent.oContained.w_STAMPAH = this.RadioValue()
    return .t.
  endfunc

  func oSTAMPAH_1_9.SetRadio()
    this.Parent.oContained.w_STAMPAH=trim(this.Parent.oContained.w_STAMPAH)
    this.value = ;
      iif(this.Parent.oContained.w_STAMPAH=='S',1,;
      0)
  endfunc

  add object oSTAMPAJ_1_10 as StdCheck with uid="YMVZATGDST",rtseq=10,rtrep=.f.,left=341, top=36, caption="Stampa quadri SS",;
    ToolTipText = "Se attivo, vengono stampati i quadri SS della dichiarazione in formato PDF",;
    HelpContextID = 194546906,;
    cFormVar="w_STAMPAJ", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSTAMPAJ_1_10.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oSTAMPAJ_1_10.GetRadio()
    this.Parent.oContained.w_STAMPAJ = this.RadioValue()
    return .t.
  endfunc

  func oSTAMPAJ_1_10.SetRadio()
    this.Parent.oContained.w_STAMPAJ=trim(this.Parent.oContained.w_STAMPAJ)
    this.value = ;
      iif(this.Parent.oContained.w_STAMPAJ=='S',1,;
      0)
  endfunc

  func oSTAMPAJ_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_STAMPAH='S')
    endwith
   endif
  endfunc

  add object oFIRMA_DICHIARAZIONE_1_14 as StdField with uid="SORZZBTLBS",rtseq=11,rtrep=.f.,;
    cFormVar = "w_FIRMA_DICHIARAZIONE", cQueryName = "FIRMA_DICHIARAZIONE",;
    bObbl = .f. , nPag = 1, value=space(250), bMultilanguage =  .f.,;
    ToolTipText = "Firma del dichiarante",;
    HelpContextID = 132314888,;
   bGlobalFont=.t.,;
    Height=21, Width=566, Left=215, Top=88, InputMask=replicate('X',250)

  add object oFIRMA_SOGGETTO1_1_17 as StdField with uid="ZEXVOOLKWC",rtseq=12,rtrep=.f.,;
    cFormVar = "w_FIRMA_SOGGETTO1", cQueryName = "FIRMA_SOGGETTO1",;
    bObbl = .f. , nPag = 1, value=space(250), bMultilanguage =  .f.,;
    ToolTipText = "Firma del soggetto",;
    HelpContextID = 166653163,;
   bGlobalFont=.t.,;
    Height=21, Width=566, Left=215, Top=119, InputMask=replicate('X',250)

  add object oFIRMA_SOGGETTO2_1_23 as StdField with uid="QZZQOVYBZD",rtseq=13,rtrep=.f.,;
    cFormVar = "w_FIRMA_SOGGETTO2", cQueryName = "FIRMA_SOGGETTO2",;
    bObbl = .f. , nPag = 1, value=space(250), bMultilanguage =  .f.,;
    ToolTipText = "Firma del soggetto",;
    HelpContextID = 166653163,;
   bGlobalFont=.t.,;
    Height=21, Width=566, Left=215, Top=150, InputMask=replicate('X',250)

  add object oFIRMA_SOGGETTO3_1_24 as StdField with uid="HTEMGNIMID",rtseq=14,rtrep=.f.,;
    cFormVar = "w_FIRMA_SOGGETTO3", cQueryName = "FIRMA_SOGGETTO3",;
    bObbl = .f. , nPag = 1, value=space(250), bMultilanguage =  .f.,;
    ToolTipText = "Firma del soggetto",;
    HelpContextID = 166653163,;
   bGlobalFont=.t.,;
    Height=21, Width=566, Left=215, Top=181, InputMask=replicate('X',250)

  add object oFIRMA_SOGGETTO4_1_25 as StdField with uid="HSTWLQIQEN",rtseq=15,rtrep=.f.,;
    cFormVar = "w_FIRMA_SOGGETTO4", cQueryName = "FIRMA_SOGGETTO4",;
    bObbl = .f. , nPag = 1, value=space(250), bMultilanguage =  .f.,;
    ToolTipText = "Firma del soggetto",;
    HelpContextID = 166653163,;
   bGlobalFont=.t.,;
    Height=21, Width=566, Left=215, Top=212, InputMask=replicate('X',250)

  add object oFIRMA_SOGGETTO5_1_26 as StdField with uid="HQGSGTDSZZ",rtseq=16,rtrep=.f.,;
    cFormVar = "w_FIRMA_SOGGETTO5", cQueryName = "FIRMA_SOGGETTO5",;
    bObbl = .f. , nPag = 1, value=space(250), bMultilanguage =  .f.,;
    ToolTipText = "Firma del soggetto",;
    HelpContextID = 166653163,;
   bGlobalFont=.t.,;
    Height=21, Width=566, Left=215, Top=243, InputMask=replicate('X',250)

  add object oFIRMA_INTERMEDIARIO_1_28 as StdField with uid="FVTGJEEOOZ",rtseq=18,rtrep=.f.,;
    cFormVar = "w_FIRMA_INTERMEDIARIO", cQueryName = "FIRMA_INTERMEDIARIO",;
    bObbl = .f. , nPag = 1, value=space(250), bMultilanguage =  .f.,;
    ToolTipText = "Firma dell'intermediario",;
    HelpContextID = 169877573,;
   bGlobalFont=.t.,;
    Height=21, Width=566, Left=215, Top=274, InputMask=replicate('X',250)

  add object oFIRMA_CAF_1_29 as StdField with uid="MGYPSTJFMC",rtseq=19,rtrep=.f.,;
    cFormVar = "w_FIRMA_CAF", cQueryName = "FIRMA_CAF",;
    bObbl = .f. , nPag = 1, value=space(250), bMultilanguage =  .f.,;
    ToolTipText = "Firma del responsabile del C.A.F o del professionista",;
    HelpContextID = 24673271,;
   bGlobalFont=.t.,;
    Height=21, Width=566, Left=215, Top=305, InputMask=replicate('X',250)


  add object oBtn_1_30 as StdButton with uid="QFDCFAAMDM",left=678, top=330, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per stampare i quadri selezionati";
    , HelpContextID = 58835734;
    , caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_30.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_31 as StdButton with uid="BRAIRZJTXI",left=731, top=330, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 58835734;
    , caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_31.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_12 as StdString with uid="DOUVHPOJSN",Visible=.t., Left=7, Top=5,;
    Alignment=0, Width=271, Height=18,;
    Caption="Selezione quadri della dichiarazione da stampare"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="NPMUGKBSUY",Visible=.t., Left=48, Top=90,;
    Alignment=1, Width=164, Height=18,;
    Caption="Firma del dichiarante:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="KLBNRNWNFY",Visible=.t., Left=69, Top=276,;
    Alignment=1, Width=143, Height=18,;
    Caption="Firma dell'intermediario:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="NZVRWIKTIO",Visible=.t., Left=48, Top=121,;
    Alignment=1, Width=164, Height=18,;
    Caption="Firma del soggetto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="RKLRAALRAR",Visible=.t., Left=48, Top=150,;
    Alignment=1, Width=164, Height=18,;
    Caption="Firma del soggetto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="YEYNQJATPA",Visible=.t., Left=48, Top=183,;
    Alignment=1, Width=164, Height=18,;
    Caption="Firma del soggetto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="VOEQMHNMLZ",Visible=.t., Left=48, Top=214,;
    Alignment=1, Width=164, Height=18,;
    Caption="Firma del soggetto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="CSVRQFWVYJ",Visible=.t., Left=48, Top=245,;
    Alignment=1, Width=164, Height=18,;
    Caption="Firma del soggetto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="GATLWGMZWS",Visible=.t., Left=8, Top=306,;
    Alignment=1, Width=204, Height=18,;
    Caption="Firma del resp. C.A.F o del profess.:"  ;
  , bGlobalFont=.t.

  add object oBox_1_13 as StdBox with uid="KHJKEHAASK",left=4, top=27, width=390,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsri_kgt','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
