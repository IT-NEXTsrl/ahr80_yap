* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_bmm                                                        *
*              Aggiornamento movimenti  ritenute                               *
*                                                                              *
*      Author: Zucchetti S.p.A.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-05-22                                                      *
* Last revis.: 2018-05-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParam
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsri_bmm",oParentObject,m.pParam)
return(i_retval)

define class tgsri_bmm as StdBatch
  * --- Local variables
  pParam = space(1)
  w_NOMECURS = space(10)
  w_SERIALE = space(10)
  w_CAMPO = space(25)
  w_ZOOM_MOV = .NULL.
  w_ZOOMSMOV = .NULL.
  w_ZOOMAFOR = .NULL.
  w_ZOOMCMOV = .NULL.
  w_COD_FORNITORE = space(15)
  w_OBJECT = .NULL.
  * --- WorkFile variables
  MOV_RITE_idx=0
  CONTI_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Routine per l'aggiornamento massivo del tipo operazione associato ai moviemnti ritenute (770/09)
    do case
      case this.oParentObject.w_TIPOAGGIORNAMENTO="I"
        this.w_NOMECURS = this.oParentObject.w_SOMNSOGG.cCursor
      case this.oParentObject.w_TIPOAGGIORNAMENTO="S"
        this.w_NOMECURS = this.oParentObject.w_MOVSRITE.cCursor
      case this.oParentObject.w_TIPOAGGIORNAMENTO="T"
        this.w_NOMECURS = this.oParentObject.w_MOVRITE.cCursor
      case this.oParentObject.w_TIPOAGGIORNAMENTO="P"
        this.w_NOMECURS = this.oParentObject.w_MOVCRITE.cCursor
    endcase
    do case
      case this.pParam = "S"
        if this.oParentObject.w_SELEZ = "S"
          UPDATE (this.w_NOMECURS) SET XCHK = 1
        else
          UPDATE (this.w_NOMECURS) SET XCHK = 0
        endif
        Select (this.w_NOMECURS)
        Go Top
      case this.pParam = "M"
        Select (this.w_NOMECURS)
        do case
          case this.oParentObject.w_TIPOAGGIORNAMENTO="I"
            Scan for xchk = 1
            this.w_COD_FORNITORE = ANCODICE
            * --- Aggiorno gli eventi eccezionali solo se la combo � diversa da nessun aggiornamento
            do case
              case this.oParentObject.w_EVEECC<>"N" And this.oParentObject.w_CODSNS<>"0"
                * --- Write into CONTI
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.CONTI_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTI_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"ANEVEECC ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_EVEECC),'CONTI','ANEVEECC');
                  +",ANCODSNS ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODSNS),'CONTI','ANCODSNS');
                      +i_ccchkf ;
                  +" where ";
                      +"ANTIPCON = "+cp_ToStrODBC("F");
                      +" and ANCODICE = "+cp_ToStrODBC(this.w_COD_FORNITORE);
                         )
                else
                  update (i_cTable) set;
                      ANEVEECC = this.oParentObject.w_EVEECC;
                      ,ANCODSNS = this.oParentObject.w_CODSNS;
                      &i_ccchkf. ;
                   where;
                      ANTIPCON = "F";
                      and ANCODICE = this.w_COD_FORNITORE;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              case this.oParentObject.w_EVEECC<>"N"
                * --- Write into CONTI
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.CONTI_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTI_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"ANEVEECC ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_EVEECC),'CONTI','ANEVEECC');
                      +i_ccchkf ;
                  +" where ";
                      +"ANTIPCON = "+cp_ToStrODBC("F");
                      +" and ANCODICE = "+cp_ToStrODBC(this.w_COD_FORNITORE);
                         )
                else
                  update (i_cTable) set;
                      ANEVEECC = this.oParentObject.w_EVEECC;
                      &i_ccchkf. ;
                   where;
                      ANTIPCON = "F";
                      and ANCODICE = this.w_COD_FORNITORE;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              case this.oParentObject.w_CODSNS<>"0"
                * --- Write into CONTI
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.CONTI_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTI_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"ANCODSNS ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODSNS),'CONTI','ANCODSNS');
                      +i_ccchkf ;
                  +" where ";
                      +"ANTIPCON = "+cp_ToStrODBC("F");
                      +" and ANCODICE = "+cp_ToStrODBC(this.w_COD_FORNITORE);
                         )
                else
                  update (i_cTable) set;
                      ANCODSNS = this.oParentObject.w_CODSNS;
                      &i_ccchkf. ;
                   where;
                      ANTIPCON = "F";
                      and ANCODICE = this.w_COD_FORNITORE;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
            endcase
            Endscan
            Ah_ErrorMsg("Aggiornamento completato",64,"")
            this.oParentObject.NotifyEvent("AggiornaI")
          case this.oParentObject.w_TIPOAGGIORNAMENTO $ "STP"
            Scan for xchk = 1
            * --- Aggiorno il tipo operazione sul movimento ritenuta selezionato
            this.w_SERIALE = MRSERIAL
            do case
              case this.oParentObject.w_TIPOAGGIORNAMENTO="T"
                * --- Write into MOV_RITE
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.MOV_RITE_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.MOV_RITE_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.MOV_RITE_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"MRTIPPAR ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_TIPOPERAZ),'MOV_RITE','MRTIPPAR');
                      +i_ccchkf ;
                  +" where ";
                      +"MRSERIAL = "+cp_ToStrODBC(this.w_SERIALE);
                         )
                else
                  update (i_cTable) set;
                      MRTIPPAR = this.oParentObject.w_TIPOPERAZ;
                      &i_ccchkf. ;
                   where;
                      MRSERIAL = this.w_SERIALE;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              case this.oParentObject.w_TIPOAGGIORNAMENTO="S"
                * --- Write into MOV_RITE
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.MOV_RITE_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.MOV_RITE_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.MOV_RITE_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"MRCODSNS ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODSNS),'MOV_RITE','MRCODSNS');
                      +i_ccchkf ;
                  +" where ";
                      +"MRSERIAL = "+cp_ToStrODBC(this.w_SERIALE);
                         )
                else
                  update (i_cTable) set;
                      MRCODSNS = this.oParentObject.w_CODSNS;
                      &i_ccchkf. ;
                   where;
                      MRSERIAL = this.w_SERIALE;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              case this.oParentObject.w_TIPOAGGIORNAMENTO="P"
                * --- Write into MOV_RITE
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.MOV_RITE_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.MOV_RITE_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.MOV_RITE_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"MRCAURIT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MOV_CAUPRE),'MOV_RITE','MRCAURIT');
                      +i_ccchkf ;
                  +" where ";
                      +"MRSERIAL = "+cp_ToStrODBC(this.w_SERIALE);
                         )
                else
                  update (i_cTable) set;
                      MRCAURIT = this.oParentObject.w_MOV_CAUPRE;
                      &i_ccchkf. ;
                   where;
                      MRSERIAL = this.w_SERIALE;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
            endcase
            Endscan
            Ah_ErrorMsg("Aggiornamento completato",64,"")
            do case
              case this.oParentObject.w_TIPOAGGIORNAMENTO="T"
                this.oParentObject.NotifyEvent("AggiornaT")
              case this.oParentObject.w_TIPOAGGIORNAMENTO="S"
                this.oParentObject.NotifyEvent("AggiornaS")
              case this.oParentObject.w_TIPOAGGIORNAMENTO="P"
                this.oParentObject.NotifyEvent("AggiornaP")
            endcase
        endcase
      case this.pParam="B"
        this.oParentObject.w_SELEZ = "D"
        this.w_ZOOM_MOV = This.oParentObject.w_Movrite
        this.w_ZOOMSMOV = This.oParentObject.w_Movsrite
        this.w_ZOOMCMOV = This.oParentObject.w_Movcrite
        this.w_ZOOMAFOR = This.oParentObject.w_Somnsogg
        do case
          case this.oParentObject.w_TIPOAGGIORNAMENTO="I"
            ZAP IN ( this.w_ZOOM_MOV.cCursor )
            ZAP IN ( this.w_ZOOMSMOV.cCursor )
            ZAP IN ( this.w_ZOOMCMOV.cCursor )
            this.w_ZOOM_MOV.VISIBLE = .F.
            this.w_ZOOMSMOV.VISIBLE = .F.
            this.w_ZOOMCMOV.VISIBLE = .F.
            this.w_ZOOMAFOR.VISIBLE = .T.
            This.oParentObject.notifyevent("AggiornaI")
          case this.oParentObject.w_TIPOAGGIORNAMENTO="S"
            ZAP IN ( this.w_ZOOM_MOV.cCursor )
            ZAP IN ( this.w_ZOOMCMOV.cCursor )
            ZAP IN ( this.w_ZOOMAFOR.cCursor )
            this.w_ZOOM_MOV.VISIBLE = .F.
            this.w_ZOOMSMOV.VISIBLE = .T.
            this.w_ZOOMCMOV.VISIBLE = .F.
            this.w_ZOOMAFOR.VISIBLE = .F.
            This.oParentObject.notifyevent("AggiornaS")
          case this.oParentObject.w_TIPOAGGIORNAMENTO="T"
            ZAP IN ( this.w_ZOOMSMOV.cCursor )
            ZAP IN ( this.w_ZOOMCMOV.cCursor )
            ZAP IN ( this.w_ZOOMAFOR.cCursor )
            this.w_ZOOM_MOV.VISIBLE = .T.
            this.w_ZOOMCMOV.VISIBLE = .F.
            this.w_ZOOMSMOV.VISIBLE = .F.
            this.w_ZOOMAFOR.VISIBLE = .F.
            This.oParentObject.notifyevent("AggiornaT")
          case this.oParentObject.w_TIPOAGGIORNAMENTO="P"
            ZAP IN ( this.w_ZOOMSMOV.cCursor )
            ZAP IN ( this.w_ZOOMAFOR.cCursor )
            ZAP IN ( this.w_ZOOM_MOV.cCursor )
            this.w_ZOOM_MOV.VISIBLE = .F.
            this.w_ZOOMSMOV.VISIBLE = .F.
            this.w_ZOOMCMOV.VISIBLE = .T.
            this.w_ZOOMAFOR.VISIBLE = .F.
            This.oParentObject.notifyevent("AggiornaP")
        endcase
      case this.pParam="A"
        do case
          case this.oParentObject.w_TIPOAGGIORNAMENTO="I"
            This.oParentObject.notifyevent("AggiornaI")
          case this.oParentObject.w_TIPOAGGIORNAMENTO="S"
            This.oParentObject.notifyevent("AggiornaS")
          case this.oParentObject.w_TIPOAGGIORNAMENTO="T"
            This.oParentObject.notifyevent("AggiornaT")
          case this.oParentObject.w_TIPOAGGIORNAMENTO="P"
            This.oParentObject.notifyevent("AggiornaP")
        endcase
      case this.pParam="R"
        this.w_OBJECT = GSRI_AMR("A")
        if !(this.w_OBJECT.bSec1)
          i_retcode = 'stop'
          return
        endif
        this.w_OBJECT.EcpFilter()     
        * --- Valorizzo i campi chiave
        this.w_OBJECT.w_MRSERIAL = IIF(this.oParentObject.w_TIPOAGGIORNAMENTO="S",this.oParentObject.w_MRSERIAL_S,IIF(this.oParentObject.w_TIPOAGGIORNAMENTO="P",this.oParentObject.w_MRSERIAL_C,this.oParentObject.w_MRSERIAL))
        * --- Carico il record richiesto
        this.w_OBJECT.EcpSave()     
      case this.pParam="P"
        this.w_OBJECT = GSAR_AFR()
        if !(this.w_OBJECT.bSec1)
          i_retcode = 'stop'
          return
        endif
        this.w_OBJECT.EcpFilter()     
        * --- Valorizzo i campi chiave
        this.w_OBJECT.w_ANTIPCON = this.oParentObject.w_TIPCON
        this.w_OBJECT.w_ANCODICE = this.oParentObject.w_ANCODICE
        * --- Carico il record richiesto
        this.w_OBJECT.EcpSave()     
    endcase
  endproc


  proc Init(oParentObject,pParam)
    this.pParam=pParam
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='MOV_RITE'
    this.cWorkTables[2]='CONTI'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParam"
endproc
