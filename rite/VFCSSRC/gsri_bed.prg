* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsri_bed                                                        *
*              Estrazioni dati 770                                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2015-05-11                                                      *
* Last revis.: 2018-01-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParame
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsri_bed",oParentObject,m.pParame)
return(i_retval)

define class tgsri_bed as StdBatch
  * --- Local variables
  pParame = space(1)
  w_OLDVERSIONE = .f.
  w_NUMMOV = 0
  w_NUMMOVAGG = 0
  w_RHSERIAL = space(10)
  w_STSERIAL = space(10)
  w_SERIAL = 0
  w_NUMSER = 0
  w_RH__ANNO = 0
  w_RHCODCON = space(15)
  w_RHCAUPRE = space(2)
  w_AGGSER = space(10)
  w_NEWMOVRITE = 0
  w_NEWESCLRITE = 0
  w_AGGMOVRITE = 0
  w_AGGESCLRITE = 0
  w_oMESS = .NULL.
  w_oPART = .NULL.
  w_NUMMOVELI = 0
  w_NUMESCLRIGA = 0
  w_NUMVERSCA = 0
  w_NUMVER = 0
  w_NUMRECST = 0
  w_NUMRSTAGG = 0
  w_CHSERIAL = space(10)
  w_CH__ANNO = 0
  w_CHCODCON = space(15)
  w_CHCAUPRE = space(2)
  w_GDSERIAL = space(10)
  w_TipoEla = space(1)
  * --- WorkFile variables
  DATESDRH_idx=0
  DATESTRH_idx=0
  TMPDATIESTRATTI_idx=0
  DATESDST_idx=0
  CUDTETDH_idx=0
  CUDTETRH_idx=0
  TMPTOTRECH_idx=0
  CUDDPTDH_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch utilizzato per l'estrazione dei movimenti ritenute
    this.w_TipoEla = "E"
    do case
      case this.pParame="E"
        this.oParentObject.w_Msg = ""
        if this.oParentObject.w_RecordH="S"
          AddMsgNL("Estrazione dati record H",this)
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        if this.oParentObject.w_RecordCU="S"
          AddMsgNL("Estrazione certificazioni uniche",this)
          this.Pag5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        if this.oParentObject.w_RecordST="S"
          AddMsgNL("Estrazione dati record ST",this)
          this.Pag4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      case this.pParame="C"
        if this.oParentObject.w_RecordH="S" and this.oParentObject.w_ElirecordH<>"S"
          ah_ErrorMsg("Attenzione: disattivando il flag si potrebbe avere%0una duplicazione dei movimenti ritenute nei dati gi� estratti")
        endif
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if this.oParentObject.w_ELIRECORDH="S"
      AddMsgNL(Space(5)+"Eliminazione estrazioni precedenti",this)
      * --- Cancello il dettaglio dei dati estratti del record H, effettuo
      *     due cancellazioni differenti a seconda abbia attivato il flag
      *     di cancellazione dei dati caricati manualmente. Controllo
      *     se sto estraendo i dati del 770 oppure della Certificazione unica
      if this.oParentObject.w_ELIRECORDHM="S"
        * --- Delete from DATESDRH
        i_nConn=i_TableProp[this.DATESDRH_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DATESDRH_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          local i_cQueryTable,i_cWhere
          i_cQueryTable=cp_getTempTableName(i_nConn)
          i_cWhere=i_cTable+".RHSERIAL = "+i_cQueryTable+".RHSERIAL";
                +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
        
          do vq_exec with '..\rite\exe\query\gsri19bed',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
                +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
      else
        * --- Delete from DATESDRH
        i_nConn=i_TableProp[this.DATESDRH_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DATESDRH_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          local i_cQueryTable,i_cWhere
          i_cQueryTable=cp_getTempTableName(i_nConn)
          i_cWhere=i_cTable+".RHSERIAL = "+i_cQueryTable+".RHSERIAL";
                +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
        
          do vq_exec with '..\rite\exe\query\gsri_bed',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
                +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
      endif
      * --- Delete from DATESTRH
      i_nConn=i_TableProp[this.DATESTRH_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DATESTRH_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        local i_cQueryTable,i_cWhere
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_cWhere=i_cTable+".RHSERIAL = "+i_cQueryTable+".RHSERIAL";
      
        do vq_exec with '..\rite\exe\query\gsri1bed',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
              +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
    endif
    AddMsgNL(Space(5)+"Estrazione dati ",this)
    * --- Create temporary table TMPDATIESTRATTI
    i_nIdx=cp_AddTableDef('TMPDATIESTRATTI') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('..\rite\exe\query\gsri2bed',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPDATIESTRATTI_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Elimino dal cursore temporaneo Tmpdatiestratti tutti i movimenti ritenute
    *     che sono presenti in un dato estratto che � stato comunicato
    * --- Delete from TMPDATIESTRATTI
    i_nConn=i_TableProp[this.TMPDATIESTRATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPDATIESTRATTI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      local i_cQueryTable,i_cWhere
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_cWhere=i_cTable+".MRSERIAL = "+i_cQueryTable+".MRSERIAL";
    
      do vq_exec with 'Gsri38bed',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
            +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Verifico se nel cursore temporaneo TmpDatiEstratti sono presenti dei
    *     record ( Anno, codice conto , causale prestazione ) che sono gi� presenti
    *     nella tabella Datestrh. Se esistono aggiorno il campo contenente il seriale
    *     ed il campo di appoggio "Aggiornamento"
    * --- Write into TMPDATIESTRATTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMPDATIESTRATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPDATIESTRATTI_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="RH__ANNO,RHCODCON,RHCAUPRE"
      do vq_exec with 'GSRI4BED',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPDATIESTRATTI_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="TMPDATIESTRATTI.RH__ANNO = _t2.RH__ANNO";
              +" and "+"TMPDATIESTRATTI.RHCODCON = _t2.RHCODCON";
              +" and "+"TMPDATIESTRATTI.RHCAUPRE = _t2.RHCAUPRE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"RHSERIAL = _t2.Rhserial";
          +",AGGIORNAMENTO = _t2.Aggiornamento";
          +",NUMMAXRIGA = _t2.Nummaxriga";
          +i_ccchkf;
          +" from "+i_cTable+" TMPDATIESTRATTI, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="TMPDATIESTRATTI.RH__ANNO = _t2.RH__ANNO";
              +" and "+"TMPDATIESTRATTI.RHCODCON = _t2.RHCODCON";
              +" and "+"TMPDATIESTRATTI.RHCAUPRE = _t2.RHCAUPRE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI, "+i_cQueryTable+" _t2 set ";
          +"TMPDATIESTRATTI.RHSERIAL = _t2.Rhserial";
          +",TMPDATIESTRATTI.AGGIORNAMENTO = _t2.Aggiornamento";
          +",TMPDATIESTRATTI.NUMMAXRIGA = _t2.Nummaxriga";
          +Iif(Empty(i_ccchkf),"",",TMPDATIESTRATTI.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="TMPDATIESTRATTI.RH__ANNO = t2.RH__ANNO";
              +" and "+"TMPDATIESTRATTI.RHCODCON = t2.RHCODCON";
              +" and "+"TMPDATIESTRATTI.RHCAUPRE = t2.RHCAUPRE";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI set (";
          +"RHSERIAL,";
          +"AGGIORNAMENTO,";
          +"NUMMAXRIGA";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.Rhserial,";
          +"t2.Aggiornamento,";
          +"t2.Nummaxriga";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="TMPDATIESTRATTI.RH__ANNO = _t2.RH__ANNO";
              +" and "+"TMPDATIESTRATTI.RHCODCON = _t2.RHCODCON";
              +" and "+"TMPDATIESTRATTI.RHCAUPRE = _t2.RHCAUPRE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI set ";
          +"RHSERIAL = _t2.Rhserial";
          +",AGGIORNAMENTO = _t2.Aggiornamento";
          +",NUMMAXRIGA = _t2.Nummaxriga";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".RH__ANNO = "+i_cQueryTable+".RH__ANNO";
              +" and "+i_cTable+".RHCODCON = "+i_cQueryTable+".RHCODCON";
              +" and "+i_cTable+".RHCAUPRE = "+i_cQueryTable+".RHCAUPRE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"RHSERIAL = (select Rhserial from "+i_cQueryTable+" where "+i_cWhere+")";
          +",AGGIORNAMENTO = (select Aggiornamento from "+i_cQueryTable+" where "+i_cWhere+")";
          +",NUMMAXRIGA = (select Nummaxriga from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Devo inserire il flag "Escludi dalla generazione" sul dettaglio dei dati estratti,
    *     nel caso in cui il movimento ritenute risulta gi� inserito
    * --- Write into TMPDATIESTRATTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMPDATIESTRATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPDATIESTRATTI_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="RHSERIAL,MRSERIAL"
      do vq_exec with 'Gsri20bed',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPDATIESTRATTI_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="TMPDATIESTRATTI.RHSERIAL = _t2.RHSERIAL";
              +" and "+"TMPDATIESTRATTI.MRSERIAL = _t2.MRSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"RHESCLGE = _t2.RHESCLGE";
          +i_ccchkf;
          +" from "+i_cTable+" TMPDATIESTRATTI, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="TMPDATIESTRATTI.RHSERIAL = _t2.RHSERIAL";
              +" and "+"TMPDATIESTRATTI.MRSERIAL = _t2.MRSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI, "+i_cQueryTable+" _t2 set ";
          +"TMPDATIESTRATTI.RHESCLGE = _t2.RHESCLGE";
          +Iif(Empty(i_ccchkf),"",",TMPDATIESTRATTI.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="TMPDATIESTRATTI.RHSERIAL = t2.RHSERIAL";
              +" and "+"TMPDATIESTRATTI.MRSERIAL = t2.MRSERIAL";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI set (";
          +"RHESCLGE";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.RHESCLGE";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="TMPDATIESTRATTI.RHSERIAL = _t2.RHSERIAL";
              +" and "+"TMPDATIESTRATTI.MRSERIAL = _t2.MRSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI set ";
          +"RHESCLGE = _t2.RHESCLGE";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".RHSERIAL = "+i_cQueryTable+".RHSERIAL";
              +" and "+i_cTable+".MRSERIAL = "+i_cQueryTable+".MRSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"RHESCLGE = (select RHESCLGE from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    this.w_NUMESCLRIGA = i_Rows
    * --- Creo un cursore di appoggio che conterr� i movimenti ritenute che saranno
    *     scartati perch� hanno indicato come causale prestazione 'F' oppure i
    *     dati estratti che verranno esclusi dalla generazione perch� hanno dei valori
    *     incongruenti
    Vq_Exec("Gsri14bed", this, "ElencoEsclusioni")
    * --- Elimino dal cursore temporaneo TmpDatiEstratti i record che hanno 
    *     il campo Cauprest valorizzato ad 'S'
    * --- Delete from TMPDATIESTRATTI
    i_nConn=i_TableProp[this.TMPDATIESTRATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPDATIESTRATTI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"CAUPREST = "+cp_ToStrODBC("S");
             )
    else
      delete from (i_cTable) where;
            CAUPREST = "S";

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    this.w_NUMMOVELI = i_Rows
    * --- La variabile w_NumMov contiene il numero di record che verranno aggiunti
    *     alla tabella "Datestrh"
    * --- Select from GSRI7BED
    do vq_exec with 'GSRI7BED',this,'_Curs_GSRI7BED','',.f.,.t.
    if used('_Curs_GSRI7BED')
      select _Curs_GSRI7BED
      locate for 1=1
      do while not(eof())
      this.w_NUMMOV = NUMERO
      this.w_NEWMOVRITE = NEWMOVRITE
        select _Curs_GSRI7BED
        continue
      enddo
      use
    endif
    * --- La variabile w_NumMovAgg contiene il numero di record che aggiorneranno
    *     la tabella "Datestrh"
    * --- Select from GSRI16BED
    do vq_exec with 'GSRI16BED',this,'_Curs_GSRI16BED','',.f.,.t.
    if used('_Curs_GSRI16BED')
      select _Curs_GSRI16BED
      locate for 1=1
      do while not(eof())
      this.w_NUMMOVAGG = NUMERO
      this.w_AGGMOVRITE = AGGMOVRITE
        select _Curs_GSRI16BED
        continue
      enddo
      use
    endif
    if this.w_NUMMOV<>0 OR this.w_NUMMOVAGG<>0
      * --- Try
      local bErr_02DB6DB0
      bErr_02DB6DB0=bTrsErr
      this.Try_02DB6DB0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        * --- accept error
        bTrsErr=.f.
        ah_ErrorMsg("Errore nella creazione dei dati estratti (%1)", "!", "", i_trsmsg)
      endif
      bTrsErr=bTrsErr or bErr_02DB6DB0
      * --- End
    else
      ah_ErrorMsg("Nessun movimento da generare")
      if RECCOUNT("ElencoEsclusioni")>0 AND ah_YesNo("Si vuole stampare l'elenco delle anomalie riscontrate?")
        Select * from ElencoEsclusioni into cursor __TMP__
        Cp_Chprn("..\rite\exe\query\gsri_bed", , this.oParentObject)
      endif
    endif
  endproc
  proc Try_02DB6DB0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    AddMsgNL(Space(5)+"Generazione progressivi",this)
    * --- Verifico la versione del database 
    this.w_RHSERIAL = space(10)
    w_Conn=i_TableProp[this.DATESTRH_IDX, 3] 
 cp_NextTableProg(this, w_Conn, "RHSER", "i_codazi,w_RHSERIAL")
    this.w_SERIAL = VAL(this.w_RHSERIAL)-1
    this.w_RHSERIAL = cp_StrZeroPad( this.w_SERIAL+this.w_NUMMOV ,10)
    cp_NextTableProg(this, w_Conn, "RHSER", "i_codazi,w_RHSERIAL")
    if upper(CP_DBTYPE)="SQLSERVER"
      sqlexec(w_Conn,"Select convert(char,SERVERPROPERTY('productversion')) As Version", "ChkVersion")
      if USED("ChkVersion") AND VAL(LEFT(ChkVersion.Version,AT(".",ChkVersion.Version)-1)) < 9
        this.w_OLDVERSIONE = .t.
      endif
    else
      if TYPE("g_ORACLEVERS")="C" AND g_ORACLEVERS="8"
        this.w_OLDVERSIONE = .t.
      endif
    endif
    if this.w_NUMMOV<>0
      * --- Genero progressivo seriale
      if this.w_OLDVERSIONE
        * --- Select from GSRI8BED
        do vq_exec with 'GSRI8BED',this,'_Curs_GSRI8BED','',.f.,.t.
        if used('_Curs_GSRI8BED')
          select _Curs_GSRI8BED
          locate for 1=1
          do while not(eof())
          this.w_NUMSER = this.w_NUMSER + 1
          this.w_RH__ANNO = _Curs_GSRI8BED.RH__ANNO
          this.w_RHCODCON = _Curs_GSRI8BED.RHCODCON
          this.w_RHCAUPRE = _Curs_GSRI8BED.RHCAUPRE
          this.w_AGGSER = cp_StrZeroPad(this.w_NUMSER + this.w_SERIAL, 10)
          * --- Write into TMPDATIESTRATTI
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.TMPDATIESTRATTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPDATIESTRATTI_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPDATIESTRATTI_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"RHSERIAL ="+cp_NullLink(cp_ToStrODBC(this.w_AGGSER),'TMPDATIESTRATTI','RHSERIAL');
                +i_ccchkf ;
            +" where ";
                +"RH__ANNO = "+cp_ToStrODBC(this.w_RH__ANNO);
                +" and RHCODCON = "+cp_ToStrODBC(this.w_RHCODCON);
                +" and RHCAUPRE = "+cp_ToStrODBC(this.w_RHCAUPRE);
                   )
          else
            update (i_cTable) set;
                RHSERIAL = this.w_AGGSER;
                &i_ccchkf. ;
             where;
                RH__ANNO = this.w_RH__ANNO;
                and RHCODCON = this.w_RHCODCON;
                and RHCAUPRE = this.w_RHCAUPRE;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
            select _Curs_GSRI8BED
            continue
          enddo
          use
        endif
      else
        * --- Write into TMPDATIESTRATTI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.TMPDATIESTRATTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPDATIESTRATTI_idx,2])
        if i_nConn<>0
          local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
          declare i_aIndex[1]
          i_cQueryTable=cp_getTempTableName(i_nConn)
          i_aIndex(1)="RH__ANNO,RHCODCON,RHCAUPRE"
          do vq_exec with 'GSRI9BED',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPDATIESTRATTI_idx,i_nConn)	
          i_cDB=cp_GetDatabaseType(i_nConn)
          do case
          case i_cDB="SQLServer"
            i_cWhere="TMPDATIESTRATTI.RH__ANNO = _t2.RH__ANNO";
                  +" and "+"TMPDATIESTRATTI.RHCODCON = _t2.RHCODCON";
                  +" and "+"TMPDATIESTRATTI.RHCAUPRE = _t2.RHCAUPRE";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"RHSERIAL = _t2.RHSERIAL";
              +i_ccchkf;
              +" from "+i_cTable+" TMPDATIESTRATTI, "+i_cQueryTable+" _t2 where "+i_cWhere)
          case i_cDB="MySQL"
            i_cWhere="TMPDATIESTRATTI.RH__ANNO = _t2.RH__ANNO";
                  +" and "+"TMPDATIESTRATTI.RHCODCON = _t2.RHCODCON";
                  +" and "+"TMPDATIESTRATTI.RHCAUPRE = _t2.RHCAUPRE";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI, "+i_cQueryTable+" _t2 set ";
              +"TMPDATIESTRATTI.RHSERIAL = _t2.RHSERIAL";
              +Iif(Empty(i_ccchkf),"",",TMPDATIESTRATTI.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
              +" where "+i_cWhere)
          case i_cDB="Oracle"
            i_cWhere="TMPDATIESTRATTI.RH__ANNO = t2.RH__ANNO";
                  +" and "+"TMPDATIESTRATTI.RHCODCON = t2.RHCODCON";
                  +" and "+"TMPDATIESTRATTI.RHCAUPRE = t2.RHCAUPRE";
            
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI set (";
              +"RHSERIAL";
              +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
              +"t2.RHSERIAL";
              +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
              +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
          case i_cDB="PostgreSQL"
            i_cWhere="TMPDATIESTRATTI.RH__ANNO = _t2.RH__ANNO";
                  +" and "+"TMPDATIESTRATTI.RHCODCON = _t2.RHCODCON";
                  +" and "+"TMPDATIESTRATTI.RHCAUPRE = _t2.RHCAUPRE";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI set ";
              +"RHSERIAL = _t2.RHSERIAL";
              +i_ccchkf;
              +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
          otherwise
            i_cWhere=i_cTable+".RH__ANNO = "+i_cQueryTable+".RH__ANNO";
                  +" and "+i_cTable+".RHCODCON = "+i_cQueryTable+".RHCODCON";
                  +" and "+i_cTable+".RHCAUPRE = "+i_cQueryTable+".RHCAUPRE";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"RHSERIAL = (select RHSERIAL from "+i_cQueryTable+" where "+i_cWhere+")";
              +i_ccchkf;
              +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          endcase
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
      AddMsgNL(Space(5)+"Inserimento dati",this)
      * --- Insert into DATESTRH
      i_nConn=i_TableProp[this.DATESTRH_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DATESTRH_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"Gsri10bed",this.DATESTRH_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
    if this.w_NUMMOVAGG<>0
      * --- Se il record ha il campo "Aggiornamento" valorizzato ad 'S' non deve essere
      *     effettuato l'inserimento nella tabella Datestrh ma devono solo essere aggiornati
      *     i dati
      AddMsgNL(Space(5)+"Aggiornamento dati ",this)
      * --- Write into DATESTRH
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DATESTRH_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DATESTRH_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="RHSERIAL"
        do vq_exec with 'Gsri5bed',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.DATESTRH_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="DATESTRH.RHSERIAL = _t2.RHSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"RHPERFIS = _t2.RHPERFIS";
            +",RHPEREST = _t2.RHPEREST";
            +",RHCODFIS = _t2.RHCODFIS";
            +",RHDESCRI = _t2.RHDESCRI";
            +",RHCOGNOM = _t2.RHCOGNOM";
            +",RH__NOME = _t2.RH__NOME";
            +",RH_SESSO = _t2.RH_SESSO";
            +",RHDATNAS = _t2.RHDATNAS";
            +",RHCOMEST = _t2.RHCOMEST";
            +",RHPRONAS = _t2.RHPRONAS";
            +",RHCATPAR = _t2.RHCATPAR";
            +",RHEVEECC = _t2.RHEVEECC";
            +",RHCOMUNE = _t2.RHCOMUNE";
            +",RHPROVIN = _t2.RHPROVIN";
            +",RHCODCOM = _t2.RHCODCOM";
            +",RHCOFISC = _t2.RHCOFISC";
            +",RHCITEST = _t2.RHCITEST";
            +",RHINDEST = _t2.RHINDEST";
            +",RHSTAEST = _t2.RHSTAEST";
            +",RHSCLGEN = _t2.RHSCLGEN";
            +",RHDAVERI = _t2.RHDAVERI";
            +i_ccchkf;
            +" from "+i_cTable+" DATESTRH, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="DATESTRH.RHSERIAL = _t2.RHSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DATESTRH, "+i_cQueryTable+" _t2 set ";
            +"DATESTRH.RHPERFIS = _t2.RHPERFIS";
            +",DATESTRH.RHPEREST = _t2.RHPEREST";
            +",DATESTRH.RHCODFIS = _t2.RHCODFIS";
            +",DATESTRH.RHDESCRI = _t2.RHDESCRI";
            +",DATESTRH.RHCOGNOM = _t2.RHCOGNOM";
            +",DATESTRH.RH__NOME = _t2.RH__NOME";
            +",DATESTRH.RH_SESSO = _t2.RH_SESSO";
            +",DATESTRH.RHDATNAS = _t2.RHDATNAS";
            +",DATESTRH.RHCOMEST = _t2.RHCOMEST";
            +",DATESTRH.RHPRONAS = _t2.RHPRONAS";
            +",DATESTRH.RHCATPAR = _t2.RHCATPAR";
            +",DATESTRH.RHEVEECC = _t2.RHEVEECC";
            +",DATESTRH.RHCOMUNE = _t2.RHCOMUNE";
            +",DATESTRH.RHPROVIN = _t2.RHPROVIN";
            +",DATESTRH.RHCODCOM = _t2.RHCODCOM";
            +",DATESTRH.RHCOFISC = _t2.RHCOFISC";
            +",DATESTRH.RHCITEST = _t2.RHCITEST";
            +",DATESTRH.RHINDEST = _t2.RHINDEST";
            +",DATESTRH.RHSTAEST = _t2.RHSTAEST";
            +",DATESTRH.RHSCLGEN = _t2.RHSCLGEN";
            +",DATESTRH.RHDAVERI = _t2.RHDAVERI";
            +Iif(Empty(i_ccchkf),"",",DATESTRH.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="DATESTRH.RHSERIAL = t2.RHSERIAL";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DATESTRH set (";
            +"RHPERFIS,";
            +"RHPEREST,";
            +"RHCODFIS,";
            +"RHDESCRI,";
            +"RHCOGNOM,";
            +"RH__NOME,";
            +"RH_SESSO,";
            +"RHDATNAS,";
            +"RHCOMEST,";
            +"RHPRONAS,";
            +"RHCATPAR,";
            +"RHEVEECC,";
            +"RHCOMUNE,";
            +"RHPROVIN,";
            +"RHCODCOM,";
            +"RHCOFISC,";
            +"RHCITEST,";
            +"RHINDEST,";
            +"RHSTAEST,";
            +"RHSCLGEN,";
            +"RHDAVERI";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.RHPERFIS,";
            +"t2.RHPEREST,";
            +"t2.RHCODFIS,";
            +"t2.RHDESCRI,";
            +"t2.RHCOGNOM,";
            +"t2.RH__NOME,";
            +"t2.RH_SESSO,";
            +"t2.RHDATNAS,";
            +"t2.RHCOMEST,";
            +"t2.RHPRONAS,";
            +"t2.RHCATPAR,";
            +"t2.RHEVEECC,";
            +"t2.RHCOMUNE,";
            +"t2.RHPROVIN,";
            +"t2.RHCODCOM,";
            +"t2.RHCOFISC,";
            +"t2.RHCITEST,";
            +"t2.RHINDEST,";
            +"t2.RHSTAEST,";
            +"t2.RHSCLGEN,";
            +"t2.RHDAVERI";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="DATESTRH.RHSERIAL = _t2.RHSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DATESTRH set ";
            +"RHPERFIS = _t2.RHPERFIS";
            +",RHPEREST = _t2.RHPEREST";
            +",RHCODFIS = _t2.RHCODFIS";
            +",RHDESCRI = _t2.RHDESCRI";
            +",RHCOGNOM = _t2.RHCOGNOM";
            +",RH__NOME = _t2.RH__NOME";
            +",RH_SESSO = _t2.RH_SESSO";
            +",RHDATNAS = _t2.RHDATNAS";
            +",RHCOMEST = _t2.RHCOMEST";
            +",RHPRONAS = _t2.RHPRONAS";
            +",RHCATPAR = _t2.RHCATPAR";
            +",RHEVEECC = _t2.RHEVEECC";
            +",RHCOMUNE = _t2.RHCOMUNE";
            +",RHPROVIN = _t2.RHPROVIN";
            +",RHCODCOM = _t2.RHCODCOM";
            +",RHCOFISC = _t2.RHCOFISC";
            +",RHCITEST = _t2.RHCITEST";
            +",RHINDEST = _t2.RHINDEST";
            +",RHSTAEST = _t2.RHSTAEST";
            +",RHSCLGEN = _t2.RHSCLGEN";
            +",RHDAVERI = _t2.RHDAVERI";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".RHSERIAL = "+i_cQueryTable+".RHSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"RHPERFIS = (select RHPERFIS from "+i_cQueryTable+" where "+i_cWhere+")";
            +",RHPEREST = (select RHPEREST from "+i_cQueryTable+" where "+i_cWhere+")";
            +",RHCODFIS = (select RHCODFIS from "+i_cQueryTable+" where "+i_cWhere+")";
            +",RHDESCRI = (select RHDESCRI from "+i_cQueryTable+" where "+i_cWhere+")";
            +",RHCOGNOM = (select RHCOGNOM from "+i_cQueryTable+" where "+i_cWhere+")";
            +",RH__NOME = (select RH__NOME from "+i_cQueryTable+" where "+i_cWhere+")";
            +",RH_SESSO = (select RH_SESSO from "+i_cQueryTable+" where "+i_cWhere+")";
            +",RHDATNAS = (select RHDATNAS from "+i_cQueryTable+" where "+i_cWhere+")";
            +",RHCOMEST = (select RHCOMEST from "+i_cQueryTable+" where "+i_cWhere+")";
            +",RHPRONAS = (select RHPRONAS from "+i_cQueryTable+" where "+i_cWhere+")";
            +",RHCATPAR = (select RHCATPAR from "+i_cQueryTable+" where "+i_cWhere+")";
            +",RHEVEECC = (select RHEVEECC from "+i_cQueryTable+" where "+i_cWhere+")";
            +",RHCOMUNE = (select RHCOMUNE from "+i_cQueryTable+" where "+i_cWhere+")";
            +",RHPROVIN = (select RHPROVIN from "+i_cQueryTable+" where "+i_cWhere+")";
            +",RHCODCOM = (select RHCODCOM from "+i_cQueryTable+" where "+i_cWhere+")";
            +",RHCOFISC = (select RHCOFISC from "+i_cQueryTable+" where "+i_cWhere+")";
            +",RHCITEST = (select RHCITEST from "+i_cQueryTable+" where "+i_cWhere+")";
            +",RHINDEST = (select RHINDEST from "+i_cQueryTable+" where "+i_cWhere+")";
            +",RHSTAEST = (select RHSTAEST from "+i_cQueryTable+" where "+i_cWhere+")";
            +",RHSCLGEN = (select RHSCLGEN from "+i_cQueryTable+" where "+i_cWhere+")";
            +",RHDAVERI = (select RHDAVERI from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    if this.w_OLDVERSIONE
      * --- Write into TMPDATIESTRATTI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMPDATIESTRATTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPDATIESTRATTI_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="RHSERIAL,MRSERIAL"
        do vq_exec with 'GSRI11BED',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPDATIESTRATTI_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="TMPDATIESTRATTI.RHSERIAL = _t2.RHSERIAL";
                +" and "+"TMPDATIESTRATTI.MRSERIAL = _t2.MRSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CPROWNUM = _t2.CPROWNUM";
            +i_ccchkf;
            +" from "+i_cTable+" TMPDATIESTRATTI, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="TMPDATIESTRATTI.RHSERIAL = _t2.RHSERIAL";
                +" and "+"TMPDATIESTRATTI.MRSERIAL = _t2.MRSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI, "+i_cQueryTable+" _t2 set ";
            +"TMPDATIESTRATTI.CPROWNUM = _t2.CPROWNUM";
            +Iif(Empty(i_ccchkf),"",",TMPDATIESTRATTI.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="TMPDATIESTRATTI.RHSERIAL = t2.RHSERIAL";
                +" and "+"TMPDATIESTRATTI.MRSERIAL = t2.MRSERIAL";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI set (";
            +"CPROWNUM";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.CPROWNUM";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="TMPDATIESTRATTI.RHSERIAL = _t2.RHSERIAL";
                +" and "+"TMPDATIESTRATTI.MRSERIAL = _t2.MRSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI set ";
            +"CPROWNUM = _t2.CPROWNUM";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".RHSERIAL = "+i_cQueryTable+".RHSERIAL";
                +" and "+i_cTable+".MRSERIAL = "+i_cQueryTable+".MRSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CPROWNUM = (select CPROWNUM from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    else
      * --- Write into TMPDATIESTRATTI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMPDATIESTRATTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPDATIESTRATTI_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="RHSERIAL,MRSERIAL"
        do vq_exec with 'GSRI12BED',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPDATIESTRATTI_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="TMPDATIESTRATTI.RHSERIAL = _t2.RHSERIAL";
                +" and "+"TMPDATIESTRATTI.MRSERIAL = _t2.MRSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CPROWNUM = _t2.CPROWNUM";
            +i_ccchkf;
            +" from "+i_cTable+" TMPDATIESTRATTI, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="TMPDATIESTRATTI.RHSERIAL = _t2.RHSERIAL";
                +" and "+"TMPDATIESTRATTI.MRSERIAL = _t2.MRSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI, "+i_cQueryTable+" _t2 set ";
            +"TMPDATIESTRATTI.CPROWNUM = _t2.CPROWNUM";
            +Iif(Empty(i_ccchkf),"",",TMPDATIESTRATTI.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="TMPDATIESTRATTI.RHSERIAL = t2.RHSERIAL";
                +" and "+"TMPDATIESTRATTI.MRSERIAL = t2.MRSERIAL";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI set (";
            +"CPROWNUM";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.CPROWNUM";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="TMPDATIESTRATTI.RHSERIAL = _t2.RHSERIAL";
                +" and "+"TMPDATIESTRATTI.MRSERIAL = _t2.MRSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI set ";
            +"CPROWNUM = _t2.CPROWNUM";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".RHSERIAL = "+i_cQueryTable+".RHSERIAL";
                +" and "+i_cTable+".MRSERIAL = "+i_cQueryTable+".MRSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CPROWNUM = (select CPROWNUM from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- Nel caso in cui siano presenti record che aggiornano la tabella Datestrh
    *     il numero di riga deve essere ricalcolato
    if this.w_NUMMOVAGG<>0
      * --- Write into TMPDATIESTRATTI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMPDATIESTRATTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPDATIESTRATTI_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="RHSERIAL,MRSERIAL"
        do vq_exec with 'GSRI17BED',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPDATIESTRATTI_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="TMPDATIESTRATTI.RHSERIAL = _t2.RHSERIAL";
                +" and "+"TMPDATIESTRATTI.MRSERIAL = _t2.MRSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CPROWNUM = _t2.CPROWNUM";
            +i_ccchkf;
            +" from "+i_cTable+" TMPDATIESTRATTI, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="TMPDATIESTRATTI.RHSERIAL = _t2.RHSERIAL";
                +" and "+"TMPDATIESTRATTI.MRSERIAL = _t2.MRSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI, "+i_cQueryTable+" _t2 set ";
            +"TMPDATIESTRATTI.CPROWNUM = _t2.CPROWNUM";
            +Iif(Empty(i_ccchkf),"",",TMPDATIESTRATTI.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="TMPDATIESTRATTI.RHSERIAL = t2.RHSERIAL";
                +" and "+"TMPDATIESTRATTI.MRSERIAL = t2.MRSERIAL";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI set (";
            +"CPROWNUM";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.CPROWNUM";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="TMPDATIESTRATTI.RHSERIAL = _t2.RHSERIAL";
                +" and "+"TMPDATIESTRATTI.MRSERIAL = _t2.MRSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI set ";
            +"CPROWNUM = _t2.CPROWNUM";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".RHSERIAL = "+i_cQueryTable+".RHSERIAL";
                +" and "+i_cTable+".MRSERIAL = "+i_cQueryTable+".MRSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CPROWNUM = (select CPROWNUM from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- Insert into DATESDRH
    i_nConn=i_TableProp[this.DATESDRH_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DATESDRH_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\rite\exe\query\gsri13bed",this.DATESDRH_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    VQ_EXEC("..\rite\exe\query\gsri18bed", this, "__TMP1__")
    if USED("__TMP1__")
      Select __TMP1__
      COUNT FOR NEWESCLRITE="S" TO this.w_NEWESCLRITE
      COUNT FOR AGGESCLRITE="S" TO this.w_AGGESCLRITE
    endif
    this.w_oMESS=createobject("AH_Message")
    this.w_oMESS.AddMsgPartNL("Generazione completata.")     
    if this.w_NUMMOV<>0
      this.w_oPART = this.w_oMESS.AddMsgPartNL("Creati %1 dati estratti (di cui  n. %2 da verificare) da n. %3 movimenti ritenute")
      this.w_oPART.AddParam(ALLTRIM(STR(this.w_NUMMOV)))     
      this.w_oPART.AddParam(ALLTRIM(STR(this.w_NEWESCLRITE)))     
      this.w_oPART.AddParam(ALLTRIM(STR(this.w_NEWMOVRITE)))     
    endif
    if this.w_NUMMOVAGG<>0
      this.w_oPART = this.w_oMESS.AddMsgPartNL("Aggiornati %1 dati estratti (di cui  n. %2 da verificare) da n. %3 movimenti ritenute")
      this.w_oPART.AddParam(ALLTRIM(STR(this.w_NUMMOVAGG)))     
      this.w_oPART.AddParam(ALLTRIM(STR(this.w_AGGESCLRITE)))     
      this.w_oPART.AddParam(ALLTRIM(STR(this.w_AGGMOVRITE)))     
    endif
    if this.w_NUMMOVELI<>0
      this.w_oPART = this.w_oMESS.AddMsgPartNL("Scartati n. %1 movimenti ritenute")
      this.w_oPART.AddParam(ALLTRIM(STR(this.w_NUMMOVELI)))     
    endif
    if this.w_NUMESCLRIGA<>0
      this.w_oPART = this.w_oMESS.AddMsgPartNL("Esclusi n. %1 movimenti ritenute")
      this.w_oPART.AddParam(ALLTRIM(STR(this.w_NUMESCLRIGA)))     
      * --- Nel caso in cui esistano movimenti ritenute duplicati perch� gi� estratti
      *     devo aggiungerli al cursore di stampa 
    endif
    this.w_oMESS.AH_ErrorMsg(48)     
    if RECCOUNT("ElencoEsclusioni")>0 AND ah_YesNo("Si vuole stampare l'elenco delle anomalie riscontrate?")
      Select * from ElencoEsclusioni into cursor __TMP__
      Cp_Chprn("..\rite\exe\query\gsri_bed", , this.oParentObject)
    endif
    return


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if USED("ChkVersion")
      USE IN ChkVersion
    endif
    if USED("__TMP1__")
      USE IN __TMP1__
    endif
    if USED("ElencoEsclusioni")
      USE IN ElencoEsclusioni
    endif
    * --- Drop temporary table TMPDATIESTRATTI
    i_nIdx=cp_GetTableDefIdx('TMPDATIESTRATTI')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPDATIESTRATTI')
    endif
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Se l'anno di estrazione � maggiore del 2014 devo verifciare che non siano
    *     presenti record ST non ancora generati. Questa operazione deve essere effettuata
    *     solo se non � stato attivato il flag di "Elimina dati caricati manualmente"
    if this.oParentObject.w_Anno>=2015 And this.oParentObject.w_ElirecordST<>"S"
      * --- Select from Gsri40bed
      do vq_exec with 'Gsri40bed',this,'_Curs_Gsri40bed','',.f.,.t.
      if used('_Curs_Gsri40bed')
        select _Curs_Gsri40bed
        locate for 1=1
        do while not(eof())
        this.w_GDSERIAL = _Curs_Gsri40bed.Stserial
          select _Curs_Gsri40bed
          continue
        enddo
        use
      endif
      if Not Empty(this.w_Gdserial)
        ah_ErrorMsg("Esiste gi� un record ST non comunicato per l'anno %1%0occorre procedere alla variazione dello stesso.%0L'estrazione non � consentita", 48,"",Alltrim(Str(this.oParentObject.w_Anno)))
        i_retcode = 'stop'
        return
      endif
    endif
    AddMsgNL(Space(5)+"Eliminazione estrazioni precedenti",this)
    if this.oParentObject.w_ELIRECORDST="S"
      * --- Delete from DATESDST
      i_nConn=i_TableProp[this.DATESDST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DATESDST_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        local i_cQueryTable,i_cWhere
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_cWhere=i_cTable+".STSERIAL = "+i_cQueryTable+".STSERIAL";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
      
        do vq_exec with '..\rite\exe\query\gsri36bed',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
              +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
    else
      * --- Delete from DATESDST
      i_nConn=i_TableProp[this.DATESDST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DATESDST_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        local i_cQueryTable,i_cWhere
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_cWhere=i_cTable+".STSERIAL = "+i_cQueryTable+".STSERIAL";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
      
        do vq_exec with '..\rite\exe\query\gsri22bed',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
              +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
    endif
    AddMsgNL(Space(5)+"Estrazione dati ",this)
    * --- Create temporary table TMPDATIESTRATTI
    i_nIdx=cp_AddTableDef('TMPDATIESTRATTI') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('..\rite\exe\query\gsri23bed',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPDATIESTRATTI_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Elimino dalla generazione tutti i versamenti che hanno pi� di un codice
    *     tributo e attivati i seguenti importi (Importi utilizzati a scomputo,crediti d'imposta,
    *     ed interessi)
    * --- Write into TMPDATIESTRATTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMPDATIESTRATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPDATIESTRATTI_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="STVERSER"
      do vq_exec with 'Gsri24bed',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPDATIESTRATTI_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="TMPDATIESTRATTI.STVERSER = _t2.STVERSER";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"ESCLUDIREC ="+cp_NullLink(cp_ToStrODBC("S"),'TMPDATIESTRATTI','ESCLUDIREC');
      +",TIPOESCL ="+cp_NullLink(cp_ToStrODBC("A"),'TMPDATIESTRATTI','TIPOESCL');
          +i_ccchkf;
          +" from "+i_cTable+" TMPDATIESTRATTI, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="TMPDATIESTRATTI.STVERSER = _t2.STVERSER";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI, "+i_cQueryTable+" _t2 set ";
      +"TMPDATIESTRATTI.ESCLUDIREC ="+cp_NullLink(cp_ToStrODBC("S"),'TMPDATIESTRATTI','ESCLUDIREC');
      +",TMPDATIESTRATTI.TIPOESCL ="+cp_NullLink(cp_ToStrODBC("A"),'TMPDATIESTRATTI','TIPOESCL');
          +Iif(Empty(i_ccchkf),"",",TMPDATIESTRATTI.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="TMPDATIESTRATTI.STVERSER = t2.STVERSER";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI set (";
          +"ESCLUDIREC,";
          +"TIPOESCL";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +cp_NullLink(cp_ToStrODBC("S"),'TMPDATIESTRATTI','ESCLUDIREC')+",";
          +cp_NullLink(cp_ToStrODBC("A"),'TMPDATIESTRATTI','TIPOESCL')+"";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="TMPDATIESTRATTI.STVERSER = _t2.STVERSER";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI set ";
      +"ESCLUDIREC ="+cp_NullLink(cp_ToStrODBC("S"),'TMPDATIESTRATTI','ESCLUDIREC');
      +",TIPOESCL ="+cp_NullLink(cp_ToStrODBC("A"),'TMPDATIESTRATTI','TIPOESCL');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".STVERSER = "+i_cQueryTable+".STVERSER";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"ESCLUDIREC ="+cp_NullLink(cp_ToStrODBC("S"),'TMPDATIESTRATTI','ESCLUDIREC');
      +",TIPOESCL ="+cp_NullLink(cp_ToStrODBC("A"),'TMPDATIESTRATTI','TIPOESCL');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Se l'anno di elaborazione � maggiore del 2014 devo eliminare dal cursore
    *     tutti i versamenti che hanno nel campo note il valore 'P'
    if this.oParentObject.w_Anno>=2015
      * --- Write into TMPDATIESTRATTI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMPDATIESTRATTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPDATIESTRATTI_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="STVERSER"
        do vq_exec with 'Gsri39bed',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPDATIESTRATTI_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="TMPDATIESTRATTI.STVERSER = _t2.STVERSER";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"ESCLUDIREC ="+cp_NullLink(cp_ToStrODBC("S"),'TMPDATIESTRATTI','ESCLUDIREC');
        +",TIPOESCL ="+cp_NullLink(cp_ToStrODBC("N"),'TMPDATIESTRATTI','TIPOESCL');
            +i_ccchkf;
            +" from "+i_cTable+" TMPDATIESTRATTI, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="TMPDATIESTRATTI.STVERSER = _t2.STVERSER";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI, "+i_cQueryTable+" _t2 set ";
        +"TMPDATIESTRATTI.ESCLUDIREC ="+cp_NullLink(cp_ToStrODBC("S"),'TMPDATIESTRATTI','ESCLUDIREC');
        +",TMPDATIESTRATTI.TIPOESCL ="+cp_NullLink(cp_ToStrODBC("N"),'TMPDATIESTRATTI','TIPOESCL');
            +Iif(Empty(i_ccchkf),"",",TMPDATIESTRATTI.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="TMPDATIESTRATTI.STVERSER = t2.STVERSER";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI set (";
            +"ESCLUDIREC,";
            +"TIPOESCL";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +cp_NullLink(cp_ToStrODBC("S"),'TMPDATIESTRATTI','ESCLUDIREC')+",";
            +cp_NullLink(cp_ToStrODBC("N"),'TMPDATIESTRATTI','TIPOESCL')+"";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="TMPDATIESTRATTI.STVERSER = _t2.STVERSER";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI set ";
        +"ESCLUDIREC ="+cp_NullLink(cp_ToStrODBC("S"),'TMPDATIESTRATTI','ESCLUDIREC');
        +",TIPOESCL ="+cp_NullLink(cp_ToStrODBC("N"),'TMPDATIESTRATTI','TIPOESCL');
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".STVERSER = "+i_cQueryTable+".STVERSER";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"ESCLUDIREC ="+cp_NullLink(cp_ToStrODBC("S"),'TMPDATIESTRATTI','ESCLUDIREC');
        +",TIPOESCL ="+cp_NullLink(cp_ToStrODBC("N"),'TMPDATIESTRATTI','TIPOESCL');
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- Aggiorno il mese di riferimento nel caso in cui le note abbiano uno dei
    *     seguenti valori (A-B-D-E) oppure il codice tributo � uguale a 1713
    * --- Write into TMPDATIESTRATTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMPDATIESTRATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPDATIESTRATTI_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="STVERSER,STCODTRI"
      do vq_exec with 'Gsri26bed',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPDATIESTRATTI_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="TMPDATIESTRATTI.STVERSER = _t2.STVERSER";
              +" and "+"TMPDATIESTRATTI.STCODTRI = _t2.STCODTRI";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ST__MESE = _t2.ST__MESE";
          +i_ccchkf;
          +" from "+i_cTable+" TMPDATIESTRATTI, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="TMPDATIESTRATTI.STVERSER = _t2.STVERSER";
              +" and "+"TMPDATIESTRATTI.STCODTRI = _t2.STCODTRI";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI, "+i_cQueryTable+" _t2 set ";
          +"TMPDATIESTRATTI.ST__MESE = _t2.ST__MESE";
          +Iif(Empty(i_ccchkf),"",",TMPDATIESTRATTI.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="TMPDATIESTRATTI.STVERSER = t2.STVERSER";
              +" and "+"TMPDATIESTRATTI.STCODTRI = t2.STCODTRI";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI set (";
          +"ST__MESE";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.ST__MESE";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="TMPDATIESTRATTI.STVERSER = _t2.STVERSER";
              +" and "+"TMPDATIESTRATTI.STCODTRI = _t2.STCODTRI";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI set ";
          +"ST__MESE = _t2.ST__MESE";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".STVERSER = "+i_cQueryTable+".STVERSER";
              +" and "+i_cTable+".STCODTRI = "+i_cQueryTable+".STCODTRI";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ST__MESE = (select ST__MESE from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Aggiorno i campi "Importi utilizzati a scomputo" e "Crediti d'imposta" nel
    *     caso in cui il campo Note vale 'K'
    * --- Write into TMPDATIESTRATTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMPDATIESTRATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPDATIESTRATTI_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="STVERSER,STCODTRI"
      do vq_exec with 'Gsri37bed',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPDATIESTRATTI_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="TMPDATIESTRATTI.STVERSER = _t2.STVERSER";
              +" and "+"TMPDATIESTRATTI.STCODTRI = _t2.STCODTRI";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"STRITECC = _t2.STRITECC";
          +",STRITCOM = _t2.STRITCOM";
          +i_ccchkf;
          +" from "+i_cTable+" TMPDATIESTRATTI, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="TMPDATIESTRATTI.STVERSER = _t2.STVERSER";
              +" and "+"TMPDATIESTRATTI.STCODTRI = _t2.STCODTRI";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI, "+i_cQueryTable+" _t2 set ";
          +"TMPDATIESTRATTI.STRITECC = _t2.STRITECC";
          +",TMPDATIESTRATTI.STRITCOM = _t2.STRITCOM";
          +Iif(Empty(i_ccchkf),"",",TMPDATIESTRATTI.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="TMPDATIESTRATTI.STVERSER = t2.STVERSER";
              +" and "+"TMPDATIESTRATTI.STCODTRI = t2.STCODTRI";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI set (";
          +"STRITECC,";
          +"STRITCOM";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.STRITECC,";
          +"t2.STRITCOM";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="TMPDATIESTRATTI.STVERSER = _t2.STVERSER";
              +" and "+"TMPDATIESTRATTI.STCODTRI = _t2.STCODTRI";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI set ";
          +"STRITECC = _t2.STRITECC";
          +",STRITCOM = _t2.STRITCOM";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".STVERSER = "+i_cQueryTable+".STVERSER";
              +" and "+i_cTable+".STCODTRI = "+i_cQueryTable+".STCODTRI";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"STRITECC = (select STRITECC from "+i_cQueryTable+" where "+i_cWhere+")";
          +",STRITCOM = (select STRITCOM from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Elimino dalla generazione i versamenti che hanno un importo negativo
    * --- Write into TMPDATIESTRATTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMPDATIESTRATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPDATIESTRATTI_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="STVERSER"
      do vq_exec with 'Gsri28bed',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPDATIESTRATTI_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="TMPDATIESTRATTI.STVERSER = _t2.STVERSER";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"ESCLUDIREC ="+cp_NullLink(cp_ToStrODBC("S"),'TMPDATIESTRATTI','ESCLUDIREC');
      +",TIPOESCL ="+cp_NullLink(cp_ToStrODBC("I"),'TMPDATIESTRATTI','TIPOESCL');
          +i_ccchkf;
          +" from "+i_cTable+" TMPDATIESTRATTI, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="TMPDATIESTRATTI.STVERSER = _t2.STVERSER";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI, "+i_cQueryTable+" _t2 set ";
      +"TMPDATIESTRATTI.ESCLUDIREC ="+cp_NullLink(cp_ToStrODBC("S"),'TMPDATIESTRATTI','ESCLUDIREC');
      +",TMPDATIESTRATTI.TIPOESCL ="+cp_NullLink(cp_ToStrODBC("I"),'TMPDATIESTRATTI','TIPOESCL');
          +Iif(Empty(i_ccchkf),"",",TMPDATIESTRATTI.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="TMPDATIESTRATTI.STVERSER = t2.STVERSER";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI set (";
          +"ESCLUDIREC,";
          +"TIPOESCL";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +cp_NullLink(cp_ToStrODBC("S"),'TMPDATIESTRATTI','ESCLUDIREC')+",";
          +cp_NullLink(cp_ToStrODBC("I"),'TMPDATIESTRATTI','TIPOESCL')+"";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="TMPDATIESTRATTI.STVERSER = _t2.STVERSER";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI set ";
      +"ESCLUDIREC ="+cp_NullLink(cp_ToStrODBC("S"),'TMPDATIESTRATTI','ESCLUDIREC');
      +",TIPOESCL ="+cp_NullLink(cp_ToStrODBC("I"),'TMPDATIESTRATTI','TIPOESCL');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".STVERSER = "+i_cQueryTable+".STVERSER";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"ESCLUDIREC ="+cp_NullLink(cp_ToStrODBC("S"),'TMPDATIESTRATTI','ESCLUDIREC');
      +",TIPOESCL ="+cp_NullLink(cp_ToStrODBC("I"),'TMPDATIESTRATTI','TIPOESCL');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Creo un cursore di appoggio che conterr� tutti i versamenti che saranno 
    *     scartati
    Vq_Exec("Gsri29bed", this, "ElencoEsclusioni")
    * --- Select from GSRI27BED
    do vq_exec with 'GSRI27BED',this,'_Curs_GSRI27BED','',.f.,.t.
    if used('_Curs_GSRI27BED')
      select _Curs_GSRI27BED
      locate for 1=1
      do while not(eof())
      this.w_NUMVERSCA = NUMVERSCA
        select _Curs_GSRI27BED
        continue
      enddo
      use
    endif
    * --- Elimino dal cursore temporaneo TmpDatiEstratti i record che hanno 
    *     il campo Escludirec valorizzato ad 'S'
    * --- Delete from TMPDATIESTRATTI
    i_nConn=i_TableProp[this.TMPDATIESTRATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPDATIESTRATTI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"ESCLUDIREC = "+cp_ToStrODBC("S");
             )
    else
      delete from (i_cTable) where;
            ESCLUDIREC = "S";

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Devo raggruppare il cursore temporaneo TMPDATIESTRATTI per
    *     seriale versamento e codice tributo
    * --- Select from GSRI30BED
    do vq_exec with 'GSRI30BED',this,'_Curs_GSRI30BED','',.f.,.t.
    if used('_Curs_GSRI30BED')
      select _Curs_GSRI30BED
      locate for 1=1
      do while not(eof())
      this.w_NUMVER = NUMVER
      this.w_NUMRECST = NUMMOV
        select _Curs_GSRI30BED
        continue
      enddo
      use
    endif
    if this.w_NUMRECST<>0
      * --- Verifico se esiste gi� un record per l'anno di estrazione
      * --- Write into TMPDATIESTRATTI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMPDATIESTRATTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPDATIESTRATTI_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="ST__ANNO"
        do vq_exec with 'GSRI6BED',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPDATIESTRATTI_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="TMPDATIESTRATTI.ST__ANNO = _t2.ST__ANNO";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"STSERIAL = _t2.STSERIAL";
            +",NUMMAXRIGA = _t2.NUMMAXRIGA";
            +",AGGIORNAMENTO = _t2.AGGIORNAMENTO";
            +i_ccchkf;
            +" from "+i_cTable+" TMPDATIESTRATTI, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="TMPDATIESTRATTI.ST__ANNO = _t2.ST__ANNO";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI, "+i_cQueryTable+" _t2 set ";
            +"TMPDATIESTRATTI.STSERIAL = _t2.STSERIAL";
            +",TMPDATIESTRATTI.NUMMAXRIGA = _t2.NUMMAXRIGA";
            +",TMPDATIESTRATTI.AGGIORNAMENTO = _t2.AGGIORNAMENTO";
            +Iif(Empty(i_ccchkf),"",",TMPDATIESTRATTI.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="TMPDATIESTRATTI.ST__ANNO = t2.ST__ANNO";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI set (";
            +"STSERIAL,";
            +"NUMMAXRIGA,";
            +"AGGIORNAMENTO";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.STSERIAL,";
            +"t2.NUMMAXRIGA,";
            +"t2.AGGIORNAMENTO";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="TMPDATIESTRATTI.ST__ANNO = _t2.ST__ANNO";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI set ";
            +"STSERIAL = _t2.STSERIAL";
            +",NUMMAXRIGA = _t2.NUMMAXRIGA";
            +",AGGIORNAMENTO = _t2.AGGIORNAMENTO";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".ST__ANNO = "+i_cQueryTable+".ST__ANNO";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"STSERIAL = (select STSERIAL from "+i_cQueryTable+" where "+i_cWhere+")";
            +",NUMMAXRIGA = (select NUMMAXRIGA from "+i_cQueryTable+" where "+i_cWhere+")";
            +",AGGIORNAMENTO = (select AGGIORNAMENTO from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      this.w_NUMRSTAGG = i_Rows
      * --- Try
      local bErr_0470CF30
      bErr_0470CF30=bTrsErr
      this.Try_0470CF30()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        * --- accept error
        bTrsErr=.f.
        ah_ErrorMsg("Errore nella creazione dei dati estratti (%1)", "!", "", i_trsmsg)
      endif
      bTrsErr=bTrsErr or bErr_0470CF30
      * --- End
    else
      ah_ErrorMsg("Nessun record ST da generare")
      if RECCOUNT("ElencoEsclusioni")>0 AND ah_YesNo("Si vuole stampare l'elenco delle anomalie riscontrate?")
        Select * from ElencoEsclusioni into cursor __TMP__
        Cp_Chprn("..\rite\exe\query\gsri1bed", , this.oParentObject)
      endif
    endif
  endproc
  proc Try_0470CF30()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    w_Conn=i_TableProp[this.DATESDST_IDX, 3]
    if this.w_NUMRSTAGG=0
      AddMsgNL(Space(5)+"Generazione progressivi",this)
      this.w_STSERIAL = space(10)
      cp_NextTableProg(this, w_Conn, "STSER", "i_codazi,w_STSERIAL")
      * --- Aggiorno massivamente la chiave della tabella TMPDATIESTRATTI
      * --- Write into TMPDATIESTRATTI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMPDATIESTRATTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPDATIESTRATTI_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="STVERSER,STCODTRI"
        do vq_exec with 'Gsri31bed',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPDATIESTRATTI_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="TMPDATIESTRATTI.STVERSER = _t2.STVERSER";
                +" and "+"TMPDATIESTRATTI.STCODTRI = _t2.STCODTRI";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"STSERIAL = _t2.STSERIAL";
            +i_ccchkf;
            +" from "+i_cTable+" TMPDATIESTRATTI, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="TMPDATIESTRATTI.STVERSER = _t2.STVERSER";
                +" and "+"TMPDATIESTRATTI.STCODTRI = _t2.STCODTRI";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI, "+i_cQueryTable+" _t2 set ";
            +"TMPDATIESTRATTI.STSERIAL = _t2.STSERIAL";
            +Iif(Empty(i_ccchkf),"",",TMPDATIESTRATTI.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="TMPDATIESTRATTI.STVERSER = t2.STVERSER";
                +" and "+"TMPDATIESTRATTI.STCODTRI = t2.STCODTRI";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI set (";
            +"STSERIAL";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.STSERIAL";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="TMPDATIESTRATTI.STVERSER = _t2.STVERSER";
                +" and "+"TMPDATIESTRATTI.STCODTRI = _t2.STCODTRI";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI set ";
            +"STSERIAL = _t2.STSERIAL";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".STVERSER = "+i_cQueryTable+".STVERSER";
                +" and "+i_cTable+".STCODTRI = "+i_cQueryTable+".STCODTRI";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"STSERIAL = (select STSERIAL from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    if this.oParentObject.w_RecordH="S"
      * --- Se ho settato il check record H non devo verificare la versione sia di
      *     SQL che di oracle perch� � gi� stata calcolata
    else
      * --- Verifico la versione del database 
      if upper(CP_DBTYPE)="SQLSERVER"
        sqlexec(w_Conn,"Select convert(char,SERVERPROPERTY('productversion')) As Version", "ChkVersion")
        if USED("ChkVersion") AND VAL(LEFT(ChkVersion.Version,AT(".",ChkVersion.Version)-1)) < 9
          this.w_OLDVERSIONE = .t.
        endif
      else
        if TYPE("g_ORACLEVERS")="C" AND g_ORACLEVERS="8"
          this.w_OLDVERSIONE = .t.
        endif
      endif
    endif
    AddMsgNL(Space(5)+"Inserimento dati",this)
    if this.w_OLDVERSIONE
      * --- Write into TMPDATIESTRATTI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMPDATIESTRATTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPDATIESTRATTI_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="STVERSER,STCODTRI"
        do vq_exec with 'GSRI33BED',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPDATIESTRATTI_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="TMPDATIESTRATTI.STVERSER = _t2.STVERSER";
                +" and "+"TMPDATIESTRATTI.STCODTRI = _t2.STCODTRI";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CPROWNUM = _t2.CPROWNUM";
            +i_ccchkf;
            +" from "+i_cTable+" TMPDATIESTRATTI, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="TMPDATIESTRATTI.STVERSER = _t2.STVERSER";
                +" and "+"TMPDATIESTRATTI.STCODTRI = _t2.STCODTRI";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI, "+i_cQueryTable+" _t2 set ";
            +"TMPDATIESTRATTI.CPROWNUM = _t2.CPROWNUM";
            +Iif(Empty(i_ccchkf),"",",TMPDATIESTRATTI.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="TMPDATIESTRATTI.STVERSER = t2.STVERSER";
                +" and "+"TMPDATIESTRATTI.STCODTRI = t2.STCODTRI";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI set (";
            +"CPROWNUM";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.CPROWNUM";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="TMPDATIESTRATTI.STVERSER = _t2.STVERSER";
                +" and "+"TMPDATIESTRATTI.STCODTRI = _t2.STCODTRI";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI set ";
            +"CPROWNUM = _t2.CPROWNUM";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".STVERSER = "+i_cQueryTable+".STVERSER";
                +" and "+i_cTable+".STCODTRI = "+i_cQueryTable+".STCODTRI";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CPROWNUM = (select CPROWNUM from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    else
      * --- Write into TMPDATIESTRATTI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMPDATIESTRATTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPDATIESTRATTI_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="STVERSER,STCODTRI"
        do vq_exec with 'GSRI32BED',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPDATIESTRATTI_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="TMPDATIESTRATTI.STVERSER = _t2.STVERSER";
                +" and "+"TMPDATIESTRATTI.STCODTRI = _t2.STCODTRI";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CPROWNUM = _t2.CPROWNUM";
            +i_ccchkf;
            +" from "+i_cTable+" TMPDATIESTRATTI, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="TMPDATIESTRATTI.STVERSER = _t2.STVERSER";
                +" and "+"TMPDATIESTRATTI.STCODTRI = _t2.STCODTRI";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI, "+i_cQueryTable+" _t2 set ";
            +"TMPDATIESTRATTI.CPROWNUM = _t2.CPROWNUM";
            +Iif(Empty(i_ccchkf),"",",TMPDATIESTRATTI.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="TMPDATIESTRATTI.STVERSER = t2.STVERSER";
                +" and "+"TMPDATIESTRATTI.STCODTRI = t2.STCODTRI";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI set (";
            +"CPROWNUM";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.CPROWNUM";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="TMPDATIESTRATTI.STVERSER = _t2.STVERSER";
                +" and "+"TMPDATIESTRATTI.STCODTRI = _t2.STCODTRI";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI set ";
            +"CPROWNUM = _t2.CPROWNUM";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".STVERSER = "+i_cQueryTable+".STVERSER";
                +" and "+i_cTable+".STCODTRI = "+i_cQueryTable+".STCODTRI";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CPROWNUM = (select CPROWNUM from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- Nel caso in cui siano presenti record che aggiornano la tabella DATESDST
    *     il numero di riga deve essere ricalcolato
    * --- Write into TMPDATIESTRATTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMPDATIESTRATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPDATIESTRATTI_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="STVERSER,STCODTRI"
      do vq_exec with 'GSRI35BED',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPDATIESTRATTI_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="TMPDATIESTRATTI.STVERSER = _t2.STVERSER";
              +" and "+"TMPDATIESTRATTI.STCODTRI = _t2.STCODTRI";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CPROWNUM = _t2.CPROWNUM";
          +i_ccchkf;
          +" from "+i_cTable+" TMPDATIESTRATTI, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="TMPDATIESTRATTI.STVERSER = _t2.STVERSER";
              +" and "+"TMPDATIESTRATTI.STCODTRI = _t2.STCODTRI";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI, "+i_cQueryTable+" _t2 set ";
          +"TMPDATIESTRATTI.CPROWNUM = _t2.CPROWNUM";
          +Iif(Empty(i_ccchkf),"",",TMPDATIESTRATTI.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="TMPDATIESTRATTI.STVERSER = t2.STVERSER";
              +" and "+"TMPDATIESTRATTI.STCODTRI = t2.STCODTRI";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI set (";
          +"CPROWNUM";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.CPROWNUM";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="TMPDATIESTRATTI.STVERSER = _t2.STVERSER";
              +" and "+"TMPDATIESTRATTI.STCODTRI = _t2.STCODTRI";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI set ";
          +"CPROWNUM = _t2.CPROWNUM";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".STVERSER = "+i_cQueryTable+".STVERSER";
              +" and "+i_cTable+".STCODTRI = "+i_cQueryTable+".STCODTRI";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CPROWNUM = (select CPROWNUM from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Insert into DATESDST
    i_nConn=i_TableProp[this.DATESDST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DATESDST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\rite\exe\query\gsri34bed",this.DATESDST_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    this.w_oMESS=createobject("AH_Message")
    this.w_oMESS.AddMsgPartNL("Generazione completata.")     
    if this.w_NUMRECST<>0
      this.w_oPART = this.w_oMESS.AddMsgPartNL("Inseriti n. %1 record ST da n. %2 versamenti")
      this.w_oPART.AddParam(ALLTRIM(STR(this.w_NUMRECST)))     
      this.w_oPART.AddParam(ALLTRIM(STR(this.w_NUMVER)))     
    endif
    if this.w_NUMVERSCA<>0
      this.w_oPART = this.w_oMESS.AddMsgPartNL("Scartati n. %1 versamenti")
      this.w_oPART.AddParam(ALLTRIM(STR(this.w_NUMVERSCA)))     
    endif
    this.w_oMESS.AH_ErrorMsg(48)     
    if RECCOUNT("ElencoEsclusioni")>0 AND ah_YesNo("Si vuole stampare l'elenco delle anomalie riscontrate?")
      Select * from ElencoEsclusioni into cursor __TMP__
      Cp_Chprn("..\rite\exe\query\gsri1bed", , this.oParentObject)
    endif
    return


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controllo che l'utente non abbia specificato un anno inferiore al 2017
    if this.oParentObject.w_Anno<2017
      ah_ErrorMsg("Estrazione disponibile dall'anno: 2017")
      i_retcode = 'stop'
      return
    endif
    * --- Cancello sempre i record riferiti ai dati previdenziali
    AddMsgNL(Space(5)+"Eliminazione estrazioni precedenti",this)
    * --- Controllo se � stato attivato il flag "Elimina anche certificazioni definitive"
    *     in questo caso vengono cancellati anche i dati estratti che sono stati
    *     impostati a certificazione definitiva
    if this.oParentObject.w_ELIRECORDCUD="S"
      * --- Delete from CUDDPTDH
      i_nConn=i_TableProp[this.CUDDPTDH_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CUDDPTDH_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        local i_cQueryTable,i_cWhere
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_cWhere=i_cTable+".DPSERIAL = "+i_cQueryTable+".DPSERIAL";
              +" and "+i_cTable+".DPROGMOD = "+i_cQueryTable+".DPROGMOD";
      
        do vq_exec with '..\Rite\Exe\Query\Gsri40bcu',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
              +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Delete from CUDTETDH
      i_nConn=i_TableProp[this.CUDTETDH_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CUDTETDH_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        local i_cQueryTable,i_cWhere
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_cWhere=i_cTable+".CHSERIAL = "+i_cQueryTable+".CHSERIAL";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
      
        do vq_exec with '..\rite\exe\query\gsri43bcu',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
              +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Delete from CUDTETRH
      i_nConn=i_TableProp[this.CUDTETRH_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CUDTETRH_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        local i_cQueryTable,i_cWhere
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_cWhere=i_cTable+".CHSERIAL = "+i_cQueryTable+".CHSERIAL";
      
        do vq_exec with '..\rite\exe\query\gsri1bcu',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
              +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
    else
      * --- Delete from CUDDPTDH
      i_nConn=i_TableProp[this.CUDDPTDH_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CUDDPTDH_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        local i_cQueryTable,i_cWhere
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_cWhere=i_cTable+".DPSERIAL = "+i_cQueryTable+".DPSERIAL";
              +" and "+i_cTable+".DPROGMOD = "+i_cQueryTable+".DPROGMOD";
      
        do vq_exec with '..\Rite\Exe\Query\Gsri44bcu',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
              +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Delete from CUDTETDH
      i_nConn=i_TableProp[this.CUDTETDH_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CUDTETDH_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        local i_cQueryTable,i_cWhere
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_cWhere=i_cTable+".CHSERIAL = "+i_cQueryTable+".CHSERIAL";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
      
        do vq_exec with '..\rite\exe\query\gsri_bcu',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
              +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Delete from CUDTETRH
      i_nConn=i_TableProp[this.CUDTETRH_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CUDTETRH_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        local i_cQueryTable,i_cWhere
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_cWhere=i_cTable+".CHSERIAL = "+i_cQueryTable+".CHSERIAL";
      
        do vq_exec with '..\rite\exe\query\gsri19bcu',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
              +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
    endif
    AddMsgNL(Space(5)+"Estrazione dati ",this)
    * --- Create temporary table TMPDATIESTRATTI
    i_nIdx=cp_AddTableDef('TMPDATIESTRATTI') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('..\rite\exe\query\gsri2bcu',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPDATIESTRATTI_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Elimino dal cursore temporaneo Tmpdatiestratti tutti i movimenti ritenute
    *     che sono presenti in un dato estratto che � stato comunicato
    * --- Delete from TMPDATIESTRATTI
    i_nConn=i_TableProp[this.TMPDATIESTRATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPDATIESTRATTI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      local i_cQueryTable,i_cWhere
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_cWhere=i_cTable+".MRSERIAL = "+i_cQueryTable+".MRSERIAL";
    
      do vq_exec with 'Gsri38bcu',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
            +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Elimino dal cursore temporaneo Tmpdatiestratti tutti i movimenti ritenute
    *     che sono presenti in un dato estratto di tipo sostitutiva/annullamento
    *     che � stato abbinato ad un altro dato estratto
    * --- Delete from TMPDATIESTRATTI
    i_nConn=i_TableProp[this.TMPDATIESTRATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPDATIESTRATTI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      local i_cQueryTable,i_cWhere
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_cWhere=i_cTable+".MRSERIAL = "+i_cQueryTable+".MRSERIAL";
    
      do vq_exec with 'Gsri39bcu',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
            +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Elimino dal cursore temporaneo Tmpdatiestratti tutti i movimenti ritenute
    *     che sono presenti in un dato estratto che � stato reso definitivo
    * --- Delete from TMPDATIESTRATTI
    i_nConn=i_TableProp[this.TMPDATIESTRATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPDATIESTRATTI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      local i_cQueryTable,i_cWhere
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_cWhere=i_cTable+".MRSERIAL = "+i_cQueryTable+".MRSERIAL";
    
      do vq_exec with 'Gsri45bcu',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
            +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Nel caso siano presenti righe con dati previdenziali valorizzati, i dati estratti
    *     creati dovranno essere esclusi dalla generazione
    * --- Write into TMPDATIESTRATTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMPDATIESTRATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPDATIESTRATTI_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="CH__ANNO,CHCODCON,CHCAUPRE,CHTIPOPE"
      do vq_exec with 'GSRI42BCU',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPDATIESTRATTI_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="TMPDATIESTRATTI.CH__ANNO = _t2.CH__ANNO";
              +" and "+"TMPDATIESTRATTI.CHCODCON = _t2.CHCODCON";
              +" and "+"TMPDATIESTRATTI.CHCAUPRE = _t2.CHCAUPRE";
              +" and "+"TMPDATIESTRATTI.CHTIPOPE = _t2.CHTIPOPE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"CHSCLGEN ="+cp_NullLink(cp_ToStrODBC("S"),'TMPDATIESTRATTI','CHSCLGEN');
          +i_ccchkf;
          +" from "+i_cTable+" TMPDATIESTRATTI, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="TMPDATIESTRATTI.CH__ANNO = _t2.CH__ANNO";
              +" and "+"TMPDATIESTRATTI.CHCODCON = _t2.CHCODCON";
              +" and "+"TMPDATIESTRATTI.CHCAUPRE = _t2.CHCAUPRE";
              +" and "+"TMPDATIESTRATTI.CHTIPOPE = _t2.CHTIPOPE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI, "+i_cQueryTable+" _t2 set ";
      +"TMPDATIESTRATTI.CHSCLGEN ="+cp_NullLink(cp_ToStrODBC("S"),'TMPDATIESTRATTI','CHSCLGEN');
          +Iif(Empty(i_ccchkf),"",",TMPDATIESTRATTI.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="TMPDATIESTRATTI.CH__ANNO = t2.CH__ANNO";
              +" and "+"TMPDATIESTRATTI.CHCODCON = t2.CHCODCON";
              +" and "+"TMPDATIESTRATTI.CHCAUPRE = t2.CHCAUPRE";
              +" and "+"TMPDATIESTRATTI.CHTIPOPE = t2.CHTIPOPE";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI set (";
          +"CHSCLGEN";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +cp_NullLink(cp_ToStrODBC("S"),'TMPDATIESTRATTI','CHSCLGEN')+"";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="TMPDATIESTRATTI.CH__ANNO = _t2.CH__ANNO";
              +" and "+"TMPDATIESTRATTI.CHCODCON = _t2.CHCODCON";
              +" and "+"TMPDATIESTRATTI.CHCAUPRE = _t2.CHCAUPRE";
              +" and "+"TMPDATIESTRATTI.CHTIPOPE = _t2.CHTIPOPE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI set ";
      +"CHSCLGEN ="+cp_NullLink(cp_ToStrODBC("S"),'TMPDATIESTRATTI','CHSCLGEN');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".CH__ANNO = "+i_cQueryTable+".CH__ANNO";
              +" and "+i_cTable+".CHCODCON = "+i_cQueryTable+".CHCODCON";
              +" and "+i_cTable+".CHCAUPRE = "+i_cQueryTable+".CHCAUPRE";
              +" and "+i_cTable+".CHTIPOPE = "+i_cQueryTable+".CHTIPOPE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"CHSCLGEN ="+cp_NullLink(cp_ToStrODBC("S"),'TMPDATIESTRATTI','CHSCLGEN');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Verifico se nel cursore temporaneo TmpDatiEstratti sono presenti dei
    *     record ( Anno, codice conto , causale prestazione ) che sono gi� presenti
    *     nella tabella Datestrh. Se esistono aggiorno il campo contenente il seriale
    *     ed il campo di appoggio "Aggiornamento"
    * --- Write into TMPDATIESTRATTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMPDATIESTRATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPDATIESTRATTI_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="CH__ANNO,CHCODCON,CHCAUPRE"
      do vq_exec with 'GSRI4BCU',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPDATIESTRATTI_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="TMPDATIESTRATTI.CH__ANNO = _t2.CH__ANNO";
              +" and "+"TMPDATIESTRATTI.CHCODCON = _t2.CHCODCON";
              +" and "+"TMPDATIESTRATTI.CHCAUPRE = _t2.CHCAUPRE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CHSERIAL = _t2.Chserial";
          +",AGGIORNAMENTO = _t2.Aggiornamento";
          +",NUMMAXRIGA = _t2.Nummaxriga";
          +i_ccchkf;
          +" from "+i_cTable+" TMPDATIESTRATTI, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="TMPDATIESTRATTI.CH__ANNO = _t2.CH__ANNO";
              +" and "+"TMPDATIESTRATTI.CHCODCON = _t2.CHCODCON";
              +" and "+"TMPDATIESTRATTI.CHCAUPRE = _t2.CHCAUPRE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI, "+i_cQueryTable+" _t2 set ";
          +"TMPDATIESTRATTI.CHSERIAL = _t2.Chserial";
          +",TMPDATIESTRATTI.AGGIORNAMENTO = _t2.Aggiornamento";
          +",TMPDATIESTRATTI.NUMMAXRIGA = _t2.Nummaxriga";
          +Iif(Empty(i_ccchkf),"",",TMPDATIESTRATTI.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="TMPDATIESTRATTI.CH__ANNO = t2.CH__ANNO";
              +" and "+"TMPDATIESTRATTI.CHCODCON = t2.CHCODCON";
              +" and "+"TMPDATIESTRATTI.CHCAUPRE = t2.CHCAUPRE";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI set (";
          +"CHSERIAL,";
          +"AGGIORNAMENTO,";
          +"NUMMAXRIGA";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.Chserial,";
          +"t2.Aggiornamento,";
          +"t2.Nummaxriga";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="TMPDATIESTRATTI.CH__ANNO = _t2.CH__ANNO";
              +" and "+"TMPDATIESTRATTI.CHCODCON = _t2.CHCODCON";
              +" and "+"TMPDATIESTRATTI.CHCAUPRE = _t2.CHCAUPRE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI set ";
          +"CHSERIAL = _t2.Chserial";
          +",AGGIORNAMENTO = _t2.Aggiornamento";
          +",NUMMAXRIGA = _t2.Nummaxriga";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".CH__ANNO = "+i_cQueryTable+".CH__ANNO";
              +" and "+i_cTable+".CHCODCON = "+i_cQueryTable+".CHCODCON";
              +" and "+i_cTable+".CHCAUPRE = "+i_cQueryTable+".CHCAUPRE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CHSERIAL = (select Chserial from "+i_cQueryTable+" where "+i_cWhere+")";
          +",AGGIORNAMENTO = (select Aggiornamento from "+i_cQueryTable+" where "+i_cWhere+")";
          +",NUMMAXRIGA = (select Nummaxriga from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Controllo se sono presenti movimenti ritenute con diverse somme non soggette valorizzate.
    *     Raggruppo per Anno, tipo conto, codice conto, causale prestazione.
    *     Escludo dalla query i movimenti ritenute che hanno una somma non 
    *     soggetta valida e che siano percipienti esteri
    * --- Write into TMPDATIESTRATTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMPDATIESTRATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPDATIESTRATTI_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="CH__ANNO,CHTIPCON,CHCODCON,CHCAUPRE"
      do vq_exec with 'GSRI47BCU',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPDATIESTRATTI_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="TMPDATIESTRATTI.CH__ANNO = _t2.CH__ANNO";
              +" and "+"TMPDATIESTRATTI.CHTIPCON = _t2.CHTIPCON";
              +" and "+"TMPDATIESTRATTI.CHCODCON = _t2.CHCODCON";
              +" and "+"TMPDATIESTRATTI.CHCAUPRE = _t2.CHCAUPRE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CODSNS = _t2.CODSNS";
          +i_ccchkf;
          +" from "+i_cTable+" TMPDATIESTRATTI, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="TMPDATIESTRATTI.CH__ANNO = _t2.CH__ANNO";
              +" and "+"TMPDATIESTRATTI.CHTIPCON = _t2.CHTIPCON";
              +" and "+"TMPDATIESTRATTI.CHCODCON = _t2.CHCODCON";
              +" and "+"TMPDATIESTRATTI.CHCAUPRE = _t2.CHCAUPRE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI, "+i_cQueryTable+" _t2 set ";
          +"TMPDATIESTRATTI.CODSNS = _t2.CODSNS";
          +Iif(Empty(i_ccchkf),"",",TMPDATIESTRATTI.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="TMPDATIESTRATTI.CH__ANNO = t2.CH__ANNO";
              +" and "+"TMPDATIESTRATTI.CHTIPCON = t2.CHTIPCON";
              +" and "+"TMPDATIESTRATTI.CHCODCON = t2.CHCODCON";
              +" and "+"TMPDATIESTRATTI.CHCAUPRE = t2.CHCAUPRE";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI set (";
          +"CODSNS";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.CODSNS";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="TMPDATIESTRATTI.CH__ANNO = _t2.CH__ANNO";
              +" and "+"TMPDATIESTRATTI.CHTIPCON = _t2.CHTIPCON";
              +" and "+"TMPDATIESTRATTI.CHCODCON = _t2.CHCODCON";
              +" and "+"TMPDATIESTRATTI.CHCAUPRE = _t2.CHCAUPRE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI set ";
          +"CODSNS = _t2.CODSNS";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".CH__ANNO = "+i_cQueryTable+".CH__ANNO";
              +" and "+i_cTable+".CHTIPCON = "+i_cQueryTable+".CHTIPCON";
              +" and "+i_cTable+".CHCODCON = "+i_cQueryTable+".CHCODCON";
              +" and "+i_cTable+".CHCAUPRE = "+i_cQueryTable+".CHCAUPRE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CODSNS = (select CODSNS from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Devo inserire il flag "Escludi dalla generazione" sul dettaglio dei dati estratti,
    *     nel caso in cui il movimento ritenute risulta gi� inserito
    * --- Write into TMPDATIESTRATTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMPDATIESTRATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPDATIESTRATTI_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="CHSERIAL,MRSERIAL"
      do vq_exec with 'Gsri20bcu',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPDATIESTRATTI_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="TMPDATIESTRATTI.CHSERIAL = _t2.CHSERIAL";
              +" and "+"TMPDATIESTRATTI.MRSERIAL = _t2.MRSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CHESCLGE = _t2.CHESCLGE";
          +i_ccchkf;
          +" from "+i_cTable+" TMPDATIESTRATTI, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="TMPDATIESTRATTI.CHSERIAL = _t2.CHSERIAL";
              +" and "+"TMPDATIESTRATTI.MRSERIAL = _t2.MRSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI, "+i_cQueryTable+" _t2 set ";
          +"TMPDATIESTRATTI.CHESCLGE = _t2.CHESCLGE";
          +Iif(Empty(i_ccchkf),"",",TMPDATIESTRATTI.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="TMPDATIESTRATTI.CHSERIAL = t2.CHSERIAL";
              +" and "+"TMPDATIESTRATTI.MRSERIAL = t2.MRSERIAL";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI set (";
          +"CHESCLGE";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.CHESCLGE";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="TMPDATIESTRATTI.CHSERIAL = _t2.CHSERIAL";
              +" and "+"TMPDATIESTRATTI.MRSERIAL = _t2.MRSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI set ";
          +"CHESCLGE = _t2.CHESCLGE";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".CHSERIAL = "+i_cQueryTable+".CHSERIAL";
              +" and "+i_cTable+".MRSERIAL = "+i_cQueryTable+".MRSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CHESCLGE = (select CHESCLGE from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    this.w_NUMESCLRIGA = i_Rows
    * --- Creo un cursore di appoggio che conterr� i movimenti ritenute che saranno
    *     scartati perch� hanno indicato come causale prestazione 'F' oppure i
    *     dati estratti che verranno esclusi dalla generazione perch� hanno dei valori
    *     incongruenti
    Vq_Exec("Gsri14bcu", this, "ElencoEsclusioni")
    * --- Elimino dal cursore temporaneo TmpDatiEstratti i record che hanno 
    *     il campo Cauprest valorizzato ad 'S'
    * --- Delete from TMPDATIESTRATTI
    i_nConn=i_TableProp[this.TMPDATIESTRATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPDATIESTRATTI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"CAUPREST = "+cp_ToStrODBC("S");
             )
    else
      delete from (i_cTable) where;
            CAUPREST = "S";

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    this.w_NUMMOVELI = i_Rows
    * --- Elimino dal cursore temporaneo TmpDatiEstratti i record che hanno 
    *     il campo Codsns valorizzato ad 'S'
    * --- Delete from TMPDATIESTRATTI
    i_nConn=i_TableProp[this.TMPDATIESTRATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPDATIESTRATTI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"CODSNS = "+cp_ToStrODBC("S");
             )
    else
      delete from (i_cTable) where;
            CODSNS = "S";

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    this.w_NUMMOVELI = this.w_NUMMOVELI+i_Rows
    * --- La variabile w_NumMov contiene il numero di record che verranno aggiunti
    *     alla tabella "Datestrh"
    * --- Select from GSRI7BCU
    do vq_exec with 'GSRI7BCU',this,'_Curs_GSRI7BCU','',.f.,.t.
    if used('_Curs_GSRI7BCU')
      select _Curs_GSRI7BCU
      locate for 1=1
      do while not(eof())
      this.w_NUMMOV = NUMERO
      this.w_NEWMOVRITE = NEWMOVRITE
        select _Curs_GSRI7BCU
        continue
      enddo
      use
    endif
    * --- La variabile w_NumMovAgg contiene il numero di record che aggiorneranno
    *     la tabella "Datestrh"
    * --- Select from GSRI16BCU
    do vq_exec with 'GSRI16BCU',this,'_Curs_GSRI16BCU','',.f.,.t.
    if used('_Curs_GSRI16BCU')
      select _Curs_GSRI16BCU
      locate for 1=1
      do while not(eof())
      this.w_NUMMOVAGG = NUMERO
      this.w_AGGMOVRITE = AGGMOVRITE
        select _Curs_GSRI16BCU
        continue
      enddo
      use
    endif
    if this.w_NUMMOV<>0 OR this.w_NUMMOVAGG<>0
      * --- Try
      local bErr_04740BD0
      bErr_04740BD0=bTrsErr
      this.Try_04740BD0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        * --- accept error
        bTrsErr=.f.
        ah_ErrorMsg("Errore nella creazione dei dati estratti (%1)", "!", "", i_trsmsg)
      endif
      bTrsErr=bTrsErr or bErr_04740BD0
      * --- End
    else
      ah_ErrorMsg("Nessuna certificazione da generare")
      if RECCOUNT("ElencoEsclusioni")>0 AND ah_YesNo("Si vuole stampare l'elenco delle anomalie riscontrate?")
        Select * from ElencoEsclusioni into cursor __TMP__
        Cp_Chprn("..\rite\exe\query\gsri_bcu", , this.oParentObject)
      endif
    endif
  endproc
  proc Try_04740BD0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    AddMsgNL(Space(5)+"Generazione progressivi",this)
    * --- Verifico la versione del database 
    this.w_CHSERIAL = space(10)
    w_Conn=i_TableProp[this.CUDTETRH_IDX, 3] 
 cp_NextTableProg(this, w_Conn, "CHSER", "i_codazi,w_CHSERIAL")
    this.w_SERIAL = VAL(this.w_CHSERIAL)-1
    this.w_CHSERIAL = cp_StrZeroPad( this.w_SERIAL+this.w_NUMMOV ,10)
    cp_NextTableProg(this, w_Conn, "CHSER", "i_codazi,w_CHSERIAL")
    if upper(CP_DBTYPE)="SQLSERVER"
      sqlexec(w_Conn,"Select convert(char,SERVERPROPERTY('productversion')) As Version", "ChkVersion")
      if USED("ChkVersion") AND VAL(LEFT(ChkVersion.Version,AT(".",ChkVersion.Version)-1)) < 9
        this.w_OLDVERSIONE = .t.
      endif
    else
      if TYPE("g_ORACLEVERS")="C" AND g_ORACLEVERS="8"
        this.w_OLDVERSIONE = .t.
      endif
    endif
    if this.w_NUMMOV<>0
      * --- Genero progressivo seriale
      if this.w_OLDVERSIONE
        * --- Select from GSRI8BCU
        do vq_exec with 'GSRI8BCU',this,'_Curs_GSRI8BCU','',.f.,.t.
        if used('_Curs_GSRI8BCU')
          select _Curs_GSRI8BCU
          locate for 1=1
          do while not(eof())
          this.w_NUMSER = this.w_NUMSER + 1
          this.w_CH__ANNO = _Curs_GSRI8BCU.CH__ANNO
          this.w_CHCODCON = _Curs_GSRI8BCU.CHCODCON
          this.w_CHCAUPRE = _Curs_GSRI8BCU.CHCAUPRE
          this.w_AGGSER = cp_StrZeroPad(this.w_NUMSER + this.w_SERIAL, 10)
          * --- Write into TMPDATIESTRATTI
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.TMPDATIESTRATTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPDATIESTRATTI_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPDATIESTRATTI_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CHSERIAL ="+cp_NullLink(cp_ToStrODBC(this.w_AGGSER),'TMPDATIESTRATTI','CHSERIAL');
                +i_ccchkf ;
            +" where ";
                +"CH__ANNO = "+cp_ToStrODBC(this.w_CH__ANNO);
                +" and CHCODCON = "+cp_ToStrODBC(this.w_CHCODCON);
                +" and CHCAUPRE = "+cp_ToStrODBC(this.w_CHCAUPRE);
                   )
          else
            update (i_cTable) set;
                CHSERIAL = this.w_AGGSER;
                &i_ccchkf. ;
             where;
                CH__ANNO = this.w_CH__ANNO;
                and CHCODCON = this.w_CHCODCON;
                and CHCAUPRE = this.w_CHCAUPRE;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
            select _Curs_GSRI8BCU
            continue
          enddo
          use
        endif
      else
        * --- Write into TMPDATIESTRATTI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.TMPDATIESTRATTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPDATIESTRATTI_idx,2])
        if i_nConn<>0
          local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
          declare i_aIndex[1]
          i_cQueryTable=cp_getTempTableName(i_nConn)
          i_aIndex(1)="CH__ANNO,CHCODCON,CHCAUPRE"
          do vq_exec with 'GSRI9BCU',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPDATIESTRATTI_idx,i_nConn)	
          i_cDB=cp_GetDatabaseType(i_nConn)
          do case
          case i_cDB="SQLServer"
            i_cWhere="TMPDATIESTRATTI.CH__ANNO = _t2.CH__ANNO";
                  +" and "+"TMPDATIESTRATTI.CHCODCON = _t2.CHCODCON";
                  +" and "+"TMPDATIESTRATTI.CHCAUPRE = _t2.CHCAUPRE";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"CHSERIAL = _t2.CHSERIAL";
              +i_ccchkf;
              +" from "+i_cTable+" TMPDATIESTRATTI, "+i_cQueryTable+" _t2 where "+i_cWhere)
          case i_cDB="MySQL"
            i_cWhere="TMPDATIESTRATTI.CH__ANNO = _t2.CH__ANNO";
                  +" and "+"TMPDATIESTRATTI.CHCODCON = _t2.CHCODCON";
                  +" and "+"TMPDATIESTRATTI.CHCAUPRE = _t2.CHCAUPRE";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI, "+i_cQueryTable+" _t2 set ";
              +"TMPDATIESTRATTI.CHSERIAL = _t2.CHSERIAL";
              +Iif(Empty(i_ccchkf),"",",TMPDATIESTRATTI.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
              +" where "+i_cWhere)
          case i_cDB="Oracle"
            i_cWhere="TMPDATIESTRATTI.CH__ANNO = t2.CH__ANNO";
                  +" and "+"TMPDATIESTRATTI.CHCODCON = t2.CHCODCON";
                  +" and "+"TMPDATIESTRATTI.CHCAUPRE = t2.CHCAUPRE";
            
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI set (";
              +"CHSERIAL";
              +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
              +"t2.CHSERIAL";
              +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
              +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
          case i_cDB="PostgreSQL"
            i_cWhere="TMPDATIESTRATTI.CH__ANNO = _t2.CH__ANNO";
                  +" and "+"TMPDATIESTRATTI.CHCODCON = _t2.CHCODCON";
                  +" and "+"TMPDATIESTRATTI.CHCAUPRE = _t2.CHCAUPRE";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI set ";
              +"CHSERIAL = _t2.CHSERIAL";
              +i_ccchkf;
              +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
          otherwise
            i_cWhere=i_cTable+".CH__ANNO = "+i_cQueryTable+".CH__ANNO";
                  +" and "+i_cTable+".CHCODCON = "+i_cQueryTable+".CHCODCON";
                  +" and "+i_cTable+".CHCAUPRE = "+i_cQueryTable+".CHCAUPRE";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"CHSERIAL = (select CHSERIAL from "+i_cQueryTable+" where "+i_cWhere+")";
              +i_ccchkf;
              +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          endcase
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
      AddMsgNL(Space(5)+"Inserimento dati",this)
      * --- Insert into CUDTETRH
      i_nConn=i_TableProp[this.CUDTETRH_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CUDTETRH_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"Gsri10bcu",this.CUDTETRH_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
    if this.w_NUMMOVAGG<>0
      * --- Se il record ha il campo "Aggiornamento" valorizzato ad 'S' non deve essere
      *     effettuato l'inserimento nella tabella Datestrh ma devono solo essere aggiornati
      *     i dati
      AddMsgNL(Space(5)+"Aggiornamento dati ",this)
      * --- Write into CUDTETRH
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.CUDTETRH_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CUDTETRH_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="CHSERIAL"
        do vq_exec with 'Gsri5bcu',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CUDTETRH_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="CUDTETRH.CHSERIAL = _t2.CHSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CHPERFIS = _t2.CHPERFIS";
            +",CHPEREST = _t2.CHPEREST";
            +",CHCODFIS = _t2.CHCODFIS";
            +",CHDESCRI = _t2.CHDESCRI";
            +",CHCOGNOM = _t2.CHCOGNOM";
            +",CH__NOME = _t2.CH__NOME";
            +",CH_SESSO = _t2.CH_SESSO";
            +",CHDATNAS = _t2.CHDATNAS";
            +",CHCOMEST = _t2.CHCOMEST";
            +",CHPRONAS = _t2.CHPRONAS";
            +",CHCATPAR = _t2.CHCATPAR";
            +",CHEVEECC = _t2.CHEVEECC";
            +",CHCOMUNE = _t2.CHCOMUNE";
            +",CHPROVIN = _t2.CHPROVIN";
            +",CHCODCOM = _t2.CHCODCOM";
            +",CHCOFISC = _t2.CHCOFISC";
            +",CHCITEST = _t2.CHCITEST";
            +",CHINDEST = _t2.CHINDEST";
            +",CHSTAEST = _t2.CHSTAEST";
            +",CHSCLGEN = _t2.CHSCLGEN";
            +",CHDAVERI = _t2.CHDAVERI";
            +",CHSCHUMA = _t2.CHSCHUMA";
            +",CHTIPREC = _t2.CHTIPREC";
            +",CHDATSTA = _t2.CHDATSTA";
            +",CHPROTEC = _t2.CHPROTEC";
            +",CHPRODOC = _t2.CHPRODOC";
            +i_ccchkf;
            +" from "+i_cTable+" CUDTETRH, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="CUDTETRH.CHSERIAL = _t2.CHSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CUDTETRH, "+i_cQueryTable+" _t2 set ";
            +"CUDTETRH.CHPERFIS = _t2.CHPERFIS";
            +",CUDTETRH.CHPEREST = _t2.CHPEREST";
            +",CUDTETRH.CHCODFIS = _t2.CHCODFIS";
            +",CUDTETRH.CHDESCRI = _t2.CHDESCRI";
            +",CUDTETRH.CHCOGNOM = _t2.CHCOGNOM";
            +",CUDTETRH.CH__NOME = _t2.CH__NOME";
            +",CUDTETRH.CH_SESSO = _t2.CH_SESSO";
            +",CUDTETRH.CHDATNAS = _t2.CHDATNAS";
            +",CUDTETRH.CHCOMEST = _t2.CHCOMEST";
            +",CUDTETRH.CHPRONAS = _t2.CHPRONAS";
            +",CUDTETRH.CHCATPAR = _t2.CHCATPAR";
            +",CUDTETRH.CHEVEECC = _t2.CHEVEECC";
            +",CUDTETRH.CHCOMUNE = _t2.CHCOMUNE";
            +",CUDTETRH.CHPROVIN = _t2.CHPROVIN";
            +",CUDTETRH.CHCODCOM = _t2.CHCODCOM";
            +",CUDTETRH.CHCOFISC = _t2.CHCOFISC";
            +",CUDTETRH.CHCITEST = _t2.CHCITEST";
            +",CUDTETRH.CHINDEST = _t2.CHINDEST";
            +",CUDTETRH.CHSTAEST = _t2.CHSTAEST";
            +",CUDTETRH.CHSCLGEN = _t2.CHSCLGEN";
            +",CUDTETRH.CHDAVERI = _t2.CHDAVERI";
            +",CUDTETRH.CHSCHUMA = _t2.CHSCHUMA";
            +",CUDTETRH.CHTIPREC = _t2.CHTIPREC";
            +",CUDTETRH.CHDATSTA = _t2.CHDATSTA";
            +",CUDTETRH.CHPROTEC = _t2.CHPROTEC";
            +",CUDTETRH.CHPRODOC = _t2.CHPRODOC";
            +Iif(Empty(i_ccchkf),"",",CUDTETRH.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="CUDTETRH.CHSERIAL = t2.CHSERIAL";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CUDTETRH set (";
            +"CHPERFIS,";
            +"CHPEREST,";
            +"CHCODFIS,";
            +"CHDESCRI,";
            +"CHCOGNOM,";
            +"CH__NOME,";
            +"CH_SESSO,";
            +"CHDATNAS,";
            +"CHCOMEST,";
            +"CHPRONAS,";
            +"CHCATPAR,";
            +"CHEVEECC,";
            +"CHCOMUNE,";
            +"CHPROVIN,";
            +"CHCODCOM,";
            +"CHCOFISC,";
            +"CHCITEST,";
            +"CHINDEST,";
            +"CHSTAEST,";
            +"CHSCLGEN,";
            +"CHDAVERI,";
            +"CHSCHUMA,";
            +"CHTIPREC,";
            +"CHDATSTA,";
            +"CHPROTEC,";
            +"CHPRODOC";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.CHPERFIS,";
            +"t2.CHPEREST,";
            +"t2.CHCODFIS,";
            +"t2.CHDESCRI,";
            +"t2.CHCOGNOM,";
            +"t2.CH__NOME,";
            +"t2.CH_SESSO,";
            +"t2.CHDATNAS,";
            +"t2.CHCOMEST,";
            +"t2.CHPRONAS,";
            +"t2.CHCATPAR,";
            +"t2.CHEVEECC,";
            +"t2.CHCOMUNE,";
            +"t2.CHPROVIN,";
            +"t2.CHCODCOM,";
            +"t2.CHCOFISC,";
            +"t2.CHCITEST,";
            +"t2.CHINDEST,";
            +"t2.CHSTAEST,";
            +"t2.CHSCLGEN,";
            +"t2.CHDAVERI,";
            +"t2.CHSCHUMA,";
            +"t2.CHTIPREC,";
            +"t2.CHDATSTA,";
            +"t2.CHPROTEC,";
            +"t2.CHPRODOC";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="CUDTETRH.CHSERIAL = _t2.CHSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CUDTETRH set ";
            +"CHPERFIS = _t2.CHPERFIS";
            +",CHPEREST = _t2.CHPEREST";
            +",CHCODFIS = _t2.CHCODFIS";
            +",CHDESCRI = _t2.CHDESCRI";
            +",CHCOGNOM = _t2.CHCOGNOM";
            +",CH__NOME = _t2.CH__NOME";
            +",CH_SESSO = _t2.CH_SESSO";
            +",CHDATNAS = _t2.CHDATNAS";
            +",CHCOMEST = _t2.CHCOMEST";
            +",CHPRONAS = _t2.CHPRONAS";
            +",CHCATPAR = _t2.CHCATPAR";
            +",CHEVEECC = _t2.CHEVEECC";
            +",CHCOMUNE = _t2.CHCOMUNE";
            +",CHPROVIN = _t2.CHPROVIN";
            +",CHCODCOM = _t2.CHCODCOM";
            +",CHCOFISC = _t2.CHCOFISC";
            +",CHCITEST = _t2.CHCITEST";
            +",CHINDEST = _t2.CHINDEST";
            +",CHSTAEST = _t2.CHSTAEST";
            +",CHSCLGEN = _t2.CHSCLGEN";
            +",CHDAVERI = _t2.CHDAVERI";
            +",CHSCHUMA = _t2.CHSCHUMA";
            +",CHTIPREC = _t2.CHTIPREC";
            +",CHDATSTA = _t2.CHDATSTA";
            +",CHPROTEC = _t2.CHPROTEC";
            +",CHPRODOC = _t2.CHPRODOC";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".CHSERIAL = "+i_cQueryTable+".CHSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CHPERFIS = (select CHPERFIS from "+i_cQueryTable+" where "+i_cWhere+")";
            +",CHPEREST = (select CHPEREST from "+i_cQueryTable+" where "+i_cWhere+")";
            +",CHCODFIS = (select CHCODFIS from "+i_cQueryTable+" where "+i_cWhere+")";
            +",CHDESCRI = (select CHDESCRI from "+i_cQueryTable+" where "+i_cWhere+")";
            +",CHCOGNOM = (select CHCOGNOM from "+i_cQueryTable+" where "+i_cWhere+")";
            +",CH__NOME = (select CH__NOME from "+i_cQueryTable+" where "+i_cWhere+")";
            +",CH_SESSO = (select CH_SESSO from "+i_cQueryTable+" where "+i_cWhere+")";
            +",CHDATNAS = (select CHDATNAS from "+i_cQueryTable+" where "+i_cWhere+")";
            +",CHCOMEST = (select CHCOMEST from "+i_cQueryTable+" where "+i_cWhere+")";
            +",CHPRONAS = (select CHPRONAS from "+i_cQueryTable+" where "+i_cWhere+")";
            +",CHCATPAR = (select CHCATPAR from "+i_cQueryTable+" where "+i_cWhere+")";
            +",CHEVEECC = (select CHEVEECC from "+i_cQueryTable+" where "+i_cWhere+")";
            +",CHCOMUNE = (select CHCOMUNE from "+i_cQueryTable+" where "+i_cWhere+")";
            +",CHPROVIN = (select CHPROVIN from "+i_cQueryTable+" where "+i_cWhere+")";
            +",CHCODCOM = (select CHCODCOM from "+i_cQueryTable+" where "+i_cWhere+")";
            +",CHCOFISC = (select CHCOFISC from "+i_cQueryTable+" where "+i_cWhere+")";
            +",CHCITEST = (select CHCITEST from "+i_cQueryTable+" where "+i_cWhere+")";
            +",CHINDEST = (select CHINDEST from "+i_cQueryTable+" where "+i_cWhere+")";
            +",CHSTAEST = (select CHSTAEST from "+i_cQueryTable+" where "+i_cWhere+")";
            +",CHSCLGEN = (select CHSCLGEN from "+i_cQueryTable+" where "+i_cWhere+")";
            +",CHDAVERI = (select CHDAVERI from "+i_cQueryTable+" where "+i_cWhere+")";
            +",CHSCHUMA = (select CHSCHUMA from "+i_cQueryTable+" where "+i_cWhere+")";
            +",CHTIPREC = (select CHTIPREC from "+i_cQueryTable+" where "+i_cWhere+")";
            +",CHDATSTA = (select CHDATSTA from "+i_cQueryTable+" where "+i_cWhere+")";
            +",CHPROTEC = (select CHPROTEC from "+i_cQueryTable+" where "+i_cWhere+")";
            +",CHPRODOC = (select CHPRODOC from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    if this.w_OLDVERSIONE
      * --- Write into TMPDATIESTRATTI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMPDATIESTRATTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPDATIESTRATTI_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="CHSERIAL,MRSERIAL"
        do vq_exec with 'GSRI11BCU',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPDATIESTRATTI_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="TMPDATIESTRATTI.CHSERIAL = _t2.CHSERIAL";
                +" and "+"TMPDATIESTRATTI.MRSERIAL = _t2.MRSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CPROWNUM = _t2.CPROWNUM";
            +i_ccchkf;
            +" from "+i_cTable+" TMPDATIESTRATTI, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="TMPDATIESTRATTI.CHSERIAL = _t2.CHSERIAL";
                +" and "+"TMPDATIESTRATTI.MRSERIAL = _t2.MRSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI, "+i_cQueryTable+" _t2 set ";
            +"TMPDATIESTRATTI.CPROWNUM = _t2.CPROWNUM";
            +Iif(Empty(i_ccchkf),"",",TMPDATIESTRATTI.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="TMPDATIESTRATTI.CHSERIAL = t2.CHSERIAL";
                +" and "+"TMPDATIESTRATTI.MRSERIAL = t2.MRSERIAL";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI set (";
            +"CPROWNUM";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.CPROWNUM";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="TMPDATIESTRATTI.CHSERIAL = _t2.CHSERIAL";
                +" and "+"TMPDATIESTRATTI.MRSERIAL = _t2.MRSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI set ";
            +"CPROWNUM = _t2.CPROWNUM";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".CHSERIAL = "+i_cQueryTable+".CHSERIAL";
                +" and "+i_cTable+".MRSERIAL = "+i_cQueryTable+".MRSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CPROWNUM = (select CPROWNUM from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    else
      * --- Write into TMPDATIESTRATTI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMPDATIESTRATTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPDATIESTRATTI_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="CHSERIAL,MRSERIAL"
        do vq_exec with 'GSRI12BCU',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPDATIESTRATTI_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="TMPDATIESTRATTI.CHSERIAL = _t2.CHSERIAL";
                +" and "+"TMPDATIESTRATTI.MRSERIAL = _t2.MRSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CPROWNUM = _t2.CPROWNUM";
            +i_ccchkf;
            +" from "+i_cTable+" TMPDATIESTRATTI, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="TMPDATIESTRATTI.CHSERIAL = _t2.CHSERIAL";
                +" and "+"TMPDATIESTRATTI.MRSERIAL = _t2.MRSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI, "+i_cQueryTable+" _t2 set ";
            +"TMPDATIESTRATTI.CPROWNUM = _t2.CPROWNUM";
            +Iif(Empty(i_ccchkf),"",",TMPDATIESTRATTI.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="TMPDATIESTRATTI.CHSERIAL = t2.CHSERIAL";
                +" and "+"TMPDATIESTRATTI.MRSERIAL = t2.MRSERIAL";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI set (";
            +"CPROWNUM";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.CPROWNUM";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="TMPDATIESTRATTI.CHSERIAL = _t2.CHSERIAL";
                +" and "+"TMPDATIESTRATTI.MRSERIAL = _t2.MRSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI set ";
            +"CPROWNUM = _t2.CPROWNUM";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".CHSERIAL = "+i_cQueryTable+".CHSERIAL";
                +" and "+i_cTable+".MRSERIAL = "+i_cQueryTable+".MRSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CPROWNUM = (select CPROWNUM from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- Nel caso in cui siano presenti record che aggiornano la tabella Datestrh
    *     il numero di riga deve essere ricalcolato
    if this.w_NUMMOVAGG<>0
      * --- Write into TMPDATIESTRATTI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMPDATIESTRATTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPDATIESTRATTI_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="CHSERIAL,MRSERIAL"
        do vq_exec with 'GSRI17BCU',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPDATIESTRATTI_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="TMPDATIESTRATTI.CHSERIAL = _t2.CHSERIAL";
                +" and "+"TMPDATIESTRATTI.MRSERIAL = _t2.MRSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CPROWNUM = _t2.CPROWNUM";
            +i_ccchkf;
            +" from "+i_cTable+" TMPDATIESTRATTI, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="TMPDATIESTRATTI.CHSERIAL = _t2.CHSERIAL";
                +" and "+"TMPDATIESTRATTI.MRSERIAL = _t2.MRSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI, "+i_cQueryTable+" _t2 set ";
            +"TMPDATIESTRATTI.CPROWNUM = _t2.CPROWNUM";
            +Iif(Empty(i_ccchkf),"",",TMPDATIESTRATTI.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="TMPDATIESTRATTI.CHSERIAL = t2.CHSERIAL";
                +" and "+"TMPDATIESTRATTI.MRSERIAL = t2.MRSERIAL";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI set (";
            +"CPROWNUM";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.CPROWNUM";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="TMPDATIESTRATTI.CHSERIAL = _t2.CHSERIAL";
                +" and "+"TMPDATIESTRATTI.MRSERIAL = _t2.MRSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDATIESTRATTI set ";
            +"CPROWNUM = _t2.CPROWNUM";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".CHSERIAL = "+i_cQueryTable+".CHSERIAL";
                +" and "+i_cTable+".MRSERIAL = "+i_cQueryTable+".MRSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CPROWNUM = (select CPROWNUM from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- Insert into CUDTETDH
    i_nConn=i_TableProp[this.CUDTETDH_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CUDTETDH_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\rite\exe\query\gsri13bcu",this.CUDTETDH_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Calcolo i totali per Anno-Codice conto-Causale Prestazione
    * --- Create temporary table TMPTOTRECH
    i_nIdx=cp_AddTableDef('TMPTOTRECH') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('..\rite\exe\query\gsri24bcu',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPTOTRECH_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Aggiorno il progressivo delle somme non soggette
    if this.w_OLDVERSIONE
      * --- Write into TMPTOTRECH
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMPTOTRECH_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPTOTRECH_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="THSERIAL,THTOT006"
        do vq_exec with 'GSRI29BCU',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPTOTRECH_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="TMPTOTRECH.THSERIAL = _t2.THSERIAL";
                +" and "+"TMPTOTRECH.THTOT006 = _t2.THTOT006";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"THPRGSNS = _t2.THPRGSNS";
            +i_ccchkf;
            +" from "+i_cTable+" TMPTOTRECH, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="TMPTOTRECH.THSERIAL = _t2.THSERIAL";
                +" and "+"TMPTOTRECH.THTOT006 = _t2.THTOT006";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPTOTRECH, "+i_cQueryTable+" _t2 set ";
            +"TMPTOTRECH.THPRGSNS = _t2.THPRGSNS";
            +Iif(Empty(i_ccchkf),"",",TMPTOTRECH.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="TMPTOTRECH.THSERIAL = t2.THSERIAL";
                +" and "+"TMPTOTRECH.THTOT006 = t2.THTOT006";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPTOTRECH set (";
            +"THPRGSNS";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.THPRGSNS";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="TMPTOTRECH.THSERIAL = _t2.THSERIAL";
                +" and "+"TMPTOTRECH.THTOT006 = _t2.THTOT006";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPTOTRECH set ";
            +"THPRGSNS = _t2.THPRGSNS";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".THSERIAL = "+i_cQueryTable+".THSERIAL";
                +" and "+i_cTable+".THTOT006 = "+i_cQueryTable+".THTOT006";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"THPRGSNS = (select THPRGSNS from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    else
      * --- Write into TMPTOTRECH
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMPTOTRECH_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPTOTRECH_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="THSERIAL,THTOT006"
        do vq_exec with 'GSRI25BCU',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPTOTRECH_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="TMPTOTRECH.THSERIAL = _t2.THSERIAL";
                +" and "+"TMPTOTRECH.THTOT006 = _t2.THTOT006";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"THPRGSNS = _t2.THPRGSNS";
            +i_ccchkf;
            +" from "+i_cTable+" TMPTOTRECH, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="TMPTOTRECH.THSERIAL = _t2.THSERIAL";
                +" and "+"TMPTOTRECH.THTOT006 = _t2.THTOT006";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPTOTRECH, "+i_cQueryTable+" _t2 set ";
            +"TMPTOTRECH.THPRGSNS = _t2.THPRGSNS";
            +Iif(Empty(i_ccchkf),"",",TMPTOTRECH.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="TMPTOTRECH.THSERIAL = t2.THSERIAL";
                +" and "+"TMPTOTRECH.THTOT006 = t2.THTOT006";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPTOTRECH set (";
            +"THPRGSNS";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.THPRGSNS";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="TMPTOTRECH.THSERIAL = _t2.THSERIAL";
                +" and "+"TMPTOTRECH.THTOT006 = _t2.THTOT006";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPTOTRECH set ";
            +"THPRGSNS = _t2.THPRGSNS";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".THSERIAL = "+i_cQueryTable+".THSERIAL";
                +" and "+i_cTable+".THTOT006 = "+i_cQueryTable+".THTOT006";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"THPRGSNS = (select THPRGSNS from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- Aggiorno i totali suddivisi per Somme non Soggette
    * --- Write into CUDTETRH
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.CUDTETRH_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CUDTETRH_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="CHSERIAL"
      do vq_exec with 'Gsri26bcu',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.CUDTETRH_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="CUDTETRH.CHSERIAL = _t2.CHSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CHTOT004 = _t2.CHTOT004";
          +",CHTOT005 = _t2.CHTOT005";
          +",CHTOT006 = _t2.CHTOT006";
          +",CHTOT008 = _t2.CHTOT008";
          +",CHTOT009 = _t2.CHTOT009";
          +",CHTOT020 = _t2.CHTOT020";
          +",CHTOT021 = _t2.CHTOT021";
          +",CHTOT034 = _t2.CHTOT034";
          +",CHTOT035 = _t2.CHTOT035";
          +",CHPROMOD = _t2.THPRGSNS";
          +",CHTOT002 = _t2.CHTOT002";
      +",CHATT1MOD ="+cp_NullLink(cp_ToStrODBC("N"),'CUDTETRH','CHATT1MOD');
      +",CHATT2MOD ="+cp_NullLink(cp_ToStrODBC("N"),'CUDTETRH','CHATT2MOD');
          +",CHTOT041 = _t2.CHTOT041";
          +",CHTOT042 = _t2.CHTOT042";
          +i_ccchkf;
          +" from "+i_cTable+" CUDTETRH, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="CUDTETRH.CHSERIAL = _t2.CHSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CUDTETRH, "+i_cQueryTable+" _t2 set ";
          +"CUDTETRH.CHTOT004 = _t2.CHTOT004";
          +",CUDTETRH.CHTOT005 = _t2.CHTOT005";
          +",CUDTETRH.CHTOT006 = _t2.CHTOT006";
          +",CUDTETRH.CHTOT008 = _t2.CHTOT008";
          +",CUDTETRH.CHTOT009 = _t2.CHTOT009";
          +",CUDTETRH.CHTOT020 = _t2.CHTOT020";
          +",CUDTETRH.CHTOT021 = _t2.CHTOT021";
          +",CUDTETRH.CHTOT034 = _t2.CHTOT034";
          +",CUDTETRH.CHTOT035 = _t2.CHTOT035";
          +",CUDTETRH.CHPROMOD = _t2.THPRGSNS";
          +",CUDTETRH.CHTOT002 = _t2.CHTOT002";
      +",CUDTETRH.CHATT1MOD ="+cp_NullLink(cp_ToStrODBC("N"),'CUDTETRH','CHATT1MOD');
      +",CUDTETRH.CHATT2MOD ="+cp_NullLink(cp_ToStrODBC("N"),'CUDTETRH','CHATT2MOD');
          +",CUDTETRH.CHTOT041 = _t2.CHTOT041";
          +",CUDTETRH.CHTOT042 = _t2.CHTOT042";
          +Iif(Empty(i_ccchkf),"",",CUDTETRH.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="CUDTETRH.CHSERIAL = t2.CHSERIAL";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CUDTETRH set (";
          +"CHTOT004,";
          +"CHTOT005,";
          +"CHTOT006,";
          +"CHTOT008,";
          +"CHTOT009,";
          +"CHTOT020,";
          +"CHTOT021,";
          +"CHTOT034,";
          +"CHTOT035,";
          +"CHPROMOD,";
          +"CHTOT002,";
          +"CHATT1MOD,";
          +"CHATT2MOD,";
          +"CHTOT041,";
          +"CHTOT042";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.CHTOT004,";
          +"t2.CHTOT005,";
          +"t2.CHTOT006,";
          +"t2.CHTOT008,";
          +"t2.CHTOT009,";
          +"t2.CHTOT020,";
          +"t2.CHTOT021,";
          +"t2.CHTOT034,";
          +"t2.CHTOT035,";
          +"t2.THPRGSNS,";
          +"t2.CHTOT002,";
          +cp_NullLink(cp_ToStrODBC("N"),'CUDTETRH','CHATT1MOD')+",";
          +cp_NullLink(cp_ToStrODBC("N"),'CUDTETRH','CHATT2MOD')+",";
          +"t2.CHTOT041,";
          +"t2.CHTOT042";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="CUDTETRH.CHSERIAL = _t2.CHSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CUDTETRH set ";
          +"CHTOT004 = _t2.CHTOT004";
          +",CHTOT005 = _t2.CHTOT005";
          +",CHTOT006 = _t2.CHTOT006";
          +",CHTOT008 = _t2.CHTOT008";
          +",CHTOT009 = _t2.CHTOT009";
          +",CHTOT020 = _t2.CHTOT020";
          +",CHTOT021 = _t2.CHTOT021";
          +",CHTOT034 = _t2.CHTOT034";
          +",CHTOT035 = _t2.CHTOT035";
          +",CHPROMOD = _t2.THPRGSNS";
          +",CHTOT002 = _t2.CHTOT002";
      +",CHATT1MOD ="+cp_NullLink(cp_ToStrODBC("N"),'CUDTETRH','CHATT1MOD');
      +",CHATT2MOD ="+cp_NullLink(cp_ToStrODBC("N"),'CUDTETRH','CHATT2MOD');
          +",CHTOT041 = _t2.CHTOT041";
          +",CHTOT042 = _t2.CHTOT042";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".CHSERIAL = "+i_cQueryTable+".CHSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CHTOT004 = (select CHTOT004 from "+i_cQueryTable+" where "+i_cWhere+")";
          +",CHTOT005 = (select CHTOT005 from "+i_cQueryTable+" where "+i_cWhere+")";
          +",CHTOT006 = (select CHTOT006 from "+i_cQueryTable+" where "+i_cWhere+")";
          +",CHTOT008 = (select CHTOT008 from "+i_cQueryTable+" where "+i_cWhere+")";
          +",CHTOT009 = (select CHTOT009 from "+i_cQueryTable+" where "+i_cWhere+")";
          +",CHTOT020 = (select CHTOT020 from "+i_cQueryTable+" where "+i_cWhere+")";
          +",CHTOT021 = (select CHTOT021 from "+i_cQueryTable+" where "+i_cWhere+")";
          +",CHTOT034 = (select CHTOT034 from "+i_cQueryTable+" where "+i_cWhere+")";
          +",CHTOT035 = (select CHTOT035 from "+i_cQueryTable+" where "+i_cWhere+")";
          +",CHPROMOD = (select THPRGSNS from "+i_cQueryTable+" where "+i_cWhere+")";
          +",CHTOT002 = (select CHTOT002 from "+i_cQueryTable+" where "+i_cWhere+")";
      +",CHATT1MOD ="+cp_NullLink(cp_ToStrODBC("N"),'CUDTETRH','CHATT1MOD');
      +",CHATT2MOD ="+cp_NullLink(cp_ToStrODBC("N"),'CUDTETRH','CHATT2MOD');
          +",CHTOT041 = (select CHTOT041 from "+i_cQueryTable+" where "+i_cWhere+")";
          +",CHTOT042 = (select CHTOT042 from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Write into CUDTETRH
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.CUDTETRH_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CUDTETRH_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="CHSERIAL"
      do vq_exec with 'Gsri27bcu',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.CUDTETRH_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="CUDTETRH.CHSERIAL = _t2.CHSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CHT1T005 = _t2.CHT1T005";
          +",CHT1T006 = _t2.CHT1T006";
          +",CHPR1MOD = _t2.THPRGSNS";
          +",CHT1T002 = _t2.CHT1T002";
          +",CHATT1MOD = _t2.CHATT1MOD";
          +i_ccchkf;
          +" from "+i_cTable+" CUDTETRH, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="CUDTETRH.CHSERIAL = _t2.CHSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CUDTETRH, "+i_cQueryTable+" _t2 set ";
          +"CUDTETRH.CHT1T005 = _t2.CHT1T005";
          +",CUDTETRH.CHT1T006 = _t2.CHT1T006";
          +",CUDTETRH.CHPR1MOD = _t2.THPRGSNS";
          +",CUDTETRH.CHT1T002 = _t2.CHT1T002";
          +",CUDTETRH.CHATT1MOD = _t2.CHATT1MOD";
          +Iif(Empty(i_ccchkf),"",",CUDTETRH.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="CUDTETRH.CHSERIAL = t2.CHSERIAL";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CUDTETRH set (";
          +"CHT1T005,";
          +"CHT1T006,";
          +"CHPR1MOD,";
          +"CHT1T002,";
          +"CHATT1MOD";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.CHT1T005,";
          +"t2.CHT1T006,";
          +"t2.THPRGSNS,";
          +"t2.CHT1T002,";
          +"t2.CHATT1MOD";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="CUDTETRH.CHSERIAL = _t2.CHSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CUDTETRH set ";
          +"CHT1T005 = _t2.CHT1T005";
          +",CHT1T006 = _t2.CHT1T006";
          +",CHPR1MOD = _t2.THPRGSNS";
          +",CHT1T002 = _t2.CHT1T002";
          +",CHATT1MOD = _t2.CHATT1MOD";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".CHSERIAL = "+i_cQueryTable+".CHSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CHT1T005 = (select CHT1T005 from "+i_cQueryTable+" where "+i_cWhere+")";
          +",CHT1T006 = (select CHT1T006 from "+i_cQueryTable+" where "+i_cWhere+")";
          +",CHPR1MOD = (select THPRGSNS from "+i_cQueryTable+" where "+i_cWhere+")";
          +",CHT1T002 = (select CHT1T002 from "+i_cQueryTable+" where "+i_cWhere+")";
          +",CHATT1MOD = (select CHATT1MOD from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Write into CUDTETRH
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.CUDTETRH_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CUDTETRH_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="CHSERIAL"
      do vq_exec with 'Gsri28bcu',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.CUDTETRH_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="CUDTETRH.CHSERIAL = _t2.CHSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CHT2T005 = _t2.CHT2T005";
          +",CHT2T006 = _t2.CHT2T006";
          +",CHPR2MOD = _t2.THPRGSNS";
          +",CHT2T002 = _t2.CHT2T002";
          +",CHATT2MOD = _t2.CHATT2MOD";
          +i_ccchkf;
          +" from "+i_cTable+" CUDTETRH, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="CUDTETRH.CHSERIAL = _t2.CHSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CUDTETRH, "+i_cQueryTable+" _t2 set ";
          +"CUDTETRH.CHT2T005 = _t2.CHT2T005";
          +",CUDTETRH.CHT2T006 = _t2.CHT2T006";
          +",CUDTETRH.CHPR2MOD = _t2.THPRGSNS";
          +",CUDTETRH.CHT2T002 = _t2.CHT2T002";
          +",CUDTETRH.CHATT2MOD = _t2.CHATT2MOD";
          +Iif(Empty(i_ccchkf),"",",CUDTETRH.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="CUDTETRH.CHSERIAL = t2.CHSERIAL";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CUDTETRH set (";
          +"CHT2T005,";
          +"CHT2T006,";
          +"CHPR2MOD,";
          +"CHT2T002,";
          +"CHATT2MOD";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.CHT2T005,";
          +"t2.CHT2T006,";
          +"t2.THPRGSNS,";
          +"t2.CHT2T002,";
          +"t2.CHATT2MOD";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="CUDTETRH.CHSERIAL = _t2.CHSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CUDTETRH set ";
          +"CHT2T005 = _t2.CHT2T005";
          +",CHT2T006 = _t2.CHT2T006";
          +",CHPR2MOD = _t2.THPRGSNS";
          +",CHT2T002 = _t2.CHT2T002";
          +",CHATT2MOD = _t2.CHATT2MOD";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".CHSERIAL = "+i_cQueryTable+".CHSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CHT2T005 = (select CHT2T005 from "+i_cQueryTable+" where "+i_cWhere+")";
          +",CHT2T006 = (select CHT2T006 from "+i_cQueryTable+" where "+i_cWhere+")";
          +",CHPR2MOD = (select THPRGSNS from "+i_cQueryTable+" where "+i_cWhere+")";
          +",CHT2T002 = (select CHT2T002 from "+i_cQueryTable+" where "+i_cWhere+")";
          +",CHATT2MOD = (select CHATT2MOD from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Write into CUDTETRH
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.CUDTETRH_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CUDTETRH_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="CHSERIAL"
      do vq_exec with 'Gsri33bcu',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.CUDTETRH_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="CUDTETRH.CHSERIAL = _t2.CHSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CHT3T005 = _t2.CHT3T005";
          +",CHT3T006 = _t2.CHT3T006";
          +",CHPR3MOD = _t2.THPRGSNS";
          +",CHT3T002 = _t2.CHT3T002";
          +",CHATT3MOD = _t2.CHATT3MOD";
          +i_ccchkf;
          +" from "+i_cTable+" CUDTETRH, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="CUDTETRH.CHSERIAL = _t2.CHSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CUDTETRH, "+i_cQueryTable+" _t2 set ";
          +"CUDTETRH.CHT3T005 = _t2.CHT3T005";
          +",CUDTETRH.CHT3T006 = _t2.CHT3T006";
          +",CUDTETRH.CHPR3MOD = _t2.THPRGSNS";
          +",CUDTETRH.CHT3T002 = _t2.CHT3T002";
          +",CUDTETRH.CHATT3MOD = _t2.CHATT3MOD";
          +Iif(Empty(i_ccchkf),"",",CUDTETRH.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="CUDTETRH.CHSERIAL = t2.CHSERIAL";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CUDTETRH set (";
          +"CHT3T005,";
          +"CHT3T006,";
          +"CHPR3MOD,";
          +"CHT3T002,";
          +"CHATT3MOD";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.CHT3T005,";
          +"t2.CHT3T006,";
          +"t2.THPRGSNS,";
          +"t2.CHT3T002,";
          +"t2.CHATT3MOD";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="CUDTETRH.CHSERIAL = _t2.CHSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CUDTETRH set ";
          +"CHT3T005 = _t2.CHT3T005";
          +",CHT3T006 = _t2.CHT3T006";
          +",CHPR3MOD = _t2.THPRGSNS";
          +",CHT3T002 = _t2.CHT3T002";
          +",CHATT3MOD = _t2.CHATT3MOD";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".CHSERIAL = "+i_cQueryTable+".CHSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CHT3T005 = (select CHT3T005 from "+i_cQueryTable+" where "+i_cWhere+")";
          +",CHT3T006 = (select CHT3T006 from "+i_cQueryTable+" where "+i_cWhere+")";
          +",CHPR3MOD = (select THPRGSNS from "+i_cQueryTable+" where "+i_cWhere+")";
          +",CHT3T002 = (select CHT3T002 from "+i_cQueryTable+" where "+i_cWhere+")";
          +",CHATT3MOD = (select CHATT3MOD from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Inserisco i dati previdenziali nel caso i totali dei contributi previdenziali siano
    *     valorizzati
    * --- Insert into CUDDPTDH
    i_nConn=i_TableProp[this.CUDDPTDH_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CUDDPTDH_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"Gsri41bcu.vqr",this.CUDDPTDH_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    VQ_EXEC("..\rite\exe\query\gsri18bcu", this, "__TMP1__")
    if USED("__TMP1__")
      Select __TMP1__
      COUNT FOR NEWESCLRITE="S" TO this.w_NEWESCLRITE
      COUNT FOR AGGESCLRITE="S" TO this.w_AGGESCLRITE
    endif
    this.w_oMESS=createobject("AH_Message")
    this.w_oMESS.AddMsgPartNL("Generazione completata.")     
    if this.w_NUMMOV<>0
      this.w_oPART = this.w_oMESS.AddMsgPartNL("Creati %1 certificazioni uniche (di cui  n. %2 da verificare) da n. %3 movimenti ritenute")
      this.w_oPART.AddParam(ALLTRIM(STR(this.w_NUMMOV)))     
      if this.oParentObject.w_TIPOPERAZ $ "AS"
        this.w_oPART.AddParam(ALLTRIM(STR(this.w_NUMMOV)))     
      else
        this.w_oPART.AddParam(ALLTRIM(STR(this.w_NEWESCLRITE)))     
      endif
      this.w_oPART.AddParam(ALLTRIM(STR(this.w_NEWMOVRITE)))     
    endif
    if this.w_NUMMOVAGG<>0
      this.w_oPART = this.w_oMESS.AddMsgPartNL("Aggiornati %1 certificazioni uniche (di cui  n. %2 da verificare) da n. %3 movimenti ritenute")
      this.w_oPART.AddParam(ALLTRIM(STR(this.w_NUMMOVAGG)))     
      if this.oParentObject.w_TIPOPERAZ $ "AS"
        this.w_oPART.AddParam(ALLTRIM(STR(this.w_NUMMOVAGG)))     
      else
        this.w_oPART.AddParam(ALLTRIM(STR(this.w_AGGESCLRITE)))     
      endif
      this.w_oPART.AddParam(ALLTRIM(STR(this.w_AGGMOVRITE)))     
    endif
    if this.w_NUMMOVELI<>0
      this.w_oPART = this.w_oMESS.AddMsgPartNL("Scartati n. %1 movimenti ritenute")
      this.w_oPART.AddParam(ALLTRIM(STR(this.w_NUMMOVELI)))     
    endif
    if this.w_NUMESCLRIGA<>0
      this.w_oPART = this.w_oMESS.AddMsgPartNL("Esclusi n. %1 movimenti ritenute")
      this.w_oPART.AddParam(ALLTRIM(STR(this.w_NUMESCLRIGA)))     
      * --- Nel caso in cui esistano movimenti ritenute duplicati perch� gi� estratti
      *     devo aggiungerli al cursore di stampa 
    endif
    this.w_oMESS.AH_ErrorMsg(48)     
    if RECCOUNT("ElencoEsclusioni")>0 AND ah_YesNo("Si vuole stampare l'elenco delle anomalie riscontrate?")
      Select * from ElencoEsclusioni into cursor __TMP__
      Cp_Chprn("..\rite\exe\query\gsri_bcu", , this.oParentObject)
    endif
    return


  proc Init(oParentObject,pParame)
    this.pParame=pParame
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,8)]
    this.cWorkTables[1]='DATESDRH'
    this.cWorkTables[2]='DATESTRH'
    this.cWorkTables[3]='*TMPDATIESTRATTI'
    this.cWorkTables[4]='DATESDST'
    this.cWorkTables[5]='CUDTETDH'
    this.cWorkTables[6]='CUDTETRH'
    this.cWorkTables[7]='*TMPTOTRECH'
    this.cWorkTables[8]='CUDDPTDH'
    return(this.OpenAllTables(8))

  proc CloseCursors()
    if used('_Curs_GSRI7BED')
      use in _Curs_GSRI7BED
    endif
    if used('_Curs_GSRI16BED')
      use in _Curs_GSRI16BED
    endif
    if used('_Curs_GSRI8BED')
      use in _Curs_GSRI8BED
    endif
    if used('_Curs_Gsri40bed')
      use in _Curs_Gsri40bed
    endif
    if used('_Curs_GSRI27BED')
      use in _Curs_GSRI27BED
    endif
    if used('_Curs_GSRI30BED')
      use in _Curs_GSRI30BED
    endif
    if used('_Curs_GSRI7BCU')
      use in _Curs_GSRI7BCU
    endif
    if used('_Curs_GSRI16BCU')
      use in _Curs_GSRI16BCU
    endif
    if used('_Curs_GSRI8BCU')
      use in _Curs_GSRI8BCU
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParame"
endproc
